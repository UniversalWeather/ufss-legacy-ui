﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.CharterQuoteService;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.IO;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class QuoteOnFile : BaseSecuredPage
    {
        #region Declarations

        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        private ExceptionManager exManager;
        private delegate void SaveSession();
        #endregion


#region comments
        //Defect# 2565 CQ > Copied Quotes > Exceptions Fixed By : Prabhu
        //Description:   see attached. ffazeli1/fpk_123
        //Ref File 64, quotes 2, 3, & 4 - The Exception displayed applies only to Quote 1 and should not be carried on copied quotes...unless valid.

 
#endregion

        /// <summary>
        /// Wire up the SaveCharterQuoteToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        SaveSession SaveCharterQuoteFileSession = new SaveSession(SaveCharterQuoteToSession);
                        Master.SaveToSession = SaveCharterQuoteFileSession;

                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;

                        Master._DeleteClick += Delete_Click;
                        Master._SaveClick += Save_Click;
                        Master._CancelClick += Cancel_Click;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditFile, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgQuote.ClientID));

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnConfirmImgDeleteYes, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(radbtnCQDelete, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgQuote, dgQuote, RadAjaxLoadingPanel1);
                        //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgQuote, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgQuote, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);


                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(fuCQ, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(ddlCQFileName, DivExternalForm, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnAddQuote, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeleteQuote, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        if (!IsPostBack)
                        {
                            hdnCalculateQuote.Value = string.Empty;

                            Page.ClientScript.RegisterStartupScript(this.GetType(), "DetectBrowser", "GetBrowserName();", true);
                            //Clear Existing Field Datas
                            ClearForm();
                            LoadLostBusiness();
                            EnableForm(false);

                            BindCharterQuoteFromSession();

                            //Default Settings
                            ControlEnable("TextBox", tbLastSubmittedDt, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbLastAcceptedDt, false, "inpt_non_edit text70");

                        }
                        else
                        {
                            string eventArgument = (this.Request["__EVENTARGUMENT"] == null) ? string.Empty : this.Request["__EVENTARGUMENT"];
                            if (eventArgument == "LoadCQImage")
                                LoadCQImage();
                            else if (eventArgument == "LoadCQImageFromSession")
                                LoadCQImageFromSession();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    ApplyPermissions();
                    SetSourceByValue(rblSource.SelectedValue);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        #region AJAX Events
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            Int64 QuoteNumInProgress = 1;
            if (Session[Master.CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[Master.CQSessionKey];
                if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                    QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                Master.CQSelectedQuoteNum = QuoteNumInProgress.ToString(); // Session[Master.CQSelectedQuoteNum].ToString();
                Master.ValidateQuote();
            }

            if (e.Argument.ToString() == "TailNo_TextChanged")
            {
                TailNo_TextChanged(sender, e);
                Session["CQConfirmCharterRate"] = "changed";
            }

            if (e.Argument.ToString() == "TypeCode_TextChanged")
                TypeCode_TextChanged(sender, e);
        }
        #endregion

        #region Quote Grid Events

        protected void Quote_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];

                        if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            BindQuoteGrid(false);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Quote_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem Item = (GridDataItem)e.Item;

                            // Set Charter Quote Source Field in Grid
                            if (Item["CQSource"].Text == "1")
                                Item["CQSource"].Text = "V";
                            else if (Item["CQSource"].Text == "2")
                                Item["CQSource"].Text = "I";
                            else if (Item["CQSource"].Text == "3")
                                Item["CQSource"].Text = "T";

                            // Format Last Accepted Date Field in Grid
                            if (Item["LastAcceptDT"] != null && !string.IsNullOrEmpty(Item["LastAcceptDT"].Text) && Item["LastAcceptDT"].Text != "&nbsp;")
                            {
                                string encodedDate = Microsoft.Security.Application.Encoder.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", Convert.ToDateTime(Item["LastAcceptDT"].Text)));
                                Item["LastAcceptDT"].Text = encodedDate;
                            }
                            // For Quote Status
                            if (Item.GetDataKeyValue("IsSubmitted") != null)
                            {
                                bool isSubmitted = Convert.ToBoolean(Item.GetDataKeyValue("IsSubmitted"));
                                if (isSubmitted)
                                    ((Label)Item["QuoteStatus"].FindControl("lbQuoteStatus")).Text = System.Web.HttpUtility.HtmlEncode("SUBMITTED");
                            }

                            if (Item.GetDataKeyValue("IsAccepted") != null)
                            {
                                bool isAccepted = Convert.ToBoolean(Item.GetDataKeyValue("IsAccepted"));
                                if (isAccepted)
                                    ((Label)Item["QuoteStatus"].FindControl("lbQuoteStatus")).Text = System.Web.HttpUtility.HtmlEncode("ACCEPTED");
                            }

                            // For showing Duration based on Compnay Profile
                            if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                            {
                                if (Item["FlightHoursTotal"] != null && Item["FlightHoursTotal"].Text != "&nbsp;")
                                    Item["FlightHoursTotal"].Text = Master.ConvertTenthsToMins(Math.Round(Convert.ToDecimal(Item["FlightHoursTotal"].Text), 1).ToString());
                            }
                            else
                            {
                                if (Item["FlightHoursTotal"] != null && Item["FlightHoursTotal"].Text != "&nbsp;")
                                    Item["FlightHoursTotal"].Text = Math.Round(Convert.ToDecimal(Item["FlightHoursTotal"].Text), 1).ToString();
                            }

                            Int64 TripId = 0;
                            if (Item.GetDataKeyValue("TripID") != null)
                                TripId = (Int64)Item.GetDataKeyValue("TripID");

                            if (TripId != 0)
                            {
                                using (PreflightService.PreflightServiceClient Service = new PreflightService.PreflightServiceClient())
                                {
                                    //Method will be changed once manager completed
                                    var objRetVal = Service.GetTrip(TripId);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        var objVal = objRetVal.EntityList[0];
                                        if (objVal.TripNUM != null)
                                            Item["TripNUM"].Text = System.Web.HttpUtility.HtmlEncode(objVal.TripNUM.ToString());
                                        if (objVal.TripStatus != null)
                                        {
                                            if (objVal.TripStatus.ToUpper() == "T")
                                                Item["TripStatus"].Text = "TRIPSHEET";
                                            else if (objVal.TripStatus.ToUpper() == "W")
                                                Item["TripStatus"].Text = "WORKSHEET";
                                            else if (objVal.TripStatus.ToUpper() == "U")
                                                Item["TripStatus"].Text = "UNFULFILLED";
                                            else if (objVal.TripStatus.ToUpper() == "X")
                                                Item["TripStatus"].Text = "CANCELED";
                                            else if (objVal.TripStatus.ToUpper() == "H")
                                                Item["TripStatus"].Text = "HOLD";
                                        }
                                        if (objVal.IsLog != null && objVal.IsLog == true)
                                        {

                                            using (PostflightService.PostflightServiceClient PostService = new PostflightService.PostflightServiceClient())
                                            {
                                                PostflightService.PostflightMain PostFlightMain = new PostflightService.PostflightMain();
                                                PostFlightMain.TripID = TripId;

                                                var Value = PostService.GetTrip(PostFlightMain);
                                                if (Value.ReturnFlag)
                                                {
                                                    if (Value.EntityInfo != null && Value.EntityInfo.LogNum != null)
                                                        Item["IsLog"].Text = System.Web.HttpUtility.HtmlEncode(Value.EntityInfo.LogNum.ToString());
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Item["IsLog"].Text = "0";
                                        }

                                    }
                                }
                            }

                            // For Exception Column
                            using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                            {
                                if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
                                {
                                    Int64 QuoteNumInProgress = 1;
                                    QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                    QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                    if (QuoteInProgress != null)
                                    {
                                        var ExceptionList = Service.GetQuoteExceptionList(QuoteInProgress.CQMainID);

                                        if (ExceptionList != null && ExceptionList.EntityList.Count() > 0)
                                        {
                                            if (Item["Exception"].FindControl("lbException") != null)
                                            {
                                                ((Label)Item["Exception"].FindControl("lbException")).Text = System.Web.HttpUtility.HtmlEncode("!");
                                                ((Label)Item["Exception"].FindControl("lbException")).ForeColor = System.Drawing.Color.Red;
                                            }
                                        }
                                        else
                                            ((Label)Item["Exception"].FindControl("lbException")).Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Quote_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                btnAddQuote_Click(sender, e);
                                break;
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                EnableForm(true);
                                GridEnable(true, false, false);
                                break;
                            case RadGrid.DeleteSelectedCommandName:
                                btnDeleteQuote_Click(sender, e);
                                break;
                            case "RowClick":
                                SaveCharterQuoteToSession();
                                BindQuoteGrid(true);
                                GridDataItem item = (GridDataItem)e.Item;
                                item.Selected = true;
                                ClearLabels();

                                LoadQuoteDetailsByNUM(Convert.ToInt32(item["QuoteNUM"].Text));
                                GridEnable(true, false, true);
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void btnAddQuote_Click(object sender, EventArgs e)
        {
            SaveCharterQuoteToSession();
            dgQuote.SelectedIndexes.Clear();
            ClearForm();
            EnableForm(true);
            GridEnable(true, false, false);

            rblSource.SelectedValue = "2";
            SetSourceByValue(rblSource.SelectedValue);

            int quoteCount = 0;

            rblQuoteStage.SelectedValue = "In Progress";

            FileRequest = (CQFile)Session[Master.CQSessionKey];
            if (FileRequest != null)
            {
                FileRequest.Mode = CQRequestActionMode.Edit;
                if (FileRequest.CQMains != null && FileRequest.CQMains.Where(x => x.IsDeleted == false).Count() > 0)
                {
                    quoteCount = FileRequest.CQMains.Where(x => x.IsDeleted == false).Max(x => x.QuoteNUM).Value;
                }
            }
            else
            {
                FileRequest = new CQFile();
                FileRequest.State = CQRequestEntityState.Added;
                FileRequest.QuoteDT = DateTime.Now;
                FileRequest.EstDepartureDT = DateTime.Now;

                //load default values for Homebase
                if (UserPrincipal.Identity != null && UserPrincipal.Identity._homeBaseId != null)
                {
                    FileRequest.HomebaseID = UserPrincipal.Identity._homeBaseId;
                    FileRequest.HomeBaseAirportID = UserPrincipal.Identity._airportId;
                }
            }

            quoteCount += 1;
            FileRequest.QuoteNumInProgress = quoteCount;

            CQMain newQuote = new CQMain();
            newQuote.State = CQRequestEntityState.Added;
            newQuote.IsDeleted = false;
            newQuote.QuoteNUM = quoteCount;
            newQuote.CQSource = Convert.ToInt64(rblSource.SelectedValue);
            newQuote.DiscountPercentageTotal = FileRequest.DiscountPercentage;

            if (FileRequest.CQMains == null)
                FileRequest.CQMains = new List<CQMain>();

            if (FileRequest.CQFileID != 0 && FileRequest.State != CQRequestEntityState.Deleted)
                FileRequest.State = CQRequestEntityState.Modified;
            newQuote.IsOverrideCost = false;
            newQuote.IsOverrideDiscountAMT = false;
            newQuote.IsOverrideDiscountPercentage = false;
            newQuote.IsOverrideLandingFee = false;
            newQuote.IsOverrideSegmentFee = false;
            newQuote.IsOverrideTaxes = false;
            newQuote.IsOverrideTotalQuote = false;
            newQuote.IsOverrideUsageAdj = false;

            newQuote.IsDailyTaxAdj = false;
            newQuote.IsLandingFeeTax = false;

            if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null)
            {
                if (UserPrincipal.Identity._fpSettings._IsAppliedTax == 1 || UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                    newQuote.IsTaxable = true;
                else
                    newQuote.IsTaxable = false;
            }
            else
                newQuote.IsTaxable = false;

            newQuote.CostTotal = 0.0M;
            newQuote.UsageAdjTotal = 0.0M;
            newQuote.SegmentFeeTotal = 0.0M;
            newQuote.DiscountPercentageTotal = 0.0M;
            newQuote.LandingFeeTotal = 0.0M;
            newQuote.DiscountAmtTotal = 0.0M;
            newQuote.TaxesTotal = 0.0M;
            newQuote.QuoteTotal = 0.0M;
            newQuote.FederalTax = UserPrincipal.Identity._fpSettings._CQFederalTax != null ? Convert.ToDecimal(UserPrincipal.Identity._fpSettings._CQFederalTax) : 0.0M;
            newQuote.QuoteRuralTax = UserPrincipal.Identity._fpSettings._RuralTax != null ? Convert.ToDecimal(UserPrincipal.Identity._fpSettings._RuralTax) : 0.0M;
            newQuote.StandardCrewDOM = 0.0M;
            newQuote.StandardCrewINTL = 0.0M;
            newQuote.SubtotalTotal = 0.0M;
            newQuote.MarginTotal = 0.0M;
            newQuote.MarginalPercentTotal = 0.0M;
            newQuote.FlightHoursTotal = 0.0M;
            newQuote.DaysTotal = 0.0M;
            newQuote.AverageFlightHoursTotal = 0.0M;
            newQuote.AdditionalFeeTotalD = 0.0M;
            newQuote.StdCrewRONTotal = 0.0M;
            newQuote.RemainingAmtTotal = 0.0M;
            newQuote.FlightChargeTotal = 0.0M;

            #region Populate Fleet based on Homebaseid
            if (UserPrincipal.Identity != null && UserPrincipal.Identity._homeBaseId != null)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                    var objRetVal = objDstsvc.GetFleetProfileList();
                    if (objRetVal.ReturnFlag)
                    {

                        Fleetlist = objRetVal.EntityList.Where(x => x.HomebaseID == UserPrincipal.Identity._homeBaseId && x.IsInActive == false).ToList();


                        if (Fleetlist != null && Fleetlist.Count == 1)
                        {
                            hdnCalculateQuote.Value = "true";
                            newQuote.FleetID = Fleetlist[0].FleetID;
                            Fleet objFleet = new Fleet { FleetID = Fleetlist[0].FleetID, TailNum = Fleetlist[0].TailNum };
                            newQuote.Fleet = objFleet;

                            tbTailNumber.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;

                            lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                            #region Aircraft
                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();

                            var objRetVal1 = objDstsvc.GetAircraftList();
                            if (objRetVal1.ReturnFlag)
                            {
                                AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID == Fleetlist[0].AircraftID).ToList();

                                if (AircraftList != null && AircraftList.Count > 0)
                                {
                                    newQuote.AircraftID = AircraftList[0].AircraftID;
                                    Aircraft objAircraft = new Aircraft { AircraftID = AircraftList[0].AircraftID, AircraftCD = AircraftList[0].AircraftCD, AircraftDescription = AircraftList[0].AircraftDescription };
                                    newQuote.Aircraft = objAircraft;
                                }
                            }
                            #endregion

                        }
                    }
                }
            }
            #endregion

            FileRequest.CQMains.Add(newQuote);

            Session[Master.CQSessionKey] = FileRequest;

            #region Populate Fleet based on Homebaseid
            if (newQuote.FleetID != null && newQuote.AircraftID != 0)
                Master.BindFleetChargeDetails((long)newQuote.FleetID, (long)newQuote.AircraftID, 0, false);
            #endregion
            hdnQuoteNum.Value = newQuote.QuoteNUM.ToString();
            //Session[Master.CQSelectedQuoteNum] = newQuote.QuoteNUM.ToString();

            BindQuoteGrid(true);

            if (dgQuote.Items.Count > 0)
            {
                dgQuote.Items[dgQuote.Items.Count - 1].Selected = true;
                LoadQuoteDetailsByNUM(Convert.ToInt32(hdnQuoteNum.Value));
            }
        }

        protected void btnDeleteQuote_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hdnQuoteNum.Value) && hdnQuoteNum.Value != "0")
            {
                if (isQuoteMovedToPreflightOrInvoiceGenerated(Convert.ToInt64(hdnQuoteNum.Value)))
                    RadWindowManager1.RadAlert("Cannot Delete, Quote Moved to Trip or Invoiced", 330, 100, "Charter Quote", null);
                else
                    RadWindowManager1.RadConfirm("You are about to delete the Charter Quote No." + hdnQuoteNum.Value + ". Are you sure you want to delete this record?", "confirmDeleteQuoteCallBack", 330, 100, null, "Confirmation!");
            }
        }

        protected void ConfirmQuoteDeleteYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Int64 QuoteNumInProgress = 1;
                FileRequest = (CQFile)Session[Master.CQSessionKey];
                if (FileRequest != null && FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                    QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                hdnQuoteNum.Value = QuoteNumInProgress.ToString(); // Session[Master.CQSelectedQuoteNum].ToString();
                DeleteQuote(Convert.ToInt64(hdnQuoteNum.Value));
                BindQuoteGrid(true);

                if (dgQuote.Items.Count > 0)
                {
                    ClearForm();

                    dgQuote.SelectedIndexes.Clear();

                    //dgQuote.SelectedIndexes.Add(0);
                    dgQuote.Items[0].Selected = true;

                    GridDataItem selItem = (GridDataItem)dgQuote.SelectedItems[0];
                    LoadQuoteDetailsByNUM(Convert.ToInt32(selItem["QuoteNUM"].Text));
                }
                else
                {
                    ClearForm();
                    EnableForm(false);
                }
            }
        }

        protected void Quote_ItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            // To find the edit button in the grid
                            LinkButton EditButton = (e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(EditButton, DivExternalForm, RadAjaxLoadingPanel1);

                            // To find the insert button in the grid
                            LinkButton InsertButton = (e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(InsertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// Method to Bind Quotes Grid
        /// </summary>
        /// <param name="isBind">Pass Boolean Value for Databind</param>
        private void BindQuoteGrid(bool isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isBind))
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                    if (FileRequest != null && FileRequest.CQMains != null)
                    {
                        dgQuote.DataSource = FileRequest.CQMains.Where(x => x.IsDeleted == false).OrderBy(x => x.QuoteNUM);
                        if (isBind)
                            dgQuote.DataBind();
                    }
                    else
                    {
                        dgQuote.DataSource = string.Empty;
                        dgQuote.DataBind();

                        GridEnable(true, false, false);
                    }
                }
                else
                {
                    dgQuote.DataSource = string.Empty;
                    dgQuote.DataBind();

                    GridEnable(true, false, false);
                }
            }
        }

        #endregion

        #region Leg Grid Events

        /// <summary>
        /// Method to Bind Legs Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Legs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            Master.BindLegs(dgLegs, false);
        }

        protected void Legs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem item = (GridDataItem)e.Item;

                            if (item["DepartDate"] != null && !string.IsNullOrEmpty(item["DepartDate"].Text) && item["DepartDate"].Text != "&nbsp;")
                            {
                                DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                                if (date != null)
                                    item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date));

                            }

                            if (item["ArrivalDate"] != null && !string.IsNullOrEmpty(item["ArrivalDate"].Text) && item["ArrivalDate"].Text != "&nbsp;")
                            {
                                DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                                if (date1 != null)
                                    item["ArrivalDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", date1));

                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }


        #endregion

        #region Text Change Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLostOpportunity_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string LostCd = ddlLostOpportunity.SelectedItem.ToString();
                        if (!string.IsNullOrEmpty(ddlLostOpportunity.SelectedValue))
                            lbLostOpportunity.Text = ddlLostOpportunity.SelectedValue.ToString();
                        if (!string.IsNullOrEmpty(LostCd))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objChartersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objChartersvc.GetLostBusinessList();
                                List<FlightPakMasterService.CQLostBusiness> LostBusinessList = new List<FlightPakMasterService.CQLostBusiness>();
                                if (objRetVal.ReturnFlag)
                                {
                                    LostBusinessList = objRetVal.EntityList.Where(x => x.CQLostBusinessCD.ToUpper().Trim() == LostCd.ToUpper().Trim()).ToList();
                                    if (LostBusinessList != null && LostBusinessList.Count > 0)
                                    {
                                        hdnLostOpportunity.Value = LostBusinessList[0].CQLostBusinessID.ToString();
                                        lbLostOpportunity.Text = Microsoft.Security.Application.Encoder.HtmlEncode(LostBusinessList[0].CQLostBusinessDescription.ToString().ToUpper());
                                    }
                                }
                            }
                        }
                        else
                        {
                            //FileRequest.ExchangeRate = null;
                            hdnLostOpportunity.Value = null;
                            lbLostOpportunity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void TaxableQuote_CheckedChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnCalculateQuote.Value = "true";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// Method to validate Vendor Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Vendor_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnVendor);
                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbVendorMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnVendor.Value = string.Empty;

                        tbTailNumber.Text = string.Empty;
                        hdnTailNo.Value = string.Empty;
                        lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");

                        tbTypeCode.Text = string.Empty;
                        hdnTypeCode.Value = string.Empty;
                        lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");

                        if (!string.IsNullOrEmpty(tbVendor.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient MasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var VendorVal = MasterService.GetVendorInfo().EntityList.Where(x => x.VendorCD.ToString().ToUpper().Trim().Equals(tbVendor.Text.ToUpper().Trim())).ToList();
                                if (VendorVal != null && VendorVal.Count > 0)
                                {
                                    hdnCalculateQuote.Value = "true";
                                    hdnVendor.Value = VendorVal[0].VendorID.ToString();
                                    tbVendor.Text = VendorVal[0].VendorCD.ToString();

                                    if (VendorVal[0].Name != null && !string.IsNullOrEmpty(VendorVal[0].Name))
                                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(VendorVal[0].Name.ToUpper());

                                    if (!string.IsNullOrEmpty(hdnVendor.Value))
                                    {
                                        ControlEnable("TextBox", tbTailNumber, true, "text70");
                                        ControlEnable("Button", btnTailNumber, true, "browse-button");
                                        ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");
                                    }

                                    SaveCharterQuoteToSession();
                                    BindCharterQuoteFromSession();
                                }
                                else
                                {
                                    lbVendorMsg.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Code.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbVendor);
                                }
                            }
                        }
                        else
                        {
                            tbTailNumber.Text = string.Empty;
                            hdnTailNo.Value = string.Empty;
                            lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                            tbTypeCode.Text = string.Empty;
                            hdnTypeCode.Value = string.Empty;
                            lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                            ControlEnable("TextBox", tbTailNumber, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");

                            ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");
                            ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");

                            ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                            ControlEnable("Button", btnTypeCodeSearch, false, "charter-rate-disable-button");
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// Method to validate TailNumber
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TailNo_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTailNumber);
                        lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTailNo.Value = string.Empty;
                        if (!string.IsNullOrEmpty(tbTailNumber.Text))
                        {
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList();
                                if (objRetVal.ReturnFlag)
                                {
                                    if (string.IsNullOrEmpty((hdnVendor.Value)))
                                    {
                                        Fleetlist = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNumber.Text.ToUpper().Trim())).ToList();
                                    }
                                    else
                                    {
                                        Fleetlist = objRetVal.EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(tbTailNumber.Text.ToUpper().Trim()) && x.VendorID == Convert.ToInt64(hdnVendor.Value)).ToList();
                                    }

                                    if (Fleetlist != null && Fleetlist.Count > 0)
                                    {
                                        hdnCalculateQuote.Value = "true";
                                        hdnTailNo.Value = Fleetlist[0].FleetID.ToString();
                                        hdnTypeCode.Value = Fleetlist[0].AircraftID.ToString();
                                        tbTypeCode.Text = string.Empty;

                                        tbTailNumber.ToolTip = Fleetlist[0].Notes != null ? Fleetlist[0].Notes : string.Empty;

                                        if (hdnTailNo.Value != null)
                                            ControlEnable("TextBox", tbTypeCode, true, "text70");
                                        else
                                            ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");

                                        lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                        #region Aircraft

                                        if (!string.IsNullOrEmpty(hdnTypeCode.Value))
                                        {
                                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();

                                            var objRetVal1 = objDstsvc.GetAircraftList();
                                            if (objRetVal1.ReturnFlag)
                                            {
                                                AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID.ToString() == hdnTypeCode.Value).ToList();

                                                if (AircraftList != null && AircraftList.Count > 0)
                                                {
                                                    tbTypeCode.Text = AircraftList[0].AircraftCD.ToString();
                                                    hdnTypeCode.Value = AircraftList[0].AircraftID.ToString();
                                                    if (!string.IsNullOrEmpty(AircraftList[0].AircraftDescription))
                                                        lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription.ToString().ToUpper());
                                                }
                                                else
                                                {
                                                    tbTypeCode.Text = string.Empty;
                                                    lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                    hdnTypeCode.Value = string.Empty;
                                                }
                                            }
                                        }
                                        #endregion

                                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                                        if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
                                        {
                                            Int64 QuoteNumInProgress = 1;
                                            QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                            // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates
                                            if (UserPrincipal.Identity._fpSettings._IsQuoteDeactivateAutoUpdateRates != null && (bool)UserPrincipal.Identity._fpSettings._IsQuoteDeactivateAutoUpdateRates)
                                            {
                                                if (QuoteInProgress != null)
                                                {
                                                    if ((FileRequest.CustomerID != null && FileRequest.CustomerID != 0) && (FileRequest.UseCustomFleetCharge != null && (bool)FileRequest.UseCustomFleetCharge))
                                                    {
                                                        Master.BindFleetChargeDetails(0, 0, (long)FileRequest.CustomerID, true);
                                                        SaveCharterQuoteToSession();
                                                        BindCharterQuoteFromSession();
                                                    }
                                                    else
                                                    {
                                                        if ((QuoteInProgress.CQFleetChargeDetails != null && QuoteInProgress.CQFleetChargeDetails.Count > 0) && Session["CQConfirmCharterRate"] != null && QuoteInProgress.State == CQRequestEntityState.Modified)
                                                        {
                                                            RadWindowManager1.RadConfirm("The Quote will be updated with New Charter Rates and Charges.", "confirmAutoUpdateRatesFn", 330, 100, null, "Confirmation!");
                                                        }
                                                        else
                                                        {
                                                            Master.BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNo.Value) ? Convert.ToInt64(hdnTailNo.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);
                                                            SaveCharterQuoteToSession();
                                                            BindCharterQuoteFromSession();
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if ((FileRequest.CustomerID != null && FileRequest.CustomerID != 0) && (FileRequest.UseCustomFleetCharge != null && (bool)FileRequest.UseCustomFleetCharge))
                                                    Master.BindFleetChargeDetails(0, 0, (long)FileRequest.CustomerID, true);
                                                else
                                                    Master.BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNo.Value) ? Convert.ToInt64(hdnTailNo.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);

                                                SaveCharterQuoteToSession();
                                                BindCharterQuoteFromSession();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        this.tbTailNumber.ToolTip = "";
                                        // Fix for UW-744 (7227)
                                        if (rblSource.SelectedValue == "1") // (isVendor)
                                            lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Aircraft Tail Number");
                                        else
                                            lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                        hdnTailNo.Value = "";
                                        tbTypeCode.Text = "";
                                        hdnTypeCode.Value = "";
                                        lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        //tbTypeCode.ReadOnly = false;
                                        ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNumber);
                                    }
                                }
                                else
                                {
                                    this.tbTailNumber.ToolTip = "";
                                    // Fix for UW-744 (7227)
                                    if (rblSource.SelectedValue == "1") // (isVendor)
                                        lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode("Invalid Vendor Aircraft Tail Number");
                                    else
                                        lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode("Tail No. Does Not Exist");
                                    hdnTailNo.Value = "";
                                    tbTypeCode.Text = "";
                                    hdnTypeCode.Value = "";
                                    lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    //tbTypeCode.ReadOnly = false;
                                    ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTailNumber);
                                }
                            }
                        }
                        else
                        {
                            this.tbTailNumber.ToolTip = "";
                            lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            hdnTailNo.Value = "";
                            tbTypeCode.Text = "";
                            hdnTypeCode.Value = "";
                            lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            //tbTypeCode.ReadOnly = false;

                            ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// Method to validate Requestor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Requestor_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnRequestor);
                        lbRequestorMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbRequestorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        tbRequestorPhone.Text = string.Empty;
                        hdnRequestor.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbRequestor.Text))
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllPassenger> PassengerList = new List<FlightPakMasterService.GetAllPassenger>();
                            using (FlightPakMasterService.MasterCatalogServiceClient RequestorService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = RequestorService.GetPassengerList();
                                if (objRetVal.ReturnFlag)
                                {
                                    PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(tbRequestor.Text.ToUpper().Trim())).ToList();
                                    if (PassengerList != null && PassengerList.Count > 0)
                                    {
                                        hdnRequestor.Value = PassengerList[0].PassengerRequestorID.ToString();
                                        if (PassengerList[0].PassengerName != null)
                                            lbRequestorDesc.Text = System.Web.HttpUtility.HtmlEncode(PassengerList[0].PassengerName);

                                        if (PassengerList[0].AdditionalPhoneNum != null)
                                            tbRequestorPhone.Text = PassengerList[0].AdditionalPhoneNum.Trim(); // .PhoneNum.Trim();

                                        SaveCharterQuoteToSession();
                                        BindCharterQuoteFromSession();
                                    }
                                    else
                                    {
                                        lbRequestorMsg.Text = System.Web.HttpUtility.HtmlEncode("Invalid Requestor Code.");
                                        //tbRequestor.Text = string.Empty;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbRequestor);
                                    }
                                }
                                else
                                {
                                    lbRequestorMsg.Text = System.Web.HttpUtility.HtmlEncode("Invalid Requestor Code.");
                                    //tbRequestor.Text = string.Empty;
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbRequestor);
                                }
                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Fed_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbFed.Text))
                        {
                            bool correctFormat = true;
                            int count = 0;
                            string taxRate = tbFed.Text;
                            for (int i = 0; i < taxRate.Length; ++i)
                            {
                                if (taxRate[i].ToString() == ".")
                                    count++;
                            }
                            if (count > 1)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                tbFed.Text = "00.00";
                                correctFormat = false;
                            }
                            if (taxRate.IndexOf(".") >= 0)
                            {
                                string[] strarr = taxRate.Split('.');
                                if (strarr[0].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                    tbFed.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (strarr[1].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                    tbFed.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[0].Length == 0)
                                        taxRate = "00" + taxRate;
                                    tbFed.Text = taxRate;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[1].Length == 0)
                                        taxRate = taxRate + "00";
                                    if (strarr[1].Length == 1)
                                        taxRate = taxRate + "0";
                                    tbFed.Text = taxRate;
                                }
                            }
                            else
                            {
                                if (taxRate.Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                    tbFed.Text = "00.00";
                                    correctFormat = false;
                                }
                                else
                                {
                                    tbFed.Text = taxRate + ".00";
                                    SaveCharterQuoteToSession();
                                }
                            }
                        }
                        hdnCalculateQuote.Value = "true";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Rural_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!string.IsNullOrEmpty(tbRural.Text))
                        {
                            bool correctFormat = true;
                            int count = 0;
                            string taxRate = tbRural.Text;
                            for (int i = 0; i < taxRate.Length; ++i)
                            {
                                if (taxRate[i].ToString() == ".")
                                    count++;
                            }
                            if (count > 1)
                            {
                                RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                tbRural.Text = "00.00";
                                correctFormat = false;
                            }
                            if (taxRate.IndexOf(".") >= 0)
                            {
                                string[] strarr = taxRate.Split('.');
                                if (strarr[0].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                    tbRural.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (strarr[1].Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                    tbRural.Text = "00.00";
                                    correctFormat = false;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[0].Length == 0)
                                        taxRate = "00" + taxRate;
                                    tbRural.Text = taxRate;
                                }
                                if (correctFormat)
                                {
                                    if (strarr[1].Length == 0)
                                        taxRate = taxRate + "00";
                                    if (strarr[1].Length == 1)
                                        taxRate = taxRate + "0";
                                    tbRural.Text = taxRate;
                                }
                            }
                            else
                            {
                                if (taxRate.Length > 2)
                                {
                                    RadWindowManager1.RadAlert("Invalid format, Required format is ##.##", 330, 100, "Charter Quote", null);
                                    tbRural.Text = "00.00";
                                    correctFormat = false;
                                }
                                else
                                {
                                    tbRural.Text = taxRate + ".00";
                                    SaveCharterQuoteToSession();
                                }
                            }
                        }
                        hdnCalculateQuote.Value = "true";
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void AcceptedDt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Master.IsValidDate(tbLastAcceptedDt.Text))
                        {
                            if (chkAccepted.Checked)
                            {
                                ControlEnable("TextBox", tbLastAcceptedDt, true, "text70");

                                tbRejectionDt.Text = string.Empty;
                                tbComments.Text = string.Empty;

                                ControlEnable("TextBox", tbRejectionDt, false, "inpt_non_edit text70");
                                ControlEnable("TextBox", tbComments, false, "inpt_non_edit text480");
                            }
                            else
                            {
                                ControlEnable("TextBox", tbLastAcceptedDt, false, "inpt_non_edit text70");

                                ControlEnable("TextBox", tbRejectionDt, true, "text70");
                                ControlEnable("TextBox", tbComments, true, "text480");
                            }

                            SaveCharterQuoteToSession();
                            BindCharterQuoteFromSession();
                        }
                        else
                        {
                            tbLastAcceptedDt.Text = string.Empty;
                            if (Session[Master.CQSessionKey] != null)
                            {
                                Int64 QuoteNumInProgress = 1;
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                if (FileRequest != null)
                                {
                                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                    {
                                        CQMain objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                        if (objCQMain != null && objCQMain.LastAcceptDT != null)
                                        {
                                            DateTime dat = (DateTime)objCQMain.LastAcceptDT;
                                            tbLastAcceptedDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dat);
                                        }
                                    }
                                }
                            }

                            Master.RadAjaxManagerMaster.FocusControl(tbLastAcceptedDt.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void LastSubmittedDt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!Master.IsValidDate(tbLastSubmittedDt.Text))
                        {
                            tbLastSubmittedDt.Text = string.Empty;
                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                Int64 QuoteNumInProgress = 1;
                                if (FileRequest != null)
                                {
                                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                    {
                                        CQMain objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                        if (objCQMain != null && objCQMain.LastSubmitDT != null)
                                        {
                                            DateTime dat = (DateTime)objCQMain.LastSubmitDT;
                                            tbLastSubmittedDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dat);
                                        }
                                    }
                                }
                            }
                            Master.RadAjaxManagerMaster.FocusControl(tbLastSubmittedDt.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        protected void RejectionDt_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!Master.IsValidDate(tbRejectionDt.Text))
                        {
                            tbRejectionDt.Text = string.Empty;
                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                Int64 QuoteNumInProgress = 1;
                                if (FileRequest != null)
                                {
                                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                    {
                                        CQMain objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                        if (objCQMain != null && objCQMain.RejectDT != null)
                                        {
                                            DateTime dat = (DateTime)objCQMain.RejectDT;
                                            tbRejectionDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", dat);
                                        }
                                    }
                                }
                            }
                            Master.RadAjaxManagerMaster.FocusControl(tbRejectionDt.ClientID);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQLeg);
                }
            }
        }

        /// <summary>
        /// Method to validate Type Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TypeCode_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).FocusControl(btnTypeCode);
                        lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnTypeCode.Value = string.Empty;

                        if (!string.IsNullOrEmpty(tbTypeCode.Text))
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetAircraftList();
                                if (objRetVal.ReturnFlag)
                                {

                                    AircraftList = objRetVal.EntityList.Where(x => x.AircraftCD.ToUpper() == tbTypeCode.Text.ToUpper()).ToList();

                                    if (AircraftList != null && AircraftList.Count > 0)
                                    {
                                        hdnCalculateQuote.Value = "true";
                                        hdnTypeCode.Value = AircraftList[0].AircraftID.ToString();

                                        if (AircraftList[0].AircraftDescription != null)
                                            lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription);

                                        Master.BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNo.Value) ? Convert.ToInt64(hdnTailNo.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);

                                        SaveCharterQuoteToSession();
                                        BindCharterQuoteFromSession();
                                    }
                                    else
                                    {
                                        lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode("Aircraft Type Does Not Exist.");
                                        RadAjaxManager.GetCurrent(Page).FocusControl(tbTypeCode);
                                    }
                                }
                                else
                                {
                                    lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode("Aircraft Type Does Not Exist.");
                                    RadAjaxManager.GetCurrent(Page).FocusControl(tbTypeCode);
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Source_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tbVendor.Text = string.Empty;
                        tbTailNumber.Text = string.Empty;
                        tbTypeCode.Text = string.Empty;
                        lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        hdnVendor.Value = string.Empty;
                        hdnTailNo.Value = string.Empty;
                        hdnTypeCode.Value = string.Empty;
                        tbTailNumber.ToolTip = string.Empty;
                        lbVendorMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTailNumberMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        SetSourceByValue(rblSource.SelectedValue);
                        hdnCalculateQuote.Value = "true";

                        SaveCharterQuoteToSession();
                        BindCharterQuoteFromSession();

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Submitted_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (chkSubmitted.Checked == true)
                            tbLastSubmittedDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", DateTime.Now);
                        else
                            tbLastSubmittedDt.Text = string.Empty;

                        SaveCharterQuoteToSession();
                        BindCharterQuoteFromSession();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void Accepted_CheckedChanged(Object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        EnableOrDisableControlsOnAcceptEvent();

                        SaveCharterQuoteToSession();
                        BindCharterQuoteFromSession();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        private void EnableOrDisableControlsOnAcceptEvent()
        {
            if (chkAccepted.Checked == true)
            {
                ControlEnable("TextBox", tbLastAcceptedDt, true, "text70");

                tbRejectionDt.Text = string.Empty;
                tbComments.Text = string.Empty;
                ControlEnable("TextBox", tbRejectionDt, false, "inpt_non_edit text70");
                ControlEnable("TextBox", tbComments, false, "inpt_non_edit text480");
            }
            else
            {
                tbLastAcceptedDt.Text = string.Empty;
                ControlEnable("TextBox", tbLastAcceptedDt, false, "inpt_non_edit text70");

                ControlEnable("TextBox", tbRejectionDt, true, "text70");
                ControlEnable("TextBox", tbComments, true, "text480");
            }
        }

        #endregion

        #region Permissions
        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.CharterQuote.ViewCQQuotesOnFile);
                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                // Check for Field Level Permissions
                if (IsAuthorized(Permission.CharterQuote.ViewCQQuotesOnFile))
                {
                    // Show read-only form
                    EnableForm(false);
                    GridEnable(false, false, false);

                    // Check Page Controls - Visibility
                    if (IsAuthorized(Permission.CharterQuote.AddCQQuotesOnFile) || IsAuthorized(Permission.CharterQuote.EditCQQuotesOnFile))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQQuotesOnFile))
                    {
                        ControlVisibility("Button", btnDelete, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.EditCQQuotesOnFile))
                    {
                        ControlVisibility("Button", btnEditFile, true);
                    }
                    // Check Page Controls - Disable/Enable
                    if (Session[Master.CQSessionKey] != null)
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];

                        if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                        {
                            if (FileRequest.CQMains != null)
                            {
                                EnableForm(true);

                                if (chkAccepted.Checked == true)
                                {
                                    ControlEnable("TextBox", tbLastAcceptedDt, true, "text70");
                                    ControlEnable("TextBox", tbRejectionDt, false, "inpt_non_edit text70");
                                    ControlEnable("TextBox", tbComments, false, "inpt_non_edit text480");
                                }
                                else
                                {
                                    ControlEnable("TextBox", tbLastAcceptedDt, false, "inpt_non_edit text70");
                                    ControlEnable("TextBox", tbRejectionDt, true, "text70");
                                    ControlEnable("TextBox", tbComments, true, "text480");
                                }
                            }

                            GridEnable(true, true, true);

                            ControlEnable("Button", btnCRTail, true, "button");
                            ControlEnable("Button", btnCRTail_Top, true, "button");
                            if (IsAuthorized(Permission.CharterQuote.AddCQQuotesOnFile) || IsAuthorized(Permission.CharterQuote.EditCQQuotesOnFile))
                            {
                                ControlEnable("Button", btnAddQuote, true, "ui_nav");
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                                ControlEnable("Button", btnCRTail, false, "button-disable");
                                ControlEnable("Button", btnCRTail_Top, false, "button-disable");
                            }
                            ControlEnable("Button", btnDeleteQuote, true, "ui_nav");
                            ControlEnable("Button", btnDelete, false, "button-disable");
                            ControlEnable("Button", btnCopy, false, "button-disable");
                            ControlEnable("Button", btnCopy_Top, false, "button-disable");
                            ControlEnable("Button", btnLiveTrip, false, "button-disable");
                            ControlEnable("Button", btnLiveTrip_Top, false, "button-disable");
                            ControlEnable("Button", btnEditFile, false, "button-disable");
                        }
                        else
                        {
                            ControlEnable("Button", btnAddQuote, false, "ui_nav_disable");
                            ControlEnable("Button", btnDeleteQuote, false, "ui_nav_disable");

                            ControlEnable("TextBox", tbLastAcceptedDt, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbRejectionDt, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbComments, false, "inpt_non_edit text480");

                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDelete, true, "button");
                            ControlEnable("Button", btnEditFile, true, "button");
                            if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Where(x => x.IsDeleted == false).Count() > 0)
                            {
                                ControlEnable("Button", btnCopy, true, "button");
                                ControlEnable("Button", btnCopy_Top, true, "button");
                                ControlEnable("Button", btnLiveTrip, true, "button");
                                ControlEnable("Button", btnLiveTrip_Top, true, "button");
                                ControlEnable("Button", btnCRTail, true, "button");
                                ControlEnable("Button", btnCRTail_Top, true, "button");
                            }
                            else
                            {
                                ControlEnable("Button", btnCopy, false, "button-disable");
                                ControlEnable("Button", btnCopy_Top, false, "button-disable");
                                ControlEnable("Button", btnLiveTrip, false, "button-disable");
                                ControlEnable("Button", btnLiveTrip_Top, false, "button-disable");
                                ControlEnable("Button", btnCRTail, false, "button-disable");
                                ControlEnable("Button", btnCRTail_Top, false, "button-disable");
                            }
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnAddQuote, false, "ui_nav_disable");
                        ControlEnable("Button", btnDeleteQuote, false, "ui_nav_disable");

                        ControlEnable("TextBox", tbLastAcceptedDt, false, "inpt_non_edit text70");
                        ControlEnable("TextBox", tbRejectionDt, false, "inpt_non_edit text70");
                        ControlEnable("TextBox", tbComments, false, "inpt_non_edit text480");

                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnCopy, false, "button-disable");
                        ControlEnable("Button", btnCopy_Top, false, "button-disable");
                        ControlEnable("Button", btnLiveTrip, false, "button-disable");
                        ControlEnable("Button", btnLiveTrip_Top, false, "button-disable");
                        ControlEnable("Button", btnCRTail, false, "button-disable");
                        ControlEnable("Button", btnCRTail_Top, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                    }
                }
            }
        }

        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            txt.CssClass = cssClass;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method to Enable and Disable Form
        /// </summary>
        /// <param name="isEnable">Boolean</param>
        protected void EnableForm(bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isEnable))
            {
                rblSource.Enabled = isEnable;
                rblQuoteStage.Enabled = isEnable;
                tbVendor.Enabled = isEnable;
                tbTailNumber.Enabled = isEnable;
                tbTypeCode.Enabled = isEnable;
                tbRequestor.Enabled = isEnable;
                tbRequestorPhone.Enabled = isEnable;
                tbFed.Enabled = isEnable;
                tbRural.Enabled = isEnable;
                chkTaxableQuote.Enabled = isEnable;
                chkSubmitted.Enabled = isEnable;
                chkAccepted.Enabled = isEnable;
                //tbLastSubmittedDt.Enabled = isEnable;
                //tbLastAcceptedDt.Enabled = isEnable;
                tbRejectionDt.Enabled = isEnable;
                tbComments.Enabled = isEnable;
                ddlLostOpportunity.Enabled = isEnable;
                chkInactive.Enabled = isEnable;
                btnVendor.Enabled = isEnable;
                btnTailNumber.Enabled = isEnable;
                btnTailNumberSearch.Enabled = isEnable;
                btnTypeCode.Enabled = isEnable;
                btnTypeCodeSearch.Enabled = isEnable;
                btnRequestor.Enabled = isEnable;
                tbCQimgName.Enabled = isEnable;
                fuCQ.Enabled = isEnable;
                ddlCQFileName.Enabled = isEnable;
                radbtnCQDelete.Enabled = isEnable;
                radbtnCQDelete.Visible = isEnable;
                imgCQ.Enabled = isEnable;
                if (isEnable)
                {
                    btnVendor.CssClass = "browse-button";
                    btnTailNumber.CssClass = "browse-button";
                    btnTypeCode.CssClass = "browse-button";
                    btnRequestor.CssClass = "browse-button";

                    if (!string.IsNullOrEmpty(tbTailNumber.Text))
                        btnTailNumberSearch.CssClass = "charter-rate-button";
                    else
                    {
                        btnTailNumberSearch.CssClass = "charter-rate-disable-button";
                        btnTailNumberSearch.Enabled = false;
                    }

                    if (!string.IsNullOrEmpty(tbTypeCode.Text))
                        btnTypeCodeSearch.CssClass = "charter-rate-button";
                    else
                    {
                        btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                        btnTypeCodeSearch.Enabled = false;
                    }
                }
                else
                {
                    btnVendor.CssClass = "browse-button-disabled";
                    btnTailNumber.CssClass = "browse-button-disabled";
                    btnTailNumberSearch.CssClass = "charter-rate-disable-button";
                    btnTypeCode.CssClass = "browse-button-disabled";
                    btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                    btnRequestor.CssClass = "browse-button-disabled";
                }
            }
        }
        #endregion

        #region Private Methods


        /// <summary>
        /// To Load Currency from ExchangeRate
        /// </summary>
        private void LoadLostBusiness()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var RetValue = objMasterService.GetLostBusinessList().EntityList.ToList();

                            if (RetValue != null && RetValue.Count() > 0)
                            {
                                ddlLostOpportunity.DataSource = RetValue;
                                ddlLostOpportunity.DataTextField = "CQLostBusinessCD";
                                ddlLostOpportunity.DataValueField = "CQLostBusinessCD";
                                ddlLostOpportunity.DataBind();
                                ddlLostOpportunity.Items.Insert(0, String.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, FlightPak.Common.ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }
        /// <summary>
        /// Method to Bind Charter Quote Data from Session
        /// </summary>
        protected void BindCharterQuoteFromSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                BindQuoteGrid(true);

                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                    if (FileRequest != null)
                    {
                        if (FileRequest.CQCustomerID != null)
                            hdnCQCustomerID.Value = FileRequest.CQCustomerID.ToString();

                        if (dgQuote.MasterTableView.Items.Count > 0)
                        {
                            long _quoteNum = (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress != 0) ? FileRequest.QuoteNumInProgress : 1;
                            //Session[Master.CQSelectedQuoteNum] = _quoteNum.ToString();
                            dgQuote.Items[Convert.ToInt32(_quoteNum - 1)].Selected = true;
                            LoadQuoteDetailsByNUM(_quoteNum);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to Load Quote Details by Quote Number
        /// </summary>
        /// <param name="quoteNum">Quote Number</param>
        protected void LoadQuoteDetailsByNUM(long quoteNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(quoteNum))
            {
                //Clear Existing Field Datas
                //ClearForm();

                hdnQuoteNum.Value = quoteNum.ToString();

                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                    CQMain objCQMain = new CQMain();
                    objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == quoteNum && x.IsDeleted == false).FirstOrDefault();

                    if (objCQMain != null)
                    {
                        FileRequest.QuoteNumInProgress = (Int64)objCQMain.QuoteNUM;
                        Session[Master.CQSessionKey] = FileRequest;
                        //Defect# 2565
                        Session.Remove(Master.CQException);
                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                        {
                            var ExceptionList = Service.GetQuoteExceptionList(objCQMain.CQMainID);
                            if (ExceptionList != null && ExceptionList.EntityList.Count() > 0)
                            {
                                Session[Master.CQException] = ExceptionList.EntityList;
                            }
                        }

                        if (objCQMain.CQLegs != null && objCQMain.CQLegs.Count > 0)
                            hdnIsHavingLeg.Value = "1";
                        else
                            hdnIsHavingLeg.Value = "0";

                        Master.ValidateQuote();
                        Master.BindException(true);
                        Master.BindLegs(dgLegs, true);
                        Master.TrackerLegs.Rebind();

                        if (objCQMain.CQSource != null)
                        {
                            rblSource.SelectedValue = objCQMain.CQSource.Value.ToString();
                            SetSourceByValue(rblSource.SelectedValue);
                        }
                        else
                            rblSource.ClearSelection();

                        if (objCQMain.Vendor != null)
                        {
                            hdnVendor.Value = objCQMain.VendorID.Value.ToString();
                            tbVendor.Text = objCQMain.Vendor.VendorCD;
                            if (objCQMain.Vendor.Name != null)
                                lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(objCQMain.Vendor.Name.ToUpper());
                        }
                        else
                        {
                            hdnVendor.Value = string.Empty;
                            tbVendor.Text = string.Empty;
                            lbVendorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbVendorMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }

                        if (objCQMain.Fleet != null)
                        {
                            hdnTailNo.Value = objCQMain.FleetID.Value.ToString();
                            tbTailNumber.Text = objCQMain.Fleet.TailNum;
                        }
                        else
                        {
                            hdnTailNo.Value = string.Empty;
                            tbTailNumber.Text = string.Empty;
                        }

                        if (!string.IsNullOrEmpty(tbTailNumber.Text))
                            ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");
                        else
                        {
                            btnTailNumberSearch.Enabled = false;
                            btnTailNumberSearch.CssClass = "charter-rate-disable-button";
                            //ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");
                        }


                        if (objCQMain.Aircraft != null)
                        {
                            hdnTypeCode.Value = objCQMain.AircraftID.ToString();
                            tbTypeCode.Text = objCQMain.Aircraft.AircraftCD;
                            if (objCQMain.Aircraft.AircraftDescription != null)
                                lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(objCQMain.Aircraft.AircraftDescription.ToUpper());
                        }
                        else
                        {
                            hdnTypeCode.Value = string.Empty;
                            tbTypeCode.Text = string.Empty;
                            lbTypeCodeDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbTypeCodeMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }

                        if (!string.IsNullOrEmpty(tbTypeCode.Text))
                            ControlEnable("Button", btnTypeCodeSearch, true, "charter-rate-button");
                        else
                        {
                            btnTypeCodeSearch.Enabled = false;
                            btnTypeCodeSearch.CssClass = "charter-rate-disable-button";
                            //ControlEnable("Button", btnTypeCodeSearch, false, "charter-rate-disable-button");
                        }

                        if (objCQMain.Passenger != null)
                        {
                            tbRequestor.Text = objCQMain.Passenger.PassengerRequestorCD;
                            hdnRequestor.Value = objCQMain.PassengerRequestorID.Value.ToString();
                            if (objCQMain.Passenger.PassengerName != null)
                                lbRequestorDesc.Text = System.Web.HttpUtility.HtmlEncode(objCQMain.Passenger.PassengerName.ToUpper());
                            if (objCQMain.Passenger.AdditionalPhoneNum != null && objCQMain.Passenger.AdditionalPhoneNum != "&nbsp;")
                                tbRequestorPhone.Text = objCQMain.Passenger.AdditionalPhoneNum;
                        }
                        else
                        {
                            tbRequestor.Text = string.Empty;
                            tbRequestorPhone.Text = string.Empty;
                            hdnRequestor.Value = string.Empty;
                            lbRequestorDesc.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbRequestorMsg.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        }

                        if (objCQMain.QuoteStage != null)
                        {
                            rblQuoteStage.SelectedValue = objCQMain.QuoteStage;
                        }
                        else
                            rblQuoteStage.SelectedValue = "In Progress";

                        if (objCQMain.FederalTax != null)
                            tbFed.Text = objCQMain.FederalTax.Value.ToString();
                        else
                            tbFed.Text = "0.0";

                        if (objCQMain.QuoteRuralTax != null)
                            tbRural.Text = objCQMain.QuoteRuralTax.Value.ToString();
                        else
                            tbRural.Text = "0.0";

                        if (objCQMain.IsTaxable != null)
                            chkTaxableQuote.Checked = Convert.ToBoolean(objCQMain.IsTaxable);
                        else
                            chkTaxableQuote.Checked = false;

                        if (objCQMain.IsSubmitted != null)
                            chkSubmitted.Checked = Convert.ToBoolean(objCQMain.IsSubmitted);
                        else
                            chkSubmitted.Checked = false;

                        if (objCQMain.IsAccepted != null)
                            chkAccepted.Checked = Convert.ToBoolean(objCQMain.IsAccepted);
                        else
                            chkAccepted.Checked = false;

                        if (objCQMain.LastSubmitDT != null)
                            tbLastSubmittedDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", objCQMain.LastSubmitDT);
                        else
                            tbLastSubmittedDt.Text = string.Empty;

                        if (objCQMain.LastAcceptDT != null)
                            tbLastAcceptedDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", objCQMain.LastAcceptDT);
                        else
                            tbLastAcceptedDt.Text = string.Empty;

                        if (objCQMain.RejectDT != null)
                            tbRejectionDt.Text = String.Format(CultureInfo.InvariantCulture, "{0:" + Master.DatePicker.DateInput.DateFormat + "}", objCQMain.RejectDT);
                        else
                            tbRejectionDt.Text = string.Empty;

                        if (objCQMain.Comments != null)
                            tbComments.Text = objCQMain.Comments;
                        else
                            tbComments.Text = string.Empty;

                        EnableOrDisableControlsOnAcceptEvent();

                        if (objCQMain.CQLostBusiness != null)
                        {
                            hdnLostOpportunity.Value = objCQMain.CQLostBusiness.CQLostBusinessID.ToString();
                            ddlLostOpportunity.SelectedValue = objCQMain.CQLostBusiness.CQLostBusinessCD.ToString();

                            if (!string.IsNullOrEmpty(hdnLostOpportunity.Value))
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient objChartersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    var objRetVal = objChartersvc.GetLostBusinessList();
                                    List<FlightPakMasterService.CQLostBusiness> LostBusinessList = new List<FlightPakMasterService.CQLostBusiness>();
                                    if (objRetVal.ReturnFlag)
                                    {
                                        LostBusinessList = objRetVal.EntityList.Where(x => x.CQLostBusinessCD.ToUpper().Trim() == ddlLostOpportunity.SelectedValue.ToString().ToUpper().Trim()).ToList();
                                        if (LostBusinessList != null && LostBusinessList.Count > 0)
                                        {
                                            lbLostOpportunity.Text = Microsoft.Security.Application.Encoder.HtmlEncode(LostBusinessList[0].CQLostBusinessDescription.ToString().ToUpper());
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            hdnLostOpportunity.Value = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            lbLostOpportunity.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            if (ddlLostOpportunity.Items.Count > 0)
                                ddlLostOpportunity.SelectedIndex = 0;
                        }

                        if (objCQMain.IsInActive != null)
                            chkInactive.Checked = (bool)objCQMain.IsInActive;
                        else
                            chkInactive.Checked = false;

                        #region Get CQ Image from Database
                        CreateCqDictionayForImgUpload();

                        imgCQ.ImageUrl = "";
                        imgCQPopup.ImageUrl = null;
                        lnkFileName.NavigateUrl = "";

                        ddlCQFileName.Items.Clear();
                        ddlCQFileName.Text = "";
                        int i = 0;

                        Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                        if (objCQMain.ImageList != null)
                        {
                            foreach (FileWarehouse objFile in objCQMain.ImageList.Where(x => x.IsDeleted == false))
                            {
                                ddlCQFileName.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(objFile.UWAFileName), objFile.UWAFileName));
                                dicImg.Add(objFile.UWAFileName, objFile.UWAFilePath);
                                //Ramesh: Commented repeated code, functionality achieved below.
                                //if (i == 0)
                                //{
                                //    byte[] picture = objFile.UWAFilePath;
                                //    //Ramesh: Code to allow any file type for upload control.
                                //    int iIndex = objFile.UWAFileName.IndexOf('.');
                                //    string strExtn = objFile.UWAFileName.Substring(iIndex, objFile.UWAFileName.Length - iIndex);
                                //    //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
                                //    //Match regMatch = Regex.Match(strExtn, @"\.(?<ext>[^(\s|.)]+)$", RegexOptions.IgnoreCase);                                    
                                //    Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                                //    if (regMatch.Success)
                                //    {
                                //        //start of modification for image issue
                                //        if (hdnBrowserName.Value == "Chrome")
                                //        {
                                //            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                //            lnkFileName.NavigateUrl = "";
                                //        }
                                //        else
                                //        {
                                //            //Session["CQCQQuoteOnFileBase64"] = picture;
                                //            Session["CQQuoteOnFileBase64"] = picture;
                                //            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase6&cd=" + Guid.NewGuid() + "&filename=" + objFile.UWAFileName);
                                //            lnkFileName.NavigateUrl = "";
                                //        }
                                //        //end of modification for image issue
                                //    }
                                //    else
                                //    {
                                //        imgCQ.Style.Add("display", "false");
                                //        lnkFileName.Style.Add("display", "true");
                                //        Session["CQQuoteOnFileBase64"] = picture;
                                //        lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + objFile.UWAFileName;
                                //        imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                                //        //Converting the Byte Array to a WordDocument Again (Retrieved from DB)
                                //        //FileStream fs = new FileStream(Request.PhysicalApplicationPath + "\\Uploads\\" + FileName, FileMode.Create, FileAccess.ReadWrite);
                                //        //BinaryWriter bw = new BinaryWriter(fs);
                                //        //bw.Write(Input);
                                //        //bw.Close();
                                //        //HttpContext context;
                                //        //context.
                                //        //Response.Clear();
                                //        //Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fwh.UWAFileName + "\"");
                                //        //Response.AddHeader("Content-Length", picture.Length.ToString());
                                //        //Response.ContentType = "application/octet-stream";
                                //        //Response.BinaryWrite(picture);
                                //        //Response.Flush();
                                //    }
                                //}
                                i = i + 1;
                            }
                            Session[Master.CQFileImage] = dicImg;
                        }

                        if (ddlCQFileName.Items.Count > 0)
                        {
                            radbtnCQDelete.Enabled = true;
                            ddlCQFileName.SelectedIndex = 0;
                            Session["CQQuoteOnFileBase64"] = dicImg[ddlCQFileName.SelectedValue];
                            byte[] picture = dicImg[ddlCQFileName.SelectedValue];

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(ddlCQFileName.SelectedValue, picture);

                            ////Ramesh: Code to allow any file type for upload control.
                            //int iIndex = ddlCQFileName.SelectedValue.IndexOf('.');
                            //string strExtn = ddlCQFileName.SelectedValue.Substring(iIndex, ddlCQFileName.SelectedValue.Length - iIndex);
                            //Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                            //if (regMatch.Success)
                            //{
                            //    //start of modification for image issue
                            //    if (hdnBrowserName.Value == "Chrome")
                            //    {
                            //        imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                            //        lnkFileName.NavigateUrl = "";
                            //    }
                            //    else
                            //    {                                    
                            //        Session["CQQuoteOnFileBase64"] = picture;
                            //        imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase6&cd=" + Guid.NewGuid());
                            //        lnkFileName.NavigateUrl = "";
                            //    }
                            //    //end of modification for image issue                              
                            //}
                            //else
                            //{
                            //    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                            //    lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + ddlCQFileName.SelectedValue;
                            //}
                        }
                        else
                        {
                            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                            lnkFileName.NavigateUrl = "";
                            radbtnCQDelete.Enabled = false;
                        }
                       //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"fnRefreshPage();");
                        #endregion
                    }
                }
            }
        }

        /// <summary>
        /// Method to save the Form Field data's into Session Variable
        /// </summary>
        protected void SaveCharterQuoteToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        bool SaveToSession = false;
                        CQMain objCQMain = new CQMain();

                        //Int32 quoteNum = Convert.ToInt32(Session[Master.CQSelectedQuoteNum]);

                        Int64 QuoteNumInProgress = 1;
                        if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                        if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                        {
                            objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                            if (objCQMain != null)
                            {
                                if (objCQMain.CQMainID == 0)
                                    objCQMain.State = CharterQuoteService.CQRequestEntityState.Added;
                                else
                                    objCQMain.State = CharterQuoteService.CQRequestEntityState.Modified;

                                if (objCQMain.State == CQRequestEntityState.Modified)
                                {
                                    if (IsAuthorized(Permission.CharterQuote.EditCQQuotesOnFile))
                                    {
                                        SaveToSession = true;
                                    }
                                }
                                else if (objCQMain.State == CQRequestEntityState.Added)
                                {
                                    if (IsAuthorized(Permission.CharterQuote.AddCQQuotesOnFile))
                                    {
                                        SaveToSession = true;
                                    }
                                }

                                if (SaveToSession)
                                {
                                    if (!string.IsNullOrEmpty(rblSource.SelectedValue))
                                        objCQMain.CQSource = Convert.ToDecimal(rblSource.SelectedValue);
                                    else
                                        objCQMain.CQSource = null;


                                    if (!string.IsNullOrEmpty(rblQuoteStage.SelectedValue))
                                        objCQMain.QuoteStage = rblQuoteStage.SelectedValue;
                                    else
                                        objCQMain.QuoteStage = null;

                                    objCQMain.VendorID = null;
                                    objCQMain.Vendor = null;
                                    if (!string.IsNullOrEmpty(hdnVendor.Value))
                                    {
                                        objCQMain.VendorID = Convert.ToInt64(hdnVendor.Value);
                                        Vendor objVendor = new Vendor { VendorID = Convert.ToInt64(hdnVendor.Value), VendorCD = tbVendor.Text, Name = System.Web.HttpUtility.HtmlDecode(lbVendorDesc.Text) };
                                        objCQMain.Vendor = objVendor;
                                    }

                                    objCQMain.FleetID = null;
                                    objCQMain.Fleet = null;
                                    if (!string.IsNullOrEmpty(hdnTailNo.Value))
                                    {
                                        objCQMain.FleetID = Convert.ToInt64(hdnTailNo.Value);
                                        Fleet objFleet = new Fleet { FleetID = Convert.ToInt64(hdnTailNo.Value), TailNum = tbTailNumber.Text };
                                        objCQMain.Fleet = objFleet;
                                    }

                                    objCQMain.AircraftID = null;
                                    objCQMain.Aircraft = null;
                                    if (!string.IsNullOrEmpty(hdnTypeCode.Value))
                                    {
                                        objCQMain.AircraftID = Convert.ToInt64(hdnTypeCode.Value);
                                        Aircraft objAircraft = new Aircraft { AircraftID = Convert.ToInt64(hdnTypeCode.Value), AircraftCD = tbTypeCode.Text, AircraftDescription = System.Web.HttpUtility.HtmlDecode(lbTypeCodeDesc.Text) };
                                        objCQMain.Aircraft = objAircraft;
                                    }

                                    objCQMain.PassengerRequestorID = null;
                                    objCQMain.Passenger = null;
                                    if (!string.IsNullOrEmpty(hdnRequestor.Value))
                                    {
                                        objCQMain.PassengerRequestorID = Convert.ToInt64(hdnRequestor.Value);
                                        Passenger objPax = new Passenger { PassengerRequestorID = Convert.ToInt64(hdnRequestor.Value), PassengerRequestorCD = tbRequestor.Text, PassengerName = System.Web.HttpUtility.HtmlDecode(lbRequestorDesc.Text), PhoneNum = tbRequestorPhone.Text };
                                        objCQMain.Passenger = objPax;
                                    }

                                    objCQMain.FederalTax = 0.0M;
                                    if (!string.IsNullOrEmpty(tbFed.Text))
                                        objCQMain.FederalTax = Convert.ToDecimal(tbFed.Text);
                                    else
                                        objCQMain.FederalTax = 0.0M;
                                    objCQMain.QuoteRuralTax = 0.0M;
                                    if (!string.IsNullOrEmpty(tbRural.Text))
                                        objCQMain.QuoteRuralTax = Convert.ToDecimal(tbRural.Text);
                                    else
                                        objCQMain.QuoteRuralTax = 0.0M;
                                    objCQMain.IsTaxable = chkTaxableQuote.Checked;
                                    objCQMain.IsSubmitted = chkSubmitted.Checked;
                                    objCQMain.IsAccepted = chkAccepted.Checked;
                                    objCQMain.LastSubmitDT = null;
                                    if (!string.IsNullOrEmpty(tbLastSubmittedDt.Text))
                                        objCQMain.LastSubmitDT = DateTime.ParseExact(tbLastSubmittedDt.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture);  //Convert.ToDateTime(tbLastSubmittedDt.Text);
                                    else
                                        objCQMain.LastSubmitDT = null;
                                    objCQMain.LastAcceptDT = null;
                                    if (!string.IsNullOrEmpty(tbLastAcceptedDt.Text))
                                        objCQMain.LastAcceptDT = DateTime.ParseExact(tbLastAcceptedDt.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture); //Convert.ToDateTime(tbLastAcceptedDt.Text);
                                    else
                                        objCQMain.LastAcceptDT = null;
                                    objCQMain.RejectDT = null;
                                    if (!string.IsNullOrEmpty(tbRejectionDt.Text))
                                        objCQMain.RejectDT = DateTime.ParseExact(tbRejectionDt.Text, Master.DatePicker.DateInput.DateFormat, CultureInfo.InvariantCulture); //Convert.ToDateTime(tbRejectionDt.Text);
                                    else
                                        objCQMain.RejectDT = null;
                                    objCQMain.Comments = tbComments.Text;
                                    objCQMain.FederalTax = 0;
                                    if (!string.IsNullOrEmpty(tbFed.Text))
                                        objCQMain.FederalTax = Convert.ToDecimal(tbFed.Text);
                                    else
                                        objCQMain.FederalTax = 0;
                                    objCQMain.QuoteRuralTax = 0;
                                    if (!string.IsNullOrEmpty(tbRural.Text))
                                        objCQMain.QuoteRuralTax = Convert.ToDecimal(tbRural.Text);
                                    else
                                        objCQMain.QuoteRuralTax = 0;
                                    objCQMain.IsInActive = chkInactive.Checked;
                                    CharterQuoteService.CQLostBusiness CqLostBusiness = new CharterQuoteService.CQLostBusiness();
                                    if (!string.IsNullOrWhiteSpace(ddlLostOpportunity.SelectedValue))
                                    {
                                        Int64 LostID = 0;
                                        if (Int64.TryParse(hdnLostOpportunity.Value, out LostID))
                                        {
                                            CqLostBusiness.CQLostBusinessID = Convert.ToInt64(hdnLostOpportunity.Value);
                                            CqLostBusiness.CQLostBusinessCD = ddlLostOpportunity.SelectedValue;
                                            objCQMain.CQLostBusinessID = CqLostBusiness.CQLostBusinessID;
                                            objCQMain.CQLostBusiness = CqLostBusiness;
                                        }
                                    }
                                    else
                                    {
                                        objCQMain.CQLostBusinessID = null;
                                        objCQMain.CQLostBusiness = null;
                                    }

                                    #region Image List Upload
                                    if (Session[Master.CQFileImage] != null)
                                    {
                                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];

                                        objCQMain.ImageList = new List<FileWarehouse>();

                                        foreach (var DicItem in dicImg)
                                        {
                                            FileWarehouse objImageFile = new FileWarehouse();
                                            objImageFile.FileWarehouseID = 0;
                                            objImageFile.RecordType = "CQQuoteOnFile";
                                            objImageFile.UWAFileName = DicItem.Key;
                                            objImageFile.RecordID = objCQMain.CQMainID;
                                            objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                            objImageFile.UWAFilePath = DicItem.Value;
                                            objImageFile.IsDeleted = false;
                                            objCQMain.ImageList.Add(objImageFile);
                                        }
                                    }
                                    else
                                    {
                                        objCQMain.ImageList = new List<FileWarehouse>();
                                    }

                                    #endregion


                                    Session[Master.CQSessionKey] = FileRequest;

                                    if (hdnCalculateQuote.Value == "true")
                                    {
                                        Master.DoLegCalculationsForDomIntlTaxable();
                                        Master.DoLegCalculationsForQuote();
                                        Master.CalculateQuoteTotal();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to Clear Form Fields
        /// </summary>
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                rblSource.ClearSelection();
                rblQuoteStage.ClearSelection();
                tbVendor.Text = string.Empty;
                hdnVendor.Value = string.Empty;
                lbVendorDesc.Text = string.Empty;
                lbVendorMsg.Text = string.Empty;
                tbTailNumber.Text = string.Empty;
                tbTypeCode.Text = string.Empty;
                hdnTailNo.Value = string.Empty;
                hdnTypeCode.Value = string.Empty;
                lbTailNumberMsg.Text = string.Empty;
                lbTypeCodeDesc.Text = string.Empty;
                lbTypeCodeMsg.Text = string.Empty;
                tbRequestor.Text = string.Empty;
                tbRequestorPhone.Text = string.Empty;
                hdnRequestor.Value = string.Empty;
                lbRequestorDesc.Text = string.Empty;
                lbRequestorMsg.Text = string.Empty;
                tbFed.Text = string.Empty;
                tbRural.Text = string.Empty;
                chkTaxableQuote.Checked = false;
                chkSubmitted.Checked = false;
                chkAccepted.Checked = false;
                tbLastSubmittedDt.Text = string.Empty;
                tbLastAcceptedDt.Text = string.Empty;
                tbRejectionDt.Text = string.Empty;
                tbComments.Text = string.Empty;
                if (ddlLostOpportunity.Items.Count > 0)
                    ddlLostOpportunity.SelectedIndex = 0;
                chkInactive.Checked = false;

                ClearLabels();

                Session[Master.CQFileImage] = null;
            }
        }

        private void ClearLabels()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                lbVendorDesc.Text = string.Empty;
                lbVendorMsg.Text = string.Empty;
                lbTailNumberMsg.Text = string.Empty;
                lbTypeCodeDesc.Text = string.Empty;
                lbTypeCodeMsg.Text = string.Empty;
                lbRequestorDesc.Text = string.Empty;
                lbRequestorMsg.Text = string.Empty;
            }
        }

        /// <summary>
        /// Method to Set the Charter Quote Source
        /// </summary>
        /// <param name="selectedSrc"></param>
        protected void SetSourceByValue(string selectedSrc)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(selectedSrc))
            {
                if (!string.IsNullOrEmpty(selectedSrc))
                {
                    rblSource.SelectedValue = selectedSrc;
                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                    if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified))
                    {
                        if (selectedSrc == "1")
                        {
                            if (string.IsNullOrEmpty(hdnVendor.Value))
                            {
                                ControlEnable("TextBox", tbTailNumber, false, "inpt_non_edit text70");
                                ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");
                                ControlEnable("TextBox", tbVendor, true, "text70");

                                ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");
                                ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");
                                ControlEnable("Button", btnVendor, true, "browse-button");
                                ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                                ControlEnable("Button", btnTypeCodeSearch, false, "charter-rate-disable-button");
                            }
                            else
                            {
                                ControlEnable("TextBox", tbTailNumber, true, "text70");
                                ControlEnable("TextBox", tbVendor, true, "text70");
                                ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");
                                ControlEnable("Button", btnTailNumber, true, "browse-button");
                                ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");

                                ControlEnable("Button", btnVendor, true, "browse-button");
                                ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                                ControlEnable("Button", btnTypeCodeSearch, false, "charter-rate-disable-button");
                            }
                        }
                        else if (selectedSrc == "2")
                        {
                            ControlEnable("TextBox", tbTailNumber, true, "text70");
                            ControlEnable("TextBox", tbVendor, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbTypeCode, false, "inpt_non_edit text70");

                            ControlEnable("Button", btnTailNumber, true, "browse-button");
                            ControlEnable("Button", btnTailNumberSearch, true, "charter-rate-button");
                            ControlEnable("Button", btnVendor, false, "browse-button-disabled");
                            ControlEnable("Button", btnTypeCode, false, "browse-button-disabled");
                            ControlEnable("Button", btnTypeCodeSearch, false, "charter-rate-disable-button");
                        }
                        else if (selectedSrc == "3")
                        {
                            ControlEnable("TextBox", tbTailNumber, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbVendor, false, "inpt_non_edit text70");
                            ControlEnable("TextBox", tbTypeCode, true, "text70");

                            ControlEnable("Button", btnTailNumber, false, "browse-button-disabled");
                            ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");

                            ControlEnable("Button", btnVendor, false, "browse-button-disabled");
                            ControlEnable("Button", btnTypeCode, true, "browse-button");
                            ControlEnable("Button", btnTypeCodeSearch, true, "charter-rate-button");
                        }

                        if (string.IsNullOrEmpty(tbTailNumber.Text))
                            ControlEnable("Button", btnTailNumberSearch, false, "charter-rate-disable-button");

                        if (string.IsNullOrEmpty(tbTypeCode.Text))
                            ControlEnable("Button", btnTypeCodeSearch, false, "charter-rate-disable-button");
                    }
                }
                else
                {
                    EnableForm(false);
                }
            }
        }

        /// <summary>
        /// Method to Delete Quote
        /// </summary>
        /// <param name="quoteNum"></param>
        protected void DeleteQuote(Int64 QuoteNum)
        {
            FileRequest = (CQFile)Session[Master.CQSessionKey];

            if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
            {
                Int32 deletedQuoteNum = 0;
                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNum && x.IsDeleted == false).FirstOrDefault();

                List<CQMain> quoteList = new List<CQMain>();
                quoteList = FileRequest.CQMains.Where(x => x.IsDeleted == false).OrderBy(x => x.QuoteNUM).ToList();

                if (QuoteInProgress != null)
                {
                    deletedQuoteNum = Convert.ToInt32(QuoteNum);

                    if (deletedQuoteNum != quoteList.Count)
                    {
                        foreach (CQMain Quote in quoteList)
                        {
                            if (Quote.QuoteNUM > deletedQuoteNum)
                            {
                                Quote.QuoteNUM = Quote.QuoteNUM - 1;
                                if (Quote.CQMainID != 0 && Quote.State != CQRequestEntityState.Deleted)
                                    Quote.State = CQRequestEntityState.Modified;
                            }
                        }
                    }
                    else
                    {
                        hdnQuoteNum.Value = (deletedQuoteNum - 1).ToString();
                        //Session[Master.CQSelectedQuoteNum] = hdnQuoteNum.Value;

                        FileRequest.QuoteNumInProgress = Convert.ToInt64(hdnQuoteNum.Value);
                    }
                    if (QuoteInProgress.CQMainID != null && QuoteInProgress.CQMainID != 0)
                    {
                        QuoteInProgress.IsDeleted = true;
                        QuoteInProgress.State = CQRequestEntityState.Deleted;
                    }
                    else
                        FileRequest.CQMains.Remove(QuoteInProgress);

                    CQMain QuoteToUpdate = new CQMain();
                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        QuoteToUpdate = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress).FirstOrDefault();
                        QuoteToUpdate = QuoteInProgress;
                    }
                    Session[Master.CQSessionKey] = FileRequest;
                }
            }
        }

        private bool isQuoteMovedToPreflightOrInvoiceGenerated(Int64 QuoteNum)
        {
            bool quotemoved = false;

            FileRequest = (CQFile)Session[Master.CQSessionKey];

            if (FileRequest != null)
            {
                if (FileRequest.CQMains != null)
                {
                    CQMain QuotetobeDeleted = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNum && x.IsDeleted == false).SingleOrDefault();
                    //Prakash15-04
                    if (QuotetobeDeleted != null)
                    {
                        if (QuotetobeDeleted.TripID != null)
                            quotemoved = true;
                    }

                    if (!quotemoved)
                    {
                        using (CharterQuoteServiceClient objCharterService = new CharterQuoteServiceClient())
                        {
                            //Prakash15-04
                            if (QuotetobeDeleted != null)
                            {
                                var RetValue = objCharterService.GetCQInvoiceMainByCQMainID(QuotetobeDeleted.CQMainID);
                                if (RetValue.ReturnFlag)
                                {
                                    if (RetValue.EntityList.Count > 0)
                                        quotemoved = true;
                                }
                            }
                        }
                    }
                }
            }
            return quotemoved;
        }

        /// <summary>
        /// Method for Grid Permissions
        /// </summary>
        /// <param name="add">Pass Boolean Value</param>
        /// <param name="edit">Pass Boolean Value</param>
        /// <param name="delete">Pass Boolean Value</param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(add, edit, delete))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    LinkButton insertCtl, editCtl, delCtl;
                    insertCtl = (LinkButton)dgQuote.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
                    delCtl = (LinkButton)dgQuote.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
                    //editCtl = (LinkButton)dgQuote.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");

                    if (IsAuthorized(Permission.CharterQuote.AddCQQuotesOnFile))
                    {
                        insertCtl.Visible = true;
                        if (add)
                        {
                            insertCtl.Enabled = true;
                        }
                        else
                        {
                            insertCtl.Enabled = false;
                        }
                    }
                    else
                    {
                        insertCtl.Visible = false;
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQQuotesOnFile))
                    {
                        delCtl.Visible = true;
                        if (delete)
                        {
                            delCtl.Enabled = true;
                            // delCtl.OnClientClick = "javascript:return ProcessDelete();";
                        }
                        else
                        {
                            delCtl.Enabled = false;
                            delCtl.OnClientClick = string.Empty;
                        }
                    }
                    else
                    {
                        delCtl.Visible = false;
                    }
                    //if (IsAuthorized(Permission.CharterQuote.EditCQQuotesOnFile))
                    //{
                    //    editCtl.Visible = true;
                    //    if (edit)
                    //    {
                    //        editCtl.Enabled = true;
                    //        editCtl.OnClientClick = "javascript:return ProcessUpdate();";
                    //    }
                    //    else
                    //    {
                    //        editCtl.Enabled = false;
                    //        editCtl.OnClientClick = string.Empty;
                    //    }
                    //}
                    //else
                    //{
                    //    editCtl.Visible = false;
                    //}
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        #endregion

        #region Button Events

        protected void Copy_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.CopyQuote();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// To delete the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        if (FileRequest != null)
                            Master.Delete((int)FileRequest.FileNUM);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// To cancel the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// To save the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Page.Validate();
                        if (Page.IsValid)
                        {
                            SaveCharterQuoteToSession();
                            FileRequest = (CQFile)Session[Master.CQSessionKey];
                            Master.Save(FileRequest);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        /// <summary>
        /// To move next tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Next_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                        Master.Next("Quote On File");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }
        /// <summary>
        /// For Displaying Msg from Live Trip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnOkMsg_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["LiveTrip"] == "yes")
                        {
                            using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];
                                var objCQFile = Service.GetCQRequestByID(FileRequest.CQFileID, false);
                                if (objCQFile.ReturnFlag)
                                {
                                    FileRequest = objCQFile.EntityList[0];
                                    Session[Master.CQSessionKey] = (CQFile)FileRequest;
                                    #region Load Saved Image File List to Quotes

                                    if (FileRequest.CQMains != null)
                                    {
                                        foreach (CQMain quote in FileRequest.CQMains)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                {
                                                    foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                    {
                                                        CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                        objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                        objImageFile.RecordType = "CQQuoteOnFile";
                                                        objImageFile.UWAFileName = fwh.UWAFileName;
                                                        objImageFile.RecordID = quote.CQMainID;
                                                        objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                        objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                        objImageFile.IsDeleted = false;
                                                        if (quote.ImageList == null)
                                                            quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                        quote.ImageList.Add(objImageFile);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"SaveCloseAndRebindLivePage();");
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        #endregion


        protected void TaxRate_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        // MR07032013 - Updated code for Company Profile setting - Deactivate Auto Updating of Rates
        protected void ConfirmAutoUpdateRateYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Master.BindFleetChargeDetails(!string.IsNullOrEmpty(hdnTailNo.Value) ? Convert.ToInt64(hdnTailNo.Value) : 0, !string.IsNullOrEmpty(hdnTypeCode.Value) ? Convert.ToInt64(hdnTypeCode.Value) : 0, 0, false);
                SaveCharterQuoteToSession();
                BindCharterQuoteFromSession();
            }
        }


        #region CQImage
        ///// <summary>
        ///// ImageToBinary
        ///// </summary>
        ///// <param name="imagePath"></param>
        ///// <returns></returns>
        //public static byte[] CQImageToBinary(string imagePath)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(imagePath))
        //    {
        //        FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
        //        byte[] buffer = new byte[fileStream.Length];
        //        fileStream.Read(buffer, 0, (int)fileStream.Length);
        //        fileStream.Close();
        //        return buffer;
        //    }
        //}
        /// <summary>
        /// To Load Image
        /// </summary>
        private void LoadCQImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                System.IO.Stream myStream;
                Int32 fileLen;
                if (fuCQ.PostedFile != null)
                {
                    if (fuCQ.PostedFile.ContentLength > 0)
                    {
                        //if (ddlCQFileName.Items.Count >= 0)
                        //{
                        string FileName = fuCQ.FileName;
                        fileLen = fuCQ.PostedFile.ContentLength;
                        Byte[] Input = new Byte[fileLen];
                        myStream = fuCQ.FileContent;
                        myStream.Read(Input, 0, fileLen);
                        
                        //Ramesh: Set the URL for image file or link based on the file type
                        SetURL(FileName, Input);

                        ////Ramesh: Code to allow any file type for upload control.
                        //int iIndex = FileName.IndexOf('.');
                        //string strExtn = FileName.Substring(iIndex, FileName.Length - iIndex);
                        ////Match regMatch = Regex.Match(strExtn, @"\.([Gg][Ii][Ff])|\.([Jj][Pp][Gg])|\.([Bb][Mm][Pp])|\.([jJ][pP][eE][gG])$", RegexOptions.IgnoreCase);
                        //Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                        //if (regMatch.Success)
                        //{
                        //    //start of modification for image issue
                        //    if (hdnBrowserName.Value == "Chrome")
                        //    {
                        //        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                        //        lnkFileName.NavigateUrl = "";
                        //    }
                        //    else
                        //    {
                        //        Session["CQQuoteOnFileBase64"] = Input;
                        //        imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                        //        lnkFileName.NavigateUrl = "";
                        //    }
                        //    //end of modification for image issue
                        //}
                        //else
                        //{
                        //    imgCQ.Style.Add("display", "false");
                        //    lnkFileName.Style.Add("display", "true");
                        //    Session["CQQuoteOnFileBase64"] = Input;
                        //    lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + FileName;
                        //    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");                            
                        //}
                        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                        if (Session[Master.CQFileImage] == null)
                            Session[Master.CQFileImage] = new Dictionary<string, byte[]>();
                        dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                        int count = dicImg.Count(D => D.Key.Equals(FileName));
                        if (count > 0)
                        {
                            dicImg[FileName] = Input;
                        }
                        else
                        {
                            #region load added image to Quote
                            if (Session[Master.CQFileImage] != null)
                            {
                                if (Session[Master.CQSessionKey] != null)
                                {
                                    FileRequest = (CQFile)Session[Master.CQSessionKey];

                                    CQMain objCQMain = new CQMain();
                                    Int64 QuoteNumInProgress = 1;
                                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                    {
                                        objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                        if (objCQMain != null)
                                        {
                                            if (objCQMain.ImageList == null)
                                                objCQMain.ImageList = new List<FileWarehouse>();

                                            FileWarehouse objImageFile = new FileWarehouse();

                                            objImageFile = objCQMain.ImageList.Where(x => x.IsDeleted == false && x.UWAFileName == FileName).SingleOrDefault();

                                            if (objImageFile == null)
                                            {
                                                objImageFile = new FileWarehouse();
                                                objImageFile.FileWarehouseID = 0;
                                                objImageFile.RecordType = "CQQuoteOnFile";
                                                objImageFile.UWAFileName = FileName;
                                                objImageFile.RecordID = objCQMain.CQMainID;
                                                objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                objImageFile.UWAFilePath = Input;
                                                objImageFile.IsDeleted = false;
                                                objCQMain.ImageList.Add(objImageFile);
                                            }
                                        }
                                        Session[Master.CQSessionKey] = FileRequest;
                                    }
                                }
                            }
                            #endregion


                            //DeleteCQOtherImage();
                            dicImg.Add(FileName, Input);
                            // ddlImg.Items.Clear();
                            ddlCQFileName.Items.Insert(ddlCQFileName.Items.Count, new ListItem(System.Web.HttpUtility.HtmlEncode(FileName), FileName));

                            //Ramesh: Set the URL for image file or link based on the file type
                            SetURL(FileName, Input);

                            ////Ramesh: Code to allow any file type for upload control.
                            //int iIndex1 = FileName.IndexOf('.');
                            //string strExtn1 = FileName.Substring(iIndex1, FileName.Length - iIndex1);                            
                            //Match regMatch1 = Regex.Match(strExtn1, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                            //if (regMatch1.Success)
                            //{
                            //    //start of modification for image issue
                            //    if (hdnBrowserName.Value == "Chrome")
                            //    {
                            //        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(Input);
                            //        lnkFileName.NavigateUrl = "";
                            //    }
                            //    else
                            //    {
                            //        Session["CQQuoteOnFileBase64"] = Input;
                            //        imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                            //        lnkFileName.NavigateUrl = "";
                            //    }
                                
                            //}
                            //else
                            //{
                            //    imgCQ.Style.Add("display", "false");
                            //    lnkFileName.Style.Add("display", "true");
                            //    Session["CQQuoteOnFileBase64"] = Input;
                            //    lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + FileName;
                            //    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");                                
                            //}
                            if (ddlCQFileName.Items.Count != 0)
                            {
                                //ddlImg.SelectedIndex = ddlImg.Items.Count - 1;
                                ddlCQFileName.SelectedValue = FileName;
                            }
                            //end of modification for image issue
                        }
                        tbCQimgName.Text = "";
                        radbtnCQDelete.Enabled = true;
                        //}
                    }
                }
            }
        }
        /// <summary>
        /// To Delete Other Image
        /// </summary>
        private void DeleteCQOtherImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                if (Session[Master.CQFileImage] == null)
                    Session[Master.CQFileImage] = new Dictionary<string, byte[]>();
                dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                int count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                if (count > 0)
                {
                    dicImg.Remove(ddlCQFileName.Text.Trim());
                    Session[Master.CQFileImage] = dicImg;
                    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CQQuoteOnFileDicImgDelete"];
                    dicImgDelete.Add(ddlCQFileName.Text.Trim(), "");
                    Session["CQQuoteOnFileDicImgDelete"] = dicImgDelete;
                    ddlCQFileName.Items.Clear();
                    ddlCQFileName.Text = "";
                    tbCQimgName.Text = "";
                    int i = 0;
                    foreach (var DicItem in dicImg)
                    {
                        ddlCQFileName.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(DicItem.Key), DicItem.Key));
                        i = i + 1;
                    }
                    imgCQ.ImageUrl = "";
                    imgCQPopup.ImageUrl = null;
                    lnkFileName.NavigateUrl = "";
                }
            }
        }
        /// <summary>
        /// <summary>
        /// Dropdown for image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCQFileName_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlCQFileName.Text.Trim() == "")
                        {
                            imgCQ.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                        }
                        if (ddlCQFileName.SelectedItem != null)
                        {
                            bool imgFound = false;
                            imgCQ.ImageUrl = "";
                            lnkFileName.NavigateUrl = "";
                            using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                // need/ to change
                                long _quoteNum = (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress != 0) ? FileRequest.QuoteNumInProgress : 1;
                                var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", _quoteNum).EntityList.Where(x => x.UWAFileName == ddlCQFileName.Text.Trim());
                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg)
                                {
                                    imgFound = true;
                                    byte[] picture = fwh.UWAFilePath;

                                    //Ramesh: Set the URL for image file or link based on the file type
                                    SetURL(fwh.UWAFileName, picture);

                                    //Ramesh: Code to allow any file type for upload control.
                                    //int iIndex = fwh.UWAFileName.IndexOf('.');
                                    //string strExtn = fwh.UWAFileName.Substring(iIndex, fwh.UWAFileName.Length - iIndex);                                   
                                    //Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                                    //if (regMatch.Success)
                                    //{
                                    //    //start of modification for image issue
                                    //    if (hdnBrowserName.Value == "Chrome")
                                    //    {
                                    //        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(picture);
                                    //        lnkFileName.NavigateUrl = "";
                                    //    }
                                    //    else
                                    //    {
                                    //        Session["CQQuoteOnFileBase64"] = picture;
                                    //        imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                                    //        lnkFileName.NavigateUrl = "";
                                    //    }
                                    //    //end of modification for image issue
                                    //}
                                    //else
                                    //{
                                    //    imgCQ.Style.Add("display", "false");
                                    //    lnkFileName.Style.Add("display", "true");
                                    //    Session["CQQuoteOnFileBase64"] = picture;
                                    //    lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + fwh.UWAFileName;
                                    //    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");                                        
                                    //}

                                }
                                tbCQimgName.Text = ddlCQFileName.Text.Trim();
                                if (!imgFound)
                                {
                                    //Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session["CQDicCqImg"];
                                    Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
                                    if (Session[Master.CQFileImage] == null)
                                        Session[Master.CQFileImage] = new Dictionary<string, byte[]>();
                                    dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                                    int count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                                    if (count > 0)
                                    {
                                        //Ramesh: Set the URL for image file or link based on the file type
                                        SetURL(ddlCQFileName.Text, dicImg[ddlCQFileName.Text.Trim()]);

                                        ////Ramesh: Code to allow any file type for upload control.
                                        //int iIndex1 = ddlCQFileName.Text.Trim().IndexOf('.');
                                        //string strExtn1 = ddlCQFileName.Text.Trim().Substring(iIndex1, ddlCQFileName.Text.Trim().Length - iIndex1);                                        
                                        //Match regMatch1 = Regex.Match(strExtn1, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                                        //if (regMatch1.Success)
                                        //{
                                        //    //start of modification for image issue
                                        //    if (hdnBrowserName.Value == "Chrome")
                                        //    {
                                        //        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlCQFileName.Text.Trim()]);
                                        //        lnkFileName.NavigateUrl = "";
                                        //    }
                                        //    else
                                        //    {

                                        //        Session["CQQuoteOnFileBase64"] = dicImg[ddlCQFileName.Text.Trim()];
                                        //        imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                                        //        lnkFileName.NavigateUrl = "";
                                        //    }
                                        //    //end of modification for image issue
                                        //}
                                        //else
                                        //{
                                        //    imgCQ.Style.Add("display", "false");
                                        //    lnkFileName.Style.Add("display", "true");
                                        //    Session["CQQuoteOnFileBase64"] = dicImg[ddlCQFileName.Text.Trim()];
                                        //    lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + ddlCQFileName.Text.Trim();
                                        //    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");                                           
                                        //}
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        protected void btncqdeleteImage_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlCQFileName.Items.Count > 0)
                        {
                            RadWindowManager1.RadConfirm("Are you sure you want to delete document type " + ddlCQFileName.SelectedItem.Text + "?", "confirmDeleteImgCallBack", 330, 100, null, "Confirmation!");
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Image File should not be Empty", 330, 100, ModuleNameConstants.CharterQuote.CQOnFile, null);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }

        }


        /// <summary>
        /// To delete image For Cq
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ConfirmDeleteImgYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Dictionary<string, byte[]> dicCqImg = new Dictionary<string, byte[]>();
                        if (Session[Master.CQFileImage] != null)
                        {
                            dicCqImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                            dicCqImg.Remove(ddlCQFileName.Text.Trim());

                            if (Session[Master.CQSessionKey] != null)
                            {
                                FileRequest = (CQFile)Session[Master.CQSessionKey];

                                CQMain objCQMain = new CQMain();

                                //Int32 quoteNum = Convert.ToInt32(Session[Master.CQSelectedQuoteNum]);

                                Int64 QuoteNumInProgress = 1;
                                if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                    QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                                {
                                    objCQMain = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                    if (objCQMain != null && objCQMain.ImageList != null)
                                    {
                                        FileWarehouse deletedImage = new FileWarehouse();
                                        deletedImage = objCQMain.ImageList.Where(x => x.UWAFileName.ToLower() == ddlCQFileName.Text.Trim().ToLower() && x.IsDeleted == false).SingleOrDefault();

                                        if (deletedImage != null)
                                        {
                                            if (objCQMain.CQMainID != 0)
                                                deletedImage.IsDeleted = true;
                                            else
                                                objCQMain.ImageList.Remove(deletedImage);

                                            Session[Master.CQSessionKey] = FileRequest;

                                            ddlCQFileName.Items.Clear();
                                            ddlCQFileName.Text = "";
                                            int i = 0;

                                            Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();

                                            foreach (FileWarehouse objFile in objCQMain.ImageList.Where(x => x.IsDeleted == false))
                                            {
                                                ddlCQFileName.Items.Insert(i, new ListItem(System.Web.HttpUtility.HtmlEncode(objFile.UWAFileName), objFile.UWAFileName));
                                                dicImg.Add(objFile.UWAFileName, objFile.UWAFilePath);
                                                //Ramesh: Commented repeated code.
                                                //if (i == 0)
                                                //{
                                                //    byte[] picture = objFile.UWAFilePath;
                                                //    //Ramesh: Code to allow any file type for upload control.
                                                //    int iIndex = objFile.UWAFileName.IndexOf('.');
                                                //    string strExtn = objFile.UWAFileName.Substring(iIndex, objFile.UWAFileName.Length - iIndex);
                                                //    //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
                                                //    Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                                                //    if (regMatch.Success)
                                                //    {
                                                //        //start of modification for image issue
                                                //        if (hdnBrowserName.Value == "Chrome")
                                                //        {
                                                //            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("data:image/jpeg;base64," + Convert.ToBase64String(picture));
                                                //            lnkFileName.NavigateUrl = "";
                                                //        }
                                                //        else
                                                //        {
                                                //            Session["CQQuoteOnFileBase64"] = picture;
                                                //            imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid());
                                                //            lnkFileName.NavigateUrl = "";
                                                //        }
                                                //        //end of modification for image issue
                                                //    }
                                                //    else
                                                //    {
                                                //        imgCQ.Style.Add("display", "false");
                                                //        lnkFileName.Style.Add("display", "true");
                                                //        Session["CQQuoteOnFileBase64"] = picture;
                                                //        lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + objFile.UWAFileName;
                                                //        imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                                                //        //Converting the Byte Array to a WordDocument Again (Retrieved from DB)
                                                //        //FileStream fs = new FileStream(Request.PhysicalApplicationPath + "\\Uploads\\" + FileName, FileMode.Create, FileAccess.ReadWrite);
                                                //        //BinaryWriter bw = new BinaryWriter(fs);
                                                //        //bw.Write(Input);
                                                //        //bw.Close();
                                                //        //HttpContext context;
                                                //        //context.
                                                //        //Response.Clear();
                                                //        //Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fwh.UWAFileName + "\"");
                                                //        //Response.AddHeader("Content-Length", picture.Length.ToString());
                                                //        //Response.ContentType = "application/octet-stream";
                                                //        //Response.BinaryWrite(picture);
                                                //        //Response.Flush();
                                                //    }
                                                //}
                                                i = i + 1;
                                            }
                                            Session[Master.CQFileImage] = dicImg;

                                            if (ddlCQFileName.Items.Count > 0)
                                            {
                                                radbtnCQDelete.Enabled = true;
                                                ddlCQFileName.SelectedIndex = 0;
                                                ddlCQFileName_SelectedIndexChanged(sender, e);
                                                //Session["CQQuoteOnFileBase64"] = dicImg[ddlCQFileName.SelectedValue];
                                                //imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/Views/Settings/ImageHandler.ashx?cd=" + dicImg[ddlCQFileName.SelectedValue]);
                                            }
                                            else
                                            {
                                                radbtnCQDelete.Enabled = false;
                                                imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                                                lnkFileName.NavigateUrl = "";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        #region Commented Codes
                        //int count = dicCqImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));

                        //if (count > 0)
                        //{
                        //    dicCqImg.Remove(ddlCQFileName.Text.Trim());
                        //    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["CQQuoteOnFileDicImgDelete"];
                        //    if (Session["CQQuoteOnFileDicImgDelete"] != null)
                        //        if (dicImgDelete.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim())) == 0)
                        //        {
                        //            dicImgDelete.Add(ddlCQFileName.Text.Trim(), "");
                        //        }
                        //    Session["CQQuoteOnFileDicImgDelete"] = dicImgDelete;
                        //    ddlCQFileName.Items.Clear();
                        //    ddlCQFileName.Text = "";
                        //    tbCQimgName.Text = "";
                        //    int i = 0;
                        //    foreach (var DicItem in dicCqImg)
                        //    {
                        //        ddlCQFileName.Items.Insert(i, new ListItem(DicItem.Key, DicItem.Key));
                        //        i = i + 1;
                        //    }
                        //    if (ddlCQFileName.Items.Count > 0)
                        //    {
                        //        Dictionary<string, byte[]> dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                        //        int Count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                        //        if (Count > 0)
                        //        {
                        //            //start of modification for image issue
                        //            if (hdnBrowserName.Value == "Chrome")
                        //            {
                        //                imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlCQFileName.Text.Trim()]);
                        //            }
                        //            else
                        //            {
                        //                Session["CQBase64"] = dicImg[ddlCQFileName.Text.Trim()];
                        //                imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQBase64=CQBase64&cd=" + Guid.NewGuid();
                        //            }
                        //            //end of modification for image issue
                        //        }
                        //    }
                        //    else
                        //    {
                        //        imgCQ.ImageUrl = "~/App_Themes/Default/images/no-image.jpg";
                        //    }
                        //    imgCQPopup.ImageUrl = null;
                        //    //imgCQ.ImageUrl = "../../../App_Themes/Default/images/noimage.jpg";
                        //    //imgCQPopup.ImageUrl = null;
                        //}
                        //else
                        //{
                        //    dicCqImg.Remove(ddlCQFileName.Text.Trim());
                        //    Session[Master.CQFileImage] = dicCqImg;
                        //    ddlCQFileName.Items.Clear();
                        //    ddlCQFileName.Text = "";
                        //    tbCQimgName.Text = "";
                        //    imgCQ.ImageUrl = "~/App_Themes/Default/images/no-image.jpg";
                        //    imgCQPopup.ImageUrl = null;
                        //    Dictionary<string, string> dicImgDelete = (Dictionary<string, string>)Session["QuoteOnFileDicCQImgDelete"];
                        //    if (Session["QuoteOnFileDicCQImgDelete"] != null)
                        //        if (dicImgDelete.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim())) == 0)
                        //        {
                        //            dicImgDelete.Add(ddlCQFileName.Text.Trim(), "");
                        //        }
                        //    Session["QuoteOnFileDicCQImgDelete"] = dicImgDelete;
                        //}
                        #endregion

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }
        /// <summary>
        /// To Create dictioanry for image upload for CQ
        /// </summary>
        private void CreateCqDictionayForImgUpload()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Dictionary<string, byte[]> cqDicImg = new Dictionary<string, byte[]>();
                Session[Master.CQFileImage] = cqDicImg;
                Dictionary<string, string> cqDicImgDelete = new Dictionary<string, string>();
                Session["CQQuoteOnFileDicImgDelete"] = cqDicImgDelete;
            }
        }
        #endregion

        private void LoadCQImageFromSession()
        {
            Dictionary<string, byte[]> dicImg = new Dictionary<string, byte[]>();
            if (Session[Master.CQFileImage] != null)
            {

                dicImg = (Dictionary<string, byte[]>)Session[Master.CQFileImage];
                int count = dicImg.Count(D => D.Key.Equals(ddlCQFileName.Text.Trim()));
                if (count > 0)
                {
                    SetURL(ddlCQFileName.Text, dicImg[ddlCQFileName.SelectedItem.Text.Trim()]);
                    ////Ramesh: Code to allow any file type for upload control.
                    //int iIndex = ddlCQFileName.Text.Trim().IndexOf('.');
                    //string strExtn = ddlCQFileName.Text.Trim().Substring(iIndex, ddlCQFileName.Text.Trim().Length - iIndex);
                    ////Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
                    //Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
                    //if (regMatch.Success)
                    //{
                    //    //start of modification for image issue
                    //    if (hdnBrowserName.Value == "Chrome")
                    //    {
                    //        imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(dicImg[ddlCQFileName.SelectedItem.Text.Trim()]);
                    //        lnkFileName.NavigateUrl = "";
                    //    }
                    //    else
                    //    {
                    //        Session["CQQuoteOnFileBase64"] = dicImg[ddlCQFileName.SelectedItem.Text.Trim()];
                    //        imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                    //        lnkFileName.NavigateUrl = "";
                    //    }
                    //    //end of modification for image issue
                    //}
                    //else
                    //{
                    //    imgCQ.Style.Add("display", "false");
                    //    lnkFileName.Style.Add("display", "true");
                    //    Session["CQQuoteOnFileBase64"] = dicImg[ddlCQFileName.SelectedItem.Text.Trim()];
                    //    lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + ddlCQFileName.Text.Trim();
                    //    imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");                        
                    //}
                }
            }
        }

        protected void AlertOK_Click(object sender, EventArgs e)
        { }

        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditFile_Click(sender, e);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        /// <summary>
        /// Common method to set the URL for image and Download file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="filedata"></param>
        private void SetURL(string filename, byte[] filedata)
        {
            //Ramesh: Code to allow any file type for upload control.
            int iIndex = filename.Trim().IndexOf('.');
          //  string strExtn = filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex);
            string strExtn = iIndex > 0 ? filename.Trim().Substring(iIndex, filename.Trim().Length - iIndex) : FlightPak.Common.Utility.GetImageFormatExtension(filedata);  
            //Match regMatch = Regex.Match(strExtn, @"[^\s]+(?=.([jJ][pP][gG]|[gG][iI][fF]|[bB][mM][pP]|[jJ][pP][eE][gG]|[tT][iI][fF][fF]))", RegexOptions.IgnoreCase);
            string SupportedImageExtPatterns = System.Configuration.ConfigurationManager.AppSettings["SupportedImageExtPatterns"];
            Match regMatch = Regex.Match(strExtn, SupportedImageExtPatterns, RegexOptions.IgnoreCase);
            if (regMatch.Success)
            {
                if (Request.UserAgent.Contains("Chrome"))
                {
                    hdnBrowserName.Value = "Chrome";
                }

                //start of modification for image issue
                if (hdnBrowserName.Value == "Chrome")
                {
                    imgCQ.Visible = true;
                    imgCQ.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(filedata);
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                else
                {
                    Session["CQQuoteOnFileBase64"] = filedata; 
                    imgCQ.Visible = true;
                    imgCQ.ImageUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid();
                    lnkFileName.NavigateUrl = "";
                    lnkFileName.Visible = false;
                }
                //end of modification for image issue
            }
            else
            {
                imgCQ.ImageUrl = System.Web.HttpUtility.HtmlEncode("~/App_Themes/Default/images/no-image.jpg");
                imgCQ.Visible = false;
                lnkFileName.Visible = true;
                Session["CQQuoteOnFileBase64"] = filedata;
                lnkFileName.NavigateUrl = "~/Views/Settings/ImageHandler.ashx?CQQuoteOnFileBase64=CQQuoteOnFileBase64&cd=" + Guid.NewGuid() + "&filename=" + filename.Trim();                
            }
        }
    }
}
