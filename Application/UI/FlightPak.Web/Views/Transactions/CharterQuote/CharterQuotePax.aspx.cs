﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Collections;
using System.ComponentModel;
using FlightPak.Web.CharterQuoteService;
using System.Globalization;
using FlightPak.Web.Framework.Helpers.Preflight;
//For Tracing and Exception Handling
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;
using System.Collections.Generic;

namespace FlightPak.Web.Views.CharterQuote
{
    [Serializable]
    public partial class CharterQuotePax : BaseSecuredPage
    {
        public CQFile FileRequest = new CQFile();
        public CQMain QuoteInProgress = new CQMain();
        private ExceptionManager exManager;
        public int IDcount = 0;
        private delegate void SaveSession();
        private delegate void SavePAXSession();
        DataRow[] drarrayPaxSummary;
        //string UserPrincipal.Identity._fpSettings._ApplicationDateFormat = "MM/dd/yyyy";
        public Int32 SeatTotal = 0;

        // Declaration for Exception
        List<FlightPak.Web.PreflightService.RulesException> ErrorList = new List<FlightPak.Web.PreflightService.RulesException>();

        private int paxCount = 0;
        private int ACount = 0;
        private static int? maxpassenger;
        public static int Availcount = 0;
        public static int footerCnt = 0;

#region comment 
        //Defect 2662 Fixed By:Prabhu. 21/01/2014
        //Adding Pax is not populating the default flight purpose.
#endregion

        /// <summary>
        /// Wire up the SaveCharterQuoteToSession to the event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveCharterQuoteFileSession = new SaveSession(SavePreflightToSessionOnTabChange);
                        Master.SaveToSession = SaveCharterQuoteFileSession;

                        SavePAXSession SavePAXSession = new SavePAXSession(SaveCharterQuoteToSession);
                        Master.SavePAXToSession = SavePAXSession;

                        //SaveSession SaveCharterQuoteFileSession = new SaveSession(SaveCharterQuoteToSession);
                        //Master.SaveToSession = SaveCharterQuoteFileSession;

                        Master.RadAjax_AjaxRequest += RadAjaxManager1_AjaxRequest;

                        Master._DeleteClick += Delete_Click;
                        Master._SaveClick += Save_Click;
                        Master._CancelClick += Cancel_Click;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                RadScriptManager.GetCurrent(Page).AsyncPostBackTimeout = 36000;
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnEditFile, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Master.MasterForm.FindControl("DivMasterForm"), Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting((RadAjaxManager)RadAjaxManager.GetCurrent(Page), DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(DivExternalForm, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAvailablePax, dgAvailablePax, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPaxSummary, dgPaxSummary, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(pnlPax, pnlPax, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, pnlPax, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, pnlPax, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, pnlPax, RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(SearchBox, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Submit1, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(ddlFlightPurpose, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAvailablePax, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPaxSummary, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDelete, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxRemoveYes, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxRemoveNo, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxNextYes, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxNextNo, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeletePaxYes, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeletePaxNo, Master.MasterForm.FindControl("DivMasterForm"), RadAjaxLoadingPanel1);

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(SearchBox, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(Submit1, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(ddlFlightPurpose, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgAvailablePax, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgPaxSummary, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDelete, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnCancel, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSave, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnNext, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxRemoveYes, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxRemoveNo, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxNextYes, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnPaxNextNo, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeletePaxYes, DivExternalForm, RadAjaxLoadingPanel1);
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnDeletePaxNo, DivExternalForm, RadAjaxLoadingPanel1);




                        if (Session[Master.CQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[Master.CQSessionKey];

                            if (FileRequest.CQCustomerID != null)
                                hdnCQCustomerID.Value = FileRequest.CQCustomerID.ToString();

                            if (FileRequest.CQMains != null && FileRequest.CQMains.Where(x => x.IsDeleted == false).Count() > 0)
                            {
                                Int64 QuoteNumInProgress = 1;
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                                if (QuoteInProgress != null)
                                {
                                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                                    {
                                        QuoteInProgress.Mode = CQRequestActionMode.Edit;

                                        if (QuoteInProgress.CQLegs == null || QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).Count() == 0)
                                            Response.Redirect("QuoteAndLegDetails.aspx?seltab=QuoteAndLeg");
                                    }
                                }
                                else
                                    Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile");
                            }
                            else
                                Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile");
                        }
                        else
                            Response.Redirect("CharterQuoteRequest.aspx?seltab=File");

                        if (!IsPostBack)
                        {
                            //Prakash Master.ValidateLegExists();

                            Session.Remove("CQAvailablePaxList");
                            Session.Remove("CQPaxSummaryList");
                            Session.Remove("CQSelectedPax");
                            Session.Remove("CQPAXnotAssigned");
                            Session.Remove("CQSelectedPaxitems");
                            Session.Remove("CQSelectedPassport");
                            Session.Remove("CQSelectedPassportID");
                            Session.Remove("CQSelectedVisaID");
                            Session.Remove("CQSelectedPrefPaxPassportExpiryDT");
                            Session.Remove("CQSelectedPrefPaxCountryCD");


                            #region Authorization
                            //ApplyPermissions();
                            #endregion

                            CalculateSeatTotal();
                            //BindPreflightFromSession();
                            BindFlightPurpose(ddlFlightPurpose, 0, string.Empty);

                            hdnLeg.Value = "1";
                            if (Session[Master.CQSessionKey] != null)
                            {
                                //Prakash hdnLeg.Value = (Convert.ToInt16(rtsLegs.Tabs[0].Value) + 1).ToString();                                
                                if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count > 0)
                                {
                                    //Prakash LoadTransportChoice(Trip.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1]);
                                    hdnArrivalICao.Value = QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].AAirportID.ToString();
                                    hdnDepartIcao.Value = QuoteInProgress.CQLegs[Convert.ToInt16(hdnLeg.Value) - 1].DAirportID.ToString();
                                    LoadHomebaseSettings(!IsPostBack);
                                    LoadPaxfromTrip();
                                    //LoadAddiLegtoPaxsummary();
                                    if (dgPaxSummary.Items.Count > 0)
                                    {
                                        dgPaxSummary.SelectedIndexes.Add(0);
                                        dgPaxSummary.Items[0].Selected = true;
                                        GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                                        Label lblPassengerRequestorID = (Label)item.FindControl("lblPassengerRequestorID");
                                        Label lblName = (Label)item.FindControl("lblName");
                                        Label lblLegID = (Label)item.FindControl("lblLegID");
                                        //hdnLegCode.Value = lblLegID.Text;                                       
                                    }
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        private void CalculateSeatTotal()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {

                        if (QuoteInProgress.FleetID != null && QuoteInProgress.FleetID != 0)
                        {
                            var FleetVal = FleetService.GetFleet().EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();
                            if (FleetVal.Count > 0)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                {
                                    for (int i = 0; i < QuoteInProgress.CQLegs.Count; i++)
                                    {
                                        if (FleetVal[0].MaximumPassenger != null)
                                        {
                                            //PS

                                            SeatTotal = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);

                                            if (QuoteInProgress.CQLegs[i].PassengerTotal != null)
                                                QuoteInProgress.CQLegs[i].ReservationAvailable = SeatTotal - QuoteInProgress.CQLegs[i].PassengerTotal;
                                            else
                                                QuoteInProgress.CQLegs[i].ReservationAvailable = SeatTotal;

                                            //Doubt QuoteInProgress.CQLegs[i].SeatTotal = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);
                                            maxpassenger = Convert.ToInt32((decimal)FleetVal[0].MaximumPassenger);
                                        }
                                        else
                                        {
                                            //PS
                                            SeatTotal = 0;
                                            //Doubt QuoteInProgress.CQLegs[i].SeatTotal = 0;
                                            maxpassenger = 0;
                                        }
                                        //PS
                                        QuoteInProgress.CQLegs[i].ReservationAvailable = SeatTotal - QuoteInProgress.CQLegs[i].PassengerTotal;
                                        //Doubt QuoteInProgress.CQLegs[i].ReservationAvailable = QuoteInProgress.CQLegs[i].SeatTotal - QuoteInProgress.CQLegs[i].PassengerTotal;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (QuoteInProgress.CQLegs != null)
                            {
                                for (int i = 0; i < QuoteInProgress.CQLegs.Count; i++)
                                {
                                    //PS
                                    SeatTotal = 0;
                                    //Doubt   QuoteInProgress.CQLegs[i].SeatTotal = 0;
                                }
                            }
                            maxpassenger = 0;
                        }
                    }
                }
            }
        }

        private void LoadAddiLegtoPaxsummary()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    List<PassengerSummary> NewpassengerSummary = new List<PassengerSummary>();
                    List<PassengerSummary> OldpassengerSummary = new List<PassengerSummary>();
                    List<PassengerSummary> FinalpassengerSummary = new List<PassengerSummary>();

                    if (Session["CQPaxSummaryList"] != null)
                    {
                        OldpassengerSummary = (List<PassengerSummary>)Session["CQPaxSummaryList"];
                    }

                    if (QuoteInProgress.CQLegs != null)
                    {
                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        for (int i = 0; i < Preflegs.Count; i++)
                        {
                            if (Preflegs[i].IsDeleted == false)
                            {
                                if (Preflegs[i].CQPassengers == null || Preflegs[i].CQPassengers.Count == 0)
                                {
                                    foreach (GridDataItem dataitem in dgAvailablePax.Items)
                                    {
                                        string PaxPassportID = string.Empty;
                                        string PassportNumber = string.Empty;
                                        string street = string.Empty;
                                        string postal = string.Empty;
                                        string city = string.Empty;
                                        string state = string.Empty;
                                        string expirydt = string.Empty;
                                        string nation = string.Empty;
                                        string billingcode = string.Empty;
                                        string dateofbirth = string.Empty;

                                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var objflightRetVal = objDstsvc.GetPassengerPassportByPassengerRequestorID(Convert.ToInt64(dataitem["PassengerRequestorID"].Text));
                                            if (objflightRetVal.ReturnFlag == true)
                                            {
                                                if (objflightRetVal.EntityList.Count > 0)
                                                {
                                                    PaxPassportID = objflightRetVal.EntityList[0].PassportID.ToString();
                                                    PassportNumber = objflightRetVal.EntityList[0].PassportNum;
                                                    //if (!string.IsNullOrEmpty(objflightRetVal.EntityList[0].PassportExpiryDT.ToString()))
                                                    if (!string.IsNullOrEmpty(Convert.ToString(objflightRetVal.EntityList[0].PassportExpiryDT)))
                                                    {
                                                        DateTime dtDate = Convert.ToDateTime(objflightRetVal.EntityList[0].PassportExpiryDT);
                                                        if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                            expirydt = dtDate.ToString();
                                                        else
                                                            expirydt = string.Empty;
                                                    }

                                                    using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        var objCountryVal = CountryService.GetCountryMasterList();
                                                        if (objCountryVal.ReturnFlag == true)
                                                        {
                                                            var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == objflightRetVal.EntityList[0].CountryID).ToList();
                                                            if (objCountryVal1.Count > 0)
                                                            {
                                                                nation = objCountryVal1[0].CountryCD;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(dataitem["PassengerRequestorID"].Text));
                                            if (objPax.ReturnFlag == true)
                                            {
                                                if (objPax.EntityList.Count > 0)
                                                {
                                                    street = objPax.EntityList[0].Addr1 + objPax.EntityList[0].Addr2 + objPax.EntityList[0].Addr3;
                                                    postal = objPax.EntityList[0].PostalZipCD;
                                                    city = objPax.EntityList[0].City;
                                                    state = objPax.EntityList[0].StateName;
                                                    //billingcode = objPax.EntityList[0].Billing;
                                                    dateofbirth = objPax.EntityList[0].DateOfBirth.ToString();
                                                    billingcode = objPax.EntityList[0].StandardBilling.ToString();
                                                }
                                            }
                                        }

                                        LinkButton lnkPaxCode = (LinkButton)dataitem.FindControl("lnkPaxCode");
                                        PassengerSummary pax = new PassengerSummary();
                                        pax.PaxID = Convert.ToInt64(dataitem["PassengerRequestorID"].Text);
                                        pax.PaxCode = lnkPaxCode.Text;
                                        pax.PaxName = dataitem["Name"].Text;
                                        pax.LegId = Preflegs[i].LegNUM;
                                        pax.Street = street;
                                        pax.Postal = postal;
                                        pax.City = city;
                                        pax.State = state;
                                        pax.Purpose = string.Empty;
                                        pax.Passport = PassportNumber;
                                        pax.BillingCode = billingcode;
                                        pax.VisaID = string.Empty;
                                        pax.PassportID = PaxPassportID;
                                        pax.PassportExpiryDT = expirydt;
                                        pax.Nation = nation;
                                        pax.DateOfBirth = dateofbirth;
                                        pax.OrderNum = (Int32)dataitem.GetDataKeyValue("OrderNUM");
                                        NewpassengerSummary.Add(pax);
                                    }
                                }
                            }
                        }
                    }

                    if (OldpassengerSummary != null)
                        FinalpassengerSummary = NewpassengerSummary.Concat(OldpassengerSummary).ToList();

                    FinalpassengerSummary = FinalpassengerSummary.Where(x => x.LegId != null).OrderBy(x => x.LegId).ToList();

                    Session["CQPaxSummaryList"] = FinalpassengerSummary;
                    dgPaxSummary.DataSource = FinalpassengerSummary;
                    dgPaxSummary.DataBind();

                    if (dgPaxSummary.Items.Count > 0)
                    {
                        dgPaxSummary.SelectedIndexes.Add(0);
                        dgPaxSummary.Items[0].Selected = true;
                        GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                        Label lblName = (Label)item.FindControl("lblName");
                        //Prakash tbPaxName.Text = lblName.Text;
                    }
                }
            }
        }

        /// <summary>
        /// ResetHomebaseSettings
        /// </summary>
        protected void ResetHomebaseSettings()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnPaxNotes.Value = "false";
            }
        }

        /// <summary>
        /// LoadHomebaseSettings
        /// </summary>
        /// <param name="InitialPageReq"></param>
        protected void LoadHomebaseSettings(bool InitialPageReq)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(InitialPageReq))
            {
                ResetHomebaseSettings();
                hdnPaxNotes.Value = UserPrincipal.Identity._fpSettings._IsPNotes.ToString();
            }
        }

        /// <summary>
        /// Method to Get Flight Purpose Details
        /// </summary>
        /// <param name="ddl"></param>
        private void BindFlightPurpose(DropDownList ddl, int LegNum, string Code)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ddl, LegNum, Code))
            {
                ddl.Items.Clear();
                using (FlightPakMasterService.MasterCatalogServiceClient PurposeService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var PurposeVal = PurposeService.GetFlightPurposeList();
                    if (PurposeVal.ReturnFlag == true)
                    {
                        foreach (FlightPakMasterService.FlightPurpose flightpurpose in PurposeVal.EntityList)
                        {
                            ddl.Items.Add(new ListItem(System.Web.HttpUtility.HtmlEncode(flightpurpose.FlightPurposeCD), flightpurpose.FlightPurposeID.ToString()));
                        }

                        if (ddl.ID == "ddlFlightPurpose")
                        {
                            ddl.Items.Insert(0, new ListItem("All Legs", "-1"));
                            ddl.Items.Insert(1, new ListItem("Select", "0"));
                        }
                        else
                            ddl.Items.Insert(0, new ListItem("Select", "0"));
                    }
                }
            }
        }

        protected void FlightPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool isdeadhead = false;
            //  QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
            // Added for Set the Selected Value into the Dropdown list
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ddlFlightPurpose.SelectedIndex >= 0)
                        {
                            RadAjaxManager.GetCurrent(Page).FocusControl(dgAvailablePax);

                            CalculateSeatTotal();
                            List<PassengerInLegs> RetainList = (List<PassengerInLegs>)Session["CQAvailablePaxList"];//RetainPAXInfoGrid(dgAvailablePax);
                            //if (RetainList != null && RetainList.Count > 0)
                            //{
                            int rowCount = 0;
                            foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                            {
                                int legCount = 1;
                                foreach (PaxLegInfo PaxLeg in RetainList[rowCount].Legs)
                                {
                                    if (dataItem.Selected && Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == RetainList[rowCount].PassengerRequestorID)
                                    {
                                        LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");

                                        foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                                        {
                                            if (QuoteInProgress.CQLegs != null)
                                            {
                                                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                                for (int legCnt = 0; legCnt < Preflegs.Count; legCnt++)
                                                {

                                                    isdeadhead = false;

                                                    #region "IsDeadorFerryHead"
                                                    //if (Preflegs[legCnt].FlightCategoryID != null)
                                                    //{
                                                    //    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    //    {
                                                    //        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                    //        var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                    //        if (objRetVal.ReturnFlag == true)
                                                    //        {
                                                    //            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[legCnt].FlightCategoryID)).ToList();
                                                    //            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                    //            {
                                                    //                if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                    //                {
                                                    //                    isdeadhead = true;
                                                    //                }
                                                    //                else
                                                    //                {
                                                    //                    isdeadhead = false;
                                                    //                }
                                                    //            }
                                                    //            else
                                                    //            {
                                                    //                isdeadhead = false;
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}
                                                    //else
                                                    //{
                                                    //    isdeadhead = false;
                                                    //}
                                                    #endregion

                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + Preflegs[legCnt].LegNUM.ToString()].FindControl("ddlLeg" + Preflegs[legCnt].LegNUM.ToString());
                                                    ddlFP.SelectedValue = ddlFlightPurpose.SelectedValue;

                                                    if (legCount == (legCnt + 1))
                                                    {
                                                        //lblPaxCount.Text = (PassengerTotalWithPurpose +1).ToString();//(Preflegs[legCnt].PassengerTotal + 1).ToString();                                                       
                                                        //lblAvailableCount.Text = (maxpassenger - (PassengerTotalWithPurpose +1)).ToString();//((Preflegs[legCnt].SeatTotal - Preflegs[legCnt].PassengerTotal) - 1).ToString();
                                                        if ((legCnt + 1) == 1)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg1", "lblLeg1p1", "lblLeg1a1", "hdnLeg1Pax", "hdnLeg1Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 1);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 2)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg2", "lblLeg2p2", "lblLeg2a2", "hdnLeg2Pax", "hdnLeg2Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 2);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 3)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg3", "lblLeg3p3", "lblLeg3a3", "hdnLeg3Pax", "hdnLeg3Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 3);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 4)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg4", "lblLeg4p4", "lblLeg4a4", "hdnLeg4Pax", "hdnLeg4Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 4);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 5)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg5", "lblLeg5p5", "lblLeg5a5", "hdnLeg5Pax", "hdnLeg5Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 5);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 6)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg6", "lblLeg6p6", "lblLeg6a6", "hdnLeg6Pax", "hdnLeg6Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 6);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 7)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg7", "lblLeg7p7", "lblLeg7a7", "hdnLeg7Pax", "hdnLeg7Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 7);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 8)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg8", "lblLeg8p8", "lblLeg8a8", "hdnLeg8Pax", "hdnLeg8Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 8);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 9)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg9", "lblLeg9p9", "lblLeg9a9", "hdnLeg9Pax", "hdnLeg9Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 9);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 10)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg10", "lblLeg10p10", "lblLeg10a10", "hdnLeg10Pax", "hdnLeg10Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 10);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 11)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg11", "lblLeg11p11", "lblLeg11a11", "hdnLeg11Pax", "hdnLeg11Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 11);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 12)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg12", "lblLeg12p12", "lblLeg12a12", "hdnLeg12Pax", "hdnLeg12Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 12);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 13)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg13", "lblLeg13p13", "lblLeg13a13", "hdnLeg13Pax", "hdnLeg13Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 13);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 14)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg14", "lblLeg14p14", "lblLeg14a14", "hdnLeg14Pax", "hdnLeg14Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 14);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 15)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg15", "lblLeg15p15", "lblLeg15a15", "hdnLeg15Pax", "hdnLeg15Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 15);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 16)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg16", "lblLeg16p16", "lblLeg16a16", "hdnLeg16Pax", "hdnLeg16Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 16);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 17)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg17", "lblLeg17p17", "lblLeg17a17", "hdnLeg17Pax", "hdnLeg17Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 17);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 18)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg18", "lblLeg18p18", "lblLeg18a18", "hdnLeg18Pax", "hdnLeg18Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 18);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 19)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg19", "lblLeg19p19", "lblLeg19a19", "hdnLeg19Pax", "hdnLeg19Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 19);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 20)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg20", "lblLeg20p20", "lblLeg20a20", "hdnLeg20Pax", "hdnLeg20Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 20);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 21)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg21", "lblLeg21p21", "lblLeg21a21", "hdnLeg21Pax", "hdnLeg21Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 21);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 22)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg22", "lblLeg22p22", "lblLeg22a22", "hdnLeg22Pax", "hdnLeg22Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 22);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 23)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg23", "lblLeg23p23", "lblLeg23a23", "hdnLeg23Pax", "hdnLeg23Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 23);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 24)
                                                        {
                                                            if (!isdeadhead)
                                                                UpdateHeaderTemplate(ddlFP, "Leg24", "lblLeg24p24", "lblLeg24a24", "hdnLeg24Pax", "hdnLeg24Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 24);
                                                            else
                                                                ddlFP.SelectedValue = "0";
                                                        }
                                                        if ((legCnt + 1) == 25)
                                                        {
                                                            UpdateHeaderTemplate(ddlFP, "Leg25", "lblLeg25p25", "lblLeg25a25", "hdnLeg25Pax", "hdnLeg25Avail", dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, 25);
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                            //else
                                            //{
                                            //    lblPaxCount.Text = (Convert.ToInt16(lblPaxCount.Text) + 1).ToString();
                                            //    lblAvailableCount.Text = (Convert.ToInt16(lblAvailableCount.Text) - 1).ToString();
                                            //}
                                        }
                                        legCount += 1;
                                    }
                                }
                                rowCount += 1;
                            }
                            //}
                            //ddlFlightPurpose.SelectedValue = "0";
                            dgAvailablePax.Rebind();
                            if (dgPaxSummary.Items.Count > 0)
                            {
                                dgPaxSummary.Items[0].Selected = true;
                                GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                                Label lblLegID = (Label)item.FindControl("lblLegID");
                                //Prakash hdnLegCode.Value = lblLegID.Text;
                                Label lblPassengerRequestorID = (Label)item.FindControl("lblPassengerRequestorID");
                            }

                            // Fix for #8221
                            if (ddlFlightPurpose.Items.Count != null && ddlFlightPurpose.Items.Count >= 0)
                                ddlFlightPurpose.SelectedIndex = 0;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        #region common Methods

        public static DataTable ConvertToDataTable<T>(IList<T> list) where T : class
        {
            int count = 1;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DataTable table = CreateDataTable<T>();
                Type objType = typeof(T);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (T item in list)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor property in properties)
                    {
                        if (!CanUseType(property.PropertyType)) continue;
                        row[property.Name] = property.GetValue(item) ?? DBNull.Value;
                    }
                    row["ID"] = count;
                    table.Rows.Add(row);
                    count = count + 1;
                }

                return table;
            }
        }

        private static DataTable CreateDataTable<T>() where T : class
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Type objType = typeof(T);
                DataTable table = new DataTable(objType.Name);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (PropertyDescriptor property in properties)
                {
                    Type propertyType = property.PropertyType;
                    if (!CanUseType(propertyType)) continue;


                    //nullables must use underlying types
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        propertyType = Nullable.GetUnderlyingType(propertyType);
                    //enums also need special treatment
                    if (propertyType.IsEnum)
                        propertyType = Enum.GetUnderlyingType(propertyType);
                    table.Columns.Add(property.Name, propertyType);
                }
                table.Columns.Add("ID");
                return table;
            }
        }

        private static bool CanUseType(Type propertyType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(propertyType))
            {
                //only strings and value types
                if (propertyType.IsArray) return false;
                if (!propertyType.IsValueType && propertyType != typeof(string)) return false;
                return true;
            }
        }

        /// <summary>
        /// Check the duplicate records in the Datatable
        /// </summary>
        /// <param name="dtDuplicate"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected DataTable DeleteDuplicateFromDataTable(DataTable dtDuplicate, string columnName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable hashT = new Hashtable();
                ArrayList arrDuplicate = new ArrayList();
                foreach (DataRow row in dtDuplicate.Rows)
                {
                    if (hashT.Contains(row[columnName]))
                        arrDuplicate.Add(row);
                    else
                        hashT.Add(row[columnName], string.Empty);
                }
                foreach (DataRow row in arrDuplicate)
                    dtDuplicate.Rows.Remove(row);

                return dtDuplicate;
            }
        }

        #endregion

        #region Pax Available Grid
        private List<GetAllCharterQuoteAvailablePaxList> RetrievePaxList()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                using (CharterQuoteService.CharterQuoteServiceClient objService = new CharterQuoteService.CharterQuoteServiceClient())
                {
                    var objRetVal = objService.GetAllCharterQuoteAvailablePaxList();
                    if (objRetVal.ReturnFlag == true)
                    {
                        return objRetVal.EntityList;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Search Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!string.IsNullOrEmpty(SearchBox.Text))
                        {
                            bool isPaxfound = false;
                            List<FlightPak.Web.FlightPakMasterService.Passenger> PassengerList = new List<FlightPakMasterService.Passenger>();
                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                var objRetVal = objDstsvc.GetPassengerRequestorList();
                                if (objRetVal.ReturnFlag)
                                {
                                    PassengerList = objRetVal.EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(SearchBox.Text.ToUpper().Trim())).ToList();
                                    if (PassengerList != null && PassengerList.Count > 0)
                                    {
                                        isPaxfound = true;
                                        SaveCharterQuoteToSession();
                                        SearchPaxAvailable();
                                        SearchPaxSummary();
                                        SearchBox.Text = string.Empty;
                                        RadAjaxManager.GetCurrent(Page).FocusControl(Submit1);
                                    }
                                }
                            }
                            if (!isPaxfound)
                            {
                                SearchBox.Text = string.Empty;
                                RadWindowManager1.RadAlert("Invalid Passenger Code.", 250, 100, ModuleNameConstants.CharterQuote.CQPax, null);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        private void SearchPaxAvailable()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    bool alreadyExists = false;
                    List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> OldpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                    List<GetAllCharterQuoteAvailablePaxList> _list = null;

                    _list = RetrievePaxList();

                    if (Session["CQSelectedPax"] != null || !string.IsNullOrEmpty(SearchBox.Text))
                    {
                        DataTable dt = (DataTable)Session["CQSelectedPax"];
                        if (Session["CQAvailablePaxList"] != null)
                            OldpassengerInLegs = (List<PassengerInLegs>)Session["CQAvailablePaxList"];

                        if (_list != null)
                        {
                            if (!string.IsNullOrEmpty(SearchBox.Text))
                            {
                                Int32 MaxORderNum = 0;
                                if (OldpassengerInLegs != null)
                                {
                                    alreadyExists = OldpassengerInLegs.Any(x => x.Code.ToUpper().Trim() == SearchBox.Text.ToUpper().Trim());
                                    if (OldpassengerInLegs.Count > 0)
                                    {
                                        MaxORderNum = OldpassengerInLegs.Max(x => x.OrderNUM);
                                    }
                                    MaxORderNum = MaxORderNum + 1;
                                }
                                if (alreadyExists == false)
                                {
                                    GetAllCharterQuoteAvailablePaxList AvailableList = new GetAllCharterQuoteAvailablePaxList();
                                    AvailableList = _list.Where(x => ((x.Code != null) && (x.Code.ToLower().Trim() == SearchBox.Text.ToLower().Trim()))).SingleOrDefault();

                                    if (AvailableList != null)
                                    {
                                        PassengerInLegs paxInfo = new PassengerInLegs();
                                        paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                        paxInfo.Code = AvailableList.Code;
                                        paxInfo.Name = AvailableList.Name;
                                        paxInfo.Passport = AvailableList.Passport;
                                        paxInfo.State = AvailableList.State;
                                        paxInfo.Street = AvailableList.Street;
                                        paxInfo.City = AvailableList.City;
                                        //Defect: 2662
                                        paxInfo.Purpose = string.Empty;
                                        paxInfo.BillingCode = AvailableList.BillingCode;
                                        paxInfo.Notes = AvailableList.Notes;
                                        paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                        paxInfo.PassportID = AvailableList.PassportID.ToString();
                                        paxInfo.VisaID = AvailableList.VisaID.ToString();
                                        paxInfo.Postal = AvailableList.Postal;
                                        //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                paxInfo.PassportExpiryDT = dtDate.ToString();
                                            else
                                                paxInfo.PassportExpiryDT = string.Empty;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.DateOfBirth)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(AvailableList.DateOfBirth);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                paxInfo.DateOfBirth = dtDate.ToString();
                                            else
                                                paxInfo.DateOfBirth = string.Empty;
                                        }
                                        paxInfo.Nation = AvailableList.Nation;
                                        OldpassengerInLegs.Add(paxInfo);
                                        paxInfo.OrderNUM = MaxORderNum;
                                        SearchBox.Text = string.Empty;
                                    }
                                }
                            }
                            if (dt != null)
                            {
                                for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                                {

                                    GetAllCharterQuoteAvailablePaxList AvailableList = new GetAllCharterQuoteAvailablePaxList();
                                    AvailableList = _list.Where(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"])).SingleOrDefault();
                                    if (AvailableList != null)
                                    {
                                        Int32 MaxORderNum = 0;

                                        if (OldpassengerInLegs != null)
                                        {
                                            alreadyExists = OldpassengerInLegs.Any(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]));
                                            if (OldpassengerInLegs.Count > 0)
                                            {
                                                MaxORderNum = OldpassengerInLegs.Max(x => x.OrderNUM);
                                            }
                                            MaxORderNum = MaxORderNum + 1;
                                        }
                                        if (alreadyExists == false && string.IsNullOrEmpty(SearchBox.Text))
                                        {
                                            PassengerInLegs paxInfo = new PassengerInLegs();
                                            paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                            paxInfo.Code = AvailableList.Code;
                                            paxInfo.Name = AvailableList.Name;
                                            paxInfo.Passport = AvailableList.Passport;
                                            paxInfo.State = AvailableList.State;
                                            paxInfo.Street = AvailableList.Street;
                                            paxInfo.City = AvailableList.City;
                                            paxInfo.Purpose = string.Empty;
                                            paxInfo.BillingCode = AvailableList.BillingCode;
                                            paxInfo.Notes = AvailableList.Notes;
                                            paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                            paxInfo.PassportID = AvailableList.PassportID.ToString();
                                            paxInfo.VisaID = AvailableList.VisaID.ToString();
                                            paxInfo.Postal = AvailableList.Postal;
                                            //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    paxInfo.PassportExpiryDT = dtDate.ToString();
                                                else
                                                    paxInfo.PassportExpiryDT = string.Empty;
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.DateOfBirth)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(AvailableList.DateOfBirth);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    paxInfo.DateOfBirth = dtDate.ToString();
                                                else
                                                    paxInfo.DateOfBirth = string.Empty;
                                            }
                                            paxInfo.Nation = AvailableList.Nation;
                                            paxInfo.OrderNUM = MaxORderNum;
                                            OldpassengerInLegs.Add(paxInfo);
                                        }
                                    }
                                }
                            }

                            if (OldpassengerInLegs != null)
                                FinalpassengerInLegs = NewpassengerInLegs.Concat(OldpassengerInLegs).ToList();


                            Session["CQAvailablePaxList"] = FinalpassengerInLegs;
                            dgAvailablePax.DataSource = FinalpassengerInLegs;
                            dgAvailablePax.DataBind();

                            //BindFlightPurpose(FinalpassengerInLegs);
                            BindDefaultPurpose(FinalpassengerInLegs);
                            Session.Remove("CQSelectedPax");
                        }
                    }
                }
            }
        }

        private void SearchPaxAvailableforInsert()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    bool alreadyExists = false;
                    List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> OldpassengerInLegs = new List<PassengerInLegs>();
                    List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                    List<GetAllCharterQuoteAvailablePaxList> _list = null;

                    List<PassengerInLegs> FirstPaxList = new List<PassengerInLegs>();
                    List<PassengerInLegs> RestPaxList = new List<PassengerInLegs>();

                    _list = RetrievePaxList();

                    if (Session["CQSelectedPax"] != null || !string.IsNullOrEmpty(SearchBox.Text))
                    {
                        DataTable dt = (DataTable)Session["CQSelectedPax"];
                        if (Session["CQAvailablePaxList"] != null)
                            OldpassengerInLegs = (List<PassengerInLegs>)Session["CQAvailablePaxList"];

                        if (_list != null)
                        {
                            if (OldpassengerInLegs != null)
                            {

                                Int32 InsertBefore = 1;
                                if (dgAvailablePax.SelectedItems.Count > 0)
                                {
                                    InsertBefore = (Int32)((GridDataItem)dgAvailablePax.SelectedItems[0]).GetDataKeyValue("OrderNUM");
                                }

                                foreach (PassengerInLegs Paxinlegs in OldpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    if (Paxinlegs.OrderNUM < InsertBefore)
                                        FirstPaxList.Add(Paxinlegs);
                                    else
                                        RestPaxList.Add(Paxinlegs);
                                }
                            }

                            if (dt != null)
                            {
                                for (int rowCnt = 0; rowCnt < dt.Rows.Count; rowCnt++)
                                {

                                    GetAllCharterQuoteAvailablePaxList AvailableList = new GetAllCharterQuoteAvailablePaxList();
                                    AvailableList = _list.Where(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"])).SingleOrDefault();
                                    if (AvailableList != null)
                                    {
                                        Int32 MaxORderNum = 0;

                                        if (OldpassengerInLegs != null)
                                        {
                                            alreadyExists = OldpassengerInLegs.Any(x => x.PassengerRequestorID == Convert.ToInt64(dt.Rows[rowCnt]["PassengerRequestorID"]));
                                            if (FirstPaxList.Count > 0)
                                            {
                                                MaxORderNum = FirstPaxList.Max(x => x.OrderNUM);
                                            }
                                            MaxORderNum = MaxORderNum + 1;
                                        }
                                        if (alreadyExists == false && string.IsNullOrEmpty(SearchBox.Text))
                                        {
                                            PassengerInLegs paxInfo = new PassengerInLegs();
                                            paxInfo.PassengerRequestorID = AvailableList.PassengerRequestorID;
                                            paxInfo.Code = AvailableList.Code;
                                            paxInfo.Name = AvailableList.Name;
                                            paxInfo.Passport = AvailableList.Passport;
                                            paxInfo.State = AvailableList.State;
                                            paxInfo.Street = AvailableList.Street;
                                            paxInfo.City = AvailableList.City;
                                            paxInfo.Purpose = string.Empty;
                                            paxInfo.BillingCode = AvailableList.BillingCode;
                                            paxInfo.Notes = AvailableList.Notes;
                                            paxInfo.PassengerAlert = AvailableList.PassengerAlert;
                                            paxInfo.PassportID = AvailableList.PassportID.ToString();
                                            paxInfo.VisaID = AvailableList.VisaID.ToString();
                                            paxInfo.Postal = AvailableList.Postal;
                                            //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    paxInfo.PassportExpiryDT = dtDate.ToString();
                                                else
                                                    paxInfo.PassportExpiryDT = string.Empty;
                                            }
                                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.DateOfBirth)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(AvailableList.DateOfBirth);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    paxInfo.DateOfBirth = dtDate.ToString();
                                                else
                                                    paxInfo.DateOfBirth = string.Empty;
                                            }
                                            paxInfo.Nation = AvailableList.Nation;
                                            paxInfo.OrderNUM = MaxORderNum;
                                            FirstPaxList.Add(paxInfo);
                                        }
                                    }
                                }
                            }

                            if (FirstPaxList != null)
                            {
                                if (RestPaxList != null)
                                {
                                    foreach (PassengerInLegs nextpax in RestPaxList.OrderBy(x => x.OrderNUM).ToList())
                                    {
                                        Int32 MaxORderNum = 0;
                                        if (FirstPaxList.Count > 0)
                                            MaxORderNum = FirstPaxList.Max(x => x.OrderNUM);
                                        MaxORderNum++;
                                        nextpax.OrderNUM = MaxORderNum;
                                        FirstPaxList.Add(nextpax);
                                    }
                                }
                                List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
                                Oldpassengerlist = (List<PassengerSummary>)Session["CQPaxSummaryList"];

                                if (Oldpassengerlist != null)
                                {
                                    foreach (PassengerInLegs pax in FirstPaxList.OrderBy(x => x.OrderNUM).ToList())
                                    {
                                        foreach (PassengerSummary PaxSum in Oldpassengerlist.Where(x => x.PaxID == pax.PassengerRequestorID).ToList())
                                        {
                                            PaxSum.OrderNum = pax.OrderNUM;
                                        }
                                    }
                                }
                                OldpassengerInLegs = FirstPaxList;
                                Session["CQAvailablePaxList"] = Oldpassengerlist;
                            }

                            if (OldpassengerInLegs != null)
                                FinalpassengerInLegs = NewpassengerInLegs.Concat(OldpassengerInLegs).ToList();


                            Session["CQAvailablePaxList"] = FinalpassengerInLegs;
                            dgAvailablePax.DataSource = FinalpassengerInLegs;
                            dgAvailablePax.DataBind();

                            //BindFlightPurpose(FinalpassengerInLegs);
                            BindDefaultPurpose(FinalpassengerInLegs);
                            Session.Remove("CQSelectedPax");
                        }
                    }
                }
            }
        }

        private void BindDefaultPurpose(List<PassengerInLegs> NewpassengerInLegs)
        {
            bool isdeadhead = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewpassengerInLegs))
            {
                //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null)
                    {
                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        for (int newlistcnt = 0; newlistcnt < NewpassengerInLegs.Count; newlistcnt++)
                        {
                            if (NewpassengerInLegs[newlistcnt].Purpose == string.Empty)
                            {
                                for (int i = 1; i <= Preflegs.Count; i++)
                                {
                                    foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                    {
                                        LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");
                                        using (FlightPakMasterService.MasterCatalogServiceClient PassengerService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var PassengerList = PassengerService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID));
                                            if (PassengerList.ReturnFlag == true)
                                            {
                                                if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(NewpassengerInLegs[newlistcnt].PassengerRequestorID))
                                                {
                                                    isdeadhead = false;

                                                    #region "isdeadhead"
                                                    //if (Preflegs[i - 1].FlightCategoryID != null)
                                                    //{
                                                    //    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    //    {
                                                    //        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                    //        var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                    //        if (objRetVal.ReturnFlag == true)
                                                    //        {
                                                    //            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i - 1].FlightCategoryID)).ToList();
                                                    //            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                    //            {
                                                    //                if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                    //                {
                                                    //                    isdeadhead = true;
                                                    //                }
                                                    //                else
                                                    //                {
                                                    //                    isdeadhead = false;
                                                    //                }
                                                    //            }
                                                    //            else
                                                    //            {
                                                    //                isdeadhead = false;
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}
                                                    //else
                                                    //{
                                                    //    isdeadhead = false;
                                                    //}
                                                    //if (Preflegs[i - 1].FlightCategoryID == null)
                                                    //{
                                                    //    isdeadhead = false;
                                                    //}
                                                    #endregion

                                                    DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i - 1].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i - 1].LegNUM).ToString());
                                                    HiddenField hdnFP = (HiddenField)dataItem["Leg" + (Preflegs[i - 1].LegNUM).ToString()].FindControl("hdnOldPurpose_" + (Preflegs[i - 1].LegNUM).ToString());
                                                    if (isdeadhead == false)
                                                    {
                                                        ddlFP.Enabled = true;
                                                        ddlFP.SelectedValue = PassengerList.EntityList[0].FlightPurposeID.ToString();
                                                        hdnFP.Value = PassengerList.EntityList[0].FlightPurposeID.ToString();
                                                        UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                        ////flightpurposeoverride
                                                        if (Session["CQSelectedPax"] != null)
                                                        {
                                                            DataTable dt = (DataTable)Session["CQSelectedPax"];
                                                            if (dt.Rows.Count > 0)
                                                            {
                                                                for (int rowCount = 0; rowCount < dt.Rows.Count; rowCount++)
                                                                {
                                                                    if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(dt.Rows[rowCount]["PassengerRequestorID"].ToString()) && !string.IsNullOrEmpty(dt.Rows[rowCount]["FlightPurposeID"].ToString()))
                                                                    {
                                                                        ddlFP.Enabled = true;
                                                                        ddlFP.SelectedValue = dt.Rows[rowCount]["FlightPurposeID"].ToString();
                                                                        hdnFP.Value = dt.Rows[rowCount]["FlightPurposeID"].ToString();
                                                                        UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ddlFP.SelectedValue = "0";
                                                        hdnFP.Value = "0";
                                                        UpdateHeaderTemplate(ddlFP, ("Leg" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "p" + Preflegs[i - 1].LegNUM), ("lblLeg" + Preflegs[i - 1].LegNUM + "a" + Preflegs[i - 1].LegNUM), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Pax"), ("hdnLeg" + Preflegs[i - 1].LegNUM + "Avail"), dataItem["PassengerRequestorID"].Text, lnkPaxCode.Text, dataItem["Name"].Text, Convert.ToInt16(Preflegs[i - 1].LegNUM));
                                                        ddlFP.Enabled = false;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //private void BindFlightPurpose(List<PassengerInLegs> FinalpassengerInLegs)
        //{
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FinalpassengerInLegs))
        //    {
        //        if (Trip.CQLegs != null)
        //        {
        //            for (int legcnt = 0; legcnt < Trip.CQLegs.Count; legcnt++)
        //            {
        //                if (Trip.CQLegs[legcnt].CQPassengers != null)
        //                {
        //                    for (int paxcnt = 0; paxcnt < Trip.CQLegs[legcnt].CQPassengers.Count; paxcnt++)
        //                    {
        //                        foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
        //                        {
        //                            foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
        //                            {
        //                                if (Convert.ToInt64(Item["PassengerRequestorID"].Text) == Trip.CQLegs[legcnt].CQPassengers[paxcnt].PassengerRequestorID)
        //                                {
        //                                    if (col.UniqueName == "Leg" + Trip.CQLegs[legcnt].LegNUM.ToString())
        //                                    {
        //                                        DropDownList ddlFP = (DropDownList)Item["Leg" + Trip.CQLegs[legcnt].LegNUM.ToString()].FindControl("ddlLeg" + Trip.CQLegs[legcnt].LegNUM.ToString());
        //                                        ddlFP.SelectedValue = Trip.CQLegs[legcnt].CQPassengers[paxcnt].FlightPurposeID.ToString();
        //                                        break;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        //Bind the Drop Down
        //        //if (Session["CQAvailablePaxList"] != null)
        //        //{
        //        //int rowCount = 0;
        //        //foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
        //        //{
        //        //    //int legCount = 1;

        //        //    //  foreach (PassengerInLegs PaxInfo in (List<PassengerInLegs>)Session["CQAvailablePaxList"])
        //        //    foreach (PassengerSummary PaxInfo in (List<PassengerSummary>)Session["CQPaxSummaryList"])
        //        //    {
        //        //        if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == PaxInfo.PaxID)
        //        //        {
        //        //            DropDownList ddlFP = (DropDownList)dataItem["Leg" + PaxInfo.LegId.ToString()].FindControl("ddlLeg" + PaxInfo.LegId.ToString());
        //        //            //  DropDownList ddlFP = (DropDownList)dataItem["Leg" + legCount.ToString()].FindControl("ddlLeg" + legCount.ToString());
        //        //            if (PaxInfo.Purpose != null)
        //        //                ddlFP.SelectedValue = PaxInfo.Purpose;
        //        //            break;
        //        //            //legCount += 1;
        //        //        }
        //        //    }
        //        //    rowCount += 1;
        //        //}
        //        // }

        //        //Session["CQAvailablePaxList"] = FinalpassengerInLegs;
        //    }
        //}

        protected void dgAvailablePax_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CQAvailablePaxList"] != null)
                        {
                            NewpassengerInLegs = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                            SaveCharterQuoteToSession();
                            dgAvailablePax.VirtualItemCount = NewpassengerInLegs.Count;
                            dgAvailablePax.DataSource = NewpassengerInLegs;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void dgAvailablePax_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        bool isdeadhead = false;
                        if (Session[Master.CQSessionKey] != null)
                        {
                            //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                            if (QuoteInProgress.CQLegs != null)
                            {
                                List<CQLeg> CQLegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                for (int icount = 1; icount <= CQLegs.Count; icount++)
                                {
                                    if (CQLegs[icount - 1].IsDeleted == false)
                                    {
                                        (dgAvailablePax.MasterTableView.GetColumn("Leg" + icount) as GridTemplateColumn).Display = true;

                                        if (e.Item is GridDataItem)
                                        {
                                            GridDataItem item = (GridDataItem)e.Item;
                                            DropDownList ddl = new DropDownList();
                                            HiddenField hdn = new HiddenField();

                                            string strCode = item.GetDataKeyValue("PassengerRequestorID").ToString();
                                            ddl.ID = "ddl" + icount;
                                            ddl = (DropDownList)item["leg" + icount].FindControl("ddlLeg" + icount);
                                            BindFlightPurpose(ddl, icount, strCode);
                                        }
                                        if (e.Item is GridHeaderItem)
                                        {
                                            foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                                            {
                                                Label lblpax = (Label)headerItem["Leg" + icount].FindControl("lblLeg" + icount + "p" + icount);// Get the header lable for pax                         
                                                Label lblavailable = (Label)headerItem["Leg" + icount].FindControl("lblLeg" + icount + "a" + icount); // Get the header lable for available 
                                                Label lblLegInfo = (Label)headerItem["Leg" + icount].FindControl("lblLeginfo" + icount);

                                                decimal? PasssengerTotal = 0;
                                                decimal? AvailableSeats = 0;
                                                int? PassengerTotalWithPurpose = CQLegs[icount - 1].CQPassengers == null ? 0 : (CQLegs[icount - 1].CQPassengers.Where(x => x.IsDeleted == false)).ToList().Count;

                                                decimal? blockedSeats = CQLegs[icount - 1].PAXBlockedSeat == null ? 0 : CQLegs[icount - 1].PAXBlockedSeat;
                                                decimal? paxTotal = 0;

                                                PasssengerTotal = blockedSeats + PassengerTotalWithPurpose;
                                                if (maxpassenger != null)
                                                    AvailableSeats = (int) maxpassenger - PasssengerTotal;

                                                if (dgAvailablePax.MasterTableView.DataSourceCount == 0)
                                                    //lblpax.Text = System.Web.HttpUtility.HtmlEncode(CQLegs[icount - 1].PassengerTotal.ToString());
                                                    paxTotal = CQLegs[icount - 1].PassengerTotal;
                                                else
                                                    paxTotal = PasssengerTotal;
                                                    
                                                lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxTotal.ToString());

                                                lblavailable.Text = System.Web.HttpUtility.HtmlEncode(AvailableSeats.ToString());

                                                CQLegs[icount - 1].PassengerTotal = paxTotal; // PasssengerTotal;
                                                CQLegs[icount - 1].ReservationAvailable = AvailableSeats;
                                                CQLegs[icount - 1].PAXBlockedSeat = blockedSeats;

                                                //lblpax.Text = System.Web.HttpUtility.HtmlEncode(CQLegs[icount - 1].PassengerTotal == null ? "0" : CQLegs[icount - 1].PassengerTotal.ToString());
                                                //maxpassenger = CQLegs[icount - 1].ReservationAvailable == null ? 0 : SeatTotal - (Int32)CQLegs[icount - 1].PassengerTotal;
                                                //lblavailable.Text = System.Web.HttpUtility.HtmlEncode(CQLegs[icount - 1].ReservationAvailable == null ? "0" : (SeatTotal - CQLegs[icount - 1].PassengerTotal).ToString());


                                                //lblpax.Text = PassengerTotalWithPurpose.ToString();
                                                //lblavailable.Text = CQLegs[icount - 1].ReservationAvailable == null ? "0" : (SeatTotal - PassengerTotalWithPurpose).ToString();


                                                if (CQLegs[icount - 1].Airport1 != null && CQLegs[icount - 1].Airport != null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(CQLegs[icount - 1].Airport1.IcaoID + "-" + CQLegs[icount - 1].Airport.IcaoID);
                                                }
                                                if (CQLegs[icount - 1].Airport1 == null && CQLegs[icount - 1].Airport != null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty + "-" + CQLegs[icount - 1].Airport.IcaoID);
                                                }
                                                if (CQLegs[icount - 1].Airport1 != null && CQLegs[icount - 1].Airport == null)
                                                {
                                                    lblLegInfo.Text = System.Web.HttpUtility.HtmlEncode(CQLegs[icount - 1].Airport1.IcaoID + "-" + string.Empty);
                                                }
                                            }
                                        }

                                        if (e.Item is GridFooterItem)
                                        {
                                            foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                                            {
                                                TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + icount].FindControl("tbLeg" + icount);// Get the header lable for pax
                                                HiddenField hdnBlockedSeats = (HiddenField)footerItem["Leg" + icount].FindControl("hdnLeg" + icount);// Get the header lable for pax

                                                decimal? blockedSeats = CQLegs[icount - 1].PAXBlockedSeat == null ? 0 : CQLegs[icount - 1].PAXBlockedSeat;
                                                txtBlockedSeats.Text = blockedSeats.ToString();
                                                hdnBlockedSeats.Value = blockedSeats.ToString();

                                                #region "OldCode"
                                                //decimal? PasssengerTotal = CQLegs[icount - 1].PassengerTotal == null ? 0 : CQLegs[icount - 1].PassengerTotal;
                                                //int? PassengerTotalWithPurpose = CQLegs[icount - 1].CQPassengers == null ? 0 : (CQLegs[icount - 1].CQPassengers.Where(x => x.IsDeleted == false)).ToList().Count;
                                                //if ((PasssengerTotal - PassengerTotalWithPurpose) > 0)
                                                //{
                                                //    txtBlockedSeats.Text = (PasssengerTotal - PassengerTotalWithPurpose).ToString();
                                                //    hdnBlockedSeats.Value = (PasssengerTotal - PassengerTotalWithPurpose).ToString();
                                                //}
                                                //else
                                                //{
                                                //    txtBlockedSeats.Text = "0";
                                                //    hdnBlockedSeats.Value = "0";
                                                //}

                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                            if (e.Item is GridDataItem)
                            {
                                GridDataItem dataItem = e.Item as GridDataItem;
                                GridColumn Column = dgAvailablePax.MasterTableView.GetColumn("Notes");
                                string Notes = dataItem["Notes"].Text.Trim();
                                string PassengerAlert = dataItem["PassengerAlert"].Text.Trim();
                                TableCell cellPaxCode = (TableCell)dataItem["PassengerRequestorID"];
                                TableCell cellName = (TableCell)dataItem["Name"];
                                //hdnTSA.Value = cellPaxCode.Text;

                                if (Notes != string.Empty && Notes != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true)
                                {
                                    cellName.ForeColor = System.Drawing.Color.Red;
                                    cellName.ToolTip = "Addl Notes :" + Notes;

                                    if (PassengerAlert != string.Empty && PassengerAlert != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true)
                                    {
                                        cellName.ToolTip = "Addl Notes :" + Notes + Environment.NewLine + "Alerts :" + PassengerAlert;
                                    }
                                }
                                if (PassengerAlert != string.Empty && PassengerAlert != "&nbsp;" && Convert.ToBoolean(hdnPaxNotes.Value) == true && Notes == string.Empty)
                                {
                                    cellName.ForeColor = System.Drawing.Color.Red;
                                    cellName.ToolTip = "Alerts :" + PassengerAlert;
                                }


                                if (QuoteInProgress.CQLegs != null)
                                {
                                    foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                                    {
                                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                        for (int i = 0; i < Preflegs.Count; i++)
                                        {
                                            #region
                                            //if (Preflegs[i].FlightCategoryID != null)
                                            //{
                                            //    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            //    {
                                            //        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                            //        var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                            //        if (objRetVal.ReturnFlag == true)
                                            //        {
                                            //            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i].FlightCategoryID)).ToList();
                                            //            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                            //            {
                                            //                if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                            //                {
                                            //                    isdeadhead = true;
                                            //                }
                                            //                else
                                            //                {
                                            //                    isdeadhead = false;
                                            //                }
                                            //            }
                                            //            else
                                            //            {
                                            //                isdeadhead = false;
                                            //            }
                                            //        }
                                            //    }
                                            //}
                                            //else
                                            //{
                                            //    isdeadhead = false;
                                            //}
                                            #endregion
                                            isdeadhead = false;

                                            if (Preflegs[i].CQPassengers != null)
                                            {
                                                for (int j = 0; j < Preflegs[i].CQPassengers.Count; j++)
                                                {
                                                    if (Convert.ToInt64(cellPaxCode.Text) == Preflegs[i].CQPassengers[j].PassengerRequestorID && col.UniqueName == "Leg" + (Preflegs[i].LegNUM).ToString())
                                                    {
                                                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                                        if (Preflegs[i].CQPassengers[j].CQPassengerID != 0)
                                                        {

                                                            string strFlightPurposeCd = string.Empty;
                                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                            {
                                                                var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[i].CQPassengers[j].FlightPurposeID));
                                                                if (objflightRetVal.ReturnFlag == true)
                                                                {
                                                                    if (objflightRetVal.EntityList.Count > 0)
                                                                    {
                                                                        strFlightPurposeCd = objflightRetVal.EntityList[0].FlightPurposeCD.ToString();
                                                                    }
                                                                }
                                                            }
                                                            if (strFlightPurposeCd != string.Empty && !ddlFP.Items.Contains(new ListItem(System.Web.HttpUtility.HtmlEncode(strFlightPurposeCd), Preflegs[i].CQPassengers[j].FlightPurposeID.ToString())))
                                                            {
                                                                ddlFP.Items.Add(new ListItem(System.Web.HttpUtility.HtmlEncode(strFlightPurposeCd), Preflegs[i].CQPassengers[j].FlightPurposeID.ToString()));
                                                            }
                                                        }
                                                        ddlFP.SelectedValue = Preflegs[i].CQPassengers[j].FlightPurposeID.ToString();

                                                        HiddenField hdnFP = (HiddenField)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("hdnOldPurpose_" + (Preflegs[i].LegNUM).ToString());
                                                        hdnFP.Value = Preflegs[i].CQPassengers[j].FlightPurposeID.ToString();

                                                        if (isdeadhead)
                                                        {
                                                            ddlFP.Enabled = false;
                                                        }
                                                        else
                                                        {
                                                            ddlFP.Enabled = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }


        protected void tbLeg1_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg1", "lblLeg1p1", "lblLeg1a1", "hdnLeg1Pax", "hdnLeg1Avail", 1, "tbLeg1");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg2_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg2", "lblLeg2p2", "lblLeg2a2", "hdnLeg2Pax", "hdnLeg2Avail", 2, "tbLeg2");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg3_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg3", "lblLeg3p3", "lblLeg3a3", "hdnLeg3Pax", "hdnLeg3Avail", 3, "tbLeg3");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg4_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg4", "lblLeg4p4", "lblLeg4a4", "hdnLeg4Pax", "hdnLeg4Avail", 4, "tbLeg4");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg5_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg5", "lblLeg5p5", "lblLeg5a5", "hdnLeg5Pax", "hdnLeg5Avail", 5, "tbLeg5");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg6_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg6", "lblLeg6p6", "lblLeg6a6", "hdnLeg6Pax", "hdnLeg6Avail", 6, "tbLeg6");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg7_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg7", "lblLeg7p7", "lblLeg7a7", "hdnLeg7Pax", "hdnLeg7Avail", 7, "tbLeg7");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg8_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg8", "lblLeg8p8", "lblLeg8a8", "hdnLeg8Pax", "hdnLeg8Avail", 8, "tbLeg8");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg9_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg9", "lblLeg9p9", "lblLeg9a9", "hdnLeg9Pax", "hdnLeg9Avail", 9, "tbLeg9");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg10_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg10", "lblLeg10p10", "lblLeg10a10", "hdnLeg10Pax", "hdnLeg10Avail", 10, "tbLeg10");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg11_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg11", "lblLeg11p11", "lblLeg11a11", "hdnLeg11Pax", "hdnLeg11Avail", 11, "tbLeg11");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg12_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg12", "lblLeg12p12", "lblLeg12a12", "hdnLeg12Pax", "hdnLeg12Avail", 12, "tbLeg12");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg13_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg13", "lblLeg13p13", "lblLeg13a13", "hdnLeg13Pax", "hdnLeg13Avail", 13, "tbLeg13");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg14_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg14", "lblLeg14p14", "lblLeg14a14", "hdnLeg14Pax", "hdnLeg14Avail", 14, "tbLeg14");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg15_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg15", "lblLeg15p15", "lblLeg15a15", "hdnLeg15Pax", "hdnLeg15Avail", 15, "tbLeg15");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg16_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg16", "lblLeg16p16", "lblLeg16a16", "hdnLeg16Pax", "hdnLeg16Avail", 16, "tbLeg16");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg17_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg17", "lblLeg17p17", "lblLeg17a17", "hdnLeg17Pax", "hdnLeg17Avail", 17, "tbLeg17");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg18_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg18", "lblLeg18p18", "lblLeg18a18", "hdnLeg18Pax", "hdnLeg18Avail", 18, "tbLeg18");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg19_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg19", "lblLeg19p19", "lblLeg19a19", "hdnLeg19Pax", "hdnLeg19Avail", 19, "tbLeg19");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg20_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg20", "lblLeg20p20", "lblLeg20a20", "hdnLeg20Pax", "hdnLeg20Avail", 20, "tbLeg20");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg21_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg21", "lblLeg21p21", "lblLeg21a21", "hdnLeg21Pax", "hdnLeg21Avail", 21, "tbLeg21");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg22_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg22", "lblLeg22p22", "lblLeg22a22", "hdnLeg22Pax", "hdnLeg22Avail", 22, "tbLeg22");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg23_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg23", "lblLeg23p23", "lblLeg23a23", "hdnLeg23Pax", "hdnLeg23Avail", 23, "tbLeg23");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg24_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg24", "lblLeg24p24", "lblLeg24a24", "hdnLeg24Pax", "hdnLeg24Avail", 24, "tbLeg24");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void tbLeg25_OnTextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        UpdateHeaderTemplateFromFooter(sender, "Leg25", "lblLeg25p25", "lblLeg25a25", "hdnLeg25Pax", "hdnLeg25Avail", 25, "tbLeg25");
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void UpdateHeaderTemplateFromFooter(Object s, string LegNum, string paxNum, string maxNum, string hdnPax, string hdnAvail, int legnumber, string block)
        {
            TextBox tbBlock = (TextBox)s;
            if (!string.IsNullOrEmpty(tbBlock.Text))
            {
                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                decimal? PasssengerTotal = Preflegs[legnumber - 1].PassengerTotal == null ? 0 : Preflegs[legnumber - 1].PassengerTotal;
                int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CQPassengers == null ? 0 : (Preflegs[legnumber - 1].CQPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;

                foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                {
                    Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 

                    decimal? AvailableSeats = 0;

                    PasssengerTotal = Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose;
                    if (maxpassenger != null)
                        AvailableSeats = (int) maxpassenger - PasssengerTotal;
                    lblpax.Text = System.Web.HttpUtility.HtmlEncode(PasssengerTotal.ToString());

                    lblavailable.Text = System.Web.HttpUtility.HtmlEncode(AvailableSeats.ToString());
                    //maxpassenger

                    Preflegs[legnumber - 1].PassengerTotal = PasssengerTotal;
                    Preflegs[legnumber - 1].ReservationAvailable = AvailableSeats;
                    Preflegs[legnumber - 1].PAXBlockedSeat = Convert.ToInt16(tbBlock.Text);


                    #region Old Calculation


                    //HiddenField hdnLeg1P = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                    //HiddenField hdnLeg1A = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);

                    //hdnLeg1P.Value = (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose).ToString();
                    //hdnLeg1A.Value = (Convert.ToInt16(maxpassenger) - (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose)).ToString();

                    //paxCount = Convert.ToInt32(hdnLeg1P.Value);
                    //ACount = Convert.ToInt32(hdnLeg1A.Value);

                    //lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                    //lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                    //hdnLeg1P.Value = paxCount.ToString();
                    //hdnLeg1A.Value = ACount.ToString();
                    //((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                    //((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();
                    //Preflegs[legnumber - 1].PassengerTotal = paxCount;
                    //Preflegs[legnumber - 1].ReservationAvailable = ACount;
                    #endregion
                }
            }
            else
            {
                tbBlock.Text = "0";
                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                decimal? PasssengerTotal = Preflegs[legnumber - 1].PassengerTotal == null ? 0 : Preflegs[legnumber - 1].PassengerTotal;
                int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CQPassengers == null ? 0 : (Preflegs[legnumber - 1].CQPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;

                foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                {
                    Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 


                    decimal? AvailableSeats = 0;

                    PasssengerTotal = Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose;
                    if (maxpassenger != null)
                        AvailableSeats = (int) maxpassenger - PasssengerTotal;
                    lblpax.Text = System.Web.HttpUtility.HtmlEncode(PasssengerTotal.ToString());

                    lblavailable.Text = System.Web.HttpUtility.HtmlEncode(AvailableSeats.ToString());
                    //maxpassenger

                    Preflegs[legnumber - 1].PassengerTotal = PasssengerTotal;
                    Preflegs[legnumber - 1].ReservationAvailable = AvailableSeats;
                    Preflegs[legnumber - 1].PAXBlockedSeat = Convert.ToInt16(tbBlock.Text);

                    #region Old Calculation

                    //HiddenField hdnLeg1P = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                    //HiddenField hdnLeg1A = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);

                    //hdnLeg1P.Value = (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose).ToString();
                    //hdnLeg1A.Value = (Convert.ToInt16(maxpassenger) - (Convert.ToInt16(tbBlock.Text) + PassengerTotalWithPurpose)).ToString();

                    //paxCount = Convert.ToInt32(hdnLeg1P.Value);
                    //ACount = Convert.ToInt32(hdnLeg1A.Value);

                    //lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                    //lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                    //hdnLeg1P.Value = paxCount.ToString();
                    //hdnLeg1A.Value = ACount.ToString();
                    //((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                    //((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();
                    //Preflegs[legnumber - 1].PassengerTotal = paxCount;
                    //Preflegs[legnumber - 1].ReservationAvailable = ACount;
                    #endregion
                }
            }
        }

        protected void ddlLeg1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg1 = editedItem.FindControl("ddlLeg1") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg1", "lblLeg1p1", "lblLeg1a1", "hdnLeg1Pax", "hdnLeg1Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 1);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void UpdateHeaderTemplate(Object s, string LegNum, string paxNum, string maxNum, string hdnPax, string hdnAvail, string Paxid, string PaxCode, string PaxName, int legnumber)
        {
            bool alreadyExists = false;
            string PassportNumber = string.Empty;
            string PaxPassportID = string.Empty;
            string street = string.Empty;
            string postal = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string expirydt = string.Empty;
            string nation = string.Empty;
            string dateofbirth = string.Empty;
            string billing = string.Empty;

            int Index = 0;
            List<PassengerInLegs> passengerlist = new List<PassengerInLegs>();
            List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
            List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
            List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();
            DropDownList Currentddl = (DropDownList)s;
            CalculateSeatTotal();
            SaveCharterQuoteToSession();
            if (Session["CQAvailablePaxList"] != null)
            {
                passengerlist = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
            }

            if (Session["CQPaxSummaryList"] != null)
            {
                Oldpassengerlist = (List<PassengerSummary>)Session["CQPaxSummaryList"];
            }

            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objflightRetVal = objDstsvc.GetPassengerPassportByPassengerRequestorID(Convert.ToInt64(Paxid));
                if (objflightRetVal.ReturnFlag == true)
                {
                    if (objflightRetVal.EntityList.Count > 0)
                    {
                        PaxPassportID = objflightRetVal.EntityList[0].PassportID.ToString();
                        PassportNumber = objflightRetVal.EntityList[0].PassportNum;
                        //if (!string.IsNullOrEmpty(objflightRetVal.EntityList[0].PassportExpiryDT.ToString()))
                        if (!string.IsNullOrEmpty(Convert.ToString(objflightRetVal.EntityList[0].PassportExpiryDT)))
                        {
                            DateTime dtDate = Convert.ToDateTime(objflightRetVal.EntityList[0].PassportExpiryDT);
                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                expirydt = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                            else
                                expirydt = string.Empty;
                        }

                        using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objCountryVal = CountryService.GetCountryMasterList();
                            if (objCountryVal.ReturnFlag == true)
                            {
                                var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == objflightRetVal.EntityList[0].CountryID).ToList();
                                if (objCountryVal1.Count > 0)
                                {
                                    nation = objCountryVal1[0].CountryCD;
                                }
                            }
                        }
                    }
                }
                var objPax = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Paxid));
                if (objPax.ReturnFlag == true)
                {
                    if (objPax.EntityList.Count > 0)
                    {
                        street = objPax.EntityList[0].Addr1 + objPax.EntityList[0].Addr2 + objPax.EntityList[0].Addr3;
                        postal = objPax.EntityList[0].PostalZipCD;
                        city = objPax.EntityList[0].City;
                        state = objPax.EntityList[0].StateName;
                        billing = objPax.EntityList[0].StandardBilling;
                        if (!string.IsNullOrEmpty(Convert.ToString(objPax.EntityList[0].DateOfBirth)))
                        {
                            DateTime dtDate = Convert.ToDateTime(objPax.EntityList[0].DateOfBirth);
                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                dateofbirth = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                            else
                                dateofbirth = string.Empty;
                        }
                    }
                }
            }


            //if (Currentddl.SelectedIndex != 0)
            //{
            foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
            {
                TextBox tbLeg = (TextBox)footerItem[LegNum].FindControl("tbLeg" + legnumber);
                if (!string.IsNullOrEmpty(tbLeg.Text))
                {
                    footerCnt = Convert.ToInt16(tbLeg.Text);
                }
                else
                {
                    footerCnt = 0;
                }
                break;

            }
            foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
            {

                if (QuoteInProgress.CQLegs != null)
                {
                    #region legs
                    List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    for (int legCount = 0; legCount < Preflegs.Count; legCount++)
                    {
                        int aliaslegCount = legCount;
                        if ((aliaslegCount + 1) == legnumber)
                        {
                            if (Oldpassengerlist != null && Oldpassengerlist.Count > 0)
                            {
                                for (int Cnt = 0; Cnt < Oldpassengerlist.Count; Cnt++)
                                {
                                    if (Oldpassengerlist[Cnt].PaxID == Convert.ToInt64(Paxid) && Oldpassengerlist[Cnt].LegId == Convert.ToInt64(legnumber))
                                    {
                                        Index = Cnt;
                                        alreadyExists = true;
                                        break;
                                    }
                                }
                                // alreadyExists = Oldpassengerlist.Any(x => x.PaxID == Convert.ToInt64(Paxid) && x.LegId == (legCount+1));

                                if (alreadyExists && Currentddl.SelectedIndex == 0)
                                {
                                    Oldpassengerlist.RemoveAt(Index);
                                }
                                if (!alreadyExists && Currentddl.SelectedIndex != 0)
                                {
                                    #region AddPAx

                                    PassengerSummary pax = new PassengerSummary();
                                    Newpassengerlist.Add(pax);
                                    pax.PaxID = Convert.ToInt64(Paxid);
                                    pax.PaxCode = PaxCode;
                                    pax.PaxName = PaxName;
                                    pax.LegId = Preflegs[legCount].LegNUM;
                                    pax.Street = street;
                                    pax.Postal = postal;
                                    pax.City = city;
                                    pax.State = state;
                                    pax.Purpose = string.Empty;
                                    pax.Passport = PassportNumber;
                                    pax.BillingCode = billing;
                                    pax.VisaID = string.Empty;
                                    pax.PassportID = PaxPassportID;
                                    pax.Nation = nation;
                                    pax.PassportExpiryDT = expirydt;
                                    pax.DateOfBirth = dateofbirth;
                                    #endregion
                                }
                                if (alreadyExists && Currentddl.SelectedIndex != 0)
                                {
                                    for (int Cnts = 0; Cnts < Oldpassengerlist.Count; Cnts++)
                                    {
                                        if (Oldpassengerlist[Cnts].PaxID == Convert.ToInt64(Paxid) && Oldpassengerlist[Cnts].LegId == Convert.ToInt64(legnumber))
                                        {
                                            #region Update PAX
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Passport))
                                                Oldpassengerlist[Cnts].Passport = PassportNumber;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].PassportID))
                                                Oldpassengerlist[Cnts].PassportID = PaxPassportID;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Street))
                                                Oldpassengerlist[Cnts].Street = street;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Postal))
                                                Oldpassengerlist[Cnts].Postal = postal;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].City))
                                                Oldpassengerlist[Cnts].City = city;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].State))
                                                Oldpassengerlist[Cnts].State = state;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].Nation))
                                                Oldpassengerlist[Cnts].Nation = nation;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].PassportExpiryDT))
                                                Oldpassengerlist[Cnts].PassportExpiryDT = expirydt;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].DateOfBirth))
                                                Oldpassengerlist[Cnts].DateOfBirth = dateofbirth;
                                            if (string.IsNullOrEmpty(Oldpassengerlist[Cnts].BillingCode))
                                                Oldpassengerlist[Cnts].BillingCode = billing;
                                            break;
                                            #endregion
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Currentddl.SelectedValue != "0" && Oldpassengerlist.Count == 0)
                                {
                                    #region AddPAX
                                    PassengerSummary pax = new PassengerSummary();
                                    Newpassengerlist.Add(pax);
                                    pax.PaxID = Convert.ToInt64(Paxid);
                                    pax.PaxCode = PaxCode;
                                    pax.PaxName = PaxName;
                                    pax.LegId = Preflegs[legCount].LegNUM;
                                    pax.Street = street;
                                    pax.Postal = postal;
                                    pax.City = city;
                                    pax.State = state;
                                    pax.Purpose = string.Empty;
                                    pax.Passport = PassportNumber;
                                    pax.BillingCode = billing;
                                    pax.VisaID = string.Empty;
                                    pax.PassportID = PaxPassportID;
                                    pax.Nation = nation;
                                    pax.PassportExpiryDT = expirydt;
                                    pax.DateOfBirth = dateofbirth;
                                    #endregion
                                }
                            }
                        }
                    }

                    if (Oldpassengerlist != null)
                        Finalpassengerlist = Newpassengerlist.Concat(Oldpassengerlist).ToList();

                    Session["CQPaxSummaryList"] = Finalpassengerlist;
                    dgPaxSummary.DataSource = Finalpassengerlist;
                    dgPaxSummary.DataBind();
                    SaveCharterQuoteToSession();

                    #endregion


                    //currentleg paxttoal PasssengerTotal
                    //PassengerTotalWithPurpose ui legs - paxcount
                    //maxpassenger - fleetmaxpax


                    #region New Calculation

                    #endregion
                    decimal? PasssengerTotal = 0;
                    decimal? AvailableSeats = 0;
                    int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CQPassengers == null ? 0 : (Preflegs[legnumber - 1].CQPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;
                    Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 

                    PasssengerTotal = footerCnt + PassengerTotalWithPurpose;
                    if (maxpassenger != null)
                    AvailableSeats = (int)maxpassenger - PasssengerTotal;
                    lblpax.Text = System.Web.HttpUtility.HtmlEncode(PasssengerTotal.ToString());

                    lblavailable.Text = System.Web.HttpUtility.HtmlEncode(AvailableSeats.ToString());
                    //maxpassenger

                    Preflegs[legnumber - 1].PassengerTotal = PasssengerTotal;
                    Preflegs[legnumber - 1].ReservationAvailable = AvailableSeats;
                    Preflegs[legnumber - 1].PAXBlockedSeat = footerCnt;

                    #region "Old PAX total and reservation available calcualtion"


                    //decimal? PasssengerTotal = Preflegs[legnumber - 1].PassengerTotal == null ? 0 : Preflegs[legnumber - 1].PassengerTotal;
                    //int? PassengerTotalWithPurpose = Preflegs[legnumber - 1].CQPassengers == null ? 0 : (Preflegs[legnumber - 1].CQPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;
                    //Label lblpax = (Label)headerItem[LegNum].FindControl(paxNum);// Get the header lable for pax                         
                    //Label lblavailable = (Label)headerItem[LegNum].FindControl(maxNum); // Get the header lable for available 
                    //HiddenField hdnLeg1P = (HiddenField)headerItem[LegNum].FindControl(hdnPax);
                    //HiddenField hdnLeg1A = (HiddenField)headerItem[LegNum].FindControl(hdnAvail);

                    //hdnLeg1P.Value = "0";

                    //if (!string.IsNullOrEmpty(maxpassenger.ToString()))
                    //{
                    //    hdnLeg1A.Value = maxpassenger.ToString();
                    //}
                    //else
                    //{
                    //    hdnLeg1A.Value = "0";
                    //}

                    //if (footerCnt != 0)
                    //{
                    //    hdnLeg1P.Value = (Convert.ToInt16(hdnLeg1P.Value) + footerCnt + PassengerTotalWithPurpose).ToString();
                    //    hdnLeg1A.Value = (Convert.ToInt16(hdnLeg1A.Value) - (footerCnt + PassengerTotalWithPurpose)).ToString();
                    //}
                    //else
                    //{
                    //    hdnLeg1P.Value = (Convert.ToInt16(hdnLeg1P.Value) + PassengerTotalWithPurpose).ToString();
                    //    hdnLeg1A.Value = (Convert.ToInt16(hdnLeg1A.Value) - PassengerTotalWithPurpose).ToString();
                    //}
                    //paxCount = Convert.ToInt32(hdnLeg1P.Value);
                    //ACount = Convert.ToInt32(hdnLeg1A.Value);
                    //int Total = Convert.ToInt16(PasssengerTotal - PassengerTotalWithPurpose);
                    ////if (passengerlist.Count > Total)
                    ////{
                    ////paxCount++;

                    ////ACount = Convert.ToInt32(hdnLeg1A.Value);
                    ////ACount--;

                    //lblpax.Text = System.Web.HttpUtility.HtmlEncode(paxCount.ToString());
                    //lblavailable.Text = System.Web.HttpUtility.HtmlEncode(ACount.ToString());
                    //hdnLeg1P.Value = paxCount.ToString();
                    //hdnLeg1A.Value = ACount.ToString();
                    //((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "p" + legnumber)).Value = paxCount.ToString();
                    //((HiddenField)DivExternalForm.FindControl("hdnLeg" + legnumber + "a" + legnumber)).Value = ACount.ToString();
                    //Preflegs[legnumber - 1].PassengerTotal = paxCount;
                    //Preflegs[legnumber - 1].ReservationAvailable = ACount;


                    #endregion
                }
            }
            if (dgPaxSummary.Items.Count > 0)
            {
                //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                dgPaxSummary.Items[0].Selected = true;
                GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                Label lblLegID = (Label)item.FindControl("lblLegID");

                Label lblPassengerRequestorID = (Label)item.FindControl("lblPassengerRequestorID");
                Label lblName = (Label)item.FindControl("lblName");
            }
        }


        protected void ddlLeg2_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg2 = editedItem.FindControl("ddlLeg2") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg2", "lblLeg2p2", "lblLeg2a2", "hdnLeg2Pax", "hdnLeg2Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 2);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg3_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg3 = editedItem.FindControl("ddlLeg3") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg3", "lblLeg3p3", "lblLeg3a3", "hdnLeg3Pax", "hdnLeg3Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 3);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg4_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg4 = editedItem.FindControl("ddlLeg4") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg4", "lblLeg4p4", "lblLeg4a4", "hdnLeg4Pax", "hdnLeg4Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 4);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg5_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg5 = editedItem.FindControl("ddlLeg5") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg5", "lblLeg5p5", "lblLeg5a5", "hdnLeg5Pax", "hdnLeg5Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 5);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg6_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg6 = editedItem.FindControl("ddlLeg6") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg6", "lblLeg6p6", "lblLeg6a6", "hdnLeg6Pax", "hdnLeg6Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 6);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg7_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg7 = editedItem.FindControl("ddlLeg7") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg7", "lblLeg7p7", "lblLeg7a7", "hdnLeg7Pax", "hdnLeg7Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 7);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg8_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg8 = editedItem.FindControl("ddlLeg8") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg8", "lblLeg8p8", "lblLeg8a8", "hdnLeg8Pax", "hdnLeg8Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 8);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg9_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg9 = editedItem.FindControl("ddlLeg9") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg9", "lblLeg9p9", "lblLeg9a9", "hdnLeg9Pax", "hdnLeg9Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 9);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg10_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg10 = editedItem.FindControl("ddlLeg10") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg10", "lblLeg10p10", "lblLeg10a10", "hdnLeg10Pax", "hdnLeg10Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 10);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }

        }
        protected void ddlLeg11_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg11 = editedItem.FindControl("ddlLeg11") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg11", "lblLeg11p11", "lblLeg11a11", "hdnLeg11Pax", "hdnLeg11Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 11);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg12_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg12 = editedItem.FindControl("ddlLeg12") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg12", "lblLeg12p12", "lblLeg12a12", "hdnLeg12Pax", "hdnLeg12Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 12);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void ddlLeg13_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg13 = editedItem.FindControl("ddlLeg13") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg13", "lblLeg13p13", "lblLeg13a13", "hdnLeg13Pax", "hdnLeg13Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 13);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg14_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg14 = editedItem.FindControl("ddlLeg14") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg14", "lblLeg14p14", "lblLeg14a14", "hdnLeg14Pax", "hdnLeg14Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 14);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg15_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg15 = editedItem.FindControl("ddlLeg15") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg15", "lblLeg15p15", "lblLeg15a15", "hdnLeg15Pax", "hdnLeg15Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 15);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg16_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg16 = editedItem.FindControl("ddlLeg16") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg16", "lblLeg16p16", "lblLeg16a16", "hdnLeg16Pax", "hdnLeg16Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 16);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg17_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg17 = editedItem.FindControl("ddlLeg17") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg17", "lblLeg17p17", "lblLeg17a17", "hdnLeg17Pax", "hdnLeg17Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 17);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg18_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg18 = editedItem.FindControl("ddlLeg18") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg18", "lblLeg18p18", "lblLeg18a18", "hdnLeg18Pax", "hdnLeg18Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 18);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg19_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg19 = editedItem.FindControl("ddlLeg19") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg19", "lblLeg19p19", "lblLeg19a19", "hdnLeg19Pax", "hdnLeg19Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 19);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg20_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg20 = editedItem.FindControl("ddlLeg20") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg20", "lblLeg20p20", "lblLeg20a20", "hdnLeg20Pax", "hdnLeg20Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 20);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg21_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg21 = editedItem.FindControl("ddlLeg21") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg21", "lblLeg21p21", "lblLeg21a21", "hdnLeg21Pax", "hdnLeg21Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 21);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg22_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg22 = editedItem.FindControl("ddlLeg22") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg22", "lblLeg22p22", "lblLeg22a22", "hdnLeg22Pax", "hdnLeg22Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 22);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg23_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg23 = editedItem.FindControl("ddlLeg23") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg23", "lblLeg23p23", "lblLeg23a23", "hdnLeg23Pax", "hdnLeg23Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 23);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg24_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg24 = editedItem.FindControl("ddlLeg24") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg24", "lblLeg24p24", "lblLeg24a24", "hdnLeg24Pax", "hdnLeg24Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 24);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void ddlLeg25_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        GridEditableItem editedItem = (sender as DropDownList).NamingContainer as GridEditableItem;
                        DropDownList ddlLeg25 = editedItem.FindControl("ddlLeg25") as DropDownList;
                        LinkButton lnkPaxCode = editedItem.FindControl("lnkPaxCode") as LinkButton;

                        UpdateHeaderTemplate(sender, "Leg25", "lblLeg25p25", "lblLeg25a25", "hdnLeg25Pax", "hdnLeg25Avail", editedItem["PassengerRequestorID"].Text, lnkPaxCode.Text, editedItem["Name"].Text, 25);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        private int RetrieveAvailableCount()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<PassengerInLegs> passengerInLegsList = new List<PassengerInLegs>();
                int AvailableCnt = 0;

                if (Session["CQAvailablePaxList"] != null)
                {
                    passengerInLegsList = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                    for (int Cnt = 0; Cnt < passengerInLegsList.Count; Cnt++)
                    {
                        if (!string.IsNullOrEmpty(passengerInLegsList[Cnt].Purpose))
                        {
                            AvailableCnt = passengerInLegsList.Count;
                        }
                    }
                }
                return AvailableCnt;
            }
        }
        protected void dgAvailablePax_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveCharterQuoteToSession();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void dgAvailablePax_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.CommandName == "RowClick")
                        {
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        #endregion

        /// <summary>
        /// Rebind the popup values in the corresponding grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.Argument)
                        {
                            case "Rebind":
                                dgAvailablePax.MasterTableView.SortExpressions.Clear();
                                dgAvailablePax.MasterTableView.GroupByExpressions.Clear();
                                dgAvailablePax.MasterTableView.CurrentPageIndex = dgAvailablePax.MasterTableView.PageCount - 1;
                                dgAvailablePax.Rebind();
                                break;
                            case "RebindAndNavigate":
                                SaveCharterQuoteToSession();
                                SearchPaxAvailable();
                                //SearchPaxSummary();
                                if (Session["PassSelectedPax"] != null && Session["PaxPassSelectedLeg"] != null)
                                {
                                    dgPaxSummary.Rebind();
                                }
                                break;
                            case "RebindAndNavigateInsert":
                                SaveCharterQuoteToSession();
                                SearchPaxAvailableforInsert();
                                //SearchPaxAvailableforInsert();
                                break;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }

        /// <summary>
        /// Define the method of IPostBackEventHandler that raises change events.
        /// </summary>
        /// <param name="sourceControl"></param>
        /// <param name="eventArgument"></param>
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sourceControl, eventArgument))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.RaisePostBackEvent(sourceControl, eventArgument);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        /// <summary>
        /// LoadPaxfromTrip
        /// </summary>
        /// <param name="dtNewAssign"></param>
        private void LoadPaxfromTrip()
        {
            bool alreadyExists = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                List<PassengerInLegs> NewpassengerInLegs = new List<PassengerInLegs>();
                List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
                if (Session[Master.CQSessionKey] != null)
                {
                    //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    if (QuoteInProgress.CQLegs != null)
                    {
                        Session.Remove("CQAvailablePaxList");
                    }
                    if (Session["CQAvailablePaxList"] == null && QuoteInProgress.CQLegs != null)
                    {
                        // Trip.CQLegs = Trip.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        for (int LegCnt = 0; LegCnt < Preflegs.Count; LegCnt++)
                        {
                            if (Preflegs[LegCnt].IsDeleted == false)
                            {
                                if (Preflegs[LegCnt].CQPassengers != null)
                                {
                                    for (int PaxCnt = 0; PaxCnt < Preflegs[LegCnt].CQPassengers.Count; PaxCnt++)
                                    {
                                        if (Preflegs[LegCnt].CQPassengers[PaxCnt].IsDeleted == false)
                                        {
                                            alreadyExists = NewpassengerInLegs.Any(X => X.PassengerRequestorID == Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerRequestorID);

                                            if (alreadyExists == false)
                                            {
                                                PassengerInLegs paxInfo = new PassengerInLegs();
                                                paxInfo.Legs = new List<PaxLegInfo>();

                                                NewpassengerInLegs.Add(paxInfo);
                                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                                PaxLeg.LegID = Convert.ToInt64(Preflegs[LegCnt].CQLegID);

                                                paxInfo.PassengerRequestorID = Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerRequestorID);

                                                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerRequestorID));

                                                    if (objPaxRetVal.ReturnFlag == true)
                                                    {
                                                        paxInfo.Code = objPaxRetVal.EntityList[0].PassengerRequestorCD;
                                                        paxInfo.Notes = objPaxRetVal.EntityList[0].Notes;
                                                        paxInfo.PassengerAlert = objPaxRetVal.EntityList[0].PassengerAlert;
                                                    }

                                                    paxInfo.Name = Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerName;
                                                    paxInfo.OrderNUM = (Int32)Preflegs[LegCnt].CQPassengers[PaxCnt].OrderNUM;
                                                    using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                                                    {

                                                        //var objPassRetVal = Service.GetPassengerPassportByPassportID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassportID));
                                                        //if (objPassRetVal.ReturnFlag == true)
                                                        //{
                                                        //    if (objPassRetVal.EntityList.Count > 0)
                                                        //    {
                                                        //        paxInfo.PassportID = objPassRetVal.EntityList[0].PassportID.ToString();
                                                        //        paxInfo.Passport = objPassRetVal.EntityList[0].PassportNum;
                                                        //    }
                                                        //}
                                                    }
                                                    //if (Preflegs[LegCnt].CQPassengers[PaxCnt].VisaID != null)
                                                    //    paxInfo.VisaID = Preflegs[LegCnt].CQPassengers[PaxCnt].VisaID.ToString();
                                                    //if (Preflegs[LegCnt].CQPassengers[PaxCnt].StateName != null)
                                                    //    paxInfo.State = Preflegs[LegCnt].CQPassengers[PaxCnt].StateName.ToString();
                                                    //if (Preflegs[LegCnt].CQPassengers[PaxCnt].Street != null)
                                                    //    paxInfo.Street = Preflegs[LegCnt].CQPassengers[PaxCnt].Street;
                                                    //if (Preflegs[LegCnt].CQPassengers[PaxCnt].TripStatus != null)
                                                    //    paxInfo.Status = Preflegs[LegCnt].CQPassengers[PaxCnt].TripStatus;
                                                    //if (Preflegs[LegCnt].CQPassengers[PaxCnt].CityName != null)
                                                    //    paxInfo.City = Preflegs[LegCnt].CQPassengers[PaxCnt].CityName;

                                                    var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].FlightPurposeID));
                                                    // GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeID.Equals(Preflegs[LegCnt].CQPassengers[PaxCnt].FlightPurposeID));
                                                    //if (objflightRetVal.Count() > 0 && objflightRetVal != null)
                                                    if (objflightRetVal.ReturnFlag == true)
                                                    {
                                                        //foreach (FlightPakMasterService.FlightPurpose flight in objflightRetVal)
                                                        //{
                                                        //    if (flight.FlightPurposeID == Preflegs[LegCnt].CQPassengers[PaxCnt].FlightPurposeID)
                                                        //    {
                                                        paxInfo.Purpose = objflightRetVal.EntityList[0].FlightPurposeID.ToString();
                                                        //flight.FlightPurposeID.ToString();
                                                        //    }
                                                        //}
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    for (int PaxCnt = 0; PaxCnt < Preflegs[LegCnt].CQPassengers.Count; PaxCnt++)
                                    {
                                        if (Preflegs[LegCnt].CQPassengers[PaxCnt].IsDeleted == false)
                                        {

                                            PassengerSummary paxSum = new PassengerSummary();
                                            Newpassengerlist.Add(paxSum);

                                            paxSum.PaxID = Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerRequestorID);
                                            paxSum.OrderNum = (Int32)Preflegs[LegCnt].CQPassengers[PaxCnt].OrderNUM;
                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objPaxRetVal = objDstsvc.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerRequestorID));

                                                if (objPaxRetVal.ReturnFlag == true)
                                                {
                                                    paxSum.PaxCode = objPaxRetVal.EntityList[0].PassengerRequestorCD;
                                                }
                                                paxSum.PaxName = Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerName;
                                                paxSum.LegId = Preflegs[LegCnt].LegNUM;
                                                //if (Preflegs[LegCnt].CQPassengers[PaxCnt].PassportID != null)
                                                //    paxSum.PassportID = Preflegs[LegCnt].CQPassengers[PaxCnt].PassportID.ToString();

                                                if (Preflegs[LegCnt].CQPassengers[PaxCnt].Billing != null)
                                                    paxSum.BillingCode = Preflegs[LegCnt].CQPassengers[PaxCnt].Billing.ToString();

                                                //if (Preflegs[LegCnt].CQPassengers[PaxCnt].DateOfBirth != null)
                                                //    paxSum.DateOfBirth = Preflegs[LegCnt].CQPassengers[PaxCnt].DateOfBirth.ToString();

                                                using (PreflightService.PreflightServiceClient Service = new PreflightService.PreflightServiceClient())
                                                {
                                                    //Prakash
                                                    var objPassRetVal = Service.GetPassengerPassportByPassportID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassportID));
                                                    if (objPassRetVal.ReturnFlag == true)
                                                    {
                                                        if (objPassRetVal.EntityList.Count > 0)
                                                        {
                                                            paxSum.PassportID = objPassRetVal.EntityList[0].PassportID.ToString();
                                                            paxSum.Passport = objPassRetVal.EntityList[0].PassportNum;
                                                            //if (!string.IsNullOrEmpty(objPassRetVal.EntityList[0].PassportExpiryDT.ToString()))
                                                            if (!string.IsNullOrEmpty(Convert.ToString(objPassRetVal.EntityList[0].PassportExpiryDT)))
                                                            {
                                                                DateTime dtDate = Convert.ToDateTime(objPassRetVal.EntityList[0].PassportExpiryDT);
                                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                                    paxSum.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                                else
                                                                    paxSum.PassportExpiryDT = string.Empty;
                                                            }

                                                            using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                            {
                                                                var objCountryVal = CountryService.GetCountryMasterList();
                                                                if (objCountryVal.ReturnFlag == true)
                                                                {
                                                                    var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == objPassRetVal.EntityList[0].CountryID).ToList();
                                                                    if (objCountryVal1.Count > 0)
                                                                    {
                                                                        paxSum.Nation = objCountryVal1[0].CountryCD;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }//Prakash
                                                }

                                                var objflightRetVal = objDstsvc.GetFlightPurposebyFlightPurposeID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].FlightPurposeID));
                                                //.GetFlightPurposeList().EntityList.Where(x => x.FlightPurposeID.Equals(Preflegs[LegCnt].CQPassengers[PaxCnt].FlightPurposeID));
                                                if (objflightRetVal.ReturnFlag == true)
                                                {
                                                    paxSum.Purpose = objflightRetVal.EntityList[0].FlightPurposeID.ToString();
                                                    //flight.FlightPurposeID.ToString();
                                                    //flight.FlightPurposeDescription;
                                                }
                                            }

                                            using (FlightPakMasterService.MasterCatalogServiceClient PaxService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                var objPassRetVal = PaxService.GetPassengerbyPassengerRequestorID(Convert.ToInt64(Preflegs[LegCnt].CQPassengers[PaxCnt].PassengerRequestorID));
                                                if (objPassRetVal.ReturnFlag == true)
                                                {
                                                    paxSum.Street = ((objPassRetVal.EntityList[0].Addr1 == null ? string.Empty : objPassRetVal.EntityList[0].Addr1) + " " + (objPassRetVal.EntityList[0].Addr2 == null ? string.Empty : objPassRetVal.EntityList[0].Addr2)).Trim();
                                                    if (objPassRetVal.EntityList[0].PostalZipCD != null)
                                                        paxSum.Postal = objPassRetVal.EntityList[0].PostalZipCD.ToString();
                                                    if (objPassRetVal.EntityList[0].City != null)
                                                        paxSum.City = objPassRetVal.EntityList[0].City.ToString();
                                                    if (objPassRetVal.EntityList[0].StateName != null)
                                                        paxSum.State = objPassRetVal.EntityList[0].StateName.ToString();
                                                    if (objPassRetVal.EntityList[0].StandardBilling != null)
                                                        paxSum.BillingCode = objPassRetVal.EntityList[0].StandardBilling.ToString();
                                                    if (!string.IsNullOrEmpty(Convert.ToString(objPassRetVal.EntityList[0].DateOfBirth)))
                                                    {
                                                        DateTime dtDate = Convert.ToDateTime(objPassRetVal.EntityList[0].DateOfBirth);
                                                        if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                            paxSum.DateOfBirth = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                        else
                                                            paxSum.DateOfBirth = string.Empty;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }

                                //foreach (GridHeaderItem headerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Header))
                                //{
                                //    Label lblPaxCount = (Label)headerItem["Leg" + (LegCnt + 1).ToString()].FindControl("lblLeg" + (LegCnt + 1).ToString() + "p" + (LegCnt + 1).ToString());
                                //    Label lblAvailableCount = (Label)headerItem["Leg" + (LegCnt + 1).ToString()].FindControl("lblLeg" + (LegCnt + 1).ToString() + "a" + (LegCnt + 1).ToString());
                                //    lblPaxCount.Text = Preflegs[LegCnt].PassengerTotal.ToString();
                                //    lblAvailableCount.Text = (Preflegs[LegCnt].SeatTotal - Preflegs[LegCnt].PassengerTotal).ToString();
                                //}
                                //foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                                //{
                                //    TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + (LegCnt + 1)].FindControl("tbLeg" + (LegCnt + 1));// Get the header lable for pax  
                                //    int? PassengerTotalWithPurpose = Preflegs[LegCnt].CQPassengers == null ? 0 : (Preflegs[LegCnt].CQPassengers.Where(x => x.IsDeleted == false && x.FlightPurposeID != 0)).ToList().Count;
                                //    txtBlockedSeats.Text = (Preflegs[LegCnt].PassengerTotal - PassengerTotalWithPurpose).ToString();
                                //}
                            }
                        }

                        Newpassengerlist = Newpassengerlist.Where(x => x.LegId != null).OrderBy(x => x.LegId).ToList();
                        Int32 MaxOrderNUM = 0;
                        foreach (PassengerInLegs PaxinLegs in NewpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                        {
                            MaxOrderNUM++;
                            PaxinLegs.OrderNUM = MaxOrderNUM;

                            foreach (PassengerSummary PaxSum in Newpassengerlist.Where(x => x.PaxID == PaxinLegs.PassengerRequestorID).ToList())
                            {
                                PaxSum.OrderNum = PaxinLegs.OrderNUM;
                            }
                        }
                        Session["CQAvailablePaxList"] = NewpassengerInLegs;
                        dgAvailablePax.DataSource = NewpassengerInLegs;
                        dgAvailablePax.DataBind();
                        Session["CQPaxSummaryList"] = Newpassengerlist;
                        dgPaxSummary.DataSource = Newpassengerlist;
                        dgPaxSummary.DataBind();

                        Session["CQAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        //foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                        //{
                        //    foreach (PassengerSummary PaxInfo in (List<PassengerSummary>)Session["CQPaxSummaryList"])
                        //    {
                        //        foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                        //        {
                        //            if (Convert.ToInt64(Item["PassengerRequestorID"].Text) == PaxInfo.PaxID)
                        //            {
                        //                if (col.UniqueName == "Leg" + PaxInfo.LegId.ToString())
                        //                {
                        //                    DropDownList ddlFP = (DropDownList)Item["Leg" + PaxInfo.LegId.ToString()].FindControl("ddlLeg" + PaxInfo.LegId.ToString());
                        //                    ddlFP.SelectedValue = PaxInfo.Purpose;                                 
                        //                    break;
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                        Session["CQPaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                        if (dgPaxSummary.Items.Count > 0)
                            dgPaxSummary.Items[0].Selected = true;
                    }
                    else
                    {
                        List<PassengerInLegs> paxInfoList = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                        dgAvailablePax.DataSource = paxInfoList;
                        dgAvailablePax.DataBind();
                        FillFlightPurpose(paxInfoList);
                    }
                }
            }
        }

        private void FillFlightPurpose(List<PassengerInLegs> paxInfoList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                int itemCount = 0;
                foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                {
                    int counter = 1;
                    for (int leg = 0; leg < paxInfoList[itemCount].Legs.Count; leg++)
                    {
                        foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                        {
                            if (col.UniqueName == "Leg" + counter.ToString())
                            {
                                // Label lblLeg1p1 = (Label) 
                                DropDownList ddlFlightPurpose = (DropDownList)Item["Leg" + counter.ToString()].FindControl("ddlLeg" + counter.ToString());
                                ddlFlightPurpose.SelectedValue = paxInfoList[itemCount].Legs[leg].FlightPurposeID.ToString();
                                break;
                            }
                        }
                        counter += 1;
                    }
                    itemCount += 1;
                }
            }
        }

        protected void btnPaxRemoveYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    if (QuoteInProgress.Mode == CQRequestActionMode.Edit)
                    {
                        Session["CQAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                        Session["CQPaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);
                        List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                        List<PassengerSummary> passengerSummaryList = RetainPaxSummaryGrid(dgPaxSummary);
                        SavePassengers(passengerList, passengerSummaryList);
                        SaveQuoteinProgressToFileSession();

                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        Master.Save(FileRequest);
                    }
                }
            }
        }

        protected void btnPaxRemoveNo_Click(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).FocusControl(btnSave);
        }

        protected void btnPaxNextYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                SaveCharterQuoteToSession();
                Master.Next("PAX");
            }
        }

        protected void btnPaxNextNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                RadAjaxManager.GetCurrent(Page).FocusControl(btnSave);
            }
        }

        private string PaxPurposeError()
        {
            string desc = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Session["CQAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);
                List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                List<PassengerInLegs> nolegsPAXlist = new List<PassengerInLegs>();


                bool nopurposeassigned = false;
                foreach (PassengerInLegs paxlegs in passengerList)
                {
                    List<PaxLegInfo> legsPAXlist = new List<PaxLegInfo>();

                    legsPAXlist = (from paxleg in paxlegs.Legs
                                   where paxleg.FlightPurposeID != 0
                                   select paxleg).ToList();
                    if (legsPAXlist == null || legsPAXlist.Count == 0)
                        nopurposeassigned = true;
                }

                if (nopurposeassigned)
                    desc = "Warning - Passenger Not Assigned To Any Legs And Will Not Be Saved. Continue Without Saving Your Entry";



                //if (passengerList != null)
                //{
                //    for (int i = 0; i < passengerList.Count; i++)
                //    {
                //        if (passengerList[i].Legs != null)
                //        {
                //            for (int j = 0; j < passengerList[i].Legs.Count; j++)
                //            {
                //                if (passengerList[i].Legs[j].FlightPurposeID == 0)
                //                {
                //                    desc += passengerList[i].Name + " is not assigned to Leg " + passengerList[i].Legs[j].LegOrder + " and will not be saved  <br />";
                //                }
                //            }
                //        }
                //    }
                //}

            }
            return desc;
        }

        protected void SavePreflightToSessionOnTabChange()
        {
            string PaxpurposeErrorstr = string.Empty;
            PaxpurposeErrorstr = PaxPurposeError();

            if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                Session["CQPAXnotAssigned"] = PaxpurposeErrorstr;
            else
            {
                Session.Remove("CQPAXnotAssigned");
                SaveCharterQuoteToSession();
            }
        }

        protected void SaveQuoteinProgressToFileSession()
        {
            if (Session[Master.CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[Master.CQSessionKey];

                CQMain QuoteToUpdate = new CQMain();

                if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                {
                    QuoteToUpdate = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                    QuoteToUpdate = QuoteInProgress;
                }
                Session[Master.CQSessionKey] = FileRequest;

            }
        }

        /// <summary>
        /// Method to Save Preflight Passenger form fields saved into session
        /// </summary>
        protected void SaveCharterQuoteToSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    if (QuoteInProgress.Mode == CQRequestActionMode.Edit)
                    {

                        Session["CQAvailablePaxList"] = RetainPAXInfoGrid(dgAvailablePax);

                        Session["CQPaxSummaryList"] = RetainPaxSummaryGrid(dgPaxSummary);

                        List<PassengerInLegs> passengerList = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                        List<PassengerSummary> passengerSummaryList = RetainPaxSummaryGrid(dgPaxSummary);

                        SavePassengers(passengerList, passengerSummaryList);

                        SaveQuoteinProgressToFileSession();
                        Master.CalculateQuoteTotal();

                    }
                }
            }
        }

        /// <summary>
        /// Method to Retain PAX Info Grid Items into List
        /// </summary>
        /// <param name="Grid"></param>
        /// <returns>Returns PAX Info List</returns>
        private List<PassengerInLegs> RetainPAXInfoGrid(RadGrid grid)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(grid))
            {
                List<PassengerInLegs> paxInfoList = new List<PassengerInLegs>();

                // if (Session[Master.CQSessionKey] != null)
                //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];

                foreach (GridDataItem Item in dgAvailablePax.MasterTableView.Items)
                {
                    PassengerInLegs PaxInfo = new PassengerInLegs();
                    LinkButton lbtnCode = (LinkButton)Item["Code"].FindControl("lnkPaxCode");
                    PaxInfo.Code = lbtnCode.Text;
                    PaxInfo.PassengerRequestorID = Convert.ToInt64(Item["PassengerRequestorID"].Text);
                    PaxInfo.Name = Item["Name"].Text;
                    PaxInfo.OrderNUM = (Int32)Item.GetDataKeyValue("OrderNUM");
                    if (QuoteInProgress != null)
                    {
                        if (QuoteInProgress.CQLegs.Count > 0)
                        {
                            List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            PaxInfo.Legs = new List<PaxLegInfo>();
                            int counter = 1;
                            for (int leg = 0; leg < Preflegs.Count; leg++)
                            {
                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                PaxLeg.LegID = Convert.ToInt64(Preflegs[leg].CQLegID);
                                if (Preflegs[leg].Airport != null)
                                {
                                    if (Preflegs[leg].Airport.IcaoID != null)
                                        PaxLeg.Depart = Preflegs[leg].Airport.IcaoID;
                                }
                                if (Preflegs[leg].Airport1 != null)
                                {
                                    if (Preflegs[leg].Airport1.IcaoID != null)
                                        PaxLeg.Arrive = Preflegs[leg].Airport1.IcaoID;
                                }

                                foreach (GridColumn col in dgAvailablePax.MasterTableView.Columns)
                                {
                                    if (col.UniqueName == "Leg" + counter.ToString())
                                    {
                                        DropDownList ddlFlightPurpose = (DropDownList)Item["Leg" + counter.ToString()].FindControl("ddlLeg" + counter.ToString());
                                        if (ddlFlightPurpose.SelectedItem != null)
                                            PaxLeg.FlightPurposeCD = ddlFlightPurpose.SelectedItem.ToString();
                                        if (!string.IsNullOrEmpty(ddlFlightPurpose.SelectedValue))
                                            PaxLeg.FlightPurposeID = Convert.ToInt64(ddlFlightPurpose.SelectedValue);
                                        PaxInfo.Purpose = ddlFlightPurpose.SelectedValue;

                                        break;
                                    }
                                }
                                PaxLeg.LegOrder = counter;
                                counter += 1;
                                PaxInfo.Legs.Add(PaxLeg);
                            }
                        }
                    }
                    paxInfoList.Add(PaxInfo);
                }
                return paxInfoList;
            }
        }

        /// <summary>
        /// page prerender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ApplyPermissions();
                        if (Session[Master.CQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[Master.CQSessionKey];

                            if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                            {
                                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                                if (QuoteInProgress != null && QuoteInProgress.Mode == CQRequestActionMode.Edit)
                                {

                                    //ControlEnable("Button", btnSave, true, "button");
                                    //ControlEnable("Button", btnCancel, true, "button");
                                    //ControlEnable("Button", btnDelete, false, "button-disable");

                                    //EnableForm(true);

                                    if (IsAuthorized(Permission.Preflight.AddPreflightPassenger) || IsAuthorized(Permission.Preflight.EditPreflightPassenger))
                                    {
                                        foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                        {
                                            // ((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = true;
                                            if (QuoteInProgress.CQLegs != null)
                                            {
                                                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                                bool isdeadhead = false;
                                                for (int i = 0; i < Preflegs.Count; i++)
                                                {
                                                    if (Preflegs[i].IsDeleted == false)
                                                    {
                                                        isdeadhead = false;

                                                        #region "IsDeadorFerryHead"
                                                        //if (Preflegs[i].FlightCategoryID != null)
                                                        //{
                                                        //    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                        //    {
                                                        //        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                                        //        var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                                        //        if (objRetVal.ReturnFlag == true)
                                                        //        {
                                                        //            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i].FlightCategoryID)).ToList();
                                                        //            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                                        //            {
                                                        //                if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                                        //                {
                                                        //                    isdeadhead = true;
                                                        //                }
                                                        //                else
                                                        //                {
                                                        //                    isdeadhead = false;
                                                        //                }
                                                        //            }
                                                        //            else
                                                        //            {
                                                        //                isdeadhead = false;
                                                        //            }
                                                        //        }
                                                        //    }
                                                        //}
                                                        //else
                                                        //    isdeadhead = false;
                                                        #endregion

                                                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                                        if (!isdeadhead)
                                                        {
                                                            ddlFP.Enabled = true;
                                                        }
                                                        else
                                                        {
                                                            ddlFP.Enabled = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                                        {
                                            Label txtStreet = (Label)dataItem.FindControl("txtStreet");
                                            txtStreet.Enabled = false;
                                            Label txtPostal = (Label)dataItem.FindControl("txtPostal");
                                            txtPostal.Enabled = false;
                                            Label txtCity = (Label)dataItem.FindControl("txtCity");
                                            txtCity.Enabled = false;
                                            Label txtState = (Label)dataItem.FindControl("txtState");
                                            txtState.Enabled = false;
                                            Label tbPurpose = (Label)dataItem.FindControl("tbPurpose");
                                            tbPurpose.Enabled = false;
                                            Label tbPassport = (Label)dataItem.FindControl("tbPassport");
                                            tbPassport.Enabled = false;
                                            Label tbBillingCode = (Label)dataItem.FindControl("tbBillingCode");
                                            tbBillingCode.Enabled = false;
                                            Label lbDateOfBirth = (Label)dataItem.FindControl("lbDateOfBirth");
                                            lbDateOfBirth.Enabled = false;
                                        }
                                    }
                                    else
                                    {
                                        foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                                        {
                                            ((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = false;
                                            if (QuoteInProgress.CQLegs != null)
                                            {
                                                for (int i = 0; i < QuoteInProgress.CQLegs.Count; i++)
                                                {
                                                    if (QuoteInProgress.CQLegs[i].IsDeleted == false)
                                                    {
                                                        DropDownList ddlFP = (DropDownList)dataItem["Leg" + (QuoteInProgress.CQLegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (QuoteInProgress.CQLegs[i].LegNUM).ToString());
                                                        ddlFP.Enabled = false;
                                                    }
                                                }
                                            }
                                        }
                                        foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                                        {
                                            Label txtStreet = (Label)dataItem.FindControl("txtStreet");
                                            txtStreet.Enabled = false;
                                            Label txtPostal = (Label)dataItem.FindControl("txtPostal");
                                            txtPostal.Enabled = false;
                                            Label txtCity = (Label)dataItem.FindControl("txtCity");
                                            txtCity.Enabled = false;
                                            Label txtState = (Label)dataItem.FindControl("txtState");
                                            txtState.Enabled = false;
                                            Label tbPurpose = (Label)dataItem.FindControl("tbPurpose");
                                            tbPurpose.Enabled = false;
                                            Label tbPassport = (Label)dataItem.FindControl("tbPassport");
                                            tbPassport.Enabled = false;
                                            Label tbBillingCode = (Label)dataItem.FindControl("tbBillingCode");
                                            tbBillingCode.Enabled = false;
                                            Label tbDateOfBirth = (Label)dataItem.FindControl("lbDateOfBirth");
                                            tbDateOfBirth.Enabled = false;
                                        }
                                    }
                                }
                            }
                            //else
                            //{
                            //    ControlEnable("Button", btnSave, false, "button-disable");
                            //    ControlEnable("Button", btnCancel, false, "button-disable");
                            //    ControlEnable("Button", btnDelete, true, "button");
                            //    EnableForm(false);
                            //}
                        }
                        //else
                        //{
                        //    ControlEnable("Button", btnSave, false, "button-disable");
                        //    ControlEnable("Button", btnCancel, false, "button-disable");
                        //    ControlEnable("Button", btnDelete, false, "button-disable");
                        //    ControlEnable("Button", btnNext, false, "button-disable");

                        //    EnableForm(false);
                        //}
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        /// <summary>
        /// disableenablefields
        /// </summary>
        /// <param name="status"></param>
        private void EnableForm(bool status)
        {
            //   QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(status))
            {
                lnkPaxRoaster.Visible = status;
                // tbPaxnotes.Enabled = status;
                btnAddremoveColumns.Enabled = status;
                ddlFlightPurpose.Enabled = status;
                SearchBox.Enabled = status;
                // dgAvailablePax.Enabled = status;

                foreach (GridNoRecordsItem item in dgAvailablePax.MasterTableView.GetItems(GridItemType.NoRecordsItem))
                {
                    GridCommandItem commandItem = (GridCommandItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    LinkButton lnkButton = (LinkButton)commandItem.FindControl("lnkPaxDelete");
                    lnkButton.Enabled = status;
                    System.Web.UI.HtmlControls.HtmlAnchor insertlink = (System.Web.UI.HtmlControls.HtmlAnchor)commandItem.FindControl("aInsertCrew");
                    if (insertlink != null)
                    {
                        if (status)
                            insertlink.Attributes.Add("onclick", "ShowPaxInfoPopup('insert')");
                        else
                            insertlink.Attributes.Add("onclick", string.Empty);

                    }
                }
                foreach (GridDataItem dataItem in dgAvailablePax.MasterTableView.Items)
                {

                    GridCommandItem commandItem = (GridCommandItem)dgAvailablePax.MasterTableView.GetItems(GridItemType.CommandItem)[0];
                    LinkButton lnkButton = (LinkButton)commandItem.FindControl("lnkPaxDelete");
                    //((Button)dataItem["DeleteColumn"].Controls[0]).Enabled = status;
                    lnkButton.Enabled = status;
                    System.Web.UI.HtmlControls.HtmlAnchor insertlink = (System.Web.UI.HtmlControls.HtmlAnchor)commandItem.FindControl("aInsertCrew");
                    if (insertlink != null)
                    {
                        if (status)
                            insertlink.Attributes.Add("onclick", "ShowPaxInfoPopup('insert')");
                        else
                            insertlink.Attributes.Add("onclick", string.Empty);

                    }
                    if (QuoteInProgress != null)
                    {
                        if (QuoteInProgress.CQLegs != null)
                        {
                            List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            bool isdeadhead = false;
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                #region
                                //if (Preflegs[i].FlightCategoryID != null)
                                //{
                                //    using (FlightPakMasterService.MasterCatalogServiceClient objFlightCategoryService = new FlightPakMasterService.MasterCatalogServiceClient())
                                //    {
                                //        List<FlightPak.Web.FlightPakMasterService.FlightCatagory> FlightCatagoryLists = new List<FlightPak.Web.FlightPakMasterService.FlightCatagory>();
                                //        var objRetVal = objFlightCategoryService.GetFlightCategoryList();
                                //        if (objRetVal.ReturnFlag == true)
                                //        {
                                //            FlightCatagoryLists = objRetVal.EntityList.Where(x => x.FlightCategoryID == Convert.ToInt64(Preflegs[i].FlightCategoryID)).ToList();
                                //            if (FlightCatagoryLists != null && FlightCatagoryLists.Count > 0)
                                //            {
                                //                if (FlightCatagoryLists[0].IsDeadorFerryHead == true)
                                //                {
                                //                    isdeadhead = true;
                                //                }
                                //                else
                                //                {
                                //                    isdeadhead = false;
                                //                }
                                //            }
                                //            else
                                //            {
                                //                isdeadhead = false;
                                //            }
                                //        }
                                //    }
                                //}
                                //else
                                //    isdeadhead = false;
                                #endregion

                                isdeadhead = false;
                                DropDownList ddlFP = (DropDownList)dataItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("ddlLeg" + (Preflegs[i].LegNUM).ToString());
                                if (!isdeadhead)
                                {
                                    ddlFP.Enabled = status;
                                }
                                else
                                {
                                    ddlFP.Enabled = false;
                                }
                            }
                        }
                    }
                }

                foreach (GridFooterItem footerItem in dgAvailablePax.MasterTableView.GetItems(GridItemType.Footer))
                {
                    if (QuoteInProgress != null)
                    {
                        if (QuoteInProgress.CQLegs != null)
                        {
                            List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                if (Preflegs[i].IsDeleted == false)
                                {
                                    TextBox txtBlockedSeats = (TextBox)footerItem["Leg" + (Preflegs[i].LegNUM).ToString()].FindControl("tbLeg" + (Preflegs[i].LegNUM).ToString());
                                    txtBlockedSeats.Enabled = status;
                                }
                            }
                        }
                    }
                }
                foreach (GridDataItem dataItem in dgPaxSummary.MasterTableView.Items)
                {
                    Label txtStreet = (Label)dataItem.FindControl("txtStreet");
                    txtStreet.Enabled = false;
                    Label txtPostal = (Label)dataItem.FindControl("txtPostal");
                    txtPostal.Enabled = false;
                    Label txtCity = (Label)dataItem.FindControl("txtCity");
                    txtCity.Enabled = false;
                    Label txtState = (Label)dataItem.FindControl("txtState");
                    txtState.Enabled = false;
                    Label tbPurpose = (Label)dataItem.FindControl("tbPurpose");
                    tbPurpose.Enabled = false;
                    Label tbPassport = (Label)dataItem.FindControl("tbPassport");
                    tbPassport.Enabled = false;
                    Label tbBillingCode = (Label)dataItem.FindControl("tbBillingCode");
                    tbBillingCode.Enabled = false;
                    Label tbDateOfBirth = (Label)dataItem.FindControl("lbDateOfBirth");
                    tbDateOfBirth.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Method to Retain Passenger Summary Grid
        /// </summary>
        /// <param name="gridPaxSummary">Pass Grid Control</param>
        /// <returns>Returns Passenger Summary List</returns>
        private List<PassengerSummary> RetainPaxSummaryGrid(RadGrid gridPaxSummary)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(gridPaxSummary))
            {
                List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();


                foreach (GridDataItem Item in gridPaxSummary.MasterTableView.Items)
                {
                    long legId = Convert.ToInt64(((Label)Item.FindControl("lblLegID")).Text);
                    long paxID = Convert.ToInt64(((Label)Item.FindControl("lblPassengerRequestorID")).Text);
                    string paxCode = ((Label)Item.FindControl("lblCode")).Text;
                    string paxName = ((Label)Item.FindControl("lblName")).Text;

                    string Street = ((Label)Item.FindControl("txtStreet")).Text;
                    string Postal = ((Label)Item.FindControl("txtPostal")).Text;
                    string City = ((Label)Item.FindControl("txtCity")).Text;
                    string State = ((Label)Item.FindControl("txtState")).Text;

                    string Purpose = ((Label)Item.FindControl("tbPurpose")).Text;
                    string Passport = ((Label)Item.FindControl("tbPassport")).Text;
                    string BillingCode = ((Label)Item.FindControl("tbBillingCode")).Text;
                    string VisaID = ((HiddenField)Item.FindControl("hdnVisaID")).Value;
                    string PassportID = ((HiddenField)Item.FindControl("hdnPassID")).Value;
                    string PassportExpiryDT = Item["PassportExpiryDT"].Text;
                    string Nation = Item["Nation"].Text;
                    string DateOfBirth = ((Label)Item.FindControl("lbDateOfBirth")).Text;
                    Int32 OrderNUM = (Int32)Item.GetDataKeyValue("OrderNUM");
                    paxSummaryList.Add(new PassengerSummary()
                    {
                        LegId = legId,
                        PaxID = paxID,
                        PaxName = paxName,
                        PaxCode = paxCode,
                        Street = Street,
                        Postal = Postal,
                        City = City,
                        State = State,
                        Purpose = Purpose,
                        Passport = Passport,
                        BillingCode = BillingCode,
                        IsNonPassenger = false,
                        OrderNum = OrderNUM,
                        WaitList = string.Empty,
                        VisaID = VisaID,
                        PassportID = PassportID,
                        PassportExpiryDT = PassportExpiryDT,
                        Nation = Nation,
                        DateOfBirth = DateOfBirth
                    });
                }
                return paxSummaryList;
            }
        }

        #region Pax Summary Grid


        private DataTable FinalPaxSummarylist(string Code)
        {
            DataTable table = null;
            DataTable dtFinal = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                table = dtFinal.Clone();
                drarrayPaxSummary = dtFinal.Select("Code = " + "'" + Code + "'");

                if (drarrayPaxSummary != null)
                {
                    foreach (DataRow drpaxsum in drarrayPaxSummary)
                        table.ImportRow(drpaxsum);
                }
                table.Merge(table);
                return table;
            }
        }

        protected void lnkPaxDelete_OnClick(object source, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (IsAuthorized(Permission.Preflight.DeletePreflightPassenger))
                        {
                            if (dgAvailablePax.SelectedItems.Count > 0)
                            {
                                string paxname = string.Empty;
                                foreach (GridDataItem dataItem in dgAvailablePax.SelectedItems)
                                {
                                    Session["CQSelectedPaxitems"] += dataItem["PassengerRequestorID"].Text + ",";

                                    LinkButton lnkPaxCode = (LinkButton)dataItem.FindControl("lnkPaxCode");
                                    paxname += lnkPaxCode.Text + ",";
                                }
                                paxname = paxname.Length > 0 ? paxname.Substring(0, paxname.Length - 1) : string.Empty;
                                string quotenum = string.Empty;
                                if (QuoteInProgress != null)
                                {
                                    quotenum = QuoteInProgress.QuoteNUM.ToString();
                                }
                                RadWindowManager1.RadConfirm("Are you sure you want to delete Pax Code " + paxname + " from Charter Quote No. " + quotenum + " ?", "confirmDeletePaxCallBackFn", 330, 100, null, "Confirmation!");
                            }
                            else
                            {
                                if (dgAvailablePax.Items != null && dgAvailablePax.Items.Count > 0)
                                {
                                    RadWindowManager1.RadAlert("Please select the record", 250, 100, ModuleNameConstants.CharterQuote.CQPax, "", null);
                                }
                                else
                                {
                                    RadWindowManager1.RadAlert("No Passenger is found for delete,Please add the Passenger", 330, 100, ModuleNameConstants.CharterQuote.CQPax, "", null);
                                }
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        protected void btnDeletePaxYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Paxdelete();
            }
        }
        protected void btnDeletePaxNo_Click(object sender, EventArgs e)
        {
        }

        protected void Paxdelete()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[Master.CQSessionKey] != null)
                {
                    if (IsAuthorized(Permission.Preflight.DeletePreflightPassenger))
                    {
                        if (dgAvailablePax.SelectedItems.Count == 0)
                        {
                            if (Session["CQSelectedPaxitems"] != null)
                            {
                                foreach (GridDataItem dataItem in dgAvailablePax.Items)
                                {
                                    string[] Paxitems = Session["CQSelectedPaxitems"].ToString().Split(',');
                                    foreach (string paxitem in Paxitems)
                                    {
                                        if (!string.IsNullOrEmpty(paxitem))
                                        {
                                            if (Convert.ToInt64(dataItem["PassengerRequestorID"].Text) == Convert.ToInt64(paxitem))
                                            {
                                                dataItem.Selected = true;
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        if (dgAvailablePax.SelectedItems.Count > 0)
                        {
                            foreach (GridDataItem dataItem in dgAvailablePax.SelectedItems)
                            {
                                // QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                                SaveCharterQuoteToSession();
                                List<PassengerInLegs> FinalpassengerInLegs = new List<PassengerInLegs>();
                                List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();
                                FinalpassengerInLegs = (List<PassengerInLegs>)Session["CQAvailablePaxList"];
                                Finalpassengerlist = (List<PassengerSummary>)Session["CQPaxSummaryList"];
                                Int64 PaxID = Convert.ToInt64(dataItem["PassengerRequestorID"].Text);
                                //Convert.ToInt64((e.Item as GridDataItem).OwnerTableView.DataKeyValues[e.Item.ItemIndex]["PassengerRequestorID"]);                              
                                if (QuoteInProgress.CQLegs != null)
                                {
                                    List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                    for (int Legcnt = 0; Legcnt < Preflegs.Count; Legcnt++)
                                    {
                                        if (Preflegs[Legcnt].CQPassengers != null)
                                        {
                                            if (Preflegs[Legcnt].CQPassengers.Count > 0)
                                            {
                                                for (int PaxCnt = 0; PaxCnt < Preflegs[Legcnt].CQPassengers.Count; PaxCnt++)
                                                {
                                                    if (Preflegs[Legcnt].CQPassengers[PaxCnt].IsDeleted == false)
                                                    {
                                                        if (PaxID == Preflegs[Legcnt].CQPassengers[PaxCnt].PassengerRequestorID)
                                                        {

                                                            if (Preflegs[Legcnt].CQPassengers[PaxCnt].CQPassengerID == 0)
                                                            {
                                                                Preflegs[Legcnt].PassengerTotal = Preflegs[Legcnt].PassengerTotal - 1;
                                                                Preflegs[Legcnt].ReservationAvailable = SeatTotal - Preflegs[Legcnt].PassengerTotal + 1;

                                                                if (Preflegs[Legcnt].CQPassengers[PaxCnt].FlightPurposeID != 0)
                                                                {
                                                                    HiddenField hdnPax = ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "p" + Preflegs[Legcnt].LegNUM));
                                                                    hdnPax.Value = Preflegs[Legcnt].PassengerTotal.ToString();
                                                                    ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "a" + Preflegs[Legcnt].LegNUM)).Value = Preflegs[Legcnt].ReservationAvailable.ToString();
                                                                }
                                                                Preflegs[Legcnt].CQPassengers.Remove(Preflegs[Legcnt].CQPassengers[PaxCnt]);
                                                                break;
                                                            }
                                                            else
                                                            {

                                                                Preflegs[Legcnt].PassengerTotal = Preflegs[Legcnt].PassengerTotal - 1;
                                                                Preflegs[Legcnt].ReservationAvailable = SeatTotal - Preflegs[Legcnt].PassengerTotal + 1;
                                                                Preflegs[Legcnt].CQPassengers[PaxCnt].FlightPurposeID = 0;
                                                                ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "p" + Preflegs[Legcnt].LegNUM)).Value = Preflegs[Legcnt].PassengerTotal.ToString();
                                                                ((HiddenField)DivExternalForm.FindControl("hdnLeg" + Preflegs[Legcnt].LegNUM + "a" + Preflegs[Legcnt].LegNUM)).Value = Preflegs[Legcnt].ReservationAvailable.ToString();
                                                                Preflegs[Legcnt].CQPassengers[PaxCnt].State = CQRequestEntityState.Deleted;
                                                                Preflegs[Legcnt].CQPassengers[PaxCnt].IsDeleted = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }

                                for (int FinalAvlCnt = 0; FinalAvlCnt < FinalpassengerInLegs.Count; FinalAvlCnt++)
                                {
                                    if (FinalpassengerInLegs[FinalAvlCnt].PassengerRequestorID == PaxID)
                                    {
                                        FinalpassengerInLegs.Remove(FinalpassengerInLegs[FinalAvlCnt]);
                                    }
                                }


                                for (int FinalAssCnt = 0; FinalAssCnt < Finalpassengerlist.Count; FinalAssCnt++)
                                {
                                    if (Finalpassengerlist[FinalAssCnt].PaxID == PaxID)
                                    {
                                        Finalpassengerlist.Remove(Finalpassengerlist[FinalAssCnt]);
                                        FinalAssCnt = FinalAssCnt - 1;
                                    }
                                }

                                Int32 MAXOrderNUM = 0;
                                foreach (PassengerInLegs Pax in FinalpassengerInLegs.OrderBy(x => x.OrderNUM).ToList())
                                {
                                    MAXOrderNUM++;
                                    Pax.OrderNUM = MAXOrderNUM;

                                    foreach (PassengerSummary PaxSum in Finalpassengerlist.Where(x => x.PaxID == Pax.PassengerRequestorID).ToList())
                                    {
                                        PaxSum.OrderNum = Pax.OrderNUM;
                                    }
                                }

                                Session.Remove("CQSelectedPaxitems");
                                SaveQuoteinProgressToFileSession();

                                //Session[Master.CQSessionKey] = QuoteInProgress;
                                Session["CQAvailablePaxList"] = FinalpassengerInLegs;
                                if (Session["CQAvailablePaxList"] != null)
                                {
                                    dgAvailablePax.DataSource = Session["CQAvailablePaxList"];
                                    dgAvailablePax.DataBind();
                                }
                                Session["CQPaxSummaryList"] = Finalpassengerlist;
                                if (Session["CQPaxSummaryList"] != null)
                                {
                                    dgPaxSummary.DataSource = Session["CQPaxSummaryList"];
                                    dgPaxSummary.DataBind();
                                }
                                if (dgPaxSummary.Items.Count > 0)
                                {
                                    dgPaxSummary.Items[0].Selected = true;
                                    GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                                    Label lblName = (Label)item.FindControl("lblName");
                                    //Prakash  tbPaxName.Text = lblName.Text;
                                    Label lblLegID = (Label)item.FindControl("lblLegID");
                                    if (Session[Master.CQSessionKey] != null)
                                    {
                                        //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];

                                        CQLeg leg = new CQLeg();
                                        leg = QuoteInProgress.CQLegs.Where(x => x.LegNUM == Convert.ToInt64(lblLegID.Text)).FirstOrDefault();
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }

        private void SearchPaxSummary()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool alreadyExists = false;
                if (Session[Master.CQSessionKey] != null)
                {
                    //  QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                    List<PassengerInLegs> passengerlist = new List<PassengerInLegs>();
                    List<PassengerSummary> Newpassengerlist = new List<PassengerSummary>();
                    List<PassengerSummary> Oldpassengerlist = new List<PassengerSummary>();
                    List<PassengerSummary> Finalpassengerlist = new List<PassengerSummary>();

                    passengerlist = (List<PassengerInLegs>)Session["CQAvailablePaxList"];

                    if (Session["CQPaxSummaryList"] != null)
                    {
                        Oldpassengerlist = (List<PassengerSummary>)Session["CQPaxSummaryList"];
                    }

                    if (QuoteInProgress.CQLegs != null)
                    {
                        for (int legCount = 1; legCount <= QuoteInProgress.CQLegs.Count; legCount++)
                        {
                            if (passengerlist != null)
                            {
                                if (Oldpassengerlist != null && Oldpassengerlist.Count > 0)
                                {
                                    for (int paxCnt = 0; paxCnt < passengerlist.Count; paxCnt++)
                                    {

                                        for (int Cnt = 0; Cnt < Oldpassengerlist.Count; Cnt++)
                                        {
                                            alreadyExists = Oldpassengerlist.Any(x => x.PaxID == passengerlist[paxCnt].PassengerRequestorID);
                                        }
                                        if (alreadyExists == false)
                                        {
                                            PassengerSummary pax = new PassengerSummary();
                                            Newpassengerlist.Add(pax);
                                            pax.PaxID = passengerlist[paxCnt].PassengerRequestorID;
                                            pax.PaxCode = passengerlist[paxCnt].Code;
                                            pax.PaxName = passengerlist[paxCnt].Name;
                                            pax.LegId = legCount;
                                            pax.Street = passengerlist[paxCnt].Street;
                                            pax.Postal = passengerlist[paxCnt].Postal;
                                            pax.City = passengerlist[paxCnt].City;
                                            pax.State = passengerlist[paxCnt].State;
                                            pax.Purpose = passengerlist[paxCnt].Purpose;
                                            pax.Passport = passengerlist[paxCnt].Passport;
                                            pax.BillingCode = passengerlist[paxCnt].BillingCode;
                                            pax.VisaID = passengerlist[paxCnt].VisaID;
                                            pax.PassportID = passengerlist[paxCnt].PassportID;
                                            //if (!string.IsNullOrEmpty(passengerlist[paxCnt].PassportExpiryDT.ToString()))
                                            if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].PassportExpiryDT)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(passengerlist[paxCnt].PassportExpiryDT);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    pax.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                else
                                                    pax.PassportExpiryDT = string.Empty;
                                            }
                                            pax.Nation = passengerlist[paxCnt].Nation;
                                            if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].DateOfBirth)))
                                            {
                                                DateTime dtDate = Convert.ToDateTime(passengerlist[paxCnt].DateOfBirth);
                                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                    pax.DateOfBirth = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                                else
                                                    pax.DateOfBirth = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    for (int paxCnt = 0; paxCnt < passengerlist.Count; paxCnt++)
                                    {
                                        PassengerSummary pax = new PassengerSummary();
                                        Newpassengerlist.Add(pax);
                                        pax.PaxID = passengerlist[paxCnt].PassengerRequestorID;
                                        pax.PaxCode = passengerlist[paxCnt].Code;
                                        pax.PaxName = passengerlist[paxCnt].Name;
                                        pax.LegId = legCount;
                                        pax.Street = passengerlist[paxCnt].Street;
                                        pax.Postal = passengerlist[paxCnt].Postal;
                                        pax.City = passengerlist[paxCnt].City;
                                        pax.State = passengerlist[paxCnt].State;
                                        pax.Purpose = passengerlist[paxCnt].Purpose;
                                        pax.Passport = passengerlist[paxCnt].Passport;
                                        pax.BillingCode = passengerlist[paxCnt].BillingCode;
                                        pax.VisaID = passengerlist[paxCnt].VisaID;
                                        pax.PassportID = passengerlist[paxCnt].PassportID;
                                        //if (!string.IsNullOrEmpty(passengerlist[paxCnt].PassportExpiryDT.ToString()))
                                        if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].PassportExpiryDT)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(passengerlist[paxCnt].PassportExpiryDT);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                pax.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                            else
                                                pax.PassportExpiryDT = string.Empty;
                                        }
                                        if (!string.IsNullOrEmpty(Convert.ToString(passengerlist[paxCnt].DateOfBirth)))
                                        {
                                            DateTime dtDate = Convert.ToDateTime(passengerlist[paxCnt].DateOfBirth);
                                            if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                                pax.DateOfBirth = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", ((DateTime)dtDate).ToShortDateString());
                                            else
                                                pax.DateOfBirth = string.Empty;
                                        }
                                        pax.Nation = passengerlist[paxCnt].Nation;
                                    }
                                }
                            }
                        }
                    }

                    if (Oldpassengerlist != null)
                        Finalpassengerlist = Newpassengerlist.Concat(Oldpassengerlist).ToList();

                    Session["CQPaxSummaryList"] = Finalpassengerlist;
                    dgPaxSummary.DataSource = Finalpassengerlist;
                    dgPaxSummary.DataBind();
                    if (dgPaxSummary.Items.Count > 0)
                    {
                        //QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                        List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        dgPaxSummary.SelectedIndexes.Add(0);
                        dgPaxSummary.Items[0].Selected = true;
                        GridDataItem item = (GridDataItem)dgPaxSummary.SelectedItems[0];
                        Label lblName = (Label)item.FindControl("lblName");
                        Label lblLegID = (Label)item.FindControl("lblLegID");
                    }
                }
            }
        }

        private List<PassengerSummary> ConvertDataTabletoList(DataTable dt)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
                foreach (DataRow row in dt.Rows)
                {
                    PassengerSummary pax = new PassengerSummary();
                    pax.LegId = Convert.ToInt64(row["LegID"].ToString());
                    pax.PaxID = Convert.ToInt64(row["PassengerRequestorID"].ToString());
                    pax.PaxCode = row["Code"].ToString();
                    pax.PaxName = row["Name"].ToString();

                    if (!string.IsNullOrEmpty(row["Postal"].ToString()))
                    {
                        pax.Postal = row["Postal"].ToString();
                    }
                    else
                    {
                        pax.Postal = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["Street"].ToString()))
                    {
                        pax.Street = row["Street"].ToString();
                    }
                    else
                    {
                        pax.Street = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["StateName"].ToString()))
                    {
                        pax.State = row["StateName"].ToString();
                    }
                    else
                    {
                        pax.State = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["City"].ToString()))
                    {
                        pax.City = row["City"].ToString();
                    }
                    else
                    {
                        pax.City = string.Empty;
                    }

                    if (!string.IsNullOrEmpty(row["Purpose"].ToString()))
                    {
                        pax.Purpose = row["Purpose"].ToString();
                    }
                    else
                    {
                        pax.Purpose = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["Passport"].ToString()))
                    {
                        pax.Passport = row["Passport"].ToString();
                    }
                    else
                    {
                        pax.Passport = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["BillingCode"].ToString()))
                    {
                        pax.BillingCode = row["BillingCode"].ToString();
                    }
                    else
                    {
                        pax.BillingCode = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["VisaID"].ToString()))
                    {
                        pax.VisaID = row["VisaID"].ToString();
                    }
                    else
                    {
                        pax.VisaID = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["PassportID"].ToString()))
                    {
                        pax.PassportID = row["PassportID"].ToString();
                    }
                    else
                    {
                        pax.PassportID = string.Empty;
                    }
                    //if (!string.IsNullOrEmpty(row["PassportExpiryDT"].ToString()))
                    if (!string.IsNullOrEmpty(Convert.ToString(row["PassportExpiryDT"])))
                    {
                        pax.PassportExpiryDT = row["PassportExpiryDT"].ToString();
                    }
                    else
                    {
                        pax.PassportExpiryDT = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["Nation"].ToString()))
                    {
                        pax.Nation = row["Nation"].ToString();
                    }
                    else
                    {
                        pax.Nation = string.Empty;
                    }
                    if (!string.IsNullOrEmpty(row["DateOfBirth"].ToString()))
                    {
                        pax.DateOfBirth = row["DateOfBirth"].ToString();
                    }
                    else
                    {
                        pax.DateOfBirth = string.Empty;
                    }
                    paxSummaryList.Add(pax);
                }
                return paxSummaryList;
            }
        }

        /// <summary>
        /// dgAssignedCrew ItemCommand
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgPaxSummary_ItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[Master.CQSessionKey] != null)
                        {
                            // QuoteInProgress = (CQMain)Session[Master.CQSessionKey];

                            if (e.CommandName == "RowClick")
                            {
                                GridDataItem test = (GridDataItem)e.Item;
                                int index = e.Item.ItemIndex;
                                e.Item.Selected = true;
                                //Prakash tbPaxName.Text = test.GetDataKeyValue("PaxName").ToString();
                                hdnPassPax.Value = test.GetDataKeyValue("PaxID").ToString();
                                int legid = Convert.ToInt16(test.GetDataKeyValue("LegID"));
                                //lblLegDisplay.Text = "Leg: " + test.GetDataKeyValue("LegID").ToString();
                                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                hdnArrivalICao.Value = Preflegs[legid - 1].AAirportID.ToString();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void dgPaxSummary_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[Master.CQSessionKey] != null)
                        {
                            // QuoteInProgress = (CQMain)Session[Master.CQSessionKey];
                            //if (Trip.Mode == CQRequestActionMode.Edit)
                            //{

                            foreach (GridDataItem dataItem in dgPaxSummary.Items)
                            {
                                if (dataItem.Selected)
                                {
                                    Label lblPassengerRequestorID = (Label)dataItem.FindControl("lblPassengerRequestorID");
                                    hdnPassPax.Value = lblPassengerRequestorID.Text;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void dgPaxSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Item is GridPagerItem)
                        {
                            GridPagerItem pager = (GridPagerItem)e.Item;
                            Label lbl = (Label)pager.FindControl("ChangePageSizeLabel");
                            lbl.Visible = false;

                            RadComboBox combo = (RadComboBox)pager.FindControl("PageSizeComboBox");
                            combo.Visible = false;
                        }
                        if (e.Item is GridDataItem)
                        {
                            GridDataItem dataItem = e.Item as GridDataItem;
                            Label lblPassengerRequestorID = (Label)dataItem.FindControl("lblPassengerRequestorID");
                            Label lblLegID = (Label)dataItem.FindControl("lblLegID");

                            //if (e.Item.Selected)
                            //    hdnPassPax.Value = lblPassengerRequestorID.Text;
                            Label tbPassport = (Label)dataItem.FindControl("tbPassport");
                            HiddenField hdnPassID = (HiddenField)dataItem.FindControl("hdnPassID");
                            HiddenField hdnVisaID = (HiddenField)dataItem.FindControl("hdnVisaID");
                            if (tbPassport != null && hdnPassPax != null && hdnPassPax.Value != null && lblPassengerRequestorID.Text == hdnPassPax.Value && hdnAssignPassLeg.Value != null && lblLegID.Text == hdnAssignPassLeg.Value)
                            {
                                if (Session["CQSelectedPassport"] != null)
                                    tbPassport.Text = System.Web.HttpUtility.HtmlEncode(Session["CQSelectedPassport"].ToString());
                                if (Session["CQSelectedPassportID"] != null)
                                    hdnPassID.Value = Session["CQSelectedPassportID"].ToString();
                                if (Session["CQSelectedVisaID"] != null)
                                    hdnVisaID.Value = Session["CQSelectedVisaID"].ToString();
                                if (Session["CQSelectedPrefPaxPassportExpiryDT"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Session["CQSelectedPrefPaxPassportExpiryDT"].ToString()))
                                    {
                                        DateTime dtDate = Convert.ToDateTime(Session["CQSelectedPrefPaxPassportExpiryDT"]);
                                        if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                            dataItem["PassportExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", (DateTime)dtDate));
                                        else
                                            dataItem["PassportExpiryDT"].Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    }
                                }
                                if (Session["CQSelectedPrefPaxCountryCD"] != null)
                                {
                                    dataItem["Nation"].Text = System.Web.HttpUtility.HtmlEncode(Session["CQSelectedPrefPaxCountryCD"].ToString());
                                }
                                Session.Remove("CQSelectedPrefPaxCountryCD");
                                Session.Remove("CQSelectedPrefPaxPassportExpiryDT");

                                #region "Disabiling Pax Summary Grid"

                                Label tbpaxStreet = (Label)dataItem.FindControl("txtStreet");
                                Label tbpaxPostal = (Label)dataItem.FindControl("txtPostal");
                                Label tbpaxCity = (Label)dataItem.FindControl("txtCity");
                                Label tbpaxState = (Label)dataItem.FindControl("txtState");
                                Label tbpaxPurpose = (Label)dataItem.FindControl("tbPurpose");
                                Label tbpaxPassport = (Label)dataItem.FindControl("tbPassport");
                                Label tbpaxBillingCode = (Label)dataItem.FindControl("tbBillingCode");
                                Label lbDateofBirth = (Label)dataItem.FindControl("lbDateOfBirth");

                                tbpaxStreet.Enabled = false;
                                tbpaxPostal.Enabled = false;
                                tbpaxCity.Enabled = false;
                                tbpaxState.Enabled = false;
                                tbpaxPurpose.Enabled = false;
                                tbpaxPassport.Enabled = false;
                                tbpaxBillingCode.Enabled = false;
                                lbDateofBirth.Enabled = false;

                                #endregion
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void dgPaxSummary_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            List<PassengerSummary> paxSummaryList = new List<PassengerSummary>();
            try
            {
                if (Session["CQPaxSummaryList"] != null)
                {
                    paxSummaryList = (List<PassengerSummary>)Session["CQPaxSummaryList"];
                    dgPaxSummary.VirtualItemCount = paxSummaryList.Count;
                    dgPaxSummary.DataSource = paxSummaryList;
                    // dgPaxSummary.DataBind();
                }
            }
            catch (Exception ex)
            { }
        }
        #endregion

        /// <summary>
        /// Method for showing Alert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Alert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (ViewState["AlertType"] != null)
                        {
                            switch (ViewState["AlertType"].ToString())
                            {
                                case "EmpType_Changed":
                                    break;
                                case "dgAvailablePax_DeleteCommand":
                                    break;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        private void SavePassengers(List<PassengerInLegs> paxlist, List<PassengerSummary> sumlist)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxlist, sumlist))
            {
                int LegNum = 0;
                if (Session[Master.CQSessionKey] != null)
                {
                    // QuoteInProgress = (CQMain)Session[Master.CQSessionKey];

                    List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    for (int i = 0; i < paxlist.Count; i++)
                    {
                        if (paxlist[i].Legs != null)
                        {
                            for (int j = 0; j < paxlist[i].Legs.Count; j++)
                            {
                                if (!string.IsNullOrEmpty(paxlist[i].Legs[j].FlightPurposeID.ToString()) && paxlist[i].Legs[j].FlightPurposeID != 0)
                                {
                                    #region "Updating PAX"
                                    for (int k = 0; k < sumlist.Count; k++)
                                    {
                                        if (paxlist[i].PassengerRequestorID == sumlist[k].PaxID && sumlist[k].LegId == paxlist[i].Legs[j].LegOrder)
                                        {
                                            LegNum = Convert.ToInt16(paxlist[i].Legs[j].LegOrder);
                                            foreach (CQLeg Leg in Preflegs)
                                            {
                                                if (Leg.IsDeleted == false)
                                                {
                                                    if (Leg.LegNUM == LegNum)
                                                    {
                                                        if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                                        {
                                                            Leg.State = CQRequestEntityState.Modified;
                                                        }

                                                        //Doubt  Preflegs[LegNum - 1].ReservationTotal = 0;
                                                        //Doubt   Preflegs[LegNum - 1].WaitNUM = 0;
                                                        #region Leg reservation
                                                        if (LegNum == 1)
                                                        {
                                                            string strpax = hdnLeg1p1.Value;
                                                            string stravailable = hdnLeg1a1.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 2)
                                                        {
                                                            string strpax = hdnLeg2p2.Value;
                                                            string stravailable = hdnLeg2a2.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 3)
                                                        {
                                                            string strpax = hdnLeg3p3.Value;
                                                            string stravailable = hdnLeg3a3.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 4)
                                                        {
                                                            string strpax = hdnLeg4p4.Value;
                                                            string stravailable = hdnLeg4a4.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 5)
                                                        {
                                                            string strpax = hdnLeg5p5.Value;
                                                            string stravailable = hdnLeg5a5.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 6)
                                                        {
                                                            string strpax = hdnLeg6p6.Value;
                                                            string stravailable = hdnLeg6a6.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 7)
                                                        {
                                                            string strpax = hdnLeg7p7.Value;
                                                            string stravailable = hdnLeg7a7.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 8)
                                                        {
                                                            string strpax = hdnLeg8p8.Value;
                                                            string stravailable = hdnLeg8a8.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 9)
                                                        {
                                                            string strpax = hdnLeg9p9.Value;
                                                            string stravailable = hdnLeg9a9.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 10)
                                                        {
                                                            string strpax = hdnLeg10p10.Value;
                                                            string stravailable = hdnLeg10a10.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 11)
                                                        {
                                                            string strpax = hdnLeg11p11.Value;
                                                            string stravailable = hdnLeg11a11.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 12)
                                                        {
                                                            string strpax = hdnLeg12p12.Value;
                                                            string stravailable = hdnLeg12a12.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 13)
                                                        {
                                                            string strpax = hdnLeg13p13.Value;
                                                            string stravailable = hdnLeg13a13.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 14)
                                                        {
                                                            string strpax = hdnLeg14p14.Value;
                                                            string stravailable = hdnLeg14a14.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 15)
                                                        {
                                                            string strpax = hdnLeg15p15.Value;
                                                            string stravailable = hdnLeg15a15.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 16)
                                                        {
                                                            string strpax = hdnLeg16p16.Value;
                                                            string stravailable = hdnLeg16a16.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 17)
                                                        {
                                                            string strpax = hdnLeg17p17.Value;
                                                            string stravailable = hdnLeg17a17.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 18)
                                                        {
                                                            string strpax = hdnLeg18p18.Value;
                                                            string stravailable = hdnLeg18a18.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 19)
                                                        {
                                                            string strpax = hdnLeg19p19.Value;
                                                            string stravailable = hdnLeg19a19.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 20)
                                                        {
                                                            string strpax = hdnLeg20p20.Value;
                                                            string stravailable = hdnLeg20a20.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 21)
                                                        {
                                                            string strpax = hdnLeg21p21.Value;
                                                            string stravailable = hdnLeg21a21.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 22)
                                                        {
                                                            string strpax = hdnLeg22p22.Value;
                                                            string stravailable = hdnLeg22a22.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 23)
                                                        {
                                                            string strpax = hdnLeg23p23.Value;
                                                            string stravailable = hdnLeg23a23.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 24)
                                                        {
                                                            string strpax = hdnLeg24p24.Value;
                                                            string stravailable = hdnLeg24a24.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }
                                                        if (LegNum == 25)
                                                        {
                                                            string strpax = hdnLeg25p25.Value;
                                                            string stravailable = hdnLeg25a25.Value;
                                                            //if (!string.IsNullOrEmpty(strpax))
                                                            //    Preflegs[LegNum - 1].PassengerTotal = Convert.ToInt32(strpax);
                                                            //if (!string.IsNullOrEmpty(stravailable))
                                                            //    Preflegs[LegNum - 1].ReservationAvailable = Convert.ToInt32(stravailable);
                                                        }

                                                        #endregion

                                                        Int64 GridPaxID = 0;
                                                        GridPaxID = Convert.ToInt64(paxlist[i].PassengerRequestorID);
                                                        if (Leg.CQPassengers != null && Leg.CQPassengers.Count > 0)
                                                        {
                                                            bool PassFound = false;
                                                            foreach (CQPassenger PrefPass in Leg.CQPassengers)
                                                            {
                                                                if (PrefPass.PassengerRequestorID == GridPaxID)
                                                                {
                                                                    #region Passenge Modfiy Mode Already Exists
                                                                    if (PrefPass.CQPassengerID != 0)
                                                                        PrefPass.State = CQRequestEntityState.Modified;
                                                                    else
                                                                        PrefPass.State = CQRequestEntityState.Added;
                                                                    PrefPass.CQLegID = Leg.CQLegID;
                                                                    PrefPass.IsDeleted = false;

                                                                    PrefPass.PassengerRequestorID = Convert.ToInt64(paxlist[i].PassengerRequestorID);
                                                                    PrefPass.PassengerName = paxlist[i].Name;



                                                                    //if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                    //    PrefPass.PassportID = Convert.ToInt64(sumlist[k].PassportID);

                                                                    //if (!string.IsNullOrEmpty(sumlist[k].VisaID))
                                                                    //    PrefPass.VisaID = Convert.ToInt64(sumlist[k].VisaID);

                                                                    PrefPass.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                    //PrefPass.PassportNUM = paxlist[i].Passport;
                                                                    PrefPass.Billing = sumlist[k].BillingCode;
                                                                    //PrefPass.DateOfBirth = sumlist[k].DateOfBirth;
                                                                    PrefPass.OrderNUM = paxlist[i].OrderNUM;
                                                                    PrefPass.IsNonPassenger = false;
                                                                    PrefPass.IsBlocked = false;
                                                                    PrefPass.IsDeleted = false;
                                                                    PrefPass.LastUpdTS = DateTime.UtcNow;
                                                                    //PrefPass.StateName = sumlist[k].State;
                                                                    //PrefPass.Street = sumlist[k].Street;
                                                                    //PrefPass.CityName = sumlist[k].City;
                                                                    //PrefPass.PostalZipCD = sumlist[k].Postal;
                                                                    PassFound = true;
                                                                    #endregion
                                                                    break;
                                                                }
                                                            }
                                                            //Add here 
                                                            if (!PassFound)
                                                            {
                                                                #region Passnot found
                                                                //it is a new Pax for this leg
                                                                CQPassenger NewPaxList = new CQPassenger();
                                                                NewPaxList.State = CQRequestEntityState.Added;
                                                                NewPaxList.CQLegID = Leg.CQLegID;
                                                                NewPaxList.IsDeleted = false;

                                                                //add the passenger list here
                                                                NewPaxList.PassengerRequestorID = Convert.ToInt64(paxlist[i].PassengerRequestorID);
                                                                NewPaxList.PassengerName = paxlist[i].Name;

                                                                if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                    NewPaxList.PassportID = Convert.ToInt64(sumlist[k].PassportID);

                                                                //if (!string.IsNullOrEmpty(sumlist[k].VisaID))
                                                                //    NewPaxList.VisaID = Convert.ToInt64(sumlist[k].VisaID);

                                                                NewPaxList.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                                //NewPaxList.PassportNUM = paxlist[i].Passport;
                                                                //if (!string.IsNullOrEmpty(paxlist[k].BillingCode))
                                                                //    NewPaxList.Billing = paxlist[k].BillingCode;
                                                                //if (!string.IsNullOrEmpty(paxlist[k].DateOfBirth))
                                                                //    NewPaxList.DateOfBirth = paxlist[k].DateOfBirth;
                                                                NewPaxList.OrderNUM = paxlist[i].OrderNUM;
                                                                NewPaxList.IsNonPassenger = false;
                                                                NewPaxList.IsBlocked = false;
                                                                NewPaxList.IsDeleted = false;
                                                                //NewPaxList.StateName = sumlist[k].State;
                                                                //NewPaxList.Street = sumlist[k].Street;
                                                                //NewPaxList.CityName = sumlist[k].City;
                                                                //NewPaxList.PostalZipCD = sumlist[k].Postal;
                                                                NewPaxList.LastUpdTS = DateTime.UtcNow;


                                                                Leg.CQPassengers.Add(NewPaxList);
                                                                #endregion
                                                            }
                                                        }
                                                        else
                                                        {
                                                            #region noPassenger in legs
                                                            //it is a new crew for this leg
                                                            if (Leg.CQPassengers == null)
                                                            {
                                                                Leg.CQPassengers = new List<CQPassenger>();
                                                            }
                                                            CQPassenger NewPaxList = new CQPassenger();
                                                            NewPaxList.State = CQRequestEntityState.Added;
                                                            NewPaxList.CQLegID = Leg.CQLegID;
                                                            NewPaxList.IsDeleted = false;
                                                            //add pax here 
                                                            NewPaxList.PassengerRequestorID = Convert.ToInt64(paxlist[i].PassengerRequestorID);

                                                            NewPaxList.PassengerName = paxlist[i].Name;

                                                            if (!string.IsNullOrEmpty(sumlist[k].PassportID))
                                                                NewPaxList.PassportID = Convert.ToInt64(sumlist[k].PassportID);

                                                            //if (!string.IsNullOrEmpty(sumlist[k].VisaID))
                                                            //    NewPaxList.VisaID = Convert.ToInt64(sumlist[k].VisaID);

                                                            NewPaxList.FlightPurposeID = Convert.ToInt64(paxlist[i].Legs[j].FlightPurposeID);
                                                            //NewPaxList.PassportNUM = paxlist[i].Passport;
                                                            //NewPaxList.Billing = paxlist[k].BillingCode;
                                                            //NewPaxList.DateOfBirth = paxlist[k].DateOfBirth;
                                                            NewPaxList.OrderNUM = paxlist[i].OrderNUM;
                                                            NewPaxList.IsNonPassenger = false;
                                                            NewPaxList.IsBlocked = false;
                                                            NewPaxList.IsDeleted = false;
                                                            NewPaxList.LastUpdTS = DateTime.UtcNow;
                                                            //NewPaxList.StateName = sumlist[k].State;
                                                            //NewPaxList.Street = sumlist[k].Street;
                                                            //NewPaxList.CityName = sumlist[k].City;
                                                            //NewPaxList.PostalZipCD = sumlist[k].Postal;
                                                            Leg.CQPassengers.Add(NewPaxList);
                                                            #endregion
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }
                                else
                                {
                                    #region Tochange flightpurpose during edit when previously set
                                    if (Preflegs != null)
                                    {
                                        for (int legcnt = 0; legcnt < Preflegs.Count; legcnt++)
                                        {
                                            LegNum = Convert.ToInt16(paxlist[i].Legs[j].LegOrder);
                                            if (Preflegs[legcnt].LegNUM == LegNum)
                                            {
                                                if (Preflegs[legcnt].CQPassengers != null)
                                                {
                                                    for (int passcnt = 0; passcnt < Preflegs[legcnt].CQPassengers.Count; passcnt++)
                                                    {
                                                        if (Preflegs[legcnt].CQPassengers[passcnt].PassengerRequestorID == paxlist[i].PassengerRequestorID && Preflegs[legcnt].LegNUM == paxlist[i].Legs[j].LegOrder)
                                                        {
                                                            if (Preflegs[legcnt].CQPassengers[passcnt].CQPassengerID != 0)
                                                            {
                                                                Preflegs[legcnt].CQPassengers[passcnt].State = CQRequestEntityState.Deleted;
                                                                Preflegs[legcnt].CQPassengers[passcnt].IsDeleted = true;
                                                                Preflegs[legcnt].CQPassengers[passcnt].FlightPurposeID = 0;
                                                            }
                                                            else
                                                            {
                                                                Preflegs[legcnt].CQPassengers.Remove(Preflegs[legcnt].CQPassengers[passcnt]);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                    }

                    if (paxlist != null && paxlist.Count == 0)
                    {
                        if (Preflegs != null)
                        {
                            for (int legscnt = 0; legscnt < Preflegs.Count; legscnt++)
                            {
                                if (Preflegs[legscnt].CQLegID != 0)
                                {
                                    Preflegs[legscnt].State = CQRequestEntityState.Modified;
                                }

                                //Doubt Preflegs[legscnt].ReservationTotal = 0;
                                //Doubt Preflegs[legscnt].WaitNUM = 0;

                                #region blocked count

                                if (legscnt + 1 == 1)
                                {
                                    string strpax = hdnLeg1p1.Value;
                                    string stravailable = hdnLeg1a1.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 2)
                                {
                                    string strpax = hdnLeg2p2.Value;
                                    string stravailable = hdnLeg2a2.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 3)
                                {
                                    string strpax = hdnLeg3p3.Value;
                                    string stravailable = hdnLeg3a3.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 4)
                                {
                                    string strpax = hdnLeg4p4.Value;
                                    string stravailable = hdnLeg4a4.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 5)
                                {
                                    string strpax = hdnLeg5p5.Value;
                                    string stravailable = hdnLeg5a5.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 6)
                                {
                                    string strpax = hdnLeg6p6.Value;
                                    string stravailable = hdnLeg6a6.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 7)
                                {
                                    string strpax = hdnLeg7p7.Value;
                                    string stravailable = hdnLeg7a7.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 8)
                                {
                                    string strpax = hdnLeg8p8.Value;
                                    string stravailable = hdnLeg8a8.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 9)
                                {
                                    string strpax = hdnLeg9p9.Value;
                                    string stravailable = hdnLeg9a9.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 10)
                                {
                                    string strpax = hdnLeg10p10.Value;
                                    string stravailable = hdnLeg10a10.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 11)
                                {
                                    string strpax = hdnLeg11p11.Value;
                                    string stravailable = hdnLeg11a11.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 12)
                                {
                                    string strpax = hdnLeg12p12.Value;
                                    string stravailable = hdnLeg12a12.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 13)
                                {
                                    string strpax = hdnLeg13p13.Value;
                                    string stravailable = hdnLeg13a13.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 14)
                                {
                                    string strpax = hdnLeg14p14.Value;
                                    string stravailable = hdnLeg14a14.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 15)
                                {
                                    string strpax = hdnLeg15p15.Value;
                                    string stravailable = hdnLeg15a15.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 16)
                                {
                                    string strpax = hdnLeg16p16.Value;
                                    string stravailable = hdnLeg16a16.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 17)
                                {
                                    string strpax = hdnLeg17p17.Value;
                                    string stravailable = hdnLeg17a17.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 18)
                                {
                                    string strpax = hdnLeg18p18.Value;
                                    string stravailable = hdnLeg18a18.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 19)
                                {
                                    string strpax = hdnLeg19p19.Value;
                                    string stravailable = hdnLeg19a19.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 20)
                                {
                                    string strpax = hdnLeg20p20.Value;
                                    string stravailable = hdnLeg20a20.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 21)
                                {
                                    string strpax = hdnLeg21p21.Value;
                                    string stravailable = hdnLeg21a21.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 22)
                                {
                                    string strpax = hdnLeg22p22.Value;
                                    string stravailable = hdnLeg22a22.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 23)
                                {
                                    string strpax = hdnLeg23p23.Value;
                                    string stravailable = hdnLeg23a23.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 24)
                                {
                                    string strpax = hdnLeg24p24.Value;
                                    string stravailable = hdnLeg24a24.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }
                                if (legscnt + 1 == 25)
                                {
                                    string strpax = hdnLeg25p25.Value;
                                    string stravailable = hdnLeg25a25.Value;
                                    //if (!string.IsNullOrEmpty(strpax))
                                    //    Preflegs[legscnt].PassengerTotal = Convert.ToInt32(strpax);
                                    //if (!string.IsNullOrEmpty(stravailable))
                                    //    Preflegs[legscnt].ReservationAvailable = Convert.ToInt32(stravailable);
                                }

                                #endregion
                            }
                        }

                    }
                }
            }
        }

        #region Permissions
        /// <summary>
        /// Method to Apply Permissions
        /// </summary>
        private void ApplyPermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                // Check for Page Level Permissions
                CheckAutorization(Permission.CharterQuote.ViewCQPAXManifest);
                base.HaveModuleAccessAndRedirect(ModuleId.CharterQuote);

                // Check for Field Level Permissions
                if (IsAuthorized(Permission.CharterQuote.ViewCQPAXManifest))
                {
                    // Show read-only form
                    EnableForm(false);

                    // Check Page Controls - Visibility
                    if (IsAuthorized(Permission.CharterQuote.AddCQPAXManifest) || IsAuthorized(Permission.CharterQuote.EditCQPAXManifest))
                    {
                        ControlVisibility("Button", btnSave, true);
                        ControlVisibility("Button", btnCancel, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.DeleteCQPAXManifest))
                    {
                        ControlVisibility("Button", btnDelete, true);
                    }
                    if (IsAuthorized(Permission.CharterQuote.EditCQPAXManifest))
                    {
                        ControlVisibility("Button", btnEditFile, true);
                    }
                    // Check Page Controls - Disable/Enable
                    if (Session[Master.CQSessionKey] != null)
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];

                        if (FileRequest != null && (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified)) // Add or Edit Mode
                        {
                            EnableForm(true);

                            if (IsAuthorized(Permission.CharterQuote.AddCQPAXManifest) || IsAuthorized(Permission.CharterQuote.EditCQPAXManifest))
                            {
                                ControlEnable("Button", btnSave, true, "button");
                                ControlEnable("Button", btnCancel, true, "button");
                            }
                            ControlEnable("Button", btnDelete, false, "button-disable");
                            ControlEnable("Button", btnEditFile, false, "button-disable");
                        }
                        else
                        {
                            ControlEnable("Button", btnSave, false, "button-disable");
                            ControlEnable("Button", btnCancel, false, "button-disable");
                            ControlEnable("Button", btnDelete, true, "button");
                            ControlEnable("Button", btnEditFile, true, "button");
                        }
                    }
                    else
                    {
                        ControlEnable("Button", btnSave, false, "button-disable");
                        ControlEnable("Button", btnCancel, false, "button-disable");
                        ControlEnable("Button", btnDelete, false, "button-disable");
                        ControlEnable("Button", btnEditFile, false, "button-disable");
                    }
                }
            }
        }

        /// <summary>
        /// Method for Controls Visibility
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Visibility Action</param>
        private void ControlVisibility(string ctrlType, Control ctrlName, bool isEnable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Visible = isEnable;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        pnl.Visible = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Visible = isEnable;
                        break;
                    default: break;
                }
            }
        }

        /// <summary>
        /// Method for Controls Enable / Disable
        /// </summary>
        /// <param name="ctrlType">Pass Control Type</param>
        /// <param name="ctrlName">Pass Control Name</param>
        /// <param name="isEnable">Pass Enable Action</param>
        /// <param name="isEnable">Pass CSS Class Name</param>
        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    case "TextBox":
                        TextBox txt = (TextBox)ctrlName;
                        txt.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            txt.CssClass = cssClass;
                        break;
                    default: break;
                }
            }
        }

        #endregion

        #region Button Events


        /// <summary>
        /// To delete the record
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileRequest = (CQFile)Session[Master.CQSessionKey];
                        Master.Delete((int)FileRequest.FileNUM);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        /// <summary>
        /// To cancel the changes made
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.Cancel();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string PaxpurposeErrorstr = string.Empty;
                        PaxpurposeErrorstr = PaxPurposeError();

                        if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                            RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmPaxRemoveCallBackFn", 330, 100, null, "Confirmation!");
                        else
                        {
                            if (IsAuthorized(Permission.Preflight.AddPreflightPassenger) || IsAuthorized(Permission.Preflight.EditPreflightPassenger))
                            {
                                Page.Validate();
                                if (Page.IsValid)
                                {
                                    SaveCharterQuoteToSession();
                                    FileRequest = (CQFile)Session[Master.CQSessionKey];
                                    Master.Save(FileRequest);
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }

        /// <summary>
        /// To move next tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Next_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string PaxpurposeErrorstr = string.Empty;
                        PaxpurposeErrorstr = PaxPurposeError();

                        if (!string.IsNullOrEmpty(PaxpurposeErrorstr))
                            RadWindowManager1.RadConfirm(PaxpurposeErrorstr, "confirmPaxNextCallBackFn", 330, 100, null, "Confirmation!");
                        else
                        {
                            SaveCharterQuoteToSession();
                            Master.Next("PAX");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
        #endregion

        protected void Edit_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Master.EditFile_Click(sender, e);

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQPax);
                }
            }
        }
    }
}
