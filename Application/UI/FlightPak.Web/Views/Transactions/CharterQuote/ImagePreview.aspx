﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImagePreview.aspx.cs" Inherits="FlightPak.Web.Views.Transactions.CharterQuote.ImagePreview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Image ID="imgPopup" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
