﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Web.PreflightService;
using System.Text;
using System.Globalization;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.CalculationService;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Web.BusinessLite;
#endregion

namespace FlightPak.Web.Views.Transactions
{
    public partial class History : BaseSecuredPage
    {

        #region Company Details
        private FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetCompanyWithFilters Companymaster = new GetCompanyWithFilters();
                    var objCompany = objDstsvc.GetCompanyWithFilters(string.Empty, HomeBaseID,true,true);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters> Company = objCompany.EntityList; 
                        //(from Comp in objCompany.EntityList where Comp.HomebaseID == HomeBaseID select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            #region This must be visible only for Preflight
            TableLogisticHistory.Visible = false;
            TableReportTitle.Visible = false;
            TableTripInfo.Visible = false;
            this.divHistory.Visible = false;

            #endregion
            lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbType.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                ExceptionManager exManager;
                PreflightTripViewModel Trip = new PreflightTripViewModel();
                PostflightService.PostflightMain TripLog = new PostflightService.PostflightMain();
                CharterQuoteService.CQFile File = new CharterQuoteService.CQFile();

                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (Request.QueryString["FromPage"] != null && (Request.QueryString["FromPage"] == "Preflight" || Request.QueryString["FromPage"] == "FleetAndCrew" || Request.QueryString["FromPage"] == "Postflight"))
                    {
                        Int64 TripId = 0;
                        Int64 CustomerId = 0;
                        if (Request.QueryString["FromPage"] == "FleetAndCrew")
                        {
                            if (Request.QueryString["TripId"] != null)
                            {
                                TripId = Int64.TryParse(Request.QueryString["TripId"].ToString(), out TripId) ? TripId : 0;
                                if (UserPrincipal != null && UserPrincipal.Identity._customerID != null)
                                {
                                    CustomerId = Convert.ToInt64(UserPrincipal.Identity._customerID.ToString().Trim());
                                }
                            }
                        }
                        else if (Request.QueryString["FromPage"] == "Preflight")
                        {
                            if (Session[WebSessionKeys.CurrentPreflightTrip] != null)
                            {
                                TableLogisticHistory.Visible = true;

                                TableTripInfo.Visible = true;
                                // tbTripsheetHistory.Visible = true;
                                Trip = (PreflightTripViewModel)Session[WebSessionKeys.CurrentPreflightTrip];
                                TripId = Trip.PreflightMain.TripID;
                                CustomerId = Convert.ToInt64(Trip.PreflightMain.CustomerID);

                                if (Trip.PreflightMain.HomebaseID != 0 || Trip.PreflightMain.HomebaseID != null)
                                {
                                    GetCompanyWithFilters company = new GetCompanyWithFilters();
                                    company = GetCompany((long)Trip.PreflightMain.HomebaseID);
                                    if (company != null)
                                        lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(company.CompanyName);
                                }

                                StringBuilder tripSheetBuilder = new StringBuilder();
                                lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.TripNUM.ToString());
                                int LastCount = 0;
                                if (Trip.PreflightLegs != null)
                                {
                                    List<PreflightLegViewModel> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                    if (Preflegs != null && Preflegs.Count > 0)
                                    {
                                        LastCount = Preflegs.Count;
                                          
                                        DateTime FirstLegDepartLocalDate = (DateTime)Preflegs[0].DepartureDTTMLocal; //Minimum Departure Date
                                        DateTime LastLegArrivalLocalDate = (DateTime)Preflegs[LastCount - 1].ArrivalDTTMLocal; // Max Arrival Date
                                        lbTripDates.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", FirstLegDepartLocalDate) + " " + "-" + " " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LastLegArrivalLocalDate));
                                    
                                    }
                                }
                                lbdescription.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.TripDescription);
                                if (Trip.PreflightMain.Fleet != null)
                                    lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Fleet.TailNum);
                                else
                                    lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                if (Trip.PreflightMain.Aircraft != null)
                                    lbType.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Aircraft.AircraftCD);
                                else
                                    lbType.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                if (Trip.PreflightMain.Passenger != null)
                                {
                                    // lbRequestor.Text = Trip.Passenger.PassengerRequestorCD;
                                    //lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(Trip.Passenger.FirstName + Trip.Passenger.MiddleInitial + Trip.Passenger.LastName);
                                    lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Passenger.FirstName) + (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Passenger.MiddleInitial)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Passenger.MiddleInitial))) + (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Passenger.LastName)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Passenger.LastName)));
                                    lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(Trip.PreflightMain.Passenger.AdditionalPhoneNum);
                                }
                                else
                                {
                                    lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }

                            }

                        }
                        if (TripId != 0)
                        {
                            TableReportTitle.Visible = true;

                            this.divHistory.Visible = true;
                            //tbTripsheetHistory.Visible = true; 
                            lblReportName.Text = System.Web.HttpUtility.HtmlEncode("TRIPSHEET HISTORY");
                            lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            PreflightServiceClient objPreflightClient = new PreflightServiceClient();
                            var ObjRetval = objPreflightClient.GetHistory(CustomerId, TripId);
                            if (ObjRetval.ReturnFlag)
                            {
                                var historyList = ObjRetval.EntityList;
                                StringBuilder historybuilder = new StringBuilder();
                                foreach (PreflightTripHistory tripHistory in historyList)
                                {
                                    historybuilder.Append("Last Updated user");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    historybuilder.Append(tripHistory.LastUpdUID.Trim());
                                    historybuilder.Append("<br>");

                                    historybuilder.Append("Last Updated Time");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    DateTime LastModifiedUTCDate = DateTime.Now;
                                    if (UserPrincipal.Identity._airportId != null)
                                    {
                                        using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                                        {
                                            LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)tripHistory.LastUpdTS, false, false);
                                        }
                                    }
                                    else
                                        LastModifiedUTCDate = (DateTime)tripHistory.LastUpdTS;
                                    historybuilder.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", LastModifiedUTCDate)+" LCL" + "(" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", (DateTime)tripHistory.LastUpdTS) + " UTC)");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("Revision No.");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    historybuilder.Append(tripHistory.RevisionNumber);
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    if (tripHistory.HistoryDescription != null)
                                        historybuilder.Append(tripHistory.HistoryDescription.Replace(Environment.NewLine, "<br>"));
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    //divHistory.InnerHtml = System.Web.HttpUtility.HtmlEncode(historybuilder.ToString());
                                    //HtmlString a = new HtmlString(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));
                                    //divHistory.InnerHtml = a.ToString();
                                    divHistory.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));
                                    // tbTripHistory.Text = historybuilder.ToString();


                                }
                            }
                            else
                            {
                                //tbTripHistory.Text = string.Empty;
                            }
                        }

                        else
                            if (Request.QueryString["FromPage"] == "Postflight")
                            {
                                if (Session["POSTFLIGHTMAIN"] != null)
                                {
                                    StringBuilder tripSheetBuilder = new StringBuilder();
                                    TripLog = (PostflightService.PostflightMain)Session["POSTFLIGHTMAIN"];
                                    TripId = TripLog.POLogID;
                                    CustomerId = Convert.ToInt64(TripLog.CustomerID);

                                    TableLogisticHistory.Attributes.Add("style", "display:none;"); //.Visible = false;
                                    TableReportTitle.Visible = true;
                                    TableTripInfo.Visible = true;
                                    this.divHistory.Visible = true;
                                    lblReportName.Text = System.Web.HttpUtility.HtmlEncode("TRIP LOG HISTORY");
                                    lbTrip.Text = System.Web.HttpUtility.HtmlEncode("Log No.");
                                    lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbTripDates.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbdescription.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    lbType.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                    if (TripLog.HomebaseID != null && TripLog.HomebaseID != 0)
                                    {
                                        GetCompanyWithFilters company = new GetCompanyWithFilters();
                                        company = GetCompany((long)TripLog.HomebaseID);
                                        if (company != null)
                                            lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(company.CompanyName);
                                    }

                                    if (TripLog.LogNum != null)
                                        lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(TripLog.LogNum.ToString());

                                    //int LastCount = 0;
                                    if (TripLog.PostflightLegs != null)
                                    {
                                        List<PostflightService.PostflightLeg> POlegs = TripLog.PostflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                        if (POlegs != null && POlegs.Count > 0)
                                        {
                                            //for (int legCnt = 0; legCnt < POlegs.Count; legCnt++)
                                            //{
                                            //LastCount = POlegs.Count;
                                            DateTime? FirstLegDepartUTCDate = null;
                                            DateTime? LastLegArrivalUTCDate = null;

                                            FirstLegDepartUTCDate = POlegs[0].ScheduledTM; //Minimum Departure Date
                                            LastLegArrivalUTCDate = POlegs[POlegs.Count - 1].InboundDTTM; // Max Arrival Date

                                            if (FirstLegDepartUTCDate != null && LastLegArrivalUTCDate != null)
                                                lbTripDates.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", FirstLegDepartUTCDate) + " " + "-" + " " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LastLegArrivalUTCDate));
                                            else if (FirstLegDepartUTCDate != null && LastLegArrivalUTCDate == null)
                                                lbTripDates.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", FirstLegDepartUTCDate));

                                            //}
                                        }
                                    }

                                    if (TripLog.POMainDescription != null)
                                        lbdescription.Text = System.Web.HttpUtility.HtmlEncode(TripLog.POMainDescription);

                                    if (TripLog.Fleet != null)
                                    {
                                        lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(TripLog.Fleet.TailNum);
                                        lbType.Text = System.Web.HttpUtility.HtmlEncode(LoadAircraftCode(TripLog.Fleet.TailNum.Trim()).ToUpper().Trim()); //TripLog.Aircraft.AircraftCD;
                                    }

                                    if (TripLog.PassengerRequestorID != null)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient RequestorService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var RequestorVal = DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqID(TripLog.PassengerRequestorID.Value);                                            
                                            if (RequestorVal != null)
                                            {
                                                lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(RequestorVal.PassengerName.ToUpper().Trim());
                                                if ((RequestorVal).PhoneNum != null)
                                                    lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(RequestorVal.PhoneNum.ToUpper().Trim());
                                            }
                                        }
                                    }
                                }

                                if (TripId != 0)
                                {
                                    using (PostflightService.PostflightServiceClient objPostflightClient = new PostflightService.PostflightServiceClient())
                                    {
                                        var ObjRetval = objPostflightClient.GetHistory(CustomerId, TripId);
                                        if (ObjRetval != null && ObjRetval.ReturnFlag)
                                        {
                                            var historyList = ObjRetval.EntityList;
                                            StringBuilder historybuilder = new StringBuilder();
                                            foreach (PostflightService.PostflightLogHistory tripHistory in historyList)
                                            {
                                                historybuilder.Append("Last Updated user");
                                                historybuilder.Append(":");
                                                historybuilder.Append(" ");
                                                historybuilder.Append(tripHistory.LastUpdUID.Trim());
                                                historybuilder.Append("<br>");

                                                historybuilder.Append("Last Updated Time");
                                                historybuilder.Append(":");
                                                historybuilder.Append(" ");
                                                DateTime LastModifiedUTCDate = DateTime.Now;
                                                if (UserPrincipal.Identity._airportId != null)
                                                {
                                                    using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                                                    {
                                                        LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)tripHistory.LastUpdTS, false, false);
                                                    }
                                                }
                                                else
                                                    LastModifiedUTCDate = (DateTime)tripHistory.LastUpdTS;
                                                historybuilder.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", LastModifiedUTCDate) + " LCL" + "(" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", (DateTime)tripHistory.LastUpdTS) + " UTC)");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                historybuilder.Append("&nbsp");
                                                //historybuilder.Append("Revision No.");
                                                //historybuilder.Append(":");
                                                //historybuilder.Append(" ");
                                                //historybuilder.Append(tripHistory.RevisionNumber);
                                                historybuilder.Append("<br>");
                                                historybuilder.Append("<br>");
                                                historybuilder.Append(tripHistory.HistoryDescription.Replace(Environment.NewLine, "<br>"));
                                                historybuilder.Append("<br>");
                                                historybuilder.Append("<br>");
                                                historybuilder.Append("<br>");
                                                //divHistory.InnerHtml = System.Web.HttpUtility.HtmlEncode(historybuilder.ToString());
                                                //divHistory.InnerText = System.Web.HttpUtility.HtmlEncode(historybuilder.ToString());
                                                //HtmlString a = new HtmlString(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));
                                                //divHistory.InnerHtml = a.ToString()
                                                divHistory.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));
                                            }
                                        }
                                    }
                                }
                            }
                    }

                    #region "Charter Quote"

                    if (Request.QueryString["FromPage"] != null && Request.QueryString["FromPage"] == "CharterQuote")
                    {
                        Int64 Mainid = 0;
                        Int64 CustomerId = 0;
                        lbTrip.Text = "File No.";
                        lbTripDate.Text = "File Date";
                        if (Session["CQFILE"] != null)
                        {
                            TableLogisticHistory.Visible = true;

                            TableTripInfo.Visible = true;
                            // tbTripsheetHistory.Visible = true;
                            File = (CharterQuoteService.CQFile)Session["CQFILE"];

                            CustomerId = Convert.ToInt64(File.CustomerID);

                            if (File.HomebaseID != 0 || File.HomebaseID != null)
                            {
                                GetCompanyWithFilters company = new GetCompanyWithFilters();
                                company = GetCompany((long)File.HomebaseID);
                                if (company != null)
                                    lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(company.CompanyName);
                            }

                            StringBuilder fileSheetBuilder = new StringBuilder();
                            lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(File.FileNUM.ToString());
                            int LastCount = 0;

                            if (File.CQMains != null)
                            {
                                Int64 QuoteNumInProgress = 1;
                                if (File.QuoteNumInProgress != null && File.QuoteNumInProgress > 0)
                                    QuoteNumInProgress = File.QuoteNumInProgress;

                                CharterQuoteService.CQMain QuoteInProgress = File.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
                                if (QuoteInProgress != null)
                                {
                                    Mainid = QuoteInProgress.CQMainID;
                                    List<CQLeg> charlegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                                    if (charlegs != null && charlegs.Count > 0)
                                    {
                                        LastCount = charlegs.Count;
                                        for (int legCnt = 0; legCnt < charlegs.Count; legCnt++)
                                        {                                            
                                            DateTime FirstLegDepartUTCDate = (DateTime)charlegs[0].DepartureGreenwichDTTM; //Minimum Departure Date
                                            DateTime LastLegArrivalUTCDate = (DateTime)charlegs[LastCount - 1].ArrivalGreenwichDTTM; // Max Arrival Date
                                            lbTripDates.Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", FirstLegDepartUTCDate) + " " + "-" + " " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LastLegArrivalUTCDate));
                                        }
                                    }

                                    if (QuoteInProgress.Fleet != null)
                                        lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Fleet.TailNum);
                                    else
                                        lbTailNumber.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    if (QuoteInProgress.Aircraft != null)
                                        lbType.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Aircraft.AircraftCD);
                                    else
                                        lbType.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                                    if (QuoteInProgress.Passenger != null)
                                    {
                                        // lbRequestor.Text = Trip.Passenger.PassengerRequestorCD;
                                        lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Passenger.FirstName + QuoteInProgress.Passenger.MiddleInitial + QuoteInProgress.Passenger.LastName);
                                        lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(QuoteInProgress.Passenger.PhoneNum);
                                    }
                                    else
                                    {
                                        lbRequestor.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                        lbContactPhone.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                    }
                                }
                            }
                            lbdescription.Text = System.Web.HttpUtility.HtmlEncode(File.CQFileDescription);
                        }

                        if (Mainid != 0)
                        {
                            TableReportTitle.Visible = true;
                            this.divHistory.Visible = true;
                            //tbTripsheetHistory.Visible = true; 
                            lblReportName.Text = System.Web.HttpUtility.HtmlEncode("File HISTORY");
                            lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                            CharterQuoteServiceClient objCharterServiceClient = new CharterQuoteServiceClient();
                            var ObjRetval = objCharterServiceClient.GetHistory(Mainid);
                            if (ObjRetval.ReturnFlag)
                            {
                                var historyList = ObjRetval.EntityList;
                                StringBuilder historybuilder = new StringBuilder();

                                foreach (CQHistory tripHistory in historyList)
                                {
                                    historybuilder.Append("Last Updated user");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    historybuilder.Append(tripHistory.LastUpdUID.Trim());
                                    historybuilder.Append("<br>");

                                    historybuilder.Append("Last Updated Time");
                                    historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    DateTime LastModifiedUTCDate = DateTime.Now;
                                    if (UserPrincipal.Identity._airportId != null)
                                    {
                                        using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                                        {
                                            LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)tripHistory.LastUpdTS, false, false);
                                        }
                                    }
                                    else
                                        LastModifiedUTCDate = (DateTime)tripHistory.LastUpdTS;
                                    historybuilder.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", LastModifiedUTCDate));
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    historybuilder.Append("&nbsp");
                                    //historybuilder.Append("Revision No.");
                                    //historybuilder.Append(":");
                                    historybuilder.Append(" ");
                                    //historybuilder.Append(tripHistory.RevisionNumber);
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    historybuilder.Append(tripHistory.LogisticsHistory.Replace(Environment.NewLine, "<br>"));
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    historybuilder.Append("<br>");
                                    ////divHistory.InnerHtml = System.Web.HttpUtility.HtmlEncode(historybuilder.ToString());
                                    //HtmlString a = new HtmlString(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));
                                    //divHistory.InnerHtml = a.ToString(); // System.Web.HttpUtility.HtmlEncode(historybuilder.ToString());
                                    //// tbTripHistory.Text = historybuilder.ToString();
                                    divHistory.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode(historybuilder.ToString()));
                                }
                            }
                            else
                            {
                                //tbTripHistory.Text = string.Empty;
                            }
                        }
                    }

                    #endregion

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }

        /// <summary>
        /// Get Aircraft Code by Tail Number
        /// </summary>
        /// <param name="TailNum">Pass Tail Number</param>
        /// <returns>Returns Aircraft Code</returns>
        public static string LoadAircraftCode(String TailNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TailNum))
            {
                string AircraftCD = string.Empty;
                using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var TailVal = TailService.GetFleetProfileList().EntityList.Where(x => x.TailNum.ToString().ToUpper().Trim().Equals(TailNum.ToUpper().Trim())).ToList();
                    if (TailVal != null && TailVal.Count > 0)
                    {
                        var AircraftVal = TailService.GetAircraftByAircraftID((long)((FlightPakMasterService.Fleet)TailVal[0]).AircraftID).EntityList;
                        if (AircraftVal != null && AircraftVal.Count > 0)
                            AircraftCD = ((FlightPakMasterService.Aircraft)AircraftVal[0]).AircraftCD.ToUpper();
                    }
                    return AircraftCD;
                }
            }
        }
    }
}

