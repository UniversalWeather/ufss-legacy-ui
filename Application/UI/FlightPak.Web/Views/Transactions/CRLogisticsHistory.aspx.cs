﻿#region Namespaces
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common;
using FlightPak.Web.CorporateRequestService;
using System.Text;
using System.Globalization;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.CalculationService;
using FlightPak.Web.PreflightService;
#endregion

namespace FlightPak.Web.Views.Transactions
{
    public partial class CRLogisticsHistory : BaseSecuredPage
    {
        private ExceptionManager exManager;
        public CRMain CorpRequest = new CRMain();


        #region Company Details
        private FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetCompanyWithFilters Companymaster = new GetCompanyWithFilters();
                    var objCompany = objDstsvc.GetCompanyWithFilters(string.Empty,HomeBaseID,true,true);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters> Company = objCompany.EntityList; 
                        //(from Comp in objCompany.EntityList where Comp.HomebaseID == HomeBaseID select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            #region This must be visible only for Preflight
            TableLogisticHistory.Visible = false;
            TableReportTitle.Visible = false;
            TableTripInfo.Visible = false;
            this.divHistory.Visible = false;

            #endregion
            lbApproverRes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbRequestNo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbTripStatus.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
            lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Int64 CRMainId = 0;
                    Int64 CustomerId = 0;
                    if (Session["CurrentCorporateRequest"] != null)
                    {
                        TableLogisticHistory.Visible = true;

                        TableTripInfo.Visible = true;
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        CRMainId = CorpRequest.CRMainID;
                        CustomerId = Convert.ToInt64(CorpRequest.CustomerID);

                        if (CorpRequest.HomebaseID != 0 || CorpRequest.HomebaseID != null)
                        {
                            GetCompanyWithFilters company = new GetCompanyWithFilters();
                            company = GetCompany((long)CorpRequest.HomebaseID);
                            if (company != null)
                                lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(company.CompanyName);
                        }

                        StringBuilder tripSheetBuilder = new StringBuilder();
                        lbRequestNo.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.CRTripNUM.ToString());
                        lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.TripID.ToString());
                        if (!string.IsNullOrEmpty(CorpRequest.CRMainDescription))
                        {
                            lbDesc.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.CRMainDescription.ToString());
                        }
                        using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                        {
                            var objRetValCorp = CorpSvc.GetRequest(CorpRequest.CRMainID);
                            if (objRetValCorp != null && objRetValCorp.ReturnFlag)
                            {
                                CorpRequest = objRetValCorp.EntityList[0];
                            }
                        }
                        if (CorpRequest.TripID != null)
                        {
                            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                            {
                                var objRetVal = PrefSvc.GetTrip((long)CorpRequest.TripID);
                                if (objRetVal != null && objRetVal.ReturnFlag)
                                {
                                    lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(objRetVal.EntityList[0].TripNUM.ToString());
                                    lbTripStatus.Text = System.Web.HttpUtility.HtmlEncode(objRetVal.EntityList[0].TripStatus.ToString());
                                    if (lbTripStatus.Text == "W")
                                        lbTripStatus.Text = System.Web.HttpUtility.HtmlEncode("Worksheet");
                                    else if (lbTripStatus.Text == "C")
                                        lbTripStatus.Text = System.Web.HttpUtility.HtmlEncode("Canceled");
                                    else if (lbTripStatus.Text == "T")
                                        lbTripStatus.Text = System.Web.HttpUtility.HtmlEncode("Tripsheet");
                                    else if (lbTripStatus.Text == "H")
                                        lbTripStatus.Text = System.Web.HttpUtility.HtmlEncode("Hold");
                                }
                                else
                                {
                                    lbTripNo.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                }
                            }
                        }

                        if (CorpRequest.Approver != null)
                            lbApproverRes.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Approver.ToString());
                        else
                            lbApproverRes.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                    }

                    if (CRMainId != 0)
                    {
                        TableReportTitle.Visible = true;

                        this.divHistory.Visible = true;
                        lblReportName.Text = System.Web.HttpUtility.HtmlEncode("Logistics History");
                        lblCompanyName.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        CorporateRequestServiceClient objCorpClient = new CorporateRequestServiceClient();
                        var ObjRetval = objCorpClient.GetCRHistory(CustomerId, CRMainId);
                        if (ObjRetval != null && ObjRetval.ReturnFlag)
                        {
                            var historyList = ObjRetval.EntityList;
                            StringBuilder historybuilder = new StringBuilder();
                            foreach (CRHistory tripHistory in historyList)
                            {
                                historybuilder.Append("User");
                                historybuilder.Append(":");
                                historybuilder.Append(" ");
                                historybuilder.Append(tripHistory.LastUpdUID.Trim());

                                historybuilder.Append(" ");
                                historybuilder.Append("On");
                                historybuilder.Append(":");
                                historybuilder.Append(" ");
                                DateTime LastModifiedUTCDate = DateTime.Now;
                                if (UserPrincipal.Identity._airportId != null)
                                {
                                    using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                                    {
                                        LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)tripHistory.LastUpdTS, false, false);
                                    }
                                }
                                else
                                    LastModifiedUTCDate = (DateTime)tripHistory.LastUpdTS;
                                historybuilder.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm}", LastModifiedUTCDate));

                                historybuilder.Append("<br>");
                                historybuilder.Append("<br>");
                                historybuilder.Append(tripHistory.LogisticsHistory.Replace(Environment.NewLine, "<br>"));
                                historybuilder.Append("<br>");
                                historybuilder.Append("<br>");
                                historybuilder.Append("<br>");
                                divHistory.InnerHtml = System.Web.HttpUtility.HtmlDecode(System.Web.HttpUtility.HtmlEncode( historybuilder.ToString()));
                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
        }


    }
}

