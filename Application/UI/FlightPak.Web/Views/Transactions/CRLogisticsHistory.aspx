﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CRLogisticsHistory.aspx.cs"
    Inherits="FlightPak.Web.Views.Transactions.CRLogisticsHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
             
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
        <table width="100%" class="box1">
            <tr>
                <td>
                    <table width="800px" id="TableReportTitle" runat="server">
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                            </td>
                            <td align="right" class="tdLabel40">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblReportName" runat="server"></asp:Label>
                            </td>
                            <td align="right" class="tdLabel40">
                                <a href="#" class="print-icon" onclick="window.print();"></a>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" class="border-box" id="TableTripInfo" runat="server">
                        <tr>
                            <td class="tdLabel100 mnd_text" valign="top">
                                <asp:Label ID="lbRequest" runat="server" Text="Request No."></asp:Label>
                            </td>
                            <td class="tdLabel150" valign="top">
                                <asp:Label ID="lbRequestNo" runat="server" CssClass="text50" MaxLength="10"></asp:Label>
                            </td>
                            <td class="tdLabel100 mnd_text" valign="top">
                                <asp:Label ID="lbApprover" runat="server" Text="Approver:"></asp:Label>
                            </td>
                            <td class="tdLabel150" valign="top">
                                <asp:Label ID="lbApproverRes" runat="server" CssClass="text50" MaxLength="10"></asp:Label>
                            </td>
                            <td class="tdLabel100 mnd_text" valign="top">
                                <asp:Label ID="lbTrip" runat="server" Text="Trip No."></asp:Label>
                            </td>
                            <td class="tdLabel150" valign="top">
                                <asp:Label ID="lbTripNo" runat="server" CssClass="text50" MaxLength="10"></asp:Label>
                            </td>
                            <td class="tdLabel100 mnd_text" valign="top">
                                <asp:Label ID="lbStatus" runat="server" Text="Trip Status:"></asp:Label>
                            </td>
                            <td class="tdLabel150" valign="top">
                                <asp:Label ID="lbTripStatus" runat="server" CssClass="text50" MaxLength="10"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdLabel100 mnd_text" valign="top">
                                <asp:Label ID="lbDescription" runat="server" Text="Description:"></asp:Label>
                            </td>
                            <td class="tdLabel150" valign="top">
                                <asp:Label ID="lbDesc" runat="server" CssClass="text50"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table id="TableLogisticHistory" width="600px" runat="server">
                        <tr>
                            <td align="left" class="mnd_text" valign="top">
                                Logistics History Information
                            </td>
                        </tr>
                    </table>
                    <table width="100%" class="border-box">
                        <tr>
                            <td class="note-box" valign="top">
                                <%--<asp:TextBox ID="tbTripHistory" runat="server" TextMode="MultiLine" CssClass="textarea-history"
                                    Enabled="true" ReadOnly="true">
                                </asp:TextBox>--%>
                                <div class="history_div" runat="server" id="divHistory">
                                </div>
                                <asp:HiddenField runat="server" ID="hdnTripID" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
