﻿using System.EnterpriseServices.Internal;
using System.Web.Script.Serialization;
using FlightPak.Web.BusinessLite.Preflight;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.PreflightService;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using FlightPak.Web.Framework.Helpers.Validators;
using System.Web;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.MasterData;
using Telerik.Web.UI;
using FlightPak.Web.Framework.Error;
using FlightPak.Web.ViewModels.Postflight;

namespace FlightPak.Web.Views.Transactions
{
    public partial class CommonValidator : BaseSecuredPage
    {
        #region VARIABLE_DECLARATION
        #endregion

        #region PAGE_EVENT
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<object> Client_Validate_Retrieve(string ClientCD)
        {
            FSSOperationResult<object> result = new FSSOperationResult<object>();
            if (!result.IsAuthorized())
                return result;

            bindHelper retObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetClientWithFilters(0, ClientCD.ToUpper().Trim(), false);
                if (objRetVal.ReturnFlag)
                {
                    ClientList = objRetVal.EntityList;
                    if (ClientList != null && ClientList.Count > 0)
                    {
                        retObj.flag = "1";
                        retObj.txtValue = System.Web.HttpUtility.HtmlEncode(ClientList[0].ClientCD);
                        retObj.lblValue = System.Web.HttpUtility.HtmlEncode(ClientList[0].ClientDescription);
                        retObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(ClientList[0].ClientID));
                    }
                    else
                    {
                        retObj.flag = "0";
                    }
                }
                else
                {
                    retObj.flag = "0";
                }
            }
            result.Result = retObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<ClientViewModel> Client_Validate_Retrieve_VM(string ClientCD)
        {
            FSSOperationResult<ClientViewModel> result = new FSSOperationResult<ClientViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetClientInfo_FromClientCD(ClientCD);
            result.Success = result.Result != null && result.Result.ClientID != 0;
            if (result.Success == false)
                result.ErrorsList.Add(WebErrorConstants.ClientCodeNotExist);
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> TailNo_Validate_Retrieve(string TailNum)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;

            List<FlightPak.Web.FlightPakMasterService.GetAllFleetWithFilters> Fleetlist = new List<FlightPakMasterService.GetAllFleetWithFilters>();
            List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = new List<FlightPakMasterService.Aircraft>();
            List<FlightPak.Web.FlightPakMasterService.EmergencyContact> EmergencyContactList = new List<FlightPakMasterService.EmergencyContact>();
            List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();
            Hashtable otherValue = new Hashtable();
            PreflightTripManagerLite managerlite = new PreflightTripManagerLite();

            Hashtable FleetHash = new Hashtable();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, TailNum.ToUpper().Trim(), 0);
                if (objRetVal.ReturnFlag)
                {
                    Fleetlist = objRetVal.EntityList;
                    bindHelper FleetRetObj = new bindHelper();
                    if (Fleetlist != null && Fleetlist.Count > 0)
                    {
                        FleetRetObj.Other=new Hashtable();
                        FleetRetObj.flag = "1";
                        FleetRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(Fleetlist[0].TailNum);
                        FleetRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(Fleetlist[0].FleetID));
                        FleetRetObj.Other["MaxPax"] = System.Web.HttpUtility.HtmlEncode(Convert.ToString(Fleetlist[0].MaximumPassenger));
                        FleetHash.Add("TailNum", FleetRetObj);

                        bindHelper TypeRetObj = new bindHelper();
                        var objRetVal1 = objDstsvc.GetAircraftByAircraftID(Convert.ToInt64(Fleetlist[0].AircraftID));
                        AircraftList = objRetVal1.EntityList;
                        if (AircraftList != null && AircraftList.Count > 0)
                        {
                            TypeRetObj.flag = "1";
                            TypeRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftCD);
                            TypeRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription);
                            TypeRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(AircraftList[0].AircraftID));
                        }
                        FleetHash.Add("Type", TypeRetObj);

                        bindHelper EmergencyRetObj = new bindHelper();
                        var objRetVal2 = objDstsvc.GetEmergencyList();
                        EmergencyContactList = objRetVal2.EntityList.Where(x => x.EmergencyContactID == Fleetlist[0].EmergencyContactID).ToList();
                        if (EmergencyContactList != null && EmergencyContactList.Count > 0)
                        {
                            EmergencyRetObj.flag = "1";
                            EmergencyRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(EmergencyContactList[0].EmergencyContactID));
                            EmergencyRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(EmergencyContactList[0].FirstName);
                            EmergencyRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(EmergencyContactList[0].EmergencyContactCD);
                        }
                        FleetHash.Add("EmergencyContact", EmergencyRetObj);

                        bindHelper HomebaseRetObj = new bindHelper();
                        if (Fleetlist[0].HomebaseID != null)
                        {
                            var objRetVal3 = objDstsvc.GetCompanyWithFilters(string.Empty, (long)Fleetlist[0].HomebaseID, true, true);
                            Int64 HomebaseID = (long)Fleetlist[0].HomebaseID;
                            CompanyList = objRetVal3.EntityList.Where(x => x.HomebaseID == HomebaseID).ToList();
                            if (CompanyList != null && CompanyList.Count > 0)
                            {
                                HomebaseRetObj.flag = "1";
                                HomebaseRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CompanyList[0].HomebaseID));
                                HomebaseRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(CompanyList[0].HomebaseCD);
                                HomebaseRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);
                                otherValue["HomebaseAirportID"] = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CompanyList[0].HomebaseAirportID));
                                HomebaseRetObj.Other = otherValue;
                                if(HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                                {
                                    PreflightTripViewModel pfViewModel = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];                        
                                    if (pfViewModel.PreflightLegs != null && pfViewModel.PreflightLegs.Count > 0 && pfViewModel.PreflightLegs[0].DepartureAirport.AirportID != 0 && pfViewModel.PreflightLegs[0].ArrivalAirport.AirportID == 0)
                                    {
                                        pfViewModel.PreflightLegs[0].DepartureAirport = managerlite.getAirportByIcaoId(CompanyList[0].HomebaseCD);
                                        HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                                    }
                                }
                            }
                        }
                        FleetHash.Add("HomeBase", HomebaseRetObj);
                    }
                    else
                    {
                        FleetRetObj.flag = "0";
                        FleetHash.Add("TailNum", FleetRetObj);
                    }
                }
            }
            result.Result = FleetHash;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> AircraftType_Validate_Retrieve(string TypeCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;

            bindHelper retObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = new List<FlightPakMasterService.Aircraft>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAircraftWithFilters(TypeCD.ToUpper(), 0, false);
                if (objRetVal.ReturnFlag)
                {
                    AircraftList = objRetVal.EntityList;
                    if (AircraftList != null && AircraftList.Count > 0)
                    {
                        retObj.flag = "1";
                        retObj.txtValue = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftCD);
                        retObj.lblValue = System.Web.HttpUtility.HtmlEncode(AircraftList[0].AircraftDescription);
                        retObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(AircraftList[0].AircraftID));
                    }
                    else
                    {
                        retObj.flag = "0";
                    }
                }
                else
                {
                    retObj.flag = "0";
                }
            }
            result.Result = retObj;
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> EmergencyContact_Validate_Retrieve(string ContactCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper retObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.EmergencyContact> EmergencyContactList = new List<FlightPakMasterService.EmergencyContact>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllEmergencyContactWithFilters(0, ContactCD.ToUpper().Trim(), false);
                if (objRetVal.ReturnFlag)
                {
                    EmergencyContactList = objRetVal.EntityList;
                    if (EmergencyContactList != null && EmergencyContactList.Count > 0)
                    {
                        retObj.flag = "1";
                        retObj.txtValue = System.Web.HttpUtility.HtmlEncode(EmergencyContactList[0].EmergencyContactCD);
                        retObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(EmergencyContactList[0].EmergencyContactID));
                        retObj.lblValue = System.Web.HttpUtility.HtmlEncode(string.Format("{0}, {1}  {2}", EmergencyContactList[0].LastName, EmergencyContactList[0].FirstName, EmergencyContactList[0].MiddleName));
                    }
                    else
                    {
                        retObj.flag = "0";
                    }
                }
                else
                {
                    retObj.flag = "0";
                }
            }
            result.Result = retObj;
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> Homebase_Validate_Retrieve(string BaseCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper retObj = new bindHelper();
            Hashtable otherValue = new Hashtable();
            List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetCompanyWithFilters(BaseCD.ToUpper().Trim(), 0, true, false);
                if (objRetVal.ReturnFlag)
                {
                    CompanyList = objRetVal.EntityList;
                    if (CompanyList != null && CompanyList.Count > 0)
                    {
                        retObj.flag = "1";
                        retObj.txtValue = System.Web.HttpUtility.HtmlEncode(CompanyList[0].HomebaseCD);
                        retObj.lblValue = System.Web.HttpUtility.HtmlEncode(CompanyList[0].BaseDescription);
                        retObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CompanyList[0].HomebaseID));
                        otherValue["HomebaseAirportID"] = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CompanyList[0].HomebaseAirportID));
                        if (HttpContext.Current.Session["CurrentPreFlightTrip"] != null)
                        {
                            var Trip = (PreflightMain)HttpContext.Current.Session["CurrentPreFlightTrip"];
                            otherValue["LegCnt"] = Trip.PreflightLegs!=null ?Trip.PreflightLegs.Count:0;
                        }
                        else {
                            otherValue["LegCnt"] = 0;
                        }
                        retObj.Other = otherValue;
                    }
                    else
                    {
                        retObj.flag = "0";
                    }
                }
                else
                {
                    retObj.flag = "0";
                }
            }
            result.Result = retObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<AircraftViewModel> AircraftType_Validate_Retrieve_VM(string aircraftCD)
        {
            FSSOperationResult<AircraftViewModel> result = new FSSOperationResult<AircraftViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetAircraftInfo_FromAircraftCD(aircraftCD);
            result.Success = result.Result != null && !String.IsNullOrWhiteSpace(result.Result.AircraftCD);
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<CrewDutyTypeViewModel> CrewDutyType_Validate_Retrieve_VM(string dutyTypeCD)
        {
            FSSOperationResult<CrewDutyTypeViewModel> result = new FSSOperationResult<CrewDutyTypeViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetCrewDutyTypeInfo_FromDutyTypeCD(dutyTypeCD);
            result.Success = result.Result != null && !String.IsNullOrWhiteSpace(result.Result.DutyTypeCD);
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<HomebaseViewModel> Homebase_Validate_Retrieve_VM(string BaseCD)
        {
            FSSOperationResult<HomebaseViewModel> result = new FSSOperationResult<HomebaseViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if(userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetHomebaseInfo_FromHomebaseCD(BaseCD);
            result.Success = result.Result != null && result.Result.HomebaseID>0 ? true:false;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<AirportViewModel> Airport_Validate_Retrieve_VM(string ICAO)
        {
            FSSOperationResult<AirportViewModel> result = new FSSOperationResult<AirportViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetAirportInfo_FromICAO(ICAO);
            result.Success = result.Result != null && result.Result.AirportID !=0;
            if (result.Success == false)
                result.ErrorsList.Add(WebErrorConstants.AirportCodeNotExist);
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> CrewCode_Validate_Retrieve(string CrewCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper retObj = new bindHelper();

            List<FlightPak.Web.FlightPakMasterService.GetAllCrewWithFilters> CrewList = new List<FlightPakMasterService.GetAllCrewWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllCrewWithFilters(0, false, false, false, string.Empty, 0, 0, CrewCD.ToUpper().Trim());
                if (objRetVal.ReturnFlag)
                {
                    CrewList = objRetVal.EntityList;
                    if (CrewList != null && CrewList.Count > 0)
                    {
                        retObj.flag = "1";
                        retObj.txtValue = System.Web.HttpUtility.HtmlEncode(CrewList[0].CrewCD);

                        retObj.lblValue = System.Web.HttpUtility.HtmlEncode(CrewList[0].LastName) + (CrewList[0].FirstName != null ? "," + CrewList[0].FirstName : " ") + " "+System.Web.HttpUtility.HtmlEncode(CrewList[0].MiddleInitial);

                        retObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(CrewList[0].CrewID));
                    }
                    else
                    {
                        retObj.flag = "0";
                    }
                }
                else
                {
                    retObj.flag = "0";
                }
            }
            result.Result = retObj;
            return result;
        }

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> Requestor_Validate_Retrieve(string ReqCD)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable ReqHash = new Hashtable();
            long DeptId=0;
            long DeptAuthId=0;

            bindHelper ReqRetObj = new bindHelper();
            bindHelper DeptRetObj = new bindHelper();
            bindHelper AuthRetObj = new bindHelper();
            UserPrincipalViewModel UserPrincipal = TripManagerBase.getUserPrincipal();
            string convPhoneNumber = string.Empty;
            List<FlightPak.Web.FlightPakMasterService.GetAllPassengerWithFilters> PassengerList = new List<FlightPakMasterService.GetAllPassengerWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllPassengerWithFilters(0, 0, 0, ReqCD.ToUpper().Trim(), false, false, string.Empty, 0);

                if (objRetVal.ReturnFlag)
                {
                    PassengerList = objRetVal.EntityList;
                    if (PassengerList != null && PassengerList.Count > 0)
                    {
                        ReqRetObj.flag = "1";
                        ReqRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(PassengerList[0].PassengerRequestorCD);
                        ReqRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(PassengerList[0].PassengerName);
                        ReqRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(PassengerList[0].PassengerRequestorID));
                        if (!string.IsNullOrEmpty(PassengerList[0].AdditionalPhoneNum))
                        {
                            convPhoneNumber = PassengerList[0].AdditionalPhoneNum.Trim();
                        }
                        else if (!string.IsNullOrEmpty(PassengerList[0].PrimaryMobile))
                        {
                            convPhoneNumber = PassengerList[0].PrimaryMobile.Trim();
                        }
                        if (PassengerList[0].DepartmentID != null && UserPrincipal._IsDepartAuthDuringReqSelection)
                        {
                            DeptId = Convert.ToInt64(PassengerList[0].DepartmentID);
                            DeptAuthId = Convert.ToInt64(PassengerList[0].AuthorizationID);
                            if (DeptId != 0)
                            {
                                DeptRetObj = Department_Validate_Retrieve(DeptId: DeptId).Result;
                            }
                            if (DeptAuthId != 0)
                            {
                                AuthRetObj = Authorization_Validate_Retrieve(AuthID: DeptAuthId, DeptID: DeptId).Result;
                            }
                        }
                        else
                        {
                            DeptRetObj.flag = "0";
                            AuthRetObj.flag = "0";
                        }
                    }
                    else
                    {
                        ReqRetObj.flag = "0";
                        DeptRetObj.flag = "0";
                        AuthRetObj.flag = "0";
                    }
                }
                else
                {
                    ReqRetObj.flag = "0";
                    DeptRetObj.flag = "0";
                    AuthRetObj.flag = "0";
                }
            }
            ReqHash.Add("Reqestor", ReqRetObj);
            ReqHash.Add("ReqPhoneNumber", System.Web.HttpUtility.HtmlEncode(convPhoneNumber));
            ReqHash.Add("Department", DeptRetObj);
            ReqHash.Add("Authorization", AuthRetObj);
            result.Result = ReqHash;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> Department_Validate_Retrieve(string DeptCD = null, long DeptId = 0)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper DeptRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.GetDepartmentByWithFilters> DeptAuthList = new List<FlightPakMasterService.GetDepartmentByWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = DeptCD == null ? objDstsvc.GetDepartmentByWithFilters(string.Empty, DeptId, 0, false) : objDstsvc.GetDepartmentByWithFilters(DeptCD.ToUpper().Trim(), 0, 0, false);
                if (objRetVal.ReturnFlag)
                {
                    DeptAuthList = objRetVal.EntityList;
                    if (DeptAuthList != null && DeptAuthList.Count > 0)
                    {
                        DeptRetObj.flag = "1";
                        DeptRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(DeptAuthList[0].DepartmentCD);
                        DeptRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(DeptAuthList[0].DepartmentName);
                        DeptRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(DeptAuthList[0].DepartmentID));
                    }
                    else
                    {
                        DeptRetObj.flag = "0";
                    }
                }
                else
                {
                    DeptRetObj.flag = "0";
                }
            }
            result.Result = DeptRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<DepartmentViewModel> Department_Validate_Retrieve_KO(string departmentCd, long? departmentId)
        {
            FSSOperationResult<DepartmentViewModel> result = new FSSOperationResult<DepartmentViewModel>();
            if (result.IsAuthorized() == false)
            {
                return result;
            }            
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetDepartmentByIDorCD(departmentId??0,departmentCd);
            if (result.Result.DepartmentID > 0)
                result.Success = true;
            else
                result.Success = false;

            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> Authorization_Validate_Retrieve(string AuthCD = null, long AuthID = 0, long DeptID = 0)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper AuthRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.DepartmentAuthorization> DeptAuthtList = new List<FlightPakMasterService.DepartmentAuthorization>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = AuthID == 0 ? objDstsvc.GetDepartmentAuthorizationWithFilters(0, DeptID, 0, AuthCD.ToUpper().Trim(), false) : objDstsvc.GetDepartmentAuthorizationWithFilters(0, DeptID, AuthID, string.Empty, false);
                if (objRetVal.ReturnFlag)
                {
                    DeptAuthtList = objRetVal.EntityList;
                    if (DeptAuthtList != null && DeptAuthtList.Count > 0)
                    {
                        AuthRetObj.flag = "1";
                        AuthRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(DeptAuthtList[0].AuthorizationCD);
                        AuthRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(DeptAuthtList[0].DeptAuthDescription);
                        AuthRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(DeptAuthtList[0].AuthorizationID));
                    }
                    else
                    {
                        AuthRetObj.flag = "0";
                    }
                }
                else
                {
                    AuthRetObj.flag = "0";
                }
            }
            result.Result = AuthRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<DepartmentAuthorizationViewModel> Authorization_Validate_Retrieve_KO(string authorizationCd="", long? authorizationId=null, long departmentId=0)
        {
            FSSOperationResult<DepartmentAuthorizationViewModel> result = new FSSOperationResult<DepartmentAuthorizationViewModel>();
            if (result.IsAuthorized() == false)
            {
                return result;
            }
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetDepartmentAuthorizationByDepIDnAuthorizationCDorID(authorizationCd,authorizationId??0,departmentId);
            if (result.Result.AuthorizationID > 0 &&  string.Compare(result.Result.AuthorizationCD,authorizationCd,true)==0)
                result.Success = true;
            else
                result.Success = false;

            return result;
        }


        #region Account Number Methods
        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> Account_Validate_Retrieve(string AccountCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper accountRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.GetAccountWithFilters> AccountsList = new List<FlightPakMasterService.GetAccountWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAccountWithFilters(AccountCD.ToUpper().Trim(), 0, string.Empty, false);
                if (objRetVal.ReturnFlag)
                {
                    AccountsList = objRetVal.EntityList;
                    if (AccountsList != null && AccountsList.Count > 0)
                    {
                        accountRetObj.flag = "1";
                        accountRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(AccountsList[0].AccountNum);
                        accountRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(AccountsList[0].AccountDescription);
                        accountRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(AccountsList[0].AccountID));
                    }
                    else
                    {
                        accountRetObj.flag = "0";
                    }
                }
                else
                {
                    accountRetObj.flag = "0";
                }
            }
            result.Result = accountRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<AccountViewModel> Account_Validate_Retrieve_VM(string AccountNumber)
        {
            FSSOperationResult<AccountViewModel> result = new FSSOperationResult<AccountViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.Success = true;
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetAccountInfo_FromAccountNo(AccountNumber);
            result.Success = result.Result != null && result.Result.AccountID != 0;
            return result;
        }

        #endregion

        [WebMethod(EnableSession=true)]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> Dispatcher_Validate_Retrieve(string DispatcherCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper DispatcherRetObj = new bindHelper();
            List<FlightPak.Web.PreflightService.IndividualDispatcher> DispatcherList = new List<IndividualDispatcher>();
            using (PreflightService.PreflightServiceClient PreflightService = new PreflightService.PreflightServiceClient())
            {
                var objRetVal = PreflightService.GetDispatcherByUsername(DispatcherCD.ToUpper().Trim());
                if (objRetVal.ReturnFlag)
                {
                    DispatcherList = objRetVal.EntityList;
                    if (DispatcherList != null && DispatcherList.Count > 0)
                    {
                        DispatcherRetObj.flag = "1";
                        DispatcherRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(DispatcherList[0].UserName);
                        DispatcherRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(DispatcherList[0].LastName + " " + DispatcherList[0].MiddleName + " " + DispatcherList[0].FirstName);
                        DispatcherRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(DispatcherList[0].PhoneNum);
                    }
                    else
                    {
                        DispatcherRetObj.flag = "0";
                    }
                }
                else
                {
                    DispatcherRetObj.flag = "0";
                }
            }
            result.Result = DispatcherRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> HotelValidation(string AirportID, string HotelCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper HotelRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.GetHotelByHotelCode> HotelList = new List<FlightPak.Web.FlightPakMasterService.GetHotelByHotelCode>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                var objRetVal = objDstsvc.GetHotelByHotelCode(long.Parse(AirportID), HotelCD.ToUpper().Trim());
                if (objRetVal.ReturnFlag)
                {
                    HotelList = objRetVal.EntityList;
                    if (HotelList != null && HotelList.Count > 0)
                    {
                        HotelRetObj.flag = "1";
                        HotelRetObj.txtValue = HotelList[0].HotelCD;
                        HotelRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(HotelList[0].Name);
                        HotelRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(HotelList[0].HotelID);
                        HotelRetObj.Other = new Hashtable();
                        HotelRetObj.Other["Addr1"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].Addr1);
                        HotelRetObj.Other["Addr2"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].Addr2);
                        HotelRetObj.Other["Addr3"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].Addr3);
                        HotelRetObj.Other["NegociatedRate"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].NegociatedRate);
                        HotelRetObj.Other["PhoneNum"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].PhoneNum);
                        HotelRetObj.Other["FaxNum"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].FaxNum);
                        HotelRetObj.Other["CityName"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].CityName);
                        HotelRetObj.Other["StateName"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].StateName);
                        HotelRetObj.Other["MetroCD"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].MetroCD);
                        HotelRetObj.Other["MetroID"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].MetroID);
                        HotelRetObj.Other["CountryCD"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].CountryCD);
                        HotelRetObj.Other["CountryID"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].CountryID);
                        HotelRetObj.Other["PostalZipCD"] = System.Web.HttpUtility.HtmlEncode(HotelList[0].PostalZipCD);
                    }
                    else
                    {
                        HotelRetObj.flag = "0";
                    }
                }
                else
                {
                    HotelRetObj.flag = "0";
                }
            }
            result.Result = HotelRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> TransportValidation(string AirportID, String TransportCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper TransportRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.GetTransportByTransportCode> TransportList = new List<FlightPak.Web.FlightPakMasterService.GetTransportByTransportCode>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetTransportByTransportCode(long.Parse(AirportID), TransportCD.ToUpper().Trim());
                if (objRetVal.ReturnFlag)
                {
                    TransportList = objRetVal.EntityList;
                    if (TransportList != null && TransportList.Count > 0)
                    {
                        TransportRetObj.flag = "1";
                        TransportRetObj.txtValue = TransportList[0].TransportCD;
                        TransportRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(TransportList[0].TransportationVendor);
                        TransportRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(TransportList[0].TransportID);
                        TransportRetObj.Other = new Hashtable();
                        TransportRetObj.Other["NegotiatedRate"] = System.Web.HttpUtility.HtmlEncode(TransportList[0].NegotiatedRate);
                        TransportRetObj.Other["PhoneNum"] = System.Web.HttpUtility.HtmlEncode(TransportList[0].PhoneNum);
                        TransportRetObj.Other["FaxNum"] = System.Web.HttpUtility.HtmlEncode(TransportList[0].FaxNum);
                    }
                    else
                    {
                        TransportRetObj.flag = "0";
                    }
                }
                else
                {
                    TransportRetObj.flag = "0";
                }
                result.Result = TransportRetObj;
                return result;
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> MetroValidation(string MetroCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper MetroRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.Metro> MetroList = new List<FlightPak.Web.FlightPakMasterService.Metro>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetMetroWithFilters(0, MetroCD.ToUpper().Trim());
                if (objRetVal.ReturnFlag)
                {
                    MetroList = objRetVal.EntityList;
                    if (MetroList != null && MetroList.Count > 0)
                    {
                        MetroRetObj.flag = "1";
                        MetroRetObj.txtValue = MetroList[0].MetroCD;
                        MetroRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(MetroList[0].MetroName);
                        MetroRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(MetroList[0].MetroID);
                    }
                    else
                    {
                        MetroRetObj.flag = "0";
                    }
                }
                else
                {
                    MetroRetObj.flag = "0";
                }
            }
            result.Result = MetroRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> CountryValidation(string CountryCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper CountryRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.Country> CountryList = new List<FlightPak.Web.FlightPakMasterService.Country>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetCountryWithFilters(CountryCD.ToUpper().Trim(), 0);
                if (objRetVal.ReturnFlag)
                {
                    CountryList = objRetVal.EntityList;
                    if (CountryList != null && CountryList.Count > 0)
                    {
                        CountryRetObj.flag = "1";
                        CountryRetObj.txtValue = CountryList[0].CountryCD;
                        CountryRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(CountryList[0].CountryName);
                        CountryRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(CountryList[0].CountryID);
                    }
                    else
                    {
                        CountryRetObj.flag = "0";
                    }
                }
                else
                {
                    CountryRetObj.flag = "0";
                }
            }
            result.Result = CountryRetObj;
            return result;
        }

        #region FBO
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> FBOValidation(long AirportID, string FBOCD, string hdnLeg, Boolean isDeparture)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper FBORetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.GetFBOByAirportIDWithFilters> FBOList = new List<FlightPak.Web.FlightPakMasterService.GetFBOByAirportIDWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetFBOByAirportWithFilters(AirportID, 0, FBOCD, false, false, false);
                if (objRetVal.ReturnFlag)
                {
                    FBOList = objRetVal.EntityList;
                    if (FBOList != null && FBOList.Count > 0)
                    {
                        FBORetObj.flag = "1";
                        FBORetObj.txtValue = FBOList[0].FBOCD;
                        FBORetObj.lblValue = System.Web.HttpUtility.HtmlEncode(FBOList[0].FBOVendor);
                        FBORetObj.hdValue = System.Web.HttpUtility.HtmlEncode(FBOList[0].FBOID);
                        FBORetObj.Other = new Hashtable();
                        FBORetObj.Other["Addr1"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].Addr1);
                        FBORetObj.Other["Addr2"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].Addr2);
                        FBORetObj.Other["Addr3"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].Addr3);
                        FBORetObj.Other["PhoneNUM1"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].PhoneNUM1);
                        FBORetObj.Other["PhoneNUM2"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].PhoneNUM2);
                        FBORetObj.Other["FaxNum"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].FaxNum);
                        FBORetObj.Other["CityName"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].CityName);
                        FBORetObj.Other["StateName"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].StateName);
                        FBORetObj.Other["UNICOM"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].UNICOM);
                        FBORetObj.Other["ARINC"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].ARINC);
                        FBORetObj.Other["EmailAddress"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].EmailAddress);
                        FBORetObj.Other["Confirm"] = "false";
                        FBORetObj.Other["PostalZipCD"] = System.Web.HttpUtility.HtmlEncode(FBOList[0].PostalZipCD);

                        if (HttpContext.Current.Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip] != null)
                        {
                            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[Framework.Constants.WebSessionKeys.CurrentPreflightTrip];
                            if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count() > 0)
                            {
                                long currentLegnum = (long)Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].LegNUM;

                                if (currentLegnum != null)
                                {
                                    var ComparedLegnum = isDeparture ? currentLegnum - 1 : currentLegnum + 1;
                                    PreflightLegViewModel Comparedleg = Trip.PreflightLegs.FirstOrDefault(x => x.LegNUM == ComparedLegnum && x.IsDeleted == false);
                                    if (Comparedleg != null)
                                    {
                                        var DepatureFlag = Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].DepartICAOID == Comparedleg.ArriveICAOID;
                                        var ArriveFlag = Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].ArriveICAOID == Comparedleg.DepartICAOID;
                                        var CompareFlag = isDeparture ? DepatureFlag : ArriveFlag;

                                        if (CompareFlag && Trip.PreflightLegPreflightLogisticsList.ContainsKey(Convert.ToString(ComparedLegnum)))
                                        {
                                            var CompaLegsPreflightFBOList = Trip.PreflightLegPreflightLogisticsList[Comparedleg.LegNUM.ToString()];
                                            if (CompaLegsPreflightFBOList.PreflightArrFboListViewModel != null && CompaLegsPreflightFBOList.PreflightDepFboListViewModel!=null)
                                            {
                                                PreflightFBOListViewModel ComparedFBO = new PreflightFBOListViewModel();
                                                ComparedFBO = CompaLegsPreflightFBOList.PreflightDepFboListViewModel;
                                                if ((ComparedFBO == null || ComparedFBO.FBOCD == null) || (ComparedFBO != null && ComparedFBO.FBOCD != null && ComparedFBO.FBOCD != null && ComparedFBO.FBOCD != FBORetObj.txtValue))
                                                {
                                                    FBORetObj.Other["Confirm"] = "true";
                                                }
                                            }
                                            else
                                            {
                                                FBORetObj.Other["Confirm"] = "true";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        FBORetObj.flag = "0";
                    }
                }
                else
                {
                    FBORetObj.flag = "0";
                }
            }
            result.Result = FBORetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<FBOViewModel> FBO_Validate_Retrieve_VM(String FBO, long AirportId)
        {
            FSSOperationResult<FBOViewModel> result = new FSSOperationResult<FBOViewModel>();
            UserPrincipalViewModel userPrincipal = TripManagerBase.getUserPrincipal();
            if (userPrincipal == null)
            {
                result.Success = false;
                result.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                result.ErrorsList.Add(WebErrorConstants.UnauthorizdOperation);
                return result;
            }
            result.StatusCode = System.Net.HttpStatusCode.OK;
            result.Result = DBCatalogsDataLoader.GetFBOInfo_FromFBOCD(FBO, AirportId);
            result.Success = result.Result != null && result.Result.FBOID !=0 ;
            return result;

        }
        #endregion FBO

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> CateringValidation(string CateringCD, long AirportID)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper CateringRetObj = new bindHelper();
            List<FlightPak.Web.FlightPakMasterService.GetAllCateringByAirportIDWithFilters> CateringList = new List<FlightPak.Web.FlightPakMasterService.GetAllCateringByAirportIDWithFilters>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllCateringByAirportIDWithFilters(AirportID, 0, CateringCD.ToUpper().Trim(), false, false);

                if (objRetVal.ReturnFlag)
                {
                    CateringList = objRetVal.EntityList;
                    if (CateringList != null && CateringList.Count > 0)
                    {
                        CateringRetObj.flag = "1";
                        CateringRetObj.txtValue = CateringList[0].CateringCD;
                        CateringRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(CateringList[0].CateringVendor);
                        CateringRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(CateringList[0].CateringID);
                        CateringRetObj.Other = new Hashtable();
                        CateringRetObj.Other["NegotiatedRate"] = System.Web.HttpUtility.HtmlEncode(CateringList[0].NegotiatedRate);
                        CateringRetObj.Other["PhoneNum"] = System.Web.HttpUtility.HtmlEncode(CateringList[0].PhoneNum);
                        CateringRetObj.Other["FaxNum"] = System.Web.HttpUtility.HtmlEncode(CateringList[0].FaxNum);
                        CateringRetObj.Other["ContactName"] = System.Web.HttpUtility.HtmlEncode(CateringList[0].ContactName);
                    }
                    else
                    {
                        CateringRetObj.flag = "0";
                    }
                }
                else
                {
                    CateringRetObj.flag = "0";
                }
            }
            result.Result = CateringRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<bindHelper> CQCustomer_Validate_Retrieve(string CQCustomerCD)
        {
            FSSOperationResult<bindHelper> result = new FSSOperationResult<bindHelper>();
            if (!result.IsAuthorized())
                return result;
            bindHelper custRetObj = new bindHelper();
            FlightPak.Web.FlightPakMasterService.GetCQCustomerWithFilters objCQCustomer = new FlightPak.Web.FlightPakMasterService.GetCQCustomerWithFilters();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetCQCustomerWithFilters(0, CQCustomerCD.Trim(), string.Empty, false);
                if (objRetVal.ReturnFlag)
                {
                    objCQCustomer = objRetVal.EntityList.FirstOrDefault();
                    if (objCQCustomer != null)
                    {
                        custRetObj.flag = "1";
                        custRetObj.txtValue = System.Web.HttpUtility.HtmlEncode(objCQCustomer.CQCustomerCD);
                        custRetObj.lblValue = System.Web.HttpUtility.HtmlEncode(objCQCustomer.CQCustomerName);
                        custRetObj.hdValue = System.Web.HttpUtility.HtmlEncode(Convert.ToString(objCQCustomer.CQCustomerID));
                    }
                    else
                    {
                        custRetObj.flag = "0";
                    }
                }
                else
                {
                    custRetObj.flag = "0";
                }
            }
            result.Result = custRetObj;
            return result;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> AirportDetail_Validate_Retrieve(string AirportIcaoId)
        {
            FSSOperationResult<Hashtable> result = new FSSOperationResult<Hashtable>();
            if (!result.IsAuthorized())
                return result;
            Hashtable RetObj = new Hashtable();
            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAirportByAirportICaoID(AirportIcaoId.ToUpper().Trim());
                if (objRetVal.ReturnFlag) 
                {
                    AirportLists = objRetVal.EntityList;
                    if (AirportLists != null && AirportLists.Count > 0)
                    {
                        RetObj["flag"] = 0;
                        RetObj["AirportData"] = AirportLists[0];
                    } else
                    {
                        RetObj["flag"] = 1;
                    }
                } else
                {
                    RetObj["flag"] = 1;
                }
            }
            result.Result = RetObj;
            return result;
        }

    }
}