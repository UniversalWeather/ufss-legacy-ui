﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="FlightPak.Web.Views.Home"
    MasterPageFile="~/Framework/Masters/Site.Master" %>
<%@ Import Namespace="System.Web.Optimization" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    
    <script type="text/javascript">
        var moduleName = getQuerystring("m", "");
        if (!IsNullOrEmptyOrUndefined(moduleName)) {
            RestrictedModuleMessage(moduleName);
        }
    </script>
</asp:Content>
