﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CharterQuoteCustomerContactCatalogPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.CharterQuote.CharterQuoteCustomerContactCatalogPopUP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Contacts</title>
    <script language="javascript" src="../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
                var oArg = new Object();
                var grid = $find("<%= dgCqCustomerConatct.ClientID %>");

                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }

                function returnToParent() {
                    //create the argument that will be returned to the parent page
                    var cell1, cell2, cell3, cell4, cell5;
                    oArg = new Object();
                    grid = $find("<%= dgCqCustomerConatct.ClientID %>");
                    var MasterTable = grid.get_masterTableView();
                    var selectedRows = MasterTable.get_selectedItems();
                    var SelectedMain = false;
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        cell1 = MasterTable.getCellByColumnUniqueName(row, "CQCustomerContactCD");
                        cell2 = MasterTable.getCellByColumnUniqueName(row, "FirstName");
                        cell3 = MasterTable.getCellByColumnUniqueName(row, "BusinessPhoneNum");
                        cell4 = MasterTable.getCellByColumnUniqueName(row, "BusinessFaxNum");
                        cell5 = MasterTable.getCellByColumnUniqueName(row, "CQCustomerContactID");
                    }

                    if (selectedRows.length > 0) {
                        oArg.CQCustomerContactCD = cell1.innerHTML;
                        oArg.FirstName = cell2.innerHTML;
                        oArg.BusinessPhoneNum = cell3.innerHTML;
                        oArg.BusinessFaxNum = cell4.innerHTML;
                        oArg.CQCustomerContactID = cell5.innerHTML;

                    }
                    else {
                        oArg.CQCustomerContactCD = "";
                        oArg.FirstName = "";
                        oArg.BusinessPhoneNum = "";
                        oArg.BusinessFaxNum = "";
                        oArg.CQCustomerContactID = "";
                    }


                    var oWnd = GetRadWindow();
                    if (oArg) {
                        oWnd.close(oArg);
                    }
                }

                function Close() {
                    GetRadWindow().Close();
                }
                function prepareSearchInput(input) { input.value = input.value; }
            </script>
        </telerik:RadCodeBlock>
        <div class="divGridPanel">
            <asp:ScriptManager ID="scr1" runat="server">
            </asp:ScriptManager>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
                <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
                <AjaxSettings>
                    <telerik:AjaxSetting AjaxControlID="dgCqCustomerConatct">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="dgCqCustomerConatct" LoadingPanelID="RadAjaxLoadingPanel1" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                    <telerik:AjaxSetting AjaxControlID="clrFilters">
                        <UpdatedControls>
                            <telerik:AjaxUpdatedControl ControlID="dgCqCustomerConatct" LoadingPanelID="RadAjaxLoadingPanel1" />
                            <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                        </UpdatedControls>
                    </telerik:AjaxSetting>
                </AjaxSettings>
            </telerik:RadAjaxManager>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
            <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
            <telerik:RadGrid ID="dgCqCustomerConatct" runat="server" AllowMultiRowSelection="true"
                AllowSorting="true" AutoGenerateColumns="false" Height="341px" PageSize="10"
                AllowPaging="true" Width="650px" AllowFilteringByColumn="true" OnNeedDataSource="dgCqCustomerConatct_BindData"
                OnItemCommand="dgCqCustomerConatct_ItemCommand" OnPreRender="dgCqCustomerConatct_PreRender">
                <MasterTableView CommandItemDisplay="Bottom">
                    <Columns>
                        <telerik:GridBoundColumn DataField="CQCustomerContactCD" UniqueName="CQCustomerContactCD" HeaderText="Code"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500" FilterControlWidth="60%" HeaderStyle-Width="10%" ItemStyle-Width="10%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500" FilterControlWidth="80%" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="StartsWith"
                            FilterDelay="500" FilterControlWidth="80%" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsChoice" HeaderText="Choice" CurrentFilterFunction="EqualTo"
                            ShowFilterIcon="false" AutoPostBackOnFilter="true" FilterControlWidth="10%" HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridBoundColumn DataField="BusinessPhoneNum" UniqueName="BusinessPhoneNum" HeaderText="Phone" CurrentFilterFunction="StartsWith"
                            ShowFilterIcon="false" AutoPostBackOnFilter="false" FilterDelay="500" FilterControlWidth="80%" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BusinessFaxNum" UniqueName="BusinessFaxNum" HeaderText="Fax" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" FilterDelay="500" FilterControlWidth="80%" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CQCustomerContactID" UniqueName="CQCustomerContactID" HeaderText="CQCustomerContactID"
                            AutoPostBackOnFilter="false" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"
                            Display="false" FilterDelay="500">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CQCustomerID" HeaderText="CQCustomerID" AutoPostBackOnFilter="false"
                            ShowFilterIcon="false" CurrentFilterFunction="StartsWith" Display="false" FilterDelay="500">
                        </telerik:GridBoundColumn>

                    </Columns>
                    <CommandItemTemplate>
                        <div style="padding: 5px 5px; text-align: right;">
                            <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                                Ok</button>
                            <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                        </div>
                    </CommandItemTemplate>
                </MasterTableView>
                <ClientSettings>
                    <ClientEvents OnRowDblClick="returnToParent" />
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <Selecting AllowRowSelect="true" />
                </ClientSettings>
                <GroupingSettings CaseSensitive="false" />
            </telerik:RadGrid>
        </div>
    </form>
</body>
</html>
