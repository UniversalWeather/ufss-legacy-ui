﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeadSourcePopup.aspx.cs"
    Inherits="FlightPak.Web.Views.CharterQuote.LeadSourcePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lead Source</title>
    <script language="javascript" src="../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgLeadSource.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgLeadSource.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;

                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length; i++) {
                        var row = selectedRows[i];
                        var cell1 = MasterTable.getCellByColumnUniqueName(row, "LeadSourceCD");
                        var cell2 = MasterTable.getCellByColumnUniqueName(row, "LeadSourceDescription");
                        var cell3 = MasterTable.getCellByColumnUniqueName(row, "LeadSourceID");
                        if (i == 0) {
                            oArg.Code = cell1.innerHTML + ",";
                            oArg.Description = cell2.innerHTML;
                            oArg.LeadSourceID = cell3.innerHTML;
                           
                        }
                        else {

                            oArg.Code += cell1.innerHTML + ",";
                            oArg.Description += cell2.innerHTML;
                            oArg.LeadSourceID += cell3.innerHTML;
                        }
                    }
                }
                else {

                    oArg.LeadSourceID = "0";
                    oArg.Code = "";
                    oArg.Description = "";
                }
                if (oArg.Code != "") {
                    oArg.Code = oArg.Code.substring(0, oArg.Code.length - 1)
                }
                else {
                    oArg.Code = "";
                }
                oArg.Arg1 = oArg.Code;
                oArg.CallingButton = "CustomerName";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgLeadSource">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgLeadSource" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgLeadSource" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgLeadSource" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" AutoGenerateColumns="false" Height="341px" PageSize="10" OnItemCreated="dgLeadSource_ItemCreated"
            AllowPaging="true" Width="500px" AllowFilteringByColumn="true" OnNeedDataSource="dgLeadSource_BindData"
            OnItemCommand="dgLeadSource_ItemCommand" OnPreRender="dgLeadSource_PreRender"
            >
            <MasterTableView CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="LeadSourceCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LeadSourceDescription" HeaderText="Description"
                        CurrentFilterFunction="StartsWith" ShowFilterIcon="false" AutoPostBackOnFilter="false"
                        HeaderStyle-Width="380px" FilterControlWidth="360px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LeadSourceID" HeaderText="LeadSourceID" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grd_ok">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                     <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                        visible="false">
                        Use CTRL key to multi select</div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
