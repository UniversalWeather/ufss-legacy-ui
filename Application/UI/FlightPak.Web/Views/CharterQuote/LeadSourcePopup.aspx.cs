﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class LeadSourcePopup : BaseSecuredPage
    {
        private string Code;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                if (!IsPostBack)
                {
                    string i = Request.QueryString["CustomerName"];

                    if (Request.QueryString["CustomerName"] != null)
                    {
                        if (Request.QueryString["CustomerName"].ToString() == "1")
                        {
                            dgLeadSource.AllowMultiRowSelection = true;
                        }
                        else
                        {
                            dgLeadSource.AllowMultiRowSelection = false;
                        }
                    }
                    else
                    {
                        dgLeadSource.AllowMultiRowSelection = false;
                    }
                }
                if (Request.QueryString["Code"] != null && !string.IsNullOrEmpty(Request.QueryString["Code"].ToString()))
                {
                    Code = Request.QueryString["Code"].Trim();
                }
            }
        }
        protected void dgLeadSource_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    LeadSource leadSource = new LeadSource();
                    List<LeadSource> leadSourceList = new List<LeadSource>();
                    var objLeadSource = objMastersvc.GetLeadSourceList();
                    if (objLeadSource.EntityList != null && objLeadSource.EntityList.Count() > 0)
                    {
                        leadSourceList = objLeadSource.EntityList.Where(x => x.IsInActive == false).ToList<LeadSource>();
                    }
                    dgLeadSource.DataSource = leadSourceList;
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                e.Updated = dgLeadSource;
                SelectItem();
            }
        }

        /// <summary>
        /// To Select Item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(Code))
                {

                    foreach (GridDataItem item in dgLeadSource.MasterTableView.Items)
                    {
                        if (item["LeadSourceCD"].Text.Trim() == Code)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgLeadSource.MasterTableView.Items.Count > 0)
                    {
                        dgLeadSource.SelectedIndexes.Add(0);

                    }
                }
            }
        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgLeadSource_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["CustomerName"] != null)
                {
                    if (Request.QueryString["CustomerName"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgLeadSource_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }

        }

        protected void dgLeadSource_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgLeadSource, Page.Session);
        }
    }
}