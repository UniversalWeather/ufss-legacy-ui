﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SalesPersonPopup.aspx.cs"
    Inherits="FlightPak.Web.Views.CharterQuote.SalesPersonPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Salesperson</title>
    <script language="javascript" src="../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgSalesperson.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgSalesperson.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "SalesPersonCD");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "FirstName");
                    var cell3 = MasterTable.getCellByColumnUniqueName(row, "SalesPersonID");
                    var cell4 = MasterTable.getCellByColumnUniqueName(row, "LastName");
                    var cell5 = MasterTable.getCellByColumnUniqueName(row, "MiddleName");

                    if (i == 0) {
                        oArg.SalesPersonCD = cell1.innerHTML;
                        oArg.FirstName = cell2.innerHTML;
                        oArg.SalesPersonID = cell3.innerHTML;
                        oArg.LastName = cell4.innerHTML;
                        oArg.MiddleName = cell5.innerHTML;
                    }
                    else {
                        oArg.SalesPersonCD += "," + cell1.innerHTML;
                        oArg.FirstName += "," + cell2.innerHTML;
                        oArg.SalesPersonID += "," + cell3.innerHTML;
                    }
                }
                if (selectedRows.length == 0) {
                    oArg.SalesPersonCD = "";
                    oArg.FirstName = "";
                    oArg.SalesPersonID = "";
                    oArg.LastName = "";
                    oArg.MiddleName = "";
                }
                oArg.Arg1 = oArg.SalesPersonCD;
                oArg.CallingButton = "SalesPersonCD";

                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
            function prepareSearchInput(input) { input.value = input.value; }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgSalesperson">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgSalesperson" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgSalesperson" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgSalesperson" runat="server" AllowMultiRowSelection="true"
            AllowSorting="true" AutoGenerateColumns="false" Height="341px" PageSize="10" OnItemCreated="dgSalesperson_ItemCreated"
            AllowPaging="true" Width="500px" AllowFilteringByColumn="true" OnNeedDataSource="dgSalesperson_BindData"
            OnItemDataBound="dgSalesperson_OnItemDataBound" OnItemCommand="dgSalesperson_ItemCommand" OnPreRender="dgSalesperson_PreRender">
            <MasterTableView CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="SalesPersonCD" HeaderText="Code" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" HeaderStyle-Width="100px"
                        FilterControlWidth="80px" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FirstName" HeaderText="FirstName" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="LastName" HeaderText="LastName" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MiddleName" HeaderText="MiddleName" CurrentFilterFunction="StartsWith"
                        ShowFilterIcon="false" AutoPostBackOnFilter="false" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="Name" HeaderText="Description" HeaderStyle-Width="400px"
                        FilterControlWidth="380px" CurrentFilterFunction="StartsWith" ShowFilterIcon="false"
                        AutoPostBackOnFilter="false" FilterDelay="500">
                        <ItemTemplate>
                            <asp:Label ID="lblSalesperson" runat="server" Width="95%" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="SalesPersonID" HeaderText="SalesPersonID" AutoPostBackOnFilter="false"
                        ShowFilterIcon="false" CurrentFilterFunction="EqualTo" Display="false" FilterDelay="500">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div class="grd_ok">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                    <div id="divMultiSelect" class="multiselect" runat="server" clientidmode="Static"
                        visible="false">
                        Use CTRL key to multi select</div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <ClientEvents OnRowDblClick="returnToParent" />
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
