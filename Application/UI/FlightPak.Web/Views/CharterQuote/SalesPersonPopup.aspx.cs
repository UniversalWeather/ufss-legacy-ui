﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class SalesPersonPopup : BaseSecuredPage
    {
        private string Code;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //start of code block for multiple selection in the grid
                if (!IsPostBack)
                {
                    string i = Request.QueryString["Code"];

                    if (Request.QueryString["Code"] != null)
                    {
                        if (Request.QueryString["Code"].ToString() == "1")
                        {
                            dgSalesperson.AllowMultiRowSelection = true;
                        }
                        else
                        {
                            dgSalesperson.AllowMultiRowSelection = false;
                        }
                    }
                    else
                    {
                        dgSalesperson.AllowMultiRowSelection = false;
                    }
                }
                //end of code block for multiple selection in the grid

                //Added for Reassign the Selected Value and highlight the specified row in the Grid

                if (Request.QueryString["Code"] != null && !string.IsNullOrEmpty(Request.QueryString["Code"].ToString()))
                {
                    Code = Request.QueryString["Code"].Trim();
                }
            }
        }
        protected void dgSalesperson_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    List<FlightPakMasterService.GetSalesPerson> lstGetSalesPerson = new List<FlightPakMasterService.GetSalesPerson>();
                    var salesPersonList = objMastersvc.GetSalesPersonList();
                    if (salesPersonList.EntityList != null && salesPersonList.EntityList.Count > 0)
                    {
                        lstGetSalesPerson = salesPersonList.EntityList;
                    }
                    dgSalesperson.DataSource = lstGetSalesPerson;
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                e.Updated = dgSalesperson;
                SelectItem();
            }
        }

        /// <summary>
        /// To Select Item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(Code))
                {

                    foreach (GridDataItem item in dgSalesperson.MasterTableView.Items)
                    {
                        if (item["SalesPersonCD"].Text.Trim() == Code)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgSalesperson.MasterTableView.Items.Count > 0)
                    {
                        dgSalesperson.SelectedIndexes.Add(0);

                    }
                }
            }
        }

        protected void dgSalesperson_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    string firstname = string.Empty;
                    string lastname = string.Empty;
                    string middlename = string.Empty;
                    Label lblName = (Label)item.FindControl("lblSalesperson");

                    firstname = item["FirstName"].Text;
                    lastname = item["LastName"].Text;
                    middlename = item["MiddleName"].Text;
                    lblName.Text = lastname + ", " + firstname + " " + middlename;
                }
            }
        }

        /// <summary>
        /// To display message for multiselect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgSalesperson_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                GridCommandItem commandItem = e.Item as GridCommandItem;
                HtmlGenericControl container = (HtmlGenericControl)commandItem.FindControl("divMultiSelect");

                if (Request.QueryString["Code"] != null)
                {
                    if (Request.QueryString["Code"].ToString() == "1")
                    {
                        container.Visible = true;
                    }
                    else
                    {
                        container.Visible = false;
                    }
                }
            }
        }

        protected void dgSalesperson_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgSalesperson_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgSalesperson, Page.Session);
        }
    }
}