﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Telerik.Web.UI;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Views.CharterQuote
{
    public partial class CharterQuoteCustomerContactCatalogPopUP : BaseSecuredPage
    {
        private string Code;

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //Added for Reassign the Selected Value and highlight the specified row in the Grid
                if (Request.QueryString["Code"] != null && !string.IsNullOrEmpty(Request.QueryString["Code"].ToString()))
                {
                    Code = Request.QueryString["Code"].Trim();
                }
            }
        }

        protected void dgCqCustomerConatct_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    List<GetCQCustomerContactByCQCustomerID> CQCustomerContactList = new List<GetCQCustomerContactByCQCustomerID>();
                    CQCustomerContact oCQCustomerContact = new CQCustomerContact();
                    if (!string.IsNullOrEmpty(Request.QueryString["CQCustomerID"]))
                    {
                        oCQCustomerContact.CQCustomerID = Convert.ToInt64((Request.QueryString["CQCustomerID"].ToString()));
                    }
                    var objCQCustomerContact = objService.GetCQCustomerContactByCQCustomerID(oCQCustomerContact);
                    if (objCQCustomerContact.ReturnFlag == true)
                    {
                        CQCustomerContactList = objCQCustomerContact.EntityList;
                    }
                    dgCqCustomerConatct.DataSource = CQCustomerContactList;
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                e.Updated = dgCqCustomerConatct;
                SelectItem();
            }
        }

        /// <summary>
        /// To Select Item
        /// </summary>
        private void SelectItem()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(Code))
                {
                    foreach (GridDataItem item in dgCqCustomerConatct.MasterTableView.Items)
                    {
                        if (item["CQCustomerContactCD"].Text.Trim() == Code)
                        {
                            item.Selected = true;
                            break;
                        }
                    }
                }
                else
                {
                    if (dgCqCustomerConatct.MasterTableView.Items.Count > 0)
                    {
                        dgCqCustomerConatct.SelectedIndexes.Add(0);

                    }
                }
            }
        }

        protected void dgCqCustomerConatct_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;
                Session[RadGridSearchPrerenderHelper.GridFilterSessionName] = filterPair.Second.ToString();
            }
        }

        protected void dgCqCustomerConatct_PreRender(object sender, EventArgs e)
        {
            FlightPak.Web.Framework.Helpers.RadGridSearchPrerenderHelper.PrepareTextFilterSearchBox(RadGridSearchPrerenderHelper.GridFilterSessionName, dgCqCustomerConatct, Page.Session);
        }
    }
}