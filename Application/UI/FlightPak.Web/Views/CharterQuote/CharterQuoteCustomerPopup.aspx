﻿﻿<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 8 ]> <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Charter Quote Customer</title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <script type="text/javascript">
        var selectedRowData = null;
        var deleteCQCustomerID = null;
        var deleteCQCustomerName = null;
        var jqgridTableId = '#gridCustomers';
        var selectedRowMultipleCustomer = "";
        var isopenlookup = false;
        var ismultiSelect = false;
        var codes = [];
        $(document).ready(function () {
            ismultiSelect = getQuerystring("IsUIReports", "") == "" ? false : true;
            $.extend(jQuery.jgrid.defaults, {
                prmNames: {
                    page: "page", rows: "size", order: "dir", sort: "sort"
                }
            });

            var selectedRowData = getQuerystring("Code", "");
            selectedRowMultipleCustomer = $.trim(decodeURI(getQuerystring("Code", "")));
            jQuery("#hdnselectedfccd").val(selectedRowMultipleCustomer);
            if (selectedRowData != "") {
                isopenlookup = true;
            }

            if (ismultiSelect) {
                if (selectedRowData.length < jQuery("#hdnselectedfccd").val().length) {
                    selectedRowData = jQuery("#hdnselectedfccd").value;
                }
                codes = selectedRowData.split(',');
            }

            $("#btnSubmit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                if ((selr != null && ismultiSelect == false) || (ismultiSelect == true && selectedRowMultipleCustomer.length > 0)) {
                    var rowData = $(jqgridTableId).getRowData(selr);
                    returnToParent(rowData, 0);
                }
                else {
                    showMessageBox('Please select a customer.', popupTitle);
                }
                return false;
            });

            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/Views/Settings/People/CharterQuoteCustomerCatalog.aspx?IsPopup=Add&CQCustomerID=-1", popupTitle, widthDoc + 319, 600, jqgridTableId);
                return false;
            });

            $("#recordDelete").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                deleteCQCustomerID = rowData['CQCustomerID'];
                deleteCQCustomerName = rowData['CQCustomerName'];
                var widthDoc = $(document).width();
                if (deleteCQCustomerID != undefined && deleteCQCustomerID != null && deleteCQCustomerID != '' && deleteCQCustomerID != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', confirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select a customer.', popupTitle);
                }
                return false;
            });

            function confirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        type: 'GET',                        
                        url: '/Views/Utilities/DeleteApi.aspx?apiType=FSS&method=Customer&CQCustomerID=' + deleteCQCustomerID,
                        contentType: 'text/html',
                        success: function (data) {
                            verifyReturnedResultForJqgrid(data);
                            if (data != undefined && data != null && data != "") {
                                if (data == "PreconditionFailed") {
                                    showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                                }
                                else {
                                    $(jqgridTableId).trigger('reloadGrid');
                                }
                            }
                            
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (jqXHR.responseText.indexOf() == -1)
                                showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                            else
                                showMessageBox("This Customer (" + deleteCQCustomerName + ") does not exist.", popupTitle);
                        }
                    });
                }
            }
            $("#recordEdit").click(function () {
                var selr = $(jqgridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(jqgridTableId).getRowData(selr);
                var CQCustomerID = rowData['CQCustomerID'];
                var widthDoc = $(document).width();
                if (CQCustomerID != undefined && CQCustomerID != null && CQCustomerID != '' && CQCustomerID != 0) {
                    popupwindow("/Views/Settings/People/CharterQuoteCustomerCatalog.aspx?IsPopup=&CQCustomerID=" + CQCustomerID, popupTitle, widthDoc + 319, 600, jqgridTableId);
                }
                else {
                    showMessageBox('Please select a customer.', popupTitle);
                }
                return false;
            });

            jQuery(jqgridTableId).jqGrid({
                url: '/Views/Utilities/ApiCallerWithFilter.aspx',
                mtype: 'GET',
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.apiType = 'fss';
                    postData.method = 'customers';
                    postData.showInactive = true;
                    postData.cqCustomerCD = function () { if (ismultiSelect && selectedRowData.length > 0) { return codes[0]; } else { return selectedRowData; } };
                    postData.markSelectedRecord = isopenlookup;
                    isopenlookup = false;
                    return postData;
                },
                height: 320,
                width: 700,
                viewrecords: true,
                rowNum: $("#rowNum").val(),
                multiselect: ismultiSelect,
                pager: "#pg_gridPager",
                colNames: ['CQCustomerID', 'Code', 'Customer Name', 'Contact Name', 'Home Base ', 'Active', 'Contact ID', 'Contact CD', 'PhoneNum', 'FaxNum', 'Credit', ''],
                colModel: [
                    { name: 'CQCustomerID', index: 'CQCustomerID', key: true, hidden: true },
                    { name: 'CQCustomerCD', index: 'CQCustomerCD', width: 80 },
                    { name: 'CQCustomerName', CQCustomerName: 'IcaoID' },
                    { name: 'BillingName', index: 'BillingName' },
                    { name: 'HomeBaseIcaoID', index: 'IcaoID', width: 80 },
                    { name: 'IsInactive', index: 'IsInactive', width: 80, align: 'center', editable: true, edittype: 'checkbox', formatter: reverse_value, formatoptions: { disabled: true }, search: false },
                    { name: 'CQCustomerContactID', index: 'CQCustomerContactID', hidden: true },
                    { name: 'CQCustomerContactCD', index: 'CQCustomerContactCD', hidden: true },
                    { name: 'PhoneNum', index: 'PhoneNum', hidden: true },
                    { name: 'FaxNum', index: 'FaxNum', hidden: true },
                    { name: 'Credit', index: 'Credit', hidden: true },
                    { name: 'HomeBaseIcaoID', index: 'HomeBaseIcaoID', width: 80, hidden: true },
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId);
                    returnToParent(rowData, 1);
                },
                onSelectRow: function (id, status) {
                    var rowData = $(this).getRowData(id);
                    var lastSel = rowData['CQCustomerCD'];//replace name with any column
                    selectedRowMultipleCustomer = JqgridOnSelectRow(ismultiSelect, status, selectedRowMultipleCustomer, rowData.CQCustomerCD);
                },
                onSelectAll: function (id, status) {
                    selectedRowMultipleCustomer = JqgridSelectAll(selectedRowMultipleCustomer, jqgridTableId, id, status, 'CQCustomerCD');
                    jQuery("#hdnselectedfccd").val(selectedRowMultipleAirport);
                },
                afterInsertRow: function (rowid, rowObject) {
                    JqgridSelectAfterInsertRow(ismultiSelect, selectedRowMultipleCustomer, rowid, rowObject.CQCustomerCD, jqgridTableId);
                },
                loadComplete: function (rowData) {
                    JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId);
                }

            });
            $("#pagesizebox").insertBefore('.ui-paging-info');
            $(jqgridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });

        });

        function reverse_value(value) {
            if (value)
                value = '<input type="checkbox" disabled="disabled" offval="no" value="0">';
            else
                value = '<input type="checkbox" disabled="disabled" offval="no" value="1" checked="checked">';
            return value;
        }

        function returnToParent(rowData, one) {
            selectedRowMultipleCustomer = jQuery.trim(selectedRowMultipleCustomer);
            if (selectedRowMultipleCustomer.lastIndexOf(",") == selectedRowMultipleCustomer.length - 1)
                selectedRowMultipleCustomer = selectedRowMultipleCustomer.substr(0, selectedRowMultipleCustomer.length - 1);

            var oArg = new Object();
            if (one == 0) {
                oArg.CQCustomerCD = selectedRowMultipleCustomer;
            }
            else {
                oArg.CQCustomerCD = rowData["CQCustomerCD"];
            }
            oArg.CQCustomerID = rowData["CQCustomerID"];

            oArg.CQCustomerName = rowData["CQCustomerName"];
            oArg.BillingName = rowData["BillingName"];
            oArg.IsInactive = rowData["IsInactive"];
            oArg.HomeBaseIcaoID = rowData["HomeBaseIcaoID"];
            oArg.CQCustomerContactID = rowData["CQCustomerContactID"];
            oArg.CQCustomerContactCD = rowData["CQCustomerContactCD"];
            oArg.PhoneNum = rowData["PhoneNum"];
            oArg.FaxNum = rowData["FaxNum"];
            oArg.Credit = rowData["Credit"];

            oArg.Arg1 = oArg.CQCustomerCD;
            oArg.CallingButton = "CQCustomerCD";

            var oWnd = GetRadWindow();
            if (oArg.CQCustomerContactID) {
                oWnd.close(oArg);
            }
            else {
                oWnd.close();
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="divGridPanel">
                <div class="jqgrid">
                    <input type="hidden" id="hdnselectedfccd" value="" />
                    <div>
                        <table class="box1">
                            <tr>
                                <td>
                                    <table id="gridCustomers" style="width: 700px !important;" class="table table-striped table-hover table-bordered"></table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="grid_icon">
                                        <div role="group" id="pg_gridPager"></div>
                                        <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                        <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                        <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                        <div id="pagesizebox">
                                            <span>Page Size:</span>
                                            <input class="psize" id="rowNum" type="text" value="20" maxlength="5" />
                                            <input id="btnChange" class="rgPagerButton2" value="Change" type="submit" name="btnChange" onclick="reloadPageSize(jqgridTableId, $('#rowNum')); return false;" />
                                        </div>
                                    </div>
                                    <div style="padding: 5px 5px; text-align: right;">
                                        <input id="btnSubmit" class="button okButton" value="OK" type="button" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
