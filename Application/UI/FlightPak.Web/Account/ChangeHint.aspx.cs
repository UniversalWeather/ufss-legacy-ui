﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Encryption;

namespace FlightPak.Web.Account
{
    public partial class ChangeHint : BaseSecuredPage
    {
        private ExceptionManager exManager;
        string RedirectURL = "~\\Views\\Home.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        if (!IsPostBack)
                        {
                            using (AuthenticationService.AuthenticationServiceClient client = new AuthenticationService.AuthenticationServiceClient())
                            {
                                var securityHint = client.GetUserSecurityHintByEmailId(UserPrincipal.Identity._email);
                                if (securityHint != null && securityHint.EntityInfo != null && !string.IsNullOrEmpty(securityHint.EntityInfo.UserName))
                                {
                                    QuestionTextBox.Text = securityHint.EntityInfo.SecurityQuestion;
                                    AnswerTextBox.Text = securityHint.EntityInfo.SecurityAnswer;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, "Update security answer");
                }
            }

        }

        protected void UpdatePushButton_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AuthenticationService.AuthenticationServiceClient client = new AuthenticationService.AuthenticationServiceClient())
                        {
                            Crypto crypto = new Crypto();
                            client.AddOrUpdateSecurityHint(
                                new AuthenticationService.UserSecurityHint
                                {
                                    UserName = UserPrincipal.Identity._name,
                                    SecurityQuestion = QuestionTextBox.Text,
                                    ActiveEndDT = DateTime.UtcNow,
                                    ActiveStartDT = DateTime.UtcNow,
                                    PWDStatus = false,
                                    LastUpdTS = DateTime.UtcNow,
                                    LastUpdUID = UserPrincipal.Identity._name,
                                    SecurityAnswer = AnswerTextBox.Text,
                                    CustomerID = UserPrincipal.Identity._customerID,
                                    UserSecurityHintID = 0
                                });

                        }
                        Session["ForceChangePassword"] = null;

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, "Update security answer");
                }
            }
            Response.Redirect(RedirectURL, true);
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(RedirectURL, true);
        }
    }
}