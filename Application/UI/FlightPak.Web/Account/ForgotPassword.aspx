﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs"
    Inherits="FlightPak.Web.Account.ForgotPassword" MasterPageFile="~/Framework/Masters/UnSecured.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="account-body">
        <div class="forgot-pswd-section">
            <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" MembershipProvider="FPMembershipProvider"
                OnUserLookupError="PasswordRecovery1_UserLookupError" OnLoad="PasswordRecovery1_Load"
                OnSendingMail="PasswordRecovery1_SendingMail" OnSendMailError="PasswordRecovery1_OnSendMailError"
                AnswerLabelText="Security Answer" AnswerRequiredErrorMessage="*" GeneralFailureText="Your password has been sent to you.</br><a href='Login.aspx'>Return to Login Page</a>"
                QuestionFailureText="QuestionFailureText" QuestionInstructionText="QuestionInstructionText"
                QuestionLabelText="QuestionLabelText" UserNameFailureText="We were unable to access your information. Please contact administrator for further assistance.</br><a href='Login.aspx'>Return to Login Page</a>"
                QuestionTitleText="QuestionTitleText" OnVerifyingUser="PasswordRecovery1_VerifyingUser">
                <UserNameTemplate>
                    <div class="forgot-pswd-outter">
                        <h1>
                            Forgot Your Password?</h1>
                        <span class="acnt_info_text">Enter your User Name to receive your password.</span>
                        <div class="acnt_input_area">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <div class="tblspace_10">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel140">
                                                    User Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="UserName" runat="server" CssClass="input_box_forgot"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel140">
                                                </td>
                                                <td class="alert-text">
                                                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                        ErrorMessage="User Name is Required." ToolTip="User Name is Required." ValidationGroup="PasswordRecovery1">User Name is required.</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel140">
                                                </td>
                                                <td class="alert-text">
                                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="tblspace_5">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="SubmitButton" runat="server" CssClass="user_submit" CommandName="Submit"
                                                        Text="Submit" ValidationGroup="PasswordRecovery1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </UserNameTemplate>
                <QuestionTemplate>
                    <div class="forgot-pswd-outter">
                        <h1>
                            Identity Confirmation</h1>
                        <span class="acnt_info_text">Answer the following question to receive your password.</span>
                        <div class="acnt_input_area">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel140">
                                                    User Name
                                                </td>
                                                <td class="input_no_bg">
                                                    <asp:Literal ID="UserName" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <div class="tblspace_5">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel140">
                                                    Question
                                                </td>
                                                <td class="input_no_bg">
                                                    <asp:Literal ID="Question" runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td>
                                                    <div class="tblspace_5">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel140">
                                                    <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Answer</asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="Answer" runat="server" CssClass="input_box_forgot"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="tdLabel140">
                                                </td>
                                                <td class="alert-text">
                                                    <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                                        ErrorMessage="Answer is Required." ToolTip="Answer is Required." ValidationGroup="PasswordRecovery1">Answer is required</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel140">
                                                </td>
                                                <td class="alert-text">
                                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="tblspace_5">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" CssClass="user_submit"
                                                        Text="Submit" ValidationGroup="PasswordRecovery1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                </QuestionTemplate>
                <SuccessTemplate>
                    <div class="pswd_success">
                        <span class="ps_text">Your password has been sent to you. <br /><a href="Login.aspx">Return to Login Page</a></span>
                        
                        <%--<table border="0" style="font-size: 10pt;">
                            <tr>
                                <td>
                                    Your password has been sent to you.
                                </td>
                            </tr>
                        </table>--%>
                        
                    </div>
                </SuccessTemplate>
            </asp:PasswordRecovery>
        </div>
    </div>
</asp:Content>
