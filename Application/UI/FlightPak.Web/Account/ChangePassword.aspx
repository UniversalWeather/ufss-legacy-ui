﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Framework/Masters/Authentication.Master"
    AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="FlightPak.Web.Account.ChangePassword" %>
<%@ Import Namespace="System.Web.Optimization" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <%= Styles.Render("~/bundles/BootStrapPopupCssBundle") %> 
    <script src="../Scripts/pwstrength.js" type="text/javascript"></script>
    <style type="text/css">
*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}
button,input,select,textarea{font-family:inherit;font-size:inherit;line-height:inherit}a{color:#337ab7;text-decoration:none}a:focus,a:hover{color:#23527c;text-decoration:underline}
.text-center{text-align:center}

        li {
            margin-top: 6px !important;
        }
            li img {
                margin-top:-1px;
                width:12px;
                margin-right:5px;
            }
        .success {
            color: #6ea334;
        }

        .error {
            color: #c04b4a;
        }

        .btn-changepass {
            border-radius: 3px;
            background-color: #0068cc;
        }

        .btn-cancel {
            background-color: #aea9a9;
            color: white;
            border-radius: 3px;
        }
        .progress {
            height:10px;
            border-radius:0px;
        }
    </style>
    <script type="text/javascript">

       
        $(document).ready(function () {
            var MSG = $("#chngpswdSummary").text();
            if (MSG.length > 40) {
                $("#InvalidOldPassword").text("Old password incorrect OR New password enterted before on last 10 passwords");
            }

        });

        function ClearMSG() {
            $("#InvalidOldPassword").text("");
        }

        var flag1Ltr, flag1CapLtr, flag1Num, flag1SplChar, flagLen = false;
        function AtLeastOneLetter(text) {
            if (text != "") {
                if (text.match("[a-z]"))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        function AtLeastOneCapitalLetter(text) {
            if (text != "") {
                if (text.match("[A-Z]"))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        function AtLeastOneNumber(text) {
            if (text != "") {
                if (text.match("[0-9]"))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        function length(text) {
            if (text.length >= 8) {
                return true;
            }
            else
                return false;
        }
        function AtLeastOneSpecialCharacter(text) {
            if (text != "") {
                if (text.match(/[-!$%^&*()_+|~=`@#{}\[\]:";'<>?,.\/\\]/))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        function validationSuccess(id) {
            if ($("#" + id).hasClass("error")) {
                $("#" + id).removeClass("error").addClass("success");
                $("#" + id + " i img").attr("src","../Scripts/tick.png");
            }
        }
        function validationError(id) {
            if ($("#" + id).hasClass("success")) {
                $("#" + id).removeClass("success").addClass("error");
                $("#" + id + " i img").attr("src", "../Scripts/cross.png");
            }
        }

        function EnableBtn() {
            if (flag1Ltr == true && flag1CapLtr == true && flag1Num == true && flagLen == true && flag1SplChar == true)
                $("#<%=ChangeUserPasswordControl.ChangePasswordTemplateContainer.FindControl("ChangePasswordPushButton").ClientID%>").attr("disabled", false);
            else
                $("#<%=ChangeUserPasswordControl.ChangePasswordTemplateContainer.FindControl("ChangePasswordPushButton").ClientID%>").attr("disabled", true);

        }
        function PasswordRequirements() {
            var pwdText = $("#<%=ChangeUserPasswordControl.ChangePasswordTemplateContainer.FindControl("NewPassword").ClientID%>").val();
            if (AtLeastOneLetter(pwdText)) {
                validationSuccess("OneLetter");
                flag1Ltr = true;
            }
            else {
                validationError("OneLetter");
                flag1Ltr = false;
            }

            if (AtLeastOneCapitalLetter(pwdText)) {
                validationSuccess("OneCapitalLetter");
                flag1CapLtr = true;
            }

            else {
                validationError("OneCapitalLetter");
                flag1CapLtr = false;
            }
            if (AtLeastOneNumber(pwdText)) {
                validationSuccess("OneNumber");
                flag1Num = true;
            }
            else {
                validationError("OneNumber");
                flag1Num = false;
            }
            if (length(pwdText)) {
                validationSuccess("8Characters");
                flagLen = true;
            }
            else {
                validationError("8Characters");
                flagLen = false;
            }
            if (AtLeastOneSpecialCharacter(pwdText)) {
                validationSuccess("SpecialCharater");
                flag1SplChar = true;
            }
            else {
                validationError("SpecialCharater");
                flag1SplChar = false;
            }

            EnableBtn();
        }
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ChangePassword ID="ChangeUserPasswordControl" runat="server" CancelDestinationPageUrl="~/"
        EnableViewState="false" RenderOuterTable="false" SuccessPageUrl="ChangePasswordSuccess.aspx"
        OnSendingMail="ChangeUserPasswordControl_OnSendingMail" OnSendMailError="ChangeUserPasswordControl_OnSendMailError">
        <ChangePasswordTemplate>
            <asp:Panel ID="pnlChangePassword" runat="server" DefaultButton="ChangePasswordPushButton">
                <span id="chngpswdSummary" class="alert-text-chngpswd" style="display:none">
                    <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="ChangeUserPasswordValidationSummary" runat="server" CssClass="failureNotification"
                    PasswordHintText="Please enter a password at least 8 characters long, containing a number and one special character."
                    NewPasswordRegularExpression='@\"(?=.{7,})(?=(.*\d){1,})(?=(.*\W){1,})' NewPasswordRegularExpressionErrorMessage="Error: Your password must be at least 8 characters long, and contain at least one number and one special character." />
                <div class="account-body">
                    <div class="container col-xs-8 col-md-push-2" style="border: 1px solid #ddd; background-color: #fbfafa; border-radius: 4px; padding-bottom: 15px;margin-top:30px">
                        <div class="col-xs-12">
                            <div class="col-md-12" style="padding: 0px">
                                <h2 class="text-center" style="color: black"><strong>Change Password</strong></h2>
                                <center>
                                    <hr style="margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #ddd;color:#ddd;">
                                </center>
                            </div>

                            <div class="col-xs-12 form-group" style="padding: 0px; margin-bottom: 0px">
                                <div class="col-xs-7" style="float: left; margin-left: 0; padding: 0px" id="pwd-container">
                                    <div class="row form-group">
                                        <div class="col-xs-4" style="padding-right: 0px; top: 7px">Old Password</div>
                                        <div class="col-xs-8">
                                            <asp:TextBox ID="CurrentPassword" TabIndex="100" runat="server" onkeypress="ClearMSG();" CssClass="input-sm form-control"
                                                TextMode="Password"></asp:TextBox>
                                             <span id="InvalidOldPassword" class="alert-text-chngpswd" >
                                               </span>
                                            <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" Display="Dynamic"
                                                CssClass="alert-text" ToolTip="Old Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">Password is Required.</asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-4" style="padding-right: 0px; top: 7px">New Password</div>
                                        <div class="col-xs-8">
                                            <asp:TextBox ID="NewPassword" TabIndex="101" runat="server" CssClass="input-sm form-control"
                                                onkeyup="PasswordRequirements()" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-4" style="padding-right: 0px; top: 7px">Confirm Password</div>
                                        <div class="col-xs-8">
                                            <asp:TextBox ID="ConfirmNewPassword" TabIndex="102" runat="server" CssClass="input-sm form-control"
                                                TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" Display="Dynamic"
                                                CssClass="alert-text" ToolTip="Confirm New Password is required." ValidationGroup="ChangeUserPasswordValidationGroup">Confirm New Password is Required.</asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                ControlToValidate="ConfirmNewPassword" CssClass="alert-text" Display="Dynamic"
                                                ValidationGroup="ChangeUserPasswordValidationGroup">The Confirm New Password must match the New Password entry.</asp:CompareValidator>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-4" style="padding-right: 0px"><span class="pwstrength_viewport_verdict"></span>&nbsp;Password</div>
                                        <span class="pwstrength_viewport_progress col-xs-8"></span>
                                    </div>
                                </div>
                                <div class="col-xs-5" style="padding-left: 0px; padding-right: 18px">
                                    <div class="col-xs-12 col-xs-push-1" style="border: 1px solid #ddd; padding: 10px">
                                        Password Minimum Requirements:
                        <ul>
                            <li id="8Characters" class="error"><i class="glyphicon glyphicon-remove"><img src="../Scripts/cross.png" /></i>Eight (8) characters</li>
                            <li id="OneLetter" class="error"><i class="glyphicon glyphicon-remove"><img src="../Scripts/cross.png" /></i>One (1) letter</li>
                            <li id="OneCapitalLetter" class="error"><i class="glyphicon glyphicon-remove"><img src="../Scripts/cross.png" /></i>One (1) capital letter</li>
                            <li id="OneNumber" class="error"><i class="glyphicon glyphicon-remove"><img src="../Scripts/cross.png" /></i>One (1) number</li>
                            <li id="SpecialCharater" class="error"><i class="glyphicon glyphicon-remove"><img src="../Scripts/cross.png" /></i>One (1) special character</li>
                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <asp:Button ID="CancelPushButton" TabIndex="104" CssClass="btn btn-cancel" runat="server"
                                        CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                                </div>
                                <div class="col-md-6 text-right">
                                    <asp:Button ID="ChangePasswordPushButton" TabIndex="103" CssClass="btn btn-primary btn-changepass" runat="server"
                                        CausesValidation="true" disabled CommandName="ChangePassword" Text="Change Password" ValidationGroup="ChangeUserPasswordValidationGroup" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </asp:Panel>
        </ChangePasswordTemplate>
    </asp:ChangePassword>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                viewports: {
                    progress: ".pwstrength_viewport_progress",
                    verdict: ".pwstrength_viewport_verdict"
                }
            };
            options.common = {};
            $('#<%=ChangeUserPasswordControl.ChangePasswordTemplateContainer.FindControl("NewPassword").ClientID%>').pwstrength(options);
        });
    </script>
</asp:Content>
