﻿<%@ Page Title="Change Password" Language="C#" MasterPageFile="~/Framework/Masters/Authentication.Master"
    AutoEventWireup="true" CodeBehind="ChangePasswordSuccess.aspx.cs" Inherits="FlightPak.Web.Account.ChangePasswordSuccess" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="account-body">
        <div class="account-pswd-section">
            <div class="account-pswd-outter">
                <div style=" text-align:center; margin:130px 0 0 0;">
                    <h2 style=" text-align:center!important;">
                        Change Password
                    </h2>
                    <p style=" color:#228e02;">
                        Your password has been changed successfully.
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
