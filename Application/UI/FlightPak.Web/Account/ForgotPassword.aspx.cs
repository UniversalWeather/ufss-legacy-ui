﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Account
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void PasswordRecovery1_UserLookupError(object sender, System.EventArgs e)
        {
            PasswordRecovery1.LabelStyle.ForeColor = System.Drawing.Color.Red;
        }

        // Reset the field label background color.
        protected void PasswordRecovery1_Load(object sender, System.EventArgs e)
        {
            PasswordRecovery1.LabelStyle.ForeColor = System.Drawing.Color.Black;
        }
        protected void PasswordRecovery1_SendingMail(object sender, MailMessageEventArgs e)
        {
            e.Cancel = true;
        }

        protected void PasswordRecovery1_OnSendMailError(object sender, SendMailErrorEventArgs e)
        {
            e.Handled = true;
        }
        protected void PasswordRecovery1_VerifyingUser(object sender, LoginCancelEventArgs e)
        {
            //Panel1.DefaultButton = PasswordRecovery1.QuestionTemplateContainer.FindControl("SubmitButton").UniqueID;
        }

    }
}