﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace FlightPak.Web.Account
{
    public partial class ChangePassword : BaseSecuredPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            RegularExpressionValidator validator
                = ((RegularExpressionValidator)(ChangeUserPasswordControl.Controls[0].FindControl("NewPasswordRegex")));

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ChangeUserPasswordControl.ChangePasswordTemplateContainer.FindControl("CurrentPassword").Focus();
                //((ValidationSummary)ChangeUserPasswordControl.ChangePasswordTemplateContainer.FindControl("ChangeUserPasswordValidationSummary")).NewPasswordRegularExpression = Membership.PasswordStrengthRegularExpression;
            }
        }

        protected void ChangeUserPasswordControl_OnSendingMail(object sender, MailMessageEventArgs e)
        {
            e.Cancel = true;
        }

        protected void ChangeUserPasswordControl_OnSendMailError(object sender, SendMailErrorEventArgs e)
        {
            e.Handled = true;
        }
    }
}
