﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Framework/Masters/Authentication.Master"
    AutoEventWireup="true" CodeBehind="ChangeHint.aspx.cs" Inherits="FlightPak.Web.Account.ChangeHint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="account-body">
        <div class="account-pswd-section">
            <div class="account-pswd-outter">
                <h1>
                    Security Information</h1>
                <div class="acnt_input_area">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel140">
                                            <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="QuestionTextBox">Security Question</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="QuestionTextBox" runat="server" CssClass="input_box_acnt"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="QuestionTextBoxRequired" runat="server" ControlToValidate="QuestionTextBox"
                                                CssClass="alert-text" ToolTip="Security question is required." ValidationGroup="SecurityValidationGroup">Security Question is Required.</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel140">
                                            <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="AnswerTextBox">Security Answer</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="AnswerTextBox" runat="server" CssClass="input_box_acnt" TextMode="Password"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="AnswerTextBoxRequired" runat="server" ControlToValidate="AnswerTextBox"
                                                CssClass="alert-text" ToolTip="Security answer is required." ValidationGroup="SecurityValidationGroup">Security Answer is Required.</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <div class="tblspace_5">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="CancelPushButton" CssClass="user_submit" runat="server" CausesValidation="False"
                                                OnClick="Cancel_Click" Text="Cancel" />
                                            <asp:Button ID="UpdatePushButton" CssClass="user_submit" runat="server" OnClick="UpdatePushButton_Click"
                                                Text="Update" ValidationGroup="SecurityValidationGroup" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
