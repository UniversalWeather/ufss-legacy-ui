﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotUserName.aspx.cs"
    Inherits="FlightPak.Web.Account.ForgotUserName" MasterPageFile="~/Framework/Masters/UnSecured.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" EnableScriptCombine="false" />
    <div class="account-body">
        <div class="forgot-user-section">
            <div class="forgot-user-outter">
                <h1>
                    Forgot your Username?</h1>
                <span>Please enter the recovery E-mail address you provided when creating your account.</span>
                <div class="acnt_input_area">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td>
                                            <div class="tblspace_10">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel140">
                                            E-mail Address:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="emailTextbox" CssClass="input_box_forgot" runat="server" ValidationGroup="save"
                                                CausesValidation="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td id="Message" runat="server">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%" style="height: 20px;">
                                    <tr>
                                        <td class="tdLabel140">
                                        </td>
                                        <td>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="save" Display="Dynamic"
                                                ControlToValidate="emailTextbox" CssClass="alert-text" runat="server" ErrorMessage="E-mail is Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="emailTextbox"
                                                CssClass="alert-text" runat="server" ID="RegularExpressionValidator2" ErrorMessage="Please Enter valid E-mail"
                                                ValidationExpression="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnNext" runat="server" Text="Submit" CssClass="user_submit" ValidationGroup="save"
                                                CausesValidation="true" OnClick="submitButton_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
