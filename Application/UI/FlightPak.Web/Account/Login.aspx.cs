﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Framework.Encryption;
using FlightPak.Web.Framework.Session;
using System.Configuration;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.Account
{
    public partial class Login : System.Web.UI.Page
    {
        string virtualPath = "~/App_Themes/Default/Images_Rotator";
        string RedirectURL = "~\\Views\\Home.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbYear.Text = DateTime.UtcNow.Year.ToString();
                //Possible reasons why we are using logout here,
                // IIS 7 has a bug, some times it returns duplicate session id. Please refer http://forums.iis.net/t/1154347.aspx
                // Firefox & Chrome retains session id even you close & reopen the browser.
                // Browsers which is having tabs, it retains old session id. Please refer http://superuser.com/questions/690/how-can-i-get-a-new-browser-session-when-opening-a-new-tab-or-window-on-firefox
                SecurityChecks();
            }
        }

        private void SecurityChecks()
        {
            LogOut();
            GenerateNewSessionId();
            if (Request.Cookies["UName"] != null)
            {
                LoginUser.UserName = Request.Cookies["UName"].Value;
                LoginUser.RememberMeSet = true;
            }
        }
        
        protected void Login_User(object sender, EventArgs e)
        {
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {

                //AuthService.LogOutByEmailId(LoginUser.UserName, Session.SessionID);
              
             

                if (Membership.ValidateUser(LoginUser.UserName, LoginUser.Password))
                {

                    bool userValidForCoreApi = AuthenticateCoreApi(LoginUser.UserName, LoginUser.Password);
                    if (!userValidForCoreApi)
                        RadWindowManager1.RadAlert("There was a problem authenticating with CoreApi", 330, 100, "CoreApi Alert", null);

                    if (LoginUser.RememberMeSet)
                    {
                        Response.Cookies["UName"].Value = LoginUser.UserName;
                        Response.Cookies["UName"].Expires = DateTime.Now.AddDays(15);
                    }
                    else
                    { Response.Cookies["UName"].Expires = DateTime.Now.AddDays(-1); }
                    Session[Session.SessionID] = new FPPrincipal(AuthService.GetUserPrincipal(Session.SessionID)._identity);
                    if (AuthService.CheckAuthenticationByEmailId(LoginUser.UserName, Session.SessionID))
                    {
                        LoginUser.FindControl("FailureText").Visible = false;
                        RadWindowManager1.RadConfirm("This will terminate your previous login sessions. Click Ok to proceed (For security reason this page will be reloaded after 30 seconds)", "confirmTerminateSessionCallBackFn", 330, 100, null, "Confirmation!");
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Refresh", "window.setInterval(function(){window.location.href='login.aspx';}, 30000);", true);
                        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Refresh", "alert('hi');", true);
                    }
                    else
                    { FormsAuthentication.RedirectFromLoginPage(LoginUser.UserName, LoginUser.RememberMeSet); }

                    //FormsAuthentication.SetAuthCookie(LoginUser.UserName, LoginUser.RememberMeSet);

                    //Response.Redirect(RedirectURL, true);

                }
                else
                    using (AdminService.AdminServiceClient client = new AdminService.AdminServiceClient())
                    {
                        var user = client.GetUserByEMailId(LoginUser.UserName);
                        bool Resetflag = client.CheckResetpasswordPeriod(LoginUser.UserName);
                        if (user != null && user.EntityList != null && user.EntityList.FirstOrDefault() != null && user.EntityList.FirstOrDefault().UserName.Length > 0 && user.EntityList.FirstOrDefault().IsUserLock.GetValueOrDefault())
                        {
                            LoginUser.FailureText = "User locked. Please contact your administrator.";
                        }
                        //Check whether the customer is locked
                        else if ((user != null && user.EntityList != null && user.EntityList.FirstOrDefault() != null &&
                            user.EntityList.FirstOrDefault().UserName.Length > 0 && user.EntityList.FirstOrDefault().IsCustomerLock))
                        {
                            Response.Redirect("~\\Views\\ErrorPages\\UnderConstruction.aspx");
                        }
                        else if (!Resetflag)
                        {
                            Response.Redirect("~\\Account\\ForgotPassword.aspx");
                        }
                        else
                        {
                             user = client.GetUserByEMailId(LoginUser.UserName);
                             if (user != null && user.EntityList != null && user.EntityList.FirstOrDefault() != null && user.EntityList.FirstOrDefault().UserName.Length > 0 && !user.EntityList.FirstOrDefault().IsUserLock.GetValueOrDefault() && user.EntityList.FirstOrDefault().InvalidLoginCount == 5)
                             {
                                 client.SendLockedEmailToUser(LoginUser.UserName);
                             }
                            LoginUser.FailureText = "The E-mail id or password you entered is incorrect.";
                        }
                    }


            }

        }

        public bool AuthenticateCoreApi(string username, string password)
        {
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
                return false;
            string clientid = ConfigurationManager.AppSettings["CoreApiClientId"];
            string clientsecret = ConfigurationManager.AppSettings["CoreApiClientSecret"];
            String coreApiAppName = ConfigurationManager.AppSettings["CoreApiAppName"];
            String coreApiServer = ConfigurationManager.AppSettings["CoreApiServer"];

            //make sure your escape the data posted to the OAuth STS
            string postData = string.Format("username={0}&password={1}&grant_type=password&login_type={2}", username, password, coreApiAppName);

            //Build HTTP Basic header value 
            string encodedClientDetails = "";
            if (!string.IsNullOrEmpty(clientid) && !string.IsNullOrEmpty(clientsecret))
            {
               encodedClientDetails= Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", clientid, clientsecret)));
            }
            string basic = string.Format("Basic {0}", encodedClientDetails);
            //Build the request
            HttpWebRequest request;
            bool errored = false;
            string response = string.Empty;

            request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["CoreApiServer"] + "/api/v1/oauth/token");

            request.Headers.Add(HttpRequestHeader.Authorization, basic);
            request.Method = "POST";
            byte[] requestPayload = new ASCIIEncoding().GetBytes(postData);
            request.ContentLength = requestPayload.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            try
            {
                using (var stream = request.GetRequestStream())
                {
                    if (requestPayload.Length > 0)
                    {
                        stream.Write(requestPayload, 0, requestPayload.Length);
                    }
                }

                //send token request
                var responseObject = (HttpWebResponse)request.GetResponse();
                using (var stream = new StreamReader(responseObject.GetResponseStream()))
                {
                    response = stream.ReadToEnd();
                }
                if (String.IsNullOrEmpty(response))
                    return false;

                dynamic json = JsonConvert.DeserializeObject(response);
                if (json == null || json.access_token == null)
                {
                    return false;
                }
                var accessToken = json.access_token.ToString();
                var refreshtoken = json.refresh_token.ToString();

                #region To be added into DEV and Tested for 07-August build
                //var strExpiresIn = json.expires_in.ToString();
                //long expiresIn = 0;
                //long.TryParse(strExpiresIn, out expiresIn);
                //if (expiresIn <= 0)
                //    expiresIn = 240;
                #endregion

                long expiresIn = 240;
                if (!errored)
                {
                    FlightPak.Web.Framework.Helpers.CoreApiManager.SetApiCredentials(accessToken, refreshtoken, expiresIn);
                    Session["Is_Authenticated"] = "true";
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckResetPassword(string email)
        {
            string postData = string.Format("Useremail={0}", email);

            ServerApiCaller APi = new ServerApiCaller();
            string responseString = APi.CallAPI("CheckResetpasswordPeriod", "Users", postData);

            if (responseString.ToLower() == "true")
                return true;

            return false;
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {
                LoginUser.Visible = false;
                AuthService.LogOutByEmailId(LoginUser.UserName, Session.SessionID);
                FormsAuthentication.RedirectFromLoginPage(LoginUser.UserName, LoginUser.RememberMeSet);
            }
        }
        protected void btnNo_Click(object sender, EventArgs e)
        {
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {
                SecurityChecks();
            }
        }
        /// <summary>
        /// Used to Log out
        /// </summary>
        protected void LogOut()
        {
            // Clear cache entry for FlightPak.Service
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                commonService.LogOut();
            }
            // Delete the record in login table
            using (AuthenticationService.AuthenticationServiceClient authenticationService = new AuthenticationService.AuthenticationServiceClient())
            {
                authenticationService.Logout(Session.SessionID);
            }

            // Sign out from Forms Authentication
            FormsAuthentication.SignOut();
            Session.Abandon();

            //Clear FormsAuthentication Cookie
            HttpCookie FormsAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            FormsAuthenticationCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(FormsAuthenticationCookie);

            //Clear ASP.NET_SessionId
            HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            SessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(SessionCookie);

            //Clear View State
            ViewState.Clear();
            this.ClearChildViewState();

        }

        protected void GenerateNewSessionId()
        {
            CustomSessionID manager = new CustomSessionID();
            manager.CreateSessionID(Context);
        }

    }
}
