﻿<%@ Page Title="Log In" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs"
    Inherits="FlightPak.Web.Account.Login" Theme="" StylesheetTheme="" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <link href="../Login/global.css" rel="stylesheet" type="text/css" />
    <link rel="apple-touch-icon" href="../login/images/customicon.png" />
    <title>Universal&reg;: Login | Scheduling Software</title>
    <meta name="Description" content="" />
    <meta name="Keywords" content="" />
    <link rel="shortcut icon" href="../login/favicon.ico" />
    
    <style type="text/css">
        .rotWrapper
        {
            margin-left: auto;
            margin-right: auto;
            height: 260px;
            font-family: Arial;
            padding: 190px 0 0;
            width: 750px;
            background: #fff url("travel_back.jpg") no-repeat 0 0;
            font: 14px 'Segoe UI' , Arial, Sans-serif;
            color: #000;
        }
        
        .RemoveRotatorBorder div.rrClipRegion
        {
            /* The following style removes the border of the rotator that is applied to the items wrapper DIV of the control by default, in case the control renders buttons.
            In case you want to removed this border, you can safely use this CSS selector. */
            border: 0px none;
            -webkit-backface-visibility: hidden; /*
            If the image vertical line issue occure in IE or Firefox enable this properties.
            -moz-backface-visibility: hidden;
            -ms-backface-visibility: hidden;*/
        }
    </style>
    <%--<script type="text/javascript">


        var animationOptions = {
            //minScale: 0.8, // The size scale [0; 1], applied to the items that are not selected.
            //yO: 60, // The offset in pixels of the center of the selected item from the top edge of the rotator.
            //xR: -30, // The offset in pixels between the selected items and the first item on the left and on the right of the selected item.
            //xItemSpacing: 50, // The offset in pixels between the items in the rotator.
            //matrix: { m11: 1, m12: 0, m21: -0.1, m22: 1 }, // The 2d transformation matrix, applied to the items that appear on the right of the selected item.
            //reflectionHeight: 0.5, // The height of the reflection
            //reflectionOpacity: 1 // The opacity, applied to the reflection
        }

        // The set_scrollAnimationOptions method takes two arguments - the first one is the ClientID of the rotator, which we want to configure and the second one is
        // a hash table with the options we want to apply.
        //Telerik.Web.UI.RadRotatorAnimation.set_scrollAnimationOptions('RadRotator1', animationOptions);
    </script>--%>
</head>
<body>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
    </telerik:RadWindowManager>
    <div id="outterPageWrapper">
        <!-- START HEADER AREA -->
        <div id="header">
            <div id="logo">
                <a href="http://www.universalweather.com/" target="_blank">
                    <img src="../login/images/universal-weather-and-aviation.png"
                        alt="Universal Weather and Aviation" width="260" height="114" /></a></div>
        </div>
        <!-- START CONTENT AREA -->
        <div id="ru-wrap">
            <div id="ru-leftside">
                <div id="MktAppSectionOneWrapper">
                    <div id="MktAppSectionOne">
                        <iframe id="MktAppSectionOneFrame" src="../login/marketing/mkt-app-section-one.html">
                        </iframe>
                    </div>
                </div>
            </div>
            <div id="ru-rightside">
                <div id="login-box">
                    <div id="login-box-content">
                        <h2>
                            Flight Scheduling Software</h2>
                        <div id="login-form">
                            <!-- FORM HERE -->
                            <form id="Form1" runat="server">
                            <script type="text/javascript">
                                function confirmTerminateSessionCallBackFn(arg) {
                                    if (arg == true) {
                                        document.getElementById('<%=btnYes.ClientID%>').click();
                                    }
                                    else
                                    { document.getElementById('<%=btnNo.ClientID%>').click(); }
                                }
                            </script>
                            <asp:ScriptManager ID="scr1" runat="server">
                            </asp:ScriptManager>
                            <asp:Login ID="LoginUser" runat="server" EnableViewState="false" RenderOuterTable="false"
                                OnAuthenticate="Login_User">
                                <LayoutTemplate>
                                    <span class="alert-text-login">
                                        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                    </span>
                                    <%--<asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                                    ValidationGroup="LoginUserValidationGroup" />--%>
                                    <div id="inputs">
                                        <span>
                                            <asp:TextBox ID="username" runat="server" CssClass="input_box"></asp:TextBox></span>
                                        <span>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                ErrorMessage="User Name is required." CssClass="alert-text-login" ToolTip="User Name is required."
                                                ValidationGroup="LoginUserValidationGroup">User Name is Required.</asp:RequiredFieldValidator></span>
                                        <span>
                                            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="input_box"></asp:TextBox></span>
                                        <span>
                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                CssClass="alert-text-login" ErrorMessage="Password is required." ToolTip="Password is required."
                                                ValidationGroup="LoginUserValidationGroup">Password is Required.</asp:RequiredFieldValidator></span>
                                        <div id="actions">
                                            <div class="user_action">
                                                <span>
                                                    <asp:CheckBox ID="RememberMe" runat="server" Checked="true" />
                                                    <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Remember me</asp:Label></span>
                                                <span><a href="ForgotPassword.aspx">Forgot your password?</a></span>
                                            </div>
                                            <div class="log_btn">
                                                <asp:Button ID="submit" runat="server" CommandName="Login" Text="Log In" ValidationGroup="LoginUserValidationGroup" />
                                            </div>
                                        </div>
                                    </div>
                                </LayoutTemplate>
                            </asp:Login>
                            <table id="tblHidden" style="display: none;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnYes" runat="server" Text="Button" OnClick="btnYes_Click" />
                                        <asp:Button ID="btnNo" runat="server" Text="Button" OnClick="btnNo_Click" />
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                        <div id="contact-content">
                            <h3>
                                Support</h3>
                            <p>
                                <strong>N. America:</strong> +1 (866) 882-4749 <br />
                                <strong>Worldwide :</strong> +1 (713) 378-2700 <br />
                                <strong>E-mail:</strong> <a href="mailto:softwaresupport@univ-wea.com">softwaresupport@univ-wea.com</a></p>
                        </div>
                    </div>
                </div>
                <div id="MktAppSectionTwoWrapper">
                    <div id="MktAppSectionTwo">
                        <iframe id="MktAppSectionTwoFrame" src="../Login/marketing/mkt-app-section-two.html">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footerCopy">
        <p>
            &copy; <asp:Label ID="lbYear" runat="server"></asp:Label> Universal Weather and Aviation, Inc.</p>
    </div>
    <!-- Google Analytics BEGIN -->
    <script type="text/javascript">
        function BeNiceIFrame(iframeid) {
                   var OldIframe = document.getElementById(iframeid);
            var NewIframe = OldIframe.cloneNode(true);
            //no scrollbars - overflow:hidden works for other browsers
            NewIframe.setAttribute("scrolling", "no");
            // IE8 and lower add a special border on frames.
            // It's actually in addition to the css border property.
            if (!document.addEventListener)
                NewIframe.setAttribute("frameBorder", 0);
            OldIframe.parentNode.replaceChild(NewIframe, OldIframe);
        }
    </script>
    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-2761521-1");
            pageTracker._trackPageview();
        } catch (err) { }
    </script>
    <!-- Google Analytics END -->
    <script type="text/javascript">
        BeNiceIFrame("MktAppSectionOneFrame");
        BeNiceIFrame("MktAppSectionTwoFrame");
    </script>

</body>
</html>
