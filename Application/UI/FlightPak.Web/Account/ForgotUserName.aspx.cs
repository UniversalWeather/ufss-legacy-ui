﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Account
{
    public partial class ForgotUserName : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void submitButton_Click(object sender, EventArgs e)
        {
            using (AuthenticationService.AuthenticationServiceClient authService = new AuthenticationService.AuthenticationServiceClient())
            {
                Message.InnerText = authService.GetUserNameByEmail(emailTextbox.Text, string.Empty, string.Empty);
            }
        }
    }
}