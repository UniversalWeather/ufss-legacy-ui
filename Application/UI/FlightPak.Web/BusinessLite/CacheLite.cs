﻿using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.BusinessLite
{
    public class CacheLite
    {
        public void AddPassportToCache(PaxPassportViewModel Pax)
        {
            if (HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] = new Dictionary<long, PaxPassportViewModel>();
            }
            Dictionary<long, PaxPassportViewModel> passports = (Dictionary<long, PaxPassportViewModel>)HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache];
            passports[Pax.PassportID] = Pax;
            HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] = passports;
        }
        public PaxPassportViewModel GetPassportFromCache(long PassportID)
        {
            if (HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] = new Dictionary<long, PaxPassportViewModel>();
            }            
            Dictionary<long, PaxPassportViewModel> passports = (Dictionary<long, PaxPassportViewModel>)HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache];
            if (passports.ContainsKey(PassportID))
                return passports[PassportID];
            else
                return null;
        }

        public void RemovePaxPassportFromCache(long PassportID)
        {
            if (HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache] = new Dictionary<long, PaxPassportViewModel>();
            }
            Dictionary<long, PaxPassportViewModel> passports = (Dictionary<long, PaxPassportViewModel>)HttpContext.Current.Session[WebSessionKeys.PassengerPassportCache];
            if (passports.ContainsKey(PassportID))
                passports.Remove(PassportID);            
        }



        public void AddCountryCDToCache(String CountryCD,long CountryID)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CountryCDCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CountryCDCache] = new Dictionary<long, PaxPassportViewModel>();
            }
            Dictionary<long, string> countries = (Dictionary<long, string>)HttpContext.Current.Session[WebSessionKeys.CountryCDCache];
            countries[CountryID] = CountryCD;
            HttpContext.Current.Session[WebSessionKeys.CountryCDCache] = countries;
        }
        public string GetCountryCDFromCache(long CountryID)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CountryCDCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CountryCDCache] = new Dictionary<long, string>();
            }
            Dictionary<long, string> countries = (Dictionary<long, string>)HttpContext.Current.Session[WebSessionKeys.CountryCDCache];
            if (countries.ContainsKey(CountryID))
                return countries[CountryID];
            else
                return null;
        }

        public FlightPakMasterService.ReturnValueOfCountry GetAllCountryMasterList()
        {
            if (HttpContext.Current.Session[WebSessionKeys.CountryMasterListCache] == null)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient CountryService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objCountryVal = CountryService.GetCountryMasterList();
                    HttpContext.Current.Session[WebSessionKeys.CountryMasterListCache] = objCountryVal;
                }
               
            }
            return (FlightPakMasterService.ReturnValueOfCountry)HttpContext.Current.Session[WebSessionKeys.CountryMasterListCache];      
        }
        public FlightPakMasterService.ReturnValueOfGetCrewByCrewID GetCrewDetailByCrewID(long crewID)
        {
            Dictionary<long, FlightPakMasterService.ReturnValueOfGetCrewByCrewID> crewDetailObject;
            if (HttpContext.Current.Session[WebSessionKeys.CrewDetailCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CrewDetailCache] = new Dictionary<long, FlightPakMasterService.ReturnValueOfGetCrewByCrewID>();
            }
            crewDetailObject = (Dictionary<long, FlightPakMasterService.ReturnValueOfGetCrewByCrewID>)HttpContext.Current.Session[WebSessionKeys.CrewDetailCache];

            if (crewDetailObject.ContainsKey(crewID))
            {
                return crewDetailObject[crewID];
            }
            else 
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    crewDetailObject.Add(crewID, objDstsvc.GetCrewbyCrewId(crewID));
                    HttpContext.Current.Session[WebSessionKeys.CrewDetailCache] = crewDetailObject;
                }
                return crewDetailObject[crewID];
            }
        }

        public void RemoveCrewDetailFromCache(long crewID)
        {
            Dictionary<long, FlightPakMasterService.ReturnValueOfGetCrewByCrewID> crewDetailObject;
            if (HttpContext.Current.Session[WebSessionKeys.CrewDetailCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CrewDetailCache] = new Dictionary<long, FlightPakMasterService.ReturnValueOfGetCrewByCrewID>();
            }
            crewDetailObject = (Dictionary<long, FlightPakMasterService.ReturnValueOfGetCrewByCrewID>)HttpContext.Current.Session[WebSessionKeys.CrewDetailCache];
            if (crewDetailObject.ContainsKey(crewID))
            {
                crewDetailObject.Remove(crewID);
            }
        }

        public FlightPakMasterService.ReturnValueOfGetAllCrewPassport GetCrewPassportFromCache(FlightPak.Web.FlightPakMasterService.CrewPassengerPassport crewPassPortObject)
        {
            Dictionary<long, FlightPakMasterService.ReturnValueOfGetAllCrewPassport> crewPassportListObject;
            if (HttpContext.Current.Session[WebSessionKeys.CrewPassportCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CrewPassportCache] = new Dictionary<long, FlightPakMasterService.ReturnValueOfGetAllCrewPassport>();
            }
            crewPassportListObject = (Dictionary<long, FlightPakMasterService.ReturnValueOfGetAllCrewPassport>)HttpContext.Current.Session[WebSessionKeys.CrewPassportCache];

            if (crewPassportListObject.ContainsKey(Convert.ToInt64(crewPassPortObject.CrewID)))
                return crewPassportListObject[Convert.ToInt64(crewPassPortObject.CrewID)];
            else
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    objDstsvc.GetCrewPassportListInfo(crewPassPortObject);
                    if (crewPassPortObject.CrewID != null)
                        crewPassportListObject.Add(Convert.ToInt64(crewPassPortObject.CrewID), objDstsvc.GetCrewPassportListInfo(crewPassPortObject));
                    HttpContext.Current.Session[WebSessionKeys.CrewPassportCache] = crewPassportListObject;
                }
                return crewPassportListObject[Convert.ToInt64(crewPassPortObject.CrewID)];
            }
        }

        public void RemoveCrewPassportFromCache(long crewID)
        {
            Dictionary<long, FlightPakMasterService.ReturnValueOfGetAllCrewPassport> crewPassportListObject;
            if (HttpContext.Current.Session[WebSessionKeys.CrewPassportCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.CrewPassportCache] = new Dictionary<long, FlightPakMasterService.ReturnValueOfGetAllCrewPassport>();
            }
            crewPassportListObject = (Dictionary<long, FlightPakMasterService.ReturnValueOfGetAllCrewPassport>)HttpContext.Current.Session[WebSessionKeys.CrewPassportCache];
            if (crewPassportListObject.ContainsKey(crewID))
                crewPassportListObject.Remove(crewID);
        }

        public void AddFlightPurposeListToCache(Dictionary<string,string> FlightPurposeList)
        {
            if (HttpContext.Current.Session[WebSessionKeys.FlightPurposeListCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.FlightPurposeListCache] = new Dictionary<string,string>();
            }            
            HttpContext.Current.Session[WebSessionKeys.FlightPurposeListCache]= FlightPurposeList;

        }
        public Dictionary<string,string> GetFlightpurposeListFromCache()
        {
            if (HttpContext.Current.Session[WebSessionKeys.FlightPurposeListCache] == null)
            {
                HttpContext.Current.Session[WebSessionKeys.FlightPurposeListCache] = new Dictionary<string, string>();
            }
            Dictionary<string, string> flightPurposes = (Dictionary<string, string>)HttpContext.Current.Session[WebSessionKeys.FlightPurposeListCache];
            return flightPurposes;
        }

    }
}