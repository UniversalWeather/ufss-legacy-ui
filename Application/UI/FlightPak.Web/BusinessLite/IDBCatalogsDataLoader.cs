﻿using FlightPak.Web.ViewModels;

namespace FlightPak.Web.BusinessLite
{
    public interface IDBCatalogsDataLoader
    {
        CrewDutyRuleViewModel GetCrewDutyRulesInfo_FromFilter_I(long CrewDutyRulesID, string CrewDutyRulesCD);
    }
}