﻿using System;
using System.Collections.Generic;
using System.Linq;
using Omu.ValueInjecter;
using FlightPak.Web.PostflightService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.MasterData;
using FlightPak.Web.ViewModels.Postflight;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.PreflightService;
using FlightPak.Web.CalculationService;
using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Constants;
namespace FlightPak.Web.BusinessLite
{
    public class DBCatalogsDataLoader : IDBCatalogsDataLoader
    {
        #region Homebase Information
        public static HomebaseViewModel GetHomebaseInfo_FromHomebaseId(long homebaseId)
        {
            HomebaseViewModel homebaseVM = new HomebaseViewModel();

            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getHomebases = masterService.GetCompanyWithFilters(String.Empty, homebaseId, true, true);
                if (getHomebases != null && getHomebases.EntityList != null)
                {
                    var homebaseInfo = getHomebases.EntityList.FirstOrDefault();
                    if (homebaseInfo != null)
                    {
                        homebaseVM = (HomebaseViewModel)homebaseVM.InjectFrom(homebaseInfo);
                        homebaseVM.HomebaseAirport = GetAirportInfo_FromAirportId(homebaseInfo.HomebaseAirportID.Value);
                    }
                }
                return homebaseVM;
            }
        }

        public static HomebaseViewModel GetHomebaseInfo_FromHomebaseCD(String homebaseCD)
        {
            HomebaseViewModel homebaseVM = new HomebaseViewModel() { HomebaseCD = homebaseCD };
            if (String.IsNullOrWhiteSpace(homebaseCD))
                return homebaseVM;
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getHomebases = masterService.GetCompanyWithFilters(homebaseCD.Trim().ToUpper(), 0, true, true);
                if (getHomebases != null && getHomebases.EntityList != null)
                {
                    var homebaseInfo = getHomebases.EntityList.FirstOrDefault();
                    if (homebaseInfo != null)
                    {
                        homebaseVM = (HomebaseViewModel)homebaseVM.InjectFrom(homebaseInfo);
                        homebaseVM.HomebaseAirport = GetAirportInfo_FromAirportId(homebaseInfo.HomebaseAirportID.Value);
                    }
                }
                return homebaseVM;
            }
        }
        #endregion

        #region Fleet Information
        public static FleetViewModel GetFleetInfo_FromFleetId(long fleetId)
        {
            FleetViewModel fleetVM = new FleetViewModel();

            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getFleetInfo = masterService.GetFleetIDInfo(fleetId);
                if (getFleetInfo != null && getFleetInfo.EntityList != null)
                {
                    var fleetInfo = getFleetInfo.EntityList.FirstOrDefault();
                    if (fleetInfo != null)
                    {
                        fleetVM = (FleetViewModel)fleetVM.InjectFrom(fleetInfo);
                    }
                }
                return fleetVM;
            }
        }

        public static List<GetPOFleetProfile> GetFleetProfileSettings(long FleetID)
        {
            using (CalculationServiceClient CalcService = new CalculationServiceClient())
            {
                // Get Fleet Profile Settings
                var fleetSettings = CalcService.GetFleetProfile(FleetID).EntityList;
                return fleetSettings;
            }
        }

        #endregion

        #region Account information
        public static AccountViewModel GetAccountInfo_FromAccountId(long accountId)
        {
            AccountViewModel accountVM = new AccountViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getAccountWithFilters = masterService.GetAccountWithFilters(String.Empty, accountId, String.Empty, false);
                if (getAccountWithFilters != null && getAccountWithFilters.EntityList != null)
                {
                    var getAccount = getAccountWithFilters.EntityList.FirstOrDefault();
                    if (getAccount != null)
                    {
                        accountVM = (AccountViewModel)accountVM.InjectFrom(getAccount);
                    }
                }
            }

            return accountVM;
        }

        public static AccountViewModel GetAccountInfo_FromAccountNo(String accountNumber)
        {
            AccountViewModel accountVM = new AccountViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getAccountWithFilters = masterService.GetAccountWithFilters(accountNumber, 0, String.Empty, false);
                if (getAccountWithFilters != null && getAccountWithFilters.EntityList != null)
                {
                    var getAccount = getAccountWithFilters.EntityList.FirstOrDefault();
                    if (getAccount != null)
                    {
                        accountVM = (AccountViewModel)accountVM.InjectFrom(getAccount);
                    }
                }
            }

            return accountVM;
        }
        #endregion

        #region Flight Category information
        public static FlightCatagoryViewModel GetFlightCategoryInfo_FromId(long flightCategoryId)
        {
            FlightCatagoryViewModel returnFlightCategoryVM = new FlightCatagoryViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var returnFlightCategory = masterService.GetFlightCategoryList();
                if (returnFlightCategory != null && returnFlightCategory.EntityList != null)
                {
                    var flightCategory = returnFlightCategory.EntityList.Where(fc => fc.FlightCategoryID == flightCategoryId).FirstOrDefault();
                    if (flightCategory != null)
                    {
                        returnFlightCategoryVM = (FlightCatagoryViewModel)returnFlightCategoryVM.InjectFrom(flightCategory);
                    }
                }
            }

            return returnFlightCategoryVM;
        }

        public static List<RoomDescriptionViewModel> RetrieveRoomDescription()
        {
            List<RoomDescriptionViewModel> roomDescriptionList = new List<RoomDescriptionViewModel>();
            using (MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = masterService.GetRoomDescriptionList();
                if (objRetVal.ReturnFlag)
                {
                    roomDescriptionList = objRetVal.EntityList.Select(t => new RoomDescriptionViewModel() { RoomDescription = t.RoomDesc, RoomDescCode = t.RoomDescCode }).ToList();
                }
            }
            return roomDescriptionList;
        }

        public static FlightCatagoryViewModel GetFlightCategoryInfo_FromCD(String flightCategoryCD)
        {
            FlightCatagoryViewModel returnFlightCategoryVM = new FlightCatagoryViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var returnFlightCategory = masterService.GetFlightCategoryList();
                if (returnFlightCategory != null && returnFlightCategory.EntityList != null)
                {
                    var flightCategory = returnFlightCategory.EntityList.Where(fc => fc.FlightCatagoryCD.ToUpper() == flightCategoryCD.ToUpper()).FirstOrDefault();
                    if (flightCategory != null)
                    {
                        returnFlightCategoryVM = (FlightCatagoryViewModel)returnFlightCategoryVM.InjectFrom(flightCategory);
                    }
                }
            }

            return returnFlightCategoryVM;
        }

        #endregion

        #region Crew Information
        public static CrewViewModel GetCrewInfo_FromCrewId(long crewId)
        {
            CrewViewModel crewVM = new CrewViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var returnCrewInfo = masterService.GetCrewbyCrewId(crewId);
                if (returnCrewInfo != null && returnCrewInfo.EntityList != null)
                {
                    var crewByCrewId = returnCrewInfo.EntityList.FirstOrDefault();
                    if (crewByCrewId != null)
                    {
                        crewVM = (CrewViewModel)crewVM.InjectFrom(crewByCrewId);
                    }
                }
            }
            return crewVM;
        }

        public static CrewViewModel GetCrewInfo_FromCrewCD(String crewCD)
        {
            CrewViewModel crewVM = new CrewViewModel();
            using (PreflightServiceClient preflightService = new PreflightServiceClient())
            {
                var returnCrewInfo = preflightService.GetCrewBYIDOrCD(crewCD, 0);
                if (returnCrewInfo != null && returnCrewInfo.EntityList != null)
                {
                    var crewByCrewId = returnCrewInfo.EntityList.FirstOrDefault();
                    if (crewByCrewId != null)
                    {
                        crewVM = (CrewViewModel)crewVM.InjectFrom(crewByCrewId);
                    }
                }
            }
            return crewVM;
        }
        #endregion

        #region GetAirport
        public static AirportViewModel GetAirportInfo_FromAirportId(long airportId)
        {
            AirportViewModel airportVM = new AirportViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetList = masterService.GetAirportByAirportID(airportId);
                if (objRetList != null && objRetList.EntityList != null)
                {
                    var objRetGetAirport = objRetList.EntityList.FirstOrDefault();
                    if (objRetGetAirport != null && objRetGetAirport.AirportID != 0)
                    {
                        airportVM = (AirportViewModel)airportVM.InjectFrom(objRetGetAirport);
                    }
                }
            }
            return airportVM;
        }
        public static AirportViewModel GetAirportInfo_FromICAO(String ICAO)
        {
            AirportViewModel airportVM = new AirportViewModel();
            if (string.IsNullOrWhiteSpace(ICAO))
                return airportVM;

            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetList = masterService.GetAirportByAirportICaoID(ICAO.ToUpper());
                if (objRetList != null && objRetList.EntityList != null)
                {
                    var objRetGetAirport = objRetList.EntityList.FirstOrDefault();
                    if (objRetGetAirport != null && objRetGetAirport.AirportID != 0)
                    {
                        airportVM = (AirportViewModel)airportVM.InjectFrom(objRetGetAirport);
                    }
                }
            }
            return airportVM;
        }
        #endregion

        #region Get FBO
        public static FBOViewModel GetFBOInfo_FromFBOId(long airportId, long fboId)
        {
            FBOViewModel fboVM = new FBOViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRet = masterService.GetFBOByAirportWithFilters(airportId, fboId, string.Empty, false, false, false);
                if (objRet != null && objRet.EntityList != null)
                {
                    var fboInf = objRet.EntityList.FirstOrDefault();
                    if (fboInf != null)
                    {
                        fboVM = (FBOViewModel)fboVM.InjectFrom(fboInf);
                    }
                }
            }
            return fboVM;
        }

        public static FBOViewModel GetFBOInfo_FromFBOCD(String FBO, long airportId)
        {
            FBOViewModel fboVM = new FBOViewModel();
            if (String.IsNullOrWhiteSpace(FBO))
                return fboVM;
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRet = masterService.GetFBOByAirportWithFilters(airportId, 0, FBO.ToUpper(), false, false, false);
                if (objRet != null && objRet.EntityList != null)
                {
                    var fboInf = objRet.EntityList.FirstOrDefault();
                    if (fboInf != null)
                    {
                        fboVM = (FBOViewModel)fboVM.InjectFrom(fboInf);
                    }
                }
            }
            return fboVM;
        }

        public static FBOViewModel GetFBOChoice_ForAirportId(long airportId)
        {
            FBOViewModel fboVM = new FBOViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRet = masterService.GetAllFBOByAirportID(airportId);
                if (objRet != null && objRet.EntityList != null && objRet.EntityList.Count() > 0)
                {
                    var fboInf = objRet.EntityList.Where(fb => fb.IsChoice.HasValue && fb.IsChoice.Value).FirstOrDefault();
                    if (fboInf != null)
                    {
                        fboVM = (FBOViewModel)fboVM.InjectFrom(fboInf);
                    }
                }
            }
            return fboVM;
        }

        #endregion

        #region Payment type
        public static PaymentTypeViewModel GetPaymentTypeInfo_FromPaymentTypeId(long paymentTypeId)
        {
            PaymentTypeViewModel payTypeVM = new PaymentTypeViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objPayTypeRet = masterService.GetAllPaymentTypeWithFilters(paymentTypeId, String.Empty, false);
                if (objPayTypeRet != null && objPayTypeRet.EntityList != null)
                {
                    var objPaymentType = objPayTypeRet.EntityList.FirstOrDefault();
                    if (objPaymentType != null)
                    {
                        payTypeVM = (PaymentTypeViewModel)payTypeVM.InjectFrom(objPaymentType);
                    }
                }
            }
            return payTypeVM;
        }

        public static List<PaymentTypeViewModel> GetPaymentTypeInfo_List(bool showActiveOnly)
        {
            List<PaymentTypeViewModel> paymentTypesVMList = new List<PaymentTypeViewModel>();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objPaymentTypes = masterService.GetPaymentType();
                if (objPaymentTypes.ReturnFlag == true)
                {
                    var paymentTypesList = objPaymentTypes.EntityList;
                    foreach (FlightPakMasterService.PaymentType pymt in paymentTypesList)
                    {
                        if (showActiveOnly == false || (showActiveOnly && pymt.IsInActive == false))
                        {
                            PaymentTypeViewModel payTypeVM = new PaymentTypeViewModel();
                            payTypeVM = (PaymentTypeViewModel)payTypeVM.InjectFrom(pymt);
                            paymentTypesVMList.Add(payTypeVM);
                        }
                    }
                }
            }
            return paymentTypesVMList;
        }

        public static PaymentTypeViewModel GetPaymentTypeInfo_FromId(long paymentId)
        {
            List<PaymentTypeViewModel> listPayments = GetPaymentTypeInfo_List(false);
            PaymentTypeViewModel payType = new PaymentTypeViewModel();
            foreach (PaymentTypeViewModel pt in listPayments)
            {
                if (pt.PaymentTypeID == paymentId)
                {
                    payType = (PaymentTypeViewModel)payType.InjectFrom(pt);
                    break;
                }
            }
            return payType;
        }

        public static PaymentTypeViewModel GetPaymentTypeInfo_FromPaymentTypeCD(String paymentTypeCD)
        {
            PaymentTypeViewModel payTypeVM = new PaymentTypeViewModel();
            if (String.IsNullOrWhiteSpace(paymentTypeCD))
                return payTypeVM;
            paymentTypeCD = paymentTypeCD.ToUpper();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objPayTypeRet = masterService.GetAllPaymentTypeWithFilters(0, paymentTypeCD.ToUpper(), false);
                if (objPayTypeRet != null && objPayTypeRet.EntityList != null)
                {
                    var objPaymentType = objPayTypeRet.EntityList.FirstOrDefault();
                    if (objPaymentType != null)
                    {
                        payTypeVM = (PaymentTypeViewModel)payTypeVM.InjectFrom(objPaymentType);
                    }
                }
            }
            return payTypeVM;
        }

        public static FSSOperationResult<bool> DeletePaymentType(PaymentTypeViewModel paymentTypeVM)
        {
            FSSOperationResult<bool> deleteResult = new FSSOperationResult<bool>();
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                var returnValue = commonService.Lock(EntitySet.Database.PaymentType, paymentTypeVM.PaymentTypeID);
                if (!returnValue.ReturnFlag)
                {
                    deleteResult.Result = false;
                    deleteResult.ErrorsList.Add(returnValue.LockMessage);
                }
                using (FlightPakMasterService.MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.PaymentType payType = new FlightPakMasterService.PaymentType();
                    payType = (FlightPakMasterService.PaymentType)payType.InjectFrom(paymentTypeVM);
                    payType.IsDeleted = true;
                    deleteResult.StatusCode = System.Net.HttpStatusCode.OK;
                    try
                    {
                        ReturnValueOfPaymentType retV = masterService.DeletePaymentType(payType);
                        deleteResult.Result = retV.ReturnFlag;
                        deleteResult.Success = retV.ReturnFlag;
                    }
                    catch (Exception ex)
                    {
                        deleteResult.Result = false;
                        if (!String.IsNullOrWhiteSpace(ex.Message) && ex.Message == DataValidation.DeletionNotAllowed)
                            deleteResult.ErrorsList.Add(ex.Message);
                    }

                }
                var returnValue1 = commonService.UnLock(EntitySet.Database.PaymentType, paymentTypeVM.PaymentTypeID);

            }
            return deleteResult;
        }
        #endregion

        #region Payment Vendor
        public static PayableVendorViewModel GetPayableVendorInfo_FromVendorId(long vendorId)
        {
            PayableVendorViewModel payVendorVM = new PayableVendorViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetPayVendors = masterService.GetPayableVendorInfo();
                if (objRetPayVendors != null && objRetPayVendors.EntityList != null)
                {
                    var objPayVendor = objRetPayVendors.EntityList.Where(vend => vend.VendorID == vendorId).FirstOrDefault();
                    if (objPayVendor != null)
                        payVendorVM = (PayableVendorViewModel)payVendorVM.InjectFrom(objPayVendor);
                }
            }
            return payVendorVM;
        }

        public static PayableVendorViewModel GetPayableVendorInfo_FromVendorCD(String vendorCD)
        {
            PayableVendorViewModel payVendorVM = new PayableVendorViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetPayVendors = masterService.GetPayableVendorInfo();
                if (objRetPayVendors != null && objRetPayVendors.EntityList != null)
                {
                    var objPayVendor = objRetPayVendors.EntityList.Where(vend => vend.VendorCD == vendorCD).FirstOrDefault();
                    if (objPayVendor != null)
                        payVendorVM = (PayableVendorViewModel)payVendorVM.InjectFrom(objPayVendor);
                }
            }
            return payVendorVM;
        }

        public static List<PayableVendorViewModel> GetPayableVendorList(bool showActiveOnly, String homebase)
        {
            List<PayableVendorViewModel> listVendors = new List<PayableVendorViewModel>();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var vendorsWithFilters = masterService.GetAllVendorWithFilters(String.Empty, 0, String.Empty, homebase, showActiveOnly);
                if (vendorsWithFilters.ReturnFlag == true)
                {
                    List<GetAllVendorWithFilters> vendors = vendorsWithFilters.EntityList;
                    foreach (var vendor in vendorsWithFilters.EntityList)
                    {
                        PayableVendorViewModel vendorVM = new PayableVendorViewModel();
                        vendorVM = (PayableVendorViewModel)vendorVM.InjectFrom(vendor);
                        listVendors.Add(vendorVM);
                    }
                }
            }

            return listVendors;
        }

        public static FSSOperationResult<bool> DeletePayableVendor(PayableVendorViewModel payableVendor)
        {
            FSSOperationResult<bool> deleteResult = new FSSOperationResult<bool>();
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                var returnValue = commonService.Lock(EntitySet.Database.Vendor, payableVendor.VendorID);
                if (!returnValue.ReturnFlag)
                {
                    deleteResult.Result = false;
                    deleteResult.ErrorsList.Add(returnValue.LockMessage);
                }
                using (FlightPakMasterService.MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Vendor payType = new FlightPakMasterService.Vendor();
                    payType = (FlightPakMasterService.Vendor)payType.InjectFrom(payableVendor);
                    payType.IsDeleted = true;
                    deleteResult.StatusCode = System.Net.HttpStatusCode.OK;
                    try
                    {
                        ReturnValueOfVendor retV = masterService.DeletePayableVendorMaster(payType);
                        deleteResult.Result = retV.ReturnFlag;
                        deleteResult.Success = retV.ReturnFlag;
                    }
                    catch (Exception ex)
                    {
                        deleteResult.Result = false;
                        if (!String.IsNullOrWhiteSpace(ex.Message) && ex.Message == DataValidation.DeletionNotAllowed)
                            deleteResult.ErrorsList.Add(ex.Message);
                    }
                }
                var returnValue1 = commonService.UnLock(EntitySet.Database.PaymentType, payableVendor.VendorID);

            }
            return deleteResult;
        }
        #endregion

        #region Fuel Locator
        public static FuelLocatorViewModel GetFuelLocatorInfo_FromLocatorId(long locatorId)
        {
            FuelLocatorViewModel fuelLocatorVM = new FuelLocatorViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetAllFuelLocator = masterService.GetFuelLocatorWithFilters(locatorId, String.Empty, 0, false);
                if (objRetAllFuelLocator != null && objRetAllFuelLocator.EntityList != null)
                {
                    var fuelLocator = objRetAllFuelLocator.EntityList.FirstOrDefault();
                    if (fuelLocator != null)
                    {
                        fuelLocatorVM = (FuelLocatorViewModel)fuelLocatorVM.InjectFrom(fuelLocator);
                    }
                }
            }
            return fuelLocatorVM;
        }

        public static FuelLocatorViewModel GetFuelLocatorInfo_FromLocatorCD(string fuelLocatorCD)
        {
            FuelLocatorViewModel fuelLocatorVM = new FuelLocatorViewModel();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetAllFuelLocator = masterService.GetFuelLocatorWithFilters(0, fuelLocatorCD, 0, false);
                if (objRetAllFuelLocator != null && objRetAllFuelLocator.EntityList != null)
                {
                    var fuelLocator = objRetAllFuelLocator.EntityList.FirstOrDefault();
                    if (fuelLocator != null)
                    {
                        fuelLocatorVM = (FuelLocatorViewModel)fuelLocatorVM.InjectFrom(fuelLocator);
                    }
                }
            }
            return fuelLocatorVM;
        }


        public static List<FuelLocatorViewModel> GetFuelLocatorList(bool activeOnly)
        {
            List<FuelLocatorViewModel> fuelLocatorVMList = new List<FuelLocatorViewModel>();
            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var objRetAllFuelLocator = masterService.GetFuelLocatorWithFilters(0, String.Empty, 0, activeOnly);
                if (objRetAllFuelLocator != null && objRetAllFuelLocator.EntityList != null)
                {
                    foreach (var fuelLocator in objRetAllFuelLocator.EntityList)
                    {
                        FuelLocatorViewModel fuelLocatorVM = new FuelLocatorViewModel();
                        fuelLocatorVM = (FuelLocatorViewModel)fuelLocatorVM.InjectFrom(fuelLocator);
                        fuelLocatorVMList.Add(fuelLocatorVM);
                    }
                }
            }
            return fuelLocatorVMList;
        }

        public static FSSOperationResult<bool> DeleteFuelLocator(FuelLocatorViewModel fuelLocatorViewModel)
        {
            FSSOperationResult<bool> deleteResult = new FSSOperationResult<bool>();
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                var returnValue = commonService.Lock(EntitySet.Database.FuelLocator, fuelLocatorViewModel.FuelLocatorID);
                if (!returnValue.ReturnFlag)
                {
                    deleteResult.Result = false;
                    deleteResult.ErrorsList.Add(returnValue.LockMessage);
                }
                using (FlightPakMasterService.MasterCatalogServiceClient masterService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.FuelLocator fuelLocator = new FlightPakMasterService.FuelLocator();
                    fuelLocator = (FlightPakMasterService.FuelLocator)fuelLocator.InjectFrom(fuelLocatorViewModel);
                    fuelLocator.IsDeleted = true;
                    deleteResult.StatusCode = System.Net.HttpStatusCode.OK;
                    try
                    {
                        ReturnValueOfFuelLocator retV = masterService.DeleteFuelLocator(fuelLocator);
                        deleteResult.Result = retV.ReturnFlag;
                        deleteResult.Success = retV.ReturnFlag;
                    }
                    catch (Exception ex)
                    {
                        deleteResult.Result = false;
                        if (!String.IsNullOrWhiteSpace(ex.Message) && ex.Message == DataValidation.DeletionNotAllowed)
                            deleteResult.ErrorsList.Add(ex.Message);
                    }

                }
                var returnValue1 = commonService.UnLock(EntitySet.Database.FuelLocator, fuelLocatorViewModel.FuelLocatorID);

            }
            return deleteResult;
        }
        #endregion

        #region Crew Duty Rules
        public static CrewDutyRuleViewModel GetCrewDutyRulesInfo_FromFilter(long CrewDutyRulesID, string CrewDutyRulesCD)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRulesID))
            {
                CrewDutyRuleViewModel dutyRules = new CrewDutyRuleViewModel();
                if (CrewDutyRulesID > 0 || !String.IsNullOrEmpty(CrewDutyRulesCD))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RulesVal = Service.GetCrewDutyRulesWithFilters(CrewDutyRulesID, CrewDutyRulesCD, false).EntityList;
                        if (RulesVal != null && RulesVal.Count > 0)
                        {
                            dutyRules = (CrewDutyRuleViewModel)dutyRules.InjectFrom(RulesVal[0]);
                        }

                    }
                }
                return dutyRules;
            }
        }
        public CrewDutyRuleViewModel GetCrewDutyRulesInfo_FromFilter_I(long CrewDutyRulesID, string CrewDutyRulesCD)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRulesID))
            {
                CrewDutyRuleViewModel dutyRules = new CrewDutyRuleViewModel();
                if (CrewDutyRulesID > 0 || !String.IsNullOrEmpty(CrewDutyRulesCD))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var RulesVal = Service.GetCrewDutyRulesWithFilters(CrewDutyRulesID, CrewDutyRulesCD, false).EntityList;
                        if (RulesVal != null && RulesVal.Count > 0)
                        {
                            dutyRules = (CrewDutyRuleViewModel)dutyRules.InjectFrom(RulesVal[0]);
                        }

                    }
                }
                return dutyRules;
            }
        }
        #endregion

        #region Delay Type
        public static PostflightLegDelayTypeViewModel GetDelayTypeInfo_FromFilter(long DelayTypeID, string DelayTypeCD)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DelayTypeCD))
            {
                PostflightLegDelayTypeViewModel delayType = new PostflightLegDelayTypeViewModel();
                if (!string.IsNullOrEmpty(DelayTypeCD) || DelayTypeID > 0)
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient DelayService = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        var DelayVal = DelayService.GetAllDelayTypeWithFilters(DelayTypeID, DelayTypeCD, false).EntityList;
                        if (DelayVal != null && DelayVal.Count > 0)
                        {
                            delayType = (PostflightLegDelayTypeViewModel)delayType.InjectFrom(DelayVal[0]);
                        }

                    }
                }
                return delayType;
            }
        }
        #endregion


        #region "Flight Purpose"
        public static List<FlightPurposeViewModel> GetAllFlightpurposeList()
        {
            List<FlightPurposeViewModel> flightPurposeList = new List<FlightPurposeViewModel>();
            // Get Flight Purpose List from Master
            using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var retVal = Service.GetFlightPurposeList();
                if (retVal != null && retVal.ReturnFlag == true)
                {
                    retVal.EntityList.ForEach(x => { flightPurposeList.Add((FlightPurposeViewModel)(new FlightPurposeViewModel().InjectFrom(x))); });
                    return flightPurposeList;
                }
            }
            return flightPurposeList;
        }
        #endregion

        #region Aircraft
        public static AircraftViewModel GetAircraftInfo_FromAircraftCD(string aircraftCD)
        {
            AircraftViewModel aircraftVM = new AircraftViewModel();

            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getAircraftInfo = masterService.GetAircraftWithFilters(aircraftCD, 0, false);
                if (getAircraftInfo != null && getAircraftInfo.EntityList != null)
                {
                    var AircraftInfo = getAircraftInfo.EntityList.FirstOrDefault();
                    if (AircraftInfo != null)
                    {
                        aircraftVM = (AircraftViewModel)aircraftVM.InjectFrom(AircraftInfo);
                    }
                }
                return aircraftVM;
            }
        }
        #endregion

        #region Aircraft
        public static CrewDutyTypeViewModel GetCrewDutyTypeInfo_FromDutyTypeCD(string dutyTypeCD)
        {
            CrewDutyTypeViewModel dutyTypeCDVM = new CrewDutyTypeViewModel();

            using (MasterCatalogServiceClient masterService = new MasterCatalogServiceClient())
            {
                var getDutyTypeInfo = masterService.GetCrewDutyTypeList();
                if (getDutyTypeInfo != null && getDutyTypeInfo.EntityList != null)
                {
                    var dutyTypeInfo = getDutyTypeInfo.EntityList.Where(x => x.DutyTypeCD.ToUpper().Trim().Equals(dutyTypeCD.ToUpper().Trim())).FirstOrDefault();
                    if (dutyTypeInfo != null)
                    {
                        dutyTypeCDVM = (CrewDutyTypeViewModel)dutyTypeCDVM.InjectFrom(dutyTypeInfo);
                    }
                }
                return dutyTypeCDVM;
            }
        }
        #endregion

        #region Client
        public static ClientViewModel GetClientInfo_FromClientCD(string ClientCD)
        {
            ClientViewModel clientVM = new ClientViewModel();
            if (string.IsNullOrWhiteSpace(ClientCD))
                return clientVM;

            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                var objRetVal = objDstsvc.GetClientWithFilters(0, ClientCD.ToUpper().Trim(), false);
                if (objRetVal.ReturnFlag)
                {
                    ClientList = objRetVal.EntityList;
                    if (ClientList != null && ClientList.Count > 0)
                    {
                        clientVM = (ClientViewModel)clientVM.InjectFrom(ClientList[0]);
                    }
                }
            }
            return clientVM;
        }
        public static ClientViewModel GetClientInfo_FromClientID(long ClientID)
        {
            ClientViewModel clientVM = new ClientViewModel();
            if (ClientID == 0)
                return clientVM;

            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<FlightPak.Web.FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();
                var objRetVal = objDstsvc.GetClientWithFilters(ClientID, string.Empty, false);
                if (objRetVal.ReturnFlag)
                {
                    ClientList = objRetVal.EntityList;
                    if (ClientList != null && ClientList.Count > 0)
                    {
                        clientVM = (ClientViewModel)clientVM.InjectFrom(ClientList[0]);
                    }
                }
            }
            return clientVM;
        }
        #endregion

        #region Sifl Rates List
        public static List<GetPOSIFLRate> GetPOSiflRate(DateTime objDate)
        {
            if (objDate == null || objDate <= DateTime.MinValue)
                objDate = DateTime.Now.Date;
            using (CalculationServiceClient CalcSiflService = new CalculationServiceClient())
            {
                // Get SIFL Rate based on current date
                var siflRate = CalcSiflService.GetSIFLRate(objDate).EntityList;
                return siflRate;
            }
        }
        #endregion

        #region GetDistance between Two ICAOs
        public static double CalculateMiles(long departICAOID, long arriveICAOID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(departICAOID, arriveICAOID))
            {
                double miles = 0;
                double stMiles = WebConstants.stMiles;

                if (departICAOID > 0 && arriveICAOID > 0)
                {
                    using (CalculationServiceClient CalcService = new CalculationServiceClient())
                    {
                        miles = CalcService.GetDistance(Convert.ToInt64(departICAOID), Convert.ToInt64(arriveICAOID));

                        // Calculate Statute Miles
                        return Math.Floor((miles * stMiles));

                    }
                }
                return 0.0;
            }
        }
        #endregion

        #region PassengerRequestor
        public static PostflightLegPassengerViewModel GetAllPreflightAvailablePaxList_FromIDORCD(string passengerRequestorCD, long? PassengerRequestorID = null)
        {
            PostflightLegPassengerViewModel paxInfo = new PostflightLegPassengerViewModel();
            using (FlightPak.Web.PreflightService.PreflightServiceClient prefsevice = new FlightPak.Web.PreflightService.PreflightServiceClient())
            {
                FlightPak.Web.PreflightService.GetAllPreFlightAvailablePaxList AvailableList = null;

                var objretval = PassengerRequestorID.HasValue ? prefsevice.GetAllPreflightAvailablePaxListByIDORCD(PassengerRequestorID.Value, string.Empty) : prefsevice.GetAllPreflightAvailablePaxListByIDORCD(0, passengerRequestorCD.ToLower().Trim());
                if (objretval.ReturnFlag)
                {
                    var _list = objretval.EntityList;
                    if (_list != null && _list.Count > 0)
                    {
                        AvailableList = _list[0];
                        paxInfo = (PostflightLegPassengerViewModel)paxInfo.InjectFrom(AvailableList);
                        paxInfo.TripStatus = AvailableList.TripStatus;
                        paxInfo.FlightPurpose = AvailableList.FlightPurpose;
                        paxInfo.PassportID = AvailableList.PassportID;
                        paxInfo.VisaID = AvailableList.VisaID;
                        paxInfo.PassengerRequestorCD = AvailableList.Code;
                        paxInfo.PassportNUM = AvailableList.Passport;
                    }
                }
                List<FlightPak.Web.PreflightService.GetPassenger> _getPassenger = null;
                var objRetValvalue = prefsevice.GetPassengerbyIDOrCD(Convert.ToInt64(paxInfo.PassengerRequestorID), string.Empty);
                if (objRetValvalue.ReturnFlag == true)
                    _getPassenger = objRetValvalue.EntityList;
                if (_getPassenger != null && _getPassenger.Count > 0)
                {
                    var PassengerList = _getPassenger.FirstOrDefault();
                    paxInfo = (PostflightLegPassengerViewModel)paxInfo.InjectFrom(PassengerList);
                    paxInfo.PassengerID = PassengerList.PassengerRequestorID;
                    paxInfo.FlightPurposeID = PassengerList.FlightPurposeID;
                    paxInfo.FlightPurpose = PassengerList.FlightPurposeCD;
                    paxInfo.PassengerFirstName = PassengerList.FirstName;
                    paxInfo.PassengerLastName = PassengerList.LastName;
                    paxInfo.PassengerMiddleName = PassengerList.MiddleInitial;
                    paxInfo.AssociatedWithCD = PassengerList.AssociatedWithCD;
                    paxInfo.City = PassengerList.City;
                    paxInfo.IsEmployeeType = PassengerList.IsEmployeeType;

                    paxInfo.Street = string.Format("{0} {1} {2}", PassengerList.Addr1, PassengerList.Addr2, PassengerList.Addr3);
                    paxInfo.Billing = PassengerList.StandardBilling;

                }

            }

            return paxInfo;
        }
        public static PostflightLegPassengerViewModel GetPassengerReqestorInfo_FromPassengerReqCD(string passengerRequestorCD)
        {
            PostflightLegPassengerViewModel passengerVM = new PostflightLegPassengerViewModel();
            passengerVM.PassengerRequestorCD = passengerRequestorCD;
            using (FlightPakMasterService.MasterCatalogServiceClient RequestorService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var RequestorVal = RequestorService.GetAllPassengerWithFilters(0, 0, 0, passengerRequestorCD, false, false, string.Empty, 0);
                if (RequestorVal != null && RequestorVal.ReturnFlag == true && RequestorVal.EntityList.Count > 0)
                {
                    var passengerRequestor = RequestorVal.EntityList.FirstOrDefault();
                    if (passengerRequestor != null)
                    {
                        passengerVM = (PostflightLegPassengerViewModel)passengerVM.InjectFrom(passengerRequestor);
                        passengerVM.PassengerFirstName = passengerRequestor.FirstName;
                        passengerVM.PassengerMiddleName = passengerRequestor.MiddleInitial;
                        passengerVM.PassengerLastName = passengerRequestor.LastName;
                        if (string.IsNullOrWhiteSpace(passengerRequestor.PassengerName))
                        {
                            passengerVM.PassengerName = (passengerRequestor.LastName + " ").TrimStart() + passengerRequestor.FirstName;
                        }
                    }
                }
            }
            return passengerVM;
        }
        public static PostflightLegPassengerViewModel GetPassengerReqestorInfo_FromPassengerReqID(long passengerRequestorID)
        {
            PostflightLegPassengerViewModel passengerVM = new PostflightLegPassengerViewModel();
            if (passengerRequestorID == 0)
                return passengerVM;

            using (FlightPakMasterService.MasterCatalogServiceClient RequestorService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var RequestorVal = RequestorService.GetAllPassengerWithFilters(0, 0, passengerRequestorID, string.Empty, false, false, string.Empty, 0);
                if (RequestorVal != null && RequestorVal.ReturnFlag == true && RequestorVal.EntityList.Count > 0)
                {
                    var passengerRequestor = RequestorVal.EntityList.FirstOrDefault();
                    if (passengerRequestor != null)
                    {
                        passengerVM = (PostflightLegPassengerViewModel)passengerVM.InjectFrom(passengerRequestor);
                        passengerVM.PassengerFirstName = passengerRequestor.FirstName;
                        passengerVM.PassengerMiddleName = passengerRequestor.MiddleInitial;
                        passengerVM.PassengerLastName = passengerRequestor.LastName;
                        if (string.IsNullOrWhiteSpace(passengerRequestor.PassengerName))
                        {
                            passengerVM.PassengerName = (passengerRequestor.LastName + " ").TrimStart() + passengerRequestor.FirstName;
                        }
                        passengerVM.RequestorDesc = passengerVM.PassengerName;
                    }
                }
            }
            return passengerVM;
        }
        #endregion

        #region Department
        public static DepartmentViewModel GetDepartmentByIDorCD(long departmentId, string departmentCd)
        {
            DepartmentViewModel departmentVM = new DepartmentViewModel();
            departmentVM.DepartmentCD = departmentCd;
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = string.IsNullOrEmpty(departmentCd)
                    ? objDstsvc.GetDepartmentByWithFilters(string.Empty, departmentId, 0, false)
                    : objDstsvc.GetDepartmentByWithFilters(departmentCd.ToUpper().Trim(), 0, 0, false);

                if (objRetVal.ReturnFlag && objRetVal.EntityList != null && objRetVal.EntityList.Count > 0)
                {
                    departmentVM = (DepartmentViewModel)departmentVM.InjectFrom(objRetVal.EntityList[0]);
                }
            }
            return departmentVM;
        }
        #endregion

        #region DepartmentAuthorization
        public static DepartmentAuthorizationViewModel GetDepartmentAuthorizationByDepIDnAuthorizationCDorID(string AuthorizationCD = null, long AuthorizationID = 0, long DepartmentID = 0)
        {
            DepartmentAuthorizationViewModel departmentAuthorization = new DepartmentAuthorizationViewModel();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = AuthorizationID == 0 ? objDstsvc.GetDepartmentAuthorizationWithFilters(0, DepartmentID, 0, AuthorizationCD.ToUpper().Trim(), false) : objDstsvc.GetDepartmentAuthorizationWithFilters(0, DepartmentID, AuthorizationID, string.Empty, false);
                if (objRetVal.ReturnFlag)
                {
                    var DeptAuthtList = objRetVal.EntityList;
                    if (DeptAuthtList != null && DeptAuthtList.Count > 0)
                    {
                        departmentAuthorization = (DepartmentAuthorizationViewModel)departmentAuthorization.InjectFrom(DeptAuthtList[0]);
                    }
                }
            }
            return departmentAuthorization;
        }
        #endregion

    }
}