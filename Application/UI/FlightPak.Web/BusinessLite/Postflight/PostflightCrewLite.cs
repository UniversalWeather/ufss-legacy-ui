﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PostflightService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using System.Collections;

namespace FlightPak.Web.BusinessLite.Postflight
{
    public class PostflightCrewLite
    {
        UserPrincipalViewModel upViewModel;
        public PostflightCrewLite()
        {
            upViewModel = FlightPak.Web.Views.Transactions.PostFlight.PostflightTripManager.getUserPrincipal();
        }
        public PostflightCrewLite(UserPrincipalViewModel upVM)
        {
            upViewModel = upVM;
        }
        public bool AddCrew(bool isCopyAll, List<PostflightLegCrewViewModel> CrewsList, int currentLegNum)
        {
            bool result = false;
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null && TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State!=TripEntityState.Deleted).ToList().Count > 0)
            {
                decimal RON = 0;
                string takeoffday = "0";
                string takeoffNight = "0";
                string landingday = "0";
                string landingnight = "0";
                string Ap = "0";
                string AN = "0";
                string instrument = "0";
                string night = "0";

                string spec3 = "0.000";
                string spec4 = "0.000";

                TimeSpan dutyBegin = new TimeSpan(0);
                TimeSpan EndTime = new TimeSpan(0);
                TimeSpan dutyEnd = TimeSpan.Zero;
                TimeSpan DutyHours = TimeSpan.Zero;

                string crewCode = string.Empty;

                if (isCopyAll)
                {
                    foreach (PostflightLegViewModel leg in TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).OrderBy(x => x.LegNUM).ToList())
                    {

                        foreach (PostflightLegCrewViewModel selectedCrew in CrewsList)
                        {
                            int crewAlreadyExistCount= leg.PostflightLegCrews.Where(c => c.CrewID == selectedCrew.CrewID && c.IsDeleted == false && c.State != TripEntityState.Deleted).Count();
                            if (crewAlreadyExistCount > 0) continue;
                            if (selectedCrew.CrewID != null)
                            {
                                var objRetVal = selectedCrew.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId((long)selectedCrew.CrewID) : new CrewViewModel();
                                if (objRetVal != null && !string.IsNullOrWhiteSpace(objRetVal.CrewCD))
                                    crewCode = objRetVal.CrewCD.ToUpper();
                            }

                            if (leg.PostflightLegCrews == null)
                                leg.PostflightLegCrews = new List<PostflightLegCrewViewModel>();

                            PostflightLegCrewViewModel LegCrew = leg.PostflightLegCrews.Where(x => x.CrewID == selectedCrew.CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();

                            if (LegCrew == null)
                            {
                                PostflightLegCrewViewModel objCrew = null;
                                objCrew = DutyHourForCrew(leg, selectedCrew.CrewID.Value);
                                if (objCrew == null)
                                    objCrew = new PostflightLegCrewViewModel();
                                objCrew.State = TripEntityState.Added;
                                objCrew.CrewFirstName = selectedCrew.CrewFirstName;
                                objCrew.CrewMiddleName = selectedCrew.CrewMiddleName;
                                objCrew.CrewLastName = selectedCrew.CrewLastName;
                                objCrew.CrewCode = crewCode;
                                objCrew.TakeOffDay = Convert.ToDecimal(takeoffday);
                                objCrew.TakeOffNight = Convert.ToDecimal(takeoffNight);
                                objCrew.LandingDay = Convert.ToDecimal(landingday);
                                objCrew.LandingNight = Convert.ToDecimal(landingnight);
                                objCrew.ApproachPrecision = Convert.ToDecimal(Ap);
                                objCrew.ApproachNonPrecision = Convert.ToDecimal(AN);
                                objCrew.Instrument = Convert.ToDecimal(instrument);
                                objCrew.Night = Convert.ToDecimal(night);
                                objCrew.BlockHours = leg.BlockHours;
                                objCrew.FlightHours = leg.FlightHours;
                                objCrew.Specification3 =upViewModel._Specdec3==null?0: Math.Round(Convert.ToDecimal(spec3), (int)upViewModel._Specdec3);
                                objCrew.Specification4 =upViewModel._Specdec4==null?0: Math.Round(Convert.ToDecimal(spec4), (int)upViewModel._Specdec4);
                                
                                objCrew.IsRemainOverNightOverride = false;
                                objCrew.IsAugmentCrew = false;
                                objCrew.IsOverride = false;
                                objCrew.OrderNUM = leg.PostflightLegCrews.Count + 1;
                                objCrew.RowNumber = leg.PostflightLegCrews.Select(r => r.RowNumber).DefaultIfEmpty().Max() + 1;
                                
                                objCrew.DaysAway = DaysAwayCalculation(TripLog, Convert.ToInt64(selectedCrew.CrewID), Convert.ToInt64(leg.LegNUM));

                                // Recalculate RON
                                if (objCrew.IsRemainOverNightOverride != true)
                                    objCrew.RemainOverNight = RONCalculation(Convert.ToInt64(selectedCrew.CrewID), Convert.ToInt64(leg.LegNUM), upViewModel, TripLog);
                                else
                                    objCrew.RemainOverNight = 0;

                                leg.PostflightLegCrews.Add(objCrew);
                                
                            }
                        }

                        
                        HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;

                    }
                    FlightPak.Web.Views.Transactions.PostFlightValidator.ReCalculateCrewDutyHour();
                    BindValues(currentLegNum);
                    result = true;
                }
                else
                {

                    List<PostflightLegCrewViewModel> CurrentCrewList = new List<PostflightLegCrewViewModel>();
                    PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                    
                    foreach (PostflightLegCrewViewModel selectedCrew in CrewsList)
                    {
                        int crewAlreadyExistCount = currentLeg.PostflightLegCrews.Where(c => c.CrewID == selectedCrew.CrewID && c.IsDeleted == false && c.State != TripEntityState.Deleted).Count();
                        if (crewAlreadyExistCount > 0) continue;
                        if (selectedCrew.CrewID != null)
                        {
                            var objRetVal = selectedCrew.CrewID.HasValue ? DBCatalogsDataLoader.GetCrewInfo_FromCrewId((long)selectedCrew.CrewID) : new CrewViewModel();
                            if (objRetVal != null && !string.IsNullOrWhiteSpace(objRetVal.CrewCD))
                                crewCode = objRetVal.CrewCD.ToUpper();
                        }
                        PostflightLegCrewViewModel objCrew = null;
                        objCrew = DutyHourForCrew(currentLeg, selectedCrew.CrewID.Value);
                        if (objCrew == null)
                            objCrew = new PostflightLegCrewViewModel();

                            objCrew.State = TripEntityState.Added;
                            objCrew.CrewID = Convert.ToInt64(selectedCrew.CrewID);
                            objCrew.DaysAway = DaysAwayCalculation(TripLog, Convert.ToInt64(selectedCrew.CrewID), Convert.ToInt64(currentLeg.LegNUM));
                            objCrew.RemainOverNight = RON;
                            objCrew.CrewFirstName = selectedCrew.CrewFirstName;
                            objCrew.CrewMiddleName = selectedCrew.CrewMiddleName;
                            objCrew.CrewLastName = selectedCrew.CrewLastName;
                            objCrew.CrewCode = crewCode;
                            objCrew.TakeOffDay = Convert.ToDecimal(takeoffday);
                            objCrew.TakeOffNight = Convert.ToDecimal(takeoffNight);
                            objCrew.LandingDay = Convert.ToDecimal(landingday);
                            objCrew.LandingNight = Convert.ToDecimal(landingnight);
                            objCrew.ApproachPrecision = Convert.ToDecimal(Ap);
                            objCrew.ApproachNonPrecision = Convert.ToDecimal(AN);
                            objCrew.Instrument = Convert.ToDecimal(instrument);
                            objCrew.Night = Convert.ToDecimal(night);
                            objCrew.Specification3 =upViewModel._Specdec3==null?0: Math.Round(Convert.ToDecimal(spec3), (int)upViewModel._Specdec3);
                            objCrew.Specification4 =upViewModel._Specdec4==null?0: Math.Round(Convert.ToDecimal(spec4),(int) upViewModel._Specdec4);
                            objCrew.IsRemainOverNightOverride = false;
                            objCrew.IsAugmentCrew = false;
                            objCrew.IsOverride = false;
                            objCrew.OrderNUM = currentLeg.PostflightLegCrews.Count + 1;
                            objCrew.RowNumber = currentLeg.PostflightLegCrews.Select(r => r.RowNumber).DefaultIfEmpty().Max() + 1;
                       
                          CurrentCrewList.Add(objCrew);
                    }
                    if (currentLeg.PostflightLegCrews == null)
                        currentLeg.PostflightLegCrews = new List<PostflightLegCrewViewModel>();

                    currentLeg.PostflightLegCrews.AddRange(CurrentCrewList);

                    HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
                    FlightPak.Web.Views.Transactions.PostFlightValidator.ReCalculateCrewDutyHour();
                    BindValues(currentLegNum);
                    result = true;
                }
            }
            return result;
        }

        public void BindValues(int legNUM)
        {
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null)
            {
                PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == legNUM && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                PostflightLegViewModel PreviousLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == legNUM - 1 && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();

                if (currentLeg != null)
                {

                    List<PostflightLegCrewViewModel> crewList = currentLeg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).OrderBy(x => x.OrderNUM).ToList();

                    if (crewList != null && crewList.Count > 0)
                    {
                        foreach (var Item in crewList)
                        {
                            PostflightLegCrewViewModel crew = crewList.Where(x => x.CrewID == Item.CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();

                            if (crew != null && crew.IsOverride != null && !(bool)crew.IsOverride)
                            {
                                if (crew.CrewDutyStartTM != null && crew.CrewDutyStartTM.HasValue)
                                {
                                    if (PreviousLeg != null)
                                    {
                                        if (PreviousLeg.PostflightLegCrews != null)
                                        {
                                            PostflightLegCrewViewModel prevlegCrew = PreviousLeg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.CrewID == crew.CrewID).FirstOrDefault();
                                            if (prevlegCrew != null)
                                            {
                                                if (PreviousLeg.LegNUM > 0 && PreviousLeg.IsDutyEnd != true)
                                                    crew.CrewDutyStartTM = null;
                                            }
                                        }
                                    }

                                    if (crew.CrewDutyEndTM != null && crew.CrewDutyEndTM.HasValue)
                                    {
                                        PostflightLegViewModel NxtLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLeg.LegNUM + 1 && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();// Added By Jajati
                                        if (NxtLeg != null && (currentLeg.IsDutyEnd == null || currentLeg.IsDutyEnd == false))
                                        {
                                            if (NxtLeg.PostflightLegCrews != null)
                                            {
                                                PostflightLegCrewViewModel nextlegCrew = NxtLeg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.CrewID == crew.CrewID).FirstOrDefault();
                                                if (nextlegCrew != null)
                                                    crew.CrewDutyEndTM = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
            }
        }

        public PostflightLegCrewViewModel DutyHourForCrew(PostflightLegViewModel Leg, Int64 CrewID)
        {
            PostflightLegCrewViewModel poCrew = null;
            if (Leg != null)
            {
                if (Leg.PostflightLegCrews != null && Leg.PostflightLegCrews.Count > 0)
                    poCrew = Leg.PostflightLegCrews.Where(x => x.CrewID == CrewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
            }
            if (poCrew == null)
                poCrew = new PostflightLegCrewViewModel();

            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null)
            {
                //decimal dutyTime = 0;
                string dutyTime = "0";
                DateTime? dutyEndDT = null; // DateTime.MinValue;
                DateTime? dutyBeginDT = null; // DateTime.MinValue;
                string dutyEndinghrs = "00:00";
                string dutybeginhrs = "00:00";

                PostflightLegViewModel PreviousLeg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == Leg.LegNUM - 1).FirstOrDefault();
                PostflightLegViewModel NextLeg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == Leg.LegNUM + 1).FirstOrDefault();

                if (Leg != null)
                {
                    double DutyEndFAR = 0, DutyBeginFAR = 0;
                    if (!string.IsNullOrWhiteSpace(Leg.CrewCurrency) && !string.IsNullOrWhiteSpace(Leg.CrewCurrency))
                    {
                        //** need to think , handle it in cache object we already using on DBCatalog.
                        var RulesVal = DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(0, Leg.CrewCurrency.Trim());
                        if (RulesVal != null)
                        {
                            if (RulesVal.DutyDayEndTM != null && RulesVal.DutyDayEndTM.HasValue)
                                DutyEndFAR = Convert.ToDouble(RulesVal.DutyDayEndTM);

                            if (RulesVal.DutyDayBeingTM != null && RulesVal.DutyDayBeingTM.HasValue)
                                DutyBeginFAR = Convert.ToDouble(RulesVal.DutyDayBeingTM);
                        }
                    }

                    TimeSpan dutyBegin = new TimeSpan(0);
                    if (Leg.BeginningDutyHours != null && (!string.IsNullOrWhiteSpace(Leg.BeginningDutyHours.ToString())))
                        dutyBegin = TimeSpan.ParseExact(Leg.BeginningDutyHours.ToString(), "c", null);

                    TimeSpan schTime = new TimeSpan(0);
                    if (Leg.ScheduledTM != null)
                        schTime = Leg.ScheduledTM.Value.TimeOfDay;

                    if (DutyBeginFAR != 0)
                    {
                        double hours = Math.Floor(DutyBeginFAR);
                        double minutes = (DutyBeginFAR - hours) * 60.0;

                        Int32 actualHours = (Int32)Math.Floor(hours);
                        Int32 actualMinutes = (Int32)Math.Floor(minutes);

                        TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);
                        dutyBegin = nTime;
                    }

                    if (PreviousLeg != null)
                    {
                        for (int i = (Convert.ToInt32(Leg.LegNUM) - 1); i >= 1; i--)
                        {
                            PostflightLegViewModel PrevLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == i && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                            PostflightLegViewModel NxtLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == i + 1 && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();

                            if (PrevLeg != null)
                            {
                                if (PrevLeg.LegNUM > 0 && PrevLeg.IsDutyEnd != true)
                                {
                                    if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2)
                                    {
                                        if (dutyBegin != null && Leg.ScheduledTM != null && PrevLeg.ScheduledTM != null)
                                        {
                                            dutyBeginDT = PrevLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                            dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                        }
                                    }
                                    else if ((dutyBegin != null) && (Leg.OutboundDTTM != null) && PrevLeg.OutboundDTTM != null)
                                    {
                                        dutyBeginDT = PrevLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                        dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                    }
                                    if (PrevLeg.LegNUM == 1)
                                        break;
                                }
                                else if (NxtLeg != null)
                                {
                                    if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2)
                                    {
                                        if (dutyBegin != null && Leg.ScheduledTM != null && NxtLeg.ScheduledTM != null)
                                        {
                                            dutyBeginDT = NxtLeg.ScheduledTM.Value.Subtract(dutyBegin);
                                            dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                        }
                                    }
                                    else if (dutyBegin != null && Leg.OutboundDTTM != null && NxtLeg.OutboundDTTM != null)
                                    {
                                        dutyBeginDT = NxtLeg.OutboundDTTM.Value.Subtract(dutyBegin);
                                        dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                                    }


                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2 && Leg.ScheduledTM != null)
                        {
                            dutyBeginDT = Leg.ScheduledTM.Value.Subtract(dutyBegin);
                            dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                        }
                        else if (Leg.OutboundDTTM != null)
                        {
                            dutyBeginDT = Leg.OutboundDTTM.Value.Subtract(dutyBegin);
                            dutybeginhrs = dutyBeginDT.Value.TimeOfDay.ToString();
                        }
                    }


                    TimeSpan dutyEnd = TimeSpan.Zero;
                    if (DutyEndFAR != 0)
                    {
                        double hours = Math.Floor(DutyEndFAR);
                        double minutes = (DutyEndFAR - hours) * 60.0;

                        Int32 actualHours = (Int32)Math.Floor(hours);
                        Int32 actualMinutes = (Int32)Math.Floor(minutes);

                        TimeSpan nTime = new TimeSpan(actualHours, actualMinutes, 0);
                        dutyEnd = nTime;
                    }

                    if (dutyEnd != null && (Leg.InboundDTTM != null))
                    {
                        if (NextLeg != null && (Leg.IsDutyEnd == null || Leg.IsDutyEnd == false))
                        {
                            dutyEndDT = Leg.InboundDTTM;
                            dutyEndinghrs = dutyEndDT.Value.TimeOfDay.ToString();
                        }
                        else
                        {
                            dutyEndDT = Leg.InboundDTTM.Value.Add(dutyEnd);
                            dutyEndinghrs = dutyEndDT.Value.TimeOfDay.ToString();
                        }
                    }

                    TimeSpan DutyHours = TimeSpan.Zero;
                    if (dutyBeginDT != null && dutyBeginDT != DateTime.MinValue && dutyEndDT != null && dutyEndDT != DateTime.MinValue && dutyBeginDT < dutyEndDT)
                        DutyHours = dutyEndDT.Value.Subtract(dutyBeginDT.Value);
                    else
                        DutyHours = TimeSpan.Zero;

                    if (Leg.DutyHrs != null)
                        dutyTime = Math.Round(Convert.ToDecimal(DutyHours.TotalHours), 2).ToString();

                    if (poCrew != null)
                    {
                        if (poCrew.CrewID == null)
                            poCrew.CrewID = CrewID;
                        poCrew.DutyHrsTime = dutyTime;
                        poCrew.CrewDutyStartTM = dutyBeginDT;
                        poCrew.CrewDutyEndTM = dutyEndDT;
                        poCrew.BeginningDuty = dutybeginhrs;
                        poCrew.DutyEnd = dutyEndinghrs;
                    }

                }
            }
            return poCrew;
        }

        public decimal DaysAwayCalculation(PostflightLogViewModel TripLog, Int64 Crewid, Int64 Legnum)
        {
            int daysAway = 0;
            //Find previous leg
            if (TripLog != null && TripLog.PostflightLegs != null)
            {
                PostflightLegViewModel previousLeg = TripLog.PostflightLegs.Where(l => l.IsDeleted == false && l.State != TripEntityState.Deleted && l.LegNUM < Legnum).OrderByDescending(ln => ln.LegNUM).FirstOrDefault();
                bool bCrewInPrevLeg = previousLeg != null && previousLeg.PostflightLegCrews != null && previousLeg.PostflightLegCrews.Where(c => c.IsDeleted == false && c.State != TripEntityState.Deleted && c.CrewID == Crewid).Count() > 0;
                if (bCrewInPrevLeg == false)
                    previousLeg = null;

                PostflightLegViewModel SelectLeg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == Legnum).FirstOrDefault();

                DateTime scheduleDate = DateTime.MinValue;
                DateTime inDate = DateTime.MinValue;
                DateTime previousScheduleDate = DateTime.MinValue;
                DateTime previousInDate = DateTime.MinValue;

                if (previousLeg != null)
                {
                    if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2) // No Out Date Display
                    {
                        if ((previousLeg.ScheduledTM != null))
                            previousScheduleDate = previousLeg.ScheduledTM.Value;
                    }
                    else
                    {
                        if ((previousLeg.OutboundDTTM != null))
                            previousScheduleDate = previousLeg.OutboundDTTM.Value;
                    }

                    if (previousLeg.InboundDTTM != null)
                        previousInDate = previousLeg.InboundDTTM.Value;

                }

                if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2) 
                {
                    if ((SelectLeg.ScheduledTM != null))
                        scheduleDate = SelectLeg.ScheduledTM.Value;
                }
                else
                {
                    if ((SelectLeg.OutboundDTTM != null))
                        scheduleDate = SelectLeg.OutboundDTTM.Value;
                }

                if (SelectLeg.InboundDTTM != null)
                    inDate = SelectLeg.InboundDTTM.Value;

                if(previousLeg == null)
                {
                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days + 1;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
                else if(scheduleDate.Date == previousScheduleDate.Date && inDate.Date == previousInDate.Date)
                {
                    daysAway = 0;
                }
                else if (scheduleDate.Date == previousScheduleDate.Date && inDate.Date != previousInDate.Date)
                {
                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
                else if (scheduleDate.Date != previousScheduleDate.Date && inDate.Date == previousInDate.Date)
                {
                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
                else if (scheduleDate.Date != previousScheduleDate.Date && inDate.Date != previousInDate.Date)
                {
                    int daysBetweenLegs = scheduleDate.Date.Subtract(previousInDate.Date).Days - 1;
                    daysBetweenLegs = daysBetweenLegs > 0 ? daysBetweenLegs : 0;

                    daysAway = inDate.Date.Subtract(scheduleDate.Date).Days + 1 + daysBetweenLegs;
                    daysAway = daysAway > 0 ? daysAway : 0;
                }
            }

            return daysAway;
        }

        #region OLD VERSION OF DaysAwayCalculation
        //public decimal DaysAwayCalculation(PostflightLogViewModel TripLog, Int64 Crewid, Int64 Legnum)
        //{
        //    int daysAway = 0;

        //    if (TripLog != null && TripLog.PostflightLegs != null)
        //    {
        //        PostflightLegViewModel SelectLeg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == Legnum).FirstOrDefault();

        //        DateTime beginDT = DateTime.MinValue;
        //        DateTime endDT = DateTime.MinValue;

        //        List<PostflightLegViewModel> legsList = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM < Legnum).OrderByDescending(x => x.LegNUM).ToList();
        //        if (legsList.Count == 0)
        //        {
        //            if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2) // No Out Date Display
        //            {
        //                if ((SelectLeg.ScheduledTM != null))
        //                    beginDT = SelectLeg.ScheduledTM.Value;
        //            }
        //            else
        //            {
        //                if ((SelectLeg.OutboundDTTM != null))
        //                    beginDT = SelectLeg.OutboundDTTM.Value;
        //            }

        //            if (SelectLeg.InboundDTTM != null)
        //                endDT = SelectLeg.InboundDTTM.Value;

        //            if (beginDT != DateTime.MinValue && endDT != DateTime.MinValue)
        //                daysAway = endDT.Date.Subtract(beginDT.Date).Days + 1;

        //        }
        //        else
        //        {
        //            int counter = 0;
        //            foreach (PostflightLegViewModel Leg in legsList)
        //            {
        //                if (Leg.PostflightLegCrews != null)
        //                {
        //                    foreach (PostflightLegCrewViewModel Crew in Leg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).OrderBy(x => x.OrderNUM))
        //                    {
        //                        if (Crew.CrewID == Crewid)
        //                        {
        //                            if (Leg.InboundDTTM != null && Leg.InboundDTTM.HasValue)
        //                                endDT = Leg.InboundDTTM.Value;

        //                            counter = 1;
        //                            break;
        //                        }
        //                    }
        //                }
        //                if (counter == 1)
        //                    break;
        //            }

        //            if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == 2) // No Out Date Display
        //            {
        //                if ((SelectLeg.ScheduledTM != null))
        //                    beginDT = SelectLeg.ScheduledTM.Value;
        //            }
        //            else
        //            {
        //                if ((SelectLeg.OutboundDTTM != null))
        //                    beginDT = SelectLeg.OutboundDTTM.Value;
        //            }

        //            if (beginDT != DateTime.MinValue && endDT != DateTime.MinValue)
        //            {
        //                daysAway = beginDT.Date.Subtract(endDT.Date).Days;
        //                if (daysAway == 0)
        //                {
        //                    daysAway = 1;
        //                }
        //            }
        //            else if (endDT == DateTime.MinValue)
        //            {
        //                daysAway = 1;
        //            }

        //            if (SelectLeg.InboundDTTM != null)
        //                daysAway += SelectLeg.InboundDTTM.Value.Subtract(beginDT.Date).Days;
        //        }
        //    }

        //    return Convert.ToDecimal(daysAway);
        //}
        #endregion OLD VERSION OF DaysAwayCalculation

        public decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Miles))
            {
                if ((!upViewModel._IsKilometer) ? false : (bool)upViewModel._IsKilometer)
                    return Math.Round(Miles * 1.852M, 0);
                else
                    return Miles;
            }
        }

        //Might to revsit this for optmization - 
        public static Hashtable CheckDutyHourOverride(long crewID, long currentLegNum, UserPrincipalViewModel upViewModel)
        {
            DateTime? g_DutyStartDTTM = null;
            DateTime? g_DutyEndDTTM = null;

            Hashtable RetObj = new Hashtable();
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (TripLog != null)
            {
                if (TripLog.PostflightLegs != null && TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).Count() > 0)
                {
                    PostflightLegViewModel currentLeg = TripLog.PostflightLegs.Where(x => x.LegNUM == currentLegNum && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                    if (currentLeg != null && currentLeg.PostflightLegCrews != null && currentLeg.PostflightLegCrews.Count > 0)
                    {
                        var crew = currentLeg.PostflightLegCrews.Where(x => x.CrewID == crewID && x.IsDeleted == false && x.State != TripEntityState.Deleted).FirstOrDefault();
                        bool isSchedTM = false;

                        string g_DutyStartDT = crew.CrewDutyStartDate;
                        if (upViewModel._DutyBasis != null && upViewModel._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep && currentLeg.ScheduledTM != null)
                            isSchedTM = true;
                        if (string.IsNullOrWhiteSpace(g_DutyStartDT))
                        {
                            if (isSchedTM)
                                g_DutyStartDT = currentLeg.ScheduledTM.FSSParseDateTimeString(upViewModel._ApplicationDateFormat);
                            else
                            {
                                if (currentLeg.OutboundDTTM != null)
                                    g_DutyStartDT = currentLeg.OutboundDTTM.FSSParseDateTimeString(upViewModel._ApplicationDateFormat);
                            }
                            crew.CrewDutyStartDate = g_DutyStartDT;
                            if (!string.IsNullOrWhiteSpace(crew.StartTime))
                                crew.CrewDutyStartTime = crew.StartTime;
                        }

                        string g_DutyEndDT = crew.CrewDutyEndDate;
                        if (string.IsNullOrWhiteSpace(g_DutyEndDT))
                        {
                            g_DutyEndDT = currentLeg.InboundDTTM.FSSParseDateTimeString(upViewModel._ApplicationDateFormat);
                            crew.CrewDutyEndDate = g_DutyEndDT;
                            if (!string.IsNullOrWhiteSpace(crew.EndTime))
                                crew.CrewDutyEndTime = crew.EndTime;
                        }

                        string g_DutyStartTM = crew.CrewDutyStartTime == ":" ? null : crew.CrewDutyStartTime; ;
                        if (string.IsNullOrWhiteSpace(g_DutyStartTM))
                        {
                            if (isSchedTM)
                                g_DutyStartTM = currentLeg.ScheduledTM.FSSParseDateTimeString("HH:mm");
                            else
                            {
                                if (currentLeg.OutboundDTTM != null)
                                    g_DutyStartTM = currentLeg.OutboundDTTM.FSSParseDateTimeString("HH:mm");
                            }
                        }

                        string g_DutyEndTM = crew.CrewDutyEndTime == ":" ? null : crew.CrewDutyEndTime;
                        if (string.IsNullOrWhiteSpace(g_DutyEndTM)) g_DutyEndTM = currentLeg.InboundDTTM.FSSParseDateTimeString("HH:mm");

                        if (!string.IsNullOrWhiteSpace(g_DutyStartDT) && !string.IsNullOrWhiteSpace(g_DutyStartTM))
                        {
                            try
                            {
                                g_DutyStartDTTM = Convert.ToDateTime(MiscUtils.FormatDate(g_DutyStartDT, upViewModel) + " " + g_DutyStartTM);
                            }
                            catch (Exception ex)
                            {
                                g_DutyStartDTTM = DateTime.ParseExact(g_DutyStartDT + " " + g_DutyStartTM, upViewModel._ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                            }
                            crew.CrewDutyStartTM = g_DutyStartDTTM;
                        }

                        if (!string.IsNullOrWhiteSpace(g_DutyEndDT) && !string.IsNullOrWhiteSpace(g_DutyEndTM))
                        {
                            try
                            {
                                    g_DutyEndDTTM = Convert.ToDateTime(MiscUtils.FormatDate(g_DutyEndDT, upViewModel) + " " + g_DutyEndTM);
                            }
                            catch (Exception ex)
                            {
                                g_DutyEndDTTM = DateTime.ParseExact(g_DutyEndDT + " " + g_DutyEndTM, upViewModel._ApplicationDateFormat + " HH:mm", CultureInfo.InvariantCulture);
                            }
                            crew.CrewDutyEndTM = g_DutyEndDTTM;
                        }

                        crew.IsOverride = false;

                        var g_tbDutyHours = "";

                        if (g_DutyStartDTTM != null && g_DutyEndDTTM != null && g_DutyStartDTTM < g_DutyEndDTTM)
                        {
                            TimeSpan dutyHours = g_DutyEndDTTM.Value.Subtract(g_DutyStartDTTM.Value);

                            if (upViewModel._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                            {
                                g_tbDutyHours = System.Math.Round(Convert.ToDecimal(dutyHours.TotalHours), 1).ToString();

                                if (g_tbDutyHours.IndexOf(".") < 0)
                                    g_tbDutyHours = g_tbDutyHours + ".0";
                            }
                            else
                            {
                                g_tbDutyHours = FPKConversionHelper.ConvertTenthsToMins(Math.Round(dutyHours.TotalHours, 3).ToString());
                            }

                        }
                        else
                        {
                            if (upViewModel._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                                g_tbDutyHours = "0.0";
                            else
                                g_tbDutyHours = "00:00";
                        }
                        crew.DutyHrsTime = g_tbDutyHours;
                        if (string.IsNullOrWhiteSpace(currentLeg.DutyHrsTime))
                        {
                            if (upViewModel._TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                                currentLeg.DutyHrsTime = "0.0";
                            else
                                currentLeg.DutyHrsTime = "00:00";
                        }

                        if (!(g_tbDutyHours.Equals(currentLeg.DutyHrsTime)))
                            crew.IsOverride = true;
                        RetObj["CrewObj"] = crew;
                    }
                }
            }
            return RetObj;
        }

        public decimal RONCalculation(Int64 Crewid, Int64 LegNum, UserPrincipalViewModel UserPrincipal, PostflightLogViewModel pfViewModel)
        {
            decimal RON = 0;

            if (pfViewModel != null && pfViewModel.PostflightLegs != null)
            {
                PostflightLegViewModel SelectLeg = pfViewModel.PostflightLegs.FirstOrDefault(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM == LegNum);
                if (SelectLeg!=null)
                {
                    if (SelectLeg.IsDutyEnd == true)
                    {
                        DateTime dutyBeginRONDT = DateTime.MinValue;
                        DateTime dutyEndRONDT = DateTime.MinValue;

                        int counter = 0;
                        foreach (PostflightLegViewModel Leg in pfViewModel.PostflightLegs.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted && x.LegNUM > SelectLeg.LegNUM).ToList().OrderBy(x => x.LegNUM))
                        {
                            if (Leg.LegNUM != SelectLeg.LegNUM)
                            {
                                if (Leg.PostflightLegCrews != null)
                                {
                                    foreach (PostflightLegCrewViewModel Crew in Leg.PostflightLegCrews.Where(x => x.IsDeleted == false && x.State != TripEntityState.Deleted).ToList())
                                    {
                                        if (Crew.CrewID == Crewid)
                                        {
                                            if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == WebConstants.POCrewDutyBasis.ScheduledDep) // No Out Date Display
                                            {
                                                if ((Leg.ScheduledTM != null))
                                                    dutyEndRONDT = Leg.ScheduledTM.Value;
                                            }
                                            else
                                            {
                                                if ((Leg.OutboundDTTM != null))
                                                    dutyEndRONDT = Leg.OutboundDTTM.Value;
                                            }
                                            counter = 1;
                                            break;
                                        }
                                    }
                                }
                                if (counter == 1)
                                    break;
                            }
                        }

                        if (SelectLeg.InboundDTTM != null && SelectLeg.InboundDTTM.HasValue)
                            dutyBeginRONDT = SelectLeg.InboundDTTM.Value;

                        if (dutyEndRONDT != DateTime.MinValue && dutyBeginRONDT != DateTime.MinValue)
                        {
                            RON = dutyEndRONDT.Date.Subtract(dutyBeginRONDT.Date).Days;

                            if (Convert.ToDecimal(RON) < 0)
                                RON = 0;
                        }
                    }
                }
            }
            return RON;
        }

    }
}
    
            
        
    
 