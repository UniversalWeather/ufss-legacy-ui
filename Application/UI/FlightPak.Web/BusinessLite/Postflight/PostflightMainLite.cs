﻿using FlightPak.Common.Constants;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PostflightService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Omu.ValueInjecter;
using FlightPak.Web.Views.Transactions.PostFlight;

namespace FlightPak.Web.BusinessLite.Postflight
{
    public class PostflightMainLite
    {
        public string[] LoadHomeBaseInfo(String HomebaseAirportID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomebaseAirportID))
            {
                string[] retVal = new string[9];

                //Bug 2567
                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.GetAllAirport Airportmaster = new FlightPakMasterService.GetAllAirport();
                    if (!string.IsNullOrEmpty(HomebaseAirportID))
                    {
                        var objAirport = objService.GetAirportByAirportID(Convert.ToInt64(HomebaseAirportID));
                        if (objAirport.ReturnFlag)
                        {
                            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> HomeBaseAirport = objAirport.EntityList;
                            if (HomeBaseAirport != null && HomeBaseAirport.Count > 0)
                            {
                                if (HomeBaseAirport[0].CityName != null && !string.IsNullOrEmpty(HomeBaseAirport[0].CityName.Trim()))
                                    retVal[0] = HomeBaseAirport[0].CityName.Trim();
                                else
                                    retVal[0] = string.Empty;
                                if (HomeBaseAirport[0].StateName != null && !string.IsNullOrEmpty(HomeBaseAirport[0].StateName.Trim()))
                                    retVal[1] = HomeBaseAirport[0].StateName.Trim();
                                else
                                    retVal[1] = string.Empty;
                                if (HomeBaseAirport[0].CountryName != null && !string.IsNullOrEmpty(HomeBaseAirport[0].CountryName.Trim()))
                                    retVal[2] = HomeBaseAirport[0].CountryName.Trim();
                                else
                                    retVal[2] = string.Empty;
                                if (HomeBaseAirport[0].DSTRegionCD != null && !string.IsNullOrEmpty(HomeBaseAirport[0].DSTRegionCD.Trim()))
                                    retVal[3] = HomeBaseAirport[0].DSTRegionCD.Trim();
                                else
                                    retVal[3] = string.Empty;
                                if (HomeBaseAirport[0].OffsetToGMT != null)
                                    retVal[4] = HomeBaseAirport[0].OffsetToGMT.Value.ToString();
                                else
                                    retVal[4] = string.Empty;
                                if (HomeBaseAirport[0].AirportName != null)
                                    retVal[5] = HomeBaseAirport[0].AirportName;
                                else
                                    retVal[5] = string.Empty;
                                if (HomeBaseAirport[0].LongestRunway != null)
                                    retVal[6] = HomeBaseAirport[0].LongestRunway.ToString();
                                else
                                    retVal[6] = string.Empty;
                                if (HomeBaseAirport[0].Iata != null)
                                    retVal[7] = HomeBaseAirport[0].Iata.ToString();
                                else
                                    retVal[7] = string.Empty;

                                if (HomeBaseAirport[0].IcaoID != null)
                                    retVal[8] = HomeBaseAirport[0].IcaoID.ToString();
                                else
                                    retVal[8] = string.Empty;

                            }
                        }
                    }
                }
                return retVal;
            }
        }

        internal PostflightLogViewModel LoadFleetAndAircraftBasedOnUsersHomebase(PostflightLogViewModel pfLogViewModel)
        {
            if (pfLogViewModel.PostflightMain.HomebaseID != null && pfLogViewModel.PostflightMain.HomebaseID > 0)
            {

                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {

                    List<FlightPakMasterService.GetFleet> GetFleetList = new List<FlightPak.Web.FlightPakMasterService.GetFleet>();

                    var fleetretval = objDstsvc.GetFleet();
                    if (fleetretval.ReturnFlag)
                    {
                        GetFleetList = fleetretval.EntityList.Where(x => x.HomebaseID == pfLogViewModel.PostflightMain.HomebaseID && x.IsInActive == false).ToList();
                        if (GetFleetList != null && GetFleetList.Count == 1)
                        {
                            PostflightService.Fleet fleetsample = new PostflightService.Fleet();
                            fleetsample.FleetID = GetFleetList[0].FleetID;
                            fleetsample.TailNum = GetFleetList[0].TailNum;
                            fleetsample.MaximumPassenger = GetFleetList[0].MaximumPassenger != null ? GetFleetList[0].MaximumPassenger : 0;

                            pfLogViewModel.PostflightMain.FleetID = GetFleetList[0].FleetID;
                            pfLogViewModel.PostflightMain.Fleet = (FleetViewModel) pfLogViewModel.PostflightMain.Fleet.InjectFrom(fleetsample);


                            if (GetFleetList[0].AircraftID != null)
                            {
                                List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();


                                var objRetVal1 = objDstsvc.GetAircraftList();
                                if (objRetVal1.ReturnFlag)
                                {
                                    AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID == (long)GetFleetList[0].AircraftID).ToList();

                                    if (AircraftList != null && AircraftList.Count > 0)
                                    {

                                        PostflightService.Aircraft aircraftSample = new PostflightService.Aircraft();

                                        aircraftSample.AircraftID = AircraftList[0].AircraftID;
                                        aircraftSample.AircraftCD = AircraftList[0].AircraftCD;
                                        aircraftSample.AircraftDescription = AircraftList[0].AircraftDescription;
                                        pfLogViewModel.PostflightMain.Aircraft = (AircraftViewModel) pfLogViewModel.PostflightMain.Aircraft.InjectFrom(aircraftSample);
                                        pfLogViewModel.PostflightMain.AircraftID = AircraftList[0].AircraftID;
                                    }
                                }
                            }

                        }
                    }
                }
            }
            return pfLogViewModel;
        }

        internal PostflightLogViewModel LoadClientInfo(PostflightLogViewModel pfViewModel)
        {
            UserPrincipalViewModel userSettings = PostflightTripManager.getUserPrincipal();
            // Get Client Description from Master Service
            string clientDescription = string.Empty;
            if (userSettings._clientId != null)
            {
                FlightPak.Web.FlightPakMasterService.Client clientObj = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userSettings._clientId))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        clientObj = new FlightPakMasterService.Client();
                        var objRetVal = objDstsvc.GetClientCodeList();
                        List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                        if (objRetVal.ReturnFlag)
                        {
                            ClientList = objRetVal.EntityList.Where(x => x.ClientID == (long)userSettings._clientId).ToList();
                            if (ClientList != null && ClientList.Count > 0)
                            {
                                clientObj = ClientList[0];
                            }
                            else
                                clientObj = null;
                        }
                        
                    }
                }

                if (clientObj != null)
                {
                    clientDescription = clientObj.ClientDescription;
                }
                pfViewModel.PostflightMain.ClientID = (long)userSettings._clientId;
                pfViewModel.PostflightMain.Client.ClientCD= userSettings._clientCd;
                pfViewModel.PostflightMain.Client.ClientID =(long)userSettings._clientId;
                pfViewModel.PostflightMain.Client.ClientDescription = clientDescription;                
            }
            return pfViewModel;
        }

        public AircraftViewModel LoadAircraftViewModelFromTaiNum(String TailNum)
        {
            AircraftViewModel AircraftVM = new AircraftViewModel() ;

            using (FlightPakMasterService.MasterCatalogServiceClient TailService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var TailVal = TailService.GetAllFleetWithFilters(0, false, "B", string.Empty, 0, TailNum, 0).EntityList;
                if (TailVal != null && TailVal.Count > 0)
                {
                    var AircraftVal = TailService.GetAircraftByAircraftID((long)TailVal[0].AircraftID).EntityList;
                    if (AircraftVal != null && AircraftVal.Count > 0)
                        AircraftVM = (AircraftViewModel)AircraftVM.InjectFrom(((FlightPakMasterService.Aircraft)AircraftVal[0]));
                }
                return AircraftVM;
            }
        }



        #region This is a duplicate copy from postflight crew, so commenting it out for now and will remove it later after testing
        /// <summary>
        /// Method to Calculate Days Away for Crew
        /// </summary>
        /// <param name="Crewid">Pass Crew ID</param>
        /// <param name="Legnum">Pass Leg Number</param>
        /// <returns>Returns Days Away Value</returns>
        //public decimal DaysAwayCalculation(PostflightMain TripLog, Int64 Crewid, Int64 Legnum, UserPrincipalViewModel userPrincipal)
        //{
        //    int daysAway = 0;

        //    if (TripLog != null && TripLog.PostflightLegs != null)
        //    {
        //        PostflightLeg SelectLeg = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.LegNUM == Legnum).FirstOrDefault();

        //        DateTime beginDT = DateTime.MinValue;
        //        DateTime endDT = DateTime.MinValue;

        //        List<PostflightLeg> legsList = TripLog.PostflightLegs.Where(x => x.IsDeleted == false && x.LegNUM < Legnum).OrderByDescending(x => x.LegNUM).ToList();
        //        if (legsList.Count == 0)
        //        {
        //            if (userPrincipal._DutyBasis != null && userPrincipal._DutyBasis == 2) // No Out Date Display
        //            {
        //                if ((SelectLeg.ScheduledTM != null))
        //                    beginDT = SelectLeg.ScheduledTM.Value;
        //            }
        //            else
        //            {
        //                if ((SelectLeg.OutboundDTTM != null))
        //                    beginDT = SelectLeg.OutboundDTTM.Value;
        //            }

        //            if (SelectLeg.InboundDTTM != null)
        //                endDT = SelectLeg.InboundDTTM.Value;

        //            if (beginDT != DateTime.MinValue && endDT != DateTime.MinValue)
        //                daysAway = endDT.Date.Subtract(beginDT.Date).Days + 1;

        //        }
        //        else
        //        {
        //            int counter = 0;
        //            foreach (PostflightLeg Leg in legsList)
        //            {
        //                if (Leg.PostflightCrews != null)
        //                {
        //                    foreach (PostflightCrew Crew in Leg.PostflightCrews.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM))
        //                    {
        //                        if (Crew.CrewID == Crewid)
        //                        {
        //                            if (Leg.InboundDTTM != null && Leg.InboundDTTM.HasValue)
        //                                endDT = Leg.InboundDTTM.Value;

        //                            counter = 1;
        //                            break;
        //                        }
        //                    }
        //                }
        //                if (counter == 1)
        //                {
        //                    break;
        //                }
        //            }

        //            if (userPrincipal._DutyBasis != null && userPrincipal._DutyBasis == 2) // No Out Date Display
        //            {
        //                if ((SelectLeg.ScheduledTM != null))
        //                    beginDT = SelectLeg.ScheduledTM.Value;
        //            }
        //            else
        //            {
        //                if ((SelectLeg.OutboundDTTM != null))
        //                    beginDT = SelectLeg.OutboundDTTM.Value;
        //            }

        //            if (beginDT != DateTime.MinValue && endDT != DateTime.MinValue)
        //            {
        //                daysAway = beginDT.Date.Subtract(endDT.Date).Days;
        //                if (daysAway == 0)
        //                {
        //                    daysAway = 1;
        //                }
        //            }
        //            else if (endDT == DateTime.MinValue)
        //            {
        //                daysAway = 1;
        //            }

        //            if (SelectLeg.InboundDTTM != null)
        //                daysAway += SelectLeg.InboundDTTM.Value.Subtract(beginDT.Date).Days;
        //        }
        //    }

        //    return Convert.ToDecimal(daysAway);
        //}
        #endregion

        /// <summary>
        /// Method to empty Foreign key objects before saving via Entity Framework
        /// </summary>
        /// <param name="Trip">PostflightMain object</param>
        public static PostflightMain CommonClearFKObjectsBeforeSave(PostflightMain Trip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                if (Trip != null)
                {
                    if (Trip.Account != null) { Trip.Account = null; Trip.AccountReference = null; }
                    if (Trip.Aircraft != null) { Trip.Aircraft = null; Trip.AircraftReference = null; }
                    if (Trip.Company != null) { Trip.Company = null; Trip.CompanyReference = null; }
                    if (Trip.Client != null) { Trip.Client = null; Trip.ClientReference = null; }
                    if (Trip.Customer != null) { Trip.Customer = null; Trip.CustomerReference = null; }
                    if (Trip.Department != null) { Trip.Department = null; Trip.DepartmentReference = null; }
                    if (Trip.DepartmentAuthorization != null) { Trip.DepartmentAuthorization = null; Trip.DepartmentAuthorizationReference = null; }
                    if (Trip.Fleet != null) { Trip.Fleet = null; Trip.FleetReference = null; }
                    if (Trip.Passenger != null) { Trip.Passenger = null; Trip.PassengerReference = null; }

                    if (Trip.ClientID == 0) { Trip.ClientID = null; } // Check for saving without sending Client ID
                    if (Trip.HomebaseID == 0) { Trip.HomebaseID = null; }
                    if (Trip.FleetID == 0) { Trip.FleetID = null; }
                    if (Trip.PassengerRequestorID == 0) { Trip.PassengerRequestorID = null; }
                    if (Trip.AccountID == 0) { Trip.AccountID = null; }
                    if (Trip.DepartmentID == 0) { Trip.DepartmentID = null; }
                    if (Trip.AuthorizationID == 0) { Trip.AuthorizationID = null; }

                    if (Trip.PostflightLegs != null)
                    {
                        foreach (PostflightLeg Leg in Trip.PostflightLegs)
                        {
                            //Below code clears foreign key referenced objects
                            if (Leg.Account != null) { Leg.Account = null; Leg.AccountReference = null; }
                            if (Leg.Airport != null) { Leg.Airport = null; Leg.AirportReference = null; }
                            if (Leg.Airport1 != null) { Leg.Airport1 = null; Leg.Airport1Reference = null; }
                            //if (Leg.Company != null) { Leg.Company = null;
                            if (Leg.FBO != null) { Leg.FBO = null; Leg.FBOReference = null; }
                            if (Leg.FBO1 != null) { Leg.FBO1 = null; Leg.FBO1Reference = null; }
                            if (Leg.Client != null) { Leg.Client = null; Leg.ClientReference = null; }
                            if (Leg.Crew != null) { Leg.Crew = null; Leg.CrewReference = null; }
                            if (Leg.Customer != null) { Leg.Customer = null; Leg.CustomerReference = null; }
                            if (Leg.DelayType != null) { Leg.DelayType = null; Leg.DelayTypeReference = null; }
                            if (Leg.Department != null) { Leg.Department = null; Leg.DepartmentReference = null; }
                            if (Leg.DepartmentAuthorization != null) { Leg.DepartmentAuthorization = null; Leg.DepartmentAuthorizationReference = null; }
                            if (Leg.FlightCatagory != null) { Leg.FlightCatagory = null; Leg.FlightCatagoryReference = null; }
                            if (Leg.Passenger != null) { Leg.Passenger = null; Leg.PassengerReference = null; }
                            if (Leg.PostflightMain != null) { Leg.PostflightMain = null; Leg.PostflightMainReference = null; }
                            //if (Leg.UserMaster != null) Leg.UserMaster = null;

                            if (Leg.AccountID == 0) { Leg.AccountID = null; }
                            if (Leg.ArriveICAOID == 0) { Leg.ArriveICAOID = null; }
                            if (Leg.AuthorizationID == 0) { Leg.AuthorizationID = null; }
                            if (Leg.ClientID == 0) { Leg.ClientID = null; }
                            if (Leg.CrewID == 0) { Leg.CrewID = null; }
                            if (Leg.CustomerID == 0) { Leg.CustomerID = null; }
                            if (Leg.DelayTypeID == 0) { Leg.DelayTypeID = null; }
                            if (Leg.DepartICAOID == 0) { Leg.DepartICAOID = null; }
                            if (Leg.DepartmentID == 0) { Leg.DepartmentID = null; }
                            if (Leg.FlightCategoryID == 0) { Leg.FlightCategoryID = null; }
                            if (Leg.PassengerRequestorID == 0) { Leg.PassengerRequestorID = null; }

                            if (Leg.PostflightCrews != null)
                            {
                                foreach (PostflightCrew Crew in Leg.PostflightCrews)
                                {
                                    if (Crew.Customer != null) { Crew.Customer = null; Crew.CustomerReference = null; }
                                    if (Crew.Crew != null) { Crew.Crew = null; Crew.CrewReference = null; }
                                    if (Crew.Crew != null) { Crew.PostflightLeg = null; Crew.PostflightLegReference = null; }
                                    if (Crew.Crew != null) { Crew.PostflightMain = null; Crew.PostflightMainReference = null; }

                                    if (Crew.CustomerID == 0) { Crew.CustomerID = null; }
                                }
                            }

                            if (Leg.PostflightPassengers != null)
                            {
                                foreach (PostflightPassenger Pax in Leg.PostflightPassengers)
                                {
                                    if (Pax.Customer != null) { Pax.Customer = null; Pax.CustomerReference = null; }
                                    if (Pax.FlightPurpose != null) { Pax.FlightPurpose = null; Pax.FlightPurposeReference = null; }
                                    if (Pax.Passenger != null) { Pax.Passenger = null; Pax.PassengerReference = null; }
                                    if (Pax.PostflightLeg != null) { Pax.PostflightLeg = null; Pax.PostflightLegReference = null; }
                                    if (Pax.PostflightMain != null) { Pax.PostflightMain = null; Pax.PostflightMainReference = null; }

                                    if (Pax.CustomerID == 0) { Pax.CustomerID = null; }
                                    if (Pax.FlightPurposeID == 0 || Pax.FlightPurposeID == -1) { Pax.FlightPurposeID = null; }
                                    if (Pax.PassengerID == 0) { Pax.PassengerID = null; }
                                }
                            }
                            if (Leg.PostflightExpenses != null)
                            {
                                foreach (PostflightExpense Expense in Leg.PostflightExpenses)
                                {
                                    if (Expense.Customer != null) { Expense.Customer = null; Expense.CustomerReference = null; }
                                    if (Expense.Account != null) { Expense.Account = null; Expense.AccountReference = null; }
                                    if (Expense.Airport != null) { Expense.Airport = null; Expense.AirportReference = null; }
                                    if (Expense.Crew != null) { Expense.Crew = null; Expense.CrewReference = null; }
                                    if (Expense.Company != null) { Expense.Company = null; Expense.CompanyReference = null; }
                                    if (Expense.FBO != null) { Expense.FBO = null; Expense.FBOReference = null; }
                                    if (Expense.Fleet != null) { Expense.Fleet = null; Expense.FleetReference = null; }
                                    if (Expense.FlightCatagory != null) { Expense.FlightCatagory = null; Expense.FlightCatagoryReference = null; }
                                    if (Expense.FuelLocator != null) { Expense.FuelLocator = null; Expense.FuelLocatorReference = null; }
                                    if (Expense.PaymentType != null) { Expense.PaymentType = null; Expense.PaymentTypeReference = null; }
                                    if (Expense.PostflightLeg != null) { Expense.PostflightLeg = null; Expense.PostflightLegReference = null; }
                                    if (Expense.PostflightMain != null) { Expense.PostflightMain = null; Expense.PostflightMainReference = null; }
                                    if (Expense.Vendor != null) { Expense.Vendor = null; Expense.VendorReference = null; }

                                    if (Expense.AccountID == 0) { Expense.AccountID = null; }
                                    if (Expense.AirportID == 0) { Expense.AirportID = null; }
                                    if (Expense.CrewID == 0) { Expense.CrewID = null; }
                                    if (Expense.CustomerID == 0) { Expense.CustomerID = null; }
                                    if (Expense.FBOID == 0) { Expense.FBOID = null; }
                                    if (Expense.FleetID == 0) { Expense.FleetID = null; }
                                    if (Expense.FlightCategoryID == 0) { Expense.FlightCategoryID = null; }
                                    if (Expense.FuelLocatorID == 0) { Expense.FuelLocatorID = null; }
                                    if (Expense.HomebaseID == 0) { Expense.HomebaseID = null; }
                                    if (Expense.PaymentTypeID == 0) { Expense.PaymentTypeID = null; }
                                    if (Expense.PaymentVendorID == 0) { Expense.PaymentVendorID = null; }
                                }
                            }

                            if (Leg.PostflightSIFLs != null)
                            {
                                foreach (PostflightSIFL Sifl in Leg.PostflightSIFLs)
                                {
                                    if (Sifl.Airport != null) { Sifl.Airport = null; Sifl.AirportReference = null; }
                                    if (Sifl.Airport1 != null) { Sifl.Airport1 = null; Sifl.Airport1Reference = null; }
                                    if (Sifl.Customer != null) { Sifl.Customer = null; Sifl.CustomerReference = null; }
                                    if (Sifl.Passenger != null) { Sifl.Passenger = null; Sifl.PassengerReference = null; }
                                    if (Sifl.Passenger1 != null) { Sifl.Passenger1 = null; Sifl.Passenger1Reference = null; }
                                    if (Sifl.FareLevel != null) { Sifl.FareLevel = null; Sifl.FareLevelReference = null; }
                                    if (Sifl.PostflightLeg != null) { Sifl.PostflightLeg = null; Sifl.PostflightLegReference = null; }
                                    if (Sifl.PostflightMain != null) { Sifl.PostflightMain = null; Sifl.PostflightMainReference = null; }

                                    if (Sifl.ArriveICAOID == 0) { Sifl.ArriveICAOID = null; }
                                    if (Sifl.AssociatedPassengerID == 0) { Sifl.AssociatedPassengerID = null; }
                                    if (Sifl.CustomerID == 0) { Sifl.CustomerID = null; }
                                    if (Sifl.DepartICAOID == 0) { Sifl.DepartICAOID = null; }
                                    if (Sifl.FareLevelID == 0) { Sifl.FareLevelID = null; }
                                    if (Sifl.PassengerRequestorID == 0) { Sifl.PassengerRequestorID = null; }
                                }
                            }
                        }
                    }
                }
            }
            return Trip;
        }

    }
}