﻿using FlightPak.Web.CalculationService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Omu.ValueInjecter;

namespace FlightPak.Web.BusinessLite.Postflight
{
    public class PostflightLegLite
    {
   
        public static Hashtable POCalculateMiles(string departICAOID, string arriveICAOID)
        {
            Hashtable RetObject = new Hashtable();
            double miles = 0;
            string retValue = "0";
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(departICAOID, arriveICAOID))
            {
                if (!string.IsNullOrEmpty(departICAOID) && !string.IsNullOrEmpty(arriveICAOID))
                {
                    using (CalculationServiceClient CalcService = new CalculationServiceClient())
                    {
                        if (departICAOID != "0" && arriveICAOID != "0")
                        {
                            miles = CalcService.GetDistance(Convert.ToInt64(departICAOID), Convert.ToInt64(arriveICAOID));
                            if (miles != null && miles > 0)
                            {
                                retValue = ConvertToKilomenterBasedOnCompanyProfile((decimal)miles).ToString();
                            }
                        }
                    }
                }
            }

            RetObject["KiloOrMiles"] = retValue;
            RetObject["Miles"] = miles;
            return RetObject; 
        }

        public static decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Miles))
            {
                UserPrincipalViewModel UserPrincipal = FlightPak.Web.Views.Transactions.PostFlight.PostflightTripManager.getUserPrincipal();
                if (UserPrincipal._IsKilometer == null ? false : (bool) UserPrincipal._IsKilometer)
                    return Math.Round(Miles*1.852M, 0);
                else
                    return Miles;
            }
        }

        public static decimal ConvertToMilesBasedOnCompanyProfile(decimal Kilometers)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Kilometers))
            {
                UserPrincipalViewModel UserPrincipal = FlightPak.Web.Views.Transactions.PostFlight.PostflightTripManager.getUserPrincipal();
                if (UserPrincipal._IsKilometer == null ? false : (bool) UserPrincipal._IsKilometer)
                    return Math.Round(Kilometers/1.852M, 0);
                else
                    return Kilometers;
            }
        }

        public static List<PostflightLegViewModel> GetLiveLegs(PostflightLogViewModel TripLogViewModel)
        {
            if (TripLogViewModel != null && TripLogViewModel.PostflightLegs != null)
                return TripLogViewModel.PostflightLegs.Where(x => x.IsDeleted == false).OrderBy(p => p.LegNUM).ToList();
            else
                return new List<PostflightLegViewModel>();
        }

    }
}