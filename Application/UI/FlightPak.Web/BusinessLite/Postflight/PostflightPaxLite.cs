﻿using FlightPak.Web.CalculationService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.PostflightService;
using FlightPak.Web.ViewModels.Postflight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Omu.ValueInjecter;
using FlightPak.Web.Framework.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web.BusinessLite.Postflight
{
    public class PostflightPaxLite
    {
        #region Passenger
        
        /// <summary>
        /// Method to Calculate SIFL (Standard Industry Fare Level)
        /// </summary>
        /// <param name="isCustom">Pass Boolean value to calculate based on custom rate or not</param>
        /// <returns>Returns SIFL total</returns>
        
        internal List<PostflightLegPassengerViewModel> GetPaxListofGrid()
        {
            List<PostflightLegPassengerViewModel> paxList = new List<PostflightLegPassengerViewModel>();
            var Trip = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            foreach (var legitem in Trip.PostflightLegs)
            {

                foreach (var itemPax in legitem.PostflightLegPassengers)
                {
                    if (!paxList.Exists(d => d.PassengerRequestorCD == itemPax.PassengerRequestorCD))
                    {
                        paxList.Add(itemPax);
                    }
                }
            }
            return paxList;
        }

        internal int AddNewPax(List<string> passengerCDs, int? InsertPosition, bool? IsPassengerIds)
        {
            int paxAdded = 0;            
            var Trip = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (Trip == null)
            {
                Trip = new PostflightLogViewModel();
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = Trip;
            }
            if (Trip != null)
            {
                var Preflegs = Trip.PostflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted==false).OrderBy(x => x.LegNUM).ToList();
                
                using (FlightPak.Web.PreflightService.PreflightServiceClient PrefService = new FlightPak.Web.PreflightService.PreflightServiceClient())
                {
                    var OldpassengerInLegs = GetPaxListofGrid();                    
                    int MaxORderNum = 0;
                    if (OldpassengerInLegs.Count > 0)
                    {
                        MaxORderNum = OldpassengerInLegs.Max(f => (int)f.OrderNUM) + 1;
                    }

                    for (int i = 0; i < passengerCDs.Count; i++)
                    {
                        var paxInfo = new PostflightLegPassengerViewModel();
                        if (IsPassengerIds == true)
                        {
                            paxInfo = DBCatalogsDataLoader.GetAllPreflightAvailablePaxList_FromIDORCD(string.Empty,Convert.ToInt64(passengerCDs[i].ToLower().Trim()));
                        }
                        else
                        {
                            paxInfo = DBCatalogsDataLoader.GetAllPreflightAvailablePaxList_FromIDORCD(passengerCDs[i].ToLower().Trim());
                        }
                        if (paxInfo != null && paxInfo.PassengerRequestorID > 0)
                        {

                            if (OldpassengerInLegs.Exists(p => p.PassengerRequestorID == paxInfo.PassengerRequestorID))
                            {
                                var AvailPax = OldpassengerInLegs.Where(p => p.PassengerRequestorID == paxInfo.PassengerRequestorID).FirstOrDefault();
                                if (AvailPax.State == TripEntityState.Deleted || AvailPax.IsDeleted == true)
                                {
                                    AvailPax.State = TripEntityState.Modified;
                                }                                
                                continue;
                            }

                            if (!string.IsNullOrWhiteSpace(Convert.ToString(paxInfo.PassportExpiryDT)))
                            {
                                DateTime? dtDate = Convert.ToDateTime(paxInfo.PassportExpiryDT);
                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                    paxInfo.PassportExpiryDT = dtDate.FSSParseDateTimeString(Trip.PostflightMain.dateFormat);
                                else
                                    paxInfo.PassportExpiryDT = null;
                            }

                            paxInfo.OrderNUM = MaxORderNum++;
                            
                            foreach (var item in Preflegs)
                            {
                                if (paxInfo.PostflightPassengerListID == 0)
                                    paxInfo.State = TripEntityState.Added;
                                else
                                    paxInfo.State = TripEntityState.Modified;

                                paxInfo.LegNUM = Convert.ToInt32(item.LegNUM);
                                paxInfo.IsDeleted = false;
                                if (InsertPosition == null)
                                    paxInfo.OrderNUM = item.PostflightLegPassengers.Where(p=>p.IsDeleted==false && p.State!= TripEntityState.Deleted).ToList().Count + 1;
                                else
                                    paxInfo.OrderNUM = InsertPosition;

                                // To Avoid Object reference pointing to same memory
                                PostflightLegPassengerViewModel PaxInLeg = (PostflightLegPassengerViewModel)new PostflightLegPassengerViewModel().InjectFrom(paxInfo);
                                item.PostflightLegPassengers.Add(PaxInLeg);
                                item.PassengerTotal = (item.PassengerTotal??0) + 1;
                            }
                            paxAdded++;
                        }                        
                    }
                }
                if (paxAdded > 0 && InsertPosition!=null)
                {
                    foreach (var legObject in Preflegs)
                    {
                        resetOrderNumOfPassengerInLeg(legObject);
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = Trip;
                return paxAdded;
            }
            return paxAdded;
        }

        internal int DeletePax(List<string> passengerRequestorCDs)
        {
            var Trip = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            int removedItems = 0;
            foreach (var legitem in Trip.PostflightLegs)
            {
                var paxlist = legitem.PostflightLegPassengers.Where(t => passengerRequestorCDs.Contains((t.PassengerRequestorCD ?? "").Trim())).ToList();
                foreach (var paxitem in paxlist)
                {
                    if (paxitem.PostflightPassengerListID == 0)
                    {
                        legitem.PostflightLegPassengers.Remove(paxitem);
                    }
                    else
                    {
                        paxitem.IsDeleted = true;
                        paxitem.State = TripEntityState.Deleted;
                    }

                }
                resetOrderNumOfPassengerInLeg(legitem);
                removedItems = paxlist.ToList().Count;

                legitem.PassengerTotal = (legitem.PassengerTotal ?? 0) - removedItems;
                
            }
            return removedItems;
        }

        internal int DeletePaxByFirstName(List<string> firstNamelist)
        {
            var Trip = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            int removedItems = 0;
            foreach (var legitem in Trip.PostflightLegs)
            {
                var paxlist = legitem.PostflightLegPassengers.Where(t => firstNamelist.Contains((t.PassengerFirstName ?? "").Trim())).ToList();
                foreach (var paxitem in paxlist)
                {
                    if (paxitem.PostflightPassengerListID == 0)
                    {
                        legitem.PostflightLegPassengers.Remove(paxitem);
                    }
                    else
                    {
                        paxitem.IsDeleted = true;
                        paxitem.State = TripEntityState.Deleted;
                    }
                }
                resetOrderNumOfPassengerInLeg(legitem);
                removedItems = paxlist.ToList().Count;

                legitem.PassengerTotal = (legitem.PassengerTotal ?? 0) - removedItems;

            }
            return removedItems;
        }

        private void resetOrderNumOfPassengerInLeg(PostflightLegViewModel POLeg)
        {
            int i=1;
            foreach (var pax in POLeg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted==false).OrderBy(o => o.OrderNUM).ToList())
            {
                if (pax.PostflightPassengerListID > 0 && pax.State== TripEntityState.NoChange)
                    pax.State = TripEntityState.Modified;

                pax.OrderNUM = i;
                i++;
            }
        }


        internal void SaveLegPAXFlightPurpose(string legNum, string PaxCode, string FlightPurposeID, string PassengerFirstName)
        {
            var Trip = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            
            if (Trip != null)
            {
                var poleg=Trip.PostflightLegs.Where(l=>l.State!= TripEntityState.Deleted && l.LegNUM==Convert.ToInt32(legNum)).FirstOrDefault();
                int countPaxValue = 0;
                if (poleg != null && poleg.PostflightLegPassengers != null && poleg.PostflightLegPassengers.Count > 0 && poleg.FlightCatagory != null)
                {
                    var paxList = poleg.PostflightLegPassengers;
                    var passenger = new PostflightLegPassengerViewModel();
                    if (string.IsNullOrWhiteSpace(PaxCode) && PassengerFirstName != null)
                    {
                        passenger = paxList.Where(p => p.PassengerFirstName == PassengerFirstName).FirstOrDefault();
                    }
                    else
                        passenger = paxList.Where(p => p.PassengerRequestorCD == PaxCode || p.PassengerRequestorCD == PaxCode).FirstOrDefault();

                    if (passenger != null && !string.IsNullOrWhiteSpace(FlightPurposeID))
                    {
                        if (Convert.ToInt64(FlightPurposeID) <= 1 && (passenger.FlightPurposeID == null || passenger.FlightPurposeID > 1))
                        {
                            countPaxValue = -1;
                        }
                        else if (Convert.ToInt64(FlightPurposeID) > 1 && (passenger.FlightPurposeID==null || passenger.FlightPurposeID <= 1))
                        {
                            countPaxValue = 1;
                        }
                        passenger.FlightPurposeID = long.Parse(FlightPurposeID);
                        if (passenger.FlightPurposeID <= 1)
                        {
                            passenger.FlightPurposeID = null;
                            passenger.DepartPercentage = 0;
                        }
                        

                        if (passenger.PostflightPassengerListID > 0)
                            passenger.State = TripEntityState.Modified;
                        else
                            passenger.State = TripEntityState.Added;

                        poleg.PassengerTotal = (poleg.PassengerTotal ?? 0) + countPaxValue;
                    }
                    poleg.PostflightLegPassengers= paxList;
                    
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = Trip;
            }

        }

        internal List<PostflightLegPassengerViewModel> LoadPaxSummaryGridData()
        {
            List<PostflightLegPassengerViewModel> Newpassengerlist = new List<PostflightLegPassengerViewModel>();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
            {
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                var UserPrinciple = (ViewModels.UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
                if (TripLog.PostflightLegs != null)
                {
                    var POlegs = TripLog.PostflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    foreach (var poLeg in POlegs)
                    {
                        if (poLeg.State != TripEntityState.Deleted && poLeg.IsDeleted==false)
                        {
                            if (poLeg.PostflightLegPassengers!=null)
                            {
                                var totalPax=poLeg.PostflightLegPassengers.Where(p=>p.IsDeleted==false && p.State != TripEntityState.Deleted && p.PassengerRequestorCD!=null).ToList();
                                foreach (var itemPax in totalPax)
                                {
                                    if (itemPax.State != TripEntityState.Deleted && itemPax.FlightPurposeID.HasValue && itemPax.FlightPurposeID > 1)
                                    {
                                        PostflightLegPassengerViewModel paxSum = new PostflightLegPassengerViewModel();
                                        Newpassengerlist.Add(paxSum);
                                        paxSum = (PostflightLegPassengerViewModel)paxSum.InjectFrom(itemPax);
                                        paxSum.LegDescription = "Leg " + poLeg.LegNUM + " (" + poLeg.DepartureAirport.IcaoID + "-" + poLeg.ArrivalAirport.IcaoID + ")";
                                        paxSum.LegNUM = poLeg.LegNUM;
                                        // Passport related info.

                                        CacheLite cacheData = new CacheLite();
                                        var passport = paxSum.PassportID.HasValue ==true? cacheData.GetPassportFromCache(paxSum.PassportID.Value):null;
                                        if (passport == null)
                                        {
                                            FlightPak.Web.BusinessLite.Preflight.PreflightPaxLite preflightPaxLite = new Preflight.PreflightPaxLite();
                                            var PaxPassportList = preflightPaxLite.GetPaxPassportData(((long)paxSum.PassengerRequestorID).ToString()).Where(g => g.IsDeleted == false && g.Choice == true).ToList();
                                            if (PaxPassportList != null && PaxPassportList.Count > 0)
                                            {
                                                cacheData.AddPassportToCache(PaxPassportList[0]);
                                            }
                                            else
                                            {
                                                cacheData.AddPassportToCache(new FlightPak.Web.ViewModels.PaxPassportViewModel() { PassengerID = (long)paxSum.PassengerRequestorID });
                                            }
                                            passport = paxSum.PassportID.HasValue ? cacheData.GetPassportFromCache(paxSum.PassportID.Value) : null;
                                        }                                        
                                        if (passport != null)
                                        {
                                            FillPaxPassportDetailsFromPassportObject(ref paxSum, passport);
                                        }

                                    }
                                }
                            }
                        } // end if - leg is deleted
                    } // end of for loop
                } // if end - session for AvailableList is not available
            }// if end

            return Newpassengerlist;
            
        }

        internal void SaveGridtoSession(dynamic Obj)
        {
            var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

            if (TripLog != null)
            {
                foreach (var leg in PostflightLegLite.GetLiveLegs(TripLog))
                {
                    if (leg.PostflightLegPassengers!=null && leg.FlightCatagory!=null)
                    {
                        var paxList = leg.PostflightLegPassengers.Where(d => d.State != TripEntityState.Deleted && d.IsDeleted == false).ToList();
                        foreach (var item in Obj)
                        {
                            if (string.Compare((item.Leg??"").ToString(), string.Concat("Leg", leg.LegNUM.ToString()), true) == 0)
                            {
                                string code = Convert.ToString(item.PassengerRequestorCD);
                                string firstName= Convert.ToString(item.PassengerFirstName);
                                var passenger = new PostflightLegPassengerViewModel();
                                if(string.IsNullOrWhiteSpace(code))
                                {
                                    passenger = paxList.Where(p => p.PassengerFirstName == firstName && p.PassengerRequestorID == 0).FirstOrDefault();
                                }else
                                 passenger = paxList.Where(p => p.PassengerRequestorCD == code).FirstOrDefault();

                                if (passenger == null)
                                {
                                    var objPaxNewVM = GetPaxListofGrid().Where(p => p.PassengerRequestorCD == code && p.State != TripEntityState.Deleted && p.IsDeleted == false).FirstOrDefault();
                                    if (objPaxNewVM != null && objPaxNewVM.State != TripEntityState.Deleted)
                                    {
                                        passenger = (PostflightLegPassengerViewModel)new PostflightLegPassengerViewModel().InjectFrom(objPaxNewVM);
                                        passenger.State = TripEntityState.Added;
                                        passenger.PostflightPassengerListID = 0;
                                        paxList.Add(passenger);
                                    }
                                }
                                if (passenger != null)
                                {
                                    if (item.FlightPurposeID <= 1)
                                        item.FlightPurposeID = null;

                                    passenger.FlightPurposeID = item.FlightPurposeID;
                                    if (passenger.PostflightPassengerListID > 0 && leg.POLegID > 0)
                                        passenger.State = TripEntityState.Modified;
                                    else
                                        passenger.State = TripEntityState.Added;


                                }
                            }
                        }
                        leg.PostflightLegPassengers = paxList;
                        if (leg.State == TripEntityState.NoChange)
                            leg.State = TripEntityState.Modified;

                        leg.PassengerTotal = leg.PostflightLegPassengers.Where(f => f.FlightPurposeID > 1 && f.State != TripEntityState.Deleted && f.IsDeleted == false).Count();
                        //leg.ReservationAvailable = leg.SeatTotal - leg.PassengerTotal;

                    }
                }


                HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
            }
        }
        public void FillPaxPassportDetailsFromPassportObject(ref PostflightLegPassengerViewModel PassengerObject,FlightPak.Web.ViewModels.PaxPassportViewModel passport)
        {
            PassengerObject.PassportID = passport.PassportID;
            PassengerObject.PassportNUM = passport.PassportNum;
            PassengerObject.IssueDT = passport.IssueDT;
            PassengerObject.PassportExpiryDT = passport.PassportExpiryDT;
            PassengerObject.Nation = passport.CountryCD;
            PassengerObject.Choice = passport.Choice;
        }

        internal bool SavePaxBillingInfo(long passengerRequstorID, long legNum, string billingInfo)
        {
            PostflightLogViewModel pfLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (pfLog != null)
            {
                if (pfLog.PostflightLegs != null && pfLog.PostflightLegs.Count > 0)
                {
                    var leg = pfLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false && l.LegNUM == legNum).FirstOrDefault();
                    if (leg != null)
                    {
                        var Pax = leg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false && p.PassengerRequestorID == passengerRequstorID).FirstOrDefault();
                        if (Pax != null)
                        {
                            Pax.Billing = billingInfo;
                            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog]=pfLog;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        #endregion

        #region SIFL

        // return array [0]-multiplier value, [1]-associated employee id
        public List<string> getMultiplierAscPaxID(bool isSIFLSec, string empType, string AssociatedEmpCode, PostflightLogViewModel PostflightLog, ref PostflightLegSIFLViewModel PoSiflVM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isSIFLSec, empType, AssociatedEmpCode))
            {
                List<string> ret = new List<string> { "0", "0" };
                if (PostflightLog.PostflightMain.Fleet != null && PostflightLog.PostflightMain.Fleet.FleetID > 0)
                {
                    if (isSIFLSec)
                    {
                        ret[0] = (PostflightLog.PostflightMain.Fleet.MultiSec ?? 0).ToString();
                    }
                    else if (empType == WebConstants.POEmployeeType.C)
                    {
                        ret[0] = (PostflightLog.PostflightMain.Fleet.SIFLMultiControlled ?? 0).ToString();
                    }
                    else if (empType == WebConstants.POEmployeeType.N)
                    {
                        ret[0] = (PostflightLog.PostflightMain.Fleet.SIFLMultiNonControled ?? 0).ToString();
                    }
                    else if (empType == WebConstants.POEmployeeType.G && !string.IsNullOrWhiteSpace(AssociatedEmpCode))
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient AssocService = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var AssocVal = AssocService.GetAllPassengerWithFilters(0, 0, 0, AssociatedEmpCode.ToUpper().Trim(), false, false, string.Empty, 0).EntityList;
                            //.GetPassengerList().EntityList.Where(x => x.PassengerRequestorCD.ToString().ToUpper().Trim().Equals(AssociatedEmpCode.ToUpper().Trim())).ToList();
                            if (AssocVal != null && AssocVal.Count > 0)
                            {
                                try
                                {
                                    PoSiflVM.AssocPassenger = (PostflightLegPassengerViewModel)new PostflightLegPassengerViewModel().InjectFrom(AssocVal[0]);
                                    PoSiflVM.AssocPaxCD = (AssociatedEmpCode ?? "").ToUpper().Trim();
                                    PoSiflVM.AssociatedPassengerID = AssocVal[0].PassengerRequestorID;
                                    if (AssocVal[0].IsSIFL != null && (bool)AssocVal[0].IsSIFL)
                                    {
                                        ret[0] = (PostflightLog.PostflightMain.Fleet.MultiSec ?? 0).ToString();
                                    }
                                    else if (!String.IsNullOrWhiteSpace(AssocVal.FirstOrDefault().IsEmployeeType.ToString().Trim()))
                                    {
                                        empType = AssocVal.FirstOrDefault().IsEmployeeType.ToString().Trim();
                                        if (empType == WebConstants.POEmployeeType.C)
                                            ret[0] = (PostflightLog.PostflightMain.Fleet.SIFLMultiControlled ?? 0).ToString();
                                        else if (empType == WebConstants.POEmployeeType.N)
                                            ret[0] = (PostflightLog.PostflightMain.Fleet.SIFLMultiNonControled ?? 0).ToString();
                                        else
                                            ret[0] = "0";
                                    }
                                    PoSiflVM.AssocPassenger.PassengerName = PoSiflVM.AssocPassenger.PassengerLastName + " ," + PoSiflVM.AssocPassenger.PassengerFirstName;
                                }
                                catch (Exception eexx)
                                {
                                    //Manually Handled
                                    // Catch Block integrated to avoid un-expected exception
                                    ret[0] = "0";
                                }                                
                                ret[1] = AssocVal.FirstOrDefault().PassengerRequestorID.ToString();                                
                            }
                        }
                    }
                }
                return ret;
            }
        }

        public decimal CalculateSIFL(bool isCustom, PostflightLegSIFLViewModel PassengerSIFLVM, List<GetPOSIFLRate> siflRateList, FleetViewModel fleetProfileList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isCustom, PassengerSIFLVM))
            {
                // Declarations
                decimal siflTotal = 0;
                decimal distanceRate1 = 0;
                decimal distanceRate2 = 0;
                decimal distanceRate3 = 0;
                decimal distancRateTotal = 0;
                PassengerSIFLVM.AircraftMultiplier = PassengerSIFLVM.AircraftMultiplier ?? 0;
                decimal maxPax = 0;
                string empType = WebConstants.POEmployeeType.N;
                decimal rate1 = 0;
                decimal rate2 = 0;
                decimal rate3 = 0;

                if (siflRateList != null && siflRateList.Count > 0)
                {

                    // If selected as True, the rate will get from the Database
                    if (!isCustom && siflRateList.Count > 0)
                    {
                        rate1 = Convert.ToDecimal(siflRateList[0].Rate1.Value);
                        rate2 = Convert.ToDecimal(siflRateList[0].Rate2.Value);
                        rate3 = Convert.ToDecimal(siflRateList[0].Rate3.Value);

                        if (siflRateList[0].TerminalCHG != null)
                            PassengerSIFLVM.TeminalCharge = decimal.Round(Convert.ToDecimal(siflRateList[0].TerminalCHG), 2);
                        else
                            PassengerSIFLVM.TeminalCharge = 0;
                    }
                    else
                    {
                        rate1 = Convert.ToDecimal(PassengerSIFLVM.Rate1);
                        rate2 = Convert.ToDecimal(PassengerSIFLVM.Rate2);
                        rate3 = Convert.ToDecimal(PassengerSIFLVM.Rate3);
                    }

                    if (PassengerSIFLVM.Distance != null)
                    {
                        if (PassengerSIFLVM.Distance.Value >= 1500)
                        {
                            PassengerSIFLVM.Distance1 = 500;
                            PassengerSIFLVM.Distance2 = 1000;
                            PassengerSIFLVM.Distance3 = PassengerSIFLVM.Distance.Value - 1500;
                        }
                        else if ((PassengerSIFLVM.Distance >= 501) && (PassengerSIFLVM.Distance <= 1500))
                        {
                            PassengerSIFLVM.Distance1 = 500;
                            PassengerSIFLVM.Distance2 = PassengerSIFLVM.Distance.Value - 500;
                            PassengerSIFLVM.Distance3 = 0;
                        }
                        else if ((PassengerSIFLVM.Distance.Value > 0) && (PassengerSIFLVM.Distance.Value <= 500))
                        {
                            PassengerSIFLVM.Distance1 = PassengerSIFLVM.Distance.Value;
                            PassengerSIFLVM.Distance2 = 0;
                            PassengerSIFLVM.Distance3 = 0;
                        }
                        else
                        {
                            PassengerSIFLVM.Distance1 = 0;
                            PassengerSIFLVM.Distance2 = 0;
                            PassengerSIFLVM.Distance3 = 0;
                        }

                        // Calculate Distance Rate
                        distanceRate1 = (rate1 * PassengerSIFLVM.Distance1 ?? 0);
                        distanceRate2 = (rate2 * PassengerSIFLVM.Distance2 ?? 0);
                        distanceRate3 = (rate3 * PassengerSIFLVM.Distance3 ?? 0);

                        distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;

                        Int64? paxId = 0;
                        Int64? assPaxId = 0;
                        if (PassengerSIFLVM.PassengerRequestorID.HasValue)
                            paxId = Convert.ToInt64(PassengerSIFLVM.PassengerRequestorID);

                        if (PassengerSIFLVM.AssociatedPassengerID.HasValue && PassengerSIFLVM.AssociatedPassengerID.Value > 0)
                            assPaxId = Convert.ToInt64(PassengerSIFLVM.AssociatedPassengerID);

                        empType = PassengerSIFLVM.EmployeeTYPE.Trim();

                        if (fleetProfileList != null && fleetProfileList.FleetID > 0)
                        {
                            maxPax = fleetProfileList.MaximumPassenger ?? 0;

                            if (empType == WebConstants.POEmployeeType.C)
                                PassengerSIFLVM.AircraftMultiplier = fleetProfileList.SIFLMultiControlled ?? 0;
                            else
                                PassengerSIFLVM.AircraftMultiplier = fleetProfileList.SIFLMultiNonControled ?? 0;

                            // if PassengerRequestorID or associatedPassengerID is ISSIFL, then multiplierTotal = MultiSec...
                            if (PassengerSIFLVM.Passenger1 != null)
                            {
                                if (PassengerSIFLVM.Passenger1.IsSIFL == true) // if passenger is SIFL secured, get MultiSec for multiplier, else check for Associated PAX
                                {
                                    PassengerSIFLVM.AircraftMultiplier = fleetProfileList.MultiSec ?? 0;
                                }
                                else if (assPaxId != null && assPaxId.Value > 0)
                                {
                                    // Sifl ViewModel should have AssocPax with full details like IsSIFL and MultiSec fields.
                                    if (PassengerSIFLVM.AssocPassenger != null)
                                    {
                                        if (PassengerSIFLVM.AssocPassenger.IsSIFL != null && PassengerSIFLVM.AssocPassenger.IsSIFL == true)
                                            PassengerSIFLVM.AircraftMultiplier = fleetProfileList.MultiSec ?? 0;
                                    }
                                }
                            }
                        }
                        // Calculate SIFL Total
                        siflTotal = Math.Round(((distancRateTotal * (Convert.ToDecimal(PassengerSIFLVM.AircraftMultiplier) / 100)) + Convert.ToDecimal(PassengerSIFLVM.TeminalCharge)), 2);
                    }
                }
                return siflTotal;
            }
        }

        internal List<PostflightSIFLPersonal> PrepareSIFLPersonalList()
        {
                var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            // *** triplog Null check 
                if (TripLog != null)
                {
                    List<PostflightSIFLPersonal> personalListAll = new List<PostflightSIFLPersonal>();
                    // Get the Maximum Passenger value from Fleet Profile Catalog
                    decimal maxPax = 0;
                    if (TripLog != null && TripLog.PostflightMain.Fleet != null && TripLog.PostflightMain.FleetID != null)
                    {
                        maxPax = TripLog.PostflightMain.Fleet.MaximumPassenger ?? 0;
                    }

                    int privatePaxCount = 0;
                    int nonPrivatePaxCount = 0;
                    Dictionary<long, long> nonPvtCountDic = new Dictionary<long, long>();
                    var NonPrivatePaxIds = new Dictionary<long, long>();
                    var PrivatePaxIds = new Dictionary<long, string>();

                    foreach (var poLeg in TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false).ToList())
                    {
                        foreach (var poPax in poLeg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false).ToList())
                        {
                            long _purposeID = 0;
                            _purposeID = poPax.FlightPurposeID ?? 0;

                            List<FlightPurposeViewModel> purposeList = new List<FlightPurposeViewModel>();
                            if (HttpContext.Current.Session[WebSessionKeys.POFlightPurposeList] == null)
                            {
                                var list = DBCatalogsDataLoader.GetAllFlightpurposeList();
                                if (list != null)
                                {
                                    HttpContext.Current.Session[WebSessionKeys.POFlightPurposeList] = list;
                                    purposeList = (List<FlightPurposeViewModel>)HttpContext.Current.Session[WebSessionKeys.POFlightPurposeList];
                                }
                            }
                            purposeList = (List<FlightPurposeViewModel>)HttpContext.Current.Session[WebSessionKeys.POFlightPurposeList];
                            purposeList = purposeList.Where(x => x.FlightPurposeID == _purposeID && x.IsPersonalTravel == true).ToList();

                            if (purposeList.Count > 0)
                            {
                                if (poPax.PassengerRequestorCD != null)
                                {
                                    PrivatePaxIds.Add(privatePaxCount, poPax.PassengerRequestorCD + "-" + poLeg.LegNUM);
                                    privatePaxCount += 1;
                                }
                            }
                            else
                            {
                                nonPrivatePaxCount += 1;
                                try
                                {
                                    nonPvtCountDic[poLeg.LegNUM ?? 0] += 1;
                                }
                                catch (Exception exx)
                                {
                                    //Manually Handled
                                    // Catch Block integrated to avoid un-expected exception
                                    nonPvtCountDic.Add(poLeg.LegNUM ?? 0, 1);
                                }
                            }

                        }
                    }

                    NonPrivatePaxIds.Add(0, nonPrivatePaxCount);

                    long nonPvtCount = NonPrivatePaxIds[0];

                    if (PrivatePaxIds.Count > 0)
                    {
                        for (int i = 0; i < PrivatePaxIds.Count; i++)
                        {
                            string[] paxArray = PrivatePaxIds[i].Split('-');

                            string _paxCD = string.Empty;
                            if (paxArray[0] != null)
                                _paxCD = paxArray[0];

                            long _legNum = Convert.ToInt64(paxArray[1]);

                            if (!string.IsNullOrWhiteSpace(_paxCD))
                            {
                                foreach (var poLeg in TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted == false))
                                {
                                    foreach (var poPax in poLeg.PostflightLegPassengers.Where(p => p.State != TripEntityState.Deleted && p.IsDeleted == false && p.PassengerRequestorCD!=null))
                                    {
                                        PostflightSIFLPersonal personal = new PostflightSIFLPersonal();
                                        personal.EmpType = poPax.IsEmployeeType;
                                        personal.SIFLSecurity = poPax.SIFLSecurity;
                                        personal.PaxID = poPax.PassengerRequestorID;
                                        personal.PaxCode = poPax.PassengerRequestorCD;
                                        personal.FirstName = poPax.PassengerFirstName;
                                        personal.MiddleName = poPax.PassengerMiddleName;
                                        personal.LastName = poPax.PassengerLastName;
                                        personal.DateOfBirth = poPax.DateOfBirth;
                                        personal.Personal = "P";
                                        personal.AssocPaxCD = poPax.AssociatedWithCD;
                                        if (_paxCD.Trim().ToUpper() == poPax.PassengerRequestorCD.Trim().ToUpper())
                                        {
                                            if (_legNum == poLeg.LegNUM)
                                            {
                                                personal.LogID = poLeg.POLogID ?? 0;
                                                personal.LegID = poLeg.POLegID;
                                                if (!string.IsNullOrWhiteSpace(poLeg.ScheduledDate))
                                                    personal.ScheduledDate = Convert.ToDateTime(poLeg.ScheduledDate); //Bug 2588

                                                personal.Depart = poLeg.DepartureAirport.IcaoID;
                                                personal.DepartICAO = poLeg.DepartICAOID ?? 0;
                                                personal.Arrive = poLeg.ArrivalAirport.IcaoID;
                                                personal.ArriveICAO = poLeg.ArriveICAOID ?? 0;
                                                if (maxPax > 0)
                                                {
                                                    try
                                                    {
                                                        personal.LoadFactor = Math.Round(((nonPvtCountDic[poLeg.LegNUM ?? 0] / maxPax) * 100), 1).ToString();
                                                    }
                                                    catch (Exception exx)
                                                    {
                                                        //Manually Handled
                                                        // Catch Block integrated to avoid un-expected exception
                                                        personal.LoadFactor = "0.0";
                                                    }
                                                }
                                                else
                                                {
                                                    personal.LoadFactor = "0.0";
                                                }
                                                personalListAll.Add(personal);

                                                personal.LegNUM = poLeg.LegNUM ?? 0;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }

                    return personalListAll;
                }else
                {
                    return new List<PostflightSIFLPersonal>();
                }
        }

        internal FSSOperationResult<PostflightLegSIFLViewModel> Validation_SIFLIcao(PostflightLegSIFLViewModel siflViewmodel,bool isDepartIcao)
        {
            FSSOperationResult<PostflightLegSIFLViewModel> ResultObj = new FSSOperationResult<PostflightLegSIFLViewModel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var tripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                        if (tripLog != null)
                        {                            
                                AirportViewModel airportObject = new AirportViewModel();
                                if (isDepartIcao)
                                    airportObject = CheckIfAirportExists(true, siflViewmodel.DepartAirport.IcaoID);
                                else
                                {
                                    airportObject = CheckIfAirportExists(false, siflViewmodel.ArriveAirport.IcaoID);
                                }
                                if (airportObject != null) // (DepartsVal != null && DepartsVal.Count > 0)
                                {
                                    if (isDepartIcao)
                                    {
                                        siflViewmodel.DepartAirport = airportObject;
                                    }
                                    else
                                    {
                                        siflViewmodel.ArriveAirport = airportObject;
                                    }

                                    siflViewmodel.Distance = Convert.ToDecimal(DBCatalogsDataLoader.CalculateMiles(siflViewmodel.DepartAirport.AirportID, siflViewmodel.ArriveAirport.AirportID));
                                    // Calculate SIFL Total

                                    siflViewmodel.SIFLTotal = CalculateSIFL(false, siflViewmodel, GetCurrentSiflRatesList(), tripLog.PostflightMain.Fleet);
                                    
                                    ResultObj.Result = siflViewmodel;
                                    // Call Miles Calculation
                                    //CalculateMiles(hdnDeparts.Value, hdnArrives.Value);
                                    ResultObj.Success = true;
                                    ResultObj.StatusCode = System.Net.HttpStatusCode.OK;

                                    siflViewmodel.ArriveICAOID= siflViewmodel.ArriveAirport.AirportID;
                                    siflViewmodel.DepartICAOID = siflViewmodel.DepartAirport.AirportID;

                                    if (tripLog != null)
                                    {
                                        var siflIndex = tripLog.PostflightLegSIFLsList.FindIndex(s => s.RowNumber == siflViewmodel.RowNumber);
                                        if (siflIndex != -1)
                                        {
                                            tripLog.PostflightLegSIFLsList[siflIndex]=siflViewmodel;
                                            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = tripLog;
                                        }
                                    }
                                }
                                siflViewmodel.AmtTotal = CalculateGrandTotalSifl();
                                if (airportObject==null || airportObject.AirportID <= 0)
                                {
                                    ResultObj.Result = siflViewmodel;
                                    if(isDepartIcao)
                                        ResultObj.ErrorsList.Add(System.Web.HttpUtility.HtmlEncode("Departure ICAO Not In Trip."));
                                    else
                                        ResultObj.ErrorsList.Add(System.Web.HttpUtility.HtmlEncode("Arrival ICAO Not In Trip."));

                                    ResultObj.Success = true;
                                    ResultObj.StatusCode = System.Net.HttpStatusCode.OK;
                                    //tbDeparts.Focus();
                                }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    ResultObj.ErrorsList.Add(ex.Message);
                    ResultObj.Success = false;
                    ResultObj.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                    //The exception will be handled, logged and replaced by our custom exception. 
                    //ProcessErrorMessage(ex, ModuleNameConstants.Postflight.POPassenger);
                }
            }
            return ResultObj;
        }
        private AirportViewModel CheckIfAirportExists(bool isDeparttype, string IcaoID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isDeparttype, IcaoID))
            {
                AirportViewModel AvailableAirportObject = new AirportViewModel();
                AvailableAirportObject.IcaoID = IcaoID;
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] != null)
                {
                    PostflightLogViewModel poMain = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
                    if (poMain.PostflightLegs != null)
                    {
                        List<PostflightLegViewModel> LegList = (List<PostflightLegViewModel>)poMain.PostflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false).ToList();
                        if (LegList.Count > 0)
                        {
                            if (isDeparttype)
                            {
                                var retVal = (from leg in LegList
                                              where (leg.DepartureAirport.IcaoID??"").ToUpper().Trim() == IcaoID.ToUpper().Trim()
                                              select leg).FirstOrDefault();

                                if (retVal != null)
                                {
                                    AvailableAirportObject = retVal.DepartureAirport;                                    
                                }
                            }
                            else if (!isDeparttype)
                            {
                                var retVal = (from leg in LegList
                                              where (leg.ArrivalAirport.IcaoID ?? "").ToUpper().Trim() == IcaoID.ToUpper().Trim()
                                              select leg).FirstOrDefault();

                                if (retVal != null)
                                {
                                    AvailableAirportObject = retVal.ArrivalAirport;                                    
                                }
                            }
                        }
                    }
                }
                return AvailableAirportObject;
            }
        }
        internal FSSOperationResult<List<PostflightLegSIFLViewModel>> AddNewSIFLClick()
        {
            FSSOperationResult<List<PostflightLegSIFLViewModel>> ResultData = new FSSOperationResult<List<PostflightLegSIFLViewModel>>();
            
                try
                {
                    //Handle methods throguh exception manager with return flag
                    var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                        var TripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];

                        if (TripLog != null)
                        {
                            if (TripLog.PostflightLegSIFLsList == null)
                                TripLog.PostflightLegSIFLsList = new List<PostflightLegSIFLViewModel>();

                            List<GetPOSIFLRate> siflRateList = new List<GetPOSIFLRate>();

                            List<PostflightLegViewModel> legsList = new List<PostflightLegViewModel>();
                            legsList = TripLog.PostflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            PostflightLegSIFLViewModel Sifl = new PostflightLegSIFLViewModel();


                            //Bug 2588
                            // Get SIFL Rate based on Scheduled date 
                            if (legsList != null && legsList.Count > 0 && legsList[0].ScheduledTM != null && legsList[0].ArriveICAOID.HasValue && legsList[0].DepartICAOID.HasValue)
                            {
                                siflRateList = DBCatalogsDataLoader.GetPOSiflRate((DateTime)legsList[0].ScheduledTM);
                                if (siflRateList != null && siflRateList.Count > 0)
                                {
                                    var isScheduleDateExceedsSiflEndDate = siflRateList.Any(x => (x.StartDT <= legsList[0].ScheduledTM.Value) && (x.EndDT >= legsList[0].ScheduledTM.Value));

                                    if (!isScheduleDateExceedsSiflEndDate)
                                    {
                                        ResultData.ErrorsList.Add("NO SIFL rates have been set for the current date range being calculated. Departure Date:" + legsList[0].ScheduledTM.Value.ToShortDateString() + ".");
                                    }
                                    SetSiflRatevaluesInViewmodel(siflRateList, Sifl);
                                }else
                                {
                                    ResultData.Success = false;
                                    ResultData.StatusCode = System.Net.HttpStatusCode.OK;
                                    return ResultData;
                                }

                            }
                            else
                            {
                                if (HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey] == null)
                                {
                                    siflRateList = DBCatalogsDataLoader.GetPOSiflRate(DateTime.Now);
                                    HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey] = siflRateList;
                                }
                                if (siflRateList == null && siflRateList.Count <= 0)
                                {
                                    ResultData.Success = false;
                                    ResultData.StatusCode = System.Net.HttpStatusCode.OK;
                                    return ResultData;
                                }
                                if (siflRateList != null)
                                {
                                    siflRateList = (List<GetPOSIFLRate>)HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey];
                                    SetSiflRatevaluesInViewmodel(siflRateList, Sifl);                                    
                                }
                            }


                            // Retain the Existing SIFL Grid values
                            List<PostflightLegSIFLViewModel> RetainSilfList = new List<PostflightLegSIFLViewModel>();
                            RetainSilfList = TripLog.PostflightLegSIFLsList.Where(x => x.Passenger1.PassengerRequestorID != null && x.DepartICAOID != null && x.ArriveICAOID != null).ToList();

                            Sifl.State = TripEntityState.Added;
                            if (TripLog.POLogID > 0)
                                Sifl.POLogID = TripLog.POLogID;
                            if (legsList[0].POLegID > 0)
                                Sifl.POLegID = legsList[0].POLegID;
                            if (siflRateList != null && siflRateList.Count > 0 && siflRateList[0].FareLevelID > 0)
                                Sifl.FareLevelID = siflRateList[0].FareLevelID;

                            Sifl.EmployeeTYPE = string.Empty;
                            Sifl.Distance = 0;
                            Sifl.LoadFactor = 0;
                            Sifl.AircraftMultiplier = 0;

                            SetSiflRatevaluesInViewmodel(siflRateList, Sifl);

                            Sifl.Distance1 = 0;
                            Sifl.Distance2 = 0;
                            Sifl.Distance3 = 0;
                            Sifl.TeminalCharge = Sifl.TeminalCharge ?? 0;

                            Sifl.AmtTotal = 0;
                            Sifl.IsDeleted = false;

                            // Maintain for deleting
                            Sifl.RowNumber = RetainSilfList.Count + 1;

                            RetainSilfList.Add(Sifl);
                            TripLog.PostflightLegSIFLsList = RetainSilfList;
                            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;

                            ResultData.Result = RetainSilfList.OrderBy(o=>o.PassengerRequestorCD).ToList();
                            ResultData.Success = true;
                            ResultData.StatusCode = System.Net.HttpStatusCode.OK;
                            return ResultData;
                        }
                        else
                        {
                            ResultData.Result = new List<PostflightLegSIFLViewModel>();
                            ResultData.Success = false;
                            ResultData.StatusCode = System.Net.HttpStatusCode.OK;
                            return ResultData;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ResultData.Success = false;
                    ResultData.StatusCode = System.Net.HttpStatusCode.OK;
                    ResultData.ErrorsList.Add(ex.Message);
                    return ResultData;
                }
            
            return ResultData;
        }

        private void SetSiflRatevaluesInViewmodel(List<GetPOSIFLRate> siflRateList,PostflightLegSIFLViewModel Sifl)
        {
            if (siflRateList!=null && siflRateList.Count > 0)
            {
                Sifl.Rate1 = Math.Round((siflRateList[0].Rate1??0),4);
                Sifl.Rate2 = siflRateList[0].Rate2??0;
                Sifl.Rate3 = siflRateList[0].Rate3??0;

                if (siflRateList[0].TerminalCHG != null)
                    Sifl.TeminalCharge = Math.Round(Convert.ToDecimal(siflRateList[0].TerminalCHG), 2);
                else
                    Sifl.TeminalCharge = 0;
            }
        }

        internal List<GetPOSIFLRate> GetCurrentSiflRatesList()
        {
            List<GetPOSIFLRate> siflRateList = new List<GetPOSIFLRate>();
            if (HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey] != null)
            {
                siflRateList = (List<GetPOSIFLRate>)HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey];
            }
            else
            {
                siflRateList = DBCatalogsDataLoader.GetPOSiflRate(DateTime.Now);
                HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey] = siflRateList;
            }
            return siflRateList;
        }

        internal decimal? CalculateGrandTotalSifl()
        {
            decimal amt = 0;
            var tripLog = (PostflightLogViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog];
            if (tripLog != null && tripLog.PostflightLegSIFLsList != null)
            {
                amt = tripLog.PostflightLegSIFLsList.Sum(t => t.SIFLTotal ?? 0);
            }
            return amt;
        }

        internal FSSOperationResult<List<PostflightLegSIFLViewModel>> RecalculateSiflLegs(PostflightLogViewModel TripLog)
        {
            FSSOperationResult<List<PostflightLegSIFLViewModel>> ResultData = new FSSOperationResult<List<PostflightLegSIFLViewModel>>();
            
            decimal siflTotal = 0;
            decimal distanceRate1 = 0;
            decimal distanceRate2 = 0;
            decimal distanceRate3 = 0;
            decimal distancRateTotal = 0;

            decimal rate1 = 0;
            decimal rate2 = 0;
            decimal rate3 = 0;
            decimal terminalCharge = 0;
            long fareLevelID = 0;

            decimal miles1 = 0;
            decimal miles2 = 0;
            decimal miles3 = 0;
            decimal statueMiles = 0;
            decimal maxPax = 0;
            string empType = string.Empty;
            decimal multiplierTotal = 0;
            bool addSiflInSaveList = true;
            // Set SIFL Rate values from Fare Level
            List<GetPOSIFLRate> siflRateList = null;
            List<PostflightLegSIFLViewModel> RecalcSiflList = new List<PostflightLegSIFLViewModel>();
                        
            int legIndex = 0;
            foreach (PostflightLegViewModel leg in TripLog.PostflightLegs)
            {
                #region Loop Each Leg
                if (HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL] == null || (HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL] != null && HttpContext.Current.Session[WebSessionKeys.IsAutoCalculateSIFL].ToString() == "0"))
                {
                    
                    // Get SIFL Rate based on Scheduled date 
                    if (leg.ScheduledTM != null)
                    {
                        var siflRateCheck = DBCatalogsDataLoader.GetPOSiflRate((DateTime)leg.ScheduledTM);
                        if (siflRateCheck != null && siflRateCheck.Count > 0)
                        {
                            var isScheduleDateExceedsSiflEndDate = siflRateCheck.Any(x => (x.StartDT <= leg.ScheduledTM.Value) && (x.EndDT >= leg.ScheduledTM.Value));

                            if (!isScheduleDateExceedsSiflEndDate)
                            {
                                ResultData.ErrorsList.Add("NO SIFL rates have been set for the current date range being calculated. Departure Date:" + leg.ScheduledTM.Value.ToShortDateString() + ".");
                            }
                        }
                        else
                        {
                            ResultData.ErrorsList.Add("No SIFL rate has been set for this Customer in Database.");
                            ResultData.Success = false;
                            ResultData.StatusCode = System.Net.HttpStatusCode.OK;
                            return ResultData;
                        }
                    }
                }

                List<PostflightSIFLPersonal> personalPaxList = (List<PostflightSIFLPersonal>)this.PrepareSIFLPersonalList();
                List<PostflightSIFLPersonal> paxLegList = personalPaxList.Where(x => x.LegNUM == leg.LegNUM).ToList();

                foreach (PostflightSIFLPersonal item in paxLegList)
                {
                    long PaxId;
                    long.TryParse(Convert.ToString(item.PaxID), out PaxId);
                    
                    FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient();
                    var lstPaxList = this.GetPaxListofGrid().Where(p => p.PassengerRequestorID == PaxId).FirstOrDefault(); 
                    DateTime? dtPaxDOB = null;
                    if (lstPaxList!=null)
                    {
                        dtPaxDOB = lstPaxList.DateOfBirth;
                    }
                    if (dtPaxDOB != null)
                        dtPaxDOB = dtPaxDOB.Value.AddYears(2);

                    if (dtPaxDOB == null || dtPaxDOB < DateTime.Now)
                    {
                        PostflightLegSIFLViewModel Sifl = new PostflightLegSIFLViewModel();
                        Sifl.State = TripEntityState.Added;

                        #region Personal PAX Loop

                        try
                        {
                            // Get SIFL Rate based on Scheduled date 
                            siflRateList = DBCatalogsDataLoader.GetPOSiflRate(item.ScheduledDate);
                            if (siflRateList != null && siflRateList.Count > 0)
                            {
                                if (siflRateList.Count > 0)
                                {
                                    SetSiflRatevaluesInViewmodel(siflRateList, Sifl);
                                    fareLevelID = siflRateList[0].FareLevelID;
                                }
                            }
                            else
                            {
                                if (HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey] != null)
                                {
                                    siflRateList = (List<GetPOSIFLRate>)HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey];
                                    if (siflRateList.Count > 0)
                                    {
                                        SetSiflRatevaluesInViewmodel(siflRateList, Sifl);
                                        fareLevelID = siflRateList[0].FareLevelID;
                                    }
                                }
                            }

                        }
                        catch (Exception ex1)
                        {
                            //Manually Handled
                            // Binded the Sifl Rate in Catch too, if any unexpected exception occured.
                            if (HttpContext.Current.Session[WebSessionKeys.POSIFLRateKey] != null)
                            {
                                siflRateList = (List<GetPOSIFLRate>)GetCurrentSiflRatesList();
                                if (siflRateList.Count > 0)
                                {
                                    SetSiflRatevaluesInViewmodel(siflRateList, Sifl);
                                    fareLevelID = siflRateList[0].FareLevelID;
                                }
                            }
                        }

                        try
                        {
                            if (item.DepartICAO > 0 && item.ArriveICAO > 0)
                            {
                                statueMiles = Convert.ToDecimal(DBCatalogsDataLoader.CalculateMiles(item.DepartICAO, item.ArriveICAO));                                
                            }
                        }
                        catch (Exception ex)
                        {
                            //Manually Handled
                            // Catch Block integrated to avoid un-expected exception
                            statueMiles = 0;
                        }

                        if (item.EmpType != null)
                        {
                            empType = item.EmpType;
                        }
                        else
                        {
                            //set default value of employee type. "" - Nothing is set
                            empType = "";
                        }

                        long? associatedPassengerID = null;
                        string associatedPassengerCD = string.Empty;

                        if (empType == WebConstants.POEmployeeType.G)
                        {
                            using (
                                PreflightService.PreflightServiceClient objService =
                                    new PreflightService.PreflightServiceClient())
                            {
                                var AssociatedValue =
                                    objService.GetAssociatePassengerID(Convert.ToInt64(item.PaxID));
                                if (AssociatedValue != null && AssociatedValue.ReturnFlag == true &&
                                    AssociatedValue.EntityList.Count > 0)
                                {
                                    associatedPassengerID = AssociatedValue.EntityList[0].PassengerRequestorID;
                                    associatedPassengerCD = AssociatedValue.EntityList[0].PassengerRequestorCD;
                                }

                                if (associatedPassengerID != null && associatedPassengerID > 0)
                                {
                                    var AssociatePassretValue =
                                        objService.GetSIFLPassengerEmployeeType(associatedPassengerID.Value);
                                    if (AssociatePassretValue != null &&
                                        AssociatePassretValue.ReturnFlag == true &&
                                        AssociatePassretValue.EntityList.Count > 0)
                                    {
                                        foreach (
                                            PreflightService.GetSIFLPassengerEmployeeType
                                                AssociatePassresultList in AssociatePassretValue.EntityList)
                                        {
                                            if (AssociatePassresultList.IsEmployeeType == null)
                                                empType = WebConstants.POEmployeeType.N;
                                            else
                                                empType = AssociatePassresultList.IsEmployeeType;
                                        }
                                    }
                                }
                                else
                                    empType = WebConstants.POEmployeeType.N;
                            }
                        }

                        
                        if (TripLog.PostflightMain.Fleet != null && TripLog.PostflightMain.Fleet.FleetID > 0)
                        {
                            if (empType == WebConstants.POEmployeeType.C)
                                multiplierTotal = TripLog.PostflightMain.Fleet.SIFLMultiControlled??0;
                            else if (empType == WebConstants.POEmployeeType.N)
                                multiplierTotal = TripLog.PostflightMain.Fleet.SIFLMultiNonControled??0;

                            //reseting EmpType to actual value - If value is G then the value gets modified to N, C
                            if (item.EmpType != null)
                            {
                                empType = item.EmpType;
                            }

                            maxPax = TripLog.PostflightMain.Fleet.MaximumPassenger??0;
                        }

                        // if PassengerRequestorID or associatedPassengerID is ISSIFL, then multiplierTotal = MultiSec...
                        using (
                            FlightPakMasterService.MasterCatalogServiceClient masterServiceClient =
                                new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var passengers =
                                masterServiceClient.GetPassengerbyPassengerRequestorID(item.PaxID).EntityList;
                            if (passengers != null && passengers.Count > 0)
                            {
                                if (passengers.First().IsSIFL == true)
                                // if passenger is SIFL secured, get MultiSec for multiplier, else check for Associated PAX
                                {
                                    multiplierTotal = TripLog.PostflightMain.Fleet.MultiSec??0;
                                }
                                else if (associatedPassengerID != null) //!string.IsNullOrEmpty(item.AssocPaxCD))
                                {
                                    var associatedPassengers =
                                        masterServiceClient.GetPassengerbyPassengerRequestorID(
                                            associatedPassengerID.Value).EntityList;
                                    if (associatedPassengers.Count > 0)
                                    {
                                        if (associatedPassengers.First().IsSIFL == true)
                                            multiplierTotal = TripLog.PostflightMain.Fleet.MultiSec??0;
                                    }
                                }
                            }
                        }

                        


                        if (item.LegID > 0)
                            Sifl.POLegID = item.LegID;
                        //temporarily storing LegNum in POSIFLID 
                        Sifl.POSIFLID = item.LegNUM;
                        if (fareLevelID > 0)
                            Sifl.FareLevelID = fareLevelID;

                        if (item.PaxID != 0)
                        {
                            Sifl.PassengerRequestorID = item.PaxID;
                            if (item.PaxCode != null)
                            {
                                PostflightLegPassengerViewModel paxObj = new PostflightLegPassengerViewModel()
                                {
                                    PassengerRequestorCD = item.PaxCode,
                                    PassengerRequestorID = item.PaxID,
                                    AssociatedWithCD = item.AssocPaxCD
                                };
                                Sifl.PassengerRequestorCD=item.PaxCode;
                                Sifl.PassengerRequestorID = item.PaxID;
                                Sifl.Passenger1 = paxObj;
                            }
                        }

                        if (Sifl.Passenger1 == null)
                            Sifl.Passenger1 = new PostflightLegPassengerViewModel();

                        if (item.FirstName != null)
                            Sifl.Passenger1.PassengerFirstName = item.FirstName;
                        else
                            Sifl.Passenger1.PassengerFirstName = string.Empty;
                        if (item.MiddleName != null)
                            Sifl.Passenger1.PassengerMiddleName = item.MiddleName;
                        else
                            Sifl.Passenger1.PassengerMiddleName = string.Empty;
                        if (item.LastName != null)
                            Sifl.Passenger1.PassengerLastName = item.LastName;
                        else
                            Sifl.Passenger1.PassengerLastName = string.Empty;
                        Sifl.EmployeeTYPE = empType;

                        // check associated passenger need conversion from code to id

                        if (associatedPassengerID != null)                        
                        {
                            Sifl.AssociatedPassengerID = associatedPassengerID.Value;
                            
                            PostflightLegPassengerViewModel assocPaxObj = new PostflightLegPassengerViewModel()
                            {
                                PassengerRequestorID = Sifl.AssociatedPassengerID??0,
                                PassengerRequestorCD = associatedPassengerCD.ToString()
                            };
                            Sifl.AssocPassenger = assocPaxObj;
                        }

                        if (item.DepartICAO != null && item.DepartICAO.ToString() != "0")
                        {
                            Sifl.DepartICAOID = item.DepartICAO;
                            AirportViewModel departObj = new AirportViewModel()
                            {
                                IcaoID = item.Depart.ToString(),
                                AirportID = item.ArriveICAO
                            };
                            Sifl.DepartAirport = departObj;
                        }

                        if (item.ArriveICAO != null && item.ArriveICAO.ToString() != "0")
                        {
                            Sifl.ArriveICAOID = item.ArriveICAO;
                            AirportViewModel arriveObj = new AirportViewModel()
                            {
                                IcaoID = item.Arrive.ToString(),
                                AirportID = item.ArriveICAO
                            };
                            Sifl.ArriveAirport = arriveObj;
                        }

                        Sifl.Distance = statueMiles;

                        if (maxPax > 0)
                        {
                            Sifl.LoadFactor = Convert.ToDecimal(item.LoadFactor); //Math.Round(((nonPvtCount / maxPax) * 100), 1);
                            // if loadfactor > 50 and emp type is conTrolled, change type to Non-Controlled / if emptype is Guest, no SIFL calculation done
                            if (Sifl.LoadFactor > 50)
                            {
                                if (empType == WebConstants.POEmployeeType.C) // Sifl.EmployeeTYPE == rbEmployeeType.SelectedValue)
                                {
                                    multiplierTotal = TripLog.PostflightMain.Fleet.SIFLMultiNonControled??0;
                                    Sifl.EmployeeTYPE = WebConstants.POEmployeeType.C;

                                }
                                else if (empType == WebConstants.POEmployeeType.G)
                                {
                                    addSiflInSaveList = false;
                                }
                            }
                        }
                        else
                        {
                            Sifl.LoadFactor = Convert.ToDecimal("0.0");
                        }
                        Sifl.Distance1 = 0;
                        Sifl.Distance2 = 0;
                        Sifl.Distance3 = 0;
                        if (statueMiles > 0)
                        {
                            if (statueMiles >= 1500)
                            {
                                Sifl.Distance1 = 500;
                                Sifl.Distance2 = 1000;
                                Sifl.Distance3 = (statueMiles - 1500);
                            }
                            else if ((statueMiles >= 501) && (statueMiles <= 1500))
                            {
                                Sifl.Distance1 = 500;
                                Sifl.Distance2 = (statueMiles - 500);
                            }
                            else if ((statueMiles > 0) && (statueMiles <= 500))
                            {
                                Sifl.Distance1 = statueMiles;
                            }

                            // Calculate Distance Rate
                            distanceRate1 = (Sifl.Rate1 ?? 0) * Convert.ToDecimal(Sifl.Distance1);
                            distanceRate2 = (Sifl.Rate2 ?? 0) * Convert.ToDecimal(Sifl.Distance2);
                            distanceRate3 = (Sifl.Rate3 ?? 0) * Convert.ToDecimal(Sifl.Distance3);

                            Sifl.AircraftMultiplier = multiplierTotal;
                            multiplierTotal = (multiplierTotal / 100);

                            // Calculate SIFL Total                                                    
                            distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;
                            siflTotal = Math.Round(((distancRateTotal * multiplierTotal) + Sifl.TeminalCharge??0), 2);
                        }

                        Sifl.Rate1 = Math.Round(Sifl.Rate1 ?? 0, 4);
                        Sifl.Rate2 = Math.Round(Sifl.Rate2 ?? 0, 4);
                        Sifl.Rate3 = Math.Round(Sifl.Rate3 ?? 0, 4);                        
                        Sifl.SIFLTotal = siflTotal;
                        Sifl.TeminalCharge = Math.Round(Sifl.TeminalCharge??0, 2);
                        Sifl.AmtTotal = Sifl.SIFLTotal;
                        foreach (var objSiflRecal in RecalcSiflList)
                        {
                            Sifl.AmtTotal += objSiflRecal.SIFLTotal;    
                        }

                        Sifl.IsDeleted = false;
                        Sifl.RowNumber = RecalcSiflList.Count + 1; // Maintain for deleting particular index
                        if (addSiflInSaveList)
                        {
                            RecalcSiflList.Add(Sifl);
                        }

                        #endregion
                    }
                }

                legIndex += 1;

                #endregion
            }

            PostflightLogViewModel TripLogtemp = (PostflightLogViewModel)TripLog;
            var passengerbyPassengerID = RecalcSiflList.GroupBy(x => x.PassengerRequestorID).ToList();
            List<PostflightLegSIFLViewModel> RecalcSiflMain = new List<PostflightLegSIFLViewModel>();
            var UserPrincipal = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
            foreach (var PFPAX in passengerbyPassengerID)
            {
                long PaxID = (long)PFPAX.Key;
                List<PostflightLegSIFLViewModel> RecalcSiflTemp = new List<PostflightLegSIFLViewModel>();

                foreach (PostflightLegViewModel leg in TripLog.PostflightLegs.Where(l => l.State != TripEntityState.Deleted && l.IsDeleted==false).ToList())
                {
                    #region Loop Each Leg
                    List<PostflightLegSIFLViewModel> RecalcSiflList_1 = new List<PostflightLegSIFLViewModel>();

                    //temporarily using POSIFLID for LegNum
                    RecalcSiflList_1 = RecalcSiflList.Where(x => x.PassengerRequestorID != null && x.PassengerRequestorID == PaxID && x.POSIFLID == leg.LegNUM).ToList();

                    if (RecalcSiflList_1.Count == 0)
                    {
                        continue;
                    }
                    else if (RecalcSiflTemp.Count == 0 && RecalcSiflList_1.Count > 0)
                    {
                        RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                    }
                    else if (RecalcSiflTemp.Count > 0 && RecalcSiflList_1.Count > 0)
                    {
                        //temporarily using POSIFLID for LegNum                                
                        long LastLegNum = RecalcSiflTemp[RecalcSiflTemp.Count - 1].POSIFLID;

                        if ((leg.LegNUM - LastLegNum) == 1)
                        {

                            double ActualLayover = CalculateDifference(TripLogtemp, Convert.ToInt16(LastLegNum));
                            double siflLayoverHrs = 10.0167;
                            if (UserPrincipal._LayoverHrsSIFL != null && UserPrincipal._LayoverHrsSIFL > 0)
                                siflLayoverHrs = Convert.ToDouble(UserPrincipal._LayoverHrsSIFL);

                            //if (ActualLayover <= Convert.ToDouble((int)UserPrincipal.Identity._fpSettings._LayoverHrsSIFL))
                            if (ActualLayover <= siflLayoverHrs) // Convert.ToDouble((int)UserPrincipal.Identity._fpSettings._LayoverHrsSIFL))
                            {
                                //RecalcSiflTemp[RecalcSiflTemp.Count - 1].AmtTotal = RecalcSiflTemp[RecalcSiflTemp.Count - 1].AmtTotal + RecalcSiflList_1[0].AmtTotal;
                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance = RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance + RecalcSiflList_1[0].Distance;
                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].ArriveAirport = (AirportViewModel)RecalcSiflTemp[RecalcSiflTemp.Count - 1].ArriveAirport.InjectFrom(RecalcSiflList_1[0].ArriveAirport);
                                //13-Mar-2014 - Fixed issue of change of arrival airport name after log save
                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].ArriveICAOID = RecalcSiflList_1[0].ArriveICAOID;
                                //temporarily using POSIFLID for LegNum
                                RecalcSiflTemp[RecalcSiflTemp.Count - 1].POSIFLID = RecalcSiflList_1[0].POSIFLID;
                                statueMiles = RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance??0;
                                miles1 = 0;
                                miles2 = 0;
                                miles3 = 0;
                                if (statueMiles > 0)
                                {
                                    if (statueMiles >= 1500)
                                    {
                                        miles1 = 500;
                                        miles2 = 1000;
                                        miles3 = (statueMiles - 1500);
                                    }
                                    else if ((statueMiles >= 501) && (statueMiles <= 1500))
                                    {
                                        miles1 = 500;
                                        miles2 = (statueMiles - 500);
                                    }
                                    else if ((statueMiles > 0) && (statueMiles <= 500))
                                    {
                                        miles1 = statueMiles;
                                    }

                                    rate1 = RecalcSiflTemp[RecalcSiflTemp.Count - 1].Rate1??0;
                                    rate2 = RecalcSiflTemp[RecalcSiflTemp.Count - 1].Rate2??0;
                                    rate3 = RecalcSiflTemp[RecalcSiflTemp.Count - 1].Rate3??0;


                                    // Calculate Distance Rate
                                    distanceRate1 = (rate1 * Convert.ToDecimal(miles1));
                                    distanceRate2 = (rate2 * Convert.ToDecimal(miles2));
                                    distanceRate3 = (rate3 * Convert.ToDecimal(miles3));
                                    terminalCharge = RecalcSiflTemp[RecalcSiflTemp.Count - 1].TeminalCharge??0;
                                    //Sifl.AircraftMultiplier = multiplierTotal;
                                    multiplierTotal = (RecalcSiflTemp[RecalcSiflTemp.Count - 1].AircraftMultiplier ?? 0) > 0 ? (RecalcSiflTemp[RecalcSiflTemp.Count - 1].AircraftMultiplier ?? 0) / 100 : 0;
                                    distancRateTotal = distanceRate1 + distanceRate2 + distanceRate3;
                                    siflTotal = Math.Round(((distancRateTotal * multiplierTotal) + terminalCharge), 2);
                                    RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance1 = miles1;
                                    RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance2 = miles2;
                                    RecalcSiflTemp[RecalcSiflTemp.Count - 1].Distance3 = miles3;
                                    RecalcSiflTemp[RecalcSiflTemp.Count - 1].AmtTotal = siflTotal;                                    
                                }
                            }
                            else
                            {
                                RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                            }
                        }
                        else
                        {
                            RecalcSiflTemp.Add(RecalcSiflList_1[0]);
                        }

                    }
                    #endregion
                }

                RecalcSiflMain.AddRange(RecalcSiflTemp);
            }

            //POSIFL used as temp legnum. Now updating to 0
            foreach (var TempSIFL in RecalcSiflMain)
            {
                TempSIFL.POSIFLID = 0;
            }

            TripLog.PostflightLegSIFLsList = RecalcSiflMain;
            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;
            foreach (var siflObj in RecalcSiflMain)
            {
                siflObj.AmtTotal = CalculateGrandTotalSifl();
            }
            ResultData.Result = RecalcSiflMain.OrderBy(o=>o.PassengerRequestorCD).ToList();
            ResultData.Success = true;
            ResultData.StatusCode = System.Net.HttpStatusCode.OK;

            HttpContext.Current.Session[WebSessionKeys.CurrentPostflightLog] = TripLog;

            return ResultData;
        }

        public double CalculateDifference(PostflightLogViewModel Trip, int LegNumber)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip, LegNumber))
            {
                double LegDifference = 0;
                var UserPrincipal = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
                List<PostflightLegViewModel> legList = new List<PostflightLegViewModel>();
                if (Trip.PostflightLegs != null)
                {
                    legList = Trip.PostflightLegs.Where(x => x.State != TripEntityState.Deleted && x.IsDeleted==false).OrderBy(x => x.LegNUM).ToList();

                    if (Trip != null && legList.Count > (LegNumber))
                    {
                        PostflightLegViewModel currentLeg = legList.Where(x => x.LegNUM == LegNumber).FirstOrDefault();
                        PostflightLegViewModel NextLeg = legList.Where(x => x.LegNUM == LegNumber + 1).FirstOrDefault();

                        DateTime InBoundValue = DateTime.MinValue;
                        DateTime OutBoundValue = DateTime.MinValue;

                        if (currentLeg != null && currentLeg.InboundDTTM != null)
                            InBoundValue = Convert.ToDateTime(currentLeg.InboundDTTM);

                        

                        if (NextLeg != null)
                        {
                            if (UserPrincipal._DutyBasis != null && UserPrincipal._DutyBasis == 2) // No Out Date Display
                            {
                                if (NextLeg.ScheduledTM != null)
                                    OutBoundValue = Convert.ToDateTime(NextLeg.ScheduledTM);
                            }
                            else
                            {
                                if (NextLeg.OutboundDTTM != null)
                                    OutBoundValue = Convert.ToDateTime(NextLeg.OutboundDTTM);
                            }
                        }

                        if (InBoundValue != DateTime.MinValue && OutBoundValue != DateTime.MinValue)
                        {
                            if (OutBoundValue > InBoundValue)
                            {
                                TimeSpan DutyDifference = OutBoundValue.Subtract(InBoundValue);
                                LegDifference = Convert.ToDouble(DutyDifference.TotalHours);
                            }
                        }
                    }
                    else
                    {
                        LegDifference = -1;
                    }
                }
                return LegDifference;
            }
        }


    #endregion
    }
}