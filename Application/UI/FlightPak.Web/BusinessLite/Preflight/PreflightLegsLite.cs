﻿using FlightPak.Web.CalculationService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.PreflightService;
using FlightPak.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FlightPak.Web.BusinessLite.Preflight
{
    public class PreflightLegsLite
    {
        string CompDateFormat = "MM/dd/yyyy";

        public List<object> GetJson(DataView dv)
        {
            var dt = dv.ToTable();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows.ToList<object>();
        }

        public DataView LoadLegDetailsfromTrip()
        {
            DataTable dtLegs = new DataTable();
            DataTable dtNewAssign = new DataTable();
            dtNewAssign.Columns.Add("LegID");
            dtNewAssign.Columns.Add("LegNUM");
            dtNewAssign.Columns.Add("DepartICAOID");
            dtNewAssign.Columns.Add("ArriveICAOID");
            dtNewAssign.Columns.Add("DepartureDTTMLocal");
            dtNewAssign.Columns.Add("ArrivalDTTMLocal");
            dtLegs = dtNewAssign.Clone();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                UserPrincipalViewModel UserPrincipal = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.getUserPrincipal();
            
                List<PreflightLegViewModel> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                for (int LegCnt = 0; LegCnt < Preflegs.Count; LegCnt++)
                {
                    if (Preflegs[LegCnt].IsDeleted == false)
                    {
                        DataRow oItemLeg = dtNewAssign.NewRow();
                        oItemLeg[0] = Preflegs[LegCnt].LegID;
                        oItemLeg[1] = Preflegs[LegCnt].LegNUM;
                        oItemLeg[2] = Preflegs[LegCnt].DepartureAirport.IcaoID == null ? string.Empty : Preflegs[LegCnt].DepartureAirport.IcaoID;
                        oItemLeg[3] = Preflegs[LegCnt].ArrivalAirport.IcaoID == null ? string.Empty : Preflegs[LegCnt].ArrivalAirport.IcaoID;
                        oItemLeg[4] = Preflegs[LegCnt].DepartureDTTMLocal == null ? string.Empty : String.Format(CultureInfo.InvariantCulture, "{0:" + (UserPrincipal._ApplicationDateFormat != null ? UserPrincipal._ApplicationDateFormat : CompDateFormat) + " HH:mm}", Preflegs[LegCnt].DepartureDTTMLocal);
                        oItemLeg[5] = Preflegs[LegCnt].ArrivalDTTMLocal == null ? string.Empty : String.Format(CultureInfo.InvariantCulture, "{0:" + (UserPrincipal._ApplicationDateFormat != null ? UserPrincipal._ApplicationDateFormat : CompDateFormat) + " HH:mm}", Preflegs[LegCnt].ArrivalDTTMLocal);
                        dtNewAssign.Rows.Add(oItemLeg);
                        dtLegs.ImportRow(oItemLeg);
                    }
                }
                DataView dv = new DataView(dtLegs);
                return dv;
            }
            return new DataView();

        }

        public void UpdateLegsDatesCalculation(PreflightTripViewModel TripVM, PreflightLegViewModel currentLegVM, UserPrincipalViewModel userPrinciple)
        {
            PreflightLegViewModel prevLegObject = null;
            prevLegObject = PreflightLegsLite.GetLiveLegs(TripVM).FirstOrDefault(l => l.LegNUM == (currentLegVM.LegNUM - 1));
            if (prevLegObject != null && prevLegObject.ArrivalDTTMLocal != null)
            {
                DateTime dtObj = (DateTime)prevLegObject.ArrivalDTTMLocal;
                double HrsToAdd = 0;
                if (prevLegObject.CheckGroup != null && prevLegObject.CheckGroup == "DOM")
                    HrsToAdd = userPrinciple._GroundTM == null ? 0 : (double)userPrinciple._GroundTM;
                else
                    HrsToAdd = userPrinciple._GroundTMIntl == null ? 0 : (double)userPrinciple._GroundTMIntl;

                dtObj = dtObj.AddHours(HrsToAdd);

                currentLegVM.DepartureDTTMLocal = dtObj;

                DateTime ldGmtDep = DateTime.MinValue;
                using (CalculationServiceClient objDstService = new CalculationServiceClient())
                {
                    ldGmtDep = objDstService.GetGMT(currentLegVM.DepartureAirport.AirportID, dtObj, true, false);
                    currentLegVM.DepartureGreenwichDTTM = ldGmtDep;
                    if (TripVM.PreflightMain.HomebaseAirportID != null)
                    {
                        currentLegVM.HomeDepartureDTTM = objDstService.GetGMT(TripVM.PreflightMain.HomebaseAirportID.Value, ldGmtDep, false, false);
                    }
                }
            }
        }

        public static List<PreflightLegViewModel> GetLiveLegs(PreflightTripViewModel TripViewModel)
        {
            if (TripViewModel != null && TripViewModel.PreflightLegs != null)
                return TripViewModel.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(p => p.LegNUM).ToList();
            else
                return new List<PreflightLegViewModel>();
        }
    }
}