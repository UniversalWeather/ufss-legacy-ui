﻿using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers.Preflight;
using FlightPak.Web.PreflightService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Views.Transactions.Preflight;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Omu.ValueInjecter;
using System.Threading.Tasks;

namespace FlightPak.Web.BusinessLite.Preflight
{
    public class PreflightPaxLite
    {
        List<GetPassenger> _getPassenger = null;
        string CompDateFormat = "MM/dd/yyyy";
        public List<PassengerViewModel> LoadPaxFromPaxSummary(int legNum)
        {
            List<PassengerViewModel> paxList = new List<PassengerViewModel>();
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            var legitem = Trip.PreflightLegs.Where(t => t.IsDeleted == false && t.LegNUM == legNum).FirstOrDefault();
            if(legitem!=null)
            {
                if (!Trip.PreflightLegPassengers.ContainsKey(legitem.LegNUM.ToString()))
                    Trip.PreflightLegPassengers[legitem.LegNUM.ToString()] = new List<PassengerViewModel>();

                return Trip.PreflightLegPassengers[legitem.LegNUM.ToString()].Where(g => g.IsDeleted == false && g.FlightPurposeID > 0).ToList();
            }            
            return new List<PassengerViewModel>();

        }

        public Hashtable GetPaxGroupIDByPaxGroupCode(string paxGroupCode)
        {
            Hashtable result = new Hashtable();
            using (FlightPakMasterService.MasterCatalogServiceClient ObjPaxService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                List<FlightPak.Web.FlightPakMasterService.PassengerGroup> PaxGroupLists = new List<FlightPak.Web.FlightPakMasterService.PassengerGroup>();
                var ObjRetValPax = ObjPaxService.GetPaxGroupList();
                PaxGroupLists = ObjRetValPax.EntityList.Where(x => x.PassengerGroupCD.ToLower().Trim().Equals(paxGroupCode.ToLower().Trim())).ToList();
                if (PaxGroupLists.Count > 0)
                {
                    result["Status"] = true;
                    result["ID"] = PaxGroupLists[0].PassengerGroupID.ToString();
                }
                else
                {
                    result["Status"] = false;                    
                }
            }
            return result;
        }

        public Hashtable CheckTSAStatus(string passengerRequestorID)
        {
            Hashtable result = new Hashtable();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (!string.IsNullOrEmpty(passengerRequestorID))
                {
                    using (PreflightServiceClient PrefService = new PreflightServiceClient())
                    {
                        dynamic _getPassenger = null;
                        var objRetVal = PrefService.GetPassengerbyIDOrCD(Convert.ToInt64(Convert.ToInt64(passengerRequestorID)), string.Empty);
                        if (objRetVal != null)
                        {
                            if (objRetVal.ReturnFlag == true)
                                _getPassenger = objRetVal.EntityList;
                            if (_getPassenger != null && _getPassenger.Count > 0)
                            {

                                var objPaxRetVal = _getPassenger[0];
                                UserPrincipalViewModel UserPrinciple = PreflightTripManager.getUserPrincipal();
                                if (objPaxRetVal != null)
                                {
                                    result["Enabled"] = false;

                                    if (objPaxRetVal.TSAStatus == "C")
                                    {
                                        if (objPaxRetVal.TSADTTM == null)
                                        {
                                            result["Status"] = "Cleared";
                                        }
                                        else
                                        {

                                            result["Status"] = "Cleared  " + String.Format(CultureInfo.InvariantCulture, "{0:" +
                                                UserPrinciple._ApplicationDateFormat + " HH:mm}", ((DateTime)objPaxRetVal.TSADTTM));

                                        }
                                        result["BackColor"] = Framework.Helpers.MiscUtils.ColorToHexConverter(System.Drawing.Color.Green);

                                    }
                                    else if (objPaxRetVal.TSAStatus == "S")
                                    {
                                        if (objPaxRetVal.TSADTTM == null)
                                        {
                                            result["Status"] = "Selectee";
                                        }
                                        else
                                        {
                                            result["Status"] = "Selectee  " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrinciple._ApplicationDateFormat + " HH:mm}", ((DateTime)objPaxRetVal.TSADTTM));

                                        }
                                        result["BackColor"] = Framework.Helpers.MiscUtils.ColorToHexConverter(System.Drawing.Color.Orange);
                                    }
                                    else if (objPaxRetVal.TSAStatus == "R")
                                    {
                                        if (objPaxRetVal.TSADTTM == null)
                                        {
                                            result["Status"] = "Restricted";
                                        }
                                        else
                                        {
                                            result["Status"] = "Restricted  " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrinciple._ApplicationDateFormat + " HH:mm}", ((DateTime)objPaxRetVal.TSADTTM));
                                        }
                                        result["BackColor"] = Framework.Helpers.MiscUtils.ColorToHexConverter(System.Drawing.Color.Red);
                                    }
                                    else
                                    {
                                        result["Status"] = "N/A";
                                        result["BackColor"] = Framework.Helpers.MiscUtils.ColorToHexConverter(System.Drawing.Color.DarkGray);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return result;
        }

        public List<PassengerViewModel> LoadPaxSelectionDataFromTrip()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                List<PassengerViewModel> NewpassengerInLegs = new List<PassengerViewModel>();
                bool alreadyExists = false;
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    if (Trip.PreflightLegs != null)
                    {
                        // Trip.PreflightLegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        for (int LegCnt = 1; LegCnt <= Preflegs.Count; LegCnt++)
                        {
                            if (Preflegs[LegCnt - 1].IsDeleted == false)
                            {
                                if (Trip.PreflightLegPassengers.ContainsKey(LegCnt.ToString()))
                                {
                                    for (int PaxCnt = 0; PaxCnt < Trip.PreflightLegPassengers[LegCnt.ToString()].Count; PaxCnt++)
                                    {
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].IsDeleted == false)
                                        {
                                            alreadyExists = NewpassengerInLegs.Any(X => X.PassengerRequestorCD == Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerRequestorCD);

                                            if (alreadyExists == false)
                                            {
                                                PassengerViewModel paxInfo = new PassengerViewModel();
                                                

                                                NewpassengerInLegs.Add(paxInfo);
                                                PaxLegInfo PaxLeg = new PaxLegInfo();
                                                PaxLeg.LegID = Convert.ToInt64(Preflegs[LegCnt - 1].LegID);
                                                Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerID = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerRequestorID;
                                                
                                                paxInfo.PassengerRequestorID = Convert.ToInt64(Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerID);
                                                paxInfo = (PassengerViewModel)paxInfo.InjectFrom(Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt]);
                                                paxInfo.PaxName = string.Format("{0}, {1} {2}", Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerLastName, Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerFirstName, Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerMiddleName);

                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                    NewpassengerInLegs = NewpassengerInLegs.OrderBy(p => p.OrderNUM).ToList();                    
                    
                }
                return NewpassengerInLegs;
            }
        }

        public List<PassengerViewModel> LoadPaxSummaryDataFromTrip()
        {
            List<PassengerViewModel> Newpassengerlist = new List<PassengerViewModel>();

            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                
                if (Trip.PreflightLegs != null)
                {
                    // Trip.PreflightLegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    for (int LegCnt = 1; LegCnt <= Preflegs.Count; LegCnt++)
                    {
                        if (Preflegs[LegCnt - 1].IsDeleted == false)
                        {
                            if (Trip.PreflightLegPassengers.ContainsKey(LegCnt.ToString()))
                            {
                                for (int PaxCnt = 0; PaxCnt < Trip.PreflightLegPassengers[LegCnt.ToString()].Count; PaxCnt++)
                                {
                                    if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].IsDeleted == false && Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeID.HasValue && Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeID > 0)
                                    {
                                        PassengerViewModel paxSum = new PassengerViewModel();
                                        Newpassengerlist.Add(paxSum);

                                        paxSum.PassengerID = Convert.ToInt64(Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerID);
                                        paxSum.OrderNUM = (Int32)Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].OrderNUM;

                                        using (PreflightServiceClient Service = new PreflightServiceClient())
                                        {
                                            if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeID == null)
                                            {
                                                var objRetVal = Service.GetPassengerbyIDOrCD(Convert.ToInt64(Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerID), string.Empty);
                                                if (objRetVal.ReturnFlag == true)
                                                    _getPassenger = objRetVal.EntityList;
                                                if (_getPassenger != null && _getPassenger.Count > 0)
                                                {
                                                    paxSum.PaxCode = _getPassenger[0].PassengerRequestorCD;
                                                    paxSum.FlightPurposeCD = _getPassenger[0].FlightPurposeCD;
                                                    paxSum.FlightPurposeID = _getPassenger[0].FlightPurposeID;
                                                }
                                            }
                                            else
                                            {
                                                paxSum.PaxCode = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerRequestorCD;
                                                paxSum.FlightPurposeCD = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeCD;
                                                paxSum.FlightPurposeID = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeID;
                                            }

                                            paxSum.PaxName = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerLastName + ", " + Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerFirstName + " " + Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerMiddleName;
                                            paxSum.LegID = Preflegs[LegCnt - 1].LegID;
                                            paxSum.LegNUM = Preflegs[LegCnt - 1].LegNUM;
                                            if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassportID != null)
                                                paxSum.PassportID = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassportID;

                                        }                                        

                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassportID != null)
                                            paxSum.PassportID = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassportID;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PaxCode != null)
                                            paxSum.PaxCode = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PaxCode;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerRequestorCD != null)
                                            paxSum.PassengerRequestorCD = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassengerRequestorCD;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeID != null)
                                            paxSum.FlightPurposeID = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeID;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeCD != null)
                                            paxSum.FlightPurposeCD = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].FlightPurposeCD;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassportNum != null)
                                            paxSum.PassportNum = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PassportNum;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].VisaID != null)
                                            paxSum.VisaID = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].VisaID;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].Street != null)
                                            paxSum.Street = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].Street;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PostalZipCD != null)
                                            paxSum.PostalZipCD = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].PostalZipCD;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].CityName != null)
                                            paxSum.CityName = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].CityName;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].StateName != null)
                                            paxSum.StateName = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].StateName;
                                        if (Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].Billing != null)
                                            paxSum.Billing = Trip.PreflightLegPassengers[LegCnt.ToString()][PaxCnt].Billing.Trim();

                                        // Passport related info.
                                        
                                        CacheLite cacheData = new CacheLite();
                                        var passport = paxSum.PassportID.HasValue ? cacheData.GetPassportFromCache(paxSum.PassportID.Value) : null;
                                        if (passport != null)
                                        {
                                            if (passport.PassportID > 0)
                                            {
                                                paxSum.PassportID = passport.PassportID;
                                                paxSum.PassportNum = passport.PassportNum;
                                                paxSum.IssueDT = passport.IssueDT;
                                                paxSum.PassportExpiryDT = passport.PassportExpiryDT;
                                                paxSum.Nation = passport.CountryCD;
                                            }
                                        }
                                        else
                                        {
                                            var PaxPassportList = GetPaxPassportData(((long)paxSum.PassengerID).ToString()).Where(g=>g.IsDeleted==false&& g.Choice==true).ToList();
                                            if (PaxPassportList != null && PaxPassportList.Count>0 )
                                            {
                                                
                                                    paxSum.PassportID = PaxPassportList[0].PassportID;
                                                    paxSum.PassportNum = PaxPassportList[0].PassportNum;
                                                    paxSum.IssueDT = PaxPassportList[0].IssueDT;
                                                    paxSum.PassportExpiryDT = PaxPassportList[0].PassportExpiryDT;
                                                    paxSum.Nation = PaxPassportList[0].CountryCD;
                                                    cacheData.AddPassportToCache(PaxPassportList[0]);
                                            }
                                            else
                                            {
                                                cacheData.AddPassportToCache(new PaxPassportViewModel() { PassengerID=(long) paxSum.PassengerID});
                                            }
                                        }
                                    }
                                }
                            }
                        } // end if - leg is deleted
                    } // end of for loop
                } // if end - session for AvailableList is not available
            }// if end
            
            return Newpassengerlist;
        }
        public Hashtable AddNewPax(List<string> passengerIdentifiers, string isAddOrInsert, int InsertAt,bool? IsPassengerIds)
        {
            Hashtable hashResult = new Hashtable();
            hashResult["PaxAdded"] = 0;
            hashResult["Message"] = "";


            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            if (Trip != null)
            {
                var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                int paxAdded = 0;
                List<string> paxCodeNotExists = new List<string>();
                List<string> paxCodeAlreadyAdded = new List<string>();
                // Under development 
                
                using (PreflightServiceClient PrefService = new PreflightServiceClient())
                {
                    var OldpassengerInLegs = GetPaxListofGrid();
                    int MaxORderNum = 0;
                    if (OldpassengerInLegs.Count > 0)
                    {
                        MaxORderNum = OldpassengerInLegs.Max(f => (int)f.OrderNUM) + 1;
                    }

                    for (int i = 0; i < passengerIdentifiers.Count; i++)
                    {
                        PassengerInLegs paxInfo = new PassengerInLegs();
                        paxInfo.Legs = new List<PaxLegInfo>();

                        GetAllPreFlightAvailablePaxList AvailableList = new GetAllPreFlightAvailablePaxList();
                        AvailableList = null;
                        using (PreflightServiceClient prefsevice = new PreflightServiceClient())
                        {
                            ReturnValueOfGetAllPreFlightAvailablePaxList objretval = null;
                            if (IsPassengerIds == true)
                            {
                                objretval = prefsevice.GetAllPreflightAvailablePaxListByIDORCD(Convert.ToInt64(passengerIdentifiers[i].ToLower().Trim()), string.Empty);
                            }
                            else
                            {
                                objretval = prefsevice.GetAllPreflightAvailablePaxListByIDORCD(0, passengerIdentifiers[i].ToLower().Trim());
                            }
                            if (objretval.ReturnFlag)
                            {
                                var _list = objretval.EntityList;
                                if (_list != null && _list.Count > 0)
                                    AvailableList = _list[0];
                            }
                            else
                            {
                                paxCodeNotExists.Add(passengerIdentifiers[i]);
                            }
                        }
                        if (AvailableList != null)
                        {

                            if (OldpassengerInLegs.Exists(p => p.PassengerRequestorID == AvailableList.PassengerRequestorID))
                            {
                                if (OldpassengerInLegs.Exists(p => p.PassengerRequestorID == AvailableList.PassengerRequestorID && p.IsDeleted == true))
                                {
                                    var paxEnable = OldpassengerInLegs.Where(p => p.PassengerRequestorID == AvailableList.PassengerRequestorID && p.IsDeleted == true).FirstOrDefault();
                                    if (paxEnable != null)
                                    {
                                        foreach (var item in Preflegs)
                                        {
                                            var paxObject = Trip.PreflightLegPassengers[item.LegNUM.ToString()].Where(r => r.PassengerRequestorCD == paxEnable.PassengerRequestorCD && r.IsDeleted == true).FirstOrDefault();

                                            paxObject.State = TripEntityState.Modified;
                                            if (isAddOrInsert == "Insert" && InsertAt != 0)
                                            {
                                                IncreasePaxid(InsertAt, Trip.PreflightLegPassengers[item.LegNUM.ToString()]);
                                                paxObject.OrderNUM = InsertAt;
                                            }
                                            else
                                            {
                                                paxObject.OrderNUM = Trip.PreflightLegPassengers[item.LegNUM.ToString()].Where(F => F.IsDeleted == false).ToList().Count + 1;
                                            }
                                            paxObject.IsDeleted = false;
                                        }
                                        paxAdded++;
                                    }
                                    continue;
                                }
                                else
                                {
                                    paxCodeAlreadyAdded.Add(AvailableList.Code);
                                    continue;
                                }
                            }
                            paxInfo = (PassengerInLegs)paxInfo.InjectFrom(AvailableList);

                            paxInfo.Status = AvailableList.TripStatus;
                            paxInfo.Purpose = AvailableList.FlightPurpose;
                            paxInfo.PassportID = AvailableList.PassportID.ToString();
                            paxInfo.VisaID = AvailableList.VisaID.ToString();
                            paxInfo.Code = AvailableList.Code;

                            //if (!string.IsNullOrEmpty(_list[lstcnt].PassportExpiryDT.ToString()))
                            if (!string.IsNullOrEmpty(Convert.ToString(AvailableList.PassportExpiryDT)))
                            {
                                DateTime dtDate = Convert.ToDateTime(AvailableList.PassportExpiryDT);
                                if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue)
                                    paxInfo.PassportExpiryDT = dtDate.ToString();
                                else
                                    paxInfo.PassportExpiryDT = string.Empty;
                            }

                            paxInfo.OrderNUM = MaxORderNum;
                            paxInfo.City = AvailableList.City;
                            paxInfo.Street = AvailableList.Street;
                            PassengerViewModel pmVM = new PassengerViewModel();
                            var objRetValvalue = PrefService.GetPassengerbyIDOrCD(Convert.ToInt64(AvailableList.PassengerRequestorID), string.Empty);
                            // Setting default flight purpose in new add pax

                            if (objRetValvalue.ReturnFlag == true)
                                _getPassenger = objRetValvalue.EntityList;

                            if (_getPassenger != null && _getPassenger.Count > 0)
                            {        
                                
                                var PassengerList = _getPassenger.Where(x => x.PassengerRequestorID == Convert.ToInt64(AvailableList.PassengerRequestorID)).FirstOrDefault();                                
                                paxInfo.FlightPurposeID = PassengerList.FlightPurposeID;
                                paxInfo.Purpose = PassengerList.FlightPurposeCD;
                                
                            }
                            
                            if (objRetValvalue.ReturnFlag == true && objRetValvalue.EntityList.Count > 0)
                            {
                                pmVM = (PassengerViewModel)pmVM.InjectFrom(objRetValvalue.EntityList[0]);
                                pmVM.CityName = objRetValvalue.EntityList[0].City;
                                pmVM.Street = string.Format("{0} {1} {2}", objRetValvalue.EntityList[0].Addr1, objRetValvalue.EntityList[0].Addr2, objRetValvalue.EntityList[0].Addr3);
                                pmVM.Billing = objRetValvalue.EntityList[0].StandardBilling;
                            }
                            foreach (var item in Preflegs)
                            {
                                var PaxNewVM = PaxVMtoPassenegrViewModalInjection(paxInfo);
                                PaxNewVM = (PassengerViewModel)PaxNewVM.InjectFrom(pmVM);
                                PaxNewVM.LegID = item.LegID;
                                if (!Trip.PreflightLegPassengers.ContainsKey(item.LegNUM.ToString()))
                                {
                                    Trip.PreflightLegPassengers.Add(item.LegNUM.ToString(), new List<PassengerViewModel>());
                                }
                                if (PaxNewVM.PreflightPassengerListID == 0)
                                    PaxNewVM.State = TripEntityState.Added;
                                else
                                    PaxNewVM.State = TripEntityState.Modified;

                                PaxNewVM.IsDeleted = false;
                                if (isAddOrInsert == "Insert" && InsertAt != 0)
                                {
                                    IncreasePaxid(InsertAt, Trip.PreflightLegPassengers[item.LegNUM.ToString()]);
                                    PaxNewVM.OrderNUM = InsertAt;
                                }
                                else
                                {
                                    PaxNewVM.OrderNUM = Trip.PreflightLegPassengers[item.LegNUM.ToString()].Where(F => F.IsDeleted == false).ToList().Count + 1;
                                }
                                Trip.PreflightLegPassengers[item.LegNUM.ToString()].Add(PaxNewVM);
                                if (PaxNewVM.FlightPurposeID > 0)
                                {
                                    item.PassengerTotal = (item.PassengerTotal ?? 0) + 1;
                                    item.ReservationAvailable = item.SeatTotal - item.PassengerTotal;

                                    if (item.State == TripEntityState.NoChange)
                                        item.State = TripEntityState.Modified;

                                }
                            }
                            paxAdded++;
                        }
                        else
                        {
                            paxCodeNotExists.Add(passengerIdentifiers[i]);
                        }
                    }

                }
                hashResult["Added"] = paxAdded;
                if (paxCodeAlreadyAdded.Count > 0)
                    hashResult["Message"] = string.Join(",", paxCodeAlreadyAdded.ToArray()) + " already selected.";

                if (paxCodeNotExists.Count > 0)
                    hashResult["Message"] = hashResult["Message"] + string.Join(",", paxCodeNotExists) + " code not exists.";

                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;

                return hashResult;

            }
            else
            {
                HttpContext.Current.Response.Redirect("/views/Transactions/Preflight/PreflightMain.aspx");
                return null;
            }

        }

        public void IncreasePaxid(int orderNUM, List<PassengerViewModel> legitem)
        {
            foreach (var itemPax in legitem)
            {
                if (itemPax.OrderNUM >= orderNUM && itemPax.IsDeleted == false)
                    itemPax.OrderNUM++;
            }
        }

        public void reserOrderNum()
        {
            List<PassengerViewModel> paxList = new List<PassengerViewModel>();
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            foreach (var legitem in Trip.PreflightLegs)
            {
                int i = 1;
                foreach (var itemPax in Trip.PreflightLegPassengers[legitem.LegNUM.ToString()].Where(F => F.IsDeleted == false).OrderBy(F => F.OrderNUM))
                {
                    if (!paxList.Exists(d => d.PassengerRequestorCD == itemPax.PassengerRequestorCD))
                    {
                        itemPax.State =itemPax.PreflightPassengerListID>0?TripEntityState.Modified:TripEntityState.Added;
                        itemPax.OrderNUM = i;
                        i++;
                    }
                }
            }
        }

        public int DeletePax(List<string> passengerRequestorCDs)
        {
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            int removedItems = 0;
            foreach (var legitem in Trip.PreflightLegs)
            {
                removedItems = 0;
                var paxlist = Trip.PreflightLegPassengers[Convert.ToString(legitem.LegNUM)].Where(t => passengerRequestorCDs.Contains(t.PassengerRequestorCD.Trim())).ToList();
                foreach (var paxitem in paxlist)
                {
                    if (paxitem.PreflightPassengerListID == 0)
                    {
                        Trip.PreflightLegPassengers[Convert.ToString(legitem.LegNUM)].Remove(paxitem);
                        if (paxitem.FlightPurposeID > 0)
                            removedItems++;
                    }
                    else
                    {
                        paxitem.IsDeleted = true;
                        paxitem.State = TripEntityState.Deleted;
                        if (paxitem.FlightPurposeID > 0)
                            removedItems++;

                        if (legitem.State == TripEntityState.NoChange)
                            legitem.State = TripEntityState.Modified;
                    }

                }
                legitem.PassengerTotal -= removedItems;
                legitem.ReservationAvailable = legitem.SeatTotal - legitem.PassengerTotal;
            }
            reserOrderNum();
            return passengerRequestorCDs.Count;
        }

        private PassengerViewModel PaxVMtoPassenegrViewModalInjection(PassengerInLegs paxInLeg)
        {
            PassengerViewModel paxVM = new PassengerViewModel();
            paxVM = (PassengerViewModel)paxVM.InjectFrom(paxInLeg);
            var name = paxInLeg.Name.Split();
            paxVM.PassengerID = paxInLeg.PassengerRequestorID;
            paxVM.Billing = paxInLeg.BillingCode;
            paxVM.CityName = paxInLeg.City;
            paxVM.PaxCode = paxInLeg.Code;
            paxVM.PassengerRequestorCD = paxInLeg.Code;
            paxVM.FirstName = name.Length > 2 ? name[1].Trim() : "";
            paxVM.LastName = name.Length > 2 ? name[0].Trim() : "";      
            paxVM.RequestorDesc = paxInLeg.Notes;
            paxVM.OrderNUM = paxInLeg.OrderNUM;
            paxVM.PassengerRequestorID = paxInLeg.PassengerRequestorID;
            if (string.IsNullOrWhiteSpace(paxInLeg.PassportID) == true)
                paxVM.PassportID = null;
            else
                paxVM.PassportID = Convert.ToInt64(paxInLeg.PassportID);

            paxVM.PassportExpiryDT = paxInLeg.PassportExpiryDT;
            paxVM.StateName = paxInLeg.State;
            paxVM.TripStatus = paxInLeg.Status;
            paxVM.Street = paxInLeg.Street;
            
            if(string.IsNullOrWhiteSpace(paxInLeg.VisaID))
            {
                paxVM.VisaID=null;
            }
            else
            {
                paxVM.VisaID=Convert.ToInt64(paxInLeg.VisaID);
            }
            paxVM.OrderNUM = paxInLeg.OrderNUM;
            paxVM.FlightPurposeCD = paxInLeg.Purpose;
            return paxVM;
        }

        private PassengerViewModel PaxVMtoPassenegrViewModalInjection(PassengerSummary paxSummary)
        {
            PassengerViewModel paxVM = new PassengerViewModel();
            paxVM = (PassengerViewModel) paxVM.InjectFrom(paxSummary);
            var name = paxSummary.PaxName.Split();
            paxVM.Billing = paxSummary.BillingCode;
            paxVM.CityName = paxSummary.City;            
            paxVM.FirstName = name.Length>2? name[1].Trim():"";
            paxVM.LastName = name.Length > 2 ? name[0].Trim() : "";
            paxVM.OrderNUM = paxSummary.OrderNum;
            paxVM.PassengerRequestorID = (long)paxSummary.PaxID;
           
            if (string.IsNullOrWhiteSpace(paxSummary.PassportID) == true)
                paxVM.PassportID = null;
            else
                paxVM.PassportID = Convert.ToInt64(paxSummary.PassportID);

            paxVM.PassportExpiryDT = paxSummary.PassportExpiryDT;
            paxVM.StateName = paxSummary.State;            
            paxVM.Street = paxSummary.Street;
            if (string.IsNullOrWhiteSpace(paxSummary.VisaID))
            {
                paxVM.VisaID = null;
            }
            else
            {
                paxVM.VisaID = Convert.ToInt64(paxSummary.VisaID);
            }
            
            
            return paxVM;
        }

        public List<PassengerViewModel> GetPaxListofGrid()
        {
            List<PassengerViewModel> paxList = new List<PassengerViewModel>();
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            foreach (var legitem in Trip.PreflightLegs.Where(t=>t.IsDeleted==false))
            {
                if (!Trip.PreflightLegPassengers.ContainsKey(legitem.LegNUM.ToString()))
                    Trip.PreflightLegPassengers[legitem.LegNUM.ToString()] = new List<PassengerViewModel>();

                foreach (var itemPax in Trip.PreflightLegPassengers[legitem.LegNUM.ToString()])
                {
                    if (!paxList.Exists(d => d.PassengerRequestorCD == itemPax.PassengerRequestorCD))
                    {
                        paxList.Add(itemPax);
                    }
                }
            }
            return paxList;
        }

        internal void SaveGridtoSession(dynamic Obj)
        {
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            
            if (Trip != null)
            {                
                foreach (var leg in Trip.PreflightLegs.Where(l=>l.IsDeleted==false).ToList())
                {
                    if (Trip.PreflightLegPassengers.ContainsKey(leg.LegNUM.ToString()))
                    {
                        var paxList = Trip.PreflightLegPassengers[leg.LegNUM.ToString()];                        
                        foreach (var item in Obj)
	                    {
                            string code = Convert.ToString(item.PassengerRequestorCD);
                            var passenger= paxList.Where(p => p.PaxCode == code || p.PassengerRequestorCD == code).FirstOrDefault();

                            if (passenger == null)
                            {
                                var objPaxNewVM = GetPaxListofGrid().Where(p => p.PassengerRequestorCD == code).FirstOrDefault();
                                if (objPaxNewVM != null && objPaxNewVM.IsDeleted==false)
                                {
                                    passenger = (PassengerViewModel)new PassengerViewModel().InjectFrom(objPaxNewVM);
                                    passenger.State = TripEntityState.Added;
                                    passenger.PreflightPassengerListID = 0;
                                    paxList.Add(passenger);
                                }
                            }

                            if (passenger != null)
                            {
                                    passenger.FlightPurposeID = item["Leg" + leg.LegNUM];
                                    if (passenger.PreflightPassengerListID > 0 && leg.LegID > 0)
                                        passenger.State = TripEntityState.Modified;
                                    else
                                        passenger.State = TripEntityState.Added;


                            }		                         
	                    }
                        Trip.PreflightLegPassengers[leg.LegNUM.ToString()] = paxList;
                        if (leg.State == TripEntityState.NoChange)
                            leg.State = TripEntityState.Modified;

                        leg.PassengerTotal = Trip.PreflightLegPassengers[leg.LegNUM.ToString()].Where(f => f.FlightPurposeID > 0 && f.IsDeleted == false).Count() + leg.BlockedSeatCount;
                        leg.ReservationAvailable = leg.SeatTotal - leg.PassengerTotal;

                    }
                }
                

                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }

        }

        internal void SaveGridtoSessionAfterPaxWarning(dynamic Obj)
        {
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

            if (Trip != null)
            {
                foreach (var leg in PreflightLegsLite.GetLiveLegs(Trip))
                {
                    if (Trip.PreflightLegPassengers.ContainsKey(leg.LegNUM.ToString()))
                    {
                        var paxList = Trip.PreflightLegPassengers[leg.LegNUM.ToString()];
                        foreach (var item in Obj)
                        {
                            string code = Convert.ToString(item.PassengerRequestorCD);
                            var passenger = paxList.Where(p => p.PaxCode == code || p.PassengerRequestorCD == code).FirstOrDefault();

                            if (passenger == null)
                            {
                                var objPaxNewVM = GetPaxListofGrid().Where(p => p.PassengerRequestorCD == code).FirstOrDefault();
                                if (objPaxNewVM != null && objPaxNewVM.IsDeleted == false)
                                {
                                    passenger = (PassengerViewModel)new PassengerViewModel().InjectFrom(objPaxNewVM);
                                    passenger.State = TripEntityState.Added;
                                    passenger.PreflightPassengerListID = 0;
                                    paxList.Add(passenger);
                                }
                            }

                            if (passenger != null)
                            {
                                passenger.FlightPurposeID = item["Leg" + leg.LegNUM];
                                if (passenger.PreflightPassengerListID > 0 && leg.LegID > 0)
                                    passenger.State = TripEntityState.Modified;
                                else
                                    passenger.State = TripEntityState.Added;

                                bool allFlightpurposeAreUnassigned = true;
                                var LegsList = PreflightLegsLite.GetLiveLegs(Trip);
                                foreach (var LegObject in  LegsList)
                                {
                                    long flightvalue= item["Leg" + LegObject.LegNUM];
                                    if (flightvalue > 0)
                                    {
                                        allFlightpurposeAreUnassigned = false;
                                        break;
                                    }
                                }
                                LegsList=null;

                                if (allFlightpurposeAreUnassigned)
                                {
                                    if (passenger.PreflightPassengerListID > 0)
                                    {
                                        passenger.State = TripEntityState.Deleted;
                                    }
                                    else
                                    {
                                        paxList.RemoveAt(paxList.FindIndex(p => p.PaxCode == code || p.PassengerRequestorCD == code));
                                    }
                                }
                            }
                        }
                        Trip.PreflightLegPassengers[leg.LegNUM.ToString()] = paxList;
                        if (leg.State == TripEntityState.NoChange)
                            leg.State = TripEntityState.Modified;

                        leg.PassengerTotal = Trip.PreflightLegPassengers[leg.LegNUM.ToString()].Where(f => f.FlightPurposeID > 0 && f.IsDeleted == false).Count();
                        leg.ReservationAvailable = leg.SeatTotal - leg.PassengerTotal;

                    }
                }


                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }

        }

        internal void SaveLegPAXFlightPurpose(string legNum, string PaxCode, string FlightPurposeID)
        {
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            
            if (Trip != null)
            {
                if (Trip.PreflightLegPassengers.ContainsKey(legNum))
                {
                    var CurrentLeg= Trip.PreflightLegs.Where(l => l.IsDeleted == false && l.LegNUM == Convert.ToInt32(legNum)).FirstOrDefault();

                    var paxList = Trip.PreflightLegPassengers[legNum];
                    var passenger = paxList.Where(p => p.PaxCode == PaxCode || p.PassengerRequestorCD == PaxCode).FirstOrDefault();
                    if (passenger == null)
                    {
                        var objPaxNewVM = GetPaxListofGrid().Where(p => p.PassengerRequestorCD == PaxCode).FirstOrDefault();
                        if (objPaxNewVM != null)
                        {
                            passenger = (PassengerViewModel)new PassengerViewModel().InjectFrom(objPaxNewVM);
                            passenger.State = TripEntityState.Added;
                            passenger.PreflightPassengerListID = 0;
                            paxList.Add(passenger);                            
                        }
                    }
                    if (passenger != null && !string.IsNullOrWhiteSpace(FlightPurposeID))
                    {
                        passenger.FlightPurposeID = long.Parse(FlightPurposeID);

                            if (passenger.PreflightPassengerListID > 0)
                            {
                                if (passenger.FlightPurposeID == 0)
                                {
                                    passenger.FlightPurposeID = null;
                                    passenger.State = TripEntityState.Deleted;
                                }
                                else
                                {
                                    passenger.State = TripEntityState.Modified;
                                }
                            }
                            else
                            {
                                passenger.State = TripEntityState.Added;
                            }

                        if (CurrentLeg != null)
                        {

                            if (CurrentLeg.State == TripEntityState.NoChange)
                                CurrentLeg.State = TripEntityState.Modified;
                        }
                    }
                    if (CurrentLeg != null)
                    {
                        Trip.PreflightLegPassengers[legNum] = paxList;
                        CurrentLeg.PassengerTotal = Trip.PreflightLegPassengers[legNum].Where(f => f.FlightPurposeID > 0 && f.IsDeleted == false).Count() + CurrentLeg.BlockedSeatCount;
                        CurrentLeg.ReservationAvailable = CurrentLeg.SeatTotal - CurrentLeg.PassengerTotal;
                    }
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }

        }

        internal void SavePaxInfoBlockedSeatsChange(string legNum, string PaxBooked, int BlockedSeatCount)
        {
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];


            if (Trip != null)
            {
                var currentLeg = Trip.PreflightLegs.Where(l => l.LegNUM == Convert.ToInt32(legNum) && l.IsDeleted == false).FirstOrDefault();
                if (currentLeg!=null && Trip.PreflightLegPassengers.ContainsKey(legNum))
                    if (currentLeg != null && Trip.PreflightLegPassengers.ContainsKey(legNum))
                {
                    currentLeg.BlockedSeatCount = BlockedSeatCount;
                    currentLeg.PassengerTotal = Convert.ToInt32(PaxBooked);
                    currentLeg.ReservationAvailable = Trip.PreflightLegs[Convert.ToInt32(legNum) - 1].SeatTotal - Trip.PreflightLegs[Convert.ToInt32(legNum) - 1].PassengerTotal??0;                    
                }
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
            }
        }

        public List<PaxPassportViewModel> GetPaxPassportData(string PassengerID)
        {
            UserPrincipalViewModel UserPrincipal = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.getUserPrincipal();

            HttpContext.Current.Session[WebSessionKeys.SelectedPaxRosterID] = PassengerID;
            //CrewPassportVisaViewModel crewPassportVisaViewModel = new CrewPassportVisaViewModel();
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
            {                
                List<GetAllCrewPassport> PassengerPassportList = new List<GetAllCrewPassport>();
                FlightPakMasterService.CrewPassengerPassport PassengerPassport = new FlightPakMasterService.CrewPassengerPassport();
                PassengerPassport.CrewID = null;
                PassengerPassport.PassengerRequestorID = Convert.ToInt64(PassengerID);
                var objPassengerPassport = Service.GetCrewPassportListInfo(PassengerPassport);

                List<PaxPassportViewModel> PaxPassportData = new List<PaxPassportViewModel>();

                if (objPassengerPassport.ReturnFlag == true)
                {
                    PaxPassportData = objPassengerPassport.EntityList.Select(x => new PaxPassportViewModel
                    {
                        Choice = x.Choice,
                        CountryCD = x.CountryCD,
                        CountryID = x.CountryID,
                        CountryName = x.CountryName,
                        PassengerID = x.PassengerRequestorID,
                        CustomerID = x.CustomerID,
                        IsDefaultPassport = x.IsDefaultPassport,
                        IsDeleted = x.IsDeleted,
                        IsInActive = x.IsInActive,
                        IssueCity = x.IssueCity,
                        IssueDT = String.Format(CultureInfo.InvariantCulture, "{0:" + (UserPrincipal._ApplicationDateFormat != null ? UserPrincipal._ApplicationDateFormat : CompDateFormat) + "}", x.IssueDT),
                        LastUpdTS = x.LastUpdTS,
                        LastUpdUID = x.LastUpdUID,
                        PassengerRequestorID = x.PassengerRequestorID,
                        PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + (UserPrincipal._ApplicationDateFormat != null ? UserPrincipal._ApplicationDateFormat : CompDateFormat) + "}", x.PassportExpiryDT),
                        PassportID = x.PassportID,
                        PassportNum = x.PassportNum,
                        PilotLicenseNum = x.PilotLicenseNum
                    }).ToList();

                }
                return PaxPassportData;
            }
        }

        public List<PaxVisaViewModel> GetPaxVisaData(string PassengerID)
        {
            UserPrincipalViewModel UserPrincipal = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.getUserPrincipal();
            HttpContext.Current.Session[WebSessionKeys.SelectedPaxRosterID] = PassengerID;
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
            {
                CrewPassengerVisa PaxVisa = new CrewPassengerVisa();
                PaxVisa.PassengerRequestorID = Convert.ToInt64(PassengerID);
                PaxVisa.CrewID = null;
                var CrewPaxVisaList = Service.GetCrewVisaListInfo(PaxVisa);

                List<PaxVisaViewModel> PaxVisaData = new List<PaxVisaViewModel>();

                if (CrewPaxVisaList.ReturnFlag == true)
                {
                    PaxVisaData = CrewPaxVisaList.EntityList.Select(x => new PaxVisaViewModel()
                    {
                        CountryCD = x.CountryCD,
                        CountryID = x.CountryID,
                        CountryName = x.CountryName,
                        PassengerID = x.PassengerRequestorID,
                        CustomerID = x.CustomerID,
                        EntriesAllowedInPassport = x.EntriesAllowedInPassport,
                        ExpiryDT = x.ExpiryDT,
                        IsDeleted = x.IsDeleted,
                        IsInActive = x.IsInActive,
                        IssueDate = String.Format(CultureInfo.InvariantCulture, "{0:" + (UserPrincipal._ApplicationDateFormat != null ? UserPrincipal._ApplicationDateFormat : CompDateFormat) + "}", x.IssueDate),
                        IssuePlace = x.IssuePlace,
                        LastUpdTS = x.LastUpdTS,
                        LastUpdUID = x.LastUpdUID,
                        Notes = x.Notes,
                        PassengerRequestorID = x.PassengerRequestorID,
                        TypeOfVisa = x.TypeOfVisa,
                        VisaExpireInDays = x.VisaExpireInDays,
                        VisaID = x.VisaID,
                        VisaNum = x.VisaNum,
                        VisaTYPE = x.VisaTYPE
                    }).ToList();
                }
                return PaxVisaData;
            }

        }


        public void UpdatePaxHotelDates(double dateDifference)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip.PreflightMain.EditMode == true)
                {
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        List<PreflightLegViewModel> flightLeg = Trip.PreflightLegs.Where(p => p.IsDeleted == false).OrderBy(p => p.LegNUM).ToList();                        
                        foreach (PreflightLegViewModel Leg in flightLeg)
                        {
                            if (Trip.PreflightLegPassengersLogisticsHotelList.ContainsKey(Leg.LegNUM.ToString()))
                            {
                                foreach (var paxHotel in Trip.PreflightLegPassengersLogisticsHotelList[Leg.LegNUM.ToString()])
                                {
                                    if (Leg.ArrivalDTTMLocal.HasValue)
                                    {
                                        paxHotel.DateIn = new DateTime(Leg.ArrivalDTTMLocal.Value.Ticks, DateTimeKind.Local);
                                    }
                                    var flightObj=flightLeg.Where(t => t.LegNUM == (Leg.LegNUM + 1)).FirstOrDefault();
                                    if ( flightObj!= null && flightObj.DepartureDTTMLocal.HasValue)
                                    {
                                        paxHotel.DateOut = new DateTime(flightObj.DepartureDTTMLocal.Value.Ticks, DateTimeKind.Local);
                                    }
                                    else if (paxHotel.DateOut.HasValue)
                                    {
                                        paxHotel.DateOut=paxHotel.DateOut.Value.AddDays(dateDifference);
                                        paxHotel.DateOut = new DateTime(paxHotel.DateOut.Value.Ticks, DateTimeKind.Local);
                                    }
                                    else
                                    {
                                        paxHotel.DateOut = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal DateTime? ValidateDateInForHotel(PreflightLegViewModel currentLegVM,PreflightLegPassengerHotelViewModel paxHotelVM)
        {
            if (paxHotelVM.DateIn.HasValue)
                return new DateTime(paxHotelVM.DateIn.Value.Ticks, DateTimeKind.Local);

            if (currentLegVM != null && currentLegVM.ArrivalDTTMLocal.HasValue)
            {
                return new DateTime(currentLegVM.ArrivalDTTMLocal.Value.Ticks, DateTimeKind.Local);
            }
            return null;
        }

        internal DateTime? ValidateDateOutForHotel(PreflightLegViewModel nextLegVM, PreflightLegPassengerHotelViewModel paxHotelVM)
        {
            if (paxHotelVM.DateOut.HasValue)
                return new DateTime(paxHotelVM.DateOut.Value.Ticks, DateTimeKind.Local);

            if (nextLegVM!= null && nextLegVM.DepartureDTTMLocal.HasValue)
            {
                return new DateTime(nextLegVM.DepartureDTTMLocal.Value.Ticks, DateTimeKind.Local);
            }
            return null;
        }
        
    }
}