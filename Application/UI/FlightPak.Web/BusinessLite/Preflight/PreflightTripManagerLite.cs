﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.ViewModels;
using FlightPak.Web.FlightPakMasterService;
using Omu.ValueInjecter;
using System.Data;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Views.Transactions.Preflight;

namespace FlightPak.Web.BusinessLite.Preflight
{
    public class PreflightTripManagerLite
    {
        public PreflightTripViewModel AddNewTrip(UserPrincipalViewModel UserPrincipal)
        {
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            pfViewModel.PreflightMain = new PreflightMainViewModel();
            pfViewModel.PreflightMain.TripStatus = string.IsNullOrEmpty(UserPrincipal._TripMGRDefaultStatus) ? "T" : UserPrincipal._TripMGRDefaultStatus;
            if (UserPrincipal._isDispatcher)
            {
                pfViewModel.PreflightMain.DispatcherUserName = UserPrincipal.UserName;
            }
            if (UserPrincipal._IsDefaultTripDepartureDate)
            {
                pfViewModel.PreflightMain.EstDepartureDT = DateTime.Now.Date;
            }
            pfViewModel.PreflightMain.RequestDT = DateTime.Now.Date;

            AirportViewModel objHomeBaseAirport = getAirportByIcaoId(UserPrincipal._homeBase);
            if (objHomeBaseAirport != null )
            {
                if (UserPrincipal._IsDefaultTripDepartureDate)
                {
                    pfViewModel.PreflightMain.EstDepartureDT =new DateTime(DateTime.UtcNow.AddHours((double) objHomeBaseAirport.OffsetToGMT).Ticks,DateTimeKind.Local);
                }
                pfViewModel.PreflightMain.RequestDT = new DateTime(DateTime.UtcNow.AddHours((double)objHomeBaseAirport.OffsetToGMT).Ticks, DateTimeKind.Local);
                if (objHomeBaseAirport.IsDayLightSaving.HasValue && objHomeBaseAirport.IsDayLightSaving.Value)
                {
                    if (pfViewModel.PreflightMain.EstDepartureDT != null)
                        pfViewModel.PreflightMain.EstDepartureDT = pfViewModel.PreflightMain.EstDepartureDT.Value.AddHours(1);
                    pfViewModel.PreflightMain.RequestDT = pfViewModel.PreflightMain.RequestDT.Value.AddHours(1);
                }
            }

            pfViewModel.PreflightMain.EditMode = true;
            pfViewModel.PreflightMain.TripID = 0;
            pfViewModel = SetADefaultHomeBase(pfViewModel, UserPrincipal._homeBaseId);
            pfViewModel.PreflightMain.Company.BaseDescription = UserPrincipal._BaseDescription;
            pfViewModel = SetADefaultFleet(pfViewModel,UserPrincipal._homeBaseId);


            pfViewModel.PreflightLegCrewLogisticsHotelList = new Dictionary<string, List<PreflightLegCrewHotelViewModel>>();
            pfViewModel.PreflightLegCrewTransportationList = new Dictionary<string, PreflightLegCrewTransportationViewModel>();
            pfViewModel.PreflightLegPassengersLogisticsHotelList = new Dictionary<string, List<PreflightLegPassengerHotelViewModel>>();
            pfViewModel.PreflightLegPaxTransportationList = new Dictionary<string, PreflightLegPassengerTransportViewModel>();
            pfViewModel.PreflightLegPreflightLogisticsList = new Dictionary<string, PreflightLogisticsViewModel>();            

            return pfViewModel;
        }

        public PreflightTripViewModel SetADefaultHomeBase(PreflightTripViewModel pfViewModel, long? homebaseId)
        {
            if (homebaseId!=null)
            {
                CompanyViewModel _homebase = new CompanyViewModel();
                _homebase.HomebaseID = (long)homebaseId;
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetCompanyWithFilters(string.Empty, homebaseId.Value, true, true);
                    List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();
                    if (objRetVal.ReturnFlag)
                    {
                        CompanyList = objRetVal.EntityList;
                        if (CompanyList != null && CompanyList.Count > 0)
                        {
                            _homebase.HomebaseAirportID = CompanyList[0].HomebaseAirportID;
                            _homebase.BaseDescription = CompanyList[0].BaseDescription;
                            _homebase.IsEnableTSA = CompanyList[0].IsEnableTSA;
                            pfViewModel.PreflightMain.HomebaseID = homebaseId;
                            pfViewModel.PreflightMain.HomeBaseAirportICAOID = CompanyList[0].HomebaseCD;
                            pfViewModel.PreflightMain.HomebaseAirportID = _homebase.HomebaseAirportID;
                            pfViewModel.PreflightMain.Company = _homebase;
                            //pfViewModel.PreflightMain.OldHomebaseID = homebaseId;
                        }
                    }
                }
            }
            return pfViewModel;
        }
        private PreflightTripViewModel SetADefaultFleet(PreflightTripViewModel pfViewModel, long? homebaseId)
        {
            FleetViewModel Fleet = new FleetViewModel();
            List<FlightPakMasterService.GetFleet> GetFleet = new List<FlightPakMasterService.GetFleet>();
            if (homebaseId != null)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var fleetretval = objDstsvc.GetFleet();
                    if (fleetretval.ReturnFlag)
                    {
                        GetFleet = fleetretval.EntityList.Where(x => x.HomebaseID == homebaseId.Value && x.IsInActive == false).ToList(); ;
                        if (GetFleet != null && GetFleet.Count == 1)
                        {
                            Fleet.FleetID = GetFleet[0].FleetID;
                            Fleet.TailNum = GetFleet[0].TailNum;
                            Fleet.MaximumPassenger = GetFleet[0].MaximumPassenger;
                            pfViewModel.PreflightMain.Fleet = Fleet;
                            pfViewModel.PreflightMain.FleetID = GetFleet[0].FleetID;

                            if (GetFleet[0].AircraftID != null)
                            {
                                pfViewModel.PreflightMain.Aircraft = SetADefaultAricraft(GetFleet[0].AircraftID.Value);
                                pfViewModel.PreflightMain.AircraftID = GetFleet[0].AircraftID;
                            } else
                            {
                                pfViewModel.PreflightMain.Aircraft = new AircraftViewModel();
                                pfViewModel.PreflightMain.AircraftID = 0;
                            }
                            if (GetFleet[0].EmergencyContactID != null)
                            {
                                pfViewModel.PreflightMain.EmergencyContact = SetADefaultEmergencyContact(GetFleet[0].EmergencyContactID.Value);
                                pfViewModel.PreflightMain.EmergencyContactID = GetFleet[0].EmergencyContactID;
                            } else
                            {
                                pfViewModel.PreflightMain.EmergencyContact = new EmergencyContactViewModel();
                                pfViewModel.PreflightMain.EmergencyContactID = null;
                            }
                        }
                    }
                }
            }
            return pfViewModel;
        }
        private AircraftViewModel SetADefaultAricraft(long AircraftID)
        {
            AircraftViewModel vmAircarft = new AircraftViewModel();
            FlightPak.Web.FlightPakMasterService.Aircraft Aircraft = new FlightPakMasterService.Aircraft();
            if (AircraftID != null) {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRet = objDstsvc.GetAircraftByAircraftID(AircraftID);
                    if (objRet.ReturnFlag && objRet.EntityList !=null && objRet.EntityList.Count>0) {
                        Aircraft = objRet.EntityList[0];
                        if (Aircraft != null) {
                            vmAircarft.AircraftCD = Aircraft.AircraftCD;
                            vmAircarft.AircraftID = Aircraft.AircraftID;
                            vmAircarft.AircraftDescription = Aircraft.AircraftDescription;
                        }
                    }
                }
            }
            return vmAircarft;
        }
        private EmergencyContactViewModel SetADefaultEmergencyContact(long EmergencyContactID)
        {
            EmergencyContactViewModel vmEmergContact = new EmergencyContactViewModel();
            if (EmergencyContactID != null) {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                { 
                     List<FlightPak.Web.FlightPakMasterService.EmergencyContact> EmergencyContactList = new List<FlightPakMasterService.EmergencyContact>();
                    var objRetValEmerg = objDstsvc.GetEmergencyList();
                    if (objRetValEmerg.ReturnFlag)
                    {
                        EmergencyContactList = objRetValEmerg.EntityList.Where(x => x.EmergencyContactID == (long)EmergencyContactID).ToList();
                        vmEmergContact.EmergencyContactID = EmergencyContactList[0].EmergencyContactID;
                        vmEmergContact.EmergencyContactCD = EmergencyContactList[0].EmergencyContactCD;
                        vmEmergContact.FirstName = EmergencyContactList[0].FirstName;
                    }
                }
            }
            return vmEmergContact;
        }
        public AirportViewModel getAirportByIcaoId(string IcaoID)
        {
            AirportViewModel airport = new AirportViewModel();
            if (String.IsNullOrEmpty(IcaoID))
                return airport;
            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
            using(FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAirportByAirportICaoID(IcaoID.ToUpper().Trim());
                if (objRetVal != null && objRetVal.ReturnFlag)
                {
                    AirportLists = objRetVal.EntityList;
                    if(AirportLists != null && AirportLists.Count > 0)
                    {
                        airport = (AirportViewModel) new AirportViewModel().InjectFrom(AirportLists.FirstOrDefault());
                    }
                }
            }
            return airport;
        }

        public bool SaveTrip(PreflightTripViewModel vmTrip)
        {
            return true;
        }

        public PreflightLegViewModel SetDefaultPreflightLegChildObjects(PreflightLegViewModel preflightLegViewmodel)
        {
            if (preflightLegViewmodel != null)
            {
                if (preflightLegViewmodel.Account == null)
                {
                    preflightLegViewmodel.Account = preflightLegViewmodel.AccountID.HasValue ? DBCatalogsDataLoader.GetAccountInfo_FromAccountId(preflightLegViewmodel.AccountID.Value): new AccountViewModel();
                }

                if (preflightLegViewmodel.Client == null)
                {
                    preflightLegViewmodel.Client = preflightLegViewmodel.ClientID.HasValue ? DBCatalogsDataLoader.GetClientInfo_FromClientID(preflightLegViewmodel.ClientID.Value):  new ClientViewModel();
                }

                if (preflightLegViewmodel.CQCustomer == null)
                {
                    preflightLegViewmodel.CQCustomer = new CQCustomerViewModel();
                }

                if (preflightLegViewmodel.CrewDutyRule == null)
                {
                    preflightLegViewmodel.CrewDutyRule = preflightLegViewmodel.CrewDutyRulesID.HasValue ? DBCatalogsDataLoader.GetCrewDutyRulesInfo_FromFilter(preflightLegViewmodel.CrewDutyRulesID.Value,""): new CrewDutyRuleViewModel();
                }

                if (preflightLegViewmodel.ArrivalAirport == null)
                {
                    preflightLegViewmodel.ArrivalAirport = preflightLegViewmodel.ArriveICAOID.HasValue? DBCatalogsDataLoader.GetAirportInfo_FromAirportId(preflightLegViewmodel.ArriveICAOID.Value): new AirportViewModel();
                }

                if (preflightLegViewmodel.DepartureAirport == null)
                {
                    preflightLegViewmodel.DepartureAirport = preflightLegViewmodel.DepartICAOID.HasValue ? DBCatalogsDataLoader.GetAirportInfo_FromAirportId(preflightLegViewmodel.DepartICAOID.Value) : new AirportViewModel();
                }

                if (preflightLegViewmodel.Department == null)
                {
                    preflightLegViewmodel.Department = preflightLegViewmodel.DepartmentID.HasValue ? DBCatalogsDataLoader.GetDepartmentByIDorCD(preflightLegViewmodel.DepartmentID.Value,"") : new DepartmentViewModel();

                }

                if (preflightLegViewmodel.DepartmentAuthorization == null)
                {
                    preflightLegViewmodel.DepartmentAuthorization = (preflightLegViewmodel.AuthorizationID.HasValue && preflightLegViewmodel.DepartmentID.HasValue) ? DBCatalogsDataLoader.GetDepartmentAuthorizationByDepIDnAuthorizationCDorID("",preflightLegViewmodel.AuthorizationID.Value,preflightLegViewmodel.DepartmentID.Value) : new DepartmentAuthorizationViewModel();
                }

                if (preflightLegViewmodel.FlightCatagory == null)
                {
                    preflightLegViewmodel.FlightCatagory = preflightLegViewmodel.FlightCategoryID.HasValue ? DBCatalogsDataLoader.GetFlightCategoryInfo_FromId(preflightLegViewmodel.FlightCategoryID.Value) : new FlightCatagoryViewModel();
                }

                if (preflightLegViewmodel.Passenger == null)
                {
                    preflightLegViewmodel.Passenger = preflightLegViewmodel.PassengerRequestorID.HasValue ? (PassengerViewModel)new PassengerViewModel().InjectFrom(DBCatalogsDataLoader.GetPassengerReqestorInfo_FromPassengerReqID(preflightLegViewmodel.PassengerRequestorID.Value)) : new PassengerViewModel();
                }

            }
            return preflightLegViewmodel;
        }

        public List<RoomTypeViewModal> RetrieveRoomType()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    var objRetVal = objDstsvc.GetRoomList();
                    var roomTypeVMList=new List<RoomTypeViewModal>();
                    if (objRetVal.ReturnFlag == true)
                    {
                        roomTypeVMList=objRetVal.EntityList.Select(t => new RoomTypeViewModal() { RoomDescription = t.RoomDescription, RoomTypeID = t.RoomTypeID }).ToList();
                    }
                    return roomTypeVMList;
                }
            }
        }

        public static void UpdatePreflightTripViewModelAsPerCalenderSelection(PreflightTripViewModel pfViewModel=null)
        {
            if (pfViewModel == null)
            {
                pfViewModel = new PreflightTripViewModel();
                pfViewModel.PreflightMain = new PreflightMainViewModel();
            }
            string urlReferer ="";
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                urlReferer = Convert.ToString(HttpContext.Current.Request.UrlReferrer);
            }            
            if (HttpContext.Current.Session["CurrentPreFlightTrip"] != null && urlReferer.ToUpper().Contains("/PREFLIGHT/SCHEDULECALENDAR/"))
            {
                FlightPak.Web.PreflightService.PreflightMain Trip = (FlightPak.Web.PreflightService.PreflightMain)HttpContext.Current.Session["CurrentPreFlightTrip"];
                pfViewModel = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.InjectPreflightMainToPreflightTripViewModel(Trip, pfViewModel);
                HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = pfViewModel;
                HttpContext.Current.Session.Remove("CurrentPreFlightTrip");
                if (Trip.TripID > 0)
                {
                    HttpContext.Current.Session[WebSessionKeys.UnchangedCurrentTrip] = Trip;
                }
                else
                {
                    pfViewModel.PreflightMain.RequestDT = DateTime.Now;
                    pfViewModel.PreflightMain.TripStatus = "T";

                }

            }            
        }

        public static DateTime GetUserHomebaseLocalDateTime(DateTime dt)
        {
            UserPrincipalViewModel UserPrincipal = PreflightTripManager.getUserPrincipal(); 
            DateTime appDt = dt;
            appDt = appDt.AddHours(DateTime.UtcNow.Hour).AddMinutes(DateTime.UtcNow.Minute);
            DateTime dtLocal = new DateTime(appDt.Ticks, DateTimeKind.Local);
            List<FlightPak.Web.FlightPakMasterService.GetAllAirport> AirportLists = new List<FlightPak.Web.FlightPakMasterService.GetAllAirport>();
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAirportByAirportICaoID(UserPrincipal._homeBase.ToUpper().Trim());
                if (objRetVal != null && objRetVal.ReturnFlag)
                {
                    AirportLists = objRetVal.EntityList;
                    if (AirportLists != null && AirportLists.Count > 0)
                    {
                        dtLocal = new DateTime(appDt.AddHours((double)AirportLists[0].OffsetToGMT).Ticks, DateTimeKind.Local);
                    }
                }
            }

            return dtLocal;
        }

        #region TripManagerCheckListGroup

        public List<object> GetJson(DataView dv)
        {
            var dt = dv.ToTable();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows.ToList<object>();
        }

        public DataView LoadAllTripManagerCheckListGroup(bool isInActive)
        {
            DataTable dtChecklist = new DataTable();
            DataTable dtNewAssign = new DataTable();
            dtNewAssign.Columns.Add("CheckGroupCD");
            dtNewAssign.Columns.Add("CheckGroupDescription");
            dtNewAssign.Columns.Add("CheckGroupID");
            dtChecklist = dtNewAssign.Clone();

            PeflightTripManagerCheckListGroupViewModel pfTripManagerViewModel = new PeflightTripManagerCheckListGroupViewModel();
            using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
            {

                pfTripManagerViewModel.tripManagerCheckListGroup = SearchAndFilter(isInActive);
            }

            for (int crewChecklist = 0; crewChecklist < pfTripManagerViewModel.tripManagerCheckListGroup.Count; crewChecklist++)
            {
                DataRow oItemChecklist = dtNewAssign.NewRow();
                oItemChecklist[0] = pfTripManagerViewModel.tripManagerCheckListGroup[crewChecklist].CheckGroupCD;
                oItemChecklist[1] = pfTripManagerViewModel.tripManagerCheckListGroup[crewChecklist].CheckGroupDescription;
                oItemChecklist[2] = pfTripManagerViewModel.tripManagerCheckListGroup[crewChecklist].CheckGroupID;

                dtNewAssign.Rows.Add(oItemChecklist);
                dtChecklist.ImportRow(oItemChecklist);

            }
            DataView dv = new DataView(dtChecklist);
            return dv;
        }

        private static List<TripManagerCheckListGroupViewModel> SearchAndFilter(bool IsInActive)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (FlightPakMasterService.MasterCatalogServiceClient ObjService = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    long CheckGroupID = 0;
                    string CheckGroupCD = string.Empty;

                    List<TripManagerCheckListGroupViewModel> tripManagerCheckListGroupList = new List<TripManagerCheckListGroupViewModel>();
                    var ReturnValue = ObjService.GetAllTripmanagerChecklistGroupWithFilters(CheckGroupID, CheckGroupCD, IsInActive);
                    if (ReturnValue.ReturnFlag)
                    {
                        tripManagerCheckListGroupList = ReturnValue.EntityList.Select(x => new TripManagerCheckListGroupViewModel
                        {
                            CheckGroupCD = x.CheckGroupCD,
                            CheckGroupDescription = x.CheckGroupDescription,
                            CheckGroupID = x.CheckGroupID,
                            IsDeleted = x.IsDeleted,
                            IsInActive = x.IsInActive
                        }).ToList();
                    }
                    return tripManagerCheckListGroupList;
                }
            }
        }
        #endregion
    }
}