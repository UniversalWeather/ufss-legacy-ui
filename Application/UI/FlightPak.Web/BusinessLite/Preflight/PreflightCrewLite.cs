﻿using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.PreflightService;
using FlightPak.Web.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FlightPak.Web.BusinessLite.Preflight
{
    public class PreflightCrewLite
    {
        string CompDateFormat;
        UserPrincipalViewModel upViewModel;
        public PreflightCrewLite()
        {
            upViewModel = FlightPak.Web.Views.Transactions.Preflight.PreflightTripManager.getUserPrincipal();
            CompDateFormat = upViewModel._ApplicationDateFormat != null ? upViewModel._ApplicationDateFormat : "MM/dd/yyyy";
        }
        public DataView BindAvailableCrew(DateTime DepartDate, DateTime ArriveDate, Int64 CrewGroupID, long HomeBaseID, long FleetID, long ArrivalAirportID, long CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, long AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                {
                    DataRow[] drarray;
                    var objRetVal = objService.GetAvailableCrewList(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID);
                    if (objRetVal.ReturnFlag == true)
                    {
                        DataTable table = ConvertToDataTable(objRetVal.EntityList);
                        DeleteDuplicateFromDataTable(table, "CrewID");
                        drarray = table.Select("IsStatus = 'True'");
                        if (drarray != null)
                        {
                            DataTable dtNew = table.Clone();
                            //populate new destination table
                            foreach (DataRow dr in drarray)
                                dtNew.ImportRow(dr);

                            DataView dv = new DataView(dtNew);
                            dv.Sort = "Standby DESC ,TailAssigned DESC ,Actype DESC ,Conflicts ASC,ChkList ASC, CrewCD ASC";

                            return dv;
                        }
                        else
                        {
                            DataView dv = new DataView(table);
                            dv.Sort = "Standby DESC ,TailAssigned DESC ,Actype DESC ,Conflicts ASC,ChkList ASC, CrewCD ASC";
                            return dv;
                        }
                    }
                    return null;
                }
            }
        }

        /// <summary>
        /// RetrieveCrewList
        /// </summary>
        /// <returns></returns>
        private DataTable RetrieveCrewList(string hdnLeg, string hdnCrewGroupID, string airport)
        {
            Int64 CurrentTripID = 0;
            Int64 crewGroupID = 0;
            Int64 AircraftID = 0;
            Int64 FleetID = 0;
            Int64 ArriveICAOID = 0;
            DateTime ddate = new DateTime();
            DateTime adate = new DateTime();
            DateTime maxArrivedate = new DateTime();
            DateTime minDepartdate = new DateTime();
            ddate = DateTime.Now;
            adate = DateTime.Now;
            maxArrivedate = DateTime.Now;
            minDepartdate = DateTime.Now;
            DataTable table = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];

                    if (Trip.PreflightMain.Fleet != null)
                    {
                        FleetID = Convert.ToInt64(Trip.PreflightMain.FleetID);
                    }
                    if (Trip.PreflightMain.AircraftID != null)
                    {
                        AircraftID = Convert.ToInt64(Trip.PreflightMain.AircraftID);
                    }
                    if (Trip.PreflightLegs == null || Trip.PreflightLegs.Count == 0)
                    {
                        CurrentTripID = Trip.PreflightMain.TripID;
                        ArriveICAOID = 0;                   
                        CurrentTripID = Trip.PreflightMain.TripID;
                    }
                    else
                    {
                        int LegtotCnt = Trip.PreflightLegs.Count;
                        CurrentTripID = Trip.PreflightMain.TripID;
                        ArriveICAOID = Convert.ToInt64(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].ArriveICAOID);

                        if (Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].DepartureGreenwichDTTM != null)
                             DateTime.TryParse(Trip.PreflightLegs[Convert.ToInt16(hdnLeg) - 1].DepartureGreenwichDTTM.ToString(), out ddate);
                        if (Trip.PreflightLegs[LegtotCnt - 1].ArrivalGreenwichDTTM != null)
                            DateTime.TryParse(Trip.PreflightLegs[LegtotCnt - 1].ArrivalGreenwichDTTM.ToString(), out adate);
                       
                        var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        for (int i = 0; i < Preflegs.Count; i++)
                        {
                            if (Preflegs[i].ArrivalGreenwichDTTM != null)
                            {
                                DateTime.TryParse(Trip.PreflightLegs.Max(x => x.ArrivalGreenwichDTTM).ToString(), out maxArrivedate);
                            }
                         
                            if (Preflegs[i].DepartureGreenwichDTTM != null)
                            {
                                DateTime.TryParse(Trip.PreflightLegs.Min(x => x.DepartureGreenwichDTTM).ToString(), out minDepartdate);
                            }
                        }

                        CurrentTripID = Trip.PreflightMain.TripID;
                    }

                    if (!string.IsNullOrEmpty(hdnCrewGroupID))
                        crewGroupID = Convert.ToInt64(hdnCrewGroupID);


                    using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                    {
                        var objRetVal = objService.GetAvailableCrewList(ddate,adate, crewGroupID, Convert.ToInt64(airport), FleetID, ArriveICAOID, CurrentTripID, minDepartdate, maxArrivedate, AircraftID);
                        if (objRetVal.ReturnFlag == true)
                        {
                            table = ConvertToDataTable(objRetVal.EntityList);
                        }
                        DeleteDuplicateFromDataTable(table, "CrewID");
                    }
                }
            }
            return table;
        }
      
        /// <summary>
        /// ConvertToDataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable<T>(IList<T> list) where T : class
        {
            int count = 1;
            try
            {
                DataTable table = CreateDataTable<T>();
                Type objType = typeof(T);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (T item in list)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor property in properties)
                    {
                        if (!CanUseType(property.PropertyType)) continue;
                        row[property.Name] = property.GetValue(item) ?? DBNull.Value;
                    }
                    row["ID"] = count;
                    table.Rows.Add(row);
                    count = count + 1;
                }

                return table;
            }
            catch (DataException ex)
            {
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// CreateDataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static DataTable CreateDataTable<T>() where T : class
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Type objType = typeof(T);
                DataTable table = new DataTable(objType.Name);
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(objType);
                foreach (PropertyDescriptor property in properties)
                {
                    Type propertyType = property.PropertyType;
                    if (!CanUseType(propertyType)) continue;


                    //nullables must use underlying types
                    if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        propertyType = Nullable.GetUnderlyingType(propertyType);
                    //enums also need special treatment
                    if (propertyType.IsEnum)
                        propertyType = Enum.GetUnderlyingType(propertyType);
                    table.Columns.Add(property.Name, propertyType);
                }
                table.Columns.Add("ID");
                return table;
            }
        }


        /// <summary>
        /// CanUseType
        /// </summary>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        private static bool CanUseType(Type propertyType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //only strings and value types
                if (propertyType.IsArray) return false;
                if (!propertyType.IsValueType && propertyType != typeof(string)) return false;
                return true;
            }
        }

        /// <summary>
        /// Check the duplicate records in the Datatable
        /// </summary>
        /// <param name="dtDuplicate"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        protected DataTable DeleteDuplicateFromDataTable(DataTable dtDuplicate, string columnName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Hashtable hashT = new Hashtable();
                ArrayList arrDuplicate = new ArrayList();
                foreach (DataRow row in dtDuplicate.Rows)
                {
                    if (hashT.Contains(row[columnName]))
                        arrDuplicate.Add(row);
                    else
                        hashT.Add(row[columnName], string.Empty);
                }
                foreach (DataRow row in arrDuplicate)
                    dtDuplicate.Rows.Remove(row);

                return dtDuplicate;
            }
        }
        public List<object> GetJson(DataView dv)
        {
            var dt = dv.ToTable();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows.ToList<object>();
        }
        public DataView Filter_CheckedChanged(bool chkCrewAvailabilityActive, bool chkPICSICOnly, bool chkCrewInactive, bool chkTypeRatedOnly, bool chkNoConflictsOnly, bool chkHomeBaseOnly, string tbCrewGroup, bool hdnIsCrewAutoAvailability, string hdnLeg, string airportId, string hdnCrewGroupID, string homeBase)
        {
            DataTable dtFilter = new DataTable();
            DataRow[] drarray = null;

            string filterExp = string.Empty;
            string sortExp = string.Empty;
            dtFilter = null;



            if (string.IsNullOrEmpty(tbCrewGroup))
            {
                dtFilter = null;
            }
            if (dtFilter == null || !string.IsNullOrEmpty(tbCrewGroup))
            {
                dtFilter = RetrieveCrewList(hdnLeg, hdnCrewGroupID, airportId);
            }

            if (dtFilter != null)
            {
                if (chkCrewAvailabilityActive)
                {
                    if (!chkCrewInactive)
                    {
                        filterExp += "IsStatus = 'True' AND ";
                    }
                    else
                    {
                        filterExp += "IsStatus = 'False' AND ";
                    }
                }
                else if (chkCrewInactive)
                {
                    filterExp += "IsStatus = 'False' AND ";
                }
                if (chkHomeBaseOnly)
                {
                    filterExp += "Home = " + "'" + homeBase + "' AND ";
                }

                if (chkPICSICOnly)
                {
                    filterExp += "CrewRating <> '' AND ";
                }

                if (chkNoConflictsOnly)
                {
                    filterExp += "Conflicts = 0 AND ";
                }

                if (chkTypeRatedOnly)
                {
                    filterExp += "Actype <> '' AND ";
                }

                if (!string.IsNullOrEmpty(filterExp))
                {
                    filterExp = filterExp.Remove(filterExp.Length - 5);
                    drarray = dtFilter.Select(filterExp);
                }

                if (drarray != null)
                {
                    //copy the schema of source table
                    DataTable dtNew = dtFilter.Clone();

                    if (drarray.Count() != 0)
                    {
                        //populate new destination table
                        foreach (DataRow dr in drarray)
                            dtNew.ImportRow(dr);

                        DataView dv = new DataView(dtNew);
                        dv.Sort = "Standby DESC ,TailAssigned DESC ,Actype DESC ,Conflicts ASC,ChkList ASC, CrewCD ASC";
                        return dv;
                    }
                }
                else
                {
                    DataView dv = new DataView(dtFilter);
                    dv.Sort = "Standby DESC ,TailAssigned DESC ,Actype DESC ,Conflicts ASC,ChkList ASC, CrewCD ASC";
                    return dv;
                }
            }


            if (dtFilter != null)
                dtFilter.Dispose();

            return new DataView();

        }

        public DataView LoadCrewfromTrip(bool IsCrewSelection)
        {
            bool alreadyExists = false;
            int CrwCnt = 0;
            string AddlNotes = string.Empty;
            string MainNotes = string.Empty;
            DataTable dtNewAssign = CreateCrewTableSchema();
            DataTable dtCrewInfo = new DataTable();

            var IDcount = 0;// rowCount;
            dtCrewInfo = dtNewAssign.Clone();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
                {
                    var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                    var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    
                      CacheLite cacheData = new CacheLite();
                      var objCountryVal = cacheData.GetAllCountryMasterList();

                        for (int LegCnt = 0; LegCnt < Preflegs.Count; LegCnt++)
                        {
                            if (Preflegs[LegCnt].IsDeleted == false)
                            {
                                if (Trip.PreflightLegCrews.Keys.Contains(Preflegs[LegCnt].LegNUM.ToString()))
                                {
                                    List<PreflightLegCrewViewModel> Crews = Trip.PreflightLegCrews[Preflegs[LegCnt].LegNUM.ToString()].Where(w => w.IsDeleted == false && w.State != TripEntityState.Deleted).OrderBy(x => x.OrderNUM).ToList();

                                    for (CrwCnt = 0; CrwCnt < Crews.Count; CrwCnt++)
                                    {
                                        
                                           DataRow oItemSelectedCrew = dtNewAssign.NewRow();
                                           oItemSelectedCrew[0] = Convert.ToInt64(Crews[CrwCnt].CrewID);
                                            var objRetVal = cacheData.GetCrewDetailByCrewID(Convert.ToInt64(Crews[CrwCnt].CrewID));
                                                if (objRetVal.ReturnFlag == true)
                                                {
                                                    if (objRetVal.EntityList.Count > 0)
                                                    {
                                                        oItemSelectedCrew[1] = objRetVal.EntityList[0].CrewCD;//crew.CrewCD;
                                                        oItemSelectedCrew[2] = objRetVal.EntityList[0].LastName + ", " + objRetVal.EntityList[0].FirstName + " " + objRetVal.EntityList[0].MiddleInitial;
                                                        oItemSelectedCrew[3] = Preflegs[LegCnt].LegNUM;
                                                        AddlNotes = objRetVal.EntityList[0].Notes2;
                                                        MainNotes = objRetVal.EntityList[0].Notes;
                                                    }
                                                }
                                                oItemSelectedCrew[4] = Crews[CrwCnt].DutyTYPE;
                                                if (Crews[CrwCnt].DutyTYPE == "0")
                                                {
                                                    oItemSelectedCrew[21] = false;
                                                }
                                                else
                                                {
                                                    oItemSelectedCrew[21] = true;
                                                }
                                                oItemSelectedCrew[5] = Crews[CrwCnt].Street;
                                                oItemSelectedCrew[6] = Crews[CrwCnt].PostalZipCD;
                                                oItemSelectedCrew[7] = Crews[CrwCnt].CityName;
                                                oItemSelectedCrew[8] = Crews[CrwCnt].StateName;
                                                FlightPak.Web.FlightPakMasterService.CrewPassengerPassport crewPassport = new FlightPak.Web.FlightPakMasterService.CrewPassengerPassport();
                                                crewPassport.PassengerRequestorID = null;
                                                crewPassport.CrewID = Convert.ToInt64(Crews[CrwCnt].CrewID);
                                                var objRetValPass = cacheData.GetCrewPassportFromCache(crewPassport);
                                                if (objRetValPass.ReturnFlag == true)
                                                {
                                                    foreach (FlightPakMasterService.GetAllCrewPassport crewPass in objRetValPass.EntityList)
                                                    {
                                                        if (crewPass.PassportID == Crews[CrwCnt].PassportID || (crewPass.Choice == true && Crews[CrwCnt].PassportID == null))
                                                        {
                                                            Crews[CrwCnt].PassportNum = crewPass.PassportNum;
                                                            if (crewPass.PassportExpiryDT != null)
                                                                oItemSelectedCrew[16] = String.Format(CultureInfo.InvariantCulture, "{0:" + (CompDateFormat) + "}", crewPass.PassportExpiryDT);
                                                           
                                                            if (crewPass.IssueDT != null)
                                                                oItemSelectedCrew[22] = String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}",crewPass.IssueDT);

                                                            if (crewPass.Choice.HasValue)
                                                            {
                                                                oItemSelectedCrew[20] = crewPass.Choice.Value;
                                                            }
                                                            else
                                                            {
                                                                oItemSelectedCrew[20] = false;
                                                            }
                                                            if (objCountryVal.ReturnFlag == true)
                                                            {
                                                                var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == crewPass.CountryID).ToList();
                                                                if (objCountryVal1.Count > 0)
                                                                {
                                                                    oItemSelectedCrew[17] = objCountryVal1[0].CountryCD;
                                                                }
                                                            }

                                                        }
                                                    }   
                                                }
                                                if (!string.IsNullOrEmpty(Crews[CrwCnt].PassportNum))
                                                    oItemSelectedCrew[9] = Crews[CrwCnt].PassportNum;
                                                oItemSelectedCrew[10] = IDcount;
                                                oItemSelectedCrew[11] = AddlNotes;
                                                if (Crews[CrwCnt].PassportID != null)
                                                    oItemSelectedCrew[12] = Crews[CrwCnt].PassportID;
                                                if (Crews[CrwCnt].VisaID != null)
                                                    oItemSelectedCrew[13] = Crews[CrwCnt].VisaID;
                                                oItemSelectedCrew[14] = MainNotes;
                                                oItemSelectedCrew[15] = Crews[CrwCnt].IsDiscount;
                                                oItemSelectedCrew[18] = Crews[CrwCnt].IsNotified;
                                                oItemSelectedCrew[19] = Crews[CrwCnt].OrderNUM;
                                                oItemSelectedCrew[23] = Crews[CrwCnt].State;

                                                dtNewAssign.Rows.Add(oItemSelectedCrew);

                                                IDcount = IDcount + 1;

                                                //Filter for dgcrewinfo 
                                                alreadyExists = dtCrewInfo.AsEnumerable().Any(x => x["CrewID"].ToString().Equals(Crews[CrwCnt].CrewID.ToString()));

                                                if (alreadyExists == false)
                                                {
                                                    dtCrewInfo.ImportRow(oItemSelectedCrew);
                                                }
                                        
                                    }
                                }
                            }
                        }

                        DataTable uniqueCols = dtNewAssign.DefaultView.ToTable(true, "OrderNUM");

                        if (uniqueCols.Rows.Count <= 1)
                        {
                            Int32 OrderNumCalculated = 0;
                            foreach (DataRow dr in dtCrewInfo.Rows)
                            {
                                OrderNumCalculated = OrderNumCalculated + 1;
                                dr["OrderNum"] = OrderNumCalculated;

                                foreach (DataRow drNewAssigned in dtNewAssign.Rows)
                                {
                                    if (dr["CrewID"].ToString() == drNewAssigned["CrewID"].ToString())
                                        drNewAssigned["OrderNum"] = dr["OrderNum"];
                                }
                            }
                        }

                        dtNewAssign.DefaultView.Sort = "OrderNUM ASC"; //, CrewCD ASC,Leg ASC"; // Fix for #8195 
                        dtNewAssign = dtNewAssign.DefaultView.Table;
                        dtNewAssign.AcceptChanges();
                        DataTable dtCloned = dtNewAssign.Clone();
                        dtCloned.Columns[3].DataType = typeof(Int32);
                        foreach (DataRow row in dtNewAssign.Rows)
                        {
                            if (row["Purpose"].ToString()!="0")
                            dtCloned.ImportRow(row);
                        }
                        dtCloned.DefaultView.Sort = "OrderNUM ASC"; //, CrewCD ASC,Leg ASC"; // Fix for #8195 
                        dtCloned = dtCloned.DefaultView.Table;
                        dtCloned.AcceptChanges();
                        if (IsCrewSelection)
                        {
                            dtCrewInfo.DefaultView.Sort = "OrderNUM ASC"; //, CrewCD ASC,Leg ASC"; // Fix for #8195 

                            DataView dview = dtCrewInfo.DefaultView;
                            dview.Sort = "OrderNUM ASC";
                            DataTable sortedDT = dview.ToTable();

                            Int32 OrderNumCalculated = 0;
                            foreach (DataRow dr in sortedDT.Rows)
                            {
                                OrderNumCalculated++;
                                dr["OrderNum"] = OrderNumCalculated;

                            }
                            DataView dv = new DataView(sortedDT);
                            dv.Sort = "OrderNUM ASC"; //, CrewCD ASC,Leg ASC"; // Fix for #8195 
                            return dv;
                        }
                        else
                        {
                            DataView dv = new DataView(dtCloned);
                            dv.Sort = "OrderNUM ASC";
                            return dv;
                        }
                }
                return new DataView();
            }
        }

        private DataTable CreateCrewTableSchema()
        {
            DataTable dtCrewSchema= new DataTable();
                dtCrewSchema.Columns.Add("CrewID", System.Type.GetType("System.Int64"));
                dtCrewSchema.Columns.Add("CrewCD");
                dtCrewSchema.Columns.Add("CrewName");
                dtCrewSchema.Columns.Add("Leg");
                dtCrewSchema.Columns.Add("Purpose");
                dtCrewSchema.Columns.Add("Street");
                dtCrewSchema.Columns.Add("Postal");
                dtCrewSchema.Columns.Add("City");
                dtCrewSchema.Columns.Add("State");
                dtCrewSchema.Columns.Add("Passport");
                dtCrewSchema.Columns.Add("ID");
                dtCrewSchema.Columns.Add("AddlNotes");
                dtCrewSchema.Columns.Add("PassportID", System.Type.GetType("System.Int64"));
                dtCrewSchema.Columns.Add("VisaID", System.Type.GetType("System.Int64"));
                dtCrewSchema.Columns.Add("Notes");
                dtCrewSchema.Columns.Add("IsDiscontinue");
                dtCrewSchema.Columns.Add("PassportExpiryDT");
                dtCrewSchema.Columns.Add("Nation");
                dtCrewSchema.Columns.Add("IsNotified");
                dtCrewSchema.Columns.Add("OrderNUM", System.Type.GetType("System.Int32"));
                dtCrewSchema.Columns.Add("Choice1");
                dtCrewSchema.Columns.Add("Display");
                dtCrewSchema.Columns.Add("IssueDT");
                dtCrewSchema.Columns.Add("tempStatus");
            return dtCrewSchema;
        }

        bool IsCrewAlreadyExistOnLeg(List<PreflightLegCrewViewModel> modelObject,string  targetField)
        {
            long crewid=0;
            long.TryParse(targetField,out crewid );
            PreflightLegCrewViewModel vModel=null;
            try
            {
                 vModel = modelObject.Count > 0 ? modelObject.Where(c => c.CrewID == crewid).First() : null;
            }
            catch (Exception ex) { }

            if (vModel != null)
            {
                return true;
            }
            else
                return false;
        }

        public void SaveGridtoSession(dynamic obj)
        {
            var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
            Dictionary<string, string> crewWithoutDutyList =new  Dictionary<string, string>();
            if (Trip != null)
            {
                if (Trip.PreflightLegs != null)
                {
                        List<PreflightLegCrewViewModel> existingCrewListOnLeg;
                        CacheLite cacheObjec= new CacheLite();
                        foreach (var gridDataitem in obj)
                        {
                            List<string> tempDutyType = new List<string>();
                            var objCountryVal = new FlightPakMasterService.ReturnValueOfCountry();
                            
                            long crewid;
                            long.TryParse(Convert.ToString(gridDataitem["CrewID"].Value), out crewid);
                            
                            if (gridDataitem["tempStatus"] == null)
                                gridDataitem["tempStatus"] = "added";

                            var objRetVal = crewid != null && gridDataitem["tempStatus"].Value.ToLower() == "added" ? cacheObjec.GetCrewDetailByCrewID(crewid) : null;

                            var Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                            foreach (var Leg in Preflegs)
                            {
                                existingCrewListOnLeg = Trip.PreflightLegCrews.ContainsKey(Leg.LegNUM.ToString())  ? Trip.PreflightLegCrews[Leg.LegNUM.ToString()] : new List<PreflightLegCrewViewModel>();
                                PreflightLegCrewViewModel legCrewViewModelObject = new PreflightLegCrewViewModel();
                                bool alreadyExists = IsCrewAlreadyExistOnLeg(existingCrewListOnLeg, Convert.ToString(gridDataitem["CrewID"].Value));

                                tempDutyType.Add(Convert.ToString(gridDataitem["Leg" + Leg.LegNUM.ToString()]));
                               
                                if (alreadyExists)
                                    legCrewViewModelObject = existingCrewListOnLeg.Where(c => c.CrewID == crewid).First();

                                legCrewViewModelObject.OrderNUM = gridDataitem["OrderNUM"].Value != null ? (int)gridDataitem["OrderNUM"].Value : 0;
                                legCrewViewModelObject.CrewCode = gridDataitem["CrewCD"].Value;
                                legCrewViewModelObject.LegNum = Leg.LegNUM.ToString();
                                string dutyType = gridDataitem["Leg" + Leg.LegNUM.ToString()];
                                if(dutyType.Length>1)
                                    dutyType = dutyType.Substring(dutyType.Length-1);
                                legCrewViewModelObject.DutyTYPE = dutyType.ToString()!=null?dutyType:"0";
                                legCrewViewModelObject.IsNotified = gridDataitem["IsNotified"].Value != null ? Convert.ToBoolean(gridDataitem["IsNotified"].Value) : false;
                                legCrewViewModelObject.PassportID = (!string.IsNullOrEmpty(Convert.ToString(gridDataitem["PassportID"].Value))) ? long.Parse(Convert.ToString(gridDataitem["PassportID"].Value)) : null;
                                legCrewViewModelObject.VisaID = (!string.IsNullOrEmpty(Convert.ToString(gridDataitem["VisaID"].Value))) ? long.Parse(Convert.ToString(gridDataitem["VisaID"].Value)) : null;
                                legCrewViewModelObject.CrewID = crewid;
                                legCrewViewModelObject.CrewCode = gridDataitem["CrewCD"].Value.ToString();
                                legCrewViewModelObject.CrewFirstName = gridDataitem["CrewName"].Value;

                                bool CrewdisContinue = legCrewViewModelObject.DutyTYPE != null && legCrewViewModelObject.DutyTYPE == "D";

                                //if (legCrewViewModelObject.DutyTYPE == "0" && (crewWithoutDutyList.Keys.Contains(crewid.ToString())) != true && Leg.LegNUM==1)
                                //    crewWithoutDutyList.Add(crewid.ToString(), "true");
                                //else if (legCrewViewModelObject.DutyTYPE != "0" && crewWithoutDutyList.Keys.Contains(crewid.ToString()))
                                //    crewWithoutDutyList.Remove(crewid.ToString());

                                if (gridDataitem["tempStatus"].Value.ToLower() == "added")
                                {
                                    if (objRetVal != null)
                                    {
                                        if (objRetVal.ReturnFlag && objRetVal.EntityList.Count > 0)
                                        {
                                            legCrewViewModelObject.CrewFirstName = objRetVal.EntityList[0].FirstName != null ? objRetVal.EntityList[0].FirstName : "";
                                            legCrewViewModelObject.CrewLastName = objRetVal.EntityList[0].LastName != null ? objRetVal.EntityList[0].LastName : "";
                                            legCrewViewModelObject.CrewMiddleName = objRetVal.EntityList[0].MiddleInitial != null ? objRetVal.EntityList[0].MiddleInitial : "";

                                            legCrewViewModelObject.Street = objRetVal.EntityList[0].Addr1 != null ? objRetVal.EntityList[0].Addr1 : "";
                                            legCrewViewModelObject.CityName = objRetVal.EntityList[0].CityName != null ? objRetVal.EntityList[0].CityName : "";
                                            legCrewViewModelObject.PostalZipCD = objRetVal.EntityList[0].PostalZipCD != null ? objRetVal.EntityList[0].PostalZipCD : "";
                                            legCrewViewModelObject.StateName = objRetVal.EntityList[0].StateName != null ? objRetVal.EntityList[0].StateName : "";
                                        }
                                    }

                                }
                                else
                                {
                                    legCrewViewModelObject.Street = gridDataitem["Street"] != null ? gridDataitem["Street"].Value : "";
                                    legCrewViewModelObject.CityName = gridDataitem["City"] != null ? gridDataitem["City"].Value : "";
                                    legCrewViewModelObject.PostalZipCD = gridDataitem["Postal"] != null ? gridDataitem["Postal"].Value : "";
                                    legCrewViewModelObject.StateName = gridDataitem["State"] != null ? gridDataitem["State"].Value : "";
                                }
                                legCrewViewModelObject.PassportNum = gridDataitem["Passport"] != null ? gridDataitem["Passport"].Value : "";
                                legCrewViewModelObject.PassportExpiryDT = gridDataitem["PassportExpiryDT"] != null && gridDataitem["PassportExpiryDT"].HasValues ? DateTime.Parse(gridDataitem["PassportExpiryDT"].Value.ToString()) : null;
                                legCrewViewModelObject.Nation = gridDataitem["Nation"] != null ? gridDataitem["Nation"].Value : "";

                                if (!alreadyExists)
                                {
                                    #region NewlyAdded


                                    if (!string.IsNullOrEmpty(legCrewViewModelObject.PassportExpiryDT))
                                    {
                                        DateTime dtDate = Convert.ToDateTime(legCrewViewModelObject.PassportExpiryDT);
                                        if (dtDate != DateTime.MinValue && dtDate != DateTime.MaxValue && (dtDate.ToString() != "01/01/1900 AM 12:00:00" || dtDate.ToString() == "01/01/1900 00:00:00" || dtDate.ToString() != "01/01/1900"))
                                            legCrewViewModelObject.PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", dtDate);
                                        else
                                            legCrewViewModelObject.PassportExpiryDT = string.Empty;
                                    }
                                    else
                                    {
                                        legCrewViewModelObject.PassportExpiryDT = string.Empty;
                                    }
                                    legCrewViewModelObject.IsDisplay = legCrewViewModelObject.DutyTYPE != null ? true : false;

                                    if (!string.IsNullOrEmpty(legCrewViewModelObject.Nation))
                                    {
                                        FlightPak.Web.FlightPakMasterService.CrewPassengerPassport crewPassport = new FlightPak.Web.FlightPakMasterService.CrewPassengerPassport();
                                        crewPassport.PassengerRequestorID = null;
                                        crewPassport.CrewID = Convert.ToInt64(legCrewViewModelObject.CrewID);
                                        var objRetValPass = cacheObjec.GetCrewPassportFromCache(crewPassport);
                                        if (objRetValPass.ReturnFlag == true)
                                        {
                                            foreach (FlightPakMasterService.GetAllCrewPassport crewPass in objRetValPass.EntityList)
                                            {
                                                {
                                                    legCrewViewModelObject.PassportNum = crewPass.PassportNum;
                                                    legCrewViewModelObject.PassportExpiryDT = crewPass.PassportExpiryDT != null ? String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", crewPass.PassportExpiryDT) : string.Empty;
                                                    legCrewViewModelObject.PassportIssueDT = crewPass.IssueDT != null ? String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", crewPass.IssueDT) : string.Empty;
                                                    legCrewViewModelObject.IsChoice = crewPass.Choice.HasValue ? crewPass.Choice.Value : false;

                                                    if (objCountryVal == null || objCountryVal.ReturnFlag == false)
                                                    {
                                                        objCountryVal = cacheObjec.GetAllCountryMasterList();
                                                    }
                                                    if (objCountryVal.ReturnFlag == true)
                                                    {
                                                        var objCountryVal1 = objCountryVal.EntityList.Where(x => x.CountryID == crewPass.CountryID).ToList();
                                                        if (objCountryVal1.Count > 0)
                                                        {
                                                            legCrewViewModelObject.Nation = objCountryVal1[0].CountryCD != null ? objCountryVal1[0].CountryCD : "";
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }


                                    legCrewViewModelObject.State = TripEntityState.Added;
                                    if (!Trip.PreflightLegCrews.ContainsKey(Leg.LegNUM.ToString()) || Trip.PreflightLegCrews[Leg.LegNUM.ToString()] == null)
                                        Trip.PreflightLegCrews[Leg.LegNUM.ToString()] = new List<PreflightLegCrewViewModel>();
                                   
                                        legCrewViewModelObject.IsDeleted = false;
                                        Trip.PreflightLegCrews[Leg.LegNUM.ToString()].Add(legCrewViewModelObject);
                                   
                                    #endregion
                                }
                                else if ((legCrewViewModelObject.DutyTYPE != "0" || legCrewViewModelObject.IsNotified != null) && alreadyExists)
                                {
                                    legCrewViewModelObject.IsDeleted = false;

                                    if (gridDataitem["IsDiscontinue_" + "Leg" + Leg.LegNUM.ToString()] != null && (Convert.ToString(gridDataitem["IsDiscontinue_" + "Leg" + Leg.LegNUM.ToString()]).ToLower() == "true" || Convert.ToString(gridDataitem["IsDiscontinue_" + "Leg" + Leg.LegNUM.ToString()]).ToLower() == "false"))
                                        legCrewViewModelObject.IsDiscount = Convert.ToBoolean(gridDataitem["IsDiscontinue_" + "Leg" + Leg.LegNUM.ToString()]);
                                    else
                                        legCrewViewModelObject.IsDiscount = false;
                                    
                                    if (legCrewViewModelObject.PreflightCrewListID > 0 && legCrewViewModelObject.IsDeleted ==false)
                                        legCrewViewModelObject.State = TripEntityState.Modified;
                                    else if(legCrewViewModelObject.State != TripEntityState.Deleted)
                                        legCrewViewModelObject.State = TripEntityState.Added;
                                    

                                    Trip.PreflightLegCrews[Leg.LegNUM.ToString()] = existingCrewListOnLeg;
                                }
                                else if (legCrewViewModelObject.PreflightCrewListID > 0 && legCrewViewModelObject.State != TripEntityState.Deleted)
                                {
                                      legCrewViewModelObject.State = TripEntityState.Modified;
                                }
                            }
                            bool isDelete = true;
                            for (int i = 0; i < tempDutyType.Count; i++)
                            {
                                if (tempDutyType[i] != "0")
                                {
                                    isDelete = false;
                                }
                            }
                            if (isDelete)
                            {
                                crewWithoutDutyList.Add(crewid.ToString(), "true");
                            }
                        }
                }
            }
            var Preflegslist = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
            foreach (var Leg in Preflegslist)
            {
                int reOrderNum=0;
                List<PreflightLegCrewViewModel> existingCrewListOnLeg = new List<PreflightLegCrewViewModel>();
                if (Trip.PreflightLegCrews.ContainsKey(Leg.LegNUM.ToString()))
                    existingCrewListOnLeg = Trip.PreflightLegCrews[Leg.LegNUM.ToString()];
                foreach (PreflightLegCrewViewModel crewObj in existingCrewListOnLeg.OrderBy(p => p.OrderNUM.Value).ToList())
                {
                    bool isExist = false;
                    bool isAssigned = true;
                    foreach (var gridDataitem in obj)
                    {
                        long crewid = 0;
                        long.TryParse(Convert.ToString(gridDataitem["CrewID"].Value), out crewid);
                        if (crewObj.CrewID == crewid)
                        {
                            isExist = true;
                            
                            if (crewWithoutDutyList.Keys.Contains(crewid.ToString()))
                            {
                                isAssigned = false;
                                crewObj.OrderNUM = 0;
                            }
                            crewObj.OrderNUM = crewObj.OrderNUM - reOrderNum;
                            break;
                        }
                    }
                    if (!isAssigned && crewObj.PreflightCrewListID < 0 )
                    {
                        crewObj.IsDeleted = true;
                        reOrderNum++;
                    }
                    if (crewObj.PreflightCrewListID == 0 && (crewObj.DutyTYPE == "0" || (crewObj.DutyTYPE != "0" && !isExist)))
                    {
                        crewObj.State = TripEntityState.NoChange;
                        crewObj.IsDeleted = true;
                        if(!isExist)
                        existingCrewListOnLeg.Remove(crewObj);
                        if (crewWithoutDutyList.Keys.Contains(crewObj.CrewID.ToString()))
                            reOrderNum++;
                    }
                    else if (crewObj.PreflightCrewListID > 0 && (crewObj.DutyTYPE == "0" || !isExist))
                    {
                        crewObj.State = TripEntityState.Deleted;
                        crewObj.IsDeleted = true;
                        if (crewWithoutDutyList.Keys.Contains(crewObj.CrewID.ToString()))
                            reOrderNum++;
                    }
                }
            }

            HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] = Trip;
        }

        TripEntityState GetRecordState(string state)
        {
            TripEntityState recState=0;
        
            switch(state)
            {
                case "added":
                    recState = TripEntityState.Added;
                    break;
                case "modified":
                    recState= TripEntityState.Modified;
                    break;
                case "deleted":
                    recState = TripEntityState.Deleted;
                    break;
                case "nochange":
                    recState = TripEntityState.NoChange;
                    break;
            }

            return recState;
        }
        public List<CrewPassportViewModel> GetCrewPassportData(string crewID)
        {
            HttpContext.Current.Session[WebSessionKeys.SelectedCrewRosterID] = crewID;
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            using (MasterCatalogServiceClient CrewRoasterService = new MasterCatalogServiceClient())
            {
                FlightPakMasterService.CrewPassengerPassport CrewPassport = new FlightPakMasterService.CrewPassengerPassport();
                CrewPassport.CrewID = Convert.ToInt64(crewID);
                CrewPassport.PassengerRequestorID = null;
                var CrewPaxPassportValue = CrewRoasterService.GetCrewPassportListInfo(CrewPassport);
                List<CrewPassportViewModel> CrewPassportData = new List<CrewPassportViewModel>();

                if (CrewPaxPassportValue.ReturnFlag == true)
                {
                    CrewPassportData = CrewPaxPassportValue.EntityList.Select(x => new CrewPassportViewModel
                    {
                        Choice = x.Choice,
                        CountryCD = x.CountryCD,
                        CountryID = x.CountryID,
                        CountryName = x.CountryName,
                        CrewID = x.CrewID,
                        CustomerID = x.CustomerID,
                        IsDefaultPassport = x.IsDefaultPassport,
                        IsDeleted = x.IsDeleted,
                        IsInActive = x.IsInActive,
                        IssueCity = x.IssueCity,
                        IssueDT = String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", x.IssueDT),
                        LastUpdTS = x.LastUpdTS,
                        LastUpdUID = x.LastUpdUID,
                        PassengerRequestorID = x.PassengerRequestorID,
                        PassportExpiryDT = String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", x.PassportExpiryDT),
                        PassportID = x.PassportID,
                        PassportNum = x.PassportNum,
                        PilotLicenseNum = x.PilotLicenseNum
                    }).ToList();

                }
                return CrewPassportData;
            }
        }

        public List<CrewVisaViewModel> GetCrewVisaData(string crewID)
        {
            HttpContext.Current.Session[WebSessionKeys.SelectedCrewRosterID] = crewID;
            PreflightTripViewModel pfViewModel = new PreflightTripViewModel();
            using (MasterCatalogServiceClient Service = new MasterCatalogServiceClient())
            {
                CrewPassengerVisa CrewVisa = new CrewPassengerVisa();
                CrewVisa.CrewID = Convert.ToInt64(crewID);
                CrewVisa.PassengerRequestorID = null;
                var CrewPaxVisaList = Service.GetCrewVisaListInfo(CrewVisa);
                List<CrewVisaViewModel> CrewVisaData = new List<CrewVisaViewModel>();

                if (CrewPaxVisaList.ReturnFlag == true)
                {
                    CrewVisaData = CrewPaxVisaList.EntityList.Select(x => new CrewVisaViewModel
                    {
                        CountryCD = x.CountryCD,
                        CountryID = x.CountryID,
                        CountryName = x.CountryName,
                        CrewID = x.CrewID,
                        CustomerID = x.CustomerID,
                        EntriesAllowedInPassport = x.EntriesAllowedInPassport,
                        ExpiryDT = x.ExpiryDT,
                        IsDeleted = x.IsDeleted,
                        IsInActive = x.IsInActive,
                        IssueDate = String.Format(CultureInfo.InvariantCulture, "{0:" + CompDateFormat + "}", x.IssueDate),
                        IssuePlace = x.IssuePlace,
                        LastUpdTS = x.LastUpdTS,
                        LastUpdUID = x.LastUpdUID,
                        Notes = x.Notes,
                        PassengerRequestorID = x.PassengerRequestorID,
                        TypeOfVisa = x.TypeOfVisa,
                        VisaExpireInDays = x.VisaExpireInDays,
                        VisaID = x.VisaID,
                        VisaNum = x.VisaNum,
                        VisaTYPE = x.VisaTYPE
                    }).ToList();
                }
                return CrewVisaData;
            }

        }

        public void UpdateCrewHotelDates(double dateDifference)
        {
            if (HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip] != null)
            {
                var Trip = (PreflightTripViewModel)HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
                if (Trip.PreflightMain.EditMode == true)
                {
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        List<PreflightLegViewModel> flightLeg = Trip.PreflightLegs.Where(p => p.IsDeleted == false).OrderBy(p => p.LegNUM).ToList();
                        foreach (PreflightLegViewModel Leg in flightLeg)
                        {
                            if (Trip.PreflightLegCrewLogisticsHotelList.ContainsKey(Leg.LegNUM.ToString()))
                            {
                                foreach (var crewHotel in Trip.PreflightLegCrewLogisticsHotelList[Leg.LegNUM.ToString()])
                                {
                                    if (Leg.ArrivalDTTMLocal.HasValue)
                                        crewHotel.DateIn = new DateTime(Leg.ArrivalDTTMLocal.Value.Ticks, DateTimeKind.Local);

                                    var flightlegObj=flightLeg.Where(t => t.LegNUM == (Leg.LegNUM + 1)).FirstOrDefault();

                                    if ( flightlegObj != null && flightlegObj.DepartureDTTMLocal.HasValue)
                                    {
                                        crewHotel.DateOut = new DateTime(flightlegObj.DepartureDTTMLocal.Value.Ticks, DateTimeKind.Local);
                                    }
                                    else if(crewHotel.DateOut.HasValue)
                                    {
                                        crewHotel.DateOut = crewHotel.DateOut.Value.AddDays(dateDifference);
                                        crewHotel.DateOut = new DateTime(crewHotel.DateOut.Value.Ticks, DateTimeKind.Local);
                                    }
                                    else
                                    {
                                        crewHotel.DateOut = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        internal DateTime? ValidateDateInForHotel(PreflightLegViewModel currentLegVM, PreflightLegCrewHotelViewModel crewHotelVM)
        {
            if (crewHotelVM.DateIn.HasValue)
                return new DateTime(crewHotelVM.DateIn.Value.Ticks, DateTimeKind.Local);

            if (currentLegVM != null && currentLegVM.ArrivalDTTMLocal.HasValue)
            {
                return new DateTime(currentLegVM.ArrivalDTTMLocal.Value.Ticks, DateTimeKind.Local);
            }
            return null;
        }

        internal DateTime? ValidateDateOutForHotel(PreflightLegViewModel nextLegVM, PreflightLegCrewHotelViewModel crewHotelVM)
        {
            if (crewHotelVM.DateOut.HasValue)
                return new DateTime(crewHotelVM.DateOut.Value.Ticks, DateTimeKind.Local);

            if (nextLegVM != null && nextLegVM.DepartureDTTMLocal.HasValue)
            {
                return new DateTime(nextLegVM.DepartureDTTMLocal.Value.Ticks, DateTimeKind.Local);
            }
            return null;
        }
    }
}




