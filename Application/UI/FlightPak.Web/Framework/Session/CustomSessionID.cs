﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace FlightPak.Web.Framework.Session
{
    public class CustomSessionID : SessionIDManager
    {
        public override string CreateSessionID(HttpContext context)
        {
            return Guid.NewGuid().ToString();
        }

        public override bool Validate(string ID)
        {
            // Code has been changed by vishwa as per review document
            // Use Try parse instead of Try... Catch Mechanism
            Guid testGuid = new Guid();
            return Guid.TryParse(ID, out testGuid);
           
        }
    }
}