﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Prinicipal;
using System.Web.Security;
using System.Collections.Specialized;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Views.Transactions;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web
{
    public class BaseSecuredPage : BasePage
    {
        protected FPPrincipal UserPrincipal
        {
            get
            {
                if ((FPPrincipal)Session[Session.SessionID] != null)
                    return (FPPrincipal)Session[Session.SessionID];
                else
                {
                    LogOut();
                    return (FPPrincipal)null;
                }
            }
        }

        protected void CheckAutorization(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                var principal = ((FPPrincipal)Session[Session.SessionID]).IsInRole(ModuleName.ToLower());
                if (!principal)
                {
                    Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                }
            }
        }
        public bool IsAuthorized(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                return ((FPPrincipal)Session[Session.SessionID]).IsInRole(ModuleName);
            }
            return false;
        }
        public bool HaveModuleAccess(string moduleId)
        { //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                return ((FPPrincipal)Session[Session.SessionID]).IsInModule(moduleId);
            }
            return false;
        }
		  public void HaveModuleAccessAndRedirect(string moduleId)
        { //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                var principal = ((FPPrincipal)Session[Session.SessionID]).IsInModule(moduleId);
                if (!principal)
                {
                    Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                }
            }
        }
        public DateTime GetDate(DateTime dt)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
                return ((FPPrincipal)Session[Session.SessionID]).GetDate(dt);
            return DateTime.Now;
        }
        public string GetDateWithoutSeconds(DateTime dt)
        {
            //Session Timeout fix
            if ((((FPPrincipal)Session[Session.SessionID]) != null) && (dt != null))
                return String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + " HH:mm }", GetDate(dt));
            return String.Empty;
        }
        public bool IsInClient(Int64 clientId, ref string clientCode)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
                return ((FPPrincipal)Session[Session.SessionID]).IsInClient(clientId, ref clientCode);
            return false;
        }
        // This method will be executed when there is a chance for FPPrincipal Data Update
        protected void RefreshSession()
        {
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {

                Session[Session.SessionID] = new FPPrincipal(AuthService.GetUserPrincipal(Session.SessionID)._identity);
            }
        }
        
        /// <summary>
        /// Format Date based on format and data
        /// </summary>
        /// <param name="Date"></param>
        /// <param name="Format"></param>
        /// <returns></returns>
        public string FormatDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                string[] DateAndTime = Date.Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }
                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                //return new DateTime(yyyy, mm, dd);
                return (yyyy + "/" + mm + "/" + dd);
            }
        }

        public bool IsPopUp
        {
            get
            {
                if (Request.QueryString["IsPopup"] == string.Empty || Request.QueryString["IsPopup"] == "Add" || Request.QueryString["IsPopup"] == "View")
                    return true;
                return false;
            }
        }

        public bool IsCalender
        {
            get
            {
                if (Request.QueryString["IsCalender"] == "1")
                    return true;
                return false;
            }
        }

        public bool IsUIReports
        {
            get
            {
                if (Request.QueryString["IsUIReports"] != null && Request.QueryString["IsUIReports"] == "1")
                    return false;
                return true;
            }
        }
        
        public bool IsAdd
        {
            get
            {
                if (Request.QueryString["IsPopup"] == "Add")
                    return true;
                return false;
            }
        }

        /// <summary>
        /// Added this Properly Specifically for View the Catalog in Pop up
        /// </summary>
        public bool IsViewOnly
        {
            get
            {
                if (Request.QueryString["IsPopup"] != null && Request.QueryString["IsPopup"] == "View")
                    return true;
                return false;
            }
        }

        public bool ShowCRUD
        {
            get
            {
                //Don't show CRUD for Reports
                if (Request.UrlReferrer.AbsoluteUri.ToUpper().Contains("/REPORTS/"))
                {
                    return false;
                }
                NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);
                string _isPopup = nameValueCollection["IsPopup"];
                if (_isPopup == string.Empty || _isPopup == "Add")
                    return false;
                return true;
            }
        }

        //Added by Ramesh for Other Crew Duty Log Popup
        public bool IsLogPopup
        {
            get
            {
                if (Request.QueryString["IsLogPopup"] == "Popup")
                    return true;
                return false;
            }
        }

        //Added by Ramesh, created common method for label display in all grids for DBCatalogs
        public void LastUpdatedUserDetails(Telerik.Web.UI.RadGrid gridName, bool IsBindData)
        {            
            if (gridName.SelectedItems.Count != 0)
            {
                System.Web.UI.WebControls.Label lbLastUpdatedUser;

                Telerik.Web.UI.GridDataItem Item = gridName.SelectedItems[0] as Telerik.Web.UI.GridDataItem;

                lbLastUpdatedUser = (System.Web.UI.WebControls.Label)gridName.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                if (Item.GetDataKeyValue("LastUpdUID") != null)
                {
                    lbLastUpdatedUser.Text = HttpUtility.HtmlEncode("Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString());
                }
                else
                {
                    lbLastUpdatedUser.Text = string.Empty;
                }
                if (Item.GetDataKeyValue("LastUpdTS") != null)
                {
                    lbLastUpdatedUser.Text = HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + GetDateWithoutSeconds(Convert.ToDateTime(Item.GetDataKeyValue("LastUpdTS").ToString().Trim())));
                }
                //if (IsBindData == true)
                //{
                    //gridName.DataBind();
                //}
            }
        }

        public void refreshUserPrincipal()
        {
            if (Session[WebSessionKeys.UserPrincipalModel] != null)
            {
                using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
                {
                    HttpContext.Current.Session.Remove(WebSessionKeys.UserPrincipalModel);
                    UserPrincipalViewModel objUP = TripManagerBase.getUserPrincipal();
                }
            }
        }


    }
}