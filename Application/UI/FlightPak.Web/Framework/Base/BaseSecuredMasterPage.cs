﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Prinicipal;
using System.Web.UI;
using System.Web.Security;
using System.Runtime.CompilerServices;

namespace FlightPak.Web
{
    public class BaseSecuredMasterPage : System.Web.UI.MasterPage
    {
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckRedirection();
                // If Session is null then log out the user
                if (Session[Session.SessionID] == null)
                {
                    LogOut();
                }
            }
        }

        protected FPPrincipal UserPrincipal
        {
            get
            {
                if ((FPPrincipal)Session[Session.SessionID] != null)
                    return (FPPrincipal)Session[Session.SessionID];
                else
                {
                    LogOut();
                    return (FPPrincipal)null;
                }
            }
        }

        protected void CheckAutorization(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                var principal = ((FPPrincipal)Session[Session.SessionID]).IsInRole(ModuleName.ToLower());
                if (!principal)
                {
                    Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                }
            }
        }
        public bool IsAuthorized(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                return ((FPPrincipal)Session[Session.SessionID]).IsInRole(ModuleName.ToLower());
            }
            return false;
        }
        public bool HaveModuleAccess(string moduleId)
        { //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                return ((FPPrincipal)Session[Session.SessionID]).IsInModule(moduleId);
            }
            return false;
        }
        public DateTime GetDate(DateTime dt)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
                return ((FPPrincipal)Session[Session.SessionID]).GetDate(dt);
            return DateTime.Now;
        }
        public bool IsInClient(Int64 clientId, ref string clientCode)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
                return ((FPPrincipal)Session[Session.SessionID]).IsInClient(clientId, ref clientCode);
            return false;
        }
        public void ProcessErrorMessage(Exception ex, string ModuleName)
        {
            //Vishwa
            CheckPrincipalEmpty(ex);
            // Radalert has been changed to javascript alert
            //string AlertMsg = "radalert('" + ex.Message + "', 360, 50, '" + ModuleName + "');";
            string errMsg = ex.Message.Replace("'", "").Replace(Environment.NewLine, " ");
            string AlertMsg = "jAlert('" + errMsg + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }

        public void ShowAlert(string message, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            //string AlertMsg = "alert('" + message + "', 360, 50, '" + ModuleName + "');";
            string AlertMsg = "radalert('" + message + "', 360, 50, '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }

        /// <summary>
        /// Used to Log out
        /// </summary>
        protected void LogOut([CallerMemberName]string memberName = "")
        {
            string RedirectPage = string.Empty;
            // Clear cache entry for FlightPak.Service
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                RedirectPage = Session["_landingPage"] != null
                    ? Convert.ToString(Session["_landingPage"])
                    : string.Empty;
                commonService.LogOut();
            }
            // Delete the record in login table
            using (AuthenticationService.AuthenticationServiceClient authenticationService = new AuthenticationService.AuthenticationServiceClient())
            {
                authenticationService.Logout(Session.SessionID);
            }


            // Sign out from Forms Authentication
            FormsAuthentication.SignOut();
            Session.Abandon();

            //Clear FormsAuthentication Cookie
            HttpCookie FormsAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            FormsAuthenticationCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(FormsAuthenticationCookie);

            //Clear ASP.NET_SessionId
            HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            SessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(SessionCookie);

            //Clear View State
            ViewState.Clear();
            this.ClearChildViewState();

            if (String.IsNullOrEmpty(RedirectPage) && memberName.Trim() != "lnkLogout_Click")
                FormsAuthentication.RedirectToLoginPage();
            else
                Response.Redirect("~//Account//Login.aspx", false);

            Response.End();
        }

        // This method will be executed when there is a chance for FPPrincipal Data Update
        protected void RefreshSession()
        {
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {

                Session[Session.SessionID] = new FPPrincipal(AuthService.GetUserPrincipal(Session.SessionID)._identity);
            }
        }
        /// <summary>
        /// This method is used to force the user to change security hint or password
        /// Possible situations:
        /// 1. Create New User - Change Password, Change Security Hint
        /// 2. Reset Password from User Settings - Change Password
        /// 3. Forgot Password - Change Password
        /// </summary>
        private void CheckRedirection()
        {
            using (AdminService.AdminServiceClient client = new AdminService.AdminServiceClient())
            {
                if (Session[Session.SessionID] != null)
                {
                    string ChangePassURL = "~/Account/Change" + "Pass" + "word.aspx", ChangeSecurityHintURL = "~/Account/ChangeHint.aspx";
                    if (Session["ForceChangePassword"] == null)
                    {
                        Session["ForceChangePassword"] = client.GetUserByUserName(((FPPrincipal)Session[Session.SessionID]).Identity._name);
                    }
                    var forceObject = (AdminService.ReturnValueOfUserMaster)Session["ForceChangePassword"];
                    if (forceObject.EntityInfo.ForceChangePassword && !HttpContext.Current.Request.Url.AbsoluteUri.Contains(ChangePassURL.Replace("~", "")) && !HttpContext.Current.Request.Url.AbsoluteUri.Contains(ChangeSecurityHintURL.Replace("~", "")))
                    { Response.Redirect(ChangePassURL, true); }
                    else if (forceObject.EntityInfo.ForceChangeSecurityHint && !HttpContext.Current.Request.Url.AbsoluteUri.Contains(ChangePassURL.Replace("~", "")) && !HttpContext.Current.Request.Url.AbsoluteUri.Contains(ChangeSecurityHintURL.Replace("~", "")))
                    { Response.Redirect(ChangeSecurityHintURL, true); }
                }

            }

        }

        protected void CheckPrincipalEmpty(Exception ex)
        {
            if (ex != null && ex.Message.Contains("Login session has expired or logged-in using other browser or other device."))
            {
                // Perform Logout
                LogOut();
            }
        }

        /// <summary>
        /// Try to resolve a web path to the current website, including the special "~/" app path.
        /// This method be used outside the context of a Control (aka Page).
        /// </summary>
        /// <param name="strWebpath">The path to try to resolve.</param>
        /// <param name="strResultUrl">The stringified resolved url (upon success).</param>
        /// <returns>true if resolution was successful in which case the out param contains a valid url, otherwise false</returns>
        /// <remarks>
        /// If a valid URL is given the same will be returned as a successful resolution.
        /// </remarks>
        /// 
        static public bool TryResolveUrl(string strWebpath, out string strResultUrl)
        {

            Uri uriMade = null;
            Uri baseRequestUri = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));

            // Resolve "~" to app root;
            // and create http://currentRequest.com/webroot/formerlyTildeStuff
            if (strWebpath.StartsWith("~"))
            {
                string strWebrootRelativePath = string.Format("{0}{1}",
                    HttpContext.Current.Request.ApplicationPath,
                    strWebpath.Substring(1));

                if (Uri.TryCreate(baseRequestUri, strWebrootRelativePath, out uriMade))
                {
                    strResultUrl = uriMade.ToString();
                    return true;
                }
            }

            // or, maybe turn given "/stuff" into http://currentRequest.com/stuff
            if (Uri.TryCreate(baseRequestUri, strWebpath, out uriMade))
            {
                strResultUrl = uriMade.ToString();
                return true;
            }

            // or, maybe leave given valid "http://something.com/whatever" as itself
            if (Uri.TryCreate(strWebpath, UriKind.RelativeOrAbsolute, out uriMade))
            {
                strResultUrl = uriMade.ToString();
                return true;
            }

            // otherwise, fail elegantly by returning given path unaltered.    
            strResultUrl = strWebpath;
            return false;
        }

        public string ModuleSelectedValue { get; set; }
    }
}