﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web.UI;
using System.Runtime.InteropServices;
using System.Web.Security;
using System.Text.RegularExpressions;
using FlightPak.Web.Views.Transactions.Preflight;

namespace FlightPak.Web
{
    public class BasePage : System.Web.UI.Page
    {
        /// <summary>
        /// All the ASP.net Pages in FlightPak should inherit this class and use WCFCall to make all WCF calls.
        /// There are two purpose for this method.
        /// 1. To pass SessionId as WCF Custom Message Header
        /// 2. To retry in case of WCF Call failure. FlightPak does lots of WCF calls to talk Application layer.
        /// We cannot afford for WCF call failure, when fails we should retry before logging to the log file and 
        /// give up the call.
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public Object WCFCall(Func<Object> func)  // WCFCall with retry and delay
        {
            //todo - // please write logic to read from config file.
            int maxRetries = 3; // please write logic to read from config file.
            int delayInMilliseconds = 2; // please write logic to read from config file.

            Object returnValue = default(Object);
            int numTries = 0;
            bool succeeded = false;
            while (numTries < maxRetries)
            {
                //todo - We should catch some specific exceptions; such as SQL Exceptions
                //When SQL Exception accours then there is no point in RETRY, should log and return NULL
                try
                {
                    returnValue = func();
                    succeeded = true;

                }
                catch (Exception ex)
                {
                    //Manually Handled
                    //todo
                    //Write code to Log to the error log file / tracing

                    //((ReturnValue<Object>)returnValue).ErrorMessage = ex.Message;

                }
                finally
                {
                    numTries++;
                }

                if (succeeded)
                {
                    //Todo - Please log to the log file as success
                    return returnValue;
                }

                //Delay for configured Milliseconds, before retry the WCF call.
                System.Threading.Thread.Sleep(delayInMilliseconds);
            }

            //Todo - Please log to the log file, if the code comes here, then its returnning NULL to the
            //Called application. Could be Critical.
            return default(Object);
        }

        public void ProcessErrorMessage(Exception ex, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            //string AlertMsg = "alert('" + ex.Message + "', 360, 50, '" + ModuleName + "');";
            CheckPrincipalEmpty(ex);
            string errMsg = ex.Message.Replace("'", "").Replace(Environment.NewLine, " ");
            string AlertMsg = "jAlert('" + errMsg + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }

        public void ProcessErrorMessageNew(Exception ex, string ModuleName)
        {
            //Manish 
            CheckPrincipalEmpty(ex);
            string errMsg = ex.Message.Replace("'", "").Replace(Environment.NewLine, " ");
            string AlertMsg = "jAlert('" + errMsg + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertscript", AlertMsg, true);
        }

        public void ShowAlert(string message, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            string AlertMsg = "radalert('" + message + "', 360, 50, '" + ModuleName + "');";
            //string AlertMsg = "radalert('" + message + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }
        protected void ShowSuccessMessage()
        {
            //string AlertMsg = "alert('" + "Test" + "');";
            //ScriptManager.(this, this.GetType(), "radalert", AlertMsg, true);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
        }

        protected void CheckPrincipalEmpty(Exception ex)
        {
            if (ex != null && ex.Message.Contains("Login session has expired or logged-in using other browser or other device."))
            {
                // Perform Logout
                LogOut();
            }
        }
        /// <summary>
        /// Used to Log out
        /// </summary>
        protected void LogOut()
        {
            // Clear cache entry for FlightPak.Service
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                commonService.LogOut();
            }
            // Delete the record in login table
            using (AuthenticationService.AuthenticationServiceClient authenticationService = new AuthenticationService.AuthenticationServiceClient())
            {
                authenticationService.Logout(Session.SessionID);
            }


            // Sign out from Forms Authentication
            FormsAuthentication.SignOut();
            Session.Abandon();

            //Clear FormsAuthentication Cookie
            HttpCookie FormsAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            FormsAuthenticationCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(FormsAuthenticationCookie);

            //Clear ASP.NET_SessionId
            HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            SessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(SessionCookie);

            //Clear View State
            ViewState.Clear();
            this.ClearChildViewState();


            FormsAuthentication.RedirectToLoginPage();
            Response.End();
        }

        public void ProcessErrorMessagePopup(Exception ex, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            //string AlertMsg = "alert('" + ex.Message + "', 360, 50, '" + ModuleName + "');";
            string AlertMsg = "function f(){radalert('" + ex.Message.Replace("'", "") + "', 360, 50, '" + ModuleName + "');; Sys.Application.remove_load(f);}; Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }
        public void ShowLockAlertPopup(string message, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            string AlertMsg = "function f(){radalert('" + message + "', 360, 50, '" + ModuleName + "'); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);";
            //string AlertMsg = "radalert('" + message + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }
        /// <summary>
        /// Try to resolve a web path to the current website, including the special "~/" app path.
        /// This method be used outside the context of a Control (aka Page).
        /// </summary>
        /// <param name="strWebpath">The path to try to resolve.</param>
        /// <param name="strResultUrl">The stringified resolved url (upon success).</param>
        /// <returns>true if resolution was successful in which case the out param contains a valid url, otherwise false</returns>
        /// <remarks>
        /// If a valid URL is given the same will be returned as a successful resolution.
        /// </remarks>
        /// 
        static public bool TryResolveUrl(string strWebpath, out string strResultUrl)
        {

            Uri uriMade = null;
            Uri baseRequestUri = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));

            // Resolve "~" to app root;
            // and create http://currentRequest.com/webroot/formerlyTildeStuff
            if (strWebpath.StartsWith("~"))
            {
                string strWebrootRelativePath = string.Format("{0}{1}",
                    HttpContext.Current.Request.ApplicationPath,
                    strWebpath.Substring(1));

                if (Uri.TryCreate(baseRequestUri, strWebrootRelativePath, out uriMade))
                {
                    strResultUrl = uriMade.ToString();
                    return true;
                }
            }

            // or, maybe turn given "/stuff" into http://currentRequest.com/stuff
            if (Uri.TryCreate(baseRequestUri, strWebpath, out uriMade))
            {
                strResultUrl = uriMade.ToString();
                return true;
            }

            // or, maybe leave given valid "http://something.com/whatever" as itself
            if (Uri.TryCreate(strWebpath, UriKind.RelativeOrAbsolute, out uriMade))
            {
                strResultUrl = uriMade.ToString();
                return true;
            }

            // otherwise, fail elegantly by returning given path unaltered.    
            strResultUrl = strWebpath;
            return false;
        }
        public void ProcessErrorMessage(string ErrorMessage, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            //string AlertMsg = "alert('" + ex.Message + "', 360, 50, '" + ModuleName + "');";
            string AlertMsg = "jAlert('" + ErrorMessage.Replace("'", "") + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }

        public string IsLocalUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }

            Uri absoluteUri;
            if (Uri.TryCreate(url, UriKind.Absolute, out absoluteUri))
            {
                return (String.Equals(this.Request.Url.Host, absoluteUri.Host,
                    StringComparison.OrdinalIgnoreCase))
                    ? url
                    : "";
            }
            else
            {
                bool isLocal = !url.StartsWith("http:", StringComparison.OrdinalIgnoreCase)
                    && !url.StartsWith("https:", StringComparison.OrdinalIgnoreCase)
                    && Uri.IsWellFormedUriString((url.Contains('?') ? url.Substring(0, url.IndexOf('?')) : url), UriKind.Relative);
                return isLocal ? url : "";
            }
        }

        static public string TruncateStringByLineBreak(string actualString)
        {
            string[] manipulatedArray = Regex.Split(actualString, "<br />");
            manipulatedArray = manipulatedArray.Take(6).ToArray();
            return string.Join("<br />", manipulatedArray) + " ...";
        }

        public void RedirectToPage(string url)
        {
            string uri=string.Empty;
            //TryResolveUrl(url, out uri);
            uri = IsLocalUrl(url);
            if (!string.IsNullOrEmpty(uri) && !string.IsNullOrWhiteSpace(uri))
            {
                Response.Redirect(uri);
            }
        }

        /// <summary>
        /// VARACODE - Neutralization of Script-Related HTML Tags in a Web Page (Basic XSS) for Byte[] array
        /// Cross Site Scripting validation for byte[]
        /// </summary>
        /// <param name="stream">Byte[] for validation</param>
        /// <returns></returns>
        public static byte[] GetFormatedString(byte[] stream)
        {
            string str = Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToBase64String(stream));
            return Convert.FromBase64String(str);
        }
    }
}
