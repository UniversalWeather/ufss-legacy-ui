﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Prinicipal;
using System.Web.Security;

namespace FlightPak.Web.Framework.Base
{
    public class BaseSecuredUserControl : System.Web.UI.UserControl
    {
        protected FPPrincipal UserPrincipal
        {
            get
            {
                if ((FPPrincipal)Session[Session.SessionID] != null)
                    return (FPPrincipal)Session[Session.SessionID];
                else
                {
                    LogOut();
                    return (FPPrincipal)null;
                }
            }
        }

        protected void CheckAutorization(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                var principal = ((FPPrincipal)Session[Session.SessionID]).IsInRole(ModuleName.ToLower());
                if (!principal)
                {
                    Response.Redirect(ResolveUrl("~/Views/ErrorPages/AccessDenied.aspx"));
                }
            }
        }
        public bool IsAuthorized(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
            {
                return ((FPPrincipal)Session[Session.SessionID]).IsInRole(ModuleName);
            }
            return false;
        }
        public DateTime GetDate(DateTime dt)
        {

            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
                return ((FPPrincipal)Session[Session.SessionID]).GetDate(dt);
            return DateTime.Now;
        }
        public bool IsInClient(Int64 clientId, ref string clientCode)
        {
            //Session Timeout fix
            if (((FPPrincipal)Session[Session.SessionID]) != null)
                return ((FPPrincipal)Session[Session.SessionID]).IsInClient(clientId, ref clientCode);
            return false;
        }
        // This method will be executed when there is a chance for FPPrincipal Data Update
        protected void RefreshSession()
        {
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {
                Session[Session.SessionID] = new FPPrincipal(AuthService.GetUserPrincipal(Session.SessionID)._identity);
            }
        }
        /// <summary>
        /// Used to Log out
        /// </summary>
        protected void LogOut()
        {
            // Clear cache entry for FlightPak.Service
            using (CommonService.CommonServiceClient commonService = new CommonService.CommonServiceClient())
            {
                commonService.LogOut();
            }
            // Delete the record in login table
            using (AuthenticationService.AuthenticationServiceClient authenticationService = new AuthenticationService.AuthenticationServiceClient())
            {
                authenticationService.Logout(Session.SessionID);
            }


            // Sign out from Forms Authentication
            FormsAuthentication.SignOut();
            Session.Abandon();

            //Clear FormsAuthentication Cookie
            HttpCookie FormsAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            FormsAuthenticationCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(FormsAuthenticationCookie);

            //Clear ASP.NET_SessionId
            HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            SessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(SessionCookie);

            //Clear View State
            ViewState.Clear();
            this.ClearChildViewState();


            FormsAuthentication.RedirectToLoginPage();
            Response.End();
        }

        protected void CheckPrincipalEmpty(Exception ex)
        {
            if (ex != null && ex.Message.Contains("Login session has expired or logged-in using other browser or other device."))
            {
                // Perform Logout
                LogOut();
            }
        }
    }
}