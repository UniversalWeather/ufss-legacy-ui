﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Constants
{
    public static class WebConstants
    {
        public const string FlightPakException = "FlightPak.ExceptionHandling";

        public const String isoDateTimeFormat = "yyyy-MM-dd HH:mm";
        public const String isoDateFormat = "yyyy-MM-dd";

        public static class URL
        {
            public const string FleetForecast = "FleetForecast.aspx";
            public const string FleetComponentHistory = "FleetComponentHistory.aspx";
            public const string FleetNewCharterRate = "FleetNewCharterRate.aspx";
            public const string FleetProfileInternationalData = "FleetProfileInternationalData.aspx";
            public const string FleetChargeHistory = "FleetChargeHistory.aspx";
            public const string FleetProfileEngineAirframeInfo = "FleetProfileEngineAirframeInfo.aspx";
            public const string MapQuest="http://www.mapquest.com/maps";
            public const string TransportCatalog = "../Logistics/TransportCatalog.aspx";
            public const string CateringCatalog = "../Logistics/CateringCatalog.aspx";
            public const string FBOCatalog = "../Logistics/FBOCatalog.aspx";
            public const string HotelCatalog = "../Logistics/HotelCatalog.aspx";
            public const string TripSheetReportViewer = "~/Views/Reports/TripSheetReportViewer.aspx";
            public const string VendorContacts = "VendorContacts.aspx";
            public const string PreflightMain = "/Views/Transactions/PreFlight/PreflightMain.aspx?seltab=PreFlight";

        }

        /// <summary>
        /// stMiles = 1.15078
        /// </summary>
        public const double stMiles = 1.15078;

        public static class LegDutyType
        {
            /// <summary>
            /// Domestic = 1
            /// </summary>
            public const decimal Domestic = 1;
            /// <summary>
            /// International = 2
            /// </summary>
            public const decimal International = 2;
        }
        
        public static class POEmployeeType
        {
            public const String G = "G";
            public const String N = "N";
            public const String C = "C";
        }

        public static class POCrewDutyBasis
        {
            /// <summary>
            /// BlockOut = 1
            /// </summary>
            public const int BlockOut = 1;
            /// <summary>
            /// ScheduledDep = 2
            /// </summary>
            public const int ScheduledDep = 2;
        }
    }
}