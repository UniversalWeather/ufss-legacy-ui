﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace FlightPak.Web.Framework.Constants
{
    public static class SearchModuleItems
    {
        private static void LoadModuleItems()
        {
            StreamReader _srSearchModules = new StreamReader(HttpContext.Current.Server.MapPath("~") + "/Framework/Constants/SearchModuleItems.xml");
            _searchModules = _srSearchModules.ReadToEnd();
            _srSearchModules.Close();
        }
        public static string SearchModules
        {
            get
            {
                //TODO: Have to decide whether build version number should be set in global.asax or here...
                //_buildVersion will be set once - Application_Start
                if (string.IsNullOrEmpty(_searchModules)) { LoadModuleItems(); } return _searchModules;
            }
        }
        private static string _searchModules;
    }


}