﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Constants
{
    /// <summary>
    /// Contains keys of session used.
    /// </summary>
    public static class WebSessionKeys
    {        
        public const string PreflightLeg = "PreflightLeg";
        public const string UserPreference = "UserPreference";
        public const string POSessionKey = "postflightmainsessionkey";
        public const String CurrentPreflightTrip = "currenttrip";
        public const String CurrentPreflightTripMain = "CurrentPreFlightTrip";
        public const string OtherCrewDutyLog = "OtherCrewDutyLog";
        public const String UserPrincipalModel = "userprincipal";
        public const string PreflightMainLock = "PreflightMains";
        public const string PreflightException = "PreflightException";
        public const string PreflightExceptionTemplet = "PreflightExceptionTemplateList";
        public const string POException = "POEXCEPTION";
        public const String CrewInSelection = "CrewInSelection";
        public const String CrewInSummery = "CrewInSummery";
        public const string PostflightExpenseCatalog = "PostflightExpenseCatalog";
        
        public const string PaxSummaryList = "PaxSummaryList";

        public const string SelectedCrewRosterID = "SelectedCrewRosterID";

        public const string SelectedPaxRosterID = "SelectedPaxRosterID";
        public const string UnchangedCurrentTrip = "UnchangedCurrentTrip";

        public const string CurrentPostflightLog = "CurrentPostflightLog";
        public const string POFleetProfileKey = "POFleetProfileKey";
        public const string POSIFLRateKey = "POSIFLRateKey";
        public const string PassengerPassportCache = "PassengerPassportCache";
        public const string CountryCDCache = "CountryCDCache";
        public const string FlightPurposeListCache = "FlightPurposeListCache";
        public const string CrewPassportCache = "CrewPassportCache";
        public const string CountryMasterListCache = "CountryMasterListCache";
        public const string CrewDetailCache = "CrewDetailCache";
        public const string PostflightMainEntityName = "PostflightMains";
        public const string UnchangedCurrentPostflightLog = "POSTFLIGHTMAIN";
        public const string POFlightPurposeList = "poflightpurposelist";
        public const string CurrentLegCrews = "CurrentLegCrews";
        public const string IsAutoCalculateSIFL = "IsAutoCalculateSIFL";
        public const string IsNewLogExceptionShow = "ISNEWLOGEXCEPTIONSHOW";
        public const string POHomeBaseKey = "HOMEBASEKEY";            // Maintain Company profile settings
        public const string POCurrentLegNo = "POCURRENTLEGNO";
        public const string TripScreens = "TRIPSCREENS";
    }
}