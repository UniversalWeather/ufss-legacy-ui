﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Constants
{
    public static class ReportConstants
    {
        #region MIME-type / Content-Type

        public const string MIMEtype_EXCEL = @"application/vnd.ms-excel";
        public const string MIMEtype_WORD = @"application/msword";
        public const string MIMEtype_PDF = @"application/pdf";
        public const string MIMEtype_XML = @"application/xml"; //"text/xml"
        public const string MIMEtype_CSV = @"application/csv";
        public const string MIMEtype_HTML = @"text/html";
        public const string MIMEtype_MHTML = @"text/mhtml";
        public const string MIMEtype_UNKNOWN = @"application/octet-stream";

        #endregion

        #region MIME-type / File Extension

        public const string Extension_EXCEL = "xls";
        public const string Extension_WORD = "doc";
        public const string Extension_PDF = "pdf";
        public const string Extension_XML = "xml"; //"text/xml"
        public const string Extension_CSV = "csv";
        public const string Extension_HTML = "html";
        public const string Extension_MHTML = "mhtml";
        public const string Extension_UNKNOWN = "";

        #endregion

        public static class ReportType
        {
            public const string Preflight = "PreFlight";
            public const string Postflight = "PostFlight";
            public const string Database = "Db";
            public const string CorporateRequest = "CorpReq";
            public const string Charter = "Charter";
        }

    }
}