﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using FlightPak.Framework.Encryption;
using FlightPak.Web.Framework.Prinicipal;

namespace FlightPak.Web.Framework.Providers
{
    public class AuthProvider : MembershipProvider
    {
        private string _applicationName;
        private bool _enablePasswordReset;
        private bool _enablePasswordRetrieval;
        private bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;
        private int _maxInvalidPasswordAttempts;
        private int _passwordAttemptWindow;
        private string _passwordStrengthRegularExpression;
        //private MembershipPasswordFormat _passwordFormat;

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
            _applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            _enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            _enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "false"));
            _requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "true"));
            _requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "false"));
            _maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "4"));
            _passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            _passwordStrengthRegularExpression = GetConfigValue(config["passwordStrengthRegularExpression"], "");

            //string temp_format = config["passwordFormat"];
            //if (temp_format == null)
            //{
            //    temp_format = "Hashed";
            //}

            //switch (temp_format)
            //{
            //    case "Hashed":
            //        _passwordFormat = MembershipPasswordFormat.Hashed;
            //        break;
            //    case "Encrypted":
            //        _passwordFormat = MembershipPasswordFormat.Encrypted;
            //        break;
            //    case "Clear":
            //        _passwordFormat = MembershipPasswordFormat.Clear;
            //        break;
            //    default:
            //        throw new System.Configuration.Provider.ProviderException("Password format not supported.");
            //}
        }

        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        public override string ApplicationName
        {
            get
            {
                return _applicationName;
            }
            set
            {
                _applicationName = value;
            }
        }

        public override bool ChangePassword(string UserName, string OldPassword, string NewPassword)
        {
            using (AdminService.AdminServiceClient adminService = new AdminService.AdminServiceClient())
            {
                int periodValue=3;
                //foreach (string key in HttpContext.Current.Request.Form)
                //{
                //    if (key.ToLower().Contains("peroid"))
                //    {
                //        var Value = HttpContext.Current.Request.Form[key];
                //        int.TryParse(Value, out periodValue);
                //    }
                //}
                //TODO move password to secure string
                //System.Security.SecureString OldPasswordString = new System.Security.SecureString();
                //System.Security.SecureString NewPasswordString = new System.Security.SecureString();


                using (AdminService.AdminServiceClient client = new AdminService.AdminServiceClient())
                {
                    var user = client.GetUserByEMailId(UserName);
                    if (user != null && user.EntityList != null && user.EntityList.FirstOrDefault() != null && user.EntityList.FirstOrDefault().UserName.Length > 0)
                    {
                       long? Code= user.EntityList.FirstOrDefault().HomebaseID;
                       if (Code.HasValue)
                       {
                           using (FlightPakMasterService.MasterCatalogServiceClient FPKMasterService = new FlightPakMasterService.MasterCatalogServiceClient())
                           {
                               var CompanyList = FPKMasterService.GetListInfoByHomeBaseId(Code.Value).EntityList.ToList();
                               if (CompanyList.Count > 0)
                               {
                                   if (CompanyList.FirstOrDefault().PasswordValidDays != null && CompanyList.FirstOrDefault().PasswordValidDays != 0)
                                   {
                                       periodValue = int.Parse(CompanyList.FirstOrDefault().PasswordValidDays.Value.ToString());
                                   }
                               }
                           }
                       }
                    }
                }

                if (PasswordHistoryValidate(UserName, NewPassword))
                {

                    // return adminService.ChangePassword(HashPasswordForAuthentication(OldPassword, UserName), HashPassword(NewPassword));
                    return adminService.ChangePasswordWithPeriod(HashPasswordForAuthentication(OldPassword, UserName), HashPassword(NewPassword), periodValue);
                }
                return false;
            }
            //throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out System.Web.Security.MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { return _enablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return _enablePasswordRetrieval; }
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override System.Web.Security.MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string emailId, string answer)
        {
            if (answer == "Add")
                return GeneratePassword();
            else
            {
                Crypto crypto = new Crypto();
                //Hope Membership and hash key should be available in UI. 
                //So generating hashed password in UI and passing it to service.
                //string hashPassword = HashPassword(GeneratePassword());
                string hashPassword = GeneratePassword();

                using (AdminService.AdminServiceClient objAdmin = new AdminService.AdminServiceClient())
                {
                    objAdmin.ResetPasswordWithoutAuth(emailId, answer, hashPassword, string.Empty);
                }
                return string.Empty;
            }
        }

        public override System.Web.Security.MembershipUser GetUser(string username, bool userIsOnline)
        {
            //This is used by change password by default

            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && HttpContext.Current.User.Identity.Name.Length > 0)
            {
                //Do Change Password Stuff
                MembershipUser mem = new MembershipUser("FPMembershipProvider", HttpContext.Current.User.Identity.Name, new object(), "dummy@gmail.com", string.Empty, string.Empty, true, false, DateTime.Now.AddDays(-300), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
                mem.IsApproved = true;
                return mem;
            }
            else
            {
                //Else Un authenticated Stuff - Forgot Password
                using (AdminService.AdminServiceClient adminService = new AdminService.AdminServiceClient())
                {
                    using (AuthenticationService.AuthenticationServiceClient authService = new AuthenticationService.AuthenticationServiceClient())
                    {
                        var result = adminService.GetUserByEMailId(username);
                        if (result != null && result.EntityList != null && result.EntityList.FirstOrDefault() != null)
                        {
                            Crypto crypto = new Crypto();
                            var question = authService.GetUserSecurityHintByEmailId(username);
                            if (question.EntityInfo != null && !string.IsNullOrEmpty(question.EntityInfo.SecurityQuestion))
                            {
                                MembershipUser mem = new MembershipUser("FPMembershipProvider", result.EntityList.FirstOrDefault().EmailID, new object(), "dummy@gmail.com", question.EntityInfo != null ? question.EntityInfo.SecurityQuestion : string.Empty,
                                    string.Empty, true, false, DateTime.Now.AddDays(-300), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-1));
                                mem.IsApproved = true;
                                return mem;
                            }
                            else
                                return null;
                        }

                        else
                            return null;
                    }
                }

            }




        }

        public override System.Web.Security.MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            //This is used by change password by default
            return null;
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return _maxInvalidPasswordAttempts; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 0; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { return _passwordAttemptWindow; }
        }

        public override System.Web.Security.MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Clear; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return _passwordStrengthRegularExpression; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return _requiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return _requiresUniqueEmail; }
        }

        public override string ResetPassword(string username, string answer)
        {
            Crypto crypto = new Crypto();
            //Hope Membership and hash key should be available in UI. 
            //So generating hashed password in UI and passing it to service.
            //string hashPassword = HashPassword(GeneratePassword());
            string hashPassword = GeneratePassword();

            using (AdminService.AdminServiceClient objAdmin = new AdminService.AdminServiceClient())
            {
                objAdmin.ResetPassword(username, answer, hashPassword);
            }

            return string.Empty;
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(System.Web.Security.MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string UserName, string Password)
        {
            using (AuthenticationService.AuthenticationServiceClient AuthService = new AuthenticationService.AuthenticationServiceClient())
            {
                //System.Security.SecureString PasswordString = new System.Security.SecureString();

                //#region Construct Secure String
                //foreach (char passchar in Password)
                //{
                //    PasswordString.AppendChar(passchar);
                //}
                //#endregion
                //TODO move password to secure string

                #region Construct Session GUID
                Guid SessionId = new Guid(HttpContext.Current.Session.SessionID);
                #endregion

                return AuthService.Login(UserName, HashPasswordForAuthentication(Password, UserName), SessionId, _maxInvalidPasswordAttempts);
            }
        }
        private string HashPasswordForAuthentication(string plainText, string userName)
        {
            using (AuthenticationService.AuthenticationServiceClient client = new AuthenticationService.AuthenticationServiceClient())
            {
                Crypto crypto = new Crypto();
                string SaltValue = client.GetSaltValue(userName);
                return crypto.GenerateHash(plainText, SaltValue);
            }
        }

        private string HashPassword(string plainText)
        {
            Crypto crypto = new Crypto();
            string SaltValue = crypto.GenerateSaltValue();
            return crypto.GenerateHash(plainText, SaltValue);
        }

        private string GeneratePassword()
        {
            //return "fpk_123";
            return Membership.GeneratePassword(12, 1);
        }

        private bool PasswordHistoryValidate(string UserName,string NewPassword)
        {
            using (AdminService.AdminServiceClient client = new AdminService.AdminServiceClient())
            { Crypto crypto = new Crypto();
                foreach (var PasswordItem in client.GetPasswordHistory(UserName).EntityList)
                {
                    string SaltValue = PasswordItem.FPPWD.Substring(44);
                    var NewPasswordHash =crypto.GenerateHash(NewPassword, SaltValue);
                    if (NewPasswordHash == PasswordItem.FPPWD)
                        return false;
                }
            }
            return true;
        }
    }
}