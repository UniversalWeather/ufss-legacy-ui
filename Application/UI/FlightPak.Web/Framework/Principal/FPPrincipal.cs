﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using FlightPak.Web.AuthenticationService;
using System.Collections;
using System.Globalization;

namespace FlightPak.Web.Framework.Prinicipal
{
    [Serializable]
    public class FPPrincipal : IPrincipal
    {
        private readonly FPIdentity _identity;

        public FPPrincipal(FPIdentity identity)
        {
            _identity = identity;

        }
        public bool IsInRole(string role)
        {
            //if ((role != null) && (this.Identity._roles != null))
            //{
            //    for (int i = 0; i < this.Identity._roles.Count; i++)
            //    {
            //        if ((this.Identity._roles[i] != null) && (string.Compare(this.Identity._roles[i], role, StringComparison.OrdinalIgnoreCase) == 0))
            //        {
            //            return true;
            //        }
            //    }
            //}
            //return false;
            if ((role != null) && (this.Identity._roles != null))
            {
                return this.Identity._roles.BinarySearch(role.ToLower()) >= 0;
            }
            return false;
        }

        public bool IsInModule(string moduleId)
        {
            if ((moduleId != null) && (this.Identity._moduleId != null))
            {
                return this.Identity._moduleId.BinarySearch(moduleId.ToLower()) >= 0;
            }
            return false;
        }


        public FPIdentity Identity
        {
            get { return _identity; }
        }

        IIdentity IPrincipal.Identity
        {
            get { return (IIdentity)_identity; }
        }
        public DateTime GetDate(DateTime dt)
        {
            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
            {
                DateTime HomeBaseDt = objDstsvc.GetGMT(Convert.ToInt64(this.Identity._airportId), dt, false, false);
                return HomeBaseDt;
            }
        }

        public bool IsInClient(Int64 clientId, ref string clientCode)
        {
            string id = "$" + Convert.ToString(clientId) + "$";
            bool returnValue = this.Identity._clientInfo.Where(x => x.Contains(id)).Count() > 0;
            if (returnValue)
                clientCode = this.Identity._clientInfo.Where(x => x.Contains(id)).FirstOrDefault().Replace(id + ":", "");
            return returnValue;
        }
    }
}