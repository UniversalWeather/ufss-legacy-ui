﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FlightPak.Web.Framework.Helpers
{
    public class RadSchedulerCustomAppointmentComparer : IComparer<Telerik.Web.UI.Appointment>
    {
        public int Compare(Telerik.Web.UI.Appointment first, Telerik.Web.UI.Appointment second)
        {
            if (first == null || second == null)
            {
                throw new InvalidOperationException("Can't compare null object(s).");
            }

            if (first.Start < second.Start)
            {
                return -1;
            }

            if (first.Start > second.Start)
            {
                return 1;
            }

            if (first.End > second.End)
            {
                return -1;
            }

            int record = String.Compare(first.Attributes["RecordType"], second.Attributes["RecordType"]);
            if (record != 0)
            {
                return record;
            }
            if (Convert.ToDateTime(first.Attributes["ArrivalDisplayTime"]) < Convert.ToDateTime(second.Attributes["ArrivalDisplayTime"]))
            {
                return -1;
            }

            if (Convert.ToDateTime(first.Attributes["ArrivalDisplayTime"]) > Convert.ToDateTime(second.Attributes["ArrivalDisplayTime"]))
            {
                return 1;
            }

            return 0;
        }
    }
}