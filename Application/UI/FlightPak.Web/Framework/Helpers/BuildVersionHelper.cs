﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Web.UI;

namespace FlightPak.Web.Framework.Helpers
{
    public static class BuildVersionHelper
    {
        private static void LoadBuildVersion()
        {
            try
            {
                XmlTextReader reader = new XmlTextReader(HttpContext.Current.Server.MapPath("~") + "BuildVersion.xml");

                reader.WhitespaceHandling = WhitespaceHandling.None;
                XmlDocument xmlDoc = new XmlDocument();

                //Load the file into the XmlDocument
                xmlDoc.Load(reader);
                //Close off the connection to the file.
                reader.Close();

                XmlNodeList releaseNo = xmlDoc.GetElementsByTagName("ReleaseNo");

                
                XmlNodeList buildNo = xmlDoc.GetElementsByTagName("BuildNo");
                
                XmlNodeList revDate = xmlDoc.GetElementsByTagName("ReleaseDate");
                if (!string.IsNullOrEmpty(releaseNo[0].InnerText))
                { _buildVersion = "Release: " + releaseNo[0].InnerText; }
                //if (!string.IsNullOrEmpty(revDate[0].InnerText))
                //{ _buildVersion = _buildVersion + " | Date: " + revDate[0].InnerText; }
                if (!string.IsNullOrEmpty(buildNo[0].InnerText))
                { _buildVersion = _buildVersion + " | Build No: " + buildNo[0].InnerText; }
                //_buildVersion = "Release: " + releaseNo[0].InnerText + " | Date: " + revDate[0].InnerText + " | Build No: " + buildNo[0].InnerText;

            }
            catch
            {
                //Manually Handled
            }

        }
        public static string BuildVersion
        {
            get
            {
                //TODO: Have to decide whether build version number should be set in global.asax or here...
                //_buildVersion will be set once - Application_Start
                if (string.IsNullOrEmpty(_buildVersion)) { LoadBuildVersion(); } return _buildVersion;
            }
        }
        private static string _buildVersion;
    }
}