﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace FlightPak.Web.Framework.Helpers
{
    public class CustomUrlResovlerReports : XmlUrlResolver
    {
        public override Uri ResolveUri(Uri baseUri, string relativeUri)
        {
            if (baseUri != null)
            {
                Uri uri = new Uri(baseUri, relativeUri);
                if (IsUnsafeHost(uri.Host))
                    return null;
            }
            else
            {
                var fileName = Path.GetFileName(relativeUri);
                var filePath = HttpContext.Current.Server.MapPath("Config\\" + fileName);
                if (relativeUri.ToLower() != filePath.ToLower())
                    return null;
            }

            return base.ResolveUri(baseUri, relativeUri);
        }

        private bool IsUnsafeHost(string host)
        {
            return false;
        }
    }  
}