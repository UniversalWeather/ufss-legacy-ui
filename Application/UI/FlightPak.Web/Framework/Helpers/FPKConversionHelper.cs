﻿using FlightPak.Web.Framework.Helpers.Preflight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.Framework.Helpers
{

    public class FPKConversionHelper
    {
        public static String POHomeBaseKey = "HOMEBASEKEY";

        #region ConvertTenthsToMins
        public static string ConvertTenthsToMins(String timeToConvert)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timeToConvert))
            {
                string result = "00:00";
                if (!String.IsNullOrEmpty(timeToConvert))
                {
                    decimal tm_tenth = Math.Round(Convert.ToDecimal(timeToConvert), 3);
                    Int32 tm_mins = Convert.ToInt32(Math.Round(tm_tenth * 60, 3));
                    TimeSpan ts = new TimeSpan(0, Convert.ToInt32(tm_mins), 0);
                    string hrs = ((ts.Days * 24) + ts.Hours).ToString();
                    string mins = ts.Minutes.ToString();
                    if (hrs.Length == 1)
                        hrs = "0" + hrs;
                    if (mins.Length == 1)
                        mins = "0" + mins;
                    result = hrs + ":" + mins;
                }

                return result;
            }
        }
        #endregion

        #region ConvertMinToTenths
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="timeToConvert">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        public static string ConvertMinToTenths(String timeToConvert, Boolean isTenths, int conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timeToConvert, isTenths, conversionType))
            {
                string result = "00.0";
                decimal hour = 0;
                decimal minute = 0;

                if (!String.IsNullOrEmpty(timeToConvert) && timeToConvert.IndexOf(":") != -1)
                {
                    string[] timeArray = timeToConvert.Split(':');
                    if (timeArray != null)
                    {
                        if (!String.IsNullOrEmpty(timeArray[0]) && !String.IsNullOrEmpty(timeArray[0].Trim()))
                        {
                            hour = Convert.ToInt32(timeArray[0]);
                        }

                        if (!String.IsNullOrEmpty(timeArray[1]) && !String.IsNullOrEmpty(timeArray[1].Trim()))
                        {
                            minute = Convert.ToInt32(timeArray[1]);
                        }
                    }

                    if (isTenths && (conversionType == 1 || conversionType == 2)) // Standard and Flightpak Conversion
                    {
                        List<StandardFlightpakConversion> StandardFlightpakConversionList = LoadStandardFPConversion();
                        var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType == conversionType).ToList();

                        if (standardList != null && standardList.Count > 0)
                        {
                            result = Convert.ToString(hour + standardList[0].Tenths);
                        }
                    }
                    else if (isTenths && conversionType == 3) // Tenths - Min Conversion : Others
                    {
                        List<FlightPak.Web.CalculationService.GetPOHomeBaseSetting> homeBaseSetting = (List<FlightPak.Web.CalculationService.GetPOHomeBaseSetting>)HttpContext.Current.Session[WebSessionKeys.POHomeBaseKey];
                        if (homeBaseSetting != null && homeBaseSetting.Count > 0)
                        {
                            List<FlightPak.Web.CalculationService.GetPOTenthsConversion> tenthConvList = homeBaseSetting[0].TenthConversions.ToList().Where(x => minute >= x.StartMinimum && minute <= x.EndMinutes).ToList();

                            if (tenthConvList != null && tenthConvList.Count > 0)
                            {
                                result = Convert.ToString(hour + tenthConvList[0].Tenths);
                            }
                            else
                            {
                                // 1. minute >= startmin and minute >  end and start > end
                                // then increase with 1 hour
                                tenthConvList = homeBaseSetting[0].TenthConversions.ToList().Where(x => minute >= x.StartMinimum && minute > x.EndMinutes && x.StartMinimum > x.EndMinutes).ToList();

                                if (tenthConvList != null && tenthConvList.Count > 0)
                                {
                                    result = Convert.ToString(hour + 1);
                                }
                                else
                                {
                                    //minute < startmmin and minute < end and start > end
                                    tenthConvList = homeBaseSetting[0].TenthConversions.ToList().Where(x => minute < x.StartMinimum && minute < x.EndMinutes && x.StartMinimum > x.EndMinutes).ToList();
                                    result = Convert.ToString(hour);
                                }
                            }

                        }
                    }
                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return Convert.ToString(Math.Round(Convert.ToDecimal(result), 3));
            }
        }

        /// <summary>
        /// Method to Set Default Standard and Flightpak Conversion into List
        /// </summary>
        public static List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });

                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });

                return conversionList;
            }
        }

        #endregion

        public static Decimal GetDecimalFormattedTime(AuthenticationService.FPSettings fpSettings, String valueTobeConverted)
        {
            Decimal? TimeDisplayTenMin = null;
            decimal decimalValue;
            if (fpSettings != null)
                TimeDisplayTenMin = fpSettings._TimeDisplayTenMin;
            if ((!string.IsNullOrEmpty(valueTobeConverted)) && ((TimeDisplayTenMin.HasValue)))
            {
                int conversionType = (int)TimeDisplayTenMin.Value;
                var charExists = (valueTobeConverted.IndexOf(':') >= 0) ? true : false;
                if (!charExists)
                    valueTobeConverted = valueTobeConverted + ":00";

                decimalValue = System.Math.Round(Convert.ToDecimal(ConvertMinToTenths(valueTobeConverted, true, conversionType)), 3);
            }
            else
            {
                decimal hour = 0;
                decimal minute = 0;

                if (valueTobeConverted.IndexOf(":") != -1)
                {
                    string[] timeArray = valueTobeConverted.Split(':');
                    if (!String.IsNullOrEmpty(timeArray[0].Trim()))
                    {
                        hour = Convert.ToInt32(timeArray[0]);
                    }
                    else
                    {
                        hour = 0;
                    }
                    if (!String.IsNullOrEmpty(timeArray[1].Trim()))
                    {
                        minute = Convert.ToInt32(timeArray[1]);
                    }
                    else
                    {
                        minute = 0;
                    }
                    decimal decimalOfMin = 0;
                    if (minute > 0) // Added conditon to avoid divide by zero error.
                        decimalOfMin = minute / 60;

                    decimalValue = hour + decimalOfMin;
                }
                else
                    decimalValue = Convert.ToDecimal(valueTobeConverted);
            }
            return decimalValue;
        }

        public static Decimal GetDecimalFormattedTime(AuthenticationService.FPSettings fpSettings, double valueTobeConverted)
        {
            return GetDecimalFormattedTime(fpSettings, valueTobeConverted.ToString());
        }

       
        public string Preflight_ConvertMinstoTenths(string time, decimal TimeDisplayTenMin)
        {
            string result = "0.0";
            if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TimeDisplayTenMin)
            {
                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);
                    decimal decimalOfMin = 0;
                    if (minute > 0)
                        decimalOfMin = minute / 60;
                    result = Convert.ToString(hour + decimalOfMin);
                    result = Convert.ToString(Math.Round(Convert.ToDecimal(result), 3));
                }
            }
            else
                result = time;
            return result;
        }

        public string Preflight_ConvertTenthsToMins(string time, decimal TimeDisplayTenMin)
        {
            string result = "00:00";            
            if (!string.IsNullOrEmpty(time))
            {
                if (TenthMinuteFormat.Minute == (TenthMinuteFormat)TimeDisplayTenMin)
                {
                    TimeSpan tempObj = TimeSpan.FromHours(double.Parse(time));
                    return tempObj.ToString().Substring(0, 5);
                }
                else
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    result = Math.Round(Convert.ToDecimal(time), 1).ToString();
                }
            }
            return result;
        }
    }
}