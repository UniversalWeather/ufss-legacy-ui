﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.XPath;

namespace FlightPak.Web.Framework.Helpers
{
    public class XmlReaderResolver
    {
         #region Memebrs
        string Path = "";
        #endregion

        #region Constractor


        public XmlReaderResolver(string localPath)
        {
            Path = localPath;
        }
        #endregion

        #region Methods


        public XmlReader XmlCleanReader()
        {
            FileValidators fileValidate = new FileValidators();
            var pathValidate = fileValidate.CleanFullPath(Path);
            if (File.Exists(pathValidate) && pathValidate.ToLower().Contains("xml"))
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Prohibit;
                settings.XmlResolver = new CustomUrlResovlerReports();
                XmlReader reader = XmlReader.Create(Path, settings);

                return reader;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Get Xml file permission Key
        /// </summary>
        /// <param name="SearchFieldDoc"></param>
        /// <returns></returns>
        public string GetXmlFilePermissionKey(XPathDocument SearchFieldDoc)
        {
            XPathNavigator n = SearchFieldDoc.CreateNavigator();
            n.MoveToChild(XPathNodeType.Element); // will succeed because any XML document has a root element
            string attribute = n.GetAttribute("UMPermissionRole", string.Empty);
            return attribute;
        }


      

        #endregion
    }
}