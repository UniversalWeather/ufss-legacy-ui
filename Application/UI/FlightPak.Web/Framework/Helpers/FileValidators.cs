﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;

namespace FlightPak.Web.Framework.Helpers
{
    public class FileValidators
    {
        #region Memebrs
        #endregion

        #region Constractor


        public FileValidators()
        {

        }
        #endregion

        #region Methods
        /// <summary>
        /// Clean Full path
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        public string CleanFullPath(string path)
        {
            var validPath = Path.GetDirectoryName(path);
            var validFileName = Path.GetFileName(path);

            validPath = CleanPathOnly(validPath);
            validFileName = CleanFileNameOnly(validFileName);

            return Path.Combine(validPath, validFileName);
        }


        /// <summary>
        /// Clean File Name from Invalid Char
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string CleanFileNameOnly(string fileName)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            fileName = r.Replace(fileName, "");

            return fileName;
        }


        /// <summary>
        /// Clean PAth Name from Invalid Char
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string CleanPathOnly(string filePath)
        {
            string regexSearch = new string(Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            filePath = r.Replace(filePath, "");

            return filePath;
        }

        #endregion
    }
}