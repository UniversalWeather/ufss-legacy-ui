﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;


namespace FlightPak.Web.Framework.Helpers
{
    public class CoreApiManager
    {

        public static void SetApiCredentials(string access_token, String refresh_token, long expiryMinutes)
        {
            HttpContext.Current.Session["access_token"] = access_token;
            HttpContext.Current.Session["refreshtoken"] = refresh_token;
            HttpContext.Current.Response.Cookies["CoreApiToken"]["access_token"] = access_token;
            HttpContext.Current.Response.Cookies["CoreApiToken"]["refresh_token"] = refresh_token;
            HttpContext.Current.Response.Cookies["CoreApiToken"].Expires = DateTime.Now.AddMinutes(expiryMinutes);
            return;
        }

        public static String GetApiAccessToken()
        {
            if (HttpContext.Current.Session["access_token"] != null && !String.IsNullOrEmpty(HttpContext.Current.Session["access_token"].ToString()))
                return HttpContext.Current.Session["access_token"].ToString();

            if (HttpContext.Current.Request.Cookies["CoreApiToken"]["access_token"] != null)
                return HttpContext.Current.Request.Cookies["CoreApiToken"]["access_token"];

            return "";
        }
    }
}