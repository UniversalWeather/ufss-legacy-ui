﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.SessionState;

namespace FlightPak.Web.Framework.Helpers
{
    public class RadGridSearchPrerenderHelper
    {
        public const String GridFilterSessionName = "filter";
        public static void PrepareTextFilterSearchBox(String filterSessionName, RadGrid grid, HttpSessionState Session)
        {
            if (Session != null && Session[filterSessionName] != null && grid.MasterTableView.GetItems(GridItemType.FilteringItem) != null && grid.MasterTableView.GetItems(GridItemType.FilteringItem).Count() > 0 )
            {
                GridFilteringItem filteringItem = (GridFilteringItem)grid.MasterTableView.GetItems(GridItemType.FilteringItem)[0];
                string ColumnUniqueName = Session[filterSessionName].ToString();
                if (!String.IsNullOrEmpty(ColumnUniqueName))
                {
                    TextBox box = filteringItem[ColumnUniqueName].Controls[0] as TextBox;
                    if (box != null)
                    {
                        box.Attributes.Add("onFocus", "prepareSearchInput(this)");
                        box.Focus();
                    }
                }
                Session.Remove(filterSessionName);
            }
        }
    }
}