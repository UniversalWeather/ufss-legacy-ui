﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using FlightPak.Web.PostflightService;
using System.Collections;
using FlightPak.Web.ViewModels;

namespace FlightPak.Web.Framework.Helpers
{
    public static class PreflightExtensions
    {
        public static bool IsLegDutyEnd(this PreflightLegViewModel LegtoUpdate)
        {
            return LegtoUpdate != null && LegtoUpdate.IsDutyEnd.HasValue && LegtoUpdate.IsDutyEnd.Value;
        }

        public static PreflightLegViewModel AddCrewDutyRuleToLeg(this PreflightLegViewModel LegtoUpdate, long CrewDutyRuleId, string CrewDutyCD, string CrewDutyDesc, string FARRule)
        {
            CrewDutyRuleViewModel CrewDtyRule = new CrewDutyRuleViewModel();
            if (CrewDutyRuleId != 0)
            {
                CrewDtyRule.CrewDutyRuleCD = CrewDutyCD;
                CrewDtyRule.CrewDutyRulesID = CrewDutyRuleId;
                CrewDtyRule.CrewDutyRulesDescription = CrewDutyDesc;
                LegtoUpdate.CrewDutyRule = CrewDtyRule;
                LegtoUpdate.CrewDutyRulesID = CrewDutyRuleId;
                LegtoUpdate.FedAviationRegNUM = System.Web.HttpUtility.HtmlDecode(FARRule);
            }
            else
            {
                LegtoUpdate.CrewDutyRule = null;
                LegtoUpdate.CrewDutyRulesID = null;
            }

            return LegtoUpdate;
        }

        public static List<PreflightService.PreflightLeg> GetLiveLegs(this PreflightService.PreflightMain Trip)
        {
            if (Trip !=null && Trip.PreflightLegs != null)
                return Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(p => p.LegNUM).ToList();
            else
                return new List<PreflightService.PreflightLeg>();
        }

        public static PreflightService.PreflightMain DeletePassengerFromLeg(this PreflightService.PreflightMain Trip, int Leg)
        {
            if (Trip.PreflightLegs != null && Trip.PreflightLegs[Leg - 1].PreflightPassengerLists != null)
            {
                int oldSet = 0;
                oldSet = (int)Trip.PreflightLegs[Leg - 1].PassengerTotal;
                foreach (PreflightService.PreflightPassengerList PrePass in Trip.PreflightLegs[Leg - 1].PreflightPassengerLists.ToList())
                {
                    if (PrePass.PreflightPassengerListID != 0)
                    {
                        PrePass.FlightPurposeID = 0;
                        PrePass.IsDeleted = true;
                        PrePass.State = PreflightService.TripEntityState.Deleted;
                    }
                    else
                    {
                        Trip.PreflightLegs[Leg - 1].PreflightPassengerLists.Remove(PrePass);
                    }
                }
                Trip.PreflightLegs[Leg - 1].PassengerTotal = 0;
                Trip.PreflightLegs[Leg - 1].ReservationAvailable = Trip.PreflightLegs[Leg - 1].ReservationAvailable + oldSet;
            }
            return Trip;
        }

        public static decimal PowerSettingNullCheck(this FlightPakMasterService.Aircraft retAircraft, decimal? PowerValue, int FractionalDigit)
        {
            return retAircraft != null ? PowerValue == null ? 0 : Math.Round((decimal)PowerValue, FractionalDigit) : 0;
        }

        public static int GetPreflightLegIndex(this PreflightTripViewModel Trip, string LegNum)
        {
            long _legNum = 0;
            long.TryParse(LegNum, out _legNum);
            PreflightLegViewModel objPreflightLegViewModel = Trip.PreflightLegs.FirstOrDefault(p => p.LegNUM.Value == _legNum && p.IsDeleted == false);
            return Trip.PreflightLegs.IndexOf(objPreflightLegViewModel);
        }
    }
}