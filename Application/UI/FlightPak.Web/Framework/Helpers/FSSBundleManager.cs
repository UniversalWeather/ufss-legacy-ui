﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FlightPak.Web.Framework.Helpers
{
    public class FSSBundleManager
    {
        public static void CreatePreflightScriptsAndStyles()
        {
            #region JQGrid Style Bundles
            var cssPreflightBundle = new StyleBundle("~/bundles/jqgrid");
            cssPreflightBundle.Include("~/Scripts/jqgrid/jqgrid.css");
            cssPreflightBundle.Include("~/Scripts/jqgrid/jquery-ui.min.css");
            
            cssPreflightBundle.Include("~/Scripts/styleJqgrid.css");
            cssPreflightBundle.Include("~/Scripts/jquery.alerts.css");
            cssPreflightBundle.Include("~/Scripts/jquerytimepicker/jquery.timepicker.css");

            BundleTable.Bundles.Add(cssPreflightBundle);

            #endregion JQGrid Style Bundles

            #region Jquery Knockout Script Bundles
            var scriptJQueryBundle = new ScriptBundle("~/bundles/jquery");
            scriptJQueryBundle.Orderer = new AsIsBundleOrderer();
            scriptJQueryBundle.Include("~/Scripts/jqgrid/jquery-1.11.0.min.js")
                               .Include("~/Scripts/scroll_toolbar.js")
                               .Include("~/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.js")
                               .Include("~/Scripts/jquery.alerts.js")
                               .Include("~/Scripts/knockout/moment.min.js");
            BundleTable.Bundles.Add(scriptJQueryBundle);

            var scriptJQGridBundle = new ScriptBundle("~/bundles/jqgridjs");
            scriptJQGridBundle.Include("~/Scripts/Common.js");
            scriptJQGridBundle.Include("~/Scripts/jqgrid/i18n/grid.locale-en.js");
            scriptJQGridBundle.Include("~/Scripts/jqgrid/jquery.jqGrid.src.js");
            BundleTable.Bundles.Add(scriptJQGridBundle);

            var scriptCommonSiteBundle = new ScriptBundle("~/bundles/sitecommon");
            scriptCommonSiteBundle.Include("~/Scripts/Common.js");
            scriptCommonSiteBundle.Include("~/Scripts/jquery-migrate-1.0.0.js");
            scriptCommonSiteBundle.Include("~/Scripts/jquery.cookie.js");
            BundleTable.Bundles.Add(scriptCommonSiteBundle);

            var scriptKnockoutBundle = new ScriptBundle("~/bundles/knockout");
            scriptKnockoutBundle.Include("~/Scripts/knockout/knockout.js");
            scriptKnockoutBundle.Include("~/Scripts/knockout/knockout.mapping.js");
            scriptKnockoutBundle.Include("~/Scripts/knockout/knockout.reactor-beta.js");
            BundleTable.Bundles.Add(scriptKnockoutBundle);
            #endregion Jquery Knockout Script Bundles

            #region Preflight Script Bundles
            var scriptPreflightBundle = new ScriptBundle("~/bundles/preflight");
            scriptPreflightBundle.Include("~/Scripts/CommonValidator.js");
            scriptPreflightBundle.Include("~/Scripts/PreflightValidation.js");
            scriptPreflightBundle.Include("~/Scripts/jquery.jscrollpane.min.js");
            scriptPreflightBundle.Include("~/Scripts/Preflight/PreflightTripManagerLite.js");
            scriptPreflightBundle.Include("~/Scripts/Preflight/PreflightLeg.js");
            
            scriptPreflightBundle.Include("~/Scripts/Preflight/Preflightglobal.js");
            scriptPreflightBundle.Include("~/Scripts/Preflight/PreflightCommon.js");
            BundleTable.Bundles.Add(scriptPreflightBundle);

            var scriptPreflightOutboundBundle = new ScriptBundle("~/bundles/preflightOutbound");
            scriptPreflightOutboundBundle.Include("~/Scripts/Preflight/PreflightLegOutboundInstruction.js");
            BundleTable.Bundles.Add(scriptPreflightOutboundBundle);

            var scriptPreflightChecklistBundle = new ScriptBundle("~/bundles/preflightChecklist");
            scriptPreflightChecklistBundle.Include("~/Scripts/Preflight/PreflightLegChecklist.js");
            BundleTable.Bundles.Add(scriptPreflightChecklistBundle);

            var scriptPreflightChecklistGroupBundle = new ScriptBundle("~/bundles/preflightChecklistGroup");
            scriptPreflightChecklistGroupBundle.Include("~/Scripts/Preflight/PreflightTripManagerCheckListGroup.js");
            BundleTable.Bundles.Add(scriptPreflightChecklistGroupBundle);

            var scriptPreflightMainBundle = new ScriptBundle("~/bundles/preflightMain");
            scriptPreflightMainBundle.Include("~/Scripts/Preflight/preflightmain_page.js");
            BundleTable.Bundles.Add(scriptPreflightMainBundle);

            var scriptPreflightLegBundle = new ScriptBundle("~/bundles/preflightLeg");
            scriptPreflightLegBundle.Include("~/Scripts/Preflight/preflightleg_page.js");
            scriptPreflightLegBundle.Include("~/Scripts/jquerytimepicker/jquery.timepicker.js");
            scriptPreflightLegBundle.Include("~/Scripts/jquery.maskedinput.min.js");
            BundleTable.Bundles.Add(scriptPreflightLegBundle);

            var scriptPreflightLogisticsBundle = new ScriptBundle("~/bundles/preflightLogistics");
            scriptPreflightLogisticsBundle.Include("~/Scripts/Preflight/PreflightLogistics.js");
            scriptPreflightLogisticsBundle.Include("~/Scripts/Preflight/preflightlogistics_page.js");
            scriptPreflightLogisticsBundle.Include("~/Scripts/jquery.maskedinput.min.js");
            BundleTable.Bundles.Add(scriptPreflightLogisticsBundle);

            var scriptPreflightPaxBundle = new ScriptBundle("~/bundles/preflightpax");
            scriptPreflightPaxBundle.Include("~/Scripts/Preflight/PreflightPax.js");
            scriptPreflightPaxBundle.Include("~/Scripts/Preflight/preflightpax_page.js");
            scriptPreflightPaxBundle.Include("~/Scripts/jquery.maskedinput.min.js");
            BundleTable.Bundles.Add(scriptPreflightPaxBundle);

            var scriptPreflightCrewBundle = new ScriptBundle("~/bundles/preflightcrews");
            scriptPreflightCrewBundle.Include("~/Scripts/Preflight/PreflightCrew.js");
            scriptPreflightCrewBundle.Include("~/Scripts/Preflight/PreflightCrewInfo_page.js");
            BundleTable.Bundles.Add(scriptPreflightCrewBundle);
            #endregion Preflight Script Bundles

            var scriptBootStrapScriptBundle = new ScriptBundle("~/bundles/BootStrapScriptBundle");
            scriptBootStrapScriptBundle.Orderer = new AsIsBundleOrderer();
            scriptBootStrapScriptBundle.Include("~/Scripts/bootstrap/FSS_Customized_bootstrap.js");
            scriptBootStrapScriptBundle.Include("~/Scripts/bootstrap/bootstrap.js");
            scriptBootStrapScriptBundle.Include("~/Scripts/bootstrap/bootbox.js");
            BundleTable.Bundles.Add(scriptBootStrapScriptBundle);
            
            var scriptBootStrapPopupCssBundle = new StyleBundle("~/bundles/BootStrapPopupCssBundle");
            scriptBootStrapPopupCssBundle.Include("~/Scripts/ResponsiveStyle/responsive_bootstrap.css");
            scriptBootStrapPopupCssBundle.Include("~/Scripts/bootstrap/bootstrap.min.css");
            BundleTable.Bundles.Add(scriptBootStrapPopupCssBundle);


            #region Select2 autocomplete for FleetprofileCatalog

            var scriptFPCSelect2 = new ScriptBundle("~/bundles/scriptFleetProfileCatalogSelect2");
            scriptFPCSelect2.Include("~/Scripts/Common.js");
            scriptFPCSelect2.Include("~/Scripts/jqgrid/jquery-1.11.0.min.js");
            scriptFPCSelect2.Include("~/Scripts/SelectAutoComplete/select2.min.js");
            scriptFPCSelect2.Include("~/Scripts/scroll_toolbar.js");
            scriptFPCSelect2.Include("~/Scripts/SelectAutoComplete/Select2CustomPlugin.js");
            BundleTable.Bundles.Add(scriptFPCSelect2);

            var StyleFPCSelect2 = new StyleBundle("~/bundles/StyleFleetProfileCatalogSelect2");
            StyleFPCSelect2.Include("~/Scripts/SelectAutoComplete/select2.css");
            StyleFPCSelect2.Include("~/Scripts/SelectAutoComplete/flag-icon.min.css");
            BundleTable.Bundles.Add(StyleFPCSelect2);

            var scriptFPCSelect3 = new ScriptBundle("~/bundles/scriptFleetProfileCatalogSelect3");
            scriptFPCSelect3.Include("~/Scripts/jqgrid/jquery-1.11.0.min.js");
            scriptFPCSelect3.Include("~/Scripts/Common.js");
            scriptFPCSelect3.Include("~/Scripts/bootstrap/FSS_Customized_bootstrap.js");
            scriptFPCSelect3.Include("~/Scripts/bootstrap/bootstrap.js");
            scriptFPCSelect3.Include("~/Scripts/bootstrap/bootbox.js");
            BundleTable.Bundles.Add(scriptFPCSelect3);

            var scriptFPCSelect4 = new ScriptBundle("~/bundles/scriptFleetProfileCatalogSelect4");
            scriptFPCSelect4.Include("~/Scripts/SelectAutoComplete/select2.min.js");
            scriptFPCSelect4.Include("~/Scripts/SelectAutoComplete/Select2CustomPlugin.js");
            BundleTable.Bundles.Add(scriptFPCSelect4);




            #endregion



            #region  Postflight bundles

            var scriptPostflightBundle = new ScriptBundle("~/bundles/postflight");
            scriptPostflightBundle.Include("~/Scripts/CommonValidator.js");
            scriptPostflightBundle.Include("~/Scripts/Postflight/PostflightValidation.js");
            scriptPostflightBundle.Include("~/Scripts/Postflight/PostflightTripManagerLite.js");
            scriptPostflightBundle.Include("~/Scripts/Postflight/Postflightglobal.js");
            scriptPostflightBundle.Include("~/Scripts/Postflight/PostflightCommon.js");
            BundleTable.Bundles.Add(scriptPostflightBundle);

            var scriptPostflightMainBundle = new ScriptBundle("~/bundles/postflightmain");
            scriptPostflightMainBundle.Include("~/Scripts/Postflight/PostflightMain_page.js");
            BundleTable.Bundles.Add(scriptPostflightMainBundle);

            var scriptPostflightPaxBundle = new ScriptBundle("~/bundles/postflightpax");
            scriptPostflightPaxBundle.Include("~/Scripts/Postflight/PostflightPax.js");            
            scriptPostflightPaxBundle.Include("~/Scripts/Postflight/PostflightPax_page.js");            
            BundleTable.Bundles.Add(scriptPostflightPaxBundle);

            var scriptPostflightExpensesBundle = new ScriptBundle("~/bundles/postflightexpenses");
            scriptPostflightExpensesBundle.Include("~/Scripts/Postflight/PostflightExpense_page.js");
            scriptPostflightExpensesBundle.Include("~/Scripts/Postflight/PostflightExpenses.js");
            BundleTable.Bundles.Add(scriptPostflightExpensesBundle);

            var scriptPostflightLegsBundle = new ScriptBundle("~/bundles/postflightlegs");
            scriptPostflightLegsBundle.Include("~/Scripts/Postflight/PostflightLeg.js");
            scriptPostflightLegsBundle.Include("~/Scripts/Postflight/PostflightLeg_page.js");
            scriptPostflightLegsBundle.Include("~/Scripts/jquerytimepicker/jquery.timepicker.js");
            scriptPostflightLegsBundle.Include("~/Scripts/jquery.maskedinput.min.js");
            BundleTable.Bundles.Add(scriptPostflightLegsBundle);

            var scriptPostflightOtherCrewDutyLogBundle = new ScriptBundle("~/bundles/postflightOtherCrewDutyLog");
            scriptPostflightOtherCrewDutyLogBundle.Include("~/Scripts/Postflight/PostflightOtherCrewDutyLog.js");
            scriptPostflightOtherCrewDutyLogBundle.Include("~/Scripts/Postflight/PostflightOtherCrewDutyLog_page.js");
            scriptPostflightOtherCrewDutyLogBundle.Include("~/Scripts/jquerytimepicker/jquery.timepicker.js");
            scriptPostflightOtherCrewDutyLogBundle.Include("~/Scripts/jquery.maskedinput.min.js");
            BundleTable.Bundles.Add(scriptPostflightOtherCrewDutyLogBundle);

            var scriptPostflightExpenseCatalogBundle = new ScriptBundle("~/bundles/postflightExpenseCatalog");
            scriptPostflightExpenseCatalogBundle.Include("~/Scripts/Postflight/PostflightExpenseCatalog.js");
            scriptPostflightExpenseCatalogBundle.Include("~/Scripts/Postflight/PostflightExpenseCatalog_page.js");
            BundleTable.Bundles.Add(scriptPostflightExpenseCatalogBundle);

            var scriptPreflightSearchRetrieveBundle = new ScriptBundle("~/bundles/PreflightSearchRetrieve");
            scriptPreflightSearchRetrieveBundle.Include("~/Scripts/Postflight/PreflightSearchRetrieve.js");
            BundleTable.Bundles.Add(scriptPreflightSearchRetrieveBundle);

            var scriptPostflightCrewBundle = new ScriptBundle("~/bundles/postflightcrew");
            scriptPostflightCrewBundle.Include("~/Scripts/Postflight/PostflightCrew.js");
            scriptPostflightCrewBundle.Include("~/Scripts/Postflight/PostflightCrew_page.js");
            scriptPostflightCrewBundle.Include("~/Scripts/jquerytimepicker/jquery.timepicker.js");
            scriptPostflightCrewBundle.Include("~/Scripts/jquery.maskedinput.min.js");
            BundleTable.Bundles.Add(scriptPostflightCrewBundle);
            #endregion

            var scriptBootStrapParentPageCssBundle = new StyleBundle("~/bundles/BootStrapParentPageCssBundle");
            scriptBootStrapParentPageCssBundle.Include("~/Scripts/ResponsiveStyle/responsive_bootstrap.css");
            scriptBootStrapParentPageCssBundle.Include("~/Scripts/bootstrap/bootstrap.css");
            BundleTable.Bundles.Add(scriptBootStrapParentPageCssBundle);

            var scriptBootStrapOnlyResponsiveCss = new StyleBundle("~/bundles/BootStrapOnlyResponsiveCss");
            scriptBootStrapOnlyResponsiveCss.Include("~/Scripts/ResponsiveStyle/responsive_bootstrap.css");
            BundleTable.Bundles.Add(scriptBootStrapOnlyResponsiveCss);

            var scriptJqueryScroll = new ScriptBundle("~/bundles/jqueryScrollBundle");
            scriptJqueryScroll.Include("~/Scripts/jquery.jscrollpane.min.js");
            BundleTable.Bundles.Add(scriptJqueryScroll);
            var pdfViewerCss = new StyleBundle("~/PDFLauncher/web/PDFViewerCss");
            pdfViewerCss.Include("~/PDFLauncher/web/viewer.css");
            BundleTable.Bundles.Add(pdfViewerCss);

            var pdfViewerJs = new ScriptBundle("~/bundles/PDFViewerJs");
            pdfViewerJs.Include("~/Scripts/jquery.js");
            pdfViewerJs.Include("~/PDFLauncher/web/compatibility.js");
            pdfViewerJs.Include("~/PDFLauncher/web/l10n.js");
            pdfViewerJs.Include("~/PDFLauncher/main/pdf.js");
            pdfViewerJs.Include("~/PDFLauncher/main/pdf.worker.js");
            pdfViewerJs.Include("~/PDFLauncher/web/debugger.js");
            pdfViewerJs.Include("~/PDFLauncher/web/viewer.js");

            BundleTable.Bundles.Add(pdfViewerJs);
        }

    }
}