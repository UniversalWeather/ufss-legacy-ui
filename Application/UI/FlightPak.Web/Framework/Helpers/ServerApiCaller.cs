﻿using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Views.Utilities;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace FlightPak.Web.Framework.Helpers
{
    public class ServerApiCaller
    {

        #region Memebrs
        public readonly string strApiUrl = "";
        private ExceptionManager exManager;
        private CoreApiManager coreApiManager;
        readonly string responseString;
        #endregion

        #region Constractor
        public ServerApiCaller()
        {
            String coreApiUrl = ConfigurationManager.AppSettings["CoreApiServer"];
            if (String.IsNullOrEmpty(coreApiUrl))
                coreApiUrl = "http://core-dev.universalweather.rdn/";
            
            strApiUrl = string.Format("{0}/api/v1/{1}", coreApiUrl, "{0}/{1}{2}");
        }

        public ServerApiCaller(string dataObject)
        {
            responseString = dataObject;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Call API Method
        /// </summary>
        /// <param name="method">Method Name</param>
        /// <param name="Type">API Type</param>
        /// <param name="paramters">paramters in QueryString Format</param>
        /// <returns></returns>
        public string CallAPI(string method,string Type,string paramters)
        {
            string resultValue = "";
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                ServerApiCaller getApiObj;
                String apiType = Type;
                String apiMethod = method;
                StringBuilder sbParams = new StringBuilder();
                sbParams.AppendFormat("?{0}", paramters);
               

                String queryStringParams = sbParams.ToString();
                if (queryStringParams.EndsWith("&"))
                    queryStringParams = queryStringParams.Remove(queryStringParams.Length - 1);

                String strUrl = String.Format(strApiUrl, apiType, apiMethod, queryStringParams);

                WebRequest webRequest = HttpWebRequest.Create(strUrl);
                String accessToken = CoreApiManager.GetApiAccessToken();
                webRequest.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + accessToken);

                try
                {
                    using (var webResponse = webRequest.GetResponse())
                    {
                        var responseStream = webResponse.GetResponseStream();

                        using (StreamReader srResponse = new StreamReader(responseStream))
                        {
                            getApiObj = new ServerApiCaller(srResponse.ReadToEnd());
                        }
                    }

                    resultValue= Microsoft.Security.Application.Encoder.HtmlEncode(getApiObj.responseString);
                }
                catch (WebException webEx)
                {
                    getApiObj = new ServerApiCaller(string.Format("Failed to execute api {0} for {1}", apiMethod, apiType));
                    if (((System.Net.HttpWebResponse)(webEx.Response)).StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Dictionary<string, object> result = new Dictionary<string, object>();
                        result.Add("StatusCode", 401);
                        getApiObj = new ServerApiCaller(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    }
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.UrlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(webEx.ToString())), WebConstants.FlightPakException);
                }
                catch (Exception ex)
                {
                    getApiObj = new ServerApiCaller(string.Format("Failed to execute api {0} for {1}", apiMethod, apiType));
                    Logger.Write(String.Format("Url:{0}-Token:{1}-Exception:{2}", Microsoft.Security.Application.Encoder.UrlEncode(strUrl), Microsoft.Security.Application.Encoder.HtmlEncode(accessToken), Microsoft.Security.Application.Encoder.HtmlEncode(ex.ToString())), WebConstants.FlightPakException);
                }
                return Microsoft.Security.Application.Encoder.HtmlEncode(getApiObj.responseString);
            }, FlightPak.Common.Constants.Policy.UILayer);
            return resultValue;
        }

        #endregion
    }
}