﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Helpers.Validators
{
    public class AircraftDetails
    {
        public long AirCraftId { get; set; }
        public double PowerSettings1TakeOffBias { get; set; }
        public double PowerSettings1LandingBias { get; set; }
        public double PowerSettings1TrueAirSpeed { get; set; }
        public double PowerSettings2TakeOffBias { get; set; }
        public double PowerSettings2LandingBias { get; set; }
        public double PowerSettings2TrueAirSpeed { get; set; }
        public double PowerSettings3TakeOffBias { get; set; }
        public double PowerSettings3LandingBias { get; set; }
        public double PowerSettings3TrueAirSpeed { get; set; }
        public string IsFixedRotary { get; set; }
        public double aircraftWindAltitude { get; set; }
        public double ChargeRate { get; set; }
        public string ChargeUnit { get; set; }
        public decimal Distance { get; set; }
        public string Bias { get; set; }
        public string LandingBias { get; set; }
        public string TAS {get;set; }
        public string ETE { get; set; }
        public double Wind { get; set; }
    }
}