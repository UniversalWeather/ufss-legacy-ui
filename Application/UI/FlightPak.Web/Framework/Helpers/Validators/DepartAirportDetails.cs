﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Helpers.Validators
{
    public class DepartAirportDetails
    {
        public double deplatdeg { get; set; }
        public double deplatmin { get; set; }
        public string lcdeplatdir { get; set; }
        public double deplngdeg { get; set; }
        public double deplngmin { get; set; }
        public string lcdeplngdir { get; set; }
        public bool? depIsDayLightSaving { get; set; }
        public DateTime? depDayLightSavingStartDT { get; set; }
        public DateTime? depDayLightSavingEndDT { get; set; }
        public string depDaylLightSavingStartTM { get; set; }
        public string depDayLightSavingEndTM { get; set; }
        public decimal? depOffsetToGMT { get; set; }
        public long lnDepartWindZone { get; set; }
        public double DepAirportTakeoffBIAS { get; set; }
        public string DepID  { get; set; }
        public string DepCD { get; set; }
    }
}