﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Helpers.Validators
{
    public class LegDateTime
    {
        public string DepLocalDate { get; set; }
        public string DepLocalTime { get; set; }
        public string DepUtcDate { get; set; }
        public string DepUtcTime { get; set; }
        public string DepHomeDate { get; set; }
        public string DepHomeTime { get; set; }
        public string ArrLocalDate { get; set; }
        public string ArrLocalTime { get; set; }
        public string ArrUtcDate { get; set; }
        public string ArrUtcTime { get; set; }
        public string ArrHomeDate { get; set; }
        public string ArrHomeTime { get; set; }
        public string Changed { get; set; }
    }
}