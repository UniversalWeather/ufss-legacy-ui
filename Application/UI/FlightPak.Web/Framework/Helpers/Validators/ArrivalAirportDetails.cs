﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Helpers.Validators
{
    public class ArrivalAirportDetails
    {
        public double arrlatdeg { get; set; }
        public double arrlatmin { get; set; }
        public string lcArrLatDir { get; set; }
        public double arrlngdeg { get; set; }
        public double arrlngmin { get; set; }
        public string lcarrlngdir { get; set; }
        public bool? arrIsDayLightSaving { get; set; }
        public DateTime? arrDayLightSavingStartDT { get; set; }
        public DateTime? arrDayLightSavingEndDT { get; set; }
        public string arrDaylLightSavingStartTM { get; set; }
        public string arrDayLightSavingEndTM { get; set; }
        public decimal? arrOffsetToGMT { get; set; }
        public long lnArrivWindZone { get; set; }
        public double ArrAirportLandingBIAS { get; set; }
        public string ArrivalID { get; set; }
        public string ArrivalCD { get; set; }
    }
}