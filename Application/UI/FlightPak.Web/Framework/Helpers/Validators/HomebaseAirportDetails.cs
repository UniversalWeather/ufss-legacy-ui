﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Helpers.Validators
{
    public class HomebaseAirportDetails
    {
        public bool? homeIsDayLightSaving { get; set; }
        public DateTime? homeDayLightSavingStartDT { get; set; }
        public DateTime? homeDayLightSavingEndDT { get; set; }
        public string homeDaylLightSavingStartTM { get; set; }
        public string homeDayLightSavingEndTM { get; set; }
        public decimal? homeOffsetToGMT { get; set; }
        public string HomebaseId { get; set; }
        public string HomebaseCD { get; set; }
    }
}