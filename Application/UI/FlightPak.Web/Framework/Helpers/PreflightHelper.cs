﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace FlightPak.Web.Framework.Helpers.Preflight
{
    [Serializable]
    public class PreflightHelper
    {
    }

    /// <summary>
    /// Maintain Passengers associated with Legs
    /// </summary>
    [Serializable]
    public class PassengerInLegs
    {       
        public long PassengerRequestorID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }  
        public string Street { get; set; }
        public string Postal { get; set; }
        public string BillingCode { get; set; }
        public string Passport { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public string PassportID { get; set; }
        public string VisaID { get; set; }
        public string Purpose { get; set; }
        public string Notes { get; set; }
        public string PassengerAlert { get; set; }
        public string Nation { get; set; }
        public string PassportExpiryDT { get; set; }
        public List<PaxLegInfo> Legs { get; set; }
        public string DateOfBirth { get; set; }
        public Int32 OrderNUM { get; set; }

        public long? FlightPurposeID { get; set; }
    }

    /// <summary>
    /// Maintain Leg Info related to Passenger In Legs
    /// </summary>
    [Serializable]
    public class PaxLegInfo
    {       
        public long LegID { get; set; }
        public string LegDesc { get; set; }
        public int LegOrder { get; set; }
        public long PaxID { get; set; }
        public long DepartICAOID { get; set; }
        public string Depart { get; set; }
        public long ArriveICAOID { get; set; }
        public string Arrive { get; set; }
        public long FlightPurposeID { get; set; }
        public string FlightPurposeCD { get; set; }
        public bool IsNonPassenger { get; set; }
        public string PassportNo { get; set; }
        public DateTime PassportExpiryDt { get; set; }
        public string BillingCode { get; set; }
        public string Nation { get; set; }
        public string WaitList { get; set; }
        public int PaxCount { get; set; }
        public int AvailableCount { get; set; }
        public DateTime DateOfBirth { get; set; }

    }

    /// <summary>
    /// Maintain Passenger Summary for PostflightPAX
    /// </summary>    
    [Serializable]
    public class PassengerSummary
    {        
        public long? LegId { get; set; }
        public long? PaxID { get; set; }
        public string PaxCode { get; set; }
        public string PaxName { get; set; }        
        public string Street { get; set; }
        public string Postal { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Purpose { get; set; }       
        public bool IsNonPassenger { get; set; } 
        public string EmpType { get; set; }
        public string LegDescription { get; set; }
        public string WaitList { get; set; }
        public int OrderNum { get; set; }
        public string BillingCode { get; set; }
        public string Passport { get; set; }
        public string PassportID { get; set; }
        public string VisaID { get; set; }
        public string Nation { get; set; }
        public string PassportExpiryDT { get; set; }
        public string DateOfBirth { get; set; }
        public long? FlightPurposeID { get; set; }
    }

    public enum TenthMinuteFormat
    {
        Tenth = 1,
        Minute = 2,
        Other = 0
    }
}