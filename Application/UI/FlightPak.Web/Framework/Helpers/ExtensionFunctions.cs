﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Helpers
{
    public static class ExtensionFunctions
    {
        public static string FSSParseDateTimeString(this DateTime? datetime, string formate)
        {
            string parsedDate = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(formate))
                    parsedDate = String.Format(CultureInfo.InvariantCulture, "{0:" + formate + "}", datetime);
                else
                    parsedDate = "";
            }
            catch (Exception ex)
            {
                if (datetime.HasValue)
                    parsedDate = datetime.Value.ToString();
            }
            return parsedDate;
        }
        public static DateTime? FSSParseDateTime(this DateTime? dateTime, string departdate, string formate)
        {
            DateTime? dt = null;
            try
            {
                dt = DateTime.ParseExact(departdate, formate, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                
            }
            return dt;
        }

    }


}