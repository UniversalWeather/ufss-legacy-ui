﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using FlightPak.Web.FlightPakMasterService;
using System.Globalization;
using FlightPak.Web.Framework.Prinicipal;
using System.Text.RegularExpressions;
using FlightPak.Web.ViewModels;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace FlightPak.Web.Framework.Helpers
{
    public class MiscUtils
    {
        public static string AppendSeeMoreLink(string CrewCD, string CrewCodes, string TripId)
        {
            StringBuilder description = new StringBuilder();
            description.Append("<tr><td colspan='2'><strong>Crew: </strong>");
            if (string.IsNullOrEmpty(CrewCD))
            {
                if (Convert.ToString(CrewCodes).Length > 67)
                {
                    description.Append(Convert.ToString(CrewCodes).Substring(0, 45) + "...  <a class='seeMoreLink' onClick='SeeMoreCrew(" + TripId + ");' href='javascript:void(0)'>See more</a>");
                }
                else
                {
                    description.Append(Convert.ToString(CrewCodes));
                }
            }
            else
            {
                if (Convert.ToString(CrewCD).Length > 67)
                {
                    description.Append(Convert.ToString(CrewCD).Substring(0, 45) + "...  <a class='seeMoreLink' onClick='SeeMoreCrew(" + TripId + ");' href='javascript:void(0)'>See more</a>");
                }
                else
                {
                    description.Append(Convert.ToString(CrewCD));
                }
            }
            return description.ToString();
        }

        public static bool HaveRequiredFuelColumnsFound(FlightPak.Web.FlightPakMasterService.FuelFileData fuelData)
        {
            if ((fuelData.FBO > -1 || fuelData.Vendor > -1) && (fuelData.ICAO > -1 || fuelData.IATA > -1) && fuelData.Price > -1 && fuelData.EffectiveDate > -1 && fuelData.High > -1 && fuelData.Low > -1)
            {
                return true;
            }
            return false;
        }

        public static bool HaveRequiredFuelColumnsFound(Dictionary<string, string> fueldata)
        {
            if ((fueldata.ContainsKey("FBO") || fueldata.ContainsKey("VENDOR")) && (fueldata.ContainsKey("ICAO") || fueldata.ContainsKey("IATA"))
                && fueldata.ContainsKey("PRICE") && fueldata.ContainsKey("EFFECTIVEDATE") && fueldata.ContainsKey("HIGH") && fueldata.ContainsKey("LOW"))
            {
                return true;
            }
            return false;
        }

        public static bool isAnyFieldHaveSameValue(FuelFileData objFuelFile,int value)
        {
            if (value == objFuelFile.FBO)
                return true;
            else if (value == objFuelFile.ICAO)
                return true;
            else if (value == objFuelFile.IATA)
                return true;
            else if (value == objFuelFile.Vendor)
                return true;
            else if (value == objFuelFile.Text)
                return true;
            else if (value == objFuelFile.High)
                return true;
            else if (value == objFuelFile.Low)
                return true;
            else if (value == objFuelFile.Price)
                return true;
            else if (value == objFuelFile.EffectiveDate)
                return true;
            else
                return false;
        }
        public static FlightPak.Web.FlightPakMasterService.FuelFileData InitializeFuelFileData(FlightPak.Web.FlightPakMasterService.FuelFileData fuelData)
        {
            fuelData.FBO = -1;
            fuelData.Vendor = -1;
            fuelData.Text -= -1;
            fuelData.Price = -1;
            fuelData.Low = -1;
            fuelData.ICAO = -1;
            fuelData.IATA = -1;
            fuelData.High = -1;
            fuelData.EffectiveDate = -1;

            return fuelData;
        }

        public static string FSSParseDateTimeString(DateTime? datetime, string formate)
        {
            string parsedDate = null;
            try
            {
                if (!string.IsNullOrWhiteSpace(formate))
                    parsedDate = String.Format(CultureInfo.InvariantCulture, "{0:" + formate + "}", datetime);
                else
                    parsedDate = "";
            }
            catch (Exception ex)
            {
                if (datetime.HasValue)
                    parsedDate = datetime.Value.ToString();
            }
            return parsedDate;
        }
        public static DateTime? FSSParseDateTime(DateTime? dateTime, string departdate, string formate)
        {
            DateTime? dt = null;
            try
            {
                dt = DateTime.ParseExact(departdate, formate, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                DateTime dtTemp;
                DateTime.TryParse(departdate, out dtTemp);
                if (dtTemp != DateTime.MinValue)
                    dt = dtTemp;
            }
            return dt;
        }
        public static bool IsAuthorized(string ModuleName)
        {
            //Session Timeout fix
            if (((FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]) != null)
            {
                return ((FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID]).IsInRole(ModuleName);
            }
            return false;
        }
        public static string ColorToHexConverter(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        public static DateTime? FSSPreflightParseFormat(String strDateTime, String format)
        {
            DateTime parseDt;
            bool parseResult = DateTime.TryParseExact(strDateTime, format, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out parseDt);
            if (parseResult && parseDt != DateTime.MinValue && parseDt != DateTime.MaxValue)
                return parseDt;
            else
                return null;

        }
        public static string FormatDate(string dt,UserPrincipalViewModel upViewModel)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dt))
            {
                string FormattedDate = string.Empty;
                if (!string.IsNullOrEmpty(dt))
                {
                    DateTimeFormatInfo formatInfo = new DateTimeFormatInfo();
                    if (upViewModel._ApplicationDateFormat != null)
                        formatInfo.ShortDatePattern = upViewModel._ApplicationDateFormat;
                    else
                        formatInfo.ShortDatePattern = "MM/dd/yyyy"; // American, MDY

                    List<string> dtList = new List<string>();

                    char splitter;
                    if (dt.Contains("-"))
                        splitter = '-';
                    else if (dt.Contains("."))
                        splitter = '.';
                    else
                        splitter = '/';

                    switch (formatInfo.ShortDatePattern)
                    {
                        case "dd/MM/yyyy": // British, French, DMY
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "dd.MM.yyyy": // German
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy.MM.dd": // ANSI
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "dd-MM-yyyy": // Italian
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[0] + "/" + dtList[2];
                            break;
                        case "yyyy/MM/dd": // Japan, Taiwan, YMD
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[1] + "/" + dtList[2] + "/" + dtList[0];
                            break;
                        case "MM/dd/yyyy": // Default
                            dtList = dt.Split(splitter).ToList();
                            FormattedDate = dtList[0] + "/" + dtList[1] + "/" + dtList[2];
                            break;
                    }
                }
                return FormattedDate;
            }
        }
        public static string CalculationTimeAdjustment(string TextboxValues)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TextboxValues))
            {
                string TextboxValue = "00:00";
                if (TextboxValues != "")
                {
                    TextboxValue = TextboxValues;
                    if ((!TextboxValue.Contains('-')) && (TextboxValue.Trim() != "00:00") && (TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != ".") && (TextboxValue.Trim() != "0") && (TextboxValue.Trim() != "0:0") && (TextboxValue.Trim() != "0:00"))
                    {
                        if (TextboxValue.IndexOf(":") != -1)
                        {
                            string[] timeArray = TextboxValue.Split(':');
                            if (string.IsNullOrEmpty(timeArray[0]))
                            {
                                timeArray[0] = "00";
                                TextboxValue = timeArray[0] + ":" + timeArray[1];
                            }
                            if (!string.IsNullOrEmpty(timeArray[0]))
                            {
                                if (timeArray[0].Length == 0)
                                {
                                    timeArray[0] = "00";
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                                if (timeArray[0].Length == 1)
                                {
                                    timeArray[0] = "0" + timeArray[0];
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                            }
                            if (!string.IsNullOrEmpty(timeArray[1]))
                            {
                                Int64 result = 0;
                                if (timeArray[1].Length != 2)
                                {
                                    result = (Convert.ToInt32(timeArray[1])) / 10;
                                    if (result < 1)
                                    {
                                        if (timeArray[0] != null)
                                        {
                                            if (timeArray[0].Trim() == "")
                                            {
                                                timeArray[0] = "00";
                                            }
                                        }
                                        else
                                        {
                                            timeArray[0] = "00";
                                        }
                                        TextboxValue = timeArray[0] + ":" + "0" + timeArray[1];

                                    }
                                }
                            }
                            else
                            {
                                TextboxValue = TextboxValue + "00";
                            }
                        }
                        else
                        {
                            TextboxValue = TextboxValue + ":00";
                        }
                    }
                    else
                    {
                        TextboxValue = "00:00";
                    }
                }
                else
                {
                    TextboxValue = "00:00";
                }
                return TextboxValue;
            }
        }

        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
           
            using (Stream stream = new MemoryStream())
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static byte[] GetFormatedString(byte[] stream)
        {
            string str = Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToBase64String(stream));
            return Convert.FromBase64String(str);
        }

        public static string GetRegexExtractAlphaNumeric(String Name, int numChars)
        {
            if (string.IsNullOrWhiteSpace(Name))
                return string.Empty;

            String regex = @"[a-zA-Z0-9]+";
            Regex pattern = new Regex(regex);
            var result = Regex.Matches(Name, regex);
            StringBuilder output = new StringBuilder();
            foreach (Match mc in result)
            {
                output.Append(mc.ToString());
            }
            if (output.ToString().Length > numChars)
                return output.ToString().Substring(0, numChars);
            else
                return output.ToString();
        }
    }
    
}