﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace FlightPak.Web.Framework.Helpers
{
    [Serializable()]
    public class PostflightHelper
    {
    }

    /// <summary>
    /// Maintain Passengers associated with Legs
    /// </summary>
    [Serializable()]
    public class PassengerInLegs
    {
        public long PaxID { get; set; }
        public string PaxCode { get; set; }
        public string PaxName { get; set; }
        public string PaxFName { get; set; }
        public string PaxMName { get; set; }
        public string PaxLName { get; set; }
        public string EmpType { get; set; }
        public string AssocPaxCD { get; set; }
        public bool SIFLSecurity { get; set; }
        public int OrderNUM { get; set; }
        public List<PaxLegInfo> Legs { get; set; }
    }

    /// <summary>
    /// Maintain Leg Info related to Passenger In Legs
    /// </summary>

    [Serializable()]
    public class PaxLegInfo
    {
        public long PaxListID { get; set; }
        public long POLogID { get; set; }
        public long POLegID { get; set; }
        public string LegDesc { get; set; }
        public long OrderNUM { get; set; }
        public long DepartICAOID { get; set; }
        public string Depart { get; set; }
        public long ArriveICAOID { get; set; }
        public string Arrive { get; set; }
        public long FlightPurposeID { get; set; }
        public string FlightPurposeCD { get; set; }
        public bool IsNonPassenger { get; set; }
        public long PassportID { get; set; }
        public string PassportNo { get; set; }
        public DateTime PassportExpiryDt { get; set; }
        public string BillInfo { get; set; }
        public string Nation { get; set; }
        public string WaitList { get; set; }
        public long LegNUM { get; set; }
        public DateTime ScheduledDate { get; set; } //Bug 2588
        public Int32 DepartPercentage { get; set; } //BRM 18
    }

    /// <summary>
    /// Maintain Passenger Summary for PostflightPAX
    /// </summary>

    [Serializable()]
    public class PassengerSummary
    {
        public long PostflightPassengerListID { get; set; }
        public long POLogID { get; set; }
        public long POLegID { get; set; }
        public long PassengerID { get; set; }
        public string PaxCode { get; set; }
        public string PassengerFirstName { get; set; }
        public string PassengerMiddleName { get; set; }
        public string PassengerLastName { get; set; }
        public long DepartICAOID { get; set; }
        public string Depart { get; set; }
        public long ArriveICAOID { get; set; }
        public string Arrive { get; set; }
        public long FlightPurposeID { get; set; }
        public string FlightPurposeCD { get; set; }
        public bool IsNonPassenger { get; set; }
        public string PassportNUM { get; set; }
        public long PassportID { get; set; }
        public DateTime PassportExpiryDt { get; set; }
        public string Nation { get; set; }
        public string Billing { get; set; }
        public string EmpType { get; set; }
        public string LegDescription { get; set; }
        public string WaitList { get; set; }
        public long OrderNUM { get; set; }
        public string AssocPaxCD { get; set; }
        public long LegNUM { get; set; }
        public Int32 DepartPercentage { get; set; } //BRM 18
        public string PaxName { get; set; }
    }

    /// <summary>
    /// Method to set control access to Postflight screens
    /// </summary>

    [Serializable()]
    public class UIControlAccess
    {
        public Control ControlToSetAccess { get; set; }

        public Type ControlType { get; set; }

        public string PermissionNameToApply { get; set; }

        public FlightpakPermissions AccessLevel { get; set; }
    }

    /// <summary>
    /// Enum collection having the permissions
    /// </summary>
    [Serializable()]
    public enum FlightpakPermissions
    {
        View = 0,
        Add = 1,
        Edit = 2,
        Delete = 3
    }

    /// <summary>
    /// Class Added for Default Standard and Flightpak Conversion Setting
    /// </summary>
    [Serializable()]
    public class StandardFlightpakConversion
    {
        public decimal Tenths { get; set; }
        public int StartMinutes { get; set; }
        public int EndMinutes { get; set; }
        public int ConversionType { get; set; }
    }

    /// <summary>
    /// Class added for SIFL Personal Grid
    /// </summary>
    [Serializable()]
    public class PostflightSIFLPersonal
    {
        public long LogID { get; set; }
        public long LegID { get; set; }
        public long PaxID { get; set; }
        public long PaxListID { get; set; }
        public string EmpType { get; set; }
        public long DepartICAO { get; set; }
        public long ArriveICAO { get; set; }
        public string Depart { get; set; }
        public string Arrive { get; set; }
        public string Personal { get; set; }
        public string LoadFactor { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string PaxCode { get; set; }
        public string AssocPaxCD { get; set; }
        public long LegNUM { get; set; }
        public bool SIFLSecurity { get; set; }
        public DateTime ScheduledDate { get; set; } //Bug 2588
        public DateTime? DateOfBirth { get; set; } //Bug 2588

        public int RowNumber { get; set; }
    }

    /// <summary>
    /// Class added for SIFL selected Airport
    /// </summary>
    [Serializable()]
    public class POSIFLAirport
    {
        public long LegID { get; set; }
        public string ICAOID { get; set; }
        public string IcaoCity { get; set; }
        public long AirportID { get; set; }
        public string AirportName { get; set; }
    }

    public static class POTenthMinuteFormat
    {
        /// <summary>
        /// Tenth = 1
        /// </summary>
        public const int Tenth = 1;
        /// <summary>
        /// Minute = 2
        /// </summary>
        public const int Minute = 2;
        /// <summary>
        /// Other = 0
        /// </summary>
        public const int Other = 0;
    }

    public static class POAircraftBasis
    {
        /// <summary>
        /// BlockTime = 1
        /// </summary>
        public const int BlockTime = 1;
        /// <summary>
        /// FlightTime = 2
        /// </summary>
        public const int FlightTime = 2;
    }
}