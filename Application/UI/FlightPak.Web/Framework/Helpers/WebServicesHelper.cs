﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.Framework.Helpers
{
    public class WebServicesHelper
    {
        private static ExceptionManager exManager;
        /// <summary>
        ///  Get Authorization by Department ID and Authorization ID
        /// </summary>
        /// <param name="DepartmentID">DepartmentID</param>
        /// <param name="AuthorizationID">AuthorizationID</param>
        /// <returns>return the DepartmentAuthorization</returns>
        public static DepartmentAuthorization GetAuthorization(Int64 departmentID, Int64 authorizationID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FlightPakMasterService.DepartmentAuthorization>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(authorizationID))
                {
                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        FlightPakMasterService.DepartmentAuthorization Auth = new FlightPakMasterService.DepartmentAuthorization();
                        var objAuthorization = objDstsvc.GetDepartmentAuthorizationWithFilters(0, departmentID, authorizationID, string.Empty, false);
                        if (objAuthorization.ReturnFlag)
                        {
                            List<DepartmentAuthorization> authorizationcode =
                                objAuthorization.EntityList;
                            if (authorizationcode != null && authorizationcode.Count > 0)
                                Auth = authorizationcode[0];
                        }
                        return Auth;
                    }
                }
            }, FlightPak.Common.Constants.Policy.UILayer);
        }

        /// <summary>
        /// Get all Passenger with filter
        /// </summary>
        /// <param name="ClientId">ClientId</param>
        /// <param name="CQCustomerID">CQCustomerID</param>
        /// <param name="PassengerId">PassengerId</param>
        /// <param name="PassengerCD">PassengerCode</param>
        /// <param name="ActiveOnly">ActiveOnly</param>
        /// <param name="RequestorOnly">RequestorOnly</param>
        /// <param name="ICAOID">ICAOID</param>
        /// <param name="PaxGroupId">PaxGroupId</param>
        /// <returns>Return all the Passenger based on Filter</returns>
        public static long GetAllPassengerWithFilters(long ClientId, long CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId, out long DepartmentAuthorizationID)
        {
            long DepartmentID = 0;
            long depAuthorizationID = 0;
            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                var objRetVal = objDstsvc.GetAllPassengerWithFilters(ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId);
                if (objRetVal.ReturnFlag)
                {
                    List<GetAllPassengerWithFilters> PassengerList = objRetVal.EntityList.ToList();
                    if (PassengerList != null && PassengerList.Count > 0)
                    {
                        DepartmentID = PassengerList[0].DepartmentID == null ? 0 : (long)PassengerList[0].DepartmentID;
                        depAuthorizationID = PassengerList[0].AuthorizationID.HasValue ? PassengerList[0].AuthorizationID.Value : 0;
                    }
                }
            }
            DepartmentAuthorizationID = depAuthorizationID;
            return DepartmentID;
        }

        public FlightPak.Web.FlightPakMasterService.Client GetClient(Int64 ClientID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.Client client = new FlightPakMasterService.Client();
                    var objRetVal = objDstsvc.GetClientWithFilters(ClientID, string.Empty, false);
                    List<FlightPakMasterService.Client> ClientList = new List<FlightPakMasterService.Client>();

                    if (objRetVal.ReturnFlag)
                    {

                        ClientList = objRetVal.EntityList;
                        if (ClientList != null && ClientList.Count > 0)
                        {
                            client = ClientList[0];

                        }
                        else
                            client = null;

                    }
                    return client;
                }
            }
        }

        public FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetCompanyWithFilters Companymaster = new GetCompanyWithFilters();
                    var objCompany = objDstsvc.GetCompanyWithFilters(string.Empty, HomeBaseID, true, true);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters> Company = objCompany.EntityList;
                        //(from Comp in objCompany.EntityList
                        //                                                                                                             where Comp.HomebaseID == HomeBaseID
                        //                                                                                                             select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }
    }
}