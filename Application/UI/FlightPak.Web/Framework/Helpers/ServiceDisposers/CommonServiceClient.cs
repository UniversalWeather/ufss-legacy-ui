﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.CommonService
{
    public partial class CommonServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.CommonService.ICommonService>, FlightPak.Web.CommonService.ICommonService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}