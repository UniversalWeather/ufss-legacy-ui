﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.SearchService
{
    public partial class SearchServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.SearchService.ISearchService>, FlightPak.Web.SearchService.ISearchService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}