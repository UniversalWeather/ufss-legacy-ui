﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.IO;

namespace FlightPak.Web.AuthenticationService
{
    public partial class AuthenticationServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.AuthenticationService.IAuthenticationService>, FlightPak.Web.AuthenticationService.IAuthenticationService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}