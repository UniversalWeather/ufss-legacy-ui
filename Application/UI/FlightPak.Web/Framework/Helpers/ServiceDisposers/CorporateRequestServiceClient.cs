﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.CorporateRequestService
{
    public partial class CorporateRequestServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.CorporateRequestService.ICorporateRequestService>, FlightPak.Web.CorporateRequestService.ICorporateRequestService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}