﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.PreflightService
{
    public partial class PreflightServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.PreflightService.IPreflightService>, FlightPak.Web.PreflightService.IPreflightService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}