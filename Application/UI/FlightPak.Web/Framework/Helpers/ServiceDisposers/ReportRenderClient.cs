﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.ReportRenderService
{
    public partial class ReportRenderClient : System.ServiceModel.ClientBase<FlightPak.Web.ReportRenderService.IReportRender>, FlightPak.Web.ReportRenderService.IReportRender, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}