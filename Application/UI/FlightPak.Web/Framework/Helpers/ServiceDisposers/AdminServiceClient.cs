﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.AdminService
{
    public partial class AdminServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.AdminService.IAdminService>, FlightPak.Web.AdminService.IAdminService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}