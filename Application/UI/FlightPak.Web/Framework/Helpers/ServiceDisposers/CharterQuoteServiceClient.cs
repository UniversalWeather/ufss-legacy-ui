﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.CharterQuoteService
{
    public partial class CharterQuoteServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.CharterQuoteService.ICharterQuoteService>, FlightPak.Web.CharterQuoteService.ICharterQuoteService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}