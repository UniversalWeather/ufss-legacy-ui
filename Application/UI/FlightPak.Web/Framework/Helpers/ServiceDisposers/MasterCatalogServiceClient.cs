﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.FlightPakMasterService
{
    public partial class MasterCatalogServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.FlightPakMasterService.IMasterCatalogService>, FlightPak.Web.FlightPakMasterService.IMasterCatalogService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}