﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.CalculationService
{
    public partial class CalculationServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.CalculationService.ICalculationService>, FlightPak.Web.CalculationService.ICalculationService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}