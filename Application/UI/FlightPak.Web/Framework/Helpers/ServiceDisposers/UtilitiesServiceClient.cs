﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace FlightPak.Web.UtilitiesService
{
    public partial class UtilitiesServiceClient : System.ServiceModel.ClientBase<FlightPak.Web.UtilitiesService.IUtilitiesService>, FlightPak.Web.UtilitiesService.IUtilitiesService, IDisposable
    {
        void IDisposable.Dispose()
        {

            if (this.State == CommunicationState.Faulted)
            {
                this.Abort();
            }
            else
            {
                this.Close();
            }

        }
    }
}