﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Framework.Error
{
    public static class WebErrorConstants
    {
        public const String UnauthorizdOperation = "User Not Logged On";
        public const String AirportCodeNotExist = "Airport Code Does Not Exist";
        public const String ClientCodeNotExist = "Client Code Does Not Exist";
        public const String CrewDutyRuleCodeNotExist = "Crew Duty Rule Code Does Not Exist";
    }
}