﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Framework.Masters
{
    public partial class HelpMaster : BaseSecuredMasterPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void HelpContentSearchButton_Click(object sender, EventArgs e)
        {
            string url = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        TryResolveUrl("views/help/HelpContentSearchResult.aspx", out url);
                        var page = this.Page as BasePage;
                        page.RedirectToPage(string.Format("{0}?q={1}", url, Microsoft.Security.Application.Encoder.UrlEncode(HelpContentSearchBox.Text)));

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Global);
                }
            }
        }
    }
}