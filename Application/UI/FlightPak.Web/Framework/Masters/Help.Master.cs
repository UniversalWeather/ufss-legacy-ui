﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FlightPak.Web.Framework.Masters
    {
    public partial class Help : System.Web.UI.MasterPage
        {
        protected void Page_Load(object sender, EventArgs e)
            {

            } 

        protected void Page_PreRender(object sender, EventArgs e)
            {

            string selectedItemValue = Request.QueryString["Screen"];
            RadPanelItem selectedItem = pnlNavigation.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(selectedItemValue));  
            if (selectedItem != null)
                {
                if (selectedItem.Items.Count > 0)
                    {
                    selectedItem.Expanded = true;
                    }
                else
                    {
                    selectedItem.Selected = true;
                    while ((selectedItem != null) &&
                           (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                        {
                        selectedItem = (RadPanelItem)selectedItem.Parent;
                        selectedItem.Expanded = true;
                        }
                    }
                }

            }

        }
    }