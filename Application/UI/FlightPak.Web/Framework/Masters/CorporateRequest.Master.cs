﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Web.UI;
using FlightPak.Web.CorporateRequestService;
using FlightPak.Web.CalculationService;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Common.Constants;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Framework.Masters
{
    public delegate void CorporateAjaxRequestHandler(object sender, AjaxRequestEventArgs e);
    ////Replicate Save Cancel Delete Buttons in header
    //public delegate void ButtonClick(object sender, EventArgs e);

    public partial class CorporateRequest : BaseSecuredMasterPage
    {
        private ExceptionManager exManager;

        public CRMain CorpRequest = new CRMain();

        private delegate void SaveCRSession();
        private delegate void SavePAXSession();
        private delegate void LoadSelectedRequest();
        private delegate void ReuestNew();
        private delegate void ReuestEdit();
        private delegate void ReuestLog();

        //Replicate Save Cancel Delete Buttons in header
        public event ButtonClick SaveClick;
        public event ButtonClick DeleteClick;
        public event ButtonClick CancelClick;

        private Delegate _SaveCRToSession;
        public Delegate SaveCRToSession
        {
            set { _SaveCRToSession = value; }
        }

        private Delegate _SaveToSessionPAX;
        public Delegate SaveToSessionPAX
        {
            set { _SaveToSessionPAX = value; }
        }

        private Delegate _SaveToSessionmain;
        public Delegate SaveToSessionmain
        {
            set { _SaveToSessionmain = value; }
        }

        public event CorporateAjaxRequestHandler RadAjax_AjaxRequest;

        string _alertmessage = "";

        public string Alertmessage
        {
            get { return this._alertmessage; }
            set { this._alertmessage = value; }
        }


        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }

        public System.Web.UI.HtmlControls.HtmlGenericControl MasterForm { get { return this.DivMasterForm; } }

        public RadAjaxManager RadAjaxManagerMaster { get { return this.RadAjaxManager1; } }

        List<CRException> ExceptionList = new List<CRException>();

        #region Changes for Legnum in CRHeader
        public Label FloatLegNum
        {
            get { return this.lblFloatLegNum; }

        }
        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "CorpReq";
                        RadDatePicker1.DateInput.DateFormat = "MM/dd/yyyy";
                        RadDatePicker1.DateInput.DisplayDateFormat = "MM/dd/yyyy";
                        DatePicker.DateInput.DateFormat = "MM/dd/yyyy";
                        DatePicker.DateInput.DisplayDateFormat = "MM/dd/yyyy";


                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                            SetRequestModeToNoChange(ref CorpRequest);
                            using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                            {
                                if (CorpRequest.CRMainID != 0)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.CorporateRequest.CRMain, CorpRequest.CRMainID);
                                    }
                                    var objPrefObj = CorpSvc.GetRequest(CorpRequest.CRMainID);
                                    if (objPrefObj.ReturnFlag)
                                    {
                                        CorpRequest = objPrefObj.EntityList[0];

                                        Session["CurrentCorporateRequest"] = CorpRequest;
                                        var ObjRetval = CorpSvc.GetRequestExceptionList(CorpRequest.CRMainID);
                                        if (ObjRetval.ReturnFlag)
                                        {
                                            Session["CorporateRequestException"] = ObjRetval.EntityList;
                                        }
                                        else
                                            Session["CorporateRequestException"] = null;
                                    }
                                    else
                                    {
                                        CorpRequest = null;
                                        Session["CorporateRequestException"] = null;
                                    }
                                }
                                else
                                {
                                    CorpRequest = null;
                                    Session["CurrentCorporateRequest"] = null;
                                    Session["CorporateRequestException"] = null;
                                    RedirectToMain();
                                }
                            }
                            SetRequestMode();
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindPage();");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void Private_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (dgLegSummary.SelectedItems.Count > 0)
                        {
                            GridDataItem Item = (GridDataItem)dgLegSummary.SelectedItems[0];
                            CRLeg oCRLeg = new CRLeg();
                            if (Item["CRLegID"] != null && Item["CRLegID"].Text != "&nbsp;")
                            {
                                oCRLeg.CRLegID = Convert.ToInt64(Item["CRLegID"].Text.ToString());

                                string IsPrivate = "";

                                if (Item["Private"] != null && Item["Private"].Text != "&nbsp;")
                                {
                                    IsPrivate = Convert.ToString(Item["Private"].Text);
                                }

                                if (IsPrivate == "P")
                                {
                                    oCRLeg.IsPrivate = false;
                                }
                                else
                                {
                                    oCRLeg.IsPrivate = true;
                                }
                                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                                {
                                    if (Session["CurrentCorporateRequest"] != null)
                                    {
                                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                        if (CorpRequest.CRLegs != null || CorpRequest.CRLegs.Count() != 0)
                                        {

                                            for (int i = 0; i < CorpRequest.CRLegs.Count(); i++)
                                            {
                                                if (CorpRequest.CRLegs[i].CRLegID == Convert.ToInt64(Item["CRLegID"].Text.ToString()))
                                                {
                                                    CorpRequest.CRLegs[i].IsPrivate = oCRLeg.IsPrivate;
                                                }
                                            }
                                        }
                                        Session["CurrentCorporateRequest"] = CorpRequest;
                                    }
                                    var Result = Service.ChangePrivate(oCRLeg);
                                    if (Result.ReturnFlag == true)
                                    {
                                        dgLegSummary.Rebind();
                                    }
                                }
                            }
                            RadAjaxManagerMaster.ResponseScripts.Add(@"window.location=window.location;");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.ChangeQueue);
                }
            }
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                        {
                            CheckAutorization(Permission.CorporateRequest.DeleteCRManager);
                            var ReturnValue = Service.Delete(CorpRequest.CRMainID);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                Session["CorporateRequestException"] = null;
                                Session["CurrentCorporateRequest"] = null;
                                RedirectToMain();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    lbFloatstatus.Visible = false;
                    lbFloatTailnum.Visible = false;
                    lbFloatTripnum.Visible = false;
                    lbFloatTypeCode.Visible = false;
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //if (!Page.IsPostBack)  // karthik - Commented because the conditions fails on legID postback event 
                        //{
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                if (CorpRequest != null)
                                {
                                    if (CorpRequest.CRMainID != 0)
                                    {
                                        lbFloatTripnum.Visible = true;
                                        lbFloatTripnum.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.CRTripNUM.ToString());
                                    }
                                    else
                                        lbFloatTripnum.Visible = false;

                                    if (CorpRequest.Fleet != null)
                                    {
                                        lbFloatTailnum.Visible = true;
                                        lbFloatTailnum.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Fleet.TailNum);
                                    }
                                    else
                                        lbFloatTailnum.Visible = false;

                                    if (CorpRequest.Aircraft != null)
                                    {
                                        lbFloatTypeCode.Visible = true;
                                        lbFloatTypeCode.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.Aircraft.AircraftCD);
                                    }
                                    else
                                        lbFloatTypeCode.Visible = false;

                                    if (!string.IsNullOrEmpty(CorpRequest.CRMainDescription))
                                    {
                                        lbFloatstatus.Visible = true;
                                        lbFloatstatus.Text = System.Web.HttpUtility.HtmlEncode(CorpRequest.CRMainDescription);
                                    }
                                    else
                                        lbFloatstatus.Visible = false;
                                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                                    {
                                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                                        dgLegs.Rebind();
                                        dgLegSummary.Rebind();
                                        BindException();


                                        //Karthik 08-02-2013 - Start

                                        if (CorpRequest.State != CorporateRequestTripEntityState.Added)
                                        {
                                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                            {
                                                if (CorpRequest.CRMainID != 0)
                                                {
                                                    var returnValue = CommonService.Lock(EntitySet.CorporateRequest.CRMain, CorpRequest.CRMainID);
                                                    if (!returnValue.ReturnFlag)
                                                    {
                                                        RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110,'" + ModuleNameConstants.CorporateRequest.CorporateRequestTripManager + "');");
                                                    }
                                                    else
                                                    {
                                                        dgLegs.Rebind();
                                                        dgLegSummary.Rebind();
                                                        BindException();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dgLegs.Rebind();
                                            dgLegSummary.Rebind();
                                            BindException();
                                        }

                                        //Karthik 08-02-2013 - End
                                    }
                                }
                            }
                        //}

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Changes for Legnum in CRHeader
                        lblFloatLegNum.Visible = false;
                        //lblFloatLegNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        #endregion

                        SaveCRSession SaveCorpRequestSession = new SaveCRSession(SaveToSaveCorpRequestSession);
                        this.CorpRequestHeader.SaveCRToSession = SaveCorpRequestSession;

                        SavePAXSession SaveToSessionPAX = new SavePAXSession(SaveToCorpSessionPAX);
                        this.CorpRequestHeader.SaveToSessionPAX = SaveToSessionPAX;

                        SavePAXSession SaveToSessionmain = new SavePAXSession(SaveToCorpSessionmain);
                        this.CorpRequestHeader.SaveToSessionmain = SaveToSessionmain;

                        LoadSelectedRequest LoadRequesttoSession = new LoadSelectedRequest(_LoadRequesttoSession);
                        this.CorpRequestSearch.SelectTrip = LoadRequesttoSession;

                        ReuestEdit requestEdit = new ReuestEdit(EditRequestEvent);
                        this.CorpRequestHeader.EditClick = requestEdit;

                        ReuestNew requestNew = new ReuestNew(NewRequestEvent);
                        this.CorpRequestHeader.NewClick = requestNew;

                        //Replicate Save Cancel Delete Buttons in header
                        this.CorpRequestHeader.SaveClick += btnSave_Click;
                        this.CorpRequestHeader.CancelClick += btnCancel_Click;
                        this.CorpRequestHeader.DeleteClick += btnDelete_Click;

                        if (!IsPostBack)
                        {
                            if (UserPrincipal.Identity._fpSettings._IsAPISAlert != null)
                            {

                            }

                            #region Authorizatioin
                            CheckAutorization(Permission.CorporateRequest.ViewCRManager);

                            ((GridBoundColumn)dgLegs.Columns[1]).DataFormatString = "{0:MM/dd/yyyy}";

                            if (!IsAuthorized(Permission.CorporateRequest.AddCRManager))
                            {
                                this.CorpRequestHeader.NewTripButton.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.EditCRManager))
                            {
                                this.CorpRequestHeader.EditTripButton.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.ViewCRManager))
                            {
                                RadTab selectedTab = this.CorpRequestHeader.HeaderTab.Tabs.FindTabByText("Request");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.ViewCRLeg))
                            {
                                RadTab selectedTab = this.CorpRequestHeader.HeaderTab.Tabs.FindTabByText("Legs");
                                selectedTab.Visible = false;
                            }


                            if (!IsAuthorized(Permission.CorporateRequest.ViewCRPAXManifest))
                            {
                                RadTab selectedTab = this.CorpRequestHeader.HeaderTab.Tabs.FindTabByText("PAX");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CorporateRequest.ViewCRLogistics))
                            {
                                RadTab selectedTab = this.CorpRequestHeader.HeaderTab.Tabs.FindTabByText("Logistics");
                                selectedTab.Visible = false;
                            }

                            #endregion Authorizatioin


                        }

                        if (!IsAuthorized(Permission.CorporateRequest.AddCRManager))
                        {
                            this.CorpRequestHeader.NewTripButton.Visible = false;
                            this.CorpRequestHeader.CopyTripButton.Visible = false;
                        }


                        if (!IsAuthorized(Permission.CorporateRequest.DeleteCRManager))
                        {
                            this.CorpRequestHeader.DeleteTripButton.Visible = false;
                        }


                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                            //if (Trip.isTripSaved)
                            //{
                            //    RadWindowManager1.RadAlert("Trip has been saved successfully", 330, 100, "Trip Alert", null);
                            //    Trip.isTripSaved = false;
                            //    Session["CurrentPreFlightTrip"] = Trip;
                            //}

                            if (CorpRequest.State == CorporateRequestTripEntityState.Added)
                            {
                                if (!IsAuthorized(Permission.Preflight.AddPreFlightMain))
                                {
                                    if (!IsAuthorized(Permission.Preflight.AddPreFlightMain))
                                    {
                                        this.CorpRequestHeader.SaveTripButton.Visible = false;
                                        this.CorpRequestHeader.CancelTripButton.Visible = false;
                                    }
                                }
                                else if (CorpRequest.State == CorporateRequestTripEntityState.Modified)
                                {
                                    if (!IsAuthorized(Permission.Preflight.EditPreFlightMain))
                                    {
                                        this.CorpRequestHeader.SaveTripButton.Visible = false;
                                        this.CorpRequestHeader.CancelTripButton.Visible = false;
                                    }
                                }

                            }
                        }

                        if (!IsAuthorized(Permission.CorporateRequest.EditCRManager))
                        {
                            this.CorpRequestHeader.EditTripButton.Visible = false;
                            this.CorpRequestHeader.CopyTripButton.Visible = false;
                        }

                        #region For Change Queue
                        if (!IsAuthorized(Permission.CorporateRequest.AddCRChangeQueue))
                        {
                            this.CorpRequestHeader.ChangeQueue.Visible = false;
                        }
                        if (!IsAuthorized(Permission.CorporateRequest.EditCRChangeQueue))
                        {
                            this.CorpRequestHeader.ChangeQueue.Visible = false;
                        }
                        if (!IsAuthorized(Permission.CorporateRequest.ViewCRChangeQueue))
                        {
                            this.CorpRequestHeader.ChangeQueue.Visible = false;
                        }
                        #endregion For Change Queue

                        SetRequestMode();
                        if (CorpRequest.Mode == CorporateRequestTripActionMode.NoChange)
                        {
                            btnPrivate.Enabled = true;
                            btnPrivate.CssClass = "button";
                        }
                        else
                        {
                            btnPrivate.Enabled = false;
                            btnPrivate.CssClass = "button-disable";
                        }

                        if (Session["CurrentCorporateRequest"] != null)
                        {
                            using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
                            {
                                CRMain CorpReq = (CRMain)Session["CurrentCorporateRequest"];

                                if (CorpReq.CRMainID != 0)
                                {
                                var ObjretVal = CorpSvc.GetRequestExceptionList(CorpReq.CRMainID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["CorporateRequestException"] = ObjretVal.EntityList;
                                    ValidationRequest(CorporateRequestService.Group.Legs);
                                    BindException();
                                }
                                else
                                    Session["CorporateRequestException"] = null;


                                }


                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CorporateRequest.CorporateRequestMain);
                }
            }
        }

        protected void SaveToCorpSessionPAX()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SaveToSessionPAX != null)
                    _SaveToSessionPAX.DynamicInvoke();
            }
        }


        protected void SaveToCorpSessionmain()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SaveToSessionmain != null)
                    _SaveToSessionmain.DynamicInvoke();
            }
        }

        //Replicate Save Cancel Delete Buttons in header
        protected void btnSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (SaveClick != null)
                {
                    SaveClick(sender, e);
                }
            }

        }
        //Replicate Save Cancel Delete Buttons in header
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
            {
                CancelClick(sender, e);
            }
        }
        //Replicate Save Cancel Delete Buttons in header
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (DeleteClick != null)
                {
                    DeleteClick(sender, e);
                }
            }
        }

        protected void SetRequestMode()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    this.CorpRequestSearch.CRMainID = CorpRequest.CRMainID;
                    if (CorpRequest.CRTripNUM != null)
                        this.CorpRequestSearch.CRTripNUM = (long)CorpRequest.CRTripNUM;
                    if (CorpRequest.Fleet != null)
                    {
                        if (CorpRequest.Fleet.TailNum != null)
                        {
                            this.CorpRequestSearch.TailNo = CorpRequest.Fleet.TailNum;
                        }
                    }
                    //2955
                    if (CorpRequest.Aircraft != null)
                    {
                        if (CorpRequest.Aircraft.AircraftCD != null)
                        {
                            this.CorpRequestSearch.TypeCode = CorpRequest.Aircraft.AircraftCD;
                        }
                    }

                    if (CorpRequest.LastUpdTS != DateTime.MinValue && CorpRequest.LastUpdTS != null)
                    {
                        DateTime LastModifiedUTCDate = DateTime.Now;
                        if (UserPrincipal.Identity._airportId != null)
                        {
                            using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                            {
                                LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)CorpRequest.LastUpdTS, false, false);
                            }
                        }
                        else
                            LastModifiedUTCDate = (DateTime)CorpRequest.LastUpdTS;
                        this.CorpRequestHeader.LastModified.Text = System.Web.HttpUtility.HtmlEncode("Last Modified: " + String.Format(CultureInfo.InvariantCulture, "{1} {0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", LastModifiedUTCDate, CorpRequest.LastUpdUID));
                    }
                    else
                        this.CorpRequestHeader.LastModified.Text = "";

                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {

                        this.CorpRequestHeader.EditTripButton.Enabled = false;
                        this.CorpRequestHeader.NewTripButton.Enabled = false;

                        this.CorpRequestHeader.CopyTripButton.Enabled = false;
                        //btnPrivate.Enabled = false;
                        //btnPrivate.CssClass = "button-disable";

                        //Replicate Save Cancel Delete Buttons in header
                        this.CorpRequestHeader.SaveTripButton.Enabled = true;
                        this.CorpRequestHeader.CancelTripButton.Enabled = true;
                        this.CorpRequestHeader.DeleteTripButton.Enabled = false;

                        //Replicate Save Cancel Delete Buttons in header
                        this.CorpRequestHeader.SaveTripButton.CssClass = "ui_nav";
                        this.CorpRequestHeader.CancelTripButton.CssClass = "ui_nav";
                        this.CorpRequestHeader.DeleteTripButton.CssClass = "ui_nav_disable";

                        this.CorpRequestHeader.EditTripButton.CssClass = "ui_nav_disable";
                        this.CorpRequestHeader.NewTripButton.CssClass = "ui_nav_disable";

                        this.CorpRequestHeader.CopyTripButton.CssClass = "ui_nav_disable";

                        this.CorpRequestHeader.ChangeQueue.Enabled = false;
                        this.CorpRequestHeader.ChangeQueue.CssClass = "ui_nav_disable";
                    }
                    else
                    {


                        this.CorpRequestHeader.NewTripButton.Enabled = true;
                        this.CorpRequestHeader.EditTripButton.Enabled = true;

                        this.CorpRequestHeader.CopyTripButton.Enabled = true;

                        //Replicate Save Cancel Delete Buttons in header
                        this.CorpRequestHeader.SaveTripButton.Enabled = false;
                        this.CorpRequestHeader.CancelTripButton.Enabled = false;
                        this.CorpRequestHeader.DeleteTripButton.Enabled = true;

                        //Replicate Save Cancel Delete Buttons in header
                        this.CorpRequestHeader.SaveTripButton.CssClass = "ui_nav_disable";
                        this.CorpRequestHeader.CancelTripButton.CssClass = "ui_nav_disable";
                        this.CorpRequestHeader.DeleteTripButton.CssClass = "ui_nav";

                        //btnPrivate.Enabled = true;
                        //btnPrivate.CssClass = "button";
                        this.CorpRequestHeader.EditTripButton.CssClass = "ui_nav";
                        this.CorpRequestHeader.NewTripButton.CssClass = "ui_nav";

                        this.CorpRequestHeader.CopyTripButton.CssClass = "ui_nav";

                        this.CorpRequestHeader.ChangeQueue.Enabled = true;
                        this.CorpRequestHeader.ChangeQueue.CssClass = "ui_nav";
                    }

                }
                else
                {

                    //Replicate Save Cancel Delete Buttons in header
                    this.CorpRequestHeader.SaveTripButton.Enabled = false;
                    this.CorpRequestHeader.CancelTripButton.Enabled = false;
                    this.CorpRequestHeader.DeleteTripButton.Enabled = false;

                    this.CorpRequestHeader.SaveTripButton.CssClass = "ui_nav_disable";
                    this.CorpRequestHeader.CancelTripButton.CssClass = "ui_nav_disable";

                    this.CorpRequestHeader.DeleteTripButton.CssClass = "ui_nav_disable";

                    this.CorpRequestHeader.NewTripButton.Enabled = true;
                    this.CorpRequestHeader.EditTripButton.Enabled = false;

                    this.CorpRequestHeader.CopyTripButton.Enabled = false;
                    //btnPrivate.Enabled = false;
                    //btnPrivate.CssClass = "button-disable";


                    this.CorpRequestHeader.NewTripButton.CssClass = "ui_nav";
                    this.CorpRequestHeader.EditTripButton.CssClass = "ui_nav_disable";

                    this.CorpRequestHeader.CopyTripButton.CssClass = "ui_nav_disable";

                    this.CorpRequestHeader.ChangeQueue.Enabled = true;
                    this.CorpRequestHeader.ChangeQueue.CssClass = "ui_nav";
                }

            }
        }


        public void EditRequestEvent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    // Lock the record
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.Lock(EntitySet.CorporateRequest.CRMain, CorpRequest.CRMainID);
                        if (!returnValue.ReturnFlag)
                        {
                            RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110);");
                        }
                        else
                        {
                            CorpRequest.Mode = CorporateRequestTripActionMode.Edit;

                            //Replicate Save Cancel Delete Buttons in header
                            this.CorpRequestHeader.SaveTripButton.Enabled = true;
                            this.CorpRequestHeader.CancelTripButton.Enabled = true;
                            this.CorpRequestHeader.DeleteTripButton.Enabled = false;


                            this.CorpRequestHeader.SaveTripButton.CssClass = "ui_nav";
                            this.CorpRequestHeader.CancelTripButton.CssClass = "ui_nav";
                            this.CorpRequestHeader.DeleteTripButton.CssClass = "ui_nav_disable";


                            this.CorpRequestHeader.EditTripButton.Enabled = false;
                            this.CorpRequestHeader.CopyTripButton.Enabled = false;
                            this.CorpRequestHeader.NewTripButton.CssClass = "ui_nav_disable";
                            this.CorpRequestHeader.EditTripButton.CssClass = "ui_nav_disable";
                            this.CorpRequestHeader.CopyTripButton.CssClass = "ui_nav_disable";
                            CorpRequest.AllowTripToNavigate = true;
                            CorpRequest.AllowTripToSave = true;
                            Session["CurrentCorporateRequest"] = CorpRequest;
                            btnPrivate.Enabled = false;
                            btnPrivate.CssClass = "button-disable";

                            this.CorpRequestHeader.ChangeQueue.Enabled = false;
                            this.CorpRequestHeader.ChangeQueue.CssClass = "ui_nav_disable";
                        }
                    }
                }
            }
        }

        protected void NewRequestEvent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                if (Session["CurrentCorporateRequest"] != null)
                {
                    CRMain CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest.Mode == CorporateRequestTripActionMode.Edit)
                    {
                        RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('Request Unsaved, please Save or Cancel Request', 330, 110);");
                    }
                    else
                    {
                        CorpRequest = new CRMain();
                        CorpRequest.Mode = CorporateRequestTripActionMode.Edit;
                        CorpRequest.AllowTripToNavigate = true;
                        CorpRequest.AllowTripToSave = true;

                        LoadHomebase(CorpRequest);
                        SetTravelCoordinator(CorpRequest);
                        Session["CurrentCorporateRequest"] = CorpRequest;

                        RedirectToMain();
                    }
                }
                else
                {
                    CorpRequest = new CRMain();
                    CorpRequest.Mode = CorporateRequestTripActionMode.Edit;
                    CorpRequest.AllowTripToNavigate = true;

                    CorpRequest.AllowTripToSave = true;
                    LoadHomebase(CorpRequest);
                    SetTravelCoordinator(CorpRequest);
                    Session["CurrentCorporateRequest"] = CorpRequest;
                    RedirectToMain();
                }
            }
        }

        private void RedirectToMain()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Response.Redirect("CorporateRequestMain.aspx?seltab=Request",false);
            }
        }


        protected void _LoadRequesttoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {


            }
        }


        protected void SaveToSaveCorpRequestSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SaveCRToSession != null)
                    _SaveCRToSession.DynamicInvoke();
            }
        }

        /// <summary>
        /// Common method used to save the whole trip into Database
        /// </summary>
        /// <param name="trip">Pass Trip Object</param>
        public void Save(CRMain CorpRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CorpRequest))
            {


                SaveRequest(CorpRequest);

            }
        }


        public void SaveRequest(CRMain CorpRequest)
        {
            bool DatCheck = true;
            DateTime CompanyTime = System.DateTime.Now;
            double CompanySetting = 0;

            if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours != 0)
            {
                CompanySetting = Convert.ToDouble(UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinHours);
            }
            CompanyTime = CompanyTime.AddHours(CompanySetting);
            if (Session["CurrentCorporateRequest"] != null)
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count() > 0)
                {

                    foreach (CRLeg legs in CorpRequest.CRLegs)
                    {
                        if (legs.DepartureDTTMLocal != null && legs.LegNUM == 1)
                        {
                            if (Convert.ToDateTime(legs.DepartureDTTMLocal) <= CompanyTime)
                            {
                                DatCheck = false;
                                break;
                            }
                        }
                    }
                }
            }

            if (DatCheck == false)
            {
                if (UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend != null && UserPrincipal.Identity._fpSettings._CorpReqExcChgWithinWeekend == true)
                {
                    if (System.DateTime.Now.DayOfWeek == DayOfWeek.Saturday || System.DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                    {
                        DatCheck = true;
                    }
                }
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CorpRequest))
            {
                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                {
                    CorpRequest.AllowTripToSave = true;

                    if (CorpRequest.CRLegs != null)
                    {
                        DateTime LegMinDate = DateTime.MinValue;

                        foreach (CRLeg Leg in CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList())
                        {
                            CRLeg Nextleg = new CRLeg();
                            Nextleg = CorpRequest.CRLegs.Where(x => x.LegNUM == (Leg.LegNUM + 1)).FirstOrDefault();

                            if (Nextleg != null)
                            {
                                if (Nextleg.DepartureGreenwichDTTM != null)
                                    Leg.NextGMTDTTM = Nextleg.DepartureGreenwichDTTM;
                                if (Nextleg.HomeDepartureDTTM != null)
                                    Leg.NextHomeDTTM = Nextleg.HomeDepartureDTTM;
                                if (Nextleg.DepartureDTTMLocal != null)
                                    Leg.NextLocalDTTM = Nextleg.DepartureDTTMLocal;
                            }
                            else
                            {
                                if (Leg.ArrivalGreenwichDTTM != null)
                                    Leg.NextGMTDTTM = Leg.ArrivalGreenwichDTTM;
                                if (Leg.HomeArrivalDTTM != null)
                                    Leg.NextHomeDTTM = Leg.HomeArrivalDTTM;
                                if (Leg.ArrivalDTTMLocal != null)
                                    Leg.NextLocalDTTM = Leg.ArrivalDTTMLocal;
                            }
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                if (LegMinDate == DateTime.MinValue)
                                    LegMinDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (LegMinDate > (DateTime)Leg.DepartureDTTMLocal)
                                    LegMinDate = (DateTime)Leg.DepartureDTTMLocal;
                            }
                        }
                        if (LegMinDate != DateTime.MinValue)
                        {
                            string dtstr = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LegMinDate);
                            DateTime dt = DateTime.ParseExact(dtstr, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                            CorpRequest.EstDepartureDT = dt;
                        }
                    }

                    #region Exception Validation

                    List<CRException> eSeverityException = new List<CRException>();
                    List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                    ExceptionTemplateList = GetExceptionTemplateList();



                    if (ExceptionTemplateList != null && ExceptionTemplateList.Count > 0)
                    {
                        var ExceptionlistforMandate = Service.ValidateRequestforMandatoryExceptions(CorpRequest);

                        if (ExceptionlistforMandate.ReturnFlag)
                        {
                            ExceptionList = ExceptionlistforMandate.EntityList;


                        }


                        if (ExceptionList != null && ExceptionList.Count > 0)
                        {
                            eSeverityException = (from Exp in ExceptionList
                                                  join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                  where ExpTempl.Severity == 1
                                                  select Exp).ToList();
                            if (eSeverityException != null && eSeverityException.Count > 0)
                            {
                                CorpRequest.AllowTripToSave = false;
                                RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('Request has mandatory exceptions, Request is not Saved', 330, 110);");

                                List<CRException> OldExceptionList = new List<CRException>();
                                OldExceptionList = (List<CRException>)Session["CorporateRequestException"];

                                if (OldExceptionList != null && OldExceptionList.Count > 0)
                                {

                                    foreach (CRException OldMandatoryException in OldExceptionList.ToList())
                                    {
                                        if (OldMandatoryException.CRExceptionID == 0)
                                            OldExceptionList.Remove(OldMandatoryException);
                                    }
                                }



                                if (ExceptionList != null && ExceptionList.Count > 0)
                                {
                                    if (OldExceptionList == null)
                                        OldExceptionList = new List<CRException>();
                                    OldExceptionList.AddRange(ExceptionList);
                                }


                                Session["CorporateRequestException"] = OldExceptionList;
                                dgException.Rebind();
                            }
                        }
                    }

                    #endregion


                    if (CorpRequest.AllowTripToSave)
                    {
                        if (CorpRequest.CRMainID > 0)
                            CorpRequest.State = CorporateRequestTripEntityState.Modified;
                        else
                            CorpRequest.State = CorporateRequestTripEntityState.Added;


                        if (UserPrincipal != null)
                            CorpRequest.LastUpdUID = UserPrincipal.Identity._name;
                        CorpRequest.LastUpdTS = DateTime.UtcNow;


                        if (CorpRequest.Aircraft != null) CorpRequest.Aircraft = null;
                        if (CorpRequest.Client != null) CorpRequest.Client = null;
                        if (CorpRequest.Company != null) CorpRequest.Company = null;
                        if (CorpRequest.Department != null) CorpRequest.Department = null;
                        if (CorpRequest.DepartmentAuthorization != null) CorpRequest.DepartmentAuthorization = null;
                        if (CorpRequest.Fleet != null) CorpRequest.Fleet = null;
                        if (CorpRequest.Passenger != null) CorpRequest.Passenger = null;
                        if (CorpRequest.TravelCoordinator != null) CorpRequest.TravelCoordinator = null;
                        if (CorpRequest.UserMaster != null) CorpRequest.UserMaster = null;
                        if (CorpRequest.RecordType == null) CorpRequest.RecordType = "T";
                        if (CorpRequest.LastUpdTS == null) CorpRequest.LastUpdTS = DateTime.UtcNow;

                        if (CorpRequest.CRDispatchNotes != null)
                        {
                            if ((bool)CorpRequest.IsDeleted)
                            {
                                if (CorpRequest.CRDispatchNotes != null) CorpRequest.CRDispatchNotes.Clear();
                            }

                        }

                        #region Legs

                        if (CorpRequest.CRLegs != null)
                        {
                            foreach (CRLeg Leg in CorpRequest.CRLegs)
                            {

                                if (CorpRequest.EstDepartureDT == null)
                                {
                                    if (Leg.LegNUM == 1 && Leg.DepartureDTTMLocal != null)
                                        CorpRequest.EstDepartureDT = Leg.DepartureDTTMLocal;
                                }

                                if (Leg.Airport != null) Leg.Airport = null;
                                if (Leg.Airport1 != null) Leg.Airport1 = null;
                                if (Leg.Client != null) Leg.Client = null;
                                if (Leg.FlightCatagory != null) Leg.FlightCatagory = null;
                                if (Leg.Department != null) Leg.Department = null;
                                if (Leg.DepartmentAuthorization != null) Leg.DepartmentAuthorization = null;
                                if (Leg.Passenger != null) Leg.Passenger = null;
                                if (Leg.FBO != null) Leg.FBO = null;
                                if (Leg.UserMaster != null) Leg.UserMaster = null;

                                if ((bool)Leg.IsDeleted)
                                {
                                    if (Leg.CRHotelLists != null) Leg.CRHotelLists.Clear();
                                    if (Leg.CRCateringLists != null) Leg.CRCateringLists.Clear();
                                    if (Leg.CRFBOLists != null) Leg.CRFBOLists.Clear();
                                    if (Leg.CRPassengers != null) Leg.CRPassengers.Clear();
                                    if (Leg.CRTransportLists != null) Leg.CRTransportLists.Clear();
                                }
                                else
                                {

                                    if (Leg.CRPassengers != null)
                                    {
                                        foreach (CRPassenger PAX in Leg.CRPassengers)
                                        {

                                            if (PAX.CRLeg != null) PAX.CRLeg = null;
                                            if (PAX.Passenger != null) PAX.Passenger = null;
                                            if (PAX.UserMaster != null) PAX.UserMaster = null;
                                        }
                                    }

                                    if (Leg.CRTransportLists != null)
                                    {
                                        foreach (CRTransportList Transport in Leg.CRTransportLists)
                                        {
                                            if (Transport.Airport != null) Transport.Airport = null;
                                            if (Transport.UserMaster != null) Transport.UserMaster = null;
                                            if (Transport.CRLeg != null) Transport.CRLeg = null;
                                            if (Transport.Transport != null) Transport.Transport = null;

                                        }
                                    }

                                    if (Leg.CRHotelLists != null)
                                    {
                                        foreach (CRHotelList Hotel in Leg.CRHotelLists)
                                        {
                                            if (Hotel.Airport != null) Hotel.Airport = null;
                                            if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                            if (Hotel.CRLeg != null) Hotel.CRLeg = null;
                                            if (Hotel.Hotel != null) Hotel.Hotel = null;

                                        }
                                    }


                                    if (Leg.CRFBOLists != null)
                                    {
                                        foreach (CRFBOList FBO in Leg.CRFBOLists)
                                        {
                                            if (FBO.Airport != null) FBO.Airport = null;
                                            if (FBO.UserMaster != null) FBO.UserMaster = null;
                                            if (FBO.CRLeg != null) FBO.CRLeg = null;
                                            if (FBO.FBO != null) FBO.FBO = null;
                                        }
                                    }

                                    if (Leg.CRCateringLists != null)
                                    {
                                        foreach (CRCateringList Catering in Leg.CRCateringLists)
                                        {
                                            if (Catering.Airport != null) Catering.Airport = null;
                                            if (Catering.UserMaster != null) Catering.UserMaster = null;
                                            if (Catering.CRLeg != null) Catering.CRLeg = null;
                                            if (Catering.Catering != null) Catering.Catering = null;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        var ReturnValue = Service.Add(CorpRequest);
                        if (ReturnValue.ReturnFlag == true)
                        {

                            CorpRequest = ReturnValue.EntityInfo;
                            CorpRequest.isTripSaved = true;
                            Session["CorporateRequestException"] = null;
                            var objRetval = Service.GetRequestExceptionList(CorpRequest.CRMainID);
                            if (objRetval.ReturnFlag)
                            {
                                Session["CorporateRequestException"] = objRetval.EntityList;
                            }
                            else
                                Session["CorporateRequestException"] = null;

                            SetRequestModeToNoChange(ref CorpRequest);
                            Session["CurrentCorporateRequest"] = CorpRequest;
                            CorpRequest = null;
                            SetRequestMode();

                            if (DatCheck == false && CompanySetting != 0)
                            {
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"confirmExcludeCallBackFn();");
                            }
                            else
                            {
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"SaveCloseAndRebindPage();");
                            }
                        }
                        else
                        {
                            RadAjaxManagerMaster.ResponseScripts.Add(@"jAlert('" + ReturnValue.ErrorMessage + " ','" + ModuleNameConstants.CorporateRequest.CorporateRequestMain + "');");
                        }
                    }
                }
            }
        }


        protected void SetRequestModeToNoChange(ref CRMain CorpRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CorpRequest))
            {
                if (CorpRequest != null)
                {
                    CorpRequest.Mode = CorporateRequestTripActionMode.NoChange;
                    CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                    if (CorpRequest.CRLegs != null)
                    {
                        foreach (CRLeg Leg in CorpRequest.CRLegs)
                        {
                            Leg.State = CorporateRequestTripEntityState.NoChange;
                            if (Leg.CRPassengers != null)
                            {
                                foreach (CRPassenger paxlist in Leg.CRPassengers)
                                {
                                    paxlist.State = CorporateRequestTripEntityState.NoChange;
                                }
                            }
                        }
                    }
                }
            }
        }

        public void Next(string TabName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TabName))
            {
                if (TabName.ToLower() == "pax")
                    TabName = "PAX";

                int tabindex = 0;
                tabindex = this.CorpRequestHeader.HeaderTab.Tabs.FindTabByText(TabName).Index;
                for (int i = tabindex + 1; i <= this.CorpRequestHeader.HeaderTab.Tabs.Count - 1; i++)
                {
                    if (this.CorpRequestHeader.HeaderTab.Tabs[i].Visible == true && this.CorpRequestHeader.HeaderTab.Tabs[i].Enabled == true)
                        Response.Redirect(this.CorpRequestHeader.HeaderTab.Tabs[i].Value);

                }
            }
        }


        public void Cancel(CRMain CorpRequest)
        {
            RadAjaxManagerMaster.ResponseScripts.Add(@"radconfirm('Are you sure want to Cancel?',confirmCancelCallBackFn, 330, 110,null,'Confirmation!');");
        }


        public void Delete(Int64 MainID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MainID))
            {
                int TripNum = -1;
                List<CRMain> CrMain = new List<CRMain>();
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                    if (CorpRequest.CRTripNUM != null && CorpRequest.CRTripNUM.ToString().Trim() != "")
                    {
                        TripNum = Convert.ToInt32(CorpRequest.CRTripNUM);
                    }
                }

                RadAjaxManagerMaster.ResponseScripts.Add(@"radconfirm('You are about to delete the Corporate Request number " + TripNum + ". Are you sure you want to delete this record?',confirmDeleteCallBackFn, 330, 110,null,'Confirmation!');");

            }
        }


        private FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllCompanyMaster Companymaster = new GetAllCompanyMaster();
                    var objCompany = objDstsvc.GetCompanyMasterListInfo();

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllCompanyMaster> Company = (from Comp in objCompany.EntityList
                                                                                                  where Comp.HomebaseID == HomeBaseID
                                                                                                  select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }

        public FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 DepartIcaoID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartIcaoID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllAirport Airportmaster = new GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(DepartIcaoID);
                    if (objAirport.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;

                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;

                }
            }
        }

        protected void SetTravelCoordinator(CRMain CorpRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (CorpRequest.CRMainID == 0)
                {
                    if (UserPrincipal.Identity._homeBaseId != null)
                    {
                        CorporateRequestService.TravelCoordinator travelcoordinator = new CorporateRequestService.TravelCoordinator();
                        CorporateRequestService.Fleet fleet = new CorporateRequestService.Fleet();
                        CorporateRequestService.Aircraft aircraft = new CorporateRequestService.Aircraft();

                        using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                        {
                            var objUserVal = objService.GetUserMasterList();
                            List<AdminService.GetAllUserMasterResult> lstUserMaster = new List<AdminService.GetAllUserMasterResult>();
                            if (objUserVal.ReturnFlag == true)
                            {
                                lstUserMaster = objUserVal.EntityList.Where(x => x.HomebaseID == UserPrincipal.Identity._homeBaseId && x.CustomerID == UserPrincipal.Identity._customerID && x.UserName.ToLower().Trim() == UserPrincipal.Identity._name.ToLower().Trim()).ToList();
                                if (lstUserMaster.Count > 0)
                                {
                                    if (lstUserMaster[0].TravelCoordID != null)
                                    {
                                        using (CorporateRequestServiceClient PrefSvc = new CorporateRequestServiceClient())
                                        {
                                            var objtravel = PrefSvc.GetTravelcoordinatorbyID(Convert.ToInt64(lstUserMaster[0].TravelCoordID));
                                            if (objtravel.ReturnFlag == true)
                                            {
                                                if (objtravel.EntityList.Count > 0)
                                                {
                                                    CorpRequest.TravelCoordinatorID = lstUserMaster[0].TravelCoordID;
                                                    CorpRequest.TravelCoordinator = objtravel.EntityList[0];

                                                }
                                            }
                                            using (FlightPakMasterService.MasterCatalogServiceClient TravelCoordinatorService = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPakMasterService.TravelCoordinatorFleet TravelCo = new FlightPakMasterService.TravelCoordinatorFleet();
                                                TravelCo.TravelCoordinatorID = Convert.ToInt64(lstUserMaster[0].TravelCoordID);
                                                TravelCo.IsDeleted = false;
                                                var objTravelReqAircraft = TravelCoordinatorService.GetTravelCoordinatorFleetInfo(TravelCo).EntityList.Where(y => (y.IsChoice == true) && (y.IsDeleted == false) && (y.TravelCoordinatorID == Convert.ToInt64(lstUserMaster[0].TravelCoordID))).ToList();
                                                if (objTravelReqAircraft.Count() > 0)
                                                {
                                                    if (objTravelReqAircraft[0].FleetID != null)
                                                    {

                                                        var objfleet = PrefSvc.GetFleetbyID(Convert.ToInt64(objTravelReqAircraft[0].FleetID));
                                                        if (objfleet.ReturnFlag == true)
                                                        {
                                                            if (objfleet.EntityList.Count > 0)
                                                            {
                                                                CorpRequest.FleetID = objfleet.EntityList[0].FleetID;
                                                                CorpRequest.Fleet = objfleet.EntityList[0];

                                                                var objaircraft = PrefSvc.GetAircraftbyID(Convert.ToInt64(objfleet.EntityList[0].AircraftID));
                                                                if (objaircraft.ReturnFlag == true)
                                                                {
                                                                    if (objaircraft.EntityList.Count > 0)
                                                                    {
                                                                        CorpRequest.AircraftID = Convert.ToInt64(objaircraft.EntityList[0].AircraftID);
                                                                        CorpRequest.Aircraft = objaircraft.EntityList[0];
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }

                                                FlightPakMasterService.TravelCoordinatorRequestor Travel = new FlightPakMasterService.TravelCoordinatorRequestor();
                                                Travel.TravelCoordinatorID = Convert.ToInt64(lstUserMaster[0].TravelCoordID);
                                                Travel.IsDeleted = false;
                                                var objTravelReq = TravelCoordinatorService.GetTravelCoordinatorRequestorInfo(Travel).EntityList.Where(y => (y.IsChoice == true) && (y.IsDeleted == false) && (y.TravelCoordinatorID == Convert.ToInt64(lstUserMaster[0].TravelCoordID))).ToList();
                                                if (objTravelReq.Count() > 0)
                                                {
                                                    if (objTravelReq[0].PassengerRequestorID != null)
                                                    {

                                                        var objpass = PrefSvc.GetPassengerbyID(Convert.ToInt64(objTravelReq[0].PassengerRequestorID));
                                                        if (objpass.ReturnFlag == true)
                                                        {
                                                            if (objpass.EntityList.Count > 0)
                                                            {
                                                                CorpRequest.PassengerRequestorID = Convert.ToInt64(objTravelReq[0].PassengerRequestorID);
                                                                CorpRequest.Passenger = objpass.EntityList[0];
                                                                if (objpass.EntityList[0].DepartmentID != 0)
                                                                {
                                                                    var objDep = PrefSvc.GetDepartmentbyID(Convert.ToInt64(objpass.EntityList[0].DepartmentID));
                                                                    if (objDep.ReturnFlag == true)
                                                                    {
                                                                        if (objDep.EntityList.Count > 0)
                                                                        {
                                                                            CorpRequest.DepartmentID = Convert.ToInt64(objpass.EntityList[0].DepartmentID);
                                                                            CorpRequest.Department = objDep.EntityList[0];
                                                                        }
                                                                    }
                                                                }
                                                                if (objpass.EntityList[0].AuthorizationID != 0)
                                                                {
                                                                    var objDepAut = PrefSvc.GetAuthorizationbyID(Convert.ToInt64(objpass.EntityList[0].AuthorizationID), Convert.ToInt64(objpass.EntityList[0].DepartmentID));
                                                                    if (objDepAut.ReturnFlag == true)
                                                                    {
                                                                        if (objDepAut.EntityList.Count > 0)
                                                                        {
                                                                            CorpRequest.AuthorizationID = objpass.EntityList[0].AuthorizationID;
                                                                            CorpRequest.DepartmentAuthorization = objDepAut.EntityList[0];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void LoadHomebase(CRMain CorpRequest)
        {
            if (CorpRequest.CRMainID == 0)
            {
                if (UserPrincipal.Identity._homeBaseId != null)
                {

                    if (UserPrincipal.Identity._homeBaseId != null)
                    {
                        Int64 HomebaseID = (long)UserPrincipal.Identity._homeBaseId;

                        CorporateRequestService.Company HomebaseSample = new CorporateRequestService.Company();
                        HomebaseSample.HomebaseID = HomebaseID;

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objDstsvc.GetCompanyMasterListInfo();
                            List<FlightPakMasterService.GetAllCompanyMaster> CompanyList = new List<FlightPakMasterService.GetAllCompanyMaster>();

                            if (objRetVal.ReturnFlag)
                            {

                                CompanyList = objRetVal.EntityList.Where(x => x.HomebaseID == HomebaseID).ToList();
                                if (CompanyList != null && CompanyList.Count > 0)
                                {
                                    HomebaseSample.HomebaseAirportID = CompanyList[0].HomebaseAirportID;
                                }
                            }

                            List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();

                            var fleetretval = objDstsvc.GetFleet();
                            if (fleetretval.ReturnFlag)
                            {
                                GetFleetList = fleetretval.EntityList.Where(x => x.HomebaseID == HomebaseID && x.IsInActive == false).ToList();
                                if (GetFleetList != null && GetFleetList.Count == 1)
                                {
                                    CorporateRequestService.Fleet fleetsample = new CorporateRequestService.Fleet();
                                    fleetsample.FleetID = GetFleetList[0].FleetID;
                                    fleetsample.TailNum = GetFleetList[0].TailNum;
                                    fleetsample.MaximumPassenger = GetFleetList[0].MaximumPassenger != null ? GetFleetList[0].MaximumPassenger : 0;

                                    CorpRequest.FleetID = GetFleetList[0].FleetID;
                                    CorpRequest.Fleet = fleetsample;


                                    if (GetFleetList[0].AircraftID != null)
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();


                                        var objRetVal1 = objDstsvc.GetAircraftList();
                                        if (objRetVal1.ReturnFlag)
                                        {
                                            AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID == (long)GetFleetList[0].AircraftID).ToList();

                                            if (AircraftList != null && AircraftList.Count > 0)
                                            {

                                                CorporateRequestService.Aircraft aircraftSample = new CorporateRequestService.Aircraft();

                                                aircraftSample.AircraftID = AircraftList[0].AircraftID;
                                                aircraftSample.AircraftCD = AircraftList[0].AircraftCD;
                                                aircraftSample.AircraftDescription = AircraftList[0].AircraftDescription;
                                                CorpRequest.Aircraft = aircraftSample;
                                                CorpRequest.AircraftID = AircraftList[0].AircraftID;
                                            }
                                        }
                                    }


                                }
                            }

                        }

                        HomebaseSample.BaseDescription = UserPrincipal.Identity._fpSettings._BaseDescription;
                        //HomebaseSample.HomebaseCD = tbHomeBase.Text;
                        CorpRequest.HomebaseID = HomebaseID;
                        CorpRequest.HomeBaseAirportICAOID = UserPrincipal.Identity._airportICAOCd;
                        if (HomebaseSample.HomebaseAirportID != null)
                        {
                            CorpRequest.HomeBaseAirportID = (long)HomebaseSample.HomebaseAirportID;
                        }
                        CorpRequest.Company = HomebaseSample;

                    }
                }
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //if (e.Argument.ToString() == "ok")
                //{
                //    Response.Redirect("PostFlightmain.aspx");
                //}
                //else
                //{
                //    //Label1.Text = "Cancel";
                //}


                if (RadAjax_AjaxRequest != null)
                {
                    RadAjax_AjaxRequest(sender, e);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
        }

        ///<summary>
        ///Bind Data into Grid
        ///</summary>
        ///<param name="sender"></param>
        ///<param name="e"></param>
        protected void dgLegSummary_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                    List<CRLeg> Dt = new List<CRLeg>();
                    if (CorpRequest.CRLegs != null)
                    {
                        Dt = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        if (Dt.Count > 0)
                        {

                            dgLegSummary.DataSource = (from u in Dt
                                                       where (u.Airport != null && u.Airport1 != null && u.IsDeleted == false)
                                                       select
                                                        new
                                                        {
                                                            LegNum = u.LegNUM,
                                                            CRLegID = u.CRLegID,
                                                            DepartAir = u.Airport.IcaoID == null ? string.Empty : u.Airport1.IcaoID,
                                                            ArrivalAir = u.Airport1.IcaoID == null ? string.Empty : u.Airport.IcaoID,
                                                            Departcity = u.Airport1.CityName,
                                                            Arrivecity = u.Airport.CityName,
                                                            DepartAirportID = u.DAirportID,
                                                            ArrivalAirportID = u.AAirportID,
                                                            ElapseTM = u.ElapseTM == null ? Convert.ToDecimal("0.0") : System.Math.Round(Convert.ToDecimal(u.ElapseTM), 1),
                                                            DepartDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                                            ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal,
                                                            Private = u.IsPrivate != null ? ((bool)u.IsPrivate ? "P" : " ") : "P",
                                                            Dept = u.Department != null ? u.Department.DepartmentCD : "",
                                                            Auth = u.DepartmentAuthorization != null ? u.DepartmentAuthorization.AuthorizationCD : "",
                                                            Cat = u.FlightCatagory != null ? u.FlightCatagory.FlightCatagoryCD : string.Empty,
                                                            EOD = u.IsDutyEnd != null ? (bool)u.IsDutyEnd : false,
                                                            DutyErr = string.Empty,
                                                            PAXNo = u.PassengerTotal,
                                                            SeatAvail = u.SeatTotal,
                                                            FlightCost = "0.00"
                                                        }).OrderBy(x => x.LegNum);
                            ExpandSummaryPanel(true);

                        }
                    }
                }
            }
        }

        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[1].Length == 1)
                            result = result + "0";
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }

        //protected string RounsETEbasedOnCompanyProfileSettings(decimal ETEVal)
        //{

        //    string ETEStr = string.Empty;

        //    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
        //    {

        //        ETEStr = ConvertTenthsToMins(RoundElpTime((double)ETEVal).ToString());
        //    }
        //    else
        //    {

        //        ETEStr = Math.Round((decimal)ETEVal, 1).ToString();
        //    }

        //    return Common.Utility.DefaultEteResult(ETEStr);
        //}

        protected void dgLegSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    if (item["PAXNo"].Text != "&nbsp;" && item["SeatAvail"].Text != "&nbsp;")
                        item.ToolTip = "No of Passengers: " + item["PAXNo"].Text + "\n Seats Available: " + item["SeatAvail"].Text;

                    if (item["DepartDate"].Text != "&nbsp;")
                    {
                        DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                        if (date != null)
                            item["DepartDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date));
                    }
                    if (item["ArrivalDate"].Text != "&nbsp;")
                    {
                        DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                        if (date1 != null)
                            item["ArrivalDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date1));
                    }
                    if (item["Private"] != null && item["Private"].Text != "&nbsp;")
                    {
                        item["Private"].ForeColor = System.Drawing.Color.Red;
                    }

                    if (item["ElapseTM"] != null && item["ElapseTM"].Text != "&nbsp;")
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            if (item["ElapseTM"].Text != null)
                            {
                                item["ElapseTM"].Text = ConvertTenthsToMins(System.Math.Round(Convert.ToDecimal((item["ElapseTM"].Text).ToString()), 1).ToString());

                                if (item["ElapseTM"].Text.Length == 4)
                                {
                                    item["ElapseTM"].Text = "0" + item["ElapseTM"].Text;
                                }
                            }
                            else
                            {
                                item["ElapseTM"].Text = "00:00";
                            }

                        }
                        else
                        {

                            if (item["ElapseTM"].Text != null)
                            {
                                if (item["ElapseTM"].Text == "0")
                                {
                                    item["ElapseTM"].Text = "0.0";
                                }
                                else
                                {
                                    item["ElapseTM"].Text = System.Math.Round(Convert.ToDecimal((item["ElapseTM"].Text).ToString()), 1).ToString();
                                }

                            }
                            else
                            {
                                item["ElapseTM"].Text = "0.0";
                            }
                        }

                    }

                    //ElapseTM = u.ElapseTM == null ? Convert.ToDecimal("0.0") : System.Math.Round(Convert.ToDecimal(u.ElapseTM),1),
                    if (CorpRequest != null)
                    {
                        if (CorpRequest.CRLegs != null)
                        {
                            TableCell cell = (TableCell)item["DutyErr"];
                            cell.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
        }



        protected void dgLegs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CurrentCorporateRequest"] != null)
                {
                    CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                    List<CRLeg> Dt = new List<CRLeg>();
                    if (CorpRequest.CRLegs != null)
                    {
                        Dt = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).ToList();

                        if (Dt.Count > 0)
                        {

                            dgLegs.DataSource = (from u in Dt
                                                 where u.Airport != null && u.Airport1 != null
                                                 select
                                                  new
                                                  {

                                                      LegNum = u.LegNUM,
                                                      DepartAir = u.Airport.IcaoID == null ? string.Empty : u.Airport1.IcaoID,
                                                      DeparttDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                                      ArrivalAir = u.Airport1.IcaoID == null ? string.Empty : u.Airport.IcaoID,
                                                      ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal


                                                  }).OrderBy(x => x.LegNum);

                        }
                    }
                }
            }
        }

        protected void dgLegs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;

                    if (item["DeparttDate"].Text != "&nbsp;")
                    {
                        DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DeparttDate");
                        item["DeparttDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date));
                        item["DeparttDate"].ToolTip = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date));
                    }
                    if (item["ArrivalDate"].Text != "&nbsp;")
                    {
                        DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                        item["ArrivalDate"].Text = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date1));
                        item["ArrivalDate"].ToolTip = System.Web.HttpUtility.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date1));
                    }
                }
            }
        }

        //Telerik Grid that is used in this UserControl
        public RadGrid TrackerLegs
        {
            get { return this.dgLegs; }
        }

        public RadGrid LegSummary
        {
            get { return this.dgLegSummary; }
        }

        public RadGrid Exceptions
        {
            get { return this.dgException; }
        }

        /// <summary>
        /// Method to Bind Exceptions from Excepton List
        /// </summary>
        /// <param name="exceptionList"></param>
        public void BindException()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CorporateRequestException"] != null)
                {
                    ExceptionList = (List<CRException>)Session["CorporateRequestException"];

                    if (ExceptionList.Count > 0)
                    {
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        using (CorporateRequestServiceClient PrefSvc = new CorporateRequestServiceClient())
                        {
                            ExceptionTemplateList = GetExceptionTemplateList();
                            dgException.DataSource = (from Exp in ExceptionList
                                                      join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                      select new
                                                      {
                                                          Severity = ExpTempl.Severity,
                                                          ExceptionDescription = Exp.CRExceptionDescription,
                                                          SubModuleID = ExpTempl.SubModuleID,
                                                          SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                                          DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                                      }).OrderBy(x => x.DisplayOrder).ToList();
                            dgException.DataBind();
                            ExpandErrorPanel(true);
                        }
                    }
                    else
                    {
                        ExpandErrorPanel(false);
                    }

                }
            }
        }

        protected void dgException_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["CorporateRequestException"] != null)
                {
                    ExceptionList = (List<CRException>)Session["CorporateRequestException"];

                    if (ExceptionList.Count > 0)
                    {
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        using (CorporateRequestServiceClient PrefSvc = new CorporateRequestServiceClient())
                        {
                            ExceptionTemplateList = GetExceptionTemplateList();
                            dgException.DataSource = (from Exp in ExceptionList
                                                      join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                      select new
                                                      {
                                                          Severity = ExpTempl.Severity,
                                                          ExceptionDescription = Exp.CRExceptionDescription,
                                                          SubModuleID = ExpTempl.SubModuleID,
                                                          SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                                          DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                                      }).OrderBy(x => x.DisplayOrder).ToList();
                            ExpandErrorPanel(true);
                        }
                    }
                    else
                    {
                        ExpandErrorPanel(false);
                    }

                }
            }
        }

        public List<ExceptionTemplate> GetExceptionTemplateList()
        {
            List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();

            if (Session["CorporateRequestExceptionTemplateList"] == null)
            {
                using (CorporateRequestServiceClient Service = new CorporateRequestServiceClient())
                {
                    var ObjExTemplRetVal = Service.GetExceptionTemplateList();
                    if (ObjExTemplRetVal.ReturnFlag)
                    {
                        ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                        Session["CorporateRequestExceptionTemplateList"] = ExceptionTemplateList;
                    }
                }
            }
            else
                ExceptionTemplateList = (List<ExceptionTemplate>)Session["CorporateRequestExceptionTemplateList"];
            return ExceptionTemplateList;
        }

        protected string GetSubmoduleModule(long SubModuleID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SubModuleID))
            {
                string Retval = string.Empty;
                switch (SubModuleID)
                {

                    case 1: Retval = "Main"; break;
                    case 2: Retval = "Leg"; break;
                    case 4: Retval = "PAX"; break;
                    case 5: Retval = "Lo"; break;
                    case 6: Retval = "Main"; break;

                }

                return Retval;
            }
        }

        /// <summary>
        /// Exception Grid Item Databound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exception_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                if (e.Item is GridDataItem)
                {
                    GridDataItem Item = (GridDataItem)e.Item;
                    Image imgSeverity = (Image)Item.FindControl("imgSeverity");
                    string severity = Item.GetDataKeyValue("Severity").ToString().Trim();
                    // Default Image Path
                    string imagePath = "~/App_Themes/Default/images/info.png";
                    if (severity == "1")
                    {
                        imagePath = "~/App_Themes/Default/images/info_red.gif";
                    }
                    else if (severity == "2")
                    {
                        imagePath = "~/App_Themes/Default/images/info_yellow.gif";
                    }
                    imgSeverity.ImageUrl = imagePath;

                }
            }
        }

        /// <summary>
        /// Method to Expand or Collapse Error Panel
        /// </summary>
        /// <param name="isExpand">Pass Boolean Value</param>
        private void ExpandErrorPanel(bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isExpand))
            {
                RadPanelItem pnlItem = pnlException.FindItemByValue("Exceptions");
                pnlItem.Expanded = isExpand;
            }
        }

        private void ExpandSummaryPanel(bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isExpand))
            {
                RadPanelItem pnlItem = pnlbarPreflightInfo.FindItemByValue("LegSummary");
                pnlItem.Expanded = isExpand;
            }

        }

        protected void btnAlert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Response.Redirect("CorporateRequestLegs.aspx?seltab=Legs");
            }
        }

        public void UpdateLegDates(int FromLegnum, DateTime ChangedDate)
        {
            if (Session["CurrentCorporateRequest"] != null)
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

                if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                {
                    List<CRLeg> Preflegs = new List<CRLeg>();

                    Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                    double dateDifference = 0;

                    foreach (CRLeg Leg in Preflegs)
                    {
                        if (Leg.LegNUM >= FromLegnum)
                        {


                            if (Leg.LegNUM == FromLegnum)
                            {
                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    DateTime initialDatetime = DateTime.MinValue;
                                    initialDatetime = ((DateTime)Leg.DepartureDTTMLocal).Date;
                                    TimeSpan t = ChangedDate - initialDatetime;
                                    dateDifference = t.Days;
                                }
                                else
                                {
                                    if (Leg.ArrivalDTTMLocal != null)
                                    {
                                        DateTime initialDatetime = DateTime.MinValue;
                                        initialDatetime = ((DateTime)Leg.ArrivalDTTMLocal).Date;
                                        TimeSpan t = ChangedDate - initialDatetime;
                                        dateDifference = t.Days;
                                    }
                                }
                            }

                            bool breakAfterThis = false;
                            if (Leg.IsDutyEnd != null && (bool)Leg.IsDutyEnd)
                            {
                                CRLeg NextLeg = Preflegs.Where(x => x.LegNUM == Leg.LegNUM + 1).SingleOrDefault();

                                if (NextLeg != null)
                                {
                                    DateTime CurrLegarrDate = DateTime.MinValue;
                                    DateTime NextLegDeptDate = DateTime.MinValue;

                                    if (Leg.DepartureDTTMLocal != null && NextLeg.DepartureDTTMLocal != null)
                                    {
                                        CurrLegarrDate = ((DateTime)Leg.ArrivalDTTMLocal).Date;
                                        NextLegDeptDate = ((DateTime)NextLeg.DepartureDTTMLocal).Date;

                                        if (CurrLegarrDate != NextLegDeptDate)
                                            breakAfterThis = true;
                                    }
                                }

                            }

                            if (dateDifference != 0)
                            {
                                if (Leg.LegID != 0 && Leg.State != CorporateRequestTripEntityState.Modified)
                                {
                                    Leg.State = CorporateRequestTripEntityState.Modified;
                                }

                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    Leg.DepartureDTTMLocal = ((DateTime)Leg.DepartureDTTMLocal).AddDays(dateDifference);
                                }

                                if (Leg.DepartureGreenwichDTTM != null)
                                {
                                    Leg.DepartureGreenwichDTTM = ((DateTime)Leg.DepartureGreenwichDTTM).AddDays(dateDifference);
                                }

                                if (Leg.HomeDepartureDTTM != null)
                                {
                                    Leg.HomeDepartureDTTM = ((DateTime)Leg.HomeDepartureDTTM).AddDays(dateDifference);
                                }

                                if (Leg.ArrivalDTTMLocal != null)
                                {
                                    Leg.ArrivalDTTMLocal = ((DateTime)Leg.ArrivalDTTMLocal).AddDays(dateDifference);
                                }

                                if (Leg.ArrivalGreenwichDTTM != null)
                                {
                                    Leg.ArrivalGreenwichDTTM = ((DateTime)Leg.ArrivalGreenwichDTTM).AddDays(dateDifference);
                                }

                                if (Leg.HomeArrivalDTTM != null)
                                {
                                    Leg.HomeArrivalDTTM = ((DateTime)Leg.HomeArrivalDTTM).AddDays(dateDifference);
                                }

                            }

                            if (breakAfterThis)
                                break;
                        }
                    }

                    Session["CurrentCorporateRequest"] = CorpRequest;
                }
            }
        }


        #region Exceptions
        /// <summary>
        /// Method for Validation
        /// </summary>
        /// <returns>Return Exceptions as a List</returns>
        public void ValidationRequest(CorporateRequestService.Group ValidationGroup)
        {
            CorpRequest = (CRMain)Session["CurrentCorporateRequest"];

            using (CorporateRequestServiceClient CorpSvc = new CorporateRequestServiceClient())
            {

                var ObjRetval = CorpSvc.ValidateRequestforMandatoryExceptions(CorpRequest);

                if (ObjRetval.ReturnFlag)
                {

                    List<CRException> OldExceptionList = new List<CRException>();
                    OldExceptionList = (List<CRException>)Session["CorporateRequestException"];
                    if (OldExceptionList != null && OldExceptionList.Count > 0)
                    {

                        foreach (CRException OldMandatoryException in OldExceptionList.ToList())
                        {
                            if (OldMandatoryException.CRExceptionID == 0)
                                OldExceptionList.Remove(OldMandatoryException);
                        }
                    }
                    ExceptionList = ObjRetval.EntityList;

                    if (ExceptionList != null && ExceptionList.Count > 0)
                    {
                        if (OldExceptionList == null)
                            OldExceptionList = new List<CRException>();
                        OldExceptionList.AddRange(ExceptionList);
                    }


                    Session["CorporateRequestException"] = OldExceptionList;
                }

                List<ExceptionTemplate> RequestExceptions = new List<ExceptionTemplate>();
                var ObjRetvalTemplates = CorpSvc.GetExceptionTemplateList();

                if (ObjRetvalTemplates.ReturnFlag)
                {
                    RequestExceptions = ObjRetvalTemplates.EntityList;
                }
            }
        }

        public void ValidateLegExists()
        {
            if (Session["CurrentCorporateRequest"] != null)
            {
                CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                if (CorpRequest.CRLegs == null || CorpRequest.CRLegs.Count == 0)
                {
                    RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('Request has no Legs!!!', 330, 110, 'Request Alert',alertCallBackFnCorp);");
                }
            }

        }

        public void DoLegCalculationsForTrip()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    if (Session["CurrentCorporateRequest"] != null)
                    {
                        CorpRequest = (CRMain)Session["CurrentCorporateRequest"];
                        if (CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                        {
                            List<CRLeg> PrefLeg = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            foreach (CRLeg Leg in PrefLeg)
                            {

                                #region Miles
                                double Miles = 0;
                                if (Leg.DAirportID > 0 && Leg.AAirportID > 0)
                                {
                                    Miles = objDstsvc.GetDistance((long)Leg.DAirportID, (long)Leg.AAirportID);

                                }
                                Leg.Distance = (decimal)Miles;
                                #endregion
                                #region Bias

                                List<double> BiasList = new List<double>();

                                if (Leg.PowerSetting == null)
                                    Leg.PowerSetting = "1";

                                if (CorpRequest.AircraftID != null && CorpRequest.AircraftID != 0)
                                {
                                    FlightPakMasterService.Aircraft CorpRequestAircraft = new FlightPakMasterService.Aircraft();
                                    CorpRequestAircraft = GetAircraft((long)CorpRequest.AircraftID);
                                    Leg.PowerSetting = CorpRequestAircraft.PowerSetting != null ? CorpRequestAircraft.PowerSetting : "1";

                                }

                                if (
                                    (Leg.DAirportID != null && Leg.DAirportID > 0)
                                    &&
                                    (Leg.AAirportID != null && Leg.AAirportID > 0)
                                    &&
                                    (CorpRequest.AircraftID != null && CorpRequest.AircraftID > 0)
                                    )
                                {
                                    BiasList = objDstsvc.CalcBiasTas((long)Leg.DAirportID, (long)Leg.AAirportID, (long)CorpRequest.AircraftID, Leg.PowerSetting);
                                    if (BiasList != null)
                                    {
                                        Leg.TakeoffBIAS = (decimal)BiasList[0];
                                        Leg.LandingBIAS = (decimal)BiasList[1];
                                        Leg.TrueAirSpeed = (decimal)BiasList[2];
                                    }
                                }
                                else
                                {
                                    Leg.TakeoffBIAS = 0.0M;
                                    Leg.LandingBIAS = 0.0M;
                                    Leg.TrueAirSpeed = 0.0M;
                                }
                                #endregion
                                #region "Wind"
                                Leg.WindsBoeingTable = 0.0M;
                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    DateTime dt;
                                    dt = (DateTime)Leg.DepartureDTTMLocal;
                                    //Function Wind Calculation
                                    double Wind = 0;
                                    if (
                                    (Leg.DAirportID != null && Leg.DAirportID > 0)
                                    &&
                                    (Leg.AAirportID != null && Leg.AAirportID > 0)
                                    &&
                                    (CorpRequest.AircraftID != null && CorpRequest.AircraftID > 0)
                                    )
                                    {
                                        Wind = objDstsvc.GetWind((long)Leg.DAirportID, (long)Leg.AAirportID, Convert.ToInt32(Leg.WindReliability), (long)CorpRequest.AircraftID, GetQtr(dt).ToString());
                                        Leg.WindsBoeingTable = (decimal)Wind;
                                    }
                                }
                                #endregion
                                #region "ETE"
                                double ETE = 0;
                                // lnToBias, lnLndBias, lnTas

                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    DateTime dt;
                                    dt = (DateTime)Leg.DepartureDTTMLocal;


                                    ETE = objDstsvc.GetIcaoEte((double)Leg.WindsBoeingTable, (double)Leg.LandingBIAS, (double)Leg.TrueAirSpeed, (double)Leg.TakeoffBIAS, (double)Leg.Distance, dt);

                                    ETE = RoundElpTime(ETE);

                                    if (ETE == 0)
                                    {
                                        if (CorpRequest.AircraftID != null)
                                        {
                                            Int64 AircraftID = Convert.ToInt64(CorpRequest.AircraftID);
                                            FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);

                                            if (Aircraft != null)
                                                if (Aircraft.IsFixedRotary != null && Aircraft.IsFixedRotary.ToUpper() == "R")
                                                    ETE = 0.1;
                                        }
                                    }

                                    ETE = Convert.ToDouble(Common.Utility.DefaultEteResult(Convert.ToString(ETE)));
                                }

                                Leg.ElapseTM = (decimal)ETE;

                                #endregion
                                #region CalculateDatetime

                                double x10, x11;

                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    DateTime ChangedDate;

                                    ChangedDate = (DateTime)Leg.DepartureDTTMLocal;

                                    if (ChangedDate != null)
                                    {


                                        DateTime EstDepartDate = (DateTime)ChangedDate;
                                        try
                                        {

                                            if ((Leg.DAirportID != null && Leg.DAirportID > 0)
                                                && (Leg.AAirportID != null && Leg.AAirportID > 0)
                                                )
                                            {

                                                if (!string.IsNullOrEmpty(Leg.ElapseTM.ToString()))
                                                {
                                                    string ETEstr = "0";

                                                    ETEstr = Leg.ElapseTM.ToString();


                                                    double CorpRequestleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                                    x10 = (CorpRequestleg_elp_time * 60 * 60);
                                                    x11 = (CorpRequestleg_elp_time * 60 * 60);
                                                }
                                                else
                                                {
                                                    x10 = 0;
                                                    x11 = 0;
                                                }
                                                DateTime DeptUTCdt;
                                                DateTime ArrUTCdt;

                                                DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;
                                                ArrUTCdt = DeptUTCdt;
                                                ArrUTCdt = ArrUTCdt.AddSeconds(x10);
                                                DateTime ldLocDep = DateTime.MinValue;
                                                DateTime ldLocArr = DateTime.MinValue;
                                                DateTime ldHomDep = DateTime.MinValue;
                                                DateTime ldHomArr = DateTime.MinValue;
                                                ldLocDep = objDstsvc.GetGMT((long)Leg.DAirportID, DeptUTCdt, false, false);



                                                if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                    ldHomDep = objDstsvc.GetGMT((long)CorpRequest.HomeBaseAirportID, DeptUTCdt, false, false);


                                                ldLocArr = objDstsvc.GetGMT((long)Leg.AAirportID, ArrUTCdt, false, false);

                                                if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                    ldHomArr = objDstsvc.GetGMT((long)CorpRequest.HomeBaseAirportID, ArrUTCdt, false, false);

                                                if (Leg.DAirportID > 0)
                                                {

                                                    Leg.DepartureDTTMLocal = ldLocDep;
                                                    if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                        Leg.HomeDepartureDTTM = ldHomDep;

                                                    Leg.DepartureGreenwichDTTM = DeptUTCdt;

                                                }
                                                else
                                                {
                                                    DateTime Localdt = EstDepartDate;

                                                    Leg.DepartureDTTMLocal = Localdt;
                                                    if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                        Leg.HomeDepartureDTTM = Localdt;
                                                    Leg.DepartureGreenwichDTTM = Localdt;

                                                }

                                                if (Leg.AAirportID > 0)
                                                {
                                                    Leg.ArrivalDTTMLocal = ldLocArr;
                                                    if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                        Leg.HomeArrivalDTTM = ldHomArr;
                                                    Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                                }
                                                else
                                                {
                                                    DateTime Localdt = EstDepartDate;

                                                    Leg.ArrivalDTTMLocal = Localdt;
                                                    if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                        Leg.HomeArrivalDTTM = Localdt;
                                                    Leg.ArrivalGreenwichDTTM = Localdt;
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            //Manually Handled
                                            if (Leg.ArrivalDTTMLocal == null)
                                            {
                                                Leg.ArrivalDTTMLocal = EstDepartDate;
                                            }
                                            if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                if (Leg.HomeArrivalDTTM == null)
                                                {
                                                    Leg.HomeArrivalDTTM = EstDepartDate;
                                                }

                                            if (Leg.ArrivalGreenwichDTTM == null)
                                            {
                                                Leg.ArrivalGreenwichDTTM = EstDepartDate;
                                            }

                                            if (Leg.DepartureDTTMLocal == null)
                                            {
                                                Leg.DepartureDTTMLocal = EstDepartDate;
                                            }

                                            if (Leg.DepartureGreenwichDTTM == null)
                                            {
                                                Leg.DepartureGreenwichDTTM = EstDepartDate;
                                            }
                                            if (CorpRequest.HomeBaseAirportID != null && CorpRequest.HomeBaseAirportID > 0)
                                                if (Leg.HomeDepartureDTTM == null)
                                                {
                                                    Leg.HomeDepartureDTTM = EstDepartDate;
                                                }
                                        }

                                    }
                                }
                                #endregion
                            }

                            #region "PreflightFARRule"


                            double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
                               lnNeededRest, lnWorkArea;

                            lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0;

                            bool llRProblem, llFProblem, llDutyBegin, llDProblem;
                            llRProblem = llFProblem = llDutyBegin = llDProblem = false;

                            DateTime ltGmtDep, ltGmtArr, llDutyend;
                            Int64 lnDuty_RulesID = 0;
                            string lcDuty_Rules = string.Empty;


                            if (CorpRequest.CRLegs != null)
                            {
                                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                                lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                                lnDuty_Hrs = 0;
                                lnRest_Hrs = 0;
                                lnFlt_Hrs = 0;

                                if (Preflegs[0].DepartureGreenwichDTTM != null)
                                {
                                    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                                    ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                                    llDutyBegin = true;
                                    double lnOldDuty_hrs = -1;

                                    int legcounter = 0;
                                    foreach (CRLeg Leg in Preflegs)
                                    {
                                        if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                                        {

                                            double dbElapseTime = 0.0;
                                            long dbCrewDutyRulesID = 0;

                                            if (Leg.ElapseTM != null)
                                                dbElapseTime = (double)Leg.ElapseTM;

                                            double lnOverRide = 0;
                                            double lnDutyLeg_Num = 0;

                                            bool llDutyEnd = false;
                                            string FARNum = "";

                                            //if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].c != null)
                                            //    lnDuty_RulesID = (Int64)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CrewDutyRulesID;
                                            //else
                                            //    lnDuty_RulesID = 0;
                                            if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd == null)
                                                llDutyEnd = false;
                                            else
                                                llDutyEnd = (bool)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd;

                                            //if it is last leg then  llDutyEnd = true;
                                            if (lnEndLegNum == CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegNUM)
                                                llDutyEnd = true;


                                            lnOverRide = (double)(CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CROverRide == null ? 0 : CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].CROverRide);



                                            lnDutyLeg_Num = (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegNUM;



                                            FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient objectDstsvc = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                                            var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                                            if (objRetVal.ReturnFlag)
                                            {
                                                List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                                          where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                                          select CrewDutyRl).ToList();

                                                if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                                {
                                                    FARNum = CrewDtyRule[0].FedAviatRegNum;
                                                    lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                                    lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                                    lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                                    lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                                    lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                                    lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                                    lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                                }
                                                else
                                                {
                                                    lnDayBeg = 0;
                                                    lnDayEnd = 0;
                                                    lnMaxDuty = 0;
                                                    lnMaxFlt = 0;
                                                    lnMinRest = 8;
                                                    lnRestMulti = 0;
                                                    lnRestPlus = 0;
                                                }
                                            }
                                            TimeSpan? Hoursdiff = (DateTime)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].DepartureGreenwichDTTM - ltGmtArr;
                                            if (Leg.ElapseTM != 0)
                                            {
                                                lnFlt_Hrs = lnFlt_Hrs + (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].ElapseTM;
                                                lnDuty_Hrs = lnDuty_Hrs + (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((CorpRequestleg.gmtdep - ltGmtArr),0)/3600,1))
                                            }
                                            else
                                            {

                                                lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((CorpRequestleg.gmtdep - ltGmtArr),0)/3600,1))
                                            }

                                            llRProblem = false;

                                            if (llDutyBegin && lnOldDuty_hrs != -1)
                                            {

                                                lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                                if (lnRestMulti > 0)
                                                {

                                                    //Here is the Multiple and Plus hours usage
                                                    lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                                    if (lnRest_Hrs < lnNeededRest)
                                                    {

                                                        llRProblem = true;
                                                    }
                                                }
                                                else
                                                {
                                                    if (lnRest_Hrs < (lnMinRest + lnRestPlus))
                                                    {

                                                        llRProblem = true;
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                if (llDutyBegin)
                                                {
                                                    lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                                }
                                            }

                                            if (llDutyEnd)
                                            {
                                                lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                                            }

                                            string lcCdAlert = "";

                                            if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than CorpRequestleg.maxflt
                                                lcCdAlert = "F";// Flight time violation and F is stored in CorpRequestleg.cdalert as a Flight time error
                                            else
                                                lcCdAlert = "";



                                            if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                                lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in CorpRequestleg.cdalert as a Duty time error

                                            // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;

                                            if (llRProblem)
                                                lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in CorpRequestleg.cdalert as a Rest time error 

                                            if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegID != 0 && CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].State != CorporateRequestTripEntityState.Deleted)
                                                CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].State = CorporateRequestTripEntityState.Modified;
                                            CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].FlightHours = (decimal)lnFlt_Hrs;
                                            CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].DutyHours = (decimal)lnDuty_Hrs;
                                            CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].RestHours = (decimal)lnRest_Hrs;
                                            //CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].a = lcCdAlert;                                       


                                            lnLeg_Num = (double)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].LegNUM;

                                            llDutyEnd = false;
                                            if (CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd != null)
                                                llDutyEnd = (bool)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd;

                                            ltGmtArr = (DateTime)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].ArrivalGreenwichDTTM;

                                            if ((bool)CorpRequest.CRLegs[CorpRequest.CRLegs.IndexOf(Leg)].IsDutyEnd)
                                            {
                                                llDutyBegin = true;
                                                lnFlt_Hrs = 0;
                                            }
                                            else
                                            {
                                                llDutyBegin = false;
                                            }

                                            lnOldDuty_hrs = lnDuty_Hrs;
                                            //check next leg if available do the below steps
                                            legcounter++;
                                            if (legcounter < Preflegs.Count)
                                            {
                                                if (llDutyEnd)
                                                {
                                                    lnDuty_Hrs = 0;

                                                    if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                                    {
                                                        TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                                        lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                                        ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                                    }
                                                }
                                                else
                                                {
                                                    lnRest_Hrs = 0;
                                                }
                                            }

                                        }

                                    }
                                }
                            }


                            #endregion

                            #region FlightCost
                            if (CorpRequest.CRLegs != null)
                            {
                                List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                                decimal TotalCost = 0.0M;

                                foreach (CRLeg Leg in Preflegs)
                                {

                                    decimal lnChrg_Rate = 0.0M;
                                    decimal lnCost = 0.0M;
                                    string lcChrg_Unit = string.Empty;

                                    if (CorpRequest.FleetID != null && CorpRequest.FleetID != 0)
                                    {
                                        FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient MasterClient = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                                        var objRetval = MasterClient.GetFleetChargeRateList((long)CorpRequest.FleetID);
                                        if (Leg.DepartureDTTMLocal != null)
                                        {
                                            if (objRetval.ReturnFlag)
                                            {
                                                List<FlightPakMasterService.FleetChargeRate> FleetchargeRate = (from Fleetchrrate in objRetval.EntityList
                                                                                                                where (Fleetchrrate.BeginRateDT <= Leg.ArrivalDTTMLocal
                                                                                                                && Fleetchrrate.EndRateDT >= Leg.DepartureDTTMLocal)
                                                                                                                where Fleetchrrate.FleetID == CorpRequest.FleetID
                                                                                                                select Fleetchrrate).ToList();
                                                if (FleetchargeRate.Count > 0)
                                                {
                                                    lnChrg_Rate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal)FleetchargeRate[0].ChargeRate;
                                                    lcChrg_Unit = FleetchargeRate[0].ChargeUnit;
                                                }
                                            }
                                        }
                                    }

                                    if (lnChrg_Rate == 0.0M)
                                    {
                                        if (CorpRequest.AircraftID != null && CorpRequest.AircraftID > 0)
                                        {
                                            Int64 AircraftID = Convert.ToInt64(CorpRequest.AircraftID);
                                            FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                                            if (Aircraft != null)
                                            {
                                                lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                                                lcChrg_Unit = Aircraft.ChargeUnit;
                                            }
                                        }
                                    }

                                    switch (lcChrg_Unit)
                                    {
                                        case "N":
                                            {
                                                lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate;
                                                break;
                                            }
                                        case "K":
                                            {
                                                lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.852M;
                                                break;
                                            }
                                        case "S":
                                            {
                                                lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.1508M;
                                                break;
                                            }
                                        case "H":
                                            {
                                                lnCost = (Leg.ElapseTM == null ? 0 : (decimal)Leg.ElapseTM) * lnChrg_Rate;
                                                break;
                                            }
                                        default: lnCost = 0.0M; break;

                                    }
                                    //Leg.FlightCost = lnCost;
                                    TotalCost = TotalCost + lnCost;
                                }

                                // CorpRequest.FlightCost = TotalCost;
                            }
                            #endregion
                        }
                    }

                    Session["CurrentCorporateRequest"] = CorpRequest;
                }
            }
        }

        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;


                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;

                }
            }
        }

        private int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dateval))
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }

        protected double RoundElpTime(double lnElp_Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lnElp_Time))
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {

                    decimal ElapseTMRounding = 0;

                    double lnElp_Min = 0.0;
                    double lnEteRound = 0.0;

                    if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                        ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                    if (ElapseTMRounding == 1)
                        lnEteRound = 0;
                    else if (ElapseTMRounding == 2)
                        lnEteRound = 5;
                    else if (ElapseTMRounding == 3)
                        lnEteRound = 10;
                    else if (ElapseTMRounding == 4)
                        lnEteRound = 15;
                    else if (ElapseTMRounding == 5)
                        lnEteRound = 30;
                    else if (ElapseTMRounding == 6)
                        lnEteRound = 60;

                    if (lnEteRound > 0)
                    {

                        lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                        if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                            lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                        else
                            lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                        if (lnElp_Min > 0 && lnElp_Min < 60)
                            lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                        if (lnElp_Min == 0)
                            lnElp_Time = Math.Floor(lnElp_Time);


                        if (lnElp_Min == 60)
                            lnElp_Time = Math.Ceiling(lnElp_Time);

                    }
                }
                return lnElp_Time;
            }
        }

        #endregion
    }
}
