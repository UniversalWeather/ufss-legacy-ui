﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Web.Security;

namespace FlightPak.Web.Framework.Masters
{
    public partial class UnSecured : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBuildVersion();
            }
        }

        /// <summary>
        /// Method to Logout from the Application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            //Clear FormsAuthentication Cookie
            HttpCookie FormsAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            FormsAuthenticationCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(FormsAuthenticationCookie);

            //Clear ASP.NET_SessionId
            HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
            SessionCookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(SessionCookie);

            ViewState.Clear();
            this.ClearChildViewState();

            FormsAuthentication.RedirectToLoginPage();
        }

        /// <summary>
        /// Bind Build Version and Date from XML file.
        /// </summary>
        private void LoadBuildVersion()
        {
            try
            {
                XmlTextReader reader = new XmlTextReader(Server.MapPath("~") + "BuildVersion.xml");

                reader.WhitespaceHandling = WhitespaceHandling.None;
                XmlDocument xmlDoc = new XmlDocument();

                //Load the file into the XmlDocument
                xmlDoc.Load(reader);
                //Close off the connection to the file.
                reader.Close();

                XmlNodeList releaseNo = xmlDoc.GetElementsByTagName("ReleaseNo");
                XmlNodeList buildNo = xmlDoc.GetElementsByTagName("BuildNo");
                XmlNodeList revDate = xmlDoc.GetElementsByTagName("ReleaseDate");

                lbBuildVer.Text = "Release: " + releaseNo[0].InnerText + " | Date: " + revDate[0].InnerText + " | Build No: " + buildNo[0].InnerText;
            }
            catch (Exception)
            {
                //Manually handled
            }
        }
    }
}