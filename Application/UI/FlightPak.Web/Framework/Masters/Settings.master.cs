﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Telerik.Web.UI;
using FlightPak.Web;
using FlightPak.Web.Framework.Prinicipal;
using System.Text;
using FlightPak.Common.Constants;

/// <summary>
/// Code Updation Details
/// Date        Developer       Signature       Reason
/// ----------------------------------------------------------------
/// 24-01-2012  Mohanraja                       Created.
/// </summary>

public partial class Masters_Settings : BaseSecuredMasterPage
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!HaveModuleAccess(ModuleId.Database))
        {
            Response.Redirect("~/Views/Home.aspx?m=Database");
        }

        if (Session[Session.SessionID] != null)
            SetMenuItemVisibility();
        
        //StringBuilder sb = new StringBuilder();
        //sb.Append("<PanelBar>");
        //sb.Append("<Item Text='Company' Font-Bold='true' CssClass='RadNavMenu'>");         
        //sb.Append("<Item Text='Accounts' Value='Accounts' NavigateUrl='../../Views/Settings/Company/AccountsCatalog.aspx?Screen=Accounts'/>");        
        //sb.Append("<Item Text='Budget' Value='Budget' NavigateUrl='../../Views/Settings/Company/BudgetCatalog.aspx?Screen=Budget'/>");        
        //sb.Append("<Item Text='Company Profile' Value='CompanyProfile' NavigateUrl='../../Views/Settings/Company/CompanyProfileCatalog.aspx?Screen=CompanyProfile'/>");        
        //sb.Append("<Item Text='Custom Address' Value='CustomAddress' NavigateUrl='../../Views/Settings/People/CustomerAddressCatalog.aspx?Screen=CustomAddress'/>");        
        //sb.Append("<Item Text='Department Group' Value='DeptGroup' NavigateUrl='../../Views/Settings/Company/DepartmentGroup.aspx?Screen=DeptGroup'/>");        
        //sb.Append("<Item Text='Department/Authorization' Value='DeptAuthorization' NavigateUrl='../../Views/Settings/Company/DepartmentAuthorizationCatalog.aspx?Screen=DeptAuthorization'/>");        
        //sb.Append("<Item Text='Payable Vendor' Value='PayableVendor' NavigateUrl='../../Views/Settings/Logistics/PayableVendor.aspx?Screen=PayableVendor'/>");        
        //sb.Append("<Item Text='Payment Types' Value='PaymentTypes' NavigateUrl='../../Views/Settings/Company/PaymentType.aspx?Screen=PaymentTypes'/>");        
        //sb.Append("<Item Text='SIFL Rate' Value='SIFLRate' NavigateUrl='../../Views/Settings/Company/SiflRateCatalog.aspx?Screen=SIFLRate'/>");        
        //sb.Append("<Item Text='Vendor' Value='Vendor' NavigateUrl='../../Views/Settings/Logistics/VendorCatalog.aspx?Screen=Vendor'/>");        
        //sb.Append("</Item>");        
        //sb.Append(" <Item Text='Root2'>");
        //sb.Append("   <Item Text='Child11'/>");
        //sb.Append("   <Item Text='Child12'/>");
        //sb.Append("   <Item Text='Child13'/>");
        //sb.Append(" </Item>");
        //sb.Append(" <Item Text='Root3'>");
        //sb.Append("   <Item Text='Child11'/>");
        //sb.Append("   <Item Text='Child12'/>");        
        //sb.Append(" </Item>");
        //sb.Append("</PanelBar>");
        //string xmlString = sb.ToString();
        //pnlNavigation1.LoadXml(xmlString);

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        string selectedItemValue = Request.QueryString["Screen"];
        RadPanelItem selectedItem = pnlNavigation.FindItemByValue(Microsoft.Security.Application.Encoder.HtmlEncode(selectedItemValue));

        if (selectedItem != null)
        {
            if (selectedItem.Items.Count > 0)
            {
                selectedItem.Expanded = true;
            }
            else
            {
                selectedItem.Selected = true;
                while ((selectedItem != null) &&
                       (selectedItem.Parent.GetType() == typeof(RadPanelItem)))
                {
                    selectedItem = (RadPanelItem)selectedItem.Parent;
                    selectedItem.Expanded = true;
                }
            }
        }


    }

    private void SetMenuItemVisibility()
    {
        bool isSysAdmin = Convert.ToBoolean(Microsoft.Security.Application.Encoder.HtmlEncode(Convert.ToString(UserPrincipal.Identity._isSysAdmin)));
        RadPanelItem selectedItem = pnlNavigation.FindItemByValue("UserAdmin");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("MaintainGroupsAndAccess");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("ChangeAccount");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("ChangePassenger");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("ChangeCrewCode");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("ChangeTail");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("ChangeCQCustomer");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("ChangeAirportICAO");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("CleanUPUtility");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("RecoverDeletedTrips");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("LoggedInUsers");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByValue("RecordLockManager");
        selectedItem.Visible = isSysAdmin;

        selectedItem = pnlNavigation.FindItemByText("Charter");
        selectedItem.Visible = HaveModuleAccess(ModuleId.CharterQuote);
        
        if (!UserPrincipal.Identity._isSysAdmin)
        {
            selectedItem = pnlNavigation.FindItemByValue("Accounts");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewAccounts);

            selectedItem = pnlNavigation.FindItemByValue("Budget");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewBudgetCatalog);

            selectedItem = pnlNavigation.FindItemByValue("CompanyProfile");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCompanyProfileCatalog);

            selectedItem = pnlNavigation.FindItemByValue("CustomAddress");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCustomerAddressCatalog);

            selectedItem = pnlNavigation.FindItemByValue("DeptGroup");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewDepartmentGroup);

            selectedItem = pnlNavigation.FindItemByValue("DeptAuthorization");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewDepartmentAuthorizationCatalog);

            selectedItem = pnlNavigation.FindItemByValue("PayableVendor");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewPayableVendor);

            selectedItem = pnlNavigation.FindItemByValue("PaymentTypes");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewPaymentType);

            selectedItem = pnlNavigation.FindItemByValue("SIFLRate");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewSiflRateCatalog);

            selectedItem = pnlNavigation.FindItemByValue("Vendor");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewVendorCatalog);

            selectedItem = pnlNavigation.FindItemByValue("AircraftDutyType");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewAircraftDutyType);

            selectedItem = pnlNavigation.FindItemByValue("AircraftType");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewAircraftType);

            selectedItem = pnlNavigation.FindItemByValue("DelayType");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewDelayTypeCatalog);

            selectedItem = pnlNavigation.FindItemByValue("FleetGroup");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFleetGroupCatalog);

            selectedItem = pnlNavigation.FindItemByValue("FleetProfile");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFleetProfile);

            selectedItem = pnlNavigation.FindItemByValue("FleetProfileAdditionalInfoCatalog");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFleetProfileAdditionalInfoCatalog);

            selectedItem = pnlNavigation.FindItemByValue("FlightCategories");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFlightCategory);

            selectedItem = pnlNavigation.FindItemByValue("FlightPurpose");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFlightPurpose);

            selectedItem = pnlNavigation.FindItemByValue("Client");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewClientCode);

            selectedItem = pnlNavigation.FindItemByValue("CrewAddInfo");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCrewAdditionalInformation);

            selectedItem = pnlNavigation.FindItemByValue("CrewChecklist");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCrewCheckListCatalog);

            selectedItem = pnlNavigation.FindItemByValue("CrewDutyRules");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCrewDutyRulesCatalog);

            selectedItem = pnlNavigation.FindItemByValue("CrewDutyTypes");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCrewDutyTypes);

            selectedItem = pnlNavigation.FindItemByValue("CrewGroup");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCrewGroupCatalog);

            selectedItem = pnlNavigation.FindItemByValue("CrewRoster");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCrewRoster);

            selectedItem = pnlNavigation.FindItemByValue("EmergencyContact");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewEmergencyContactCatalog);

            selectedItem = pnlNavigation.FindItemByValue("PassengerAddInfo");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewPassengerAdditionalInfo);

            selectedItem = pnlNavigation.FindItemByValue("PassengerGroup");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewPassengerGroupCatalog);

            selectedItem = pnlNavigation.FindItemByValue("PassengerRequestor");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewPassengerRequestor);

            selectedItem = pnlNavigation.FindItemByValue("TravelCoordinator");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewTravelCoordinator);

            selectedItem = pnlNavigation.FindItemByValue("Airport");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewAirport);

            selectedItem = pnlNavigation.FindItemByValue("Country");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCountryCatalog);

            selectedItem = pnlNavigation.FindItemByValue("DSTRegion");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewDaylightSavingTimeRegion);

            selectedItem = pnlNavigation.FindItemByValue("ExchangeRate");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewExchangeRatesCatalog);

            selectedItem = pnlNavigation.FindItemByValue("FuelLocator");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFuelLocator);

            selectedItem = pnlNavigation.FindItemByValue("FuelVendor");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFuelVendor);

            selectedItem = pnlNavigation.FindItemByValue("Metro");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewMetro);

            selectedItem = pnlNavigation.FindItemByValue("TripMgrChkLst");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewTripManagerCheckListCatalog);

            selectedItem = pnlNavigation.FindItemByValue("TripMgrChkLstGrp");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewTripManagerCheckListGroupCatalog);

            selectedItem = pnlNavigation.FindItemByValue("FeeSchedule");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFeeSchedule);

            selectedItem = pnlNavigation.FindItemByValue("FeeScheduleGroup");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewFeeGroup);

            selectedItem = pnlNavigation.FindItemByValue("LeadSource");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewLeadSourceCatalog);

            selectedItem = pnlNavigation.FindItemByValue("Salesperson");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewSalesPersonCatalog);

            selectedItem = pnlNavigation.FindItemByValue("CharterQuote");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewCQCustomerCatalog);
    
            selectedItem = pnlNavigation.FindItemByValue("LostBusiness");
            selectedItem.Visible = IsAuthorized(Permission.Database.ViewLostBusiness);

        }
    }
    
}
