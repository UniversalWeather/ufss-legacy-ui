﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using FlightPak.Common.Constants;
using Telerik.Web.UI;
using System.Web.UI.WebControls;

namespace FlightPak.Web.Framework.Masters
{
    public partial class Preflight : BaseSecuredMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.Preflight))
            {
                Response.Redirect("~/Views/Home.aspx?m=Preflight");
            }
        }
    }
}
