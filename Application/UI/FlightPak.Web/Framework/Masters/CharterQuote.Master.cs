﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
//using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.CharterQuoteService;
using FlightPak.Web.CalculationService;
using System.Globalization;
using FlightPak.Common.Constants;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Text.RegularExpressions;
using FlightPak.Web.FlightPakMasterService;
using System.Web;

namespace FlightPak.Web.Framework.Masters
{
    public partial class CharterQuote : BaseSecuredMasterPage
    {
        #region Declarations
        public string CQSessionKey = "CQFILE";          // Maintain whole FILE 
        public string CQException = "CQEXCEPTION";      // Maintain Session Variable for CQ Exception List
        public string CQExceptionTempatelist = "CQExceptionTemplateList"; //All ExceptionTemplate list
        public string CQSelectedQuoteNum = "CQSELECTEDQUOTENUM"; // Maintain for selected Quote Number after save scenario.
        public string CQFileImage = "CQQUOTEONFILEDICIMG"; // Maintain for File Uploads in Quotes

        public CQMain QuoteInProgress = new CQMain();

        private ExceptionManager exManager;
        List<CQException> ExceptionList = new List<CQException>();

        private delegate void SaveSession();
        private delegate void SavePAXSession();

        private Delegate _SaveToSession;
        public Delegate SaveToSession
        {
            set { _SaveToSession = value; }
        }

        private Delegate _SavePAXToSession;
        public Delegate SavePAXToSession
        {
            set { _SavePAXToSession = value; }
        }

        public event AjaxRequestHandler RadAjax_AjaxRequest;
        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }
        public System.Web.UI.HtmlControls.HtmlGenericControl MasterForm { get { return this.DivMasterForm; } }
        public RadAjaxManager RadAjaxManagerMaster { get { return this.RadAjaxManager1; } }

        private delegate void LoadSelectedQuote();
        private delegate void NewFile();
        private delegate void EditFile();
        private delegate void TransferFile();



        public CQFile FileRequest = new CQFile();

        const decimal statuateConvFactor = 1.1508M;
        public RadGrid TrackerLegs
        {
            get { return this.dgLegs; }
        }

        public delegate void ButtonClickEventHandler(object sender, EventArgs e);

        public event ButtonClickEventHandler _DeleteClick;
        public event ButtonClickEventHandler _SaveClick;
        public event ButtonClickEventHandler _CancelClick;

        // Changes for Legnum in CQ Header
        public Label FloatLegNum
        {
            get { return this.lblFloatLegNum; }
        }

        #endregion

        #region Comments
        //Defect : 2565 Fix by Prabhu: 1/17/2014
        //Exceptions is not showing properly for quote.

        //Defect:  2955  The system shall have the ability to display the aircraft description next to the Tail Number in the header area
        //Fixed By Prabhu. 06/02/2014.

        #endregion
        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "File";

                        RadDatePicker1.DateInput.DateFormat = "MM/dd/yyyy";
                        RadDatePicker1.DateInput.DisplayDateFormat = "MM/dd/yyyy";
                        DatePicker.DateInput.DateFormat = "MM/dd/yyyy";
                        DatePicker.DateInput.DisplayDateFormat = "MM/dd/yyyy";

                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }
                        if (HttpContext.Current.Request.Cookies["CharterQuoteUpdate"] != null)
                        {
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "ShowSuccessMessage", "ShowSuccessMessage()", true);
                                HttpContext.Current.Response.Cookies.Remove("CharterQuoteUpdate");
                                HttpContext.Current.Response.Cookies["CharterQuoteUpdate"].Expires = DateTime.Now.AddDays(-1);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
					
					  if (Request.QueryString["seltab"] != null)
                        {
                            string selectedItemValue = Request.QueryString["seltab"];
                            if (selectedItemValue == "CQRW")
                            {
                                pnlTracker.Visible = false;
                                pnlException.Visible = false;
								CQSearch.Visible = false;
                            }
                        }
                        // Set Master Page Permissions
                        ApplyMasterPagePermissions();

                        // Set Floating Control Values
                        lbFileNum.Visible = false;
                        lbQuoteNum.Visible = false;
                        lbCustomerName.Visible = false;
                        lbTailNum.Visible = false;
                        //Defect: 2955 
                        lbFloatAircraft.Visible = false;

                        // Fix for UW-987 (7422)
                        this.CQSearch.lnkPreviewReport.Visible = false;

                        if (Session[CQSessionKey] != null)
                        {
                            CQFile FileRequest = (CQFile)Session[CQSessionKey];
                            if (FileRequest.FileNUM != null)
                            {
                                lbFileNum.Visible = true;
                                lbFileNum.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.FileNUM.ToString());
                            }

                            if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                            {
                                if (FileRequest.QuoteNumInProgress == null || (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress == 0))
                                    FileRequest.QuoteNumInProgress = 1;

                                if (FileRequest.QuoteNumInProgress != null)
                                {
                                    lbQuoteNum.Visible = true;
                                    lbQuoteNum.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.QuoteNumInProgress.ToString());
                                }
                                else
                                    lbQuoteNum.Text = string.Empty;
                            }

                            if (FileRequest.CQCustomerName != null)
                            {
                                lbCustomerName.Visible = true;
                                lbCustomerName.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.CQCustomerName);
                            }
                            else
                                lbCustomerName.Text = string.Empty;

                            if (FileRequest.CQFileDescription != null)
                            {
                                lbDescription.Visible = true;
                                lbDescription.Text = System.Web.HttpUtility.HtmlEncode(FileRequest.CQFileDescription);
                            }
                            else
                                lbDescription.Text = string.Empty;

                            if (FileRequest != null)
                            {
                                if (FileRequest.CQMains != null)
                                {
                                    Int64 QuoteNumInProgress = 1;

                                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                        QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                                    QuoteInProgress = FileRequest.CQMains.Where(x => x.IsDeleted == false && x.QuoteNUM == QuoteNumInProgress).SingleOrDefault();

                                    if (QuoteInProgress != null)
                                    {
                                        if (QuoteInProgress.FleetID != null && QuoteInProgress.FleetID != 0)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                                var objRetVal = objDstsvc.GetFleetProfileList().EntityList.Where(x => x.FleetID == QuoteInProgress.FleetID && x.IsDeleted == false);
                                                if (objRetVal.Count() > 0)
                                                {
                                                    Fleetlist = objRetVal.ToList();
                                                    if (Fleetlist != null)
                                                    {
                                                        if (Fleetlist[0].TailNum != null)
                                                        {
                                                            //veracode fix for 2921
                                                            string tailnum = string.Empty;
                                                            tailnum = Fleetlist[0].TailNum;
                                                            lbTailNum.Visible = true;
                                                            lbTailNum.Text = System.Web.HttpUtility.HtmlEncode(tailnum);
                                                            this.CQSearch.TailNUM = Fleetlist[0].TailNum;
                                                        }
                                                        else
                                                            lbTailNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                    }
                                                }
                                            }
                                        }

                                        //Defect: 2955 
                                        if (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID != 0)
                                        {
                                            using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                            {
                                                FlightPak.Web.FlightPakMasterService.Aircraft Aircarftlist = new FlightPakMasterService.Aircraft();

                                                Aircarftlist = GetAircraft((long)QuoteInProgress.AircraftID);

                                                if (Aircarftlist != null)
                                                {
                                                    lbFloatAircraft.Visible = true;
                                                    lbFloatAircraft.Text = System.Web.HttpUtility.HtmlEncode(Aircarftlist.AircraftCD);
                                                    this.CQSearch.TypeCode = Aircarftlist.AircraftCD;
                                                }
                                                else
                                                {
                                                    lbFloatAircraft.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                                                    this.CQSearch.TypeCode = string.Empty;
                                                    lbFloatAircraft.Visible = false;
                                                }

                                            }
                                        }
                                    }
                                }

                                if (FileRequest.Mode == CQRequestActionMode.Edit)
                                {
                                    // To Disable Reports Tab, While Edit Button Clicked.
                                    if (this.CQHeader.HeaderTab.SelectedTab.Text != null && this.CQHeader.HeaderTab.SelectedTab.Text == "Reports")
                                    {
                                        for (int i = 0; i <= 4; i++)
                                            this.CQHeader.HeaderTab.Tabs[i].Enabled = false;
                                    }
                                    else
                                    {
                                        this.CQHeader.HeaderTab.Tabs[5].Enabled = false;
                                    }
                                }
                            }

                            // Fix for UW-987 (7422)
                            string reportName = string.Empty;
                            if (Session["CQQuoteReport"] != null && Session["CQQuoteReport"].ToString() == "IR")
                            {
                                reportName = "RptCQInvoice";
                                this.CQSearch.ReportToolTip = "Preview Invoice Report";
                            }
                            else
                            {
                                reportName = "RptCQQuote";
                                this.CQSearch.ReportToolTip = "Preview Quote Report";
                            }

                            if (FileRequest != null)
                            {
                                if (FileRequest.Mode == CQRequestActionMode.NoChange)
                                {
                                    if (Request.QueryString["seltab"] != null && Request.QueryString["seltab"].ToString().ToLower() == "reports")
                                    {
                                        this.CQSearch.lnkPreviewReport.Visible = true;
                                        this.CQSearch.ReportName = reportName; // "RptCQQuote";
                                        this.CQSearch.UserName = UserPrincipal.Identity._name;
                                    }
                                }
                                else
                                {
                                    if (Request.QueryString["seltab"] != null && Request.QueryString["seltab"].ToString().ToLower() == "reports")
                                    {
                                        this.CQSearch.lnkPreviewReport.Visible = true;
                                        this.CQSearch.ReportName = reportName; // "RptCQQuote";
                                        this.CQSearch.UserName = UserPrincipal.Identity._name;
                                    }
                                }
                            }
                            else
                            {
                                if (Request.QueryString["seltab"] != null && Request.QueryString["seltab"].ToString().ToLower() == "reports")
                                {
                                    this.CQSearch.lnkPreviewReport.Visible = true;
                                    this.CQSearch.ReportName = reportName; // "RptCQQuote";
                                    this.CQSearch.UserName = UserPrincipal.Identity._name;
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HaveModuleAccess(ModuleId.CharterQuote))
            {
                Response.Redirect("~/Views/Home.aspx?m=CharterQuote");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SaveSession SaveCQSession = new SaveSession(SaveToCharterQuoteSession);
                        this.CQHeader.SaveToSession = SaveCQSession;
                        SavePAXSession SavePAXToSession = new SavePAXSession(SavePAXToCharterQuoteSession);
                        this.CQHeader.SaveToSessionPAX = SavePAXToSession;
                        btnSaveFile.Enabled = true;
                        //LoadSelectedTrip LoadTriptoSession = new LoadSelectedTrip(_LoadTriptoSession);
                        //this.PreflightSearch.SelectTrip = LoadTriptoSession;

                        if (Session[CQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[CQSessionKey];
                            if (FileRequest.CQMains != null && FileRequest.CQMains.Where(x => x.IsDeleted == false).Count() > 0)
                                QuoteInProgress = getQuoteInProgress(FileRequest);
                        }
                        SetQuoteMode();

                        if (!IsPostBack)
                        {
                            #region Authorizatioin

                            CheckAutorization(Permission.CharterQuote.ViewCQFile);

                            if (!IsAuthorized(Permission.CharterQuote.AddCQFile))
                                btnNewFile.Visible = false;

                            if (!IsAuthorized(Permission.CharterQuote.EditCQFile))
                                btnEditFile.Visible = false;

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQExpressQuote))
                                btnExpQuote.Visible = false;

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQFile))
                            {
                                RadTab selectedTab = this.CQHeader.HeaderTab.Tabs.FindTabByText("File");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQQuotesOnFile))
                            {
                                RadTab selectedTab = this.CQHeader.HeaderTab.Tabs.FindTabByText("Quote On File");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQLeg))
                            {
                                RadTab selectedTab = this.CQHeader.HeaderTab.Tabs.FindTabByText("Quote & Leg Details");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQPAXManifest))
                            {
                                RadTab selectedTab = this.CQHeader.HeaderTab.Tabs.FindTabByText("PAX");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                            {
                                RadTab selectedTab = this.CQHeader.HeaderTab.Tabs.FindTabByText("Logistics");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.CharterQuote.ViewCQReportWriter))
                            {
                                RadTab selectedTab = this.CQHeader.HeaderTab.Tabs.FindTabByText("CQ Report Writer");
                                selectedTab.Visible = false;
                            }

                            #endregion Authorizatioin
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void ExpressQuote_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Response.Redirect("ExpressQuote.aspx", false);
                        CQHeader.Visible = false;
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        public void DisableControl(string ctrlName, bool isVisible)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlName, isVisible))
            {
                if (ctrlName == "CQHeader")
                {
                    CQHeader.Visible = isVisible;
                }
            }
        }

        protected void SaveToCharterQuoteSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SaveToSession != null)
                    _SaveToSession.DynamicInvoke();
            }
        }

        protected void SavePAXToCharterQuoteSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SavePAXToSession != null)
                    _SavePAXToSession.DynamicInvoke();
            }
        }

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (RadAjax_AjaxRequest != null)
                {
                    RadAjax_AjaxRequest(sender, e);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                //if (RadAjax_AjaxRequest != null)
                //{
                //    RadAjax_AjaxRequest(sender, e);
                //}
            }
        }

        protected void NewFile_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Clears an Existing Session
                        ClearCQSessions();

                        Session.Remove(CQSessionKey);

                        // Set State for New File
                        CQFile FileRequest = new CQFile();
                        FileRequest.State = CQRequestEntityState.Added;
                        FileRequest.EstDepartureDT = DateTime.Now;
                        FileRequest.QuoteDT = DateTime.Now;
                        FileRequest.Mode = CQRequestActionMode.Edit;
                        FileRequest.HomebaseID = UserPrincipal.Identity._homeBaseId;
                        FileRequest.HomeBaseAirportICAOID = UserPrincipal.Identity._airportICAOCd;
                        FileRequest.HomeBaseAirportID = UserPrincipal.Identity._airportId;
                        FlightPak.Web.CharterQuoteService.Company companyObj = new FlightPak.Web.CharterQuoteService.Company() { HomebaseID = (long)UserPrincipal.Identity._homeBaseId, BaseDescription = UserPrincipal.Identity._fpSettings._BaseDescription, HomebaseAirportID = UserPrincipal.Identity._airportId };
                        FlightPak.Web.CharterQuoteService.Airport icaoCD = new FlightPak.Web.CharterQuoteService.Airport() { IcaoID = UserPrincipal.Identity._airportICAOCd, AirportID = (long)UserPrincipal.Identity._airportId, AirportName = UserPrincipal.Identity._airportName };
                        companyObj.Airport = icaoCD;
                        FileRequest.Company = companyObj;

                        Session[CQSessionKey] = FileRequest;

                        Response.Redirect("CharterQuoteRequest.aspx?seltab=File", false);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        public void EditFile_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session[CQSessionKey] != null)
                        {
                            CQFile FileRequest = (CQFile)Session[CQSessionKey];
                            if (FileRequest != null && FileRequest.CQFileID > 0)
                            {
                                using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                {
                                    var returnValue = CommonService.Lock(FileRequest.EntityKey.EntitySetName, FileRequest.CQFileID);
                                    if (returnValue != null && !returnValue.ReturnFlag)
                                    {
                                        RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "' ) ;");
                                    }
                                    else
                                    {
                                        FileRequest.Mode = CQRequestActionMode.Edit;
                                        FileRequest.State = CQRequestEntityState.Modified;

                                        ControlEnable("Button", btnNewFile, false, "ui_nav_disable");
                                        ControlEnable("Button", btnEditFile, false, "ui_nav_disable");
                                        ControlEnable("Button", btnExpQuote, false, "ui_nav_disable");

                                        Session[CQSessionKey] = FileRequest;
                                    }
                                }

                                // To Disable Reports Tab, While Edit Button Clicked.
                                if (this.CQHeader.HeaderTab.SelectedTab.Text == "Reports")
                                    Response.Redirect("CharterQuoteRequest.aspx?seltab=File");
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void DeleteFile_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (_DeleteClick != null)
                            _DeleteClick.DynamicInvoke(sender, e);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void CancelFile_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (_CancelClick != null)
                            _CancelClick.DynamicInvoke(sender, e);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void SaveFile_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (_SaveClick != null)
                            _SaveClick.DynamicInvoke(sender, e);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        /// <summary>
        /// Method to Clear the Charter Quote Session Variables
        /// </summary>
        public void ClearCQSessions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Session.Remove(CQSessionKey);
                Session.Remove(CQException);
                Session.Remove(CQExceptionTempatelist);
                Session.Remove(CQSelectedQuoteNum);
            }
        }

        /// <summary>
        /// Method to Apply Permission for Master Page Controls
        /// </summary>
        public void ApplyMasterPagePermissions()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[CQSessionKey];
                    if (FileRequest.State == CQRequestEntityState.Added || FileRequest.State == CQRequestEntityState.Modified) // Add Mode or Edit Mode
                    {
                        ControlEnable("Button", btnNewFile, false, "ui_nav_disable");
                        ControlEnable("Button", btnEditFile, false, "ui_nav_disable");
                        ControlEnable("Button", btnExpQuote, false, "ui_nav_disable");
                        ControlEnable("Button", btnDeleteFile, false, "ui_nav_disable");
                        ControlEnable("Button", btnSaveFile, true, "ui_nav");
                        ControlEnable("Button", btnCancelFile, true, "ui_nav");
                    }
                    else
                    {
                        ControlEnable("Button", btnNewFile, true, "ui_nav");
                        ControlEnable("Button", btnEditFile, true, "ui_nav");
                        ControlEnable("Button", btnExpQuote, true, "ui_nav");
                        ControlEnable("Button", btnDeleteFile, true, "ui_nav");
                        ControlEnable("Button", btnSaveFile, false, "ui_nav_disable");
                        ControlEnable("Button", btnCancelFile, false, "ui_nav_disable");
                    }
                }
                else
                {
                    ControlEnable("Button", btnNewFile, true, "ui_nav");
                    ControlEnable("Button", btnEditFile, false, "ui_nav_disable");
                    ControlEnable("Button", btnExpQuote, true, "ui_nav");
                    ControlEnable("Button", btnDeleteFile, false, "ui_nav_disable");
                    ControlEnable("Button", btnSaveFile, false, "ui_nav_disable");
                    ControlEnable("Button", btnCancelFile, false, "ui_nav_disable");
                }
            }
        }

        private void ControlEnable(string ctrlType, Control ctrlName, bool isEnable, string cssClass)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ctrlType, ctrlName, isEnable))
            {
                switch (ctrlType)
                {
                    case "Button":
                        Button btn = (Button)ctrlName;
                        btn.Enabled = isEnable;
                        if (!string.IsNullOrEmpty(cssClass))
                            btn.CssClass = cssClass;
                        break;
                    case "RadPanelBar":
                        RadPanelBar pnl = (RadPanelBar)ctrlName;
                        if (!string.IsNullOrEmpty(cssClass))
                            pnl.CssClass = cssClass;
                        pnl.Enabled = isEnable;
                        break;
                    default: break;
                }
            }
        }

        public void Cancel()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                RadAjaxManagerMaster.ResponseScripts.Add(@"radconfirm('Are you sure want to Cancel?',confirmCancelCallBackFn, 330, 110,null,'Confirmation!');");
            }
        }

        /// <summary>
        /// Method to Navigate the Page based on the Tab Name
        /// </summary>
        /// <param name="TabName">Pass Current Tab Name</param>
        public void Next(string TabName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TabName))
            {
                int tabindex = 0;
                tabindex = this.CQHeader.HeaderTab.Tabs.FindTabByText(TabName).Index;
                for (int i = tabindex + 1; i <= this.CQHeader.HeaderTab.Tabs.Count - 1; i++)
                {
                    if (this.CQHeader.HeaderTab.Tabs[i].Visible == true && this.CQHeader.HeaderTab.Tabs[i].Enabled == true)
                        Response.Redirect(this.CQHeader.HeaderTab.Tabs[i].Value, true);
                }
            }
        }

        public void Delete(Int64 fileNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileNum))
            {
                RadAjaxManagerMaster.ResponseScripts.Add(@"radconfirm('Warning - You are about to delete the Charter Quote File No. " + fileNum + " Are you sure you want to delete this record?',confirmDeleteCallBackFn, 330, 110,null,'Confirmation!');");
            }
        }

        protected void CancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                        {
                            CQFile FileRequest = (CQFile)Session[CQSessionKey];

                            SetQuoteModeToNoChange(ref FileRequest);

                            using (CharterQuoteServiceClient CQService = new CharterQuoteServiceClient())
                            {
                                if (FileRequest.CQFileID != 0)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(FileRequest.EntityKey.EntitySetName, FileRequest.CQFileID);
                                    }
                                    var objCQFile = CQService.GetCQRequestByID(FileRequest.CQFileID, false);
                                    if (objCQFile.ReturnFlag)
                                    {
                                        FileRequest = objCQFile.EntityList[0];

                                        if (FileRequest.CQMains != null)
                                        {
                                            foreach (CQMain quote in FileRequest.CQMains)
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                    if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                    {
                                                        foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                        {
                                                            CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                            objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                            objImageFile.RecordType = "CQQuoteOnFile";
                                                            objImageFile.UWAFileName = fwh.UWAFileName;
                                                            objImageFile.RecordID = quote.CQMainID;
                                                            objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                            objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                            objImageFile.IsDeleted = false;
                                                            if (quote.ImageList == null)
                                                                quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                            quote.ImageList.Add(objImageFile);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        Session[CQSessionKey] = FileRequest;
                                        //// Uncomment the below line of code, once CQ Exception table is ready
                                        //var ObjRetval = CQService.GetQuoteExceptionList(FileRequest.CQFileID);
                                        //if (ObjRetval.ReturnFlag)
                                        //{
                                        //    Session[CQException] = ObjRetval.EntityList;
                                        //}
                                        //else
                                        //    Session[CQException] = null;
                                    }
                                    else
                                    {
                                        FileRequest = null;
                                        Session.Remove(CQException);
                                    }
                                }
                                else
                                {
                                    FileRequest = null;
                                    Session.Remove(CQSessionKey);
                                    Session.Remove(CQException);

                                    RedirectToMain();
                                }
                            }

                            SetQuoteMode();
                            RadAjaxManagerMaster.ResponseScripts.Add(@"window.location=window.location;");
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void CancelNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void RedirectToFile_Click(object sender, EventArgs e)
        {
            Response.Redirect("CharterQuoteRequest.aspx?seltab=File");
        }

        protected void btnRedirectToQuote_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuoteOnFile.aspx?seltab=QuoteOnFile");
        }

        protected void DeleteYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                        {
                            CheckAutorization(Permission.CharterQuote.DeleteCQFile);

                            // UnComment when Delete file method business completed..
                            var ReturnValue = Service.DeleteFile(FileRequest.CQFileID);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                Session.Remove(CQException);
                                Session.Remove(CQSessionKey);
                                RedirectToMain();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }
        }

        protected void DeleteNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void SetQuoteModeToNoChange(ref CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                if (fileRequest != null)
                {
                    fileRequest.Mode = CQRequestActionMode.NoChange;
                    fileRequest.State = CQRequestEntityState.NoChange;

                    if (fileRequest.CQMains != null)
                    {
                        foreach (CQMain quote in fileRequest.CQMains)
                        {
                            quote.State = CQRequestEntityState.NoChange;
                            if (quote.CQLegs != null)
                            {
                                foreach (CQLeg leg in quote.CQLegs)
                                {
                                    leg.State = CQRequestEntityState.NoChange;

                                }
                            }
                        }
                    }
                }
            }
        }

        private void RedirectToMain()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Response.Redirect("CharterQuoteRequest.aspx?seltab=File");
            }
        }

        protected void SetQuoteMode()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[CQSessionKey];

                    if (FileRequest.FileNUM != null)
                        this.CQSearch.FileNUM = Convert.ToInt32(FileRequest.FileNUM);

                    if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                        this.CQSearch.QuoteNUM = FileRequest.QuoteNumInProgress;

                    if (QuoteInProgress != null)
                    {

                    }


                    if (FileRequest.LastUpdTS != DateTime.MinValue && FileRequest.LastUpdTS != null)
                    {
                        DateTime LastModifiedUTCDate = DateTime.Now;
                        if (UserPrincipal.Identity._airportId != null)
                        {
                            using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                            {
                                LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)FileRequest.LastUpdTS, false, false);
                            }
                        }
                        else
                            LastModifiedUTCDate = (DateTime)FileRequest.LastUpdTS;
                        this.CQSearch.LastModified.Text = System.Web.HttpUtility.HtmlEncode("Last Modified: " + String.Format(CultureInfo.InvariantCulture, "{1} {0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", LastModifiedUTCDate, FileRequest.LastUpdUID));
                    }
                    else
                        this.CQSearch.LastModified.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);

                    if (FileRequest.Mode == CQRequestActionMode.Edit)
                    {
                        ControlEnable("Button", btnNewFile, false, "ui_nav_disable");
                        ControlEnable("Button", btnEditFile, false, "ui_nav_disable");
                        ControlEnable("Button", btnExpQuote, false, "ui_nav_disable");
                    }
                    else
                    {
                        ControlEnable("Button", btnNewFile, true, "ui_nav");
                        ControlEnable("Button", btnEditFile, true, "ui_nav");
                        ControlEnable("Button", btnExpQuote, true, "ui_nav");
                    }

                }
                else
                {
                    ControlEnable("Button", btnNewFile, true, "ui_nav");
                    ControlEnable("Button", btnEditFile, true, "ui_nav");
                    ControlEnable("Button", btnExpQuote, true, "ui_nav");
                }
            }
        }

        /// <summary>
        /// Common method used to save the whole trip into Database
        /// </summary>
        /// <param name="fileRequest">Pass File Object</param>
        public void Save(CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                SaveFile(fileRequest);
            }
        }

        public void SaveFile(CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                if (fileRequest != null)
                {
                    fileRequest.AllowFileToSave = false;

                    using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                    {
                        if (fileRequest.CQMains != null && fileRequest.CQMains.Count > 0)
                        {
                            foreach (CQMain cqMain in fileRequest.CQMains.Where(x => x.IsDeleted == false))
                            {
                                if (cqMain.CQLegs != null && cqMain.CQLegs.Count > 0)
                                {
                                    foreach (CQLeg cqleg in cqMain.CQLegs)
                                    {
                                        CQchoice(cqleg);
                                    }
                                }
                            }
                        }

                        #region Exception Validation

                        List<CQException> eSeverityException = new List<CQException>();
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        ExceptionTemplateList = GetExceptionTemplateList();

                        FileRequest.AllowFileToSave = true;
                        ExceptionList = null;
                        bool isFileExp = false;

                        if (ExceptionTemplateList != null && ExceptionTemplateList.Count > 0)
                        {
                            if (FileRequest.HomebaseID == null || FileRequest.HomebaseID == 0)
                            {
                                CQException Fileexp = new CQException();
                                Fileexp.CQExceptionID = 0;
                                Fileexp.CQExceptionDescription = "Homebase is Required";
                                Fileexp.ExceptionTemplateID = (long)CQBusinessErrorReference.HomebaseReq;
                                if (ExceptionList == null)
                                    ExceptionList = new List<CQException>();
                                ExceptionList.Add(Fileexp);

                                FileRequest.AllowFileToSave = false;

                                isFileExp = true;
                            }

                            if (FileRequest.EstDepartureDT == null)
                            {
                                CQException Fileexp = new CQException();
                                Fileexp.CQExceptionID = 0;
                                Fileexp.CQExceptionDescription = "Trip Date is Required";
                                Fileexp.ExceptionTemplateID = (long)CQBusinessErrorReference.EstDepartureDate;
                                if (ExceptionList == null)
                                    ExceptionList = new List<CQException>();
                                ExceptionList.Add(Fileexp);

                                FileRequest.AllowFileToSave = false;
                                isFileExp = true;
                            }

                            if (FileRequest.QuoteDT == null)
                            {
                                CQException Fileexp = new CQException();
                                Fileexp.CQExceptionID = 0;
                                Fileexp.CQExceptionDescription = "Quote Date is Required";
                                Fileexp.ExceptionTemplateID = (long)CQBusinessErrorReference.QuoteDate;
                                if (ExceptionList == null)
                                    ExceptionList = new List<CQException>();
                                ExceptionList.Add(Fileexp);
                                FileRequest.AllowFileToSave = false;
                                isFileExp = true;
                            }

                            if (FileRequest.CQMains != null)
                            {
                                foreach (CQMain cqMain in FileRequest.CQMains.Where(x => x.IsDeleted == false).ToList())
                                {
                                    var ExceptionlistforMandate = Service.ValidateQuoteforMandatoryExcep(cqMain);

                                    if (ExceptionlistforMandate.ReturnFlag)
                                    {
                                        if (ExceptionList == null)
                                            ExceptionList = new List<CharterQuoteService.CQException>();

                                        ExceptionList.AddRange(ExceptionlistforMandate.EntityList); ;



                                        if (ExceptionList != null && ExceptionList.Count > 0)
                                        {
                                            eSeverityException = (from Exp in ExceptionList
                                                                  join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                                  where ExpTempl.Severity == 1
                                                                  select Exp).ToList();

                                            if (eSeverityException != null && eSeverityException.Count > 0)
                                            {
                                                FileRequest.AllowFileToSave = false;

                                                FileRequest.QuoteNumInProgress = (int)cqMain.QuoteNUM;
                                                break;
                                            }
                                        }
                                    }

                                }
                            }

                            if (!FileRequest.AllowFileToSave)
                            {

                                List<CQException> OldExceptionList = new List<CQException>();
                                OldExceptionList = (List<CQException>)Session[CQException];

                                if (OldExceptionList != null && OldExceptionList.Count > 0)
                                {

                                    foreach (CQException OldMandatoryException in OldExceptionList.ToList())
                                    {
                                        if (OldMandatoryException.CQExceptionID == 0)
                                            OldExceptionList.Remove(OldMandatoryException);
                                    }
                                }

                                if (ExceptionList != null && ExceptionList.Count > 0)
                                {
                                    if (OldExceptionList == null)
                                        OldExceptionList = new List<CQException>();
                                    OldExceptionList.AddRange(ExceptionList);
                                }
                                Session[CQException] = OldExceptionList;

                                if (isFileExp)
                                    RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('File or Quote has mandatory exceptions, File is not Saved', 330, 110,'" + ModuleNameConstants.CharterQuote.CQOnFile + "',alertCallBackFileExp);");
                                else
                                    RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('File or Quote has mandatory exceptions, File is not Saved', 330, 110,'" + ModuleNameConstants.CharterQuote.CQOnFile + "',alertCallBackQuoteExp);");
                            }
                        }
                        #endregion


                        if (fileRequest.AllowFileToSave)
                        {

                            CQFile RetainFile = new CQFile();
                            RetainFile = fileRequest;

                            ClearChildProperties(ref fileRequest);
                            var ReturnValue = Service.SaveFileRequest(fileRequest);

                            if (ReturnValue.ReturnFlag == true)
                            {
                                FileRequest = ReturnValue.EntityInfo;

                                #region File Upload in Quote

                                if (FileRequest.CQMains != null)
                                {
                                    foreach (CQMain quote in FileRequest.CQMains)
                                    {
                                        CQMain retainQuote = RetainFile.CQMains.Where(x => x.QuoteNUM == quote.QuoteNUM && x.IsDeleted == false).SingleOrDefault();

                                        if (retainQuote != null && retainQuote.ImageList != null)
                                        {
                                            // Delete all images for quote using quote.cqmainid

                                            if (quote.CQMainID != null && quote.CQMainID != 0)
                                                Service.DeleteQuoteImageFiles(quote.CQMainID);

                                            foreach (CharterQuoteService.FileWarehouse file2Save in retainQuote.ImageList.Where(x => x.IsDeleted == false))
                                            {
                                                using (FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                {
                                                    FlightPakMasterService.FileWarehouse FileService = new FlightPakMasterService.FileWarehouse();
                                                    FileService.FileWarehouseID = 0;
                                                    FileService.RecordType = "CQQuoteOnFile";
                                                    FileService.UWAFileName = file2Save.UWAFileName;
                                                    FileService.RecordID = quote.CQMainID;
                                                    FileService.UWAWebpageName = "QuoteOnFile.aspx";
                                                    FileService.UWAFilePath = file2Save.UWAFilePath;
                                                    FileService.IsDeleted = false;
                                                    FileService.FileWarehouseID = 0;
                                                    objService.AddFWHType(FileService);
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion

                                Session.Remove(CQSessionKey);
                                Session.Remove(CQException);
                                CQMain Quoteinprogress = new CQMain();
                                FileRequest.QuoteNumInProgress = fileRequest.QuoteNumInProgress;
                                Quoteinprogress = getQuoteInProgress(FileRequest);
                                if (Quoteinprogress != null)
                                {
                                    var objRetval = Service.GetQuoteExceptionList(Quoteinprogress.CQMainID);
                                    if (objRetval.ReturnFlag)
                                    {
                                        Session[CQException] = objRetval.EntityList;
                                    }
                                }


                                #region Load Saved Image File List to Quotes

                                if (FileRequest.CQMains != null)
                                {
                                    foreach (CQMain quote in FileRequest.CQMains)
                                    {
                                        using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                        {
                                            var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                            if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                            {
                                                foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                {
                                                    CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                    objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                    objImageFile.RecordType = "CQQuoteOnFile";
                                                    objImageFile.UWAFileName = fwh.UWAFileName;
                                                    objImageFile.RecordID = quote.CQMainID;
                                                    objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                    objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                    objImageFile.IsDeleted = false;
                                                    if (quote.ImageList == null)
                                                        quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                    quote.ImageList.Add(objImageFile);
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion

                                SetCQModeToNoChange(ref FileRequest);
                                Session[CQSessionKey] = FileRequest;
                                FileRequest = null;
                                HttpCookie cookie = new HttpCookie("CharterQuoteUpdate");
                                cookie.Value = "Quote Updated";
                                cookie.Expires = DateTime.Now.AddDays(1);
                                HttpContext.Current.Response.Cookies.Add(cookie);
                                
                                string uri = string.Empty;
                                TryResolveUrl(HttpContext.Current.Request.Url.ToString(), out uri);
                                if (!string.IsNullOrEmpty(uri) && !string.IsNullOrWhiteSpace(uri))
                                {
                                    Response.Redirect(uri);
                                }
                            }
                            else
                            {
                                if (ReturnValue.ReturnFlag == false && !string.IsNullOrEmpty(ReturnValue.ErrorMessage))
                                {
                                    //RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + ReturnValue.ErrorMessage + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "');");
                                    RadAjaxManagerMaster.ResponseScripts.Add(@"jAlert('" + ReturnValue.ErrorMessage + " ','" + ModuleNameConstants.CharterQuote.CQFile + "');");
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void SetCQModeToNoChange(ref CQFile fileRequest)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileRequest))
            {
                if (fileRequest != null)
                {
                    fileRequest.Mode = CQRequestActionMode.NoChange;
                    fileRequest.State = CQRequestEntityState.NoChange;

                    if (fileRequest.CQMains != null)
                    {
                        foreach (CQMain cqMain in fileRequest.CQMains)
                        {
                            cqMain.State = CQRequestEntityState.NoChange;
                            cqMain.Mode = CQRequestActionMode.NoChange;

                            if (cqMain.CQFleetChargeDetails != null)
                            {
                                foreach (CQFleetChargeDetail cqFleetCharge in cqMain.CQFleetChargeDetails)
                                {
                                    cqFleetCharge.State = CQRequestEntityState.NoChange;
                                }
                            }

                            if (cqMain.CQLegs != null)
                            {
                                foreach (CQLeg cqLeg in cqMain.CQLegs)
                                {
                                    cqLeg.State = CQRequestEntityState.NoChange;

                                    if (cqLeg.CQCateringLists != null)
                                    {
                                        foreach (CQCateringList cqCatering in cqLeg.CQCateringLists)
                                        {
                                            cqCatering.State = CQRequestEntityState.NoChange;
                                        }
                                    }

                                    if (cqLeg.CQFBOLists != null)
                                    {
                                        foreach (CQFBOList cqFBO in cqLeg.CQFBOLists)
                                        {
                                            cqFBO.State = CQRequestEntityState.NoChange;
                                        }
                                    }

                                    if (cqLeg.CQHotelLists != null)
                                    {
                                        foreach (CQHotelList cqHotel in cqLeg.CQHotelLists)
                                        {
                                            cqHotel.State = CQRequestEntityState.NoChange;
                                        }
                                    }

                                    if (cqLeg.CQPassengers != null)
                                    {
                                        foreach (CQPassenger cqPax in cqLeg.CQPassengers)
                                        {
                                            cqPax.State = CQRequestEntityState.NoChange;
                                        }
                                    }

                                    if (cqLeg.CQTransportLists != null)
                                    {
                                        foreach (CQTransportList cqTransport in cqLeg.CQTransportLists)
                                        {
                                            cqTransport.State = CQRequestEntityState.NoChange;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void CopyQuote()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Int64 CQQuoteId = 0;
                        if (Session[CQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[CQSessionKey];
                            //long QuoteId = Convert.ToInt64(Session[CQSelectedQuoteNum].ToString());
                            Int64 QuoteNumInProgress = 1;
                            if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                                QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                            QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress).SingleOrDefault();

                            if (QuoteInProgress != null)
                            {
                                CQQuoteId = ((Int64)QuoteInProgress.CQMainID);
                                using (CharterQuoteService.CharterQuoteServiceClient CQService = new CharterQuoteService.CharterQuoteServiceClient())
                                {
                                    if (FileRequest.CQFileID != null && CQQuoteId != null)
                                    {

                                        var retval = CQService.CopyQuoteDetails(FileRequest.CQFileID, CQQuoteId);
                                        if (retval.ReturnFlag)
                                        {

                                            FileRequest = (CQFile)retval.EntityList[0];
                                            Session[CQSessionKey] = FileRequest;
                                            //Session[CQSelectedQuoteNum] = FileRequest.CQMains.Where(x => x.IsDeleted == false).Max(x => x.QuoteNUM).Value;
                                            //long NewQuoteId = Convert.ToInt64(Session[CQSelectedQuoteNum].ToString());
                                            //FileRequest.QuoteNumInProgress = NewQuoteId;

                                            Session.Remove(CQException);
                                            CQMain Quoteinprogress = new CQMain();
                                            Quoteinprogress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress).SingleOrDefault();
                                            if (Quoteinprogress != null)
                                            {
                                                var objRetval = CQService.GetQuoteExceptionList(Quoteinprogress.CQMainID);
                                                if (objRetval.ReturnFlag)
                                                {
                                                    Session[CQException] = objRetval.EntityList;
                                                }
                                            }
                                            #region Load Saved Image File List to Quotes

                                            if (FileRequest.CQMains != null)
                                            {
                                                foreach (CQMain quote in FileRequest.CQMains)
                                                {
                                                    using (FlightPakMasterService.MasterCatalogServiceClient ObjImgService = new FlightPakMasterService.MasterCatalogServiceClient())
                                                    {
                                                        var ObjRetImg = ObjImgService.GetFileWarehouseList("CQQuoteOnFile", quote.CQMainID);

                                                        if (ObjRetImg != null && ObjRetImg.ReturnFlag)
                                                        {
                                                            foreach (FlightPakMasterService.FileWarehouse fwh in ObjRetImg.EntityList)
                                                            {
                                                                CharterQuoteService.FileWarehouse objImageFile = new CharterQuoteService.FileWarehouse();
                                                                objImageFile.FileWarehouseID = fwh.FileWarehouseID;
                                                                objImageFile.RecordType = "CQQuoteOnFile";
                                                                objImageFile.UWAFileName = fwh.UWAFileName;
                                                                objImageFile.RecordID = quote.CQMainID;
                                                                objImageFile.UWAWebpageName = "QuoteOnFile.aspx";
                                                                objImageFile.UWAFilePath = fwh.UWAFilePath;
                                                                objImageFile.IsDeleted = false;
                                                                if (quote.ImageList == null)
                                                                    quote.ImageList = new List<CharterQuoteService.FileWarehouse>();
                                                                quote.ImageList.Add(objImageFile);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                            {
                                                var returnValue = CommonService.Lock(FileRequest.EntityKey.EntitySetName, FileRequest.CQFileID);
                                                if (returnValue != null && !returnValue.ReturnFlag)
                                                {
                                                    RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "' ) ;");
                                                }
                                                else
                                                {
                                                    FileRequest.Mode = CQRequestActionMode.Edit;
                                                    FileRequest.QuoteNumInProgress = Convert.ToInt64(Quoteinprogress.QuoteNUM);
                                                    // SetCQModeToNoChange(ref FileRequest);
                                                    Session[CQSessionKey] = FileRequest;
                                                    FileRequest = null;
                                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"SaveCopyQuoteAndRebindPage();");
                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }



                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQOnFile);
                }
            }
        }

        #region "Calculation Methods"

        private void ClearChildProperties(ref CQFile cqFile)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqFile))
            {
                if (cqFile.CQCustomerID != null) cqFile.CQCustomer = null;
                if (cqFile.CQCustomerContactID != null) cqFile.CQCustomerContact = null;
                if (cqFile.LeadSourceID != null) cqFile.LeadSource = null;
                if (cqFile.SalesPersonID != null) cqFile.SalesPerson = null;
                if (cqFile.HomebaseID != null) cqFile.Company = null;
                if (cqFile.ExchangeRateID != null) cqFile.ExchangeRate1 = null;
                #region CQMain Properties

                if (cqFile.CQMains != null)
                {
                    foreach (CQMain cqMain in cqFile.CQMains.ToList())
                    {

                        #region CQMain Properties
                        if (cqMain.VendorID != null) cqMain.Vendor = null;
                        if (cqMain.FleetID != null) cqMain.Fleet = null;
                        if (cqMain.AircraftID != null) cqMain.Aircraft = null;
                        if (cqMain.PassengerRequestorID != null) cqMain.Passenger = null;
                        if (cqMain.HomebaseID != null) cqMain.Company = null;
                        if (cqMain.ClientID != null) cqMain.Client = null;
                        if (cqMain.CQLostBusinessID != null) cqMain.CQLostBusiness = null;
                        if (cqMain.FeeGroupID != null) cqMain.FeeGroup = null;
                        #endregion

                        #region CQFleetChargeDetail Properties
                        if (cqMain.CQFleetChargeDetails != null)
                        {
                            foreach (CQFleetChargeDetail cqfltChrgDet in cqMain.CQFleetChargeDetails.ToList())
                            {
                                if (cqfltChrgDet.AccountID != null) cqfltChrgDet.Account = null;
                                if (cqfltChrgDet.AircraftCharterRateID != null) cqfltChrgDet.AircraftCharterRate = null;
                                if (cqfltChrgDet.FleetCharterRateID != null) cqfltChrgDet.FleetCharterRate = null;
                                //if (cqfltChrgDet.FeeGroupID != null) cqfltChrgDet.FeeGroup = null;
                            }
                        }
                        #endregion

                        if (cqMain.CQLegs != null)
                        {
                            foreach (CQLeg Leg in cqMain.CQLegs.ToList())
                            {

                                #region CQLeg Properties

                                if (Leg.DAirportID != null) Leg.Airport1 = null;
                                if (Leg.AAirportID != null) Leg.Airport = null;
                                if (Leg.FBOID != null) Leg.FBO = null;
                                if (Leg.CrewDutyRulesID != null) Leg.CrewDutyRule = null;
                                if (Leg.ClientID != null) Leg.Client = null;
                                if (Leg.OIAAirportID != null) Leg.Airport2 = null;
                                if (Leg.OIDAirportID != null) Leg.Airport3 = null;
                                if (Leg.OQAAirportID != null) Leg.Airport4 = null;
                                if (Leg.OQDAirportID != null) Leg.Airport5 = null;
                                #endregion

                                #region CQPassenger Properties

                                if (Leg.CQPassengers != null)
                                {
                                    foreach (CQPassenger PAX in Leg.CQPassengers.ToList())
                                    {
                                        if (PAX.PassengerRequestorID != null) PAX.Passenger = null;
                                        if (PAX.FlightPurposeID != null) PAX.FlightPurpose = null;
                                        if (PAX.PassportID != null) PAX.CrewPassengerPassport = null;
                                    }
                                }
                                #endregion

                                #region CQFBOList Properties

                                if (Leg.CQFBOLists != null)
                                {
                                    foreach (CQFBOList fbo in Leg.CQFBOLists.ToList())
                                    {
                                        if (fbo.FBOID != null) fbo.FBO = null;
                                        if (fbo.AirportID != null) fbo.Airport = null;
                                    }
                                }
                                #endregion

                                #region CQHotelList Properties

                                if (Leg.CQHotelLists != null)
                                {
                                    foreach (CQHotelList hotel in Leg.CQHotelLists.ToList())
                                    {
                                        if (hotel.HotelID != null) hotel.Hotel = null;
                                        if (hotel.AirportID != null) hotel.Airport = null;
                                    }
                                }
                                #endregion

                                #region CQTransportList Properties

                                if (Leg.CQTransportLists != null)
                                {
                                    foreach (CQTransportList transport in Leg.CQTransportLists.ToList())
                                    {
                                        if (transport.TransportID != null) transport.Transport = null;
                                        if (transport.AirportID != null) transport.Airport = null;
                                    }
                                }
                                #endregion

                                #region CQCateringList Properties
                                if (Leg.CQCateringLists != null)
                                {
                                    foreach (CQCateringList Catering in Leg.CQCateringLists.ToList())
                                    {
                                        if (Catering.CateringID != null) Catering.Catering = null;
                                        if (Catering.AirportID != null) Catering.Airport = null;
                                    }
                                }
                                #endregion

                            }
                        }
                    }
                }
                #endregion
            }
        }


        public string RounsETEbasedOnCompanyProfileSettings(decimal ETEVal)
        {
            string ETEStr = string.Empty;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ETEVal))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null &&
                            UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                        {
                            ETEStr = ConvertTenthsToMins(RoundElpTime((double) ETEVal).ToString());
                        }
                        else
                        {
                            ETEStr = Math.Round((decimal) ETEVal, 1).ToString();
                        }

                        ETEStr = Common.Utility.DefaultEteResult(ETEStr);
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.CharterQuote.CQFile);
                }
            }

            return ETEStr;
        }

        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        public string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {

                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)

                        time = time + ".0";

                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[0].Length == 1)
                        {
                            result = "0" + result;
                        }
                        if (timeArray[1].Length == 1)
                        {
                            result = result + "0";
                        }
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }

        public double RoundElpTime(double lnElp_Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lnElp_Time))
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {

                    decimal ElapseTMRounding = 0;
                    double lnElp_Min = 0.0;
                    double lnEteRound = 0.0;

                    if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                        ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                    if (ElapseTMRounding == 1)
                        lnEteRound = 0;
                    else if (ElapseTMRounding == 2)
                        lnEteRound = 5;
                    else if (ElapseTMRounding == 3)
                        lnEteRound = 10;
                    else if (ElapseTMRounding == 4)
                        lnEteRound = 15;
                    else if (ElapseTMRounding == 5)
                        lnEteRound = 30;
                    else if (ElapseTMRounding == 6)
                        lnEteRound = 60;

                    if (lnEteRound > 0)
                    {
                        lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                        if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                            lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                        else
                            lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                        if (lnElp_Min > 0 && lnElp_Min < 60)
                            lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                        if (lnElp_Min == 0)
                            lnElp_Time = Math.Floor(lnElp_Time);


                        if (lnElp_Min == 60)
                            lnElp_Time = Math.Ceiling(lnElp_Time);
                    }
                }
                return lnElp_Time;
            }
        }

        #endregion

        /// <summary>
        /// Common Method to Bind Legs Data
        /// </summary>
        /// <param name="objGrid">Pass Grid Object</param>
        /// <param name="isBind">Pass Boolean value for Rebind/Not</param>
        public void BindLegs(RadGrid objGrid, Boolean isBind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objGrid, isBind))
            {
                objGrid.DataSource = new string[] { };
                if (isBind)
                    objGrid.Rebind();

                if (Session[CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[CQSessionKey];

                    CQMain QuoteInProgress = new CQMain();
                    List<CQLeg> LegList = new List<CQLeg>();
                    if (FileRequest != null && FileRequest.CQMains != null)
                    {
                        Int64 QuoteNumInProgress = 1;
                        if (FileRequest.QuoteNumInProgress != null && FileRequest.QuoteNumInProgress > 0)
                            QuoteNumInProgress = FileRequest.QuoteNumInProgress;

                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
                        {
                            LegList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            if (LegList.Count > 0)
                            {
                                objGrid.DataSource = (from u in LegList
                                                      where (u.IsDeleted == false)
                                                      select
                                                       new
                                                       {
                                                           LegNum = u.LegNUM,
                                                           DepartAir = u.Airport1 != null ? u.Airport1.IcaoID == null ? string.Empty : u.Airport1.IcaoID : string.Empty,
                                                           ArrivalAir = u.Airport != null ? u.Airport.IcaoID == null ? string.Empty : u.Airport.IcaoID : string.Empty,
                                                           ETE = RounsETEbasedOnCompanyProfileSettings(u.ElapseTM != null ? (decimal)u.ElapseTM : 0),
                                                           DepartAirportID = u.DAirportID,
                                                           ArrivalAirportID = u.AAirportID,
                                                           DepartDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                                           TA = u.IsDepartureConfirmed != null ? ((bool)u.IsDepartureConfirmed ? "D" : "A") : "D",
                                                           ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal,
                                                           POS = u.IsPositioning != null ? u.IsPositioning : false,
                                                           Tax = u.IsTaxable != null ? u.IsTaxable : false,
                                                           TaxRate = u.TaxRate != null ? Math.Round((decimal)u.TaxRate, 2).ToString() : "0.00",
                                                           Rate = u.ChargeRate != null ? Math.Round((decimal)u.ChargeRate, 2).ToString() : "0.00",
                                                           Charges = u.ChargeTotal != null ? Math.Round((decimal)u.ChargeTotal, 2).ToString() : "0.00",
                                                           RON = u.RemainOverNightCNT != null ? u.RemainOverNightCNT.ToString() : "0",
                                                           DayRoom = u.DayRONCNT != null ? u.DayRONCNT.ToString() : "0",
                                                           PaxTotal = u.PassengerTotal != null ? u.PassengerTotal.ToString() : "0",
                                                           CD = u.CrewDutyAlert,
                                                           Distance = u.Distance != null ? u.Distance.ToString() : "0",
                                                           //PAXNo = u.PassengerTotal,
                                                           //SeatAvail = u.SeatTotal,
                                                       }).OrderBy(x => x.LegNum);
                                if (isBind)
                                    objGrid.Rebind();
                            }
                        }
                    }
                }
            }
        }


        #region "Exception"
        private CQMain getQuoteInProgress(CQFile cqFile)
        {
            CQMain QuoteInProgress = new CQMain();
            if (cqFile.CQMains != null && cqFile.CQMains.Count > 0)
            {
                Int64 QuoteNumInProgress = 1;
                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();
            }
            return QuoteInProgress;
        }
        public void ValidateQuote()
        {
            if (Session[CQSessionKey] != null)
            {

                FileRequest = (CQFile)Session[CQSessionKey];
                using (CharterQuoteServiceClient CharterQuoteSvc = new CharterQuoteServiceClient())
                {
                    CQMain QuoteInProgress = getQuoteInProgress(FileRequest);

                    var ObjRetval = CharterQuoteSvc.ValidateQuoteforMandatoryExcep(QuoteInProgress);

                    if (ObjRetval.ReturnFlag)
                    {


                        List<CQException> OldExceptionList = new List<CQException>();
                        OldExceptionList = (List<CQException>)Session[CQException];
                        if (OldExceptionList != null && OldExceptionList.Count > 0)
                        {

                            foreach (CQException OldMandatoryException in OldExceptionList.ToList())
                            {
                                if (OldMandatoryException.CQExceptionID == 0)
                                    OldExceptionList.Remove(OldMandatoryException);
                            }
                        }
                        ExceptionList = ObjRetval.EntityList;

                        if (ExceptionList != null && ExceptionList.Count > 0)
                        {
                            if (OldExceptionList == null)
                                OldExceptionList = new List<CQException>();
                            OldExceptionList.AddRange(ExceptionList);
                        }

                        if (FileRequest.HomebaseID == null || FileRequest.HomebaseID == 0)
                        {
                            CQException Fileexp = new CQException();
                            Fileexp.CQExceptionID = 0;
                            Fileexp.CQExceptionDescription = "Homebase is Required";
                            Fileexp.ExceptionTemplateID = (long)CQBusinessErrorReference.HomebaseReq;
                            if (OldExceptionList == null)
                                OldExceptionList = new List<CQException>();
                            OldExceptionList.Add(Fileexp);
                        }

                        if (FileRequest.EstDepartureDT == null)
                        {
                            CQException Fileexp = new CQException();
                            Fileexp.CQExceptionID = 0;
                            Fileexp.CQExceptionDescription = "Trip Date is Required";
                            Fileexp.ExceptionTemplateID = (long)CQBusinessErrorReference.EstDepartureDate;
                            if (OldExceptionList == null)
                                OldExceptionList = new List<CQException>();
                            OldExceptionList.Add(Fileexp);
                        }

                        if (FileRequest.QuoteDT == null)
                        {
                            CQException Fileexp = new CQException();
                            Fileexp.CQExceptionID = 0;
                            Fileexp.CQExceptionDescription = "Quote Date is Required";
                            Fileexp.ExceptionTemplateID = (long)CQBusinessErrorReference.QuoteDate;
                            if (OldExceptionList == null)
                                OldExceptionList = new List<CQException>();
                            OldExceptionList.Add(Fileexp);
                        }

                        Session[CQException] = OldExceptionList;
                    }

                    //List<ExceptionTemplate> TripExceptions = new List<ExceptionTemplate>();
                    //var ObjRetvalTemplates = CharterQuoteSvc.GetCQBusinessErrorsList();

                    //if (ObjRetvalTemplates.ReturnFlag)
                    //{
                    //    TripExceptions = ObjRetvalTemplates.EntityList;
                    //}
                }
            }
        }
        protected void Exception_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                BindException(false);
            }
        }

        public void BindException(bool isRebind)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[CQException] != null)
                {
                    ExceptionList = (List<CQException>)Session[CQException];

                    if (ExceptionList.Count > 0)
                    {
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        using (CharterQuoteServiceClient CharterQuoteSvc = new CharterQuoteServiceClient())
                        {
                            ExceptionTemplateList = GetExceptionTemplateList();
                            dgException.DataSource = (from Exp in ExceptionList
                                                      join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                      select new
                                                      {
                                                          Severity = ExpTempl.Severity,
                                                          ExceptionDescription = Exp.CQExceptionDescription,
                                                          SubModuleID = ExpTempl.SubModuleID,
                                                          SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                                          DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                                      }).OrderBy(x => x.DisplayOrder).ToList();
                            if (isRebind)
                                dgException.Rebind();
                            ExpandErrorPanel(true);
                        }
                    }
                    else
                    {
                        //Defect : 2565
                        dgException.DataSource = new string[] { };
                        if (isRebind)
                            dgException.Rebind();
                        ExpandErrorPanel(false);
                    }
                }
                else
                {
                    //Defect : 2565
                    dgException.DataSource = new string[] { };
                    if (isRebind)
                        dgException.Rebind();
                }

            }
        }

        private void ExpandErrorPanel(bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isExpand))
            {
                RadPanelItem pnlItem = pnlException.FindItemByValue("pnlItemException");
                pnlItem.Expanded = isExpand;
            }
        }
        protected string GetSubmoduleModule(long SubModuleID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SubModuleID))
            {
                string Retval = string.Empty;
                switch (SubModuleID)
                {
                    case 0: Retval = "File"; break;
                    case 1: Retval = "Quote"; break;
                    case 2: Retval = "Leg"; break;
                    case 3: Retval = "PAX"; break;
                    case 4: Retval = "Logistics"; break;
                    case 5: Retval = "Reports"; break;
                }

                return Retval;
            }
        }


        protected void dgLegs_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                QuoteInProgress = getQuoteInProgress(FileRequest);
                if (QuoteInProgress != null)
                {

                    List<CQLeg> Dt = new List<CQLeg>();
                    if (QuoteInProgress.CQLegs != null)
                    {
                        Dt = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).ToList();

                        if (Dt.Count > 0)
                        {

                            dgLegs.DataSource = (from u in Dt
                                                 where u.Airport != null && u.Airport1 != null
                                                 select
                                                  new
                                                  {

                                                      LegNum = u.LegNUM,
                                                      DepartAir = u.Airport.IcaoID == null ? string.Empty : u.Airport1.IcaoID,
                                                      DeparttDate = u.DepartureDTTMLocal == null ? null : u.DepartureDTTMLocal,
                                                      ArrivalAir = u.Airport1.IcaoID == null ? string.Empty : u.Airport.IcaoID,
                                                      ArrivalDate = u.ArrivalDTTMLocal == null ? null : u.ArrivalDTTMLocal,
                                                      ArrivalAirCityName=u.Airport1.CityName == null ? string.Empty : u.Airport1.CityName,
                                                      DepartAirCityName=u.Airport.CityName==null?string.Empty:u.Airport.CityName
                                                  }).OrderBy(x => x.LegNum);

                        }
                        else
                        {
                            dgLegs.DataSource = new string[] { };
                        }
                        //if (Dt.Count > 0)
                        //{
                        //    dgLegSummary.DataSource = Dt;
                        //}
                    }
                }
            }
        }

        protected void dgLegs_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    string dateFormat = "";

                    if (item["DeparttDate"].Text != "&nbsp;")
                    {
                        DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DeparttDate");
                        dateFormat = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date);
                        item["DeparttDate"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(dateFormat);
                        item["DeparttDate"].ToolTip = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date);
                    }
                    if (item["ArrivalDate"].Text != "&nbsp;")
                    {
                        DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                        dateFormat = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", date1);
                        item["ArrivalDate"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(dateFormat);
                        item["ArrivalDate"].ToolTip = String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date1);
                    }
                }
            }
        }


        /// <summary>
        /// Exception Grid Item Databound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exception_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                if (e.Item is GridDataItem)
                {
                    GridDataItem Item = (GridDataItem)e.Item;
                    Image imgSeverity = (Image)Item.FindControl("imgSeverity");
                    string severity = Item.GetDataKeyValue("Severity").ToString().Trim();
                    // Default Image Path
                    string imagePath = "~/App_Themes/Default/images/info.png";
                    if (severity == "1")
                    {
                        imagePath = "~/App_Themes/Default/images/info_red.gif";
                    }
                    else if (severity == "2")
                    {
                        imagePath = "~/App_Themes/Default/images/info_yellow.gif";
                    }
                    imgSeverity.ImageUrl = imagePath;

                }

                //if (Severity.Critical == allErrorList[0].ExceptionSeverity)
                //{
                //    RedirectToMain();
                //    //if (seltab == "Main")
                //    //else
                //    //    Response.Redirect("PreflightLegs.aspx?seltab=Legs");

                //}
            }
        }

        public List<ExceptionTemplate> GetExceptionTemplateList()
        {
            List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();

            if (Session[CQExceptionTempatelist] == null)
            {
                using (CharterQuoteServiceClient Service = new CharterQuoteServiceClient())
                {
                    var ObjExTemplRetVal = Service.GetCQBusinessErrorsList();
                    if (ObjExTemplRetVal.ReturnFlag)
                    {
                        ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                        Session[CQExceptionTempatelist] = ExceptionTemplateList;
                    }
                }
            }
            else
                ExceptionTemplateList = (List<ExceptionTemplate>)Session[CQExceptionTempatelist];
            return ExceptionTemplateList;
        }
        #endregion


        #region Quote Calculation Methods

        public void CalculateQuoteTotal()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnTotFltHrs = 0;
                decimal lnTotDays = 0;
                decimal lnTotalDays = 0;
                decimal lnMinDays = 0;
                decimal lnMinDailyUsage = 0;
                decimal lnTotLndDsc = 0;
                decimal lnDiscRate = 0;
                decimal lnTotCqDisc = 0;
                decimal lnTotBuyAmt = 0;
                decimal lnTotAdjAmt = 0;
                decimal lnTotTaxAmt = 0;
                decimal lnTotCrwRon = 0;
                decimal lnTotUseAdj = 0;
                decimal lnTotlndfee = 0;
                decimal totchrg = 0;
                decimal lnTotDiscAmt = 0;
                decimal totsub = 0;
                decimal lnTotCost = 0;
                decimal lnTotMargin = 0;
                decimal lnTotMarPct = 0;
                decimal lnTotSegFee = 0;

                string lcFleetType = string.Empty;
                DateTime locarrdt, locdepdt;

                if (QuoteInProgress != null)
                {
                    lnTaxRate = QuoteInProgress.FederalTax != null ? (decimal)QuoteInProgress.FederalTax : 0;
                    lnExRate = (FileRequest.ExchangeRate != null && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;

                    if (QuoteInProgress.SubtotalTotal == null)
                        QuoteInProgress.SubtotalTotal = 0.0M;

                    if (QuoteInProgress.QuoteTotal == null)
                        QuoteInProgress.QuoteTotal = 0.0M;

                    if (QuoteInProgress.CostTotal == null)
                        QuoteInProgress.CostTotal = 0.0M;

                    if (QuoteInProgress.UsageAdjTotal == null)
                        QuoteInProgress.UsageAdjTotal = 0.0M;

                    if (QuoteInProgress.DiscountAmtTotal == null)
                        QuoteInProgress.DiscountAmtTotal = 0.0M;


                    if (QuoteInProgress.IsOverrideCost == null) QuoteInProgress.IsOverrideCost = false;
                    if (QuoteInProgress.IsOverrideDiscountAMT == null) QuoteInProgress.IsOverrideDiscountAMT = false;
                    if (QuoteInProgress.IsOverrideDiscountPercentage == null) QuoteInProgress.IsOverrideDiscountPercentage = false;
                    if (QuoteInProgress.IsOverrideLandingFee == null) QuoteInProgress.IsOverrideLandingFee = false;
                    if (QuoteInProgress.IsOverrideSegmentFee == null) QuoteInProgress.IsOverrideSegmentFee = false;
                    if (QuoteInProgress.IsOverrideTaxes == null) QuoteInProgress.IsOverrideTaxes = false;
                    if (QuoteInProgress.IsOverrideTotalQuote == null) QuoteInProgress.IsOverrideTotalQuote = false;
                    if (QuoteInProgress.IsOverrideUsageAdj == null) QuoteInProgress.IsOverrideUsageAdj = false;
                    if (QuoteInProgress.IsTaxable == null) QuoteInProgress.IsTaxable = false;
                    if (QuoteInProgress.IsDailyTaxAdj == null) QuoteInProgress.IsDailyTaxAdj = false;
                    if (QuoteInProgress.IsLandingFeeTax == null) QuoteInProgress.IsLandingFeeTax = false;

                    if (QuoteInProgress.FleetID != null)
                    {
                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                            var objRetVal = objDstsvc.GetFleetProfileList();
                            if (objRetVal != null && objRetVal.ReturnFlag)
                            {
                                Fleetlist = objRetVal.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();

                                if (Fleetlist != null && Fleetlist.Count > 0)
                                {
                                    lcFleetType = Fleetlist[0].FleetSize;
                                    if (Fleetlist[0].MinimumDay != null)
                                        lnMinDays = (decimal)Fleetlist[0].MinimumDay;
                                }
                            }
                        }
                    }

                    lnTotFltHrs = CalculateSumOfETE(4); // any no greater than three to get all values of ete.
                    locdepdt = CalculateMinDeparttDate();
                    locarrdt = CalculateMaxArrivalDate();
                    totchrg = CalculateSumOfTotalCharge();
                    if (locarrdt != DateTime.MinValue && locdepdt != DateTime.MinValue)
                    {
                        //Fix for date subtraction issue
                        DateTime ArrDate = new DateTime(locarrdt.Year, locarrdt.Month, locarrdt.Day);
                        DateTime DepDate = new DateTime(locdepdt.Year, locdepdt.Month, locdepdt.Day);
                        TimeSpan daydiff = ArrDate.Subtract(DepDate);
                        lnTotalDays = daydiff.Days + 1;

                    }
                    lnTotDays = lnTotalDays;
                    if (lnTotalDays < 1)
                        lnTotalDays = 1;
                    lnMinDailyUsage = lnTotalDays * lnMinDays;
                    lnTotAdjAmt = Math.Round(lnMinDailyUsage - lnTotFltHrs, 2);

                    if (lnTotAdjAmt < 0)
                        lnTotAdjAmt = 0;

                    lnTotLndDsc = 0;
                    lnDiscRate = 0;
                    lnTotCqDisc = 0;
                    lnTotBuyAmt = 0;

                    lnTotSegFee = CalculateSegFee();

                    if (FileRequest.DiscountPercentage != null)
                        lnDiscRate = (decimal)FileRequest.DiscountPercentage;

                    if (QuoteInProgress.IsOverrideDiscountPercentage != null && (bool)QuoteInProgress.IsOverrideDiscountPercentage) // Override Discount % in quote 
                        lnDiscRate = QuoteInProgress.DiscountPercentageTotal != null ? (decimal)QuoteInProgress.DiscountPercentageTotal : 0;
                    else
                        QuoteInProgress.DiscountPercentageTotal = lnDiscRate;

                    if (QuoteInProgress.CQFleetChargeDetails != null)
                    {
                        #region Initializing to 0
                        foreach (CQFleetChargeDetail cqFleetChrgDet in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList())
                        {
                            if (cqFleetChrgDet.CQFleetChargeDetailID != 0)
                                cqFleetChrgDet.State = CQRequestEntityState.Modified;
                            cqFleetChrgDet.FeeAMT = 0;

                            if (cqFleetChrgDet.Quantity == null)
                                cqFleetChrgDet.Quantity = 0;


                            if (cqFleetChrgDet.SellDOM == null)
                                cqFleetChrgDet.SellDOM = 0;

                            if (cqFleetChrgDet.IsDiscountDOM == null)
                                cqFleetChrgDet.IsDiscountDOM = false;

                            if (cqFleetChrgDet.IsTaxDOM == null)
                                cqFleetChrgDet.IsTaxDOM = false;

                            if (cqFleetChrgDet.BuyDOM == null)
                                cqFleetChrgDet.BuyDOM = 0;

                            if (cqFleetChrgDet.SellIntl == null)
                                cqFleetChrgDet.SellIntl = 0;

                            if (cqFleetChrgDet.IsDiscountIntl == null)
                                cqFleetChrgDet.IsDiscountIntl = false;

                            if (cqFleetChrgDet.IsTaxIntl == null)
                                cqFleetChrgDet.IsTaxIntl = false;

                            if (cqFleetChrgDet.BuyIntl == null)
                                cqFleetChrgDet.BuyIntl = 0;




                        }
                        lnTotCqDisc = 0;
                        //QuoteInProgress.DiscountAmtTotal = 0; //lnTotCqDisc
                        lnTotTaxAmt = 0;
                        //QuoteInProgress.TaxesTotal = 0; //lnTotTaxAmt
                        lnTotBuyAmt = 0;
                        //QuoteInProgress.CostTotal = 0; //lnTotBuyAmt
                        lnTotlndfee = 0;
                        //QuoteInProgress.LandingFeeTotal = 0; //lnTotlndfee
                        totsub = 0;
                        //QuoteInProgress.SubtotalTotal = 0; //totsun
                        lnTotCrwRon = 0;
                        //QuoteInProgress.StdCrewRONTotal =0//lnTotCrwRon
                        #endregion

                        #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition

                        foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3 && x.NegotiatedChgUnit != null && x.NegotiatedChgUnit == 5))
                        {
                            decimal chargeRateDisc = 0.0M;

                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;


                            chargeRate.FeeAMT = chargeRate.Quantity * chargeRate.SellDOM;

                            // Calculate Discount
                            if ((bool)chargeRate.IsDiscountDOM)
                            {
                                chargeRateDisc = Math.Round((decimal)chargeRate.FeeAMT * lnDiscRate / 100, 2);
                                lnTotCqDisc += chargeRateDisc;
                            }

                            // Calculate Tax
                            if ((bool)chargeRate.IsTaxDOM && (bool)QuoteInProgress.IsTaxable && chargeRate.FeeAMT != null)
                            {
                                decimal taxAmount = 0.0M;
                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                    taxAmount = Math.Round(((decimal)chargeRate.FeeAMT - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                else
                                    taxAmount = Math.Round((decimal)chargeRate.FeeAMT * (decimal)lnTaxRate / 100, 2);

                                lnTotTaxAmt += taxAmount;
                            }

                            decimal buyAmount = 0.0M;
                            buyAmount = (decimal)chargeRate.Quantity * (decimal)chargeRate.BuyDOM;
                            lnTotBuyAmt += buyAmount;

                        }
                        #endregion

                        #region Landing Fee
                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                        {
                            foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null))
                            {
                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {

                                    decimal lnLegLndngFee = 0;
                                    FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirpot = GetAirport((long)Leg.AAirportID);

                                    if (ArrAirpot != null)
                                    {
                                        switch (lcFleetType)
                                        {
                                            case "S":
                                                lnLegLndngFee = ArrAirpot.LandingFeeSmall != null ? (decimal)ArrAirpot.LandingFeeSmall : 0;
                                                break;
                                            case "M":
                                                lnLegLndngFee = ArrAirpot.LandingFeeMedium != null ? (decimal)ArrAirpot.LandingFeeMedium : 0;
                                                break;
                                            case "L":
                                                lnLegLndngFee = ArrAirpot.LandingFeeLarge != null ? (decimal)ArrAirpot.LandingFeeLarge : 0;
                                                break;
                                        }
                                        lnTotlndfee += lnLegLndngFee;
                                    }
                                }


                            }

                        }
                        #endregion

                        #region Calculate Fee Amount for all the Standard Charges and Additional Fees, Where OrderNum = 1

                        var StandardCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();

                        if (StandardCharge != null && QuoteInProgress.CQLegs != null)
                        {
                            if (StandardCharge.CQFleetChargeDetailID != 0)
                                StandardCharge.State = CQRequestEntityState.Modified;

                            decimal buyAmount = 0.0M;
                            foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning != null && x.IsPositioning == false))
                            {
                                decimal lnFeeAmt = 0;

                                if (leg.ElapseTM == null)
                                    leg.ElapseTM = 0;

                                if (leg.DutyTYPE == null)
                                    leg.DutyTYPE = 1;


                                if (StandardCharge.NegotiatedChgUnit == 1)
                                {
                                    lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }
                                else if (StandardCharge.NegotiatedChgUnit == 2)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }
                                else if (StandardCharge.NegotiatedChgUnit == 3)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }
                                else if (StandardCharge.NegotiatedChgUnit == 8)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.BuyDOM : (decimal)StandardCharge.BuyIntl);
                                }

                                leg.ChargeTotal = lnFeeAmt;
                                leg.ChargeRate = (decimal)leg.DutyTYPE == 1 ? (decimal)StandardCharge.SellDOM : (decimal)StandardCharge.SellIntl;
                                StandardCharge.FeeAMT += lnFeeAmt;
                                lnTotBuyAmt += buyAmount;


                                // Calculate Discount
                                decimal chargeRateDisc = 0.0M;
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsDiscountDOM : StandardCharge.IsDiscountIntl))
                                {
                                    chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }

                                // Calculate Tax
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? StandardCharge.IsTaxDOM : StandardCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && lnFeeAmt != null)
                                {
                                    decimal taxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        taxAmount = Math.Round((decimal)lnFeeAmt * lnTaxRate / 100, 2);

                                    lnTotTaxAmt += taxAmount;
                                }

                            }
                        }
                        #endregion

                        #region Calculate Fee Amount for all Positioning Charges, Where OrderNum = 2
                        var PositionCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 2).FirstOrDefault();

                        if (PositionCharge != null && QuoteInProgress.CQLegs != null)
                        {
                            if (PositionCharge.CQFleetChargeDetailID != 0)
                                PositionCharge.State = CQRequestEntityState.Modified;

                            decimal buyAmount = 0.0M;
                            foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning != null && x.IsPositioning == true))
                            {
                                decimal lnFeeAmt = 0;

                                if (PositionCharge.NegotiatedChgUnit == 1)
                                {
                                    lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }
                                else if (PositionCharge.NegotiatedChgUnit == 2)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }
                                else if (PositionCharge.NegotiatedChgUnit == 3)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }
                                else if (PositionCharge.NegotiatedChgUnit == 8)
                                {
                                    lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl);
                                    buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.BuyDOM : (decimal)PositionCharge.BuyIntl);
                                }

                                leg.ChargeTotal = lnFeeAmt;
                                leg.ChargeRate = (decimal)leg.DutyTYPE == 1 ? (decimal)PositionCharge.SellDOM : (decimal)PositionCharge.SellIntl;

                                PositionCharge.FeeAMT += lnFeeAmt;
                                lnTotBuyAmt += buyAmount;

                                // Calculate Discount
                                decimal chargeRateDisc = 0.0M;
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsDiscountDOM : PositionCharge.IsDiscountIntl))
                                {
                                    chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                    lnTotCqDisc += chargeRateDisc;
                                }

                                // Calculate Tax
                                if ((bool)(((decimal)leg.DutyTYPE == 1) ? PositionCharge.IsTaxDOM : PositionCharge.IsTaxIntl) && (bool)QuoteInProgress.IsTaxable && PositionCharge.FeeAMT != null)
                                {
                                    decimal taxAmount = 0.0M;
                                    if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                        taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                    else
                                        taxAmount = Math.Round((decimal)lnFeeAmt * lnTaxRate / 100, 2);
                                    lnTotTaxAmt += taxAmount;
                                }
                            }
                        }
                        #endregion

                        #region Calculate Standard Crew RON Charge, Where OrderNum = 3
                        var RonCharge = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 3).FirstOrDefault();

                        if (RonCharge != null && QuoteInProgress.CQLegs != null)
                        {
                            if (RonCharge.CQFleetChargeDetailID != 0)
                                RonCharge.State = CQRequestEntityState.Modified;

                            decimal domRonQty = 0;
                            decimal intlRonQty = 0;
                            decimal domFeeAmt = 0;
                            decimal intlFeeAmt = 0;
                            decimal domBuyAmt = 0;
                            decimal intlBuyAmt = 0;

                            domRonQty = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DutyTYPE == 1).Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                            intlRonQty = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DutyTYPE == 2).Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;

                            domFeeAmt = (QuoteInProgress.StandardCrewDOM != null ? (decimal)QuoteInProgress.StandardCrewDOM : 0.0M) * (decimal)domRonQty * (decimal)RonCharge.SellDOM;
                            intlFeeAmt = (QuoteInProgress.StandardCrewINTL != null ? (decimal)QuoteInProgress.StandardCrewINTL : 0.0M) * (decimal)intlRonQty * (decimal)RonCharge.SellIntl;

                            domBuyAmt = (QuoteInProgress.StandardCrewDOM != null ? (decimal)QuoteInProgress.StandardCrewDOM : 0.0M) * (decimal)domRonQty * (decimal)RonCharge.BuyDOM;
                            intlBuyAmt = (QuoteInProgress.StandardCrewINTL != null ? (decimal)QuoteInProgress.StandardCrewINTL : 0.0M) * (decimal)intlRonQty * (decimal)RonCharge.BuyIntl;

                            RonCharge.FeeAMT = domFeeAmt + intlFeeAmt;

                            decimal buyAmount = 0.0M;
                            buyAmount = domBuyAmt + intlBuyAmt;
                            lnTotBuyAmt += buyAmount;
                            lnTotCrwRon += (decimal)RonCharge.FeeAMT;

                            decimal domchargeRateDisc = 0.0M;
                            if (RonCharge.IsDiscountDOM != null && (bool)RonCharge.IsDiscountDOM)
                            {
                                domchargeRateDisc = Math.Round((decimal)domFeeAmt * lnDiscRate / 100, 2);
                                lnTotCqDisc += domchargeRateDisc;
                            }

                            decimal intlchargeRateDisc = 0.0M;
                            if (RonCharge.IsDiscountIntl != null && (bool)RonCharge.IsDiscountIntl)
                            {
                                intlchargeRateDisc = Math.Round((decimal)intlFeeAmt * lnDiscRate / 100, 2);
                                lnTotCqDisc += intlchargeRateDisc;
                            }


                            // Calculate Tax
                            if ((bool)RonCharge.IsTaxDOM && (QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT != null)
                            {
                                decimal domtaxAmount = 0.0M;
                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                    domtaxAmount = Math.Round(((decimal)domFeeAmt - (decimal)domchargeRateDisc) * lnTaxRate / 100, 2);
                                else
                                    domtaxAmount = Math.Round((decimal)domFeeAmt * lnTaxRate / 100, 2);

                                lnTotTaxAmt += domtaxAmount;
                            }

                            if ((bool)RonCharge.IsTaxIntl && (QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsTaxable) && RonCharge.FeeAMT != null)
                            {
                                decimal intltaxAmount = 0.0M;
                                if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount != null && UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                    intltaxAmount = Math.Round(((decimal)intlFeeAmt - (decimal)intlchargeRateDisc) * lnTaxRate / 100, 2);
                                else
                                    intltaxAmount = Math.Round((decimal)intlFeeAmt * lnTaxRate / 100, 2);

                                lnTotTaxAmt += intltaxAmount;
                            }

                        }
                        #endregion

                        #region Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3) Condition

                        foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3))
                        {


                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;

                            decimal buyAmount = 0.0M;
                            if (QuoteInProgress.CQLegs != null)
                            {
                                foreach (CQLeg leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null))
                                {
                                    decimal lnFeeAmt = 0;
                                    if (chargeRate.NegotiatedChgUnit == 1)
                                    {
                                        lnFeeAmt = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.ElapseTM * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 2)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.Distance * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 3)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.Distance * statuateConvFactor * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 4)
                                    {
                                        lnFeeAmt = (decimal)((leg.ElapseTM * chargeRate.Quantity) / 100) * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)((leg.ElapseTM * chargeRate.Quantity) / 100) * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 5)
                                    {
                                        // Already Captured in this region - Calculate Fee Amount for Fixed Charge Unit, Where (OrderNum > 3 and ChargeUnit = 5) Condition
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 6)
                                    {
                                        lnFeeAmt = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 7)
                                    {
                                        lnFeeAmt = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)chargeRate.Quantity * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }
                                    else if (chargeRate.NegotiatedChgUnit == 8)
                                    {
                                        lnFeeAmt = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.SellDOM : (decimal)chargeRate.SellIntl);
                                        buyAmount = (decimal)leg.Distance * 1.852M * ((decimal)leg.DutyTYPE == 1 ? (decimal)chargeRate.BuyDOM : (decimal)chargeRate.BuyIntl);
                                    }

                                    chargeRate.FeeAMT += lnFeeAmt;
                                    lnTotBuyAmt += buyAmount;

                                    decimal chargeRateDisc = 0.0M;
                                    if (chargeRate.NegotiatedChgUnit != 5)
                                    {
                                        // Calculate Discount
                                        if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl))
                                        {
                                            chargeRateDisc = Math.Round((decimal)lnFeeAmt * lnDiscRate / 100, 2);
                                            lnTotCqDisc += chargeRateDisc;
                                        }

                                        // Calculate Tax
                                        if ((bool)(((decimal)leg.DutyTYPE == 1) ? chargeRate.IsDiscountDOM : chargeRate.IsDiscountIntl) && (bool)QuoteInProgress.IsTaxable && lnFeeAmt != null)
                                        {
                                            decimal taxAmount = 0.0M;
                                            if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                                taxAmount = Math.Round(((decimal)lnFeeAmt - (decimal)chargeRateDisc) * lnTaxRate / 100, 2);
                                            else
                                                taxAmount = Math.Round((decimal)lnFeeAmt * (decimal)lnTaxRate / 100, 2);

                                            lnTotTaxAmt += taxAmount;
                                        }
                                    }

                                }
                            }
                        }
                        #endregion

                        #region Calculate Adjusted hours TotAdjAmt for first leg and ordernum 1

                        if (QuoteInProgress.CQLegs != null)
                        {
                            CQLeg FirstLeg = new CQLeg();
                            FirstLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.LegNUM == 1).SingleOrDefault();
                            lnTotUseAdj = 0;
                            if (FirstLeg != null)
                            {
                                //if (FirstLeg.DutyTYPE == 1)
                                //{
                                var StandardChargeDet = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM == 1).FirstOrDefault();

                                if (StandardChargeDet != null)
                                {
                                    if (StandardChargeDet.CQFleetChargeDetailID != 0)
                                        StandardChargeDet.State = CQRequestEntityState.Modified;
                                    decimal buyAmount = 0.0M;

                                    if (QuoteInProgress.IsOverrideUsageAdj != null)
                                        lnTotUseAdj = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.SellDOM : (decimal)StandardChargeDet.SellIntl), 2);

                                    if (QuoteInProgress.IsOverrideUsageAdj != null)
                                        buyAmount = (bool)QuoteInProgress.IsOverrideUsageAdj ? (decimal)QuoteInProgress.UsageAdjTotal : Math.Round(lnTotAdjAmt * ((decimal)FirstLeg.DutyTYPE == 1 ? (decimal)StandardChargeDet.BuyDOM : (decimal)StandardChargeDet.BuyIntl), 2);

                                    lnTotBuyAmt += buyAmount;

                                    // Calculate Discount
                                    decimal chargeRateDisc = 0.0M;
                                    if ((bool)(((decimal)FirstLeg.DutyTYPE == 1) ? StandardChargeDet.IsDiscountDOM : StandardChargeDet.IsDiscountIntl))
                                    {
                                        chargeRateDisc = Math.Round((decimal)lnTotUseAdj * lnDiscRate / 100, 2);
                                        lnTotCqDisc += chargeRateDisc;
                                    }

                                    // Calculate Tax
                                    if (QuoteInProgress.IsDailyTaxAdj != null && QuoteInProgress.IsTaxable != null && (bool)QuoteInProgress.IsDailyTaxAdj && (bool)QuoteInProgress.IsTaxable)
                                    {
                                        decimal taxAmount = 0.0M;
                                        if (UserPrincipal.Identity._fpSettings._IsQuoteTaxDiscount)
                                            taxAmount = Math.Round(((decimal)lnTotUseAdj - (decimal)chargeRateDisc) * (decimal)QuoteInProgress.FederalTax / 100, 2);
                                        else
                                            taxAmount = Math.Round((decimal)lnTotUseAdj * (decimal)QuoteInProgress.FederalTax / 100, 2);

                                        lnTotTaxAmt += taxAmount;
                                    }
                                }
                                //}
                            }
                        }
                        #endregion

                        totchrg = CalculateSumOfTotalCharge();


                        lnTotlndfee = (QuoteInProgress.IsOverrideLandingFee != null && (bool)QuoteInProgress.IsOverrideLandingFee) ? (decimal)QuoteInProgress.LandingFeeTotal : lnTotlndfee;

                        if (QuoteInProgress.IsLandingFeeTax != null && (bool)QuoteInProgress.IsLandingFeeTax)
                            lnTotTaxAmt += Math.Round((lnTotlndfee * lnTaxRate) / 100, 2);

                        //Sum of fee amount where ordernum > 3
                        decimal totQuoteChrg = 0;
                        totQuoteChrg = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false && x.OrderNUM > 3).Sum(x => x.FeeAMT).Value;

                        QuoteInProgress.SubtotalTotal = totQuoteChrg + totchrg;

                        if (QuoteInProgress.IsOverrideSegmentFee != null && !(bool)QuoteInProgress.IsOverrideSegmentFee)
                            QuoteInProgress.SegmentFeeTotal = lnTotSegFee;

                        if (QuoteInProgress.IsOverrideUsageAdj != null && !(bool)QuoteInProgress.IsOverrideUsageAdj)
                            QuoteInProgress.UsageAdjTotal = lnTotUseAdj;

                        if (QuoteInProgress.IsOverrideLandingFee != null && !(bool)QuoteInProgress.IsOverrideLandingFee)
                            QuoteInProgress.LandingFeeTotal = lnTotlndfee;

                        QuoteInProgress.SubtotalTotal = QuoteInProgress.SubtotalTotal + QuoteInProgress.UsageAdjTotal +
                                QuoteInProgress.SegmentFeeTotal + lnTotCrwRon + QuoteInProgress.LandingFeeTotal;
                        lnTotDiscAmt = lnTotCqDisc + lnTotLndDsc;

                        if (QuoteInProgress.IsOverrideDiscountAMT != null && !(bool)QuoteInProgress.IsOverrideDiscountAMT)
                            QuoteInProgress.DiscountAmtTotal = lnTotDiscAmt;
                        else
                        {
                            if (QuoteInProgress.DiscountAmtTotal != null)
                                lnTotDiscAmt = (decimal)QuoteInProgress.DiscountAmtTotal;

                            decimal subTotal = 0;
                            subTotal = ((decimal)QuoteInProgress.SubtotalTotal - ((decimal)QuoteInProgress.LandingFeeTotal + (decimal)QuoteInProgress.SegmentFeeTotal));

                            // Condtion for Attempted to divide by zero Error.
                            if (lnTotDiscAmt != 0 && subTotal != 0)
                                lnDiscRate = ((lnTotDiscAmt * 100) / subTotal);

                            lnDiscRate = Math.Round(lnDiscRate, 2);
                            QuoteInProgress.DiscountPercentageTotal = lnDiscRate;
                        }


                        if (QuoteInProgress.IsOverrideTaxes != null && !(bool)QuoteInProgress.IsOverrideTaxes)
                            QuoteInProgress.TaxesTotal = lnTotTaxAmt;
                        else if (QuoteInProgress.TaxesTotal != null)
                            lnTotTaxAmt = (decimal)QuoteInProgress.TaxesTotal;


                        QuoteInProgress.QuoteTotal = (QuoteInProgress.IsOverrideTotalQuote != null && (bool)QuoteInProgress.IsOverrideTotalQuote) ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt);
                        QuoteInProgress.FlightHoursTotal = lnTotFltHrs;
                        QuoteInProgress.DaysTotal = lnTotDays;
                        QuoteInProgress.AverageFlightHoursTotal = lnTotDays == 0 ? 0 : Math.Round(lnTotFltHrs / lnTotDays, 2);
                        QuoteInProgress.AdditionalFeeTotalD = totQuoteChrg;
                        QuoteInProgress.StdCrewRONTotal = lnTotCrwRon;

                        QuoteInProgress.UsageAdjTotal = (QuoteInProgress.IsOverrideUsageAdj != null && (bool)QuoteInProgress.IsOverrideUsageAdj) ? (decimal)QuoteInProgress.UsageAdjTotal : lnTotUseAdj;

                        QuoteInProgress.AdjAmtTotal = lnTotAdjAmt;
                        if (QuoteInProgress.PrepayTotal == null)
                            QuoteInProgress.PrepayTotal = 0;


                        QuoteInProgress.RemainingAmtTotal = ((QuoteInProgress.IsOverrideTotalQuote != null && (bool)QuoteInProgress.IsOverrideTotalQuote) ? (decimal)QuoteInProgress.QuoteTotal : ((decimal)QuoteInProgress.SubtotalTotal + lnTotTaxAmt - lnTotDiscAmt)) - (decimal)QuoteInProgress.PrepayTotal;

                        QuoteInProgress.FlightChargeTotal = totchrg;

                        if (QuoteInProgress.IsOverrideCost != null && (bool)QuoteInProgress.IsOverrideCost)
                            lnTotCost = (decimal)QuoteInProgress.CostTotal;
                        else
                            QuoteInProgress.CostTotal = lnTotBuyAmt;

                        //Margin Calculation
                        lnTotMargin = (decimal)QuoteInProgress.SubtotalTotal - (decimal)QuoteInProgress.CostTotal - (decimal)QuoteInProgress.DiscountAmtTotal;
                        lnTotMarPct = (decimal) QuoteInProgress.SubtotalTotal == 0 ? 0 : Math.Round((lnTotMargin/(decimal) QuoteInProgress.SubtotalTotal)*100, 2);

                        QuoteInProgress.MarginTotal = lnTotMargin;
                        QuoteInProgress.MarginalPercentTotal = lnTotMarPct;
                    }
                    //}
                }

                SaveQuoteinProgressToFileSession();

            }
        }

        public decimal CalculateSumOfETE(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {

                    if (orderNum == 1)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == false && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;

                    }
                    else if (orderNum == 2)
                    {
                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == true && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                    else
                    {

                        var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.ElapseTM != null);
                        if (quoteList.ToList().Count > 0)
                            result = quoteList.Sum(x => x.ElapseTM).Value;
                    }
                }
                return result;
            }
        }

        public DateTime CalculateMinDeparttDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DepartureDTTMLocal != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Min(x => x.DepartureDTTMLocal).Value;
                }
                return result;
            }
        }

        public DateTime CalculateMaxArrivalDate()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                DateTime result = DateTime.MinValue;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.ArrivalDTTMLocal != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Max(x => x.ArrivalDTTMLocal).Value;
                }
                return result;
            }
        }

        public decimal CalculateSumOfTotalCharge()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;
                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.ChargeTotal).Value;
                }
                return result;
            }
        }

        public decimal CalculateSumOfDistance(int orderNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderNum))
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    if (orderNum == 1)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == false).Sum(x => x.Distance).Value;
                    }
                    else if (orderNum == 2)
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.IsPositioning == true).Sum(x => x.Distance).Value;
                    }
                    else
                    {
                        result = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Sum(x => x.Distance).Value;
                    }
                }
                return result;
            }
        }

        public decimal CalculateSumOfRON()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal result = 0.0M;

                if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                {
                    var quoteList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null && x.DayRONCNT != null && x.RemainOverNightCNT != null);
                    if (quoteList.ToList().Count > 0)
                        result = quoteList.Sum(x => x.DayRONCNT + x.RemainOverNightCNT).Value;
                }
                return result;
            }
        }

        public decimal CalculateSegFee()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                decimal lnTotSegFee = 0;
                decimal lnTaxRate = 0;
                decimal lnExRate = 0;
                decimal lnCqSegAk = 0;
                decimal lnCqSegHi = 0;
                decimal lnIntlSegFeeRate = 0;
                decimal lnDomSegFeeRate = 0;

                if (QuoteInProgress != null)
                {
                    if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0)
                    {
                        lnTaxRate = QuoteInProgress.FederalTax != null ? (decimal)QuoteInProgress.FederalTax : 0.0M;
                        lnExRate = (FileRequest.ExchangeRate != null && FileRequest.ExchangeRate > 0) ? (decimal)FileRequest.ExchangeRate : 1;

                        FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId company = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                        if (FileRequest.HomebaseID != null)
                            company = GetCompany((long)FileRequest.HomebaseID);
                        else
                        {
                            if (UserPrincipal.Identity._homeBaseId != null)
                                company = GetCompany((long)UserPrincipal.Identity._homeBaseId);
                        }
                        if (company != null)
                        {
                            if (company.SegmentFeeAlaska != null)
                                lnCqSegAk = (decimal)company.SegmentFeeAlaska * lnExRate;
                            if (company.SegementFeeHawaii != null)
                                lnCqSegHi = (decimal)company.SegementFeeHawaii * lnExRate;
                            if (company.ChtQouteIntlSegCHG != null)
                                lnIntlSegFeeRate = (decimal)company.ChtQouteIntlSegCHG * lnExRate;
                            if (company.ChtQouteDOMSegCHG != null)
                                lnDomSegFeeRate = (decimal)company.ChtQouteDOMSegCHG * lnExRate;
                        }
                        foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => (x.IsDeleted == false && x.DAirportID != null && x.AAirportID != null)).ToList())
                        {
                            FlightPak.Web.FlightPakMasterService.GetAllAirport DepartAirport = GetAirport((long)Leg.DAirportID);
                            FlightPak.Web.FlightPakMasterService.GetAllAirport ArrAirport = GetAirport((long)Leg.AAirportID);
                            decimal lnDomSegFee = 0;
                            decimal lnIntlSegFee = 0;
                            decimal lnPax_Total = 0;

                            if (DepartAirport != null && ArrAirport != null)
                            {
                                bool DepartRural = false;
                                bool ArrRural = false;
                                if (DepartAirport.IsRural != null)
                                    DepartRural = (bool)DepartAirport.IsRural;
                                if (ArrAirport.IsRural != null)
                                    ArrRural = (bool)ArrAirport.IsRural;

                                if (!ArrRural && !DepartRural)
                                {
                                    string lcCqSegState = string.Empty;
                                    if (DepartAirport.StateName != null && ArrAirport.StateName != null)
                                    {
                                        lcCqSegState = GetStateSegFee(DepartAirport.StateName, ArrAirport.StateName);
                                    }

                                    if ((bool)QuoteInProgress.IsTaxable && (bool)Leg.IsTaxable)
                                    {
                                        switch (lcCqSegState)
                                        {
                                            case "AK":
                                                lnDomSegFeeRate = lnCqSegAk > 0 ? lnCqSegAk : lnDomSegFeeRate;
                                                break;
                                            case "HI":
                                                lnDomSegFeeRate = lnCqSegHi > 0 ? lnCqSegHi : lnDomSegFeeRate; ;
                                                break;
                                        }
                                        lnPax_Total = Leg.PassengerTotal != null ? (decimal)Leg.PassengerTotal : 0;
                                        if (Leg.DutyTYPE == 1)
                                            lnDomSegFee = lnDomSegFeeRate * lnPax_Total; //lnDomSegFee = lnDomSegFeeRate * lnExRate * lnPax_Total;
                                        else if (Leg.DutyTYPE == 2)
                                            lnIntlSegFee = lnIntlSegFeeRate * lnPax_Total; //lnIntlSegFee = lnIntlSegFeeRate * lnExRate * lnPax_Total;

                                        lnTotSegFee += lnDomSegFee + lnIntlSegFee;
                                    }
                                }
                            }
                        }
                        //QuoteInProgress.SegmentFeeTotal = lnTotSegFee;
                        //SaveQuoteinProgressToFileSession();
                    }
                }
                return lnTotSegFee;
            }
        }

        public void CalculateQuantity()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                {
                    foreach (CQFleetChargeDetail chargeRate in QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false))
                    {
                        if (chargeRate.NegotiatedChgUnit != null)
                        {
                            if (chargeRate.CQFleetChargeDetailID != 0)
                                chargeRate.State = CQRequestEntityState.Modified;

                            //Quantity = Sum of ETE where cqfltchg.nchrgunit is 1 
                            if (chargeRate.NegotiatedChgUnit == 1 && chargeRate.OrderNUM != null)
                            {
                                chargeRate.Quantity = CalculateSumOfETE((int)chargeRate.OrderNUM);
                            }

                            //Quantity = Sum of Distance where cqfltchg.nchrgunit is 2
                            //Quantity = Sum of Distance multiplied by StatueConvFactor where nchrgunit is 3
                            if (chargeRate.NegotiatedChgUnit == 2 && chargeRate.OrderNUM != null)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM);
                            }

                            if (chargeRate.NegotiatedChgUnit == 3 && chargeRate.OrderNUM != null)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * statuateConvFactor;
                            }

                            //Quantity = Total number of legs where nchrgunit is 7
                            if (chargeRate.NegotiatedChgUnit == 7)
                            {
                                if (QuoteInProgress.CQLegs != null)
                                    chargeRate.Quantity = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.AAirportID != null && x.DAirportID != null).Count();
                            }

                            //Quantity = Sum of Distance * 1.852M, where cqfltchg.nchrgunit is 8
                            if (chargeRate.NegotiatedChgUnit == 8 && chargeRate.OrderNUM != null)
                            {
                                chargeRate.Quantity = CalculateSumOfDistance((int)chargeRate.OrderNUM) * 1.852M;
                            }

                            //Quantity = Sum of RON and Day Room of all legs where ordernum is 3 
                            if (chargeRate.OrderNUM == 3)
                            {
                                chargeRate.Quantity = CalculateSumOfRON();
                            }
                        }
                    }

                    SaveQuoteinProgressToFileSession();

                    //BindStandardCharges(true);
                }
            }
        }

        public string GetStateSegFee(string DepartState, string ArrivalState)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartState, ArrivalState))
            {
                string segState = string.Empty;
                if (!string.IsNullOrEmpty(DepartState) && (DepartState == "AK" || DepartState == "HI"))
                    segState = DepartState;
                if (!string.IsNullOrEmpty(ArrivalState) && (ArrivalState == "AK" || ArrivalState == "HI"))
                    segState = ArrivalState;
                return segState;
            }
        }

        public FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Companymaster = new FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                    var objCompany = objDstsvc.GetListInfoByHomeBaseId(HomeBaseID);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId> Company = (from Comp in objCompany.EntityList
                                                                                                              where Comp.HomebaseID == HomeBaseID
                                                                                                              select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }

        public FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 AirportID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPak.Web.FlightPakMasterService.GetAllAirport Airportmaster = new FlightPak.Web.FlightPakMasterService.GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(AirportID);
                    if (objAirport.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = (from Arpt in objAirport.EntityList
                        //                                                                       where Arpt.IcaoID == DAirportID
                        //                                                                       select Arpt).ToList();

                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;

                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;

                }
            }
        }

        public FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = (from aircraft in objPowerSetting.EntityList
                        //                                                                    where aircraft.AircraftID == AircraftID
                        //                                                                    select aircraft).ToList();

                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;


                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;

                }
            }
        }

        public void SaveQuoteinProgressToFileSession()
        {
            if (Session[CQSessionKey] != null)
            {
                FileRequest = (CQFile)Session[CQSessionKey];

                CQMain QuoteToUpdate = new CQMain();

                if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                {
                    QuoteToUpdate = FileRequest.CQMains.Where(x => x.QuoteNUM == FileRequest.QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                    QuoteToUpdate = QuoteInProgress;
                }
                Session[CQSessionKey] = FileRequest;

            }
        }

        /// <summary>
        /// Method to Bind Standard Charges
        /// </summary>
        /// <param name="isRebind"></param>
        public void BindStandardCharges(bool isRebind, RadGrid dgStandardCharges)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isRebind))
            {
                dgStandardCharges.DataSource = new string[] { };
                if (isRebind)
                    dgStandardCharges.Rebind();

                if (Session[CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[CQSessionKey];

                    if (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0)
                    {
                        Int64 QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                        QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                        if (QuoteInProgress != null && QuoteInProgress.CQFleetChargeDetails != null)
                        {
                            dgStandardCharges.DataSource = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM).ToList();

                            if (isRebind)
                                dgStandardCharges.Rebind();
                        }
                        else
                        {
                            List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

                            if (QuoteInProgress != null)
                            {
                                using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                                {
                                    // Get New Charter Rates from Fleet Profile Catalog
                                    if (QuoteInProgress != null && QuoteInProgress.FleetID != null)
                                    {
                                        FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == Convert.ToInt64(QuoteInProgress.FleetID) && x.AircraftTypeID == null).ToList();
                                    }
                                    // Get New Charter Rates from Aircraft Type Catalog, if the Source is selected as Type and Tail Number is Null/Empty
                                    else if (QuoteInProgress.FleetID == null && QuoteInProgress.AircraftID != null)
                                    {
                                        FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID == null && (x.AircraftTypeID != null && x.AircraftTypeID == Convert.ToInt64(QuoteInProgress.AircraftID))).ToList();
                                    }

                                    if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                                    {
                                        QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();
                                        List<CQFleetChargeDetail> chargeList = new List<CQFleetChargeDetail>();

                                        foreach (FlightPakMasterService.GetAllFleetNewCharterRate chargeRate in FleetChargeRateList)
                                        {
                                            CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                            fleetCharge.State = CQRequestEntityState.Added;
                                            fleetCharge.CQFlightChargeDescription = chargeRate.FleetNewCharterRateDescription;
                                            fleetCharge.ChargeUnit = chargeRate.ChargeUnit;
                                            fleetCharge.NegotiatedChgUnit = chargeRate.NegotiatedChgUnit;
                                            fleetCharge.Quantity = 0;
                                            fleetCharge.BuyDOM = chargeRate.BuyDOM;
                                            fleetCharge.SellDOM = chargeRate.SellDOM;
                                            fleetCharge.IsTaxDOM = chargeRate.IsTaxDOM;
                                            fleetCharge.IsDiscountDOM = chargeRate.IsDiscountDOM;
                                            fleetCharge.BuyIntl = chargeRate.BuyIntl;
                                            fleetCharge.SellIntl = chargeRate.SellIntl;
                                            fleetCharge.IsTaxIntl = chargeRate.IsTaxIntl;
                                            fleetCharge.IsDiscountIntl = chargeRate.IsDiscountIntl;
                                            fleetCharge.FeeAMT = 0;
                                            fleetCharge.OrderNUM = chargeRate.OrderNum;
                                            fleetCharge.IsDeleted = false;

                                            chargeList.Add(fleetCharge);
                                        }
                                        QuoteInProgress.CQFleetChargeDetails = chargeList;

                                        CalculateQuantity();

                                        dgStandardCharges.DataSource = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).OrderBy(x => x.OrderNUM).ToList();
                                    }
                                }

                                SaveQuoteinProgressToFileSession();

                                if (isRebind)
                                    dgStandardCharges.Rebind();
                            }
                        }
                    }
                }
            }
        }

        public void DoFileCalculation()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session[CQSessionKey] != null)
                {
                    FileRequest = (CQFile)Session[CQSessionKey];

                    if (FileRequest != null && FileRequest.CQMains != null && FileRequest.CQMains.Count() > 0)
                    {

                        foreach (CQMain quote in FileRequest.CQMains.Where(x => x.IsDeleted == false))
                        {
                            QuoteInProgress = quote;
                            DoLegCalculationsForQuote();
                            //if (FileRequest.UseCustomFleetCharge != null && (bool)FileRequest.UseCustomFleetCharge)
                            //    BindFleetChargeDetails(0, 0, FileRequest.CQCustomerID != null ? (long)FileRequest.CQCustomerID : 0, true);
                            //else
                            //    BindFleetChargeDetails(quote.FleetID != null ? (long)quote.FleetID : 0, quote.AircraftID != null ? (long)quote.AircraftID : 0, 0, false);

                            CalculateQuantity();
                            CalculateQuoteTotal();
                        }

                    }
                }
            }
        }

        public void DoLegCalculationsForDomIntlTaxable()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId Fleetcompany = new FlightPakMasterService.GetAllCompanyMasterbyHomeBaseId();
                List<FlightPakMasterService.GetFleet> GetFleetList = new List<FlightPakMasterService.GetFleet>();
                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<FlightPakMasterService.GetAllAirport>();
                var fleetretval = FleetService.GetFleet();
                if (fleetretval.ReturnFlag)
                {

                    if (QuoteInProgress != null)
                    {
                        #region commented DOM/INTL  based on Fleet
                        //if (QuoteInProgress.FleetID != 0 && QuoteInProgress.FleetID != null)
                        //{
                        //    GetFleetList = fleetretval.EntityList.Where(x => x.FleetID == (long)QuoteInProgress.FleetID).ToList();
                        //    if (GetFleetList != null && GetFleetList.Count > 0)
                        //    {

                        //        if (GetFleetList[0].HomebaseID != null)
                        //        {
                        //            Fleetcompany = GetCompany((long)GetFleetList[0].HomebaseID);
                        //            FlightPakMasterService.GetAllAirport Fleetcompanyairport = new FlightPakMasterService.GetAllAirport();
                        //            if (Fleetcompany != null && Fleetcompany.HomebaseAirportID != null)
                        //            {
                        //                Int64 countryID = 0;
                        //                Fleetcompanyairport = GetAirport((long)Fleetcompany.HomebaseAirportID);
                        //                if (Fleetcompanyairport != null && Fleetcompanyairport.CountryID != null)
                        //                    countryID = (long)Fleetcompanyairport.CountryID;
                        #endregion
                        Int64 countryID = 0;
                        if (Session[CQSessionKey] != null)
                        {
                            FileRequest = (CQFile)Session[CQSessionKey];
                            if (FileRequest.HomebaseID != 0 && FileRequest.HomebaseID != null)
                            {
                                Fleetcompany = GetCompany((long)FileRequest.HomebaseID);
                                if (Fleetcompany != null && Fleetcompany.HomebaseAirportID != null)
                                {
                                    
                                    FlightPakMasterService.GetAllAirport Fleetcompanyairport = new FlightPakMasterService.GetAllAirport();
                                    Fleetcompanyairport = GetAirport((long)Fleetcompany.HomebaseAirportID);
                                    if (Fleetcompanyairport != null && Fleetcompanyairport.CountryID != null)
                                        countryID = (long)Fleetcompanyairport.CountryID;
                                }
                            }
                        }
                        if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count >= 0)
                        {
                            List<CQLeg> PrefLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            foreach (CQLeg Leg in PrefLeg)
                            {
                                bool isRural = false;
                                if (Leg.DAirportID != 0 && Leg.AAirportID != 0 && Leg.DAirportID != null && Leg.AAirportID != null)
                                {
                                    using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objDepRetVal = objDstsvc.GetAirportByAirportID((long)Leg.DAirportID).EntityList;
                                        //List<FlightPakMasterService.GetAllAirport> getDeptAirportList = new List<GetAllAirport>();

                                        var objArrRetVal = objDstsvc.GetAirportByAirportID((long)Leg.AAirportID).EntityList;
                                        //List<FlightPakMasterService.GetAllAirport> getArrivalAirportList = new List<GetAllAirport>();

                                        Leg.DutyTYPE = 1;
                                        #region "DOM/INTL"
                                        if (countryID != null && countryID != 0)
                                        {

                                            if (objDepRetVal.Count > 0 && objArrRetVal.Count > 0)
                                            {
                                                if (objDepRetVal[0].CountryID == null)
                                                {
                                                    Leg.DutyTYPE = 1;
                                                }
                                                else
                                                {
                                                    if (objDepRetVal[0].CountryID != null)
                                                    {
                                                        if (countryID != objDepRetVal[0].CountryID)
                                                        {
                                                            Leg.DutyTYPE = 2;
                                                        }
                                                    }
                                                }


                                                if (objArrRetVal[0].CountryID == null)
                                                {
                                                    Leg.DutyTYPE = 1;
                                                }
                                                else
                                                {
                                                    if (objArrRetVal[0].CountryID != null)
                                                    {

                                                        if (countryID != objArrRetVal[0].CountryID)
                                                        {
                                                            Leg.DutyTYPE = 2;
                                                        }

                                                    }
                                                }
                                            }

                                        }

                                        #endregion
                                        if (objDepRetVal[0].IsRural != null && (bool)objDepRetVal[0].IsRural)
                                            isRural = true;

                                        if (objArrRetVal[0].IsRural != null && (bool)objArrRetVal[0].IsRural)
                                            isRural = true;

                                        #region TAX
                                        //Taxable Company profile Settings
                                        if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 1)
                                        {
                                            Leg.IsTaxable = true;

                                        }
                                        else if (UserPrincipal.Identity._fpSettings._IsAppliedTax != null && UserPrincipal.Identity._fpSettings._IsAppliedTax == 2)
                                        {
                                            if (Leg.DutyTYPE == 1)
                                                Leg.IsTaxable = true;
                                        }
                                        else
                                            Leg.IsTaxable = false;


                                        if ((bool)Leg.IsTaxable)
                                        {


                                            Leg.TaxRate = 0;

                                            if (isRural)
                                            {
                                                if (UserPrincipal.Identity._fpSettings._RuralTax != null)
                                                    Leg.TaxRate = UserPrincipal.Identity._fpSettings._RuralTax;
                                            }
                                            else
                                            {
                                                if (UserPrincipal.Identity._fpSettings._CQFederalTax != null)
                                                    Leg.TaxRate = UserPrincipal.Identity._fpSettings._CQFederalTax;
                                            }



                                        }
                                        //Taxable Company profile Settings
                                        #endregion

                                    }
                                }

                                //            }
                                //        }
                                //    }
                                //}
                            }
                        }
                        SaveQuoteinProgressToFileSession();
                    }
                }


            }

        }

        public void DoLegCalculationsForQuote()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient CalcService = new CalculationService.CalculationServiceClient())
                {
                    // Loop through Leg level for Calculation
                    if (QuoteInProgress != null && (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Count() > 0))
                    {
                        List<CQLeg> LegList = new List<CQLeg>();
                        LegList = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        foreach (CQLeg Leg in LegList)
                        {
                            #region Miles
                            double Miles = 0;
                            if (Leg.DAirportID > 0 && Leg.AAirportID > 0)
                            {
                                Miles = CalcService.GetDistance((long)Leg.DAirportID, (long)Leg.AAirportID);
                            }
                            Leg.Distance = (decimal)Miles;
                            #endregion
                            #region Bias

                            List<double> BiasList = new List<double>();

                            if (Leg.PowerSetting == null)
                                Leg.PowerSetting = "1";

                            if (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID != 0)
                            {
                                FlightPakMasterService.Aircraft TripAircraft = new FlightPakMasterService.Aircraft();
                                TripAircraft = GetAircraft((long)QuoteInProgress.AircraftID);
                                Leg.PowerSetting = TripAircraft.PowerSetting != null ? TripAircraft.PowerSetting : "1";

                            }

                            if (
                                (Leg.DAirportID != null && Leg.DAirportID > 0)
                                &&
                                (Leg.AAirportID != null && Leg.AAirportID > 0)
                                &&
                                (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID > 0)
                                )
                            {
                                BiasList = CalcService.CalcBiasTas((long)Leg.DAirportID, (long)Leg.AAirportID, (long)QuoteInProgress.AircraftID, Leg.PowerSetting);
                                if (BiasList != null)
                                {
                                    // lnToBias, lnLndBias, lnTas
                                    Leg.TakeoffBIAS = (decimal)BiasList[0];
                                    Leg.LandingBIAS = (decimal)BiasList[1];
                                    Leg.TrueAirSpeed = (decimal)BiasList[2];
                                }
                            }
                            else
                            {
                                Leg.TakeoffBIAS = 0.0M;
                                Leg.LandingBIAS = 0.0M; ;
                                Leg.TrueAirSpeed = 0.0M;
                            }
                            #endregion
                            #region "Wind"
                            Leg.WindsBoeingTable = 0.0M;
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;
                                //Function Wind Calculation
                                double Wind = 0;
                                if (
                                (Leg.DAirportID != null && Leg.DAirportID > 0)
                                &&
                                (Leg.AAirportID != null && Leg.AAirportID > 0)
                                &&
                                (QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID > 0)
                                )
                                {
                                    Wind = CalcService.GetWind((long)Leg.DAirportID, (long)Leg.AAirportID, Convert.ToInt32(Leg.WindReliability), (long)QuoteInProgress.AircraftID, GetQtr(dt).ToString());
                                    Leg.WindsBoeingTable = (decimal)Wind;
                                }
                            }
                            #endregion
                            #region "ETE"
                            double ETE = 0;
                            // lnToBias, lnLndBias, lnTas

                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;


                                ETE = CalcService.GetIcaoEte((double)Leg.WindsBoeingTable, (double)Leg.LandingBIAS, (double)Leg.TrueAirSpeed, (double)Leg.TakeoffBIAS, (double)Leg.Distance, dt);

                                ETE = RoundElpTime(ETE);

                                if (ETE == 0)
                                {
                                    if (QuoteInProgress.AircraftID != null)
                                    {
                                        Int64 AircraftID = Convert.ToInt64(QuoteInProgress.AircraftID);
                                        FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);

                                        if (Aircraft != null)
                                            if (Aircraft.IsFixedRotary != null && Aircraft.IsFixedRotary.ToUpper() == "R")
                                                ETE = 0.1;
                                    }
                                }
                            }

                            Leg.ElapseTM = (decimal)ETE;

                            #endregion
                            #region CalculateDatetime

                            double x10, x11;
                            // int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                            // DateTime Arrivaldt;
                            //try
                            //{


                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime ChangedDate;

                                ChangedDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (ChangedDate != null)
                                {


                                    DateTime EstDepartDate = (DateTime)ChangedDate;
                                    //if (QuoteInProgress.EstDepartureDT != null)
                                    //{
                                    //    EstDepartDate = (DateTime)QuoteInProgress.EstDepartureDT;
                                    //}



                                    try
                                    {

                                        //(FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)&&
                                        if ((Leg.DAirportID != null && Leg.DAirportID > 0)
                                            && (Leg.AAirportID != null && Leg.AAirportID > 0)
                                            )
                                        {

                                            if (!string.IsNullOrEmpty(Leg.ElapseTM.ToString()))
                                            {
                                                string ETEstr = "0";

                                                ETEstr = Leg.ElapseTM.ToString();


                                                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                                x10 = (tripleg_elp_time * 60 * 60);
                                                x11 = (tripleg_elp_time * 60 * 60);
                                            }
                                            else
                                            {
                                                x10 = 0;
                                                x11 = 0;
                                            }
                                            DateTime DeptUTCdt;
                                            DateTime ArrUTCdt;

                                            DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;
                                            ArrUTCdt = DeptUTCdt;
                                            ArrUTCdt = ArrUTCdt.AddSeconds(x10);
                                            DateTime ldLocDep = DateTime.MinValue;
                                            DateTime ldLocArr = DateTime.MinValue;
                                            DateTime ldHomDep = DateTime.MinValue;
                                            DateTime ldHomArr = DateTime.MinValue;
                                            ldLocDep = CalcService.GetGMT((long)Leg.DAirportID, DeptUTCdt, false, false);



                                            if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                ldHomDep = CalcService.GetGMT((long)FileRequest.HomeBaseAirportID, DeptUTCdt, false, false);


                                            ldLocArr = CalcService.GetGMT((long)Leg.AAirportID, ArrUTCdt, false, false);

                                            if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                ldHomArr = CalcService.GetGMT((long)FileRequest.HomeBaseAirportID, ArrUTCdt, false, false);

                                            //DateTime ltBlank = EstDepartDate;

                                            if (Leg.DAirportID > 0)
                                            {

                                                Leg.DepartureDTTMLocal = ldLocDep;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = ldHomDep;

                                                Leg.DepartureGreenwichDTTM = DeptUTCdt;

                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;

                                                Leg.DepartureDTTMLocal = Localdt;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = Localdt;
                                                Leg.DepartureGreenwichDTTM = Localdt;

                                            }

                                            if (Leg.AAirportID > 0)
                                            {
                                                // DateTime dt = ldLocArr;

                                                Leg.ArrivalDTTMLocal = ldLocArr;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = ldHomArr;
                                                Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;

                                                Leg.ArrivalDTTMLocal = Localdt;
                                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = Localdt;
                                                Leg.ArrivalGreenwichDTTM = Localdt;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //Manually Handled
                                        if (Leg.ArrivalDTTMLocal == null)
                                        {
                                            Leg.ArrivalDTTMLocal = EstDepartDate;
                                        }
                                        if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                            if (Leg.HomeArrivalDTTM == null)
                                            {
                                                Leg.HomeArrivalDTTM = EstDepartDate;
                                            }

                                        if (Leg.ArrivalGreenwichDTTM == null)
                                        {
                                            Leg.ArrivalGreenwichDTTM = EstDepartDate;
                                        }

                                        if (Leg.DepartureDTTMLocal == null)
                                        {
                                            Leg.DepartureDTTMLocal = EstDepartDate;
                                        }

                                        if (Leg.DepartureGreenwichDTTM == null)
                                        {
                                            Leg.DepartureGreenwichDTTM = EstDepartDate;
                                        }
                                        if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID > 0)
                                            if (Leg.HomeDepartureDTTM == null)
                                            {
                                                Leg.HomeDepartureDTTM = EstDepartDate;
                                            }
                                    }

                                }
                            }
                            #endregion
                        }

                        CalculateFAR();

                        SaveQuoteinProgressToFileSession();
                    }
                }
            }
        }

        public void CalculateFAR()
        {
            #region "PreflightFARRule"
            double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
               lnNeededRest, lnWorkArea;

            lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0;

            bool llRProblem, llFProblem, llDutyBegin, llDProblem;
            llRProblem = llFProblem = llDutyBegin = llDProblem = false;

            DateTime ltGmtDep, ltGmtArr, llDutyend;
            Int64 lnDuty_RulesID = 0;
            string lcDuty_Rules = string.Empty;


            if (QuoteInProgress.CQLegs != null && QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).Count() > 0)
            {
                List<CQLeg> Preflegs = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                lnDuty_Hrs = 0;
                lnRest_Hrs = 0;
                lnFlt_Hrs = 0;
                //if ((DateTime)Preflegs[0].DepartureGreenwichDTTM != null)
                //{
                //    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                //}
                //else
                //{
                //    ltGmtDep = DateTime.MinValue;
                //}

                if (Preflegs[0].DepartureGreenwichDTTM != null)
                {
                    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                    ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                    llDutyBegin = true;
                    double lnOldDuty_hrs = -1;

                    int legcounter = 0;
                    foreach (CQLeg Leg in Preflegs)
                    {
                        if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                        {

                            double dbElapseTime = 0.0;
                            long dbCrewDutyRulesID = 0;

                            if (Leg.ElapseTM != null)
                                dbElapseTime = (double)Leg.ElapseTM;

                            //if (Leg.CrewDutyRulesID != null)
                            //{

                            //    dbCrewDutyRulesID = (Int64)Leg.CrewDutyRulesID;
                            //}
                            //if (dbCrewDutyRulesID != 0)
                            //{
                            double lnOverRide = 0;
                            double lnDutyLeg_Num = 0;

                            bool llDutyEnd = false;
                            string FARNum = "";

                            if (Leg.CrewDutyRulesID != null)
                                lnDuty_RulesID = (Int64)Leg.CrewDutyRulesID;
                            else
                                lnDuty_RulesID = 0;
                            if (Leg.IsDutyEnd == null)
                                llDutyEnd = false;
                            else
                                llDutyEnd = (bool)Leg.IsDutyEnd;

                            //if it is last leg then  llDutyEnd = true;
                            if (lnEndLegNum == Leg.LegNUM)
                                llDutyEnd = true;


                            lnOverRide = (double)(Leg.CQOverRide == null ? 0 : Leg.CQOverRide);



                            lnDutyLeg_Num = (double)Leg.LegNUM;



                            FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient objectDstsvc = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                            var objRetVal = objectDstsvc.GetCrewDutyRuleList();
                            if (objRetVal.ReturnFlag)
                            {
                                List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = (from CrewDutyRl in objRetVal.EntityList
                                                                                          where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID
                                                                                          select CrewDutyRl).ToList();

                                if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                {
                                    FARNum = CrewDtyRule[0].FedAviatRegNum;
                                    lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                    lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                    lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                    lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                    lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                    lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                    lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                }
                                else
                                {
                                    lnDayBeg = 0;
                                    lnDayEnd = 0;
                                    lnMaxDuty = 0;
                                    lnMaxFlt = 0;
                                    lnMinRest = 8;
                                    lnRestMulti = 0;
                                    lnRestPlus = 0;
                                }
                            }
                            TimeSpan? Hoursdiff = (DateTime)Leg.DepartureGreenwichDTTM - ltGmtArr;
                            if (Leg.ElapseTM != 0)
                            {
                                lnFlt_Hrs = lnFlt_Hrs + (double)Leg.ElapseTM;
                                lnDuty_Hrs = lnDuty_Hrs + (double)Leg.ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                            }
                            else
                            {

                                lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                            }

                            llRProblem = false;

                            if (llDutyBegin && lnOldDuty_hrs != -1)
                            {

                                lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                if (lnRestMulti > 0)
                                {

                                    //Here is the Multiple and Plus hours usage
                                    lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                    if (lnRest_Hrs < lnNeededRest)
                                    {

                                        llRProblem = true;
                                    }
                                }
                                else
                                {
                                    if (lnRest_Hrs < (lnMinRest + lnRestPlus))
                                    {

                                        llRProblem = true;
                                    }

                                }
                            }
                            else
                            {
                                if (llDutyBegin)
                                {
                                    lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                }
                            }

                            if (llDutyEnd)
                            {
                                lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                            }

                            string lcCdAlert = "";

                            if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than tripleg.maxflt
                                lcCdAlert = "F";// Flight time violation and F is stored in tripleg.cdalert as a Flight time error
                            else
                                lcCdAlert = "";



                            if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in tripleg.cdalert as a Duty time error

                            // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;

                            if (llRProblem)
                                lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in tripleg.cdalert as a Rest time error 

                            if (Leg.CQLegID != 0 && Leg.State != CQRequestEntityState.Deleted)
                                Leg.State = CQRequestEntityState.Modified;
                            Leg.FlightHours = (decimal)lnFlt_Hrs;
                            Leg.DutyHours = (decimal)lnDuty_Hrs;
                            Leg.RestHours = (decimal)lnRest_Hrs;
                            Leg.CrewDutyAlert = lcCdAlert;
                            //Leg.IsDutyEnd = llDutyEnd;


                            lnLeg_Num = (double)Leg.LegNUM;

                            llDutyEnd = false;
                            if (Leg.IsDutyEnd != null)
                                llDutyEnd = (bool)Leg.IsDutyEnd;

                            ltGmtArr = (DateTime)Leg.ArrivalGreenwichDTTM;

                            if ((bool)Leg.IsDutyEnd)
                            {
                                llDutyBegin = true;
                                lnFlt_Hrs = 0;
                            }
                            else
                            {
                                llDutyBegin = false;
                            }

                            lnOldDuty_hrs = lnDuty_Hrs;
                            //check next leg if available do the below steps
                            legcounter++;
                            if (legcounter < Preflegs.Count)
                            {
                                if (llDutyEnd)
                                {
                                    lnDuty_Hrs = 0;

                                    if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                    {
                                        TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                        lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                        //ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                        ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                    }
                                }
                                else
                                {
                                    lnRest_Hrs = 0;
                                }
                            }
                            //check next leg if available do the below steps

                            //}
                        }

                    }
                }
            }
            //}
            //catch (Exception)
            //{
            //    lbTotalDuty.Text = "0.0";
            //    lbTotalFlight.Text = "0.0";
            //    lbRest.Text = "0.0";
            //}

            #endregion
        }

        public void DoDatetimecalculationforLeg(ref CQLeg Leg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                CalculateDateTime(ref Leg);
                CalculateWind(ref Leg);
                CalculateETE(ref Leg);
                Leg.IsDepartureConfirmed = true;
                CalculateDateTime(ref Leg);
            }
        }

        private void CalculateETE(ref CQLeg Leg)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                //try
                //{
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {
                    double ETE = 0;
                    // lnToBias, lnLndBias, lnTas
                    double Wind = 0;
                    double LandBias = 0;
                    double TAS = 0;
                    double Bias = 0;
                    double Miles = 0;

                    if (Leg.WindsBoeingTable != null)
                        Wind = (double)Leg.WindsBoeingTable;

                    if (Leg.LandingBIAS != null)
                        LandBias = (double)Leg.LandingBIAS;

                    if (Leg.TakeoffBIAS != null)
                        Bias = (double)Leg.TakeoffBIAS;


                    if (Leg.TrueAirSpeed != null)
                        TAS = (long)Leg.TrueAirSpeed;



                    if (Leg.Distance != null)
                    {
                        Miles = (double)Leg.Distance;
                    }

                    DateTime dt = DateTime.Now;

                    if (Leg.DepartureDTTMLocal != null)
                        dt = (DateTime)Leg.DepartureDTTMLocal;
                    ETE = objDstsvc.GetIcaoEte(Wind, LandBias, TAS, Bias, Miles, dt);

                    ETE = RoundElpTime(ETE);

                    Leg.ElapseTM = (decimal)ETE;
                }

            }
        }

        private void CalculateWind(ref CQLeg Leg)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //try
                //{
                if (Leg.DepartureDTTMLocal != null)
                {
                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                    {
                        //string StartTime = rmtlocaltime.Text.Trim();
                        string StartTime = "0000";

                        DateTime dt = (DateTime)Leg.DepartureDTTMLocal;

                        //Function Wind Calculation
                        double Wind = 0;
                        if (Leg.DAirportID != null && Leg.AAirportID != null && Leg.DAirportID != 0 && Leg.AAirportID != 0
                            && QuoteInProgress.AircraftID != null && QuoteInProgress.AircraftID != null
                            )
                        {
                            Wind = objDstsvc.GetWind((long)Leg.DAirportID, (long)Leg.AAirportID, (int)Leg.WindReliability, (long)QuoteInProgress.AircraftID, GetQtr(dt).ToString());
                            Leg.WindsBoeingTable = (decimal)Wind;
                        }
                    }
                }
                else
                    Leg.WindsBoeingTable = 0;

            }
        }

        private void CalculateDateTime(ref CQLeg Leg)//bool isDepartureConfirmed, DateTime? ChangedDate)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Leg))
            {
                double x10, x11;
                DateTime Arrivaldt;
                //try
                //{
                DateTime ChangedDate = (DateTime)Leg.DepartureDTTMLocal;
                bool isDepartureConfirmed = true;
                if (ChangedDate != null)
                {


                    DateTime EstDepartDate = (DateTime)ChangedDate;

                    try
                    {

                        //(!string.IsNullOrEmpty(hdnHomebaseAirport.Value) && hdnHomebaseAirport.Value != "0")&& 

                        if (Leg.DAirportID != null && Leg.AAirportID != null && Leg.DAirportID != 0 && Leg.AAirportID != 0)
                        {

                            if (Leg.ElapseTM != null)
                            {
                                double QuoteInProgressleg_elp_time = RoundElpTime((double)Leg.ElapseTM);
                                x10 = (QuoteInProgressleg_elp_time * 60 * 60);
                                x11 = (QuoteInProgressleg_elp_time * 60 * 60);
                            }
                            else
                            {
                                x10 = 0;
                                x11 = 0;
                            }
                            DateTime DeptUTCdt = DateTime.MinValue;
                            DateTime ArrUTCdt = DateTime.MinValue;

                            if (isDepartureConfirmed)
                            {
                                //DeptUTCDate
                                if (Leg.DepartureGreenwichDTTM != null)
                                    DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;

                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {
                                    Arrivaldt = DeptUTCdt;
                                    Arrivaldt = Arrivaldt.AddSeconds(x10);
                                    ArrUTCdt = Arrivaldt;

                                    Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                }
                            }

                            using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                            {
                                DateTime ldLocDep = DateTime.MinValue;
                                DateTime ldLocArr = DateTime.MinValue;
                                DateTime ldHomDep = DateTime.MinValue;
                                DateTime ldHomArr = DateTime.MinValue;
                                if (Leg.DAirportID != null && Leg.DAirportID != 0)
                                {
                                    ldLocDep = objDstsvc.GetGMT((long)Leg.DAirportID, DeptUTCdt, false, false);
                                }

                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                {
                                    ldHomDep = objDstsvc.GetGMT((long)FileRequest.HomeBaseAirportID, DeptUTCdt, false, false);
                                }

                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {
                                    ldLocArr = objDstsvc.GetGMT((long)Leg.AAirportID, ArrUTCdt, false, false);
                                }

                                if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                {
                                    ldHomArr = objDstsvc.GetGMT((long)FileRequest.HomeBaseAirportID, ArrUTCdt, false, false);
                                }

                                //DateTime ltBlank = EstDepartDate;

                                if (Leg.DAirportID != null && Leg.DAirportID != 0)
                                {
                                    Leg.DepartureDTTMLocal = ldLocDep;
                                    if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                        // DateTime Homedt = ldHomDep;
                                        Leg.HomeDepartureDTTM = ldHomDep;
                                }

                                if (Leg.AAirportID != null && Leg.AAirportID != 0)
                                {
                                    Leg.ArrivalDTTMLocal = ldLocArr;
                                    // DateTime dt = ldLocArr;

                                    if (FileRequest.HomeBaseAirportID != null && FileRequest.HomeBaseAirportID != 0)
                                        Leg.HomeArrivalDTTM = ldHomArr;
                                    // DateTime HomeArrivaldt = ldHomArr;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        //Manually handled

                        if (Leg.ArrivalDTTMLocal == null)
                            Leg.ArrivalDTTMLocal = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                        if (Leg.HomeDepartureDTTM == null || Leg.HomeArrivalDTTM == null)
                        {
                            Leg.HomeDepartureDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);
                            Leg.HomeArrivalDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);
                        }


                        if (Leg.ArrivalGreenwichDTTM == null)
                            Leg.ArrivalGreenwichDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                        if (Leg.DepartureDTTMLocal == null)
                            Leg.DepartureDTTMLocal = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                        if (Leg.DepartureGreenwichDTTM == null)
                            Leg.DepartureGreenwichDTTM = new DateTime(EstDepartDate.Year, EstDepartDate.Month, EstDepartDate.Day);

                    }

                }

            }

        }


        public void CalculateRemainingRon(Int64 LegNUM)
        {
            if (QuoteInProgress != null)
            {
                if (QuoteInProgress.CQLegs != null)
                {
                    CQLeg LeginProcess = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM).SingleOrDefault();

                    foreach (CQLeg Leg in QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null))
                    {
                        if (Leg.LegNUM >= LegNUM)
                        {
                            Int64 currlegnum = (Int64)Leg.LegNUM;
                            Int64 nextLegnum = currlegnum + 1;

                            CQLeg CurrentLeg = new CQLeg();
                            CurrentLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == currlegnum && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();

                            if (CurrentLeg != null)
                            {
                                CQLeg NextLeg = new CQLeg();
                                NextLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && (x.LegNUM == nextLegnum) && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();
                                if (NextLeg != null)
                                {
                                    using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                    {
                                        if (NextLeg.CQLegID != 0)
                                            NextLeg.State = CQRequestEntityState.Modified;

                                        if (NextLeg.DepartureDTTMLocal != null && CurrentLeg.ArrivalDTTMLocal != null)
                                        {
                                            DateTime CurrDate = new DateTime();
                                            DateTime NextDate = new DateTime();

                                            NextDate = new DateTime(((DateTime)NextLeg.DepartureDTTMLocal).Year, ((DateTime)NextLeg.DepartureDTTMLocal).Month, ((DateTime)NextLeg.DepartureDTTMLocal).Day);
                                            CurrDate = new DateTime(((DateTime)CurrentLeg.ArrivalDTTMLocal).Year, ((DateTime)CurrentLeg.ArrivalDTTMLocal).Month, ((DateTime)CurrentLeg.ArrivalDTTMLocal).Day);

                                            TimeSpan daydiff = NextDate.Subtract(CurrDate);
                                            if (daydiff.Days < 0)
                                            {
                                                CurrentLeg.RemainOverNightCNT = 0;
                                                LeginProcess.ShowWarningMessage = false;
                                                RadAjaxManagerMaster.ResponseScripts.Add(@"radconfirm('Warning - A problem within the legs has occurred and Auto RON can not be used until corrected.  Either the Departure Date or Time, Arrival Date or Time or the Ron Count needs to be adjusted.  Check the AutoRON Is Activated/AutoRON Is De-Activated After all corrections have been made.',null, 440, 210,null,'Confirmation!');");
                                                break;
                                            }
                                            else
                                            {
                                                if (NextLeg.CQLegID != 0)
                                                    NextLeg.State = CQRequestEntityState.Modified;
                                                int hourtoadd = 0;
                                                int minutestoAdd = 0;
                                                hourtoadd = ((DateTime)NextLeg.DepartureDTTMLocal).Hour;
                                                minutestoAdd = ((DateTime)NextLeg.DepartureDTTMLocal).Minute;

                                                NextLeg.DepartureDTTMLocal = ((DateTime)CurrentLeg.ArrivalDTTMLocal).AddDays((double)CurrentLeg.RemainOverNightCNT);

                                                NextLeg.DepartureDTTMLocal = new DateTime(
                                                    ((DateTime)NextLeg.DepartureDTTMLocal).Year,
                                                    ((DateTime)NextLeg.DepartureDTTMLocal).Month,
                                                    ((DateTime)NextLeg.DepartureDTTMLocal).Day,
                                                    hourtoadd,
                                                    minutestoAdd,
                                                    0);

                                                NextLeg.DepartureGreenwichDTTM = objDstsvc.GetGMT((Int64)CurrentLeg.DAirportID, (DateTime)NextLeg.DepartureDTTMLocal, true, false);
                                                DoDatetimecalculationforLeg(ref NextLeg);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        public void calculateRON(Int64 LegNUM, string FieldChanged)
        {

            if (QuoteInProgress != null && QuoteInProgress.CQLegs != null)
            {
                CQLeg CurrentLeg = new CQLeg();
                CurrentLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && x.LegNUM == LegNUM && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();

                if (CurrentLeg != null)
                {
                    if (UserPrincipal.Identity._fpSettings._IsAutoRON != null && (bool)UserPrincipal.Identity._fpSettings._IsAutoRON)
                    {
                        switch (FieldChanged.ToUpper())
                        {
                            case "RON":
                                {
                                    CQLeg NextLeg = new CQLeg();
                                    NextLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && (x.LegNUM == LegNUM + 1) && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();
                                    if (NextLeg != null)
                                    {
                                        using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                                        {
                                            if (NextLeg.CQLegID != 0)
                                                NextLeg.State = CQRequestEntityState.Modified;

                                            int hourtoadd = 0;
                                            int minutestoAdd = 0;
                                            hourtoadd = ((DateTime)NextLeg.DepartureDTTMLocal).Hour;
                                            minutestoAdd = ((DateTime)NextLeg.DepartureDTTMLocal).Minute;


                                            NextLeg.DepartureDTTMLocal = ((DateTime)CurrentLeg.ArrivalDTTMLocal).AddDays((double)CurrentLeg.RemainOverNightCNT);

                                            NextLeg.DepartureDTTMLocal = new DateTime(
                                                ((DateTime)NextLeg.DepartureDTTMLocal).Year,
                                                ((DateTime)NextLeg.DepartureDTTMLocal).Month,
                                                ((DateTime)NextLeg.DepartureDTTMLocal).Day,
                                                hourtoadd,
                                                minutestoAdd,
                                                0);

                                            NextLeg.DepartureGreenwichDTTM = objDstsvc.GetGMT((Int64)CurrentLeg.DAirportID, (DateTime)NextLeg.DepartureDTTMLocal, true, false);
                                            DoDatetimecalculationforLeg(ref NextLeg);
                                        }
                                    }

                                }
                                break;
                            case "DATE":
                                {
                                    CQLeg PrevLeg = new CQLeg();
                                    PrevLeg = QuoteInProgress.CQLegs.Where(x => x.IsDeleted == false && (x.LegNUM == LegNUM - 1) && x.DepartureDTTMLocal != null && x.ArrivalDTTMLocal != null).SingleOrDefault();
                                    if (PrevLeg != null)
                                    {
                                        if (PrevLeg.CQLegID != 0)
                                            PrevLeg.State = CQRequestEntityState.Modified;

                                        DateTime CurrDate = new DateTime();
                                        DateTime PrevDate = new DateTime();

                                        CurrDate = new DateTime(((DateTime)CurrentLeg.DepartureDTTMLocal).Year, ((DateTime)CurrentLeg.DepartureDTTMLocal).Month, ((DateTime)CurrentLeg.DepartureDTTMLocal).Day);
                                        PrevDate = new DateTime(((DateTime)PrevLeg.ArrivalDTTMLocal).Year, ((DateTime)PrevLeg.ArrivalDTTMLocal).Month, ((DateTime)PrevLeg.ArrivalDTTMLocal).Day);

                                        TimeSpan daydiff = CurrDate.Subtract(PrevDate);
                                        if (daydiff.Days >= 0)
                                            PrevLeg.RemainOverNightCNT = daydiff.Days;
                                        else
                                        {
                                            PrevLeg.RemainOverNightCNT = 0;
                                            CurrentLeg.ShowWarningMessage = false;
                                            RadAjaxManagerMaster.ResponseScripts.Add(@"radconfirm('Warning - A problem within the legs has occurred and Auto RON can not be used until corrected.  Either the Departure Date or Time, Arrival Date or Time or the Ron Count needs to be adjusted.  Check the AutoRON Is Activated/AutoRON Is De-Activated After all corrections have been made.',null, 440, 210,null,'Confirmation!');");
                                        }
                                    }
                                }
                                break;

                        }

                        if (UserPrincipal.Identity._fpSettings._IsAutoRollover != null && (bool)UserPrincipal.Identity._fpSettings._IsAutoRollover)
                            CalculateRemainingRon(LegNUM);
                    }
                }
                SaveQuoteinProgressToFileSession();
            }

        }

        private int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dateval))
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }
                        #endregion

        public bool IsValidNumericFormat(string validString, int? maxLength, int numericValue, int decimalPlaces, string customHeader)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool isValid = true;
                Regex expression = new Regex("^[0-9]{0," + numericValue.ToString() + "}(\\.[0-9]{0," + decimalPlaces.ToString() + "})?$");
                string formatString = string.Empty;

                formatString = formatString.PadLeft(numericValue, '#') + "." + formatString.PadLeft(decimalPlaces, '#');

                if (!string.IsNullOrEmpty(validString))
                {
                    if (!expression.IsMatch(validString))
                    {
                        isValid = false;
                        RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + customHeader + " - Invalid format, Required format is " + formatString + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "' ) ;");
                    }
                    else if (maxLength != null && validString.Length > maxLength)
                    {
                        isValid = false;
                        RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + customHeader + " - Maximum allowed length should be [" + maxLength.ToString() + "]', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "' ) ;");
                    }
                }
                return isValid;
            }
        }

        public bool IsValidDate(string dateString)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool isValid = true;

                if (!string.IsNullOrEmpty(dateString))
                {
                    DateTime mindate = new DateTime(1900, 1, 1);
                    DateTime maxdate = new DateTime(2100, 12, 31);

                    DateTime Departdate = DateTime.ParseExact(dateString, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                    if (Departdate.Year < 1900 || Departdate.Year > 2100)
                    {
                        isValid = false;
                        RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('Please enter/select Date between " + String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", mindate) + " and " + String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + "}", maxdate) + "', 330, 110,'" + ModuleNameConstants.CharterQuote.CQFile + "' ) ;");
                    }
                }
                return isValid;
            }
        }

        private void CQchoice(CQLeg quoteLeg)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(quoteLeg))
            {
                if (quoteLeg.DepartAirportChanged)
                {
                    #region "Depart FBO"
                    if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    {
                        if (quoteLeg.CQFBOLists != null && quoteLeg.CQFBOLists.Count() > 0)
                        {
                            CQFBOList Departfbo = (from fbo in quoteLeg.CQFBOLists.ToList()
                                                   where fbo.IsDeleted == false && fbo.RecordType == "FD"
                                                   select fbo).SingleOrDefault();

                            if (Departfbo != null)
                            {
                                if (Departfbo.CQFBOListID == 0)
                                    quoteLeg.CQFBOLists.Remove(Departfbo);
                                else
                                {

                                    Departfbo.IsDeleted = true;
                                    Departfbo.State = CQRequestEntityState.Deleted;
                                }
                            }
                        }

                        #region "Add Depart FBO"


                        if (quoteLeg.DAirportID != null && quoteLeg.DAirportID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID(Convert.ToInt64(quoteLeg.DAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                CQFBOList Fbo = new CQFBOList();

                                CharterQuoteService.FBO DepFbo = new CharterQuoteService.FBO();

                                DepFbo.FBOID = ObjRetValGroup[0].FBOID;
                                DepFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                Fbo.FBO = DepFbo;
                                Fbo.AirportID = quoteLeg.DAirportID;
                                Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                Fbo.RecordType = "FD";
                                Fbo.LastUpdTS = DateTime.UtcNow;
                                Fbo.IsDeleted = false;
                                Fbo.State = CQRequestEntityState.Added;
                                if (quoteLeg.CQFBOLists == null)
                                    quoteLeg.CQFBOLists = new List<CQFBOList>();
                                quoteLeg.CQFBOLists.Add(Fbo);
                            }
                        }
                        #endregion

                    }
                    #endregion
                    //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                    #region "Maintanence Catering"
                    //if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    //{
                    //    if (quoteLeg.CQCateringLists != null && quoteLeg.CQCateringLists.Count() > 0)
                    //    {
                    //        CQCateringList MaintCatering = (from catering in quoteLeg.CQCateringLists.ToList()
                    //                                        where catering.IsDeleted == false && catering.RecordType == "CM"
                    //                                        select catering).SingleOrDefault();

                    //        if (MaintCatering != null)
                    //        {
                    //            if (MaintCatering.CQCateringListID == 0)
                    //                quoteLeg.CQCateringLists.Remove(MaintCatering);
                    //            else
                    //            {

                    //                MaintCatering.IsDeleted = true;
                    //                MaintCatering.State = CQRequestEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Maintanence Catering"


                    //    if (quoteLeg.DAirportID != null && quoteLeg.DAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetAllCateringByAirportID(Convert.ToInt64(quoteLeg.DAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            CQCateringList catering = new CQCateringList();

                    //            CharterQuoteService.Catering maintCatering = new CharterQuoteService.Catering();

                    //            maintCatering.CateringID = ObjRetValGroup[0].CateringID;
                    //            maintCatering.CateringCD = ObjRetValGroup[0].CateringCD;
                    //            catering.Catering = maintCatering;

                    //            catering.AirportID = quoteLeg.DAirportID;
                    //            catering.CateringID = ObjRetValGroup[0].CateringID;
                    //            if (ObjRetValGroup[0].CateringVendor != null)
                    //                catering.CQCateringListDescription = ObjRetValGroup[0].CateringVendor;
                    //            if (ObjRetValGroup[0].PhoneNum != null)
                    //                catering.PhoneNUM = ObjRetValGroup[0].PhoneNum;
                    //            if (ObjRetValGroup[0].FaxNum != null)
                    //                catering.FaxNUM = ObjRetValGroup[0].FaxNum;
                    //            if (ObjRetValGroup[0].ContactEmail != null)
                    //                catering.Email = ObjRetValGroup[0].ContactEmail;
                    //            if (ObjRetValGroup[0].NegotiatedRate != null)
                    //                catering.Rate = (decimal)ObjRetValGroup[0].NegotiatedRate;
                    //            catering.RecordType = "CM";
                    //            catering.LastUpdTS = DateTime.UtcNow;
                    //            catering.IsDeleted = false;
                    //            catering.State = CQRequestEntityState.Added;
                    //            if (quoteLeg.CQCateringLists == null)
                    //                quoteLeg.CQCateringLists = new List<CQCateringList>();
                    //            quoteLeg.CQCateringLists.Add(catering);
                    //        }
                    //    }
                    //    #endregion

                    //}
                    #endregion

                    quoteLeg.DepartAirportChanged = false;
                }

                if (quoteLeg.ArrivalAirportChanged)
                {
                    #region "Arrive FBO"
                    if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    {
                        if (quoteLeg.CQFBOLists != null && quoteLeg.CQFBOLists.Count() > 0)
                        {
                            CQFBOList Arrivefbo = (from fbo in quoteLeg.CQFBOLists.ToList()
                                                   where fbo.IsDeleted == false && fbo.RecordType == "FA"
                                                   select fbo).SingleOrDefault();

                            if (Arrivefbo != null)
                            {
                                if (Arrivefbo.CQFBOListID == 0)
                                    quoteLeg.CQFBOLists.Remove(Arrivefbo);
                                else
                                {

                                    Arrivefbo.IsDeleted = true;
                                    Arrivefbo.State = CQRequestEntityState.Deleted;
                                }
                            }
                        }

                        #region "Add Arrive FBO"


                        if (quoteLeg.AAirportID != null && quoteLeg.AAirportID > 0)
                        {
                            MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            var ObjRetValGroup = ObjService.GetAllFBOByAirportID(Convert.ToInt64(quoteLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            {
                                CQFBOList Fbo = new CQFBOList();

                                CharterQuoteService.FBO PrefFbo = new CharterQuoteService.FBO();

                                PrefFbo.FBOID = ObjRetValGroup[0].FBOID;
                                PrefFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                Fbo.FBO = PrefFbo;
                                Fbo.AirportID = quoteLeg.AAirportID;
                                Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                Fbo.RecordType = "FA";
                                Fbo.LastUpdTS = DateTime.UtcNow;
                                Fbo.IsDeleted = false;
                                Fbo.State = CQRequestEntityState.Added;
                                if (quoteLeg.CQFBOLists == null)
                                    quoteLeg.CQFBOLists = new List<CQFBOList>();
                                quoteLeg.CQFBOLists.Add(Fbo);
                            }
                        }
                        #endregion

                    }
                    #endregion

                    //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                    #region "Transport"
                    //if (IsAuthorized(Permission.CharterQuote.ViewCQLogistics))
                    //{
                    //    if (quoteLeg.CQTransportLists != null && quoteLeg.CQTransportLists.Count() > 0)
                    //    {
                    //        CQTransportList paxTrans = (from paxtransport in quoteLeg.CQTransportLists.ToList()
                    //                                    where paxtransport.IsDeleted == false && paxtransport.RecordType == "TP"
                    //                                    select paxtransport).SingleOrDefault();

                    //        CQTransportList crewTrans = (from paxtransport in quoteLeg.CQTransportLists.ToList()
                    //                                     where paxtransport.IsDeleted == false && paxtransport.RecordType == "TC"
                    //                                     select paxtransport).SingleOrDefault();

                    //        if (paxTrans != null)
                    //        {
                    //            if (paxTrans.CQTransportListID == 0)
                    //                quoteLeg.CQTransportLists.Remove(paxTrans);
                    //            else
                    //            {
                    //                paxTrans.IsDeleted = true;
                    //                paxTrans.State = CQRequestEntityState.Deleted;
                    //            }
                    //        }

                    //        if (crewTrans != null)
                    //        {
                    //            if (crewTrans.CQTransportListID == 0)
                    //                quoteLeg.CQTransportLists.Remove(crewTrans);
                    //            else
                    //            {
                    //                crewTrans.IsDeleted = true;
                    //                crewTrans.State = CQRequestEntityState.Deleted;
                    //            }
                    //        }
                    //    }

                    //    #region "Add Transport for Pax and Crew"

                    //    if (quoteLeg.AAirportID != null && quoteLeg.AAirportID > 0)
                    //    {
                    //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                    //        var ObjRetValGroup = ObjService.GetTransportByAirportID(Convert.ToInt64(quoteLeg.AAirportID)).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                    //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                    //        {
                    //            #region "Pax"
                    //            CQTransportList PaxTransport = new CQTransportList();

                    //            CharterQuoteService.Transport paxTrans = new CharterQuoteService.Transport();

                    //            paxTrans.TransportID = ObjRetValGroup[0].TransportID;
                    //            paxTrans.TransportCD = ObjRetValGroup[0].TransportCD;
                    //            PaxTransport.Transport = paxTrans;

                    //            PaxTransport.AirportID = quoteLeg.AAirportID;
                    //            PaxTransport.TransportID = ObjRetValGroup[0].TransportID;
                    //            if (ObjRetValGroup[0].TransportationVendor != null)
                    //                PaxTransport.CQTransportListDescription = ObjRetValGroup[0].TransportationVendor;
                    //            if (ObjRetValGroup[0].PhoneNum != null)
                    //                PaxTransport.PhoneNUM = ObjRetValGroup[0].PhoneNum;
                    //            if (ObjRetValGroup[0].FaxNum != null)
                    //                PaxTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                    //            if (ObjRetValGroup[0].ContactEmail != null)
                    //                PaxTransport.Email = ObjRetValGroup[0].ContactEmail;
                    //            if (ObjRetValGroup[0].NegotiatedRate != null)
                    //                PaxTransport.Rate = (decimal)ObjRetValGroup[0].NegotiatedRate;
                    //            PaxTransport.RecordType = "TP";
                    //            PaxTransport.LastUpdTS = DateTime.UtcNow;
                    //            PaxTransport.IsDeleted = false;
                    //            PaxTransport.State = CQRequestEntityState.Added;
                    //            if (quoteLeg.CQTransportLists == null)
                    //                quoteLeg.CQTransportLists = new List<CQTransportList>();
                    //            quoteLeg.CQTransportLists.Add(PaxTransport);
                    //            #endregion

                    //            #region "Crew"
                    //            CQTransportList CrewTransport = new CQTransportList();

                    //            CharterQuoteService.Transport crewTrans = new CharterQuoteService.Transport();

                    //            crewTrans.TransportID = ObjRetValGroup[0].TransportID;
                    //            crewTrans.TransportCD = ObjRetValGroup[0].TransportCD;
                    //            CrewTransport.Transport = crewTrans;

                    //            CrewTransport.AirportID = quoteLeg.AAirportID;
                    //            CrewTransport.TransportID = ObjRetValGroup[0].TransportID;
                    //            if (ObjRetValGroup[0].TransportationVendor != null)
                    //                CrewTransport.CQTransportListDescription = ObjRetValGroup[0].TransportationVendor;
                    //            if (ObjRetValGroup[0].PhoneNum != null)
                    //                CrewTransport.PhoneNUM = ObjRetValGroup[0].PhoneNum;
                    //            if (ObjRetValGroup[0].FaxNum != null)
                    //                CrewTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                    //            if (ObjRetValGroup[0].ContactEmail != null)
                    //                CrewTransport.Email = ObjRetValGroup[0].ContactEmail;
                    //            if (ObjRetValGroup[0].NegotiatedRate != null)
                    //                CrewTransport.Rate = (decimal)ObjRetValGroup[0].NegotiatedRate;
                    //            CrewTransport.RecordType = "TC";
                    //            CrewTransport.LastUpdTS = DateTime.UtcNow;
                    //            CrewTransport.IsDeleted = false;
                    //            CrewTransport.State = CQRequestEntityState.Added;
                    //            if (quoteLeg.CQTransportLists == null)
                    //                quoteLeg.CQTransportLists = new List<CQTransportList>();
                    //            quoteLeg.CQTransportLists.Add(CrewTransport);
                    //            #endregion
                    //        }
                    //    }
                    //    #endregion
                    //}
                    #endregion

                    quoteLeg.ArrivalAirportChanged = false;
                }

                Session[CQSessionKey] = FileRequest;
            }
        }

        public void BindFleetChargeDetails(long fleetID, long aircraftID, long CQCustomerID, bool isCQCustomer)
        {
            // Bind Fleet Charge Details into QuoteInProgress Object, based on Tail Number / Type Code
            FileRequest = (CQFile)Session[CQSessionKey];

            if (FileRequest != null && (FileRequest.CQMains != null && FileRequest.CQMains.Count > 0))
            {
                Int64 QuoteNumInProgress = 1;
                QuoteNumInProgress = FileRequest.QuoteNumInProgress > 0 ? FileRequest.QuoteNumInProgress : 1;
                QuoteInProgress = FileRequest.CQMains.Where(x => x.QuoteNUM != null && x.QuoteNUM == QuoteNumInProgress && x.IsDeleted == false).FirstOrDefault();

                if (QuoteInProgress != null)
                {
                    List<FlightPakMasterService.GetAllFleetNewCharterRate> FleetChargeRateList = new List<FlightPakMasterService.GetAllFleetNewCharterRate>();

                    if (QuoteInProgress.CQFleetChargeDetails != null)
                    {
                        // Delete the Existing Fleet Charge Details
                        List<CQFleetChargeDetail> ChargeToDeleteList = new List<CQFleetChargeDetail>();
                        ChargeToDeleteList = QuoteInProgress.CQFleetChargeDetails.Where(x => x.IsDeleted == false).ToList();

                        if (ChargeToDeleteList != null && ChargeToDeleteList.Count > 0)
                        {
                            foreach (CQFleetChargeDetail fleetCharge in ChargeToDeleteList)
                            {
                                if (fleetCharge.CQFleetChargeDetailID != 0)
                                {
                                    fleetCharge.IsDeleted = true;
                                    fleetCharge.State = CQRequestEntityState.Deleted;
                                }
                                else
                                    QuoteInProgress.CQFleetChargeDetails.Remove(fleetCharge);
                            }
                        }
                    }

                    using (FlightPakMasterService.MasterCatalogServiceClient Service = new FlightPakMasterService.MasterCatalogServiceClient())
                    {
                        // Get New Charter Rates from Fleet Profile Catalog, based on CQCustomerID 
                        if (isCQCustomer == true && (CQCustomerID != null && CQCustomerID != 0))
                        {
                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.CQCustomerID != null && x.CQCustomerID == CQCustomerID).ToList();
                            //Get Quote details from CQCustomer
                            using (FlightPakMasterService.MasterCatalogServiceClient objMastersvc = new FlightPakMasterService.MasterCatalogServiceClient())
                            {
                                FlightPakMasterService.CQCustomer oCQCustomer = new FlightPakMasterService.CQCustomer();

                                var objCQCustomer = objMastersvc.GetCQCustomerList();
                                if (objCQCustomer != null && objCQCustomer.EntityList != null && objCQCustomer.EntityList.Count > 0)
                                {
                                    oCQCustomer = objCQCustomer.EntityList.Where(c => c.CQCustomerID == CQCustomerID).SingleOrDefault();
                                    if (oCQCustomer != null)
                                    {
                                        QuoteInProgress.StandardCrewDOM = oCQCustomer.DomesticStdCrewNum;
                                        QuoteInProgress.StandardCrewINTL = oCQCustomer.IntlStdCrewNum;
                                        QuoteInProgress.IsLandingFeeTax = oCQCustomer.LandingFeeTax;
                                        QuoteInProgress.IsDailyTaxAdj = oCQCustomer.DailyUsageAdjTax;
                                    }
                                }
                            }

                            using (MasterCatalogServiceClient objDstsvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList().EntityList.Where(x => x.FleetID == fleetID && x.IsDeleted == false);
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    Fleetlist = objRetVal.ToList();
                                    if (Fleetlist != null && Fleetlist.ToList().Count > 0)
                                    {
                                        if (Fleetlist[0].TailNum != null)
                                        {
                                            QuoteInProgress.StandardCrewDOM = Fleetlist[0].StandardCrewDOM;
                                            QuoteInProgress.StandardCrewINTL = Fleetlist[0].StandardCrewIntl;
                                            QuoteInProgress.IsLandingFeeTax = Fleetlist[0].IsTaxLandingFee;
                                            QuoteInProgress.IsDailyTaxAdj = Fleetlist[0].IsTaxDailyAdj;
                                        }
                                    }
                                }
                            }
                        }
                        // Get New Charter Rates from Fleet Profile Catalog
                        else if (fleetID != null && fleetID != 0)
                        {
                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID != null && x.FleetID == fleetID && x.AircraftTypeID == null).ToList();
                            //Get Quote details from Fleet
                            FlightPak.Web.FlightPakMasterService.Fleet fleet = new FlightPakMasterService.Fleet();

                            using (MasterCatalogServiceClient objDstsvc = new MasterCatalogServiceClient())
                            {
                                List<FlightPak.Web.FlightPakMasterService.Fleet> Fleetlist = new List<FlightPakMasterService.Fleet>();
                                var objRetVal = objDstsvc.GetFleetProfileList().EntityList.Where(x => x.FleetID == fleetID && x.IsDeleted == false);
                                if (objRetVal != null && objRetVal.Count() > 0)
                                {
                                    Fleetlist = objRetVal.ToList();
                                    if (Fleetlist != null)
                                    {
                                        if (Fleetlist[0].TailNum != null)
                                        {
                                            QuoteInProgress.StandardCrewDOM = Fleetlist[0].StandardCrewDOM;
                                            QuoteInProgress.StandardCrewINTL = Fleetlist[0].StandardCrewIntl;
                                            QuoteInProgress.IsLandingFeeTax = Fleetlist[0].IsTaxLandingFee;
                                            QuoteInProgress.IsDailyTaxAdj = Fleetlist[0].IsTaxDailyAdj;
                                        }
                                    }
                                }
                            }
                        }
                        // Get New Charter Rates from Aircraft Type Catalog, if the Source is selected as Type and Tail Number is Null/Empty
                        else if (aircraftID != null && aircraftID != 0)
                        {
                            FleetChargeRateList = Service.GetAllFleetNewCharterRate().EntityList.Where(x => x.FleetID == null && (x.AircraftTypeID != null && x.AircraftTypeID == aircraftID)).ToList();
                            //Get Quote details from Aircraft
                            FlightPak.Web.FlightPakMasterService.Aircraft aircraft = new FlightPakMasterService.Aircraft();
                            aircraft = GetAircraft(aircraftID);
                            if (aircraft != null)
                            {
                                QuoteInProgress.StandardCrewDOM = aircraft.DomesticStdCrewNum;
                                QuoteInProgress.StandardCrewINTL = aircraft.IntlStdCrewNum;
                                QuoteInProgress.IsLandingFeeTax = aircraft.LandingFeeTax;
                                QuoteInProgress.IsDailyTaxAdj = aircraft.DailyUsageAdjTax;
                            }
                        }

                        if (FleetChargeRateList != null && FleetChargeRateList.Count > 0)
                        {
                            if (QuoteInProgress.CQFleetChargeDetails == null)
                                QuoteInProgress.CQFleetChargeDetails = new List<CQFleetChargeDetail>();

                            foreach (FlightPakMasterService.GetAllFleetNewCharterRate chargeRate in FleetChargeRateList)
                            {
                                CQFleetChargeDetail fleetCharge = new CQFleetChargeDetail();
                                fleetCharge.State = CQRequestEntityState.Added;
                                fleetCharge.CQFlightChargeDescription = chargeRate.FleetNewCharterRateDescription;
                                fleetCharge.ChargeUnit = chargeRate.ChargeUnit;
                                fleetCharge.NegotiatedChgUnit = chargeRate.NegotiatedChgUnit;
                                fleetCharge.Quantity = 0;
                                fleetCharge.BuyDOM = chargeRate.BuyDOM;
                                fleetCharge.SellDOM = chargeRate.SellDOM;
                                fleetCharge.IsTaxDOM = chargeRate.IsTaxDOM;
                                fleetCharge.IsDiscountDOM = chargeRate.IsDiscountDOM;
                                fleetCharge.BuyIntl = chargeRate.BuyIntl;
                                fleetCharge.SellIntl = chargeRate.SellIntl;
                                fleetCharge.IsTaxIntl = chargeRate.IsTaxIntl;
                                fleetCharge.IsDiscountIntl = chargeRate.IsDiscountIntl;
                                fleetCharge.FeeAMT = 0;
                                fleetCharge.OrderNUM = chargeRate.OrderNum;
                                fleetCharge.IsDeleted = false;

                                QuoteInProgress.CQFleetChargeDetails.Add(fleetCharge);
                            }
                            CalculateQuantity();
                            CalculateQuoteTotal();
                            SaveQuoteinProgressToFileSession();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Function to adjust the time textbox values
        /// </summary>
        /// <param name="TextboxValues"></param>
        /// <returns></returns>
        public string CalculationTimeAdjustment(string TextboxValues)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TextboxValues))
            {
                string TextboxValue = "00:00";
                if (TextboxValues != "")
                {
                    TextboxValue = TextboxValues;
                    if ((!TextboxValue.Contains('-')) && (TextboxValue.Trim() != "00:00") && (TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != ".") && (TextboxValue.Trim() != "0") && (TextboxValue.Trim() != "0:0") && (TextboxValue.Trim() != "0:00"))
                    {
                        if (TextboxValue.IndexOf(":") != -1)
                        {
                            string[] timeArray = TextboxValue.Split(':');
                            if (string.IsNullOrEmpty(timeArray[0]))
                            {
                                timeArray[0] = "00";
                                TextboxValue = timeArray[0] + ":" + timeArray[1];
                            }
                            if (!string.IsNullOrEmpty(timeArray[0]))
                            {
                                if (timeArray[0].Length == 0)
                                {
                                    timeArray[0] = "00";
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                                if (timeArray[0].Length == 1)
                                {
                                    timeArray[0] = "0" + timeArray[0];
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                            }
                            if (!string.IsNullOrEmpty(timeArray[1]))
                            {
                                Int64 result = 0;
                                if (timeArray[1].Length != 2)
                                {
                                    result = (Convert.ToInt32(timeArray[1])) / 10;
                                    if (result < 1)
                                    {
                                        if (timeArray[0] != null)
                                        {
                                            if (timeArray[0].Trim() == "")
                                            {
                                                timeArray[0] = "00";
                                            }
                                        }
                                        else
                                        {
                                            timeArray[0] = "00";
                                        }
                                        TextboxValue = timeArray[0] + ":" + "0" + timeArray[1];

                                    }
                                }
                            }
                            else
                            {
                                TextboxValue = TextboxValue + "00";
                            }
                        }
                        else
                        {
                            TextboxValue = TextboxValue + ":00";
                        }
                    }
                    else
                    {
                        TextboxValue = "00:00";
                    }
                }
                else
                {
                    TextboxValue = "00:00";
                }
                return TextboxValue;
            }
        }
    }
}
