﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
//using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PreflightService;
using FlightPak.Web.CalculationService;
using System.Globalization;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Web.Views.Transactions.PostFlight;
using FlightPak.Common.Constants;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.FlightPakMasterService;

namespace FlightPak.Web.Framework.Masters
{
    public delegate void AjaxRequestHandler(object sender, AjaxRequestEventArgs e);
    //Replicate Save Cancel Delete Buttons in header
    public delegate void ButtonClick(object sender, EventArgs e);

    public partial class PreflightLegacy : BaseSecuredMasterPage
    {
        private ExceptionManager exManager;

        private delegate void SaveSession();
        private delegate void SavePAXCrewSession();
        private delegate void LoadSelectedTrip();
        private delegate void TripNew();
        private delegate void TripEdit();
        private delegate void TripLog();

        //Replicate Save Cancel Delete Buttons in header
        public event ButtonClick SaveClick;
        public event ButtonClick DeleteClick;
        public event ButtonClick CancelClick;

        #region Comments
        //Defect:  2955  The system shall have the ability to display the aircraft description next to the Tail Number in the header area
        //Fixed By Prabhu. 06/02/2014.

        #endregion
        private Delegate _SaveToSession;
        public Delegate SaveToSession
        {
            set { _SaveToSession = value; }
        }


        private Delegate _SaveToSessionPAXCrew;
        public Delegate SaveToSessionPAXCrew
        {
            set { _SaveToSessionPAXCrew = value; }
        }


        public event AjaxRequestHandler RadAjax_AjaxRequest;

        string _alertmessage = "";

        public string Alertmessage
        {
            get { return this._alertmessage; }
            set { this._alertmessage = value; }
        }

        public string RevisionDescription
        {
            get { return this.PreflightSearch.RevisionDescription; }
            set { this.PreflightSearch.RevisionDescription = value; }
        }

        #region Changes for Legnum in PreflightHeader
        public Label FloatLegNum
        {
            get { return this.lblFloatLegNum; }

        }
        #endregion

        public RadDatePicker DatePicker { get { return this.RadDatePicker1; } }

        public System.Web.UI.HtmlControls.HtmlGenericControl MasterForm { get { return this.DivMasterForm; } }

        public RadAjaxManager RadAjaxManagerMaster { get { return this.RadAjaxManager1; } }

        public PreflightMain Trip = new PreflightMain();




        #region Bindexception Method

        List<PreflightService.RulesException> allErrorList = new List<PreflightService.RulesException>();
        List<PreflightService.RulesException> mainErrorList = new List<PreflightService.RulesException>();
        List<PreflightService.RulesException> legErrorList = new List<PreflightService.RulesException>();

        List<PreflightTripException> ExceptionList = new List<PreflightTripException>();
        #endregion

        List<FlightPak.Web.PreflightService.RulesException> ErrorList = new List<FlightPak.Web.PreflightService.RulesException>();

        protected void Page_Init(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Session["TabSelect"] = "PreFlight";
                        //Master.SaveSession += new CommandEventHandler(_SavePreflightToSession);
                        string dateFormate = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        RadDatePicker1.DateInput.DateFormat = dateFormate; //PrefHomebaseSettings.ApplicationDateFormat;
                        RadDatePicker1.DateInput.DisplayDateFormat = dateFormate;
                        DatePicker.DateInput.DateFormat = dateFormate;
                        DatePicker.DateInput.DisplayDateFormat = dateFormate;


                        if (UserPrincipal.Identity._fpSettings._ApplicationDateFormat != null)
                        {
                            RadDatePicker1.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            RadDatePicker1.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                            DatePicker.DateInput.DisplayDateFormat = UserPrincipal.Identity._fpSettings._ApplicationDateFormat;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
        protected void page_prerender(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    lbFloatstatus.Visible = false;
                    lbFloatTailnum.Visible = false;
                    //Defect:  2955
                    lbFloatAircraft.Visible = false;
                    lbFloatTripnum.Visible = false;
                    lbFloatLognum.Visible = false;

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Request.QueryString["seltab"] != null)
                        {
                            string selectedItemValue = Request.QueryString["seltab"];
                            if (selectedItemValue == "Reports" || selectedItemValue == "TRW")
                            {
                                pnlbarPreflightInfo.Visible = false;
                                RadPanelBar1.Visible = false;
                                pnlException.Visible = false;
                                PreflightSearch.Visible = false;
                            }
                        }
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            if (Trip != null)
                            {
                                if (Trip.TripID != 0)
                                {
                                    lbFloatTripnum.Visible = true;
                                    lbFloatTripnum.Text = System.Web.HttpUtility.HtmlEncode(Trip.TripNUM.ToString());
                                }
                                else
                                    lbFloatTripnum.Visible = false;

                                if (Trip.Fleet != null)
                                {
                                    lbFloatTailnum.Visible = true;
                                    lbFloatTailnum.Text = System.Web.HttpUtility.HtmlEncode(Trip.Fleet.TailNum);

                                }
                                else
                                    lbFloatTailnum.Visible = false;
                                //Defect:  2955
                                if (Trip.Aircraft != null)
                                {
                                    lbFloatAircraft.Visible = true;
                                    lbFloatAircraft.Text = System.Web.HttpUtility.HtmlEncode(Trip.Aircraft.AircraftCD);

                                }
                                else
                                    lbFloatAircraft.Visible = false;



                                if (Trip.IsLog != null && (bool)Trip.IsLog && Trip.TripID != 0)
                                {

                                    using (PreflightServiceClient SVC = new PreflightServiceClient())
                                    {
                                        var returnVal = SVC.GetLogByTripID((long)Trip.TripID);

                                        if (returnVal.ReturnFlag)
                                        {
                                            if (returnVal.EntityList.Count > 0)
                                            {
                                                lbFloatLognum.Visible = true;
                                                lbFloatLognum.Text = System.Web.HttpUtility.HtmlEncode(returnVal.EntityList[0].LogNum != null ? returnVal.EntityList[0].LogNum.ToString() : string.Empty); ;
                                            }

                                        }
                                    }

                                }
                                else
                                    lbFloatLognum.Visible = false;

                                if (!string.IsNullOrEmpty(Trip.TripStatus))
                                {
                                    string StatusStr = string.Empty;
                                    switch (Trip.TripStatus)
                                    {
                                        case "W": StatusStr = "W-Worksheet"; break;
                                        case "T": StatusStr = "T-Tripsheet"; break;
                                        case "U": StatusStr = "U-Unfulfilled"; break;
                                        case "X": StatusStr = "X-Cancelled"; break;
                                        case "H": StatusStr = "H-Hold"; break;
                                    }

                                    lbFloatstatus.Text = StatusStr;
                                    lbFloatstatus.Visible = true;
                                }
                                else
                                    lbFloatstatus.Visible = false;

                            }

                            if (!Page.IsPostBack)
                            {

                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (Trip != null)
                                {


                                    if (Trip.Mode == TripActionMode.Edit)
                                    {

                                        if (this.PreflightHeader.HeaderTab.SelectedTab.Text != "UWA Services")
                                        {
                                            for (int i = 0; i <= 4; i++)
                                                this.PreflightHeader.HeaderTab.Tabs[i].Enabled = true;
                                            this.PreflightHeader.HeaderTab.Tabs[5].Enabled = false;
                                        }
                                        else
                                        {
                                            for (int i = 0; i <= 4; i++)
                                                this.PreflightHeader.HeaderTab.Tabs[i].Enabled = false;
                                            this.PreflightHeader.HeaderTab.Tabs[5].Enabled = true;

                                        }

                                        //ValidationTrip(PreflightService.Group.Main);

                                        if (Trip.State != TripEntityState.Added)
                                        {
                                            using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                            {
                                                if (Trip.TripID != 0)
                                                {
                                                    var returnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Trip.TripID);
                                                    if (!returnValue.ReturnFlag)
                                                    {
                                                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                                                        //RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                                                    }
                                                    else
                                                    {
                                                        dgLegs.Rebind();
                                                        dgLegSummary.Rebind();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            dgLegs.Rebind();
                                            dgLegSummary.Rebind();
                                        }
                                    }
                                }


                            }
                        }

                        RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("UWA Services");
                        if (selectedTab != null)
                        {
                            selectedTab.Visible = false;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        #region Changes for Legnum in PreflightHeader
                        lblFloatLegNum.Visible = false;
                        //lblFloatLegNum.Text = System.Web.HttpUtility.HtmlEncode(string.Empty);
                        #endregion
                        SaveSession SavePreflightSession = new SaveSession(SaveToPreflightSession);
                        this.PreflightHeader.SaveToSession = SavePreflightSession;

                        SavePAXCrewSession SaveToSessionPAXCrew = new SavePAXCrewSession(SaveToPreflightSessionPAXCrew);
                        this.PreflightHeader.SaveToSessionPAXCrew = SaveToSessionPAXCrew;
                        LoadSelectedTrip LoadTriptoSession = new LoadSelectedTrip(_LoadTriptoSession);
                        this.PreflightSearch.SelectTrip = LoadTriptoSession;

                        //Replicate Save Cancel Delete Buttons in header
                        this.PreflightHeader.SaveClick += btnSave_Click;
                        this.PreflightHeader.CancelClick += btnCancel_Click;
                        this.PreflightHeader.DeleteClick += btnDelete_Click;

                        dgLegSummary.DataSource = string.Empty;
                        dgLegs.DataSource = string.Empty;
                        MasterDateFormat.Value = RadDatePicker1.DateInput.DateFormat;
                        MasterTimeDisplayTenMin.Value = UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == null ? "1" : UserPrincipal.Identity._fpSettings._TimeDisplayTenMin.ToString();
                        MasterElapseTM.Value = UserPrincipal.Identity._fpSettings._ElapseTMRounding == null ? "0" : Convert.ToString(UserPrincipal.Identity._fpSettings._ElapseTMRounding);
                        if (!IsPostBack)
                        {
                            #region Authorizatioin
                            CheckAutorization(Permission.Preflight.ViewTripManager);

                            ((GridBoundColumn)dgLegs.Columns[1]).DataFormatString = "{0:MM/dd/yyyy}";

                            if (!IsAuthorized(Permission.Preflight.AddTripManager))
                            {
                                this.PreflightHeader.NewTripButton.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.DeleteTripManager))
                            {
                                this.PreflightHeader.DeleteTripButton.Visible = false;
                            }


                            if (Session["CurrentPreFlightTrip"] != null)
                            {
                                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                                //if (Trip.isTripSaved)
                                //{
                                //    RadWindowManager1.RadAlert("Trip has been saved successfully", 330, 100, "Trip Alert", null);
                                //    Trip.isTripSaved = false;
                                //    Session["CurrentPreFlightTrip"] = Trip;
                                //}

                                if (Trip.State == TripEntityState.Added)
                                {
                                    if (!IsAuthorized(Permission.Preflight.AddPreFlightMain))
                                    {
                                        if (!IsAuthorized(Permission.Preflight.AddPreFlightMain))
                                        {
                                            this.PreflightHeader.SaveTripButton.Visible = false;
                                            this.PreflightHeader.CancelTripButton.Visible = false;
                                        }
                                    }
                                    else if (Trip.State == TripEntityState.Modified)
                                    {
                                        if (!IsAuthorized(Permission.Preflight.EditPreFlightMain))
                                        {
                                            this.PreflightHeader.SaveTripButton.Visible = false;
                                            this.PreflightHeader.CancelTripButton.Visible = false;
                                        }
                                    }

                                }
                            }

                            if (!IsAuthorized(Permission.Preflight.EditTripManager))
                            {
                                this.PreflightHeader.EditTripButton.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightAPIS))
                            {
                                this.PreflightHeader.APISTripButton.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.AddTripLog))
                            {
                                this.PreflightHeader.LogButton.Visible = false;
                            }
                            if (!IsAuthorized(Permission.Preflight.AddTripSIFL))
                            {
                                this.PreflightHeader.SIFLTripButton.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreFlightMain))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("PreFlight");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightLeg))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("Legs");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightCrew))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("Crew");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightPassenger))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("PAX");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightFuel) && !IsAuthorized(Permission.Preflight.ViewPreflightFBO) && !IsAuthorized(Permission.Preflight.ViewPreflightCatering))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("Logistics");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightUWA))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("UWA Services");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightReports))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("Reports");
                                selectedTab.Visible = false;
                            }

                            if (!IsAuthorized(Permission.Preflight.ViewPreflightTripsheetReportWriter))
                            {
                                RadTab selectedTab = this.PreflightHeader.HeaderTab.Tabs.FindTabByText("Tripsheet Report Writer");
                                selectedTab.Visible = false;
                            }
                            if (!UserPrincipal.Identity._fpSettings._IsAPISSupport)
                            {
                                this.PreflightHeader.APISTripButton.Visible = false;
                            }

                            #endregion Authorizatioin
                        }

                        TripEdit tripEdit = new TripEdit(EditTripEvent);
                        //this.PreflightSearch.EditClick = tripEdit;
                        this.PreflightHeader.EditClick = tripEdit;


                        TripNew tripNew = new TripNew(NewTripEvent);
                        this.PreflightHeader.NewClick = tripNew;

                        TripLog tripLog = new TripLog(LogTripEvent);
                        this.PreflightHeader.LogClick = tripLog;

                        SetTripMode();
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                            {
                                PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                                if (Trip.PreflightLegs!=null && Trip.PreflightLegs.Count > 0)
                                    ExpandSummaryPanel(true);
                                else
                                    ExpandSummaryPanel(false);
                                if (Trip.TripID != 0)
                                {
                                    var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                    if (ObjretVal.ReturnFlag)
                                    {
                                        Session["PreflightException"] = ObjretVal.EntityList;
                                        ValidationTrip(PreflightService.Group.Main);
                                        BindException();
                                    }
                                    else
                                        Session["PreflightException"] = null;


                                }

                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
            ExpandErrorPanel(true);
        }

       

        //Replicate Save Cancel Delete Buttons in header
        protected void btnSave_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (SaveClick != null)
                {
                    SaveClick(sender, e);
                }
            }

        }
        //Replicate Save Cancel Delete Buttons in header
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelClick != null)
            {
                CancelClick(sender, e);
            }
        }
        //Replicate Save Cancel Delete Buttons in header
        protected void btnDelete_Click(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (DeleteClick != null)
                {
                    DeleteClick(sender, e);
                }
            }
        }


        private FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters GetCompany(Int64 HomeBaseID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetCompanyWithFilters Companymaster = new GetCompanyWithFilters();
                    var objCompany = objDstsvc.GetCompanyWithFilters(string.Empty, HomeBaseID, true, true);

                    if (objCompany.ReturnFlag)
                    {
                        List<FlightPak.Web.FlightPakMasterService.GetCompanyWithFilters> Company = objCompany.EntityList;
                        //(from Comp in objCompany.EntityList where Comp.HomebaseID == HomeBaseID select Comp).ToList();
                        if (Company != null && Company.Count > 0)
                            Companymaster = Company[0];

                        else
                        {
                            Companymaster = null;
                        }
                    }
                    return Companymaster;
                }
            }

        }
        public FlightPak.Web.FlightPakMasterService.GetAllAirport GetAirport(Int64 DepartIcaoID)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartIcaoID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    GetAllAirport Airportmaster = new GetAllAirport();
                    var objAirport = objDstsvc.GetAirportByAirportID(DepartIcaoID);
                    if (objAirport.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = (from Arpt in objAirport.EntityList
                        //                                                                       where Arpt.IcaoID == DepartIcaoID
                        //                                                                       select Arpt).ToList();

                        List<FlightPak.Web.FlightPakMasterService.GetAllAirport> DepAirport = objAirport.EntityList;

                        if (DepAirport != null && DepAirport.Count > 0)
                            Airportmaster = DepAirport[0];
                        else
                            Airportmaster = null;
                    }
                    return Airportmaster;

                }
            }
        }
        protected void SetTripMode()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    this.PreflightSearch.TripID = Trip.TripID;
                    if (Trip.TripNUM != null)
                        this.PreflightSearch.TripNUM = (long)Trip.TripNUM;

                    if ((bool)UserPrincipal.Identity._fpSettings._IsAutoRevisionNum)
                    {
                        using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                        {
                            if (Trip.TripID > 0)
                            {
                                long RevisionNum = 0;
                                RevisionNum = objService.GetRevisionNum(Trip.TripID);
                                Trip.RevisionNUM = RevisionNum;
                            }
                        }
                    }
                    this.PreflightSearch.RevisionNumber = Trip.RevisionNUM;
                    //this.PreflightSearch.RevisionNumber = Trip.RevisionNUM;
                    if (Trip.Fleet != null)
                    {
                        if (Trip.Fleet.TailNum != null)
                        {
                            this.PreflightSearch.TailNo = Trip.Fleet.TailNum;
                        }
                    }
                    if (Trip.Aircraft != null)
                    {
                        if (Trip.Aircraft.AircraftCD != null)
                        {
                            this.PreflightSearch.TypeCode = Trip.Aircraft.AircraftCD;
                        }
                    }
                    if (Trip.LastUpdTS != DateTime.MinValue && Trip.LastUpdTS != null)
                    {
                        DateTime LastModifiedUTCDate = DateTime.Now;
                        if (UserPrincipal.Identity._airportId != null)
                        {
                            using (CalculationServiceClient CalcSvc = new CalculationServiceClient())
                            {
                                LastModifiedUTCDate = CalcSvc.GetGMT(UserPrincipal.Identity._airportId, (DateTime)Trip.LastUpdTS, false, false);
                            }
                        }
                        else
                            LastModifiedUTCDate = (DateTime)Trip.LastUpdTS;
                        this.PreflightSearch.LastModified.Text = System.Web.HttpUtility.HtmlEncode("Last Modified: " + String.Format(CultureInfo.InvariantCulture, "{1} {0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", LastModifiedUTCDate, Trip.LastUpdUID));
                    }
                    else
                        this.PreflightSearch.LastModified.Text = string.Empty;

                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        // this.PreflightSearch.EditButton.Enabled = false;
                        this.PreflightHeader.EditTripButton.Enabled = false;
                        this.PreflightHeader.NewTripButton.Enabled = false;
                        this.PreflightHeader.MoveTripButton.Enabled = false;
                        this.PreflightHeader.LogButton.Enabled = false;
                        this.PreflightHeader.EmailButton.Enabled = false; // PROD-38
                        this.PreflightHeader.TravelsenseButton.Enabled = false;
                        this.PreflightHeader.CopyTripButton.Enabled = false;
                        this.PreflightHeader.APISTripButton.Enabled = false;
                        this.PreflightHeader.SIFLTripButton.Enabled = false;
                        //Replicate Save Cancel Delete Buttons in header
                        this.PreflightHeader.SaveTripButton.Enabled = true;
                        this.PreflightHeader.CancelTripButton.Enabled = true;
                        this.PreflightHeader.DeleteTripButton.Enabled = false;

                        //Replicate Save Cancel Delete Buttons in header
                        this.PreflightHeader.SaveTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.CancelTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.DeleteTripButton.CssClass = "ui_nav_disable";



                        // this.PreflightSearch.EditButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.EditTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.NewTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.MoveTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.LogButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.EmailButton.CssClass = "ui_nav_disable"; // PROD-38
                        this.PreflightHeader.TravelsenseButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.CopyTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.APISTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.SIFLTripButton.CssClass = "ui_nav_disable";
                        if (Trip.TripID == 0)
                            this.PreflightHeader.HeaderTab.Tabs[5].Enabled = false;

                    }
                    else
                    {

                        //this.PreflightSearch.EditButton.Enabled = true;
                        this.PreflightHeader.NewTripButton.Enabled = true;
                        this.PreflightHeader.EditTripButton.Enabled = true;
                        this.PreflightHeader.MoveTripButton.Enabled = true;
                        this.PreflightHeader.LogButton.Enabled = true;
                        this.PreflightHeader.EmailButton.Enabled = true; // PROD-38
                        this.PreflightHeader.TravelsenseButton.Enabled = true;
                        this.PreflightHeader.CopyTripButton.Enabled = true;
                        this.PreflightHeader.APISTripButton.Enabled = true;
                        this.PreflightHeader.SIFLTripButton.Enabled = true;

                        //Replicate Save Cancel Delete Buttons in header
                        this.PreflightHeader.SaveTripButton.Enabled = false;
                        this.PreflightHeader.CancelTripButton.Enabled = false;
                        this.PreflightHeader.DeleteTripButton.Enabled = true;

                        //Replicate Save Cancel Delete Buttons in header
                        this.PreflightHeader.SaveTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.CancelTripButton.CssClass = "ui_nav_disable";
                        this.PreflightHeader.DeleteTripButton.CssClass = "ui_nav";


                        //this.PreflightSearch.EditButton.CssClass = "ui_nav";
                        this.PreflightHeader.EditTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.NewTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.MoveTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.LogButton.CssClass = "ui_nav";
                        this.PreflightHeader.EmailButton.CssClass = "ui_nav"; // PROD-38
                        this.PreflightHeader.TravelsenseButton.CssClass = "ui_nav";
                        this.PreflightHeader.CopyTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.APISTripButton.CssClass = "ui_nav";
                        this.PreflightHeader.SIFLTripButton.CssClass = "ui_nav";


                    }

                }
                else
                {
                    this.PreflightHeader.NewTripButton.Enabled = true;
                    this.PreflightHeader.APISTripButton.Enabled = true;
                    this.PreflightHeader.EditTripButton.Enabled = false;
                    //this.PreflightSearch.EditButton.Enabled = false;
                    this.PreflightHeader.MoveTripButton.Enabled = false;
                    this.PreflightHeader.LogButton.Enabled = false;
                    this.PreflightHeader.EmailButton.Enabled = false; // PROD-38
                    this.PreflightHeader.TravelsenseButton.Enabled = false;
                    this.PreflightHeader.CopyTripButton.Enabled = false;

                    this.PreflightHeader.SIFLTripButton.Enabled = false;

                    //Replicate Save Cancel Delete Buttons in header
                    this.PreflightHeader.SaveTripButton.Enabled = false;
                    this.PreflightHeader.CancelTripButton.Enabled = false;
                    this.PreflightHeader.DeleteTripButton.Enabled = false;

                    this.PreflightHeader.SaveTripButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.CancelTripButton.CssClass = "ui_nav_disable";

                    this.PreflightHeader.DeleteTripButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.NewTripButton.CssClass = "ui_nav";
                    this.PreflightHeader.APISTripButton.CssClass = "ui_nav";
                    //this.PreflightSearch.EditButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.EditTripButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.MoveTripButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.LogButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.EmailButton.CssClass = "ui_nav_disable"; // PROD-38
                    this.PreflightHeader.TravelsenseButton.CssClass = "ui_nav_disable";
                    this.PreflightHeader.CopyTripButton.CssClass = "ui_nav_disable";

                    this.PreflightHeader.SIFLTripButton.CssClass = "ui_nav_disable";
                }

                //if (UserPrincipal.Identity._airportId != null)
                //{
                //    using (PreflightService.PreflightServiceClient objService = new PreflightService.PreflightServiceClient())
                //    {
                //        var objRetVal = objService.GetAirportbyAirportIDs(UserPrincipal.Identity._airportId);
                //        if (objRetVal.ReturnFlag == true)
                //        {
                //            if (objRetVal.EntityList.Count > 0)
                //            {
                //                if (objRetVal.EntityList[0].CountryName != "UNITED STATES")
                //                {
                //                    //this.PreflightHeader.SIFLTripButton.Visible = false;
                //                }
                //            }
                //        }
                //    }
                //}
            }
        }

        protected void LoadHomebase(PreflightMain Trip)
        {
            if (Trip.TripID == 0)
            {
                if (UserPrincipal.Identity._homeBaseId != null)
                {

                    if (UserPrincipal.Identity._homeBaseId != null)
                    {
                        Int64 HomebaseID = (long)UserPrincipal.Identity._homeBaseId;

                        PreflightService.Company HomebaseSample = new PreflightService.Company();
                        HomebaseSample.HomebaseID = HomebaseID;

                        using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                        {
                            var objRetVal = objDstsvc.GetCompanyWithFilters(string.Empty, HomebaseID, true, true);
                            List<FlightPakMasterService.GetCompanyWithFilters> CompanyList = new List<FlightPakMasterService.GetCompanyWithFilters>();

                            if (objRetVal.ReturnFlag)
                            {

                                CompanyList = objRetVal.EntityList.Where(x => x.HomebaseID == HomebaseID).ToList();
                                if (CompanyList != null && CompanyList.Count > 0)
                                {
                                    HomebaseSample.HomebaseAirportID = CompanyList[0].HomebaseAirportID;
                                }
                            }

                            List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();

                            var fleetretval = objDstsvc.GetFleet();
                            if (fleetretval.ReturnFlag)
                            {
                                GetFleetList = fleetretval.EntityList.Where(x => x.HomebaseID == HomebaseID && x.IsInActive == false).ToList();
                                if (GetFleetList != null && GetFleetList.Count == 1)
                                {
                                    PreflightService.Fleet fleetsample = new PreflightService.Fleet();
                                    fleetsample.FleetID = GetFleetList[0].FleetID;
                                    fleetsample.TailNum = GetFleetList[0].TailNum;
                                    fleetsample.MaximumPassenger = GetFleetList[0].MaximumPassenger != null ? GetFleetList[0].MaximumPassenger : 0;

                                    Trip.FleetID = GetFleetList[0].FleetID;
                                    Trip.Fleet = fleetsample;


                                    if (GetFleetList[0].AircraftID != null)
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.GetAllAircraft> AircraftList = new List<FlightPakMasterService.GetAllAircraft>();


                                        var objRetVal1 = objDstsvc.GetAircraftList();
                                        if (objRetVal1.ReturnFlag)
                                        {
                                            AircraftList = objRetVal1.EntityList.Where(x => x.AircraftID == (long)GetFleetList[0].AircraftID).ToList();

                                            if (AircraftList != null && AircraftList.Count > 0)
                                            {

                                                PreflightService.Aircraft aircraftSample = new PreflightService.Aircraft();

                                                aircraftSample.AircraftID = AircraftList[0].AircraftID;
                                                aircraftSample.AircraftCD = AircraftList[0].AircraftCD;
                                                aircraftSample.AircraftDescription = AircraftList[0].AircraftDescription;
                                                Trip.Aircraft = aircraftSample;
                                                Trip.AircraftID = AircraftList[0].AircraftID;
                                            }
                                        }
                                    }

                                    if (GetFleetList[0].EmergencyContactID != null)
                                    {
                                        List<FlightPak.Web.FlightPakMasterService.EmergencyContact> EmergencyContactList = new List<FlightPakMasterService.EmergencyContact>();
                                        var objRetValEmerg = objDstsvc.GetEmergencyList();
                                        if (objRetValEmerg.ReturnFlag)
                                        {
                                            EmergencyContactList = objRetValEmerg.EntityList.Where(x => x.EmergencyContactID == (long)GetFleetList[0].EmergencyContactID).ToList();
                                            if (EmergencyContactList != null && EmergencyContactList.Count > 0)
                                            {
                                                PreflightService.EmergencyContact EC = new PreflightService.EmergencyContact();

                                                EC.EmergencyContactID = EmergencyContactList[0].EmergencyContactID;
                                                EC.EmergencyContactCD = EmergencyContactList[0].EmergencyContactCD;
                                                EC.FirstName = EmergencyContactList[0].FirstName;
                                                Trip.EmergencyContact = EC;
                                                Trip.EmergencyContactID = EmergencyContactList[0].EmergencyContactID;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        HomebaseSample.BaseDescription = UserPrincipal.Identity._fpSettings._BaseDescription;
                        //HomebaseSample.HomebaseCD = tbHomeBase.Text;
                        Trip.HomebaseID = HomebaseID;
                        Trip.HomeBaseAirportICAOID = UserPrincipal.Identity._airportICAOCd;
                        if (HomebaseSample.HomebaseAirportID != null)
                        {
                            Trip.HomeBaseAirportID = (long)HomebaseSample.HomebaseAirportID;
                        }
                        Trip.Company = HomebaseSample;

                    }
                }
            }
        }
        protected void NewTripEvent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.Mode == TripActionMode.Edit)
                    {
                        //this.PreflightHeader.HeaderMessage.ForeColor = System.Drawing.Color.Red;
                        //this.PreflightHeader.HeaderMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip Unsaved, please Save or Cancel Trip', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                    }
                    else
                    {
                        Trip = new PreflightMain();
                        Trip.Mode = TripActionMode.Edit;
                        Trip.AllowTripToNavigate = true;
                        Trip.AllowTripToSave = true;
                        Trip.resetLogisticsChecklist = false;
                        LoadHomebase(Trip);
                        Session["CurrentPreFlightTrip"] = Trip;
                        Session["PreflightException"] = null;
                        RedirectToMain();
                    }
                }
                else
                {
                    Trip = new PreflightMain();
                    Trip.Mode = TripActionMode.Edit;
                    Trip.AllowTripToNavigate = true;
                    Trip.resetLogisticsChecklist = false;
                    Trip.AllowTripToSave = true;
                    LoadHomebase(Trip);
                    Session["CurrentPreFlightTrip"] = Trip;
                    RedirectToMain();
                }
            }
        }

        protected void LogTripEvent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.Mode == TripActionMode.NoChange && Trip.TripID != 0)
                    {
                        if (Trip.FleetID != null && Trip.FleetID > 0) //Bug 2375 - #4874
                        {
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('Are you sure you want to create a Flight Log?',TripLogconfirmCallBackFn, 350, 110,null,'Confirmation!');");
                        }
                        else
                        {
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Tail Number Is Required To Log Tripsheet', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "',alertCallBack());");
                        }
                    }
                }
            }
        }

        private void LogTrip()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.Mode == TripActionMode.NoChange && Trip.TripID != 0)
                    {

                        PreflightSearchRetrieve PostflightMove = new PreflightSearchRetrieve();
                        bool tripmoved = false;
                        tripmoved = PostflightMove.GetTripandConfirm(Trip.TripID);

                        if (tripmoved)
                        {

                            FlightPak.Web.PostflightService.PostflightMain POLog = (FlightPak.Web.PostflightService.PostflightMain)Session["POSTFLIGHTMAIN"];
                            if (POLog != null)
                            {
                                Trip.IsLog = true;
                                Session["CurrentPreFlightTrip"] = Trip;
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('Trip Logged into Postflight, Click YES to View Postflight Log No:" + POLog.LogNum.ToString() + "',MasterconfirmCallBackFn, 330, 110,null,'Confirmation!');");
                            }
                        }

                    }
                }
            }
        }

        protected void ConfirmLogYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LogTrip();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void ConfirmLogNo_Click(object sender, EventArgs e)
        {
            //
        }

        protected void btnAlert_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Response.Redirect("PreflightLegs.aspx?seltab=Legs", true);
            }
        }

        protected void btnDeleteYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (PreflightServiceClient Service = new PreflightServiceClient())
                        {
                            CheckAutorization(Permission.Preflight.DeleteTripManager);
                            var ReturnValue = Service.Delete(Trip.TripID);
                            if (ReturnValue.ReturnFlag == true)
                            {
                                Session["PreflightException"] = null;
                                Session["CurrentPreFlightTrip"] = null;
                                RedirectToMain();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void btnDeleteNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void btnCancelYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (PreflightServiceClient Service = new PreflightServiceClient())
                        {
                            PreflightMain PreflightTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            SetTripModeToNoChange(ref PreflightTrip);
                            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                            {
                                if (PreflightTrip.TripID != 0)
                                {
                                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                                    {
                                        var returnValue = CommonService.UnLock(EntitySet.Preflight.PreflightMain, PreflightTrip.TripID);
                                    }
                                    var objPrefObj = PrefSvc.GetTrip(PreflightTrip.TripID);
                                    if (objPrefObj.ReturnFlag)
                                    {
                                        PreflightTrip = objPrefObj.EntityList[0];

                                        Session["CurrentPreFlightTrip"] = PreflightTrip;
                                        var ObjRetval = PrefSvc.GetTripExceptionList(PreflightTrip.TripID);
                                        if (ObjRetval.ReturnFlag)
                                        {
                                            Session["PreflightException"] = ObjRetval.EntityList;
                                        }
                                        else
                                            Session["PreflightException"] = null;
                                    }
                                    else
                                    {
                                        PreflightTrip = null;
                                        Session["PreflightException"] = null;
                                    }
                                }
                                else
                                {
                                    PreflightTrip = null;
                                    Session["CurrentPreFlightTrip"] = null;
                                    Session["PreflightException"] = null;
                                    RedirectToMain();
                                }
                            }
                            SetTripMode();
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"CloseAndRebindPage();");
                            //RedirectToMain();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void btnCancelNo_Click(object sender, EventArgs e)
        {
            // Your code for "No"
        }

        protected void LogYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (Session["CurrentPreFlightTrip"] != null)
                        {
                            PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                            if (Trip.Mode == TripActionMode.NoChange && Trip.TripID != 0)
                            {

                                bool TripLogged = false;
                                TripLogged = Trip.IsLog == null ? false : (bool)Trip.IsLog;

                                if (TripLogged)
                                {
                                    //this.PreflightHeader.HeaderMessage.ForeColor = System.Drawing.Color.Red;
                                    //this.PreflightHeader.HeaderMessage.Text = "Trip already Logged to Postflight";
                                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('The selected Tripsheet is already logged in Postflight. Selecting YES will remove all data previously entered per leg in this log.',TripFlightLogconfirmCallBackFn, 330, 110,null,'Confirmation!');");
                                }
                                else
                                {
                                    LogTrip();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
                }
            }
        }

        protected void LogNo_Click(object sender, EventArgs e)
        {
            //
        }

        protected void Yes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                Response.Redirect("../PostFlight/PostFlightLegs.aspx?seltab=legs&fromPage=retrieve", true);
            }
        }

        protected void No_Click(object sender, EventArgs e)
        {
            //
        }

        public void EditTripEvent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    PreflightMain Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    // Lock the record
                    using (CommonService.CommonServiceClient CommonService = new CommonService.CommonServiceClient())
                    {
                        var returnValue = CommonService.Lock(EntitySet.Preflight.PreflightMain, Trip.TripID);
                        if (!returnValue.ReturnFlag)
                        {                      
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('" + returnValue.LockMessage + "', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");

                            this.PreflightHeader.SaveTripButton.Enabled = false;
                            this.PreflightHeader.CancelTripButton.Enabled = false;
                            this.PreflightHeader.DeleteTripButton.Enabled = true;

                            this.PreflightHeader.SaveTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.CancelTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.DeleteTripButton.CssClass = "ui_nav";
                        }
                        else
                        {
                            Trip.Mode = TripActionMode.Edit;
                            //this.PreflightSearch.EditButton.Enabled = false;
                            this.PreflightHeader.EditTripButton.Enabled = false;
                            this.PreflightHeader.NewTripButton.Enabled = false;
                            this.PreflightHeader.MoveTripButton.Enabled = false;
                            this.PreflightHeader.LogButton.Enabled = false;
                            this.PreflightHeader.EmailButton.Enabled = false; // PROD-38
                            this.PreflightHeader.TravelsenseButton.Enabled = false;
                            this.PreflightHeader.CopyTripButton.Enabled = false;
                            this.PreflightHeader.APISTripButton.Enabled = false;
                            this.PreflightHeader.SIFLTripButton.Enabled = false;

                            if (this.PreflightHeader.HeaderTab.SelectedTab.Text == "UWA Services")
                            {
                                for (int i = 0; i <= 4; i++)
                                    this.PreflightHeader.HeaderTab.Tabs[i].Enabled = false;
                            }
                            else
                            {
                                this.PreflightHeader.HeaderTab.Tabs[5].Enabled = false;

                            }

                            //Replicate Save Cancel Delete Buttons in header
                            this.PreflightHeader.SaveTripButton.Enabled = true;
                            this.PreflightHeader.CancelTripButton.Enabled = true;
                            this.PreflightHeader.DeleteTripButton.Enabled = false;


                            this.PreflightHeader.SaveTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.CancelTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.DeleteTripButton.CssClass = "ui_nav_disable";

                            // this.PreflightSearch.EditButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.NewTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.EditTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.MoveTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.LogButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.EmailButton.CssClass = "ui_nav_disable"; // PROD-38
                            this.PreflightHeader.TravelsenseButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.CopyTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.APISTripButton.CssClass = "ui_nav_disable";
                            this.PreflightHeader.SIFLTripButton.CssClass = "ui_nav_disable";
                            Trip.AllowTripToNavigate = true;
                            Trip.AllowTripToSave = true;
                            Trip.resetLogisticsChecklist = false;
                            Session["CurrentPreFlightTrip"] = Trip;
                        }
                    }
                }
            }
        }

        protected void _LoadTriptoSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["CurrentPreFlightTrip"] != null)
                {
                    PreflightMain CurrTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (CurrTrip.Mode == TripActionMode.Edit && CurrTrip.TripID != this.PreflightSearch.TripID)
                    {
                        //this.PreflightHeader.HeaderMessage.ForeColor = System.Drawing.Color.Red;
                        //this.PreflightHeader.HeaderMessage.Text = "Trip Unsaved, please Save or Cancel Trip";
                        RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip Unsaved, please Save or Cancel Trip', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");
                    }
                    else
                    {
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            var objRetVal = PrefSvc.GetTrip(this.PreflightSearch.TripID);
                            if (objRetVal.ReturnFlag)
                            {
                                Trip = objRetVal.EntityList[0];
                                var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                                if (ObjretVal.ReturnFlag)
                                {
                                    Session["PreflightException"] = ObjretVal.EntityList;
                                }
                                else
                                    Session["PreflightException"] = null;
                                SetTripModeToNoChange(ref Trip);
                                //this.PreflightSearch.EditButton.Enabled = true;
                                this.PreflightHeader.EditTripButton.Enabled = true;
                                this.PreflightHeader.NewTripButton.Enabled = true;
                                this.PreflightHeader.MoveTripButton.Enabled = true;
                                this.PreflightHeader.LogButton.Enabled = true;
                                this.PreflightHeader.EmailButton.Enabled = true; // PROD-38
                                this.PreflightHeader.TravelsenseButton.Enabled = true;
                                this.PreflightHeader.CopyTripButton.Enabled = true;
                                this.PreflightHeader.APISTripButton.Enabled = true;
                                this.PreflightHeader.SIFLTripButton.Enabled = true;

                                //this.PreflightSearch.EditButton.CssClass = "ui_nav";
                                this.PreflightHeader.EditTripButton.CssClass = "ui_nav";
                                this.PreflightHeader.NewTripButton.CssClass = "ui_nav";
                                this.PreflightHeader.MoveTripButton.CssClass = "ui_nav";
                                this.PreflightHeader.LogButton.CssClass = "ui_nav";
                                this.PreflightHeader.EmailButton.CssClass = "ui_nav"; // PROD-38
                                this.PreflightHeader.TravelsenseButton.CssClass = "ui_nav";
                                this.PreflightHeader.CopyTripButton.CssClass = "ui_nav";
                                this.PreflightHeader.APISTripButton.CssClass = "ui_nav";
                                this.PreflightHeader.SIFLTripButton.CssClass = "ui_nav";
                                Trip.resetLogisticsChecklist = false;

                                Session["CurrentPreFlightTrip"] = Trip;

                                SetTripMode();
                                //RedirectToMain();
                            }
                        }
                    }
                }
                else
                {
                    using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                    {
                        var objRetVal = PrefSvc.GetTrip(this.PreflightSearch.TripID);
                        if (objRetVal.ReturnFlag)
                        {
                            Trip = objRetVal.EntityList[0];
                            var ObjretVal = PrefSvc.GetTripExceptionList(Trip.TripID);
                            if (ObjretVal.ReturnFlag)
                            {
                                Session["PreflightException"] = ObjretVal.EntityList;
                            }
                            else
                                Session["PreflightException"] = null;
                            SetTripModeToNoChange(ref Trip);
                            //this.PreflightSearch.EditButton.Enabled = true;
                            this.PreflightHeader.EditTripButton.Enabled = true;
                            this.PreflightHeader.NewTripButton.Enabled = true;
                            this.PreflightHeader.MoveTripButton.Enabled = true;
                            this.PreflightHeader.LogButton.Enabled = true;
                            this.PreflightHeader.EmailButton.Enabled = true; // PROD-38
                            this.PreflightHeader.TravelsenseButton.Enabled = true;
                            this.PreflightHeader.CopyTripButton.Enabled = true;
                            this.PreflightHeader.APISTripButton.Enabled = true;
                            this.PreflightHeader.SIFLTripButton.Enabled = true;

                            //this.PreflightSearch.EditButton.CssClass = "ui_nav";
                            this.PreflightHeader.EditTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.NewTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.MoveTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.LogButton.CssClass = "ui_nav";
                            this.PreflightHeader.EmailButton.CssClass = "ui_nav"; // PROD-38
                            this.PreflightHeader.TravelsenseButton.CssClass = "ui_nav";
                            this.PreflightHeader.CopyTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.APISTripButton.CssClass = "ui_nav";
                            this.PreflightHeader.SIFLTripButton.CssClass = "ui_nav";

                            Session["CurrentPreFlightTrip"] = Trip;
                            SetTripMode();
                            //RedirectToMain();
                        }
                    }
                }
            }
        }

        private void RedirectToMain()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Response.Redirect("PreflightMain.aspx?seltab=PreFlight", true);
            }
        }

        protected void SaveToPreflightSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SaveToSession != null)
                    _SaveToSession.DynamicInvoke();
            }
        }

        protected void SaveToPreflightSessionPAXCrew()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (_SaveToSessionPAXCrew != null)
                    _SaveToSessionPAXCrew.DynamicInvoke();
            }
        }

        protected void SetTripModeToNoChange(ref PreflightMain Trip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                if (Trip != null)
                {
                    Trip.Mode = TripActionMode.NoChange;
                    Trip.State = TripEntityState.NoChange;
                    if (Trip.PreflightLegs != null)
                        foreach (PreflightLeg Leg in Trip.PreflightLegs)
                        {
                            Leg.State = TripEntityState.NoChange;
                            if (Leg.PreflightCrewLists != null)
                                foreach (PreflightCrewList CrewList in Leg.PreflightCrewLists)
                                {
                                    CrewList.State = TripEntityState.NoChange;
                                }
                        }
                }
            }
        }

        /// <summary>
        /// Common method used to save the whole trip into Database
        /// </summary>
        /// <param name="trip">Pass Trip Object</param>
        public void Save(PreflightMain PreflightTrip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PreflightTrip))
            {
                if (PreflightTrip != null && PreflightTrip.resetLogisticsChecklist && Trip.State == TripEntityState.Modified && UserPrincipal.Identity._fpSettings._IsChecklistWarning)
                {
                    RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('Reset Logistics Checklist?',confirmResetLogisticsCallBackFn, 330, 110,null,'Confirmation!');");
                }
                else
                {
                    SaveTrip(PreflightTrip);
                }
            }
        }

        protected void btnResetLogisticsYes_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                PreflightMain PreflightTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                List<PreflightLeg> Preflegs = new List<PreflightLeg>();
                if (PreflightTrip.PreflightLegs != null && PreflightTrip.PreflightLegs.Count > 0)
                {
                    Preflegs = PreflightTrip.PreflightLegs.Where(x => x.IsDeleted == false).ToList();

                    if (Preflegs != null && Preflegs.Count > 0)
                    {

                        foreach (PreflightLeg Leg in Preflegs)
                        {
                            if (Leg.PreflightCheckLists != null)
                            {
                                foreach (PreflightCheckList Chklst in Leg.PreflightCheckLists)
                                {
                                    if (Chklst.PreflightCheckListID != 0 && Chklst.State != TripEntityState.Deleted)
                                        Chklst.State = TripEntityState.Modified;
                                    Chklst.IsCompleted = false;
                                }
                            }
                        }
                    }
                }
                SaveTrip(PreflightTrip);
            }
        }

        protected void btnResetLogisticsNo_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                PreflightMain PreflightTrip = (PreflightMain)Session["CurrentPreFlightTrip"];
                SaveTrip(PreflightTrip);
            }
        }

        private void CrewTransportchoice()
        {
            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            if (Trip != null)
            {
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    foreach (PreflightLeg TripLeg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                    {
                        if (TripLeg.DepartAirportChangedUpdateCrew)
                        {
                            #region "Depart Transport"
                            if (IsAuthorized(Permission.Preflight.ViewPreflightCrewTransport))
                            {

                                if (TripLeg.PreflightTransportLists != null && TripLeg.PreflightTransportLists.Count > 0)
                                {
                                    PreflightTransportList DeparTrans = (from transport in TripLeg.PreflightTransportLists.ToList()
                                                                         where transport.IsDeleted == false && transport.IsDepartureTransport == true && transport.CrewPassengerType == "C"
                                                                         select transport).SingleOrDefault();

                                    if (DeparTrans != null)
                                    {
                                        if (DeparTrans.PreflightTransportID == 0)
                                            TripLeg.PreflightTransportLists.Remove(DeparTrans);
                                        else
                                        {

                                            DeparTrans.IsDeleted = true;
                                            DeparTrans.State = TripEntityState.Deleted;
                                        }
                                    }
                                }

                                #region "Add Depart Transport"


                                if (TripLeg.DepartICAOID != null && TripLeg.DepartICAOID > 0)
                                {
                                    MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                                    var ObjRetValGroup = ObjService.GetTransportByAirportID((long)TripLeg.DepartICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                    if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                                    {
                                        PreflightTransportList DepTransport = new PreflightTransportList();

                                        PreflightService.Transport PrefTrans = new PreflightService.Transport();

                                        PreflightService.Transport PrefTransport = new PreflightService.Transport();

                                        PrefTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        PrefTransport.TransportCD = ObjRetValGroup[0].TransportCD;
                                        DepTransport.Transport = PrefTransport;

                                        DepTransport.CrewPassengerType = "C";
                                        DepTransport.AirportID = ObjRetValGroup[0].AirportID;
                                        DepTransport.IsArrivalTransport = false;
                                        DepTransport.IsDepartureTransport = true;
                                        DepTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        DepTransport.PreflightTransportName = ObjRetValGroup[0].TransportationVendor;
                                        DepTransport.PhoneNum1 = ObjRetValGroup[0].PhoneNum;
                                        DepTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                                        DepTransport.PhoneNum4 = ObjRetValGroup[0].NegotiatedRate.ToString();
                                        DepTransport.LastUpdTS = DateTime.UtcNow;
                                        DepTransport.IsDeleted = false;
                                        DepTransport.State = TripEntityState.Added;

                                        if (TripLeg.PreflightTransportLists == null)
                                            TripLeg.PreflightTransportLists = new List<PreflightTransportList>();
                                        TripLeg.PreflightTransportLists.Add(DepTransport);
                                    }
                                }
                                #endregion

                            }
                            #endregion

                            TripLeg.DepartAirportChangedUpdateCrew = false;
                        }
                        if (TripLeg.ArrivalAirportChangedUpdateCrew)
                        {
                            #region "Arrive Transport"
                            if (IsAuthorized(Permission.Preflight.ViewPreflightCrewTransport))
                            {
                                if (TripLeg.PreflightTransportLists != null && TripLeg.PreflightTransportLists.Count > 0)
                                {
                                    PreflightTransportList Arrivaltransport = (from transport in TripLeg.PreflightTransportLists.ToList()
                                                                               where transport.IsDeleted == false && transport.IsArrivalTransport == true && transport.CrewPassengerType == "C"
                                                                               select transport).SingleOrDefault();

                                    if (Arrivaltransport != null)
                                    {
                                        if (Arrivaltransport.PreflightTransportID == 0)
                                            TripLeg.PreflightTransportLists.Remove(Arrivaltransport);
                                        else
                                        {
                                            Arrivaltransport.IsDeleted = true;
                                            Arrivaltransport.State = TripEntityState.Deleted;
                                        }
                                    }
                                }

                                #region "Add Arrival Transport"
                                if (TripLeg.ArriveICAOID != null && TripLeg.ArriveICAOID > 0)
                                {
                                    MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                                    var ObjRetValGroup = ObjService.GetTransportByAirportID((long)TripLeg.ArriveICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                    if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                                    {
                                        PreflightTransportList ArrTransport = new PreflightTransportList();

                                        PreflightService.Transport PrefTransport = new PreflightService.Transport();

                                        PrefTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        PrefTransport.TransportCD = ObjRetValGroup[0].TransportCD;
                                        ArrTransport.Transport = PrefTransport;

                                        ArrTransport.CrewPassengerType = "C";
                                        ArrTransport.AirportID = ObjRetValGroup[0].AirportID;
                                        ArrTransport.IsArrivalTransport = true;
                                        ArrTransport.IsDepartureTransport = false;
                                        ArrTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        ArrTransport.PreflightTransportName = ObjRetValGroup[0].TransportationVendor;
                                        ArrTransport.PhoneNum1 = ObjRetValGroup[0].PhoneNum;
                                        ArrTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                                        ArrTransport.PhoneNum4 = ObjRetValGroup[0].NegotiatedRate.ToString();
                                        ArrTransport.LastUpdTS = DateTime.UtcNow;
                                        ArrTransport.IsDeleted = false;
                                        ArrTransport.State = TripEntityState.Added;
                                        if (TripLeg.PreflightTransportLists == null)
                                            TripLeg.PreflightTransportLists = new List<PreflightTransportList>();
                                        TripLeg.PreflightTransportLists.Add(ArrTransport);
                                    }
                                }

                                #endregion
                            }
                            #endregion

                            TripLeg.ArrivalAirportChangedUpdateCrew = false;
                        }
                    }
                }
            }
        }

        private void PaxTransportchoice()
        {
            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            if (Trip != null)
            {
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    foreach (PreflightLeg TripLeg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                    {
                        if (TripLeg.DepartAirportChangedUpdatePAX)
                        {
                            #region "Depart Transport"
                            if (IsAuthorized(Permission.Preflight.ViewPreflightPassengerTransport))
                            {

                                if (TripLeg.PreflightTransportLists != null && TripLeg.PreflightTransportLists.Count > 0)
                                {
                                    PreflightTransportList DeparTrans = (from transport in TripLeg.PreflightTransportLists.ToList()
                                                                         where transport.IsDeleted == false && transport.IsDepartureTransport == true && transport.CrewPassengerType == "P"
                                                                         select transport).SingleOrDefault();

                                    if (DeparTrans != null)
                                    {
                                        if (DeparTrans.PreflightTransportID == 0)
                                            TripLeg.PreflightTransportLists.Remove(DeparTrans);
                                        else
                                        {

                                            DeparTrans.IsDeleted = true;
                                            DeparTrans.State = TripEntityState.Deleted;
                                        }
                                    }
                                }

                                #region "Add Depart Transport"


                                if (TripLeg.DepartICAOID != null && TripLeg.DepartICAOID > 0)
                                {
                                    MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                                    var ObjRetValGroup = ObjService.GetTransportByAirportID((long)TripLeg.DepartICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                    if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                                    {
                                        PreflightTransportList DepTransport = new PreflightTransportList();

                                        PreflightService.Transport PrefTransport = new PreflightService.Transport();

                                        PrefTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        PrefTransport.TransportCD = ObjRetValGroup[0].TransportCD;
                                        DepTransport.Transport = PrefTransport;

                                        DepTransport.CrewPassengerType = "P";
                                        DepTransport.AirportID = ObjRetValGroup[0].AirportID;
                                        DepTransport.IsArrivalTransport = false;
                                        DepTransport.IsDepartureTransport = true;
                                        DepTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        DepTransport.PreflightTransportName = ObjRetValGroup[0].TransportationVendor;
                                        DepTransport.PhoneNum1 = ObjRetValGroup[0].PhoneNum;
                                        DepTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                                        DepTransport.PhoneNum4 = ObjRetValGroup[0].NegotiatedRate.ToString();
                                        DepTransport.LastUpdTS = DateTime.UtcNow;
                                        DepTransport.IsDeleted = false;
                                        DepTransport.State = TripEntityState.Added;

                                        if (TripLeg.PreflightTransportLists == null)
                                            TripLeg.PreflightTransportLists = new List<PreflightTransportList>();
                                        TripLeg.PreflightTransportLists.Add(DepTransport);
                                    }
                                }
                                #endregion

                            }
                            #endregion

                            TripLeg.DepartAirportChangedUpdatePAX = false;
                        }
                        if (TripLeg.ArrivalAirportChangedUpdatePAX)
                        {
                            #region "Arrive Transport"
                            if (IsAuthorized(Permission.Preflight.ViewPreflightPassengerTransport))
                            {
                                if (TripLeg.PreflightTransportLists != null && TripLeg.PreflightTransportLists.Count > 0)
                                {
                                    PreflightTransportList Arrivaltransport = (from transport in TripLeg.PreflightTransportLists.ToList()
                                                                               where transport.IsDeleted == false && transport.IsArrivalTransport == true && transport.CrewPassengerType == "P"
                                                                               select transport).SingleOrDefault();

                                    if (Arrivaltransport != null)
                                    {
                                        if (Arrivaltransport.PreflightTransportID == 0)
                                            TripLeg.PreflightTransportLists.Remove(Arrivaltransport);
                                        else
                                        {
                                            Arrivaltransport.IsDeleted = true;
                                            Arrivaltransport.State = TripEntityState.Deleted;
                                        }
                                    }
                                }

                                #region "Add Arrival Transport"
                                if (TripLeg.ArriveICAOID != null && TripLeg.ArriveICAOID > 0)
                                {
                                    MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                                    var ObjRetValGroup = ObjService.GetTransportByAirportID((long)TripLeg.ArriveICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                    if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                                    {
                                        PreflightTransportList ArrTransport = new PreflightTransportList();

                                        PreflightService.Transport PrefTransport = new PreflightService.Transport();

                                        PrefTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        PrefTransport.TransportCD = ObjRetValGroup[0].TransportCD;
                                        ArrTransport.Transport = PrefTransport;

                                        ArrTransport.CrewPassengerType = "P";
                                        ArrTransport.AirportID = ObjRetValGroup[0].AirportID;
                                        ArrTransport.IsArrivalTransport = true;
                                        ArrTransport.IsDepartureTransport = false;
                                        ArrTransport.TransportID = ObjRetValGroup[0].TransportID;
                                        ArrTransport.PreflightTransportName = ObjRetValGroup[0].TransportationVendor;
                                        ArrTransport.PhoneNum1 = ObjRetValGroup[0].PhoneNum;
                                        ArrTransport.FaxNUM = ObjRetValGroup[0].FaxNum;
                                        ArrTransport.PhoneNum4 = ObjRetValGroup[0].NegotiatedRate.ToString();
                                        ArrTransport.LastUpdTS = DateTime.UtcNow;
                                        ArrTransport.IsDeleted = false;
                                        ArrTransport.State = TripEntityState.Added;
                                        if (TripLeg.PreflightTransportLists == null)
                                            TripLeg.PreflightTransportLists = new List<PreflightTransportList>();
                                        TripLeg.PreflightTransportLists.Add(ArrTransport);
                                    }
                                }

                                #endregion
                            }
                            #endregion

                            TripLeg.ArrivalAirportChangedUpdatePAX = false;
                        }
                    }
                }
            }
        }

        private void FBOandCateringchoice()
        {
            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
            if (Trip != null)
            {
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    foreach (PreflightLeg TripLeg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                    {

                        if (TripLeg.DepartAirportChanged)
                        {
                            #region "Depart FBO"
                            if (IsAuthorized(Permission.Preflight.ViewPreflightFBO))
                            {

                                if (TripLeg.PreflightFBOLists != null && TripLeg.PreflightFBOLists.Count > 0)
                                {
                                    PreflightFBOList Deparfbo = (from fbo in TripLeg.PreflightFBOLists.ToList()
                                                                 where fbo.IsDeleted == false && fbo.IsDepartureFBO == true
                                                                 select fbo).SingleOrDefault();

                                    if (Deparfbo != null)
                                    {
                                        if (Deparfbo.PreflightFBOID == 0)
                                            TripLeg.PreflightFBOLists.Remove(Deparfbo);
                                        else
                                        {

                                            Deparfbo.IsDeleted = true;
                                            Deparfbo.State = TripEntityState.Deleted;
                                        }
                                    }
                                }

                                #region "Add Depart FBO"


                                if (TripLeg.DepartICAOID != null && TripLeg.DepartICAOID > 0)
                                {
                                    MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                                    var ObjRetValGroup = ObjService.GetAllFBOByAirportID((long)TripLeg.DepartICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                    if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                                    {
                                        PreflightFBOList Fbo = new PreflightFBOList();

                                        PreflightService.FBO PrefFbo = new PreflightService.FBO();

                                        PrefFbo.FBOID = ObjRetValGroup[0].FBOID;
                                        PrefFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                        Fbo.FBO = PrefFbo;

                                        Fbo.AirportID = TripLeg.DepartICAOID;
                                        Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                        Fbo.IsArrivalFBO = false;
                                        Fbo.IsDepartureFBO = true;
                                        Fbo.PreflightFBOName = ObjRetValGroup[0].FBOVendor;
                                        Fbo.CityName = ObjRetValGroup[0].CityName;
                                        Fbo.StateName = ObjRetValGroup[0].StateName;
                                        Fbo.PostalZipCD = ObjRetValGroup[0].PostalZipCD;
                                        Fbo.PhoneNum1 = ObjRetValGroup[0].PhoneNUM1;
                                        Fbo.PhoneNum2 = ObjRetValGroup[0].PhoneNUM2;
                                        Fbo.FaxNUM = ObjRetValGroup[0].FaxNum;
                                        Fbo.FBOInformation = ObjRetValGroup[0].FBOVendor + "," + ObjRetValGroup[0].CityName + "," + ObjRetValGroup[0].StateName + "," + ObjRetValGroup[0].PostalZipCD + "," + ObjRetValGroup[0].PhoneNUM1 + "," + ObjRetValGroup[0].PhoneNUM2 + "," + ObjRetValGroup[0].FaxNum;
                                        Fbo.LastUpdTS = DateTime.UtcNow;
                                        Fbo.IsDeleted = false;
                                        Fbo.State = TripEntityState.Added;
                                        if (TripLeg.PreflightFBOLists == null)
                                            TripLeg.PreflightFBOLists = new List<PreflightFBOList>();
                                        TripLeg.PreflightFBOLists.Add(Fbo);
                                    }
                                }
                                #endregion

                            }
                            #endregion

                            //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                            #region "Depart Catering"

                            //if (IsAuthorized(Permission.Preflight.ViewPreflightCatering))
                            //{
                            //    if (TripLeg.PreflightCateringDetails != null && TripLeg.PreflightCateringDetails.Count > 0)
                            //    {
                            //        PreflightCateringDetail Deparcater = (from fbo in TripLeg.PreflightCateringDetails.ToList()
                            //                                              where fbo.IsDeleted == false && fbo.ArriveDepart == "D"
                            //                                              select fbo).SingleOrDefault();

                            //        if (Deparcater != null)
                            //        {
                            //            if (Deparcater.PreflightCateringID == 0)
                            //                TripLeg.PreflightCateringDetails.Remove(Deparcater);
                            //            else
                            //            {
                            //                Deparcater.IsDeleted = true;
                            //                Deparcater.State = TripEntityState.Deleted;
                            //            }
                            //        }
                            //    }

                            //    #region "Add Depart Catering"

                            //    if (TripLeg.DepartICAOID != null && TripLeg.DepartICAOID > 0)
                            //    {
                            //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            //        var ObjRetValGroup = ObjService.GetAllCateringByAirportID((long)TripLeg.DepartICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            //        {
                            //            PreflightCateringDetail catering = new PreflightCateringDetail();

                            //            PreflightService.Catering Prefcatering = new PreflightService.Catering();

                            //            Prefcatering.CateringID = ObjRetValGroup[0].CateringID;
                            //            Prefcatering.CateringCD = ObjRetValGroup[0].CateringCD;
                            //            catering.Catering = Prefcatering;

                            //            catering.AirportID = TripLeg.DepartICAOID;
                            //            catering.CateringID = ObjRetValGroup[0].CateringID;
                            //            catering.ArriveDepart = "D";
                            //            catering.ContactName = ObjRetValGroup[0].CateringVendor;
                            //            catering.ContactPhone = ObjRetValGroup[0].PhoneNum;
                            //            catering.ContactFax = ObjRetValGroup[0].FaxNum;
                            //            catering.Cost = ObjRetValGroup[0].NegotiatedRate;
                            //            catering.LastUpdTS = DateTime.UtcNow;
                            //            catering.IsDeleted = false;
                            //            catering.State = TripEntityState.Added;
                            //            if (TripLeg.PreflightCateringDetails == null)
                            //                TripLeg.PreflightCateringDetails = new List<PreflightCateringDetail>();
                            //            TripLeg.PreflightCateringDetails.Add(catering);

                            //            //if (catering != null)
                            //            //{
                            //            //    if (!string.IsNullOrEmpty(catering.CateringID.ToString()))
                            //            //        hdDepartCaterID.Value = catering.CateringID.ToString();
                            //            //    tbDepartCaterName.Text = catering.ContactName;
                            //            //    tbDepartCaterPhone.Text = catering.ContactPhone;
                            //            //    tbDepartCaterFax.Text = catering.ContactFax;
                            //            //    tbDepartCaterCost.Text = catering.Cost.ToString();

                            //            //    if (catering.Catering != null)
                            //            //    {
                            //            //        if (!string.IsNullOrEmpty(catering.Catering.CateringCD.ToString()))
                            //            //            tbDepartCaterCode.Text = catering.Catering.CateringCD;
                            //            //    }
                            //            //}
                            //        }
                            //    }

                            //    #endregion

                            //}

                            #endregion

                            TripLeg.DepartAirportChanged = false;
                        }

                        if (TripLeg.ArrivalAirportChanged)
                        {
                            #region "Arrive FBO"
                            if (IsAuthorized(Permission.Preflight.ViewPreflightFBO))
                            {
                                if (TripLeg.PreflightFBOLists != null && TripLeg.PreflightFBOLists.Count > 0)
                                {
                                    PreflightFBOList Arrivalfbo = (from fbo in TripLeg.PreflightFBOLists.ToList()
                                                                   where fbo.IsDeleted == false && fbo.IsArrivalFBO == true
                                                                   select fbo).SingleOrDefault();

                                    if (Arrivalfbo != null)
                                    {
                                        if (Arrivalfbo.PreflightFBOID == 0)
                                            TripLeg.PreflightFBOLists.Remove(Arrivalfbo);
                                        else
                                        {
                                            Arrivalfbo.IsDeleted = true;
                                            Arrivalfbo.State = TripEntityState.Deleted;
                                        }
                                    }
                                }

                                #region "Add Arrival FBO"
                                if (TripLeg.ArriveICAOID != null && TripLeg.ArriveICAOID > 0)
                                {
                                    MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                                    var ObjRetValGroup = ObjService.GetAllFBOByAirportID((long)TripLeg.ArriveICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                                    if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                                    {
                                        PreflightFBOList Fbo = new PreflightFBOList();
                                        PreflightService.FBO PrefFbo = new PreflightService.FBO();

                                        PrefFbo.FBOID = ObjRetValGroup[0].FBOID;
                                        PrefFbo.FBOCD = ObjRetValGroup[0].FBOCD;
                                        Fbo.FBO = PrefFbo;

                                        Fbo.AirportID = TripLeg.ArriveICAOID;
                                        Fbo.FBOID = ObjRetValGroup[0].FBOID;
                                        Fbo.IsArrivalFBO = true;
                                        Fbo.IsDepartureFBO = false;
                                        Fbo.PreflightFBOName = ObjRetValGroup[0].FBOVendor;
                                        Fbo.CityName = ObjRetValGroup[0].CityName;
                                        Fbo.StateName = ObjRetValGroup[0].StateName;
                                        Fbo.PostalZipCD = ObjRetValGroup[0].PostalZipCD;
                                        Fbo.PhoneNum1 = ObjRetValGroup[0].PhoneNUM1;
                                        Fbo.PhoneNum2 = ObjRetValGroup[0].PhoneNUM2;
                                        Fbo.FaxNUM = ObjRetValGroup[0].FaxNum;
                                        Fbo.FBOInformation = ObjRetValGroup[0].FBOVendor + "," + ObjRetValGroup[0].CityName + "," + ObjRetValGroup[0].StateName + "," + ObjRetValGroup[0].PostalZipCD + "," + ObjRetValGroup[0].PhoneNUM1 + "," + ObjRetValGroup[0].PhoneNUM2 + "," + ObjRetValGroup[0].FaxNum;
                                        Fbo.LastUpdTS = DateTime.UtcNow;
                                        Fbo.IsDeleted = false;
                                        Fbo.State = TripEntityState.Added;
                                        if (TripLeg.PreflightFBOLists == null)
                                            TripLeg.PreflightFBOLists = new List<PreflightFBOList>();
                                        TripLeg.PreflightFBOLists.Add(Fbo);
                                    }
                                }

                                #endregion
                            }
                            #endregion

                            //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                            #region "Arrive Catering"
                            //if (IsAuthorized(Permission.Preflight.ViewPreflightCatering))
                            //{
                            //    if (TripLeg.PreflightCateringDetails != null && TripLeg.PreflightCateringDetails.Count > 0)
                            //    {
                            //        PreflightCateringDetail Arrivalcater = (from fbo in TripLeg.PreflightCateringDetails.ToList()
                            //                                                where fbo.IsDeleted == false && fbo.ArriveDepart == "A"
                            //                                                select fbo).SingleOrDefault();

                            //        if (Arrivalcater != null)
                            //        {
                            //            if (Arrivalcater.PreflightCateringID == 0)
                            //                TripLeg.PreflightCateringDetails.Remove(Arrivalcater);
                            //            else
                            //            {
                            //                Arrivalcater.IsDeleted = true;
                            //                Arrivalcater.State = TripEntityState.Deleted;
                            //            }
                            //        }
                            //    }

                            //    #region "Add Arrive Catering"

                            //    if (TripLeg.ArriveICAOID != null && TripLeg.ArriveICAOID > 0)
                            //    {
                            //        MasterCatalogServiceClient ObjService = new MasterCatalogServiceClient();
                            //        var ObjRetValGroup = ObjService.GetAllCateringByAirportID((long)TripLeg.ArriveICAOID).EntityList.Where(x => x.IsChoice == true && x.IsDeleted == false).ToList();

                            //        if (ObjRetValGroup.Count > 0 && ObjRetValGroup != null)
                            //        {
                            //            PreflightCateringDetail catering = new PreflightCateringDetail();

                            //            PreflightService.Catering Prefcatering = new PreflightService.Catering();

                            //            Prefcatering.CateringID = ObjRetValGroup[0].CateringID;
                            //            Prefcatering.CateringCD = ObjRetValGroup[0].CateringCD;
                            //            catering.Catering = Prefcatering;

                            //            catering.AirportID = TripLeg.ArriveICAOID;
                            //            catering.CateringID = ObjRetValGroup[0].CateringID;
                            //            catering.ArriveDepart = "A";
                            //            catering.ContactName = ObjRetValGroup[0].CateringVendor;
                            //            catering.ContactPhone = ObjRetValGroup[0].PhoneNum;
                            //            catering.ContactFax = ObjRetValGroup[0].FaxNum;
                            //            catering.Cost = ObjRetValGroup[0].NegotiatedRate;
                            //            catering.LastUpdTS = DateTime.UtcNow;
                            //            catering.IsDeleted = false;
                            //            catering.State = TripEntityState.Added;
                            //            if (TripLeg.PreflightCateringDetails == null)
                            //                TripLeg.PreflightCateringDetails = new List<PreflightCateringDetail>();
                            //            TripLeg.PreflightCateringDetails.Add(catering);
                            //        }
                            //    }

                            //    #endregion
                            //}
                            #endregion

                            TripLeg.ArrivalAirportChanged = false;
                        }

                    }
                }
            }
            Session["CurrentPreFlightTrip"] = Trip;
        }

        public void SaveTrip(PreflightMain PreflightTrip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PreflightTrip))
            {
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {
                    //UWA Defect ID:7232 Catering, transport, should not auto load So commenting out these lines
                    //CrewTransportchoice();
                    //PaxTransportchoice();
                    FBOandCateringchoice();

                    PreflightTrip.AllowTripToSave = true;

                    if (PreflightTrip.PreflightLegs != null)
                    {
                        DateTime LegMinDate = DateTime.MinValue;

                        foreach (PreflightLeg Leg in PreflightTrip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList())
                        {
                            PreflightLeg Nextleg = new PreflightLeg();
                            Nextleg = PreflightTrip.PreflightLegs.Where(x => x.LegNUM == (Leg.LegNUM + 1)).FirstOrDefault();

                            if (Nextleg != null)
                            {
                                if (Nextleg.DepartureGreenwichDTTM != null)
                                    Leg.NextGMTDTTM = Nextleg.DepartureGreenwichDTTM;
                                if (Nextleg.HomeDepartureDTTM != null)
                                    Leg.NextHomeDTTM = Nextleg.HomeDepartureDTTM;
                                if (Nextleg.DepartureDTTMLocal != null)
                                    Leg.NextLocalDTTM = Nextleg.DepartureDTTMLocal;
                            }
                            else
                            {
                                if (Leg.ArrivalGreenwichDTTM != null)
                                    Leg.NextGMTDTTM = Leg.ArrivalGreenwichDTTM;
                                if (Leg.HomeArrivalDTTM != null)
                                    Leg.NextHomeDTTM = Leg.HomeArrivalDTTM;
                                if (Leg.ArrivalDTTMLocal != null)
                                    Leg.NextLocalDTTM = Leg.ArrivalDTTMLocal;
                            }
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                if (LegMinDate == DateTime.MinValue)
                                    LegMinDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (LegMinDate > (DateTime)Leg.DepartureDTTMLocal)
                                    LegMinDate = (DateTime)Leg.DepartureDTTMLocal;
                            }
                            //Defect: 2764
                            if (Leg.LegNUM == 1)
                            {
                                PreflightTrip.EstDepartureDT = Leg.DepartureDTTMLocal;
                            }

                        }
                        //Defect: 2764
                        //if (LegMinDate != DateTime.MinValue)
                        //{
                        //    string dtstr = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity._fpSettings._ApplicationDateFormat + "}", LegMinDate);
                        //    DateTime dt = DateTime.ParseExact(dtstr, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                        //    PreflightTrip.EstDepartureDT = dt;
                        //}

                        //PreflightTrip.EstDepartureDT = DateTime.ParseExact(LegMinDate, RadDatePicker1.DateInput.DateFormat, CultureInfo.InvariantCulture);
                    }


                    #region Exception Validation

                    List<PreflightTripException> eSeverityException = new List<PreflightTripException>();
                    List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                    ExceptionTemplateList = GetExceptionTemplateList();



                    if (ExceptionTemplateList != null && ExceptionTemplateList.Count > 0)
                    {
                        var ExceptionlistforMandate = Service.ValidateTripforMandatoryExceptions(PreflightTrip);

                        if (ExceptionlistforMandate.ReturnFlag)
                        {
                            ExceptionList = ExceptionlistforMandate.EntityList;


                        }



                        //ExceptionList = (List<PreflightTripException>)Session["PreflightException"];

                        if (ExceptionList != null && ExceptionList.Count > 0)
                        {
                            eSeverityException = (from Exp in ExceptionList
                                                  join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                  where ExpTempl.Severity == 1
                                                  select Exp).ToList();
                            if (eSeverityException != null && eSeverityException.Count > 0)
                            {
                                PreflightTrip.AllowTripToSave = false;
                                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip has mandatory exceptions, Trip is not Saved', 330, 110,'" + ModuleNameConstants.Preflight.TripManager + "');");

                                List<PreflightTripException> OldExceptionList = new List<PreflightTripException>();
                                OldExceptionList = (List<PreflightTripException>)Session["PreflightException"];

                                if (OldExceptionList != null && OldExceptionList.Count > 0)
                                {

                                    foreach (PreflightTripException OldMandatoryException in OldExceptionList.ToList())
                                    {
                                        if (OldMandatoryException.TripExceptionID == 0)
                                            OldExceptionList.Remove(OldMandatoryException);
                                    }
                                }



                                if (ExceptionList != null && ExceptionList.Count > 0)
                                {
                                    if (OldExceptionList == null)
                                        OldExceptionList = new List<PreflightTripException>();
                                    OldExceptionList.AddRange(ExceptionList);
                                }


                                Session["PreflightException"] = OldExceptionList;
                                dgException.Rebind();
                            }
                        }
                    }

                    #endregion

                    if (PreflightTrip.AllowTripToSave)
                    {
                        //RemoveTripNavigationProp(ref trip);
                        PreflightTrip.PreviousNUM = 0;
                        if (PreflightTrip.TripID > 0)
                            PreflightTrip.State = TripEntityState.Modified;
                        else
                            PreflightTrip.State = TripEntityState.Added;

                        if (PreflightTrip.State == TripEntityState.Added)
                        {
                            CheckAutorization(Permission.Preflight.AddTripManager);

                        }
                        else if (PreflightTrip.State == TripEntityState.Modified)
                        {
                            CheckAutorization(Permission.Preflight.AddTripManager);
                        }

                        if ((bool)UserPrincipal.Identity._fpSettings._IsAutoRevisionNum)
                        {

                            if (PreflightTrip.State == TripEntityState.Added)
                            {
                                Trip.RevisionNUM = 1;

                            }
                            else
                                Trip.RevisionNUM = (Trip.RevisionNUM == null ? 0 : Trip.RevisionNUM) + 1;
                        }


                        if (UserPrincipal != null)
                            Trip.LastUpdUID = UserPrincipal.Identity._name;
                        Trip.LastUpdTS = DateTime.UtcNow;

                        //For other than Copy trip we make IsTripCopied to false
                        if (PreflightTrip.State == TripEntityState.Added)
                        {
                            Trip.IsTripCopied = false;
                        }
                        if (PreflightTrip.Account != null) PreflightTrip.Account = null;
                        if (PreflightTrip.Aircraft != null) PreflightTrip.Aircraft = null;
                        if (PreflightTrip.Client != null) PreflightTrip.Client = null;
                        if (PreflightTrip.Crew != null) PreflightTrip.Crew = null;
                        if (PreflightTrip.Company != null) PreflightTrip.Company = null;
                        if (PreflightTrip.Department != null) PreflightTrip.Department = null;
                        if (PreflightTrip.DepartmentAuthorization != null) PreflightTrip.DepartmentAuthorization = null;
                        if (PreflightTrip.EmergencyContact != null) PreflightTrip.EmergencyContact = null;
                        if (PreflightTrip.Fleet != null) PreflightTrip.Fleet = null;
                        if (PreflightTrip.Passenger != null) PreflightTrip.Passenger = null;
                        if (PreflightTrip.UserMaster != null) PreflightTrip.UserMaster = null;
                        if (PreflightTrip.UserMaster1 != null) PreflightTrip.UserMaster1 = null;
                        if (PreflightTrip.RecordType == null) PreflightTrip.RecordType = "T";
                        if (PreflightTrip.LastUpdTS == null) PreflightTrip.LastUpdTS = DateTime.UtcNow;
                        if (PreflightTrip.CQCustomer != null) PreflightTrip.CQCustomer = null;

                        #region Legs

                        if (PreflightTrip.PreflightLegs != null)
                        {


                            foreach (PreflightLeg Leg in PreflightTrip.PreflightLegs)
                            {

                                if (PreflightTrip.EstDepartureDT == null)
                                {
                                    if (Leg.LegNUM == 1 && Leg.DepartureDTTMLocal != null)
                                        PreflightTrip.EstDepartureDT = Leg.DepartureDTTMLocal;
                                }
                                //Leg.TripID = PreflightTrip.TripID;
                                //Leg.PreflightMain = PreflightTrip;ate
                                if (Leg.Account != null) Leg.Account = null;
                                if (Leg.Airport != null) Leg.Airport = null;
                                if (Leg.Airport1 != null) Leg.Airport1 = null;
                                if (Leg.Client != null) Leg.Client = null;
                                if (Leg.FlightCatagory != null) Leg.FlightCatagory = null;
                                if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                                if (Leg.Department != null) Leg.Department = null;
                                if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                                if (Leg.DepartmentAuthorization != null) Leg.DepartmentAuthorization = null;
                                if (Leg.FBO != null) Leg.FBO = null;
                                if (Leg.Passenger != null) Leg.Passenger = null;
                                if (Leg.UserMaster != null) Leg.UserMaster = null;
                                if (Leg.CQCustomer != null) Leg.CQCustomer = null;

                                if ((bool)Leg.IsDeleted)
                                {
                                    if (Leg.PreflightCrewLists != null) Leg.PreflightCrewLists.Clear();
                                    if (Leg.PreflightCateringDetails != null) Leg.PreflightCateringDetails.Clear();
                                    if (Leg.PreflightCateringLists != null) Leg.PreflightCateringLists.Clear();
                                    if (Leg.PreflightCheckLists != null) Leg.PreflightCheckLists.Clear();
                                    if (Leg.PreflightFBOLists != null) Leg.PreflightFBOLists.Clear();
                                    if (Leg.PreflightPassengerLists != null) Leg.PreflightPassengerLists.Clear();
                                    if (Leg.PreflightTransportLists != null) Leg.PreflightTransportLists.Clear();
                                    if (Leg.PreflightHotelLists != null) Leg.PreflightHotelLists.Clear();
                                    if (Leg.PreflightTripOutbounds != null) Leg.PreflightTripOutbounds.Clear();
                                    if (Leg.PreflightTripSIFLs != null) Leg.PreflightTripSIFLs.Clear();
                                    if (Leg.UWALs != null) Leg.UWALs.Clear();
                                    if (Leg.UWALTsPers != null) Leg.UWALTsPers.Clear();
                                    if (Leg.UWAPermits != null) Leg.UWAPermits.Clear();
                                    if (Leg.UWASpServs != null) Leg.UWASpServs.Clear();
                                    if (Leg.UWATSSLegs != null) Leg.UWATSSLegs.Clear();

                                }
                                else
                                {
                                    if (Leg.PreflightCrewLists != null)
                                    {
                                        foreach (PreflightCrewList Crew in Leg.PreflightCrewLists)
                                        {
                                            //Crew.LegID = Leg.LegID;
                                            //Crew.PreflightLeg = Leg;
                                            if (Crew.PreflightLeg != null) Crew.PreflightLeg = null;
                                            if (Crew.Crew != null) Crew.Crew = null;
                                            if (Crew.UserMaster != null) Crew.UserMaster = null;
                                            #region Hotelcode for Preflight Crew needs to be removed
                                            if (Crew.PreflightCrewHotelLists != null)
                                            {
                                                foreach (PreflightCrewHotelList Hotel in Crew.PreflightCrewHotelLists)
                                                {
                                                    if (Hotel.Airport != null) Hotel.Airport = null;
                                                    if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                                    if (Hotel.Hotel != null) Hotel.Hotel = null;
                                                }
                                            }
                                            #endregion
                                        }
                                    }

                                    #region New Code
                                    if (Leg.PreflightHotelLists != null)
                                    {

                                        foreach (PreflightHotelList Hotel in Leg.PreflightHotelLists)
                                        {
                                            if (Hotel.Airport != null) Hotel.Airport = null;
                                            if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                            if (Hotel.Hotel != null) Hotel.Hotel = null;
                                        }

                                    }
                                    #endregion

                                    if (Leg.PreflightPassengerLists != null)
                                    {
                                        foreach (PreflightPassengerList PAX in Leg.PreflightPassengerLists)
                                        {

                                            if (PAX.PreflightLeg != null) PAX.PreflightLeg = null;
                                            if (PAX.Passenger != null) PAX.Passenger = null;
                                            if (PAX.UserMaster != null) PAX.UserMaster = null;

                                            #region Hotelcode for Preflight PAX needs to be removed when PAX re design is implemented
                                            if (PAX.PreflightPassengerHotelLists != null)
                                            {
                                                foreach (PreflightPassengerHotelList Hotel in PAX.PreflightPassengerHotelLists)
                                                {
                                                    if (Hotel.Airport != null) Hotel.Airport = null;
                                                    if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                                    if (Hotel.Hotel != null) Hotel.Hotel = null;
                                                }
                                            }
                                            #endregion
                                        }
                                    }

                                    if (Leg.PreflightTransportLists != null)
                                    {
                                        foreach (PreflightTransportList Transport in Leg.PreflightTransportLists)
                                        {
                                            //Transport.LegID = Leg.LegID;
                                            //Transport.PreflightLeg = Leg;
                                            if (Transport.Airport != null) Transport.Airport = null;
                                            if (Transport.UserMaster != null) Transport.UserMaster = null;
                                            if (Transport.Transport != null) Transport.Transport = null;

                                        }
                                    }

                                    //if (Leg.PreflightHotelXrefLists != null)
                                    //{

                                    //    foreach (PreflightHotelXrefList Hotel in Leg.PreflightHotelXrefLists)
                                    //    {
                                    //        //Hotel.LegID = Leg.LegID;
                                    //        //Hotel.PreflightLeg = Leg;
                                    //        if (Hotel.Airport != null) Hotel.Airport = null;
                                    //        if (Hotel.UserMaster != null) Hotel.UserMaster = null;
                                    //        if (Hotel.Hotel != null) Hotel.Hotel = null;

                                    //    }
                                    //}


                                    if (Leg.PreflightFBOLists != null)
                                    {
                                        foreach (PreflightFBOList FBO in Leg.PreflightFBOLists)
                                        {
                                            if (FBO.Airport != null) FBO.Airport = null;
                                            if (FBO.UserMaster != null) FBO.UserMaster = null;
                                            if (FBO.PreflightLeg != null) FBO.PreflightLeg = null;
                                            if (FBO.FBO != null) FBO.FBO = null;
                                        }
                                    }

                                    if (Leg.PreflightCateringDetails != null)
                                    {
                                        foreach (PreflightCateringDetail Catering in Leg.PreflightCateringDetails)
                                        {
                                            if (Catering.Airport != null) Catering.Airport = null;
                                            if (Catering.UserMaster != null) Catering.UserMaster = null;
                                            if (Catering.PreflightLeg != null) Catering.PreflightLeg = null;
                                            if (Catering.Catering != null) Catering.Catering = null;
                                        }
                                    }


                                    if (Leg.PreflightTripOutbounds != null)
                                    {
                                        foreach (PreflightTripOutbound outboundInstruction in Leg.PreflightTripOutbounds)
                                        {
                                            if (outboundInstruction.UserMaster != null) outboundInstruction.UserMaster = null;
                                            if (outboundInstruction.PreflightLeg != null) outboundInstruction.PreflightLeg = null;

                                        }
                                    }

                                    if (Leg.PreflightCheckLists != null)
                                    {
                                        foreach (PreflightCheckList checklist in Leg.PreflightCheckLists)
                                        {
                                            if (checklist.UserMaster != null) checklist.UserMaster = null;
                                            if (checklist.PreflightLeg != null) checklist.PreflightLeg = null;
                                            if (checklist.TripManagerCheckList != null) checklist.TripManagerCheckList = null;
                                        }
                                    }

                                }
                            }
                        }
                        #endregion


                        var ReturnValue = Service.Add(PreflightTrip);
                        if (ReturnValue.ReturnFlag == true)
                        {

                            PreflightTrip = ReturnValue.EntityInfo;
                            PreflightTrip.isTripSaved = true;
                            Session["PreflightException"] = null;
                            var objRetval = Service.GetTripExceptionList(PreflightTrip.TripID);
                            if (objRetval.ReturnFlag)
                            {
                                Session["PreflightException"] = objRetval.EntityList;
                            }
                            else
                                Session["PreflightException"] = null;

                            //Update CorporateRequest from PreFlight
                            Service.UpdateCRFromPreflight(PreflightTrip);

                            SetTripModeToNoChange(ref PreflightTrip);
                            Session["CurrentPreFlightTrip"] = PreflightTrip;
                            PreflightTrip = null;
                            if (Session["AvaialbleCrew"] != null)
                                Session["AvaialbleCrew"] = null;
                            if (Session["tmpTable"] != null)
                                Session["tmpTable"] = null;

                            SetTripMode();

                            // karthik - Making Current Corporate Request Session as null if any.This has been added for the purpose of Request Acknowledge.
                            if (Session["CurrentCorporateRequest"] != null)
                            {
                                Session.Remove("CurrentCorporateRequest");
                            }
                            //RadAjaxManagerMaster.ResponseScripts.Add(@"radalert('Trip has been saved successfully', 330, 110,'Trip Alert');");
                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"SaveCloseAndRebindPage();");
                            //RedirectToMain();
                        }
                        else
                        {

                            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"jAlert('" + ReturnValue.ErrorMessage + " ','" + ModuleNameConstants.Preflight.TripManager + "');");
                        }
                    }
                }
            }
        }

        public void Cancel(PreflightMain PreflightTrip)
        {
            RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('Are you sure want to Cancel?',confirmCancelCallBackFn, 330, 110,null,'Confirmation!');");
        }

        public void Next(string TabName)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TabName))
            {
                if (TabName.ToLower() == "pax")
                    TabName = "PAX";

                int tabindex = 0;
                tabindex = this.PreflightHeader.HeaderTab.Tabs.FindTabByText(TabName).Index;
                for (int i = tabindex + 1; i <= this.PreflightHeader.HeaderTab.Tabs.Count - 1; i++)
                {
                    if (this.PreflightHeader.HeaderTab.Tabs[i].Visible == true && this.PreflightHeader.HeaderTab.Tabs[i].Enabled == true)
                        Response.Redirect(this.PreflightHeader.HeaderTab.Tabs[i].Value);

                }
            }
        }

        public void Delete(Int64 TripID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radconfirm('You are about to delete all data for this TRIP. All data for this TRIP will be lost. Are you sure you want to do this?',confirmDeleteCallBackFn, 330, 110,null,'Confirmation!');");
            }
        }

        private void RemoveTripNavigationProp(ref PreflightMain PreflightTrip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PreflightTrip))
            {
                if (PreflightTrip.Account != null) PreflightTrip.Account = null;
                if (PreflightTrip.Aircraft != null) PreflightTrip.Aircraft = null;
                if (PreflightTrip.Client != null) PreflightTrip.Client = null;
                if (PreflightTrip.Company != null) PreflightTrip.Company = null;
                if (PreflightTrip.Department != null) PreflightTrip.Department = null;
                if (PreflightTrip.DepartmentAuthorization != null) PreflightTrip.DepartmentAuthorization = null;
                if (PreflightTrip.EmergencyContact != null) PreflightTrip.EmergencyContact = null;
                if (PreflightTrip.Fleet != null) PreflightTrip.Fleet = null;
                if (PreflightTrip.Passenger != null) PreflightTrip.Passenger = null;
                if (PreflightTrip.UserMaster != null) PreflightTrip.UserMaster = null;


                if (PreflightTrip.PreflightLegs != null)
                    foreach (PreflightLeg Leg in PreflightTrip.PreflightLegs)
                    {
                        Leg.TripID = PreflightTrip.TripID;
                        Leg.PreflightMain = PreflightTrip;
                        if (Leg.Account != null) Leg.Account = null;
                        if (Leg.Airport != null) Leg.Airport = null;
                        if (Leg.Airport1 != null) Leg.Airport1 = null;
                        if (Leg.Client != null) Leg.Client = null;
                        if (Leg.CrewDutyRule != null) Leg.CrewDutyRule = null;
                        if (Leg.Department != null) Leg.Department = null;
                        if (Leg.DepartmentAuthorization != null) Leg.DepartmentAuthorization = null;
                        if (Leg.FBO != null) Leg.FBO = null;
                        if (Leg.Passenger != null) Leg.Passenger = null;
                        if (Leg.UserMaster != null) Leg.UserMaster = null;

                        if (Leg.PreflightCrewLists != null)
                        {
                            foreach (PreflightCrewList Crew in Leg.PreflightCrewLists)
                            {
                                if (Crew.Crew != null) Crew.Crew = null;
                                if (Crew.UserMaster != null) Crew.UserMaster = null;
                            }
                        }

                    }
            }
        }

        protected string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", timeArray[1]));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[1].Length == 1)
                            result = result + "0";
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }

        protected string RounsETEbasedOnCompanyProfileSettings(decimal ETEVal)
        {
            string ETEStr = string.Empty;

            try
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null &&
                        UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                    {
                        ETEStr = ConvertTenthsToMins(RoundElpTime((double) ETEVal).ToString());
                    }
                    else
                    {
                        ETEStr = Math.Round((decimal) ETEVal, 1).ToString();
                    }

                    ETEStr = Common.Utility.DefaultEteResult(ETEStr);

                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            catch (Exception ex)
            {
                //The exception will be handled, logged and replaced by our custom exception. 
                ProcessErrorMessage(ex, ModuleNameConstants.Preflight.PreflightMain);
            }

            return ETEStr;
        }

        protected void dgLegSummary_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    if (item["PAXNo"].Text != "&nbsp;" && item["SeatAvail"].Text != "&nbsp;")
                        item.ToolTip = "No of Passengers: " + item["PAXNo"].Text + "\n Seats Available: " + item["SeatAvail"].Text;

                    if (item["DepartDate"].Text != "&nbsp;")
                    {
                        DateTime date = (DateTime)DataBinder.Eval(item.DataItem, "DepartDate");
                        if (date != null)
                            item["DepartDate"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date));
                    }
                    if (item["ArrivalDate"].Text != "&nbsp;")
                    {
                        DateTime date1 = (DateTime)DataBinder.Eval(item.DataItem, "ArrivalDate");
                        if (date1 != null)
                            item["ArrivalDate"].Text = Microsoft.Security.Application.Encoder.HtmlEncode(String.Format(CultureInfo.InvariantCulture, "{0:" + RadDatePicker1.DateInput.DateFormat + " HH:mm}", date1)); 
                    }
                    if (Trip != null)
                    {
                        if (Trip.PreflightLegs != null)
                        {
                            TableCell cell = (TableCell)item["DutyErr"];
                            cell.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }
            }
        }

        //Telerik Grid that is used in this UserControl
        public RadGrid TrackerLegs
        {
            get { return this.dgLegs; }
        }

        public RadGrid LegSummary
        {
            get { return this.dgLegSummary; }
        }

        public RadGrid Exceptions
        {
            get { return this.dgException; }
        }

        /// <summary>
        /// Method to Bind Exceptions from Excepton List
        /// </summary>
        /// <param name="exceptionList"></param>
        public void BindException()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["PreflightException"] != null)
                {
                    ExceptionList = (List<PreflightTripException>)Session["PreflightException"];

                    if (ExceptionList.Count > 0)
                    {
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            ExceptionTemplateList = GetExceptionTemplateList();
                            dgException.DataSource = (from Exp in ExceptionList
                                                      join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                      select new
                                                      {
                                                          Severity = ExpTempl.Severity,
                                                          ExceptionDescription = Exp.ExceptionDescription,
                                                          SubModuleID = ExpTempl.SubModuleID,
                                                          SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                                          DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                                      }).OrderBy(x => x.DisplayOrder).ToList();
                            dgException.DataBind();
                            ExpandErrorPanel(true);
                        }
                    }
                    else
                    {
                        ExpandErrorPanel(false);
                    }

                }
            }
        }

        protected void dgException_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (Session["PreflightException"] != null)
                {
                    ExceptionList = (List<PreflightTripException>)Session["PreflightException"];

                    if (ExceptionList.Count > 0)
                    {
                        List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                        using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
                        {
                            ExceptionTemplateList = GetExceptionTemplateList();
                            dgException.DataSource = (from Exp in ExceptionList
                                                      join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                      select new
                                                        {
                                                            Severity = ExpTempl.Severity,
                                                            ExceptionDescription = Exp.ExceptionDescription,
                                                            SubModuleID = ExpTempl.SubModuleID,
                                                            SubModuleGroup = GetSubmoduleModule((long)ExpTempl.SubModuleID),
                                                            DisplayOrder = Exp.DisplayOrder == null ? string.Empty : Exp.DisplayOrder
                                                        }).OrderBy(x => x.DisplayOrder).ToList();
                            ExpandErrorPanel(true);
                        }
                    }
                    else
                    {
                        ExpandErrorPanel(false);
                    }

                }
            }
        }

        protected string GetSubmoduleModule(long SubModuleID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SubModuleID))
            {
                string Retval = string.Empty;
                switch (SubModuleID)
                {

                    case 1: Retval = "Main"; break;
                    case 2: Retval = "Leg"; break;
                    case 3: Retval = "Crew"; break;
                    case 4: Retval = "PAX"; break;
                    case 5: Retval = "Lo"; break;
                    case 6: Retval = "Main"; break;

                }

                return Retval;
            }
        }

        /// <summary>
        /// Exception Grid Item Databound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exception_ItemDataBound(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {

                if (e.Item is GridDataItem)
                {
                    GridDataItem Item = (GridDataItem)e.Item;
                    Image imgSeverity = (Image)Item.FindControl("imgSeverity");
                    string severity = Item.GetDataKeyValue("Severity").ToString().Trim();
                    // Default Image Path
                    string imagePath = "~/App_Themes/Default/images/info.png";
                    if (severity == "1")
                    {
                        imagePath = "~/App_Themes/Default/images/info_red.gif";
                    }
                    else if (severity == "2")
                    {
                        imagePath = "~/App_Themes/Default/images/info_yellow.gif";
                    }
                    imgSeverity.ImageUrl = imagePath;

                }

                //if (Severity.Critical == allErrorList[0].ExceptionSeverity)
                //{
                //    RedirectToMain();
                //    //if (seltab == "Main")
                //    //else
                //    //    Response.Redirect("PreflightLegs.aspx?seltab=Legs");

                //}
            }
        }

        /// <summary>
        /// Method to Expand or Collapse Error Panel
        /// </summary>
        /// <param name="isExpand">Pass Boolean Value</param>
        private void ExpandErrorPanel(bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isExpand))
            {
                RadPanelItem pnlItem = pnlException.FindItemByValue("Exceptions");
                pnlItem.Expanded = isExpand;
            }
        }

        private void ExpandSummaryPanel(bool isExpand)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(isExpand))
            {
                RadPanelItem pnlItem = pnlbarPreflightInfo.FindItemByValue("LegSummary");
                pnlItem.Expanded = isExpand;
            }

        }

        #region Exceptions
        /// <summary>
        /// Method for Validation
        /// </summary>
        /// <returns>Return Exceptions as a List</returns>
        public void ValidationTrip(PreflightService.Group ValidationGroup)
        {
            Trip = (PreflightMain)Session["CurrentPreFlightTrip"];


            using (PreflightServiceClient PrefSvc = new PreflightServiceClient())
            {

                var ObjRetval = PrefSvc.ValidateTripforMandatoryExceptions(Trip);

                if (ObjRetval.ReturnFlag)
                {


                    List<PreflightTripException> OldExceptionList = new List<PreflightTripException>();
                    OldExceptionList = (List<PreflightTripException>)Session["PreflightException"];
                    if (OldExceptionList != null && OldExceptionList.Count > 0)
                    {

                        foreach (PreflightTripException OldMandatoryException in OldExceptionList.ToList())
                        {
                            if (OldMandatoryException.TripExceptionID == 0)
                                OldExceptionList.Remove(OldMandatoryException);
                        }
                    }
                    ExceptionList = ObjRetval.EntityList;

                    if (ExceptionList != null && ExceptionList.Count > 0)
                    {
                        if (OldExceptionList == null)
                            OldExceptionList = new List<PreflightTripException>();
                        OldExceptionList.AddRange(ExceptionList);
                    }


                    Session["PreflightException"] = OldExceptionList;
                }

                List<ExceptionTemplate> TripExceptions = new List<ExceptionTemplate>();
                var ObjRetvalTemplates = PrefSvc.GetExceptionTemplateList();

                if (ObjRetvalTemplates.ReturnFlag)
                {
                    TripExceptions = ObjRetvalTemplates.EntityList;
                }
            }
        }

        public void ValidateLegExists()
        {

            if (Session["CurrentPreFlightTrip"] != null)
            {
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                if (Trip.PreflightLegs == null || Trip.PreflightLegs.Count == 0)
                {
                    Response.Redirect("PreflightLegs.aspx?seltab=Legs", true);
                    //RadAjaxManager.GetCurrent(this.Page).ResponseScripts.Add(@"radalert('Trip has no Legs!!!', 330, 110, ,'" + ModuleNameConstants.Preflight.TripManager + "',alertCallBackFn);");
                }
            }

        }

        public List<ExceptionTemplate> GetExceptionTemplateList()
        {
            List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();

            if (Session["PreflightExceptionTemplateList"] == null)
            {
                using (PreflightServiceClient Service = new PreflightServiceClient())
                {
                    var ObjExTemplRetVal = Service.GetExceptionTemplateList();
                    if (ObjExTemplRetVal.ReturnFlag)
                    {
                        ExceptionTemplateList = ObjExTemplRetVal.EntityList;
                        Session["PreflightExceptionTemplateList"] = ExceptionTemplateList;
                    }
                }
            }
            else
                ExceptionTemplateList = (List<ExceptionTemplate>)Session["PreflightExceptionTemplateList"];
            return ExceptionTemplateList;
        }
        #endregion

        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (e.Argument.ToString() == "ok")
                {
                    Response.Redirect("PostFlightmain.aspx", true);
                }
                else
                {
                    //Label1.Text = "Cancel";
                }


                if (RadAjax_AjaxRequest != null)
                {
                    RadAjax_AjaxRequest(sender, e);
                }
            }
        }

        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            //if (RadAjax_AjaxRequest != null)
            //{
            //    RadAjax_AjaxRequest(sender, e);
            //}
        }

        //TripCalculations
        #region "Preflight Leg Calculations"

        public void UpdateLegDates(int FromLegnum, DateTime ChangedDate)
        {
            if (Session["CurrentPreFlightTrip"] != null)
            {
                Trip = (PreflightMain)Session["CurrentPreFlightTrip"];

                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    List<PreflightLeg> Preflegs = new List<PreflightLeg>();
                    Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                    double dateDifference = 0;
                    foreach (PreflightLeg Leg in Preflegs)
                    {
                        if (Leg.LegNUM >= FromLegnum)
                        {
                            if (Leg.LegNUM == FromLegnum)
                            {
                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    DateTime initialDatetime = DateTime.MinValue;
                                    initialDatetime = ((DateTime)Leg.DepartureDTTMLocal).Date;
                                    TimeSpan t = ChangedDate - initialDatetime;
                                    dateDifference = t.Days;
                                }
                                else
                                {
                                    if (Leg.ArrivalDTTMLocal != null)
                                    {
                                        DateTime initialDatetime = DateTime.MinValue;
                                        initialDatetime = ((DateTime)Leg.ArrivalDTTMLocal).Date;
                                        TimeSpan t = ChangedDate - initialDatetime;
                                        dateDifference = t.Days;
                                    }
                                }
                            }
                            if (dateDifference != 0)
                            {
                                if (Leg.LegID != 0 && Leg.State != TripEntityState.Modified)
                                {
                                    Leg.State = TripEntityState.Modified;
                                }

                                if (Leg.DepartureDTTMLocal != null)
                                {
                                    Leg.DepartureDTTMLocal = ((DateTime)Leg.DepartureDTTMLocal).AddDays(dateDifference);
                                }

                                if (Leg.DepartureGreenwichDTTM != null)
                                {
                                    Leg.DepartureGreenwichDTTM = ((DateTime)Leg.DepartureGreenwichDTTM).AddDays(dateDifference);
                                }

                                if (Leg.HomeDepartureDTTM != null)
                                {
                                    Leg.HomeDepartureDTTM = ((DateTime)Leg.HomeDepartureDTTM).AddDays(dateDifference);
                                }

                                if (Leg.ArrivalDTTMLocal != null)
                                {
                                    Leg.ArrivalDTTMLocal = ((DateTime)Leg.ArrivalDTTMLocal).AddDays(dateDifference);
                                }

                                if (Leg.ArrivalGreenwichDTTM != null)
                                {
                                    Leg.ArrivalGreenwichDTTM = ((DateTime)Leg.ArrivalGreenwichDTTM).AddDays(dateDifference);
                                }

                                if (Leg.HomeArrivalDTTM != null)
                                {
                                    Leg.HomeArrivalDTTM = ((DateTime)Leg.HomeArrivalDTTM).AddDays(dateDifference);
                                }

                            }
                        }
                    }
  Trip.PreflightLegs = Preflegs;
                    Session["CurrentPreFlightTrip"] = Trip;
                }
            }
        }

        public void DoLegCalculationsForTrip()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (CalculationService.CalculationServiceClient objDstsvc = new CalculationService.CalculationServiceClient())
                {

                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                    {
                        List<PreflightLeg> PrefLeg = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        foreach (PreflightLeg Leg in PrefLeg)
                        {
                            #region Depart Airport Details
                            double deplatdeg = 0;
                            double deplatmin = 0;
                            string lcdeplatdir = "N";
                            double deplngdeg = 0;
                            double deplngmin = 0;
                            string lcdeplngdir = "E";
                            bool? depIsDayLightSaving = null;
                            DateTime? depDayLightSavingStartDT = null;
                            DateTime? depDayLightSavingEndDT = null;
                            string depDaylLightSavingStartTM = string.Empty;
                            string depDayLightSavingEndTM = string.Empty;
                            decimal? depOffsetToGMT = null;
                            long lnDepartWindZone = 0;
                            double DepAirportTakeoffBIAS = 0;


                            if (Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                            {
                                FlightPakMasterService.GetAllAirport DepAirport = GetAirport((long)Leg.DepartICAOID);
                                if (DepAirport != null)
                                {
                                    if (DepAirport.LatitudeDegree != null)
                                        deplatdeg = (double)DepAirport.LatitudeDegree.Value;
                                    if (DepAirport.LatitudeMinutes != null)
                                        deplatmin = (double)DepAirport.LatitudeMinutes.Value;
                                    if (DepAirport.LatitudeNorthSouth != null)
                                        lcdeplatdir = DepAirport.LatitudeNorthSouth;

                                    if (DepAirport.LongitudeDegrees != null)
                                        deplngdeg = (double)DepAirport.LongitudeDegrees.Value;
                                    if (DepAirport.LongitudeMinutes != null)
                                        deplngmin = (double)DepAirport.LongitudeMinutes.Value;
                                    if (DepAirport.LongitudeEastWest != null)
                                        lcdeplngdir = DepAirport.LongitudeEastWest;
                                    if (DepAirport.IsDayLightSaving != null)
                                        depIsDayLightSaving = DepAirport.IsDayLightSaving;
                                    if (DepAirport.DayLightSavingStartDT != null)
                                        depDayLightSavingStartDT = DepAirport.DayLightSavingStartDT;
                                    if (DepAirport.DayLightSavingEndDT != null)
                                        depDayLightSavingEndDT = DepAirport.DayLightSavingEndDT;
                                    if (DepAirport.DaylLightSavingStartTM != null)
                                        depDaylLightSavingStartTM = DepAirport.DaylLightSavingStartTM;
                                    if (DepAirport.DayLightSavingEndTM != null)
                                        depDayLightSavingEndTM = DepAirport.DayLightSavingEndTM;
                                    if (DepAirport.OffsetToGMT != null)
                                        depOffsetToGMT = DepAirport.OffsetToGMT;
                                    if (DepAirport.WindZone != null)
                                        lnDepartWindZone = (long)DepAirport.WindZone;
                                    if (DepAirport.TakeoffBIAS != null)
                                        DepAirportTakeoffBIAS = (double)DepAirport.TakeoffBIAS;
                                }
                            }
                            #endregion

                            #region Arrival Airport Details
                            double arrlatdeg = 0;
                            double arrlatmin = 0;
                            string lcArrLatDir = "N";
                            double arrlngdeg = 0;
                            double arrlngmin = 0;
                            string lcarrlngdir = "E";
                            bool? arrIsDayLightSaving = null;
                            DateTime? arrDayLightSavingStartDT = null;
                            DateTime? arrDayLightSavingEndDT = null;
                            string arrDaylLightSavingStartTM = string.Empty;
                            string arrDayLightSavingEndTM = string.Empty;
                            decimal? arrOffsetToGMT = null;
                            long lnArrivWindZone = 0;
                            double ArrAirportLandingBIAS = 0;

                            if (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                            {
                                FlightPakMasterService.GetAllAirport ArrAirport = GetAirport((long)Leg.ArriveICAOID);
                                if (ArrAirport != null)
                                {
                                    if (ArrAirport.LatitudeDegree != null)
                                        arrlatdeg = (double)ArrAirport.LatitudeDegree.Value;
                                    if (ArrAirport.LatitudeMinutes != null)
                                        arrlatmin = (double)ArrAirport.LatitudeMinutes.Value;
                                    if (ArrAirport.LatitudeNorthSouth != null)
                                        lcArrLatDir = ArrAirport.LatitudeNorthSouth;

                                    if (ArrAirport.LongitudeDegrees != null)
                                        arrlngdeg = (double)ArrAirport.LongitudeDegrees.Value;
                                    if (ArrAirport.LongitudeMinutes != null)
                                        arrlngmin = (double)ArrAirport.LongitudeMinutes.Value;
                                    if (ArrAirport.LongitudeEastWest != null)
                                        lcarrlngdir = ArrAirport.LongitudeEastWest;
                                    if (ArrAirport.IsDayLightSaving != null)
                                        arrIsDayLightSaving = ArrAirport.IsDayLightSaving;
                                    if (ArrAirport.DayLightSavingStartDT != null)
                                        arrDayLightSavingStartDT = ArrAirport.DayLightSavingStartDT;
                                    if (ArrAirport.DayLightSavingEndDT != null)
                                        arrDayLightSavingEndDT = ArrAirport.DayLightSavingEndDT;
                                    if (ArrAirport.DaylLightSavingStartTM != null)
                                        arrDaylLightSavingStartTM = ArrAirport.DaylLightSavingStartTM;
                                    if (ArrAirport.DayLightSavingEndTM != null)
                                        arrDayLightSavingEndTM = ArrAirport.DayLightSavingEndTM;
                                    if (ArrAirport.OffsetToGMT != null)
                                        arrOffsetToGMT = ArrAirport.OffsetToGMT;
                                    if (ArrAirport.WindZone != null)
                                        lnArrivWindZone = (long)ArrAirport.WindZone;
                                    if (ArrAirport.LandingBIAS != null)
                                        ArrAirportLandingBIAS = (double)ArrAirport.LandingBIAS;
                                }
                            }
                            #endregion

                            #region Homebase Airport Details

                            bool? homeIsDayLightSaving = null;
                            DateTime? homeDayLightSavingStartDT = null;
                            DateTime? homeDayLightSavingEndDT = null;
                            string homeDaylLightSavingStartTM = string.Empty;
                            string homeDayLightSavingEndTM = string.Empty;
                            decimal? homeOffsetToGMT = null;


                            if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                            {
                                FlightPakMasterService.GetAllAirport HomebaseAirport = GetAirport((long)Trip.HomeBaseAirportID);
                                if (HomebaseAirport != null)
                                {

                                    if (HomebaseAirport.IsDayLightSaving != null)
                                        homeIsDayLightSaving = HomebaseAirport.IsDayLightSaving;
                                    if (HomebaseAirport.DayLightSavingStartDT != null)
                                        homeDayLightSavingStartDT = HomebaseAirport.DayLightSavingStartDT;
                                    if (HomebaseAirport.DayLightSavingEndDT != null)
                                        homeDayLightSavingEndDT = HomebaseAirport.DayLightSavingEndDT;
                                    if (HomebaseAirport.DaylLightSavingStartTM != null)
                                        homeDaylLightSavingStartTM = HomebaseAirport.DaylLightSavingStartTM;
                                    if (HomebaseAirport.DayLightSavingEndTM != null)
                                        homeDayLightSavingEndTM = HomebaseAirport.DayLightSavingEndTM;
                                    if (HomebaseAirport.OffsetToGMT != null)
                                        homeOffsetToGMT = HomebaseAirport.OffsetToGMT;
                                }
                            }

                            #endregion


                            string PowerSetting = "1";
                            if (Leg.PowerSetting != null)
                                PowerSetting = Leg.PowerSetting;

                            Int32 _windReliability = 0;
                            if (Leg.WindReliability != null)
                                _windReliability = Leg.WindReliability.Value;

                            #region Aircraft Details


                            double PowerSettings1TakeOffBias = 0;
                            double PowerSettings1LandingBias = 0;
                            double PowerSettings1TrueAirSpeed = 0;
                            double PowerSettings2TakeOffBias = 0;
                            double PowerSettings2LandingBias = 0;
                            double PowerSettings2TrueAirSpeed = 0;
                            double PowerSettings3TakeOffBias = 0;
                            double PowerSettings3LandingBias = 0;
                            double PowerSettings3TrueAirSpeed = 0;
                            string IsFixedRotary = string.Empty;
                            double aircraftWindAltitude = 0;
                            double ChargeRate = 0;
                            string ChargeUnit = string.Empty;
                            if (Trip.AircraftID != null && Trip.AircraftID > 0)
                            {
                                FlightPakMasterService.Aircraft TripAircraft = GetAircraft((long)Trip.AircraftID);
                                if (TripAircraft != null)
                                {
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings1TakeOffBias = (double)TripAircraft.PowerSettings1TakeOffBias;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings1LandingBias = (double)TripAircraft.PowerSettings1LandingBias;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings1TrueAirSpeed = (double)TripAircraft.PowerSettings1TrueAirSpeed;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings2TakeOffBias = (double)TripAircraft.PowerSettings2TakeOffBias;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings2LandingBias = (double)TripAircraft.PowerSettings2LandingBias;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings2TrueAirSpeed = (double)TripAircraft.PowerSettings2TrueAirSpeed;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings3TakeOffBias = (double)TripAircraft.PowerSettings3TakeOffBias;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings3LandingBias = (double)TripAircraft.PowerSettings3LandingBias;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        PowerSettings3TrueAirSpeed = (double)TripAircraft.PowerSettings3TrueAirSpeed;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        IsFixedRotary = TripAircraft.IsFixedRotary;
                                    if (TripAircraft.PowerSettings1TakeOffBias != null)
                                        aircraftWindAltitude = (double)TripAircraft.WindAltitude;
                                    if (TripAircraft.ChargeRate != null)
                                        ChargeRate = (double)TripAircraft.ChargeRate;
                                    if (TripAircraft.ChargeUnit != null)
                                        ChargeUnit = TripAircraft.ChargeUnit;
                                    if (TripAircraft.PowerSetting != null)
                                        PowerSetting = TripAircraft.PowerSetting;
                                }
                            }
                            #endregion


                            #region Miles
                            double Miles = 0;
                            if (Leg.DepartICAOID > 0 && Leg.ArriveICAOID > 0)
                            {
                                Miles = CalculateDistance(deplatdeg, deplatmin, lcdeplatdir, deplngdeg, deplngmin, lcdeplngdir, arrlatdeg, arrlatmin, lcArrLatDir, arrlngdeg, arrlngmin, lcarrlngdir);
                                //objDstsvc.GetDistance((long)Leg.DepartICAOID, (long)Leg.ArriveICAOID);
                            }
                            Leg.Distance = (decimal)Miles;
                            #endregion
                            #region Bias

                            List<double> BiasList = new List<double>();

                            if (Leg.PowerSetting == null)
                                Leg.PowerSetting = "1";

                            //if (Trip.AircraftID != null && Trip.AircraftID != 0)
                            //{
                            //    FlightPakMasterService.Aircraft TripAircraft = new FlightPakMasterService.Aircraft();
                            //    TripAircraft = GetAircraft((long)Trip.AircraftID);
                            //    Leg.PowerSetting = TripAircraft.PowerSetting != null ? TripAircraft.PowerSetting : "1";

                            //}

                            if (
                                (Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                                &&
                                (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                                &&
                                (Trip.AircraftID != null && Trip.AircraftID > 0)
                                )
                            {
                                BiasList = CalcBiasTas(PowerSetting, PowerSettings1TakeOffBias, PowerSettings1LandingBias, PowerSettings1TrueAirSpeed, PowerSettings2TakeOffBias, PowerSettings2LandingBias, PowerSettings2TrueAirSpeed, PowerSettings3TakeOffBias, PowerSettings3LandingBias, PowerSettings3TrueAirSpeed, DepAirportTakeoffBIAS, ArrAirportLandingBIAS);
                                //objDstsvc.CalcBiasTas((long)Leg.DepartICAOID, (long)Leg.ArriveICAOID, (long)Trip.AircraftID, Leg.PowerSetting);
                                if (BiasList != null)
                                {
                                    // lnToBias, lnLndBias, lnTas
                                    Leg.TakeoffBIAS = (decimal)BiasList[0];
                                    Leg.LandingBias = (decimal)BiasList[1];
                                    Leg.TrueAirSpeed = (decimal)BiasList[2];
                                }
                            }
                            else
                            {
                                Leg.TakeoffBIAS = 0.0M;
                                Leg.LandingBias = 0.0M; ;
                                Leg.TrueAirSpeed = 0.0M;
                            }
                            #endregion
                            #region "Wind"
                            Leg.WindsBoeingTable = 0.0M;
                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;
                                //Function Wind Calculation
                                double Wind = 0;
                                if (
                                (Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                                &&
                                (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                                &&
                                (Trip.AircraftID != null && Trip.AircraftID > 0)
                                )
                                {
                                    Wind = objDstsvc.GetWindBasedOnParameter(aircraftWindAltitude, _windReliability, GetQtr(dt).ToString(),
            deplatdeg, deplatmin, lcdeplatdir,
            deplngdeg, deplngmin, lcdeplngdir, lnDepartWindZone,
            arrlatdeg, arrlatmin, lcArrLatDir,
            arrlngdeg, arrlngmin, lcarrlngdir, lnArrivWindZone);
                                    //objDstsvc.GetWind((long)Leg.DepartICAOID, (long)Leg.ArriveICAOID, Convert.ToInt32(Leg.WindReliability), (long)Trip.AircraftID, GetQtr(dt).ToString());
                                    Leg.WindsBoeingTable = (decimal)Wind;
                                }
                            }
                            #endregion
                            #region "ETE"
                            double ETE = 0;
                            // lnToBias, lnLndBias, lnTas

                            if (Leg.LegNUM == 1)
                            {
                                if (Leg.DepartureDTTMLocal == null && Trip.EstDepartureDT != null)
                                {
                                    Leg.DepartureDTTMLocal = Trip.EstDepartureDT;

                                    if (Leg.DepartICAOID != null)
                                    {
                                        Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Leg.DepartICAOID.Value, Leg.DepartureDTTMLocal.Value, true, false);
                                    }
                                }
                            }
                            else
                            {
                                #region AutoCalulateLegtimes
                                PreflightLeg PrevLeg = Trip.PreflightLegs.Where(x => x.LegNUM == (Leg.LegNUM.Value - 1) && x.IsDeleted == false).SingleOrDefault();

                                if (PrevLeg != null)
                                {
                                    if (PrevLeg.ArrivalDTTMLocal != null)
                                    {
                                        DateTime dt = (DateTime)PrevLeg.ArrivalDTTMLocal;

                                        double hourtoadd = 0;
                                        if (PrevLeg.CheckGroup != null && PrevLeg.CheckGroup == "DOM")
                                            hourtoadd = UserPrincipal.Identity._fpSettings._GroundTM == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTM;
                                        else
                                            hourtoadd = UserPrincipal.Identity._fpSettings._GroundTMIntl == null ? 0 : (double)UserPrincipal.Identity._fpSettings._GroundTMIntl;

                                        dt = dt.AddHours(hourtoadd);

                                        Leg.DepartureDTTMLocal = dt;

                                        if (Leg.DepartICAOID != null)
                                        {
                                            Leg.DepartureGreenwichDTTM = objDstsvc.GetGMT(Leg.DepartICAOID.Value, Leg.DepartureDTTMLocal.Value, true, false);
                                        }
                                    }
                                    PrevLeg = null;
                                }
                                #endregion
                            }


                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime dt;
                                dt = (DateTime)Leg.DepartureDTTMLocal;


                                ETE = GetIcaoEte((double)Leg.WindsBoeingTable, (double)Leg.LandingBias, (double)Leg.TrueAirSpeed, (double)Leg.TakeoffBIAS, (double)Leg.Distance, dt, IsFixedRotary);
                                //objDstsvc.GetIcaoEte((double)Leg.WindsBoeingTable, (double)Leg.LandingBias, (double)Leg.TrueAirSpeed, (double)Leg.TakeoffBIAS, (double)Leg.Distance, dt);

                                ETE = RoundElpTime(ETE);
                            }

                            Leg.ElapseTM = (decimal)ETE;

                            #endregion
                            #region CalculateDatetime

                            double x10, x11;
                            // int DeptUTCHrst, DeptUTCMtstr, Hrst, Mtstr, ArrivalUTChrs, ArrivalUTCmts;
                            // DateTime Arrivaldt;
                            //try
                            //{


                            if (Leg.DepartureDTTMLocal != null)
                            {
                                DateTime ChangedDate;

                                ChangedDate = (DateTime)Leg.DepartureDTTMLocal;

                                if (ChangedDate != null)
                                {


                                    DateTime EstDepartDate = (DateTime)ChangedDate;
                                    //if (Trip.EstDepartureDT != null)
                                    //{
                                    //    EstDepartDate = (DateTime)Trip.EstDepartureDT;
                                    //}



                                    try
                                    {

                                        //(Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)&&
                                        if ((Leg.DepartICAOID != null && Leg.DepartICAOID > 0)
                                            && (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0)
                                            )
                                        {

                                            if (!string.IsNullOrEmpty(Leg.ElapseTM.ToString()))
                                            {
                                                string ETEstr = "0";

                                                ETEstr = Leg.ElapseTM.ToString();


                                                double tripleg_elp_time = RoundElpTime(Convert.ToDouble(ETEstr));

                                                x10 = (tripleg_elp_time * 60 * 60);
                                                x11 = (tripleg_elp_time * 60 * 60);
                                            }
                                            else
                                            {
                                                x10 = 0;
                                                x11 = 0;
                                            }
                                            DateTime DeptUTCdt;
                                            DateTime ArrUTCdt;

                                            DeptUTCdt = (DateTime)Leg.DepartureGreenwichDTTM;
                                            ArrUTCdt = DeptUTCdt;
                                            ArrUTCdt = ArrUTCdt.AddSeconds(x10);
                                            DateTime ldLocDep = DateTime.MinValue;
                                            DateTime ldLocArr = DateTime.MinValue;
                                            DateTime ldHomDep = DateTime.MinValue;
                                            DateTime ldHomArr = DateTime.MinValue;
                                            ldLocDep = GetGMT(DeptUTCdt, false, depIsDayLightSaving, depDayLightSavingStartDT, depDayLightSavingEndDT, depDaylLightSavingStartTM, depDayLightSavingEndTM, depOffsetToGMT, false);
                                            //objDstsvc.GetGMT((long)Leg.DepartICAOID, DeptUTCdt, false, false);



                                            if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                ldHomDep = GetGMT(DeptUTCdt, false, homeIsDayLightSaving, homeDayLightSavingStartDT, homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM, homeOffsetToGMT, false);
                                            //objDstsvc.GetGMT((long)Trip.HomeBaseAirportID, DeptUTCdt, false, false);


                                            ldLocArr = GetGMT(ArrUTCdt, false, arrIsDayLightSaving, arrDayLightSavingStartDT, arrDayLightSavingEndDT, arrDaylLightSavingStartTM, arrDayLightSavingEndTM, arrOffsetToGMT, false);
                                            //objDstsvc.GetGMT((long)Leg.ArriveICAOID, ArrUTCdt, false, false);

                                            if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                ldHomArr = GetGMT(ArrUTCdt, false, homeIsDayLightSaving, homeDayLightSavingStartDT, homeDayLightSavingEndDT, homeDaylLightSavingStartTM, homeDayLightSavingEndTM, homeOffsetToGMT, false);
                                            //objDstsvc.GetGMT((long)Trip.HomeBaseAirportID, ArrUTCdt, false, false);

                                            //DateTime ltBlank = EstDepartDate;

                                            if (Leg.DepartICAOID > 0)
                                            {

                                                Leg.DepartureDTTMLocal = ldLocDep;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = ldHomDep;

                                                Leg.DepartureGreenwichDTTM = DeptUTCdt;

                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;

                                                Leg.DepartureDTTMLocal = Localdt;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeDepartureDTTM = Localdt;
                                                Leg.DepartureGreenwichDTTM = Localdt;

                                            }

                                            if (Leg.ArriveICAOID > 0)
                                            {
                                                // DateTime dt = ldLocArr;

                                                Leg.ArrivalDTTMLocal = ldLocArr;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = ldHomArr;
                                                Leg.ArrivalGreenwichDTTM = ArrUTCdt;
                                            }
                                            else
                                            {
                                                DateTime Localdt = EstDepartDate;

                                                Leg.ArrivalDTTMLocal = Localdt;
                                                if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                                    Leg.HomeArrivalDTTM = Localdt;
                                                Leg.ArrivalGreenwichDTTM = Localdt;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //Manually Handled
                                        if (Leg.ArrivalDTTMLocal == null)
                                        {
                                            Leg.ArrivalDTTMLocal = EstDepartDate;
                                        }
                                        if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                            if (Leg.HomeArrivalDTTM == null)
                                            {
                                                Leg.HomeArrivalDTTM = EstDepartDate;
                                            }

                                        if (Leg.ArrivalGreenwichDTTM == null)
                                        {
                                            Leg.ArrivalGreenwichDTTM = EstDepartDate;
                                        }

                                        if (Leg.DepartureDTTMLocal == null)
                                        {
                                            Leg.DepartureDTTMLocal = EstDepartDate;
                                        }

                                        if (Leg.DepartureGreenwichDTTM == null)
                                        {
                                            Leg.DepartureGreenwichDTTM = EstDepartDate;
                                        }
                                        if (Trip.HomeBaseAirportID != null && Trip.HomeBaseAirportID > 0)
                                            if (Leg.HomeDepartureDTTM == null)
                                            {
                                                Leg.HomeDepartureDTTM = EstDepartDate;
                                            }
                                    }

                                }
                            }
                            #endregion
                        }

                        CalculateFARAndFlightCost(ref Trip);
                    }

                    Session["CurrentPreFlightTrip"] = Trip;
                }
            }
        }

        public void CalculateFARAndFlightCost(ref PreflightMain Trip)
        {
            #region "PreflightFARRule"


            double lnLeg_Num, lnEndLegNum, lnOrigLeg_Num, lnDuty_Hrs, lnRest_Hrs, lnFlt_Hrs, lnMaxDuty, lnMaxFlt, lnMinRest, lnRestMulti, lnRestPlus, lnOldDuty_Hrs, lnDayBeg, lnDayEnd,
               lnNeededRest, lnWorkArea;

            lnEndLegNum = lnNeededRest = lnWorkArea = lnLeg_Num = lnOrigLeg_Num = lnDuty_Hrs = lnRest_Hrs = lnFlt_Hrs = lnMaxDuty = lnMaxFlt = lnMinRest = lnRestMulti = lnRestPlus = lnOldDuty_Hrs = lnDayBeg = lnDayEnd = 0;

            bool llRProblem, llFProblem, llDutyBegin, llDProblem;
            llRProblem = llFProblem = llDutyBegin = llDProblem = false;

            DateTime ltGmtDep, ltGmtArr, llDutyend;
            Int64 lnDuty_RulesID = 0;
            string lcDuty_Rules = string.Empty;


            if (Trip.PreflightLegs != null)
            {
                List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                lnOrigLeg_Num = (double)Preflegs[0].LegNUM;
                lnEndLegNum = (double)Preflegs[Preflegs.Count - 1].LegNUM;

                lnDuty_Hrs = 0;
                lnRest_Hrs = 0;
                lnFlt_Hrs = 0;
                //if ((DateTime)Preflegs[0].DepartureGreenwichDTTM != null)
                //{
                //    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                //}
                //else
                //{
                //    ltGmtDep = DateTime.MinValue;
                //}

                if (Preflegs[0].DepartureGreenwichDTTM != null)
                {
                    ltGmtDep = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;
                    ltGmtArr = (DateTime)Preflegs[0].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[0].DepartureGreenwichDTTM;

                    llDutyBegin = true;
                    double lnOldDuty_hrs = -1;

                    int legcounter = 0;
                    foreach (PreflightLeg Leg in Preflegs)
                    {
                        if (Leg.ArrivalGreenwichDTTM != null && Leg.DepartureGreenwichDTTM != null)
                        {

                            double dbElapseTime = 0.0;
                            long dbCrewDutyRulesID = 0;

                            if (Leg.ElapseTM != null)
                                dbElapseTime = (double)Leg.ElapseTM;

                            //if (Leg.CrewDutyRulesID != null)
                            //{

                            //    dbCrewDutyRulesID = (Int64)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID;
                            //}
                            //if (dbCrewDutyRulesID != 0)
                            //{
                            double lnOverRide = 0;
                            double lnDutyLeg_Num = 0;

                            bool llDutyEnd = false;
                            string FARNum = "";

                            if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID != null)
                                lnDuty_RulesID = (Int64)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyRulesID;
                            else
                                lnDuty_RulesID = 0;
                            if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd == null)
                                llDutyEnd = false;
                            else
                                llDutyEnd = (bool)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd;

                            //if it is last leg then  llDutyEnd = true;
                            if (lnEndLegNum == Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM)
                                llDutyEnd = true;


                            lnOverRide = (double)(Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].OverrideValue == null ? 0 : Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].OverrideValue);



                            lnDutyLeg_Num = (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM;

                            lnDayBeg = 0;
                            lnDayEnd = 0;
                            lnMaxDuty = 0;
                            lnMaxFlt = 0;
                            lnMinRest = 8;
                            lnRestMulti = 0;
                            lnRestPlus = 0;
                            if (lnDuty_RulesID > 0)
                            {
                                using (MasterCatalogServiceClient objectDstsvc = new MasterCatalogServiceClient())
                                {
                                    var objRetVal = objectDstsvc.GetCrewDutyRulesWithFilters(lnDuty_RulesID, string.Empty, false);
                                    if (objRetVal.ReturnFlag)
                                    {
                                        List<FlightPakMasterService.CrewDutyRules> CrewDtyRule = objRetVal.EntityList;
                                        //(from CrewDutyRl in objRetVal.EntityList where CrewDutyRl.CrewDutyRulesID == lnDuty_RulesID select CrewDutyRl).ToList();

                                        if (CrewDtyRule != null && CrewDtyRule.Count > 0)
                                        {
                                            FARNum = CrewDtyRule[0].FedAviatRegNum;
                                            lnDayBeg = lnOverRide == 0 ? Convert.ToDouble(CrewDtyRule[0].DutyDayBeingTM == null ? 0 : CrewDtyRule[0].DutyDayBeingTM) : lnOverRide;
                                            lnDayEnd = Convert.ToDouble(CrewDtyRule[0].DutyDayEndTM == null ? 0 : CrewDtyRule[0].DutyDayEndTM);
                                            lnMaxDuty = Convert.ToDouble(CrewDtyRule[0].MaximumDutyHrs == null ? 0 : CrewDtyRule[0].MaximumDutyHrs);
                                            lnMaxFlt = Convert.ToDouble(CrewDtyRule[0].MaximumFlightHrs == null ? 0 : CrewDtyRule[0].MaximumFlightHrs);
                                            lnMinRest = Convert.ToDouble(CrewDtyRule[0].MinimumRestHrs == null ? 0 : CrewDtyRule[0].MinimumRestHrs);
                                            lnRestMulti = Convert.ToDouble(CrewDtyRule[0].RestMultiple == null ? 0 : CrewDtyRule[0].RestMultiple);
                                            lnRestPlus = Convert.ToDouble(CrewDtyRule[0].RestMultipleHrs == null ? 0 : CrewDtyRule[0].RestMultipleHrs);
                                        }

                                    }
                                }
                            }
                            TimeSpan? Hoursdiff = (DateTime)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DepartureGreenwichDTTM - ltGmtArr;
                            if (Leg.ElapseTM != 0)
                            {
                                lnFlt_Hrs = lnFlt_Hrs + (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ElapseTM;
                                lnDuty_Hrs = lnDuty_Hrs + (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ElapseTM + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                            }
                            else
                            {

                                lnDuty_Hrs = lnDuty_Hrs + Hoursdiff.Value.TotalHours;  //(ROUND(ROUND((tripleg.gmtdep - ltGmtArr),0)/3600,1))
                            }

                            llRProblem = false;

                            if (llDutyBegin && lnOldDuty_hrs != -1)
                            {

                                lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;

                                if (lnRestMulti > 0)
                                {

                                    //Here is the Multiple and Plus hours usage
                                    lnNeededRest = ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus > lnMinRest + lnRestPlus) ? ((lnOldDuty_hrs * lnRestMulti) + lnRestPlus) : lnMinRest + lnRestPlus;
                                    if (lnRest_Hrs < lnNeededRest)
                                    {

                                        llRProblem = true;
                                    }
                                }
                                else
                                {
                                    if (lnRest_Hrs < (lnMinRest + lnRestPlus))
                                    {

                                        llRProblem = true;
                                    }

                                }
                            }
                            else
                            {
                                if (llDutyBegin)
                                {
                                    lnDuty_Hrs = lnDuty_Hrs + lnDayBeg;
                                }
                            }

                            if (llDutyEnd)
                            {
                                lnDuty_Hrs = lnDuty_Hrs + lnDayEnd;
                            }

                            string lcCdAlert = "";

                            if (lnFlt_Hrs > lnMaxFlt) //if flight hours is greater than tripleg.maxflt
                                lcCdAlert = "F";// Flight time violation and F is stored in tripleg.cdalert as a Flight time error
                            else
                                lcCdAlert = "";



                            if (lnDuty_Hrs > lnMaxDuty) //If Duty Hours is greater than Maximum Duty
                                lcCdAlert = lcCdAlert + "D"; //Duty type violation and D is stored in tripleg.cdalert as a Duty time error

                            // ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;

                            if (llRProblem)
                                lcCdAlert = lcCdAlert + "R";// Rest time violation and R is stored in tripleg.cdalert as a Rest time error 

                            if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegID != 0 && Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].State != TripEntityState.Deleted)
                                Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].State = TripEntityState.Modified;
                            Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].FlightHours = (decimal)lnFlt_Hrs;
                            Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].DutyHours = (decimal)lnDuty_Hrs;
                            Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].RestHours = (decimal)lnRest_Hrs;
                            Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].CrewDutyAlert = lcCdAlert;
                            //Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd = llDutyEnd;


                            lnLeg_Num = (double)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].LegNUM;

                            llDutyEnd = false;
                            if (Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd != null)
                                llDutyEnd = (bool)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd;

                            ltGmtArr = (DateTime)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].ArrivalGreenwichDTTM;

                            if ((bool)Trip.PreflightLegs[Trip.PreflightLegs.IndexOf(Leg)].IsDutyEnd)
                            {
                                llDutyBegin = true;
                                lnFlt_Hrs = 0;
                            }
                            else
                            {
                                llDutyBegin = false;
                            }

                            lnOldDuty_hrs = lnDuty_Hrs;
                            //check next leg if available do the below steps
                            legcounter++;
                            if (legcounter < Preflegs.Count)
                            {
                                if (llDutyEnd)
                                {
                                    lnDuty_Hrs = 0;

                                    if (Preflegs[legcounter].DepartureGreenwichDTTM != null)
                                    {
                                        TimeSpan? hrsdiff = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM - ltGmtArr;

                                        lnRest_Hrs = ((hrsdiff.Value.Days * 24) + hrsdiff.Value.Hours + (double)((double)hrsdiff.Value.Minutes / 60)) - lnDayBeg - lnDayEnd;
                                        //ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                        ltGmtArr = (DateTime)Preflegs[legcounter].DepartureGreenwichDTTM;
                                    }
                                }
                                else
                                {
                                    lnRest_Hrs = 0;
                                }
                            }
                            //check next leg if available do the below steps

                            //}
                        }

                    }
                }
            }
            //}
            //catch (Exception)
            //{
            //    lbTotalDuty.Text = "0.0";
            //    lbTotalFlight.Text = "0.0";
            //    lbRest.Text = "0.0";
            //}

            #endregion

            #region FlightCost
            if (Trip.PreflightLegs != null)
            {
                decimal lnAircraftChrg_Rate = 0.0M;
                string lnAircraftChrg_Unit = string.Empty;


                if (Trip.AircraftID != null && Trip.AircraftID > 0)
                {


                    Int64 AircraftID = Convert.ToInt64(Trip.AircraftID);
                    FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                    if (Aircraft != null)
                    {
                        lnAircraftChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                        lnAircraftChrg_Unit = Aircraft.ChargeUnit;
                    }
                }

                List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                decimal TotalCost = 0.0M;

                foreach (PreflightLeg Leg in Preflegs)
                {

                    decimal lnChrg_Rate = 0.0M;
                    decimal lnCost = 0.0M;
                    string lcChrg_Unit = string.Empty;

                    if (Trip.FleetID != null && Trip.FleetID != 0)
                    {
                        FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient MasterClient = new FlightPak.Web.FlightPakMasterService.MasterCatalogServiceClient();
                        var objRetval = MasterClient.GetFleetChargeRateList((long)Trip.FleetID);
                        if (Leg.DepartureDTTMLocal != null)
                        {
                            if (objRetval.ReturnFlag)
                            {
                                Int64 _fleetId = Convert.ToInt64(Trip.FleetID);
                                List<FlightPakMasterService.FleetChargeRate> FleetchargeRate = (from Fleetchrrate in objRetval.EntityList
                                                                                                where (Fleetchrrate.BeginRateDT <= Leg.ArrivalDTTMLocal
                                                                                                && Fleetchrrate.EndRateDT >= Leg.DepartureDTTMLocal)
                                                                                                where Fleetchrrate.FleetID == _fleetId
                                                                                                select Fleetchrrate).ToList();
                                if (FleetchargeRate.Count > 0)
                                {
                                    lnChrg_Rate = FleetchargeRate[0].ChargeRate == null ? 0.0M : (decimal)FleetchargeRate[0].ChargeRate;
                                    lcChrg_Unit = FleetchargeRate[0].ChargeUnit;
                                }
                            }
                        }
                    }

                    if (lnChrg_Rate == 0.0M)
                    {
                        if (Trip.AircraftID != null && Trip.AircraftID > 0)
                        {
                            //Int64 AircraftID = Convert.ToInt64(Trip.AircraftID);
                            //FlightPakMasterService.Aircraft Aircraft = GetAircraft(AircraftID);
                            //if (Aircraft != null)
                            //{
                            //    lnChrg_Rate = Aircraft.ChargeRate == null ? 0.0M : (decimal)Aircraft.ChargeRate;
                            //    lcChrg_Unit = Aircraft.ChargeUnit;
                            //}

                            lnChrg_Rate = lnAircraftChrg_Rate;
                            lcChrg_Unit = lnAircraftChrg_Unit;
                        }


                    }

                    switch (lcChrg_Unit)
                    {
                        case "N":
                            {
                                lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate;
                                break;
                            }
                        case "K":
                            {
                                lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.852M;
                                break;
                            }
                        case "S":
                            {
                                lnCost = (Leg.Distance == null ? 0 : (decimal)Leg.Distance) * lnChrg_Rate * 1.1508M;
                                break;
                            }
                        case "H":
                            {
                                lnCost = (Leg.ElapseTM == null ? 0 : (decimal)Leg.ElapseTM) * lnChrg_Rate;
                                break;
                            }
                        default: lnCost = 0.0M; break;

                    }
                    Leg.FlightCost = lnCost;
                    TotalCost = TotalCost + lnCost;
                }

                Trip.FlightCost = TotalCost;
            }
            #endregion
        }

        public void DoLegCalculationsForDomIntl()
        {
            using (FlightPakMasterService.MasterCatalogServiceClient FleetService = new FlightPakMasterService.MasterCatalogServiceClient())
            {
                FlightPakMasterService.GetCompanyWithFilters company = new GetCompanyWithFilters();
                List<FlightPakMasterService.GetFleet> GetFleetList = new List<GetFleet>();
                List<FlightPakMasterService.GetAllAirport> getAllAirportList = new List<GetAllAirport>();
                var fleetretval = FleetService.GetFleet();
                if (fleetretval.ReturnFlag)
                {
                    Trip = (PreflightMain)Session["CurrentPreFlightTrip"];
                    if (Trip != null && Trip.HomebaseID != null)
                    {
                        company = GetCompany((long)Trip.HomebaseID);
                        if (company != null)
                        {
                            FlightPakMasterService.GetAllAirport companyairport = new GetAllAirport();
                            companyairport = GetAirport((long)company.HomebaseAirportID);
                            if (companyairport != null)
                                MasterhdnCountryID.Value = companyairport.CountryID.ToString();
                        }

                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count >= 0)
                        {
                            List<PreflightLeg> PrefLeg = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                            foreach (PreflightLeg Leg in PrefLeg)
                            {

                                if (Leg.DepartICAOID != 0 && Leg.ArriveICAOID != 0 && Leg.DepartICAOID != null && Leg.ArriveICAOID != null)
                                {
                                    using (MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                                    {
                                        var objDepRetVal = objDstsvc.GetAirportByAirportID((long)Leg.DepartICAOID).EntityList;
                                        //List<FlightPakMasterService.GetAllAirport> getDeptAirportList = new List<GetAllAirport>();

                                        var objArrRetVal = objDstsvc.GetAirportByAirportID((long)Leg.ArriveICAOID).EntityList;
                                        //List<FlightPakMasterService.GetAllAirport> getArrivalAirportList = new List<GetAllAirport>();

                                        Leg.CheckGroup = "DOM";
                                        if (!string.IsNullOrEmpty(MasterhdnCountryID.Value) && MasterhdnCountryID.Value != "0")
                                        {

                                            if (objDepRetVal.Count > 0 && objArrRetVal.Count > 0)
                                            {
                                                if (objDepRetVal[0].CountryID == null || objArrRetVal[0].CountryID == null)
                                                {
                                                    Leg.CheckGroup = "DOM";
                                                }
                                                else
                                                {
                                                    if (objDepRetVal[0].CountryID != null || objArrRetVal[0].CountryID != null)
                                                    {
                                                        if (objDepRetVal[0].CountryID != null)
                                                        {
                                                            if (MasterhdnCountryID.Value != objDepRetVal[0].CountryID.ToString())
                                                            {
                                                                Leg.CheckGroup = "INT";
                                                            }
                                                        }

                                                        if (objArrRetVal[0].CountryID != null)
                                                        {
                                                            if (MasterhdnCountryID.Value != objArrRetVal[0].CountryID.ToString())
                                                            {
                                                                Leg.CheckGroup = "INT";
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }

                                    }
                                }
                            }
                        }
                    }
                }


            }
            Session["CurrentPreFlightTrip"] = Trip;
        }
        protected double RoundElpTime(double lnElp_Time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lnElp_Time))
            {
                if (UserPrincipal.Identity._fpSettings._TimeDisplayTenMin != null && UserPrincipal.Identity._fpSettings._TimeDisplayTenMin == 2)
                {

                    decimal ElapseTMRounding = 0;

                    //lnEteRound=(double)CompanyLists[0].ElapseTMRounding~lnRound;
                    double lnElp_Min = 0.0;
                    double lnEteRound = 0.0;

                    if (UserPrincipal.Identity._fpSettings._ElapseTMRounding != null)
                        ElapseTMRounding = (decimal)UserPrincipal.Identity._fpSettings._ElapseTMRounding;

                    if (ElapseTMRounding == 1)
                        lnEteRound = 0;
                    else if (ElapseTMRounding == 2)
                        lnEteRound = 5;
                    else if (ElapseTMRounding == 3)
                        lnEteRound = 10;
                    else if (ElapseTMRounding == 4)
                        lnEteRound = 15;
                    else if (ElapseTMRounding == 5)
                        lnEteRound = 30;
                    else if (ElapseTMRounding == 6)
                        lnEteRound = 60;

                    if (lnEteRound > 0)
                    {

                        lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                        //lnElp_Min = lnElp_Min % lnEteRound;
                        if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                            lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                        else
                            lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));


                        if (lnElp_Min > 0 && lnElp_Min < 60)
                            lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                        if (lnElp_Min == 0)
                            lnElp_Time = Math.Floor(lnElp_Time);


                        if (lnElp_Min == 60)
                            lnElp_Time = Math.Ceiling(lnElp_Time);

                    }
                }
                return lnElp_Time;
            }
        }

        public int GetQtr(DateTime Dateval)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Dateval))
            {
                int lnqtr = 0;

                if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
                {
                    lnqtr = 1;
                }
                else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
                {
                    lnqtr = 2;
                }
                else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
                {
                    lnqtr = 3;
                }
                else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
                {
                    lnqtr = 4;
                }
                else
                    lnqtr = 0;


                return lnqtr;

            }

        }

        ////checkthis function homebase airportid
        private FlightPak.Web.FlightPakMasterService.Aircraft GetAircraft(Int64 AircraftID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {
                    FlightPakMasterService.Aircraft retAircraft = new FlightPakMasterService.Aircraft();
                    var objPowerSetting = objDstsvc.GetAircraftByAircraftID(AircraftID);

                    if (objPowerSetting.ReturnFlag)
                    {
                        //List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = (from aircraft in objPowerSetting.EntityList
                        //                                                                    where aircraft.AircraftID == AircraftID
                        //                                                                    select aircraft).ToList();

                        List<FlightPak.Web.FlightPakMasterService.Aircraft> AircraftList = objPowerSetting.EntityList;


                        if (AircraftList != null && AircraftList.Count > 0)
                            retAircraft = AircraftList[0];
                        else
                            retAircraft = null;
                    }
                    return retAircraft;

                }
            }
        }

        #region Miles Calculation
        /// <summary>
        /// Get the input values from UI For DepartureICAO & Arrival ICAO & calculate Miles
        /// </summary>
        /// <param name="DepartAirportID"></param>
        /// <param name="ArriveAirportID"></param>
        /// <returns></returns>
        public static double GetDistance(Int64 DepartAirportID, Int64 ArriveAirportID)
        {
            double lnMiles = 0.0;

            if (DepartAirportID != null && ArriveAirportID != null)
            {
                using (FlightPakMasterService.MasterCatalogServiceClient objDstsvc = new FlightPakMasterService.MasterCatalogServiceClient())
                {

                    List<GetAllAirport> DepAirport = new List<GetAllAirport>();
                    List<GetAllAirport> ArrAirport = new List<GetAllAirport>();
                    var objRetVal = objDstsvc.GetAirportByAirportID(DepartAirportID);
                    if (objRetVal.ReturnFlag)
                    {
                        DepAirport = objRetVal.EntityList;
                    }
                    objRetVal = null;
                    objRetVal = objDstsvc.GetAirportByAirportID(ArriveAirportID);
                    if (objRetVal.ReturnFlag)
                    {
                        ArrAirport = objRetVal.EntityList;
                    }



                    double deplatdeg = 0;
                    double deplatmin = 0;
                    string lcdeplatdir = "N";

                    double deplngdeg = 0;
                    double deplngmin = 0;
                    string lcdeplngdir = "E";



                    double arrlatdeg = 0;
                    double arrlatmin = 0;
                    string lcArrLatDir = "N";

                    double arrlngdeg = 0;
                    double arrlngmin = 0;
                    string arrlngdir = "E";


                    if (DepAirport != null && DepAirport.Count > 0)
                    {
                        if (DepAirport[0].LatitudeDegree != null)
                            deplatdeg = (double)DepAirport[0].LatitudeDegree;
                        if (DepAirport[0].LatitudeMinutes != null)
                            deplatmin = (double)DepAirport[0].LatitudeMinutes;
                        if (DepAirport[0].LatitudeNorthSouth != null)
                            lcdeplatdir = DepAirport[0].LatitudeNorthSouth;

                        if (DepAirport[0].LongitudeDegrees != null)
                            deplngdeg = (double)DepAirport[0].LongitudeDegrees;
                        if (DepAirport[0].LongitudeMinutes != null)
                            deplngmin = (double)DepAirport[0].LongitudeMinutes;
                        if (DepAirport[0].LongitudeEastWest != null)
                            lcdeplngdir = DepAirport[0].LongitudeEastWest;
                    }

                    if (ArrAirport != null && ArrAirport.Count > 0)
                    {
                        if (ArrAirport[0].LatitudeDegree != null)
                            arrlatdeg = (double)ArrAirport[0].LatitudeDegree;
                        if (ArrAirport[0].LatitudeMinutes != null)
                            arrlatmin = (double)ArrAirport[0].LatitudeMinutes;
                        if (ArrAirport[0].LatitudeNorthSouth != null)
                            lcArrLatDir = ArrAirport[0].LatitudeNorthSouth;
                        if (ArrAirport[0].LongitudeDegrees != null)
                            arrlngdeg = (double)ArrAirport[0].LongitudeDegrees;
                        if (ArrAirport[0].LongitudeMinutes != null)
                            arrlngmin = (double)ArrAirport[0].LongitudeMinutes;
                        if (ArrAirport[0].LongitudeEastWest != null)
                            arrlngdir = ArrAirport[0].LongitudeEastWest;
                    }



                    //lnMiles = .CalculateDistance(deplatdeg, deplatmin, lcdeplatdir, deplngdeg, deplngmin, lcdeplngdir, arrlatdeg, arrlatmin, lcArrLatDir, arrlngdeg, arrlngmin, arrlngdir);
                }
            }

            if (double.IsNaN(lnMiles))
                lnMiles = 0;
            // int intlnMiles =  lnMiles.ToString().IndexOf('.');
            //lnMiles = Convert.ToDouble(lnMiles.ToString().Substring(0,intlnMiles));
            return Math.Floor(lnMiles);

        }
        /// <summary>
        /// Function to calculate Distance based on the airport parameters
        /// </summary>
        /// <param name="deplatdeg">Depart Latitude Degree</param>
        /// <param name="deplatmin">Depart Latitude Minutes</param>
        /// <param name="lcdeplatdir">Depart Latitude Direction (N/S) </param>
        /// <param name="deplngdeg">Depart Longitude Degree</param>
        /// <param name="deplngmin">Depart Longitude Minutes</param>
        /// <param name="lcdeplngdir">Depart Longitude Direction (E/W)</param>
        /// <param name="arrlatdeg">Arrival Lattitude Degree</param>
        /// <param name="arrlatmin">Arrival Lattitude Minutes</param>
        /// <param name="lcArrLatDir">Arrival Lattitude Direction (N/S)</param>
        /// <param name="arrlngdeg">Arrival Longitude Degree</param>
        /// <param name="arrlngmin">Arrival Longitude Minutes</param>
        /// <param name="arrlngdir">Arrival Longitude Direction (E/W)</param>
        /// <returns>Miles</returns>
        public double CalculateDistance(double deplatdeg = 0,
            double deplatmin = 0,
            string lcdeplatdir = "N",
            double deplngdeg = 0,
            double deplngmin = 0,
            string lcdeplngdir = "E",
            double arrlatdeg = 0,
            double arrlatmin = 0,
            string lcArrLatDir = "N",
            double arrlngdeg = 0,
            double arrlngmin = 0,
            string arrlngdir = "E")
        {
            double lnMiles = 0;
            double lnDepLat = ((deplatdeg + (deplatmin / 60)) * ((lcdeplatdir.ToUpper() == "N") ? 1 : -1));
            double lnDepLng = ((deplngdeg + (deplngmin / 60)) * ((lcdeplngdir.ToUpper() == "W") ? 1 : -1));
            double lnArrLat = ((arrlatdeg + (arrlatmin / 60)) * ((lcArrLatDir.ToUpper() == "N") ? 1 : -1));
            double lnArrLng = ((arrlngdeg + (arrlngmin / 60)) * ((arrlngdir.ToUpper() == "W") ? 1 : -1));

            double dLat1InRad = lnDepLat * (Math.PI / 180.0);
            double dLong1InRad = lnDepLng * (Math.PI / 180.0);
            double dLat2InRad = lnArrLat * (Math.PI / 180.0);
            double dLong2InRad = lnArrLng * (Math.PI / 180.0);

            double dLongitude = dLong2InRad - dLong1InRad;
            double dLatitude = dLat2InRad - dLat1InRad;

            double lnLongDiff = lnArrLng - lnDepLng;

            double RadlnDepLat = (Math.PI / 180.0) * lnDepLat;
            double RadlnArrLat = (Math.PI / 180.0) * lnArrLat;
            double RadlnlnLongDiff = (Math.PI / 180.0) * lnLongDiff;


            lnMiles = 60 * ((180.0 / Math.PI) * (
                                           Math.Acos(
                                                           Math.Sin(
                                                           RadlnDepLat
                                                           )
                                                           *
                                                           Math.Sin(
                                                           RadlnArrLat
                                                           )
                                                           +
                                                           Math.Cos(
                                                           RadlnDepLat
                                                           )
                                                           *
                                                           Math.Cos(
                                                           RadlnArrLat
                                                           )
                                                           *
                                                           Math.Cos(
                                                           RadlnlnLongDiff
                                                           )
                                                     )
                                                )
                        );

            if (double.IsNaN(lnMiles))
                lnMiles = 0;
            // int intlnMiles =  lnMiles.ToString().IndexOf('.');
            //lnMiles = Convert.ToDouble(lnMiles.ToString().Substring(0,intlnMiles));
            return Math.Floor(lnMiles);
        }
        #endregion

        #region Bias Calculation
        /// <summary>
        /// Function to calculate BIAS based on aircraft settings and Depart Airport take Off Bias and Arrival Airport Landing Bias
        /// </summary>
        /// <param name="PowerSetting">Power setting selected (1,2,3)</param>
        /// <param name="PowerSettings1TakeOffBias"></param>
        /// <param name="PowerSettings1LandingBias"></param>
        /// <param name="PowerSettings1TrueAirSpeed"></param>
        /// <param name="PowerSettings2TakeOffBias"></param>
        /// <param name="PowerSettings2LandingBias"></param>
        /// <param name="PowerSettings2TrueAirSpeed"></param>
        /// <param name="PowerSettings3TakeOffBias"></param>
        /// <param name="PowerSettings3LandingBias"></param>
        /// <param name="PowerSettings3TrueAirSpeed"></param>
        /// <param name="DepAirportTakeoffBIAS"></param>
        /// <param name="ArrAirportLandingBIAS"></param>
        /// <returns>T/O Bias, Landing Bias, TAS(True Air Speed)</returns>
        public List<double> CalcBiasTas(string PowerSetting, double PowerSettings1TakeOffBias, double PowerSettings1LandingBias, double PowerSettings1TrueAirSpeed, double PowerSettings2TakeOffBias, double PowerSettings2LandingBias, double PowerSettings2TrueAirSpeed, double PowerSettings3TakeOffBias, double PowerSettings3LandingBias, double PowerSettings3TrueAirSpeed, double DepAirportTakeoffBIAS, double ArrAirportLandingBIAS)
        {
            double lnToBias = 0.0f;
            double lnLndBias = 0.0f;
            double lnTas = 0.0f;

            switch (PowerSetting)
            {
                case "1":
                    {
                        lnToBias = PowerSettings1TakeOffBias;
                        lnLndBias = PowerSettings1LandingBias;
                        lnTas = PowerSettings1TrueAirSpeed;
                        break;
                    }
                case "2":
                    {
                        lnToBias = PowerSettings2TakeOffBias;
                        lnLndBias = PowerSettings2LandingBias;
                        lnTas = PowerSettings2TrueAirSpeed;
                        break;
                    }

                case "3":
                    {
                        lnToBias = PowerSettings3TakeOffBias;
                        lnLndBias = PowerSettings3LandingBias;
                        lnTas = PowerSettings3TrueAirSpeed;
                        break;
                    }
                default:
                    {
                        lnToBias = 0;
                        lnLndBias = 0;
                        lnTas = 0;
                        break;
                    }
            }

            lnToBias = lnToBias + DepAirportTakeoffBIAS;
            lnLndBias = lnLndBias + ArrAirportLandingBIAS;
            return new List<double>() { lnToBias, lnLndBias, lnTas };
        }
        #endregion


        #region ETE Calculation
        /// <summary>
        /// Get the input values from UI screens for Wind, LandingBias,TrueAirSpeed,TakeOffBias,Miles
        /// </summary>
        /// <param name="lnwind"></param>
        /// <param name="lnLndBias"></param>
        /// <param name="lntas"></param>
        /// <param name="lnToBias"></param>
        /// <param name="lnmiles"></param>
        /// <param name="DateVal"></param>
        /// <returns></returns>
        public double GetIcaoEte(double lnwind, double lnLndBias, double lntas, double lnToBias, double lnmiles, DateTime DateVal, string IsFixedRotary)
        {
            double lnEte = 0;


            string lcLndBias = string.Empty;

            //var r = GetQtr(DateVal);
            if ((lnwind + lntas) > 0)
            {
                double lnFHr = 0;
                double lnFMin = 0;
                lnEte = Math.Round(lnmiles / (lntas + lnwind), 3);
                if (lnEte == 0 && IsFixedRotary.ToUpper() == "R")
                    lnEte = 0.1;

                string lcEte = Convert.ToString(lnEte);
                int lnPos = lcEte.IndexOf('.');
                double lnHr = 0, lnMin = 0;
                if (lnPos > 0)
                {
                    if (!double.TryParse(lcEte.Substring(0, lnPos), out lnHr))
                        lnHr = 0;
                    if (double.TryParse(lcEte.Substring(lnPos), out lnMin))
                        lnMin = Math.Round(lnMin * 60, 0);
                    else
                        lnMin = 0;
                }
                else
                {
                    if (!double.TryParse(lcEte.ToString(), out lnHr))
                        lnHr = 0;
                }
                lnFHr = lnHr;
                lnFMin = lnMin;
                lnHr = 0;
                lnMin = 0;
                string lcToBias = string.Empty;
                lcToBias = lnToBias.ToString();//Convert.ToString(Math.Round(lnToBias, 3));
                lcToBias.Trim();
                lnPos = lcToBias.IndexOf('.');
                if (lnPos > 0)
                {
                    if (!double.TryParse(lcToBias.Substring(0, lnPos), out lnHr))
                        lnHr = 0;
                    if (double.TryParse(lcToBias.Substring(lnPos), out lnMin)) //ROUND(VAL(SUBSTR(lcToBias,lnPos+1))/1000 * 60,0)
                        lnMin = Math.Round(lnMin * 60, 0);
                    else
                        lnMin = 0;
                }
                else
                {
                    if (!double.TryParse(lcToBias.ToString(), out lnHr))
                        lnHr = 0;
                }
                lnFHr = lnFHr + lnHr;
                lnFMin = lnFMin + lnMin;
                lcLndBias = lnLndBias.ToString();// Convert.ToString(Math.Round(lnLndBias, 3));

                lnHr = 0;
                lnMin = 0;

                lnPos = lcLndBias.IndexOf('.');//AT(".",lcLndBias)
                if (lnPos > 0)
                {
                    if (!double.TryParse(lcLndBias.Substring(0, lnPos), out lnHr)) //VAL(LEFT(lcLndBias,lnPos-1))
                        lnHr = 0;
                    if (double.TryParse(lcLndBias.Substring(lnPos), out lnMin)) //ROUND(VAL(SUBSTR(lcLndBias,lnPos+1))/1000 * 60,0)
                        lnMin = Math.Round(lnMin * 60, 0);
                    else
                        lnMin = 0;
                }
                else
                {
                    if (!double.TryParse(lcLndBias.ToString(), out lnHr)) //VAL(LEFT(lcLndBias,lnPos-1))
                        lnHr = 0;
                }
                lnFHr = lnFHr + lnHr;
                lnFMin = lnFMin + lnMin;
                lnEte = lnFHr + (double)Math.Round((lnFMin / 60), 3);

                lcEte = lnEte.ToString();
                lnPos = lcEte.IndexOf('.');
                string lcHr, lcMin;
                lcHr = lcMin = string.Empty;
                if (lnPos > 0)
                {

                    lcHr = lcEte.Substring(0, lnPos);
                    lcMin = lcEte.Substring(lnPos + 1, (3 > lcEte.Substring(lnPos + 1, lcEte.Length - 1 - lnPos).Length) ? lcEte.Substring(lnPos + 1, lcEte.Length - 1 - lnPos).Length : 3);

                    lcEte = lcHr + "." + lcMin;

                    if (!double.TryParse(lcEte.ToString(), out lnEte))
                        lnEte =0;
                }
            }

            return lnEte;

        }
        #endregion



        #region GMT Calculation
        /// <summary>
        /// GMT Calculation
        /// </summary>
        /// <param name="ldDtTm"></param>
        /// <param name="AP_OffsetToGMTEnd">DepartureAirport Offset to GMT</param>
        /// <param name="airport_dst_yn">AirPort Destination -Yes/No</param>
        /// <param name="AP_DayLightSDT">Departure AirPort DayLightSavingStartDate </param>
        /// <param name="AP_DaylLightSTM">Departure DaylLightSavingStartTime</param>
        /// /// <param name="AP_DayLightSDT">Departure AirPort DayLightSavingEndDate </param>
        /// <param name="AP_DaylLightSTM">Departure DaylLightSavingEndTime</param>
        /// <param name="AP_OffsetToGMTStart">ArrivalAirportOffsetToGMT</param>
        /// <param name="AP_OffsetToGMT">AirPortOffsetToGMT</param>
        /// <returns></returns>
        public DateTime GetGMT(DateTime ldDtTm, bool IIFindGMT,
            bool? IsDayLightSaving,
            DateTime? DayLightSavingStartDT,
            DateTime? DayLightSavingEndDT,
            string DaylLightSavingStartTM,
            string DayLightSavingEndTM,
            decimal? OffsetToGMT,
            bool IIGmt = false
            )
        {
            DateTime ldNewDtTm = DateTime.MinValue;

            DateTime ldDstBegtm = DateTime.MinValue;
            DateTime ldDstEndtm = DateTime.MinValue;
            double lnDst = 0;

            try
            {

                if (ldDtTm == null || ldDtTm == DateTime.MinValue)
                {
                    return ldDtTm;
                }
                ldNewDtTm = ldDtTm;
                if (IIGmt == true)
                {
                    return ldDtTm;
                }

                //AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                //var objRetVal = AirportCatalog.GetAirportByAirportID(AirportID);

                //List<FlightPak.Data.MasterCatalog.GetAllAirport> airport = new List<GetAllAirport>();
                //if (objRetVal.ReturnFlag)
                //{
                //    airport = objRetVal.EntityList;
                //}


                //List<FlightPak.Data.MasterCatalog.GetAllAirport> airport = (from Arpt in objRetVal.EntityList
                //                                                            where Arpt.AirportID == AirportID
                //                                                            select Arpt).ToList();

                //Find Local and Home Time Based on GMT
                //DST Begin date and time stored in ldDstBeg1
                DateTime ldDstBeg1 = DateTime.MinValue;
                DateTime ldDstEnd1 = DateTime.MinValue;
                bool lbIsDayLightSaving = false;
                decimal lnOffsetToGMT = 0;

                //if (airport != null && airport.Count > 0)
                //{
                if (IsDayLightSaving != null)
                {
                    if (DayLightSavingStartDT != null && DayLightSavingEndDT != null)
                        lbIsDayLightSaving = (bool)IsDayLightSaving;
                }

                if (DayLightSavingStartDT != null)
                {
                    ldDstBeg1 = (DateTime)DayLightSavingStartDT;
                    ldDstBeg1 = new DateTime(ldDtTm.Year, ldDstBeg1.Month, ldDstBeg1.Day);
                    if (!string.IsNullOrEmpty(DaylLightSavingStartTM))
                        ldDstBegtm = Convert.ToDateTime(DaylLightSavingStartTM);
                    else
                        lbIsDayLightSaving = false;

                    ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                    ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                }
                if (DayLightSavingEndDT != null)
                {
                    ldDstEnd1 = (DateTime)DayLightSavingEndDT;
                    ldDstEnd1 = new DateTime(ldDtTm.Year, ldDstEnd1.Month, ldDstEnd1.Day);
                    if (!string.IsNullOrEmpty(DayLightSavingEndTM))
                        ldDstEndtm = Convert.ToDateTime(DayLightSavingEndTM);
                    else
                        lbIsDayLightSaving = false;
                    ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                    ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                    ldDstEnd1 = ldDstEnd1.AddSeconds(-60);
                }

                if (OffsetToGMT != null)
                    lnOffsetToGMT = (decimal)OffsetToGMT;



                if (!IIFindGMT)
                {
                    if (lbIsDayLightSaving)
                    {
                        //bool summertimeflag = true;
                        if (ldDstEnd1.Month < ldDstBeg1.Month)
                        {
                            if (ldDtTm.Month >= ldDstBeg1.Month && ldDtTm.Month <= 12)
                            {
                                ldDstEnd1 = new DateTime(ldDstEnd1.Year + 1, ldDstEnd1.Month, ldDstEnd1.Day);
                                ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                                ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                            }

                            else if (ldDtTm.Month >= 1 && ldDtTm.Month <= ldDstEnd1.Month)
                            {
                                ldDstBeg1 = new DateTime(ldDstBeg1.Year - 1, ldDstBeg1.Month, ldDstBeg1.Day);
                                ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                                ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                            }
                        }


                        if (ldDtTm.Equals(ldDstBeg1))
                        {
                            double ldBegOffSet = (double)(lnOffsetToGMT * 3600) + 3600;
                            //TimeSpan timeSpanBeg = new TimeSpan((long)(ldBegOffSet));
                            ldNewDtTm = ldDtTm.AddSeconds(ldBegOffSet);
                        }
                        else if (ldDtTm.Equals(ldDstEnd1))
                        {
                            double ldEndOffSet = (((double)lnOffsetToGMT * 3600) + (1 * 3600));
                            //TimeSpan timeSpanEnd = new TimeSpan((long)(ldEndOffSet));
                            ldNewDtTm = ldDtTm.AddSeconds(ldEndOffSet);
                        }

                        else if ((ldDstBeg1 < ldDstEnd1) && (ldDtTm >= ldDstBeg1 && ldDtTm <= ldDstEnd1))
                        {
                            double airPortOffSet = (((double)lnOffsetToGMT * 3600) + 3600);
                            //TimeSpan timeSpan = new TimeSpan((long)(airPortOffSet));
                            ldNewDtTm = ldDtTm.AddSeconds(airPortOffSet);
                        }
                        else
                        {
                            double gmtOffSet = ((double)lnOffsetToGMT * 3600);
                            //TimeSpan timeSpan4 = new TimeSpan((long)(gmtOffSet));
                            ldNewDtTm = ldDtTm.AddSeconds(gmtOffSet);
                        }
                    }
                    else
                    {
                        double gmtOffSet = ((double)lnOffsetToGMT * 3600) + lnDst;
                        //TimeSpan timeSpan4 = new TimeSpan((long)(gmtOffSet));
                        ldNewDtTm = ldDtTm.AddSeconds(gmtOffSet);
                    }
                }
                else
                {
                    if (lbIsDayLightSaving)
                    {
                        if ((ldDstEnd1.Month) < (ldDstBeg1.Month))
                        {
                            if (ldDtTm.Month >= ldDstBeg1.Month && ldDtTm.Month <= 12)
                            {
                                ldDstEnd1 = new DateTime(ldDstEnd1.Year + 1, ldDstEnd1.Month, ldDstEnd1.Day);
                                ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                                ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                            }
                            else if ((ldDtTm.Month >= 1) && (ldDtTm.Month <= ldDstEnd1.Month))
                            {
                                ldDstBeg1 = new DateTime(ldDstBeg1.Year - 1, ldDstBeg1.Month, ldDstBeg1.Day);
                                ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                                ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                            }
                        }
                        double x5 = (double)lnOffsetToGMT * 3600;
                        //TimeSpan ts5 = new TimeSpan((long)(x5));
                        DateTime ltLocDstChgBeg = ldDstBeg1.AddSeconds(x5);
                        //DateTime ltLocDstChgBeg = ldDstBeg1.Add(ts5);

                        double x6 = ((double)OffsetToGMT * 3600) + (((double)lnOffsetToGMT != 0) ? (1 * 3600) : 0);
                        //TimeSpan ts6 = new TimeSpan((long)(x6));
                        DateTime ltLocDstChgEnd = ldDstEnd1.AddSeconds(x6);
                        //DateTime ltLocDstChgEnd = ldDstEnd1.Add(ts6);



                        double x7 = (double)lnOffsetToGMT * 3600;
                        //TimeSpan ts7 = new TimeSpan((long)(x7));

                        double x8 = (((double)lnOffsetToGMT * 3600) + 3540);
                        //TimeSpan ts8 = new TimeSpan((long)(x8));

                        if ((ldDtTm >= ltLocDstChgBeg) && (ldDtTm <= ltLocDstChgEnd))
                        {

                            lnDst = 1 * 3600;
                        }
                        if ((ldDtTm >= ldDstBeg1.AddSeconds(x7)) && (ldDtTm <= ldDstBeg1.AddSeconds(x8)))
                        {
                            lnDst = 0;
                        }
                    }

                    double x9 = (((double)lnOffsetToGMT * 3600) + lnDst);
                    //TimeSpan ts9 = new TimeSpan((long)(x9));
                    ldNewDtTm = ldDtTm.AddSeconds(-(x9));

                }
                //}
            }
            catch (Exception e)
            {
                //manually handled
                ldNewDtTm = ldDtTm;
            }
            return ldNewDtTm;
        }
        #endregion

        #endregion

        /// </summary>
        /// <param name="TextboxValues"></param>
        /// <returns></returns>
        public string CalculationTimeAdjustment(string TextboxValues)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TextboxValues))
            {
                string TextboxValue = "00:00";
                if (TextboxValues != "")
                {
                    TextboxValue = TextboxValues;
                    if ((!TextboxValue.Contains('-')) && (TextboxValue.Trim() != "00:00") && (TextboxValue.Trim() != "") && (TextboxValue.Trim() != ":") && (TextboxValue.Trim() != ".") && (TextboxValue.Trim() != "0") && (TextboxValue.Trim() != "0:0") && (TextboxValue.Trim() != "0:00"))
                    {
                        if (TextboxValue.IndexOf(":") != -1)
                        {
                            string[] timeArray = TextboxValue.Split(':');
                            if (string.IsNullOrEmpty(timeArray[0]))
                            {
                                timeArray[0] = "00";
                                TextboxValue = timeArray[0] + ":" + timeArray[1];
                            }
                            if (!string.IsNullOrEmpty(timeArray[0]))
                            {
                                if (timeArray[0].Length == 0)
                                {
                                    timeArray[0] = "00";
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                                if (timeArray[0].Length == 1)
                                {
                                    timeArray[0] = "0" + timeArray[0];
                                    TextboxValue = timeArray[0] + ":" + timeArray[1];
                                }
                            }
                            if (!string.IsNullOrEmpty(timeArray[1]))
                            {
                                Int64 result = 0;
                                if (timeArray[1].Length != 2)
                                {
                                    result = (Convert.ToInt32(timeArray[1])) / 10;
                                    if (result < 1)
                                    {
                                        if (timeArray[0] != null)
                                        {
                                            if (timeArray[0].Trim() == "")
                                            {
                                                timeArray[0] = "00";
                                            }
                                        }
                                        else
                                        {
                                            timeArray[0] = "00";
                                        }
                                        TextboxValue = timeArray[0] + ":" + "0" + timeArray[1];

                                    }
                                }
                            }
                            else
                            {
                                TextboxValue = TextboxValue + "00";
                            }
                        }
                        else
                        {
                            TextboxValue = TextboxValue + ":00";
                        }
                    }
                    else
                    {
                        TextboxValue = "00:00";
                    }
                }
                else
                {
                    TextboxValue = "00:00";
                }
                return TextboxValue;
            }
        }
    }
}
