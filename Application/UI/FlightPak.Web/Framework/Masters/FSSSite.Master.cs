﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Web.Security;
using FlightPak.Web.Framework.Prinicipal;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;

namespace FlightPak.Web.Framework.Masters
{
    public partial class FSSSite : BaseSecuredMasterPage
    {
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lbYear.Text = DateTime.UtcNow.Year.ToString();
                        Response.AddHeader("X-UA-Compatible", "IE=8,9");
                        //Response.AddHeader("X-UA-Compatible", "IE=Edge");
                        if (!IsPostBack)
                        {
                            // Set Logged-in User Name
                            FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                            if (identity != null && identity.Identity._name != null)
                            {
                                lbUserName.InnerText = identity.Identity._name.Trim();
                            }

                            //Vishwa
                            //Load build version from a static variable. It should be singleton, Should be loaded once per app life cycle
                            lbBuildVer.Text = Helpers.BuildVersionHelper.BuildVersion;
                            RadSearchCombo.LoadXml(Framework.Constants.SearchModuleItems.SearchModules);
                            if (!string.IsNullOrEmpty(ModuleSelectedValue))
                            {
                                RadSearchCombo.SelectedValue = ModuleSelectedValue;
                            }

                            using (CorporateRequestService.CorporateRequestServiceClient ObjService = new CorporateRequestService.CorporateRequestServiceClient())
                            {
                                var ObjRetVal = ObjService.GetCRChangeQueue();
                                List<CorporateRequestService.GetAllCRMain> lstAccount = new List<CorporateRequestService.GetAllCRMain>();
                                if (ObjRetVal.ReturnFlag == true)
                                {
                                    lstAccount = ObjRetVal.EntityList.Where(x => x.IsDeleted == false && x.CRTripNUM != null && x.CorporateRequestStatus == "S").ToList();
                                }
                                //if (lstAccount.Count > 0)
                                //{
                                //    btnChangeQueue.Text = lstAccount.Count.ToString();
                                //    btnChangeQueue.CssClass = "cq_status_alert";
                                //}
                                //else
                                //{
                                //    btnChangeQueue.Text = "0";
                                //    btnChangeQueue.CssClass = "cq_status";
                                //}
                            }
                        }

                        //Added for Loading Widgets
                        //LoadPreferences();
                        LoadPreferencesLink();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Global);
                }
            }
        }

        /// <summary>
        /// Method to Logout from the Application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        base.LogOut();
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Global);
                }
            }
        }

        /// <summary>
        /// For Link
        /// </summary>
        private void LoadPreferencesLink()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                var utilityList = UserPrincipal.Identity._fpUserPreferences;
                if (utilityList.Count > 0)
                {
                    liAirportTable.Visible = false;
                    liAirportPairs.Visible = false;
                    liItineraryPlanning.Visible = false;
                    liLocator.Visible = false;
                    liWorldWinds.Visible = false;
                    liWorldClock.Visible = false;
                    liBookmark.Visible = false;
                    liUVTripPlanner.Visible = false;

                    foreach (var util in utilityList)
                    {
                        if (util.KeyName != null && util.KeyName.Trim().ToLower() == "airport table" && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liAirportTable.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "airport pairs" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liAirportPairs.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "airport pairs leg worksheet" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            // No Items in UI
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "itinerary planning" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liItineraryPlanning.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "itinerary planning legs" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            // No Items in UI
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "uvtripplanner" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liUVTripPlanner.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "winds on the world air routes" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liWorldWinds.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "world clock" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liWorldClock.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "locator" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liLocator.Visible = true;
                        }
                        else if (util.KeyName != null && util.KeyName.Trim().ToLower() == "bookmark" && util.KeyValue != null && util.KeyValue.ToString().ToLower() == "true")
                        {
                            liBookmark.Visible = true;
                        }
                    }
                }
                //}
            }
        }


        protected void SearchButton_Click(object sender, EventArgs e)
        {
            string url = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        TryResolveUrl("views/search/searchresult.aspx", out url);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Global);
                }
            }
            var page = this.Page as BasePage;
            page.RedirectToPage(string.Format("{0}?q={1}&&c={2}", url, Microsoft.Security.Application.Encoder.UrlEncode(SearchTextBox.Text), Microsoft.Security.Application.Encoder.UrlEncode(RadSearchCombo.SelectedValue)));
        }

        protected void lnkSetAsHomepage_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CommonService.UserPreference oUserPreference = new CommonService.UserPreference();
                        using (CommonService.CommonServiceClient Service = new CommonService.CommonServiceClient())
                        {                            
                            oUserPreference.UserPreferenceID = 0;
                            oUserPreference.CustomerID = 10000;
                            oUserPreference.UserName = UserPrincipal.Identity._name;
                            oUserPreference.CategoryName = "landingpage";
                            oUserPreference.SubCategoryName = "";
                            oUserPreference.KeyName = "landingpage";
                            if (Request.QueryString["seltab"] != null)
                            {
                                oUserPreference.KeyValue = Request.Url.AbsolutePath + "?seltab=" + Request.QueryString["seltab"];
                            }
                            else
                            {
                                oUserPreference.KeyValue = Request.Url.AbsolutePath;
                            }
                            oUserPreference.LastUpdTS = DateTime.Now;
                            var Result = Service.GetBookmark(oUserPreference).EntityList;
                            if (Result.Count > 0)
                            {
                                oUserPreference.UserPreferenceID = Result[0].UserPreferenceID;
                            }
                            var Result1 = Service.AddBookmark(oUserPreference);
                            if (Result1.ReturnFlag == true)
                            {                                
                            }
                            else
                            {
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Global);
                }
            }
        }

    }
}
