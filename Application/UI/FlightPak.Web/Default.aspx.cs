﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.FlightPakMasterService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.CommonService;
using FlightPak.Common;

namespace FlightPak.Web
{
    public partial class _Default : BaseSecuredPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string RedirectURL = string.Empty;

            if (!string.IsNullOrEmpty(UserPrincipal.Identity._landingPage))
            {
                Session["_landingPage"] = UserPrincipal.Identity._landingPage;

                if (UserPrincipal.Identity._landingPage == "/Views/Home.aspx" && RedirectToCalendarView())
                    return;

                TryResolveUrl(UserPrincipal.Identity._landingPage, out RedirectURL);
                RedirectToPage(RedirectURL);
            }
            else
            {
                if (!RedirectToCalendarView())
                    Response.Redirect("~\\Views\\Home.aspx");
            }
        }


        public bool RedirectToCalendarView()
        {
            string defaultView = string.Empty;
            string redirectUrl = "~\\Views\\Transactions\\PreFlight\\ScheduleCalendar\\{0}";
            using (var commonServiceClient = new CommonServiceClient())
            {
                defaultView = commonServiceClient.GetUserDefaultCalendarView();
            }

            switch (defaultView)
            {
                case ModuleNameConstants.Preflight.WeeklyCrew:
                    redirectUrl = string.Format(redirectUrl, "WeeklyCrew.aspx");
                    break;

                case ModuleNameConstants.Preflight.WeeklyMain:
                    redirectUrl = string.Format(redirectUrl, "WeeklyMain.aspx");
                    break;

                case ModuleNameConstants.Preflight.WeeklyDetail:
                    redirectUrl = string.Format(redirectUrl, "WeeklyDetail.aspx");
                    break;

                case ModuleNameConstants.Preflight.Month:
                    redirectUrl = string.Format(redirectUrl, "Monthly.aspx?seltab=Monthly");
                    break;

                case ModuleNameConstants.Preflight.BusinessWeek:
                    redirectUrl = string.Format(redirectUrl, "BusinessWeek.aspx?seltab=BusinessWeek");
                    break;

                case ModuleNameConstants.Preflight.Corporate:
                    redirectUrl = string.Format(redirectUrl, "CorporateView.aspx?seltab=Corporate");
                    break;

                case ModuleNameConstants.Preflight.Day:
                    redirectUrl = string.Format(redirectUrl, "DayView.aspx?seltab=Day");
                    break;

                case ModuleNameConstants.Preflight.Planner:
                    redirectUrl = string.Format(redirectUrl, "Planner.aspx?seltab=Planner");
                    break;

                default:
                    redirectUrl = "";
                    break;
            }

            if (string.IsNullOrEmpty(redirectUrl)) return false;
            else { Response.Redirect(redirectUrl); return true; }

        }

    }
}
