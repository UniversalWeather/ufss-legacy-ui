﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.ViewModels.MasterData;
namespace FlightPak.Web.ViewModels.Postflight
{
    public class OtherCrewDutyLogViewModel
    {
        public OtherCrewDutyLogViewModel()  
        {
            Initialize();
        }
        public OtherCrewDutyLogViewModel(UserPrincipalViewModel userPrincipal)
        {
            Initialize();
            if (userPrincipal != null)
            {

                if (userPrincipal._TimeDisplayTenMin != null)
                    if (userPrincipal._TimeDisplayTenMin > 0)
                        this.TimeDisplayTenMin = (decimal)userPrincipal._TimeDisplayTenMin;

                if (userPrincipal._HoursMinutesCONV != null)
                    if (userPrincipal._HoursMinutesCONV > 0)
                        this.HoursMinutesCONV = (int)userPrincipal._HoursMinutesCONV;
            }
        }
        private void Initialize()
        {
            Homebase = new HomebaseViewModel();
            Crew = new CrewViewModel();
            Client = new ClientViewModel();
            Airport = new AirportViewModel();
            Aircraft = new AircraftViewModel();
            CrewDutyType = new CrewDutyTypeViewModel();
            TimeDisplayTenMin = POTenthMinuteFormat.Tenth;
        }
        public decimal TimeDisplayTenMin { get; set; }
        public int HoursMinutesCONV { get; set; }

        public Int64 SimulatorID { get; set; }
        public Int64? CustomerID { get; set; }
        public Int64? CrewID { get; set; }
        public String CrewCD { get; set; }
        public String FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public DateTime? SessionDT { get; set; }
        public Int64? AircraftID { get; set; }
        public String AircraftCD { get; set; }
        public String DutyTYPE { get; set; }
        public Decimal? TakeOffDay { get; set; }
        public Decimal? TakeOffNight { get; set; }
        public Decimal? LandingDay { get; set; }
        public Decimal? LandingNight { get; set; }
        public Decimal? ApproachPrecision { get; set; }
        public Decimal? ApproachNonPrecision { get; set; }
        public Decimal? Instrument { get; set; }
        public Decimal? Night { get; set; }
        public Decimal? FlightHours { get; set; }
        public Decimal? DutyHours { get; set; }
        public bool? Specification1 { get; set; }
        public bool? Sepcification2 { get; set; }
        public Int64? HomeBaseID { get; set; }
        public String LastUserID { get; set; }
        public DateTime? LastUptTS { get; set; }
        public Int64? TripID { get; set; }
        public Decimal? Specification3 { get; set; }
        public Decimal? Specification4 { get; set; }
        public Int64? DutyTypeID { get; set; }
        public String DutyTypeCD { get; set; }
        public Int64? AirportID { get; set; }
        public String IcaoID { get; set; }
        public Int64? ClientID { get; set; }
        public String ClientCD { get; set; }
        public String SimulatorDescription { get; set; }
        public DateTime? DepartureDTTMLocal { get; set; }
        public DateTime? ArrivalDTTMLocal { get; set; }
        public DateTime? DepartureTMGMT { get; set; }
        public DateTime? ArrivalTMGMT { get; set; }
        public Boolean IsDeleted { get; set; }
        public String HomebaseCD { get; set; }
        public HomebaseViewModel Homebase { get; set; }
        public CrewViewModel Crew { get; set; }
        public ClientViewModel Client { get; set; }
        public AirportViewModel Airport { get; set; }
        public AircraftViewModel Aircraft { get; set; }
        public CrewDutyTypeViewModel CrewDutyType { get; set; }
        public string GotoDate { get; set; }
        public string LastModifiedDate { get; set; }
        public string InstrumentTM
        {
            get { return SetPropertyDecimalValue(Instrument); }
            set { Instrument = GetDecimalValue(value); }
        }
        public string NightTM
        {
            get { return SetPropertyDecimalValue(Night); }
            set { Night = GetDecimalValue(value); }
        }
        public string FlightHoursTM
        {
            get { return SetPropertyDecimalValue(FlightHours); }
            set { FlightHours = GetDecimalValue(value); }
        }
        public string DutyHoursTM
        {
            get { return SetPropertyDecimalValue(DutyHours); }
            set { DutyHours = GetDecimalValue(value); }
        }
        private decimal GetDecimalValue(string propValue)
        {
            return TimeDisplayTenMin == 0 ? 0 : ((TimeDisplayTenMin == POTenthMinuteFormat.Tenth) ? Math.Round(Convert.ToDecimal(propValue), 1) : Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(propValue, true, HoursMinutesCONV)));
        }
        private string SetPropertyDecimalValue(decimal? propValue)
        {
            return (TimeDisplayTenMin == POTenthMinuteFormat.Tenth) ? (propValue == null ? "0.0" : Convert.ToDecimal(propValue).ToString("#0.0")) : FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(propValue));
        }

        private string _DepartureDTTMLocalDate;
        public String DepartureDTTMLocalDate
        {
            get
            {
                string date = DepartureDTTMLocal.FSSParseDateTimeString(WebConstants.isoDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _DepartureDTTMLocalDate = value;
                DepartureDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : DepartureDTTMLocal.FSSParseDateTime(value, WebConstants.isoDateFormat);
            }
        }

        public String DepartureDTTMLocalTime
        {
            get
            {
                string time = DepartureDTTMLocal.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? ":" : time;
            }
            set
            {
                DepartureDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : DepartureDTTMLocal.FSSParseDateTime(string.Format("{0} {1}", _DepartureDTTMLocalDate, value), WebConstants.isoDateFormat + " HH:mm");
            }
        }

        private string _ArrivalDTTMLocalDate;
        public String ArrivalDTTMLocalDate
        {
            get
            {
                string date = ArrivalDTTMLocal.FSSParseDateTimeString(WebConstants.isoDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _ArrivalDTTMLocalDate = value;
                ArrivalDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : ArrivalDTTMLocal.FSSParseDateTime(value, WebConstants.isoDateFormat);
            }
        }
        public String ArrivalDTTMLocalTime
        {
            get
            {
                string time = ArrivalDTTMLocal.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? ":" : time;
            }
            set
            {
                ArrivalDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : ArrivalDTTMLocal.FSSParseDateTime(string.Format("{0} {1}", _ArrivalDTTMLocalDate, value), WebConstants.isoDateFormat + " HH:mm");
            }
        }

        public bool DutyTYPEpic
        {
            get { return DutyTYPE == null ? false : DutyTYPE.Contains("P"); }
            set { UpdateDutyType("P", value); }
        }
        public bool DutyTYPEsic
        {
            get { return DutyTYPE == null ? false : DutyTYPE.Contains("S"); }
            set { UpdateDutyType("S", value); }
        }
        public bool DutyTYPEengineer
        {
            get { return DutyTYPE == null ? false : DutyTYPE.Contains("E"); }
            set { UpdateDutyType("E", value); }
        }
        public bool DutyTYPEinstructor
        {
            get { return DutyTYPE == null ? false : DutyTYPE.Contains("I"); }
            set { UpdateDutyType("I", value); }
        }
        public bool DutyTYPEattendant
        {
            get { return DutyTYPE == null ? false : DutyTYPE.Contains("A"); }
            set { UpdateDutyType("A", value); }
        }
        public bool DutyTYPEother
        {
            get { return DutyTYPE == null ? false : DutyTYPE.Contains("O"); }
            set { UpdateDutyType("O", value); }
        }
        private void UpdateDutyType(string type, bool include)
        {
            string dutyType = (DutyTYPE == null ? "" : DutyTYPE.Trim());
            if (dutyType.Contains(type) && include == false)
            {
                dutyType = dutyType.Replace(type, "").Replace(",,", ",");
                if (dutyType.EndsWith(",")) dutyType = dutyType.Substring(0, dutyType.Length - 1);
                if (dutyType.StartsWith(",")) dutyType = dutyType.Substring(1, dutyType.Length - 1);
                DutyTYPE = dutyType;
            }
            if (dutyType.Contains(type) == false && include)
            {
                dutyType += (dutyType.Length == 0 ? type : "," + type);
                DutyTYPE = dutyType;
            }
        }
    }
}