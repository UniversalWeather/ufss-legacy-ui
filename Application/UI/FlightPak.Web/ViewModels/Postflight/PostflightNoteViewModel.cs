﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightNoteViewModel
    {
        public string Notes { get; set; }
        public string OriginalNotes { get; set; }
        public long PostflightNoteID { get; set; }
        public FlightPak.Web.PostflightService.TripEntityState State { get; set; }
    }
}