﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightExpenseCatalogViewModel : PostflightLegExpenseViewModel
    {
        public PostflightExpenseCatalogViewModel()
        {
            PostflightLeg = new List<PostflightLegViewModel>();
        }
        public long? LogNum { get; set; }
        public List<PostflightLegViewModel> PostflightLeg { get; set; }
        public bool FromExpenseTab { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}