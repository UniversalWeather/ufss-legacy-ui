﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightLegSIFLViewModel
    {
        public int RowNumber { get; set; }
        public long POSIFLID { get; set; }
        public long? PassengerRequestorID { get; set; }
        public string PassengerRequestorCD { get; set; }

        public decimal? AircraftMultiplier { get; set; }
        public decimal? AmtTotal { get; set; }
        public long? ArriveICAOID { get; set; }
        public long? AssociatedPassengerID { get; set; }
        public long? DepartICAOID { get; set; }
        public decimal? Distance { get; set; }
        public decimal? Distance1 { get; set; }
        public decimal? Distance2 { get; set; }
        public decimal? Distance3 { get; set; }
        public string EmployeeTYPE { get; set; }
        public long? FareLevelID { get; set; }
        public long? POLegID { get; set; }
        public long? POLogID { get; set; }

        
        public bool IsDeleted { get; set; }
        public decimal? LoadFactor { get; set; }
        

        public PostflightLegPassengerViewModel Passenger1 { get; set; }
        public PostflightLegPassengerViewModel AssocPassenger { get; set; }
        
        public AirportViewModel DepartAirport { get; set; }
        public AirportViewModel ArriveAirport { get; set; }

        public decimal? TeminalCharge { get; set; }
        public decimal? SIFLTotal { get; set; }
        public FlightPak.Web.PostflightService.TripEntityState State { get; set; }


        public string alertlbcvAssoc { get; set; }
        public string lbPaxName { get; set; }
        public string alertlbcvPaxName { get; set; }
        public bool AssocPaxEnabled { get; set; }
        public bool btnAssocPaxEnabled { get; set; }
        public string AssocPaxCD { get; set; }
        

        public decimal? Rate1 { get; set; }
        public decimal? Rate2 { get; set; }
        public decimal? Rate3 { get; set; }
        

        
        public PostflightLegSIFLViewModel()
        {
            Passenger1 = new PostflightLegPassengerViewModel();
            AssocPassenger = new PostflightLegPassengerViewModel();
            ArriveAirport = new AirportViewModel();
            DepartAirport = new AirportViewModel();
        }
    }
}