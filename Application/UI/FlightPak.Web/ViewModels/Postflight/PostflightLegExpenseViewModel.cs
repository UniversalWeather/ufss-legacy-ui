﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.ViewModels.MasterData;
using FlightPak.Web.PostflightService;
using System.Web.Script.Serialization;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightLegExpenseViewModel
    {
        public PostflightLegExpenseViewModel()
        {
            Homebase = new HomebaseViewModel();
            Account = new AccountViewModel();
            Fleet = new FleetViewModel();
            Airport = new AirportViewModel();
            FBO = new FBOViewModel();
            FuelLocator = new FuelLocatorViewModel();
            PaymentType = new PaymentTypeViewModel();
            PayableVendor = new PayableVendorViewModel();
            Crew = new CrewViewModel();
            FlightCategory = new FlightCatagoryViewModel();
            PostflightNotes = new List<PostflightNoteViewModel>();
        }
        public Int64 PostflightExpenseID { get; set; }
        public Nullable<Int64> POLogID { get; set; }
        public Nullable<Int64> POLegID { get; set; }
        public Nullable<Int64> CustomerID { get; set; }
        public Nullable<Int64> HomebaseID { get; set; }
        public HomebaseViewModel Homebase { get; set; }
        public Nullable<Int64> SlipNUM { get; set; }
        public Nullable<Int64> LegNUM { get; set; }
        public Nullable<Int64> AccountID { get; set; }
        public AccountViewModel Account { get; set; }
        public String AccountPeriod { get; set; }
        public Nullable<Int64> FleetID { get; set; }
        public FleetViewModel Fleet { get; set; }
        public Nullable<DateTime> PurchaseDT { get; set; }
        public Nullable<Int64> AirportID { get; set; }
        public AirportViewModel Airport { get; set; }
        public Nullable<Int64> FBOID { get; set; }
        public FBOViewModel FBO { get; set; }
        public Nullable<Int64> FuelLocatorID { get; set; }
        public FuelLocatorViewModel FuelLocator { get; set; }
        public Nullable<Int64> PaymentTypeID { get; set; }
        public PaymentTypeViewModel PaymentType { get; set; }
        public Nullable<Int64> PaymentVendorID { get; set; }
        public PayableVendorViewModel PayableVendor { get; set; }
        public Nullable<Int64> CrewID { get; set; }
        public CrewViewModel Crew { get; set; }
        public Nullable<Int64> FlightCategoryID { get; set; }
        public FlightCatagoryViewModel FlightCategory { get; set; }
        public String DispatchNUM { get; set; }
        public String InvoiceNUM { get; set; }
        public Nullable<Decimal> ExpenseAMT { get; set; }
        public Nullable<Decimal> FuelPurchase { get; set; }
        public Nullable<Decimal> FuelQTY { get; set; }
        public Nullable<Decimal> UnitPrice { get; set; }
        public Nullable<Decimal> FederalTAX { get; set; }
        public Nullable<Decimal> SateTAX { get; set; }
        public Nullable<Decimal> SaleTAX { get; set; }
        public Nullable<Boolean> IsAutomaticCalculation { get; set; }
        public Nullable<Boolean> IsBilling { get; set; }
        public String LastUpdUID { get; set; }
        [ScriptIgnore]
        public Nullable<DateTime> LastUpdTS { get; set; }
        public Nullable<Decimal> PostEDPR { get; set; }
        public Nullable<Decimal> PostFuelPrice { get; set; }
        public Boolean IsDeleted { get; set; }
        public TripEntityState State { get; set; }
        public List<PostflightNoteViewModel> PostflightNotes { get; set; }
        public PostflightNoteViewModel PostflightNote
        {
            get
            {
                if (PostflightNotes != null && PostflightNotes.Count() > 0)
                {
                    return PostflightNotes.FirstOrDefault();
                }
                else
                    return new PostflightNoteViewModel();
            }
            set
            {
                if (value != null)
                {
                    if (PostflightNotes == null || PostflightNotes.Count() <= 0)
                    {
                        PostflightNotes = new List<PostflightNoteViewModel>();
                        PostflightNotes.Add(value);
                    }
                    else
                    {
                        PostflightNoteViewModel existing = PostflightNotes.FirstOrDefault();
                        if (existing != null)
                        {
                            existing.OriginalNotes = value.OriginalNotes;
                            existing.State = value.State;
                            existing.Notes = value.Notes;
                        }
                    }
                }
                else
                {
                    PostflightNotes.Clear();
                }
            }
        }
        public int RowNumber { get; set; }
    }
}