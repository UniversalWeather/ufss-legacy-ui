﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.PostflightService;
using FlightPak.Web.Framework.Constants;


namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightLegCrewViewModel 
    {
        private string _ApplicationDateFormat;
        private decimal? _TimeDisplayTenMin;
        private decimal? _HoursMinutesCONV;

        public PostflightLegCrewViewModel()
        {
            InitPOUserPrinicipalData(FlightPak.Web.Views.Transactions.PostFlight.PostflightTripManager.getUserPrincipal());
           
        }
        public PostflightLegCrewViewModel(UserPrincipalViewModel upViewModel)
        {
            InitPOUserPrinicipalData(upViewModel);

        }
        private void InitPOUserPrinicipalData(UserPrincipalViewModel upViewModel)
        {
         
            _ApplicationDateFormat = upViewModel._ApplicationDateFormat;
            _TimeDisplayTenMin = upViewModel._TimeDisplayTenMin;
            _HoursMinutesCONV = upViewModel._HoursMinutesCONV;

        }

        public long RowNumber { get; set; }
        public long PostflightCrewListID { get; set; }
        public long? POLogID { get; set; }
        public long? POLegID { get; set; }
        public long? CustomerID { get; set; }
        public long? CrewID { get; set; }
        public string CrewCode { get; set; }
        public string CrewFirstName { get; set; }
        public string CrewMiddleName { get; set; }
        public string CrewLastName { get; set; }
        public string DutyTYPE { get; set; }
        public decimal? TakeOffDay { get; set; }
        public decimal? TakeOffNight { get; set; }
        public decimal? LandingDay { get; set; }
        public decimal? LandingNight { get; set; }
        public decimal? ApproachPrecision { get; set; }
        public decimal? ApproachNonPrecision { get; set; }
        public decimal? Instrument { get; set; }
        public string InstrumentStr{
            get
            {
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    return ((Instrument == null || Instrument == 0) ? "000.0" : Math.Round(Convert.ToDecimal(Instrument), 1).ToString());
                else
                    return ((Instrument == null || Instrument == 0) ? "00:00" : FPKConversionHelper.ConvertTenthsToMins(Math.Round(Convert.ToDecimal(Instrument), 1).ToString()));
            }
            set
            {
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    Instrument = Convert.ToDecimal(value);
                else
                    Instrument = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(value, true, (int)_HoursMinutesCONV).ToString());
            }
        }
        public decimal? Night { get; set; }
        public string NightStr
        {
            get
            {
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    return ((Night == null || Night == 0) ? "000.0" : Math.Round(Convert.ToDecimal(Night), 1).ToString());
                else
                    return ((Night == null || Night == 0) ? "00:00" : FPKConversionHelper.ConvertTenthsToMins(Math.Round(Convert.ToDecimal(Night), 1).ToString()));
            }
            set
            {
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    Night = Convert.ToDecimal(value);
                else
                    Night = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(value, true, (int)_HoursMinutesCONV).ToString());
            }
        }
        public decimal? RemainOverNight { get; set; }
        public string BeginningDuty { get; set; }
        public string DutyEnd { get; set; }
        public decimal? DutyHours { get; set; }
        public string DutyHrsTime
        {
            get
            {
                string hours = string.Empty;
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    hours = (DutyHours == null ? "0.0" : Convert.ToDecimal(DutyHours).ToString("#0.0"));
                else
                    hours = (DutyHours == null ? "00:00" : FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(DutyHours)).ToString());
                return hours;
            }
            set
            {

                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    DutyHours = Math.Round(Convert.ToDecimal(value), 3);
                else
                {
                    if(value.Contains(':'))
                        DutyHours = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(value, true, 0).ToString());
                    else
                        DutyHours = Math.Round(Convert.ToDecimal(value), 3);
                }
            }
        }    
        public bool? IsOverride { get; set; }
        public bool? Discount { get; set; }
        public decimal? BlockHours { get; set; }
        public string BlockHoursTime {
            get
            {
                string hours = string.Empty;
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    hours = (BlockHours == null ? "0.0" : Convert.ToDecimal(BlockHours).ToString("#0.0"));
                else
                    hours = (BlockHours == null ? "00:00" : FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(BlockHours)).ToString());
                return hours;
            }
            set
            {
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    BlockHours = Math.Round(Convert.ToDecimal(value), 1);
                else
                    BlockHours = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(value, true, 0).ToString());
                    
            }
        }
        public decimal? FlightHours { get; set; }
        public string FlightHoursTime
        {
            get
            {
                string hours = string.Empty;
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    hours = (FlightHours == null ? "0.0" : Convert.ToDecimal(FlightHours).ToString("#0.0"));
                else
                    hours = (FlightHours == null ? "00:00" : FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(FlightHours)).ToString());
                return hours;
            }
            set
            {
                if (_TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    FlightHours = Math.Round(Convert.ToDecimal(value), 1);
                else
                    FlightHours = Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(value, true, 0).ToString());
                    
            }
        }
        public bool? IsAugmentCrew { get; set; }
        public bool? IsRemainOverNightOverride { get; set; }
        public string Seat { get; set; }
        public string LastUpdUID { get; set; }
		[ScriptIgnore]
        public DateTime? LastUpdTS { get; set; }
		[ScriptIgnore]
        public DateTime? OutboundDTTM { get; set; }
        public string OutDate
        {
            get
            {
                string date = OutboundDTTM.FSSParseDateTimeString(_ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                OutboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : OutboundDTTM.FSSParseDateTime(value, _ApplicationDateFormat);
            }
        }
        public string OutTime
        {
            get
            {
                string time = OutboundDTTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? ":" : time;
            }
            set
            {
                OutboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : OutboundDTTM.FSSParseDateTime(string.Format("{0} {1}", OutDate, value), _ApplicationDateFormat + " HH:mm");
            }
        }
		[ScriptIgnore]
        public DateTime? InboundDTTM { get; set; }
        public string InDate
        {
            get
            {
                string date = InboundDTTM.FSSParseDateTimeString(_ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                InboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : InboundDTTM.FSSParseDateTime(value, _ApplicationDateFormat);
            }
        }
        public string InTime
        {
            get
            {
                string time = InboundDTTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? ":" : time;
            }
            set
            {
                InboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : InboundDTTM.FSSParseDateTime(string.Format("{0} {1}", InDate, value), _ApplicationDateFormat + " HH:mm");
            }
        }
        [ScriptIgnore]
        public DateTime? CrewDutyStartTM { get; set; }
        public string StartTime { get; set; }
        public string CrewDutyStartDate
        {
            get
            {
                string date = CrewDutyStartTM.FSSParseDateTimeString(_ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                CrewDutyStartTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : CrewDutyStartTM.FSSParseDateTime(value, _ApplicationDateFormat);
            }
        }
        public string CrewDutyStartTime
        {
            get
            {
                string time = CrewDutyStartTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? ":" : time;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(CrewDutyStartDate))
                {
                    if (value == ":") value = "12:00";
                    CrewDutyStartTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : CrewDutyStartTM.FSSParseDateTime(string.Format("{0} {1}", CrewDutyStartDate, value), _ApplicationDateFormat + " HH:mm");
                if (value!=":")
                StartTime = value;
                }
           
            }
        }
		[ScriptIgnore]
        public DateTime? CrewDutyEndTM { get; set; }
        public string CrewDutyEndDate
        {
            get
            {
                string date = CrewDutyEndTM.FSSParseDateTimeString(_ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                CrewDutyEndTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : CrewDutyEndTM.FSSParseDateTime(value, _ApplicationDateFormat);
            }
        }
        public string EndTime { get; set; }
        public string CrewDutyEndTime
        {
            get
            {
                string time = CrewDutyEndTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? ":" : time;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(CrewDutyEndDate))
                {
                    if (value == ":") value = "12:00";
                    CrewDutyEndTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : CrewDutyEndTM.FSSParseDateTime(string.Format("{0} {1}", CrewDutyEndDate, value), _ApplicationDateFormat + " HH:mm");
                }
                if (value != ":")
                    EndTime = value;
            }
        }
        public bool? ItsNew { get; set; }
        public bool? Specification1 { get; set; }
        public bool? Sepcification2 { get; set; }
        public decimal? Specification3 { get; set; }
        public decimal? Specification4 { get; set; }
        public decimal? DaysAway { get; set; }
        public Int32? OrderNUM { get; set; }
        public TripEntityState State { get; set; }
        public bool IsDeleted { get; set; }
    }
}