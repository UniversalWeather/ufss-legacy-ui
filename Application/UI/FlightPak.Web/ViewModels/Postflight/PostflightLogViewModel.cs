﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightLogViewModel
    {
        public long POLogID { get; set; }

        public long? FleetID { get; set; }

        public long? CurrentLegNUM { get; set; }

        public PostflightMainViewModel PostflightMain { get; set; }

        public List<PostflightLegViewModel> PostflightLegs { get; set; }

        public List<PostflightLegSIFLViewModel> PostflightLegSIFLsList { get; set; }

        public PostflightLogViewModel()
        {
            PostflightMain = new PostflightMainViewModel();
            this.PostflightLegs = new List<PostflightLegViewModel>();
            this.PostflightLegs.Add(new PostflightLegViewModel() { LegNUM=1, IsDeleted=false});
            this.CurrentLegNUM = 1;
            this.PostflightLegSIFLsList = new List<PostflightLegSIFLViewModel>();
        }
    }
}