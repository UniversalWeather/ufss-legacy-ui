﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using FlightPak.Web.PostflightService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightLegViewModel
    {
        public PostflightLegViewModel()
        {
            InitializeComponent();
        }

        public PostflightLegViewModel(UserPrincipalViewModel userPrincipal)
        {
            InitializeComponent();
            if (userPrincipal != null)
            {
                if (userPrincipal._TimeDisplayTenMin != null)
                    if (userPrincipal._TimeDisplayTenMin > 0)
                        this.TimeDisplayTenMin = (decimal) userPrincipal._TimeDisplayTenMin;

                if (userPrincipal._HoursMinutesCONV != null)
                    if (userPrincipal._HoursMinutesCONV > 0)
                        this.HoursMinutesCONV = (int) userPrincipal._HoursMinutesCONV;

                this.IsKilometer = (bool)userPrincipal._IsKilometer;

            }
        }

        public decimal TimeDisplayTenMin { get; set; }
        public int HoursMinutesCONV { get; set; }
        public bool IsKilometer { get; set; }

        public long? LegNUM { get; set; }
        public DateTime? DepartureDTTMLocal { get; set; }
        public bool IsDeleted { get; set; }

        //public string AVTrak { get; set; }
        public long? AccountID { get; set; }
        public decimal? AdditionalDist { get; set; }
        public decimal? AirFrameHours { get; set; }
        
        public string AirFrameHoursTime
        {
            get { return SetPropertyDecimalValue(AirFrameHours); }
            set { AirFrameHours = GetDecimalValue(value); }
        }
        public decimal? AirframeCycle { get; set; }
        public long? ArrivalFBO { get; set; }
        public long? ArriveICAOID { get; set; }
        public long? AuthorizationID { get; set; }
        public string BeginningDutyHours { get; set; }
        public decimal? BlockHours { get; set; }
        public string BlockHoursTime
        {
            get { return SetPropertyDecimalValue(BlockHours); }
            set { BlockHours = GetDecimalValue(value); }
        }
        public string BlockIN { get; set; }
        public string BlockInLocal { get; set; }
        public string BlockOut { get; set; }
        public string BlockOutLocal { get; set; }
        public decimal? CargoIn { get; set; }
        public decimal? CargoOut { get; set; }
        public decimal? CargoThru { get; set; }
        public long? ClientID { get; set; }
        public string CrewCurrency { get; set; }
        public long? CrewDutyRulesID { get; set; }
        public long? CrewID { get; set; }
        public long? CustomerID { get; set; }
        public decimal? DelayTM { get; set; }
        public string DelayTMTime
        {
            get { return SetPropertyDecimalValue(DelayTM); }
            set { DelayTM = GetDecimalValue(value); }
        }
        public long? DelayTypeID { get; set; }
        public long? DepartICAOID { get; set; }
        public long? DepartmentID { get; set; }
        public long? DepatureFBO { get; set; }
        public string Description { get; set; }
        public decimal? Distance { get; set; }
        public decimal ConvertedDistance
        {
            get { return (IsKilometer ? Math.Round((Distance ?? 0) * 1.852M, 0) : (Distance ?? 0)); }
            set { Distance = IsKilometer ? Math.Round(value / 1.852M, 0) : value; }
        }
        public decimal? DutyHrs { get; set; }
        public string DutyHrsStr { get; set; }
        public string DutyHrsTime
        {
            get { return SetPropertyDecimalValue(DutyHrs); }
            set { DutyHrs = GetDecimalValue(value); }
        }
        public decimal? DutyTYPE { get; set; }
        public string EndDutyHours { get; set; }
        public decimal? Engine1Cycle { get; set; }
        public decimal? Engine1Hours { get; set; }
        public string Engine1HoursTime
        {
            get { return SetPropertyDecimalValue(Engine1Hours); }
            set { Engine1Hours = GetDecimalValue(value); }
        }
        public decimal? Engine2Cycle { get; set; }
        public decimal? Engine2Hours { get; set; }
        public string Engine2HoursTime
        {
            get { return SetPropertyDecimalValue(Engine2Hours); }
            set { Engine2Hours = GetDecimalValue(value); }
        }
        public decimal? Engine3Cycle { get; set; }
        public decimal? Engine3Hours { get; set; }
        public string Engine3HoursTime
        {
            get { return SetPropertyDecimalValue(Engine3Hours); }
            set { Engine3Hours = GetDecimalValue(value); }
        }
        public decimal? Engine4Cycle { get; set; }
        public decimal? Engine4Hours { get; set; }
        public string Engine4HoursTime
        {
            get { return SetPropertyDecimalValue(Engine4Hours); }
            set { Engine4Hours = GetDecimalValue(value); }
        }
        public string FedAviationRegNum { get; set; }
        public long? FlightCategoryID { get; set; }
        public decimal? FlightCost { get; set; }
        public decimal? FlightHours { get; set; }
        public string FlightHoursStr { get; set; }
        public string FlightHoursTime
        {
            get { return SetPropertyDecimalValue(FlightHours); }
            set { FlightHours = GetDecimalValue(value); }
        }
        public string FlightNum { get; set; }
        public string FlightPurpose { get; set; }
        public decimal? FuelBurn { get; set; }
        public decimal? FuelIn { get; set; }
        public decimal? FuelOn { get; set; }
        public decimal? FuelOut { get; set; }
        public decimal? FuelUsed { get; set; }
        [ScriptIgnore]
        public DateTime? InboundDTTM { get; set; }
        private string _InDate;
        public string InDate
        {
            get
            {
                string date = InboundDTTM.FSSParseDateTimeString(WebConstants.isoDateFormat);
                return string.IsNullOrWhiteSpace(date) ? "" : date;
            }
            set
            {
                _InDate = value;
                InboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : InboundDTTM.FSSParseDateTime(value, WebConstants.isoDateFormat);
            }
        }
        public string InTime
        {
            get
            {
                string time = InboundDTTM.FSSParseDateTimeString("HH:mm");
                time = (time == "00:00" ? "" : time);
                return string.IsNullOrWhiteSpace(time) ? (string.IsNullOrWhiteSpace(BlockIN) ? "" : BlockIN) : time;
            }
            set
            {
                BlockIN = value;
                if (string.IsNullOrWhiteSpace(_InDate) == false)
                    InboundDTTM = string.IsNullOrWhiteSpace(value) ? InboundDTTM : InboundDTTM.FSSParseDateTime(string.Format("{0} {1}", _InDate, value), WebConstants.isoDateTimeFormat);
            }
        }
        public bool? IsAugmentCrew { get; set; }
        public bool? IsDutyEnd { get; set; }
        public bool? IsException { get; set; }
        [ScriptIgnore]
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdUID { get; set; }
        [ScriptIgnore]
        public DateTime? NextDTTM { get; set; }
        [ScriptIgnore]
        public DateTime? OutboundDTTM { get; set; }
        private string _OutDate;
        public string OutDate
        {
            get
            {
                string date = OutboundDTTM.FSSParseDateTimeString(WebConstants.isoDateFormat);
                return string.IsNullOrWhiteSpace(date) ? "" : date;
            }
            set
            {
                _OutDate = value;
                OutboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : OutboundDTTM.FSSParseDateTime(value, WebConstants.isoDateFormat);
            }
        }
        public string OutTime
        {
            get
            {
                string time = OutboundDTTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrWhiteSpace(time) ? (string.IsNullOrWhiteSpace(BlockOut) ? "" : BlockOut) : time;
            }
            set
            {
                BlockOut = value;
                if (string.IsNullOrWhiteSpace(_OutDate) == false)
                    OutboundDTTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : OutboundDTTM.FSSParseDateTime(string.Format("{0} {1}", _OutDate, value), WebConstants.isoDateTimeFormat);
            }
        }
        public long POLegID { get; set; }
        public long? POLogID { get; set; }
        public long? PassengerRequestorID { get; set; }
        public decimal? PassengerTotal { get; set; }
        public decimal? Re1Cycle { get; set; }
        public decimal? Re1Hours { get; set; }
        public string Re1HoursTime
        {
            get { return SetPropertyDecimalValue(Re1Hours); }
            set { Re1Hours = GetDecimalValue(value); }
        }
        public decimal? Re2Cycle { get; set; }
        public decimal? Re2Hours { get; set; }
        public string Re2HoursTime
        {
            get { return SetPropertyDecimalValue(Re2Hours); }
            set { Re2Hours = GetDecimalValue(value); }
        }
        public decimal? Re3Cycle { get; set; }
        public decimal? Re3Hours { get; set; }
        public string Re3HoursTime
        {
            get { return SetPropertyDecimalValue(Re3Hours); }
            set { Re3Hours = GetDecimalValue(value); }
        }
        public decimal? Re4Cycle { get; set; }
        public decimal? Re4Hours { get; set; }
        public string Re4HoursTime
        {
            get { return SetPropertyDecimalValue(Re4Hours); }
            set { Re4Hours = GetDecimalValue(value); }
        }
        public decimal? RestHrs { get; set; }
        [ScriptIgnore]
        public DateTime? ScheduleDTTMLocal { get; set; }
        [ScriptIgnore]
        public DateTime? ScheduledTM { get; set; }
        private string _ScheduledDate;
        public string ScheduledDate
        {
            get
            {
                string date = ScheduledTM.FSSParseDateTimeString(WebConstants.isoDateFormat);
                return string.IsNullOrWhiteSpace(date) ? "" : date;
            }
            set
            {
                _ScheduledDate = value;
                ScheduledTM = string.IsNullOrWhiteSpace(value) ? (DateTime?)null : ScheduledTM.FSSParseDateTime(value, WebConstants.isoDateFormat);
            }
        }
        public string ScheduledTime
        {
            get
            {
                string time = ScheduledTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrWhiteSpace(time) ? "" : time;
            }
            set
            {
                ScheduledTM = string.IsNullOrWhiteSpace(value) ? (DateTime?) null : ScheduledTM.FSSParseDateTime(string.Format("{0} {1}", _ScheduledDate, value), WebConstants.isoDateTimeFormat);
            }
        }
        public decimal? StatuteMiles { get; set; }
        public string TechLog { get; set; }
        public string TimeOff { get; set; }
        public string TimeOffLocal { get; set; }
        public string TimeOn { get; set; }
        public string TimeOnLocal { get; set; }
        public long? TripLegID { get; set; }
        public TripEntityState State { get; set; }
        //public string CheckGroup { get; set; }
        public string CrewDutyRulesDescription { get; set; }
        public string CrewRulesToolTipInfo { get; set; }

        #region AIRPORT
        public AirportViewModel ArrivalAirport { get; set; }
        public AirportViewModel DepartureAirport { get; set; }
        #endregion

        public List<PostflightLegCrewViewModel> PostflightLegCrews { get; set; }
        public List<PostflightLegPassengerViewModel> PostflightLegPassengers { get; set; }
        public List<PostflightLegExpenseViewModel> PostflightLegExpensesList { get; set; }
        
        public PostflightLegPassengerViewModel Passenger { get; set; }
        public FlightCatagoryViewModel FlightCatagory { get; set; }
        public CrewViewModel Crew { get; set; }
        public ClientViewModel Client { get; set; }
        public DepartmentViewModel Department { get; set; }
        public DepartmentAuthorizationViewModel DepartmentAuthorization { get; set; }
        public PostflightLegDelayTypeViewModel DelayType { get; set; }
        public CrewDutyRuleViewModel CrewDutyRule { get; set; }
        public AccountViewModel Account { get; set; }
        //public HomebaseViewModel Homebase { get; set; }

        private decimal GetDecimalValue(string propValue)
        {
            return TimeDisplayTenMin == 0
                ? 0
                : ((TimeDisplayTenMin == POTenthMinuteFormat.Tenth)
                    ? (propValue.Contains(":") 
                        ? Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(propValue, true, 0))
                        : Math.Round(Convert.ToDecimal(propValue), 3))
                    : Convert.ToDecimal(FPKConversionHelper.ConvertMinToTenths(propValue, true, 0)));
        }
        private string SetPropertyDecimalValue(decimal? propValue)
        {
            return (TimeDisplayTenMin == POTenthMinuteFormat.Tenth) ? (propValue == null ? "0.0" : Convert.ToDecimal(propValue).ToString("#0.0")) : FPKConversionHelper.ConvertTenthsToMins(Convert.ToString(propValue));
        }
        private void InitializeComponent()
        {
            this.PostflightLegPassengers = new List<PostflightLegPassengerViewModel>();
            this.PostflightLegCrews = new List<PostflightLegCrewViewModel>();
            this.PostflightLegExpensesList = new List<PostflightLegExpenseViewModel>();

            this.Passenger = new PostflightLegPassengerViewModel();
            this.FlightCatagory = new FlightCatagoryViewModel();
            this.Crew = new CrewViewModel();
            this.Client = new ClientViewModel();
            this.Department = new DepartmentViewModel();
            this.DepartmentAuthorization = new DepartmentAuthorizationViewModel();
            this.DelayType = new PostflightLegDelayTypeViewModel();
            this.ArrivalAirport = new AirportViewModel();
            this.DepartureAirport = new AirportViewModel();
            this.CrewDutyRule = new CrewDutyRuleViewModel();
            this.Account = new AccountViewModel();
            //this.Homebase= new HomebaseViewModel();

            this.Distance = 0;
            //this.StatuteMiles = 0;
            this.AirframeCycle = 0;
            this.Engine1Cycle = 0;
            this.Engine2Cycle = 0;
            this.Engine3Cycle = 0;
            this.Engine4Cycle = 0;
            this.Re1Cycle = 0;
            this.Re2Cycle = 0;
            this.Re3Cycle = 0;
            this.Re4Cycle = 0;
            this.FuelOn = 0;           
        }
    }
}