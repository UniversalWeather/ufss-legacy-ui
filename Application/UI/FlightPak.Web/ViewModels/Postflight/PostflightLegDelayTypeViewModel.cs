﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightLegDelayTypeViewModel
    {
        public PostflightLegDelayTypeViewModel()
        {
            this.DelayTypeID = 0;
        }
        public string DelayTypeCD { get; set; }
        public string DelayTypeDescription { get; set; }
        public string _DelayTypeDescShort { get; set; }
        public string DelayTypeDescShort
        {
            get
            {
                string strDesc = string.Empty;
                if (DelayTypeDescription != null)
                    strDesc = DelayTypeDescription.Length > 22 ? DelayTypeDescription.Substring(0, 22) + "..." : DelayTypeDescription;
                return strDesc;
            }
            set { _DelayTypeDescShort = value; }
        }

        public long DelayTypeID { get; set; }
        public Nullable<long> ClientID { get; set; }
        public Nullable<long> CustomerID { get; set; }
        //public bool IsDeleted { get; set; }
        //public Nullable<bool> IsInActive { get; set; }
        public Nullable<bool> IsSiflExcluded { get; set; }
    }
}