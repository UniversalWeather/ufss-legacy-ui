﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.PostflightService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.BusinessLite.Postflight;
using System.Web.Script.Serialization;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class PostflightMainViewModel
    {
        public long? LogNum { get; set; }
        public long POLogID { get; set; }
        public string dateFormat { get; set; }
        
        public string DispatchNum { get; set; }
        public long? AccountID { get; set; }
        public long? AircraftID { get; set; }
        public string AuthorizationDescription { get; set; }
        public long? AuthorizationID { get; set; }
        public long? ClientID { get; set; }
        public long? CustomerID { get; set; }

        [ScriptIgnore]
        public Dictionary<long,long> DeletedCrewIDs { get; set; }
        [ScriptIgnore]
        public Dictionary<long,long> DeletedExceptionIDs { get; set; }
        [ScriptIgnore]
        public Dictionary<long,long> DeletedExpenseIDs { get; set; }

        public List<long> DeletedLegIDs { get; set; }
        [ScriptIgnore]
        public Dictionary<long,long> DeletedPAXIDs { get; set; }
        [ScriptIgnore]
        public Dictionary<long,long> DeletedSIFLIDs { get; set; }
        public string DepartmentDescription { get; set; }
        public long? DepartmentID { get; set; }
        
        [ScriptIgnore]
        public DateTime? EstDepartureDT { get; set; }
        public long? FleetID { get; set; }
        public string FlightNum { get; set; }
        public string History { get; set; }
        public long? HomebaseID { get; set; }
        public bool? IsCompleted { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsException { get; set; }
        public bool? IsLastAdj { get; set; }
        public bool? IsPersonal { get; set; }
        public bool IsPostedFromPreflight { get; set; }
        /// <summary>
        /// This Property we are using to identify if Log created via Postflight > RetreiveTrip
        /// </summary>
        public bool IsTripToSaveFromPreflightLogEvent { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdTSStr { get; set; }
        public string LastUpdUID { get; set; }        
        public string Notes { get; set; }
        public long? PassengerRequestorID { get; set; }
        public string POMainDescription { get; set; }
        public object RecordLockInfo { get; set; }
        public string RecordType { get; set; }
        public string RequestorName { get; set; }
        public TripEntityState State { get; set; }
        public string TechLog { get; set; }
        public TripAction TripActionStatus { get; set; }
        public long? TripID { get; set; }
        [ScriptIgnore]
        public DateTime TripLastUpdatedOn { get; set; }
        public string TripLastUpdatedUser { get; set; }

        public HomebaseViewModel Homebase { get; set; }
        public ClientViewModel Client { get; set; }
        public FleetViewModel Fleet { get; set; }
        public AircraftViewModel Aircraft { get; set; }
        public AccountViewModel Account { get; set; }
        public CompanyViewModel Company { get; set; }
        public CQCustomerViewModel Customer { get; set; }
        public DepartmentViewModel Department { get; set; }
        public DepartmentAuthorizationViewModel DepartmentAuthorization { get; set; }
        public PostflightLegPassengerViewModel Passenger { get; set; }
        public AirportViewModel Airport { get; set; }

        
        public long? _airportId { get; set; }
        public string TypeCode { get; set; } // Just to show on PostflightMain label control
        public bool POEditMode { get; set; }
        public string TailNum { get; set; }  // Just to show on PostflightMain label control

        public String LastUpdatedBy { get; set; }

        public PostflightMainViewModel(string _ApplicationDateFormat)
        {
            InitMembers(_ApplicationDateFormat);
        }

        public PostflightMainViewModel()
        {
            InitMembers("");
        }

        private void InitMembers(string _ApplicationDateFormat)
        {
            if (!string.IsNullOrWhiteSpace(_ApplicationDateFormat))
            {
                dateFormat = _ApplicationDateFormat;
            }
            else
            {
                dateFormat = "mm/DD/yyyy";
            }
            POEditMode = false;
            Client = new ClientViewModel();
            Fleet = new FleetViewModel();
            Aircraft = new AircraftViewModel();
            Homebase = new HomebaseViewModel();
            Airport = new AirportViewModel();
            Customer = new CQCustomerViewModel();
            Passenger = new PostflightLegPassengerViewModel();
            Department = new DepartmentViewModel();
            DepartmentAuthorization = new DepartmentAuthorizationViewModel();
            Company = new CompanyViewModel();
            DeletedExpenseIDs = new Dictionary<long, long>();
            DeletedCrewIDs = new Dictionary<long, long>();
            DeletedExceptionIDs = new Dictionary<long, long>();
            DeletedLegIDs = new List<long>();
            DeletedPAXIDs = new Dictionary<long, long>();
            DeletedSIFLIDs = new Dictionary<long, long>();
        }
       
    }
}