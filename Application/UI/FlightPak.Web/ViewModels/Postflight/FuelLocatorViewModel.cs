﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class FuelLocatorViewModel
    {
        public String ClientCD { get; set; }
        public long? ClientID { get; set; }
        public long? CustomerID { get; set; }
        public String FuelLocatorCD { get; set; }
        public String FuelLocatorDescription { get; set; }
        public long FuelLocatorID { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsInactive { get; set; }
    }
}