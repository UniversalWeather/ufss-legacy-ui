﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.Postflight
{
    public class CrewDutyTypeViewModel
    {
        public Int64  DutyTypeID { get; set; }
        public string DutyTypeCD { get; set; }
        public string DutyTypesDescription { get; set; }
    }
}