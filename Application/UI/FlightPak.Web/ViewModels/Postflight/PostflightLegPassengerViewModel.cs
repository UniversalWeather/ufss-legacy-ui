﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.PostflightService;
using System.Web.Script.Serialization;

namespace FlightPak.Web.ViewModels.Postflight
{
    [Serializable]
    public class PostflightLegPassengerViewModel
    {
        public int? RowNumber { get; set; }

        // Tag P: Below are prop of PreflightMain.Passenger object.
        public string PassengerName { get; set; }
        public string AdditionalPhoneNum { get; set; }
        public string TSAStatus { get; set; }
        public string PhoneNum { get; set; }
        [ScriptIgnore]
        public DateTime? TSADTTM { get; set; }

        public string Title { get; set; }
        public string StateName { get; set; }
        public string StandardBilling { get; set; }
        public string SSN { get; set; }
        public decimal? SalaryLevel { get; set; }
        public int? ResidentSinceYear { get; set; }
        public string PrimaryMobile { get; set; }
        public string PersonalIDNum { get; set; }
        public string PersonalEmail { get; set; }
        public string PaxTitle { get; set; }
        public string PaxScanDoc { get; set; }
        public string PassengerWeightUnit { get; set; }
        public decimal? PassengerWeight { get; set; }


        // -Added by Snehalshah

        public long PassengerRequestorID { get; set; }
        public string PassengerRequestorCD { get; set; }
        public string Billing { get; set; }
        public long? CustomerID { get; set; }
        public decimal? DepartPercentage { get; set; }
        public long? FlightPurposeID { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsNonPassenger { get; set; }
        public int? OrderNUM { get; set; }
        public string PassengerFirstName { get; set; }
        public long? PassengerID { get; set; }
        public string PassengerLastName { get; set; }
        public string PassengerMiddleName { get; set; }
        public string PassportNUM { get; set; }
        public long? POLegID { get; set; }
        public long? POLogID { get; set; }
        public long PostflightPassengerListID { get; set; }
        public TripEntityState State { get; set; }

        public string WaitList { get; set; }


        // Extra properties as Label and Error Messages
        public string lbPaxName { get; set; }
        public string alertlbcvPaxName { get; set; }
        public string AssociatedWithCD { get; set; }
        public bool? AssocPaxEnabled { get; set; }
        public long? AssocPaxID { get; set; }
        public string AssocPaxName { get; set; }
        public string alertlbcvAssoc { get; set; }

        public string IsEmployeeType { get; set; }        
        public bool SIFLSecurity { get; set; }

        public string SIFLMultiControlled { get; set; }
        public bool? btnAssocPaxEnabled { get; set; }
        public decimal? TerminalCHG { get; set; }

        public decimal? Rate1 { get; set; }
        public decimal? Rate2 { get; set; }
        public decimal? Rate3 { get; set; }
        public string Miles1 { get; set; }
        public string Miles2 { get; set; }
        public string Miles3 { get; set; }

        public bool? IsSIFL { get; set; }
        public decimal? MultiSec { get; set; }

        public string TripStatus { get; set; }
        public string FlightPurpose { get; set; }
        public long? PassportID { get; set; }
        public long? VisaID { get; set; }
        public string PassportExpiryDT { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string LegDescription { get; set; }
        public string IssueDT { get; set; }
        public string Nation { get; set; }
        public long? LegNUM { get; set; }

        public string RequestorDesc { get; set; }
        
        public bool? Choice { get; set; }
        [ScriptIgnore]
        public DateTime? DateOfBirth { get; set; }
    }
}