﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class DepartmentViewModel
    {
        public Int64 DepartmentID
        {
            get;
            set;
        }
        public String DepartmentCD { get; set; }
        public String DepartmentName { get; set; }
    }
}