﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class CrewPassportViewModel
    {
        public bool? Choice { get; set; }
        public string CountryCD { get; set; }
        public long? CountryID { get; set; }
        public string CountryName { get; set; }
        public long? CrewID { get; set; }
        public long? CustomerID { get; set; }
        public bool? IsDefaultPassport { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsInActive { get; set; }
        public string IssueCity { get; set; }
        public string IssueDT { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdUID { get; set; }
        public long? PassengerRequestorID { get; set; }
        public string PassportExpiryDT { get; set; }
        public long PassportID { get; set; }
        public string PassportNum { get; set; }
        public string PilotLicenseNum { get; set; }

    }
}