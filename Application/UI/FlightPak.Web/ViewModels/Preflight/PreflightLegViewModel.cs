﻿using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Helpers.Preflight;
using FlightPak.Web.Framework.Helpers.Validators;
using FlightPak.Web.PostflightService;
using FlightPak.Web.Views.Transactions.Preflight;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Telerik.Web.UI;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegViewModel
    {
        public PreflightLegViewModel()
        {
            OverrideValue = 0;
        }
        public FPKConversionHelper Utilities = new FPKConversionHelper();
        public PreflightUtils PreUtilities = new PreflightUtils();
        UserPrincipalViewModel _userprincipal;
        public UserPrincipalViewModel UserPrincipal
        {
            get { return (UserPrincipalViewModel)HttpContext.Current.Session["userprincipal"]; }
        }
        string _powersetting;
        #region LEG DATETIME

        private string _ArrivalLocalDate;
        public string ArrivalLocalDate {
            get
            {
                string date = ArrivalDTTMLocal.FSSParseDateTimeString(UserPrincipal._ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _ArrivalLocalDate = value;
                ArrivalDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : ArrivalDTTMLocal.FSSParseDateTime(value, UserPrincipal._ApplicationDateFormat);
            }
        }
        public DateTime? ArrivalDTTMLocal { get; set; }

        private string _ArrivalLocalTime;
        public string ArrivalLocalTime
        {
            get
            {
                string time = ArrivalDTTMLocal.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? "00:00" : time;
            }
            set
            {
                _ArrivalLocalTime = value;
                ArrivalDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : ArrivalDTTMLocal.FSSParseDateTime(string.Format("{0} {1}", _ArrivalLocalDate, value), UserPrincipal._ApplicationDateFormat + " HH:mm");
            }
        }

        private string _HomeArrivalDate;
        public string HomeArrivalDate
        {
            get
            {
                string date = HomeArrivalDTTM.FSSParseDateTimeString(UserPrincipal._ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _HomeArrivalDate = value;
                HomeArrivalDTTM = string.IsNullOrEmpty(value) ? null : HomeArrivalDTTM.FSSParseDateTime(value, UserPrincipal._ApplicationDateFormat);
            }
        }
        public DateTime? HomeArrivalDTTM { get; set; }

        private string _HomeArrivalTime;
        public string HomeArrivalTime
        {
            get
            {
                string time = HomeArrivalDTTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? "00:00" : time;
            }
            set
            {
                _HomeArrivalTime = value;
                HomeArrivalDTTM = string.IsNullOrEmpty(value) ? (DateTime?)null : HomeArrivalDTTM.FSSParseDateTime(string.Format("{0} {1}", _HomeArrivalDate, value), UserPrincipal._ApplicationDateFormat + " HH:mm");
            }
        }

        private string _ArrivalGreenwichDate;
        public string ArrivalGreenwichDate {
            get
            {
                string date = ArrivalGreenwichDTTM.FSSParseDateTimeString(UserPrincipal._ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _ArrivalGreenwichDate = value;
                ArrivalGreenwichDTTM = string.IsNullOrEmpty(value) ? (DateTime?)null : ArrivalGreenwichDTTM.FSSParseDateTime(value, UserPrincipal._ApplicationDateFormat);
            } 
        }
        public DateTime? ArrivalGreenwichDTTM { get; set; }

        private string _ArrivalGreenwichTime;
        public string ArrivalGreenwichTime
        {
            get
            {
                string time = ArrivalGreenwichDTTM.FSSParseDateTimeString("HH:mm");
                return string.IsNullOrEmpty(time) ? "00:00" : time;
            }
            set
             {
                _ArrivalGreenwichTime = value;
                ArrivalGreenwichDTTM = string.IsNullOrEmpty(value) ? (DateTime?)null : ArrivalGreenwichDTTM.FSSParseDateTime(string.Format("{0} {1}", _ArrivalGreenwichDate, value), UserPrincipal._ApplicationDateFormat + " HH:mm");
            }
        }

        private string _DepartureLocalDate;
        public string DepartureLocalDate
        {
            get
            {
                string date = DepartureDTTMLocal.FSSParseDateTimeString(UserPrincipal._ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _DepartureLocalDate = value;
                DepartureDTTMLocal = string.IsNullOrEmpty(value) ? null : DepartureDTTMLocal.FSSParseDateTime(value, UserPrincipal._ApplicationDateFormat);
            }
        }
        public DateTime? DepartureDTTMLocal { get; set; }

        private string _DepartureLocalTime;
        public string DepartureLocalTime
        {
            get
            {
                return DepartureDTTMLocal == null? "00:00": DepartureDTTMLocal.FSSParseDateTimeString("HH:mm");
            }
            set
            {
                _DepartureLocalTime = value;
                DepartureDTTMLocal = string.IsNullOrEmpty(value) ? (DateTime?)null : DepartureDTTMLocal.FSSParseDateTime(string.Format("{0} {1}", _DepartureLocalDate, value), UserPrincipal._ApplicationDateFormat + " HH:mm");
            }
        }

        private string _DepartureGreenwichDate;
        public string DepartureGreenwichDate
        {
            get
            {
                string date = DepartureGreenwichDTTM.FSSParseDateTimeString(UserPrincipal._ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _DepartureGreenwichDate = value;
                DepartureGreenwichDTTM = string.IsNullOrEmpty(value) ? null : DepartureGreenwichDTTM.FSSParseDateTime(value, UserPrincipal._ApplicationDateFormat);
            }
        }
        public DateTime? DepartureGreenwichDTTM { get; set; }

        private string _DepartureGreenwichTime;
        public string DepartureGreenwichTime
        {
            get
            {
                return DepartureGreenwichDTTM == null ? "00:00" : DepartureGreenwichDTTM.FSSParseDateTimeString("HH:mm");
            }
            set
            {
                _DepartureGreenwichTime = value;
                DepartureGreenwichDTTM = string.IsNullOrEmpty(value) ? (DateTime?)null : DepartureGreenwichDTTM.FSSParseDateTime(string.Format("{0} {1}", _DepartureGreenwichDate, value), UserPrincipal._ApplicationDateFormat + " HH:mm");
            }
        }

        private string _HomeDepartureDate;
        public string HomeDepartureDate
        {
            get
            {
                string date = HomeDepartureDTTM.FSSParseDateTimeString(UserPrincipal._ApplicationDateFormat);
                return string.IsNullOrEmpty(date) ? "" : date;
            }
            set
            {
                _HomeDepartureDate = value;
                HomeDepartureDTTM = string.IsNullOrEmpty(value) ? null : HomeDepartureDTTM.FSSParseDateTime(value, UserPrincipal._ApplicationDateFormat);
            }
        }
        public DateTime? HomeDepartureDTTM { get; set; }

        private string _HomeDepartureLocal;
        public string HomeDepartureLocal
        {
            get
            {
                return HomeDepartureDTTM == null ? "00:00" : HomeDepartureDTTM.FSSParseDateTimeString("HH:mm");
            }
            set
            {
                _HomeDepartureLocal = value;
                HomeDepartureDTTM = string.IsNullOrEmpty(value) ? (DateTime?)null : HomeDepartureDTTM.FSSParseDateTime(string.Format("{0} {1}", _HomeDepartureDate, value), UserPrincipal._ApplicationDateFormat + " HH:mm");
            }
        }
        #endregion

        #region DEPARTURE
        public long? DepartICAOID {
            get
            {
                if(DepartureAirport != null)
                {
                    return DepartureAirport.AirportID;
                }
                return 0;
            }
            set
            {
                if(DepartureAirport != null)
                {
                    DepartureAirport.AirportID = value.Value;
                }
            }
        }
        public string DepartureICAO { get; set; }
        public bool DepartAirportChanged { get; set; }
        public bool DepartAirportChangedUpdateCrew { get; set; }
        public bool DepartAirportChangedUpdatePAX { get; set; }
        #endregion

        #region ARRIVAL
        public long? ArriveICAOID{
            get
            {
                if (ArrivalAirport != null)
                {
                    return ArrivalAirport.AirportID;
                }
                return 0;
            }
            set
            {
                if (ArrivalAirport != null)
                {
                    ArrivalAirport.AirportID = value.Value;
                }
            }
        }
        public bool ArrivalAirportChanged { get; set; }
        public string ArrivalICAO { get; set; }
        public bool ArrivalAirportChangedUpdateCrew { get; set; }
        public bool ArrivalAirportChangedUpdatePAX { get; set; }
        #endregion

        public long? LegNUM { get; set; }
        public string AdditionalCrew { get; set; }
        public FlightPak.Web.PreflightService.TripEntityState State { get; set; }
        public string CheckGroup{ get; set; }
        public long ConfirmID{ get; set; }
        public string CrewDutyAlert { get; set; }
        public string CrewFuelLoad { get; set; }
        public long? CustomerID { get; set; }
        public string DescLabelLeg { get; set; }
        public string Description { get; set; }
        public decimal? DutyHours { get; set; }
        public string DutyTYPE { get; set; }
        public decimal? DutyTYPE1 { get; set; }
        public decimal? ElapseTM { get; set; }
        public string ArrivalAirCityName { get; set; }
        public string DepartAirCityName { get; set; }
        public string StrElapseTM
        {
            get
            {
                double ETE = 0.0;
                if(ElapseTM != null)
                {
                    ETE = PreUtilities.RoundElpTime(Convert.ToDouble(ElapseTM.Value), UserPrincipal._TimeDisplayTenMin.Value, UserPrincipal._ElapseTMRounding.Value);
                }
                return UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                    ? PreUtilities.DefaultEteResult(Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(ETE), UserPrincipal._TimeDisplayTenMin.Value))
                    : PreUtilities.DefaultEteResult(Math.Round(LandingBias == null ? 0 : ETE, 1).ToString());
            }
            set
            {
                string ETE = string.Empty;
                decimal toETE = 0;
                ETE = UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                       ? Utilities.Preflight_ConvertMinstoTenths(value, UserPrincipal._TimeDisplayTenMin.Value)
                       : value;
                ElapseTM = decimal.TryParse(ETE, out toETE) ? toETE : 0;
            }
        }
        public bool isDeadCategory { get; set; }
        public decimal? EstFuelQTY { get; set; }
        public string FedAviationRegNUM { get; set; }
        public decimal? FlightCost { get; set; }
        public decimal? FlightHours { get; set; }
        public string FlightNUM { get; set; }
        public string FlightPurpose { get; set; }
        public decimal? FuelLoad { get; set; }
        public decimal? FuelUnits { get; set; }
        public string GoTime { get; set; }
        public bool? IsApproxTM { get; set; }
        public bool? IsArrivalConfirmation { get; set; }
        public bool? IsCrewDiscount { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsDepartureConfirmed { get; set; }
        public bool? IsDutyEnd { get; set; }
        public bool? IsPrivate { get; set; }
        public bool? IsScheduledServices { get; set; }
        public decimal? LandingBias { get; set; }
        public string StrLandingBias {
            get
            {
                return UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                    ? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(LandingBias), UserPrincipal._TimeDisplayTenMin.Value)
                    : Math.Round(LandingBias == null ? 0 : LandingBias.Value, 1).ToString();
            }
            set
            {
                string bias = string.Empty;
                decimal toBias = 0;
                bias = UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin 
                       ? Utilities.Preflight_ConvertMinstoTenths(value, UserPrincipal._TimeDisplayTenMin.Value) 
                       : value;
                LandingBias = decimal.TryParse(bias, out toBias) ? toBias : 0;
            }
        }
        public long LegID { get; set; }
        public long? LegID1 { get; set; }
        public string LogBreak { get; set; }
        public DateTime? NextGMTDTTM { get; set; }
        public DateTime? NextHomeDTTM { get; set; }
        public DateTime? NextLocalDTTM { get; set; }

        public string Notes { get; set; }
        public string OutbountInstruction { get; set; }
        public decimal? OverrideValue { get; set; }
        public int? PassengerTotal { get; set; }
        public string PilotInCommand { get; set; }
        public string PowerSetting {
            get { return string.IsNullOrEmpty(_powersetting) ? "1" : _powersetting; }
            set
            {
                this._powersetting = value;
            }
        }
        public int? ReservationAvailable { get; set; }
        public int? ReservationTotal { get; set; }
        public string Response { get; set; }
        public decimal? RestHours { get; set; }
        public int? SeatTotal { get; set; }
        public string SecondInCommand { get; set; }
        public decimal? TakeoffBIAS { get; set; }
        public string StrTakeoffBIAS
        {
            get
            {
                return UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                    ? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(TakeoffBIAS), UserPrincipal._TimeDisplayTenMin.Value)
                    : Math.Round(TakeoffBIAS == null ? 0 : TakeoffBIAS.Value, 1).ToString();
            }
            set
            {
                string bias = string.Empty;
                decimal toBias = 0;
                bias = UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                       ? Utilities.Preflight_ConvertMinstoTenths(value, UserPrincipal._TimeDisplayTenMin.Value)
                       : value;
                TakeoffBIAS = decimal.TryParse(bias, out toBias) ? toBias : 0;
            }
        }
        public long? TripID { get; set; }
        public long? TripNUM { get; set; }
        public decimal? TrueAirSpeed { get; set; }
        public string USCrossing { get; set; }
        public string UWAID { get; set; }
        public string WaitList { get; set; }
        public int? WaitNUM { get; set; }
        public int? WindReliability { get; set; }
        public string StrWindReliability
        {
            get { return WindReliability == null ? "3" : Convert.ToString(WindReliability); }
            set { WindReliability=string.IsNullOrEmpty(value) ? 3 : Convert.ToInt16(value); }
        }
        public decimal? WindsBoeingTable { get; set; }

        public decimal? Distance { get; set; }
        public string StrConvertedDistance
        {
            get
            {
                if (UserPrincipal._IsKilometer && Distance!=null)
                    return Convert.ToString(Math.Floor(Distance.Value * 1.852M));
                return Convert.ToString(Distance);
            }
        }
        public string LegTitleforTabs { get; set; }
        public string CrewNotes { get; set; }
        public int BlockedSeatCount { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public DateTime? LastUpdTSCrewNotes { get; set; }
        public DateTime? LastUpdTSPaxNotes { get; set; }
        public string LastUpdUID { get; set; }
        public string _fieldName { get; set; }
        public bool IsLegEdited { get; set; }
        public string LegFuelComment { get; set; }
        #region ACCOUNT
        public long? AccountID { get; set; }
        public AccountViewModel Account { get; set; }
        #endregion

        #region AIRPORT
        public AirportViewModel ArrivalAirport { get; set; }
        public AirportViewModel DepartureAirport { get; set; }
        #endregion

        #region CQCustomer
        public long? CQCustomerID { get; set; }
        public CQCustomerViewModel CQCustomer { get; set; }
        #endregion

        #region CLIENT
        public long? ClientID { get; set; }
        public ClientViewModel Client { get; set; }
        #endregion

        #region CrewDutyRule
        public long? CrewDutyRulesID { get; set; }
        public CrewDutyRuleViewModel CrewDutyRule { get; set; }
        #endregion

        #region DEPARTMENT
        public long? DepartmentID { get; set; }
        public DepartmentViewModel Department { get; set; }
        public string DepartmentName { get; set; }
        #endregion

        #region DEPARTMENT AUTHORIZATION
        public long? AuthorizationID { get; set; }
        public DepartmentAuthorizationViewModel DepartmentAuthorization { get; set; }
        #endregion

        #region FBO
        public long? FBOID { get; set; }
        #endregion

        #region FLIGHT CATAGORY
        public long? FlightCategoryID { get; set; }
        public FlightCatagoryViewModel FlightCatagory { get; set; }
        #endregion

        #region PASSENGER
        public PassengerViewModel Passenger { get; set; }
        public long? PassengerRequestorID { get; set; }
        public string RequestorName { get; set; }

        #endregion
        
        #region OUT BOUNDS INSTRUCTIONS
        public List<PreflightTripOutboundViewModel> PreflightTripOutbounds { get; set; }

        public string OutboundInstruction1 { get; set; }

        public string OutboundInstruction2 { get; set; }

        public string OutboundInstruction3 { get; set; }

        public string OutboundInstruction4 { get; set; }

        public string OutboundInstruction5 { get; set; }

        public string OutboundInstruction6 { get; set; }

        public string OutboundInstruction7 { get; set; }
        #endregion
        #region CHECKLIST
        public List<PreflightChecklistViewModel> PreflightChecklist { get; set; }
        #endregion
    }
}