﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class CrewViewModel
    {
        string _releasedbyDesc;
        public Int64 CrewID
        {
            get;
            set;
        }
        public String CrewCD { get; set; }
        public String FirstName { get; set; }
        public String MiddleInitial { get; set; }
        public String LastName { get; set; }
        public String DisplayName
        {
            get
            {
                String outStr = "";
                if (!String.IsNullOrWhiteSpace(LastName))
                    outStr = LastName + ",";
                if(!String.IsNullOrWhiteSpace(MiddleInitial) )
                    outStr += " "+ MiddleInitial+ " ";
                 if(!String.IsNullOrWhiteSpace(FirstName))
                     outStr += " " + FirstName ;
                 outStr = outStr.Trim(new char[] { ' ', ',' });
                 return outStr;

            }
            set
            {

            }
        }
        public String ReleasedbyDesc
        {
            get 
            {
                if (string.IsNullOrEmpty(_releasedbyDesc))

                    _releasedbyDesc =System.Web.HttpUtility.HtmlEncode((string.IsNullOrWhiteSpace(LastName))? string.Empty: (LastName)) +

                        System.Web.HttpUtility.HtmlEncode((string.IsNullOrWhiteSpace(FirstName))?string.Empty:(","+FirstName)) +

                        System.Web.HttpUtility.HtmlEncode((string.IsNullOrWhiteSpace(MiddleInitial))? string.Empty: (" " + MiddleInitial));

                return _releasedbyDesc;
            }
            set
            {
                _releasedbyDesc = value;
            }
        }
    }
}