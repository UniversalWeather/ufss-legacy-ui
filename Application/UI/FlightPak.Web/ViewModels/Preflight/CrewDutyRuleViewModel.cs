﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class CrewDutyRuleViewModel
    {
        public CrewDutyRuleViewModel()
        {
            this.CrewDutyRulesID = 0;
        }

        public string CrewDutyRuleCD { get; set; }
        public long CrewDutyRulesID { get; set; }
        public decimal? DutyDayBeingTM { get; set; }
        public decimal? DutyDayEndTM { get; set; }
        public decimal? MaximumDutyHrs { get; set; }
        public decimal? MaximumFlightHrs { get; set; }
        public decimal? MinimumRestHrs { get; set; }
        public string CrewDutyRulesDescription { get; set; }
        public string tooltipInfo
        {
            get
            {
                var tooltip = "Duty Day Start : " + (DutyDayBeingTM != null ? Math.Round((decimal)DutyDayBeingTM, 1).ToString() : string.Empty)
                    + "\nDuty Day End : " + (DutyDayEndTM != null ? Math.Round((decimal)DutyDayEndTM, 1).ToString() : string.Empty)
                    + "\nMax. Duty Hrs. Allowed : " + (MaximumDutyHrs != null ? Math.Round((decimal)MaximumDutyHrs, 1).ToString() : string.Empty)
                    + "\nMax. Flight Hrs. Allowed : " + (MaximumFlightHrs != null ? Math.Round((decimal)MaximumFlightHrs.Value, 1).ToString() : string.Empty)
                    + "\nMin. Fixed Rest : " + (MinimumRestHrs != null ? Math.Round((decimal)MinimumRestHrs.Value, 1).ToString() : string.Empty);

                return Microsoft.Security.Application.Encoder.HtmlEncode(tooltip);
            }
        }
        public string FedAviatRegNum { get; set; }

    }
}