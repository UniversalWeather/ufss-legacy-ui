﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.FlightPakMasterService;
namespace FlightPak.Web.ViewModels
{
    /// <summary>
    /// This is a view model for the FlightPak.Data.MasterCatalog.Fleet class
    /// </summary>
    public class FleetViewModel
    {
        public Int64 FleetID { get; set; }
        public String AircraftCD { get; set; }
        public long? AircraftID { get; set; }
        public String TailNum { get; set; }

        public String TailNumberDescription { get; set; }
        public Nullable<Int64> CustomerID { get; set; }
        public decimal? MaximumPassenger { get; set; }

        public decimal? SIFLMultiControlled { get; set; }
        public decimal? SIFLMultiNonControled { get; set; }
        public decimal? MultiSec { get; set; }

    }

}