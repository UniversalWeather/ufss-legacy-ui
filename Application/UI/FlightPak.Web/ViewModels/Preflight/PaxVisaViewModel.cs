﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PaxVisaViewModel
    {
        public string CountryCD { get; set; }
        public long? CountryID { get; set; }
        public string CountryName { get; set; }
        public long? PassengerID { get; set; }
        public long? CustomerID { get; set; }
        public int? EntriesAllowedInPassport { get; set; }
        public DateTime? ExpiryDT { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsInActive { get; set; }
        public string IssueDate { get; set; }
        public string IssuePlace { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdUID { get; set; }
        public string Notes { get; set; }
        public long? PassengerRequestorID { get; set; }
        public string TypeOfVisa { get; set; }
        public int? VisaExpireInDays { get; set; }
        public long VisaID { get; set; }
        public string VisaNum { get; set; }
        public string VisaTYPE { get; set; }
    }
}