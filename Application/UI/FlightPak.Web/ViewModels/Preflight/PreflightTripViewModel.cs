﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightTripViewModel
    {
        public PreflightTripViewModel()
        {
            TripId = 0;
            PreflightMain = new PreflightMainViewModel();
            PreflightLegs = new List<PreflightLegViewModel>();
            PreflightLegCrews = new Dictionary<string, List<PreflightLegCrewViewModel>>();
            PreflightLegPassengers = new Dictionary<string, List<PassengerViewModel>>();
            PreflightLegPassengersLogisticsHotelList = new Dictionary<String, List<PreflightLegPassengerHotelViewModel>>();
            PreflightLegPreflightLogisticsList = new Dictionary<string, PreflightLogisticsViewModel>();
            PreflightLegCrewLogisticsHotelList = new Dictionary<string, List<PreflightLegCrewHotelViewModel>>();
            PreflightLegCrewTransportationList = new Dictionary<string, PreflightLegCrewTransportationViewModel>();
            PreflightLegPaxTransportationList = new Dictionary<string, PreflightLegPassengerTransportViewModel>();
            CurrentLegNUM = 1;
        }
        public long? TripId { get; set; }
        public PreflightMainViewModel PreflightMain { get; set; }

        public List<PreflightLegViewModel> PreflightLegs { get; set; }

        public Dictionary<string, List<PreflightLegCrewViewModel>> PreflightLegCrews { get; set; }

        public Dictionary<string, List<PassengerViewModel>> PreflightLegPassengers { get; set; }

        public Dictionary<String, List<PreflightLegPassengerHotelViewModel>> PreflightLegPassengersLogisticsHotelList { get; set; }

        public Dictionary<string, PreflightLogisticsViewModel> PreflightLegPreflightLogisticsList { get; set; }

        public Dictionary<String, List<PreflightLegCrewHotelViewModel>> PreflightLegCrewLogisticsHotelList { get; set; }

        public Dictionary<String, PreflightLegCrewTransportationViewModel> PreflightLegCrewTransportationList { get; set; }
        public Dictionary<String, PreflightLegPassengerTransportViewModel> PreflightLegPaxTransportationList { get; set; }

        public long? CurrentLegNUM { get; set; }

    }
}