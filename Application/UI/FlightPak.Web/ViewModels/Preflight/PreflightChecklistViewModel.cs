﻿using FlightPak.Web.PreflightService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightChecklistViewModel
    {
        public long? LegID { get; set; }
        public long PreflightCheckListID { get; set; }
        public long? CustomerID { get; set; }
        public string ComponentCD { get; set; }
        public long? CheckListID { get; set; }
        public string CheckListDescription { get; set; }
        public string ComponentDescription { get; set; }
        public string LastUpdUID { get; set; }
        public bool? IsCompleted { get; set; }
        public string CheckGroupCD { get; set; }
        public long? CheckGroupID { get; set; }
        public string CheckGroupDescription { get; set; }
        public TripEntityState State { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsInactive { get; set; }
        //public DateTime? LastUpdTS { get; set; }
        //public TripManagerChecklistViewModel TripManagerChecklist { get; set; }
    }
}