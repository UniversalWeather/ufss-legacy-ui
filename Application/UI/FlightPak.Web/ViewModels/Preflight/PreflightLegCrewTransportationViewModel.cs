﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.PreflightService;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegCrewTransportationViewModel
    {
        public PreflightLegCrewTransportationViewModel()
        {
            SelectedDepartTransportStatus = "Select";
            SelectedArrivalTransportStatus = "Select";
        }
        public string SelectedDepartTransportStatus { get; set; }
        public long DepartureAirportId { get; set; }
        public long PreflightDepartTransportId { get; set; }
        public long DepartTransportId { get; set; }
        public string DepartCode { get; set; }
        public String DepartRate { get; set; }
        public string DepartName { get; set; }
        public string DepartPhone { get; set; }
        public string DepartFax { get; set; }
        public string DepartConfirmation { get; set; }
        public string DepartComments { get; set; }

        public string SelectedArrivalTransportStatus { get; set; }
        public long ArrivalAirportId { get; set; }
        public long PreflightArrivalTransportId { get; set; }
        public long ArrivalTransportId { get; set; }
        public string ArrivalCode { get; set; }
        public String ArrivalRate { get; set; }
        public string ArrivalName { get; set; }
        public string ArrivalPhone { get; set; }
        public string ArrivalFax { get; set; }
        public string ArrivalConfirmation { get; set; }
        public string ArrivalComments { get; set; }
        public long LegNUM { get; set; }
        public TripEntityState State { get; set; }

    }
}