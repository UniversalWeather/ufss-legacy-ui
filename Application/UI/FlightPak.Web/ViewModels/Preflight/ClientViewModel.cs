﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class ClientViewModel
    {
        public long ClientID
        {
            get;
            set;
        }
        public string ClientCD { get; set; }
        public string ClientDescription { get; set; }
    }
}