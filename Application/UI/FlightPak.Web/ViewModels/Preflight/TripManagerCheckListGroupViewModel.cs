﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class TripManagerCheckListGroupViewModel
    {
        public Int64 CheckGroupID { get; set; }
        public string CheckGroupCD { get; set; }
        public string CheckGroupDescription { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsInActive { get; set; }
    }
}