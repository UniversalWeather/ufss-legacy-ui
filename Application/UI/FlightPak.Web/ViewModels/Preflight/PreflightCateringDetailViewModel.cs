﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightCateringDetailViewModel
    {
        public long? AirportID { get; set; }
        public string ArriveDepart { get; set; }
        public string CateringComments { get; set; }
        public string CateringConfirmation { get; set; }
        public string CateringContactName { get; set; }
        public long? CateringID { get; set; }
        public string CateringCD { get; set; }
        public string ContactFax { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public decimal? Cost { get; set; }
        public long CustomerID { get; set; }
        public string Description { get; set; }
        public bool? IsCompleted { get; set; }
        public bool? IsUWAArranger { get; set; }
        public long PreflightCateringID { get; set; }
        public string Status { get; set; }
        public FlightPak.Web.PreflightService.TripEntityState State { get; set; }

        public string _fieldName { get; set; }
    }
}