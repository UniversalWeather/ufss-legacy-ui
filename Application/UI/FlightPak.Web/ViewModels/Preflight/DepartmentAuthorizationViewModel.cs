﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class DepartmentAuthorizationViewModel
    {
        public Int64 AuthorizationID
        {
            get;
            set;
        }
        public String AuthorizationCD { get; set; }
        public String DeptAuthDescription { get; set; }
    }
}