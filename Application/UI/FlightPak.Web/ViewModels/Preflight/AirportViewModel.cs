﻿using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.Framework.Helpers.Preflight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Constants;
using System.Web.Script.Serialization;

namespace FlightPak.Web.ViewModels
{
    public class AirportViewModel
    {
        public FPKConversionHelper Utilities = new FPKConversionHelper();
        UserPrincipalViewModel _userprincipal;
        UserPrincipalViewModel UserPrincipal
        {
            get { return FlightPak.Web.Views.Transactions.TripManagerBase.getUserPrincipal(); }
        }
        public string IcaoID { get; set; }
        public long AirportID { get; set; }
        public string CityName { get; set; }
        public string StateName { get; set; }
        public string CountryName { get; set; }
        public string AirportName { get; set; }
        public string DSTRegionCD { get; set; }
        public Nullable<decimal> OffsetToGMT { get; set; }
        public Nullable<decimal> LongestRunway { get; set; }
        public string Iata { get; set; }
        public Nullable<decimal> TakeoffBIAS { get; set; }
        public String StrTakeoffBIAS
        {
            get
            {
                return UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                ? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(TakeoffBIAS), UserPrincipal._TimeDisplayTenMin.Value)
                : Math.Round(TakeoffBIAS == null ? 0 : TakeoffBIAS.Value, 1).ToString();
            }
        }
        public Nullable<decimal> LandingBIAS { get; set; }
        public String StrLandingBIAS
        {
            get
            {
                return UserPrincipal._TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)UserPrincipal._TimeDisplayTenMin
                ? Utilities.Preflight_ConvertTenthsToMins(Convert.ToString(LandingBIAS), UserPrincipal._TimeDisplayTenMin.Value)
                : Math.Round(LandingBIAS == null ? 0 : LandingBIAS.Value, 1).ToString();
            }
        }
        public string Alerts { get; set; }
        public string GeneralNotes { get; set; }
        public string tooltipInfo
        {
            get
            {
                String strTooltip = String.Format("ICAO : {0}\nCity: {1}\nState/Province : {2}\nCountry :{3}\nAirport: {4}\n DST Region : {5}\nUTC+/- : {6}\nLongest Runway : {7}\nIATA: {8}",
                    IcaoID, CityName, StateName, CountryName, AirportName, DSTRegionCD, OffsetToGMT, LongestRunway, Iata);
                return Microsoft.Security.Application.Encoder.HtmlEncode(strTooltip);
            }
            set { }
        }
        public long? CountryID { get; set; }
        public decimal? LatitudeDegree { get; set; }
        public decimal? LatitudeMinutes { get; set; }
        public string LatitudeNorthSouth { get; set; }
        public decimal? LongitudeDegrees { get; set; }
        public decimal? LongitudeMinutes { get; set; }
        public string LongitudeEastWest { get; set; }
        public bool? IsDayLightSaving { get; set; }
        [ScriptIgnore]
        public DateTime? DayLightSavingStartDT { get; set; }

        [ScriptIgnore]
        public DateTime? DayLightSavingEndDT { get; set; }
        public string DaylLightSavingStartTM { get; set; }
        public string DayLightSavingEndTM { get; set; }
        public decimal? WindZone { get; set; }

        
        public long? LegID { get; set; }
        // ^^ may be null in many cases.
    }
}