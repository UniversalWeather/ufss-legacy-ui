﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class AircraftViewModel
    {
        public Int64 AircraftID
        {
            get;
            set;
        }
        public String AircraftCD { get; set; }
        public String AircraftDescription { get; set; }

        public Nullable<decimal> ChargeRate { get; set; }
        public string ChargeUnit { get; set; }
    }
}