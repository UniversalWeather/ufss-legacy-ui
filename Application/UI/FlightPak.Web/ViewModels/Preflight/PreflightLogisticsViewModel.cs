﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLogisticsViewModel
    {
        public PreflightLegViewModel PreflightLeg { get; set; }        
        public PreflightFBOListViewModel PreflightDepFboListViewModel { get; set; }
        public PreflightCateringDetailViewModel ArrivalLogisticsCatering { get; set; }
        public PreflightCateringDetailViewModel DepartLogisticsCatering { get; set; }

        public PreflightFBOListViewModel PreflightArrFboListViewModel { get; set; }

        public string FuelAmountType = "0";
        public string FuelVendorText { get; set; }
        public string VendorName { get; set; }
        public string EstimatedCost { get; set; }
        //public string UserPrefferedDateFormat { get; set; }
        public long? LegNUM { get; set; }
        public string LblEstimateType = "U.S. Gallons";
        public bool NoUpdated = false;

        public PreflightLogisticsViewModel()
        {
            PreflightLeg = new PreflightLegViewModel();
            PreflightLeg.ArrivalAirport = new AirportViewModel();
            PreflightLeg.DepartureAirport = new AirportViewModel();
            PreflightLeg.Passenger = new PassengerViewModel();
            PreflightArrFboListViewModel = new PreflightFBOListViewModel() { IsArrivalFBO = true, IsDepartureFBO = false };
            PreflightDepFboListViewModel = new PreflightFBOListViewModel() { IsArrivalFBO = false, IsDepartureFBO = true };
            ArrivalLogisticsCatering = new PreflightCateringDetailViewModel() { ArriveDepart = "A" };
            DepartLogisticsCatering = new PreflightCateringDetailViewModel() { ArriveDepart = "D" };
        }

    }
}