﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class AccountViewModel
    {
        public Int64 AccountID
        {
            get;
            set;
        }
        public String AccountNum { get; set; }
        public String AccountDescription { get; set; }
    }
}