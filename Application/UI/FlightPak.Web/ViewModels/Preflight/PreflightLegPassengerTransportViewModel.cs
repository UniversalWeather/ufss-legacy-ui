﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegPassengerTransportViewModel
    {
        public long? DepartTransportID { get; set; }
        public long? ArriveTransportID { get; set; }
        public bool EnableMode { get; set; }

        public string SelectedDepartTransportStatus { get; set; }
        public long DepartureAirportId { get; set; }
        public long PreflightDepartTransportId { get; set; }        
        public string DepartCode { get; set; }
        public string DepartRate { get; set; }
        public string DepartName { get; set; }
        public string DepartPhone { get; set; }
        public string DepartFax { get; set; }
        public string DepartConfirmation { get; set; }
        public string DepartComments { get; set; }
        

        public string SelectedArrivalTransportStatus { get; set; }
        public long ArrivalAirportId { get; set; }
        public long PreflightArrivalTransportId { get; set; } 
        public string ArrivalCode { get; set; }
        public string ArrivalRate { get; set; }
        public string ArrivalName { get; set; }
        public string ArrivalPhone { get; set; }
        public string ArrivalFax { get; set; }
        public string ArrivalConfirmation { get; set; }
        public string ArrivalComments { get; set; }


        public string _fieldName { get; set; }

        public PreflightLegPassengerTransportViewModel()
        {
            SelectedArrivalTransportStatus = "Select";
            SelectedDepartTransportStatus = "Select";
        }
    }
}