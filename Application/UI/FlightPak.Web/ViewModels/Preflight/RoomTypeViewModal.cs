﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class RoomTypeViewModal
    {
        public Nullable<long> RoomTypeID { get; set; }
        public string RoomDescription { get; set; }

    }
}