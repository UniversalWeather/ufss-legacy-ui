﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class CQCustomerViewModel
    {
        public long CQCustomerID { get; set; }
        public string CQCustomerCD { get; set; }
        public string CQCustomerName { get; set; }
    }
}