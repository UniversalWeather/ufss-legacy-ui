﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;


namespace FlightPak.Web.ViewModels
{
    public class PreflightLegCrewHotelViewModel
    {
        public PreflightLegCrewHotelViewModel()
        {
            Status = "Select";
            CrewIDList = new List<long?>();
        }
        public Int64 PreflightHotelListID { get; set; }
        public String PreflightHotelName { get; set; }
        public Nullable<Int64> LegID { get; set; }
        public Nullable<Int64> HotelID { get; set; }
        public Nullable<Int64> AirportID { get; set; }
        public String RoomType { get; set; }
        public Nullable<Decimal> RoomRent { get; set; }
        public String RoomConfirm { get; set; }
        public String RoomDescription { get; set; }
        public String Street { get; set; }
        public String CityName { get; set; }
        public String StateName { get; set; }
        public String PostalZipCD { get; set; }
        public String PhoneNum1 { get; set; }
        public String PhoneNum2 { get; set; }
        public String PhoneNum3 { get; set; }
        public String PhoneNum4 { get; set; }
        public String FaxNUM { get; set; }
        public Boolean IsDeleted { get; set; }
        public String HotelArrangedBy { get; set; }
        public Nullable<Decimal> Rate { get; set; }
        public Nullable<DateTime> DateIn {
            get
            {
                return MiscUtils.FSSPreflightParseFormat(ClientDateIn, WebConstants.isoDateFormat);
            }
            set
            {
                ClientDateIn = (value != null) ? String.Format("{0:" + WebConstants.isoDateFormat + "}", value) : "";
            }
        }
        public String ClientDateIn
        {
            get;
            set;
        }
        public Nullable<DateTime> DateOut {
            get
            {
                return MiscUtils.FSSPreflightParseFormat(ClientDateOut, WebConstants.isoDateFormat);
            }
            set
            {
                ClientDateOut = (value != null) ? String.Format("{0:" + WebConstants.isoDateFormat + "}", value) : "";
            }
        }
        public String ClientDateOut
        {
            get;
            set;
        }
        public Nullable<Boolean> IsSmoking { get; set; }
        public Nullable<Int64> RoomTypeID { get; set; }
        public Nullable<Decimal> NoofBeds { get; set; }
        public Nullable<Boolean> IsAllCrewOrPax { get; set; }
        public Nullable<Boolean> IsCompleted { get; set; }
        public Nullable<Boolean> IsEarlyCheckIn { get; set; }
        public String EarlyCheckInOption { get; set; }
        public String SpecialInstructions { get; set; }
        public String ConfirmationStatus { get; set; }
        public String Comments { get; set; }
        public Nullable<Int64> CustomerID { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String Address3 { get; set; }
        public Nullable<Int64> MetroID { get; set; }
        public String City { get; set; }
        public String StateProvince { get; set; }
        public Nullable<Int64> CountryID { get; set; }
        public String PostalCode { get; set; }
        public String Status { get; set; }
        public Nullable<Boolean> isArrivalHotel { get; set; }
        public Nullable<Boolean> isDepartureHotel { get; set; }
        public String crewPassengerType { get; set; }
        public Nullable<Decimal> NoofRooms { get; set; }
        public String ClubCard { get; set; }
        public Nullable<Boolean> IsRateCap { get; set; }
        public Nullable<Decimal> MaxCapAmount { get; set; }
        public Nullable<Boolean> IsRequestOnly { get; set; }
        public Nullable<Boolean> IsBookNight { get; set; }
        public  FlightPak.Web.PreflightService.TripEntityState State { get; set; }

        public string Description { get; set; }
        public List<Int64?> CrewIDList { get; set; }
        public Int64 HotelIdentifier { get; set; }
        public string HotelCode { get; set; }
        public string CountryCode { get; set; }

        public long LegNUM { get; set; }
        public string CrewCode { get; set; }
        public String MetroCD { get; set; }

        public string HotelCodeEdit { get; set; }


    }
}