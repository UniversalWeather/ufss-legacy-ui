﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegChecklistViewModel
    {
        public long? LegNum { get; set; }

        public Boolean EditMode { get; set; }

        public string CheckGroupDescription { get; set; }

        public string ChecklistGroupID { get; set; }

        public List<PreflightChecklistViewModel> CurrentLegChecklist { get; set; }

        
    }
}