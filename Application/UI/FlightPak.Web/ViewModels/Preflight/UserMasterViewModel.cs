﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class UserMasterViewModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DispatcherDesc{
            get{
               return  System.Web.HttpUtility.HtmlEncode(FirstName + (string.IsNullOrWhiteSpace(MiddleName) ? string.Empty : (" " + MiddleName)) + (string.IsNullOrWhiteSpace(LastName) ? string.Empty : (" " + LastName)));
            }
        }
        public string PhoneNum { get; set; }
    }
}