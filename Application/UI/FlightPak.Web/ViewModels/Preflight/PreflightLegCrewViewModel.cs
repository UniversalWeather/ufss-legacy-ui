﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegCrewViewModel
    {
        public string CityName { get; set; }
        public string ClubCard { get; set; }
        public string CrewFirstName { get; set; }
        public long? CrewID { get; set; }
        public string CrewCode { get; set; }
        public string CrewLastName { get; set; }
        public string CrewMiddleName { get; set; }
        public long? CustomerID { get; set; }
        public string Description { get; set; }
        public string DutyTYPE { get; set; }
        public string HotelName { get; set; }
        public bool? IsAdditionalCrew { get; set; }
        public bool? IsBlackberrMailSent { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsDiscount { get; set; }
        public bool? IsNotified { get; set; }
        public bool? IsRateCap { get; set; }
        public bool? IsDisplay { get; set; }
        public bool? IsChoice { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdUID { get; set; }
        public long? LegID { get; set; }
        public string LegNum { get; set; }
        public decimal? MaxCapAmount { get; set; }
        public decimal? NoofRooms { get; set; }
        public int? OrderNUM { get; set; }
        public long? PassportID { get; set; }
        public string PassportNum { get; set; }
        public string PostalZipCD { get; set; }
        public long PreflightCrewListID { get; set; }
        public string StateName { get; set; }
        public string Street { get; set; }
        public string Nation { get; set; }
        public long? VisaID { get; set; }
        public string PassportExpiryDT { get; set; }
        public string PassportIssueDT { get; set; }
        public FlightPak.Web.PreflightService.TripEntityState State { get; set; }
        
    }
}