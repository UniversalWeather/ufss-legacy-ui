﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class EmergencyContactViewModel
    {
        string _emergencyContactDesc;
        public Int64 EmergencyContactID
        {
            get;
            set;
        }
        public String EmergencyContactCD { get; set; }
        public String FirstName { get; set; }
        public String MiddleName { get; set; }
        public String LastName { get; set; }
        public String EmergencyContactDesc {
            get
            {
                if (string.IsNullOrEmpty(_emergencyContactDesc))
                    _emergencyContactDesc = System.Web.HttpUtility.HtmlEncode(FirstName) + System.Web.HttpUtility.HtmlEncode((string.IsNullOrWhiteSpace(MiddleName)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(MiddleName))) + (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(LastName)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(LastName)));
                return _emergencyContactDesc;
            }
            set
            {
                _emergencyContactDesc = value;
            }
        }
    }
}