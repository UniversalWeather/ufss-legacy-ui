﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegPassengerViewModel
    {

        public long PaxID { get; set; }
        public long LegID { get; set; }
        public string PaxName { get; set; }
        public string OrderNUM { get; set; }
        public bool IsSelect { get; set; }


    }
}