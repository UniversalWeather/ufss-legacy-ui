﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PassengerViewModel
    {
        string _requestorDesc;
        public Int64 PassengerRequestorID
        {
            get { return _PassengerID; }
            set { _PassengerID = value; }
        }
        public String PassengerRequestorCD
        {
            get; set;
        }
        public String AdditionalPhoneNum { get; set; }
        public String FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public String MiddleInitial
        {
            get { return _middleName; }
            set { _middleName = value; }
        }
        public String LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public String RequestorDesc
        {
            get
            {
                if (string.IsNullOrEmpty(_requestorDesc))
                    _requestorDesc = (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(LastName)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(LastName + ", "))) + System.Web.HttpUtility.HtmlEncode(FirstName) + (string.IsNullOrWhiteSpace(System.Web.HttpUtility.HtmlEncode(MiddleInitial)) ? string.Empty : (" " + System.Web.HttpUtility.HtmlEncode(MiddleInitial)));
                return _requestorDesc;
            }
            set
            {
                _requestorDesc = value;
            }
        }
        private string _firstName;
        private string _lastName;
        private string _middleName;
        private long _PassengerID;
        public string _paxcode;


        public long? PassengerID { get { return _PassengerID; } set { _PassengerID = (long)value; } }
        public long? LegID { get; set; }
        public int? OrderNUM { get; set; }
        public bool IsSelect { get; set; }
        public string PassengerLastName { get { return _lastName; } set { _lastName = value; } }
        public string PassengerMiddleName { get { return _middleName; } set { _middleName = value; } }
        public string PassengerFirstName { get { return _firstName; } set { _firstName = value; } }
        public string PaxCode { get { return _paxcode; } set { _paxcode = value; } }
        public long? VisaID { get; set; }
        public string TripStatus { get; set; }
        public long? FlightPurposeID { get; set; }
        public string Billing { get; set; }
        public bool? IsAdditionalCrew { get; set; }
        public bool? IsBlackberrMailSent { get; set; }
        public bool IsDeleted { get; set; }
        public bool? IsDiscount { get; set; }
        public bool? IsNotified { get; set; }
        public bool? IsRateCap { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdUID { get; set; }
        public decimal? MaxCapAmount { get; set; }
        public decimal? NoofRooms { get; set; }
        public long? PassportID { get; set; }
        public string PostalZipCD { get; set; }
        public long? PreflightCrewListID { get; set; }
        public string StateName { get; set; }
        public string Street { get; set; }
        public string CityName { get; set; }
        public string PassportExpiryDT { get; set; }

        public FlightPak.Web.PreflightService.TripEntityState State { get; set; }

        private string _PassportNum;
        private string _FlightPurposeCD;
        public long PreflightPassengerListID { get; set; }
        public string PaxName { get; set; }
        public string PassportNum { get { return _PassportNum; } set { _PassportNum = value; } }
        public string CountryCD { get; set; }
        public string FlightPurposeCD { get { return _FlightPurposeCD; } set { _FlightPurposeCD = value; } }
        public long? LegNUM { get; set; }
        public string PassengerAlert { get; set; }
        public string Notes { get; set; } 
        public string Purpose { get { return _FlightPurposeCD; } set { _FlightPurposeCD = value; } }

        public string IssueDT { get; set; }
        public string Nation { get; set; }

    }
}