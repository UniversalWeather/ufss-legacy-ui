﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class FlightpakFuelDataViewModel
    {
        public int Id { get; set; }
        public System.Nullable<long> DepartureICAOID {get;set;}
        public System.Nullable<System.DateTime> EffectiveDT {get;set;}
        public string FBOName {get;set;}
        public System.Nullable<long> FuelVendorID {get;set;}
        public System.Nullable<decimal> Price1 {get;set;}
        public System.Nullable<decimal> Price10 {get;set;}
        public System.Nullable<decimal> Price11 {get;set;}
        public System.Nullable<decimal> Price12 {get;set;}
        public System.Nullable<decimal> Price13 {get;set;}
        public System.Nullable<decimal> Price14 {get;set;}
        public System.Nullable<decimal> Price15 {get;set;}
        public System.Nullable<decimal> Price16 {get;set;}
        public System.Nullable<decimal> Price17 {get;set;}
        public System.Nullable<decimal> Price18 {get;set;}
        public System.Nullable<decimal> Price19 {get;set;}
        public System.Nullable<decimal> Price2 {get;set;}
        public System.Nullable<decimal> Price20 {get;set;}
        public System.Nullable<decimal> Price21 {get;set;}
        public System.Nullable<decimal> Price3 {get;set;}
        public System.Nullable<decimal> Price4 {get;set;}
        public System.Nullable<decimal> Price5 {get;set;}
        public System.Nullable<decimal> Price6 {get;set;}
        public System.Nullable<decimal> Price7 {get;set;}
        public System.Nullable<decimal> Price8 {get;set;}
        public System.Nullable<decimal> Price9 {get;set;}
        public string StartRange1 {get;set;}
        public string StartRange10 {get;set;}
        public string StartRange11 {get;set;}
        public string StartRange12 {get;set;}
        public string StartRange13 {get;set;}
        public string StartRange14 {get;set;}
        public string StartRange15 {get;set;}
        public string StartRange16 {get;set;}
        public string StartRange17 {get;set;}
        public string StartRange18 {get;set;}
        public string StartRange19 {get;set;}
        public string StartRange2 {get;set;}
        public string StartRange20 {get;set;}
        public string StartRange21 {get;set;}
        public string StartRange3 {get;set;}
        public string StartRange4 {get;set;}
        public string StartRange5 {get;set;}
        public string StartRange6 {get;set;}
        public string StartRange7 {get;set;}
        public string StartRange8 {get;set;}
        public string StartRange9 {get;set;}
        public string VendorCD {get;set;}
        public string VendorName {get;set;}
        public Nullable<double> BestPrice { get; set; }

    }
}