﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PaxPassportVisaViewModel
    {
        public long? PassengerID { get; set; }
        public string Leg { get; set; }
        public string PaxName { get; set; }
        public Boolean EditMode { get; set; }
        public UserPrincipalViewModel UserPrincipal { get; set; }
        public List<PaxPassportViewModel> PaxPassportData { get; set; }
        public List<PaxVisaViewModel> PaxVisaData { get; set; }
    }
}