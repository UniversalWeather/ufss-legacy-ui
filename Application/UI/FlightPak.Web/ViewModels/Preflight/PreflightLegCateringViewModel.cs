﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegCateringViewModel
    {
        public string ArriveDepart { get; set; }
        public long? AirportID { get; set; }
        public long? CateringID { get; set; }
        public bool? IsCompleted { get; set; }
        public string CateringCD { get; set; }
        public decimal? Cost { get; set; }
        public string ContactName { get; set; }
        public string CateringContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }
        public string CateringConfirmation { get; set; }
        public string CateringComments { get; set; }
        public string Status { get; set; }


    }
}