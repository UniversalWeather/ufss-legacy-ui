﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class FlightCatagoryViewModel
    {
        public string FlightCatagoryCD { get; set; }
        public long FlightCategoryID { get; set; }
        public string FlightCatagoryDescription { get; set; }
        public bool? IsDeadorFerryHead { get; set; }
    }
}