﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class RoomDescriptionViewModel
    {
        public string RoomDescCode { get; set; }
        public string RoomDescription { get; set; }
    }
}