using FlightPak.Web.Framework.Constants;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Helpers;
using Telerik.Web.UI;
using System.Globalization;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.ViewModels
{
    public class PreflightMainViewModel
    {
        private UserPrincipalViewModel userPrincipal = new UserPrincipalViewModel();
        public PreflightMainViewModel()
        {
            EditMode = true;
            TripID = 0;
            if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel]!=null)
            {
                userPrincipal = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
                Company=new CompanyViewModel();
            }
        }
        public PreflightMainViewModel(long TripId)
        {
            TripID = TripId;
            if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] != null)
            {
                userPrincipal = (UserPrincipalViewModel)HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel];
            }
        }
        public string dateFormat { get; set; }
        public Int64 TripID
        {
            get;
            set;
        }
        public Nullable<Int64> CustomerID
        {
            get;
            set;
        }
        public Nullable<Int64> TripNUM
        {
            get;
            set;
        }
        public Nullable<Int64> PreviousNUM
        {
            get;
            set;
        }
        public String TripDescription
        {
            get;
            set;
        }
        public String DispatchNUM
        {
            get;
            set;
        }
        public String TripStatus
        {
            get;
            set;
        }
        public Nullable<DateTime> EstDepartureDT
        {
            get
            {
                return MiscUtils.FSSPreflightParseFormat(ClientEstDepartureDT, WebConstants.isoDateFormat);
            }
            set
            {
                ClientEstDepartureDT = (value != null) ? String.Format("{0:" + WebConstants.isoDateFormat + "}", value) : "";
            }
        }
        public String ClientEstDepartureDT
        {
            get;
            set;
        }

        public Nullable<DateTime> EstArrivalDT
        {
            get;
            set;
        }
        public String RecordType
        {
            get;
            set;
        }
        public String DepartmentDescription
        {
            get;
            set;
        }
        public String AuthorizationDescription
        {
            get;
            set;
        }
        public Nullable<DateTime> RequestDT
        {
            get
            {
                return MiscUtils.FSSPreflightParseFormat(ClientRequestDT, WebConstants.isoDateFormat);
            }
            set
            {
                ClientRequestDT = (value != null) ? String.Format("{0:" + WebConstants.isoDateFormat + "}", value) : "";
            }
        }
        public String ClientRequestDT
        {
            get;
            set;
        }
        public Nullable<Boolean> IsCrew
        {
            get;
            set;
        }
        public Nullable<Boolean> IsPassenger
        {
            get;
            set;
        }
        public Nullable<Boolean> IsQuote
        {
            get;
            set;
        }
        public Nullable<Boolean> IsScheduledServices
        {
            get;
            set;
        }
        public Nullable<Boolean> IsAlert
        {
            get;
            set;
        }
        public String Notes
        {
            get;
            set;
        }
        public String LogisticsHistory
        {
            get;
            set;
        }
        public Nullable<Boolean> IsAirportAlert
        {
            get;
            set;
        }
        public Nullable<Boolean> IsLog
        {
            get;
            set;
        }
        public Nullable<DateTime> BeginningGMTDT
        {
            get;
            set;
        }
        public Nullable<DateTime> EndingGMTDT
        {
            get;set;
        }
        public String LastUpdUID
        {
            get;set;
        }
        public Nullable<DateTime> LastUpdTS
        {
            get;
            set;
        }
        public string LastUpdTSStr { get; set; }
        public String LastUpdatedBy
        {
            get
            {
                if (!string.IsNullOrEmpty(LastUpdUID))
                {
                    DateTime? dt =LastUpdTS;
                    if (LastUpdTS.HasValue)
                        LastUpdTSStr = LastUpdTS.Value.ToString("yyyy-MM-dd HH:mm:ss");

                    if (userPrincipal._airportId != null)
                    {
                        CalculationService.CalculationServiceClient CalcSvc = new CalculationService.CalculationServiceClient();
                        dt = CalcSvc.GetGMT(userPrincipal._airportId, LastUpdTS.Value, false, false);
                    }
                    DateTime? UTCdt = LastUpdTS.Value.ToUniversalTime();
                    return "Last Modified: " + LastUpdUID + " " + dt.FSSParseDateTimeString(dateFormat + " HH:mm")+" LCL" + " (" + LastUpdTS.FSSParseDateTimeString(dateFormat + " HH:mm") + " UTC)";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public Nullable<Boolean> IsPrivate
        {
            get;set;
        }
        public Nullable<Boolean> IsCorpReq
        {
            get;set;
        }
        public String Acknowledge
        {
            get;set;
        }
        public String DispatchNotes
        {
            get;set;
        }
        public String TripRequest
        {
            get;set;
        }
        public String WaitList
        {
            get;set;
        }            
        public String FlightNUM
        {
            get;set;
        }
        public Nullable<Decimal> FlightCost
        {
            get;
            set;
        }
        public string strFlightCost
        {
            get
            {
                return FlightCost == null ? "0.00" : Math.Round((decimal)FlightCost, 2).ToString();
            }
            set
            {
                if (value == null) throw new ArgumentNullException("value");
                FlightCost = Convert.ToDecimal(value);
            }
        }
        public String TripException
        {
            get;set;
        }
        public String TripSheetNotes
        {
            get;set;
        }
        public String CancelDescription
        {
            get;set;
        }
        public Nullable<Boolean> IsCompleted
        {
            get;set;
        }
        public Nullable<Boolean> IsFBOUpdFlag
        {
            get;set;
        }
        public Nullable<Decimal> RevisionNUM
        {
            get;set;
        }
        public String RevisionDescriptioin
        {
            get;set;
        }            
        public Nullable<Boolean> IsPersonal
        {
            get;set;
        }
        public String DsptnName
        {
            get;set;
        }
        public String ScheduleCalendarHistory
        {
            get;set;
        }            
        public Nullable<DateTime> ScheduleCalendarUpdate
        {
            get;set;
        }
        public String CommentsMessage
        {
            get;set;
        }
        public Nullable<Decimal> EstFuelQTY
        {
            get;set;
        }
        public bool AllowTripToSave { get; set; }
        public long? LogNum { get; set; }
        public bool IsViewModelEdited { get; set; }

        #region Passenger Requestor Id
        public Nullable<Int64> PassengerRequestorID
        {
            get;
            set;
        }
        public PassengerViewModel Passenger { get; set; }
        #endregion

        #region HomeBase
        public Nullable<Int64> HomebaseID
        {
            get;
            set;
        }
        public String HomeBaseAirportICAOID { get; set; }
        public Nullable<Int64> HomebaseAirportID { get; set; }
        public CompanyViewModel Company { get; set; }
        public string HomebaseTooltip{get;set;}
        public Nullable<Int64> OldHomebaseID { get; set; }
        #endregion

        #region Client Code
        public Nullable<Int64> ClientID
        {
            get;
            set;
        }
        public ClientViewModel Client {get; set; }
        #endregion

        #region Account Number
        public Nullable<Int64> AccountID
        {
            get;
            set;
        }
        public AccountViewModel Account { get; set; }
        #endregion

        #region Crew Or ReleasedBy
        public Nullable<Int64> CrewID
        {
            get;set;
        }
        public CrewViewModel Crew { get; set; }
        #endregion 

        #region Fleet Tail Number
        public Nullable<Int64> AircraftID
        {
            get;
            set;
        }
        public AircraftViewModel Aircraft { get; set; }
        public FleetViewModel Fleet { get; set; }
        public Nullable<Int64> FleetID
        {
            get;
            set;
        }
        #endregion 

        #region Others
        public Boolean IsDeleted
        {
            get;set;
        }

        public String VerifyNUM
        {
            get;set;
        }
            
        public Nullable<DateTime> APISSubmit
        {
            get;set;
        }
            
        public String APISStatus
        {
            get;set;
        }
            
        public String APISException
        {
            get;set;
        }
            
        public Nullable<Boolean> IsAPISValid
        {
            get;set;
        }
        #endregion

        #region Department
        public Nullable<Int64> DepartmentID
        {
            get;set;
        }
        public DepartmentViewModel Department { get; set; }
        #endregion

        #region Authorization
        public Nullable<Int64> AuthorizationID
        {
            get;set;
        }
        public DepartmentAuthorizationViewModel DepartmentAuthorization { get;set; }
        #endregion

        #region CQCustomer
        public Nullable<Int64> CQCustomerID { get; set; }
        public CQCustomerViewModel CQCustomer { get; set; }
        #endregion

        #region Dispatcher
        public string DispatcherUserName
        {
            get;set;
        }
        public UserMasterViewModel UserMaster { get; set; }
        #endregion

        #region Emergency Contact
        public Nullable<Int64> EmergencyContactID
        {
            get;set;
        }
        public EmergencyContactViewModel EmergencyContact { get; set; }
        #endregion
            
        #region Others
        public Boolean EditMode { get; set; }
        public String FleetCalendarNotes
        {
            get;set;
        }
            
        public String CrewCalendarNotes
        {
            get;set;
        }
            
        public Nullable<Decimal> FuelUnits
        {
            get;set;
        }
            
        public Nullable<Boolean> IsTripCopied
        {
            get;set;
        }
        #endregion 

    }
}