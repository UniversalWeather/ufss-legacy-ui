﻿using FlightPak.Web.PreflightService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightTripOutboundViewModel
    {
       
        public long? LegID { get; set; }

        public string OutboundDescription { get; set; }

        public long? OutboundInstructionNUM { get; set; }

        public long PreflightTripOutboundID { get; set; }

        public TripEntityState State { get; set; }

        public bool IsDeleted { get; set; }

        public System.Nullable<System.DateTime> LastUpdTS { get; set; }

        public string LastUpdUID { get; set; }
    }
}