﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightFBOListViewModel
    {
        public long? AirportID { get; set; }
        public string CityName { get; set; }
        public string Comments { get; set; }
        public string ConfirmationStatus { get; set; }
        public long? CustomerID { get; set; }
        public string Description { get; set; }
        public string FaxNUM { get; set; }        
        public bool? FBOArrangedBy {get;set;}
        public long? FBOID { get; set; }
        public string FBOInformation { get; set; }
        public bool? IsArrivalFBO { get; set; }
        public string FBOCD { get; set; }
        public bool  IsCompleted { get; set; }
        public bool? IsDepartureFBO { get; set; }
        public long LegID { get; set; }
        public bool NoUpdated { get; set; }
        public string PhoneNum1 { get; set; }
        public string PhoneNum2 { get; set; }
        public string PhoneNum3 { get; set; }
        public string PhoneNum4 { get; set; }
        public string PostalZipCD { get; set; }
        public long PreflightFBOID { get; set; }
        public string PreflightFBOName { get; set; }
        public string StateName { get; set; }
        public string Status { get; set; }
        public string Street { get; set; }        

        public string UNICOM { get; set; }
        public string ARINC { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string Addr3 { get; set; }
        public string EmailAddress { get; set; }
        public FlightPak.Web.PreflightService.TripEntityState State { get; set; }

        public string _fieldName { get; set; }

    }
}
