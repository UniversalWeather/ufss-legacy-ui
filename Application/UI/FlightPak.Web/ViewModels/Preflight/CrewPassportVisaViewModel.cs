﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class CrewPassportVisaViewModel
    {
        public long? CrewID { get; set; }
        public string Leg { get; set; }
        public string CrewName { get; set; }
        public Boolean EditMode { get; set; }
        public UserPrincipalViewModel UserPrincipal { get; set; }
        public List<CrewPassportViewModel> CrewPassportData { get; set; }
        public List<CrewVisaViewModel> CrewVisaData { get; set; }
    }
}