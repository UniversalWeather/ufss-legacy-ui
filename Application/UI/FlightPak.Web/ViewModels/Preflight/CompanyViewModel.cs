﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class CompanyViewModel
    {
        public String BaseDescription { get; set; }
        public long HomebaseID
        {
            get;
            set;
        }
        public Nullable<Int64> HomebaseAirportID { get; set; }
        public Nullable<Boolean> IsEnableTSA { get; set; }
    }
}