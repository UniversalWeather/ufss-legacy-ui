﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class PreflightLegCrewHotelSelectedCrewViewModel
    {
        public String CrewCode { get; set; }
        public long? CrewID { get; set; }
        public string CrewFirstName { get; set; }
        public string CrewLastName { get; set; }
        public string CrewMiddleName { get; set; }
        public int? OrderNUM { get; set; }
        public bool IsSelect { get; set; }
        public long LegID { get; set; }
        public long LegNUM { get; set; }

    }
}