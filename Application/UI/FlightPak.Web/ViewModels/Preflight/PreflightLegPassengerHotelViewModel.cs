﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;

namespace FlightPak.Web.ViewModels
{
    
    public class PreflightLegPassengerHotelViewModel
    {
        public long? HotelID { get; set; }
        private bool? _isArrivalHotel;
        public bool? isArrivalHotel { get { return _isArrivalHotel; } set { if (value == false) { isDepartureHotel = true; } else { isDepartureHotel = false; } _isArrivalHotel = value; } }
        public string HotelCode { get; set; }
        public string PreflightHotelName { get; set; }
        public decimal? Rate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string CityName { get; set; }
        public string StateProvince { get; set; }
        public string CountryCode { get; set; }
        public string Metro { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNum1 { get; set; }
        public string FaxNUM { get; set; }
        public bool? IsBookNight { get; set; }
        public bool? IsEarlyCheckIn { get; set; }
        public Nullable<DateTime> DateIn
        {
            get
            {
                return MiscUtils.FSSPreflightParseFormat(ClientDateIn, WebConstants.isoDateFormat);
            }
            set
            {
                ClientDateIn = (value != null) ? String.Format("{0:" + WebConstants.isoDateFormat + "}", value) : "";
            }
        }
        public String ClientDateIn
        {
            get;
            set;
        }
        public Nullable<DateTime> DateOut
        {
            get
            {
                return MiscUtils.FSSPreflightParseFormat(ClientDateOut, WebConstants.isoDateFormat);
            }
            set
            {
                ClientDateOut = (value != null) ? String.Format("{0:" + WebConstants.isoDateFormat + "}", value) : "";
            }
        }
        public String ClientDateOut
        {
            get;
            set;
        }
        public long? RoomTypeID { get; set; }
        public decimal? NoofRooms { get; set; }
        public decimal? NoofBeds { get; set; }
        public string RoomDescription { get; set; }
        public bool? IsRequestOnly { get; set; }
        public bool? IsSmoking { get; set; }
        public string ClubCard { get; set; }
        public decimal? MaxCapAmount { get; set; }
        public string Comments { get; set; }
        public string ConfirmationStatus { get; set; }
        public bool NoPAX { get; set; }
        public string Status { get; set; }
        public string UserPrefferedDateFormat { get; set; }
        public FlightPak.Web.PreflightService.TripEntityState State { get; set; }
        // hidden field values

        public long? MetroID { get; set; }
        public long? CountryID { get; set; }        
        public bool EnableMode { get; set; }
        public int LegNUM { get; set; }
        public string PAXCode { get; set; }
        public bool? IsRateCap { get; set; }
        public bool? IsAllCrewOrPax {get;set;}
        
        public string crewPassengerType { get { return "P"; } }

        public bool IsDeleted { get; set; }
        public long PreflightHotelListID { get; set; }
        public bool? isDepartureHotel { get; set; }

        //public bool IsHotelEdit { get; set; }
        public string HotelCodeEdit { get; set; }

        public PreflightLegPassengerHotelViewModel()
        {
            isArrivalHotel = true;
            IsDeleted = false;
        }
    }
}