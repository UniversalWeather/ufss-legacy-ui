﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using FlightPak.Web.Framework.Constants;

namespace FlightPak.Web.ViewModels
{
    public class FSSOperationResult<T> where T : new()
    {
        public FSSOperationResult()
        {
            Result = new T();
            ResultList = new List<T>();
            ErrorsList = new List<string>();
        }
        public bool Success { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public List<String> ErrorsList { get; set; }
        public T Result { get; set; }
        public List<T> ResultList { get; set; }

        public bool IsAuthorized()
        {
            this.StatusCode = HttpStatusCode.Unauthorized;
            this.Success = false;
            try
            {
                if (HttpContext.Current.Session[WebSessionKeys.UserPrincipalModel] != null)
                {
                    FlightPak.Web.Framework.Prinicipal.FPPrincipal UserPrincipal = (FlightPak.Web.Framework.Prinicipal.FPPrincipal)HttpContext.Current.Session[HttpContext.Current.Session.SessionID];
                    if (UserPrincipal != null)
                    {
                        this.StatusCode = HttpStatusCode.OK;
                        this.Success = true;
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                this.ErrorsList.Add(ex.Message);
                return false;
            }
        }
    }
}