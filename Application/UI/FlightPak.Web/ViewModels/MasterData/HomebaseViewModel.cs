﻿using FlightPak.Web.BusinessLite.Postflight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Web.ViewModels
{
    public class HomebaseViewModel
    {
        public HomebaseViewModel()
        {
            HomebaseAirport = new AirportViewModel();
        }
        public long HomebaseID { get; set; }
        public string BaseDescription { get; set; }
        public long? ClientID { get; set; }

        public string CompanyName { get; set; }
        public string HomebaseToolTip { get; set; }

        private long? homebaseAirportID;
        public long? HomebaseAirportID { get { return homebaseAirportID; } set { homebaseAirportID = value; LoadTooltipOfHomebase(); } }
        public AirportViewModel HomebaseAirport { get; set; }

        public string HomebaseCD { get; set; }


        #region commented out for now, use them when required

/*        public string APISUrl { get; set; }
        public DateTime? AcceptDTTM { get; set; }

        public string AccountFormat { get; set; }

        public DateTime? ActiveDT { get; set; }

        public string Additional2FeeDefault { get; set; }

        public string AdditionalCrewCD { get; set; }

        public string AdditionalCrewRON { get; set; }

        public decimal? AircraftBasis { get; set; }

        public decimal? AircraftBlockFlight { get; set; }

        public string ApplicationDateFormat { get; set; }

        public string ApplicationMessage { get; set; }

        public decimal? Approach { get; set; }

        public decimal? ApproachMinimum { get; set; }

        public decimal? AugmentCrewPercentage { get; set; }

        public string AutoClimbTime { get; set; }

        public DateTime? BackupDT { get; set; }

        public string BaseFax { get; set; }

        public decimal? BusinessWeekStart { get; set; }

        public string CQCrewDuty { get; set; }

        public decimal? CQFederalTax { get; set; }

        public decimal? CQMessageCD { get; set; }

        public int? CRSearchBack { get; set; }

        public decimal? CalendarMonthMaximum { get; set; }

        public decimal? CalendarQTRMaximum { get; set; }

        public decimal? CalendarYearMaximum { get; set; }

        public string CanPassNum { get; set; }

        public decimal? ChtQouteDOMSegCHG { get; set; }

        public string ChtQouteDOMSegCHGACCT { get; set; }

        public string ChtQouteFooterDetailRpt { get; set; }

        public string ChtQouteHeaderDetailRpt { get; set; }

        public decimal? ChtQouteIntlSegCHG { get; set; }

        public string ChtQouteIntlSegCHGACCT { get; set; }

        public string ChtQuoteCompany { get; set; }

        public string ChtQuoteCompanyNameINV { get; set; }

        public string ChtQuoteFooterINV { get; set; }

        public string ChtQuoteHeaderINV { get; set; }

        public string CorpAccountFormat { get; set; }

        public decimal? CorpReqChgQueueTimer { get; set; }

        public decimal? CorpReqExcChgWithinHours { get; set; }

        public bool? CorpReqExcChgWithinWeekend { get; set; }

        public decimal? CorpReqMinimumRunwayLength { get; set; }

        public string CorpReqStartTM { get; set; }

        public decimal? CorpReqTMEntryFormat { get; set; }

        public decimal? CorpReqWeeklyCalTimer { get; set; }

        public decimal? Crew5DayTimer { get; set; }

        public decimal? CrewChecklistAlertDays { get; set; }

        public string CrewChecklistGroupCD { get; set; }

        public long? CrewDutyID { get; set; }

        public string CrewDutyRuleCD { get; set; }

        public string CrewLogCustomLBLLong1 { get; set; }

        public string CrewLogCustomLBLLong2 { get; set; }

        public string CrewLogCustomLBLShort1 { get; set; }

        public string CrewLogCustomLBLShort2 { get; set; }

        public int? CrewOFullName { get; set; }

        public int? CrewOLast { get; set; }

        public int? CrewOLastFirst { get; set; }

        public int? CrewOLastLast { get; set; }

        public int? CrewOLastMiddle { get; set; }

        public int? CrewOMiddle { get; set; }

        public string CurrencySymbol { get; set; }

        public string CustomRptMessage { get; set; }

        public long? CustomerID { get; set; }

        public decimal? Day30 { get; set; }

        public decimal? Day365 { get; set; }

        public decimal? Day7 { get; set; }

        public decimal? Day90 { get; set; }

        public decimal? DayLanding { get; set; }

        public decimal? DayLandingMinimum { get; set; }

        public decimal? DayMaximum30 { get; set; }

        public decimal? DayMaximum365 { get; set; }

        public decimal? DayMaximum7 { get; set; }

        public decimal? DayMaximum90 { get; set; }

        public decimal? DayMonthFormat { get; set; }

        public long? DefaultCheckListGroupID { get; set; }

        public long? DefaultFlightCatID { get; set; }

        public string DefaultInvoice { get; set; }

        public long? DepartmentID { get; set; }

        public string Deposit2FlightCharge { get; set; }

        public string DepostFlightCharge { get; set; }

        public string Discount2AmountDeposit { get; set; }

        public string DiscountAmountDefault { get; set; }

        public string Domain { get; set; }

        public decimal? DutyBasis { get; set; }

        public decimal? ElapseTMRounding { get; set; }

        public string ExchangeRateCD { get; set; }

        public long? ExchangeRateID { get; set; }

        public string FedAviationRegNum { get; set; }

        public string FederalACCT { get; set; }

        public string FederalTax { get; set; }

        public decimal? FiscalYREnd { get; set; }

        public decimal? FiscalYRStart { get; set; }

        public decimal? FleetCalendar5DayTimer { get; set; }

        public string FlightCatagoryCD { get; set; }

        public string FlightCatagoryPassenger { get; set; }

        public string FlightCatagoryPassengerNum { get; set; }

        public string FlightPurpose { get; set; }

        public string FooterTextRpt { get; set; }

        public decimal? FuelBurnKiloLBSGallon { get; set; }

        public decimal? FuelPurchase { get; set; }

        public string GeneralAviationDesk { get; set; }

        public decimal? GroundTM { get; set; }

        public decimal? GroundTMIntl { get; set; }

        public string HTMLBackgroun { get; set; }

        public string HTMLFooter { get; set; }

        public string HTMLHeader { get; set; }

        public string HeardTextRpt { get; set; }

        public string HomebasePhoneNUM { get; set; }

        public decimal? HoursMinutesCONV { get; set; }

        public bool? IFirstPG { get; set; }

        public decimal? IVMessageCD { get; set; }

        public string Ilogo { get; set; }

        public int? IlogoPOS { get; set; }

        public decimal? ImageCnt { get; set; }

        public decimal? ImagePosition { get; set; }

        public decimal? Instrument { get; set; }

        public decimal? InstrumentMinimum { get; set; }

        public int? InvoiceSalesLogoPosition { get; set; }

        public string InvoiceTitle { get; set; }

        public bool? IsANotes { get; set; }

        public bool? IsAPISAlert { get; set; }

        public bool? IsAPISSupport { get; set; }

        public bool? IsAircraftTypeCode { get; set; }

        public bool? IsAirportDescription { get; set; }

        public bool? IsAllBase { get; set; }

        public decimal? IsAppliedTax { get; set; }

        public bool? IsAutoCreateCrewAvailInfo { get; set; }

        public bool? IsAutoCrew { get; set; }

        public bool? IsAutoDispat { get; set; }

        public bool? IsAutoFillAF { get; set; }

        public bool? IsAutoPassenger { get; set; }

        public bool? IsAutoPopulated { get; set; }

        public bool? IsAutoRON { get; set; }

        public bool? IsAutoRevisionNum { get; set; }

        public bool? IsAutoRollover { get; set; }

        public bool? IsAutoValidateTripManager { get; set; }

        public bool? IsAutomaticCalcLegTimes { get; set; }

        public bool? IsAutomaticCommentTrips { get; set; }

        public bool? IsAutomaticDispatchNum { get; set; }

        public bool? IsBlackberrySupport { get; set; }

        public bool? IsBlockSeats { get; set; }

        public bool? IsCNotes { get; set; }

        public bool? IsCOLFee3 { get; set; }

        public bool? IsCOLFee4 { get; set; }

        public bool? IsCOLFee5 { get; set; }

        public bool? IsCanPass { get; set; }

        public bool? IsChangeQueueAirport { get; set; }

        public bool? IsChangeQueueCity { get; set; }

        public bool? IsChangeQueueDetail { get; set; }

        public bool? IsCharterQuote5 { get; set; }

        public bool? IsCharterQuoteFee3 { get; set; }

        public bool? IsCharterQuoteFee4 { get; set; }

        public bool? IsChecklistAssign { get; set; }

        public bool? IsChecklistWarning { get; set; }

        public bool? IsCityDescription { get; set; }

        public bool? IsCompanyName { get; set; }

        public bool? IsCompanyName2 { get; set; }

        public bool? IsConflictCheck { get; set; }

        public bool? IsCorpReqActivateAlert { get; set; }

        public bool? IsCorpReqActivateApproval { get; set; }

        public bool? IsCorpReqAllowLogUpdCateringInfo { get; set; }

        public bool? IsCorpReqAllowLogUpdCrewInfo { get; set; }

        public bool? IsCorpReqAllowLogUpdFBOInfo { get; set; }

        public bool? IsCorpReqAllowLogUpdPaxInfo { get; set; }

        public bool? IsCorpReqOverrideSubmittoChgQueue { get; set; }

        public bool? IsCrewOlap { get; set; }

        public bool? IsCrewQaulifiedFAR { get; set; }

        public bool? IsDeactivateHomeBaseFilter { get; set; }

        public bool IsDeleted { get; set; }

        public bool? IsDepartAuthDuringReqSelection { get; set; }

        public bool? IsDisplayNoteFlag { get; set; }

        public bool? IsEnableTSA { get; set; }

        public bool? IsEnableTSAPX { get; set; }

        public bool? IsEndDutyClient { get; set; }

        public bool IsInActive { get; set; }

        public bool? IsInvoiceDTsystemDT { get; set; }

        public bool? IsInvoiceRptArrivalDate { get; set; }

        public bool? IsInvoiceRptDetailDT { get; set; }

        public bool? IsInvoiceRptFromDescription { get; set; }

        public bool? IsInvoiceRptLegNum { get; set; }

        public bool? IsInvoiceRptQuoteDate { get; set; }

        public bool? IsKilometer { get; set; }

        public bool? IsLogoUpload { get; set; }

        public bool? IsLogsheetWarning { get; set; }

        public bool? IsMaxCrew { get; set; }

        public bool? IsMiles { get; set; }

        public bool? IsNonLogTripSheetIncludeCrewHIST { get; set; }

        public bool? IsOpenHistory { get; set; }

        public bool? IsOutlook { get; set; }

        public bool? IsPNotes { get; set; }

        public bool? IsPODepartPercentage { get; set; }

        public bool? IsPassengerOlap { get; set; }

        public bool? IsPrintUWAReports { get; set; }

        public bool? IsQuote2PrepayPrint { get; set; }

        public bool? IsQuoteAdditional2CrewPrepaid { get; set; }

        public bool? IsQuoteAdditional2CrewRON { get; set; }

        public bool? IsQuoteAdditional2FeePrint { get; set; }

        public bool? IsQuoteAdditionalCrewPrepaid { get; set; }

        public bool? IsQuoteAdditonalCrewRON { get; set; }

        public bool? IsQuoteChangeQueueFees { get; set; }

        public bool? IsQuoteChangeQueueICAO { get; set; }

        public bool? IsQuoteChangeQueueSubtotal { get; set; }

        public bool? IsQuoteChangeQueueSum { get; set; }

        public bool? IsQuoteDeactivateAutoUpdateRates { get; set; }

        public bool? IsQuoteDetailFlightHours { get; set; }

        public bool? IsQuoteDetailInvoiceAmt { get; set; }

        public bool? IsQuoteDetailMiles { get; set; }

        public bool? IsQuoteDetailPassengerCnt { get; set; }

        public bool? IsQuoteDetailPrint { get; set; }

        public bool? IsQuoteDetailRptToDescription { get; set; }

        public bool? IsQuoteDiscount2AmountPrepaid { get; set; }

        public bool? IsQuoteDispatch { get; set; }

        public bool? IsQuoteDispcountAmtPrint { get; set; }

        public bool? IsQuoteEdit { get; set; }

        public bool? IsQuoteFeelColumnDescription { get; set; }

        public bool? IsQuoteFeelColumnInvoiceAmt { get; set; }

        public bool? IsQuoteFirstPage { get; set; }

        public bool? IsQuoteFlight2ChargePrepaid { get; set; }

        public bool? IsQuoteFlight2LandingFeePrint { get; set; }

        public bool? IsQuoteFlightChargePrepaid { get; set; }

        public bool? IsQuoteFlightLandingFeePrint { get; set; }

        public bool? IsQuoteICAODescription { get; set; }

        public bool? IsQuoteInformation { get; set; }

        public bool? IsQuoteInformation2 { get; set; }

        public bool? IsQuoteInvoice2Print { get; set; }

        public bool? IsQuoteInvoicePrint { get; set; }

        public bool? IsQuotePrepaidMin2UsageFeeAdj { get; set; }

        public bool? IsQuotePrepaidMinimUsageFeeAmt { get; set; }

        public bool? IsQuotePrepaidMinimumUsage2FeeAdj { get; set; }

        public bool? IsQuotePrepaidSegement2Fee { get; set; }

        public bool? IsQuotePrepaidSegementFee { get; set; }

        public bool? IsQuotePrepayPrint { get; set; }

        public bool? IsQuotePrintAdditionalFee { get; set; }

        public bool? IsQuotePrintSubtotal { get; set; }

        public bool? IsQuotePrintSum { get; set; }

        public bool? IsQuoteRemaining2AmtPrint { get; set; }

        public bool? IsQuoteRemainingAmtPrint { get; set; }

        public bool? IsQuoteSales { get; set; }

        public bool? IsQuoteStdCrew2RONPrepaid { get; set; }

        public bool? IsQuoteStdCrewRonPrepaid { get; set; }

        public bool? IsQuoteSubtotal2Print { get; set; }

        public bool? IsQuoteSubtotalPrint { get; set; }

        public bool? IsQuoteTailNum { get; set; }

        public bool? IsQuoteTaxDiscount { get; set; }

        public bool? IsQuoteTaxes2Print { get; set; }

        public bool? IsQuoteTaxesPrint { get; set; }

        public bool? IsQuoteTypeCD { get; set; }

        public bool? IsQuoteUnpaid2DiscountAmt { get; set; }

        public bool? IsQuoteUnpaidAdditional2Crew { get; set; }

        public bool? IsQuoteUnpaidFlight2Chg { get; set; }

        public bool? IsQuoteUnpaidFlightChg { get; set; }

        public bool? IsQuoteUnpaidStd2CrewRON { get; set; }

        public bool? IsQuoteUpaid2WaitingChg { get; set; }

        public bool? IsQuoteUpaidAdditionl2CrewRON { get; set; }

        public bool? IsQuoteWaiting2ChargePrepaid { get; set; }

        public bool? IsQuoteWaitingChargePrepaid { get; set; }

        public bool? IsQuoteWarningMessage { get; set; }

        public bool? IsReportCrewDuty { get; set; }

        public bool? IsReqOnly { get; set; }

        public bool? IsRptQuoteDetailArrivalDate { get; set; }

        public bool? IsRptQuoteDetailDepartureDT { get; set; }

        public bool? IsRptQuoteDetailFlightHours { get; set; }

        public bool? IsRptQuoteDetailFromDescription { get; set; }

        public bool? IsRptQuoteDetailLegNum { get; set; }

        public bool? IsRptQuoteDetailMiles { get; set; }

        public bool? IsRptQuoteDetailPassengerCnt { get; set; }

        public bool? IsRptQuoteDetailQuoteDate { get; set; }

        public bool? IsRptQuoteDetailQuotedAmt { get; set; }

        public bool? IsRptQuoteDetailToDescription { get; set; }

        public bool? IsRptQuoteFeeDescription { get; set; }

        public bool? IsRptQuoteFeeQuoteAmt { get; set; }

        public bool? IsSChecklist { get; set; }

        public bool? IsSConflict { get; set; }

        public bool? IsSCrewCurrency { get; set; }

        public bool? IsSIFLCalMessage { get; set; }

        public bool? IsSLeg { get; set; }

        public bool? IsSPassenger { get; set; }

        public bool? IsSSL { get; set; }

        public bool? IsSTMPBlock { get; set; }

        public bool? IsScheduleDTTM { get; set; }

        public bool? IsTailBase { get; set; }

        public bool? IsTailInsuranceDue { get; set; }

        public bool? IsTailNumber { get; set; }

        public bool? IsTextAutoTab { get; set; }

        public bool? IsTimeStampRpt { get; set; }

        public bool? IsTransWaitListPassengerfrmPreflightPostflight { get; set; }

        public bool? IsTripsheetView { get; set; }

        public bool? IsVChecklist { get; set; }

        public bool? IsVConflict { get; set; }

        public bool? IsVCrewCurrency { get; set; }

        public bool? IsVLeg { get; set; }

        public bool? IsVPassenger { get; set; }

        public bool? IsZeroSuppressActivityAircftRpt { get; set; }

        public bool? IsZeroSuppressActivityCrewRpt { get; set; }

        public bool? IsZeroSuppressActivityDeptRpt { get; set; }

        public bool? IsZeroSuppressActivityPassengerRpt { get; set; }

        public string LandingFee { get; set; }

        public DateTime? LastUpdTS { get; set; }

        public string LastUpdUID { get; set; }

        public int? LayoverHrsSIFL { get; set; }

        public int? LogFixed { get; set; }

        public int? LogRotary { get; set; }

        public string LoginUrl { get; set; }

        public string MainFee { get; set; }

        public string MenuBackground { get; set; }

        public string MenuFooter { get; set; }

        public string MenuHeader { get; set; }

        public decimal? MinuteHoursRON { get; set; }

        public decimal? Month12 { get; set; }

        public decimal? Month6 { get; set; }

        public decimal? MonthMaximum12 { get; set; }

        public decimal? MonthMaximum6 { get; set; }

        public decimal? NightLanding { get; set; }

        public decimal? NightllandingMinimum { get; set; }

        public int? PassengerOFullName { get; set; }

        public int? PassengerOLast { get; set; }

        public int? PassengerOLastFirst { get; set; }

        public int? PassengerOLastLast { get; set; }

        public int? PassengerOLastMiddle { get; set; }

        public int? PassengerOMiddle { get; set; }

        public decimal? PasswordHistoryNum { get; set; }

        public decimal? PasswordValidDays { get; set; }

        public decimal? Previous2QTRMaximum { get; set; }

        public string publicTrip { get; set; }

        public string QouteStdCrew2RONDeposit { get; set; }

        public string Quote2Subtotal { get; set; }

        public string Quote2TaxesDefault { get; set; }

        public string QuoteAddition2CrewDeposit { get; set; }

        public string QuoteAddition2CrewRON { get; set; }

        public string QuoteAdditionCrewRON { get; set; }

        public string QuoteAdditionalFeeDefault { get; set; }

        public string QuoteAdditonCrewDeposit { get; set; }

        public bool? QuoteDescription { get; set; }

        public decimal? QuoteIDTYPE { get; set; }

        public string QuoteInvoice2Default { get; set; }

        public string QuoteLandingFeeDefault { get; set; }

        public string QuoteLogo { get; set; }

        public int? QuoteLogoPosition { get; set; }

        public string QuoteMinimumUse2FeeDepositAdj { get; set; }

        public string QuoteMinimumUseFeeDepositAmt { get; set; }

        public decimal? QuotePageOff { get; set; }

        public decimal? QuotePageOffsettingInvoice { get; set; }

        public string QuotePrepay2Default { get; set; }

        public string QuotePrepayDefault { get; set; }

        public string QuoteRemaining2AmountDefault { get; set; }

        public string QuoteRemainingAmountDefault { get; set; }

        public int? QuoteSalesLogoPosition { get; set; }

        public string QuoteSegment2FeeDeposit { get; set; }

        public string QuoteSegmentFeeDeposit { get; set; }

        public string QuoteStdCrewRONDeposit { get; set; }

        public string QuoteSubtotal { get; set; }

        public string QuoteTaxesDefault { get; set; }

        public string QuoteTitle { get; set; }

        public string QuoteWaiting2Charge { get; set; }

        public string QuoteWaitingCharge { get; set; }

        public decimal? RefreshTM { get; set; }

        public string ReportWriteLogo { get; set; }

        public int? ReportWriteLogoPosition { get; set; }

        public decimal? RestCalendarQTRMinimum { get; set; }

        public decimal? RestDays { get; set; }

        public decimal? RestDaysMinimum { get; set; }

        public decimal? RowsOnPageCnt { get; set; }

        public decimal? RptSize { get; set; }

        public string RptTabOutbdInstLab1 { get; set; }

        public string RptTabOutbdInstLab2 { get; set; }

        public string RptTabOutbdInstLab3 { get; set; }

        public string RptTabOutbdInstLab4 { get; set; }

        public string RptTabOutbdInstLab5 { get; set; }

        public string RptTabOutbdInstLab6 { get; set; }

        public string RptTabOutbdInstLab7 { get; set; }

        public string RuralAccountNum { get; set; }

        public decimal? RuralTax { get; set; }

        public string SSBACKGRND { get; set; }

        public string SSFOOTER { get; set; }

        public string SSHEADER { get; set; }

        public decimal? SSHRFormat { get; set; }

        public string SaleTax { get; set; }

        public decimal? ScheduleServiceListCTDetail { get; set; }

        public decimal? ScheduleServiceTimeFormat { get; set; }

        public long? ScheduleServiceWaitListID { get; set; }

        public decimal? ScheduledServicesSecurity { get; set; }

        public int? SearchBack { get; set; }

        public decimal? SearchGridTM { get; set; }

        public int? Searchack { get; set; }

        public string SegementAircraftHawaii { get; set; }

        public decimal? SegementFeeHawaii { get; set; }

        public decimal? SegmentFeeAlaska { get; set; }

        public string SeqmentAircraftAlaska { get; set; }

        public string ServerName { get; set; }

        public decimal? ServerRPF { get; set; }

        public decimal? ServerReport { get; set; }

        public int? Specdec3 { get; set; }

        public int? Specdec4 { get; set; }

        public string SpecificationLong3 { get; set; }

        public string SpecificationLong4 { get; set; }

        public string SpecificationShort3 { get; set; }

        public string SpecificationShort4 { get; set; }

        public string StateTax { get; set; }

        public string StdCrewRON { get; set; }

        public decimal? TakeofNightMinimum { get; set; }

        public decimal? TakeoffDay { get; set; }

        public decimal? TakeoffDayMinimum { get; set; }

        public decimal? TakeoffNight { get; set; }

        public string TaxiTime { get; set; }

        public decimal? TimeDisplayTenMin { get; set; }

        public string TripMGRDefaultStatus { get; set; }

        public int? TripS { get; set; }

        public string TripSheetLogoPosition { get; set; }

        public decimal? TripsheetDTWarning { get; set; }

        public string TripsheetRptWriteGroup { get; set; }

        public int? TripsheetSearchBack { get; set; }

        public string TripsheetTabColor { get; set; }

        public string Tsense { get; set; }

        public string UserEmail { get; set; }

        public string UserID { get; set; }

        public string UserNameCBP { get; set; }

        public string UserPassword { get; set; }

        public string UserPasswordCBP { get; set; }

        public string WaitTM { get; set; }

        public string WebBackground { get; set; }

        public string WebFooter { get; set; }

        public string WebHeader { get; set; }

        public string WebPageBackgroundColor { get; set; }

        public decimal? WebSecurityType { get; set; }

        public int? WindReliability { get; set; }

        public string XMLPath { get; set; }

        public bool? isQuotePrintFees { get; set; }

        public string uote2LandingFeeDefault { get; set; }
 */ 

        #endregion


        private void LoadTooltipOfHomebase()
        {

            PostflightMainLite pmLite = new PostflightMainLite();
            string[] homeBaseInfo = pmLite.LoadHomeBaseInfo(this.HomebaseAirportID.ToString());
            string homeBaseToolTip = string.Empty;
            homeBaseToolTip = "ICAO : " + homeBaseInfo[8]
                + "\n" + "City : " + homeBaseInfo[0]
                + "\n" + "State/Province : " + homeBaseInfo[1]
                + "\n" + "Country : " + homeBaseInfo[2]
                + "\n" + "Airport : " + homeBaseInfo[5]
                + "\n" + "DST Region : " + homeBaseInfo[3]
                + "\n" + "UTC+/- : " + homeBaseInfo[4]
                + "\n" + "Longest Runway : " + homeBaseInfo[6]
                + "\n" + "IATA : " + homeBaseInfo[7];

            this.HomebaseToolTip = homeBaseToolTip;
        }
    }
}
