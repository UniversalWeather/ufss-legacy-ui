﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.MasterData
{
    public class PayableVendorViewModel
    {
        public string VendorCD { get; set; }
        public string VendorContactName { get; set; }
        public long VendorID { get; set; }
        public long? AirportID { get; set; }
        public string Name { get; set; }
        public string VendorType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HomeBaseCD { get; set; }
        public long? HomebaseID { get; set; }

/*
        public string AdditionalInsurance { get; set; }
        public string AuditingCompany { get; set; }
        public string BillingAddr1 { get; set; }
        public string BillingAddr2 { get; set; }
        public string BillingAddr3 { get; set; }
        public string BillingCity { get; set; }
        public string BillingFaxNum { get; set; }
        public string BillingName { get; set; }
        public string BillingPhoneNum { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BusinessEmail { get; set; }
        public string BusinessPhone { get; set; }
        public string CellPhoneNum { get; set; }
        public string CellPhoneNum2 { get; set; }
        public string CertificateNumber { get; set; }
        public string ClosestICAO { get; set; }
        public string Contact { get; set; }
        public long? CountryID { get; set; }
        public decimal? Credit { get; set; }
        public long? CustomerID { get; set; }
        public DateTime? DateAddedDT { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public string EmailID { get; set; }
        public string HomeFax { get; set; }
        public bool? IsApplicationFiled { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsDrugTest { get; set; }
        public bool? IsFAR135Approved { get; set; }
        public bool? IsFAR135CERT { get; set; }
        public bool? IsInActive { get; set; }
        public bool? IsInsuranceCERT { get; set; }
        public bool? IsTaxExempt { get; set; }
        public DateTime? LastUpdTS { get; set; }
        public string LastUpdUID { get; set; }
        public decimal? LatitudeDegree { get; set; }
        public decimal? LatitudeMinutes { get; set; }
        public string LatitudeNorthSouth { get; set; }
        public decimal? LongitudeDegree { get; set; }
        public string LongitudeEastWest { get; set; }
        public decimal? LongitudeMinutes { get; set; }
        public string MCBusinessEmail { get; set; }
        public string MetroCD { get; set; }
        public long? MetroID { get; set; }
        public string NationalityCD { get; set; }
        public string Notes { get; set; }
        public string OtherEmail { get; set; }
        public string OtherPhone { get; set; }
        public string PersonalEmail { get; set; }
        public string SITA { get; set; }
        public string TaxID { get; set; }
        public string Terms { get; set; }
        public string TollFreePhone { get; set; }
        public string WebAddress { get; set; }
        public string Website { get; set; }
        public string WebsiteAddress { get; set; }
        */
    }
}