﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.MasterData
{
    public class PaymentTypeViewModel
    {
        public long PaymentTypeID { get; set; }
        public String PaymentTypeCD { get; set; }
        public String PaymentTypeDescription { get; set; }
        public bool? IsInActive { get; set; }
        public bool IsDeleted { get; set; }
    }
} 