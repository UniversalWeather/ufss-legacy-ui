﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class FlightPurposeViewModel
    {
        public long? ClientID { get; set; }
        public string FlightPurposeCD { get; set; }

        public string FlightPurposeDescription { get; set; }
        public long FlightPurposeID { get; set; }
        public bool? IsDefaultPurpose { get; set; }
        public bool? IsPersonalTravel { get; set; }

    }
}