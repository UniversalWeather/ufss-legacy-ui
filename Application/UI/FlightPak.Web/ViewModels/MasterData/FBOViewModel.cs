﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels.MasterData
{
    public class FBOViewModel
    {
       public Int64 FBOID{get;set;}
       public Nullable<Int64> AirportID{get;set;}
        public String FBOCD{get;set;}
        public Nullable<Int64> CustomerID{get;set;}
        public Nullable<Boolean> IsChoice{get;set;}
        public String FBOVendor { get; set; }
    }
}