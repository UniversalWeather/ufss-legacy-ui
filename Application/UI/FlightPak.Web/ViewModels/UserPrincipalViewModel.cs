﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.ViewModels
{
    public class UserPrincipalViewModel
    {
        public long? _homeBaseId
        {
            get;
            set;
        }
        public long _airportId
        {
            get;
            set;
        }
        public string _homeBase { get; set; }
        public String _ApplicationDateFormat
        {
            get;
            set;
        }
        public string _TripMGRDefaultStatus
        {
            get;
            set;
        }
        public string _BaseDescription { get; set; }
        public bool _isDispatcher
        {
            get;
            set;
        }
        public String UserName { get; set; }
        public String _middleName { get; set; }
        public String _firstName { get; set; }
        public String _lastName { get; set; }
        public String FullName { 
            get 
            {
                return _firstName + (string.IsNullOrWhiteSpace(_middleName) ? string.Empty : (" " + _middleName)) + (string.IsNullOrWhiteSpace(_lastName) ? string.Empty : (" " + _lastName));
            } 
        }
        public bool _IsAutoRevisionNum
        {
            get;
            set;
        }
        public String _name
        {
            get;
            set;
        }
        public long? _DepartmentID
        {
            get;
            set;
        }
        public bool _IsAPISSupport { get; set; }
        public bool _IsKilometer { get; set; }
        public decimal? _TimeDisplayTenMin { get; set; }
        public decimal? _ElapseTMRounding { get; set; }
        public bool _IsReqOnly { get; set; }
        public bool _IsANotes { get; set; }
        public bool _IsDepartAuthDuringReqSelection { get; set; } 
        public bool _IsAutomaticCalcLegTimes { get; set; }
        public int? _WindReliability { get; set; }
        public bool _IsAutoCreateCrewAvailInfo { get; set; }
        public long? _DefaultFlightCatID { get; set; }
        public string _FedAviationRegNum { get; set; }
        public long? _CrewDutyID { get; set; }
        public string _clientCd { get; set; }
        public decimal? _GroundTM { get; set; }
        public decimal? _GroundTMIntl { get; set; }
        public long? _DefaultCheckListGroupID { get; set; }

        public bool? AllowAddTripManager { get; set; }
        public bool? AllowEditTripManager { get; set; }
        public bool? AllowDeleteTripManager { get; set; }
        public bool? AddTripLog { get; set; }
        public bool? AddTripSIFL { get; set; }

        public bool? ViewPreFlightMain { get; set; }
        public bool? ViewPreflightLeg { get; set; }
        public bool? ViewPreflightCrew { get; set; }
        public bool? ViewPreflightPassenger { get; set; }
        public bool? ViewPreflightLogistics
        {
            get
            {
                return (ViewPreflightFBO ?? false) && (ViewPreflightFuel ?? false) && (ViewPreflightCatering ?? false);
            }
        }
        public bool? ViewPreflightFuel { get; set; }
        public bool? ViewPreflightFBO { get; set; }
        public bool? ViewPreflightCatering { get; set; }
        public bool? ViewPreflightUWA { get; set; }
        public bool? ViewPreflightReports { get; set; }
        public bool? ViewPreflightTripsheetReportWriter { get; set; }
        public bool _IsAutomaticDispatchNum { get; set; }
        public bool _IsAutoDispat { get; set; }
        public bool _IsOpenHistory { get; set; }
        public bool _IsDefaultTripDepartureDate { get; set; }


        public string _airportICAOCd { get; set; }
        public string _airportName { get; set; }
        public string _email { get; set; }
        public long _customerID { get; set; }

        public Nullable<long> _clientId { get; set; }       


        // Postflight related permission
        public bool? AddPOLogManager { get; set; }
        public bool? EditPOLogManager { get; set; }
        public bool? DeletePOLogManager { get; set; }
        public bool? ViewPORetrieveLog { get; set; }
        public bool? ViewPOOtherCrewDutyLog { get; set; }
        public bool? ViewPOExpenseCatalog { get; set; }
        public bool? ViewPOLogManager { get; set; }

        public decimal? _DutyBasis { get; set; }
        public decimal? _HoursMinutesCONV { get; set; }
        public bool _IsAutoFillAF { get; set; }
        public decimal? _AircraftBlockFlight { get; set; }
        public decimal? _AircraftBasis { get; set; }
        public decimal? _FuelPurchase { get; set; }
        public string _CrewLogCustomLBLShort1 { get; set; }
        public string _CrewLogCustomLBLShort2 { get; set; }
        public string _SpecificationShort3 { get; set; }
        public string _SpecificationShort4 { get; set; }
        public bool _IsAutoPopulated { get; set; }
        public int? _LayoverHrsSIFL { get; set; }
        public bool _IsEndDutyClient { get; set; }
        public bool _IsScheduleDTTM { get; set; }
        public bool _IsPODepartPercentage { get; set; }
        public Int32? _Specdec4 { get; set; }
        public Int32? _Specdec3 { get; set; }
        public decimal? _FuelBurnKiloLBSGallon { get; set; }
        public bool _IsSIFLCalMessage { get; set; }
        public int? _TripsheetSearchBack { get; set; }
        public bool _IsLogsheetWarning { get; set; }
        public string _TaxiTime { get; set; }
        public int? _LogFixed { get; set; }
        public int? _LogRotary { get; set; }
        public bool _IsBlockSeats { get; set; }
        public string _FlightPurpose { get; set; }
        public bool IsSysAdmin { get; set; }
        public bool IsTripPrivacy { get; set; }

        public int? _Searchack { get; set; }
    }
}