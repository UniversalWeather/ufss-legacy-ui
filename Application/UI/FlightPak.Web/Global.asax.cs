﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Common.Constants;

namespace FlightPak.Web
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            FlightPak.Web.Framework.Helpers.FSSBundleManager.CreatePreflightScriptsAndStyles();
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                ExceptionManager exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                System.Web.HttpContext context = HttpContext.Current;
                System.Exception ex = Context.Server.GetLastError();

                //Handle & Log your exception here
                
                System.Exception outEx = null;
                // With out the OutEx variable the focus has been set to this line @ development time for each and every fatal exception ("ex: File not found")
                exManager.HandleException(ex, Policy.UILayer, out outEx);

                //Redirection should be done here.
                if (ex.InnerException!=null && ex.InnerException.Message.Contains("Login session has expired"))
                {
                    HttpCookie SessionCookie = new HttpCookie("ASP.NET_SessionId", "");
                    SessionCookie.Expires = DateTime.Now.AddYears(-1);
                    HttpContext.Current.Response.Cookies.Add(SessionCookie);
                    HttpContext.Current.Response.Redirect("~/Account/Login.aspx");
                }
            }

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
