<%@ WebHandler Language="C#" Class="Handler" %>
using System.Web;
using System.Web.SessionState;
using FlightPak.Common;
using FlightPak.Web.Framework.Helpers;

public class Handler : IHttpHandler, IRequiresSessionState
{
	#region IHttpHandler Members

	private DBDataServer dataServer;
	private DBDataServer DataServer
	{
		get
		{
			if (dataServer == null)
			{
				dataServer = new DBDataServer(System.Configuration.ConfigurationManager.ConnectionStrings["TelerikConnectionString"].ConnectionString);
			}
			return dataServer;
		}
	}

	private HttpContext Context { get; set; }

	public void ProcessRequest(HttpContext context)
	{
		Context = context;

		if (context.Request.QueryString["path"] == null)
		{
			return;
		}
		string path = Context.Server.UrlDecode(Context.Request.QueryString["path"]);

		var item = DataServer.GetItem(path);
		if (item == null) return;

		WriteFile((byte[])item["Content"], item["Name"].ToString(), item["MimeType"].ToString(), context.Response);
	}

	/// <summary>
	/// Sends a byte array to the client
	/// </summary>
	/// <param name="content">binary file content</param>
	/// <param name="fileName">the filename to be sent to the client</param>
	/// <param name="contentType">the file content type</param>
	private void WriteFile(byte[] content, string fileName, string contentType, HttpResponse response)
	{
        response.Buffer = true;
        response.Clear();
        response.ContentType = Microsoft.Security.Application.Encoder.HtmlEncode(contentType);
        string extension = System.IO.Path.GetExtension(fileName).ToLower();
        if (extension != ".htm" && extension != ".html" && extension != ".xml" && extension != ".jpg" &&
            extension != ".gif" && extension != ".png")
        {
            response.AddHeader("content-disposition", "attachment; filename=" + Microsoft.Security.Application.Encoder.HtmlEncode(fileName));
            response.BinaryWrite(MiscUtils.GetFormatedString(content));
            response.Flush();
            response.End();
        }
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}

	#endregion
}