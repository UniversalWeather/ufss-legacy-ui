﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using FlightPak.Web.GridHelpers.CoreApiFormatters;
using Telerik.Web.UI;

namespace FlightPak.Web.GridHelpers
{
    public static class TelerikGridHelper
    {
        /// <summary>
        /// Get TelerikPaging object that contains the Record range to fetch from database. Ex 1 to 10 or 11 to 20 in case of page size is 10
        /// </summary>
        /// <param name="CurrentPageIndex">{TelerikGrid}.CurrentPageIndex Property</param>
        /// <param name="PageSize">{TelerikGrid}.PageSize Property</param>
        /// <returns>TelerikPaging</returns>
        public static TelerikPaging GetPaging(int CurrentPageIndex, int PageSize)
        {
            int PageNumber = 0;

            if (CurrentPageIndex <= 0)
                PageNumber = 1;
            else
                PageNumber = CurrentPageIndex + 1;

            TelerikPaging telerikPaging = new TelerikPaging();

            telerikPaging.Start = ((PageNumber - 1) * PageSize) + 1;
            telerikPaging.End = (telerikPaging.Start + PageSize) - 1;

            return telerikPaging;
        }

        /// <summary>
        /// Return Sort Expression string for Multiple or Single columns sorting
        /// </summary>
        /// <param name="gridSortExpressionCollection"></param>
        /// <returns>Sort Expression String</returns>
        public static string GetSortExpressionString(GridSortExpressionCollection gridSortExpressionCollection)
        {
            string strSortOrder = string.Empty;

            foreach (var sortExpression in gridSortExpressionCollection)
            {
                string exp = sortExpression.ToString();

                if (string.IsNullOrEmpty(strSortOrder))
                    strSortOrder = exp;
                else
                    strSortOrder += "," + exp;
            }

            return strSortOrder;
        }
        
    }

    /// <summary>
    /// Teletik paging class contains start and end range of record to fetch from database
    /// </summary>
    public class TelerikPaging
    {
        public int Start { get; set; }
        public int End { get; set; }
    }
}