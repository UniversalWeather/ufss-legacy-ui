﻿
function CalendarFleetCrewTreeViewCheck(isChecked, treeView1, treeView2) {
    //uncheck all checkbox in second tree 
    if (isChecked) {
        for (var i = 0; i < treeView2.get_nodes().get_count() ; i++) {
            var node = treeView2.get_nodes().getNode(i);
            node.set_checked(false);
        }
    }
    //checking checkbox checked in first tree
    var isNodeChecked = false;
    for (var i = 0; i < treeView1.get_nodes().get_count() ; i++) {
        var node = treeView1.get_nodes().getNode(i);
        if (node.get_checked())
            isNodeChecked = true;
    }
    //if all checkbox are unchecked in first tree then all checkbox will checked in second tree
    if (isNodeChecked == false) {
        for (var i = 0; i < treeView2.get_nodes().get_count() ; i++) {
            var node = treeView2.get_nodes().getNode(i);
            node.set_checked(true);
        }
    }
}

function CheckDuplicates(isChecked, valueChecked, treeView1) {
    var allNodes = treeView1.get_allNodes();
    var allNodesCount = allNodes.length;
    for (var i = 0; i < allNodesCount; i++) {
        var node = allNodes[i];
        if (node.get_text() == valueChecked) {
            node.set_checked(isChecked);
        }
    }
}