﻿var PreflightRetrieveGrid = "#dgPreflightRetrieve";

$(document).ready(function () {
    $.extend(jQuery.jgrid.defaults, {
        prmNames: {
            page: "page", rows: "size", order: "dir", sort: "sort"
        }
    });
    PreflightRetrieveGridBind();
    $("#btnSubmit").click(function () {
        var selectedrows = $(PreflightRetrieveGrid).jqGrid('getGridParam', 'selrow');
        if (selectedrows) {
            var rowData = $(PreflightRetrieveGrid).getRowData(selectedrows);
            var TripID = rowData['TripID'];
            $("#hdSelectedTripID").val(TripID);
            returnToParent();
        }
        return false;
    });
});

var PreflightRetrieveViewModel = function () {
    self.PreflightRetrieveAll = {};
    var currentDate = new Date();
    self.DepDate = ko.observable(currentDate.setDate(currentDate.getDate() - (30)));
    var PreflightRetrieveInitializationObject = htmlDecode($("[id$='hdnPFRetrieveInitializationSchemaObject']").val());
    self.PreflightRetrieveAll = ko.mapping.fromJS(JSON.parse(PreflightRetrieveInitializationObject));
};

function returnToParent() {
    oArg = new Object();
    var selectedRows = $(PreflightRetrieveGrid).jqGrid('getGridParam', 'selrow');

    if (selectedRows.length > 0) {
        oArg.TripID = $("#hdSelectedTripID").val();
    }
    else {
        oArg.TripID = "";
    }
    var oWnd = GetRadWindow();
    if (oArg) {
        oWnd.close(oArg);
    }
}

function dataFormatter(cellvalue, options, rowObject) {
    return moment(cellvalue).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
}

function ClientValidator_KO() {
    var btn = document.getElementById("btnClientCode");
    var clientErrorLabel = "#lbcvClient";
    Client_Validate_Retrieve_KO(self.PreflightRetrieveAll, self.PreflightRetrieveAll.ClientID, clientErrorLabel, btn);
}

function ClientCidePopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.PreflightRetrieveAll.ClientCD(arg.ClientCD);
            self.PreflightRetrieveAll.ClientID(arg.ClientID);
            self.PreflightRetrieveAll.ClientDescription(arg.ClientDescription);
            $('#lbcvClient').text('');
        }
        else {
            self.PreflightRetrieveAll.ClientCD("");
            self.PreflightRetrieveAll.ClientID("");
            self.PreflightRetrieveAll.ClientDescription("");
        }
    }
}

function PreflightRetrieveGridBind() {
    jQuery(PreflightRetrieveGrid).jqGrid({
        url: '/Views/Utilities/ApiCallerWithFilter.aspx',
        mtype: 'GET',
        datatype: "json",
        cache: false,
        async: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }
            if (postData.filters != null) {
                var filters = JSON.parse(postData.filters);
                $.each(filters.rules, function (key, result) {
                    if (result.field == "RequestDT" || result.field == "EstDepartureDT") {
                        var value = result.data.trim();
                        if (value.length == 10) {
                            result.data = moment(value, [self.UserPrincipal._ApplicationDateFormat.toUpperCase(), isodateformat]).format(isodateformat);
                        }
                        else {
                            result.data = "";
                        }
                    }
                    postData.filters = JSON.stringify(filters)
                });
            }
            postData.apiType = 'fss';
            postData.method = 'PreflightSearchRetrieve';
            var homebaseID = 0;
            if ($('#chkHomebase').prop("checked")) {
                homebaseID = self.UserPrincipal._homeBaseId;
            }
            var clientID = 0;
            if (!IsNullOrEmpty($("#hdnClient").val())) {
                clientID = $("#hdnClient").val();
            }
            var Tripsheet = "";
            if ($('#ChkTripsheet').prop("checked")) {
                Tripsheet = "T";
            }
            var Worksheet = "";
            if ($('#ChkWorksheet').prop("checked")) {
                Worksheet = "W";
            }
            var Hold = "";
            if ($('#ChkHold').prop("checked")) {
                Hold = "H";
            }
            var Cancelled = "";
            if ($('#ChkCancelled').prop("checked")) {
                Cancelled = "C";
            }
            var SchedServ = "";
            if ($('#ChkSchedServ').prop("checked")) {
                SchedServ = "S";
            }
            var UnFillFilled = "";
            if ($('#ChUnFillFilled').prop("checked")) {
                UnFillFilled = "U";
            }
            var IsLog = false;
            if ($('#ChkExcLog').prop("checked")) {
                IsLog = true;
            }
            var startDepart = formatDateISO(new Date(0));
            if (!IsNullOrEmpty(self.DepDate()))
                startDepart = formatDateISO(self.DepDate());
            var homebaseIDCSVStr = "";
            var isPrivateUser = false;

            if (!self.UserPrincipal.IsSysAdmin) {
                if (self.UserPrincipal.IsTripPrivacy) {
                    isPrivateUser = true;
                }
            }
            postData.clientID = clientID;
            postData.isPrivateUser = isPrivateUser;
            postData.EstDepartDate = startDepart;
            postData.isLog = IsLog;
            if (homebaseID) { postData.homebaseID = homebaseID; } else { postData.homebaseID = undefined; }
            if (Tripsheet) { postData.tripSheet = Tripsheet; } else { postData.tripSheet = undefined; }
            if (Worksheet) { postData.worksheetstr = Worksheet; } else { postData.worksheetstr = undefined; }
            if (Hold) { postData.hold = Hold; } else { postData.hold = undefined; }
            if (Cancelled) { postData.cancelled = Cancelled; } else { postData.cancelled = undefined; }
            if (SchedServ) { postData.schedServ = SchedServ; } else { postData.schedServ = undefined; }
            if (UnFillFilled) { postData.Unfulfilled = UnFillFilled; } else { postData.Unfulfilled = undefined; }
            if (homebaseIDCSVStr) { postData.homebaseIDCSVStr = homebaseIDCSVStr; } else { postData.homebaseIDCSVStr = undefined; }
            return postData;
        },
        autowidth: false,
        width: 917,
        height: 210,
        rowNum: $("#rowNum").val(),
        pager: "#pg_gridPager",
        shrinkToFit: false,
        ignoreCase: true,
        viewrecords: true,
        colNames: ['Trip ID', 'Trip No.', 'Departure Date', 'Requestor', 'Trip Description', 'Status', 'Requestor Date', 'Tail No.'
            , 'Log No.', 'Acknowledge', 'Wait List'],
        colModel: [
            { name: 'TripID', index: 'TripID', hidden: true, key: true },
            { name: 'TripNUM', index: 'TripNUM', width: 60 },
            { name: 'EstDepartureDT', index: 'EstDepartureDT', width: 90, formatter: dataFormatter, searchoptions: { sopt: ['eq'] } },
            { name: 'RequestorFirstName', index: 'RequestorFirstName', width: 70 },
            { name: 'TripDescription', index: 'TripDescription', width: 170 },
            { name: 'TripStatus', index: 'TripStatus', width: 40 },
            { name: 'RequestDT', index: 'RequestDT', width: 90, formatter: dataFormatter, searchoptions: { sopt: ['eq'] } },
            { name: 'TailNum', index: 'TailNum', width: 70 },
            { name: 'LogNum', index: 'LogNum', width: 50 },
            { name: 'Acknowledge', index: 'Acknowledge', width: 80 },
            { name: 'WaitList', index: 'WaitList', width: 50 }
        ],
        ondblClickRow: function (TripID) {
            $("#hdSelectedTripID").val(TripID);
            returnToParent();
        },
        onSelectRow: function (id) {
            var rowData = $(this).getRowData(id);
            var lastSel = rowData['TripID'];
        },
    });
    jQuery("#btnSearch").click(function () {
        jQuery(PreflightRetrieveGrid).setGridParam({ datatype: 'json' });
        jQuery(PreflightRetrieveGrid).trigger("reloadGrid");
    });
    $(PreflightRetrieveGrid).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
    $("#pagesizebox").insertBefore('.ui-paging-info');
}

function reloadPageSize() {
    var myGrid = $(PreflightRetrieveGrid);
    var currentValue = $("#rowNum").val();
    myGrid.setGridParam({ rowNum: currentValue });
    myGrid.trigger('reloadGrid');
}

function reloadPage() {
    var myGrid = $(PreflightRetrieveGrid);
    myGrid.trigger('reloadGrid');
}

function openWin(radWin) {
    var url = '';
    if (radWin == "RadClientCodePopup") {
        url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById("tbClientCode").value;
    }
    var oWnd = radopen(url, radWin);
}
