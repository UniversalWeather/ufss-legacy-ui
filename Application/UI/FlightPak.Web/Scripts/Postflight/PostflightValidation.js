﻿var PostflightAirportBlackList = "IcaoID,AirportID";
function DepartureIcao_Validate_Retrieve(tbDepartureIcao, TimeDisplayTenMin, CurrentLegNum, btn) {
    var depatureName = tbDepartureIcao();
    clearAirportElements(document.getElementById("tbDepart"), 'DEPT');
    resetKOViewModel(self.CurrentLegData.DepartureAirport, PostflightAirportBlackList);
    $("[id$='lbcvDepart']").text("");
    if (depatureName != '' && depatureName != null) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Airport_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: "{ICAO:'" + depatureName + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == false || result.d.Success == false) {
                    $("[id$='lbcvDepart']").text(result.d.ErrorsList);
                    End_Loading(btn, false);
                    getTripExceptionGrid("reload");
                    return false;
                }

                var jsonObj = result.d;//JSON.parse(result.d);
                if (jsonObj.Success == true) {
                    var jsonAirportData = jsonObj.Result;
                    setDepAirportDetail(jsonAirportData);
                    self.CurrentLegData.DepartICAOID(jsonAirportData.AirportID);
                    if (self.CurrentLegData.DepartureAirport == undefined) {
                        self.CurrentLegData.DepartureAirport = ko.mapping.fromJS(jsonAirportData);
                    } else {
                        ko.mapping.fromJS(jsonAirportData, {}, self.CurrentLegData.DepartureAirport);
                    }
                    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.DepartureAirport);
                    CurrentLegData.DepartureAirport.tooltipInfo(htmlDecode(CurrentLegData.DepartureAirport.tooltipInfo()));
                    if (jsonAirportData.AirportID == self.CurrentLegData.DepartureAirport.AirportID()) {
                        setAlertforAirport(self.CurrentLegData.DepartureAirport, document.getElementById("tbDepart"), 'DEPT');
                    }
                    var arrIcaoID = IsNullOrEmpty(self.CurrentLegData.ArrivalAirport.IcaoID()) ? "" : self.CurrentLegData.ArrivalAirport.IcaoID();
                    $("#Leg" + self.CurrentLegData.LegNUM()).text('Leg ' + self.CurrentLegData.LegNUM() + '(' + self.CurrentLegData.DepartureAirport.IcaoID() + '-' + arrIcaoID + ')');
                    DomesticInternational_Validate_Retrieve();
                    POCalculateMiles_Validate_Retrieve();
                    End_Loading(btn, true);
                } else {
                    End_Loading(btn, false);
                    $("[id$='lbcvDepart']").text(jsonObj.ErrorsList[0]);
                }

                getTripExceptionGrid("reload");
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    } else {
        
        $("#Leg" + self.CurrentLegData.LegNUM()).text('Leg ' + self.CurrentLegData.LegNUM() + '(' + (self.CurrentLegData.DepartureAirport.IcaoID() == null ? '' : self.CurrentLegData.DepartureAirport.IcaoID()) + '-' + (self.CurrentLegData.ArrivalAirport.IcaoID() == null ? '' : self.CurrentLegData.ArrivalAirport.IcaoID()) + ')');
        getTripExceptionGrid("reload");
    }
}

function ArrivalIcao_Validate_Retrieve(tbArrivesIcao, TimeDisplayTenMin, CurrentLegNum, btn) {
    var arrivesName = tbArrivesIcao();
    clearAirportElements(document.getElementById("tbArrival"), 'ARR');
    resetKOViewModel(self.CurrentLegData.ArrivalAirport, PostflightAirportBlackList);
    $("[id$='lbcvArrival']").text("");
    if (arrivesName != '' && arrivesName != null) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Airport_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: "{ICAO:'" + arrivesName + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == false || result.d.Success == false) {
                    $("[id$='lbcvArrival']").text(result.d.ErrorsList);
                    End_Loading(btn, false);
                    getTripExceptionGrid("reload");
                    return false;
                }
                var jsonObj = result.d;// JSON.parse(result.d);
                if (jsonObj.Success == true) {
                    var jsonAirportData = jsonObj.Result;
                    setArrivalAirportDetail(jsonAirportData);
                    self.CurrentLegData.ArriveICAOID(jsonAirportData.AirportID);
                    if (self.CurrentLegData.ArrivalAirport == undefined) {
                        self.CurrentLegData.ArrivalAirport = ko.mapping.fromJS(jsonAirportData);
                    } else {
                        ko.mapping.fromJS(jsonAirportData, {}, self.CurrentLegData.ArrivalAirport);
                    }
                    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.ArrivalAirport);
                    CurrentLegData.ArrivalAirport.tooltipInfo(htmlDecode(CurrentLegData.ArrivalAirport.tooltipInfo()));
                    if (jsonAirportData.AirportID == self.CurrentLegData.ArrivalAirport.AirportID()) {
                        setAlertforAirport(self.CurrentLegData.ArrivalAirport, document.getElementById("tbArrival"), 'ARR');
                    }
                    var DepartureIcaoID = IsNullOrEmpty(self.CurrentLegData.DepartureAirport.IcaoID()) ? "" : self.CurrentLegData.DepartureAirport.IcaoID();
                    $("#Leg" + self.CurrentLegData.LegNUM()).text('Leg ' + self.CurrentLegData.LegNUM() + '(' + DepartureIcaoID + '-' + self.CurrentLegData.ArrivalAirport.IcaoID() + ')');
                    DomesticInternational_Validate_Retrieve();
                    POCalculateMiles_Validate_Retrieve();
                    End_Loading(btn, true);
                } else {
                    End_Loading(btn, false);
                    $("[id$='lbcvArrival']").text(jsonObj.ErrorsList[0]);
                    
                }
                getTripExceptionGrid("reload");
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    } else {
        
        $("#Leg" + self.CurrentLegData.LegNUM()).text('Leg ' + self.CurrentLegData.LegNUM() + '(' + self.CurrentLegData.DepartureAirport.IcaoID() + '-' + self.CurrentLegData.ArrivalAirport.IcaoID() + ')');
        getTripExceptionGrid("reload");
    }
}

function setHomebaseAirportDetail(homebaseAirportObj) {
    var jsonObj = homebaseAirportObj;
    if (jsonObj != null && jsonObj != '') {
        $("[id$='HomebaseAirpotObj']").val(JSON.stringify(homebaseAirportObj));
    }
}
function setArrivalAirportDetail(arrAirportObj) {
    if (arrAirportObj != null && arrAirportObj != '') {
        $("[id$='ArriAirpotObj']").val(JSON.stringify(arrAirportObj));
    }
}
function setAircraftDetail(aircraftObj) {
    if (aircraftObj != null && aircraftObj != '') {
        $("[id$='AircraftObj']").val(JSON.stringify(aircraftObj));
    }
}
function setDepAirportDetail(depAirportDetail) {
    if (depAirportDetail != '' && depAirportDetail != null) {
        $("[id$='DepAirpotObj']").val(JSON.stringify(depAirportDetail));
    }
}

function setAirportDetails(airportObservable, AirportData) {
    airportObservable.AirportID(AirportData.AirportID);
    airportObservable.IcaoID(AirportData.IcaoID == null ? "" : AirportData.IcaoID);
    airportObservable.CityName(AirportData.CityName);
    airportObservable.StateName(AirportData.StateName);
    airportObservable.CountryName(AirportData.CountryName);
    airportObservable.AirportName(AirportData.AirportName);
    airportObservable.CountryID(AirportData.CountryID);
    airportObservable.LatitudeDegree(AirportData.LatitudeDegree);
    airportObservable.LatitudeMinutes(AirportData.LatitudeMinutes);
    airportObservable.LatitudeNorthSouth(AirportData.LatitudeNorthSouth);
    airportObservable.LongitudeDegrees(AirportData.LongitudeDegrees);
    airportObservable.LongitudeMinutes(AirportData.LongitudeMinutes);
    airportObservable.LongitudeEastWest(AirportData.LongitudeEastWest);
    airportObservable.IsDayLightSaving(AirportData.IsDayLightSaving);
    airportObservable.DayLightSavingStartDT(AirportData.DayLightSavingStartDT != null ? moment(AirportData.DayLightSavingStartDT).format() : null);
    airportObservable.DayLightSavingEndDT(AirportData.DayLightSavingEndDT != null ? moment(AirportData.DayLightSavingEndDT).format() : null);
    airportObservable.DaylLightSavingStartTM(AirportData.DaylLightSavingStartTM);
    airportObservable.DayLightSavingEndTM(AirportData.DayLightSavingEndTM);
    airportObservable.OffsetToGMT(AirportData.OffsetToGMT);
    airportObservable.WindZone((AirportData.WindZone == null || AirportData.WindZone == '') ? 0 : AirportData.WindZone);
    airportObservable.LandingBIAS(AirportData.LandingBIAS);
    airportObservable.TakeoffBIAS(AirportData.TakeoffBIAS);
}

function DomesticInternational_Validate_Retrieve() {
    var DepartureCountry = self.CurrentLegData.DepartureAirport.CountryID();
    var ArrivalCountry = self.CurrentLegData.ArrivalAirport.CountryID();
    var HomebaseCountry = $("#hdnHomeBaseCountry").val();
    var LegNUM = self.CurrentLegData.LegNUM();
    $.ajax({
        async: true,
        url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/DomesticInternational_Validate_Retrieve",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ "HomeBaseCountryId": HomebaseCountry, "DepartureCountryId": DepartureCountry, "ArrivalCountryId": ArrivalCountry, "LegNUM": LegNUM }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                self.CurrentLegData.DutyTYPE(result.d.Result.DutyTYPE);
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function POCalculateMiles_Validate_Retrieve() {
    var departICAOID = self.CurrentLegData.DepartICAOID();
    var arriveICAOID = self.CurrentLegData.ArriveICAOID();
    $.ajax({
        async: true,
        url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/POCalculateMiles_Validate_Retrieve",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ "departICAOID": departICAOID, "arriveICAOID": arriveICAOID }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                self.CurrentLegData.Distance(result.d.Result.Miles);
                self.CurrentLegData.ConvertedDistance(result.d.Result.KiloOrMiles);
                SaveCurrentLegDataToSession_Schedule_Calculation();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function PODelayType_Validate_Retrieve(delayType, btn) {
    resetKOViewModel(self.CurrentLegData.DelayType, "DelayTypeCD");
    self.CurrentLegData.DelayTypeID(0);
    self.CurrentLegData.DelayType.DelayTypeID(0);
    $("[id$='lbcvDelay']").text('');

    if (delayType != '' && delayType != null) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/PODelayType_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ "DelayType": delayType }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = result.d;
                $("[id$='lbcvDelay']").text("");
                if (jsonObj.Success == true) {
                    var jsonObjDelayType = jsonObj.Result;
                    if (self.CurrentLegData.DelayType == undefined) {
                        self.CurrentLegData.DelayType = ko.mapping.fromJS(jsonObjDelayType);
                    } else {
                        ko.mapping.fromJS(jsonObjDelayType, {}, self.CurrentLegData.DelayType);
                    }
                    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.DelayType);
                    self.CurrentLegData.DelayTypeID(jsonObjDelayType.DelayTypeID);
                    End_Loading(btn, true);
                    getTripExceptionGrid("reload");
                }
                else {
                    End_Loading(btn, false);
                    if (jsonObj.ErrorsList.length > 0) {
                        $("[id$='lbcvDelay']").text(jsonObj.ErrorsList);
                    }
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportPostflightError(jqXHR, textStatus, errorThrown);
                getTripExceptionGrid("reload");
            }
        });
    } else {
        getTripExceptionGrid("reload");
    }
}

function setLegCalculationResults(currentLeg)
{
    self.CurrentLegData.FedAviationRegNum(currentLeg.FedAviationRegNum);

    self.CurrentLegData.BeginningDutyHours(currentLeg.BeginningDutyHours);
    self.CurrentLegData.EndDutyHours(currentLeg.EndDutyHours);

    self.CurrentLegData.BlockHoursTime(currentLeg.BlockHoursTime);
    self.CurrentLegData.FlightHoursTime(currentLeg.FlightHoursTime);
    self.CurrentLegData.DutyHrsTime(currentLeg.DutyHrsTime);

    self.CurrentLegData.FlightCost(currentLeg.FlightCost);

    self.CurrentLegData.AirFrameHoursTime(currentLeg.AirFrameHoursTime);
    self.CurrentLegData.Engine1HoursTime(currentLeg.Engine1HoursTime);
    self.CurrentLegData.Engine2HoursTime(currentLeg.Engine2HoursTime);
    self.CurrentLegData.Engine3HoursTime(currentLeg.Engine3HoursTime);
    self.CurrentLegData.Engine4HoursTime(currentLeg.Engine4HoursTime);

    self.CurrentLegData.AirframeCycle(currentLeg.AirframeCycle);
    self.CurrentLegData.Engine1Cycle(currentLeg.Engine1Cycle);
    self.CurrentLegData.Engine2Cycle(currentLeg.Engine2Cycle);
    self.CurrentLegData.Engine3Cycle(currentLeg.Engine3Cycle);
    self.CurrentLegData.Engine4Cycle(currentLeg.Engine4Cycle);

}

function ScheduledHours_Calculate_Validate_Retrieve(isCalculateDutyHour) {
    if (IsNullOrEmpty(self.CurrentLegData.ScheduledDate())) {
        return false;
    }

    var LegNum = CurrentLegNum();
    var scheduledTime = self.CurrentLegData.ScheduledTime();
    var outTime = self.CurrentLegData.OutTime();
    var inTime = self.CurrentLegData.InTime();
    var offTime = self.CurrentLegData.TimeOff();
    var onTime = self.CurrentLegData.TimeOn();
    var scheduledDate = self.CurrentLegData.ScheduledDate();
    var outDate = self.CurrentLegData.OutDate();
    var inDate = self.CurrentLegData.InDate();
    var miles = self.CurrentLegData.Distance();
    var stMiles = self.CurrentLegData.StatuteMiles();

    if (miles == undefined || IsNullOrEmpty(miles)) {
        miles = 0;
        self.CurrentLegData.Distance(miles);
    }

    if (stMiles == undefined || IsNullOrEmpty(stMiles)) {
        stMiles = 0;
        //self.CurrentLegData.StatuteMiles(stMiles);
    }

    if (LegNum == undefined)
        LegNum = 1;

    var validateTimeValues = false;

    //&& IsNullOrEmpty(inTime) == false
    if (IsNullOrEmpty(scheduledTime) == false && IsNullOrEmpty(outTime) == false && IsNullOrEmpty(offTime) == false && IsNullOrEmpty(onTime) == false) {
        if (scheduledTime.length == 5 && outTime.length == 5 && offTime.length == 5 && onTime.length == 5) {
            validateTimeValues = true;
        }
    }
    setScheduleDateFontColor();
    //&& IsNullOrEmpty(inDate) == false

    if (IsNullOrEmpty(LegNum) == false && validateTimeValues == true && IsNullOrEmpty(scheduledDate) == false && IsNullOrEmpty(outDate) == false && miles >= 0) {
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/ScheduledHours_Calculate_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ "LegNUM": LegNum, "scheduleTime": scheduledTime, "outTime": outTime, "inTime": inTime, "offTime": offTime, "onTime": onTime, "scheduleDate": scheduledDate, "outDate": outDate, "inDate": inDate, "hdnMiles": miles, "tbStMiles": stMiles }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    var jsonObj = response.d.Result;
                    var jsonObjLeg = jsonObj.Leg;
                    setLegCalculationResults(jsonObjLeg);
                    if (jsonObj.DutyHoursStyle == 'red') {
                        $('#tbDutyHours').attr("style", "background-color: #fe5b5c !important;border-color: #ea0303 !important;");
                    } else {
                        $('#tbDutyHours').attr("style", "background-color: #F1F1F1 !important;border-color: #cccccc !important;");
                    }
                    if (isCalculateDutyHour == true) {
                        setTimeout(function() {
                            CrewDutyHours_Validate_Retrieve_KO();
                        }, 1000);
                    } else {
                        InitializeBottomAndSummaryGrids();
                    }

                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });

    } 
}

function GMT_Validate_Retrieve(changedValue, time, DepartOrArr) {
    var calculate = true;
    var timeArray = time.split(':');
    var hrs = Number(timeArray[0]);
    var mins = Number(timeArray[1]);
    if (hrs > 23) {
        jAlert("Hour entry must be between 0 and 23", "Trip Alert");
        calculate = false;
    }
    else if (mins > 59) {
        jAlert("Minute entry must be between 0 and 59", "Trip Alert");
        calculate = false;
    }
    return calculate;
}

function POCrewDutyRule_Validate_Retrieve(btn) {
    crewDutyRulesID = CurrentLegData.CrewDutyRulesID();
    crewDutyRuleCD = CurrentLegData.CrewDutyRule.CrewDutyRuleCD();
    if (crewDutyRulesID != '' && crewDutyRulesID != null)
        crewDutyRulesID = 0;

    resetKOViewModel(self.CurrentLegData.CrewDutyRule, "CrewDutyRuleCD");
    self.CurrentLegData.CrewCurrency("");
    self.CurrentLegData.CrewDutyRulesID(0);
    self.CurrentLegData.CrewDutyRule.CrewDutyRulesID(0);
    $("[id$='lbcvRules']").text('');

    if ((crewDutyRulesID != '' && crewDutyRulesID != null) || (crewDutyRuleCD != '' && crewDutyRuleCD != null)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/POCrewDutyRule_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ "CrewDutyRulesID": crewDutyRulesID, "CrewDutyRuleCD": crewDutyRuleCD }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == false || result.d.Success == false) {
                    $("[id$='lbcvRules']").text(result.d.ErrorsList[0]);
                    End_Loading(btn, false);
                    return false;
                }
                var jsonObj = result.d;
                if (jsonObj.Success == true) {
                    var jsonObjDutyType = jsonObj.Result;
                    if (self.CurrentLegData.CrewDutyRule == undefined) {
                        self.CurrentLegData.CrewDutyRule = ko.mapping.fromJS(jsonObjDutyType);
                    } else {
                        ko.mapping.fromJS(jsonObjDutyType, {}, self.CurrentLegData.CrewDutyRule);
                    }
                    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.CrewDutyRule);
                    CurrentLegData.CrewDutyRule.CrewDutyRuleCD(jsonObjDutyType.CrewDutyRuleCD.toString().trim());
                    CurrentLegData.CrewCurrency(jsonObjDutyType.CrewDutyRuleCD);
                    CurrentLegData.CrewDutyRulesID(jsonObjDutyType.CrewDutyRulesID);
                    CurrentLegData.FedAviationRegNum(jsonObjDutyType.FedAviatRegNum.toString().trim());
                    CurrentLegData.CrewDutyRule.tooltipInfo(htmlDecode(CurrentLegData.CrewDutyRule.tooltipInfo()));
                    End_Loading(btn, true);
                    SaveCurrentLegDataToSession_Schedule_Calculation();
                }
                else {
                    if (jsonObj.ErrorsList.length > 0) {
                        End_Loading(btn, false);
                    }
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    } else {
        $("[id$='lbcvRules']").text("");
    }
}
 
function POCategory_Validate_Retrieve(tbCategoryCD, lbCategoryDesc, hdLegNum, btn) {
    if (tbCategoryCD() != '' && tbCategoryCD() != null) {
        var LegNum = hdLegNum;
        Start_Loading(btn);
        try {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/POCategory_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'CategoryCD': tbCategoryCD(), 'LegNum': LegNum }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var jsonObj = result.d;
                    if (jsonObj.Success == true) {
                        End_Loading(btn, true);
                        objFlightCat = jsonObj.Result;
                        if (objFlightCat.ShowConfirmPopUp == true) {
                            CurrentLegData.FlightCategoryID(objFlightCat.FlightCategoryID);
                            tbCategoryCD(objFlightCat.FlightCatagoryCD);
                            lbCategoryDesc(objFlightCat.FlightCatagoryDescription);
                            $("[id$='lbcvCategory']").text('');
                            jConfirm("Passengers existing for this leg will be deleted. Do you want to continue changing the Flight category?", "Confirmation!", confirmDeadHeadLegCallBackFn);
                        }
                        else {
                            tbCategoryCD(objFlightCat.FlightCatagoryCD);
                            lbCategoryDesc(objFlightCat.FlightCatagoryDescription);
                            CurrentLegData.FlightCategoryID(objFlightCat.FlightCategoryID);
                            $("[id$='lbcvCategory']").text('');
                        }
                    } else {
                        End_Loading(btn, false);
                        $("[id$='lbcvCategory']").text(jsonObj.ErrorsList[0]);
                        $("[id$='hdnDeadCategory']").val('');
                        $("[id$='hdCategory']").val('');
                    }

                }, error: function (jqXHR, textStatus, errorThrown) {
                    End_Loading(btn, false);
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                }
            });
        } catch (e) { }
    } else {
        
        tbCategoryCD('');
        lbCategoryDesc('')
        CurrentLegData.FlightCategoryID('');
        $("[id$='lbcvCategory']").text('');
        $("[id$='hdnDeadCategory']").val('');
        $("[id$='hdCategory']").val('');
    }
}

function POEndofDuty_Check_Validate_Retrieve(LegNum, tbBlockHours, tbFlightHours, tbDutyHours) {
    if (tbBlockHours != '' && tbBlockHours != null
        && tbFlightHours != '' && tbFlightHours != null
        && tbDutyHours != '' && tbDutyHours != null) {
        try {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/POEndofDuty_Check_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'LegNum': LegNum, 'blockHours': tbBlockHours, 'flightHours': tbFlightHours, 'dutyHours': tbDutyHours }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var jsonObj = result.d;
                    if (jsonObj.Success == true) {
                        objLeg = jsonObj.Result;
                        SaveCurrentLegDataToSession_Schedule_Calculation();
                    } 
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                }
            });
        } catch (e) { }
    } else {
        
    }
}

function PayableVendor_Validate_Retrieve_KO(vendorVM, vendorID, vendorErrorLabel, btn) {
    vendorVM.VendorCD(vendorVM.VendorCD().toUpperCase());
    var vendorCD = vendorVM.VendorCD();
    if (!IsNullOrEmpty(vendorCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/PayableVendor_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'VendorCD': vendorCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var vendorErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, vendorVM);
                    vendorID(vendorVM.VendorID());
                    End_Loading(btn, true);
                }
                else {
                    vendorErrorText = "Invalid Vendor Code";
                    resetKOViewModel(vendorVM,["VendorCD"]);
                    vendorID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(vendorErrorLabel)) {
                    if (!vendorErrorLabel.match('^#'))
                        vendorErrorLabel = '#' + vendorErrorLabel;
                    $(vendorErrorLabel).text(vendorErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(vendorVM);
        vendorID("");        
        if (!vendorErrorLabel.match('^#'))
            vendorErrorLabel = '#' + vendorErrorLabel;
        $(vendorErrorLabel).text("");
    }
}

function PaymentType_Validate_Retrieve_KO(paymentTypeVM, paymentTypeID, paymentTypeErrorLabel, btn) {
    paymentTypeVM.PaymentTypeCD(paymentTypeVM.PaymentTypeCD().toUpperCase());
    var paymentCD = paymentTypeVM.PaymentTypeCD();
    if (!IsNullOrEmpty(paymentCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/PaymentType_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'PaymentCD': paymentCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var paymentTypeErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, paymentTypeVM);
                    paymentTypeID(paymentTypeVM.PaymentTypeID());
                    End_Loading(btn, true);
                }
                else {
                    End_Loading(btn, false);
                    paymentTypeErrorText = "Code Does Not Exist";
                    resetKOViewModel(paymentTypeVM,["PaymentTypeCD"]);
                    paymentTypeID("");
                }
                if (!IsNullOrEmptyOrUndefined(paymentTypeErrorLabel)) {
                    if (!paymentTypeErrorLabel.match('^#'))
                        paymentTypeErrorLabel = '#' + paymentTypeErrorLabel;
                    $(paymentTypeErrorLabel).text(paymentTypeErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(paymentTypeVM);
        paymentTypeID("");        
        if (!paymentTypeErrorLabel.match('^#'))
            paymentTypeErrorLabel = '#' + paymentTypeErrorLabel;
        $(paymentTypeErrorLabel).text("");
    }
}

function Crew_Validate_Retrieve_KO(crewVM, crewID, crewErrorLabel, btn) {
    crewVM.CrewCD(crewVM.CrewCD().toUpperCase());
    var crewCD = crewVM.CrewCD();
    if (!IsNullOrEmpty(crewCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/Crew_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'CrewCD': crewCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var crewErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, crewVM);
                    crewID(crewVM.CrewID());
                    End_Loading(btn, true);
                }
                else {
                    crewErrorText = "Crew Code Does Not Exist";
                    resetKOViewModel(crewVM, ["CrewCD"]);
                    crewID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(crewErrorLabel)) {
                    if (!crewErrorLabel.match('^#'))
                        crewErrorLabel = '#' + crewErrorLabel;
                    $(crewErrorLabel).text(crewErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(crewVM);
        crewID("");
        
        if (!crewErrorLabel.match('^#'))
            crewErrorLabel = '#' + crewErrorLabel;
        $(crewErrorLabel).text("");
    }
}

function FlightCatagory_Validate_Retrieve_KO(flightCategoryVM, flightCategoryID, flightCategoryErrorLabel, btn) {
    flightCategoryVM.FlightCatagoryCD(flightCategoryVM.FlightCatagoryCD().toUpperCase());
    var flightCategoryCD = flightCategoryVM.FlightCatagoryCD();
    if (!IsNullOrEmpty(flightCategoryCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/FlightCatagory_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'FlightCatagoryCD': flightCategoryCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var flightCategoryErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, flightCategoryVM);
                    removeUnwantedPropertiesKOViewModel(flightCategoryVM);
                    flightCategoryID(flightCategoryVM.FlightCategoryID());
                    End_Loading(btn, true);
                }
                else {
                    flightCategoryErrorText = "Category Code Does Not Exist";
                    resetKOViewModel(flightCategoryVM, ["FlightCategoryID", "FlightCatagoryCD"]);
                    flightCategoryID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(flightCategoryErrorLabel)) {
                    if (!flightCategoryErrorLabel.match('^#'))
                        flightCategoryErrorLabel = '#' + flightCategoryErrorLabel;
                    $(flightCategoryErrorLabel).text(flightCategoryErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(flightCategoryVM, ["FlightCategoryID"]);
        flightCategoryID("");
        if (!flightCategoryErrorLabel.match('^#'))
            flightCategoryErrorLabel = '#' + flightCategoryErrorLabel;
        $(flightCategoryErrorLabel).text("");
    }
}

function FuelLocator_Validate_Retrieve_KO(fuelLocatorVM, fuelLocatorID, fuelLocatorErrorLabel, btn) {
    fuelLocatorVM.FuelLocatorCD(fuelLocatorVM.FuelLocatorCD().toUpperCase());
    var fuelLocCD = fuelLocatorVM.FuelLocatorCD();
    if (!IsNullOrEmpty(fuelLocCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/FuelLocator_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'FuelLocatorCD': fuelLocCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var fuelLocatorErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, fuelLocatorVM);
                    fuelLocatorID(fuelLocatorVM.FuelLocatorID());
                    End_Loading(btn, true);
                }
                else {
                    fuelLocatorErrorText = "Code Does Not Exist";
                    resetKOViewModel(fuelLocatorVM, ["FuelLocatorCD"]);
                    fuelLocatorID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(fuelLocatorErrorLabel)) {
                    if (!fuelLocatorErrorLabel.match('^#'))
                        fuelLocatorErrorLabel = '#' + fuelLocatorErrorLabel;
                    $(fuelLocatorErrorLabel).text(fuelLocatorErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(fuelLocatorVM);
        fuelLocatorID("");
        
        if (!fuelLocatorErrorLabel.match('^#'))
            fuelLocatorErrorLabel = '#' + fuelLocatorErrorLabel;
        $(fuelLocatorErrorLabel).text("");
    }
}

function CrewDutyHours_Validate_Retrieve_KO() {

    var legNum = CurrentLegNum();
    var tbBlockHours = self.CurrentLegData.BlockHours();
    var tbFlightHours = self.CurrentLegData.FlightHours();
    var tbDutyHours = self.CurrentLegData.DutyHrs();
    
    if (legNum == undefined)
        legNum = 1;

    if (IsNullOrEmpty(legNum) == false && IsNullOrEmpty(tbBlockHours) == false && IsNullOrEmpty(tbFlightHours) == false && IsNullOrEmpty(tbDutyHours) == false ) {
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/CrewDutyHours_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ "LegNUM": legNum, "blockHours": tbBlockHours, "flightHours": tbFlightHours, "dutyHours": tbDutyHours }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    setLegCalculationResults(response.d.Result);
                    FormatLegDates();
                    InitializeBottomAndSummaryGrids();
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    } else {

    }
}

function Requestor_Validate_Retrieve_KO(PassengerRequestorVM, passengerRequestorID, passengerRequestorErrorLabel, btn) {
    PassengerRequestorVM.PassengerRequestorCD(PassengerRequestorVM.PassengerRequestorCD().toUpperCase());
    var paxCD = PassengerRequestorVM.PassengerRequestorCD();
    if (!IsNullOrEmpty(paxCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/Requestor_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'PassengerRequestorCD': paxCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var paxRequestorErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, PassengerRequestorVM);
                    passengerRequestorID(PassengerRequestorVM.PassengerRequestorID());
                    End_Loading(btn, true);
                }
                else {
                    paxRequestorErrorText = "Invalid Requestor Code.";
                    resetKOViewModel(PassengerRequestorVM, ["PassengerRequestorCD", "PassengerRequestorID", "PostflightPassengerListID", "SIFLSecurity", "IsDeleted", "State"]);
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(paxRequestorErrorText)) {
                    if (!passengerRequestorErrorLabel.match('^#'))
                        passengerRequestorErrorLabel = '#' + passengerRequestorErrorLabel;
                    $(passengerRequestorErrorLabel).html(paxRequestorErrorText);
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);                
                reportValidationError(jqXHR, textStatus, errorThrown);                
                return;
            }
        });
    }
    else {
        if (!passengerRequestorErrorLabel.match('^#'))
            passengerRequestorErrorLabel = '#' + passengerRequestorErrorLabel;
        $(passengerRequestorErrorLabel).html("");
        resetKOViewModel(PassengerRequestorVM, ["PassengerRequestorID", "PostflightPassengerListID", "SIFLSecurity", "IsDeleted","State"]);
        passengerRequestorID(0);        
        
    }
}

function TailNum_PostflightValidate_Retrieve_KO(FleetID,fleetErrorlabel,btn,AircraftCD,poMainVM)
{
    if (poMainVM.Fleet.TailNum() == null)
        poMainVM.Fleet.TailNum("");

    poMainVM.Fleet.TailNum(poMainVM.Fleet.TailNum().toUpperCase());
    var tailNum = (poMainVM.Fleet.TailNum().toUpperCase());
    if (!IsNullOrEmpty(tailNum)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/TailNum_PostflightValidate_Retrieve_KO",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'TailNumber': tailNum }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var tailNumErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    if (response.d.Result.Fleet != undefined) {
                        if (poMainVM != undefined && poMainVM.Fleet != undefined) {
                            ko.mapping.fromJS(response.d.Result.Fleet, {}, poMainVM.Fleet);
                        } 
                        FleetID(response.d.Result.Fleet.FleetID);
                        AircraftCD(response.d.Result.Fleet.AircraftCD);
                        poMainVM.AircraftID(response.d.Result.AircraftID);
                    }
                    End_Loading(btn, true);
                }
                else {
                    tailNumErrorText = "Invalid Aircraft Tail No.";                    
                    resetKOViewModel(poMainVM.Fleet, ["FleetID", "TailNum"]);
                    FleetID("");
                    End_Loading(btn, false);
                }
                if (response.d.Result.homebase != undefined) {
                    if (poMainVM != undefined && poMainVM.Homebase != undefined) {
                        ko.mapping.fromJS(response.d.Result.homebase, {}, poMainVM.Homebase);
                        poMainVM.HomebaseID(response.d.Result.homebase.HomebaseID);
                    }else
                    {
                        poMainVM.HomebaseID(null);
                    }
                }

                if (!IsNullOrEmptyOrUndefined(fleetErrorlabel)) {
                    if (!fleetErrorlabel.match('^#'))
                        fleetErrorlabel = '#' + fleetErrorlabel;
                    $(fleetErrorlabel).html(tailNumErrorText);
                }                              
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
                return;
            }
        });
    }
    else {
        if (!fleetErrorlabel.match('^#'))
            fleetErrorlabel = '#' + fleetErrorlabel;
        $(fleetErrorlabel).html("");
        resetKOViewModel(poMainVM.Fleet, ["FleetID"]);
        FleetID("");
        
    }
}

function TailNum_Validate_Retrieve_KO(fleetVM, fleetID, fleetErrorlabel, btn) {
    fleetVM.TailNum(fleetVM.TailNum().toUpperCase());
    var tailNum = fleetVM.TailNum();
    if (!IsNullOrEmpty(tailNum)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx//TailNum_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'TailNum': tailNum }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var fleetErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, fleetVM);
                    fleetID(fleetVM.FleetID());
                    End_Loading(btn, true);
                }
                else {
                    fleetErrorText = "TailNo Does Not Exist";
                    resetKOViewModel(fleetVM, ["FleetID", "TailNum"]);
                    fleetID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(fleetErrorlabel)) {
                    if (!fleetErrorlabel.match('^#'))
                        fleetErrorlabel = '#' + fleetErrorlabel;
                    $(fleetErrorlabel).text(fleetErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(fleetVM);
        fleetID("");
        
        if (!fleetErrorlabel.match('^#'))
            fleetErrorlabel = '#' + fleetErrorlabel;
        $(fleetErrorlabel).text("");
    }
}

function Crew_Validate_Retrieve(CrewCD, crewErrorLabel, btn) {
    var crewCD = CrewCD.toUpperCase();
    if (!IsNullOrEmpty(crewCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/Crew_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'CrewCD': crewCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var crewErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    AddCrews(response.d.ResultList, true);
                    End_Loading_Toppage_Search(btn, true);
                }
                else {
                    crewErrorText = "Crew Code Does Not Exist";
                    End_Loading_Toppage_Search(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(crewErrorLabel)) {
                    if (!crewErrorLabel.match('^#'))
                        crewErrorLabel = '#' + crewErrorLabel;
                    $(crewErrorLabel).text(crewErrorText);
                }                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading_Toppage_Search(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
                return true;
            }            
        });
    }
    else {
        if (!crewErrorLabel.match('^#'))
            crewErrorLabel = '#' + crewErrorLabel;
        $(crewErrorLabel).text('');
    }
}

function AssociatePassengerCD_Retrieve_KO(AssocPaxVM,AssocID,errorLabel,btn)
{
    var paxCD = AssocPaxVM.AssociatedWithCD().toUpperCase();
    if (!IsNullOrEmpty(paxCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/AssociatePassengerCD_Retrieve_KO",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'PassengerCD': paxCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var ErrorText = "";
                if (returnResult == true && response.d.Success == true) {                    
                    AssocPaxVM.AssociatedWithCD(response.d.Result.PassengerRequestorCD);
                    AssocPaxVM.PassengerName(response.d.Result.PassengerName);
                    AssocID(AssocPaxVM.PassengerRequestorID());
                    End_Loading(btn, true);
                }
                else {
                    ErrorText = "Invalid Associated With Passenger Code.";
                    AssocPaxVM.PassengerName("");
                    AssocPaxVM.PassengerRequestorCD("");
                    AssocID(0);
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(errorLabel)) {
                    if (!errorLabel.match('^#'))
                        errorLabel = '#' + errorLabel;
                    $(errorLabel).text(ErrorText);
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);                
                reportValidationError(jqXHR, textStatus, errorThrown);
                return true;
            }
        });
    }
    else {
        AssocPaxVM.PassengerName("");
        AssocPaxVM.PassengerRequestorCD("");
        AssocID(0);
        $(errorLabel).text("");        
    }
}