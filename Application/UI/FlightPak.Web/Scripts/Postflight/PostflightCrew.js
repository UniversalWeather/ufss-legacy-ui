﻿var Tenth = 1;
var Minute = 2;
var Other = 0;
var jqCrewTableId = "#jqCrewGrid";
var msgTitle = "Flight Log Manager - Crew";
var legNum;
var self = this;
var crewsList = new Array();
var lastSelectedRow = null;
var inputBox = '<input type="text" style="font-family:Arial;font-size:12px;width:95%;" onchange="javascript:UpdateCrew(this);" value="';
var inputBoxWOEvent = '<input type="text" style="font-family:Arial;font-size:12px;width:95%;" value="';
var colNames = ['PostflightCrewListID', 'RowNumber', 'CrewID', 'Crew Name', 'OrderNUM', 'CrewFirstName', 'CrewLastName', 'POS', 'T/D', 'T/N', 'L/D', 'L/N', 'App/P', 'App/N', 'Instr', 'Night', 'Days Away', 'IsRemainOverNightOverride', 'RON', 'Duty Beg', 'Duty End', 'IsOverride', 'Duty', 'Blk Hrs', 'Flt Hrs', 'Seat', 'IsAugmentCrew', 'chkIsAugmentCrew', 'Duty Beg', 'Duty End'];
var viewModel;

$(document).ready(function () {
    getUserPrincipal();
    SetTabSelection();
    ApplyControlMask();
    FlighthrsBlockhrs_ChangeEvent();
    $(".textDatePicker").datepicker({
        showOn: 'input',
        changeMonth: true,
        changeYear: true,
        dateFormat: self.UserPrincipal._ApplicationDateFormat.toLowerCase().replace(/yyyy/g, "yy")
    });
});

var PostflightCrewViewModel = function () {
    self.Legs = ko.observableArray().extend({ notify: 'always' });
    self.CurrentLegData = ko.observable().extend({ notify: 'always' });
    self.CurrentLegNum = ko.observable(1);
    self.SelectedCrewData = ko.observable().extend({ notify: 'always' });
    var schemaElement = document.getElementById('hdnPostflightLegObject').value;
    document.getElementById('hdnPostflightLegObject').value = "";
    var jsonSchema = JSON.parse(htmlDecode(schemaElement));
    
    UserIdentity(PopupLoad);
    function InitializePostflightCrew() {
        RequestStart();
        if (!IsNullOrEmpty(jsonSchema)) {
            self.Legs = ko.mapping.fromJS(jsonSchema.Result.Legs);
            self.CurrentLegData = ko.mapping.fromJS(jsonSchema.Result.CurrentLeg);
            self.POEditMode(jsonSchema.Result.POEditMode);
            FormatLegDates();
            GetCurrentLegCrews();
            RefreshCrewSection();
            legNum = 1;
            setTimeout(function () {
                if (self.CurrentLegData.LegNUM() > 1) {
                    self.CurrentLegNum(self.CurrentLegData.LegNUM());
                    legNum = self.CurrentLegData.LegNUM();
                }
            }, 700);
        }
        ResponseEnd();
    
    }
    self.CurrentLegNum.subscribe(function (legnumVal) {
        RequestStart();
        if (!IsNullOrEmptyOrUndefined(legnumVal))
            legNum = Number(legnumVal);
        if (legNum > 0) {
            self.CurrentLegNumFloatbar(legNum);
            $.ajax({
                async: true,
                url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/InitializePostflightLegs",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'legNum': legNum }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var jsonObj = result.d;
                    if (jsonObj.Success != undefined && jsonObj.Success == true) {
                        if (jsonObj.Result != undefined) {
                            if (self.CurrentLegData.LegNUM == undefined) {
                                self.CurrentLegData = ko.mapping.fromJS(jsonObj.Result.CurrentLegData);
                            }
                            else
                                ko.mapping.fromJS(jsonObj.Result.CurrentLegData, self.CurrentLegData);
                        }
                        if (self.SelectedCrewData!=undefined)
                              self.SelectedCrewData='';
                        
                        GetCurrentLegCrews();
                        RefreshCrewSection();
                        FormatLegDates();
                        RefreshLegDates();
                        $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                        $("#Leg" + CurrentLegData.LegNUM()).removeClass("tabStripItem").addClass("tabStripItemSelected");
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }
    });
    self.POEditMode.subscribe(function (value) {
        if (value) {
            $(jqCrewTableId).trigger('reloadGrid');
            $("#btnRonAdj").removeAttr('class', 'ui_nav_disable');
            $("#btnRonAdj").attr('class', 'ui_nav');
        }
    });
    self.LegTab_Click = function (Leg) {
        lastSelectedRow = null;
        self.CurrentLegNum(Leg.LegNUM());
        SelectTab(Leg);
    };
    self.AdjustRON_Click = function () {
        var selRowId = $(jqCrewTableId).jqGrid('getGridParam', 'selrow');
        if (selRowId == null)
            selRowId = $('#jqCrewGrid tr:nth-child(2)').attr('id'); /// if its null pick first row for select
        if (selRowId != null) {
            var isChecked = $('#chkIsRemainOverNightOverride' + selRowId).is(':checked');
            if (isChecked) {
                jConfirm("Reset RON Override Back to Default values?", msgTitle, function (r) {
                    if (r) {
                        $('#tbRON' + selRowId).attr('style', 'background-color: #F1F1F1!important');
                        var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
                        RequestStart();
                        $.ajax({
                            async: false,
                            url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/RONCalculation",
                            type: "POST",
                            dataType: "json",
                            data: JSON.stringify({ 'CrewID': selRowId, 'currentLegNum': currentLegNum }),
                            contentType: "application/json; charset=utf-8",
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                                if (returnResult == true && response.d.Success == true) {
                                    $('#tbRON' + selRowId).val(response.d.Result);
                                    $('#tbRON' + selRowId).attr('title', response.d.Result);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                reportPostflightError(jqXHR, textStatus, errorThrown);
                            },
                            complete: function () { ResponseEnd(); }
                        });
                        $('#tbRON' + selRowId).attr('readonly', 'readonly');
                        $('#chkIsRemainOverNightOverride' + selRowId).removeAttr('checked');
                    }
                    else {
                        $('#tbRON' + selRowId).attr('style', 'background-color:  #fe5b5c!important', 'border-color: #ea0303!important');
                        $('#tbRON' + selRowId).attr('readonly', 'readonly');
                        $('#chkIsRemainOverNightOverride' + selRowId).removeAttr('checked');
                    }
                });
            }
            else {
                $('#tbRON' + selRowId).attr('style', 'background-color: #fe5b5c!important', 'border-color:#ea0303!important');
                $('#tbRON' + selRowId).removeAttr('readonly');
                $('#chkIsRemainOverNightOverride' + selRowId).attr('checked', 'checked');
            }
        }
    };
    self.DutyAdjReset_Click = function () {
        RequestStart();
        var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
        var selCrewId = $(jqCrewTableId).jqGrid('getGridParam', 'selrow');
        if (selCrewId == null)
            selCrewId = $('#jqCrewGrid tr:nth-child(2)').attr('id');
        if (selCrewId != null) {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/DutyAdjReset",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'CrewID': selCrewId, 'currentLegNum': currentLegNum }),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var returnResult = verifyReturnedResult(response);
                    if (returnResult == true && response.d.Success == true) {
                        GetCurrentLegCrews();
                        GetSelectedCrewData(selCrewId);
                        SetDutyHrsTextboxColor(selCrewId,false);
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }
    };
    self.AugmentCrewReset_Click = function () {
        RequestStart();
        var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
        var selCrewId = $(jqCrewTableId).jqGrid('getGridParam', 'selrow');
        if (selCrewId == null)
            selCrewId = $('#jqCrewGrid tr:nth-child(2)').attr('id');
        if (selCrewId != null) {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/AugmentCrewReset",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'CrewID': selCrewId, 'currentLegNum': currentLegNum }),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var returnResult = verifyReturnedResult(response);
                    if (returnResult == true && response.d.Success == true) {
                        GetCurrentLegCrews();
                        GetSelectedCrewData(selCrewId);
                        SetBlockFlightHrsTextboxColor(selCrewId, $('#hdnBlockHours').val(), $("#tbCrewBlockHours").val());
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }
    };
    InitializeKOWatchMainNotes();
    InitializePostflightCrew();
    self.POEditMode(self.POEditMode());
};

function FlighthrsBlockhrs_ChangeEvent() {
    $("#tbCrewBlockHours").change(function () {
        var crewData = ko.mapping.toJS(self.SelectedCrewData);
        if (!IsNullOrEmptyOrUndefined(crewData)) {
            var enteredval = $(this).val();
            var controlId = "tbBlkHrs" + crewData.CrewID;
            var result = BlockFlightHrsValidation(controlId, enteredval, $('#hdnBlockHours').val(), crewData.CrewID);
            if (result) {
                self.SelectedCrewData.BlockHoursTime($(this).val());
                SaveEditedCrewInfoToSession();
                SetBlockFlightHrsTextboxColor(crewData.CrewID, 'BlockHours');
            }
            else {
                self.SelectedCrewData.BlockHoursTime($('#hdnBlockHours').val());
                $('#' + controlId).val($('#hdnBlockHours').val());
                $("#tbCrewBlockHours").val($('#hdnBlockHours').val());
                SaveEditedCrewInfoToSession();
                $("#tbCrewBlockHours").focus();
            }
        }
    });
    $("#tbCrewFlightHours").change(function () {
        var crewData = ko.mapping.toJS(self.SelectedCrewData);
        if (!IsNullOrEmptyOrUndefined(crewData)) {
            var enteredval = $(this).val();
            var controlId = "tbFlightHrs" + crewData.CrewID;
            var result = BlockFlightHrsValidation(controlId, enteredval, $('#hdnFlightHours').val(), crewData.CrewID);
            if (result) {
                self.SelectedCrewData.FlightHoursTime($(this).val());
                SaveEditedCrewInfoToSession();
                SetBlockFlightHrsTextboxColor(crewData.CrewID, 'FlightHours');
            }
            else {
                self.SelectedCrewData.FlightHoursTime($('#hdnFlightHours').val());
                $('#' + controlId).val($('#hdnFlightHours').val());
                $("#tbCrewFlightHours").val($('#hdnFlightHours').val());
                SaveEditedCrewInfoToSession();
                $("#tbCrewFlightHours").focus();
            }
        }    
    });

    $(".textDatePicker").change(function () {
        SaveEditedCrewInfoToSession();
        var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
        var crewID = ko.mapping.toJS(self.SelectedCrewData.CrewID);
        CheckCrewDutyOverride(crewID, currentLegNum);
    });
}

function GetSelectedCrewData(crewid) {
    var CrewID = Number(crewid);
    var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
    if (CrewID > 0) {
        $.ajax({
            async: false,
            url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/GetSelectedCrewData",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'CrewID': CrewID, 'currentLegNum': currentLegNum }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    if (response.d.Result != undefined) {
                        if (self.SelectedCrewData.CrewID == undefined)
                            self.SelectedCrewData = ko.mapping.fromJS(response.d.Result);
                        else
                            ko.mapping.fromJS(response.d.Result, self.SelectedCrewData);
                        RefreshCrewSection();
                        var crew = ko.mapping.toJS(self.SelectedCrewData);
                        SetCrewAugmentMsgColor("",crew.CrewID);
                    }                  
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    
}

function FormatLegDates() {
    self.CurrentLegData.LastUpdTS = ko.observable(self.CurrentLegData.LastUpdTS).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    if (!IsNullOrEmptyOrUndefined(self.CurrentLegData.ScheduledDate()))
        self.CurrentLegData.ScheduledDate = ko.observable(self.CurrentLegData.ScheduledDate()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    if (!IsNullOrEmptyOrUndefined(self.CurrentLegData.InDate()))
        self.CurrentLegData.InDate = ko.observable(self.CurrentLegData.InDate()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
}

function GetCurrentLegCrews() {
    crewsList = new Array();
    var currentLegData = ko.mapping.toJS(self.CurrentLegData);
    $.ajax({
        async: false,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/GetCurrentLegCrews",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'legNum': currentLegData.LegNUM }),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (response.d.Result.currentSelectedCrew != null) {
                    if (self.SelectedCrewData.CrewID == undefined) {
                        self.SelectedCrewData = ko.mapping.fromJS(response.d.Result.currentSelectedCrew);
                    }
                    else
                        ko.mapping.fromJS(response.d.Result.currentSelectedCrew, self.SelectedCrewData);
                }
                crewsList = response.d.Result.results;
                if (!IsNullOrEmptyOrUndefined(crewsList) && crewsList.length > 0 && self.POEditMode()) {
                    $("#btnRonAdj").removeAttr('class', 'ui_nav_disable');
                    $("#btnRonAdj").attr('class', 'ui_nav');
                    $('#btnAugmentCrewReset').attr('class', 'reset-icon').removeAttr("disabled");
                    $('#btnDutyAdjReset').attr('class', 'reset-icon').removeAttr("disabled");
                }
                else {
                    $("#btnRonAdj").attr('class', 'ui_nav_disable');
                    $("#btnRonAdj").removeAttr('class', 'ui_nav');
                    $('#btnAugmentCrewReset').attr({ 'class': 'reset-icon-disable', 'disabled': 'disabled' });
                    $('#btnDutyAdjReset').attr({ 'class': 'reset-icon-disable', 'disabled': 'disabled' });
                }
                BindCrewSelectionGrid();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    })
};

function BindCrewSelectionGrid() {
    jQuery(jqCrewTableId).jqGrid('GridUnload');
    jQuery(jqCrewTableId).jqGrid({
        datatype: "local",
        data: crewsList,
        mtype: 'POST',
        Height: 'auto',
        width: "710",
        multiselect: true,
        multiselectWidth: 17,
        shrinkToFit: false,
        pager: "#pg_gridPagerCrew",
        pgbuttons: false,
        viewrecords: false,
        pgtext: "",
        pginput: false,
        ignoreCase:true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        colNames: colNames,
        colModel: [
            { name: 'PostflightCrewListID', index: 'PostflightCrewListID', hidden: true, frozen: true },
            { name: 'RowNumber', index: 'RowNumber', hidden: true, frozen: true },
            { name: 'CrewID', index: 'CrewID', key: true, hidden: true, frozen: true },
            { name: 'CrewCode', index: 'CrewFirstName', width: 200, frozen: true,sortable: true, formatter: CrewNameFormatter },
            { name: 'OrderNUM', index: 'OrderNUM', width: 0, hidden: true },
            { name: 'CrewFirstName', index: 'CrewFirstName', width: 0, hidden: true },
            { name: 'CrewLastName', index: 'CrewLastName', width: 0,  hidden: true },
            { name: 'POS', index: 'POS', width: 80, frozen: false, sortable: false, search: true, formatter: CrewPositionFormatter },
            {
                name: 'TakeOffDay', index: 'TakeOffDay', width: 30, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (!IsNullOrEmptyOrUndefined(cellvalue) ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=TakeOffDay' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="2" class="text20" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)" col="TakeOffDay"/>';
                }
            },
            {
                name: 'TakeOffNight', index: 'TakeOffNight', width: 30, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (cellvalue != null ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=TakeOffNight' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="3" class="text20" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)" col="TakeOffNight"/>';
                }
            },
            {
                name: 'LandingDay', index: 'LandingDay', width: 30, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (cellvalue != null ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=LandingDay' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="4" class="text20" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)" col="LandingDay"/>';
                }
            },
            {
                name: 'LandingNight', index: 'LandingNight', width: 30, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (cellvalue != null ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=LandingNight' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="5" class="text20" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)" col="LandingDayNight"/>';
                }
            },
            {
                name: 'ApproachPrecision', index: 'ApproachPrecision', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (cellvalue != null ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=ApproachPrecision' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="6" class="text40" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)" col="ApproachPrecision" />';
                }
            },
            {
                name: 'ApproachNonPrecision', index: 'ApproachNonPrecision', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (cellvalue != null ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=ApproachNonPrecision' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="7" class="text40" MaxLength="2" onKeyPress="return fnAllowNumeric(this, event)" col="ApproachNonPrecision" />';
                }
            },
            {
                name: 'InstrumentStr', index: 'InstrumentStr', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbInstr' + rowObject["CrewID"] + '" tabindex="8" onfocus="this.select();" class="text40 Minutetime" col="Instrument" maxlength="6" onchange="tbInsNight_TextChanged(this);"/>';
                }
            },
            {
                name: 'NightStr', index: 'NightStr', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbNight' + rowObject["CrewID"] + '" tabindex="9" onfocus="this.select();" class="text40 Minutetime" col="Night"  maxlength="6" onchange="tbInsNight_TextChanged(this);" />';
                }

            },
            {
                name: 'DaysAway', index: 'DaysAway', width: 30, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = (cellvalue != null ? cellvalue : 0)
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '"id=DaysAway' + rowObject["CrewID"] + ' leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" tabindex="10" class="text40" MaxLength="2" onfocus="this.select();" onKeyPress="return fnAllowNumeric(this, event)" col="DaysAway" />';
                },
                cellattr: function () { return 'tabindex="10"' }
            },
             {
                 name: 'IsRemainOverNightOverride', index: 'IsRemainOverNightOverride', width: 30, frozen: false, sortable: false, hidden: true, search: true, formatter: function (cellvalue, options, rowObject) {
                     if (cellvalue == true)
                         return '<input type="checkbox" tabindex="11"  id="chkIsRemainOverNightOverride' + rowObject["CrewID"] + '" style="display: none" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" checked="checked"/>'
                     else
                         return '<input type="checkbox" tabindex="11" id="chkIsRemainOverNightOverride' + rowObject["CrewID"] + '" style="display: none" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '"/>'
                 },
                 cellattr: function(){return 'tabindex="11"'}
             },
            {
                name: 'RemainOverNight', index: 'RemainOverNight', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbRON' + rowObject["CrewID"] + '" tabindex="12" readonly="readonly"  class="text40" MaxLength="1" col="RemainOverNight" onKeyPress="return fnAllowNumeric(this, event)" />';
                }
            },
            {
                name: 'CrewDutyStartTime', index: 'CrewDutyStartTime', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbDutyBegin' + rowObject["CrewID"] + '" tabindex="13" class="datetimechange" MaxLength="5"  col="CrewDutyStartTM" onchange="Duty_TextChanged(this);" />';
                }
            },
            {
                name: 'CrewDutyEndTime', index: 'CrewDutyEndTime', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbDutyEnd' + rowObject["CrewID"] + '" tabindex="14" class="datetimechange" MaxLength="5" col="CrewDutyEndTM" onchange="Duty_TextChanged(this);" />';
                }
            },
            {
                name: 'IsOverride', index: 'IsOverride', width: 30, frozen: false, sortable: false, hidden: true, search: true, formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == true)
                        return '<input type="checkbox" tabindex="15" id="chkIsDutyOverride' + rowObject["CrewID"] + '" style="display: none" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" checked="checked"/>'
                    else
                        return '<input type="checkbox" tabindex="15" id="chkIsDutyOverride' + rowObject["CrewID"] + '" style="display: none" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '"/>'
                }
            },
            {
                name: 'DutyHrsTime', index: 'DutyHrsTime', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBox + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" readonly="readonly" id="tbDuty' + rowObject["CrewID"] + '" tabindex="16" BackColor="#F1F1F1F1" MaxLength="6" class="text40" col="DutyHours" />';
                }
            },
            {
                name: 'BlockHoursTime', index: 'BlockHoursTime', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" MaxLength="5" id="tbBlkHrs' + rowObject["CrewID"] + '" tabindex="17" class="text40 Minutetime" onfocus="this.select();" col="BlockHours"  onchange="Hours_TextChanged(this);" />';
                }
            },
            {
                name: 'FlightHoursTime', index: 'FlightHoursTime', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" MaxLength="5" id="tbFlightHrs' + rowObject["CrewID"] + '" tabindex="18" class="text40 Minutetime" onfocus="this.select();" col="FlightHours"   onchange="Hours_TextChanged(this);" />';
                }
            },
            { name: 'Seat', index: 'Seat', width: 60, frozen: false, sortable: false, search: true, formatter: CrewSeatFormatter },
            { name: 'IsAugmentCrew', index: 'IsAugmentCrew', width: 30, frozen: true, sortable: false, search: true, hidden: true },
            {
                name: 'IsAugmentCrew', index: 'IsAugmentCrew', width: 30, frozen: false, sortable: false, hidden: true, search: true, formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == true)
                        return '<input type="checkbox" tabindex="20" id="chkIsAugmentCrew' + rowObject["CrewID"] + '" style="display: none" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" checked="checked"/>'
                    else
                        return '<input type="checkbox" tabindex="20" id="chkIsAugmentCrew' + rowObject["CrewID"] + '" style="display: none" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '"/>'
                }
            },
            { name: 'BeginningDuty', index: 'BeginningDuty', width: 50, frozen: true, sortable: false, search: true, hidden: true },
            { name: 'DutyEnd', index: 'DutyEnd', width: 50, frozen: true, sortable: false, search: true, hidden: true },
             {
                 name: 'Specification1', index: 'Specification1', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                     if (cellvalue == true)
                         return '<input type="checkbox" tabindex="21" id="chkCustom1' + rowObject["CrewID"] + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" checked="checked" onclick="UpdateCrew(this);"  col="Specification1"/>'
                     else
                         return '<input type="checkbox" tabindex="21" id="chkCustom1' + rowObject["CrewID"] + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" onclick="UpdateCrew(this);" col="Specification1"/>'
                 }
             },
            {
                name: 'Sepcification2', index: 'Sepcification2', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == true)
                        return '<input type="checkbox" tabindex="22" id="chkCustom2' + rowObject["CrewID"] + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" checked="checked" onclick="UpdateCrew(this);" col="Specification2"/>'
                    else
                        return '<input type="checkbox" tabindex="22" id="chkCustom2' + rowObject["CrewID"] + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" onclick="UpdateCrew(this);" col="Specification2"/>'
                }
            },
            {
                name: 'Specification3', index: 'Specification3', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = SpecificationFormatter(cellvalue, "Spec3");
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbSpec3' + rowObject["CrewID"] + '" tabindex="23" class="text40" MaxLength="7" onKeyPress="return fnAllowNumericAndDot(this, event);" col="Specification3" onchange="Specification_TextChanged(this);" />';
                }
            },
            {
                name: 'Specification4', index: 'Specification4', width: 40, frozen: false, sortable: false, search: true, formatter: function (cellvalue, options, rowObject) {
                    cellvalue = SpecificationFormatter(cellvalue, "Spec4");
                    return inputBoxWOEvent + cellvalue + '" row-id="' + rowObject["CrewID"] + '" leg-no="' + rowObject["POLegID"] + '" title= "' + cellvalue + '" id="tbSpec4' + rowObject["CrewID"] + '" tabindex="24" class="text40" MaxLength="7" onKeyPress="return fnAllowNumericAndDot(this, event);" col="Specification4" onchange="Specification_TextChanged(this);" />';
                }
            },

        ],
        //onCellSelect: function (rowid, icol, cellcontent, e) {
        // if (icol > 0) { $(jqCrewTableId).jqGrid('setSelection', rowid, false); }
        //},
        onSelectRow: function (rowid, status) {
            if (self.POEditMode()) {
                if (status)
                    EnableGridRow(rowid);
                else
                    DisableCrewInfoSection();
            }

            lastSelectedRow = rowid;
            GetSelectedCrewData(rowid);

        },
        onSelectAll: function (ids, status) {
            if (status) {
                DisableCrewInfoSection();
            }
        },
        loadComplete: function (rowData) {
            var crews = $(jqCrewTableId).jqGrid('getGridParam', 'data');
            SetCrewAugmentMsgColor(crews);
            EnableDisableControlsofGrid(self.POEditMode());
            if (crews.length > 0 && globalIdentity._fpSettings._TimeDisplayTenMin == Minute)
                $('.Minutetime').mask("99:99");
            if (crews.length > 0 && self.POEditMode()) {
                $('#btnAugmentCrewReset').attr('class', 'reset-icon').removeAttr("disabled");
                $('#btnDutyAdjReset').attr('class', 'reset-icon').removeAttr("disabled");           
            }
            else
            {
                $('#btnAugmentCrewReset').attr({ 'class': 'reset-icon-disable', 'disabled': 'disabled' });
                $('#btnDutyAdjReset').attr({ 'class': 'reset-icon-disable', 'disabled': 'disabled' });
            }
            $("th.ui-th-column").css({ "font-weight": "bold" });
            HideJqGridLoader(jqCrewTableId);
            selRowId = $('#jqCrewGrid tr:nth-child(2)').attr('id');
            if (self.POEditMode()) {
                if (lastSelectedRow != null) {
                    $(jqCrewTableId).jqGrid("setSelection", lastSelectedRow, false);
                    EnableControls(lastSelectedRow);
                    EnableGridRow(lastSelectedRow);
                    GetSelectedCrewData(lastSelectedRow);
                }
                else {
                    DisableControls(selRowId);
                    if (selRowId != null)
                        EnableGridRow(selRowId);
                    SetCrewAugmentMsgColor("",selRowId);
                }
            }
            else
                SetCrewAugmentMsgColor("", selRowId);
        },
        beforeRequest: function () {
            ShowJqGridLoader(jqCrewTableId);
        },
        gridComplete: function () {
            MessageWhileBlankJqGrid(jqCrewTableId);

            if (globalIdentity._fpSettings._CrewLogCustomLBLShort1 == null)
                $(jqCrewTableId).hideCol("Specification1");
            if (globalIdentity._fpSettings._CrewLogCustomLBLShort2 == null)
                $(jqCrewTableId).hideCol("Sepcification2");
            if (globalIdentity._fpSettings._SpecificationShort3 == null)
                $(jqCrewTableId).hideCol("Specification3");
            if (globalIdentity._fpSettings._SpecificationShort4 == null)
                $(jqCrewTableId).hideCol("Specification4");
        }
    });

    $(jqCrewTableId).jqGrid('navGrid', '#pg_gridPagerCrew', { resize: false, add: false, search: false, del: false, refresh: false, edit: false });
    $(jqCrewTableId).jqGrid('navButtonAdd', "#pg_gridPagerCrew", {
        caption: "", title: "Delete Selected Crew(s)", buttonicon: "ui-icon ui-icon-trash",
        onClickButton: function () {
            var isDeleteAllCrew = false;
            var selRowIds = $(jqCrewTableId).jqGrid('getGridParam', 'selarrrow');
            if (selRowIds.length > 0) {
                setAlertTextToYesNo();
                jConfirm("Are you sure you want to delete this record?", "Confirmation", function (r) {
                    if (r) {
                        var temp = new Array();
                        jQuery.each(selRowIds, function (i, val) {
                            var gridData = $(jqCrewTableId).jqGrid('getRowData', val);
                            var dtNewAssign = new Object();
                            dtNewAssign.CrewID = gridData.CrewID;
                            dtNewAssign.PostflightCrewListID = gridData.PostflightCrewListID;
                            dtNewAssign.RowNumber = gridData.RowNumber;
                            temp.push(dtNewAssign);
                        });

                        DeleteCrewInfoFromSession(temp, isDeleteAllCrew);
                        $(jqCrewTableId).jqGrid('resetSelection');
                    }
                    else
                        $(jqCrewTableId).jqGrid('resetSelection');
                });
                setAlertTextToOkCancel();
            } else {
                jAlert("Please select the record to delete!", 'Postflight - Crew');
            }
            return;
        }
    });
    $(jqCrewTableId).jqGrid('navButtonAdd', "#pg_gridPagerCrew", {
        caption: "", title: "Edit Selected Crew", buttonicon: "ui-icon ui-icon-edit",
        onClickButton: function () {
            if (self.POEditMode()) {
                setAlertTextToOkCancel();
                var selRowIds = $(jqCrewTableId).jqGrid('getGridParam', 'selarrrow');
                if (selRowIds.length > 1) {
                    jAlert("Please select only one crew to edit.", 'Postflight - Crew', function (r) {
                            $(jqCrewTableId).jqGrid('resetSelection');
                            DisableCrewInfoSection();
                    });
                }
                else {
                    var selRowId = $(jqCrewTableId).jqGrid('getGridParam', 'selrow');                   
                    if (selRowId != null && selRowId.length > 0)
                        EnableControls(selRowId);
                    else {
                        jAlert("Please select crew to edit.", 'Postflight - Crew');
                    }
                }
            }
        },
        position: "last"
    });
    $(jqCrewTableId).jqGrid('setColProp', 'cb', { frozen: true });
    $(jqCrewTableId).jqGrid('setColProp', 'CrewCode', { frozen: true });
    $(jqCrewTableId).jqGrid('setFrozenColumns');
};

function SpecificationFormatter(cellvalue, colName) {
    var value = "0";
    var decimalPlace = "";
    if (cellvalue != null && cellvalue == 0) {
        if (colName == "Spec3")
            decimalPlace = globalIdentity._fpSettings._Specdec3;
        if (colName == "Spec4")
            decimalPlace = globalIdentity._fpSettings._Specdec4;
        value = fndecimalPadding(decimalPlace, value)
    }
    else
        value = cellvalue;
    return value;
}

function fndecimalPadding(decimalPlace, value) {
    var decimalPadding = "";
    for (var i = 0; i < decimalPlace; i++) {
        decimalPadding += "0";
    }
    if (decimalPadding.length > 0)
        value = value + "." + decimalPadding;
    return value;
}

function AddCrewToSession(crewsArray, isCopyToAllLegs, isAddedBySearch) {
    ShowJqGridLoader(jqCrewTableId);
    if (isAddedBySearch) {
        var Crews = new Array();
        for (var i = 0; i < crewsArray.length; i++) {
            removeUnwantedPropertiesKOViewModel(crewsArray[i]);
            var dtNewAssign = new Object();
            dtNewAssign.CrewID = crewsArray[i].CrewID;
            dtNewAssign.CrewCode = crewsArray[i].CrewCD;
            dtNewAssign.CrewFirstName = crewsArray[i].FirstName;
            dtNewAssign.CrewMiddleName = crewsArray[i].MiddleInitial;
            dtNewAssign.CrewLastName = crewsArray[i].LastName;
            Crews.push(dtNewAssign);
        }
        crewsArray = Crews;
    }
    var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/AddCrewToSession',
        async: true,
        data: JSON.stringify({ 'CrewsList': crewsArray, 'CopyToAllLegs': isCopyToAllLegs, 'currentLegNum': currentLegNum }),
        contentType: 'application/json',
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                GetCurrentLegCrews();
                RefreshCrewSection();
            }
            HideJqGridLoader(jqCrewTableId);
        },
        error: function (xhr, status, error) {
            HideJqGridLoader(jqCrewTableId);
            reportPostflightError(xhr, status, error);
        }

    });
}

function RefreshLegDates() {
    if (!IsNullOrEmptyOrUndefined(self.CurrentLegData)) {
        var legData = ko.mapping.toJS(self.CurrentLegData);
        $('#lblDepartDate').text(!IsNullOrEmptyOrUndefined(legData.ScheduledDate) ? legData.ScheduledDate : '');
        $('#lblArriveDate').text(!IsNullOrEmptyOrUndefined(legData.InDate) ? legData.InDate : '');
        $('#tbLegDate').val(!IsNullOrEmptyOrUndefined(legData.ScheduledDate) ? legData.ScheduledDate : '');
        $('#tbLegIndate').val(!IsNullOrEmptyOrUndefined(legData.InDate) ? legData.InDate : '');
    }
}

function RefreshCrewSection()
{
    if (self.SelectedCrewData != undefined && self.SelectedCrewData.CrewDutyStartDate != undefined) {
        if (!IsNullOrEmptyOrUndefined(self.SelectedCrewData.CrewDutyStartDate()))
            self.SelectedCrewData.CrewDutyStartDate = ko.observable(self.SelectedCrewData.CrewDutyStartDate()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });

        if (!IsNullOrEmptyOrUndefined(self.SelectedCrewData.CrewDutyEndDate()))
            self.SelectedCrewData.CrewDutyEndDate = ko.observable(self.SelectedCrewData.CrewDutyEndDate()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });

        var crewData = ko.mapping.toJS(self.SelectedCrewData);
            $('#tbCrewName').val(CrewName());
            $('#tbDateBegin').val(!IsNullOrEmptyOrUndefined(crewData.CrewDutyStartDate) ? crewData.CrewDutyStartDate : '');
            $('#tbDateEndDate').val(!IsNullOrEmptyOrUndefined(crewData.CrewDutyEndDate) ? crewData.CrewDutyEndDate : '');
            $('#tbDutyBegintime').val(!IsNullOrEmptyOrUndefined(crewData.CrewDutyStartTime) ? crewData.CrewDutyStartTime : '');
            $('#tbDutyEndtime').val(!IsNullOrEmptyOrUndefined(crewData.CrewDutyEndTime) ? crewData.CrewDutyEndTime : '');
            $('#tbCrewBlockHours').val(!IsNullOrEmptyOrUndefined(crewData.BlockHoursTime) ? crewData.BlockHoursTime : '');
            $('#tbCrewFlightHours').val(!IsNullOrEmptyOrUndefined(crewData.FlightHoursTime) ? crewData.FlightHoursTime : '');
           
        }
    else {
        $('#tbCrewName').val('');
        $('#tbDateBegin').val('');
        $('#tbDateEndDate').val('');
        $('#tbDutyBegintime').val('');
        $('#tbDutyEndtime').val('');
        $('#tbCrewBlockHours').val('');
        $('#tbCrewFlightHours').val('');

    }
}

function DeleteCrewInfoFromSession(DeletedCrewsArray, isDeleteAllCrew) {
    ShowJqGridLoader(jqCrewTableId);
    var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/DeleteCrewInfoFromSession',
        async: true,
        data: JSON.stringify({ 'CrewsList': DeletedCrewsArray, 'isDeleteAllCrew': isDeleteAllCrew, 'currentLegNum': currentLegNum }),
        contentType: 'application/json',
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                GetCurrentLegCrews();
                RefreshCrewSection();
                EnableDisableControlsofGrid(self.POEditMode());
                $('#dutyBegin').removeAttr('class', 'pocs_textbox');
                $('#dutyEnd').removeAttr('class', 'pocs_textbox');
                DisableCrewInfoSection();
            }
            HideJqGridLoader(jqCrewTableId);
        },
        error: function (xhr, status, error) {
            HideJqGridLoader(jqCrewTableId);
            reportPostflightError(xhr, status, error);
        }
    });
}

function Specification_TextChanged(control) {
    var col = control.getAttribute("col");
    var controlId = control.getAttribute("id");
    var enteredval = control.value;
    var val = 3;
    var decimalPlace = "";
    if (col == "Specification3")
        decimalPlace = globalIdentity._fpSettings._Specdec3;
    else
        decimalPlace = globalIdentity._fpSettings._Specdec4;

    var msg = fnNumericValidation(enteredval, val, decimalPlace);
    if (msg != "") {
        jAlert(msg, msgTitle, function (r) {
            if (r) {
                $('#' + controlId).focus();
                $('#' + controlId).val(fndecimalPadding(decimalPlace, "0"));
            }
        });
    }
    else
        UpdateCrew(control);
}

function Hours_TextChanged(control) {
    var enteredval = control.value.trim();
    var controlId = control.getAttribute("id");
    var col = control.getAttribute("col");
    var crewID = ko.mapping.toJS(self.SelectedCrewData.CrewID);
    var legHhr;
    var crewHrsValue;
    if (col == "BlockHours")
    {
        legHhr = $('#hdnBlockHours').val();
        crewHrsValue=$('#tbCrewBlockHours').val();
    }
    if (col == "FlightHours")
    {
        legHhr = $('#hdnFlightHours').val();
        crewHrsValue=$('#tbCrewFlightHours').val();
    }
    var result = BlockFlightHrsValidation(controlId, enteredval, legHhr, crewID);
    if (result) {
        SetBlockFlightHrsTextboxColor(crewID, col);
        UpdateCrew(control);
    }
    else {
        if (col == "BlockHours") {
            $('#' + controlId).val($('#hdnBlockHours').val());
            $('#tbCrewBlockHours').val($('#hdnBlockHours').val());
            UpdateCrew(control);
        }
        if (col == "FlightHours") {
            $('#' + controlId).val($('#hdnFlightHours').val());
            $('#tbCrewFlightHours').val($('#hdnFlightHours').val());
            UpdateCrew(control);
        }
    }
}

function Duty_TextChanged(control) {
    var enteredval = control.value.trim();
    var controlId = control.getAttribute("id");
    var crewID = ko.mapping.toJS(self.SelectedCrewData.CrewID);

    var errMsg = ValidateTime(enteredval);
    if (errMsg != "") {
        jAlert(errMsg, msgTitle, function (r) {
            $('#' + controlId).val("00:00");
            $('#' + controlId).focus();
        });
    }
    else {
        var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
        UpdateCrew(control);
        CheckCrewDutyOverride(crewID, currentLegNum);
    }
}

function tbInsNight_TextChanged(control) {
    var enteredVal = control.value;
    var controlId = control.getAttribute("id");
    var result = ValidateEnteredData(controlId, enteredVal);
    if (result)
        UpdateCrew(control);
}

function SaveEditedCrewInfoToSession() {
    ShowJqGridLoader(jqCrewTableId);
    var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
    var crewData = ko.mapping.toJS(self.SelectedCrewData);

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/SaveEditedCrewInfoToSession',
        async: false,
        data: JSON.stringify({ 'CrewObj': crewData, 'currentLegNum': currentLegNum }),
        contentType: 'application/json',
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                var crewID = crewData.CrewID;
                $('#tbDutyBegin' + crewID).val(crewData.CrewDutyStartTime);
                $('#tbDutyEnd' + crewID).val(crewData.CrewDutyEndTime);
                $('#tbBlkHrs' + crewID).val(crewData.BlockHoursTime);
                $('#tbFlightHrs' + crewID).val(crewData.FlightHoursTime);
                $('#tbDuty' + crewID).val(crewData.DutyHrsTime);
            }
            HideJqGridLoader(jqCrewTableId);
        },
        error: function (xhr, status, error) {
            HideJqGridLoader(jqCrewTableId);
            reportPostflightError(xhr, status, error);
        }
    });
}
function AddCrews(Crews, isAddedBySearch) {
    var isCopyToAll = false;
    var legsCount = ko.mapping.toJS(self.Legs);
    if (!IsNullOrEmptyOrUndefined(legsCount)) {
        if(legsCount.length > 1)
        {
            setAlertTextToYesNo();
            jConfirm('Would you like to assign this Crew to all Legs?', msgTitle, function (r) {
                if (r) {
                    isCopyToAll = true;
                    CheckIfCrewExists(Crews, isCopyToAll, isAddedBySearch);
                }
                else {
                    CheckIfCrewExists(Crews, isCopyToAll, isAddedBySearch);
                }
            });
        }
        else
            CheckIfCrewExists(Crews, isCopyToAll, isAddedBySearch);

    }

}
function UpdateCrew(control) {
    var CrewID = control.getAttribute("row-id");
    var col = control.getAttribute("col");
    var id = control.getAttribute("id");
    var val = control.value;
    var decimalPlace = "";
    if (val == '') {
        if (col == 'TakeOffDay' || col == 'TakeOffNight' || col == 'LandingDay' || col == 'LandingDayNight' || col == 'ApproachPrecision' || col == 'ApproachNonPrecision' || col == 'DaysAway') {
            $('#' + id).val("0");
            $('#' + id).focus();
            return;
        }
        else if (col == "Specification3" || col == "Specification4") {
            if (col == "Specification3")
                decimalPlace = globalIdentity._fpSettings._Specdec3;
            else
                decimalPlace = globalIdentity._fpSettings._Specdec4;
            $('#' + id).val(fndecimalPadding(decimalPlace, "0"));
            $('#' + id).focus();
            return;
        }
    }
    else if (col == 'Specification1' || col == 'Specification2')
        var val = $('#' + id).is(':checked');

    ShowJqGridLoader(jqCrewTableId);
    var params = '{ColName: "' + col + '",Value: "' + val + '",CrewID: "' + CrewID + '",LegNUM: ' + legNum + '}';
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/SaveCrewCells',
        async: false,
        data: params,
        contentType: 'application/json',
        success: function (rowData, textStatus, xhr) {
            if (rowData.d.Success == true) {
                GetSelectedCrewData(CrewID);
                var Html = $(control);
                if (!IsNullOrEmptyOrUndefined(Html)) {
                    if (control.type == 'select-one')
                        Html.attr('title', Html.find("option:selected").text());
                    else if (control.type == 'text')
                        Html.attr('title', Html.val());
                }
            }
            HideJqGridLoader(jqCrewTableId);
        },
        error: function (xhr, status, error) {
            HideJqGridLoader(jqCrewTableId);
            reportPostflightError(xhr, status, error);
        }
    });
}
function DisableCrewInfoSection()
{
    $('#tbDateBegin').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
    $('#tbDateEndDate').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
    $('#tbDutyBegintime').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
    $('#tbDutyEndtime').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
    $('#tbCrewBlockHours').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
    $('#tbCrewFlightHours').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
}

function CheckCrewDutyOverride(crewID, currentLegNum)
{
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/CheckDutyHourOverride',
        async: false,
        data: JSON.stringify({ 'crewID': crewID, 'currentLegNum': currentLegNum }),
        contentType: 'application/json',
        success: function (response) {
            var jsonObj = response.d;
            if (jsonObj != null) {
                if (self.SelectedCrewData.CrewID == undefined) {
                    self.SelectedCrewData = ko.mapping.fromJS(jsonObj.Result.CrewObj);
                }
                else
                    ko.mapping.fromJS(jsonObj.Result.CrewObj, self.SelectedCrewData);
                var crewData = ko.mapping.toJS(self.SelectedCrewData);
                if (crewData.IsOverride)
                    $('#chkIsDutyOverride' + crewID).prop('checked', true);
                else
                    $('#chkIsDutyOverride' + crewID).prop('checked', false);
                SetDutyHrsTextboxColor(crewID, crewData.IsOverride);
                $('#tbDutyBegintime').val(crewData.CrewDutyStartTime);
                $('#tbDateBegin ').val(crewData.CrewDutyStartDate);
                $('#tbDutyEndtime ').val(crewData.CrewDutyEndTime);
                $('#tbDateEndDate ').val(crewData.CrewDutyEndDate);
                $('#tbDuty' + crewID).val(crewData.DutyHrsTime);
                $('#tbDuty' + crewID).attr('title', crewData.DutyHrsTime);
                $('#tbDutyBegin ' + crewID).val(crewData.CrewDutyStartTime);
                $('#tbDutyBegin ' + crewID).attr('title', crewData.CrewDutyStartTime);
                $('#tbDutyEnd ' + crewID).val(crewData.CrewDutyEndTime);
                $('#tbDutyEnd ' + crewID).attr('title', crewData.CrewDutyEndTime);
            }
        },
        error: function (xhr, status, error) {
            reportPostflightError(xhr, status, error);
        }
    });
}