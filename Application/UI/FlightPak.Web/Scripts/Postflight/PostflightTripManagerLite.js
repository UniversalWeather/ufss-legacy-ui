﻿var DetailedLegSummary = "#DetailedLegSummary";
var ShortLegSummary = "#SortSummaryDetail";
var self = this;
var PostflightConfirmTitle = "Flight Log Manager - Postflight";
var PostflightMainPropertyNamesList = "";
var PostflightViewModel = function () {

    if ($.cookie('TripLog') == "Updated") {
        ShowSuccessMessage();
        $.removeCookie('TripLog');
    }


    self.POEditMode = ko.observable(false);
    self.LogSearch = ko.observable().extend({ notify: 'always' });;
    self.POLogID = ko.observable();
    self.Postflight = {};
    self.CurrentLegNumFloatbar = ko.observable();
    self.UserPrincipal = {};
    self.UserAccessControl = {};
    
    var Postflight = htmlDecode($("[id$='hdnPostflightMainInitializationObject']").val());
    var UserPrincipal = htmlDecode($("[id$='hdnUserPrincipalInitializationObject']").val());

    $("[id$='hdnPostflightMainInitializationObject']").val("");
    $("[id$='hdnUserPrincipalInitializationObject']").val("")

    if (!IsNullOrEmpty(Postflight) == true && !IsNullOrEmpty(UserPrincipal) == true) {
        var PostflightJsonObj = JSON.parse(Postflight);
        self.UserPrincipal = JSON.parse(UserPrincipal);
        self.Postflight = ko.mapping.fromJS(PostflightJsonObj);
    }
    
    GetACurrentTripLog();
    
    self.LogSearch.subscribe(function (newValue) {
        var LogNum = newValue;
        if (isNaN(Number(LogNum)) == true)
        {
            $("[id$='lbSearchMsg']").text("Log No Does Not Exist");
            return;
        }

        if (self.POEditMode() == false && !IsNullOrEmpty(LogNum)) {

            RequestStart();
            $.ajax({
                async: true,
                url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/LogSearch",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'LogNum': LogNum }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d;
                    if (returnResult == true && jsonObj.Success == true) {                    
                        var Trip = jsonObj.Result;
                        self.POLogID(Trip.POLogID);
                        if (self.Postflight.PostflightMain == undefined)
                            self.Postflight = ko.mapping.fromJS(Trip.PostflightMain);
                        else
                            ko.mapping.fromJS(Trip.PostflightMain, self.Postflight);

                        $("[id$='lbSearchMsg']").text("");
                        refreshTrip();
                    } else {
                        $("[id$='lbSearchMsg']").text("Log No Does Not Exist");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        } else {
            $("[id$='lbSearchMsg']").text("");
            if (!IsNullOrEmpty(LogNum)) {
                jAlert("Log Unsaved, please Save or Cancel Log", "Postflight");
            }
        }

    });
    self.BrowseBtn = ko.pureComputed(function () {
        return this.POEditMode() ? "browse-button" : "browse-button-disabled";
    }, self);
    self.ResetBtn = ko.pureComputed(function () {
        return this.POEditMode() ? "reset-icon" : "reset-icon-disable";
    }, self);
    
    self.CopyIcon = ko.pureComputed(function () {
        return this.POEditMode() ? "copy-icon" : "copy-icon-disabled";
    }, self);

    self.BlueButtonClass = ko.pureComputed(function () {
        return this.POEditMode() ? "button" : "button-disable";
    }, self);

    self.CharterRateBtn = ko.computed(function () {        
        $("#btnTailNoChargeHistory").prop("disabled", true);
        if (self.POEditMode() == true && self.Postflight.PostflightMain!=undefined && self.Postflight.PostflightMain.Fleet != undefined && self.Postflight.PostflightMain.FleetID() > 0) {
            $("#btnTailNoChargeHistory").prop("disabled", false);
            return "charter-rate-button";
        } else {
            $("#btnTailNoChargeHistory").prop("disabled", true);
            return "charter-rate-disable-button";            
        }
        return "charter-rate-disable-button";
    }, self);
    self.SavePostflightLog_Click = function () {
        //SavePostflightLog


        if (self.Postflight != null && self.Postflight != '' && self.POEditMode() == true) {
            if (POMaintainModelStatusBeforePageLeave(null,null,self.SavePostflightLog_Click) == false) {
                ResponseEnd();
                return;
            }                

            ChangeStateSaveCancelButtons(false);

            RequestStart();
            $.ajax({
                async: true,
                url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePostflightLog",
                type: "POST",
                dataType: "json",
                data: {},
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d;                    
                    if (jsonObj.Success == true && returnResult==true) {                                                
                        $.cookie('TripLog', 'Updated');
                        var tab = getQuerystring("seltab", "main");
                        if (window.location.search!=undefined && window.location.search.length > 0)
                            window.location.href = window.location.href.replace(window.location.search, "?seltab=" + tab);
                        else
                            window.location.href = window.location.href.concat("?seltab=" + tab);

                    } else {
                        ResponseEnd();
                        ChangeStateSaveCancelButtons(true);
                        getTripExceptionGrid("reload");
                        if (jsonObj.Success == false && jsonObj.Result == "RECALCULATESIFL") {
                            RecalculateSiflConfirm();
                        } else {                            
                            jAlert(jsonObj.ErrorsList[0], "Trip Alert");
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {                    
                    ChangeStateSaveCancelButtons(true);
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }

    };
    self.btnSearchClick = function () {
        self.LogSearch(self.LogSearch())
    };
    self.onSearchEnter = function (d, e) {
        if (e.keyCode === 13) {
            self.LogSearch($("#tbSearchBox").val());
        }
        return true;
    };
    self.btnNewLogClick = function () { PostflightNewLogButtonClick(); };
    self.btnCancel_Click = function () {
        HideSuccessMessage();
        setAlertTextToYesNo();
        jConfirm("Are you sure want to Cancel?", PostflightConfirmTitle, confirmTripLogCancelCallBackFn);
        setAlertTextToOkCancel();

    };
    self.btnEditLogClick = function () { HideSuccessMessage();  CheckIsLogUpdatedBySomeone(PostflightEditLogButtonClick); };
    self.btnDeleteLogClick = function () { PostflightDeleteLogButtonClick(); };
    self.btnOtherCrewDutyClick = function () { window.location = "/views/Transactions/PostFlight/OtherCrewDutyLog.aspx"; };
    self.btnExpenseLogClick = function () { window.location = "/views/Transactions/PostFlight/ExpenseCatalog.aspx"; };
    self.btnCopyExpense_Click = function (element, event) {
        if (self.CurrentLegExpenseData == null || self.CurrentLegExpenseData.PostflightExpenseID() == null || self.CurrentLegExpenseData.PostflightExpenseID() <= 0) {
            jAlert("Please select any expense", "Postflight");
            return;
        }

        window.location.href = "/Views/Transactions/Postflight/ExpenseCatalog.aspx?POExpenseID=" + self.CurrentLegExpenseData.PostflightExpenseID();
    };
    self.CopyExpenseBtn = ko.computed(function () {
        return self.POEditMode() == false ? "button" : "aspNetDisabled button-disable";
    }, this);

    self.HomebaseValidator_KO_change = function () { HomebaseValidator_KO(); };
    self.RequestorValidator_KO_change = function () {

        var btn = document.getElementById("btnRequestor");
        var passengerRequestorErrorLabel = "#lbcvRequestor";
        $(passengerRequestorErrorLabel).html("");
        var passengerRequestorID = self.Postflight.PostflightMain.PassengerRequestorID;
        passengerRequestorID(null);
        Requestor_Validate_Retrieve_KO(self.Postflight.PostflightMain.Passenger, passengerRequestorID, passengerRequestorErrorLabel, btn)
    };
    self.CopyFlightNumber_click = function () {
        if (IsNullOrEmptyOrUndefined(self.Postflight.PostflightMain.FlightNum()))
            return;
        setAlertTextToYesNo();
        jConfirm("Copy Flight Number To All Legs?", PostflightConfirmTitle, function (arg) {
            if (arg == true) {
                var params = {
                    property: "FLIGHTNUMBER",
                    propertyValue: self.Postflight.PostflightMain.FlightNum()
                };
                ExecuteCopyToLegsRequest(params);
            }
        });
        setAlertTextToOkCancel();
    };
    self.CopyRequestor_click = function () {
        if (IsNullOrEmptyOrUndefined(self.Postflight.PostflightMain.PassengerRequestorID()))
            return;
        setAlertTextToYesNo();
        jConfirm("Copy Requestor To All Legs?", PostflightConfirmTitle, function (arg) {
            if (arg == true) {
                var params = {
                    property: "REQUESTOR",
                    propertyValue: self.Postflight.PostflightMain.PassengerRequestorID()
                };
                ExecuteCopyToLegsRequest(params);
            }
        });
        setAlertTextToOkCancel();
    };
    self.CopyAccount_click = function () {
        if (IsNullOrEmptyOrUndefined(self.Postflight.PostflightMain.AccountID()))
            return;
        setAlertTextToYesNo();
        jConfirm("Copy Account No. To All Legs?", PostflightConfirmTitle, function (arg) {
            if (arg == true) {
                var params = {
                    property: "ACCOUNT",
                    propertyValue: self.Postflight.PostflightMain.AccountID()
                };
                ExecuteCopyToLegsRequest(params);
            }
        });
        setAlertTextToOkCancel();
    };
    self.CopyDepartment_click = function () {
        if (IsNullOrEmptyOrUndefined(self.Postflight.PostflightMain.DepartmentID()))
            return;
        setAlertTextToYesNo();
        jConfirm("Copy Department To All Legs?", PostflightConfirmTitle, function (arg) {
            if (arg == true) {
                var params = {
                    property: "DEPARTMENT",
                    propertyValue: self.Postflight.PostflightMain.DepartmentID()
                };
                ExecuteCopyToLegsRequest(params);
            }
        });
        setAlertTextToOkCancel();
    };
    self.CopyAuth_click = function () {
        if (IsNullOrEmptyOrUndefined(self.Postflight.PostflightMain.AuthorizationID()))
            return;
        setAlertTextToYesNo();
        jConfirm("Warning, Both the Current Department and Authorization will be copied to all Legs. Copy Authorization To All Legs?", PostflightConfirmTitle, function (arg) {
            if (arg == true) {
                var params = {
                    property: "DEPARTMENT",
                    propertyValue: self.Postflight.PostflightMain.DepartmentID()
                };
                ExecuteCopyToLegsRequest(params);
                setTimeout(function () {
                    params = {
                        property: "AUTHORIZATION",
                        propertyValue: self.Postflight.PostflightMain.AuthorizationID()
                    };
                    ExecuteCopyToLegsRequest(params);
                }, 1500);
            }
        });
        setAlertTextToOkCancel();
    };
    self.CopyDescription_click = function () {
        if (IsNullOrEmptyOrUndefined(self.Postflight.PostflightMain.POMainDescription()))
            return;
        setAlertTextToYesNo();
        jConfirm("Copy Trip Purpose To All Legs?", PostflightConfirmTitle, function (arg) {
            if (arg == true) {
                var params = {
                    property: "DESCRIPTION",
                    propertyValue: self.Postflight.PostflightMain.POMainDescription()
                };
                ExecuteCopyToLegsRequest(params);
            }
        });
        setAlertTextToOkCancel();
    };


    self.DepartmentValidator_KO_change = function () {

        self.Postflight.PostflightMain.DepartmentID(null);
        var departmentErrorLabel = "#lbcvDepartment";
        var btn = document.getElementById("btnDepartment");
        var DepartmentAuthorizationVM = self.Postflight.PostflightMain.DepartmentAuthorization;
        var authorizationLabel = "#lbcvAuthorization";
        $(authorizationLabel).html("");
        Department_Validate_Retrieve_KO(self.Postflight.PostflightMain.Department, self.Postflight.PostflightMain.DepartmentID, departmentErrorLabel, btn,DepartmentAuthorizationVM , authorizationLabel)
    };
    self.Authorization_KO_change = function () {
        var VM = self.Postflight.PostflightMain;
        var authorizationErrorLabel = "#lbcvAuthorization";
        var btn = document.getElementById("btnAuthorization");

        if (!IsNullOrEmpty(VM.DepartmentAuthorization.AuthorizationCD())) {
            if (IsNullOrEmpty(self.Postflight.PostflightMain.DepartmentID()) || IsNullOrEmpty(self.Postflight.PostflightMain.Department.DepartmentCD())) {                
                VM.DepartmentAuthorization.AuthorizationCD("");
                $(authorizationErrorLabel).html("Please enter the Department Code before entering the Authorization Code.");
                return;
            }
        }
        Authorization_Validate_Retrieve_OK(VM.DepartmentAuthorization,VM.AuthorizationID, authorizationErrorLabel, btn, VM.DepartmentID, VM.Department.DepartmentCD)
    };
    self.AccountValidate_KO_change = function () { AccountValidate_KO(); };


    self.TailNum_PostflightValidate= function()
    {
        
        var VM = self.Postflight.PostflightMain;
        $("#lbcvHomeBase").html("");
        VM.Aircraft.AircraftCD("");
        VM.FleetID(null);
        var fleetErrorlabel = "#lbcvTailNumber";
        $(fleetErrorlabel).html("");
        var btn = document.getElementById("btnTailNo");
        TailNum_PostflightValidate_Retrieve_KO(VM.FleetID, fleetErrorlabel, btn, VM.Aircraft.AircraftCD, self.Postflight.PostflightMain)
        
    }

    InitializeKOWatchMainNotes();

    self.FlightLogIcon = ko.pureComputed(function () {
        return (self.POLogID() && self.POEditMode()==false) ? "print_preview_icon" : "print_preview_icon_disable";
    }, self);

    self.HistoryIcon = ko.pureComputed(function () {
        return self.POLogID() ? "history-icon" : "history-icon-disable";
    }, self);

    
    self.lnkReportPreviewClick = function () {
        if (!IsNullOrEmptyOrUndefined(POLogID()) && POEditMode()==false) {
            var paramters = { POLogID: POLogID() };
            RequestStart();
            $.ajax({
                type: "POST",
                cache: false,
                url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/lnkReportPreview_Click',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paramters),
                success: function (response) {
                    var returnResult = verifyReturnedResult(response);
                    if (returnResult == true && response.d.Success == true) {
                        callToJSforReportDownload();
                    }
                },
                complete: function (jqXHR, textStatus) {
                    ResponseEnd();
                },
                error: function (xhr, textStatus, errorThrown) {
                    reportPostflightError(xhr, textStatus, errorThrown);
                }
            });
        }
    };
    self.ExceptionReloadCount = ko.observable(1).extend({ rateLimit: { timeout: 1000, method: "notifyWhenChangesStop" } });
    self.ExceptionReloadCount.subscribe(function (newvalue) {
        getTripExceptionGrid("reload");
    });

}
function RecalculateSiflConfirm()
{
    setAlertTextToYesNo();
    jConfirm("Recalculate SIFL?", "Confirmation!", function (arg) {        
        if (arg == true) {
            
            var btnPaxrecalculate=document.getElementById("btnReCalcSIFL");
            if (btnPaxrecalculate == undefined) {
                window.location = "/views/Transactions/PostFlight/PostFlightPax.aspx?seltab=pax&action=recalcsifl";                
            } else {                
                SiflRecalculateConfirmCall(true);
            }
        } else {
            self.SavePostflightLog_Click();
        }
    });
    setAlertTextToOkCancel();
}

function ExecuteCopyToLegsRequest(params)
{
    $.ajax({
        async: true,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/CopyValueToAllLegs",
        type: "POST",
        dataType: "json",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var retResult = verifyReturnedResult(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
    
}

function PostflightNewLogButtonClick()
{
    
    $("#btnNewTrip").prop('disabled', "disabled");
    
    $.ajax({
        async: true,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/NewLogButtonClick",
        type: "POST",
        dataType: "json",
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            var jsonObj = result.d;
            if (jsonObj.Success == true && returnResult==true) {                            
                window.location = "/views/Transactions/PostFlight/PostFlightMain.aspx?seltab=PostFlight";                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#btnNewTrip").prop('disabled', "");
            ResponseEnd();
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }        
    });
}

function PostflightDeleteLogButtonClick()
{
    if (self.Postflight.PostflightMain.POLogID()!=undefined && self.Postflight.PostflightMain.POLogID() > 0) {
        setAlertTextToYesNo();
        jConfirm("You are about to DELETE all data for this TRIP. All data for this TRIP will be lost. Are you sure you want to do this?", PostflightConfirmTitle, confirmTripLogDelete)
        setAlertTextToOkCancel();
    } else {
        jAlert("Log should not be Empty!", PostflightConfirmTitle);
    }
}
function confirmTripLogDelete(arg)
{
    if (arg == true) {
        var postflight = ko.mapping.toJS(self.Postflight);
        if (postflight != null && postflight != '') {
            $("#btnDeleteLog,#btnDelete").prop('disabled', "disabled");
            

            RequestStart(null, null);
            $.ajax({
                async: true,
                url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/DeletePostflightLog",
                type: "POST",
                dataType: "json",
                data: {},
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d;
                    if (jsonObj.Success==true && returnResult==true) {
                        jAlert("Log has been deleted successfully.", PostflightConfirmTitle, refreshTrip);
                    } else {
                        jAlert("Log can not be deleted!", PostflightConfirmTitle);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#btnDeleteLog,#btnDelete").prop('disabled', "");
                    ResponseEnd();
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () {
                    ResponseEnd();
                }
            });
        }
    }
}

function PostflightEditLogButtonClick()
{
    var tripLogid = self.Postflight.PostflightMain.POLogID();
    if (tripLogid != null && tripLogid != '' && tripLogid > 0) {
        $("#btnEditTrip,#btnEditFlightLog").prop('disabled', "disabled");
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/EditPostflightLog",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonData = result.d;
                if (jsonData.Success) {                    
                    self.POEditMode(true);
                    self.Postflight.PostflightMain.POEditMode(true);
                    if (UserPrincipal._IsOpenHistory == true) {
                        if (self.POLogID() > 0) {
                            openWinSharedPostflight("rdHistory");
                        } else {
                            jAlert("Please select a trip", title + ' - Trip History');
                            return;
                        }
                        return false;
                    }
                } else {
                    $("#btnEditTrip,#btnEditFlightLog").prop('disabled', false);
                    $("#btnEditTrip,#btnEditFlightLog").removeClass("ui_nav_disable");
                    $("#btnEditTrip,#btnEditFlightLog").addClass("ui_nav");

                    if (jsonData.StatusCode == 200 && jsonData.Success == false) {
                        jAlert(jsonData.ErrorsList[0],PostflightConfirmTitle);
                    } else {
                        refreshTrip();
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#btnEditTrip,#btnEditFlightLog").prop('disabled', false);
                ResponseEnd();
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { ResponseEnd(); }
        });

    }
}

function CheckIsLogUpdatedBySomeone(callback)
{
    if (self.Postflight.PostflightMain.POLogID() <= 0) {
        callback();
    } else {
        var params = "method=IsLogUpdated&apiType=fss&poLogId=" + self.Postflight.PostflightMain.POLogID() + "&lastUpdateDatetime=" + self.Postflight.PostflightMain.LastUpdTSStr();
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Utilities/GetApi.aspx?" + params,
            type: "GET",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                verifyReturnedResultForJqgrid(result);
                if (result == false)                    
                    callback();
                else
                {
                    jAlert("This log has been updated by another user. Please reload the log.", PostflightConfirmTitle);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#btnEditTrip,#btnEditFlightLog").prop('disabled', false);
                ResponseEnd();
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { ResponseEnd(); }
        });
    }
}

function getUserPrincipal() {
    $.ajax({
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/getUserPrincipal",
        type: "POST",
        dataType: "json",
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var jsonObj = result.d;
            self.UserPrincipal = jsonObj;
            InitializeBottomAndSummaryGrids();
            getTripExceptionGrid("reload");

        }, error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function InitializeBottomAndSummaryGrids() {
    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/PostFlight/PostFlightValidator.aspx/Summary_Validate_Retrieve",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            if (response.d.results != null) {
                   InitializeDetailedLegSummary(response.d.results)
                   InitializeSortSummaryDetail(response.d.results)
            }
        },
        complete: function (jqXHR, textStatus) {
          
        },
        error: function (xhr, textStatus, errorThrown) {
             reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}
function InitializeDetailedLegSummary(Data) {
    $(DetailedLegSummary).jqGrid('GridUnload');
    jQuery(DetailedLegSummary).jqGrid({
        mtype: 'POST',
        data:Data,
        datatype: "local",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        autowidth: false,
        width: 727,
        rowNum: 500,
        shrinkToFit: false,
        colNames: ['Legnum', 'Leg', 'Depart', 'Arrive', 'Miles', 'Scheduled', 'Out', 'Off', 'On', 'In', 'Blk. Hrs', 'Flt. Hrs', 'FAR', 'Category', 'End', 'Flt. No.', 'Flt. Cost'],
        colModel: [
            { name: 'LegNum', index: 'LegNum', width: 40, hidden: true },
            { name: 'Leg', index: 'Leg', width: 25 },
            { name: 'Depart', index: 'Depart', width: 40 },
            { name: 'Arrive', index: 'Arrive', width: 40 },
            { name: 'Miles', index: 'Miles', width: 30 },
            { name: 'Scheduled', index: 'Scheduled', width: 100 },
            { name: 'Out', index: 'Out', width: 30 },
            { name: 'Off', index: 'Off', width: 30 },
            { name: 'On', index: 'On',  width: 30 },
            { name: 'In', index: 'In',  width: 30 },
            { name: 'BlkHrs', index: 'BlkHrs', width: 42 },
            { name: 'FltHrs', index: 'FltHrs', width: 42 },
            { name: 'FAR', index: 'FAR', width: 30 },
            { name: 'Category', index: 'Category', width: 50 },
            { name: 'End', index: 'End', formatter: "checkbox", formatoptions: { disabled: true }, width: 25 },
            { name: 'FltNo', index: 'FltNo', width: 40 },
            { name: 'FlightCost', index: 'FlightCost', width: 70, formatter: 'currency', formatoptions: { prefix: '$', thousandsSeparator: ',' }}
        ],
        viewrecords: false,
        loadComplete: function () {
            if (jQuery(DetailedLegSummary).getGridParam("records") == 0) {
                $(DetailedLegSummary + ' tbody').html("<div style='padding-left:5px;font: 11px/16px Arial,Helvetica,sans-serif !important;'>No records found</div>");
            }
        }
    });
}
function InitializeSortSummaryDetail(Data) {
    $(ShortLegSummary).jqGrid('GridUnload');
    jQuery(ShortLegSummary).jqGrid({
        data:Data,
        mtype: 'POST',
        datatype: "local",
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        height: 'auto',
        width: 238,
        rowNum: 500,
        autowidth: false,
        shrinkToFit: true,
        colNames: ['Departure', 'From', 'To', 'Arrival'],
        colModel: [
            {
                name: 'DepartDate', index: 'DepartDate', width: 170, cellattr: function (rowId, val, rawObject)
                { return ' title="' + (IsNullOrEmptyOrUndefined(rawObject.DepartDate) ? "" : (rawObject.DepartDate + " " + rawObject.DepartTime)) + '"'; }
            },
            { name: 'Depart', index: 'Depart', width: 120 },
            { name: 'Arrive', index: 'Arrive', width: 120 },
            {
                name: 'ArrivalDate', index: 'ArrivalDate', width: 170, cellattr: function (rowId, val, rawObject)
                { return ' title="' + (IsNullOrEmptyOrUndefined(rawObject.ArrivalDate) ? "" : (rawObject.ArrivalDate + " " + rawObject.ArrivalTime)) + '"'; }
            }
        ],
        viewrecords: false,
        loadComplete: function () {
            if (jQuery(ShortLegSummary).getGridParam("records") == 0) {
                $(ShortLegSummary + ' tbody').html("<div style='padding-left:5px;font: 11px/16px Arial,Helvetica,sans-serif !important;'>No records found</div>");
            }
        }
    });
}
function getTripExceptionGrid(arg) {

    if (self.POEditMode() || self.POLogID() > 0) {

        $.ajax({
            async: true,
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/TripException",
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = JSON.parse(result.d);
                if (arg == "init")
                    BindExceptionGrid(jsonObj.ExceptionList);
                else {
                    $("#Exception").jqGrid('GridUnload');
                    BindExceptionGrid(jsonObj.ExceptionList);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ResponseEnd();
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}
function setSeverityImage(cellvalue, options, rowObject) {
    var cellValueInt = parseInt(cellvalue);
    if (cellValueInt == 1) {
        return "<center><img src='/App_Themes/Default/images/info_red.gif' /></center>";
    }
    else {
        return "<center><img src='/App_Themes/Default/images/info_yellow.gif' /></center>";
    }
}
function BindExceptionGrid(data) {
    jQuery("#Exception").jqGrid({
        data: data,
        datatype: "local",
        height: 'auto',
        autowidth: false,
        shrinkToFit: false,
        width: null,
        colModel: [
            { name: 'Severity', index: 'Severity', width: 30, formatter: setSeverityImage },
            { name: 'ExceptionDescription', index: 'ExceptionDescription', width: 280 },
            { name: 'SubModuleGroup', index: 'SubModuleGroup', width: 140, summaryType: setExceptionGroupHeader },
            { name: 'SubModuleID', width: 140, hidden: true }
        ],
        viewrecords: true,
        sortname: 'SubModuleID',
        sortorder: 'asc',
        grouping: true,
        rowNum: 500,
        groupingView: {
            groupField: ['SubModuleID'],
            groupColumnShow: [true],
            groupText: ['{SubModuleGroup}'],
            groupCollapse: false
        },
        loadComplete: function () {
            if (jQuery('#Exception').getGridParam("records") == 0) {
                $('#gbox_Exception').css('height', '0px');
                $('#gbox_Exception').css('overflow', 'hidden');
            }
            $(this).jqGrid('hideCol', ["SubModuleGroup"]);
            $(this).jqGrid('hideCol', ["SubModuleID"]);
        },
        gridComplete: function () {
            jQuery("#Exception").css('width', '');
        }
    });
}
function setExceptionGroupHeader(val, name, record) {
    if (!IsNullOrEmptyOrUndefined(record))
        return "Group: " + record[name];
    else return "";
}


function OnLogSearchClose(oWnd, args) {
    if (args != null) {
        var arg = args.get_argument();
        if (arg) {
            self.LogSearch(arg.LogNum);
            if (self.POEditMode() == true) {
                $("[id$='SearchBox']").val("");
            }
        }
    }
}
function refreshTrip() {
    var reloadUrl = removeQueryStringParam(" ", window.location.href);
    window.location = reloadUrl;
}
function GetACurrentTripLog(isAsync) {
    if (isAsync == undefined)
        isAsync = false;

   $.ajax({
        async: isAsync,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/GetACurrentTripLog",
        type: "POST",
        dataType: "json",
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            var jsonObj = result.d;            
            if (returnResult==true && jsonObj.Success == true) {
                var TripLog = jsonObj.Result.PostflightMain;                
                
                self.UserPrincipal = jsonObj.Result.Principal;

                self.POLogID(TripLog.POLogID);
                self.POEditMode(TripLog.PostflightMain.POEditMode);

                if (self.Postflight.PostflightMain == undefined) {
                    self.Postflight = ko.mapping.fromJS(TripLog)
                }
                else {
                    ko.mapping.fromJS(TripLog, self.Postflight)                    
                }

                if (self.POEditMode() == true) {
                    setTimeout(function () {
                        if (typeof fireValidationOnPageloadForWrongCodes === "function") {
                            fireValidationOnPageloadForWrongCodes();
                        }
                    }, 500);
                }

            }
           }, error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}


ko.bindingHandlers.PostflightHeader = {
    init: function (element, valueAccessor, allBindings) {
        var POlogid = valueAccessor();
        var mode = allBindings.get('mode');
        var btn = allBindings.get('btn');
        PostflightHeaderBtn(element, POlogid, mode, btn);
    },
    update: function (element, valueAccessor, allBindings) {
        var POlogid = valueAccessor();
        var mode = allBindings.get('mode');
        var btn = allBindings.get('btn');
        PostflightHeaderBtn(element, POlogid, mode, btn);
    }
};
function PostflightHeaderBtn(element, POlogid, mode, btn) {
    if (POlogid == null) POlogid = 0;
    if (POlogid == 0 && mode == false && (btn == "New" || btn == "Retrieve")) {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    }
    else if ((btn == "Delete" || btn == "Edit") && POlogid == 0 && mode == false) {
        $(element).addClass("ui_nav_disable");
        $(element).removeClass("ui_nav");
        $(element).prop("disabled", true);
    }
    else if ((btn == "Delete" || btn == "Edit") && POlogid > 0 && mode == false) {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    }
    else if (POlogid != 0 && mode == true && btn == "Save") {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    } else if (POlogid != 0 && mode == true && btn == "Cancel") {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    } else if (POlogid == 0 && mode == true && btn == "Save") {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    } else if (POlogid == 0 && mode == true && btn == "Cancel") {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    }
    else if (mode == true && (btn == "Expense" || btn == "OtherCrewDutyLog")) {
        $(element).addClass("ui_nav_disable");
        $(element).removeClass("ui_nav");
        $(element).prop("disabled", true);
    }
    else if (mode == false && (btn == "Expense" || btn == "OtherCrewDutyLog")) {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    }
    else if (POlogid != 0 && mode == false && btn != "Save" && btn != "Cancel") {
        $(element).addClass("ui_nav");
        $(element).removeClass("ui_nav_disable");
        $(element).prop("disabled", false);
    } else {
        $(element).addClass("ui_nav_disable");
        $(element).removeClass("ui_nav");
        $(element).prop("disabled", true);
    }
}

ko.bindingHandlers.PostflightFooter = {
    init: function (element, valueAccessor, allBindings) {
        var logid = valueAccessor();
        var mode = allBindings.get('mode');
        var btn = allBindings.get('btn');
        PostflightFooterBtn(element, logid, mode, btn);
    },
    update: function (element, valueAccessor, allBindings) {
        var logid = valueAccessor();
        var mode = allBindings.get('mode');
        var btn = allBindings.get('btn');
        PostflightFooterBtn(element, logid, mode, btn);
    }
};
function PostflightFooterBtn(element, logid, mode, btn) {
    if (logid == null) logid = 0;
    if (logid == 0 && mode == false) {
        $(element).addClass("button-disable");
        $(element).removeClass("button");
        $(element).prop("disabled", true);
    } else if (logid != 0 && mode == false && (btn == "Delete" || btn == "Edit" || btn == "Next")) {
        $(element).addClass("button");
        $(element).removeClass("button-disable");
        $(element).prop("disabled", false);
    } else if ((logid != 0 || logid == 0) && mode == true && (btn == "Cancel" || btn == "Save" || btn == "Next")) {
        $(element).addClass("button");
        $(element).removeClass("button-disable");
        $(element).prop("disabled", false);
    } else {
        $(element).addClass("button-disable");
        $(element).removeClass("button");
        $(element).prop("disabled", true);
    }
}

function confirmTripLogCancelCallBackFn(arg) {
    if (arg == true) {
        //$("#preflightTripViewerLink").attr("href", "~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PreTSTripSheetMain.xml&tripnum=" + self.Preflight.PreflightMain.TripNUM() + "&tripid=" + self.Preflight.TripId());

        ChangeStateSaveCancelButtons(false);
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/CancelAPostflightLog",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d;
                if (jsonObj.Success && returnResult==true) {
                    var PoLog = jsonObj.Result;
                    self.POLogID(PoLog.PostflightMain.POLogID);
                    self.POEditMode(PoLog.PostflightMain.POEditMode);
                    ko.mapping.fromJS(PoLog, self.Postflight);

                }
                AfterCancelCallComplete(arg);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ResponseEnd();
                reportPostflightError(jqXHR, textStatus, errorThrown);
                ChangeStateSaveCancelButtons(true);

            },
            complete: function () { ResponseEnd(); ChangeStateSaveCancelButtons(true); }
        });
    } else {
        self.POEditMode(true);
    }
}

function AfterCancelCallComplete(arg) {
    if (self.POLogID() != null && self.POLogID() == 0 && arg == true) {
        self.POLogID(null);
        window.location = "/views/Transactions/Postflight/PostflightMain.aspx?seltab=PostFlight";
    } else if (self.POLogID != null && self.POLogID() > 0 && arg == true) {
        var reloadUrl = removeQueryStringParam(" ", window.location.href);
        window.location = reloadUrl;
    }
    //$("#Exception").jqGrid('GridUnload');
}

function ChangeStateSaveCancelButtons(IsEnable)
{
    if (IsEnable) {
        $("#btnSaveLog,#btnSave").prop('disabled', "");
        $("#btnCancel,#btnCancelLog").prop("disabled", "");
    } else {
        $("#btnSaveLog,#btnSave").prop('disabled', "disabled");
        $("#btnCancel,#btnCancelLog").prop("disabled", "disabled");
    }
}

function OnPFSearchRetrieveClose(oWnd, args) {
    if (args != null) {
        var arg = args.get_argument();
        if (arg) {
            PFSearchRetrieveRowSelect(arg.TripID);
        }
    }
}

function PFSearchRetrieveRowSelect(tripID) {
    var paramters = { TripID: tripID, isPostFlight: true };
    RequestStart();
    $.ajax({
        type: "POST",
        cache: false,
        url: '/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx/PFSearchRetrieveRowSelect',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(paramters),
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            var jsonObj = response.d;
            if (returnResult == true && jsonObj.Success == true) {
                if (jsonObj.Result.boolResult) {
                    jAlert('Tripsheet Retrieval Completed', 'Retrieval Status', callBackRedirect);
                }
                else if (!IsNullOrEmpty(jsonObj.Result.Message)) {
                    if (!jsonObj.Result.isConfirm) {
                        jAlert(jsonObj.Result.Message, PostflightConfirmTitle);
                    }
                    else {
                        setAlertTextToYesNo();
                        jConfirm(jsonObj.Result.Message, PostflightConfirmTitle, function (r) {
                            if (r) {
                                ConfirmPFSearchRetrieve();
                            }
                        });
                        setAlertTextToOkCancel();
                    }
                }
            }
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function ConfirmPFSearchRetrieve() {
    var paramters = { isPostFlight: true };
    RequestStart();
    $.ajax({
        type: "POST",
        cache: false,
        url: '/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx/ConfirmPFSearchRetrieve',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(paramters),
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                jAlert('Tripsheet Retrieval Completed', 'Retrieval Status', callBackRedirect);
            }
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}
