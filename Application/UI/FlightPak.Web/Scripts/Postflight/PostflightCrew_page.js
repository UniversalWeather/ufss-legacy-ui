﻿var Tenth = 1;
var Minute = 2;
var Other = 0;
var globalIdentity = "";

function PopupLoad(Identity) {
    globalIdentity = Identity;
    var isKilometer = Identity._fpSettings._IsKilometer == null ? false : Identity._fpSettings._IsKilometer;
    if (isKilometer)
        $('#lbMiles').text("Kilometer");
    else
        $('#lbMiles').text("Miles(N)");
    if (Identity._fpSettings._TimeDisplayTenMin == Tenth)
        $('#tbInstr').val("000.0");
    else
        $('#tbNight').val("000.00");

    if (Identity._fpSettings._CrewLogCustomLBLShort1 != null)
        var custom1Header1 = Identity._fpSettings._CrewLogCustomLBLShort1;
    if (Identity._fpSettings._CrewLogCustomLBLShort2 != null)
        var custom1Header2 = Identity._fpSettings._CrewLogCustomLBLShort2;
    if (Identity._fpSettings._SpecificationShort3 != null)
        var custom1Header3 = Identity._fpSettings._SpecificationShort3;
    if (Identity._fpSettings._SpecificationShort4 != null)
        var custom1Header4 = Identity._fpSettings._SpecificationShort4;
    if (!IsNullOrEmpty(custom1Header1) && custom1Header1 != "0")
        colNames.push(custom1Header1);
    else
        colNames.push("spec1");
    if (!IsNullOrEmpty(custom1Header2) && custom1Header2 != "0")
        colNames.push(custom1Header2);
    else
        colNames.push("spec2");

    if (!IsNullOrEmpty(custom1Header3) && custom1Header3 != "0")
        colNames.push(custom1Header3);
    else
        colNames.push("spec3");

    if (!IsNullOrEmpty(custom1Header4) && custom1Header4 != "0")
        colNames.push(custom1Header4);
    else
        colNames.push("spec4");
}
function SetTabSelection() {
    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $(".tabStrip li").eq(0).removeClass("tabStripItem").addClass("tabStripItemSelected");
}
function SelectTab(Leg) {
    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $("#Leg" + Leg.LegNUM()).removeClass("tabStripItem").addClass("tabStripItemSelected");
}
function ApplyControlMask()
{
    if (globalIdentity._fpSettings._TimeDisplayTenMin == Minute) {
        $("#tbCrewBlockHours").mask("99:99");
        $("#tbCrewFlightHours").mask("99:99");
    }

    $("#tbDutyBegintime").mask("99:99").change(function () {
        if (!IsNullOrEmptyOrUndefined($(this).val())) {
            var errMsg = ValidateTime($(this).val());
            if (errMsg != "") {
                jAlert(errMsg, msgTitle, function (r) {
                });
                $('#tbDutyBegintime').focus();
            }
            else {
                self.SelectedCrewData.CrewDutyStartTime($(this).val());
                var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
                var crewID = ko.mapping.toJS(self.SelectedCrewData.CrewID);
                SaveEditedCrewInfoToSession();
                CheckCrewDutyOverride(crewID, currentLegNum);
            }
        }
    });
    $("#tbDutyEndtime").mask("99:99").change(function () {
        if (!IsNullOrEmptyOrUndefined($(this).val())) {
            var errMsg = ValidateTime($(this).val());
            if (errMsg != "") {
                jAlert(errMsg, msgTitle, function (r) {
                });
                $('#tbDutyEndtime').focus();
            }
            else {
                self.SelectedCrewData.CrewDutyEndTime($(this).val());
                var currentLegNum = ko.mapping.toJS(self.CurrentLegNum);
                var crewID = ko.mapping.toJS(self.SelectedCrewData.CrewID);
                SaveEditedCrewInfoToSession();
                CheckCrewDutyOverride(crewID, currentLegNum);
               
            }
        }
    });

}
function SetCrewAugmentMsgColor(crewsList,CrewID) {
    if (!IsNullOrEmptyOrUndefined(crewsList) && crewsList.length > 0)
    {
        $.each(crewsList, function (key, Crew) {
            SetCrewSelectionGridTextboxColor(Crew.CrewID);
        });
    }
    else
        SetCrewSelectionGridTextboxColor(CrewID);
}
function EnableControls(rowId) {
    $("#tbDateBegin").click(function () { $("#tbDateBegin").datepicker('show'); });
    $("#tbDateEndDate").click(function () { $("#tbDateEndDate").datepicker('show'); });

    $('#dutyBegin').attr('class', 'pocs_textbox');
    $('#dutyEnd').attr('class', 'pocs_textbox');
    $('#tbDateBegin').removeAttr('readonly disabled').removeClass('inpt_non_edit');
    $('#tbDateEndDate').removeAttr('readonly disabled').removeClass('inpt_non_edit');
    $('#tbDutyBegintime').removeAttr('readonly disabled').removeClass('inpt_non_edit');
    $('#tbDutyEndtime').removeAttr('readonly disabled').removeClass('inpt_non_edit');
    $('#tbCrewBlockHours').removeAttr('readonly disabled').removeClass('inpt_non_edit');
    $('#tbCrewFlightHours').removeAttr('readonly disabled').removeClass('inpt_non_edit');

}
function DisableControls(rowId) {
    $('#dutyBegin').removeAttr('class', 'pocs_textbox');
    $('#dutyEnd').removeAttr('class', 'pocs_textbox');
    $('#tbDateBegin').attr({ 'readonly': 'readonly','disabled':'disabled'}).addClass('inpt_non_edit');
    $('#tbDateEndDate').attr({ 'readonly': 'readonly','disabled':'disabled'}).addClass('inpt_non_edit');
    $('#tbDutyBegintime').attr({ 'readonly': 'readonly','disabled':'disabled'}).addClass('inpt_non_edit');
    $('#tbDutyEndtime').attr({ 'readonly': 'readonly', 'disabled': 'disabled'}).addClass('inpt_non_edit');
    $('#tbCrewBlockHours').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');
    $('#tbCrewFlightHours').attr({ 'readonly': 'readonly'}).addClass('inpt_non_edit');

}
function EnableGridRow(rowid) {
    $('#gbox_jqCrewGrid').find("input").attr("disabled", "disabled");
    $('#gbox_jqCrewGrid').find("select").attr("disabled", "disabled");
    $('#' + rowid).find("input").removeAttr("disabled");
    $('#' + rowid).find("select").removeAttr("disabled");
    $('#gview_jqCrewGrid').find(".cbox").removeAttr("disabled");
}
function CrewNameFormatter(cellvalue, options, rowObject) {
    var name = '';
    if (IsNullOrEmptyOrUndefined(rowObject["CrewFirstName"]) == false)
        name = $.trim(rowObject["CrewFirstName"]);
    if (IsNullOrEmptyOrUndefined(rowObject["CrewLastName"]) == false)
        name = name + ' ' + $.trim(rowObject["CrewLastName"]);
    name = name.replace(/null/g, "");
    return name;
}
function CrewName() {
    var name = '';
    var SelectedCrewData = ko.mapping.toJS(self.SelectedCrewData);
    if (!IsNullOrEmptyOrUndefined(SelectedCrewData) && SelectedCrewData.length != 0) {
        var fName = ko.mapping.toJS(self.SelectedCrewData.CrewFirstName);
        var lName = ko.mapping.toJS(self.SelectedCrewData.CrewLastName);
        if (!IsNullOrEmptyOrUndefined(fName))
            name = fName.trim() + ' ';
        if (!IsNullOrEmptyOrUndefined(lName))
            name = name + lName.trim();
        name = name.replace(/null/g, "");
    }
    return name;
}
function CrewPositionFormatter(cellvalue, options, rowObject) { 
    var DutyCode = rowObject["DutyTYPE"];
    var PIC = "", SIC = "", Engineer = "", Instructor = "", Attendant = "", Other = "", Zero = "";
    if (DutyCode == "P")
        PIC = "selected='selected'";
    else if (DutyCode == "S")
        SIC = "selected='selected'";
    else if (DutyCode == "E")
        Engineer = "selected='selected'";
    else if (DutyCode == "I")
        Instructor = "selected='selected'";
    else if (DutyCode == "A")
        Attendant = "selected='selected'";
    else if (DutyCode == "O")
        Other = "selected='selected'";
    else if (DutyCode == "0" || DutyCode == "")
        Zero = "selected='selected'";

    var Html = "<select tabindex='1' onchange='javascript:UpdateCrew(this);' id='ddl" + rowObject["CrewID"] + "' row-id='" + rowObject["CrewID"] + "' leg-no='" + rowObject["POLegID"] + "' col='ddlCrewDutyType'>";
    Html += "<option value='0' " + Zero + ">--Select--</option>";
    Html += "<option value='P' " + PIC + ">PIC</option>";
    Html += "<option value='S' " + SIC + ">SIC</option>";
    Html += "<option value='E' " + Engineer + ">Engineer</option>";
    Html += "<option value='I' " + Instructor + ">Instructor</option>";
    Html += "<option value='A' " + Attendant + ">Attendant</option>";
    Html += "<option value='O' " + Other + ">Other</option>";
    Html += "</select>";
    var $Html = $(Html);
    $Html.attr('title', $Html.find("option:selected").text());
    return $('<div/>').append($Html).html();
}
function CrewSeatFormatter(cellvalue, options, rowObject) {
    var SelSeat = rowObject["Seat"];
    var Left = "", Right = "", Other = "", None = "", Zero = "";
    if (SelSeat == "L")
        Left = "selected='selected'";
    else if (SelSeat == "R")
        Right = "selected='selected'";
    else if (SelSeat == "O")
        Other = "selected='selected'";
    else if (SelSeat == "N")
        None = "selected='selected'";
    else if (SelSeat == "0" || SelSeat == "" || SelSeat == null)
        Zero = "selected='selected'";
    var Html = "<select tabindex='19' onchange='UpdateCrew(this);' id='ddl'" + rowObject["CrewID"] + " row-id='" + rowObject["CrewID"] + "' leg-no='" + rowObject["POLegID"] + "' col='ddlCrewSeat'>";
    Html += "<option value='0' " + Zero + ">Select</option>";
    Html += "<option value='L' " + Left + ">Left</option>";
    Html += "<option value='R' " + Right + ">Right</option>";
    Html += "<option value='O' " + Other + ">Other</option>";
    Html += "<option value='N' " + None + ">None</option>";
    Html += "</select>";
    var $Html = $(Html);
    $Html.attr('title', $Html.find("option:selected").text());
    return $('<div/>').append($Html).html();
}
function EnableDisableControlsofGrid(enable) {
    if (enable) {
        $('#gbox_jqCrewGrid').find("input").attr("disabled", "disabled");
        $('#gbox_jqCrewGrid').find("select").attr("disabled", "disabled");
        $('#gview_jqCrewGrid').find(".cbox").removeAttr("disabled");
        $('#jqCrewGrid').find(".datetimechange").mask("99:99");
        $('#' + $('#jqCrewGrid tr:nth-child(2)').attr('id')).find("input").removeAttr("disabled");
        $('#' + $('#jqCrewGrid tr:nth-child(2)').attr('id')).find("select").removeAttr("disabled");
        $('#pg_gridPagerCrew').show();
    }
    else {
        $('#gbox_jqCrewGrid').find("input").attr("disabled", "disabled");
        $('#gbox_jqCrewGrid').find("select").attr("disabled", "disabled");
        $('#gview_jqCrewGrid').find(".cbox").attr("disabled", "disabled");
        $('#pg_gridPagerCrew').hide();

    }
}
function SetDutyHrsTextboxColor(crewID, isOverride) {
    if (!IsNullOrEmptyOrUndefined(crewID)) {
        if (isOverride || $('#chkIsDutyOverride' + crewID).is(':checked')) {
            $('#tbDuty' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;');
            $('#tbDutyBegin' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;font:12px Arial!important;');
            $('#tbDutyEnd' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;font:12px Arial!important;');
        }
        else {
            $('#tbDuty' + crewID).attr('style', 'background-color: #F1F1F1!important;');
            $('#tbDutyBegin' + crewID).attr('style', 'color:#111111!important;font:12px Arial!important;');
            $('#tbDutyEnd' + crewID).attr('style', 'color:#111111!important;font:12px Arial!important;');
        }
    }
}
function SetBlockFlightHrsTextboxColor(crewID, col)
{
    var legHrsValue;
    var crewHrsValue;
    if (col == "BlockHours") {
        legHrsValue = $('#hdnBlockHours').val();
        crewHrsValue = $('#tbBlkHrs' + crewID).val();
    }
    else if(col == "FlightHours") {
        legHrsValue = $('#hdnFlightHours').val();
        crewHrsValue = $('#tbFlightHrs' + crewID).val();
    }
    if (!IsNullOrEmptyOrUndefined(crewID)) {
        if (legHrsValue != crewHrsValue) {
            $('#chkIsAugmentCrew' + crewID).prop('checked', true);
            $('#tbBlkHrs' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;');
            $('#tbFlightHrs' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;');
            $('#lbCrewAugmentMsg').text("Augmented Crew Leg!");
            $('#lbCrewAugmentMsg').attr('style', 'color:red !important')
            $('#lbCrewAugmentMsg').css('visibility', 'visible');
        }
        else {
            $('#chkIsAugmentCrew' + crewID).prop('checked', false);
            $('#lbCrewAugmentMsg').text("").css('display', 'none');
            $('#tbBlkHrs' + crewID).attr('style', 'color:#111111!important;');
            $('#tbFlightHrs' + crewID).attr('style', 'color:#111111!important;');
        }
    }
}
function SetCrewSelectionGridTextboxColor(crewID) {
    //RON Red color 
    if ($('#chkIsRemainOverNightOverride' + crewID).is(':checked')) {
        $('#tbRON' + crewID).attr('style', 'background-color:  #fe5b5c!important', 'border-color: #ea0303!important');
        $('#tbRON' + crewID).removeAttr('readonly');
    }
    else {
        $('#tbRON' + crewID).attr('style', 'background-color: #F1F1F1!important;');
        $('#tbRON' + crewID).attr('readonly', 'readonly');
    }
    if ($('#chkIsDutyOverride' + crewID).is(':checked')) {
        $('#tbDuty' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;');
        $('#tbDutyBegin' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;font:12px Arial!important;');
        $('#tbDutyEnd' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;font:12px Arial!important;');
    }
    else {
        $('#tbDuty' + crewID).attr('style', 'background-color: #F1F1F1!important;');
        $('#tbDutyBegin' + crewID).attr('style', 'color:#111111!important;font:12px Arial!important;');
        $('#tbDutyEnd' + crewID).attr('style', 'color:#111111!important;font:12px Arial!important;');
    }

    if ($('#chkIsAugmentCrew' + crewID).is(':checked')) {
        $('#tbBlkHrs' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;');
        $('#tbFlightHrs' + crewID).attr('style', 'background-color: #fe5b5c!important;border-color:#ea0303!important;color:#111111!important;');
        $('#lbCrewAugmentMsg').text("Augmented Crew Leg!");
        $('#lbCrewAugmentMsg').attr('style', 'color:red !important')
        $('#lbCrewAugmentMsg').css('visibility', 'visible');
    }
    else {
        $('#lbCrewAugmentMsg').text("").css('display', 'none');
        $('#tbBlkHrs' + crewID).attr('style', 'color:#111111!important;');
        $('#tbFlightHrs' + crewID).attr('style', 'color:#111111!important;');
    }
}
function fnNumericValidation(enteredval, val, decimalPlace) {
    var pattern = "^[0-9]{0," + val + "}(\\.[0-9]{0," + decimalPlace + "})?$";
    var regularExp = new RegExp(pattern);
    var errorMsg = "";
    if (!IsNullOrEmpty(enteredval)) {
        if (!regularExp.test(enteredval)) {
            var valPadding = "";
            var decimalPadding = "";
            for (var i = 0; i < val; i++) {
                valPadding += "N";
            }
            for (var i = 0; i < decimalPlace; i++) {
                decimalPadding += "N";
            }
            errorMsg = "You entered invalid format : " + enteredval + ".<br/>Accepted format is " + valPadding + "." + decimalPadding;
        }
    }
    return errorMsg;
}
function CheckBlockOrFlightHourOverride(crewID) {
    var result = false;

    if (($('#hdnBlockHours').val()) != ($('#tbBlockHours').val())) {
            $('#chkIsAugmentCrew' + crewID).attr('checked', 'checked');
            result = true;
        }

    if (($('#hdnFlightHours').val()) != ($('#tbFlightHours').val())) {
            $('#chkIsAugmentCrew' + crewID).attr('checked', 'checked');
            result = true;
        }
    
    return result;
}
function setScrollPostFlightCrew() {
    var table_header = $('#gview_jqCrewGrid').find('.ui-jqgrid-hbox').css("position", "relative");
    $('#gview_jqCrewGrid').find('.ui-jqgrid-bdiv').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
        table_header.css('right', scrollPositionX);
    }).jScrollPane({
        scrollbarWidth: 15,
        scrollbarMargin: 0
    });
}
function formatAlert(controlId, enteredVal) {
    var msgToDisplay = "You entered: " + enteredVal + " . <br/>Invalid Format.<br/> The Format is HH:MM";
    jAlert(msgToDisplay, msgTitle, function (r) {
        $('#' + controlId).val("00:00");
        $('#' + controlId).focus();
    });
}
function ValidateEnteredData(controlId, enteredVal) {
    var IsCheck = true;
    if (globalIdentity != null) {
        var Tenths = new RegExp("^[0-9]{0,3}(\\.[0-9]{0,1})?$");
        var Minutes = new RegExp("^[0-9]{0,3}(:([0-5]{0,1}[0-9]{0,1}))?$");
        if (globalIdentity._fpSettings._TimeDisplayTenMin == Tenth) {
            if (!IsNullOrEmpty(enteredVal)) {
                if (!Tenths.test(enteredVal)) {
                    var msgToDisplay = "You entered: " + enteredVal + ".<br/>Its a Invalid Format.<br/> The Format is NNN.N";
                    jAlert(msgToDisplay, msgTitle, function (r) {
                        $('#' + controlId).val("000.0");
                        $('#' + controlId).focus();
                    });
                    IsCheck = false;
                }
            }
        }
        else {
            if (!IsNullOrEmpty(enteredVal)) {
                var errMsg = ValidateTime(enteredVal);
                if (errMsg != "") {
                    jAlert(errMsg, msgTitle, function (r) {
                        $('#' + controlId).val("00:00");
                        $('#' + controlId).focus();
                    });
                    IsCheck = false;
                }
            }
        }
    }

    if (IsCheck) {
        if (globalIdentity._fpSettings._TimeDisplayTenMin != null) {
            if (globalIdentity._fpSettings._TimeDisplayTenMin == Minute) {
                if ((enteredVal.trim() != "") && (enteredVal.trim() != ":") && (enteredVal.trim() != ".")) {
                    if (enteredVal.indexOf(":") != -1) {
                        var timeArray = enteredVal.split(':');
                        if (IsNullOrEmpty(timeArray[0])) {
                            timeArray[0] = "0";
                            $('#' + controlId).val(timeArray[0] + ":" + timeArray[1]);
                        }
                        if (!IsNullOrEmpty(timeArray[1])) {
                            var result = 0;
                            if (timeArray[1].length != 2) {
                                result = (parseInt(timeArray[1])) / 10;
                                if (result < 1) {
                                    if (timeArray[0] != null) {
                                        if (timeArray[0].trim() == "")
                                            timeArray[0] = "0";
                                    }
                                    else
                                        timeArray[0] = "0";
                                    $('#' + controlId).val(timeArray[0] + ":" + "0" + timeArray[1]);
                                }
                            }
                        }
                        else
                            $('#' + controlId).val(enteredVal + "00");
                    }
                    else
                        $('#' + controlId).val(enteredVal + ":00");
                }
                else
                    $('#' + controlId).val("00:00");
                $('#' + controlId).focus();
            }
            else {
                if ((enteredVal.trim() != "") && (enteredVal.trim() != ":") && (enteredVal.trim() != ".")) {
                    if (enteredVal.indexOf(".") != -1) {
                        var timeArray = enteredVal.split('.');
                        if (IsNullOrEmpty(timeArray[0])) {
                            timeArray[0] = "0";
                            $('#' + controlId).val(timeArray[0] + "." + timeArray[1]);
                        }
                        if (!IsNullOrEmpty(timeArray[1])) {
                            if (timeArray[0] != null) {
                                if (timeArray[0].trim() == "")
                                    timeArray[0] = "0";
                            }
                            else
                                timeArray[0] = "0";
                            $('#' + controlId).val(timeArray[0] + "." + timeArray[1]);
                        }
                        else
                            $('#' + controlId).val(enteredVal + "0");
                    }
                    else
                        $('#' + controlId).val(enteredVal + ".0");
                }
                else
                    $('#' + controlId).val("000.0");
                $('#' + controlId).focus();
            }
        }
        else {
            if ((enteredVal.trim() != "") && (enteredVal.trim() != ":") && (enteredVal.trim() != ".")) {
                if (enteredVal.indexOf(".") != -1) {
                    var timeArray = enteredVal.split('.');
                    if (IsNullOrEmpty(timeArray[0])) {
                        timeArray[0] = "000";
                        $('#' + controlId).val(timeArray[0] + "." + timeArray[1]);
                    }
                    if (!IsNullOrEmpty(timeArray[1])) {
                        if (timeArray[0] != null) {
                            if (timeArray[0].trim() == "")
                                timeArray[0] = "000";
                        }
                        else
                            timeArray[0] = "000";
                        $('#' + controlId).val(timeArray[0] + "." + "0");
                    }
                    else
                        $('#' + controlId).val(enteredVal + "0");
                }
                else
                    $('#' + controlId).val(enteredVal + ".0");
            }
            else
                $('#' + controlId).val("000.0");
            $('#' + controlId).focus();
        }
    }
    return IsCheck;
}
function CheckIfCrewExists(Crews, isCopyToAll, isAddedBySearch) {
    var alreadyExistCrews = "";
    var crewsArray = new Array();
    var dataArray = $(jqCrewTableId).jqGrid('getGridParam', 'data');
    $.each(Crews, function (key, Crew) {
        var crewData;
        if (dataArray != undefined) {
            crewData = dataArray.filter(function (gridCrew, index) {
                if (gridCrew.CrewID == Crew.CrewID)
                    return gridCrew;
            });
        }
        if (crewData != undefined && crewData.length > 0)
            alreadyExistCrews += Crew.CrewCD + ',';
        crewsArray.push(Crew);
    });
    if (!IsNullOrEmpty(alreadyExistCrews)) {
        setAlertTextToOkCancel();
        jConfirm('Warning: Crew Code(s) [' + alreadyExistCrews + ' ] already Exists.', msgTitle, function (r) {
            if (r) {
                if (crewsArray.length > 0) {
                    AddCrewToSession(crewsArray, isCopyToAll, isAddedBySearch);
                }
            }
        });
    }
    else {
        if (crewsArray.length > 0) {
            AddCrewToSession(crewsArray, isCopyToAll, isAddedBySearch);
        }
    }
}
function BlockFlightHrsValidation(controlId, enteredval, legBlockORflightHrs, crewID) {
    if (!IsNullOrEmptyOrUndefined(enteredval)) {
        if (globalIdentity._fpSettings._TimeDisplayTenMin == Minute) {
            errMsg = ValidateTime(enteredval);
        }
        else
            errMsg = fnNumericValidation(enteredval, 2, 1);
        if (errMsg != "") {
            jAlert(errMsg, msgTitle, function (r) {
                if (!IsNullOrEmptyOrUndefined(legBlockORflightHrs))
                    $('#' + controlId).val(legBlockORflightHrs);
            });
            return false;
        }
        else {

            if (CheckBlockOrFlightHourOverride(crewID)) {
                $('#lbCrewAugmentMsg').text("Augmented Crew Leg!");
                $('#lbCrewAugmentMsg').attr('style', 'color:red !important');
                $('#lbCrewAugmentMsg').css('visibility', 'visible');
            }
            else
                $('#lbCrewAugmentMsg').text("").css('display', 'none');
            return true;
        }
    }
    else
        return false;
}
