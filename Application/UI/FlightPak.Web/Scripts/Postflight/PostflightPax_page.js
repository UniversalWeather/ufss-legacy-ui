﻿
//To display plane image at right place
var isPaxSummaryExpanded = true;
var isSIFLExpanded = true;
var isLogNotesExpanded = false;
var isradValueSet = false;
var agt = navigator.userAgent.toLowerCase();

$(document).ready(function () {

    if (getQuerystring("action","") == "recalcsifl") {        
        SiflRecalculateConfirmCall();        
    }

});


function pnlPaxSummaryCollapse() {
    isPaxSummaryExpanded = false;
}
function pnlPaxSummaryExpand() {
    isPaxSummaryExpanded = true;
}
function pnlSIFLCollapse() {
    isSIFLExpanded = false;
}
function pnlSIFLExpand() {
    isSIFLExpanded = true;
}
function pnlLogNotesCollapse() {
    isLogNotesExpanded = false;
}
function pnlLogNotesExpand() {
    isLogNotesExpanded = true;
}

function Init(){

    $("#tbSearchPAX").keypress(function (e) {
        if (e.keyCode == 13) {
            var paxCodes=$("#tbSearchPAX").val().split(',');
            AddPassenger(paxCodes,null,null);
            return false;
        }
    });

    $("#ddlFlightPurpose").on("change", function () {

        var SelectedValue = this.value;

        var paxids = $(jqgriddgPassenger).getGridParam("selarrrow");
        if (paxids.length > 0) {
            for (var i = 0; i < paxids.length; i++) {
                var rowData = jQuery(jqgriddgPassenger).getRowData(paxids[i]);
                var rowid = rowData["RowNumber"];
                var controls = $("select[rownumber=" + rowid + "]").val(SelectedValue);
            }
            $(this).val("0");
            Save_PaxInfo(true);
        } else {
            jAlert("Please select record.", alerttitle);
            $(this).val("0");
        }
    });
};

function PaxSearchClick() {
    var paxcds = $("#tbSearchPAX").val().split(',');
    AddPassenger(paxcds, null, null);
}
function GetPaxPurposeList(isAsync)
{
    if (isAsync == undefined)
        isAsync = false;
    //TripLogPaxSelectionLegs
    LegPaxCodeFlightPurposeList = {};
    LegDepartPercentageLegNumWise = {};
    


    $.ajax({
        cache: false,
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/TripLogPaxSelectionLegs',
        async: isAsync,
        contentType: 'application/json',
        success: function (result, textStatus, xhr) {
            var retResult = verifyReturnedResult(result);
            if (retResult == true && result.d.Success == true) {
                var rowData = result.d.Result;

                PaxSelectionLegs = rowData.results;
                PaxPurposeList = rowData.PurposeList;
                PassengerList = rowData.PassengerList;

                LegsisDeadCategoryList = {};
                var width = 0;
                if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1)
                    { width += 0; }

                $("#ddlFlightPurpose").find('option').remove();
                $("#ddlFlightPurpose").append("<option  value='0' >All Legs</option>");
                $("#ddlFlightPurpose").append("<option  value='1' >Select</option>");
                $.each(PaxPurposeList, function (index, PaxPurposeVale) {
                    $("#ddlFlightPurpose").append("<option  value='" + PaxPurposeVale + "' >" + index + "</option>");
                });
                $.each(PaxSelectionLegs, function (index, Leg) {

                    var LegNum = "Leg" + Leg.LegNUM;
                    var LegName = "Leg " + Leg.LegNUM + Leg.LegColumnTitle;
                    var LegPaxList = Leg.PaxFlightpurposeList;
                    var LegDepartPercentage = Leg.PaxDepartPercentage;
                    $(jqgriddgPassenger).setLabel(LegNum, LegName);
                    LegPaxCodeFlightPurposeList[LegNum] = LegPaxList;
                    LegDepartPercentageLegNumWise[LegNum] = LegDepartPercentage;

                    ColNames.push(LegName);
                    ColModel.push({
                        name: LegNum, title: false, index: LegNum, width: (140 + width), sortable: false, search: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue) {
                                if (cellvalue.indexOf("input") > 0)
                                { return cellvalue; }
                            }
                            var PaxList = LegPaxCodeFlightPurposeList[LegNum];

                            var fpKey = rowObject["PassengerRequestorID"] + rowObject["PassengerFirstName"];

                            var isDeadCategory = LegsisDeadCategoryList[LegNum];
                            var FlightPurposeID = "";
                            FlightPurposeID = PaxList[fpKey]

                            var DepartPercentage = "0";
                            var dHtml = "";
                            if (self.UserPrincipal._IsPODepartPercentage) {
                                DepartPercentage =ConvertNullToEmptyString(LegDepartPercentageLegNumWise[LegNum]["DepartPercentage" + fpKey]);
                                dHtml += " <span style='padding-left:5px'>  <input type='text' style='width:50px' title='Allowed values between 0 to 100' onkeypress='return fnAllowNumeric(this, event)' maxlength='3' value='" + DepartPercentage + "' rowcode='" + rowObject["PassengerRequestorCD"] + "'  row-id='" + rowObject["PassengerRequestorID"] + "' LegNUM='" + Leg.LegNUM + "' leg-id='" + LegNum + "'  id='txtDepartPercentage" + Leg.LegNUM + rowObject["PassengerRequestorID"] + "' onchange='return DepartPercentageChange(this);' PassengerFirstName='" + rowObject["PassengerFirstName"] + "' /> </span>";
                            }
                            var Html = "<select onchange='SaveOneFlightpurposeChange(this)'; PassengerFirstName='" + rowObject["PassengerFirstName"] + "'  rowcode='" + ConvertNullToEmptyString(rowObject["PassengerRequestorCD"]) + "'  row-id='" + rowObject["PassengerRequestorID"] + "' LegNUM='" + Leg.LegNUM + "' leg-id='" + LegNum + "'  id='ddlLeg" + Leg.LegNUM + rowObject["PassengerRequestorID"] + "'  rownumber='" + rowObject["RowNumber"] + "' >";
                            Html += "<option value='1'>Select</option>";
                            $.each(PaxPurposeList, function (index, PaxPurposeVale) {
                                if (FlightPurposeID == PaxPurposeVale) {
                                    Html += "<option selected='selected' value='" + PaxPurposeVale + "' >" + index + "</option>";
                                } else {
                                    Html += "<option  value='" + PaxPurposeVale + "' >" + index + "</option>";
                                }
                            });
                            Html += "</select>";
                            if (self.UserPrincipal._IsPODepartPercentage) {
                                Html += dHtml;
                            }
                            return Html;
                        }
                    });
                });

                
                if ($(jqgriddgPassenger)[0].grid) {
                    reloadPaxPurposeGrid();
                } else
                    InitializePaxPurposeGrid();

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function InitializePaxPurposeGrid()
{
        $(jqgriddgPassenger).jqGrid({
            datatype: "local",
            data: PassengerList.results,
            loadonce: true,
            multiselectWidth: 100,
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                return JSON.stringify(postData);
            },
            height: "200",
            width: "700",
            pager: "#pg_gridPagerPaxSelection",
            autowidth: false,
            shrinkToFit: false,
            multiselect: true,
            pgbuttons: false,
            pginput: false,
            pgtext: "",
            ignoreCase: true,
            colNames: ColNames,
            colModel: ColModel,
            onSelectRow: function (id, status) { },
            loadComplete: function () {
                $(jqgriddgPassenger).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });

                var gridid = jqgriddgPassenger.substr(1, jqgriddgPassenger.length);
                $("#jqgh_" + gridid + "_cb").find("span").remove();
                $("#jqgh_" + gridid + "_cb").find("br").remove();

                $("#jqgh_" + gridid + "_cb").prepend("<span>Select All</span><br/>");
                $("#jqgh_" + gridid + "_cb").css("height", "32px");
                $(jqgriddgPassenger + " [type=checkbox]").css("width", "100%");

                $(jqgriddgPassenger + "_cb").css("width", "50px");
                $(jqgriddgPassenger + "_cb").css("font-size", "10px");
                $(jqgriddgPassenger + " tbody tr").children().first("td").css("width", "50px");

                EnableDisableJqGridControls(self.POEditMode(), jqgriddgPassenger);
            }
        });

        $(jqgriddgPassenger).jqGrid('navGrid', '#pg_gridPagerPaxSelection', { resize: false, add: false, search: false, del: false, refresh: false, edit: false });
        $(jqgriddgPassenger).jqGrid('navButtonAdd', "#pg_gridPagerPaxSelection", {
            caption: "", title: "Delete Selected Pax(s)", buttonicon: "ui-icon-trash",
            onClickButton: function () {
                if (self.POEditMode()) {

                    var myGrid = $(jqgriddgPassenger);
                    var selRowIds = myGrid.jqGrid('getGridParam', 'selarrrow');

                    if (selRowIds.length < 1) {
                        jAlert("Select at least one Passenger!", alerttitle);
                        return
                    }
                    setAlertTextToYesNo();
                    jConfirm("Are you sure you want to delete this record?", "Confirmation", function (r) {

                        if (r) {
                            var fNameList = new Array();
                            var paxCDs = new Array();
                            for (var i = 0; i < selRowIds.length; i++) {
                                var rowData = jQuery(jqgriddgPassenger).getRowData(selRowIds[i]);
                                paxCDs.push($.trim(rowData["PassengerRequestorCD"]));
                                
                                if ($.trim(rowData["PassengerRequestorCD"]) == "") {
                                    fNameList.push($.trim(rowData["PaxName"]));                                    
                                }
                            }

                            
                            ShowJqGridLoader(jqgriddgPassenger);
                            var params = {};
                            params.passengerRequestorCDs = paxCDs;
                            params.firstNamelist = fNameList;


                            $.ajax({
                                type: 'POST',
                                dataType: 'json',
                                url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/DeletePax',
                                async: true,
                                data: JSON.stringify(params),
                                contentType: 'application/json',
                                success: function (result, textStatus, xhr) {
                                    var returnResult = verifyReturnedResult(result);
                                    if (returnResult == true && result.d.Success == true) {
                                        GetPaxPurposeList();
                                        reloadPaxPurposeGrid();
                                        reloadPaxsummaryRemoteCall();
                                        getTripExceptionGrid("reload");
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    HideJqGridLoader(jqgriddgPassenger);
                                    reportPostflightError(jqXHR, textStatus, errorThrown);
                                }
                            }).done(function () {
                                HideJqGridLoader(jqgriddgPassenger);
                            });


                        }

                    });
                    setAlertTextToOkCancel();
                    return;
                }
            }
        });
        $(jqgriddgPassenger).jqGrid('navButtonAdd', "#pg_gridPagerPaxSelection", {
            caption: "", title: "Insert Pax", buttonicon: "ui-icon-insert-file",
            onClickButton: function () {
                if (self.POEditMode()) {
                    ShowInsertPAXPopup();
                }
                return;
            }
        });
    }

    function PaxCodeValidationPostflight(LegNum, paxCode) {
        var btnTrans = document.getElementById("btnPax");
        if (!IsNullOrEmpty(paxCode)) {

            var paramData = {};
            paramData.LegNum = LegNum;
            paramData.paxCode = paxCode;
            paramData.SiflViewModel = ko.mapping.toJS(self.POSIFL);

            Start_Loading(btnTrans);
            $.ajax({
                async: true,
                cache: false,
                url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/Validate_PaxCode_and_SetProfileSettings",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(paramData),
                contentType: "application/json; charset=utf-8",
                success: function (result, textStatus, jqXHR) {
                    var retResult = verifyReturnedResult(result);
                    var jsonObj = result.d;
                    if (retResult == true && jsonObj.Success != undefined && jsonObj.Success == true) {
                        if (jsonObj.Result != undefined) {
                            ko.mapping.fromJS(jsonObj.Result, self.POSIFL);
                            if (self.POSIFL.Passenger1.AssocPaxID() != null || !IsNullOrEmpty(self.POSIFL.Passenger1.AssociatedWithCD())) {
                                self.POPaxSIFLAssociatePaxEditMode(true);
                            } else {
                                self.POPaxSIFLAssociatePaxEditMode(false);
                            }
                        }
                        End_Loading(btnTrans, true);
                    }
                    else if (jsonObj.Success == false && jsonObj.StatusCode == 401) {
                        jqXHR.status = jsonObj.StatusCode;
                        reportPostflightError(jqXHR, textStatus, null);
                    }
                    else {
                        if (jsonObj.ErrorsList.length > 0) {
                            ko.mapping.fromJS(jsonObj.Result, self.POSIFL);
                            self.POPaxSIFLAssociatePaxEditMode(false);
                            self.POSIFL.Passenger1.PassengerName("");
                            self.POSIFL.PassengerRequestorID(0);
                            self.POSIFL.Passenger1.PassengerRequestorID(0);

                            self.POSIFL.alertlbcvPaxName(jsonObj.ErrorsList[0]);
                        }
                        End_Loading(btnTrans, false);
                    }

                }

            });
        }
        else {

            //clear all fields        
            self.POSIFL.PassengerRequestorID(0);
            self.POSIFL.Passenger1.PassengerRequestorID(0);
            self.POSIFL.alertlbcvPaxName("");
            self.POSIFL.Passenger1.PassengerName("");
            self.POPaxSIFLAssociatePaxEditMode(false);
            
        }
    }

    function OnClientClosePaxInfoPopup(oWnd, args) {
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg.PaxPassport == undefined) {

                if (arg.PassengerRequestorCD.length > 0) {

                    if (document.getElementById('hdnInsert').value == "true") {

                        var rowId = $(jqgriddgPassenger).jqGrid('getGridParam', 'selrow');
                        var rowData = jQuery(jqgriddgPassenger).getRowData(rowId);
                        var orderIndex = rowData['OrderNUM'];
                        if (isNaN(orderIndex) == false) {
                            orderIndex = orderIndex - 1;
                        } else {
                            orderIndex = undefined;
                        }
                    var paxIds = arg.PassengerRequestorID.split(',');
                    AddPassenger(paxIds, orderIndex,true);
                        document.getElementById('hdnInsert').value = "false";
                    } else {
                    var paxIds = arg.PassengerRequestorID.split(',');
                    AddPassenger(paxIds,null,true);
                    }
                }
            } else if (arg.PaxPassport == true) {
                if (IsNullOrEmpty(arg.PassportNum) == false && self.POEditMode()==true) {

                    setAlertTextToYesNo();
                    jConfirm("Update Passport Number for All Passenger Legs?", alerttitle, function (confirmOption) {
                        // find rownumber of record to delete
                        if (confirmOption == true) {
                            UpdatePassportAsSelected(arg,true);
                        }else
                        {
                            UpdatePassportAsSelected(arg,false);
                        }
                    });
                    setAlertTextToOkCancel();
                    
                }                
            }
        }
    }

    function AddPassenger(paxCDs, insertposition,isPassengerIds) {
        if (!IsNullOrEmpty(paxCDs)) {
            if (insertposition == undefined)
                insertposition = null;
            if (isPassengerIds == undefined)
                isPassengerIds = false;

            var params = {};
            params.passengerRequestorCDs = paxCDs;
            params.InsertPosition = insertposition;
            params.IsPassengerIds = isPassengerIds;

            ShowJqGridLoader($(jqgriddgPassenger));
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/AddNewPax',
                async: true,
                data: JSON.stringify(params),
                contentType: 'application/json',
                success: function (data) {
                    var returnResult = verifyReturnedResult(data);
                    if (returnResult == true && data.d.Success == true && data.d.Result > 0) {
                        GetPaxPurposeList();
                        reloadPaxPurposeGrid();                        
                        getTripExceptionGrid("reload");
                    }

                    $("#tbSearchPAX").val("");

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    HideJqGridLoader($(jqgriddgPassenger));
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () {
                    HideJqGridLoader($(jqgriddgPassenger));
                    reloadPaxsummaryRemoteCall();
                }
            });

        }
    }

    function reloadPaxPurposeGrid() {        
            $(jqgriddgPassenger).jqGrid('clearGridData');
            $(jqgriddgPassenger).setGridParam({ data: PassengerList.results, datatype: 'local', page: 1 }).trigger('reloadGrid');
    }

    function SaveOneFlightpurposeChange(ctrl) {
        var params = {};
        params.PaxCode = $(ctrl).attr("rowcode");
        params.legNum = $(ctrl).attr("LegNUM");
        params.FlightPurposeID = $(ctrl).val();
        params.PassengerFirstName = $(ctrl).attr("PassengerFirstName");

        ShowJqGridLoader(jqgriddgPassenger);
        $.ajax({
            cache: false,
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/SaveFlightPurposeOfOnePaxByLeg',
            async: true,
            data: JSON.stringify(params),
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                HideJqGridLoader(jqgriddgPassenger);
                GetPaxPurposeList();
                reloadPaxPurposeGrid();
                reloadPaxsummaryRemoteCall();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                HideJqGridLoader(jqgriddgPassenger);
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function Save_PaxInfo(arg) {

        if (arg != undefined && arg == false) {
            return false;
        }

        ShowJqGridLoader(jqgriddgPassenger);
        //OrderNumbers();
        var dataPaxList = UpdateModel_Legs();
        var dataArray = $(jqgriddgPassenger).jqGrid('getGridParam', 'data');

        var myJsonString = "'" + JSON.stringify(dataPaxList) + "'";        
        var params = '{GridJson: ' + myJsonString + '}';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePaxGrid',
            async: true,
            data: params,
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {

                GetPaxPurposeList();
                reloadPaxPurposeGrid();
                reloadPaxsummaryRemoteCall();
                HideJqGridLoader(jqgriddgPassenger);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                HideJqGridLoader(jqgriddgPassenger);
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function OrderNumbers() {
        var myGrid = $(jqgriddgPassenger);
        var dataArray = myGrid.jqGrid('getGridParam', 'data');
        for (var i = 0; i < dataArray.length; i++) {
            dataArray[i].OrderNUM = i + 1;
            myGrid.jqGrid('setCell', dataArray[i].PassengerRequestorID, 'OrderNUM', i + 1);
        }
    }


    // Pax Summary Grid Related

    function InitializePaxSummaryGrid() {
        jQuery(jqGriddgPaxSummary).jqGrid({
            url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/PaxSummaryGrid',
            mtype: 'POST',
            datatype: "json",
            cache: false,
            async: true,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData._search = undefined;
                postData.nd = undefined;
                postData.size = undefined;
                postData.page = undefined;
                postData.sort = undefined;
                postData.dir = undefined;
                postData.filters = undefined;

                return JSON.stringify(postData);
            },
            height: 200,
            width: 700,
            autowidth: false,
            shrinkToFit: false,
            rowNum: 10000,
            multiselect: false,
            pgbuttons: false,
            viewrecords: false,
            pgtext: "",
            pginput: false,
            datatype: "json",
            pager: "pg_gridPagerdgPaxSummary",
            colNames: ['RowNumber', 'PassengerRequestorID', 'Code', "LName", "MName", 'Name', "Leg Info.", "Billing Info.", "Passport No.", "Issue Date", "Expiration", "Choice", "Nationality", 'Legnum'],
            colModel: [
                        { name: 'RowNumber', index: 'RowNumber', hidden: true, sortable: false },
                        { name: 'PassengerRequestorID', index: 'PassengerRequestorID', key: true, hidden: true, sortable: false },
                        {
                            name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: 40, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                                return '<span ID="lnkPaxCode" >' + cellvalue + '</span>';
                            }
                        },
                        { name: 'PassengerLastName', index: 'PassengerLastName', width: 150, sortable: false, hidden: true, search: false },
                        { name: 'PassengerMiddleName', index: 'PassengerMiddleName', width: 150, sortable: false, hidden: true, search: false },
                       {
                           name: 'PassengerFirstName', index: 'PassengerFirstName', width: 180, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                               return GetValidatedValue(rowObject, "PassengerLastName") + ", " + cellvalue + " " + GetValidatedValue(rowObject, "PassengerMiddleName");
                           }
                       },
                       { name: 'LegDescription', index: 'LegDescription', width: 120, sortable: false, search: false },
                       {
                           name: 'Billing', index: 'Billing', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                               var onchange = "onchange='UpdatePaxSummaryBillingInfo(this);'";
                               var value = "";
                               if (cellvalue) {
                                   value = cellvalue;
                               }
                               return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"   ' + onchange + '  value="' + value + '" row-id="' + rowObject["PassengerRequestorID"] + '" leg-no="' + rowObject["LegNUM"] + '" col="Billing" />';
                           }
                       },
                       {
                           name: 'Passport', index: 'Passport', width: 150, sortable: false, classes: "text_centeralignment", search: false, formatter: function (cellvalue, options, rowObject) {                                                              
                               var ShowPaxPassport = "return ShowPaxPassportPopup('" + rowObject["PassengerRequestorID"] + "','" + rowObject["LegNUM"] + "','" + (rowObject["PassengerName"] + "").replace(/'/g, "\\'") + "');";
                               var Content = "";
                               var Content = '<div style="width:100%;"> <span style="font-family:Arial;font-size:12px;width:80%;word-wrap:break-word;display:inline-block;"  class="alwaysdisabled aspNetDisabled tdLabel70" id="tbPassport' + rowObject["PassengerRequestorID"] + rowObject["LegNUM"] + '" >' + ((rowObject["PassportNUM"] == null) ? '' : rowObject["PassportNUM"]) + ' </span>';
                               Content = Content + '<input type="hidden" id="hdnVisaID" value="' + rowObject["VisaID"] + '">';
                               Content = Content + '<input type="hidden" id="hdnPassID" value="' + rowObject["PassengerRequestorID"] + '">';
                               Content = Content + '<input type="hidden" id="hdnPassNum" value="' + rowObject["PassportNUM"] + '">';
                               Content = Content + '<input type="hidden" id="hdnLegNum" value="' + rowObject["LegNUM"] + '">';
                               Content = Content + '<a style="display:inline-block;height:16px;width:16px;float:right" class="browse-button" id="lnkPassport" onclick="' + ShowPaxPassport + '" data-bind="enable: EditMode,css: BrowseBtn"></a> </div>';
                               return Content;
                           }
                       },
                       { name: 'IssueDT', index: 'IssueDT', width: 80, sortable: false, search: false },
                       { name: 'PassportExpiryDT', index: 'PassportExpiryDT', width: 80, sortable: false, search: false },                        
                       { name: 'Choice', index: 'Choice', width: 40, sortable: true, formatter: "checkbox", classes: "text_centeralignment", editoptions: { value: "Enabled:Disabled" } },
                       { name: 'Nation', index: 'Nation', width: 60, sortable: false, search: false },
                       { name: 'LegNUM', index: 'LegNUM', width: 60, sortable: true, search: false, hidden: true, firstsortorder: 'asc' }

            ],
            loadComplete: function (rowData) {
                EnableDisableJqGridControls(self.POEditMode(), jqGriddgPaxSummary);
                var jqGridId = jqGriddgPaxSummary;
                if (jqGridId.substr(0, 1) == "#") {
                    jqGridId = jqGridId.substr(1);
                }
                $("#gbox_" + jqGridId + " input[type='checkbox']").attr("disabled", "disabled");
                MessageWhileBlankJqGrid(jqGriddgPaxSummary);

            }
        });
    }

    function reloadPaxsummaryRemoteCall() {
        if ($(jqGriddgPaxSummary)[0].grid) {
            $(jqGriddgPaxSummary).jqGrid('GridUnload');
        }
        InitializePaxSummaryGrid();
        reloadPersonalSIFLGrid();
    }

    function UpdatePaxSummaryBillingInfo(ctrl) {

        var legnum = $(ctrl).attr("leg-no");
        var PassengerRequestorID = $(ctrl).attr("row-id");
        var billingInfo = $(ctrl).val();

        //SavePaxSummaryBillingUpdate
        var params = { passengerRequstorID: PassengerRequestorID, legNum: legnum, billingInfo: billingInfo };
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePaxSummaryBillingUpdate',
            async: true,
            data: JSON.stringify(params),
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                var retResult = verifyReturnedResult(rowData);
                if (retResult == true && rowData.d.Success == false) {
                    if (rowData.d.StatusCode == 401)
                        reportPostflightError(jqXHR, textStatus, null);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function UpdateModel_Legs() {
        var listPax = new Array();
        $(jqgriddgPassenger).find('select').each(function () {

            var codeow = $(this).attr("rowcode");
            var rowid = $(this).attr("id").replace("ddl", "");
            var columnName = $(this).attr("leg-id");
            var value = $(this).val();

            var Passenger = {};
            Passenger.FlightPurposeID = value;
            Passenger.PassengerRequestorCD = codeow;
            Passenger.Leg = columnName;
            Passenger.PassengerFirstName = $(this).attr("PassengerFirstName");

            listPax.push(Passenger);
            //var rowData = $(jqgriddgPassenger).jqGrid('getRowData', codeow);
            //rowData[columnName] = value;
            //$(jqgriddgPassenger).jqGrid('setRowData', codeow, rowData);
        });

        return listPax;
    }

    // Pax SIFL panel part

    function InitializeSIFLPersonalGrid() {
        jQuery(jqgriddgSIFLPersonal).jqGrid({
            url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/InitializePaxSIFLGrid',
            mtype: 'POST',
            datatype: "json",
            cache: false,
            async: true,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData._search = undefined;
                postData.nd = undefined;
                postData.size = undefined;
                postData.page = undefined;
                postData.sort = undefined;
                postData.dir = undefined;
                postData.filters = undefined;

                return JSON.stringify(postData);
            },
            height: 200,
            width: 340,
            autowidth: false,
            shrinkToFit: false,
            multiselect: false,
            pgbuttons: false,
            viewrecords: false,
            pgtext: "",
            pginput: false,
            datatype: "json",
            pager: "pg_gridPagerSIFL",
            colNames: ["RowNumber", "Depart", "Arrive", "Personal", "L/F"],
            colModel: [
                        { name: 'RowNumber', index: 'RowNumber', hidden: true, key: true, sortable: false, width: 80 },
                        { name: 'Depart', index: 'Depart', sortable: false, width: 80, classes: "text_centeralignment" },
                        { name: 'Arrive', index: 'Arrive', sortable: false, width: 80, classes: "text_centeralignment" },
                        { name: 'Personal', index: 'Personal', sortable: false, width: 50, classes: "text_centeralignment" },
                        { name: 'LoadFactor', index: 'LoadFactor', sortable: false, width: 70, classes: "text_centeralignment" }
            ],
            loadComplete: function (rowData) {
                EnableDisableJqGridControls(self.POEditMode(), jqgriddgSIFLPersonal);
                MessageWhileBlankJqGrid(jqgriddgSIFLPersonal);
            }
        });
    }

    function reloadPersonalSIFLGrid() {
        if ($(jqgriddgSIFLPersonal)[0].grid) {
            $(jqgriddgSIFLPersonal).jqGrid('GridUnload');
        }

        InitializeSIFLPersonalGrid();
    }

    function InitializeSIFLGrid() {
        jQuery(jqGriddgSIFL).jqGrid({
            datatype: "local",
            data: SIFLLegsListDataSource,
            loadonce: true,
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                return JSON.stringify(postData);
            },
            height: "200",
            width: "730",
            autowidth: false,
            shrinkToFit: false,
            multiselect: false,
            pgbuttons: false,
            pginput: false,
            pager: "pg_gridPagerdgSIFL",
            colNames: ["PAX", "PassengerRequestorID", "C/N/G", "Depart", "Arrive", "L/F", "Statute", "T.Charge", "SIFL Total", "POSIFLID", "RowNumber"],
            colModel: [
                        { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', sortable: false, width: 40 },
                        { name: 'PassengerRequestorID', index: 'PassengerRequestorID', sortable: false, width: 80, hidden: true },
                        { name: 'EmployeeTYPE', index: 'EmployeeTYPE', sortable: false, classes: "text_centeralignment", width: 50 },
                        { name: 'DepartAirport', index: 'Depart', sortable: false, classes: "text_centeralignment", width: 80, formatter: function (cellvalue, options, rowObject) { return GetValidatedValue(cellvalue, "IcaoID"); } },
                        { name: 'ArriveAirport', index: 'Arrive', sortable: false, classes: "text_centeralignment", width: 80, formatter: function (cellvalue, options, rowObject) { return GetValidatedValue(cellvalue, "IcaoID"); } },
                        { name: 'LoadFactor', index: 'LoadFactor', sortable: false, classes: "text_centeralignment", width: 70, formatter: 'number', formatoptions: { decimalSeparator: ".", decimalPlaces: 1, defaultValue: '0.0' } },
                        { name: 'Distance', index: 'Distance', sortable: false, classes: "text_centeralignment", width: 90 },
                        { name: 'TeminalCharge', index: 'TeminalCharge',classes: "text_centeralignment", sortable: false, width: 90, formatter: currencyFormatter, formatoptions: { decimalPlaces: 2, prefix: "$" } },
                        { name: 'SIFLTotal', index: 'SIFLTotal',classes: "text_centeralignment", sortable: false, width: 110, formatter: currencyFormatter, formatoptions: { decimalPlaces: 2, prefix: "$" } },
                        { name: 'POSIFLID', index: 'POSIFLID', sortable: false, width: 70, hidden: true },
                        { name: 'RowNumber', index: 'RowNumber', sortable: false, width: 70, hidden: true, key: true }

            ],
            onSelectRow: function (id, status) {
                var rowData = jQuery(jqGriddgSIFL).jqGrid('getRowData', id);
                for (var i = 0; i < SIFLLegsListDataSource.length; i++) {
                    if (SIFLLegsListDataSource[i].RowNumber == rowData["RowNumber"]) {
                        ko.mapping.fromJS(SIFLLegsListDataSource[i], self.POSIFL);
                        self.POPaxSIFLControlEditMode(true);
                    }
                }
                if (SIFLLegsListDataSource.length == null || SIFLLegsListDataSource.length == 0) {
                    ko.mapping.fromJS(SIFLLegsListDataSource[i], self.POSIFL);
                }

            },
            loadComplete: function (rowData) {
                var ids = jQuery(jqGriddgSIFL).jqGrid("getDataIDs");
                if (ids.length>0)
                    jQuery(jqGriddgSIFL).jqGrid("setSelection", ids[0]);

                if (ids.length == 0) {
                    resetKOViewModel(self.POSIFL);
                    resetKOViewModel(self.POSIFL.Passenger1);
                    resetKOViewModel(self.POSIFL.DepartAirport);
                    resetKOViewModel(self.POSIFL.ArriveAirport);
                }

                EnableDisableJqGridControls(self.POEditMode(), jqGriddgSIFL);
                MessageWhileBlankJqGrid(jqGriddgSIFL);
            }
        });
    }

    function currencyFormatter(cellValue, options, rowObject) {

        if (cellValue != null) {
            return $.fn.fmatter.call(this, "currency", cellValue, options)
        }
        else
            return "";
    }

    
    function reloadSIFLLegsGrid() {
        if ($(jqGriddgSIFL)[0].grid) {
            $(jqGriddgSIFL).jqGrid('GridUnload');
        }
        
        InitializeSIFLGrid();
    }

    function GetSIFLLegsList(updateClick) {
        var paramters = {};
        if (updateClick == undefined)
            updateClick = false;

        paramters.isUpdateClick = updateClick;
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/GetListOfSIFLLegs",
            data: JSON.stringify(paramters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                if (response.d.Success == true && retResult == true) {
                    SIFLLegsListDataSource = {};
                    SIFLLegsListDataSource = response.d.Result;                    
                    reloadSIFLLegsGrid();                   
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function SiflIcao_Validation(isDepartIcao) {
        //Validation_SIFLIcao_KO
        if (isDepartIcao == true && IsNullOrEmpty(self.POSIFL.DepartAirport.IcaoID())) {
            resetKOViewModel(self.POSIFL.DepartAirport, ["AirportID"]);
            self.POSIFL.DepartICAOID(null);
            $("#lbcvDeparts").html("");
            return;
        } else if (isDepartIcao == false && IsNullOrEmpty(self.POSIFL.ArriveAirport.IcaoID())) {
            resetKOViewModel(self.POSIFL.ArriveAirport, ["AirportID"]);
            self.POSIFL.ArriveICAOID(null);
            $("#lbcvArrives").html("");
            return;
        }
        var btn = {};
        if (isDepartIcao)
            btn = document.getElementById("btnDeparts");
        else
            btn = document.getElementById("btnArrives");


        Start_Loading(btn);

        var paramters = {};
        paramters.siflViewmodel = ko.mapping.toJS(self.POSIFL);
        paramters.isDepartIcao = isDepartIcao;
        
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/Validation_SIFLIcao_KO",
            data: JSON.stringify(paramters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                if (response.d.Success == true && retResult == true) {

                    ko.mapping.fromJS(response.d.Result, self.POSIFL);
                    if (response.d.ErrorsList.length > 0) {
                        if (isDepartIcao)
                            $("#lbcvDeparts").html(response.d.ErrorsList[0]);
                        else
                            $("#lbcvArrives").html(response.d.ErrorsList[0]);

                        End_Loading(btn, false);
                    } else {
                        if (isDepartIcao)
                            $("#lbcvDeparts").html("");
                        else
                            $("#lbcvArrives").html("");

                        End_Loading(btn, true);
                    }
                }
                else if (response.d.StatusCode == 401 && response.d.Success == false) {
                    var newjqXHR = $.extend(true, {}, jqXHR);
                    newjqXHR.status = response.d.StatusCode;
                    reportPostflightError(newjqXHR, textStatus, null);
                    End_Loading(btn, false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
                End_Loading(btn, false);
            }
        });
    }


    function SIFLDistanceCalculation() {
        ShowJqGridLoader(jqGriddgSIFL);
        var paramters = {};
        paramters.SIFLViewModel = ko.mapping.toJS(self.POSIFL);

        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SIFLDistanceCalculation",
            data: JSON.stringify(paramters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                if (response.d.Success == true && retResult == true) {                    
                    ko.mapping.fromJS(response.d.Result, self.POSIFL);
                    self.POPaxSIFLAssociatePaxEditMode(false);
                    if($.trim(self.POSIFL.EmployeeTYPE())=="G")
                    {
                        jAlert("Must Be Associated With Employee.", "Postflight");
                        self.POPaxSIFLAssociatePaxEditMode(true);
                    } else {
                        self.POSIFL.AssocPassenger.PassengerName("");
                        self.POSIFL.AssocPassenger.AssociatedWithCD("");
                        self.POSIFL.AssociatedPassengerID(null);
                        document.getElementById("lbcvAssoc").innerHTML = "";
                    }
                }
                else if (response.d.StatusCode == 401 && response.d.Success == false) {
                    var newjqXHR = $.extend(true, {}, jqXHR);
                    newjqXHR.status = response.d.StatusCode;
                    reportPostflightError(newjqXHR, textStatus, null);
                }
                HideJqGridLoader(jqGriddgSIFL);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
                HideJqGridLoader(jqGriddgSIFL);
            }
        });


    }


    function Update_SiflClick() {

        var errorMsg = "";

        if (IsNullOrEmpty(self.POSIFL.Passenger1.PassengerRequestorCD()))
            errorMsg = "Passenger Code is Required. <br/>";

        if (IsNullOrEmpty(self.POSIFL.DepartICAOID()))
            errorMsg += "Depart ICAO Not In Trip. <br/>";

        if (IsNullOrEmpty(self.POSIFL.ArriveICAOID()))
            errorMsg += "Arrival ICAO Not In Trip.";

        if (!IsNullOrEmpty(errorMsg)) {
            jAlert(errorMsg, alerttitle);
            return;
        } else {
            SaveSiflChange();            
        }
    }


    function DeleteSifl_LegClick() {
        // Check is any row  selected ?
        var selectedRowId = jQuery(jqGriddgSIFL).jqGrid('getGridParam', 'selrow');
        if (selectedRowId == null) {
            jAlert("Select atleast one SIFL Leg!", alerttitle);
            return;
        }
        setAlertTextToYesNo();
        jConfirm("Delete SIFL Leg", alerttitle, function (arg) {

            // confirm about delete

            // find rownumber of record to delete
            if (arg == true) {
                var paramters = {};
                paramters.siflViewModel = ko.mapping.toJS(self.POSIFL);

                $.ajax({
                    async: true,
                    type: "POST",
                    url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/Delete_Sifl_Leg",
                    data: JSON.stringify(paramters),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response, textStatus, jqXHR) {
                        var retResult = verifyReturnedResult(response);
                        if (response.d.Success == true && retResult == true) {
                            self.POPaxSIFLControlEditMode(false);
                            GetSIFLLegsList();
                        }
                        else if (response.d.StatusCode == 401 && response.d.Success == false) {
                            var newjqXHR = $.extend(true, {}, jqXHR);
                            newjqXHR.status = response.d.StatusCode;
                            reportPostflightError(newjqXHR, textStatus, null);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reportPostflightError(jqXHR, textStatus, errorThrown);
                    }
                });
            }
        });
        setAlertTextToOkCancel();


    }

    function Recalculate_SiflLeg_Click() {
        setAlertTextToYesNo();
        jConfirm("The Recalc Process Will Delete All Existing SIFL Records For This Trip Before Creating New Ones. Continue With The Recalc Process ?", alerttitle, function (arg) {
            if (arg == true) {
                ClearAllErrorLabels(SiflErrorLabels);
                var paramters = {};
                //paramters.siflViewModel = ko.mapping.toJS(self.POSIFL);
                RequestStart();
                $.ajax({
                    async: true,
                    type: "POST",
                    url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/Recalculate_SiflClick",
                    data: JSON.stringify(paramters),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response, textStatus, jqXHR) {
                        var retResult = verifyReturnedResult(response);
                        if (retResult == true && response.d.Success == true) {
                            if (response.d.ErrorsList.length > 0) {
                                jAlert(response.d.ErrorsList[0], alerttitle);
                            }

                            SIFLLegsListDataSource = response.d.Result;
                            reloadSIFLLegsGrid();

                        } else if (response.d.Success == false) {
                            SIFLLegsListDataSource = {};
                            reloadSIFLLegsGrid();
                            if (response.d.ErrorsList.length > 0) {
                                jAlert(response.d.ErrorsList[0], alerttitle);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reportPostflightError(jqXHR, textStatus, errorThrown);
                    },
                    complete: function () { ResponseEnd(); }
                });

            }
        });
        setAlertTextToOkCancel();
    }


    function downloadURL(url, count) {
        var hiddenIFrameID = 'hiddenDownloader' + count++;
        var iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = url;
    }

    function DepartPercentageChange(ctrl) {
        var newValue = parseInt(ctrl.value);
        if (newValue > 100  || isNaN(newValue)==true) {
            ctrl.value = 0;
        }
        newValue = parseInt(ctrl.value);
        var legnum = $(ctrl).attr("LegNUM");
        var rowid = $(ctrl).attr("rowcode");
        var flightpurposeid = parseInt($("#ddlLeg" + legnum + $(ctrl).attr("row-id")));
        if (flightpurposeid <= 1) {
            ctrl.value = 0;
            return;
        }
        // (int legNum,string PaxCode, int departPercentage)
        var parameters = {};
        parameters.legNum = legnum;
        parameters.PaxCode = $(ctrl).attr("rowcode");
        parameters.departPercentage = newValue;
        parameters.PassengerFirstName = $(ctrl).attr("PassengerFirstName");
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePaxLegDepartPercentage",
            data: JSON.stringify(parameters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });


    }


    function CheckDeptPercentage(callBack) {
        //CheckDeptPercentage

        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/CheckDeptPercentage",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                var data = response.d;
                if (retResult == true && data.Success == true && data.Result == true) {
                    IsSaveAllowFromPax = true;
                    callBack();
                }
                else if (data.Result == false && data.Success == false) {
                    if (data.ErrorsList.length > 0) {
                        var alerthtml = "";
                        for (var i = 0; i < data.ErrorsList.length; i++) {
                            alerthtml += htmlDecode(data.ErrorsList[i] + " \n");
                        }
                        IsSaveAllowFromPax = false;
                        jAlert(alerthtml, alerttitle);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });

    }

    //this function is used to navigate to pop up screen's with the selected code
    function openWin(radWin) {
        var url = '';
        if (radWin == "RadPassengerAssociatePopup") {
            url = '/Views/Settings/People/PassengerAssociatePopup.aspx?PaxCode=' + ConvertNullToEmptyString(self.POSIFL.AssocPassenger.AssociatedWithCD()) + "&clientid=" + ConvertNullToEmptyString(self.Postflight.PostflightMain.ClientID());
        }
        else if (radWin == "RadSelectDepartPopup") {
            url = '/Views/Transactions/PostFlight/PostFlightSIFLAirport.aspx?flag=Depart';
        }
        else if (radWin == "RadSelectArrivalPopup") {
            url = '/Views/Transactions/PostFlight/PostFlightSIFLAirport.aspx?flag=Arrival';
        }
        else if (radWin == "RadSelectPassengerPopup") {
            url = '/Views/Transactions/PostFlight/PostFlightSelectPassenger.aspx?rebind=1';
        }
        var oWnd = radopen(url, radWin);
    }



    // this function is used to display the value of selected Passenger code from popup
    function PassengerAssociatePopupClose(oWnd, args) {
        
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                var btn = document.getElementById("btnAssocPax");
                Start_Loading(btn);

                document.getElementById("lbcvAssoc").innerHTML = "";
                self.POSIFL.AssociatedPassengerID(arg.PassengerRequestorID);
                self.POSIFL.AssocPassenger.AssociatedWithCD(arg.PassengerRequestorCD);

                if (arg.PassengerName != null)
                    self.POSIFL.AssocPassenger.PassengerName(arg.PassengerName);
                End_Loading(btn, true);
            }
            else {
                self.POSIFL.AssocPassenger.AssociatedWithCD("");
                self.POSIFL.AssociatedPassengerID("");
                self.POSIFL.AssocPassenger.PassengerName("");
                document.getElementById("lbcvAssoc").innerHTML = "";
                
            }
        }
    }

    //this function is used to display the value of selected Depart Airport code from popup
    function SelectDepartPopupClose(oWnd, args) {

        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("lbcvDeparts").innerHTML = "";
                self.POSIFL.DepartAirport.IcaoID(arg.ICAOID);
                self.POSIFL.DepartAirport.AirportID(arg.AirportID);
                self.POSIFL.DepartICAOID(arg.AirportID);

                if (arg.AirportName != null) {
                    self.POSIFL.DepartAirport.AirportName(arg.AirportName);
                }
            }
            else {
                self.POSIFL.DepartAirport.AirportName("");
                document.getElementById("lbcvDeparts").innerHTML = "";
                self.POSIFL.DepartAirport.IcaoID("");
                self.POSIFL.DepartAirport.AirportID("");
                self.POSIFL.DepartICAOID("");
                
            }
            self.siflDepartChange();
        }
    }

    //this function is used to display the value of selected Arrive Airport code from popup
    function SelectArrivalPopupClose(oWnd, args) {
        
        //get the transferred arguments
        var arg = args.get_argument();
        if (arg !== null) {
            if (arg) {
                document.getElementById("lbcvArrives").innerHTML = "";
                self.POSIFL.ArriveAirport.IcaoID(arg.ICAOID);
                self.POSIFL.ArriveAirport.AirportID(arg.AirportID);
                self.POSIFL.ArriveICAOID(arg.AirportID);
                if (arg.AirportName != null) {
                    self.POSIFL.ArriveAirport.AirportName(arg.AirportName);
                }
            }
            else {
                self.POSIFL.ArriveAirport.IcaoID("");
                document.getElementById("lbcvArrives").innerHTML = "";
                self.POSIFL.ArriveAirport.AirportName("");
                self.POSIFL.ArriveAirport.AirportID(null);
                self.POSIFL.ArriveICAOID("");
                
            }
            self.siflArriveChange();
        }
    }

    //this function is used to display the value of selected metro code from popup
    function SelectPassengerPopupClose(oWnd, args) {
        try {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg !== null) {
                if (arg) {
                    self.POSIFL.PassengerRequestorCD(arg.PassengerRequestorCD);
                    //document.getElementById("tbPax").value = arg.PassengerRequestorCD;
                    self.POSIFL.Passenger1.PassengerRequestorID(arg.PassengerRequestorID);
                    self.POSIFL.PassengerRequestorID(arg.PassengerRequestorID);
                    document.getElementById("hdnPax").value = arg.PassengerRequestorID;
                    if (arg.PassengerName != null) {
                        self.POSIFL.Passenger1.PassengerName(arg.PassengerName);
                    }
                    if (arg.IsEmployeeType != null) {
                        self.POSIFL.EmployeeTYPE(arg.IsEmployeeType);
                    }
                    PaxCodeValidationPostflight(1, self.POSIFL.PassengerRequestorCD());
                }
                else {
                    document.getElementById("tbPax").value = "";
                    document.getElementById("hdnPax").value = "";
                    self.POSIFL.Passenger1.PassengerRequestorID(null);
                    self.POSIFL.Passenger1.PassengerName("");
                }

            }
        }
        catch (e) {
            //alert(e.Description);
        }
    }

    // this function is used to display the Passenger popup 
    function ShowPAXPopup(arg) {
        document.getElementById('hdnInsert').value = "false";
        var oWnd = radopen("/Views/Transactions/PreFlight/PaxInfoPopupJqgrid.aspx?IsUIReports=true", "radPaxInfoPopup"); // Added for Postflight PAX Tab
        return false;
    }

    function ShowInsertPAXPopup() {
        document.getElementById('hdnInsert').value = "true";
        var oWnd = radopen("/Views/Transactions/PreFlight/PaxInfoPopupJqgrid.aspx?IsUIReports=true", "radPaxInfoPopup"); // Added for Postflight PAX Tab
        return false;
    }

    // this function is used to get the dimensions
    function GetDimensions(sender, args) {
        var bounds = sender.getWindowBounds();
        return;
    }

    //this function is used to resize the pop-up window
    function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
        return oWindow;
    }

    //this function is used to replace default value when textbox is empty
    function validateEmptyMilesTextbox(ctrlID, e) {
        if (ctrlID.value == "") {
            ctrlID.value = "0";
        }
    }

    //this function is used to replace default value when textbox is empty
    function validateEmptyMultiplerTextbox(ctrlID, e) {
        if (ctrlID.value == "") {
            ctrlID.value = "0.0";
        }
    }

    //this function is used to replace default value when textbox is empty
    function validateEmptyTerminalTextbox(ctrlID, e) {
        if (ctrlID.value == "") {
            ctrlID.value = "0.00";
        }
    }

    //this function is used to replace default value when textbox is empty
    function validateEmptyRateTextbox(ctrlID, e) {
        if (ctrlID.value == "") {
            ctrlID.value = "0.0000";
        }
    }

    // this function is used to display the Pax passport visa type popup 
    function ShowPaxPassportPopup(Paxid, PaxPaxLegID, PaxName) {
        window.radopen("/Views/Transactions/PostFlight/PaxPassportVisa.aspx?PassengerRequestorID=" + Paxid + "&PaxName=" + PaxName, "radPaxInfoPopup");
        document.getElementById("hdnPasspaxID").value = Paxid;
        document.getElementById("hdnPassPaxLegID").value = PaxPaxLegID;
        return false;
    }


    function SiflRecalculateConfirmCall(fromPaxTab)
    {
        if (fromPaxTab!=undefined && fromPaxTab == true) {
            ajaxSiflRecalculateCall();
        } else {

            setAlertTextToYesNo();
            jConfirm("The Recalc Process Will Delete All Existing SIFL Records For This Trip Before Creating New Ones. Continue With The Recalc Process ?", alerttitle, function (arg) {
                if (arg == true) {
                    ajaxSiflRecalculateCall();
                } else {
                    self.SavePostflightLog_Click();
                }
            });
            setAlertTextToOkCancel();
        }
    }
    function ajaxSiflRecalculateCall()
    {
        var paramters = {};        
        RequestStart();
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/Recalculate_SiflClick",
            data: JSON.stringify(paramters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                if (retResult == true && response.d.Success == true) {
                    self.SavePostflightLog_Click();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { }
        });
    }

    function UpdatePassportAsSelected(obj,updateforalllegs)
    {
        ///UpdatePassportAsSelected
        var paramters = {};
        paramters.updateForAllLegs = updateforalllegs;
        paramters.legNum = document.getElementById("hdnPassPaxLegID").value;
        paramters.selectedPaxID = obj.PassengerRequestorID;
        paramters.passportId = obj.PassportID;

        RequestStart();
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/UpdatePassportAsSelected",
            data: JSON.stringify(paramters),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                if (retResult == true && response.d.Success == true) {
                    reloadPaxsummaryRemoteCall();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { ResponseEnd(); }
        });
    }
