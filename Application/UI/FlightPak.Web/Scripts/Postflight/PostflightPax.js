﻿
var IsSaveAllowFromPax = false;
var SiflErrorLabels = ["lbcvPaxName", "lbcvAssoc", "lbcvDeparts", "lbcvArrives"];
var jqGriddgPaxSummary = "#dgPaxSummary";
var jqgriddgPassenger = "#dgPassenger";
var jqgriddgSIFLPersonal = "#dgSIFLPersonal";
var jqGriddgSIFL = "#dgSIFL";
var SIFLLegsListDataSource = new Array();
var alerttitle = "Flight Log Manager - Passenger"
var PassengerList = {}, LegPaxCodeFlightPurposeList = {}, LegDepartPercentageLegNumWise = {};
var ColNames = ['RowNumber', 'Code', 'Name', 'OrderNUM', 'PassengerRequestorID', 'Notes', 'PassengerAlert', 'BillingCode'];
var ColModel = [
            { name: 'RowNumber', index: 'RowNumber', width: 60, frozen: true, hidden: true, key: true },
            {
                name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: 40, frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                    return "<a style='cursor: pointer;' onclick='ShowPaxDetailsPopup(" + rowObject.PassengerRequestorID + ");' > " + htmlEncode(cellvalue) + "</a>";
                },
                unformat: function (cellvalue, options, cell)
                {
                    return cellvalue;
                }
            },
            {
                name: 'PaxName', index: 'PassengerLastName', width: 180, frozen: true, sortable: true, search: false, formatter: function (cellvalue, options, rowObject) {
                    var lname = GetValidatedValue(rowObject, "PassengerLastName");
                    if (lname != "" && lname!=undefined)
                        lname = lname + ", ";

                    return lname + GetValidatedValue(rowObject, "PassengerFirstName") + " " + GetValidatedValue(rowObject, "PassengerMiddleName");
                }
            },
            { name: 'OrderNUM', index: 'OrderNUM', width: 100, sortable: false, search: false, hidden: true },
            { name: 'PassengerRequestorID', index: 'PassengerRequestorID', width: 55, sortable: false, search: false, hidden: true },
            { name: 'Notes', index: 'Notes', width: 55, sortable: false, search: false, hidden: true },
            { name: 'PassengerAlert', index: 'PassengerAlert', width: 55, sortable: false, search: false, hidden: true },
            { name: 'Billing', index: 'Billing', width: 55, sortable: false, search: false, hidden: true }
];
var PostflightPaxViewModel = function () {

    self.PostflightPax = {};
    self.POPaxSIFLControlEditMode = ko.observable(false);
    self.POPaxSIFLAssociatePaxEditMode = ko.observable(false);

    self.POPaxSIFLEditMode = ko.computed(function () {
        return (self.POEditMode() && self.POPaxSIFLControlEditMode());
    }, this);

    self.CssPOPaxSIFLUpdateButton = ko.computed(function () {
        return self.POPaxSIFLEditMode() == true ? "button" : "button-disable";
    }, self);

    self.BrowseBtnInSIFL = ko.pureComputed(function () {
        return self.POPaxSIFLEditMode() ? "browse-button" : "browse-button-disabled";
    }, self);

    self.BrowseBtnPOPaxSIFLAssociatePax = ko.computed(function () {
        return self.POPaxSIFLAssociatePaxEditMode() ? "browse-button" : "browse-button-disabled";
    }, self);


    
    self.AddSIFLLegClick = function () { AddSIFLLeg_Click(); };

    self.SIFLPaxCodeChangeEvent = function () {
        PaxCodeValidationPostflight(1, document.getElementById("tbPax").value);
    };
    self.siflDepartChange = function () { return SiflIcao_Validation(true); };
    self.siflArriveChange = function () { return SiflIcao_Validation(false); };
    self.siflDistanceChange = function () {        
        SIFLDistanceCalculation();
        return true;
    };

    self.siflDistanceChangeOnTerminalCharge = function () {

        self.POSIFL.TeminalCharge($("#tbTerminalCharge").val());
        SIFLDistanceCalculation();

    };


    self.POEditMode.extend({ notify: 'always' });
    self.POEditMode.subscribe(function (newValue) {
        EnableDisableJqGridControls(newValue, jqgriddgPassenger);
        EnableDisableJqGridControls(newValue, jqGriddgPaxSummary);
        EnableDisableJqGridControls(newValue, jqGriddgSIFL);
    });

    self.AssocPaxChange = function () {
        var btn = document.getElementById("btnAssocPax");
        var errorLabel = "#lbcvAssoc";
        var assocId = self.POSIFL.AssociatedPassengerID;
        AssociatePassengerCD_Retrieve_KO(self.POSIFL.AssocPassenger, assocId, errorLabel, btn);
    };

    PassengerList.results = new Array();
    var paxHtmlSchema = htmlDecode(document.getElementById("hdnPostflightPaxInitializationObject").value);
    if (!IsNullOrEmpty(paxHtmlSchema)) {
        var jsonSchema = JSON.parse(paxHtmlSchema);
        self.PostflightPax = ko.mapping.fromJS(jsonSchema.PaxViewModel);

        self.POSIFL = ko.mapping.fromJS(jsonSchema.PaxSIFLViewModel);
    }
    document.getElementById("hdnPostflightPaxInitializationObject").value = "";
    Init();
    GetPaxPurposeList();

    InitializePaxSummaryGrid();
    getTripExceptionGrid('reload');

    InitializeSIFLPersonalGrid();
    GetSIFLLegsList();
    InitializeBottomAndSummaryGrids();

    self.POSIFL.AssocPaxEnabled.subscribe(function (newValue) {
        self.POPaxSIFLAssociatePaxEditMode(newValue);

    });

};





function ShowPaxDetailsPopup(Paxid) {
    window.radopen("/Views/Transactions/Preflight/PreflightPassengerPopup.aspx?PaxID=" + Paxid, "rdPaxPage");
    return false;
}

// use verifyresult common method
function AddSIFLLeg_Click() {

    var isValid = ValidateErrorLabels(SiflErrorLabels);
    if (isValid == false) {
        jAlert("Please set valid values before adding new SIFL.",alerttitle);
        return;
    }

    var legsData = ko.toJS(Postflight.PostflightLegs());
    // Check Leg count and show alert  - return 
    var legCount = Postflight.PostflightLegs().length;
    if (legCount <= 0) {
        jAlert("Atleast one Leg should be available.", alerttitle);
        return
    }

    resetKOViewModel(self.POSIFL);
    
    var paramters;
    $.ajax({
        async: true,
        type: "POST",
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/AddNewSIFLClick",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response, textStatus, jqXHR) {
            var retResult = verifyReturnedResult(response);
            if (retResult == true && response.d.Success == true) {
                $("#lbcvDeparts").html(""); $("#lbcvArrives").html("");
                self.POPaxSIFLControlEditMode(true);
                self.POPaxSIFLAssociatePaxEditMode(false);

                SIFLLegsListDataSource = response.d.Result;
                reloadSIFLLegsGrid();

                ko.mapping.fromJS(SIFLLegsListDataSource[0], self.POSIFL);
                if (response.d.ErrorsList.length > 0) {
                    jAlert(response.d.ErrorsList[0], alerttitle);
                }
            }         
            else {
                    jAlert(response.d.ErrorsList[0], alerttitle);
                    return;                
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}


function SaveSiflChange() {
    RequestStart();
    var paramters = {};
    paramters.siflViewModel = ko.mapping.toJS(self.POSIFL);

    $.ajax({
        async: true,
        type: "POST",
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SaveSiflViewModel",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response, textStatus, jqXHR) {
            var retResult = verifyReturnedResult(response);
            if (retResult == true && response.d.Result == true) {
                GetSIFLLegsList(true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () {
            ResponseEnd();
        }
    });
}



function lnkSiflReport_Click() {
    if (self.Postflight.PostflightMain.POLogID() > 0) {
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SiflReport_Click",
            data: {},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response, textStatus, jqXHR) {
                var retResult = verifyReturnedResult(response);
                if (response.d.Success == true) {

                    downloadURL("/Views/Reports/ReportRenderView.aspx", 1);
                    return true;
                }
                else {
                    jAlert("No data exists for SIFL report!", alerttitle);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            }
        });
    } else {
        jAlert("No data exists for SIFL report!", alerttitle);
        return;
    }
}

function ValidateErrorLabels(errorLabelIds)
{
    for (var i = 0; i < errorLabelIds.length; i++) {
        var element = $("#" + errorLabelIds[i]);
        if (element.html().length > 0) {
            return false;
        }
    }
    return true;
}
function ClearAllErrorLabels(errorLabelIds)
{
    for (var i = 0; i < errorLabelIds.length; i++) {
        $("#" + errorLabelIds[i]).html("");
    }
}

