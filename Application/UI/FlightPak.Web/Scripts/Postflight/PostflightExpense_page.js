﻿$(document).ready(function () {
    $('#chkFuelEntry').change(function () {
        if ($(this).is(":checked") == false) {
            jConfirm("Delete All Fuel Information?", title, function (r) {
                if (r) {
                    self.CurrentLegExpenseData.FuelLocatorID("");
                    self.CurrentLegExpenseData.FuelLocator.FuelLocatorCD("");
                    self.CurrentLegExpenseData.FuelLocator.FuelLocatorDescription("");
                    self.CurrentLegExpenseData.FuelQTY("");
                    self.CurrentLegExpenseData.UnitPrice("");
                    self.CurrentLegExpenseData.PostFuelPrice("");
                    self.CurrentLegExpenseData.FederalTAX("");
                    self.CurrentLegExpenseData.SateTAX("");
                    self.CurrentLegExpenseData.SaleTAX("");
                    self.CurrentLegExpenseData.ExpenseAMT("");
                }
                else {
                    self.CurrentLegExpenseData.IsAutomaticCalculation(true);
                }
            });
        }
    });
});

function CalculateExpense(obj) {
    var _qty = document.getElementById('tbQuantity').value;
    var _unitPrice = document.getElementById('tbUnitPrice').value;

    if (_qty == "") { _cargoOut = "0"; }
    if (_unitPrice == "") { _cargoIn = "0"; }

    var _totalExpenses = parseFloat(_qty) * parseFloat(_unitPrice);
    document.getElementById('tbTotalExpense').value = _totalExpenses;
}

//this function is used to navigate to pop up screen's with the selected code
function openWin(radWin) {

    var url = '';
    if (radWin == "radCompanyMasterPopup") {
        url = '/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('tbHomeBase').value;
    }
    else if (radWin == "radAccountMasterPopup") {
        url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('tbAccountNumber').value;
    }
    else if (radWin == "radPayableVendorPopup") {
        url = '/Views/Settings/Logistics/PayableVendorPopUp.aspx?VendorCD=' + document.getElementById('tbPayVendor').value;
    }
    else if (radWin == "radAirportPopup") {
        url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbICAO').value;
    }
    else if (radWin == "radCrewRosterPopup") {
        url = '/Views/Settings/People/CrewRosterPopup.aspx?CrewCD=' + document.getElementById('tbExpCrewCode').value;
    }
    else if (radWin == "RadFBOCodePopup") {
        url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('hdnICAO').value + '&FBOCD=' + document.getElementById('tbFBO').value;
    }
    else if (radWin == "radPaymentTypePopup") {
        url = '/Views/Settings/Company/PaymentTypePopup.aspx?PaymentCD=' + document.getElementById('tbPaymentType').value;
    }
    else if (radWin == "RadFlightCategoryPopup") {
        url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCatagoryCD=' + document.getElementById('tbFlightCat').value + '&FlightCategoryID=' + document.getElementById('hdnFlightCat').value;
    }
    else if (radWin == "RadFuelLocatorPopup") {
        url = '/Views/Settings/Logistics/FuelLocatorPopup.aspx?FuelLocatorCD=' + document.getElementById('tbFuelLocator').value;
    }
    var oWnd = radopen(url, radWin);
}

// this function is used to display the value of selected homebase code from popup
function HomeBasePopupClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnHomeBase");
            Start_Loading(btn);
            self.CurrentLegExpenseData.Homebase.HomebaseCD(arg.HomeBase);
            self.CurrentLegExpenseData.Homebase.HomebaseID(arg.HomebaseID);
            self.CurrentLegExpenseData.HomebaseID(arg.HomebaseID);
            self.CurrentLegExpenseData.Homebase.BaseDescription(arg.BaseDescription);
            $('#lbcvHomeBase').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.Homebase.HomebaseCD("");
            self.CurrentLegExpenseData.Homebase.HomebaseID("");
            self.CurrentLegExpenseData.HomebaseID("");
            self.CurrentLegExpenseData.Homebase.BaseDescription("");
        }
    }
}
// this function is used to display the value of selected Account code from popup
function OnClientAccountClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnAccountNo");
            Start_Loading(btn);
            self.CurrentLegExpenseData.Account.AccountNum(arg.AccountNum);
            self.CurrentLegExpenseData.Account.AccountDescription(arg.AccountDescription);
            self.CurrentLegExpenseData.Account.AccountID(arg.AccountId);
            self.CurrentLegExpenseData.AccountID(arg.AccountId);
            var isbilling = (arg.IsBilling != null && arg.IsBilling == "True");
            self.CurrentLegExpenseData.IsBilling(isbilling);
            $('#lbAccountNumber').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.Account.AccountNum("");
            self.CurrentLegExpenseData.Account.AccountDescription("");
            self.CurrentLegExpenseData.Account.AccountID("");
            self.CurrentLegExpenseData.AccountID("");
            self.CurrentLegExpenseData.IsBilling(false);
        }
    }
}

function OnVendorPayClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnPayVendor");
            Start_Loading(btn);
            self.CurrentLegExpenseData.PayableVendor.VendorCD(arg.VendorCD);
            self.CurrentLegExpenseData.PayableVendor.VendorID(arg.VendorID);
            self.CurrentLegExpenseData.PaymentVendorID(arg.VendorID);
            self.CurrentLegExpenseData.PayableVendor.Name(arg.Name);
            $('#lbPayVendor').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.PayableVendor.VendorCD("");
            self.CurrentLegExpenseData.PayableVendor.PaymentVendorID("");
            self.CurrentLegExpenseData.PaymentVendorID("");
            self.CurrentLegExpenseData.PayableVendor.Name("");
        }
    }
}

function OnAirportClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        var oldAirportId = self.CurrentLegExpenseData.Airport.AirportID();
        if (arg) {
            var btn = document.getElementById("btnICAO");
            Start_Loading(btn);
            self.CurrentLegExpenseData.Airport.IcaoID(arg.ICAO);
            self.CurrentLegExpenseData.Airport.AirportID(arg.AirportID);
            self.CurrentLegExpenseData.AirportID(arg.AirportID);
            self.CurrentLegExpenseData.Airport.AirportName(arg.AirportName);
            $('#lbICAO').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.Airport.IcaoID("");
            self.CurrentLegExpenseData.Airport.AirportID("");
            self.CurrentLegExpenseData.AirportID("");
            self.CurrentLegExpenseData.Airport.AirportName("");
        }

        if (oldAirportId != "" && oldAirportId != self.CurrentLegExpenseData.Airport.AirportID())
        {
            self.CurrentLegExpenseData.FBO.FBOCD("");
            self.CurrentLegExpenseData.FBO.FBOID("");
            self.CurrentLegExpenseData.FBOID("");
            self.CurrentLegExpenseData.FBO.FBOVendor("");
        }
    }
}

function OnCrewCodeClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnCrew");
            Start_Loading(btn);
            self.CurrentLegExpenseData.Crew.CrewCD(arg.CrewCD);
            self.CurrentLegExpenseData.Crew.CrewID(arg.CrewID);
            self.CurrentLegExpenseData.CrewID(arg.CrewID);
            self.CurrentLegExpenseData.Crew.DisplayName(arg.CrewName);
            $('#lbCrew').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.Crew.CrewCD("");
            self.CurrentLegExpenseData.Crew.CrewID("");
            self.CurrentLegExpenseData.CrewID("");
            self.CurrentLegExpenseData.Crew.DisplayName("");
        }
    }
}

function OnFBOCodeClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnFBO");
            Start_Loading(btn);
            self.CurrentLegExpenseData.FBO.FBOCD(arg.FBOCD);
            self.CurrentLegExpenseData.FBO.FBOID(arg.FBOID);
            self.CurrentLegExpenseData.FBOID(arg.FBOID);
            self.CurrentLegExpenseData.FBO.FBOVendor(arg.FBOVendor);
            $('#lbFBO').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.FBO.FBOCD("");
            self.CurrentLegExpenseData.FBO.FBOID("");
            self.CurrentLegExpenseData.FBOID("");
            self.CurrentLegExpenseData.FBO.FBOVendor("");
        }
    }
}

function OnPayTypeClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnPaymentType");
            Start_Loading(btn);
            self.CurrentLegExpenseData.PaymentType.PaymentTypeCD(arg.PaymentTypeCD);
            self.CurrentLegExpenseData.PaymentType.PaymentTypeID(arg.PaymentTypeID);
            self.CurrentLegExpenseData.PaymentTypeID(arg.PaymentTypeID);
            self.CurrentLegExpenseData.PaymentType.PaymentTypeDescription(arg.PaymentTypeDescription);
            $('#lbPaymentType').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.PaymentType.PaymentTypeCD("");
            self.CurrentLegExpenseData.PaymentType.PaymentTypeID("");
            self.CurrentLegExpenseData.PaymentTypeID("");
            self.CurrentLegExpenseData.PaymentType.PaymentTypeDescription("");
        }
    }
}

function OnFlightCatClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnFlightCat");
            Start_Loading(btn);
            self.CurrentLegExpenseData.FlightCategory.FlightCatagoryCD(arg.FlightCatagoryCD);
            self.CurrentLegExpenseData.FlightCategory.FlightCategoryID(arg.FlightCategoryID);
            self.CurrentLegExpenseData.FlightCategoryID(arg.FlightCategoryID);
            self.CurrentLegExpenseData.FlightCategory.FlightCatagoryDescription(arg.FlightCatagoryDescription);
            $('#lbFlightCat').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.FlightCategory.FlightCatagoryCD("");
            self.CurrentLegExpenseData.FlightCategory.FlightCategoryID("");
            self.CurrentLegExpenseData.FlightCategoryID("");
            self.CurrentLegExpenseData.FlightCategory.FlightCatagoryDescription("");
        }
    }
}

function OnFuelLocatorClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnFuelLocator");
            Start_Loading(btn);
            self.CurrentLegExpenseData.FuelLocator.FuelLocatorCD(arg.FuelLocatorCD);
            self.CurrentLegExpenseData.FuelLocator.FuelLocatorID(arg.FuelLocatorID);
            self.CurrentLegExpenseData.FuelLocatorID(arg.FuelLocatorID);
            self.CurrentLegExpenseData.FuelLocator.FuelLocatorDescription(arg.FuelLocatorDescription);
            $('#lbFuelLocator').text('');
            End_Loading(btn, true);
        }
        else {
            self.CurrentLegExpenseData.FuelLocator.FuelLocatorCD("");
            self.CurrentLegExpenseData.FuelLocator.FuelLocatorID("");
            self.CurrentLegExpenseData.FuelLocatorID("");
            self.CurrentLegExpenseData.FuelLocator.FuelLocatorDescription("");
        }
    }
}

function refreshPage(arg) {
    window.location = "PostFlightMain.aspx?seltab=main";
}

function HomebaseValidator_KO()
{
    var btn = document.getElementById("btnHomeBase");
    var homebaseErrorLabel = "#lbcvHomeBase";
    Homebase_Validate_Retrieve_KO(self.CurrentLegExpenseData.Homebase, self.CurrentLegExpenseData.HomebaseID,homebaseErrorLabel , btn);
}

function AirportValidator_KO() {
    var btn = document.getElementById("btnICAO");
    var airportErrorLabel = "lbICAO";
    var oldAirportId = self.CurrentLegExpenseData.Airport.AirportID();
    Airport_Validate_Retrieve_KO(self.CurrentLegExpenseData.Airport, self.CurrentLegExpenseData.AirportID, airportErrorLabel, btn);
    if (oldAirportId != "" && oldAirportId != self.CurrentLegExpenseData.Airport.AirportID()) {
        self.CurrentLegExpenseData.FBO.FBOCD("");
        self.CurrentLegExpenseData.FBO.FBOID("");
        self.CurrentLegExpenseData.FBOID("");
        self.CurrentLegExpenseData.FBO.FBOVendor("");
    }
}

function FBOValidator_KO()
{
    var btn = document.getElementById("btnFBO");
    var fboErrorLabel = "lbFBO";
    FBO_Validate_Retrieve_KO(self.CurrentLegExpenseData.FBO, self.CurrentLegExpenseData.AirportID, self.CurrentLegExpenseData.FBOID, fboErrorLabel, btn);
}

function AccountValidator_KO()
{
    var btn = document.getElementById("btnAccountNo");
    var accountErrorLabel = "lbAccountNumber";
    self.CurrentLegExpenseData.AccountID(null);
    Account_Validate_Retrieve_KO(self.CurrentLegExpenseData.Account, self.CurrentLegExpenseData.AccountID, accountErrorLabel, btn);
}

function PaymentVendorValidator_KO() {
    var btn = document.getElementById("btnPayVendor");
    var vendorErrorLabel = "lbPayVendor";
    PayableVendor_Validate_Retrieve_KO(self.CurrentLegExpenseData.PayableVendor, self.CurrentLegExpenseData.PaymentVendorID, vendorErrorLabel, btn);
}

function PaymentTypeValidator_KO()
{
    var btn = document.getElementById("btnPaymentType");
    var paymentTypeErrorLabel = "lbPaymentType";
    PaymentType_Validate_Retrieve_KO(self.CurrentLegExpenseData.PaymentType, self.CurrentLegExpenseData.PaymentTypeID, paymentTypeErrorLabel, btn);
}

function CrewValidator_KO()
{
    var btn = document.getElementById("btnCrew");
    var crewErrorLabel = "lbCrew";
    Crew_Validate_Retrieve_KO(self.CurrentLegExpenseData.Crew, self.CurrentLegExpenseData.CrewID, crewErrorLabel, btn);
}

function FlightCatagoryValidator_KO() {
    var btn = document.getElementById("btnFlightCat");
    var fcErrorLabel = "lbFlightCat";
    FlightCatagory_Validate_Retrieve_KO(self.CurrentLegExpenseData.FlightCategory, self.CurrentLegExpenseData.FlightCategoryID, fcErrorLabel, btn);
}

function FuelLocatorValidator_KO()
{
    var btn = document.getElementById("btnFuelLocator");
    var fuelErrorLabel = "lbFuelLocator";
    FuelLocator_Validate_Retrieve_KO(self.CurrentLegExpenseData.FuelLocator, self.CurrentLegExpenseData.FuelLocatorID, fuelErrorLabel, btn);
}

function FuelQtyUnitPrice_Changed() {
    var fuelqty = $('#tbQuantity').val();
    var unitprice = $('#tbUnitPrice').val();

    if (!IsNullOrEmptyOrUndefined(fuelqty)) {
        fuelqty = fuelqty.replace('$', '');
        fuelqty = $.trim(fuelqty);
    }

    if (!IsNullOrEmptyOrUndefined(unitprice)) {
        unitprice = unitprice.replace('$', '');
        unitprice = $.trim(unitprice);
    }
    var totExpense = fuelqty * unitprice;
    self.CurrentLegExpenseData.ExpenseAMT(totExpense.toFixed(2));
}

function FuelTotalExpense_Changed() {
    var fuelqty = $('#tbQuantity').val();
    var totExpense = $('#tbTotalExpense').val();

    if (!IsNullOrEmptyOrUndefined(fuelqty)) {
        fuelqty = fuelqty.replace('$', '');
        fuelqty = $.trim(fuelqty);
    }

    if (!IsNullOrEmptyOrUndefined(totExpense)) {
        totExpense = totExpense.replace('$', '');
        totExpense = $.trim(totExpense);
    }
    var unitprice = totExpense / fuelqty;
    if (isNaN(unitprice) || unitprice == Infinity)
        unitprice = 0;
    self.CurrentLegExpenseData.UnitPrice(unitprice);
}