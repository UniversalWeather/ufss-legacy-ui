﻿$(document).ready(function () {
    $("#btnRetrieveTrip,#btnDeleteLog,#btnEditTrip,#btnSaveLog,#btnCancelLog,#btnExpenseLog,#toolbar").hide();
    enableDisableControls();
    $("[id$='rtPostFlight']").hide();
    $("[id$='pnlTrip']").hide();

    if ($.cookie("Expense") == "delete") {
        $.removeCookie("Expense");
        showOperationCompleteMessage(true, "delete");
    }

    var POExpenseID = GetParameterValues('POExpenseID'); 
    function GetParameterValues(param) {  
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');  
        for (var i = 0; i < url.length; i++) {  
            var urlparam = url[i].split('=');  
            if (urlparam[0] == param) {  
                return urlparam[1];  
            }  
        }  
    }
    if (!IsNullOrEmpty(POExpenseID)) ExpenseSearchByExpenseId(POExpenseID);
});

//this function is used to navigate to pop up screen's with the selected code
function openWin(radWin) {

    var url = '';
    if (radWin == "radCompanyMasterPopup") {
        url = '/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('tbHomeBase').value;
    }
    else if (radWin == "radAccountMasterPopup") {
        url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('tbAccountNumber').value;
    }
    else if (radWin == "radPayableVendorPopup") {
        url = '/Views/Settings/Logistics/PayableVendorPopUp.aspx';
    }
    else if (radWin == "radAirportPopup") {
        url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbICAO').value;
    }
    else if (radWin == "radCrewRosterPopup") {
        url = '/Views/Settings/People/CrewRosterPopup.aspx?CrewCD=' + document.getElementById('tbCrew').value;
    }
    else if (radWin == "RadFBOCodePopup") {
        url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('hdnICAO').value + '&FBOCD=' + document.getElementById('tbFBO').value;
    }
    else if (radWin == "radPaymentTypePopup") {
        url = '/Views/Settings/Company/PaymentTypePopup.aspx';
    }
    else if (radWin == "radFlightCategoryPopup") {
        url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx';
    }
    else if (radWin == "RadFuelLocatorPopup") {
        url = '/Views/Settings/Logistics/FuelLocatorPopup.aspx';
    }
    else if (radWin == "RadSearchPopup") {
        url = '/Views/Transactions/PostFlight/PostFlightSearchAllExpenses.aspx';
    }
    else if (radWin == "RadLogPopup") {
        url = '/Views/Transactions/PostFlight/PostFlightSearchAll.aspx';
    }
    else if (radWin == "radFleetProfilePopup") {
        url = '/Views/Settings/Fleet/FleetProfilePopup.aspx?TailNumber=' + document.getElementById('tbTailNo').value;
    }
    else if (radWin == "RadRetrievePopup") {
        url = '/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx';
    }
    var oWnd = radopen(url, radWin);
    oWnd.moveTo(oWnd.center(), 0);
}
function openReportPopup(url, radWin) {
    var oWnd = radopen(url, radWin);
}
function OnSearchAllExpensesClose(oWnd, args) {
    $("#divMessage").hide();
    if (args != null) {
        var arg = args.get_argument();
        if (arg) {
            self.ExpenseSearch(arg.SlipNUM);
        }
    }
}
function OnLogPopupClose(oWnd, args)
{
    if (args != null) {
        var arg = args.get_argument();
        if (arg) {
            self.ExpenseCatalog.LogNum(arg.LogNum);
            self.ExpenseCatalog.POLogID(arg.POLogID);
        }
    }
}
// this function is used to display the value of selected homebase code from popup
function FleetPopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnTailNo");
            Start_Loading(btn);
            self.ExpenseCatalog.Fleet.TailNum(arg.TailNum);
            self.ExpenseCatalog.FleetID(arg.FleetID);
            $('#lbcvTailNo').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.Fleet.TailNum("");
            self.ExpenseCatalog.FleetID("");
        }
    }
}
// this function is used to display the value of selected homebase code from popup
function HomeBasePopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnHomeBase");
            Start_Loading(btn);
            self.ExpenseCatalog.Homebase.HomebaseCD(arg.HomeBase);
            self.ExpenseCatalog.HomebaseID(arg.HomebaseID);
            self.ExpenseCatalog.Homebase.BaseDescription(arg.BaseDescription);
            $('#lbHomeBase').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.Homebase.HomebaseCD("");
            self.ExpenseCatalog.HomebaseID("");
            self.ExpenseCatalog.Homebase.BaseDescription("");
        }
    }
}
// this function is used to display the value of selected Account code from popup
function OnClientAccountClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnAccountNo");
            Start_Loading(btn);
            self.ExpenseCatalog.Account.AccountNum(arg.AccountNum);
            self.ExpenseCatalog.Account.AccountDescription(arg.AccountDescription);
            self.ExpenseCatalog.AccountID(arg.AccountId);
            var isbilling = (arg.IsBilling != null && arg.IsBilling == "True");
            self.ExpenseCatalog.IsBilling(isbilling);
            $('#lbAccountNumber').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.Account.AccountNum("");
            self.ExpenseCatalog.Account.AccountDescription("");
            self.ExpenseCatalog.AccountID("");
            self.ExpenseCatalog.IsBilling(false);
        }
    }
}
function OnVendorPayClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnPayVendor");
            Start_Loading(btn);
            self.ExpenseCatalog.PayableVendor.VendorCD(arg.VendorCD);
            self.ExpenseCatalog.PaymentVendorID(arg.VendorID);
            self.ExpenseCatalog.PayableVendor.Name(arg.Name);
            $('#lbPayVendor').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.PayableVendor.VendorCD("");
            self.ExpenseCatalog.PaymentVendorID("");
            self.ExpenseCatalog.PayableVendor.Name("");
        }
    }
}
function OnAirportClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    var oldAirportId = self.ExpenseCatalog.AirportID();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnICAO");
            Start_Loading(btn);
            self.ExpenseCatalog.Airport.IcaoID(arg.ICAO);
            self.ExpenseCatalog.AirportID(arg.AirportID);
            self.ExpenseCatalog.Airport.AirportName(arg.AirportName);
            $('#lbICAO').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.Airport.IcaoID("");
            self.ExpenseCatalog.AirportID("");
            self.ExpenseCatalog.Airport.AirportName("");
        }
        if (IsNullOrEmptyOrUndefined(self.ExpenseCatalog.AirportID()) || oldAirportId != self.ExpenseCatalog.AirportID()) {
            resetKOViewModel(self.ExpenseCatalog.FBO);
            self.ExpenseCatalog.FBOID("");
        }
    }
}
function OnCrewCodeClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnCrew");
            Start_Loading(btn);
            self.ExpenseCatalog.Crew.CrewCD(arg.CrewCD);
            self.ExpenseCatalog.CrewID(arg.CrewID);
            self.ExpenseCatalog.Crew.DisplayName(arg.CrewName);
            $('#lbCrew').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.Crew.CrewCD("");
            self.ExpenseCatalog.CrewID("");
            self.ExpenseCatalog.Crew.DisplayName("");
        }
    }
}
function OnFBOCodeClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnFBO");
            Start_Loading(btn);
            self.ExpenseCatalog.FBO.FBOCD(arg.FBOCD);
            self.ExpenseCatalog.FBOID(arg.FBOID);
            self.ExpenseCatalog.FBO.FBOVendor(arg.FBOVendor);
            $('#lbFBO').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.FBO.FBOCD("");
            self.ExpenseCatalog.FBOID("");
            self.ExpenseCatalog.FBO.FBOVendor("");
        }
    }
}
function OnPayTypeClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnPaymentType");
            Start_Loading(btn);
            self.ExpenseCatalog.PaymentType.PaymentTypeCD(arg.PaymentTypeCD);
            self.ExpenseCatalog.PaymentTypeID(arg.PaymentTypeID);
            self.ExpenseCatalog.PaymentType.PaymentTypeDescription(arg.PaymentTypeDescription);
            $('#lbPaymentType').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.PaymentType.PaymentTypeCD("");
            self.ExpenseCatalog.PaymentTypeID("");
            self.ExpenseCatalog.PaymentType.PaymentTypeDescription("");
        }
    }
}
function OnFlightCatClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnFlightCat");
            Start_Loading(btn);
            self.ExpenseCatalog.FlightCategory.FlightCatagoryCD(arg.FlightCatagoryCD);
            self.ExpenseCatalog.FlightCategoryID(arg.FlightCategoryID);
            self.ExpenseCatalog.FlightCategory.FlightCatagoryDescription(arg.FlightCatagoryDescription);
            $('#lbFlightCat').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.FlightCategory.FlightCatagoryCD("");
            self.ExpenseCatalog.FlightCategoryID("");
            self.ExpenseCatalog.FlightCategory.FlightCatagoryDescription("");
        }
    }
}
function OnFuelLocatorClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnFuelLocator");
            Start_Loading(btn);
            self.ExpenseCatalog.FuelLocator.FuelLocatorCD(arg.FuelLocatorCD);
            self.ExpenseCatalog.FuelLocatorID(arg.FuelLocatorID);
            self.ExpenseCatalog.FuelLocator.FuelLocatorDescription(arg.FuelLocatorDescription);
            $('#lbFuelLocator').text('');
            End_Loading(btn, true);
        }
        else {
            self.ExpenseCatalog.FuelLocator.FuelLocatorCD("");
            self.ExpenseCatalog.FuelLocatorID("");
            self.ExpenseCatalog.FuelLocator.FuelLocatorDescription("");
        }
    }
}

function fnValidateFBO() {
    if (IsNullOrEmptyOrUndefined(self.ExpenseCatalog.AirportID())) 
        jAlert(FBOWarningMsg, ExpenseLogConfirmTitle);
    else
        openWin('RadFBOCodePopup');
}

function HomebaseValidator_KO() {
    var btn = document.getElementById("btnHomeBase");
    var homebaseErrorLabel = "#lbHomeBase";
    Homebase_Validate_Retrieve_KO(self.ExpenseCatalog.Homebase, self.ExpenseCatalog.HomebaseID, homebaseErrorLabel, btn);
}
function AirportValidator_KO() {
    var btn = document.getElementById("btnICAO");
    var airportErrorLabel = "#lbICAO";
    var oldAirportId = self.ExpenseCatalog.AirportID();
    Airport_Validate_Retrieve_KO(self.ExpenseCatalog.Airport, self.ExpenseCatalog.AirportID, airportErrorLabel, btn);
    if (IsNullOrEmptyOrUndefined(self.ExpenseCatalog.AirportID()) || oldAirportId != self.ExpenseCatalog.AirportID()) {
        resetKOViewModel(self.ExpenseCatalog.FBO);
        self.ExpenseCatalog.FBOID("");
    }
}
function FBOValidator_KO() {
    var btn = document.getElementById("btnFBO");
    var fboErrorLabel = "lbFBO";
    if (IsNullOrEmptyOrUndefined(self.ExpenseCatalog.AirportID())) {
        jAlert(FBOWarningMsg, ExpenseLogConfirmTitle);
        self.ExpenseCatalog.FBO.FBOCD("");
    }
    else
        FBO_Validate_Retrieve_KO(self.ExpenseCatalog.FBO, self.ExpenseCatalog.AirportID, self.ExpenseCatalog.FBOID, fboErrorLabel, btn);
}
function AccountValidator_KO() {
    var btn = document.getElementById("btnAccountNo");
    var accountErrorLabel = "lbAccountNumber";
    Account_Validate_Retrieve_KO(self.ExpenseCatalog.Account, self.ExpenseCatalog.AccountID, accountErrorLabel, btn);
}
function CrewValidator_KO() {
    var btn = document.getElementById("btnCrew");
    var crewErrorLabel = "lbCrew";
    Crew_Validate_Retrieve_KO(self.ExpenseCatalog.Crew, self.ExpenseCatalog.CrewID, crewErrorLabel, btn);
}
function PaymentVendorValidator_KO() {
    var btn = document.getElementById("btnPayVendor");
    var vendorErrorLabel = "lbPayVendor";
    PayableVendor_Validate_Retrieve_KO(self.ExpenseCatalog.PayableVendor, self.ExpenseCatalog.PaymentVendorID, vendorErrorLabel, btn);
}
function PaymentTypeValidator_KO() {
    var btn = document.getElementById("btnPaymentType");
    var paymentTypeErrorLabel = "lbPaymentType";
    PaymentType_Validate_Retrieve_KO(self.ExpenseCatalog.PaymentType, self.ExpenseCatalog.PaymentTypeID, paymentTypeErrorLabel, btn);
}
function FlightCatagoryValidator_KO() {
    var btn = document.getElementById("btnFlightCat");
    var fcErrorLabel = "lbFlightCat";
    FlightCatagory_Validate_Retrieve_KO(self.ExpenseCatalog.FlightCategory, self.ExpenseCatalog.FlightCategoryID, fcErrorLabel, btn);
}
function FuelLocatorValidator_KO() {
    var btn = document.getElementById("btnFuelLocator");
    var fuelErrorLabel = "lbFuelLocator";
    FuelLocator_Validate_Retrieve_KO(self.ExpenseCatalog.FuelLocator, self.ExpenseCatalog.FuelLocatorID, fuelErrorLabel, btn);
}
function TailNumValidator_KO() {
    var btn = document.getElementById("btnTailNo");
    var fleetErrorlabel = "lbcvTailNo";
    TailNum_Validate_Retrieve_KO(self.ExpenseCatalog.Fleet, self.ExpenseCatalog.FleetID, fleetErrorlabel, btn);
}
 
function FuelQtyUnitPrice_Changed() {
    var fuelqty = $('#tbQuantity').val();
    var unitprice = $('#tbUnitPrice').val();

    if (!IsNullOrEmptyOrUndefined(fuelqty)) {
        fuelqty = fuelqty.replace('$', '');
        fuelqty = $.trim(fuelqty);
    }

    if (!IsNullOrEmptyOrUndefined(unitprice)) {
        unitprice = unitprice.replace('$', '');
        unitprice = $.trim(unitprice);
    }
    var totExpense = fuelqty * unitprice;
    self.ExpenseCatalog.ExpenseAMT(totExpense.toFixed(2));
}
function FuelTotalExpense_Changed() {
    var fuelqty = $('#tbQuantity').val();
    var totExpense = $('#tbTotalExpense').val();

    if (!IsNullOrEmptyOrUndefined(fuelqty)) {
        fuelqty = fuelqty.replace('$', '');
        fuelqty = $.trim(fuelqty);
    }

    if (!IsNullOrEmptyOrUndefined(totExpense)) {
        totExpense = totExpense.replace('$', '');
        totExpense = $.trim(totExpense);
    }
    var unitprice = totExpense / fuelqty;
    self.ExpenseCatalog.UnitPrice(unitprice);
}

function showOperationCompleteMessage(isSuccess,operation)
{
    var msgBlock = $("#divMessage");
    msgBlock.removeClass();
    if (isSuccess) {
        msgBlock.addClass("success_msg");
        if (operation == "save") {
            msgBlock.html(saveSuccessMsg);
        }
        if (operation == "delete") {
            msgBlock.html(msgDeleteSuccess);
        }
    } else {
        msgBlock.addClass("infored");
        if (operation == "save") {
            msgBlock.html(saveUnsuccessMsg);
        }
        if (operation == "delete") {
            msgBlock.html(msgDeleteUnsuccess);
        }
    }
    msgBlock.css("display", "inline");
    msgBlock.delay(10000).fadeOut(10000);
}

function enableDisableControls() {
    if (self.ECEditMode()) {
        $("#btnSave,#btnCancel").removeClass("button-disable").addClass("button");
        $("#btnCopyExpense").removeClass("button").addClass("button-disable");
        $("#btnOtherCrewDuty,#btnNewTrip").prop("disabled", true).addClass("ui_nav_disable").removeClass("ui_nav");
        $("#divMessage").hide();
    }
    else {
        $("#ExpenseEntryArea .alert-text").text("");
        $("#btnSave,#btnCancel").removeClass("button").addClass("button-disable");
        $("#btnOtherCrewDuty,#btnNewTrip").prop("disabled", false).addClass("ui_nav").removeClass("ui_nav_disable");
    }
}
function FuelQtyUnitPrice_Changed() {
    var fuelqty = $('#tbQuantity').val();
    var unitprice = $('#tbUnitPrice').val();

    if (!IsNullOrEmptyOrUndefined(fuelqty)) {
        fuelqty = fuelqty.replace('$', '');
        fuelqty = $.trim(fuelqty);
    }

    if (!IsNullOrEmptyOrUndefined(unitprice)) {
        unitprice = unitprice.replace('$', '');
        unitprice = $.trim(unitprice);
    }
    var totExpense = fuelqty * unitprice;
    self.ExpenseCatalog.ExpenseAMT(totExpense.toFixed(2));
}
function FuelTotalExpense_Changed() {
    var fuelqty = $('#tbQuantity').val();
    var totExpense = $('#tbTotalExpense').val();

    if (!IsNullOrEmptyOrUndefined(fuelqty)) {
        fuelqty = fuelqty.replace('$', '');
        fuelqty = $.trim(fuelqty);
    }

    if (!IsNullOrEmptyOrUndefined(totExpense)) {
        totExpense = totExpense.replace('$', '');
        totExpense = $.trim(totExpense);
    }
    var unitprice = totExpense / fuelqty;
    if (isNaN(unitprice) || unitprice == Infinity)
        unitprice = 0;
    self.ExpenseCatalog.UnitPrice(unitprice);
}