﻿var legsType;
$(document).ready(function () {
    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $(".tabStrip li").eq(0).removeClass("tabStripItem").addClass("tabStripItemSelected");
    getUserPrincipal();
    $("#tbScheduledTime").mask("?99:99").change(function () { self.CurrentLegData.ScheduledTime($(this).val()); });
    $("#tbOutTime").mask("?99:99").change(function () { self.CurrentLegData.OutTime($(this).val()); });
    $("#tbOffTime").mask("?99:99").change(function () { self.CurrentLegData.TimeOff($(this).val()); });
    $("#tbOnTime").mask("?99:99").change(function () { self.CurrentLegData.TimeOn($(this).val()); });
    $("#tbInTime").mask("?99:99").change(function () { self.CurrentLegData.InTime($(this).val()); });
    $(".textDatePicker").datepicker({
        showOn: 'input',
        changeMonth: true,
        changeYear: true,
        dateFormat: self.UserPrincipal._ApplicationDateFormat.toLowerCase().replace(/yyyy/g, "yy")
    });

    $("#tbScheduledDate").click(function () { $("#tbScheduledDate").datepicker('show'); });
    $("#tbOutDate").click(function () { $("#tbOutDate").datepicker('show'); });
    $("#tbInDate").click(function () { $("#tbInDate").datepicker('show'); });

    
    if (self.UserPrincipal._TimeDisplayTenMin == "2") {
        $("#tbDelayTime").mask("99:99").change(function () { self.CurrentLegData.AirFrameHoursTime($(this).val()); });
    }

    if (self.UserPrincipal._IsAutoFillAF == true) {
        
        $("#tbAirFrameHours").attr("disabled", "disabled");
        $("#tbAirFramecycles").attr("disabled", "disabled");

        $("#tbEngineers1Hr").attr("disabled", "disabled");
        $("#tbEngineers2Hr").attr("disabled", "disabled");
        $("#tbEngineers3Hr").attr("disabled", "disabled");
        $("#tbEngineers4Hr").attr("disabled", "disabled");

        $("#tbEngineers1Cycle").attr("disabled", "disabled");
        $("#tbEngineers2Cycle").attr("disabled", "disabled");
        $("#tbEngineers3Cycle").attr("disabled", "disabled");
        $("#tbEngineers4Cycle").attr("disabled", "disabled");

        $("#tbReverse1Hr").attr("disabled", "disabled");
        $("#tbReverse2Hr").attr("disabled", "disabled");
        $("#tbReverse3Hr").attr("disabled", "disabled");
        $("#tbReverse4Hr").attr("disabled", "disabled");

        $("#tbReverse1Cycle").attr("disabled", "disabled");
        $("#tbReverse2Cycle").attr("disabled", "disabled");
        $("#tbReverse3Cycle").attr("disabled", "disabled");
        $("#tbReverse4Cycle").attr("disabled", "disabled");
    }
    
    if (self.UserPrincipal._TimeDisplayTenMin == "2") {
        $("#tbAirFrameHours").attr("maxlength", "10");
        $("#tbEngineers1Hr").attr("maxlength", "10");
        $("#tbEngineers2Hr").attr("maxlength", "10");
        $("#tbEngineers3Hr").attr("maxlength", "10");
        $("#tbEngineers4Hr").attr("maxlength", "10");
        $("#tbReverse1Hr").attr("maxlength", "10");
        $("#tbReverse2Hr").attr("maxlength", "10");
        $("#tbReverse3Hr").attr("maxlength", "10");
        $("#tbReverse4Hr").attr("maxlength", "10");
        $("#tbDelayTime").attr("maxlength", "5");
        $("#tbFlightHours").attr("maxlength", "5");
        $("#tbBlockHours").attr("maxlength", "5");
    }
    else {
        $("#tbAirFrameHours").attr("maxlength", "9");
        $("#tbEngineers1Hr").attr("maxlength", "9");
        $("#tbEngineers2Hr").attr("maxlength", "9");
        $("#tbEngineers3Hr").attr("maxlength", "9");
        $("#tbEngineers4Hr").attr("maxlength", "9");
        $("#tbReverse1Hr").attr("maxlength", "9");
        $("#tbReverse2Hr").attr("maxlength", "9");
        $("#tbReverse3Hr").attr("maxlength", "9");
        $("#tbReverse4Hr").attr("maxlength", "9");
        $("#tbDelayTime").attr("maxlength", "4");
        $("#tbFlightHours").attr("maxlength", "4");
        $("#tbBlockHours").attr("maxlength", "4");
    }

    InitializeKOWatchPostflightLeg();
    verifyIfLogIsFromCalendar();
});
$(document).on("click", ".tabStripItem", function () {
    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $(this).removeClass("tabStripItem").addClass("tabStripItemSelected");
});

function departureChanged() {
    self.CurrentLegData.DepartICAOID(null);
    SaveCurrentLegDataToSession();
    var btn = document.getElementById("btnClosestIcao");
    var depart = CurrentLegData.DepartureAirport.IcaoID;
    var TimeDisplayTenMin = self.UserPrincipal._TimeDisplayTenMin;
    DepartureIcao_Validate_Retrieve(depart, TimeDisplayTenMin, self.CurrentLegData.LegNUM, btn);
}
function RadDepartPopupClose(oWnd, args) {
    if (legsType == "Depart") {
        var arg = args.get_argument();
        if (arg != null) {
            if (arg) {
                $("[id$='lbcvDepart']").text("");
                resetKOViewModel(self.CurrentLegData.DepartureAirport, PostflightAirportBlackList);
                var btn = document.getElementById("btnClosestIcao");
                Start_Loading(btn);
                CurrentLegData.DepartureAirport.IcaoID(arg.ICAO);
                CurrentLegData.DepartureAirport.AirportID(arg.AirportID);
                if (arg.AirportName != null)
                    CurrentLegData.DepartureAirport.AirportName(arg.AirportName);
                departureChanged();
                End_Loading(btn, true);
            } 
        }
    }
}
function arrivalChanged() {
    self.CurrentLegData.ArriveICAOID(null);
    SaveCurrentLegDataToSession();
    var btn = document.getElementById("btnArrival");
    var arrival = self.CurrentLegData.ArrivalAirport.IcaoID;
    var TimeDisplayTenMin = self.UserPrincipal._TimeDisplayTenMin;
    ArrivalIcao_Validate_Retrieve(arrival, TimeDisplayTenMin, self.CurrentLegData.LegNUM, btn);
    return false;
}
function RadArrivePopupClose(oWnd, args) {
    if (legsType == "Arrival") {
        var arg = args.get_argument();
        if (arg != null) {
            if (arg) {
                $("[id$='lbcvArrival']").text("");
                resetKOViewModel(self.CurrentLegData.ArrivalAirport, PostflightAirportBlackList);
                var btn = document.getElementById("btnArrival");
                Start_Loading(btn);
                CurrentLegData.ArrivalAirport.IcaoID(arg.ICAO);
                CurrentLegData.ArrivalAirport.AirportID(arg.AirportID);
                if (arg.AirportName != null)
                    CurrentLegData.ArrivalAirport.AirportName(arg.AirportName);
                arrivalChanged();
                End_Loading(btn, true);
            } 
        }
    }
}
function ClearICAOErrorMessages()
{
    document.getElementById("lbcvArrival").innerHTML = "";
    document.getElementById("lbcvDepart").innerHTML = "";
    document.getElementById("lbcvRules").innerHTML = "";
    document.getElementById("lbcvDelay").innerHTML = "";
    document.getElementById("lbcvClient").innerHTML = "";
    document.getElementById("lbcvDepartment").innerHTML = "";
    document.getElementById("lbcvAccountNumber").innerHTML = "";
    document.getElementById("lbcvAuthorization").innerHTML = "";
    document.getElementById("lbcvRequestor").innerHTML = "";
    document.getElementById("lbcvCategory").innerHTML = "";
}

function ClearViewModelChildren(CurrentLeg) {

    if (IsNullOrEmpty(CurrentLeg.DelayTypeID)) {
        delete CurrentLeg.DelayType;
    }

    if (IsNullOrEmpty(CurrentLeg.CrewDutyRulesID)) {
        delete CurrentLeg.CrewDutyRule;
    }
    if (IsNullOrEmpty(CurrentLeg.ClientID)) {
        delete CurrentLeg.Client;
    }
    if (IsNullOrEmpty(CurrentLeg.DepartmentID)) {
        delete CurrentLeg.Department;
    }
    if (IsNullOrEmpty(CurrentLeg.FlightCategoryID)) {
        delete CurrentLeg.FlightCatagory;
    }
    if (IsNullOrEmpty(CurrentLeg.PassengerRequestorID)) {
        delete CurrentLeg.Passenger;
    }
    if (IsNullOrEmpty(CurrentLeg.AuthorizationID)) {
        delete CurrentLeg.DepartmentAuthorization;
    }
    if (IsNullOrEmpty(CurrentLeg.AccountID)) {
        delete CurrentLeg.Account;
    }
    if (IsNullOrEmpty(CurrentLeg.ArrivalAirport.AirportID)) {
        delete CurrentLeg.ArrivalAirport;
    }
    if (IsNullOrEmpty(CurrentLeg.DepartureAirport.AirportID)) {
        delete CurrentLeg.DepartureAirport;
    }

}

function AddInsertValidateLegICAOs(LegData)
{
    // mandatory field validation before push data to server
    if (IsNullOrEmpty(LegData.DepartICAOID)) {
        $("#lbcvDepart").text("Departure ICAO is Required.");
        return false;
    }

    if (IsNullOrEmpty(LegData.ArriveICAOID)) {
        $("#lbcvArrival").text("Arrival ICAO is Required.");
        return false;
    }
}

function AddLeg() {
    FormatLegDates();
    removeUnwantedPropertiesKOViewModel_CurrentLeg();
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    var LegNum = self.CurrentLegData.LegNUM();

    if (AddInsertValidateLegICAOs(LegData) == false)
    {
        return;
    }
    ClearViewModelChildren(LegData);
    RequestStart();
    self.getTripExceptionGrid("reload");
    $.ajax({
        async: true,
        cache: false,
        url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/AddNewLeg",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'LegNUM': LegNum, 'CurrentLeg': LegData }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == false || result.d.Success == false) {
                return false;
            }

            var jsonObj = result.d.Result;
            if (jsonObj != null) {
                ClearICAOErrorMessages();
                if (self.CurrentLegData.LegNUM == undefined) {                    
                    self.CurrentLegData = ko.mapping.fromJS(jsonObj.NewLeg);
                    self.Legs = ko.mapping.fromJS(jsonObj.Legs);
                } else {
                    self.Legs.push(ko.mapping.fromJS(jsonObj.NewLeg));
                    ko.mapping.fromJS(jsonObj.NewLeg, {}, self.CurrentLegData);
                    removeUnwantedPropertiesKOViewModel_CurrentLeg();
                    self.CurrentLegNum(self.CurrentLegData.LegNUM());
                }
                setTooltipforAirport(self.CurrentLegData.DepartureAirport.IcaoID(), self.CurrentLegData.DepartureAirport.tooltipInfo, document.getElementById("tbDepart"), "DEPT");
                setTooltipforAirport(self.CurrentLegData.ArrivalAirport.IcaoID(), self.CurrentLegData.ArrivalAirport.tooltipInfo, document.getElementById("tbArrival"));
                $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                $("#Leg" + jsonObj.NewLeg.LegNUM).removeClass("tabStripItem").addClass("tabStripItemSelected");
                $('#tbDutyHours').attr("style", "background-color: #F1F1F1 !important;border-color: #cccccc !important;");
                CurrentLegData.CrewDutyRule.tooltipInfo(htmlDecode(CurrentLegData.CrewDutyRule.tooltipInfo()));
                InitializeBottomAndSummaryGrids();
            }
            ResponseEnd();
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}
function btnInsertLegButton_Click() {
    setAlertTextToYesNo();
    if (self.CurrentLegData.PostflightLegCrews().length > 0) {
        jConfirm("Add same crew/postion to inserted leg?", PostflightConfirmTitle, function(arg) {
            InsertLeg(arg);
        });
    } else {
        InsertLeg(false);
    }

    setAlertTextToOkCancel();

}
function InsertLeg(isCopyCrew) {
    FormatLegDates();
    removeUnwantedPropertiesKOViewModel_CurrentLeg();
    var legNum = Number(self.CurrentLegData.LegNUM());
    var LegData = ko.mapping.toJS(self.CurrentLegData);

    if (AddInsertValidateLegICAOs(LegData) == false) {
        return;
    }
    ClearViewModelChildren(LegData);
    self.getTripExceptionGrid("reload");
    RequestStart();
    $.ajax({
        async: true,
        url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/InsertNewLeg",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'CurrentLegNum': legNum, 'CurrentLeg': LegData, 'isCopyCrew': isCopyCrew }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == false || result.d.Success == false) {
                return false;
            }

            var jsonObj = result.d.Result;
            if (jsonObj != null) {
                ClearICAOErrorMessages();
                ko.mapping.fromJS(jsonObj.NewLeg, {}, self.CurrentLegData);
                removeUnwantedPropertiesKOViewModel_CurrentLeg();
                ko.mapping.fromJS(jsonObj.Legs, {}, self.Legs);
                removeUnwantedPropertiesKOViewModel(self.Legs);
                self.CurrentLegNum(self.CurrentLegData.LegNUM());
                
                $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                $("#Leg" + jsonObj.NewLeg.LegNUM).removeClass("tabStripItem").addClass("tabStripItemSelected");
                $('#tbDutyHours').attr("style", "background-color: #F1F1F1 !important;border-color: #cccccc !important;");
                CurrentLegData.CrewDutyRule.tooltipInfo(htmlDecode(CurrentLegData.CrewDutyRule.tooltipInfo()));
                InitializeBottomAndSummaryGrids();
                ResponseEnd();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}
function btnDeleteLegButton_Click() {
    var depIcao = IsNullOrEmptyOrUndefined(CurrentLegData.DepartureAirport.IcaoID()) ? '' : CurrentLegData.DepartureAirport.IcaoID();
    var arrIcao = IsNullOrEmptyOrUndefined(CurrentLegData.ArrivalAirport.IcaoID()) ? '' : CurrentLegData.ArrivalAirport.IcaoID();
    jConfirm("Are you sure?\nYou want to delete Leg " + self.CurrentLegNum() + " (" + depIcao + "-" + arrIcao + ") ?", PostflightLegAlertTitle, function (arg) {
        if (arg) {            
            DeleteLeg();
        }
    });
}
function DeleteLeg() {
    var legNum = Number(self.CurrentLegData.LegNUM());
    RequestStart();
    $.ajax({
        async: true,
        cache: false,
        url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/DeleteLeg",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'CurrentLegNum': legNum }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == false || result.d.Success == false) {
                if (result.d.ErrorsList.length > 0) {
                    jAlert(htmlDecode(result.d.ErrorsList[0]), PostflightLegAlertTitle, function () { return false; });
                }
                return false;
            }
            var jsonObj = result.d.Result;
            if (jsonObj != null) {
                ClearICAOErrorMessages();
                ko.mapping.fromJS(jsonObj.NewLeg, {}, self.CurrentLegData);
                removeUnwantedPropertiesKOViewModel_CurrentLeg();
                ko.mapping.fromJS(jsonObj.Legs, {}, self.Legs);
                removeUnwantedPropertiesKOViewModel(self.Legs);
                self.CurrentLegNum(self.CurrentLegData.LegNUM());
                $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                $("#Leg" + jsonObj.NewLeg.LegNUM).removeClass("tabStripItem").addClass("tabStripItemSelected");
                jQuery(ShortLegSummary).jqGrid('GridUnload');
                jQuery(DetailedLegSummary).jqGrid('GridUnload');
                InitializeBottomAndSummaryGrids();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}
function FormatLegDates() {
    if (self.CurrentLegData.LastUpdTS() != null) {
        self.CurrentLegData.LastUpdTS = ko.observable(self.CurrentLegData.LastUpdTS()).extend({ dateFormat: isodatetimeformat });
    }
    if (self.CurrentLegData.DepartureAirport.DayLightSavingEndDT() != null) {
        self.CurrentLegData.DepartureAirport.DayLightSavingEndDT(moment(self.CurrentLegData.DepartureAirport.DayLightSavingEndDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.DepartureAirport.DayLightSavingStartDT() != null) {
        self.CurrentLegData.DepartureAirport.DayLightSavingStartDT(moment(self.CurrentLegData.DepartureAirport.DayLightSavingStartDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT() != null) {
        self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT(moment(self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT() != null) {
        self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT(moment(self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.ScheduleDTTMLocal() != null) {
        self.CurrentLegData.ScheduleDTTMLocal = ko.observable(self.CurrentLegData.ScheduleDTTMLocal()).extend({ dateFormat: isodatetimeformat });
    }
    if (self.CurrentLegData.DepartureDTTMLocal() != null) {
        self.CurrentLegData.DepartureDTTMLocal = ko.observable(self.CurrentLegData.DepartureDTTMLocal()).extend({ dateFormat: isodatetimeformat });
    }

    //Postflight Expence 
    for (i = 0; i < self.CurrentLegData.PostflightLegExpensesList().length; i++) {
        var expObj = self.CurrentLegData.PostflightLegExpensesList()[i];
        if (!IsNullOrEmptyOrUndefined(expObj.PurchaseDT())) {
            expObj.PurchaseDT = ko.observable(expObj.PurchaseDT()).extend({ dateFormat: isodatetimeformat });
        }
    }
}
function MilesValidation() {
    if (self.POEditMode() == true) {
        FormatLegDates();
        var LegData = ko.mapping.toJS(self.CurrentLegData);
        var LegNum = self.CurrentLegData.LegNUM();
        $.ajax({
            async: true,
            cache: false,
            url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/SaveCurrentLegDataAndReturn",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'LegNUM': LegNum, 'CurrentLegData': LegData }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = result.d.Result;
                if (jsonObj != null) {
                    ScheduledHours_Calculate_Validate_Retrieve(false);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () {
            }
        });
        return false;
    }
}
function delayTypeChange() {
    var btn = document.getElementById("btnDelayType");
    var delayType = $("[id$='tbDelayType']").val();
    PODelayType_Validate_Retrieve(delayType, btn);
    return false;
}
function DelayTypePopupClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            var btn = document.getElementById("btnDelayType");
            Start_Loading(btn);
            var delayType = $("[id$='tbDelayType']").val();
            PODelayType_Validate_Retrieve(arg.DelayTypeCD, btn);
            End_Loading(btn, true);
        }
    }
}

function ScheduledTimeChanged() {
    SaveCurrentLegDataToSession_Schedule_Calculation();
    return false;
}
function OutTimeChanged() {
    
    var schTm = GetDateTimeFromString(self.CurrentLegData.ScheduledDate(), self.CurrentLegData.ScheduledTime());
    var outTm;

    if (UserPrincipal._DutyBasis == 2) {
//        $("#tbOffTime").focus();
        self.CurrentLegData.OutDate(self.CurrentLegData.ScheduledDate());
        outTm = GetDateTimeFromString(self.CurrentLegData.ScheduledDate(), self.CurrentLegData.OutTime());
    }
    else {
//        $("#tbOutDate").focus();
        if (!IsNullOrEmpty(self.CurrentLegData.OutDate())) {
            outTm = GetDateTimeFromString(self.CurrentLegData.OutDate(), self.CurrentLegData.OutTime());
        }
        else if (!IsNullOrEmpty(self.CurrentLegData.ScheduledDate())) {
            outTm = GetDateTimeFromString(self.CurrentLegData.ScheduledDate(), self.CurrentLegData.OutTime());
        }
    }

    if (outTm == null || schTm == null)
        return false;

    setAlertTextToYesNo();
    if (outTm < schTm) {
        var errorMgs;
        if (UserPrincipal._DutyBasis == 2) {
            addDateKO = self.CurrentLegData.ScheduledDate();
            isInDateKO=true;
            isOutDateKO=false;
            if (UserPrincipal._IsScheduleDTTM) {
                errorMgs = "Your OUT Time is earlier than your Scheduled Time, should the IN date be changed to the next date?";
                jConfirm(errorMgs, PostflightLegAlertTitle, function (arg) {
                    if (arg) {
                        addDateKO = self.CurrentLegData.ScheduledDate();
                        isInDateKO = true;
                        isOutDateKO = true;
                    }
                    confirmScheduleDateFromSettings(arg);
                    $("#tbOffTime").focus();
                });
            }
            else {
                setScheduleDateFromSettings();
            }
        }
        else {
            if (UserPrincipal._IsScheduleDTTM) {
                errorMgs = "Your OUT Time is earlier than your Scheduled Time, should the OUT date be changed to the next date?";
                jConfirm(errorMgs, PostflightLegAlertTitle, function (arg) {
                    if (arg) {
                        addDateKO = self.CurrentLegData.ScheduledDate();
                        isInDateKO = true;
                        isOutDateKO = true;
                    }
                    confirmScheduleDateFromSettings(arg);
                    $("#tbOutDate").focus();
                });
            }
            else {
                addDateKO = self.CurrentLegData.ScheduledDate();
                isInDateKO = true;
                isOutDateKO = true;
                setScheduleDateFromSettings();
            }
        }

        if (IsNullOrEmpty(self.CurrentLegData.TimeOff())) {
            self.CurrentLegData.TimeOff(self.CurrentLegData.OutTime());
        }
    }
    if (UserPrincipal._DutyBasis == 2) {
        $("#tbOffTime").focus();
    }
    else {
        $("#tbOutDate").focus();
    }
    setAlertTextToOkCancel();
    SaveCurrentLegDataToSession_Schedule_Calculation();
    return false;
}
function OffTimeChanged() {
    setAlertTextToYesNo();
    var outDateTm;
    var offDateTm;
    if (UserPrincipal._DutyBasis == 2)
    {
        outDateTm = GetDateTimeFromString(self.CurrentLegData.ScheduledDate(), self.CurrentLegData.OutTime());
        offDateTm = GetDateTimeFromString(self.CurrentLegData.ScheduledDate(), self.CurrentLegData.TimeOff());
    }
    else
    {
        outDateTm = GetDateTimeFromString(self.CurrentLegData.OutDate(), self.CurrentLegData.OutTime());
        offDateTm = GetDateTimeFromString(self.CurrentLegData.OutDate(), self.CurrentLegData.TimeOff());
    }

    if (offDateTm < outDateTm) {
        if (UserPrincipal._IsScheduleDTTM) {
            var errorMgs = "Your OFF Time is earlier than your OUT Time, should the OFF date be changed to the next date?";
            jConfirm(errorMgs, PostflightLegAlertTitle, function (arg) {
                if (arg) {
                    addDateKO = self.CurrentLegData.ScheduledDate();
                    isInDateKO = true;
                    isOutDateKO = true;
                }
                confirmScheduleDateFromSettings(arg);
                $("#tbOnTime").focus();
            });
        }
        else {
            addDateKO = self.CurrentLegData.ScheduledDate();
            isInDateKO = true;
            isOutDateKO = true;
            setScheduleDateFromSettings();
        }
    }
    setAlertTextToOkCancel();
    SaveCurrentLegDataToSession_Schedule_Calculation();
    return false;
}
function OnTimeChanged() {
    setAlertTextToYesNo();
    var offDateTm = GetDateTimeFromString(self.CurrentLegData.InDate(), self.CurrentLegData.TimeOff());
    var onDateTm = GetDateTimeFromString(self.CurrentLegData.InDate(), self.CurrentLegData.TimeOn()); 
    if (onDateTm < offDateTm) {
        addDateKO = self.CurrentLegData.OutDate();
        isInDateKO = true;
        isOutDateKO = false;
        if (UserPrincipal._IsScheduleDTTM) {
            var errorMgs = "Your ON Time is earlier than your OFF Time, should the ON date be changed to the next date?";
            jConfirm(errorMgs, PostflightLegAlertTitle, function (arg) { confirmScheduleDateFromSettings(arg); $("#tbInTime").focus(); });
        }
        else {
            setScheduleDateFromSettings();
        }
    }
    setAlertTextToOkCancel();
    SaveCurrentLegDataToSession_Schedule_Calculation();
    return false;
}
function InTimeChanged() {
    setAlertTextToYesNo();
    var inDateTm = GetDateTimeFromString(self.CurrentLegData.InDate(), self.CurrentLegData.InTime());
    var onDateTm = GetDateTimeFromString(self.CurrentLegData.InDate(), self.CurrentLegData.TimeOn());
    if (inDateTm < onDateTm) {
        addDateKO = self.CurrentLegData.OutDate();
        isInDateKO = true;
        isOutDateKO = false;
        if (UserPrincipal._IsScheduleDTTM) {
            var errorMgs = "Your IN Time is earlier than your ON Time, should the IN date be changed to the next date?";
            jConfirm(errorMgs, PostflightLegAlertTitle, function (arg) { confirmScheduleDateFromSettings(arg); $("#tbInDate").focus(); });
        }
        else {
            setScheduleDateFromSettings();
        }
    }
    setAlertTextToOkCancel();
    SaveCurrentLegDataToSession_Schedule_Calculation();
    return false;
}
function RemoveTimeValidationMessage() {
    $("#lbErrMsg").text("");
}
function DateTimeChanged(data, event) {
    var currenttextboxId = event.target.id;
    switch (currenttextboxId) {
        case "tbScheduledDate":
            $("#tbScheduledDate").css("color", "black");
            var tbDateVal = $("#tbScheduledDate").val();
            if (!IsNullOrEmptyOrUndefined(tbDateVal)) {
                if (self.CurrentLegNum() == 1 && moment().isBefore(self.CurrentLegData.ScheduledDate(), 'day')) {
                    errorMgs = "You are adding Postflight data for a future date. Are you sure you want to continue?";
                    setAlertTextToYesNo();
                    jConfirm(errorMgs, PostflightLegAlertTitle, function (arg) {
                        if (arg == false) {
                            if (!IsNullOrEmptyOrUndefined(self.CurrentLegData.ScheduledTM())) {
                                self.CurrentLegData.ScheduledDate(moment(self.CurrentLegData.ScheduledTM()).format(isodateformat));
                            } else {
                                self.CurrentLegData.ScheduledDate(moment().format(isodateformat));
                            }
                        }

                        if (POEditMode() && IsNullOrEmpty(self.CurrentLegData.OutDate()))
                            self.CurrentLegData.OutDate(self.CurrentLegData.ScheduledDate());

                        if (POEditMode() && IsNullOrEmpty(self.CurrentLegData.InDate()))
                            self.CurrentLegData.InDate(self.CurrentLegData.OutDate());

                        SaveCurrentLegDataToSession_Schedule_Calculation();
                    });
                    setAlertTextToOkCancel();
                } else {

                    if (POEditMode() && IsNullOrEmpty(self.CurrentLegData.OutDate()))
                        self.CurrentLegData.OutDate(self.CurrentLegData.ScheduledDate());

                    if (POEditMode() && IsNullOrEmpty(self.CurrentLegData.InDate()))
                        self.CurrentLegData.InDate(self.CurrentLegData.OutDate());

                    SaveCurrentLegDataToSession_Schedule_Calculation();
                }
            }
            break;
        default:
            SaveCurrentLegDataToSession_Schedule_Calculation();
            break;
    }
    return false;
}

function crewDutyTypeChange() {
    var btn = document.getElementById("btnRules");
    POCrewDutyRule_Validate_Retrieve(btn);
}
function RulesPopupClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvRules']").text("");
            var btn = document.getElementById("btnRules");
            Start_Loading(btn);
            CurrentLegData.CrewDutyRulesID(arg.CrewDutyRulesID);
            CurrentLegData.CrewDutyRule.CrewDutyRuleCD(arg.CrewDutyRuleCD);
            CurrentLegData.CrewCurrency(arg.CrewDutyRuleCD);
            POCrewDutyRule_Validate_Retrieve(btn);
            End_Loading(btn, true);
        }
    }
}
function fnFuelBurnCalculate(obj) {
    $("[id$='lbcvFuelUsed']").text("");
    var _fuelOut = CurrentLegData.FuelOut();
    var _fuelIn = CurrentLegData.FuelIn();

    if (IsNullOrEmptyOrUndefined(_fuelOut)) { _fuelOut = "0"; CurrentLegData.FuelOut(0); }
    if (IsNullOrEmptyOrUndefined(_fuelIn)) { _fuelIn = "0"; CurrentLegData.FuelIn(0); }

    var _fuelUsed = parseFloat(_fuelOut) - parseFloat(_fuelIn);    
    if (_fuelUsed < 0) {
        //jAlert("Fuel Used cannot be a negative value.", PostflightLegAlertTitle, function () {
            CurrentLegData.FuelUsed(_fuelUsed); // _fuelUsed;
            $("[id$='lbcvFuelUsed']").text("Fuel Used cannot be a negative value."); // "Fuel Used Cannot Be a Negative Value.";
            $("#tbFuelIn").focus();
            CurrentLegData.FuelUsed(_fuelUsed);
        //});
    }
    else {
        CurrentLegData.FuelUsed(_fuelUsed);
    }

    POSaveCurrentLegDataToSession();
}
function fnFuelOutChange()
{
    //CurrentLegData.FuelIn(0);
    fnFuelBurnCalculate(this);
}

function ClientCodeValidation() {
    $("[id$='lbcvClient']").text("");
    var btn = document.getElementById("btnClientCode");
    Client_Validate_Retrieve_KO(self.CurrentLegData.Client, self.CurrentLegData.ClientID, "lbcvClient", btn)
}
function ClientCodePopupClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvClient']").text("");
            var btn = document.getElementById("btnClientCode");
            Start_Loading(btn);
            CurrentLegData.Client.ClientCD(arg.ClientCD);
            CurrentLegData.Client.ClientID(arg.ClientID);
            CurrentLegData.ClientID(arg.ClientID);
            Client_Validate_Retrieve_KO(self.CurrentLegData.Client, self.CurrentLegData.ClientID, "lbcvClient", btn)
            End_Loading(btn, true);
        }
        else {
            CurrentLegData.Client.ClientCD("");
            CurrentLegData.Client.ClientID("");
            CurrentLegData.ClientID("");
            CurrentLegData.Client.ClientDescription("");
        }
    }
}
function RequestorValidator() {
    var btn = document.getElementById("btnRequestor");
    var passengerRequestorErrorLabel = "#lbcvRequestor";
    CurrentLegData.PassengerRequestorID(null);
    $(passengerRequestorErrorLabel).html("");
    Requestor_Validate_Retrieve_KO(self.CurrentLegData.Passenger, CurrentLegData.PassengerRequestorID, passengerRequestorErrorLabel, btn)
}
function RequestorPopupClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvRequestor']").text("");
            var btn = document.getElementById("btnRequestor");
            Start_Loading(btn);
            CurrentLegData.Passenger.PassengerRequestorCD(arg.PassengerRequestorCD);
            CurrentLegData.Passenger.PassengerRequestorID(arg.PassengerRequestorID);
            CurrentLegData.PassengerRequestorID(arg.PassengerRequestorID);
            Requestor_Validate_Retrieve_KO(self.CurrentLegData.Passenger, CurrentLegData.PassengerRequestorID, "#lbcvRequestor", btn)
            End_Loading(btn, true);
        }
    }
}
function DepartmentValidator() {
    self.CurrentLegData.DepartmentID(null);
    var departmentErrorLabel = "#lbcvDepartment";
    var btn = document.getElementById("btnDepartment");
    var DepartmentAuthorizationVM = self.CurrentLegData.DepartmentAuthorization;
    var authorizationLabel = "#lbcvAuthorization";
    $(authorizationLabel).html("");
    Department_Validate_Retrieve_KO(self.CurrentLegData.Department, self.CurrentLegData.DepartmentID, departmentErrorLabel, btn, DepartmentAuthorizationVM, authorizationLabel)
}
function OnClientDeptClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvDepartment']").text("");
            var btn = document.getElementById("btnDepartment");
            Start_Loading(btn);
            CurrentLegData.Department.DepartmentCD(arg.DepartmentCD);
            CurrentLegData.Department.DepartmentID(arg.DepartmentID);
            CurrentLegData.DepartmentID(arg.DepartmentID);
            var departmentErrorLabel = "#lbcvDepartment";
            var DepartmentAuthorizationVM = self.CurrentLegData.DepartmentAuthorization;
            var authorizationLabel = "#lbcvAuthorization";
            $(authorizationLabel).html("");
            Department_Validate_Retrieve_KO(self.CurrentLegData.Department, self.CurrentLegData.DepartmentID, departmentErrorLabel, btn, DepartmentAuthorizationVM, authorizationLabel)
            End_Loading(btn, true);
        }
        else {
            CurrentLegData.Department.DepartmentCD("");
            CurrentLegData.Department.DepartmentID("");
            CurrentLegData.DepartmentID("");
            CurrentLegData.Department.DepartmentName("");
        }
    }
}
function AuthorizationValidator() {

    var VM = CurrentLegData;
    var authorizationErrorLabel = "#lbcvAuthorization";
    var btn = document.getElementById("btnAuthorization");

    if (!IsNullOrEmpty(VM.DepartmentAuthorization.AuthorizationCD())) {
        if (IsNullOrEmpty(VM.DepartmentID()) || IsNullOrEmpty(VM.Department.DepartmentCD())) {            
            VM.DepartmentAuthorization.AuthorizationCD("");
            $(authorizationErrorLabel).html("Please enter the Department Code before entering the Authorization Code.");
            return;
        }
    }    
    Authorization_Validate_Retrieve_OK(VM.DepartmentAuthorization, VM.AuthorizationID, authorizationErrorLabel, btn, VM.DepartmentID, VM.Department.DepartmentCD)
}
function OnClientAuthClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvAuthorization']").text("");
            var btn = document.getElementById("btnAuthorization");
            Start_Loading(btn);
            CurrentLegData.DepartmentAuthorization.AuthorizationCD(arg.AuthorizationCD);
            CurrentLegData.DepartmentAuthorization.AuthorizationID(arg.AuthorizationID);
            CurrentLegData.AuthorizationID(arg.AuthorizationID);
            var VM = CurrentLegData;
            var authorizationErrorLabel = "#lbcvAuthorization";
            if (!IsNullOrEmpty(VM.DepartmentAuthorization.AuthorizationCD())) {
                if (IsNullOrEmpty(VM.DepartmentID()) || IsNullOrEmpty(VM.Department.DepartmentCD())) {
                    VM.DepartmentAuthorization.AuthorizationCD("");
                    $(authorizationErrorLabel).html("Please enter the Department Code before entering the Authorization Code.");
                    return;
                }
            }
            Authorization_Validate_Retrieve_OK(VM.DepartmentAuthorization, VM.AuthorizationID, authorizationErrorLabel, btn, VM.DepartmentID, VM.Department.DepartmentCD)
            End_Loading(btn, true);
        }
        else {
            CurrentLegData.DepartmentAuthorization.AuthorizationCD("");
            CurrentLegData.DepartmentAuthorization.AuthorizationID("");
            CurrentLegData.AuthorizationID("");
            CurrentLegData.DepartmentAuthorization.DeptAuthDescription("");
        }
    }
}
function fnValidateAuth() {
    if (document.getElementById("hdnDepartment").value == "" || CurrentLegData.DepartmentID() == null || CurrentLegData.DepartmentID() == 0 || CurrentLegData.DepartmentID()=="") {
        jAlert("Please enter the Department Code before entering the Authorization Code.", PostflightLegAlertTitle, function () { });
    }
    else {
        openWin('RadAuthCodePopup');
    }
    return false;
}
function CategoryValidation() {
    $("[id$='lbcvCategory']").text("");
    var btn = document.getElementById("btnCategory");
    CurrentLegData.FlightCategoryID(null);
    FlightCatagory_Validate_Retrieve_KO(CurrentLegData.FlightCatagory, CurrentLegData.FlightCategoryID, "lbcvCategory", btn);
}
function FlightCatagoryClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvCategory']").text("");
            var btn = document.getElementById("btnCategory");
            Start_Loading(btn);
            CurrentLegData.FlightCatagory.FlightCatagoryCD(arg.FlightCatagoryCD);
            CurrentLegData.FlightCatagory.FlightCategoryID(arg.FlightCategoryID);
            CurrentLegData.FlightCategoryID(arg.FlightCategoryID);
            FlightCatagory_Validate_Retrieve_KO(CurrentLegData.FlightCatagory, CurrentLegData.FlightCategoryID, "lbcvCategory", btn);
            End_Loading(btn, true);
        }
        else {
            CurrentLegData.FlightCategoryID("");
            resetKOViewModel(CurrentLegData.FlightCatagory, ["FlightCategoryID"]);
        }
    }
}
function AccountValidator() {
    $("[id$='lbcvAccountNumber']").text("");
    var btn = document.getElementById("btnAccountNo");
    var accountcvlbl = "lbcvAccountNumber";
    var accountId = CurrentLegData.AccountID;
    Account_Validate_Retrieve_KO(CurrentLegData.Account, accountId, accountcvlbl, btn);
}
function OnClientAccountClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            $("[id$='lbcvAccountNumber']").text("");
            var btn = document.getElementById("btnAccountNo");
            Start_Loading(btn);
            CurrentLegData.Account.AccountNum(arg.AccountNum);
            CurrentLegData.Account.AccountID(arg.AccountID);
            CurrentLegData.AccountID(arg.AccountID);
            var accountcvlbl = "lbcvAccountNumber";
            var accountId = CurrentLegData.AccountID;
            Account_Validate_Retrieve_KO(CurrentLegData.Account, accountId, accountcvlbl, btn);
            End_Loading(btn, true);
        }
        else {
            CurrentLegData.Account.AccountNum("");
            CurrentLegData.Account.AccountID("");
            CurrentLegData.AccountID("");
            CurrentLegData.Account.AccountDescription("");
        }
    }
}
function fnCargoCalculate(obj) {
    $("[id$='lbcvCargoThrough']").text("");
    var _cargoOut = CurrentLegData.CargoOut();
    var _cargoIn = CurrentLegData.CargoIn();
    if (_cargoOut == "") { _cargoOut = "0"; }
    if (_cargoIn == "") { _cargoIn = "0"; }
    var _cargoThrough = parseFloat(_cargoOut) - parseFloat(_cargoIn);
    if (_cargoThrough < 0) {
        jAlert("Cargo through cannot be a negative value.", PostflightLegAlertTitle, function () {
            CurrentLegData.CargoThru("0"); // _cargoThrough;
            $("[id$='lbcvCargoThrough']").text(""); // "Cargo Through Cannot Be a Negative Value.";
        });
    }
    else {
        if (isNaN(_cargoThrough))
            _cargoThrough = '';
        CurrentLegData.CargoThru(_cargoThrough);
    }
}
function changeLegType() {
    DomesticInternational_Validate_Retrieve();
}
function endofDutyDayChange() {
    if (self.POEditMode() == true) {
        FormatLegDates();
        var LegData = ko.mapping.toJS(self.CurrentLegData);
        var LegNum = self.CurrentLegData.LegNUM();
        $.ajax({
            async: true,
            cache: false,
            url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/SaveCurrentLegDataAndReturn",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'LegNUM': LegNum, 'CurrentLegData': LegData }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = result.d.Result;
                if (jsonObj != null) {
                    var LegNum = self.CurrentLegData.LegNUM();
                    var tbBlockHours = CurrentLegData.BlockHours();
                    var tbFlightHours = CurrentLegData.FlightHours();
                    var tbDutyHours = CurrentLegData.DutyHrs();
                    POEndofDuty_Check_Validate_Retrieve(LegNum, tbBlockHours, tbFlightHours, tbDutyHours);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () {
            }
        });
        return true;
    }
}
function GetDateTimeFromString(strDate, strTime) {
    var dateArray = strDate.split("-");
    var timeArray = strTime.split(":");
    var retDate = new Date(dateArray[0], dateArray[1], dateArray[2], timeArray[0], timeArray[1]);
    return retDate;
}
function setScheduleDateFromSettings() {
    var isoDate;
    if (IsNullOrEmpty(self.CurrentLegData.OutDate()))
        self.CurrentLegData.OutDate(self.CurrentLegData.ScheduledDate());

    if (IsNullOrEmpty(self.CurrentLegData.InDate()))
        self.CurrentLegData.InDate(self.CurrentLegData.OutDate());
    
    if (!IsNullOrEmpty(addDateKO))
    {
        if (isInDateKO == true) {
            isoDate = moment(addDateKO).startOf('day').add(1, 'd').format(isodateformat);
            self.CurrentLegData.InDate(isoDate);
        }
        if (isOutDateKO == true) {
            isoDate = moment(addDateKO).startOf('day').add(1, 'd').format(isodateformat);
            self.CurrentLegData.OutDate(isoDate);
        }
    }
}
function confirmScheduleDateFromSettings(arg) {
    if (arg == true) {
        var isoDate;
        if (IsNullOrEmpty(self.CurrentLegData.OutDate()))
            self.CurrentLegData.OutDate(self.CurrentLegData.ScheduledDate());

        if (IsNullOrEmpty(self.CurrentLegData.InDate()))
            self.CurrentLegData.InDate(self.CurrentLegData.OutDate());

        if (!IsNullOrEmpty(addDateKO)) {
            if (isInDateKO == true) {
                isoDate = moment(addDateKO).startOf('day').add(1, 'd').format(isodateformat);
                self.CurrentLegData.InDate(isoDate);
                isInDateKO = null;
            }
            if (isOutDateKO == true) {
                isoDate = moment(addDateKO).startOf('day').add(1, 'd').format(isodateformat);
                self.CurrentLegData.OutDate(isoDate);
                isOutDateKO = null;
            }
            addDateKO = null;
        }
    } else {
        addDateKO = null;
        isInDateKO = null;
        isOutDateKO = null;
    }
}
function ValidateAdjustmentTimeForTenMin(sender, from) {
    var ret = true;
    var val = sender.value;
    var regex;
    var formatmessage;
    if (self.UserPrincipal._TimeDisplayTenMin == "2") {
        regex = /^[0-9]{0,6}(\:[0-9]{0,2})?$/;
        formatmessage = " The Format is NNNNNN:NN";
        if (!regex.test(val)) {
            jAlert("You entered:" + val + "." + "<br/>" + "Its a Invalid Format." + "<br/>" + formatmessage, PostflightLegAlertTitle, function() {
                sender.value = '00:00';
                sender.focus();
            });
            ret = false;
        } else {
            if (val.indexOf(":") != -1) {
                var timeArray = val.split(':');
                var _minute = "00";
                if (!IsNullOrEmpty(timeArray[1]))
                    _minute = timeArray[1];
                var minute = parseInt(_minute);
                if (minute > 59) {
                    jAlert("You entered:" + val + "." + "<br/>" + "Its a Invalid Format." + "<br/>Minute entry must be between 0 and 59.", PostflightLegAlertTitle, function() {
                        sender.value = '00:00';
                        sender.focus();
                    });
                    ret = false;
                }
            } else if (val.IndexOf(":") < 0) {
               var result = "Invalid Time Format! <br/>";
                result += "Hour entry must be between 0 and 23. <br/>";
                result += "Minute entry must be between 0 and 59.";
                jAlert(result, PostflightLegAlertTitle, function() {
                    sender.value = '00:00';
                    sender.focus();
                });
                ret = false;
            }
        }
    } else {
        regex = /^[0-9]{0,8}(\.[0-9]{0,1})?$/;
        formatmessage = " The Format is NNNNNNNN.N";
        if (!regex.test(val)) {
            jAlert("You entered:" + val + "." + "<br/>" + "Its a Invalid Format." + "<br/>" + formatmessage, PostflightLegAlertTitle, function () { sender.value = '0.0'; sender.focus(); });
            ret = false;
        }
    }
    return ret;
}
function SaveCurrentLegDataToSession_Schedule_Calculation() {
    FormatLegDates();
    removeUnwantedPropertiesKOViewModel_CurrentLeg();
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    var LegNum = self.CurrentLegData.LegNUM();
    $.ajax({
        async: true,
        cache: false,
        url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/SaveCurrentLegDataAndReturn",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'LegNUM': LegNum, 'CurrentLegData': LegData }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var jsonObj = result.d.Result;
            if (jsonObj != null) {
                ScheduledHours_Calculate_Validate_Retrieve(true);
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () {
        }
    });
}
function InitializeKOWatchPostflightLeg() {
    ko.watch(self.CurrentLegData, { depth: 1, tagFields: true }, function (parents, child, item) {
        if (jQuery.inArray(child._fieldName, LegPropertyNamesList.split(",")) != -1) {
            setTimeout(function () {
                if (self.POEditMode() == true) {
                    SaveCurrentLegDataToSession();
                    UpdateCurrentLegTripEntityState(self.CurrentLegData.LegNUM());
                }
            }, 1000);
        }
    });
}

//this function is used to navigate to pop up screen's with the selected code
function openWin(radWin) {
    var url = '';

    if (radWin == "RadClientCodePopup") {
        url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('tbClient').value;
    }
    else if (radWin == "RadDepartCodePopup") {
        url = '/Views/Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + document.getElementById('tbDepartment').value;
    }
    else if (radWin == "radAccountMasterPopup") {
        url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + document.getElementById('tbAccountNumber').value;
    }
    else if (radWin == "RadAuthCodePopup") {
        url = '/Views/Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=' + document.getElementById('tbAuthorization').value + '&deptId=' + document.getElementById("hdnDepartment").value;
    }
    else if (radWin == "RadDepartPopup") {
        url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbDepart').value;
    }
    else if (radWin == "RadArrivePopup") {
        url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbArrival').value;
    }
    else if (radWin == "radPaxInfoPopup") {
        url = '/Views/Settings/People/PassengerRequestorsPopup.aspx?PassengerRequestorCD=' + document.getElementById('tbRequestor').value + "&ShowRequestor=" + document.getElementById("hdShowRequestor").value;
    }
    else if (radWin == "radCrewDutyRulesPopup") {
        url = '/Views/Settings/People/CrewDutyRulesPopup.aspx?CrewDutyRuleCD=' + document.getElementById('tbRules').value;
    }
    else if (radWin == "RadFlightCatagoryPopup") {
        url = '/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCategoryID=' + document.getElementById('hdnCategory').value;
    }
    else if (radWin == "radDelayTypePopup") {
        url = '/Views/Settings/Fleet/DelayTypePopUp.aspx';
    }

    var oWnd = radopen(url, radWin);
}
function SeeAllPopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            document.getElementById("tbDepart").value = arg.HomeBase;
            if (arg.PassengerName != "&nbsp;")
                document.getElementById("tbRequestor").value = arg.Reqstr;
            if (arg.Dept != "&nbsp;")
                document.getElementById("tbDepartment").value = arg.Dept;
            if (arg.Auth != "&nbsp;")
                document.getElementById("tbAuthorization>").value = arg.Auth;
            if (arg.FltNo != "&nbsp;")
                document.getElementById("tbFlightNo").value = arg.FltNo;
        }
    }
}
// this function is used to get the dimensions
function GetDimensions(sender, args) {
    var bounds = sender.getWindowBounds();
    return;
}
//this function is used to resize the pop-up window
function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}
//this function is used to replace default value when textbox is empty
function validateEmptyMilesTextbox(ctrlID, e) {
    if (ctrlID.value == "") {
        ctrlID.value = "0";
        self.CurrentLegData.Distance(0);
        self.CurrentLegData.ConvertedDistance(0);
    }
}

function validateEmptyStatuteMilesTextbox(ctrlID, e) {
    if (ctrlID.value == "") {
        ctrlID.value = "0";
        self.CurrentLegData.StatuteMiles(0);
    }
}

function openWinAirport(type) {
    var url = '';
    if (type == "DEPART") {
        legsType = "Depart";
        url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbDepart').value;
        var oWnd = radopen(url, 'radAirportPopup');
        oWnd.add_close(RadDepartPopupClose);
    }
    else {
        legsType = "Arrival";
        url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbArrival').value;
        var oWnd = radopen(url, 'radAirportPopup');
        oWnd.add_close(RadArrivePopupClose);
    }
}