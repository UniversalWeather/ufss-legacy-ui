﻿var self = this;
var LegPropertyNamesList = "FlightPurpose,FlightNUM,CrewDutyRulesID";
var PostflightLegAlertTitle = "Postflight Leg";

var addDateKO;
var isInDateKO;
var isOutDateKO;

var PostflightLegViewModel = function () {
    self.Legs = ko.observableArray().extend({ notify: 'always' });
    self.CurrentLegData = ko.observable().extend({ notify: 'always' });
    self.CurrentLegData.CrewDutyRule = ko.observable().extend({ notify: 'always' });
    self.CurrentLegNum = ko.observable(1);
    self.CurrentLegNumFloatbar(self.CurrentLegNum());
    self.CurrentLegNum.subscribe(function (newValue) {
        removeUnwantedPropertiesKOViewModel_CurrentLeg();
        FormatLegDates();
        var legNum = Number(newValue);
        //self.CurrentLegData.LastUpdTS = ko.observable(self.CurrentLegData.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
        FormatLegDates();
        var LegData = ko.mapping.toJS(self.CurrentLegData);
        if (legNum >= 0) {
            ClearViewModelChildren(LegData);
            RequestStart();
            self.CurrentLegNumFloatbar(legNum);
            $.ajax({
                async: true,
                url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/SavePrevLegAndReturnCurrentLegData",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'LegNUM': legNum, 'PreviousLegData': LegData }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var jsonObj = result.d.Result;
                    if (jsonObj != null) {
                        if (self.CurrentLegData.LegNUM == undefined) {
                            self.CurrentLegData = ko.mapping.fromJS(jsonObj);
                        } else {
                            ko.mapping.fromJS(jsonObj, {}, self.CurrentLegData);
                            removeUnwantedPropertiesKOViewModel_CurrentLeg();
                        }
                        
                        FormatLegDates();
                        CurrentLegData.DepartureAirport.tooltipInfo(htmlDecode(CurrentLegData.DepartureAirport.tooltipInfo()));
                        CurrentLegData.ArrivalAirport.tooltipInfo(htmlDecode(CurrentLegData.ArrivalAirport.tooltipInfo()));
                        if (IsNullOrEmptyOrUndefined(CurrentLegData.DutyTYPE())) {
                            DomesticInternational_Validate_Retrieve();
                        }
                        setAlertforAirport(self.CurrentLegData.DepartureAirport, document.getElementById("tbDepart"), 'DEPT');
                        setAlertforAirport(self.CurrentLegData.ArrivalAirport, document.getElementById("tbArrival"), 'ARR');
                        $('#tbDutyHours').attr("style", "background-color: #F1F1F1 !important;border-color: #cccccc !important;");
                        if(self.POEditMode())
                            ScheduledHours_Calculate_Validate_Retrieve(false);

                        CurrentLegData.CrewDutyRule.tooltipInfo(htmlDecode(CurrentLegData.CrewDutyRule.tooltipInfo()));
                        $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                        $("#Leg" + CurrentLegData.LegNUM()).removeClass("tabStripItem").addClass("tabStripItemSelected");
                    }
                    ResponseEnd();
                }, error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }
    });
    self.LegTab_Click = function (Leg) {
        ClearICAOErrorMessages();
        removeUnwantedPropertiesKOViewModel_CurrentLeg();
        self.CurrentLegNum(Leg.LegNUM());
        $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $("#Leg" + Leg.LegNUM()).removeClass("tabStripItem").addClass("tabStripItemSelected");
    };
    self.IsHaveLeg = ko.observable();
    self.IsKilometers = ko.pureComputed(function () {
        var isKilo = self.UserPrincipal._IsKilometer == null ? false : self.UserPrincipal._IsKilometer;
        if (isKilo) {
            return Math.floor(CurrentLegData.Distance() / 1.852);
        } else {
            return CurrentLegData.Distance;
        }
    }).extend({ notify: 'always' });
    self.DelayTMTime = ko.pureComputed(function () {
        var isTenMin = self.UserPrincipal._TimeDisplayTenMin == "2";
        if (isTenMin) {
            return "00:00";
        } else {
            return "0.00";
        }
    }).extend({ notify: 'always' });

    function InitializePostflightLegs() {
        RequestStart();
        self.getTripExceptionGrid("reload");
        var isAsync;
        var poLegData = htmlDecode($("[id$='hdnPostflightLegInitializationObject']").val());
        $("[id$='hdnPostflightLegInitializationObject']").val("");
        if (IsNullOrEmpty(poLegData) == true) {
            isAsync = false;
        } else {
            isAsync = true;
            var legJsonObj = JSON.parse(poLegData);
            self.Legs = ko.mapping.fromJS(legJsonObj.Result.Legs);
            self.CurrentLegData = ko.mapping.fromJS(legJsonObj.Result.CurrentLeg);
            self.POEditMode(legJsonObj.Result.POEditMode);
            self.IsHaveLeg(legJsonObj.IsHaveLeg);
            if (self.POEditMode() == true && IsNullOrEmptyOrUndefined(CurrentLegData.DutyTYPE())) {
                DomesticInternational_Validate_Retrieve();
            }
            FormatLegDates();
            if (self.POEditMode() == true) {
                SaveCurrentLegDataToSession_Schedule_Calculation();
            }
            InitializeBottomAndSummaryGrids();
            CurrentLegData.CrewDutyRule.tooltipInfo(htmlDecode(CurrentLegData.CrewDutyRule.tooltipInfo()));
            ResponseEnd();

            setTimeout(function () {
                var legRequested = getQuerystring("legNum", "0");
                if (!IsNullOrEmptyOrUndefined(legRequested) && self.Legs().length >= legRequested && legRequested != "0") {
                    self.CurrentLegNum(legRequested);
                }
                else if (self.CurrentLegData.LegNUM() > 1) {
                    self.CurrentLegNum(self.CurrentLegData.LegNUM());
                }
            }, 700);
        }
    }

    function HomebaseSetting() {
        var HomebaseIcaoId = self.Postflight.PostflightMain.Homebase.HomebaseCD();
        if (HomebaseIcaoId != null && HomebaseIcaoId != '') {
            $.ajax({
                async: false,
                url: "/Views/Transactions/CommonValidator.aspx/AirportDetail_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'AirportIcaoId': HomebaseIcaoId }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj != null && jsonObj.flag == 0) {
                        var airport = jsonObj.AirportData;
                        $("#hdnHomeBaseCountry").val(airport.CountryID);
                        $("#hdHomebaseAirport").val(airport.AirportID);
                        setHomebaseAirportDetail(airport);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
    
    InitializePostflightLegs();
    HomebaseSetting();

    CurrentLegData.DepartureAirport.tooltipInfo(htmlDecode(CurrentLegData.DepartureAirport.tooltipInfo()));
    CurrentLegData.ArrivalAirport.tooltipInfo(htmlDecode(CurrentLegData.ArrivalAirport.tooltipInfo()));

    setAlertforAirport(self.CurrentLegData.DepartureAirport, document.getElementById("tbDepart"), 'DEPT');
    setAlertforAirport(self.CurrentLegData.ArrivalAirport, document.getElementById("tbArrival"), 'ARR');

    self.CrewDutyToolTipInfo = ko.computed(function () {
        if (!IsNullOrEmptyOrUndefined(CurrentLegData.CrewDutyRule)) {
            return htmlDecode(CurrentLegData.CrewDutyRule.tooltipInfo());
        }
        else {
            return "";
        }
    }).extend({ notify: 'always' });
    self.DepartureAirportToolTipInfo = ko.computed(function () {
        if (!IsNullOrEmptyOrUndefined(CurrentLegData.DepartureAirport)) {
            return htmlDecode(CurrentLegData.DepartureAirport.tooltipInfo());
        }
        else {
            return "";
        }
    }).extend({ notify: 'always' });
    self.ArrivalAirportToolTipInfo = ko.computed(function () {
        if (!IsNullOrEmptyOrUndefined(CurrentLegData.ArrivalAirport)) {
            return htmlDecode(CurrentLegData.ArrivalAirport.tooltipInfo());
        }
        else {
            return "";
        }
    }).extend({ notify: 'always' });

    self.POEditModeAdjustment = ko.computed(function () {
        if (self.POEditMode() == true ) {
            if (self.UserPrincipal._IsAutoFillAF == true) 
                return false;
            else 
                return true;
        }
        else {
            return false;
        }
    }).extend({ notify: 'always' });


};

function UpdateCurrentLegTripEntityState(legNum) {
    var ajaxParam = "{'legNum':" + legNum + "}";
    $.ajax({
        async: true,
        type: "POST",
        url: "/Views/Transactions/PostFlight/PostflightTripManager.aspx/UpdateCurrentLegEntityState",
        data: ajaxParam,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function SaveCurrentLegDataToSession() {
    FormatLegDates();
    removeUnwantedPropertiesKOViewModel_CurrentLeg();
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    var LegNum = self.CurrentLegData.LegNUM();
    //RequestStart();
    //self.getTripExceptionGrid("reload");
    $.ajax({
        async: true,
        cache: false,
        url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/SaveCurrentLegDataAndReturn",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'LegNUM': LegNum, 'CurrentLegData': LegData }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var jsonObj = result.d.Result;
            if (jsonObj != null) {
                //if (self.CurrentLegData.LegNUM == undefined) {
                //    self.CurrentLegData = ko.mapping.fromJS(jsonObj.NewLeg);
                //    self.Legs = ko.mapping.fromJS(jsonObj.Legs);
                //} else {
                //    ko.mapping.fromJS(jsonObj.NewLeg, self.CurrentLegData);
                //    self.Legs.push(ko.mapping.fromJS(jsonObj.NewLeg));
                //}
                //ResponseEnd();
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            //ResponseEnd();
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () {
            //ResponseEnd(); 
        }
    });
}

function POSaveCurrentLegDataToSession() {
    FormatLegDates();
    removeUnwantedPropertiesKOViewModel_CurrentLeg();
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    var LegNum = self.CurrentLegData.LegNUM();
    $.ajax({
        async: false,
        url: "/Views/Transactions/PostFlight/PostFlightTripManager.aspx/SaveCurrentLegDataAndReturn",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'LegNUM': LegNum, 'CurrentLegData': LegData }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}

function removeUnwantedPropertiesKOViewModel_CurrentLeg() {
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.Passenger);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.FlightCatagory);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.Crew);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.Client);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.Department);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.DepartmentAuthorization);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.DelayType);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.ArrivalAirport);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.DepartureAirport);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.CrewDutyRule);
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.Account);

    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.PostflightLegPassengers());
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.PostflightLegCrews());
    removeUnwantedPropertiesKOViewModel(self.CurrentLegData.PostflightLegExpensesList());
}

function setScheduleDateFontColor() {
    $("#tbScheduledDate").css("color", "black");
    if (self.CurrentLegNum() == 1 && moment().isBefore(self.CurrentLegData.ScheduledDate(), 'day')) {
        $("#tbScheduledDate").css("color", "red");
    }
}
