﻿var self = this;
var titleConfirm = "Confirmation!";
var ExpenseLogConfirmTitle = "Flight Log Manager - Expense Catalog";
var saveSuccessMsg = "Expense Catalog has been saved successfully.";
var saveUnsuccessMsg = "Expense Catalog can not be saved.";
var msgDeleteSuccess = "Record Deleted successfully.";
var msgDeleteUnsuccess = "Record can not be deleted!";
var FBOWarningMsg = "Please enter the ICAO Code before entering the FBO Code.";


var ExpenseCatalogViewModel = function () {
    self.ECEditMode = ko.observable(false);
    self.FuelEntryEnabled = ko.observable(false);
    self.ExpenseSearch = ko.observable();
    self.ExpenseCatalog = {};
    var POLegExpense = htmlDecode($("[id$='hdnPOLegExpenseInitializationObject']").val());
    self.ExpenseCatalog = ko.mapping.fromJS(JSON.parse(POLegExpense));

    self.BrowseBtn = ko.computed(function () {
        return self.ECEditMode() ? "browse-button" : "browse-button-disabled";
    }, this);

    self.EditFuelEntry = ko.computed(function () {
        return self.ECEditMode() && self.FuelEntryEnabled();
    }, this);

    self.PrintPreviewBtn = ko.pureComputed(function () {
        return (ExpenseCatalog.SlipNUM() && self.ECEditMode() == false) ? "print_preview_icon" : "print_preview_icon_disable";
    }, self);

    self.ExportBtn = ko.pureComputed(function () {
        return (ExpenseCatalog.SlipNUM() && self.ECEditMode() == false) ? "export_icon" : "export_icon_disable";
    }, self);

    self.ExpenseCatalog.LogNum.subscribe(function () {
        if (!self.ECEditMode()) return;
        if (IsNullOrEmpty(ExpenseCatalog.LogNum())) {
            self.ExpenseCatalog.POLogID(null);
            $("#lbLogNo").text("");
            return;
        }
        var paramters = { LogNum: ExpenseCatalog.LogNum() };
        RequestStart();
        $.ajax({
            type: "POST",
            cache: false,
            url: '/Views/Transactions/PostFlight/PostFlightValidator.aspx/LogNo_PostflightValidate_Retrieve_KO',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(paramters),
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    self.ExpenseCatalog.POLogID(response.d.Result.POLogID);
                    self.ExpenseCatalog.PurchaseDT(moment(response.d.Result.PurchaseDT).startOf('day').format(isodateformat));
                    self.ExpenseCatalog.DispatchNUM(response.d.Result.DispatchNUM);
                    ko.mapping.fromJS(response.d.Result.Crew, {}, self.ExpenseCatalog.Crew);
                    ko.mapping.fromJS(response.d.Result.PostflightLeg, {}, self.ExpenseCatalog.PostflightLeg);
                    ko.mapping.fromJS(response.d.Result.FBO, {}, self.ExpenseCatalog.FBO);
                    ko.mapping.fromJS(response.d.Result.Airport, {}, self.ExpenseCatalog.Airport);
                    ko.mapping.fromJS(response.d.Result.Fleet, {}, self.ExpenseCatalog.Fleet);
                    ko.mapping.fromJS(response.d.Result.FlightCategory, {}, self.ExpenseCatalog.FlightCategory);
                    self.ExpenseCatalog.CrewID(self.ExpenseCatalog.Crew.CrewID() == 0 ? null : self.ExpenseCatalog.Crew.CrewID());
                    self.ExpenseCatalog.AirportID(self.ExpenseCatalog.Airport.AirportID() == 0 ? null : self.ExpenseCatalog.Airport.AirportID());
                    self.ExpenseCatalog.FleetID(self.ExpenseCatalog.Fleet.FleetID() == 0 ? null : self.ExpenseCatalog.Fleet.FleetID());
                    self.ExpenseCatalog.FlightCategoryID(self.ExpenseCatalog.FlightCategory.FlightCategoryID() == 0 ? null : self.ExpenseCatalog.FlightCategory.FlightCategoryID());
                    self.ExpenseCatalog.FBOID(self.ExpenseCatalog.FBO.FBOID() == 0 ? null : self.ExpenseCatalog.FBO.FBOID());
                    $("#lbLogNo").text("");
                }
                else {
                    self.ExpenseCatalog.POLogID(null);
                    $("#lbLogNo").text("Invalid Log No !");
                }

            },
            complete: function (jqXHR, textStatus) {
                ResponseEnd();
            },
            error: function (xhr, textStatus, errorThrown) {
                reportPostflightError(xhr, textStatus, errorThrown);
            }
        });
    });

    self.lnkInitEditClick = function () {
        if (ExpenseCatalog.SlipNUM() != null) {
            self.ECEditMode(true);
            enableDisableControls()
        }
        else {
            jAlert("Log should not be Empty!", ExpenseLogConfirmTitle);
        }
    };

    self.lnkInitInsertClick = function () {
        self.ECEditMode(true);
        self.ResetExpenseCatalogVM();
        AddNewExpenseCatalog();
        enableDisableControls()
    };

    self.ResetExpenseCatalogVM = function (blacklist) {
        resetKOViewModel(self.ExpenseCatalog, blacklist);
        resetKOViewModel(self.ExpenseCatalog.Homebase, blacklist);
        resetKOViewModel(self.ExpenseCatalog.Account, blacklist);
        resetKOViewModel(self.ExpenseCatalog.Fleet, blacklist);
        resetKOViewModel(self.ExpenseCatalog.Airport, blacklist);
        resetKOViewModel(self.ExpenseCatalog.FBO, blacklist);
        resetKOViewModel(self.ExpenseCatalog.FuelLocator, blacklist);
        resetKOViewModel(self.ExpenseCatalog.PaymentType, blacklist);
        resetKOViewModel(self.ExpenseCatalog.PayableVendor, blacklist);
        resetKOViewModel(self.ExpenseCatalog.Crew, blacklist);
        resetKOViewModel(self.ExpenseCatalog.FlightCategory, blacklist);
        resetKOViewModel(self.ExpenseCatalog.PostflightNote, blacklist);
        resetKOViewModel(self.ExpenseCatalog.PostflightLeg, blacklist);
    };

    self.btnCancelClick = function () {
        setAlertTextToYesNo();
        jConfirm("Are you sure you want to Cancel this record?", titleConfirm, function (r) {
            if (r) {
                GetPostFlightExpenseCatalog();
                self.ECEditMode(false);
                enableDisableControls();
            }
        });
        setAlertTextToOkCancel();
    };

    self.lnkDeleteClick = function () {
        $("#divMessage").hide();
        var POExpenseID = ExpenseCatalog.PostflightExpenseID();
        if (!IsNullOrEmptyOrUndefined(POExpenseID)) {
            setAlertTextToYesNo();
            jConfirm("Are you sure you want to delete this record?", titleConfirm, function (r) {
                if (r) {
                    if (POExpenseID != null)
                        DeleteExpenseLog(POExpenseID);
                }
            });
            setAlertTextToOkCancel();
        }
        else {
            jAlert("Log should not be Empty!", ExpenseLogConfirmTitle);
        }
    };

    self.ExpenseSearch.subscribe(function (newValue) {
        var SlipNum = newValue;
        if (!self.ECEditMode() && !IsNullOrEmpty(SlipNum)) {
            RequestStart();
            $.ajax({
                async: true,
                url: "/Views/Transactions/Postflight/ExpenseCatalog.aspx/ExpenseSearch",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'SlipNUM': SlipNum }),
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    var returnResult = verifyReturnedResult(response);
                    if (returnResult == true && response.d.Success) {
                        KODataBind(response.d.Result);
                        self.FuelEntryEnabled(ExpenseCatalog.IsAutomaticCalculation());
                        $("#btnCopyExpense").prop('disabled', false).removeClass("button-disable").addClass("button");
                        $("[id$='lbSearchMsg']").text("");
                    }
                    else {
                        $("[id$='lbSearchMsg']").text("Invalid Slip No.!");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reportPostflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () {
                    $("#tbSearchBox").val("");
                    ResponseEnd();

                }
            });
        } else {
            $("[id$='lbSearchMsg']").text("");
            if (!IsNullOrEmpty(SlipNum)) {
                jAlert("Expense Unsaved, please Save or Cancel Expense", ExpenseLogConfirmTitle);
                $("#tbSearchBox").val("");
            }
        }
    });

    self.EditFuelEntry = ko.computed(function () {
        return self.ECEditMode() && self.FuelEntryEnabled();
    }, this);

    self.btnCopyExpenseClick = function () {
        self.ECEditMode(true);
        ExpenseCatalog.PostflightExpenseID(0);
        ExpenseCatalog.SlipNUM(null);
        enableDisableControls()
    };

    self.btnSaveChangesClick = function () {
        if (IsNullOrEmpty(self.ExpenseCatalog.AccountID())) {
            $("#lbAccountNumber").text("Account No. is Required.");
            return;
        }
        self.ExpenseCatalog.PurchaseDT(formatDateISO(self.ExpenseCatalog.PurchaseDT()));
        var ExpenseCatalogData = ko.toJS(self.ExpenseCatalog);
        ExpenseCatalogData.LastUpdTS = formatDateISO(ExpenseCatalogData.LastUpdTS);
        ExpenseCatalogData.IsDeleted = false;
        if (ExpenseCatalogData.PostflightExpenseID == null) ExpenseCatalogData.PostflightExpenseID = 0;
        if (ExpenseCatalogData.SlipNUM == null) ExpenseCatalogData.SlipNUM = 0;
        if (FuelEntryEnabled()) ExpenseCatalogData.IsAutomaticCalculation = true;
        else ExpenseCatalogData.IsAutomaticCalculation = false;
        if (ExpenseCatalogData.State == null) ExpenseCatalogData.State = 0;
        if (ExpenseCatalogData.PostflightNote.State == null) ExpenseCatalogData.PostflightNote.State = 0;
        if (ExpenseCatalogData.RowNumber == null) ExpenseCatalogData.RowNumber = 0;
        if (ExpenseCatalogData.PostflightNote.PostflightNoteID == null) ExpenseCatalogData.PostflightNote.PostflightNoteID = 0;

        var clearedExpense = removeUnwantedPropertiesKOViewModel(ExpenseCatalogData);

        delete clearedExpense.Homebase;
        delete clearedExpense.PostflightLeg;
        delete clearedExpense.Account;
        delete clearedExpense.Airport;
        delete clearedExpense.Crew;
        delete clearedExpense.FBO;
        delete clearedExpense.Fleet;
        delete clearedExpense.FlightCategory;
        delete clearedExpense.FuelLocator;
        delete clearedExpense.PayableVendor;
        delete clearedExpense.PaymentType;
        delete clearedExpense.PostflightNotes;
        var param = {
            ExpenseCatalogVM: clearedExpense
        };
        RequestStart();
        $.ajax({
            type: "POST",
            cache: false,
            url: '/Views/Transactions/PostFlight/ExpenseCatalog.aspx/Save',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                   showOperationCompleteMessage(true, "save");
                    self.ECEditMode(false);
                    enableDisableControls()
                    KODataBind(response.d.Result);
                        self.FuelEntryEnabled(ExpenseCatalog.IsAutomaticCalculation());
                    $("#btnCopyExpense").prop('disabled', false).removeClass("button-disable").addClass("button");
                }
                else {
                    showOperationCompleteMessage(false, "save");                    
                }
            },
            complete: function (jqXHR, textStatus) { ResponseEnd(); },
            error: function (xhr, textStatus, errorThrown) {
                reportPostflightError(xhr, textStatus, errorThrown);
            }
        });
    }



    self.lnkExportClick = function () {
        $("#divMessage").hide();
        if (self.ECEditMode() == true || self.ExpenseCatalog.SlipNUM() == null)
            return;
        var logNo = ExpenseCatalog.LogNum() == null ? "0" : ExpenseCatalog.LogNum();
        var legNo = IsNullOrEmpty($('#ddlLegs').find('option:selected').text()) ? "0" : $('#ddlLegs').find('option:selected').text();
        var slipNo = ExpenseCatalog.SlipNUM() == null ? "0" : ExpenseCatalog.SlipNUM();
        var url = '/Views/Reports/ExportReportInformation.aspx?Report=RptPOSTExpenseCatalogExport&P1=' + logNo + '&P2=' + legNo + '&P3=' + slipNo;
        openReportPopup(url, 'rdExportReport');
    };

    self.lnkPreviewClick = function () {
        $("#divMessage").hide();
        if (self.ECEditMode() == true || self.ExpenseCatalog.SlipNUM() == null)
            return;
        var logNo = ExpenseCatalog.LogNum() == null ? "0" : ExpenseCatalog.LogNum();
        var legNo = IsNullOrEmpty($('#ddlLegs').find('option:selected').text()) ? "0" : $('#ddlLegs').find('option:selected').text();
        var slipNo = ExpenseCatalog.SlipNUM() == null ? "0" : ExpenseCatalog.SlipNUM();
        var paramters = { logNum: logNo, legNum: legNo, slipNum: slipNo };
        RequestStart();
        $.ajax({
            type: "POST",
            cache: false,
            url: '/Views/Transactions/PostFlight/ExpenseCatalog.aspx/lnkPreview_Click',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(paramters),
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                     window.open('/Views/Reports/ReportRenderView.aspx', '_blank');
                }
            },
            complete: function (jqXHR, textStatus) {
                ResponseEnd();
            },
            error: function (xhr, textStatus, errorThrown) {
                reportPostflightError(xhr, textStatus, errorThrown);
            }
        });

    };
};

function ExpenseSearchByExpenseId(expenseId) {
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Postflight/ExpenseCatalog.aspx/ExpenseSearchByExpenseId",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'ExpenseId': expenseId }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success) {
                    KODataBind(response.d.Result);
                    self.FuelEntryEnabled(ExpenseCatalog.IsAutomaticCalculation());
                    self.ECEditMode(true);
                    ExpenseCatalog.SlipNUM(null);
                    ExpenseCatalog.PostflightExpenseID(0);
                    enableDisableControls();
                    ExpenseCatalog.FromExpenseTab = true;
                }
                else {
                    $("[id$='lbSearchMsg']").text("Invalid Expense Id!");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () {
                ResponseEnd();
            }
        });
}
function GetPostFlightExpenseCatalog() {
    RequestStart();
    $.ajax({
        async: true,
        url: "/Views/Transactions/Postflight/ExpenseCatalog.aspx/GetPostFlightExpenseCatalog",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true) {
                if (response.d.Success) {
                    KODataBind(response.d.Result);
                    self.FuelEntryEnabled(ExpenseCatalog.IsAutomaticCalculation());
                    $("#btnCopyExpense").prop('disabled', false).removeClass("button-disable").addClass("button");
                }
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () {
            ResponseEnd();
        }
    });
}

function AddNewExpenseCatalog() {
    RequestStart();
    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Postflight/ExpenseCatalog.aspx/AddNewExpenseCatalog",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (response.d.Success) {
                KODataBind(response.d.Result);
            }
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function DeleteExpenseLog(POExpenseID) {
    self.ECEditMode(false);
    var paramters = { POExpenseID: POExpenseID };
    RequestStart();
    $.ajax({
        type: "POST",
        cache: false,
        url: '/Views/Transactions/PostFlight/ExpenseCatalog.aspx/DeleteExpenseLog',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(paramters),
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                $.cookie("Expense", "delete");
                refreshTrip();
            } else {
                showOperationCompleteMessage(false, "delete");
            }
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function KODataBind(data) {
    if (self.ExpenseCatalog == undefined) {
        self.ExpenseCatalog = ko.mapping.fromJS(data)
    }
    else {
        ko.mapping.fromJS(data, {}, self.ExpenseCatalog)
    }
    self.ExpenseCatalog.PurchaseDT(moment(self.ExpenseCatalog.PurchaseDT()).startOf('day').format(isodateformat));
    if (self.ExpenseCatalog.Homebase.HomebaseAirport != undefined && self.ExpenseCatalog.Homebase.HomebaseAirport != null)
        self.ExpenseCatalog.Homebase.HomebaseAirport.tooltipInfo(htmlDecode(self.ExpenseCatalog.Homebase.HomebaseAirport.tooltipInfo()));
    if (self.ExpenseCatalog.Airport != undefined && self.ExpenseCatalog.Airport != null)
        self.ExpenseCatalog.Airport.tooltipInfo(htmlDecode(self.ExpenseCatalog.Airport.tooltipInfo()));
}