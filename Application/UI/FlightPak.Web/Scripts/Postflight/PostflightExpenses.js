﻿var self = this;
var jqExpensesListGrid = "#ExpensesListGrid";
var title = "Flight Log Manager - Expenses";
var PropertyNamesList = "HomebaseID,AccountID,AccountPeriod,FleetID,PurchaseDT,AirportID,FBOID,FuelLocatorID,PaymentTypeID,PaymentVendorID,CrewID,FlightCategoryID,DispatchNUM,InvoiceNUM,ExpenseAMT,FuelPurchase,FuelQTY,UnitPrice,FederalTAX,SateTAX,SaleTAX,IsAutomaticCalculation,IsBilling,PostEDPR,PostFuelPrice,Notes";
var afterInitialization = false;

var PostflightExpensesViewModel = function () {
    self.ExpenseEditEnabled = ko.observable(false);
    self.CurrentLegExpenseData = {};
    self.Legs = {};
    self.CurrentExpenseLegNumber = ko.observable();
    self.CurrentExpenseLegNumber.subscribe(function (newValue) {
        self.CurrentLegNumFloatbar(self.CurrentExpenseLegNumber());
    });
    self.CurrentExpenseLegNumber(1);

    self.ExpenseEditMode = ko.computed(function () {
        return self.POEditMode() && self.ExpenseEditEnabled() ;
    }, this);

    self.BrowseBtnExpense = ko.computed(function () {
        return self.ExpenseEditMode()  ? "browse-button" : "browse-button-disabled";
    }, this);

    self.ExpenseButtonStyle = ko.computed(function () {
        return self.POEditMode() ? "button" : "aspNetDisabled button-disable";
    }, this);

    self.BrowseBtnFuel = ko.computed(function () {
        return self.ExpenseEditMode() && self.CurrentLegExpenseData.IsAutomaticCalculation() ? "browse-button" : "browse-button-disabled";
    }, this);

    self.CopyExpenseBtn = ko.computed(function () {
        return self.POEditMode() == false ? "button" : "aspNetDisabled button-disable";
    }, this);

    self.EditFuelEntry = ko.computed(function () {
        return self.ExpenseEditMode() && self.CurrentLegExpenseData.IsAutomaticCalculation();
    }, this);


    self.btnAddExpense_Click = function (element, event) {
        if (self.Legs == null)
        {
            $("#lbAddExpenseError").text("No Legs present");
        }
        var param = {
            legNum: self.CurrentExpenseLegNumber()
        };
        var ids = $(jqExpensesListGrid).jqGrid("getDataIDs");
        
        if (IsNullOrEmpty(self.CurrentLegExpenseData.AccountID()) || ids.length > 1) {            
                for (var i = 0; i < ids.length; i++) {
                    var rowData = $(jqExpensesListGrid).jqGrid('getRowData', ids[i]);
                    if (IsNullOrEmpty(rowData["Account.AccountDescription"])) {
                        $(jqExpensesListGrid).jqGrid('setSelection', ids[i]);
                        document.getElementById("lbAccountNumber").innerHTML = "Account number required.";
                        return;
                    }
                }            
        }


        RequestStart();

        $.ajax({
            type: "POST",
            cache: false,
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/AddNewExpense",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true) {
                    afterInitialization = false;
                    if (self.CurrentLegExpenseData.SlipNUM == undefined) {
                        self.CurrentLegExpenseData = ko.mapping.fromJS(response.d.Result);
                    }
                    else {
                        ko.mapping.fromJS(response.d.Result, self.CurrentLegExpenseData);
                    }
                    self.CurrentLegExpenseData.Homebase.HomebaseAirport.tooltipInfo(htmlDecode(self.CurrentLegExpenseData.Homebase.HomebaseAirport.tooltipInfo()));
                    self.CurrentLegExpenseData.Airport.tooltipInfo(htmlDecode(self.CurrentLegExpenseData.Airport.tooltipInfo()));
                    GetAllPostflightLegExpensesForLeg(self.CurrentExpenseLegNumber(), self.CurrentLegExpenseData.RowNumber());

                    self.ExpenseEditEnabled(true);
                    afterInitialization = true;
                }
                else {
                    jAlert(response.d.ErrorsList.join(), title);
                }
            },
            complete: function (jqXHR, textStatus) {
                ResponseEnd();
            },
            error: function (xhr, textStatus, errorThrown) {
                reportPostflightError(xhr, textStatus, errorThrown);
            }
        });

    };

    self.btnDeleteExpense_Click = function (element, event) {
        var selectedRowId = $(jqExpensesListGrid).jqGrid('getGridParam', 'selrow');
        if (!IsNullOrEmptyOrUndefined(selectedRowId)) {
            RequestStart();
            var rowNumber = jQuery(jqExpensesListGrid).jqGrid('getCell', selectedRowId, 'RowNumber');
            var param = {
                legNum: self.CurrentExpenseLegNumber(),
                expenseRowNumber: rowNumber
            };
            $.ajax({
                type: "POST",
                cache: false,
                url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/DeletePostflightLegExpense",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(param),
                success: function (response) {
                    GetAllPostflightLegExpensesForLeg(self.CurrentExpenseLegNumber());
                    MessageWhileBlankJqGrid(jqExpensesListGrid);
                },
                complete: function (jqXHR, textStatus) {
                    ResponseEnd();
                }
            });
        }
        else
        {
            jAlert("Please select an expense to delete.", title);
        }
    };

    self.ExpenseLegtabClick = function (legItem, event) {
        $("#legTabExpenses").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $(event.currentTarget).addClass("tabStripItemSelected");
        self.CurrentExpenseLegNumber(legItem.LegNUM());
        GetAllPostflightLegExpensesForLeg(legItem.LegNUM());
        $("#lbAddExpenseError").text("");
    };

    InitializePostflightExpenseVM();
    
    GetPostflightLiteLegsList();
    GetAllPostflightLegExpensesForLeg(currentTabLegNum);
    InitializeBottomAndSummaryGrids();
    getTripExceptionGrid('reload');
    afterInitialization = true;
    $("#lbAddExpenseError").text("");

    setTimeout(function () {
        $("#legTabExpenses").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $("#Leg" + currentTabLegNum).removeClass("tabStripItem").addClass("tabStripItemSelected");
        self.CurrentExpenseLegNumber(currentTabLegNum);
    }, 700);

};

function resetExpenseKOViewModel()
{
    resetKOViewModel(self.CurrentLegExpenseData);
    resetKOViewModel(self.CurrentLegExpenseData.Homebase);
    resetKOViewModel(self.CurrentLegExpenseData.Account);
    resetKOViewModel(self.CurrentLegExpenseData.Fleet);
    resetKOViewModel(self.CurrentLegExpenseData.Airport);
    resetKOViewModel(self.CurrentLegExpenseData.FBO);
    resetKOViewModel(self.CurrentLegExpenseData.FuelLocator);
    resetKOViewModel(self.CurrentLegExpenseData.PaymentType);
    resetKOViewModel(self.CurrentLegExpenseData.Crew);
    resetKOViewModel(self.CurrentLegExpenseData.FlightCategory);
    resetKOViewModel(self.CurrentLegExpenseData.PostflightNote);
    resetKOViewModel(self.CurrentLegExpenseData.PayableVendor);
}

function SavePostflightLegExpense() {
    if (self.CurrentLegExpenseData == null || self.CurrentLegExpenseData.PostflightExpenseID() == null)
        return;
    self.CurrentLegExpenseData.LastUpdTS(moment(new Date()).format(isodatetimeformat));
    self.CurrentLegExpenseData.PurchaseDT(formatDateISO(self.CurrentLegExpenseData.PurchaseDT()));
    var legExpense = ko.toJS(self.CurrentLegExpenseData);
    var rowIds = $(jqExpensesListGrid).jqGrid('getDataIDs');

    for (i = 1; i <= rowIds.length; i++) {
        rowData = $(jqExpensesListGrid).jqGrid('getRowData', i);

        if (rowData['RowNumber'] == self.CurrentLegExpenseData.RowNumber()) {
            $(jqExpensesListGrid).jqGrid('setRowData', i, legExpense);
            //$(jqExpensesListGrid).jqGrid('setSelection', i);

        } //if

    } //for
    
    var clearedExpense = removeUnwantedPropertiesKOViewModel(legExpense);
    if (IsNullOrEmptyOrUndefined(clearedExpense.AccountID))
        delete clearedExpense.Account;
    if (IsNullOrEmptyOrUndefined(clearedExpense.AirportID))
        delete clearedExpense.Airport;
    if (IsNullOrEmptyOrUndefined(clearedExpense.CrewID))
        delete clearedExpense.Crew;
    if (IsNullOrEmptyOrUndefined(clearedExpense.FBOID))
        delete clearedExpense.FBO;
    if (IsNullOrEmptyOrUndefined(clearedExpense.FleetID))
        delete clearedExpense.Fleet;
    if (IsNullOrEmptyOrUndefined(clearedExpense.FlightCategoryID))
        delete clearedExpense.FlightCategory;
    if (IsNullOrEmptyOrUndefined(clearedExpense.FuelLocatorID))
        delete clearedExpense.FuelLocator;
    if (IsNullOrEmptyOrUndefined(clearedExpense.HomebaseID))
        delete clearedExpense.Homebase;
    if (IsNullOrEmptyOrUndefined(clearedExpense.PayableVendor.VendorID))
        delete clearedExpense.PayableVendor;
    if (IsNullOrEmptyOrUndefined(clearedExpense.PaymentType.PaymentTypeID))
        delete clearedExpense.PaymentType;

    var param = {
        legNum : self.CurrentExpenseLegNumber(),
        legExpense: clearedExpense
    };

    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePostflightLegExpense",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(param),
        success: function (response) {
            //GetAllPostflightLegExpensesForLeg(self.CurrentExpenseLegNumber());
        },
        complete: function (jqXHR, textStatus) {
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });

}

function GetAllPostflightLegExpensesForLeg(legNum, selRow) {
    resetExpenseKOViewModel();
    afterInitialization = false;
    self.ExpenseEditEnabled(false);
    jQuery(jqExpensesListGrid).jqGrid('GridUnload');
    jQuery(jqExpensesListGrid).jqGrid({
        url: '/Views/Transactions/Postflight/PostflightTripManager.aspx/GetAllPostflightLegExpensesForLeg',
        mtype: 'POST',
        datatype: "json",
        async: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false, async: false },
        serializeGridData: function (postData) {
            postData.LegNum = legNum;
            return JSON.stringify(postData);
        },
        height: 164,
        width: 716,
        loadonce: true,
        autowidth: false,
        shrinkToFit: false,
        multiselect: false,
        pgbuttons: false,
        pginput: false,
        colNames: ['RowNumber','ExpenseId','Invoice No.', 'Slip<br />No.', 'Purchase Date', 'Tail No.', 'ICAO', 'Account Description', 'Expense<br/>Amount', 'Fuel<br/>Quantity',
            'Unit Price', 'Posted Price', 'Fuel Locator', 'Home Base'],
        colModel: [
                { name: 'RowNumber', index: 'RowNumber', hidden: true, key: true },
                { name: 'PostflightExpenseID', index: 'PostflightExpenseID', hidden: true},
                { name: 'InvoiceNUM', index: 'InvoiceNUM', width: 60 },
                { name: 'SlipNUM', index: 'SlipNUM', width: 25},
                {
                    name: 'PurchaseDT', index: 'PurchaseDT', width: 80, classes: "text_centeralignment", formatter: function (cellvalue, options, rowObject) {
                        if (!IsNullOrEmptyOrUndefined(cellvalue))
                            return moment(cellvalue).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
                        else
                            return "";
                    }
                },
                { name: 'Fleet.TailNum', index: 'Fleet.TailNum', width: 50, search: false },
                { name: 'Airport.IcaoID', index: 'Airport.IcaoID', width: 40, search: false },
                { name: 'Account.AccountDescription', index: 'Account.AccountDescription', width: 100, search: false },
                { name: 'ExpenseAMT', index: 'ExpenseAMT', width: 70, search: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 2, prefix: "$" }, classes: 'text_rightalignment' },
                { name: 'FuelQTY', index: 'FuelQTY', width: 70, search: false, classes: 'text_rightalignment' },
                { name: 'UnitPrice', index: 'UnitPrice', width: 71, search: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 2, prefix: "$" }, classes: 'text_rightalignment' },
                { name: 'PostFuelPrice', index: 'PostFuelPrice', width: 71, search: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 2, prefix: "$" }, classes: 'text_rightalignment' },
                { name: 'FuelLocator.FuelLocatorCD', width: 50, index: 'FuelLocator.FuelLocatorCD'},
                { name: 'Homebase.HomebaseCD', index: 'Homebase.HomebaseCD', width: 50, search: false }
        ],
        beforeRequest: function () {
            ShowJqGridLoader(jqExpensesListGrid);
        },
        loadComplete: function () {
            HideJqGridLoader(jqExpensesListGrid);

            var ids = $(jqExpensesListGrid).jqGrid("getDataIDs");
            if (ids.length > 0) {
                if (selRow == undefined || selRow == null) {
                    jQuery(this).jqGrid("setSelection", ids[0]);
                }
                else {
                    for (i = 0; i < ids.length; i++) {
                        var rowData = $(jqExpensesListGrid).jqGrid('getRowData', ids[i]);

                        if (rowData['RowNumber'] == selRow) {
                            $(jqExpensesListGrid).jqGrid('setSelection', ids[i]);
                        } //if
                    } //for
                }
            }

            MessageWhileBlankJqGrid(jqExpensesListGrid);
        },
        onSelectRow: function (id) {
            var rowNumber = $(this).jqGrid('getCell', id, 'RowNumber');
            GetPostFlightExpense(legNum, rowNumber);
        }
    });

}

function GetPostFlightExpense(legNumber, RowNumber)
{
    document.getElementById("lbAccountNumber").innerHTML = "";
    afterInitialization = false;
    var parameters = {};
    parameters.legNum = legNumber;
    parameters.rowNumber = RowNumber;
    RequestStart();
    $.ajax({
        async: false,
        cache: false,
        type: "POST",
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/GetPostflightLegExpenses",
        data: JSON.stringify(parameters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true) {
                if (self.CurrentLegExpenseData.SlipNUM == undefined) {
                    self.CurrentLegExpenseData = ko.mapping.fromJS(response.d.Result);
                }
                else
                {
                    ko.mapping.fromJS(response.d.Result, self.CurrentLegExpenseData);
                }
                if (self.CurrentLegExpenseData.Homebase.HomebaseAirport != undefined && self.CurrentLegExpenseData.Homebase.HomebaseAirport != null)
                    self.CurrentLegExpenseData.Homebase.HomebaseAirport.tooltipInfo(htmlDecode(self.CurrentLegExpenseData.Homebase.HomebaseAirport.tooltipInfo()));
                if (self.CurrentLegExpenseData.Airport != undefined && self.CurrentLegExpenseData.Airport != null)
                    self.CurrentLegExpenseData.Airport.tooltipInfo(htmlDecode(self.CurrentLegExpenseData.Airport.tooltipInfo()));
                self.CurrentLegExpenseData.PurchaseDT(moment(self.CurrentLegExpenseData.PurchaseDT()).startOf('day').format(isodateformat));
                self.ExpenseEditEnabled(true);
                setTimeout(function () { afterInitialization = true; }, 2500);
                
            }
            else {
                jAlert(response.d.ErrorsList.join(), title);
            }
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function currencyFormatter(cellValue, options, rowObject) {

    if (cellValue != null) {
        return $.fn.fmatter.call(this, "currency", cellValue, options)
    }
    else
        return "";
}
