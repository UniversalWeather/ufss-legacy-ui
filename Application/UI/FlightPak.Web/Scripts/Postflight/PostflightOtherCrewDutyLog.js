﻿var jqGridOtherCrewDuty = "#dgOtherCrewDuty";
var titleConfirm = "Confirmation!";
var OtherCrewDutyConfirmTitle = "Flight Log Manager - Other Crew Duty";
var title = "Flight Log Manager - Other Crew Duty";
var self = this;
var saveSuccessMsg = "Other Crew Duty has been saved successfully.";
var saveUnsuccessMsg = "Other Crew Duty can not be saved.";
var msgDeleteSuccess = "Record Deleted successfully.";
var msgDeleteUnsuccess = "Record can not be deleted!";
var lastSimulatorId = 0;
var currentPageNo = 0;
var scrollPosition = 0;
var saveInProgress = false;
var IsLastRecordSelection = false;
var IsCopied = false;
var OtherCrewDutyViewModel = function () {
    self.OCDEditMode = ko.observable(false);
    self.OtherCrewDuty = {};
    var OtherCrewDutyObject = htmlDecode($("[id$='hdnOtherCrewDutyInitializationObject']").val());
    self.OtherCrewDuty = ko.mapping.fromJS(JSON.parse(OtherCrewDutyObject));

    self.BrowseBtn = ko.computed(function () {
        return self.OCDEditMode() ? "browse-button" : "browse-button-disabled";
    }, this);

    self.lnkInitEditClick = function () {
        self.OCDEditMode(true);
        $("#divMessage").hide();
        enableDisableControls();
    };

    self.lnkDeleteClick = function () {
        $("#divMessage").hide();
        if (self.OtherCrewDuty.SimulatorID() != null) {
            setAlertTextToYesNo();
            jConfirm("Are you sure you want to delete this record?", titleConfirm, function (r) {
                if (r) {
                    DeleteOtherCrewDuty(self.OtherCrewDuty.SimulatorID());
                }
            });
            setAlertTextToOkCancel();
        }
        else {
            jAlert("Please select a record to delete.", title);
        }
    };

    self.lnkInitInsertClick = function () {
        self.OCDEditMode(true);
        enableDisableControls();
        self.ResetOtherCrewDuty();
        $("#divMessage").hide();
        $(".alert-text").text("");
        AddNewOtherCrewDutylog();
        jQuery(jqGridOtherCrewDuty).jqGrid('resetSelection');
    };

    self.ResetOtherCrewDuty = function (blacklist) {
        resetKOViewModel(self.OtherCrewDuty, blacklist);
        resetKOViewModel(self.OtherCrewDuty.Homebase, blacklist);
        resetKOViewModel(self.OtherCrewDuty.Crew, blacklist);
        resetKOViewModel(self.OtherCrewDuty.CrewDutyType, blacklist);
        resetKOViewModel(self.OtherCrewDuty.Aircraft, blacklist);
        resetKOViewModel(self.OtherCrewDuty.Airport, blacklist);
        resetKOViewModel(self.OtherCrewDuty.Client, blacklist);
    };

    self.btnCancelClick = function () {
        setAlertTextToYesNo();
        jConfirm("Are you sure you want to Cancel this record?", titleConfirm, function (r, blacklist) {
            if (r) {
                resetKOViewModel(self.OtherCrewDuty, blacklist);
                self.OCDEditMode(false);
                jQuery(jqGridOtherCrewDuty).trigger('reloadGrid');
                enableDisableControls();
                $(".alert-text").text("");
            }
        });
        setAlertTextToOkCancel();
    };

    self.lnkCopyClick = function () {
        $("#divMessage").hide();
        var OtherCrewDutyLog = ko.toJS(self.OtherCrewDuty);
        var OtherCrewDuty = removeUnwantedPropertiesKOViewModel(OtherCrewDutyLog);

        OtherCrewDuty.DepartureTMGMT = formatDateISO(OtherCrewDutyLog.DepartureTMGMT);
        OtherCrewDuty.ArrivalTMGMT = formatDateISO(OtherCrewDutyLog.ArrivalTMGMT);
        OtherCrewDuty.LastUptTS = formatDateISO(OtherCrewDutyLog.LastUptTS);
        OtherCrewDuty.ArrivalDTTMLocal = formatDateISO(OtherCrewDutyLog.ArrivalDTTMLocal);
        OtherCrewDuty.DepartureDTTMLocal = formatDateISO(OtherCrewDutyLog.DepartureDTTMLocal);
        OtherCrewDuty.SessionDT = formatDateISO(OtherCrewDutyLog.SessionDT);
        OtherCrewDuty.SimulatorID = 0;
        var param = {
            OtherCrewDutyVM: OtherCrewDuty
        };
        RequestStart();
        $.ajax({
            type: "POST",
            cache: false,
            url: '/Views/Transactions/PostFlight/OtherCrewDutyLog.aspx/Save',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    lastSimulatorId = response.d.Result;
					IsCopied = true;
                    jAlert("Other Crew Duty has been copied successfully.", OtherCrewDutyConfirmTitle, reloadGrid);
					self.OCDEditMode(true);
					$("#divMessage").hide();
                    enableDisableControls();
                }
                else {
					IsCopied = false;
                    jAlert("Other Crew Duty can not be copied.", OtherCrewDutyConfirmTitle);
                }
            },
            complete: function (jqXHR, textStatus) { ResponseEnd(); },
            error: function (xhr, textStatus, errorThrown) {
                reportPostflightError(xhr, textStatus, errorThrown);
            }
        });
    }

    self.btnSaveChangesClick = function () {
        var isValid = true
        if (IsNullOrEmpty(self.OtherCrewDuty.CrewID())) {
            $("#lbCrewCode").text("Crew Code is Required.");
            isValid = false;
        }
        if (IsNullOrEmpty(self.OtherCrewDuty.DutyTypeID())) {
            $("#lbDutyCode").text("Crew Duty Type Code is Required.");
            isValid = false;
        }
        if (!isValid) return;
        var OtherCrewDutyLog = ko.toJS(self.OtherCrewDuty);
        var ClearOtherCrewDuty = removeUnwantedPropertiesKOViewModel(OtherCrewDutyLog);
        ClearOtherCrewDuty.UserPrincipal = null;
        ClearOtherCrewDuty.Homebase = null;
        ClearOtherCrewDuty.Crew = null;
        ClearOtherCrewDuty.CrewDutyType = null;
        ClearOtherCrewDuty.Aircraft = null;
        ClearOtherCrewDuty.Airport = null;
        ClearOtherCrewDuty.Client = null;

        ClearOtherCrewDuty.DepartureTMGMT = formatDateISO(OtherCrewDutyLog.DepartureTMGMT);
        ClearOtherCrewDuty.ArrivalTMGMT = formatDateISO(OtherCrewDutyLog.ArrivalTMGMT);
        ClearOtherCrewDuty.LastUptTS = formatDateISO(OtherCrewDutyLog.LastUptTS);
        ClearOtherCrewDuty.ArrivalDTTMLocal = formatDateISO(OtherCrewDutyLog.ArrivalDTTMLocal);
        ClearOtherCrewDuty.DepartureDTTMLocal = formatDateISO(OtherCrewDutyLog.DepartureDTTMLocal);
        ClearOtherCrewDuty.SessionDT = formatDateISO(OtherCrewDutyLog.SessionDT);
        if (ClearOtherCrewDuty.Specification1 == null) ClearOtherCrewDuty.Specification1 = false;
        if (ClearOtherCrewDuty.Sepcification2 == null) ClearOtherCrewDuty.Sepcification2 = false;
        if (ClearOtherCrewDuty.DutyTYPEpic == null) ClearOtherCrewDuty.DutyTYPEpic = false;
        if (ClearOtherCrewDuty.DutyTYPEinstructor == null) ClearOtherCrewDuty.DutyTYPEinstructor = false;
        if (ClearOtherCrewDuty.DutyTYPEsic == null) ClearOtherCrewDuty.DutyTYPEsic = false;
        if (ClearOtherCrewDuty.DutyTYPEattendant == null) ClearOtherCrewDuty.DutyTYPEattendant = false;
        if (ClearOtherCrewDuty.DutyTYPEengineer == null) ClearOtherCrewDuty.DutyTYPEengineer = false;
        if (ClearOtherCrewDuty.DutyTYPEother == null) ClearOtherCrewDuty.DutyTYPEother = false;
        if (ClearOtherCrewDuty.SimulatorID == null) ClearOtherCrewDuty.SimulatorID = 0;
        ClearOtherCrewDuty.IsDeleted = false;

        var param = {
            OtherCrewDutyVM: ClearOtherCrewDuty
        };
        RequestStart();
        $.ajax({
            type: "POST",
            cache: false,
            url: '/Views/Transactions/PostFlight/OtherCrewDutyLog.aspx/Save',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param),
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    lastSimulatorId = response.d.Result;
                    showOperationCompleteMessage(true, "save");
                    reloadGrid();
                    self.OCDEditMode(false);
                    $(".alert-text").text("");
                }
                else {                    
                    showOperationCompleteMessage(false, "save");
                }
            },
            complete: function (jqXHR, textStatus) {
                enableDisableControls();
                ResponseEnd();
            },
            error: function (xhr, textStatus, errorThrown) {
                reportPostflightError(xhr, textStatus, errorThrown);
            }
        });
    }

    OtherCrewDutyGridBind();
};

function dataFormatter(cellvalue, options, rowObject) {
    return moment(cellvalue).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
}
function reloadPageSize() {
    var myGrid = $(jqGridOtherCrewDuty);
    var currentValue = $("#rowNum").val();
    myGrid.setGridParam({ rowNum: currentValue });
    myGrid.trigger('reloadGrid');
}

function OtherCrewDutyGridBind() {
    jQuery(jqGridOtherCrewDuty).jqGrid({
        url: '/Views/Transactions/PostFlight/OtherCrewDutyLog.aspx/OtherCrewDutyGridBind',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: true,
        viewrecords: true,
        loadonce: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }
            postData.homeBaseOnly = $("#chkHomebase").prop("checked");
            var gDate = $('#tbGoTo').val();
            postData.gotoDate = (gDate == "" ? null : moment(gDate, self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format(isodateformat));
            return JSON.stringify(postData);
        },
        autowidth: false,
        width: 707,
        rowNum: $("#rowNum").val(),
        pager: "#pg_gridPager",
        shrinkToFit: false,
        colNames: ['Crew Code', 'Crew Name', 'Session Date', 'Type Code', 'Home Base', 'SimulatorID'],
        colModel: [
            { name: 'CrewCD', index: 'CrewCD', width: 90 },
            {
                name: 'FirstName', index: 'FirstName', width: 260, formatter: function (cellvalue, options, rowObject) {
                    var lname = GetValidatedValue(rowObject, "LastName");
                    if (IsNullOrEmpty(lname) == false)
                        lname = lname + ", ";

                    return  lname + GetValidatedValue(rowObject, "FirstName") + " " + GetValidatedValue(rowObject, "MiddleInitial");
                }
            },
            { name: 'SessionDT', index: 'SessionDT', width: 95, formatter: dataFormatter },
            { name: 'AircraftCD', index: 'AircraftCD', width: 90 },
            { name: 'HomebaseCD', index: 'HomebaseCD', width: 90 },
            { name: 'SimulatorID', index: 'SimulatorID', width: 40, hidden: true, key:true }
        ],
        sortname: "SessionDT",
        sortorder: "desc",
		scrollrows:true,
        beforeSelectRow: function (rowid, e) {
            if (self.OCDEditMode())
                return false;
            return true;
        },
        onSelectRow: function (SimulatorId) {
            if (!saveInProgress)
                $("#divMessage").hide();
            saveInProgress = false;
            if (!self.OCDEditMode()) {
                GetOtherCrewDutyRowData(SimulatorId);
            }
			currentPageNo = $(".ui-pg-input").val();
            var rwid = $(jqGridOtherCrewDuty).find(">tbody>tr.jqgrow:last");
            var rows = $(jqGridOtherCrewDuty)[0].rows, lastRowDOM = rows[rows.length - 1]
            var rowsCount = rows[rows.length - 1].id;
            if (rowsCount == SimulatorId) {
                currentPageNo = parseInt(currentPageNo) + 1;
                IsLastRecordSelection = true;

            }
            else IsLastRecordSelection = false;
        },
        loadComplete: function () {
            if (jQuery(jqGridOtherCrewDuty).getGridParam("records") > 0) {
                var rowid = 0;
                if (lastSimulatorId > 0) {
                    rowid = lastSimulatorId;
                    lastSimulatorId = 0;
                }
                else
                    rowid = $(jqGridOtherCrewDuty).getDataIDs()[0];

                               
                if (this.p.datatype === 'json' && currentPageNo !== 1 && IsCopied===true) {
                    setTimeout(function () {
                        $(jqGridOtherCrewDuty).trigger("reloadGrid", [{ current: true }]);
                        jQuery(jqGridOtherCrewDuty).setSelection(rowid, true);
                    }, 50);
                }
                else
                    jQuery(jqGridOtherCrewDuty).setSelection(rowid, true)
            }
        }
    });
    jQuery("#btnSearch").click(function () {
        reloadGrid();
    });
    $("#pagesizebox").insertBefore('.ui-paging-info');
}

function reloadGrid() {
	scrollPosition = jQuery(jqGridOtherCrewDuty).closest(".ui-jqgrid-bdiv").scrollTop()
    if (IsLastRecordSelection)
        scrollPosition = 0;
    jQuery(jqGridOtherCrewDuty).setGridParam({ datatype: 'json' });
    jQuery(jqGridOtherCrewDuty).trigger("reloadGrid", [{ page: currentPageNo }]);
  }

function DeleteOtherCrewDuty(otherCrewSimulatorID) {
    self.OCDEditMode(false);
    var paramters = { OtherCrewSimulatorID: otherCrewSimulatorID };
    RequestStart();
    $.ajax({
        type: "POST",
        cache: false,
        url: '/Views/Transactions/PostFlight/OtherCrewDutyLog.aspx/DeleteOtherCrewDuty',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(paramters),
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {                
                showOperationCompleteMessage(true, "delete");
                reloadGrid();
            } else {                
                showOperationCompleteMessage(false, "delete");                
            }
            enableDisableControls();
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function AddNewOtherCrewDutylog() {
    RequestStart();
    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Postflight/OtherCrewDutyLog.aspx/AddNewOtherCrewDutylog",
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (response.d.Success) {
                if (self.OtherCrewDuty == undefined) {
                    self.OtherCrewDuty = ko.mapping.fromJS(response.d.Result)
                }
                else {
                    ko.mapping.fromJS(response.d.Result, {}, self.OtherCrewDuty)
                }
                if (self.OtherCrewDuty.Homebase.HomebaseAirport != undefined && self.OtherCrewDuty.Homebase.HomebaseAirport != null)
                    self.OtherCrewDuty.Homebase.HomebaseAirport.tooltipInfo(htmlDecode(self.OtherCrewDuty.Homebase.HomebaseAirport.tooltipInfo()));
                if (self.OtherCrewDuty.Airport != undefined && self.OtherCrewDuty.Airport != null)
                    self.OtherCrewDuty.Airport.tooltipInfo(htmlDecode(self.OtherCrewDuty.Airport.tooltipInfo()));
            }
        },
        complete: function (jqXHR, textStatus) {
            ResponseEnd();
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function GetOtherCrewDutyRowData(otherCrewSimulatorID) {
    var paramters = {};
    paramters.otherCrewSimulatorID = otherCrewSimulatorID;

    RequestStart();
    $.ajax({
        url: "/Views/Transactions/PostFlight/OtherCrewDutyLog.aspx/GetOtherCrewDutyRowData",
        type: "POST",
        dataType: "json",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true) {
                if (response.d.Result != null) {
                    if (self.OtherCrewDuty == undefined) {
                        self.OtherCrewDuty = ko.mapping.fromJS(response.d.Result)
                    }
                    else {
                        ko.mapping.fromJS(response.d.Result, {}, self.OtherCrewDuty)
                    }
                    if (self.OtherCrewDuty.Homebase.HomebaseAirport != undefined && self.OtherCrewDuty.Homebase.HomebaseAirport != null)
                        self.OtherCrewDuty.Homebase.HomebaseAirport.tooltipInfo(htmlDecode(self.OtherCrewDuty.Homebase.HomebaseAirport.tooltipInfo()));
                    if (self.OtherCrewDuty.Airport != undefined && self.OtherCrewDuty.Airport != null)
                        self.OtherCrewDuty.Airport.tooltipInfo(htmlDecode(self.OtherCrewDuty.Airport.tooltipInfo()));
                    $("#lbLastUpdatedUser").html(self.OtherCrewDuty.LastModifiedDate())
                }
            }
            return null;
        }, complete: function () { ResponseEnd(); },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });
}

function enableDisableControls() {
    var enableSorting = !self.OCDEditMode();
    jQuery(jqGridOtherCrewDuty).setColProp('CrewCD', { sortable: enableSorting });
    jQuery(jqGridOtherCrewDuty).setColProp('FirstName', { sortable: enableSorting });
    jQuery(jqGridOtherCrewDuty).setColProp('AircraftCD', { sortable: enableSorting });
    jQuery(jqGridOtherCrewDuty).setColProp('HomebaseCD', { sortable: enableSorting });
    jQuery(jqGridOtherCrewDuty).setColProp('SessionDT', { sortable: enableSorting });
    if (self.OCDEditMode()) {
        $("#pg_gridPager").hide();
        $("#btnSaveChanges,#btnCancel").removeClass("button-disable").addClass("button");
        $("#btnExpenseLog,#btnNewTrip").prop("disabled", true).addClass("ui_nav_disable").removeClass("ui_nav");
    }
    else {
        $("#pg_gridPager").show();
        $("#btnSaveChanges,#btnCancel").removeClass("button").addClass("button-disable");
        $("#btnExpenseLog,#btnNewTrip").prop("disabled", false).addClass("ui_nav").removeClass("ui_nav_disable");
    }
}


function showOperationCompleteMessage(isSuccess,operation)
{
    var msgBlock = $("#divMessage");    
    msgBlock.removeClass();
    if (isSuccess) {
        msgBlock.addClass("success_msg");
        if (operation == "save") {                        
            msgBlock.html(saveSuccessMsg);
        }
        if (operation == "delete") {
            msgBlock.html(msgDeleteSuccess);
        }
    } else {
        msgBlock.addClass("infored");
        if (operation == "save") {
            msgBlock.html(saveUnsuccessMsg);
        }
        if (operation == "delete") {
            msgBlock.html(msgDeleteUnsuccess);
        }
    }
    msgBlock.css("display", "inline");
    msgBlock.delay(10000).fadeOut(10000);
    saveInProgress = true;
}