﻿var noteUpdatedDate = new Date((new Date()) - (60000));

var currentTabLegNum = 1;
function InitializeDefaultSelectedTabFeature() {
    ko.bindingHandlers.DefaultTabSelected = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called when the binding is first applied to an element
            // Set up any initial state, event handlers, etc. here
            if (viewModel.LegNUM() == 1) {
                $(element).click();
            }
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called once when the binding is first applied to an element,
            // and again whenever any observables/computeds that are accessed change
            // Update the DOM element based on the supplied values here.
        }
    };
}
function InitializeDateFormatExtender() {

    ko.extenders.dateFormat = function (target, format) {
        //create a writable computed observable to intercept writes to our observable

        var result = ko.pureComputed({
            read: target,  //always return the original observables value
            write: function (newValue) {
                if (newValue != null) {
                    valueToWrite = moment(newValue).format(format);
                    if (valueToWrite != undefined && valueToWrite != null && valueToWrite.toUpperCase() == "INVALID DATE") {
                        valueToWrite = moment(newValue, format).format(format);
                        if (valueToWrite != undefined && valueToWrite != null && valueToWrite.toUpperCase() == "INVALID DATE") 
                                valueToWrite = "";
                    }

                    target(valueToWrite);
                }
            }
        }).extend({ notify: 'always' });
        //initialize with current value to make sure it is rounded appropriately
        result(target());
        //return the new computed observable
        return result;
    };
}

function setAlertTextToYesNo() {
    $.alerts.okButton = "Yes";
    $.alerts.cancelButton = "No";
}
function setAlertTextToOkCancel() {
    $.alerts.okButton = "OK";
    $.alerts.cancelButton = "Cancel";
}


function ShowJqGridLoader(grid) {
    grid = manageGridIDForLoader(grid);
    if ($(grid).find(".jqgrid-loader").length <= 0) {
        grid = manageGridIDForLoader(grid);

        $(grid).append($(".divloader")[0].outerHTML);
        $(grid).append("<div class='jqgrid-loader-wrapper' style='position: absolute;height: " + $(grid).height() + "px;background: rgba(255, 255, 255, 0.498039);width: 100%;top: 0;'></div>");
        $(grid).css("position:relative");
        $(grid).find(".divloader").removeClass("divloader").addClass("jqgrid-loader");
    }
    $(grid).find(".jqgrid-loader-wrapper").css("display", "block");
    $(grid).find(".jqgrid-loader").css("display", "block");
}

function HideJqGridLoader(grid) {
    grid = manageGridIDForLoader(grid);
    $(grid).find(".jqgrid-loader-wrapper").css("display", "none");
    $(grid).find(".jqgrid-loader").css("display", "none");
}

function manageGridIDForLoader(grid) {

    // check when grid is object
    if (typeof grid == "object") {
        grid = "#"+$(grid).attr("id");
    }

    if (grid.substr(0, 1) == "#") {
        var removedHashGridId = grid.substr(1);
        if (removedHashGridId.substr(0, 5) != "gbox_") {
            grid = "#gbox_" + removedHashGridId;
        } else {
            grid = "#"+removedHashGridId;
        }
    } else {
        grid = "#" + grid;
        grid = manageGridIDForLoader(grid);        
    }
    return grid;
}



function GetPostflightLegsList() {
    $.ajax({
        async: false,
        type: "POST",
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/InitializePostflightLegs",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true) {
                self.Legs = ko.mapping.fromJS(response.d.Result);
            }
            else {
                jAlert(response.d.ErrorsList.join(), "Postflight");
            }
        },
        failure: function (response) {
            //console.log(response.d)
        }
    });
}

function GetPostflightLiteLegsList() {
    $.ajax({
        async: false,
        type: "POST",
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/GetLegsLiteList",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true) {
                if (IsNullOrEmptyOrUndefined(response.d.Result.CurrentLeg) == false) {
                    currentTabLegNum = response.d.Result.CurrentLeg.LegNUM;
                }
                self.Legs = ko.mapping.fromJS(response.d.Result.Legs);
            }
            else {
                jAlert(response.d.ErrorsList.join(), "Postflight");
            }
        },
        failure: function (response) {
            //console.log(response.d)
        }
    });
}

function openWinSharedPostflight(radWin) {
    var url = '';    
    if (radWin == "rdHistory") {        
        url = "/Views/Transactions/History.aspx?FromPage=" + "Postflight";
    }
    if (url != "")
        var oWnd = radopen(url, radWin);

}

function fnAllowNumericAndOneDecimal(fieldname, myfield, e) {
    var key;
    var keychar;
    var allowedString = "0123456789" + ".";

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    if (keychar == ".") {

        if (myfield.value.indexOf(keychar) > -1) {
            return false;
        }
    }

    if (fieldname == "override") {
        if (myfield.value.length > 0) {
            if (myfield.value.indexOf(".") >= 0) {
                if (myfield.value.indexOf(".") < myfield.value.length - 1) {
                    var strarr = myfield.value.split(".");
                    if (strarr[0].length >= 2)
                        return false;
                    if (strarr[1].length > 0 && strarr[1].length == 1)
                        return false;
                }
            }
            else {
                if (myfield.value.length >= 2 && keychar != ".")
                    return false;
            }
        }
    }
    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // numeric values and allow slash("/")
    else if ((allowedString).indexOf(keychar) > -1)
        return true;
    else
        return false;
}


function reportPostflightError(jqXHR, textStatus, errorThrown) {
    if (jqXHR.status && jqXHR.status == 401) {
        window.location.href = "/Account/Login.aspx";
    }
    else if (jqXHR.status && jqXHR.status == 400) {
        jAlert(jqXHR.responseText, title);
    }
}


function EnableDisableJqGridControls(enable, jqGridId) {

    if (jqGridId.substr(0, 1) == "#") {        
        jqGridId = jqGridId.substr(1);
    }
    if (enable) {        
        $("#gbox_" + jqGridId + " input[type='checkbox'],#gbox_" + jqGridId + " input[type='text'],#gbox_" + jqGridId + " select").removeAttr("disabled");
    } else {
        $("#gbox_" + jqGridId + " input[type='checkbox'],#gbox_" + jqGridId + " input[type='text'],#gbox_" + jqGridId + " select").attr("disabled", "disabled");
    }

}

function POMaintainModelStatusBeforePageLeave(ctrl, calledFrom, callbackMethod) {
    // Find which is current page
    var tabName = getQuerystring("seltab", "POSTFLIGHT");
    tabName = tabName.toUpperCase();
    if (self.POLogID() == null) {
        return false;
    }
    if (tabName == "POSTFLIGHT" || tabName == "MAIN") {
        if (self.POEditMode() == true) {
            //SavePostflightMain();
        }
        $("#tbPostflight").addClass("tabSelected");
        return true;
    }
    else if (tabName == "LEGS") {
        if (self.POEditMode() == true) {
            POSaveCurrentLegDataToSession();
        }
        $("#tbLegs").addClass("tabSelected");
        return true;
    }
    else if (tabName == "CREW") {
        if (self.POEditMode() == true) {
          
        }
        $("#tbCrew").addClass("tabSelected");
        return true;
    }
    else if (tabName == "PAX") {
        if (self.POEditMode() == true) {
            if (callbackMethod == null) {
                callbackMethod = function () {
                    if (calledFrom == "next")
                        window.location = ctrl;  // here it is redirect url
                    else if(calledFrom=="redirect") {
                        window.location = $(ctrl).attr("href");
                    }

                };
            }
            if (IsSaveAllowFromPax == false) {
                CheckDeptPercentage(callbackMethod);
                return false;
            }
        }
        $("#tbPAX").addClass("tabSelected");
        return true;
    }
    else if (tabName == "EXPENSES") {
        if (self.POEditMode()) {

        }
        $("#tbExpenses").addClass("tabSelected");
        return true;
    }
    else if (tabName == "REPORTS") {
        $("#tbReports").addClass("tabSelected");
        return true;
    }
    return false;
}

function openPostflightWinShared(radWin) {

    var url = '';
    if (radWin == "RadSearchPopup") {
        url = '/Views/Transactions/PostFlight/PostFlightSearchAll.aspx';
    }
    else if (radWin == "RadRetrievePopup") {
        url = '/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx';
    }
    else if (radWin == "rdHistory") {

        if (self.POLogID() > 0) {
            url = "/Views/Transactions/History.aspx?FromPage=" + "Postflight";
        } else {
            jAlert("Please select a log ", 'Postflight - Trip History');
            return;
        }
    }
    var oWnd = radopen(url, radWin);
}

function InitializeKOWatchMainNotes()
{
    ko.watch(self.Postflight.PostflightMain.Notes, { depth: 1, tagFields: true }, function (parents, child, item) {        
        if (self.POEditMode() == true) {
            savePostflightLogNotes();            
        }
    });

}

function savePostflightLogNotes()
{
    var param = {
        logNotes: self.Postflight.PostflightMain.Notes()
    };

    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePostflightMainLogNotes",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(param),
        success: function (response) {
        },
        complete: function (jqXHR, textStatus) {
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPostflightError(xhr, textStatus, errorThrown);
        }
    });

}


function ShowSuccessMessage() {
    hidePostflightSystemError();
    $(document).ready(function () {
        $("#tdSuccessMessage").css("display", "inline");
        $('#tdSuccessMessage').delay(10000).fadeOut(60000);
    });    
}


function showPostflightSystemError(errorMessage) {
    HideSuccessMessage();
    $("#tdErrorMessage").text(errorMessage);
    $("#tdErrorMessage").css("display", "inline");
    $('#tdErrorMessage').delay(10000).fadeOut(60000);
}

function hidePostflightSystemError() {
    $("#tdErrorMessage").css("display", "none");
}

function verifyIfLogIsFromCalendar()
{
    //seltab=main&reqFrom=SC&tripId=1000117629 - this is a request from calendar
    var tabName = getQuerystring("seltab", "");
    var reqFrom = getQuerystring("reqFrom", "");
    var tripId = getQuerystring("tripId", "");
    var tabMatch = tabName.match(/MAIN|LEGS/i);

    if (!IsNullOrEmptyOrUndefined(tabName) && tabMatch && !IsNullOrEmptyOrUndefined(reqFrom) && !IsNullOrEmptyOrUndefined(tripId)) {
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SearchPostflightTrip",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'TripId': tripId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d;
                if (returnResult == true && jsonObj.Success == true) {
                    var Trip = jsonObj.Result;
                    self.POLogID(Trip.POLogID);
                    if (self.Postflight.PostflightMain == undefined)
                        self.Postflight = ko.mapping.fromJS(Trip.PostflightMain);
                    else
                        ko.mapping.fromJS(Trip.PostflightMain, self.Postflight);
                    hidePostflightSystemError();
                    var reloadUrl = removeQueryStringParam("reqFrom", window.location.href);
                    document.location.replace(reloadUrl);
                } else {
                    showPostflightSystemError("Invalid Log Details Received");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ResponseEnd();
                reportPostflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { ResponseEnd(); }
        });
    }

}