﻿
var PostflightMainPropertyNamesList = "IsCompleted,DispatchNum,FleetID,ClientID,FlightNum,HomebaseID,TechLog,PassengerRequestorID,AccountID,DepartmentID,AuthorizationID,POMainDescription";

$(document).ready(function () {
    getUserPrincipal();
    InitializeKOWatchPostflightMain();
    $("#tdIsCompleteBox").removeAttr("style");
    verifyIfLogIsFromCalendar();
});

function fireValidationOnPageloadForWrongCodes()
{
    if (self.Postflight.PostflightMain.DepartmentID() == null && self.Postflight.PostflightMain.Department.DepartmentCD()!=null && self.Postflight.PostflightMain.Department.DepartmentCD().length > 0) {
        self.DepartmentValidator_KO_change();
    }

    if (self.Postflight.PostflightMain.AccountID() == null && self.Postflight.PostflightMain.Account.AccountNum()!=null && self.Postflight.PostflightMain.Account.AccountNum().length > 0)
    {
        self.AccountValidate_KO_change();
    }

    if (self.Postflight.PostflightMain.ClientID() == null && self.Postflight.PostflightMain.Client.ClientCD()!=null && self.Postflight.PostflightMain.Client.ClientCD().length > 0) {
        ClientCodeValidation();
    }

    if (self.Postflight.PostflightMain.FleetID() == null && self.Postflight.PostflightMain.Fleet.TailNum()!=null && self.Postflight.PostflightMain.Fleet.TailNum().length > 0) {
        self.TailNum_PostflightValidate();
    }
    if (self.Postflight.PostflightMain.AuthorizationID() == null && self.Postflight.PostflightMain.DepartmentAuthorization.AuthorizationCD()!=null && self.Postflight.PostflightMain.DepartmentAuthorization.AuthorizationCD().length > 0) {
        self.Authorization_KO_change();
    }
    if (self.Postflight.PostflightMain.PassengerRequestorID() == null && self.Postflight.PostflightMain.Passenger.PassengerRequestorCD()!=null && self.Postflight.PostflightMain.Passenger.PassengerRequestorCD().length > 0) {
        self.RequestorValidator_KO_change();
    }

}

function ClientCodeValidation() {
    var btn = document.getElementById("btnClientCode");
    var clienttext = self.Postflight.PostflightMain.Client.ClientCD;
    var clientlbl = self.Postflight.PostflightMain.Client.ClientDescription;
    var clienthdn = self.Postflight.PostflightMain.Client.ClientID;
    clienthdn(null);
    var pfClientId = self.Postflight.PostflightMain.ClientID;
    var clientcvlbl = document.getElementById("lbcvClient");
    Client_Validate_Retrieve(clienttext, clientlbl, clienthdn, clientcvlbl, pfClientId, btn);
}


function HomebaseValidator_KO() {
    var btn = document.getElementById("btnHomeBase");
    var homebaseErrorLabel = "#lbcvHomeBase";    
    Homebase_Validate_Retrieve_KO(self.Postflight.PostflightMain.Homebase, self.Postflight.PostflightMain.HomebaseID, homebaseErrorLabel, btn);
}


function AccountValidate_KO()
{
    var accountErrorLabel = "#lbcvAccountNumber";
    var btn = document.getElementById("btnAccountNo");
    Account_Validate_Retrieve_KO(self.Postflight.PostflightMain.Account, self.Postflight.PostflightMain.AccountID, accountErrorLabel, btn);
}


function openFleetChargeHistory() {
    var oWnd = radopen("/Views/Settings/Fleet/FleetChargeHistory.aspx?IsPopup=&FromPage=postflight&FleetID=" + ConvertNullToEmptyString(self.Postflight.PostflightMain.FleetID()) + "&TailNum=" + ConvertNullToEmptyString(Postflight.PostflightMain.Fleet.TailNum()), "rdFleetChargeHistory");
}
// BugID:1172, 2703
function fnValidateAuth() {
    
    if (IsNullOrEmpty(self.Postflight.PostflightMain.DepartmentID())) {
        jAlert("Please enter the Department Code before entering the Authorization Code.", PostflightConfirmTitle);
    }
    else {
        openWin('radAuthorizationPopup');
    }
}


//this function is used to navigate to pop up screen's with the selected code
function openWin(radWin) {
    var url = '';
    if (radWin == "radCompanyMasterPopup") {
        url = '/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.Homebase.HomebaseCD());
    }
    else if (radWin == "RadClientCodePopup") {
        url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.Client.ClientCD());
    }
    else if (radWin == "radFleetProfilePopup") {
        url = '/Views/Settings/Fleet/FleetProfilePopup.aspx?TailNumber=' + ConvertNullToEmptyString(Postflight.PostflightMain.Fleet.TailNum()) + '&FromPage=postflight';
    }
    else if (radWin == "radDepartmentPopup") {
        url = '/Views/Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.Department.DepartmentCD()) ;
    }
    else if (radWin == "radAccountMasterPopup") {
        url = '/Views/Settings/Company/AccountMasterPopup.aspx?AccountNum=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.Account.AccountNum());
    }
    else if (radWin == "radAuthorizationPopup") {
        url = '/Views/Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.DepartmentAuthorization.AuthorizationCD()) + '&deptId=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.DepartmentID());
    }
    else if (radWin == "radPaxInfoPopup") {
        url = '/Views/Settings/People/PassengerRequestorsPopup.aspx?PassengerRequestorCD=' + ConvertNullToEmptyString(self.Postflight.PostflightMain.Passenger.PassengerRequestorCD()) + "&ShowRequestor=" + document.getElementById("hdShowRequestor").value;
    }

    var oWnd = radopen(url, radWin);

}

// this function is used to display the value of selected homebase code from popup
function HomeBasePopupClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            
            var btn = document.getElementById("btnHomeBase");
            Start_Loading(btn);
            document.getElementById("lbcvHomeBase").innerHTML = "";
            self.Postflight.PostflightMain.Homebase.BaseDescription(arg.BaseDescription);
            self.Postflight.PostflightMain.Homebase.HomebaseCD(arg.HomeBase);
            self.Postflight.PostflightMain.HomebaseID(arg.HomebaseID);
            End_Loading(btn, true);
        }
    }
}


// this function is used to display the value of selected client code from popup
function ClientCodePopupClose(oWnd, args) {
    var combo = $find("tbClient");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        
        if (arg) {
            var btn = document.getElementById("btnClientCode");
            Start_Loading(btn);
            if (arg.ClientDescription != null)
                self.Postflight.PostflightMain.Client.ClientDescription(arg.ClientDescription);

            
            self.Postflight.PostflightMain.Client.ClientID(arg.ClientID);

            if (arg.ClientCD != null)
                document.getElementById("lbcvClient").innerHTML = "";
            self.Postflight.PostflightMain.Client.ClientCD(arg.ClientCD);
            self.Postflight.PostflightMain.ClientID(arg.ClientID);
            End_Loading(btn, true);
        }
        else {
            self.Postflight.PostflightMain.Client.ClientCD("");
            self.Postflight.PostflightMain.Client.ClientDescription("");
            self.Postflight.PostflightMain.ClientID(null);
            
        }
    }
}

// this function is used to display the value of selected TailNumber from popup
function OnClientTailNoClose(oWnd, args) {
    
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.Postflight.PostflightMain.Fleet.TailNum(arg.TailNum);

            if (arg.FleetID != null)
                self.Postflight.PostflightMain.FleetID(arg.FleetID);
            if (arg.TailNum != null)
                self.Postflight.PostflightMain.FleetID(arg.FleetID);
            document.getElementById("lbcvTailNumber").innerHTML = "";
            self.TailNum_PostflightValidate();
        }
        else {
            self.Postflight.PostflightMain.Fleet.TailNum("");
            self.Postflight.PostflightMain.FleetID(null);
        }
    }


}
function onSuccess(result) {
    Postflight.PostflightMain.TypeCode(result)    
}


function onFailure(error) {
    alert("Error in loading Aircraft Type");
}



// this function is used to display the value of selected Department code from popup
function OnClientDeptClose(oWnd, args) {
    
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        
        if (arg) {
            var btn = document.getElementById("btnDepartment");
            Start_Loading(btn);

            Postflight.PostflightMain.Department.DepartmentCD(arg.DepartmentCD);
            if (arg.DepartmentName != null)
                Postflight.PostflightMain.Department.DepartmentName(arg.DepartmentName);
                
            if (arg.DepartmentCD != null)
                document.getElementById("lbcvDepartment").innerHTML = "";
            Postflight.PostflightMain.DepartmentID(arg.DepartmentID);
            $("#lbcvAuthorization").html("");
            $("#lbcvDepartment").html("");
            resetKOViewModel(Postflight.PostflightMain.DepartmentAuthorization, ["AuthorizationID"]);
            End_Loading(btn, true);
        }
        else {
            Postflight.PostflightMain.Department.DepartmentCD("");
            Postflight.PostflightMain.Department.DepartmentName("");
            Postflight.PostflightMain.DepartmentID("");
            
        }
    }
}

// this function is used to display the value of selected AccountNumber from popup
function OnClientAccountClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        
        if (arg) {
            var btn = document.getElementById("btnAccountNo");
            Start_Loading(btn);
            self.Postflight.PostflightMain.Account.AccountDescription(arg.AccountDescription);
            self.Postflight.PostflightMain.Account.AccountNum(arg.AccountNum);
            self.Postflight.PostflightMain.AccountID(arg.AccountId);
            document.getElementById("lbcvAccountNumber").innerHTML = "";
            End_Loading(btn, true);
        }
        else {
            self.Postflight.PostflightMain.Account.AccountDescription("");
            self.Postflight.PostflightMain.Account.AccountNum("");
            self.Postflight.PostflightMain.AccountID("");
            document.getElementById("lbcvAccountNumber").innerHTML = "";
            
        }
    }
}
// this function is used to display the value of selected Authorization code from popup
function OnClientAuthClose(oWnd, args) {
    
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        
        if (arg) {
            var btn = document.getElementById("btnAuthorization");
            Start_Loading(btn);
            self.Postflight.PostflightMain.DepartmentAuthorization.AuthorizationCD(arg.AuthorizationCD);
            self.Postflight.PostflightMain.AuthorizationID(arg.AuthorizationID);
            
            if (arg.DeptAuthDescription != null)
                Postflight.PostflightMain.DepartmentAuthorization.DeptAuthDescription(arg.DeptAuthDescription);
            if (arg.AuthorizationCD != null)
                document.getElementById("lbcvAuthorization").innerHTML = "";
            End_Loading(btn, true);
        }
        else {
            self.Postflight.PostflightMain.DepartmentAuthorization.AuthorizationCD("");
            self.Postflight.PostflightMain.AuthorizationID("");
            Postflight.PostflightMain.DepartmentAuthorization.DeptAuthDescription("");
            
        }
    }
}

// this function is used to display the value of selected Requestor code from popup
function RequestorPopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        
        if (arg) {
            var btn = document.getElementById("btnRequestor");
            Start_Loading(btn);

            self.Postflight.PostflightMain.Passenger.AdditionalPhoneNum(arg.AdditionalPhoneNum)
            self.Postflight.PostflightMain.Passenger.PassengerName(arg.PassengerName)
            self.Postflight.PostflightMain.Passenger.PassengerRequestorCD(arg.PassengerRequestorCD)
            self.Postflight.PostflightMain.PassengerRequestorID(arg.PassengerRequestorID);
            $("#lbcvRequestor").html("");
            End_Loading(btn, true);
        }
        else {
            resetKOViewModel(self.Postflight.PostflightMain.Passenger, ["PassengerRequestorID", "PostflightPassengerListID", "SIFLSecurity"]);
            
        }
    }
}

function refreshPage(arg) {
    window.location = "PostFlightMain.aspx?seltab=main";
}

function OnClientFleetChargeHistory(oWnd, args) {
    self.TailNum_PostflightValidate();
}


function SavePostflightMainViewModel()
{
    self.Postflight.PostflightMain = removeUnwantedPropertiesKOViewModel(self.Postflight.PostflightMain);
    var postflightMainVM = ko.mapping.toJS(self.Postflight.PostflightMain);
    postflightMainVM.LastUpdTS = formatDateISO(postflightMainVM.LastUpdTS);
    $.ajax({
        async: true,
        url: "/Views/Transactions/Postflight/PostflightTripManager.aspx/SavePostflightMainViewModel",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ "postflightMainVM": postflightMainVM }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {            
            var returnResult = verifyReturnedResult(result);
            self.ExceptionReloadCount(parseInt(self.ExceptionReloadCount()) + 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPostflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function InitializeKOWatchPostflightMain() {
    ko.watch(self.Postflight.PostflightMain, { depth: 1, tagFields: true }, function (parents, child, item) {
        if (self.POEditMode()) {
            if (jQuery.inArray(child._fieldName, PostflightMainPropertyNamesList.split(",")) != -1 && self.Postflight.PostflightMain != undefined) {
                SavePostflightMainViewModel();
            }
        }
    });
}