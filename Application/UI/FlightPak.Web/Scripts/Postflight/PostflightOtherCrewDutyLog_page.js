﻿$(document).ready(function () {
    $("#btnRetrieveTrip,#btnDeleteLog,#btnEditTrip,#btnSaveLog,#btnCancelLog,#btnOtherCrewDuty,#toolbar").hide();
    $("[id$='rtPostFlight']").hide();
    $("[id$='pnlTrip']").hide();
    $("#tbStartTime").mask("?99:99").change(function () { self.OtherCrewDuty.DepartureDTTMLocalTime($(this).val()); });
    $("#tbEndTime").mask("?99:99").change(function () { self.OtherCrewDuty.ArrivalDTTMLocalTime($(this).val()); });
    if (self.UserPrincipal._TimeDisplayTenMin == "2") {
        $("#tbInstrument").mask("?99:99").change(function () { self.OtherCrewDuty.InstrumentTM($(this).val()); });
        $("#tbFlight").mask("?99:99").change(function () { self.OtherCrewDuty.FlightHoursTM($(this).val()); });
        $("#tbHoursNight").mask("?99:99").change(function () { self.OtherCrewDuty.NightTM($(this).val()); });
        $("#tbHoursDuty").mask("?99:99").change(function () { self.OtherCrewDuty.DutyHoursTM($(this).val()); });
    }
    var simulatorIDParam = getQuerystring("SimulatorID");
    CustomBinding();
    if (!IsNullOrEmptyOrUndefined(simulatorIDParam))
        lastSimulatorId = simulatorIDParam;
});

function CustomBinding() {
    var userPrincipal = self.UserPrincipal;
    custom1Header1 = userPrincipal._CrewLogCustomLBLShort1;
    custom1Header2 = userPrincipal._CrewLogCustomLBLShort2;
    custom1Header3 = userPrincipal._SpecificationShort3;
    custom1Header4 = userPrincipal._SpecificationShort4;

    if (IsNullOrEmpty(custom1Header1) && IsNullOrEmpty(custom1Header2) && IsNullOrEmpty(custom1Header3) && IsNullOrEmpty(custom1Header4)) {
        $("[id$='tdCustom']").hide();
    }
    else {
        if (!IsNullOrEmpty(custom1Header1) && custom1Header1 != "0") {
            $('#lbchkNav').text(custom1Header1);
        }
        else {
            $('#lbchkNav , #chkNav').hide();
        }
        if (!IsNullOrEmpty(custom1Header2) && custom1Header2 != "0") {
            $('#lbchkHld').text(custom1Header2);
           
        }
        else {
            $('#lbchkHld , #chkHld').hide();
        }
        if (!IsNullOrEmpty(custom1Header3) && custom1Header3 != "0") {
            $('#lbCR').text(custom1Header3);
           
        }
        else {
            $('#lbCR , #tbCR').hide();
        }
        if (!IsNullOrEmpty(custom1Header4) && custom1Header4 != "0") {
            $('#lbPos').text(custom1Header4);
            
        }
        else {
            $('#lbPos , #tbPos').hide();
        }
    }
}

function textboxMultilineMaxNumber(txt, maxLen) {
    try {
        if (txt.value.length > (maxLen - 1)) return false;
    } catch (e) {
    }
    return true;
}

//this function is used to replace default value when textbox is empty
function validateEmptyTimeTextbox(ctrlID, e) {
    if (ctrlID.value == "") {
        ctrlID.value = "00.00";
    }
}

//this function is used to replace default value when textbox is empty
function validateEmptyHoursTextbox(ctrlID, e) {
    if (ctrlID.value == "") {
        ctrlID.value = "0.00";
    }
}

//this function is used to replace default value when textbox is empty
function validateEmptyTakeoffTextbox(ctrlID, e) {
    if (ctrlID.value == "") {
        ctrlID.value = "0";
    }
}

//this function is used to navigate to pop up screen's with the selected code
function openWin(radWin) {
    var url = '';
    if (radWin == "radCrewRosterPopup") {
        url = '/Views/Settings/People/CrewRosterPopup.aspx?CrewCD=' + $("#tbCrew1").val();
    }
    else if (radWin == "radCrewDutyTypePopup") {
        url = '/Views/Settings/People/CrewDutyTypePopup.aspx?DutyTypeCD=' + $("#tbDuty").val();
    }
    else if (radWin == "radAircraftTypePopup") {
        url = '/Views/Settings/Fleet/AircraftTypePopUp.aspx?AircraftCD=' + $("#tbAircraftType").val();
    }
    else if (radWin == "RadClientCodePopup") {
        url = '/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=' + $("#tbClient").val();
    }
    else if (radWin == "radAirportPopup") {
        url = '/Views/Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + $("#tbICAO").val();
    }
    else if (radWin == "RadHomeBasePopup") {
        url = '/Views/Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + $("#tbHomeBase").val();
    }
    else if (radWin == "RadRetrievePopup") {
        url = '/Views/Transactions/PostFlight/PreflightSearchRetrieve.aspx';
    }
    var oWnd = radopen(url, radWin);
}

// this function is used to display the value of selected client code from popup
function CrewRosterPopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnCrew");
            Start_Loading(btn);
            self.OtherCrewDuty.Crew.CrewCD(arg.CrewCD);
            self.OtherCrewDuty.CrewID(arg.CrewID);
            self.OtherCrewDuty.Crew.DisplayName(arg.CrewName);
            $('#lbCrewCode').text('');
            End_Loading(btn, true);
        }
        else {
            self.OtherCrewDuty.Crew.CrewCD("");
            self.OtherCrewDuty.CrewID("");
            self.OtherCrewDuty.Crew.DisplayName("");
        }
    }
}

// this function is used to display the value of selected Crew Duty Type from popup
function CrewDutyTypePopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnDuty");
            Start_Loading(btn);
            self.OtherCrewDuty.CrewDutyType.DutyTypeCD(arg.DutyTypeCD);
            self.OtherCrewDuty.DutyTypeID(arg.DutyTypeID);
            self.OtherCrewDuty.CrewDutyType.DutyTypesDescription(arg.DutyTypesDescription);
            $('#lbDutyCode').text('');
            End_Loading(btn, true);
        }
        else {
            self.OtherCrewDuty.CrewDutyType.DutyTypeCD("");
            self.OtherCrewDuty.DutyTypeID("");
            self.OtherCrewDuty.CrewDutyType.DutyTypesDescription("");
        }
    }
}

// this function is used to display the value of selected client code from popup
function AircraftTypePopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnAircraftType");
            Start_Loading(btn);
            self.OtherCrewDuty.Aircraft.AircraftCD(arg.AircraftCD);
            self.OtherCrewDuty.AircraftID(arg.AircraftID);
            self.OtherCrewDuty.Aircraft.AircraftDescription(arg.FleetID);
            $('#lbAircraftTypeCode').text('');
            End_Loading(btn, true);
        }
        else {
            self.OtherCrewDuty.Aircraft.AircraftCD("");
            self.OtherCrewDuty.AircraftID("");
            self.OtherCrewDuty.Aircraft.AircraftDescription("");
        }
    }
}

// this function is used to display the value of selected client code from popup
function ClientCodePopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnClient");
            Start_Loading(btn);
            self.OtherCrewDuty.Client.ClientCD(arg.ClientCD);
            self.OtherCrewDuty.ClientID(arg.ClientID);
            self.OtherCrewDuty.Client.ClientDescription(arg.ClientDescription);
            $('#lbClientCode').text('');
            End_Loading(btn, true);
        }
        else {
            self.OtherCrewDuty.Client.ClientCD("");
            self.OtherCrewDuty.ClientID("");
            self.OtherCrewDuty.Client.ClientDescription("");
        }
    }
}

// this function is used to display the value of selected client code from popup
function AirportMasterPopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnICAO");
            Start_Loading(btn);
            self.OtherCrewDuty.Airport.IcaoID(arg.ICAO);
            self.OtherCrewDuty.AirportID(arg.AirportID);
            self.OtherCrewDuty.Airport.AirportName(arg.AirportName);
            $('#lbICAOcode').text('');
            End_Loading(btn, true);
        }
        else {
            self.OtherCrewDuty.Airport.IcaoID("");
            self.OtherCrewDuty.AirportID("");
            self.OtherCrewDuty.Airport.AirportName("");
        }
    }
}

// this function is used to display the value of selected homebase code from popup
function HomeBasePopupClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnHomeBase");
            Start_Loading(btn);
            self.OtherCrewDuty.Homebase.HomebaseCD(arg.HomeBase);
            self.OtherCrewDuty.HomeBaseID(arg.HomebaseID);
            self.OtherCrewDuty.Homebase.BaseDescription(arg.BaseDescription);
            $('#lbHomebaseCode').text('');
            End_Loading(btn, true);
        }
        else {
            self.OtherCrewDuty.Homebase.HomebaseCD("");
            self.OtherCrewDuty.HomeBaseID("");
            self.OtherCrewDuty.Homebase.BaseDescription("");
        }
    }
}

function RemoveTimeValidationMessage() {
    $("#divTimeValidateMessage").text("");
    $("#divTimeValidateMessage").hide();
}

function DateTimeChanged(data, event) {
    var time = null;
    var changedValue = null;
    var currenttextboxId = event.target.id;
    var isDepartOrArr = null;
    var isPostback;
    switch (currenttextboxId) {
        case "tbStartDate":
        case "tbStartTime":
            time = $("#tbStartTime").val();
            changedValue = "Local";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "tbStartTime");
            break;
            case "tbEndDate":
            case "tbEndTime":
                time = $("#tbEndTime").val();
                changedValue = "Local";
                isDepartOrArr = "Dep";
                isPostback = timeValidation(time, "tbEndTime");
                break;
            default:
            break;
    }
    if (isPostback) {
        RemoveTimeValidationMessage();
        GMT_Validate_Retrieve(changedValue, time, isDepartOrArr);
    }
}

function timeValidation(val, textboxId) {
    var strInvalidTimeFormatMessage = "Invalid format, Required format is HH:MM.";
    var strInvalidHoursMessage = "Hours entry must be between 0 and 23 period";
    var strInvalidMinuteMessage = "Minute entry must be between 0 and 59 period";
    RemoveTimeValidationMessage();
    var postbackval = true;
    var hourvalue = "00";
    var minvalue = "00";
    var count = 0;
    if (IsNullOrEmpty(val))
        postbackval = false;
    if (val.indexOf(".") >= 0) {
        $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
        $("#divTimeValidateMessage").text(strInvalidTimeFormatMessage);
        $("#divTimeValidateMessage").show();
        postbackval = false;
    }
    else if (val.indexOf(":") >= 0) {
        var strarr = val.split(":");
        if (strarr[0].length <= 2) {
            if (Math.abs(strarr[0]) > 23) {
                $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
                $("#divTimeValidateMessage").text(strInvalidHoursMessage);
                $("#divTimeValidateMessage").show();
                postbackval = false;
            } else {
                hourvalue = "00" + strarr[0];
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            }
        }
        else {
            $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
            $("#divTimeValidateMessage").text(strInvalidTimeFormatMessage);
            $("#divTimeValidateMessage").show();
            postbackval = false;
        }
        if (postbackval) {
            if (strarr[1].length <= 2) {
                if (Math.abs(strarr[1]) > 59) {
                    $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
                    $("#divTimeValidateMessage").text(strInvalidMinuteMessage);
                    $("#divTimeValidateMessage").show();
                    hourvalue = "00";
                    minvalue = "00";
                    postbackval = false;
                } else {
                    minvalue = "00" + strarr[1];
                    minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                }
            } else {
                $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
                $("#divTimeValidateMessage").text(strInvalidMinuteMessage);
                $("#divTimeValidateMessage").show();
                postbackval = false;
            }
        }
    }
    else {
        if (val.length <= 2) {
            hourvalue = "00" + val;
            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
        }
        else {
            $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
            $("#divTimeValidateMessage").text(strInvalidTimeFormatMessage);
            $("#divTimeValidateMessage").show();
            postbackval = false;
        }
    }
    return postbackval;
}

function HomebaseValidator_KO() {
    var btn = document.getElementById("btnHomeBase");
    var homebaseErrorLabel = "#lbHomebaseCode";
    Homebase_Validate_Retrieve_KO(self.OtherCrewDuty.Homebase, self.OtherCrewDuty.HomeBaseID, homebaseErrorLabel, btn);
}

function CrewValidator_KO() {
    var btn = document.getElementById("btnCrew");
    var crewErrorLabel = "#lbCrewCode";
    Crew_Validate_Retrieve_KO(self.OtherCrewDuty.Crew, self.OtherCrewDuty.CrewID, crewErrorLabel, btn);
}

function AirportValidator_KO() {
    var btn = document.getElementById("btnICAO");
    var airportErrorLabel = "#lbICAOcode";
    Airport_Validate_Retrieve_KO(self.OtherCrewDuty.Airport, self.OtherCrewDuty.AirportID, airportErrorLabel, btn);
}

function ClientValidator_KO() {
    var btn = document.getElementById("btnClient");
    var clientErrorLabel = "#lbClientCode";
    Client_Validate_Retrieve_KO(self.OtherCrewDuty.Client, self.OtherCrewDuty.ClientID, clientErrorLabel, btn);
}

function AircraftTypeValidator_KO() {
    var btn = document.getElementById("btnAircraftType");
    var aircraftErrorLabel = "#lbAircraftTypeCode";
    AircraftType_Validate_Retrieve_KO(self.OtherCrewDuty.Aircraft, self.OtherCrewDuty.AircraftID, aircraftErrorLabel, btn);
}

function CrewDutyTypeValidator_KO() {
    var btn = document.getElementById("btnDuty");
    var dutyTypeErrorLabel = "#lbDutyCode";
    CrewDutyType_Validate_Retrieve_KO(self.OtherCrewDuty.CrewDutyType, self.OtherCrewDuty.DutyTypeID, dutyTypeErrorLabel, btn);
}
