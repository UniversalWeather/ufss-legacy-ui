﻿var TripLog;
var isodateformat = 'YYYY-MM-DD';
var isodatetimeformat = 'YYYY-MM-DD HH:mm:ss';

$(function () {
    TripLog = function () {

        InitializeDateFormatExtender();
        InitializeDefaultSelectedTabFeature();        
        InitializeCostFormatterBindingHandler();
        InitializeDatepickerBindingHandler();
        InitializeKOtouppercase();
        InitializeTimepickerBindingHandler();
        InitializeKOTrimmedFunction();
        InitializeFormatSingleDecimalBindingHandler();
        InitializeFormatDoubleDecimalBindingHandler();

        var currentTab = getQuerystring("seltab", "main");
        
        var main = new PostflightViewModel();
        
        if (typeof OtherCrewDutyViewModel == "function")
            var other = new OtherCrewDutyViewModel();

        if (typeof POSearchAllViewModel == "function")
            var POSearchAll = new POSearchAllViewModel();

        if (typeof ExpenseCatalogViewModel == "function")
            var ExpenseCatalog = new ExpenseCatalogViewModel();

        if (typeof POSearchAllExpensesViewModel == "function")
            var POSearchAllExpenses = new POSearchAllExpensesViewModel();

        if (typeof PreflightRetrieveViewModel == "function")
            var PFRetrieveViewModel = new PreflightRetrieveViewModel();

        var leg = null;
        if (currentTab == "legs") {
            leg = new PostflightLegViewModel();
        }

        var crew = null;
        if (currentTab == "crew") {
            crew = new PostflightCrewViewModel();
        }

        var pax = null;
        if (currentTab == "pax") {
            pax = new PostflightPaxViewModel();
        }
        var expenses = null;
        if (currentTab == "expenses") {
            expenses = new PostflightExpensesViewModel();
        }
        return {
            main: this.main,
            leg: this.leg,
            pax: this.pax,
            expenses: this.expenses
        };
    };
    ko.applyBindings(new TripLog());
});

function RequestStart() {
    $("#LoadingSpinner").height($("[id$='DivExternalForm']").height() + "px");
    $("#LoadingSpinner").show();
}

function ResponseEnd() {
    $("#LoadingSpinner").hide();
}

