﻿(function ($) {
    jQuery.fn.createSelect2AutoComplete = function (options) {
        // default settings:
        var defaults = {
            servicePageUrl: "/Views/Utilities/ApiCallerWithFilter.aspx",
            idField: "id",
            textField: "text",
            textHiddenId: "",
            valueHiddenId: "",
            searchFieldName: "",
            postData: new Object(),
            formatFunction: f_fun,
            formatSelection: f_Selection,
            extraParaNames:""
        };
        var optns = $.extend({}, defaults, options);
        $(this).select2({
            ajax: {
                url: optns.servicePageUrl,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var rules = new Object();
                    if (optns.searchFieldName != "") {
                        rules["groupOp"] = "AND";
                        var f = [];
                        var fields = optns.searchFieldName.split('|');
                        for (i = 0; i < fields.length; i++) {
                            var Rule = new Object();
                            Rule["field"] = fields[i];
                            Rule["op"] = "bw";
                            Rule["data"] = params;
                            f.push(Rule);
                        }
                        rules["rules"] = f;
                    }
                    optns.postData["filters"] = JSON.stringify(rules);
                    optns.postData["page"] = 1;
                    optns.postData["rows"] = 500;
                    return optns.postData;
                },
                processResults: function (data) {
                    var r = [];
                    for (i = 0; i < data.results.length; i++) {
                        var a = new Object();
                        a["text"] = data.results[i][optns.textField];
                        a["id"] = data.results[i][optns.idField];
                        if (optns.extraParaNames.split('|').length > 0 && optns.extraParaNames != "")
                        {
                            for (extrapara = 0; extrapara < optns.extraParaNames.split('|').length; extrapara++)
                                a[optns.extraParaNames.split('|')[extrapara]] = data.results[i][optns.extraParaNames.split('|')[extrapara]];
                        }
                        r.push(a);
                    }
                    return {
                        results: r
                    };
                },
                cache: true
            },
            minimumInputLength: 1,
            allowClear: true,
            formatResult: optns.formatFunction,
            formatSelection:optns.formatSelection,
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $(this).on('change', function () {
            if ($(this).val() != "-1") {
                var id = $(this).val();
                if (id != "") {
                    $(optns.valueHiddenId).val(id);
                    $(optns.textHiddenId).val($(".select2-container-active .select2-chosen").text());
                    $(".select2-search-choice-close").css("display", "block");
                }
                else {
                    $(optns.valueHiddenId).val("0");
                    $(optns.textHiddenId).val("");
                    $(".select2-search-choice-close").css("display", "none");
                }
            }
            $(".select2-search-choice-close").on("click", function () {
                $(optns.valueHiddenId).val("0");
                $(optns.textHiddenId).val("");
            });
        });

        if ($(optns.valueHiddenId).val() != "0" && $(optns.valueHiddenId).val() != undefined && $(optns.valueHiddenId) != null && $(optns.valueHiddenId).val() != "") {
            $(this).select2('data', { id: $(optns.valueHiddenId).val(), text: $(optns.textHiddenId).val() });
            $(".select2-search-choice-close").css("display", "block");
        }
    };

}(jQuery));

function f_fun(data) {
    return data.text;
}
function f_Selection(data) {
    return data.text;
}