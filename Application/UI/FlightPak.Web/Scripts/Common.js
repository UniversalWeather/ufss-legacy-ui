﻿var currentPreflightLeg = 1;
var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function replaceSpecialChars(quotedString) {
    if (quotedString == undefined || quotedString == null)
        return quotedString;
    return quotedString.replace(/['"]/g, function (s) { return entityMap[s]; });
}

/*
* <summary>
* Get the querystring value by providing Query Id
* </summary>
* <param name="key">A string contains the querystring key</param>
* <param name="defaultVal">Object which get returns when there is not key</param>
**/
function getQuerystring(key, defaultVal) {
    if (defaultVal == null) {
        defaultVal = "";
    }
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null) {
        return defaultVal;
    }
    else {
        return qs[1]; 
    }
}

/*
* <summary>
* Get the Selected Value from the Radio Button
* </summary>
* <param name="radioObj">Object which has been passed</param>
**/
function getCheckedValue(radioObj) {
    if (!radioObj)
        return "";
    var radioLength = radioObj.length;
    if (radioLength == undefined)
        if (radioObj.checked)
            return radioObj.value;
        else
            return "";
    for (var i = 0; i < radioLength; i++) {
        if (radioObj[i].checked) {
            return radioObj[i].value;
        }
    }
    return "";
}

/*
* <summary>
* Function to allow only a Alphabet Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnAllowAlpha(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // alpha characters
    else if (("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ").indexOf(keychar) > -1)
        return true;
    else
        return false;
}

function fnAllowNumericAndDot(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // alpha and numric characters
    else if (("0123456789.").indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to allow only a Alphabet and Numeric Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnAllowAlphaNumeric(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // alpha and numric characters
    else if (("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ").indexOf(keychar) > -1)
        return true;
    else
        return false;
}
/*
* <summary>
* Function not to any Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnNotAllowKeys(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return false;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // no keys
    else if (("").indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to allow only a Numeric Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
* <param name="dec">Decimal Point to be passed(optional)</param>
**/
function fnAllowNumeric(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // numbers
    else if (("0123456789").indexOf(keychar) > -1)
        return true;
    else
        return false;
}
/*
Verifies that a given (trip) number is not larger than bigint(or in64.max)
*/
function fnVerifyInt64MaxLimit(n)
{
    if (n != null && n != undefined && n.toString().trim() != '')
    {
        //Max length for int64
        if (n.toString().length > 19) return false;
        //Converting to Number type which can hold larger value than int64
        var number = Number(n.toString());
        //in64.Max check
        if (number != null && number != undefined && number > 9223372036854775807) {
            return false;
        }
        return true;
    }
}
/*
* <summary>
* Function to allow Numeric with customized Characters (i.e., for Date, Phone etc. Eg: 10/12/2011, 123-456-7890)
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
* <param name="char">Character you need to allow to be passed</param>
**/
function fnAllowNumericAndChar(myfield, e, char) {
    var key;
    var keychar;
    var allowedString = "0123456789" + char;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // numeric values and allow slash("/")
    else if ((allowedString).indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to allow Alphabets, Numeric and customized Characters (i.e., for Color Specs, #FC10EE)
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
* <param name="char">Character you need to allow to be passed</param>
**/
function fnAllowAlphaNumericAndChar(myfield, e, char) {
    var key;
    var keychar;
    var allowedString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" + char;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // numeric values and allow slash("/")
    else if ((allowedString).indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to Ask for Confirmation, if you select any row to delete.
* </summary>
**/
function ProcessDelete(customMsg) {

    //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
    var grid = $find(window['gridId']);

    var msg = 'Are you sure you want to delete this record?';

    if (customMsg != null) {
        msg = customMsg;
    }

    var oManager; 

    if (window['windowManagerId'] != null) {
        oManager = $find(window['windowManagerId']);
    }

    if (grid.get_masterTableView().get_selectedItems().length > 0) {

        if (oManager == null) {
            radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
        }
        else {
            oManager.radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
        }

        return false;
        //        if (confirm(msg)) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
    }
    else {
        if (oManager == null) {
            radalert('Please select a record from the above table.', 330, 100, "Delete", "");
        }
        else {
            oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", "");
        }
        
        return false;
    }
}

function ProcessCopy() {
    var grid = $find(window['gridId']);
    var oManager; 

    if (window['windowManagerId'] != null) {
        oManager = $find(window['windowManagerId']);
    }

    if (grid.get_masterTableView().get_selectedItems().length == 0) {
        if (oManager == null) {
            radalert('Please select a record from the above table.', 330, 100, "Copy", "");
        }
        else {
            oManager.radalert('Please select a record from the above table.', 330, 100, "Copy", "");
        }
        
        return false;
    }
}

function callBackFn(confirmed) {
    if (confirmed) {
        var grid = $find(window['gridId']);
        grid.get_masterTableView().fireCommand("DeleteSelected");
    }
}

/*
* <summary>
* To check whether the row is selected in grid before update
* </summary>
**/
function ProcessUpdate() {
    var grid = $find(window['gridId']);
    var oManager; 

    if (window['windowManagerId'] != null) {
        oManager = $find(window['windowManagerId']);
    }

    if (grid.get_masterTableView().get_selectedItems().length == 0) {
        if (oManager == null) {
            radalert('Please select a record from the above table.', 330, 100, "Edit", "");
        }
        else {
            oManager.radalert('Please select a record from the above table.', 330, 100, "Edit", "");
        }
        
        return false;
    }
}

/*
* <summary>
* Function to check if any special characters are available, while copy from any source.
* If any special characters found, it will automatically removed.
* </summary>
**/
function RemoveSpecialChars(elementRef) {
    var checkValue = new String(elementRef.value);
    var newValue = '';
    var isExists = false;

    for (var i = 0; i < checkValue.length; i++) {
        var currentChar = checkValue.charAt(i);

        if ((currentChar != '`') && (currentChar != '~') && (currentChar != '!') && (currentChar != '^') && (currentChar != '&') && (currentChar != '*')
                && (currentChar != '(') && (currentChar != ')') && (currentChar != '+') && (currentChar != '=') && (currentChar != '{') && (currentChar != '}')
                && (currentChar != '[') && (currentChar != ']') && (currentChar != '?') && (currentChar != '/') && (currentChar != '<') && (currentChar != '>')
                && (currentChar != ':') && (currentChar != ';') && (currentChar != '|') && (currentChar != '#') && (currentChar != '%') && (currentChar != '\\')
                && (currentChar != '\"') && (currentChar != '\'') && (currentChar != '$') && (currentChar != '-') && (currentChar != '_') && (currentChar != '@')) {
            newValue += currentChar;
        }

        if (checkValue.indexOf(currentChar) > -1) {
            isExists = true;
        }
    }

    elementRef.value = newValue;

}

//Disable right click script
//visit http://www.rainbow.arch.scriptmania.com/scripts/
var message = "Sorry, right-click has been disabled";
///////////////////////////////////
function clickIE() { if (document.all) { (message); return false; } }
function clickNS(e) {
    if 
(document.layers || (document.getElementById && !document.all)) {
        if (e.which == 2 || e.which == 3) { (message); return false; }
    }
}
//To disable right click comment in the below code
//if (document.layers)
//{ document.captureEvents(Event.MOUSEDOWN); document.onmousedown = clickNS; }
//else { document.onmouseup = clickNS; document.oncontextmenu = clickIE; }
//document.oncontextmenu = new Function("return false")


//this function is used to parse the date entered or selected by the user
function parseDate(sender, e) {
    if (currentDatePicker != null) {
        var date = currentDatePicker.get_dateInput().parseDate(sender.value);
        var dateInput = currentDatePicker.get_dateInput();
        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
        sender.value = formattedDate;
    }
}


/*
* <summary>
* Function to allow Phone format
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnAllowPhoneFormat(myfield, e) {
    //    var key;
    //    var keychar;
    //    //add the character and special character to 'spchar' that need to be allowed for phone format
    //    var spchar = "EeXxTtNn -+()#,"; 
    //    var allowedString = "0123456789" + spchar;
    //    if (window.event)
    //        key = window.event.keyCode;
    //    else if (e)
    //        key = e.which;
    //    else
    //        return true;
    //    keychar = String.fromCharCode(key);
    //    // control keys
    //    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
    //        return true;
    //    // numeric values and allow slash("/")
    //    else if ((allowedString).indexOf(keychar) > -1)
    //        return true;
    //    else
    //        return false;
    return true;
}

function RemoveSpecialCharsInPhone(elementRef) {
    //    var checkValue = new String(elementRef.value);
    //    var newValue = '';
    //    var isExists = false;
    //    var spchar = "EeXxTtNn -+()#,";
    //    var allowedString = "0123456789" + spchar;

    //    for (var i = 0; i < checkValue.length; i++) {
    //        var currentChar = checkValue.charAt(i);
    //        
    //        if (allowedString.indexOf(currentChar) != -1) {
    //            newValue += currentChar;
    //        }

    //        if (checkValue.indexOf(currentChar) > -1) {
    //            isExists = true;
    //        }
    //    }

    //    elementRef.value = newValue;
    return true;

}


function PrintWindow(content) {
    var printWindow = window.open('', 'PrintWindow');
    printWindow.document.write('<html><head><title>Print Window - Universal Weather and Aviation</title>');
    printWindow.document.write('</head><body ><img src=\'');
    printWindow.document.write(content.src);
    printWindow.document.write('\' /></body></html>');
    printWindow.document.close();
    printWindow.print();
}

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    if (value) {
        return $('<div/>').text(value).html();
    }
    else { return ""; }
}

function htmlDecode(value) {
    if (value) {
        return $('<div/>').html(value).text();
    }
    else { return "" }
}

function browserName() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("msie") != -1) return 'Internet Explorer';
    if (agt.indexOf("chrome") != -1) return 'Chrome';
    if (agt.indexOf("opera") != -1) return 'Opera';
    if (agt.indexOf("firefox") != -1) return 'Firefox';
    if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
    if (agt.indexOf("netscape") != -1) return 'Netscape';
    if (agt.indexOf("safari") != -1) return 'Safari';
    if (agt.indexOf("staroffice") != -1) return 'Star Office';
    if (agt.indexOf("webtv") != -1) return 'WebTV';
    if (agt.indexOf("beonex") != -1) return 'Beonex';
    if (agt.indexOf("chimera") != -1) return 'Chimera';
    if (agt.indexOf("netpositive") != -1) return 'NetPositive';
    if (agt.indexOf("phoenix") != -1) return 'Phoenix';
    if (agt.indexOf("skipstone") != -1) return 'SkipStone';
    if (agt.indexOf('\/') != -1) {
        if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
            return navigator.userAgent.substr(0, agt.indexOf('\/'));
        }
        else return 'Netscape';
    } else if (agt.indexOf(' ') != -1)
        return navigator.userAgent.substr(0, agt.indexOf(' '));
    else return navigator.userAgent;
}

function SetFocus(CtrlClientID) {
    var CtrlID = document.getElementById(CtrlClientID);
    CtrlID.focus();
}
function openWin2(page, radwindow) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(page, radwindow);
    oWnd2.moveTo(oWnd2.center(), 0);
    window.setTimeout(function () {
        oWnd2.setActive(true);
    }, 0);
}
function GetRadWindowPopup() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}

function AdjustRadWidow() {
    var oWindow = GetRadWindow();
    setTimeout(function () { oWindow.autoSize(true); if ($telerik.isChrome || $telerik.isSafari) ChromeSafariFix(oWindow); }, 500);
}

//fix for Chrome/Safari due to absolute positioned popup not counted as part of the content page layout
function ChromeSafariFix(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var body = iframe.contentWindow.document.body;

    setTimeout(function () {
        var height = body.scrollHeight;
        var width = body.scrollWidth;

        var iframeBounds = $telerik.getBounds(iframe);
        var heightDelta = height - iframeBounds.height;
        var widthDelta = width - iframeBounds.width;

        if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
        if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
        oWindow.center();

    }, 310);
}

function ProcessUpdatePopup(gridControl) {
    var oManager; 

    if (window['windowManagerId'] != null) {
        oManager = $find(window['windowManagerId']);
    }

    if (gridControl.get_masterTableView().get_selectedItems().length == 0) {
        if (oManager == null) {
            radalert('Please select a record from the above table.', 330, 100, "Edit", "");
        }
        else {
            oManager.radalert('Please select a record from the above table.', 330, 100, "Edit", "");
        }
        return false;
    }
}

function ProcessDeletePopup(gridId, radwindowId) {
    //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    var grid = oWindow.GetContentFrame().contentWindow.$find(gridId);

    var msg = 'Are you sure you want to delete this record?';
    var oManager; 

    if (window['windowManagerId'] != null) {
        oManager = $find(window['windowManagerId']);
    }

    if (grid.get_masterTableView().get_selectedItems().length > 0) {

        if (oManager == null) {
            radconfirm(msg, function (confirmed) {
                if (confirmed) {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    var grid = oWindow.GetContentFrame().contentWindow.$find(gridId);
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }, 330, 100, '', 'Delete');
        }
        else {
            oManager.radconfirm(msg, function (confirmed) {
                if (confirmed) {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    var grid = oWindow.GetContentFrame().contentWindow.$find(gridId);
                    grid.get_masterTableView().fireCommand("DeleteSelected");
                }
            }, 330, 100, '', 'Delete');
        }

        return false;
        //        if (confirm(msg)) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
    }
    else {
        if (oManager == null) {
            radalert('Please select a record from the above table.', 330, 100, "Delete", "");
        }
        else {
            oManager.radalert('Please select a record from the above table.', 330, 100, "Delete", "");
        }
        return false;
    }
}
function callBackFnPopUp(confirmed, gridId) {
    if (confirmed) {
        var oWindow = null;
        if (window.radWindow) oWindow = window.radWindow;
        else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
        var grid = oWindow.GetContentFrame().contentWindow.$find(gridId);
        grid.get_masterTableView().fireCommand("DeleteSelected");
    }
}
//Code to check max length for Textbox with Multiline
function checkMaxLen(txt, maxLen) {
    try {
        if (txt.value.length > (maxLen - 1)) {
            var cont = txt.value;
            txt.value = cont.substring(0, (maxLen - 1));
            return false;
        };
    } catch (e) {
    }
}

function maxLength(field, maxChars) {
    if (field.value.length >= maxChars) {
        event.returnValue = false;
        alert("more than " + maxChars + " chars");
        return false;
    }
}

function maxLengthPaste(field, maxChars) {
    event.returnValue = false;
    if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
        alert("more than " + maxChars + " chars");
        return false;
    }
    event.returnValue = true;
}

function AjaxRequestStart_IE8Fix() {
    if ($telerik.isIE8) {
        document.documentElement.focus();
    }
}
function OnKeyPressRadNumeric(sender, eventArgs) {
    if (eventArgs.get_keyCharacter() == ".") {
        eventArgs.set_cancel(true);
        if (sender._textBoxElement.value.indexOf(",") < 0)
            sender._textBoxElement.value = sender._textBoxElement.value.replace(".", "") + ",";
    }
}

function checkRadNumericValue(sender, eventArgs) {

    var value = sender.get_value().toString();
    if (sender.get_textBoxValue().length > 0)
    {
        if (value.indexOf('.') >= 0) {
            if (value.indexOf('.') > 0) {
                var strarr = value.split('.');
                if (strarr[0].length > 3) {
                    alert("Invalid format, Required format is ###.## ");
                    sender.set_value('000.00');
                    sender.focus();
                    sender.selectAllText();

                }
                if (strarr[1].length > 2) {
                    alert("Invalid format, Required format is ###.## ");
                    sender.set_value('000.00');
                    sender.focus();
                    sender.selectAllText();

                }
            }
        }
        else {

            if ((value.length > 3)&& (value!="0")) {
                alert("Invalid format, Required format is ###.## ");
                sender.set_value('000.00');
                sender.focus();
                sender.selectAllText();


            }
        }
    }



}

function Focus(sender, args) {
    var input = sender.get_value();
    if (input.length != 0) {
        sender.focus();
        sender.selectAllText()
    }
}

function RadBlur(sender, args) {
    var input = sender.get_value().toString();
    if (input == "" || input != "0") {
        sender.set_value('0.0000');
    }
}


function checkUnitPriceRadNumericValue(sender, eventArgs) {

    var postbackval = true;
    var value = sender.get_value().toString();
    if (sender.get_textBoxValue().length > 0) {
        if (value.indexOf('.') >= 0) {
            if (value.indexOf('.') > 0) {
                var strarr = value.split('.');
                if (strarr[0].length > 6) {
                    alert("Invalid format, Required format is #####.####");
                    sender.set_value('00000.0000');
                    sender.focus();
                    sender.selectAllText();
                    postbackval = false;
                    eventArgs.set_cancel(true);
                    return false;
                    eventArgs.get_domEvent().preventDefault();

                }
                if (strarr[1].length > 4) {
                    alert("Invalid format, Required format is #####.####");
                    sender.set_value('00000.0000');
                    sender.focus();
                    sender.selectAllText();
                    postbackval = false;
                    eventArgs.set_cancel(true);
                    return false;
                    eventArgs.get_domEvent().preventDefault();

                }
            }
        }
        else {


            if ((sender.get_textBoxValue().length > 6) && (value != "0")) {
                alert("Invalid format, Required format is #####.#### ");
                sender.set_value('00000.0000');
                sender.focus();
                sender.selectAllText();
                postbackval = false;
                eventArgs.set_cancel(true);
                return false;
                eventArgs.get_domEvent().preventDefault();

            }
        }
    }

    return postbackval;

}

function checkTaxRadNumericValue(sender, eventArgs) {
    var postbackval = true;
    var value = sender.get_value().toString();
    if (sender.get_textBoxValue().length > 0) {
        if (value.indexOf('.') >= 0) {
            if (value.indexOf('.') > 0) {
                var strarr = value.split('.');
                if (strarr[0].length > 15) {
                    alert("Invalid format, Required format is ##############.##");
                    sender.set_value('00000000000000.0000');
                    sender.focus();
                    sender.selectAllText();
                    postbackval = false;
                    eventArgs.get_domEvent().preventDefault();
                }
                if (strarr[1].length > 2) {
                    alert("Invalid format, Required format is ##############.##");
                    sender.set_value('00000000000000.00');
                    sender.focus();
                    sender.selectAllText();
                    postbackval = false;
                    eventArgs.get_domEvent().preventDefault();

                }
            }
        }
        else {


            if ((sender.get_textBoxValue().length >15) && (value != "0")) {
                alert("Invalid format, Required format is ##############.## ");
                sender.set_value('00000000000000.00');
                sender.focus();
                sender.selectAllText();
                postbackval = false;
                eventArgs.get_domEvent().preventDefault();

            }
        }
    }

    return postbackval;

}


function checkQuantityRadNumericValue(myfield)
{

    var postbackval = true;
    if (myfield.value.length > 0)
    {
        if (myfield.value.indexOf(".") >= 0)
        {
            if (myfield.value.indexOf(".") > 0)
            {
                var strarr = myfield.value.split(".");
                if (strarr[0].length > 13)
                {
                    postbackval = false;
                    alert("Invalid format, Required format is ############.## ");
                    myfield.value = "0.00";
                    myfield.focus();
                    

                }
                if (strarr[1].length > 2) {
                    postbackval = false;
                    alert("Invalid format, Required format is ############.## ");
                    myfield.value = "0.00";
                    myfield.focus();
                    
                }
            }
        }
        else
        {
            if (myfield.value.length > 13) {
                postbackval = false;
                alert("Invalid format, Required format is ############.## ");
                myfield.value = "0.00";
                myfield.focus();
                
            }
        }
    }

   
    return postbackval;

}

/*
Method to disable button after click to prevent double clicking
*/
function DisableButtons() {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i in inputs) {
        if (inputs[i].type == "button" || inputs[i].type == "submit") {
            inputs[i].disabled = true;
        }
    }
}

/*
Let's keep a constant for message box and popup titles
*/
var popupTitle = "Universal Weather And Aviation";

// this function is used to get the dimensions
function GetDimensions(sender, args) {
    var bounds = sender.getWindowBounds();
    return;
}

//this function is used to resize the pop-up window
function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}

function OnPageLoadComplete(sender, eventArgs) {
    sender.remove_pageLoad();
    setTimeout(function () {
        sender.set_status('');
    }, 0);
    setTimeout(function () {
        sender.setActive(true);
    }, 0);
}

function OnPageClosed(sender, eventArgs) {
    sender.remove_close();
    if (sender.pageName == 'CrewRoster') {
        reloadCrewRoster(sender.argument);
    }
    else if(sender.pageName=='DepartmentAuthorization')
    {
        reloadDepartments(sender.argument);
    }
    else if (sender.pageName == 'AddPassengerRequestorGroup') {
        if (sender.argument != null) {
            reloadPassengersSetSelection(sender.argument);
        }
    }
    else {
        if (sender.argument != undefined && sender.argument != null) {
            if (sender.argument.indexOf("#") >= 0) {
                $(sender.argument).trigger('reloadGrid');
            }
            else {
                $('#' + sender.argument).trigger('reloadGrid');
            }
        }
    }
    if (sender.pageName != 'CrewRoster') {
        sender.argument = null;
    }
    else if (sender.pageName != 'DepartmentAuthorization') {
        sender.argument = null;
    }
    else if (sender.pageName != 'PassengerRequestorGroup') {
        sender.argument = null;
    }

}

//function checks if the browser is windows 
function isWindowsSafari()
{
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("safari/") !== -1 && ua.indexOf("windows") !== -1 && ua.indexOf("chrom") === -1) {
        return true;
    }
    else
        return false;
}

//function checks if the browser is safari on MAC
function isMacSafari()
{
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("safari/") !== -1 && ua.indexOf("macintosh") !== -1 && ua.indexOf("chrom") === -1) {
        return true;
    }
    else
        return false;

}



function openSizedWindow(page, radwindow, w, h, grid, centerPopup) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(page, radwindow);
    oWnd2.setSize(w, h);
    try { oWnd2.add_pageLoad(OnPageLoadComplete); } catch (e) { }
    oWnd2.argument = grid;
    if (page.toString().indexOf('/Views/Settings/People/CrewRoster.aspx?IsPopup=') > -1)
        oWnd2.pageName = 'CrewRoster';
    
    if (page.toString().indexOf('/Views/Settings/People/PassengerCatalog.aspx?IsPopup=Add') > -1)
        oWnd2.pageName = 'AddPassengerRequestorGroup';
    
    try {oWnd2.add_close(OnPageClosed);} catch(e) {}
    oWnd2.set_modal(true);
    oWnd2.set_visibleStatusbar(true);
    try { oWnd2.moveTo(oWnd2.center(), 0); } catch (e) { }
    oWnd2.set_status('');
    if (centerPopup) {
        oWnd2.center();
    }
}

function openSizedWindow2(page, radwindow, w, h, grid, centerPopup) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(page, radwindow);
    oWnd2.setSize(w, h);
    try { oWnd2.add_pageLoad(OnPageLoadComplete); } catch (e) { }
    oWnd2.argument = grid;
    if (page.toString().indexOf('/Views/Settings/Company/DepartmentAuthorizationCatalog.aspx') > -1)
        oWnd2.pageName = 'DepartmentAuthorization';
    try { oWnd2.add_close(OnPageClosed); } catch (e) { }
    oWnd2.set_visibleStatusbar(true);
    try { oWnd2.moveTo(oWnd2.center(),0); } catch (e) { }
    oWnd2.set_status('');
    if (centerPopup) {
        oWnd2.center();
    }
}

function popupwindow(url, title, w, h, grid) {
    var oWindow = window.GetRadWindow();
    openSizedWindow(url, oWindow, w, h, grid);
    return true;
}

function popupwindow(url, title, w, h, grid, oWindow, centerPopup) {
    openSizedWindow2(url, oWindow, w, h, grid, centerPopup);
    return true;
}

function popupwindowSearch(url, w, h, CallbackClose) {
    var oWindow = window.GetRadWindow();
    openSizedWindowSearch(url, oWindow, w, h, CallbackClose);
    return true;
}

function openSizedWindowSearch(page, radwindow, w, h, CallbackClose) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(page, radwindow);
    oWnd2.setSize(w, h);
    try { oWnd2.add_pageLoad(OnPageLoadComplete); } catch (e) { }
    try { oWnd2.add_close(CallbackClose); } catch (e) { }
    oWnd2.set_modal(true);
    oWnd2.set_visibleStatusbar(true);
    oWnd2.center();
    oWnd2.set_status('');
}

function showMessageBox(message, title) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var oManager = parentPage.GetRadWindowManager();
    var width = 300;
    var height = 120;

    if (message.length > 300) {
        width = 430;
        height = 280;
    }

    if (message.length > 1300) {
        width = 550;
        height = 400;
    }
    oManager.radalert(message, width, height, title);
}

function popupwindowLevel2(url, title, w, h, grid, elementId1, propertyName1,elementId2,propertyName2) {
    var oWindow = window.GetRadWindow();
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(url, oWindow);
    oWnd2.setSize(w, h);
    try { oWnd2.add_pageLoad(OnPageLoadCompleteLevel2); } catch (e) { }
    if (elementId1.indexOf("#") >= 0) {
        oWnd2.argumentcontrol = $(elementId1);
    }
    else {
        oWnd2.argumentcontrol = $('#' + elementId1);
    }
    if (elementId2.indexOf("#") >= 0) {
        oWnd2.argumentcontrol = $(elementId2);
    }
    else {
        oWnd2.argumentcontrol1 = $('#' + elementId2);
    }

    oWnd2.argumentField = propertyName1;
    oWnd2.argumentField1 = propertyName2;
    oWnd2.add_close(OnPageClosedLevel2);

    oWnd2.set_modal(true);
    oWnd2.set_visibleStatusbar(true);
    oWnd2.center();
    oWnd2.set_status('');
    return true;
}

function OnPageLoadCompleteLevel2(sender, eventArgs) {
    sender.remove_pageLoad();
    setTimeout(function () {
        sender.set_status('');
    }, 0);
    setTimeout(function () {
        sender.setActive(true);
    }, 0);
}

function OnPageClosedLevel2(sender, eventArgs) {
    sender.remove_close();
    var argvals = eventArgs.get_argument();
    if (sender != null && sender.argumentcontrol != null && sender.argumentField != null && argvals != null) {
        sender.argumentcontrol.val(eventArgs.get_argument()[sender.argumentField]);
    }
    if (sender != null && sender.argumentcontrol1 != null && sender.argumentField1 && argvals != null) {
        sender.argumentcontrol1.val(eventArgs.get_argument()[sender.argumentField1]);
    }
    if (sender.argumentField1 == "CrewGroupID")
    {
        reloadCrew();
    }
        
}
// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function showConfirmPopup(message, callbackfn, popupTitle) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var oManager = parentPage.GetRadWindowManager();
    oManager.radconfirm(message, callbackfn, 330, 100, '', popupTitle);
}

//Read a given URL variables and return them as an associative array.Added by Sunil 
function getUrlVarsWithParam(url) {
    var vars = [], hash;
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

//Remove Pager Property from Jqgrid.Added by Sunil
function RemovePaging(jqgridTableId) {
    var pager = $(jqgridTableId).jqGrid().getGridParam('pager');
    $(".ui-pg-button", pager).hide();
    $(".ui-pg-input", pager).hide();
    $(pager).find('table tr td[dir="ltr"]').html('');
}






// Check Client Code
function ClientCodeCheck(ClientCode) {
    var postdata = new Object();
    if (ClientCode) {
        $.ajax({
            type: 'GET',
            url: '/Views/Utilities/GETApi.aspx?apiType=FSS&method=CheckClientCode&clientCD=' + ClientCode,
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (rowData, textStatus, xhr) {
                var NotFound = rowData.indexOf('Failed to execute api');
                if (rowData != undefined && rowData != "" && rowData != null && NotFound == -1) {
                    var rowData = JSON.parse(rowData.replace(/&quot;/g, '"'));
                    var ClientID = rowData.results.ClientId;
                    var ClientDescription = rowData.results.ClientDescription;
                    var ClientCD = rowData.results.ClientCD;

                    if (ClientDescription != undefined && ClientDescription != null)
                        postdata.ClientDescription = ClientDescription;
                    if (ClientID != undefined && ClientID != null)
                        postdata.ClientID = ClientID;
                    if (ClientCD != undefined && ClientCD != null)
                        postdata.ClientCD = ClientCD;
                }
                else {
                    postdata.Error="Client Code Does Not Exist";
                    postdata.ClientID = "";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //Check for any errors
                postdata.Error = "Client Code Does Not Exist";
                postdata.ClientID = "";
            }
        });
        return postdata;
    }
}

// Check HomeBase Code
function HomeBaseCheck(ICAOId) {
    var postdata = new Object();
   
    if (ICAOId) {
        $.ajax({
            type: 'GET',
            url: '/Views/Utilities/GETApi.aspx?apiType=FSS&method=CheckHomeBase&ICAOId=' + ICAOId,
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function (rowData, textStatus, xhr) {
                if (rowData != undefined && rowData != "" && rowData != null) {
                    var rowData = JSON.parse(rowData.replace(/&quot;/g, '"'));
                    var BaseDescription = rowData.results[0].BaseDescription;
                    var HomeBaseID = rowData.results[0].HomebaseID;
                    var HomebaseCD = rowData.results[0].HomebaseCD;

                    if (BaseDescription != undefined && BaseDescription != null)
                        postdata.BaseDescription = BaseDescription;
                    if (HomeBaseID != undefined && HomeBaseID != null)
                        postdata.HomeBaseID = HomeBaseID;
                    if (HomebaseCD != undefined && HomebaseCD != null)
                        postdata.HomebaseCD = HomebaseCD;
                }
                else {
                    postdata.Error = "Homebase Code Does Not Exist";
                    postdata.HomeBaseID = "";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //Check for any errors
                postdata.Error = "Homebase Code Does Not Exist";
                postdata.HomeBaseID = "";
            }
        });
        return postdata;
    }
}

function UserIdentity(Callback)
{
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/Views/Utilities/UserPrincipalCaller.aspx',
        contentType: 'application/json',
        async: false,
        success: function (Identity, textStatus, xhr) {
            if (Identity != undefined && Identity != null) {
                Callback(Identity);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}


function GetHotelDetailByHotelCode(hotelCode, airportId) {
    var postdata = new Object();
    $.ajax({
        type: 'GET',
        url: '/Views/Utilities/GetApi.aspx?apiType=FSS&method=HotelDetailByHotelCD&hotelCode=' + hotelCode + '&airportId=' + airportId,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (rowData, textStatus, xhr) {
            verifyReturnedResultForJqgrid(rowData);
            var rowData = JSON.parse(rowData.replace(/&quot;/g, '"'));
            if (rowData.results[0] != undefined && rowData.results[0] != null) {
                    postdata.success = "true";
                    postdata.d = rowData.results[0];
            }
            else {
                postdata.Error = "Hotel Code Does Not Exist";
                postdata.success = "false";
                postdata.d = "";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //Check for any errors                        

            postdata.Error = "Hotel Code Does Not Exist";
            postdata.success = "false";
            postdata.d = "";
        }
    });

    return postdata;
}
function GetHotelDetailByHotelID(hotelid) {
    var postdata = new Object();
    $.ajax({
        type: 'GET',
        url: '/Views/Utilities/GetApi.aspx?apiType=FSS&method=HotelDetailByHotelID&hotelId=' + hotelid,
        async: false,
        contentType: 'application/json; charset=utf-8',
        success: function (rowData, textStatus, xhr) {
            verifyReturnedResultForJqgrid(rowData);
            if (rowData != undefined && rowData != null) {
                var rowData = JSON.parse(rowData.replace(/&quot;/g, '"'));
                postdata.success = "true";
                postdata.d = rowData.results[0];
            }
            else {
                postdata.Error = "Hotel Code Does Not Exist";
                postdata.success = "false";
                postdata.d = "";
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //Check for any errors             
            postdata.Error = "Hotel Code Does Not Exist";
            postdata.success = "false";
            postdata.d = "";
        }
    });
    return postdata;
}
function GetValidatedValue(dataObject, propertyName) {
    var validValue=null;
    if (dataObject.hasOwnProperty(propertyName)) {
        if (dataObject[propertyName] != null && dataObject[propertyName] != undefined)
            validValue = dataObject[propertyName];
        else
            validValue = "";
    }
    else {
        validValue = ""
    }
    return validValue;
}
var idsOfSelectedRows = [];
var selectedObjects = [];
var updateIdsOfSelectedRows = function (id, isSelected, object) {
    var contains = idsOfSelectedRows.contains(id);
    if (!isSelected && contains) {
        for (var i = 0; i < idsOfSelectedRows.length; i++) {
            if (idsOfSelectedRows[i] == id) {
                idsOfSelectedRows.splice(i, 1);
                selectedObjects.splice(i, 1);
                break;
            }
        }
    } else if (!contains) {
        idsOfSelectedRows.push(id);
        selectedObjects.push(object);
    }
};
idsOfSelectedRows.contains = function (k) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === k) {
            return true;
        }
    }
    return false;
}

function getSelectedItems(key, defaultVal) {
    if (defaultVal == null) {
        defaultVal = "";
    }
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^rwndrnd#]*)");
    var qs = regex.exec(window.location.href);

    if (qs == null) {
        return defaultVal;
    }
    else {
        var res = qs[1].split('&');
        var values = res[0].split(',');

        return values;
    }
}


function setFullViewSchedulerRowHeight(scheduler, rowCount, schHeight) {
    
    var tableHeight = parseInt(rowCount) * parseInt(scheduler.get_rowHeight());

    if (schHeight > tableHeight) {
        var rh = ((schHeight - tableHeight) / parseInt(rowCount)) + parseInt(scheduler.get_rowHeight());

        $('.rsAllDayTable tr').each(function (index) {
            if ($(this).innerHeight() < parseInt(rh)) {
                $(this).css('height', parseInt(rh).toString() + 'px');
            }
        });

        var rh = parseInt(rh) - 2;
        $('.rsVerticalHeaderTable tr').each(function (index) {
            if ($(this).innerHeight() < parseInt(rh)) {
                $(this).css('height', parseInt(rh).toString() + 'px');
                $(this).find('th').css('height', parseInt($(this).innerHeight() - 1).toString() + 'px');
            }
        });

        var headers = $('.rsVerticalHeaderTable tr th');
        $('.rsAllDayTable tbody tr.rsAllDayRow', scheduler).each(function (index) {
            headers.eq(index).css('height', ($(this).innerHeight() - 2) + 'px');
        });

    }
}

function setFullViewSchedulerRowColumnSize(scheduler, rowCount, schHeight) {

    var tableHeight = parseInt(rowCount) * parseInt(scheduler.get_rowHeight());
    if (schHeight > tableHeight) {
        var rh = ((schHeight - tableHeight) / parseInt(rowCount)) + parseInt(scheduler.get_rowHeight());
        $('.rsAllDayTable tr').each(function (index) {
            if ($(this).innerHeight() < parseInt(rh)) {
                $(this).css('height', parseInt(rh).toString() + 'px !important');
                $('.rsContentScrollArea').css('height', parseInt(rh).toString() + 'px !important');
            }
        });
    }

    var colCount = $('.rsHorizontalHeaderTable tr td').length;
    var schWidth = $('.rsHorizontalHeaderTable').width();
    var scrWidth = $(window).width();

    if (scrWidth > schWidth) {
        scrWidth = scrWidth - 25;
        $('.rsHorizontalHeaderTable').css('width', parseInt(scrWidth).toString() + 'px');
        $('.rsAllDayTable').css('width', parseInt(scrWidth).toString() + 'px');
    }
}

function setFullViewSchedulerColumnResize() {

    var colCount = $('.rsHorizontalHeaderTable tr td').length;
    var schWidth = $('.rsHorizontalHeaderTable').width();
    var scrWidth = $(window).width();

    if (scrWidth > schWidth) {
        scrWidth = scrWidth - 25;
        $('.rsHorizontalHeaderTable').css('width', parseInt(scrWidth).toString() + 'px');
        $('.rsAllDayTable').css('width', parseInt(scrWidth).toString() + 'px');
    }
}

function setFullViewSchedulerRowHeightWithCss(scheduler, rowCount, schHeight,cssClass) {

    var tableHeight = parseInt(rowCount) * parseInt(scheduler.get_rowHeight());

    if (schHeight > tableHeight) {
        var rh = ((schHeight - tableHeight) / parseInt(rowCount)) + parseInt(scheduler.get_rowHeight());

        $(cssClass.toString() + '.rsAllDayTable tr').each(function (index) {
            if ($(this).innerHeight() < parseInt(rh)) {
                $(this).css('height', parseInt(rh).toString() + 'px');
            }
        });

        var rh = parseInt(rh) - 2;
        $(cssClass.toString() + '.rsVerticalHeaderTable tr').each(function (index) {
            if ($(this).innerHeight() < parseInt(rh)) {
                $(this).css('height', parseInt(rh).toString() + 'px');
                $(this).find('th').css('height', parseInt($(this).innerHeight() - 1).toString() + 'px');
            }
        });

        var headers = $(cssClass.toString() + '.rsVerticalHeaderTable tr th');
        $(cssClass.toString() + '.rsAllDayTable tbody tr.rsAllDayRow', scheduler).each(function (index) {
            headers.eq(index).css('height', ($(this).innerHeight() - 2) + 'px');
        });

    }
}

/*
* <summary>
* Function to check weather given value is not null, empty or undefined
* </summary>
* <param name="value">Control value to be passed</param>
**/
function IsNullOrEmpty(value) {
    return (value == null || value == "");
}

function OnClientAutoSizeEnd(sender) {
    if ($telerik.isIE8) {
        setTimeout(function () {
            sender.set_height(sender.get_height());
        }, 0);
    }
}
function radalertCommonError(msg, width, height, zIndex, element) {
    var ErrorMsg = radalert(msg, width, height);
    ErrorMsg.get_popupElement().style.zIndex = zIndex;
    ErrorMsg.add_close(function () { element.select(); })
}

/*
* <summary>
* Function to reload the grid
* </summary>
* <param name="value">Control value to be passed</param>
**/
function reloadPageSize(jqgridTableId,rowNumId) {
    var myGrid = $(jqgridTableId);
    var currentValue = $(rowNumId).val();
    myGrid.setGridParam({ rowNum: currentValue });
    myGrid.trigger('reloadGrid');
}
function openWinAddEdit(page, radwindow, w, h, grid) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(page, radwindow);
    oWnd2.setSize(w, h);
    oWnd2.argument = grid;
    try { oWnd2.add_pageLoad(OnPageLoadComplete); } catch (e) { }
    try { oWnd2.add_close(OnPageClosed); } catch (e) { }
    oWnd2.set_modal(true);
    oWnd2.set_visibleStatusbar(true);
    oWnd2.center();
    oWnd2.set_status('');
    window.setTimeout(function () {
        oWnd2.setActive(true);
    }, 0);
} 

// Set ETE value default to 0.1 when its value is rounded to 0
function DefaultEteResult(result, companyprofileSetting) {
    if (result != "" && result != null) {
        if (result == "0" || result == "00.00" || result == "0.0" || result == "00.0" || result == "0.00" || result == "00:00" || result == "0:0" || result == "00:0" || result == "0:00") {
            if (companyprofileSetting == 1) {
                result = "0.1";
            } else {
                result = "00:05";
            }
        }
    }

    return result;
}

//Common Function to Check code exist or not.
function CheckClientCodeExistance(value, hiddenField, errorDiv, jqgridTableId) {
        $.support.cors = true;
            $.ajax({
                async: false,
                type: 'Get',
                crossDomain: true,
                url: '/Views/Utilities/GetApi.aspx?apiType=FSS&method=ClientCode&clientId=0&clientCD=' + value + '&isInActive=false',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    verifyReturnedResultForJqgrid(data);
                    var NotFound = data.indexOf('Failed to execute api');
                    if (NotFound == 0) {
                        $(hiddenField).val(0);
                        $(errorDiv).addClass('showDiv');
                    } else {
                        var data = JSON.parse(data.replace(/&quot;/g, '"'));
                        if (data != 0) {
                            $(hiddenField).val(data);
                            if ($("#hdnSearchResult").val() == "1") {
                                $(jqgridTableId).jqGrid().trigger('reloadGrid');
                                $("#hdnSearchResult").val("");
                            }
                            $(errorDiv).removeClass().addClass('hideDiv');
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var content = jqXHR.responseText;
                    if (IsNullOrEmptyOrUndefined(content) == false && content.indexOf('404')) {
                        $(hiddenField).val(0);
                        $(errorDiv).addClass('showDiv');
                    }
                 }
            });
}

                
function CheckCrewGroupExistance(value, hiddenField, errorDiv, jqgridTableId)
{
    $.ajax({
        async: true,
        type: 'Get',
        crossDomain: true,
        url: '/Views/Utilities/GetApi.aspx?apiType=FSS&method=CrewGroup&clientId=0&crewGroupCD=' + value,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            verifyReturnedResultForJqgrid(result);
            var data = JSON.parse(result);
            if (data != 0) {
                $(hiddenField).val(data);
                if ($("#hdnSearchResult").val() == "1")
                {
                    $(jqgridTableId).jqGrid().trigger('reloadGrid');
                    $("#hdnSearchResult").val("");
                }
                $(jqgridTableId).jqGrid().trigger('reloadGrid');
                $(errorDiv).removeClass().addClass('hideDiv');
            }
            else {
                $(hiddenField).val(0);
                $(errorDiv).addClass('showDiv');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var content = jqXHR.responseText;
            if (content == null || content == undefined || content.indexOf('404')) {
                $(hiddenField).val(0);
                $(errorDiv).addClass('showDiv');
            }
        }
    });
}

function JqgridOnSelectRow(ismultiSelect, status, selectedRow, rowData) {
    if (ismultiSelect) {
        var tempcds = selectedRow.split(",");
        if (status) {
            if (selectedRow.lastIndexOf(",") != selectedRow.length - 1) {
                selectedRow += ",";
            }
            if (jQuery.inArray(jQuery.trim(rowData), tempcds) == -1) {
                selectedRow += jQuery.trim(rowData);
            }

            selectedRow.substring(0, selectedRow.length - 1);
        } else {

            if (jQuery.inArray(jQuery.trim(rowData), tempcds) != -1) {
                tempcds[jQuery.inArray(jQuery.trim(rowData), tempcds)] = undefined;
            }
            tempcds = jQuery.grep(tempcds, function (value) { return value != undefined && value != "" });
            selectedRow = tempcds.join(",");
            selectedRow.slice(0, selectedRow.length - 1);
        }
    }
    else {
        selectedRow = $.trim(rowData);
    }

    return selectedRow;
}

function JqgridSelectAfterInsertRow(ismultiSelect, selectedRow, rowId, rowData, jqgridTableId) {
    if (ismultiSelect) {
        var tempcds = selectedRow.split(",");
        if (jQuery.inArray(jQuery.trim(rowData), tempcds) != -1) {
            jQuery(jqgridTableId).setSelection(rowId, true);
        }
    } else {

        if ($.trim(rowData.toLowerCase()) == $.trim(selectedRow.toLowerCase()) && jQuery.trim(selectedRow) != "") {
            $(this).find(".selected").removeClass('selected');
            $(this).find('.ui-state-highlight').addClass('selected');
            $(jqgridTableId).setSelection(rowId, true);
        }
    }
}

function JqgridSelectAll(selectedRow, jqgridTableId, id, status, rowData) {
    var tempcd = selectedRow.split(",");
    if (selectedRow.lastIndexOf(",") != selectedRow.length - 1) {
        selectedRow += ",";
    }
    for (var i = 0; i < id.length; i++) {
        var rowDataOject = $(jqgridTableId).jqGrid("getRowData", id[i]);
        if (status) {
            if (jQuery.inArray(jQuery.trim(rowDataOject[rowData]), tempcd) == -1) {
                selectedRow += jQuery.trim(rowDataOject[rowData]) + ",";
            }
        } else {
            if (jQuery.inArray(jQuery.trim(rowDataOject[rowData]), tempcd) != -1) {
                tempcd[jQuery.inArray(jQuery.trim(rowDataOject[rowData]), tempcd)] = undefined;
            }
            tempcd = jQuery.grep(tempcd, function (value) { return value != undefined && value != "" });
            selectedRow = tempcd.join(",");
        }
    }

    selectedRow = jQuery.trim(selectedRow);
    if (selectedRow.lastIndexOf(",") == selectedRow.length - 1)
        selectedRow = selectedRow.substr(0, selectedRow.length - 1);

    return selectedRow;
}

function JqgridCreateSelectAllOption(ismultiSelect, jqgridTableId)
{
    if (ismultiSelect) {
        var gridid = jqgridTableId.substring(1, jqgridTableId.length);
        $("#jqgh_" + gridid + "_cb").find("b").remove();

        $("#jqgh_" + gridid + "_cb").append("<b> Select All</b>");
        $("#jqgh_" + gridid + "_cb").css("height", "25px");
        $(jqgridTableId + "_cb").css("width", "100px");
        $(jqgridTableId + " tbody tr").children().first("td").css("width", "100px");
    }
}

function openWinForFilterGrid(url, radwindow, title, w, h, grid, elementId1, propertyName1, elementId2, propertyName2) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(url, radwindow);
    oWnd2.setSize(w, h);
    try { oWnd2.add_pageLoad(OnPageLoadCompleteLevel2); } catch (e) { }
    if (elementId1.indexOf("#") >= 0) {
        oWnd2.argumentcontrol = $(elementId1);
    }
    else {
        oWnd2.argumentcontrol = $('#' + elementId1);
    }
    if (elementId2.indexOf("#") >= 0) {
        oWnd2.argumentcontrol = $(elementId2);
    }
    else {
        oWnd2.argumentcontrol1 = $('#' + elementId2);
    }

    oWnd2.argumentField = propertyName1;
    oWnd2.argumentField1 = propertyName2;
    oWnd2.add_close(OnPageClosedLevel2);

    oWnd2.set_modal(true);
    oWnd2.set_visibleStatusbar(true);
    oWnd2.center();
    oWnd2.set_status('');
    return true;
}

function popupwindowForFilterGrid(url, raddWindow, title, w, h, grid) {
    openSizedWindow(url, raddWindow, w, h, grid);
    return true;
}

/// Remove unwanted properties from KO view model. So it can map same as C# ViewModel class
// return ViewModel
function removeUnwantedPropertiesKOViewModel(koViewModel) {
    for (var prop in koViewModel) {
        if (typeof (koViewModel[prop]) === "object") {
            koViewModel[prop] = removeUnwantedPropertiesKOViewModel(koViewModel[prop]);
        }
    }
    try{
        // delete unwanted property 
        if (koViewModel.hasOwnProperty("__type"))
            delete koViewModel.__type;
        if (koViewModel.hasOwnProperty("__ko_mapping__"))
            delete koViewModel.__ko_mapping__;

        if (koViewModel.hasOwnProperty("__proto__"))
            delete koViewModel.__proto__;

    }catch(e)
    {

    }
    return koViewModel;


}
function IsMobileSafari() {
    return $telerik.isMobileSafari;
}



function GetLegsList() {
    $.ajax({
        async: false,
        type: "POST",
        url: "PreflightTripManager.aspx/InitializePreflghtLegs",
        data: JSON.stringify({ 'selectedLegNum': '0' }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                self.Legs = ko.mapping.fromJS(response.d.Result.Leg);
                currentPreflightLeg = response.d.Result.CurrentLeg.LegNUM;
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function ApplyCustomScroll(item) {
    if (navigator.userAgent.match(/iPad/i) != null) {
        $(item).ApplyCustomeScroll(true);
    }
}

/* Multiple/Single CrewRoster delete */
function DeleteCrewList(arg, JqGridId, CrewLists) {
    $.ajax({
        type: 'GET',
        async: false,
        data: JSON.stringify(CrewLists),
        url: '/Views/Utilities/DeleteList.aspx?apiName=FSS&apiType=DELETE&method=CrewLists',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            verifyReturnedResultForJqgrid(data);
            if (data != undefined && data != "") {
                var IsValidResponse = data.indexOf("Failed to execute api");
                if (IsValidResponse == 0) {
                    showMessageBox(data, popupTitle);
                } else {
                    try{
                        data = JSON.parse(htmlDecode(data));
                    } catch (ec) {
                        data = JSON.parse(data.replace(/&quot;/g, '"'));
                    }
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (i == 0) {
                                if (data[i]["Value"] == 0) {
                                    crewcdList = data[i]["CrewCD"];
                                }
                            } else {
                                if (data[i]["Value"] == 0) {
                                    if (crewcdList.length > 0) {
                                        crewcdList = crewcdList + ',' + data[i]["CrewCD"];
                                    } else
                                        crewcdList = data[i]["CrewCD"];
                                }
                            }
                        }
                        if (crewcdList != "") {
                            showMessageBox("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.", popupTitle);
                        }
                    }
                }
            }
            CrewLists.length = 0;
            crewcdList = "";
            $(JqGridId).trigger('reloadGrid');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            var content = jqXHR.responseText;

            if (content == "Conflict" || content == "NotFound" || content == "InternalServerError") {
                showMessageBox(content, popupTitle);
            }
        }
    });
}

function DeleteApiResponse(jqgridTableId, data, message, popupTitle) {
    verifyReturnedResultForJqgrid(data);
    if (data.indexOf("PreconditionFailed") == 0 || data.indexOf("NotFound") == 0 || data.indexOf("Conflict") == 0 || data == 0) {
        showMessageBox(message, popupTitle);
    } else if (data.indexOf("Failed to execute api") == 0) {
        showMessageBox(data, popupTitle);
    } else {
        $(jqgridTableId).trigger('reloadGrid');
    }
}

function DeleteApiResponseWithBootStrapDialLogBox(jqgridTableId, data, message, popupTitle) {
    if (data.indexOf("PreconditionFailed") == 0 || data.indexOf("NotFound") == 0 || data.indexOf("Conflict") == 0 || data == 0) {
        BootboxAlertWithTitle(message, popupTitle);
    } else if (data.indexOf("Failed to execute api") == 0) {
        BootboxAlertWithTitle(data, popupTitle);
    } else {
        $(jqgridTableId).trigger('reloadGrid');
    }
}

/*
Ipad touch event handle code for calendar pages start here
*/
var lastCalendarContext = null;
var CalendarScheduler = null;

function HandleIpadTouchCalendarContextMenu() {
    var MenuIds = "";
    $(".rsContentScrollArea").bind('scroll', function (e) {
        $(".RadMenu_Context").css("display", "none");
        $(".RadMenu_Context").css("opacity", "0"); // Opacity improves the user exp. Hide the context menu flickering
        lastCalendarContext = null;
        clearTimeout(MenuIds);
    });

    $(".rsContentScrollArea").on("click", function (e) {
        CalendarScheduler = $find($(this).parents(".RadScheduler").attr("id"));
        clearTimeout(MenuIds);
        MenuIds = setTimeout(function () {
            if (lastCalendarContext != null) {
                longTouch(CalendarScheduler);
            }
        }, 500);
    });
}

function longTouch(scheduler) {
    var eventArgs = null;
    var target = null;
    $(".RadMenu_Context").css("opacity", "1"); // Opacity improves the user exp. Hide the context menu flickering
    if (lastCalendarContext.target.outerHTML.indexOf('rsAptContent') != -1 || $(lastCalendarContext.target).closest(".rsAptContent").size() > 0) {
        target = scheduler.getAppointmentFromDomElement(lastCalendarContext.target);
        eventArgs = new Telerik.Web.UI.SchedulerAppointmentContextMenuEventArgs(target, lastCalendarContext);
        scheduler._raiseAppointmentContextMenu(eventArgs);
    }
    else if (lastCalendarContext.target.nodeName == 'DIV' || lastCalendarContext.target.nodeName == 'TD' || lastCalendarContext.target.outerHTML.indexOf('rsWrap rsLastSpacingWrapper') > 0) {
        target = scheduler._activeModel.getTimeSlotFromDomElement(lastCalendarContext.target);
        eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastCalendarContext, target);
        scheduler._raiseTimeSlotContextMenu(eventArgs);
    }
}

function handleTouchStart(e) {
    lastCalendarContext = e;
    lastCalendarContext.target = e.originalTarget;
    lastCalendarContext.pageX = e.changedTouches[0].pageX;
    lastCalendarContext.pageY = e.changedTouches[0].pageY;
    lastCalendarContext.clientX = e.changedTouches[0].clientX;
    lastCalendarContext.clientY = e.changedTouches[0].clientY;
}

/*
Ipad touch event handle code end here
*/

function IsNullOrEmptyOrUndefined(value) {
    return (value == undefined || value == null || value == "");
}

function browserName() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("msie") != -1) return 'Internet Explorer';
    if (agt.indexOf("chrome") != -1) return 'Chrome';
    if (agt.indexOf("opera") != -1) return 'Opera';
    if (agt.indexOf("firefox") != -1) return 'Firefox';
    if (agt.indexOf("netscape") != -1) return 'Netscape';
    if (agt.indexOf("safari") != -1) return 'Safari';
    if (agt.indexOf("staroffice") != -1) return 'Star Office';
    if (agt.indexOf("webtv") != -1) return 'WebTV';
    if (agt.indexOf("beonex") != -1) return 'Beonex';
    if (agt.indexOf("chimera") != -1) return 'Chimera';
    if (agt.indexOf("netpositive") != -1) return 'NetPositive';
    if (agt.indexOf("phoenix") != -1) return 'Phoenix';
    if (agt.indexOf("skipstone") != -1) return 'SkipStone';
    if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';



    if (agt.indexOf('\/') != -1) {
        if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
            return navigator.userAgent.substr(0, agt.indexOf('\/'));
        }
        else return 'Netscape';
    } else if (agt.indexOf(' ') != -1)
        return navigator.userAgent.substr(0, agt.indexOf(' '));
    else return navigator.userAgent;
}

String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}

/*
Ipad touch event handle code end here
*/

function SetBrowserDetails() {
    if (!jQuery.browser) {

        jQuery.browser = {};
        jQuery.browser.mozilla = false;
        jQuery.browser.webkit = false;
        jQuery.browser.opera = false;
        jQuery.browser.safari = false;
        jQuery.browser.chrome = false;
        jQuery.browser.msie = false;
        jQuery.browser.android = false;
        jQuery.browser.blackberry = false;
        jQuery.browser.ios = false;
        jQuery.browser.operaMobile = false;
        jQuery.browser.windowsMobile = false;
        jQuery.browser.mobile = false;

        var nAgt = navigator.userAgent;
        jQuery.browser.ua = nAgt;

        jQuery.browser.name = navigator.appName;
        jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
        jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // In Opera, the true version is after "Opera" or after "Version"
        if ((verOffset = nAgt.indexOf("Opera")) != -1) {
            jQuery.browser.opera = true;
            jQuery.browser.name = "Opera";
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
        }

            // In MSIE < 11, the true version is after "MSIE" in userAgent
        else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
            jQuery.browser.msie = true;
            jQuery.browser.name = "Microsoft Internet Explorer";
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 5);
        }

            // In TRIDENT (IE11) => 11, the true version is after "rv:" in userAgent
        else if (nAgt.indexOf("Trident") != -1) {
            jQuery.browser.msie = true;
            jQuery.browser.name = "Microsoft Internet Explorer";
            var start = nAgt.indexOf("rv:") + 3;
            var end = start + 4;
            jQuery.browser.fullVersion = nAgt.substring(start, end);
        }

            // In Chrome, the true version is after "Chrome"
        else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
            jQuery.browser.webkit = true;
            jQuery.browser.chrome = true;
            jQuery.browser.name = "Chrome";
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
        }
            // In Safari, the true version is after "Safari" or after "Version"
        else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
            jQuery.browser.webkit = true;
            jQuery.browser.safari = true;
            jQuery.browser.name = "Safari";
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
        }
            // In Safari, the true version is after "Safari" or after "Version"
        else if ((verOffset = nAgt.indexOf("AppleWebkit")) != -1) {
            jQuery.browser.webkit = true;
            jQuery.browser.name = "Safari";
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
        }
            // In Firefox, the true version is after "Firefox"
        else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
            jQuery.browser.mozilla = true;
            jQuery.browser.name = "Firefox";
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 8);
        }
            // In most other browsers, "name/version" is at the end of userAgent
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            jQuery.browser.name = nAgt.substring(nameOffset, verOffset);
            jQuery.browser.fullVersion = nAgt.substring(verOffset + 1);
            if (jQuery.browser.name.toLowerCase() == jQuery.browser.name.toUpperCase()) {
                jQuery.browser.name = navigator.appName;
            }
        }

        /*Check all mobile environments*/
        jQuery.browser.android = (/Android/i).test(nAgt);
        jQuery.browser.blackberry = (/BlackBerry/i).test(nAgt);
        jQuery.browser.ios = (/iPhone|iPad|iPod/i).test(nAgt);
        jQuery.browser.operaMobile = (/Opera Mini/i).test(nAgt);
        jQuery.browser.windowsMobile = (/IEMobile/i).test(nAgt);
        jQuery.browser.mobile = jQuery.browser.android || jQuery.browser.blackberry || jQuery.browser.ios || jQuery.browser.windowsMobile || jQuery.browser.operaMobile;


        // trim the fullVersion string at semicolon/space if present
        if ((ix = jQuery.browser.fullVersion.indexOf(";")) != -1)
            jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);
        if ((ix = jQuery.browser.fullVersion.indexOf(" ")) != -1)
            jQuery.browser.fullVersion = jQuery.browser.fullVersion.substring(0, ix);

        jQuery.browser.majorVersion = parseInt('' + jQuery.browser.fullVersion, 10);
        if (isNaN(jQuery.browser.majorVersion)) {
            jQuery.browser.fullVersion = '' + parseFloat(navigator.appVersion);
            jQuery.browser.majorVersion = parseInt(navigator.appVersion, 10);
        }
        jQuery.browser.version = jQuery.browser.majorVersion;
    }
}

var resetKOViewModel = function (obj, blacklist) {
    if (IsNullOrEmptyOrUndefined(blacklist)) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop) && ko.isObservable(obj[prop])) {
                obj[prop](null);
            }
        }
    }
    else {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop) && ko.isObservable(obj[prop]) && blacklist.indexOf(prop) == -1) {
                obj[prop](null);
            }
        }
    }
};

function verifyReturnedResult(fssOperationResult) {
    if (fssOperationResult != undefined && fssOperationResult != null && fssOperationResult.d != undefined && fssOperationResult.d != null) {
        if (fssOperationResult.d.StatusCode == 200)
        {
            return fssOperationResult.d.Success;
        }
        else if (fssOperationResult.d.Success == false && fssOperationResult.d.StatusCode == 401) {
            window.location.href = "/Account/Login.aspx";
        }
        else
            return true;
    }
    return false;
}
function verifyReturnedResultForJqgrid(fssOperationResult) {
    fssOperationResult = htmlDecode(fssOperationResult);

    if (fssOperationResult != undefined && fssOperationResult != null && fssOperationResult != "") {
        try{            
            if (typeof fssOperationResult == "string") {
                fssOperationResult = JSON.parse(fssOperationResult);
            }
            if (fssOperationResult.StatusCode == 401) {            
                parent.window.location.href = "/Account/Login.aspx";
            }
        }
        catch(ex){
        }
    }
}

function formatRateValue(value, precision, EmptyIfNull) {
    if (precision == 'undefined') precision = 2;
    if (EmptyIfNull == 'undefined' || EmptyIfNull == undefined) EmptyIfNull = false;
    if (EmptyIfNull == true)
        return value != null ? "$" + parseFloat(value).formatMoney(precision) : "";
    else
        return "$" + parseFloat(value).formatMoney(precision) ;

}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function InitializeCostFormatterBindingHandler() {
    ko.bindingHandlers.CostCurrencyElement = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var rateValue = valueAccessor();
            var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers.CostCurrencyElement.defaultPrecision;
            var isEmptyIfNull = ko.utils.unwrapObservable(allBindingsAccessor().EmptyIfNull) || ko.bindingHandlers.CostCurrencyElement.EmptyIfNullDefault;
            if (isNaN(rateValue())) {
                rateValue("0");
            }
            $(element).focus(function () {

                if (isNaN(rateValue()) == false) {
                    $(element).val(rateValue());
                }
                else {
                    rateValue(parseFloat($(element).val()).toFixed(precision));
                    if (isNaN(rateValue())) {
                        rateValue("0");
                    }
                    $(element).val(formatRateValue(rateValue(), precision, isEmptyIfNull));
                }
            });
            $(element).blur(function (allBindingsAccessor) {
                rateValue(parseFloat($(element).val()).toFixed(precision));
                if (isNaN(rateValue())) {
                    rateValue("0");
                }
                $(element).val(formatRateValue(rateValue(), precision, isEmptyIfNull));
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers.CostCurrencyElement.defaultPrecision;
            var isEmptyIfNull = ko.utils.unwrapObservable(allBindingsAccessor().EmptyIfNull) || ko.bindingHandlers.CostCurrencyElement.EmptyIfNullDefault;
            var value = valueAccessor();
            if (isNaN(value())) {
                value("0");
            }
            if (isNaN(value()) == false) {
                $(element).val(formatRateValue(value(), precision, isEmptyIfNull));
            }
        },
        defaultPrecision: 2,
        EmptyIfNullDefault: false
    };
}

/*....................Start bootstrap popup function*/
function OpenWithBootStrapPopup(url, myModelId, myBodyId, iframeId) {
    window.parent.$(myModelId).css("z-index", "1600");
    window.parent.$(myModelId).css("opacity", "1");
    window.parent.$(myModelId).css("display", "block");
    window.parent.$(myBodyId).html('<iframe id="' + iframeId + '" width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true" src="' + url + '"></iframe>');
}
function BootboxAlert(message) {
    bootbox.dialog({
        message: '<img src="/Scripts/images/error.gif" /> <span>'+message+'</span>',
        title: '<img src="/Scripts/images/title_icon.gif" /> <span>Universal Weather And Aviation</span>',
        buttons: {
            main: {
                label: "OK",
                className: "btn-primary",
                callback: function () {
                }
            }
        }
    });
}
function BootboxAlertWithTitle(message,popupTitle) {
    bootbox.dialog({
        message: '<img src="/Scripts/images/error.gif" /> <span>' + message + '</span>',
        title: '<img src="/Scripts/images/title_icon.gif" /> <span>' + popupTitle + '</span>',
        buttons: {
            main: {
                label: "OK",
                className: "btn-primary",
                callback: function () {
                }
            }
        }
    });
}
function ManuallyCloseBootstrapPopup(bootstrapDivId) {
    $(bootstrapDivId).css("display", "none");
    $(bootstrapDivId).parent("body").removeClass('modal-open');
}
/*....................End Start bootstrap popup function*/

function checkdecimalValueWithParam(sender, from, digit, decimal) {
    var ret = true;
    val = sender.value;
    if (digit == null)
        digit = 7;
    if (decimal == null)
        decimal = 1;

    var regex = /^[0-9]{0,7}(\.[0-9]{0,1})?$/;
    var formatmessage = " The Format is NNNNNNN.N";

    if (digit == 2 && decimal == 2) {
        regex = /^[0-9]{0,2}(\.[0-9]{0,2})?$/;
        formatmessage = " The Format is NN.NN";
    }

    if (digit == 7 && decimal == 1) {
        regex = /^[0-9]{0,7}(\.[0-9]{0,1})?$/;
        formatmessage = " The Format is NNNNNNN.N";
    }

    if (digit == 6 && decimal == 6) {
        regex = /^[0-9]{0,6}(\.[0-9]{0,6})?$/;
        formatmessage = " The Format is NNNNNN.NNNNNN";
    }

    if (digit == 9 && decimal == 1) {
        regex = /^[0-9]{0,9}(\.[0-9]{0,1})?$/;
        formatmessage = " The Format is NNNNNNN.N";
    }

    if (!regex.test(sender.value)) {
        jAlert("You entered:" + val + "." + "<br/>" + "Its a Invalid Format." + "<br/>" + formatmessage, from, function () { sender.value = 0; sender.focus(); });
        ret = false;
    }
    return ret;
}

function InitializeDatepickerBindingHandler() {
    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var unwrap = ko.utils.unwrapObservable;
            var dataSource = valueAccessor();
            var binding = allBindingsAccessor();
            var options = {
                changeMonth: true,
                changeYear: true,
                dateFormat: self.UserPrincipal._ApplicationDateFormat.toLowerCase().replace(/yyyy/g, "yy")
            };
            $(element).datepicker(options);
            if (dataSource() != null && dataSource() != "") {
                var d = moment(dataSource()).startOf('day').format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
                $(element).datepicker('setDate', d);
                var isoDate = moment($(element).datepicker("getDate")).startOf('day').format(isodateformat);
                dataSource(isoDate);
            }
            ko.utils.registerEventHandler(element, "change", function () {
                observable = valueAccessor();
                if ($(element).datepicker("getDate") == null) {
                    observable(null);
                } else {
                    var isoDate = moment($(element).datepicker("getDate")).startOf('day').format(isodateformat);
                    observable(isoDate);
                    var cd = moment(isoDate).startOf('day').format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
                    if (cd != $(element).val()) {
                        jAlert("Enter valid date", "Postflight", function () {
                            $(element).datepicker('setDate', '');
                            observable('');
                        });
                    }
                }
            });
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).datepicker("destroy");
            });
        },
        update: function (element, valueAccessor) {
            var dataSource = valueAccessor();
            if (dataSource() != null && dataSource() != "") {
                var changedDate;
                var isoDate;
                if (dataSource().toString().indexOf("/Date") == -1) {
                    changedDate = moment(dataSource()).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
                }
                else {
                    changedDate = moment(dataSource()).startOf('day').format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
                    isoDate = moment(dataSource()).startOf('day').format(isodateformat);
                    dataSource(isoDate);
                }
                if (changedDate.toLowerCase() == "invalid date") {
                    changedDate = moment(dataSource(), self.UserPrincipal._ApplicationDateFormat.toUpperCase()).startOf('day').format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
                }
                $(element).datepicker('setDate', changedDate);
            } else {
                $(element).val("");
                dataSource("");
            }
        }
    };
}


function InitializeKOtouppercase() {
    ko.observable.fn.kotouppercase = function () {
        var textValue = this();
        if (!IsNullOrEmpty(textValue))
            return htmlDecode(this()).toUpperCase();
        return "";
    }
}

function InitializeTimepickerBindingHandler() {
    ko.bindingHandlers.timepicker = {
        init: function (element, valueAccessor) {
            var dataSource = valueAccessor();
            var options = {
                'timeFormat': 'H:i',
                'step': 1,
                showOnFocus: false
            };
            $(element).timepicker(options);
            ko.utils.registerEventHandler(element, "change", function () {
                observable = valueAccessor();
                observable(moment($(element).timepicker("getTime")).format("HH:mm"));
            });
            if (dataSource() != null && IsNullOrEmptyOrUndefined(dataSource()) == false) {
                $(element).timepicker('setTime', dataSource());
                var observable = valueAccessor();
                var timeFormattedValue = moment($(element).timepicker("getTime")).format("HH:mm");
                if (timeFormattedValue == "Invalid date")
                    timeFormattedValue = "";
                observable(timeFormattedValue);
            }
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).timepicker('remove');
            });
        },
        update: function (element, valueAccessor) {
            var dataSource = valueAccessor();
            if (dataSource() != null && IsNullOrEmptyOrUndefined(dataSource()) == false) {
                var d = dataSource();
                $(element).timepicker("setTime", d);
                var observable = valueAccessor();
                var timeFormattedValue = moment($(element).timepicker("getTime")).format("HH:mm");
                if (timeFormattedValue == "Invalid date")
                    timeFormattedValue = "";
                observable(timeFormattedValue);
            } else {
                $(element).val("");
                observable = valueAccessor();
                observable("");
            }
        }
    };
}

function ValidateScheduleTime(sender, from) {
    var ret = true;
    var timeVal = sender.value;
    var result = ValidateTime(timeVal);
    if (result != '') {
        jAlert(result, from, function() { sender.value = ''; sender.focus(); });
        ret = false;
    }
    return ret;
}

function ValidateTimeForTenMin(sender, from) {
    var ret = true;
    if (self.UserPrincipal._TimeDisplayTenMin == "2") {
        var timeVal = sender.value;
        var result = ValidateTime(timeVal);
        if (result != '') {
            jAlert(result, from, function() { sender.value = '00:00'; sender.focus(); });
            ret = false;
        }
    } else {
         ret = checkdecimalValueWithParam(sender, from, 9, 1);
    }
    
    return ret;
}

function ValidateTime(timeVal) {
    var result = '';
    if (timeVal.indexOf(":") != -1) {
        var timeArray = timeVal.split(':');
        var _hour = "00";
        var _minute = "00";

        if (!IsNullOrEmpty(timeArray[0]))
            _hour = timeArray[0];
        if (!IsNullOrEmpty(timeArray[1]))
            _minute = timeArray[1];

        var hour = parseInt(_hour);
        var minute = parseInt(_minute);

        if (hour > 23) {
            result = "Hour entry must be between 0 and 23. <br/>";
        }
        if (minute > 59) {
            result += "Minute entry must be between 0 and 59.";
        }
    } else if (timeVal.indexOf(":") < 0) {
        result = "Invalid Time Format! <br/>";
        result += "Hour entry must be between 0 and 23. <br/>";
        result += "Minute entry must be between 0 and 59.";
    }
    return result;
}

var ConvertNullToEmptyString = function (v) {
    if (v == null || v == 'null') return "";
    return v;
}
function InitializeKOTrimmedFunction()
{
    ko.subscribable.fn['Trimmed'] = function () {
        return ko.computed({
            read: function () {
                return $.trim(this());
            },
            write: function (value) {
                this($.trim(value));
                this.valueHasMutated();
            },
            owner: this
        });
    };
}

function InitializeFormatSingleDecimalBindingHandler() {
    ko.bindingHandlers.formatSingleDecimal = {
        init: function (element, valueAccessor) {
            var valueText = ko.utils.unwrapObservable(valueAccessor());
            var formattedNumber = getSingleDecimalFromText(valueText);
            $(element).val(formattedNumber);
            var observable = valueAccessor();
            observable(formattedNumber);

            ko.utils.registerEventHandler(element, "change", function () {
                var observable = valueAccessor();
                var valueText = $(element).val();
                var formattedNumber = getSingleDecimalFromText(valueText);
                $(element).val(formattedNumber);
                observable(formattedNumber);
            });
        },
        update: function (element, valueAccessor) {
            var observable = valueAccessor();
            var valueText = ko.utils.unwrapObservable(valueAccessor());
            var formattedNumber = getSingleDecimalFromText(valueText);
            $(element).val(formattedNumber);
            observable(formattedNumber);
        }
    };
}

function getSingleDecimalFromText(receivedText) {
    if (!isNaN(parseFloat(receivedText)) && isFinite(receivedText))
        return parseFloat(receivedText).toFixed(1);
    else
        return receivedText;
}

function InitializeFormatDoubleDecimalBindingHandler() {
    ko.bindingHandlers.formatDoubleDecimal = {
        init: function (element, valueAccessor) {
            var valueText = ko.utils.unwrapObservable(valueAccessor());
            var formattedNumber = getDoubleDecimalFromText(valueText);
            $(element).val(formattedNumber);
            var observable = valueAccessor();
            observable(formattedNumber);

            ko.utils.registerEventHandler(element, "change", function () {
                var observable = valueAccessor();
                var valueText = $(element).val();
                var formattedNumber = getDoubleDecimalFromText(valueText);
                $(element).val(formattedNumber);
                observable(formattedNumber);
            });
        },
        update: function (element, valueAccessor) {
            var observable = valueAccessor();
            var valueText = ko.utils.unwrapObservable(valueAccessor());
            var formattedNumber = getDoubleDecimalFromText(valueText);
            $(element).val(formattedNumber);
            observable(formattedNumber);
        }
    };
}

function getDoubleDecimalFromText(receivedText) {
    if (!isNaN(parseFloat(receivedText)) && isFinite(receivedText))
        return parseFloat(receivedText).toFixed(2);
    else
        return receivedText;
}

/*----------------------------------Start setting key value pair for page and control*/
var pageControlArray = new Object;
function SetHtmlControl(pageName, control) {
    pageControlArray.pageName = control;
}
function SetHtmlControlValue(pageName, value) {
    $(pageControlArray.pageName).val(value);
    $(pageControlArray.pageName).change();
}
/*--------------------------------End setting key value pair for page and control*/

//Operating System
var OSName =
{
    isMac: function () {
        if (navigator.appVersion.indexOf("Mac") != -1)
            return true;
        else
            return false;
    },
    isWindows: function () {
        if (navigator.appVersion.indexOf("Win") != -1)
            return true;
        else
            return false;
    },
    isLinux: function () {
        if (navigator.appVersion.indexOf("Linux") != -1)
            return true;
        else
            return false;
    },
    isUnix: function () {
        if (navigator.appVersion.indexOf("X11") != -1)
            return true;
        else
            return false;
    }
};
function formatDateISO(data) {
    if (!IsNullOrEmpty(data))
        return moment(data).format(isodateformat);
    else
        return null;
}

// Used to set blank text in jqgrid while no records in jqgrid. Jqgrid need to use GridUnload and again Init if use of this function is done in loadComplete. See PostflightPax_page.js for example.

function GetLastInsertedID(id) {
    self.parent.GetLastInsertedID.Id = id;
    GetRadWindowManager().closeActiveWindow();
    var radWindowName = GetRadWindowManager().getActiveWindow()._name;
    window[radWindowName].reloadCrewDutyRules();
}

function MessageWhileBlankJqGrid(jqGridId)
{
    if (!jqGridId.match('^#'))
        jqGridId = '#' + jqGridId;

    if (jQuery(jqGridId).getGridParam("records") == 0) {
        $(jqGridId + ' tbody').html("<div style='padding-left:5px;font: 11px/16px Arial,Helvetica,sans-serif !important;'>No records found</div>");
    }
}

function removeQueryStringParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function Start_Loading(Control) {
    if (Control) {
        $(Control).removeClass("browse-button");
        $(Control).addClass("browse-button-Validation-Loading");
    }
}

function End_Loading(Control, Found) {
    if (Control) {
        if (Found) {
            $(Control).removeClass("browse-button-Validation-Loading");
            $(Control).addClass("browse-button-Validation-Correct");
            setTimeout(function () {
                $(Control).removeClass("browse-button-Validation-Correct");
                $(Control).addClass("browse-button");
            }, 900);
        }
        else {
            $(Control).removeClass("browse-button-Validation-Loading");
            $(Control).addClass("browse-button-Validation-InCorrect");
            setTimeout(function () {
                $(Control).removeClass("browse-button-Validation-InCorrect");
                $(Control).addClass("browse-button");
            }, 900);
        }
    }
}


function HideSuccessMessage() {
    $('#tdSuccessMessage').css("display", "none");
}

function RestrictedModuleMessage(moduleName) {
    jAlert("Your account is not configured to access " + moduleName + ". <br/> Please contact your account administrator.", "Universal Weather");
}

function fullNameFormatter(cellvalue, options, rowObject) {
    var mname = "";
    var lname = "";
    var fname = "";
    if (rowObject.MiddleInitial == null) {
        mname = "";
    } else {
        mname = rowObject.MiddleInitial;
    }
    if (rowObject.LastName == null) {
        lname = "";
    } else {
        lname = rowObject.LastName;
    }
    if (rowObject.FirstName == null) {
        fname = "";
    } else {
        fname = rowObject.FirstName;
    }
    return lname + ', ' + fname + ' ' + mname;
}
