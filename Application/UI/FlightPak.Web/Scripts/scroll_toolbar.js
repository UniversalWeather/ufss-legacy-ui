$(window).scroll(function () {
    //	whichBrowser function identify  browser  
    if (whichBrowser() == 'chrome') {
        $scroll_final = document.body.scrollTop;   /// identify  scroll position in pixels for chorme
    }
    if (whichBrowser() == 'Internet Explorer' || whichBrowser() == 'Firefox') {
        $scroll_final = document.documentElement.scrollTop; // identify scroll position for IE and firefox 
    }
    if (whichBrowser() == "Mozilla")//for IE 11 whichBrowser() returns mozilla
    {
        $scroll_final = document.documentElement.scrollTop;
    }
    if (whichBrowser() == "Safari")
    {
        $scroll_final = document.body.scrollTop;
    }
    var toolbarid = document.getElementById('toolbar');
    if ($scroll_final <= 0 && toolbarid != undefined) {
        if (toolbarid.style != undefined)
            toolbarid.style.visibility = 'hidden';
    }

    if ($scroll_final > 0 && $scroll_final < 600 && toolbarid != undefined) {

        toolbarid.style.opacity = '100';
        toolbarid.style.filter = 'alpha(opacity=100)';
        toolbarid.style.visibility = 'visible';
    }

    if ($scroll_final > 600 && $scroll_final < 1000 && toolbarid != undefined) {

        toolbarid.style.opacity = '100';
        toolbarid.style.filter = 'alpha(opacity=100)';
        toolbarid.style.visibility = 'visible';
    }

    if ($scroll_final >= 1000) {
        toolbarid.style.opacity = '100';
        toolbarid.style.filter = 'alpha(opacity=100)';
        toolbarid.style.visibility = 'visible';
    }
});

var time = 0;
$(document).ready(function () {

    $("#toolbar").stop(true, true).mouseover(function () {
        if (time == 0) {
            $(".img_animate").slideToggle("slow");
            time = 1;
        }
    });
    $(".img_animate").click(function () {
        if (time == 1) {
            $(".img_animate").slideToggle("slow");
            time = 0;
        }
    });

});

$(document).bind('click', function (e) {
    var $clicked = $(e.target);
    if (!($clicked.is('.img_animate') || $clicked.parents().is('.img_animate'))) {

        $(".img_animate").stop(true, true);


        time = 0;
        $(".img_animate").hide();
    }
    else {
    }
});


function whichBrowser() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("opera") != -1) return 'Opera';
    if (agt.indexOf("staroffice") != -1) return 'Star Office';
    if (agt.indexOf("webtv") != -1) return 'WebTV';
    if (agt.indexOf("beonex") != -1) return 'Beonex';
    if (agt.indexOf("chimera") != -1) return 'Chimera';
    if (agt.indexOf("netpositive") != -1) return 'NetPositive';
    if (agt.indexOf("phoenix") != -1) return 'Phoenix';
    if (agt.indexOf("firefox") != -1) return 'Firefox';
    if (agt.indexOf("chrome") != -1) return 'chrome';
    if (agt.indexOf("safari") != -1) return 'Safari';
    if (agt.indexOf("skipstone") != -1) return 'SkipStone';
    if (agt.indexOf("msie") != -1) return 'Internet Explorer';
    if (agt.indexOf("netscape") != -1) return 'Netscape';
    if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
    if (agt.indexOf('\/') != -1) {
        if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
            return navigator.userAgent.substr(0, agt.indexOf('\/'));
        }
        else return 'Netscape';
    } else if (agt.indexOf(' ') != -1)
        return navigator.userAgent.substr(0, agt.indexOf(' '));
    else return navigator.userAgent;

}
