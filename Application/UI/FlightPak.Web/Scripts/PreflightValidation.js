﻿// Structure for PreflightCommonCalculation
// START
var FlagForTimeChange = 0;
var globSummaryGrid = null;
var isInsertedLeg = false;

var AirportDep = {
    deplatdeg: 0,
    deplatmin: 0,
    lcdeplatdir: "N",
    deplngdeg: 0,
    deplngmin: 0,
    lcdeplngdir: "E",
    depIsDayLightSaving: null,
    depDayLightSavingStartDT: null,
    depDayLightSavingEndDT: null,
    depDaylLightSavingStartTM: "",
    depDayLightSavingEndTM: "",
    depOffsetToGMT: null,
    lnDepartWindZone: 0,
    DepAirportTakeoffBIAS: 0,
    DepID:null,
    DepCD:null
};

var AirportArrival = {
    arrlatdeg: 0,
    arrlatmin: 0,
    lcArrlatdir: "N",
    arrlngdeg: 0,
    arrlngmin: 0,
    lcArrlngdir: "E",
    arrIsDayLightSaving: null,
    arrDayLightSavingStartDT: null,
    arrDayLightSavingEndDT: null,
    arrDaylLightSavingStartTM: "",
    arrDayLightSavingEndTM: "",
    arrOffsetToGMT: null,
    lnArrivWindZone: 0,
    ArrAirportLandingBIAS: 0,
    ArrivalID:null,
    ArrivalCD:null
};

var LegDateTime = {
    DepLocalDate:null,
    DepLocalTime:null, 
    DepUtcDate :null,
    DepUtcTime :null,
    DepHomeDate :null,
    DepHomeTime :null,
    ArrLocalDate :null,
    ArrLocalTime :null,
    ArrUtcDate :null,
    ArrUtcTime :null,
    ArrHomeDate :null,
    ArrHomeTime: null,
    Changed:null
};

var HomeBaseAirport = {
    homeIsDayLightSaving: null,
    homeDayLightSavingStartDT: null,
    homeDayLightSavingEndDT: null,
    homeDaylLightSavingStartTM: '',
    homeDayLightSavingEndTM: '',
    homeOffsetToGMT: null,
    HomebaseId:null,
    HomebaseCD:null
};

var AirCraftDetail = {
    AirCraftId:0,
    PowerSettings1TakeOffBias: 0,
    PowerSettings1LandingBias: 0,
    PowerSettings1TrueAirSpeed: 0,
    PowerSettings2TakeOffBias: 0,
    PowerSettings2LandingBias: 0,
    PowerSettings2TrueAirSpeed: 0,
    PowerSettings3TakeOffBias: 0,
    PowerSettings3LandingBias: 0,
    PowerSettings3TrueAirSpeed: 0,
    IsFixedRotary: "",
    aircraftWindAltitude: 0,
    ChargeRate: 0,
    ChargeUnit: "",
    Distance: 0,
    Bias: "",
    LandingBias: "",
    TAS: "",
    ETE: "",
    Wind: 0
};

function setHomebaseAirportDetail(homebaseAirportObj) {
    var jsonObj = homebaseAirportObj;
    if(jsonObj!=null && jsonObj!='') {
        $("[id$='HomebaseAirpotObj']").val(JSON.stringify(homebaseAirportObj));
    }
}
function setArrivalAirportDetail(arrAirportObj) {
    if (arrAirportObj != null && arrAirportObj!='') {
        $("[id$='ArriAirpotObj']").val(JSON.stringify(arrAirportObj));
    }
}
function setAircraftDetail(aircraftObj) {
    if (aircraftObj != null && aircraftObj!='') {
        $("[id$='AircraftObj']").val(JSON.stringify(aircraftObj));
    }
}
function setDepAirportDetail(depAirportDetail) {
    if (depAirportDetail != '' && depAirportDetail!=null) {
        $("[id$='DepAirpotObj']").val(JSON.stringify(depAirportDetail));
    }
}
//END

function setHombaseObj() {
    var homeBase=$("[id$='HomebaseAirpotObj']").val();
    if (homeBase != null && homeBase != '') {
        var jsonObj = JSON.parse($("[id$='HomebaseAirpotObj']").val());
        if (jsonObj != null && jsonObj != '') {
            HomeBaseAirport["homeIsDayLightSaving"] = jsonObj.IsDayLightSaving;
            HomeBaseAirport["homeDayLightSavingStartDT"] = IsNullOrEmpty(jsonObj.DayLightSavingStartDT) ? null : moment(jsonObj.DayLightSavingStartDT).format();
            HomeBaseAirport["homeDayLightSavingEndDT"]= IsNullOrEmpty(jsonObj.DayLightSavingEndDT) ? null : moment(jsonObj.DayLightSavingEndDT).format();
            HomeBaseAirport["homeDaylLightSavingStartTM"] = jsonObj.DaylLightSavingStartTM;
            HomeBaseAirport["homeDayLightSavingEndTM"] = jsonObj.DayLightSavingEndTM;
            HomeBaseAirport["homeOffsetToGMT"] = jsonObj.OffsetToGMT;
            HomeBaseAirport["HomebaseId"] = jsonObj.AirportID;
            HomeBaseAirport["HomebaseCD"] = jsonObj.IcaoID;
        }
    }
}
function setDepObj() {
    if (self.CurrentLegData.DepartureAirport != null && self.CurrentLegData.DepartureAirport != '') {
        AirportDep["deplatdeg"] =self.CurrentLegData.DepartureAirport.LatitudeDegree()==null ? 0 : self.CurrentLegData.DepartureAirport.LatitudeDegree();
        AirportDep["deplatmin"] =self.CurrentLegData.DepartureAirport.LatitudeMinutes()==null ? 0 : self.CurrentLegData.DepartureAirport.LatitudeMinutes();
        AirportDep["lcdeplatdir"] = IsNullOrEmpty(self.CurrentLegData.DepartureAirport.LatitudeNorthSouth()) ? "" : self.CurrentLegData.DepartureAirport.LatitudeNorthSouth();
        AirportDep["deplngdeg"] =self.CurrentLegData.DepartureAirport.LongitudeDegrees()==null ? 0 : self.CurrentLegData.DepartureAirport.LongitudeDegrees();
        AirportDep["deplngmin"] =self.CurrentLegData.DepartureAirport.LongitudeMinutes()==null ? 0 : self.CurrentLegData.DepartureAirport.LongitudeMinutes();
        AirportDep["lcdeplngdir"] = IsNullOrEmpty(self.CurrentLegData.DepartureAirport.LongitudeEastWest()) ? "" : self.CurrentLegData.DepartureAirport.LongitudeEastWest();
        AirportDep["depIsDayLightSaving"] = self.CurrentLegData.DepartureAirport.IsDayLightSaving();
        AirportDep["depDayLightSavingStartDT"]=self.CurrentLegData.DepartureAirport.DayLightSavingStartDT();
        AirportDep["depDayLightSavingEndDT"]=self.CurrentLegData.DepartureAirport.DayLightSavingEndDT();
        AirportDep["depDaylLightSavingStartTM"] = self.CurrentLegData.DepartureAirport.DaylLightSavingStartTM() == null ? "" : self.CurrentLegData.DepartureAirport.DaylLightSavingStartTM();
        AirportDep["depDayLightSavingEndTM"] = self.CurrentLegData.DepartureAirport.DayLightSavingEndTM() == null ? "" : self.CurrentLegData.DepartureAirport.DayLightSavingEndTM();
        AirportDep["depOffsetToGMT"] = self.CurrentLegData.DepartureAirport.OffsetToGMT();
        AirportDep["lnDepartWindZone"] = (self.CurrentLegData.DepartureAirport.WindZone() == null || self.CurrentLegData.DepartureAirport.WindZone() == '') ? 0 : self.CurrentLegData.DepartureAirport.WindZone();
        AirportDep["DepAirportTakeoffBIAS"] = (self.CurrentLegData.DepartureAirport.TakeoffBIAS() == null || self.CurrentLegData.DepartureAirport.TakeoffBIAS() == '') ? 0 : self.CurrentLegData.DepartureAirport.TakeoffBIAS();
        AirportDep["DepID"] = self.CurrentLegData.DepartureAirport.AirportID() == 0 || self.CurrentLegData.DepartureAirport.AirportID() == null ? null : self.CurrentLegData.DepartureAirport.AirportID();
        AirportDep["DepCD"] = self.CurrentLegData.DepartureAirport.IcaoID();
        }
} 
function setArrObj() {
        if (self.CurrentLegData.ArrivalAirport != null && self.CurrentLegData.ArrivalAirport != '') {
            AirportArrival["arrlatdeg"] = self.CurrentLegData.ArrivalAirport.LatitudeDegree() == null ? 0 : self.CurrentLegData.ArrivalAirport.LatitudeDegree();
            AirportArrival["arrlatmin"] = self.CurrentLegData.ArrivalAirport.LatitudeMinutes() == null ? 0 : self.CurrentLegData.ArrivalAirport.LatitudeMinutes();
            AirportArrival["lcArrlatdir"] = IsNullOrEmpty(self.CurrentLegData.ArrivalAirport.LatitudeNorthSouth()) ? "N" : self.CurrentLegData.ArrivalAirport.LatitudeNorthSouth();
            AirportArrival["arrlngdeg"] = self.CurrentLegData.ArrivalAirport.LongitudeDegrees() == null ? 0 : self.CurrentLegData.ArrivalAirport.LongitudeDegrees();
            AirportArrival["arrlngmin"] = self.CurrentLegData.ArrivalAirport.LongitudeMinutes() == null ? 0 : self.CurrentLegData.ArrivalAirport.LongitudeMinutes();
            AirportArrival["lcArrlngdir"] = IsNullOrEmpty(self.CurrentLegData.ArrivalAirport.LongitudeEastWest()) ? "E" : self.CurrentLegData.ArrivalAirport.LongitudeEastWest();
            AirportArrival["arrIsDayLightSaving"] = self.CurrentLegData.ArrivalAirport.IsDayLightSaving();
            AirportArrival["arrDayLightSavingStartDT"]=self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT();
            AirportArrival["arrDayLightSavingEndDT"]=self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT();
            AirportArrival["arrDaylLightSavingStartTM"] = self.CurrentLegData.ArrivalAirport.DaylLightSavingStartTM() == null ? "" : self.CurrentLegData.ArrivalAirport.DaylLightSavingStartTM();
            AirportArrival["arrDayLightSavingEndTM"] = self.CurrentLegData.ArrivalAirport.DayLightSavingEndTM() == null ? "" : self.CurrentLegData.ArrivalAirport.DayLightSavingEndTM();
            AirportArrival["arrOffsetToGMT"] = self.CurrentLegData.ArrivalAirport.OffsetToGMT();
            AirportArrival["lnArrivWindZone"] = (self.CurrentLegData.ArrivalAirport.WindZone() == null || self.CurrentLegData.ArrivalAirport.WindZone() == '') ? 0 : self.CurrentLegData.ArrivalAirport.WindZone();
            AirportArrival["ArrAirportLandingBIAS"] = (self.CurrentLegData.ArrivalAirport.LandingBIAS() == null || self.CurrentLegData.ArrivalAirport.LandingBIAS() == '') ? 0 : self.CurrentLegData.ArrivalAirport.LandingBIAS();
            AirportArrival["ArrivalID"]=self.CurrentLegData.ArrivalAirport.AirportID()==0||self.CurrentLegData.ArrivalAirport.AirportID()==null ? null : self.CurrentLegData.ArrivalAirport.AirportID();
            AirportArrival["ArrivalCD"] = self.CurrentLegData.ArrivalAirport.IcaoID();
    }
}
function setAircraftObj() {

        var aircraft = $("[id$='AircraftObj']").val();
        if (aircraft != null && aircraft!='') {
            var jsonObj = JSON.parse($("[id$='AircraftObj']").val());
            if (jsonObj != null && jsonObj != '') {
                AirCraftDetail["AirCraftId"] = jsonObj.AircraftID;
                AirCraftDetail["PowerSettings1TakeOffBias"] = jsonObj.PowerSettings1TakeOffBias;
                AirCraftDetail["PowerSettings1LandingBias"] = jsonObj.PowerSettings1LandingBias;
                AirCraftDetail["PowerSettings1TrueAirSpeed"] = jsonObj.PowerSettings1TrueAirSpeed;
                AirCraftDetail["PowerSettings2TakeOffBias"] = jsonObj.PowerSettings2TakeOffBias;
                AirCraftDetail["PowerSettings2LandingBias"] = jsonObj.PowerSettings2LandingBias;
                AirCraftDetail["PowerSettings2TrueAirSpeed"] = jsonObj.PowerSettings2TrueAirSpeed;
                AirCraftDetail["PowerSettings3TakeOffBias"] = jsonObj.PowerSettings3TakeOffBias;
                AirCraftDetail["PowerSettings3LandingBias"] = jsonObj.PowerSettings3LandingBias;
                AirCraftDetail["PowerSettings3TrueAirSpeed"] = jsonObj.PowerSettings3TrueAirSpeed;
                AirCraftDetail["IsFixedRotary"] = jsonObj.IsFixedRotary;
                AirCraftDetail["aircraftWindAltitude"] = jsonObj.WindAltitude;
                AirCraftDetail["ChargeRate"] = jsonObj.ChargeRate;
                AirCraftDetail["ChargeUnit"] = jsonObj.ChargeUnit;
                AirCraftDetail["Distance"] = (self.CurrentLegData.StrConvertedDistance() == null || self.CurrentLegData.StrConvertedDistance() == '') ? 0 : self.CurrentLegData.StrConvertedDistance();
                AirCraftDetail["Bias"] = $("[id$='tbBias']").val();
                AirCraftDetail["LandingBias"] = $("[id$='tbLandBias']").val();
                AirCraftDetail["TAS"] = $("[id$='tbTAS']").val();
                AirCraftDetail["ETE"] =  $("[id$='tbETE']").val();
                AirCraftDetail["Wind"] = IsNullOrEmpty($("[id$='tbWind']").val()) ? 0 : $("[id$='tbWind']").val();
            }
        }
}
function setDate() {
    LegDateTime["DepLocalDate"] = checkedDateValue(self.CurrentLegData.DepartureLocalDate());
    LegDateTime["DepLocalTime"] = checkInvalidTime(self.CurrentLegData.DepartureLocalTime());
    LegDateTime["DepUtcDate"] = checkedDateValue(self.CurrentLegData.DepartureGreenwichDate());
    LegDateTime["DepUtcTime"] = checkInvalidTime(self.CurrentLegData.DepartureGreenwichTime());
    LegDateTime["DepHomeDate"] = checkedDateValue(self.CurrentLegData.HomeDepartureDate());
    LegDateTime["DepHomeTime"] = checkInvalidTime(self.CurrentLegData.HomeDepartureLocal());
    LegDateTime["ArrLocalDate"] = checkedDateValue(self.CurrentLegData.ArrivalLocalDate());
    LegDateTime["ArrLocalTime"] = checkInvalidTime(self.CurrentLegData.ArrivalLocalTime());
    LegDateTime["ArrUtcDate"] = checkedDateValue(self.CurrentLegData.ArrivalGreenwichDate());
    LegDateTime["ArrUtcTime"] = checkInvalidTime(self.CurrentLegData.ArrivalGreenwichTime());
    LegDateTime["ArrHomeDate"] = checkedDateValue(self.CurrentLegData.HomeArrivalDate());
    LegDateTime["ArrHomeTime"] = checkInvalidTime(self.CurrentLegData.HomeArrivalTime());
}
function initializeTripMain() {
    $("[id$='tbPhone']").attr("readonly", true);
    $("[id$='tbCost']").attr("readonly", true);
    if ($("[id$='hdTailNo']").val() != '') {
        $("[id$='tbType']").attr("disabled", "disabled");
    }
}

function checkedDateValue(date) {
    var newParseDate;
    if (IsNullOrEmpty(date)) {
        return null;
    } else {
        newParseDate = moment(date, self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase());
        if(newParseDate=="Invalid Date") {
            return null;
        } else {
           return newParseDate;
        }
    }
}

function checkInvalidTime(time) {
    return time.toLowerCase() == "invalid date" ? "00:00" : time;
}

function setAirportDetails(airportObservable, AirportData) {
    airportObservable.AirportID(AirportData.AirportID);
    airportObservable.IcaoID(AirportData.IcaoID == null ? "" : AirportData.IcaoID);
    airportObservable.CityName(AirportData.CityName);
    airportObservable.StateName(AirportData.StateName);
    airportObservable.CountryName(AirportData.CountryName);
    airportObservable.AirportName(AirportData.AirportName);
    airportObservable.CountryID(AirportData.CountryID);
    airportObservable.LatitudeDegree(AirportData.LatitudeDegree);
    airportObservable.LatitudeMinutes(AirportData.LatitudeMinutes);
    airportObservable.LatitudeNorthSouth(AirportData.LatitudeNorthSouth);
    airportObservable.LongitudeDegrees(AirportData.LongitudeDegrees);
    airportObservable.LongitudeMinutes(AirportData.LongitudeMinutes);
    airportObservable.LongitudeEastWest(AirportData.LongitudeEastWest);
    airportObservable.IsDayLightSaving(AirportData.IsDayLightSaving);
    airportObservable.DayLightSavingStartDT(AirportData.DayLightSavingStartDT != null ? moment(AirportData.DayLightSavingStartDT).format() : null);
    airportObservable.DayLightSavingEndDT(AirportData.DayLightSavingEndDT != null ? moment(AirportData.DayLightSavingEndDT).format() : null);
    airportObservable.DaylLightSavingStartTM(AirportData.DaylLightSavingStartTM);
    airportObservable.DayLightSavingEndTM(AirportData.DayLightSavingEndTM);
    airportObservable.OffsetToGMT(AirportData.OffsetToGMT);
    airportObservable.WindZone((AirportData.WindZone == null || AirportData.WindZone == '') ? 0 : AirportData.WindZone);
    airportObservable.LandingBIAS(AirportData.LandingBIAS);
    airportObservable.TakeoffBIAS(AirportData.TakeoffBIAS);
}
function Summary_Validate_Retrieve(SortSummary) {
    var dateformat = self.UserPrincipal._ApplicationDateFormat;
    var TenMin = self.UserPrincipal._TimeDisplayTenMin == null ? 1 : self.UserPrincipal._TimeDisplayTenMin;
    var ElapseTMRounding = self.UserPrincipal._ElapseTMRounding;
    if (SortSummary != '' && SortSummary != null) {
        try {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PreFlightValidator.aspx/Summary_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'DateFormat': dateformat, 'TenMin': TenMin, 'ElapseTM': ElapseTMRounding }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var jsonObj = JSON.parse(result.d);
                }
            });
        } catch (e) {}
        
    }
}

function PowerSettingGrid_Validate_Retrieve(dgPowerSetting) {
    if (dgPowerSetting != null && dgPowerSetting != '') {
        try {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PreFlightValidator.aspx/PowersettingGrid_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'aircraftId': self.Preflight.PreflightMain.Aircraft.AircraftID(), 'TenMin': self.UserPrincipal._TimeDisplayTenMin }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj != '' && jsonObj != null && jsonObj.PowerGrid != null && jsonObj.PowerGrid != '') {
                        var jsondata = jsonObj.PowerGrid;
                        jQuery(dgPowerSetting).jqGrid({
                            data: jsondata,
                            datatype: "local",
                            height: 'auto',
                            autowidth: false,
                            shrinkToFit: true,
                            width: 360,
                            colModel: [
                                { name: 'PowerSetting', index: 'PowerSetting', width: 550 },
                                { name: 'TAS', index: 'TAS' },
                                { name: 'HrRange', index: 'HrRange', width: 200 },
                                { name: 'TOBias', index: 'TOBias', width: 200 },
                                { name: 'LandBias', index: 'LandBias', width: 200 },
                                { name: 'ConvertTOBias', index: 'ConvertTOBias', hidden: true },
                                { name: 'ConvertLandBias', index: 'ConvertLandBias', hidden: true },
                                { name: 'Power', index: 'Power', hidden: true }
                            ],
                            viewrecords: true,
                            ondblClickRow: function (rowId) {
                                var rowData = jQuery(this).getRowData(rowId);
                                self.CurrentLegData.PowerSetting(rowData['Power']);
                                self.CurrentLegData.TrueAirSpeed(rowData['TAS']);
                                self.CurrentLegData.StrTakeoffBIAS(rowData['ConvertTOBias']);
                                self.CurrentLegData.StrLandingBias(rowData['ConvertLandBias']);
                                ClosePowerSettingPopup();
                                PowersettingValidation();
                                FlagForTimeChange = 1;
                                var isDepartureConfirmed = true;
                                if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
                                    isDepartureConfirmed = false;
                            }
                        });
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        } catch (e) { }
    }
}

function GetSelectedPowerSetting() {
    var selectedRow = $("#dgPowerSetting").jqGrid('getGridParam', 'selrow');
    if (selectedRow.length > 0) {
        var rowData = $("#dgPowerSetting").getRowData(selectedRow);
        self.CurrentLegData.PowerSetting(rowData['Power']);
        self.CurrentLegData.TrueAirSpeed(rowData['TAS']);
        self.CurrentLegData.StrTakeoffBIAS(rowData['ConvertTOBias']);
        self.CurrentLegData.StrLandingBias(rowData['ConvertLandBias']);
        ClosePowerSettingPopup();
        PowersettingValidation();
        FlagForTimeChange = 1;
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
    }
}

function ValidationDeaprtDate(tbDepartDate, hdDepartDate) {
    var departdate = $(tbDepartDate).val();
    var hddepartDtae = $(hdDepartDate).val();
    $(hdDepartDate).val(departdate);
    if (departdate != null && departdate != '' && departdate != hddepartDtae) {
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/DepartDateValidation",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'departDate':departdate}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.Result == "true") {
                    jConfirm("This will auto update all leg dates?", "Confirmation!", confirmDateCallBackFn);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function ChangeLegDate(tbDepartDate, dateFormate) {
    var departdate = $(tbDepartDate).val();
    if (departdate != '' && departdate != null) {
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/LegDateChange",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'departdate':departdate,'formate':dateFormate}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    $(ShortLegSummary).jqGrid('GridUnload');
                    $(DetailedLegSummary).jqGrid('GridUnload');
                    InitializeSortSummaryDetail();
                    InitializeDetailedLegSummary();
                    getTripExceptionGrid("reload");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function DepartureIcao_Validate_Retrieve(tbDepartureIcao, TimeDisplayTenMin, CurrentLegNum, btn) {
    var depatureName = tbDepartureIcao();
        Start_Loading(btn);
        var LegNum = CurrentLegNum();
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/DepartureIcao_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: "{Departure:'" + depatureName + "',LegNum:'" + LegNum + "',TimeDisplayTenMin:'" + TimeDisplayTenMin + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.Result != '0' && jsonObj.Result != null) {
                        setDepAirportDetail(jsonObj.AirportData);
                        $("[id$='lbcvDepart']").text("");

                        self.CurrentLegData.DepartureAirport.StrTakeoffBIAS(jsonObj.DepartsTakeoffbias);
                        self.CurrentLegData.DepartureAirport.StrLandingBIAS(jsonObj.DepartsLandingbias);

                        setAirportDetails(self.CurrentLegData.DepartureAirport, jsonObj.AirportData);
                        //TOOL_TIP
                        //START
                        var tooltipString;
                        tooltipString = "ICAO : " + (jsonObj.AirportData.IcaoID)
                            + "\n" + "City : " + (jsonObj.AirportData.CityName)
                            + "\n" + "State/Province : " + (jsonObj.AirportData.StateName)
                            + "\n" + "Country : " + (jsonObj.AirportData.CountryName)
                            + "\n" + "Airport : " + (jsonObj.AirportData.AirportName)
                            + "\n" + "DST Region : " + (jsonObj.AirportData.DSTRegionCD)
                            + "\n" + "UTC+/- : " + (jsonObj.AirportData.OffsetToGMT)
                            + "\n" + "Longest Runway : " + (jsonObj.AirportData.LongestRunway)
                            + "\n" + "IATA : " + (jsonObj.AirportData.Iata);
                        $("[id$='lbDepartIcao']").attr("title", tooltipString);
                        $("[id$='lbDepart']").attr("title", tooltipString);

                        //Company Profile-Color changes in Airport textbox if Notes & Alerts are present

                        if (jsonObj.AirportData.AirportID == self.CurrentLegData.DepartureAirport.AirportID()) {
                            tooltipString = '';
                            if (jsonObj.AirportData.Alerts != '' && jsonObj.AirportData.Alerts != null) {
                                $("[id$='tbDepart']").css("color", "red");
                                tooltipString = "Alerts : \n" + jsonObj.AirportData.Alerts;
                            } else {
                                $("[id$='tbDepart']").css("color", "black");
                            }

                            if (jsonObj.AirportData.GeneralNotes != '' && jsonObj.AirportData.GeneralNotes != null) {
                                tooltipString += tooltipString == '' ? "Notes : \n" + jsonObj.AirportData.GeneralNotes : "\n\nNotes : \n" + jsonObj.AirportData.GeneralNotes;
                            }
                            $("[id$='tbDepart']").attr("title", tooltipString);
                            var isDepartureConfirmed = true;
                            if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
                                isDepartureConfirmed = false;
                            FlagForTimeChange = 1;
                            //Change Leg tab Header
                            var arrIcaoID = IsNullOrEmpty(self.CurrentLegData.ArrivalAirport.IcaoID()) ? "" : self.CurrentLegData.ArrivalAirport.IcaoID();
                            $("#Leg" + self.CurrentLegData.LegNUM()).text('Leg ' + self.CurrentLegData.LegNUM() + '(' + self.CurrentLegData.DepartureAirport.IcaoID() + '-' + arrIcaoID + ')');

                            DomesticInternational_Validate_Retrieve();
                            End_Loading(btn, true);
                            var localdate = $("[id$='tbLocalDate']").val();
                            if (localdate == null && localdate == '') {
                                localdate = true;
                            }
                            PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Airport", null);
                            getTripExceptionGrid("reload");
                            collapseItem(false);
                        }
                        //END
                    } else {
                    End_Loading(btn, false);
                    if ( !IsNullOrEmpty( depatureName)) {
                        $("[id$='lbcvDepart']").text("Airport Code Does Not Exist");
                    }
                        self.CurrentLegData.DepartICAOID(null);
                        resetKOViewModel(self.CurrentLegData.DepartureAirport, ["AirportID"]);
                        self.CurrentLegData.DepartureAirport.AirportID(0);
                        $("[id$='lbDepart']").text('');
                        $("[id$='lbDepartsICAO']").text('');
                        $("[id$='lbDepartsCity']").text('');
                        $("[id$='lbDepartsState']").text('');
                        $("[id$='lbDepartsCountry']").text('');
                        $("[id$='lbDepartsAirport']").text('');
                        getTripExceptionGrid("reload");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
}

function ArrivalIcao_Validate_Retrieve(tbArrivesIcao, TimeDisplayTenMin, CurrentLegNum,btn) {
    var ArrivesName = tbArrivesIcao();
        Start_Loading(btn);
        var LegNum = CurrentLegNum();
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/ArrivalIcao_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: "{arrival:'" + ArrivesName + "',LegNUM:'" + LegNum + "',TimeDisplayTenMin:'" + TimeDisplayTenMin + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj.Result != '0' && jsonObj.Result != null) {
                        setArrivalAirportDetail(jsonObj.AirportData);
                        $("[id$='lbcvArrival']").text("");

                        self.CurrentLegData.ArrivalAirport.StrTakeoffBIAS(jsonObj.ArrivesTakeoffbias);
                        self.CurrentLegData.ArrivalAirport.StrLandingBIAS(jsonObj.ArrivesLandingbias);

                        setAirportDetails(self.CurrentLegData.ArrivalAirport, jsonObj.AriportViewModel);
                        //TOOL_TIP
                        //START
                        var tooltipString;
                        tooltipString = "ICAO : " + (jsonObj.AirportData.IcaoID)
                            + "\n" + "City : " + (jsonObj.AirportData.CityName)
                            + "\n" + "State/Province : " + (jsonObj.AirportData.StateName)
                            + "\n" + "Country : " + (jsonObj.AirportData.CountryName)
                            + "\n" + "Airport : " + (jsonObj.AirportData.AirportName)
                            + "\n" + "DST Region : " + (jsonObj.AirportData.DSTRegionCD)
                            + "\n" + "UTC+/- : " + (jsonObj.AirportData.OffsetToGMT)
                            + "\n" + "Longest Runway : " + (jsonObj.AirportData.LongestRunway)
                            + "\n" + "IATA : " + (jsonObj.AirportData.Iata);
                        $("[id$='lbArriveIcao']").attr("title", tooltipString);
                        $("[id$='lbArrival']").attr("title", tooltipString);
                        //Company Profile-Color changes in Airport textbox if Notes & Alerts are present 
                        if (jsonObj.AirportData.AirportID == self.CurrentLegData.ArrivalAirport.AirportID()) {
                            tooltipString = '';
                            if (jsonObj.AirportData.Alerts != '' && jsonObj.AirportData.Alerts != null) {
                                $("[id$='tbArrival']").css("color", "red");
                                tooltipString = "Alerts : \n" + jsonObj.AirportData.Alerts;
                            } else {
                                $("[id$='tbArrival']").css("color", "black");
                            }

                            if (jsonObj.AirportData.GeneralNotes != '' && jsonObj.AirportData.GeneralNotes != null) {
                                tooltipString += tooltipString == '' ? "Notes : \n" + jsonObj.AirportData.GeneralNotes : "\n\nNotes : \n" + jsonObj.AirportData.GeneralNotes;
                            }
                            $("[id$='tbArrival']").attr("title", tooltipString);
                        }
                        //Leg Tab Header
                        //START
                        var DepartureIcaoID = IsNullOrEmpty(self.CurrentLegData.DepartureAirport.IcaoID()) ? "" : self.CurrentLegData.DepartureAirport.IcaoID();
                        $("#Leg" + self.CurrentLegData.LegNUM()).text('Leg ' + self.CurrentLegData.LegNUM() + '(' + DepartureIcaoID + '-' + self.CurrentLegData.ArrivalAirport.IcaoID() + ')');
                        //END

                        DomesticInternational_Validate_Retrieve();
                        End_Loading(btn, true);
                        var isDepartureConfirmed = true;
                        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
                            isDepartureConfirmed = false;
                        FlagForTimeChange = 1;
                        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Airport", null);
                        collapseItem(false);
                        //END
                    } else {
                        End_Loading(btn, false);
                        if ( !IsNullOrEmpty( ArrivesName)) {
                            $("[id$='lbcvArrival']").text("Airport Code Does Not Exist");
                        }
                        self.CurrentLegData.ArrivalAirport.AirportName('');
                        $("[id$='lbArrivesICAO']").text('');
                        self.CurrentLegData.ArrivalAirport.CityName('');
                        self.CurrentLegData.ArrivalAirport.StateName('');
                        self.CurrentLegData.ArrivalAirport.CountryName('');
                        self.CurrentLegData.ArriveICAOID(null);
                        resetKOViewModel(self.CurrentLegData.ArrivalAirport, ["AirportID"]);
                        self.CurrentLegData.ArrivalAirport.AirportID(0);
                        getTripExceptionGrid("reload");
                    }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
}

function Category_Validate_Retrieve(tbCategoryCD, hdLegNum,btn) {
    if (tbCategoryCD() != '' && tbCategoryCD() != null) {
        var LegNum = hdLegNum;
        Start_Loading(btn);
        try {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PreFlightValidator.aspx/Category_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'CategoryCD': tbCategoryCD(), 'LegNum': LegNum }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj.Result == "1") {
                        End_Loading(btn, true);
                        if (jsonObj.ShowConfirmPopUp == true) {
                            self.CurrentLegData.FlightCategoryID(jsonObj.DeadCategory);
                            self.CurrentLegData.isDeadCategory(jsonObj.isDeadCategory);
                            tbCategoryCD(jsonObj.CategoryCD);
                            $("[id$='lbcvCategory']").text('');
                            jConfirm("Passengers existing for this leg will be deleted. Do you want to continue changing the Flight category?", "Confirmation!", confirmDeadHeadLegCallBackFn);
                        }
                        else {
                            tbCategoryCD(jsonObj.CategoryCD);
                            self.CurrentLegData.isDeadCategory(jsonObj.isDeadCategory);
                            self.CurrentLegData.FlightCategoryID(jsonObj.hdCategoryID);
                            $("[id$='lbcvCategory']").text('');
                        }
                    } else {
                        End_Loading(btn, false);
                        $("[id$='lbcvCategory']").text('Category Does Not Exist');
                        $("[id$='hdnDeadCategory']").val('');
                        $("[id$='hdCategory']").val('');
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    End_Loading(btn, false);
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        } catch (e) { }
    } else {
        End_Loading(btn, false);
        tbCategoryCD('');
        self.CurrentLegData.FlightCategoryID('');
        self.CurrentLegData.isDeadCategory(false);
        $("[id$='lbcvCategory']").text('');
        $("[id$='hdnDeadCategory']").val('');
        $("[id$='hdCategory']").val('');
    }
}

function CategoryChange_PassengersDelete(hdLegNum,isDelete) {
    var LegNum = hdLegNum();
    $.ajax({
        async: true,
        url: "/Views/Transactions/PreFlightValidator.aspx/CategoryChange_PassengersDelete",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({'LegNum':LegNum,'isDelete':isDelete}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.Result == "1") {
                    if (isDelete == true) {
                        self.CurrentLegData.PassengerTotal(jsonObj.tbPax);
                        self.CurrentLegData.ReservationAvailable(jsonObj.tbAvail);
                    }
                    else {
                        self.CurrentLegData.FlightCatagory.FlightCatagoryCD(jsonObj.FlightCatagoryCD);
                        self.CurrentLegData.FlightCategoryID(jsonObj.FlightCatagoryID);
                    }
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
}

function CrewDutyRule_Validate_Retrieve(CrewDutyRuleCD,btn) {
    var LegNum=self.CurrentLegData.LegNUM();
    var IsEndDuty = self.CurrentLegData.IsDutyEnd() == null ? false : self.CurrentLegData.IsDutyEnd();
    var RuleCd = $.trim(CrewDutyRuleCD());
    if (RuleCd != '' && RuleCd != null) {
        Start_Loading(btn);
        try {
            $.ajax({
                async: true,
                url: "/Views/Transactions/PreFlightValidator.aspx/CrewDutyRule_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'CrewDutyRuleCD': RuleCd, 'LegNum': LegNum, 'isEndDuty': IsEndDuty }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj.Result == "1") {
                        End_Loading(btn, true);
                        $("[id$='lbcvCrewRules']").text("");
                        jAlert("warning- Changing the rules on this leg will change the rules for the entire duty day", "Trip Alert", null);
                        self.CurrentLegData.CrewDutyRulesID(jsonObj.CrewDutyRule[0].CrewDutyRulesID);
                        self.CurrentLegData.CrewDutyRule.CrewDutyRulesID(jsonObj.CrewDutyRule[0].CrewDutyRulesID);
                        self.CurrentLegData.CrewDutyRule.CrewDutyRuleCD(jsonObj.CrewDutyRule[0].CrewDutyRuleCD);
                        self.CurrentLegData.FedAviationRegNUM(jsonObj.CrewDutyRule[0].FedAviatRegNum);

                        //TOOLTIP
                        //START
                        var tootltip = '';
                        tootltip = "Duty Day Start  : " + (jsonObj.CrewDutyRule[0].DutyDayBeingTM)
                        + "\n" + "Duty Day End    : " + (jsonObj.CrewDutyRule[0].DutyDayEndTM)
                        + "\n" + "Max. Duty Hrs. Allowed     : " + (jsonObj.CrewDutyRule[0].MaximumDutyHrs)
                        + "\n" + "Max. Flight Hrs. Allowed     : " + (jsonObj.CrewDutyRule[0].MaximumFlightHrs)
                        + "\n" + "Min. Fixed Rest : " + (jsonObj.CrewDutyRule[0].MinimumRestHrs);
                        $("[id$='lbCrewRules']").attr('title', tootltip);
                        $("[id$='tbCrewRules']").attr('title', tootltip);
                        //END
                        var isDepartureConfirmed = true;
                        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
                            isDepartureConfirmed = false;
                        FlagForTimeChange = 1;
                        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "CrewDutyRule");
                    } else {
                        End_Loading(btn, false);
                        self.CurrentLegData.CrewDutyRule.CrewDutyRulesID("");
                        self.CurrentLegData.CrewDutyRulesID("");
                        $("[id$='lbcvCrewRules']").text("Crew Duty Code Does Not Exist");
                        self.CurrentLegData.FedAviationRegNUM("");
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    End_Loading(btn, false);
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        } catch (e) { }
    }
}

function Miles_Validate_Retrieve() {
    var isDepartureConfirmed = true;
    if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
        isDepartureConfirmed = false;
    FlagForTimeChange = 1;
    PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Miles");
}

function PowerSetting_Validate_Retrieve(tbPower, tbAircraft, TimeDisplayTenMin,btnPowerSeting) {
    if (tbPower() != '' && tbPower() != null) {
        var power = tbPower();
        var aircraftId = IsNullOrEmpty(tbAircraft()) == true ? "0" : tbAircraft();
        Start_Loading(btnPowerSeting);
        return $.ajax({
                async: true,
                url: "/Views/Transactions/PreFlightValidator.aspx/PowerSetting_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: "{Power:'" + power + "',AircraftID:'" + aircraftId + "',TimeDisplayTenMin:'" + TimeDisplayTenMin + "'}",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj.Result == "1") {
                        $("[id$='lbcvPower']").text("");
                        self.CurrentLegData.TrueAirSpeed(jsonObj.TAS);
                        self.CurrentLegData.StrTakeoffBIAS(jsonObj.DepartsTakeoffbias);
                        self.CurrentLegData.StrLandingBias(jsonObj.DepartsLandingbias);
                        var isDepartureConfirmed = true;
                        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
                            isDepartureConfirmed = false;
                        FlagForTimeChange = 1;
                        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Power");
                        End_Loading(btnPowerSeting, true);
                    } else {
                        $("[id$='lbcvPower']").text("Power Setting must be 1, 2, or 3");
                        self.CurrentLegData.TrueAirSpeed(jsonObj.TAS);
                        self.CurrentLegData.StrTakeoffBIAS(jsonObj.DepartsTakeoffbias);
                        self.CurrentLegData.StrLandingBias(jsonObj.DepartsLandingbias);
                        End_Loading(btnPowerSeting, false);
                    } 
                }, error: function (jqXHR, textStatus, errorThrown) {
                    End_Loading(btnPowerSeting,false);
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
    }
}

function CalculateLegDateTime_Validate_Retrieve() {
    setDate();
    var isAutoLegDateClac = self.UserPrincipal._IsAutomaticCalcLegTimes == null ? false : self.UserPrincipal._IsAutomaticCalcLegTimes;
    var dateformat = self.UserPrincipal._ApplicationDateFormat;
    var hdDepart = self.CurrentLegData.DepartureAirport.AirportID();
    var hdHomebase = $("[id$='hdHomebaseAirport']").val() == '' ? 0 : $("[id$='hdHomebaseAirport']").val();
    return $.ajax({
        async: true,
        url: "/Views/Transactions/PreFlightValidator.aspx/CalculateLegDateTime_Validate_Retrieve",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({
            'DateFormat': dateformat, 'IsAutomaticCalcLegTimes': isAutoLegDateClac, 'DepartICAO': hdDepart, 'date': LegDateTime
        }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                var jsonObj = result.d.Result;
                self.CurrentLegData.DepartureDTTMLocal(jsonObj.DepartureLocalDate);
                self.CurrentLegData.DepartureLocalTime(jsonObj.LocalDepartureTime);
                self.CurrentLegData.DepartureGreenwichDTTM(jsonObj.DepartureUtcDate);
                self.CurrentLegData.DepartureGreenwichTime(jsonObj.UtcDepartureTime);
                self.CurrentLegData.HomeDepartureDTTM(jsonObj.DepartureHomeDate);
                self.CurrentLegData.HomeDepartureLocal(jsonObj.HomeDepartureTime);
                self.CurrentLegData.HomeArrivalTime(jsonObj.HomeArrivalTime);
                self.CurrentLegData.ArrivalLocalTime(jsonObj.LocalArrivalTime);
                self.CurrentLegData.ArrivalGreenwichTime(jsonObj.UTCArrivalTime);
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function GMT_Validate_Retrieve(changedValue, time, DepartOrArr) {
    var calculate = true;
    var timeArray = time.split(':');
    var hrs = Number(timeArray[0]);
    var mins = Number(timeArray[1]);
    if(hrs>23) {
        jAlert("Hour entry must be between 0 and 23", "Trip Alert");
        calculate = false;
    }
    else if (mins > 59) {
        jAlert("Minute entry must be between 0 and 59", "Trip Alert");
        calculate = false;
    }
   if (calculate==true) {
       if (DepartOrArr == "Dep") {
           PreflightCalculation_Validate_Retrieve(true, "Date", changedValue);
       }
       else {
           PreflightCalculation_Validate_Retrieve(false, "Date", changedValue);
       }
   }
   return calculate;
}

function DomesticInternational_Validate_Retrieve() {
    var DepartureCountry = self.CurrentLegData.DepartureAirport.CountryID();
    var ArrivalCountry = self.CurrentLegData.ArrivalAirport.CountryID();
    var HomebaseCountry = $("#hdnHomeBaseCountry").val();
    var LegNum = self.CurrentLegData.LegNUM();
    $.ajax({
        async: true,
        url: "/Views/Transactions/PreFlightValidator.aspx/DomesticInternational_Validate_Retrieve",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ "HomeBaseCountryId": HomebaseCountry, "DepartureCountryId": DepartureCountry, "ArrivalCountryId": ArrivalCountry, "LegNum": LegNum }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                self.CurrentLegData.CheckGroup(result.d.Result);
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, EventChange, changedValue) {
    FormatLegDates();
    setHombaseObj();
    setDepObj();
    setArrObj();
    setAircraftObj();
    setDate();
    LegDateTime["Changed"] = changedValue == null ? "" : changedValue;
    var TenMin = self.UserPrincipal._TimeDisplayTenMin;
    var ElapseTMRounding = self.UserPrincipal._ElapseTMRounding;
    var Kilo = self.UserPrincipal._IsKilometer;
    var powersetting=self.CurrentLegData.PowerSetting();
    var WindRelibility = self.CurrentLegData.StrWindReliability();
    var localdate = self.CurrentLegData.DepartureLocalDate();
    if ((localdate == null || localdate == '' || localdate == undefined) && isInsertedLeg == true) {
        localdate = self.CurrentLegData.ArrivalLocalDate();
        isInsertedLeg == false;
    }
    var dateformat = self.UserPrincipal._ApplicationDateFormat;
    var LegNum = self.CurrentLegData.LegNUM();
    var CrewDutyRule = (self.CurrentLegData.CrewDutyRulesID() == null || self.CurrentLegData.CrewDutyRulesID() == '') ? null : self.CurrentLegData.CrewDutyRulesID();
    var OverrideValue = self.CurrentLegData.OverrideValue() == null ? 0 : self.CurrentLegData.OverrideValue();
    var IsEndDuty = self.CurrentLegData.IsDutyEnd() == null ? false : self.CurrentLegData.IsDutyEnd();
    var Distance = (self.CurrentLegData.StrConvertedDistance() == null || self.CurrentLegData.StrConvertedDistance() == '') ? 0 : self.CurrentLegData.StrConvertedDistance();
    var IsAutocalculate = self.UserPrincipal._IsAutomaticCalcLegTimes;
    RequestStart();
    $.ajax({
        async: false,
        url: "/Views/Transactions/PreFlightValidator.aspx/PreflightCalculation_Validate_Retrieve",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({
            'depAirport': AirportDep, 'arrAirport': AirportArrival, 'homebaseAirport': HomeBaseAirport,
            'airecraft': AirCraftDetail,'date': LegDateTime, 'TenMin': TenMin, 'Kilo': Kilo, 'WindRelibility': WindRelibility,
            'localdate': localdate, 'dateformat': dateformat, 'PowerSetting': powersetting, 'ElapseTime': ElapseTMRounding, 'LegNum': LegNum,
            'CrewDutyRule': CrewDutyRule, 'OverrideValue': OverrideValue, 'IsEndDuty': IsEndDuty, 'isDepartureConfirmed': isDepartureConfirmed,
            'Distance': Distance, 'EventChange': EventChange, 'IsAutomaticCalcLegTimes': IsAutocalculate
        }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            var jsonObj = result.d.Result;
            if (returnResult == true && result.d.Success == true && jsonObj != null && jsonObj != '') {

                if (jsonObj.Distance != null && jsonObj.Distance != '') {
                    self.CurrentLegData.StrConvertedDistance(jsonObj.Distance);
                    self.CurrentLegData.Distance(jsonObj.Miles);
                }

                if (jsonObj.CalculateBias != null && jsonObj.CalculateBias != '') {
                    self.CurrentLegData.StrTakeoffBIAS(jsonObj.CalculateBias.Bias);
                    self.CurrentLegData.StrLandingBias(jsonObj.CalculateBias.LandBias);
                    self.CurrentLegData.TrueAirSpeed(jsonObj.CalculateBias.TAS);
                }

                if (jsonObj.Wind != null && jsonObj.Wind != '') {
                    self.CurrentLegData.WindsBoeingTable(jsonObj.Wind);
                }

                if (jsonObj.ETE != null && jsonObj.ETE != '') {
                    self.CurrentLegData.StrElapseTM(jsonObj.ETE);
                }

                if (jsonObj.FlightCost != null && jsonObj.FlightCost != '') {
                    self.CurrentLegData.FlightCost(jsonObj.FlightCost);
                }

                if (jsonObj.FARRules != null && jsonObj.FARRules != '') {
                    $("[id$='lbFar']").text(jsonObj.FARRules.Far);
                    $("[id$='hdnFar']").val(jsonObj.FARRules.Far);

                    self.CurrentLegData.DutyHours(jsonObj.FARRules.lbTotalDuty);
                    $("[id$='hdnTotalDuty']").val(jsonObj.FARRules.lbTotalDuty);

                    self.CurrentLegData.FlightHours(jsonObj.FARRules.lbTotalFlight);
                    $("[id$='hdnTotalFlight']").val(jsonObj.FARRules.lbTotalFlight);

                    self.CurrentLegData.RestHours(jsonObj.FARRules.lbRest);
                    $("[id$='hdnRest']").val(jsonObj.FARRules.lbRest);
                    self.CurrentLegData.CrewDutyAlert(jsonObj.FARRules.lcCdAlert);
                }

                if (jsonObj.LegDate != null && jsonObj.LegDate != '') {
                    self.CurrentLegData.DepartureLocalDate(jsonObj.LegDate.DeptLocalDate);
                    if (jsonObj.LegDate.DeptLocalTime != null && jsonObj.LegDate.DeptLocalTime != '')
                        self.CurrentLegData.DepartureLocalTime(jsonObj.LegDate.DeptLocalTime);
                    else
                        self.CurrentLegData.DepartureLocalTime("00:00");

                    self.CurrentLegData.DepartureGreenwichDate(jsonObj.LegDate.DeptUtcDate);
                    if (jsonObj.LegDate.DeptUtcTime != null && jsonObj.LegDate.DeptUtcTime != '')
                        self.CurrentLegData.DepartureGreenwichTime(jsonObj.LegDate.DeptUtcTime);
                    else
                        self.CurrentLegData.DepartureGreenwichTime("00:00");

                    self.CurrentLegData.HomeDepartureDate(jsonObj.LegDate.DeptHomeDate);
                    if (jsonObj.LegDate.DeptHomeTime != null && jsonObj.LegDate.DeptHomeTime != '')
                        self.CurrentLegData.HomeDepartureLocal(jsonObj.LegDate.DeptHomeTime);
                    else
                        self.CurrentLegData.HomeDepartureLocal("00:00");

                    self.CurrentLegData.HomeArrivalDate(jsonObj.LegDate.ArrivalHomeDate);
                    if (jsonObj.LegDate.ArrivalHomeTime != null && jsonObj.LegDate.ArrivalHomeTime != '')
                        self.CurrentLegData.HomeArrivalTime(jsonObj.LegDate.ArrivalHomeTime);
                    else
                        self.CurrentLegData.HomeArrivalTime("00:00");

                    self.CurrentLegData.ArrivalLocalDate(jsonObj.LegDate.ArrivalLocalDate);
                    if (jsonObj.LegDate.ArrivalLocalTime != null && jsonObj.LegDate.ArrivalLocalTime != '')
                        self.CurrentLegData.ArrivalLocalTime(jsonObj.LegDate.ArrivalLocalTime);
                    else
                        self.CurrentLegData.ArrivalLocalTime("00:00");

                    self.CurrentLegData.ArrivalGreenwichDate(jsonObj.LegDate.ArrivalUtcDate);
                    if (jsonObj.LegDate.ArrivalUtcTime != null && jsonObj.LegDate.ArrivalUtcTime != '')
                        self.CurrentLegData.ArrivalGreenwichTime(jsonObj.LegDate.ArrivalUtcTime);
                    else
                        self.CurrentLegData.ArrivalGreenwichTime("00:00");

                    Summary_Validate_Retrieve(globSummaryGrid, globSummaryGrid);
                    if (isDepartureConfirmed) {
                        self.CurrentLegData.IsDepartureConfirmed(isDepartureConfirmed);
                        self.CurrentLegData.IsArrivalConfirmation(false);
                        $("[id$='lbArrivalDate']").removeClass("time_anchor_active");
                        $("[id$='lbArrivalDate']").addClass("time_anchor");
                        $("[id$='lbDepartureDate']").removeClass("time_anchor");
                        $("[id$='lbDepartureDate']").addClass("time_anchor_active");
                    }
                    else {
                        self.CurrentLegData.IsDepartureConfirmed(isDepartureConfirmed);
                        self.CurrentLegData.IsArrivalConfirmation(true);
                        $("[id$='lbDepartureDate']").removeClass("time_anchor_active");
                        $("[id$='lbDepartureDate']").addClass("time_anchor");
                        $("[id$='lbArrivalDate']").removeClass("time_anchor");
                        $("[id$='lbArrivalDate']").addClass("time_anchor_active");
                    }
                    FlagForTimeChange = 0;
                    jQuery(ShortLegSummary).jqGrid('GridUnload');
                    InitializeSortSummaryDetail();
                    jQuery(DetailedLegSummary).jqGrid('GridUnload');
                    InitializeDetailedLegSummary();
                    getTripExceptionGrid("reload");
                }
            }
            
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}

function Preflight_ReCalculation_Validate_Retrieve(vTailNum) {
    SavePreflightMain();
    RequestStart();
    $.ajax({
        async: false,
        url: "/Views/Transactions/PreFlightValidator.aspx/Preflight_ReCalculation_Validate_Retrieve",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'vTailNum': vTailNum }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            var jsonObj = result.d.Result;
            if (returnResult == true && result.d.Success == true && jsonObj == true) {
                jQuery(ShortLegSummary).jqGrid('GridUnload');
                InitializeSortSummaryDetail();
                jQuery(DetailedLegSummary).jqGrid('GridUnload');
                InitializeDetailedLegSummary();
                getTripExceptionGrid("reload");
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },complete: function () { ResponseEnd(); }
    });
}

function UpdateAllLegsCalculation() {
    $.ajax({
        async: true,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/UpdateAllLegsCalculation",
        type: "POST",
        dataType: "json",
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
        }, error: function (jqXHR, textStatus, errorThrown) {
            reportValidationError(jqXHR, textStatus, errorThrown);
        }
    });
}
