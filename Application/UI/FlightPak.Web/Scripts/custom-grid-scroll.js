﻿
$(function () {
    (function ($) {
        $.fn.ApplyCustomeScroll = function (isJqGrid) {

            if (isJqGrid == undefined || isJqGrid == "" || isJqGrid == null) {
                isJqGrid = false;
            }

            var itemNumber = 0;

            function getElementId(item) {
                return $(item).attr("data-id");
            }

            function getGridElement(item) {
                return $(".grid_element_" + getElementId(item));
            }

            function applyUniqueId(item) {
                $(item).attr("data-id", itemNumber);
                $(item).addClass("grid_element_" + itemNumber);
            }
            
            function bindScrollEvents() {
                
                $(".nav-top-scroll").click(function () {
                    var $grid_element = getGridElement($(this));
                    var leftPos = $($grid_element).scrollTop();
                    
                    $($grid_element).animate({
                        scrollTop: leftPos - 100
                    }, 100);
                    
                    $($grid_element).ApplyCustomeScroll_TopScrollStatus();
                });

                $(".nav-bottom-scroll").click(function () {
                    var $grid_element = getGridElement($(this));
                    var leftPos = $($grid_element).scrollTop();
                    
                    $($grid_element).animate({
                        scrollTop: leftPos + 100
                    }, 100);
                    
                    $($grid_element).ApplyCustomeScroll_TopScrollStatus();
                });

                $(".nav-left-scroll").click(function () {
                    var $grid_element = getGridElement($(this));
                    var leftPos = $($grid_element).scrollLeft();
                    
                    $($grid_element).animate({
                        scrollLeft: leftPos - 100
                    }, 100);
                    
                    $($grid_element).ApplyCustomeScroll_LeftScrollStatus();
                });

                $(".nav-right-scroll").click(function () {
                    var $grid_element = getGridElement($(this));
                    var leftPos = $($grid_element).scrollLeft();
                    
                    $($grid_element).animate({
                        scrollLeft: leftPos + 100
                    }, 100);
                    
                    $($grid_element).ApplyCustomeScroll_LeftScrollStatus();
                });
            }

            $.fn.ApplyCustomeScroll_TopScrollStatus = function () {
                var $rgDataDiv = this;
                var id = getElementId($rgDataDiv);
                if ($($rgDataDiv).scrollTop() <= 0) {
                    $(".top-scroll-event_" + id).css('display', 'none');
                    $($rgDataDiv).scrollTop(0);
                } else {
                    $(".top-scroll-event_" + id).css('display', 'block');
                }

                if ($($rgDataDiv).scrollTop() + $($rgDataDiv).innerHeight() >= $($rgDataDiv)[0].scrollHeight) {
                    $(".bottom-scroll-event_" + id).css('display', 'none');
                    if (!isJqGrid) {
                        $($rgDataDiv).scrollTop($($rgDataDiv)[0].scrollHeight + 50);
                    }
                } else {
                    $(".bottom-scroll-event_" + id).css('display', 'block');
                }
                return this;
            };

            $.fn.ApplyCustomeScroll_LeftScrollStatus = function () {
                var $rgDataDiv = this;
                var id = getElementId($rgDataDiv);
                if ($($rgDataDiv).scrollLeft() <= 0) {
                    $(".left-scroll-event_" + id).css('display', 'none');
                    $($rgDataDiv).scrollLeft(0);
                } else {
                    $(".left-scroll-event_" + id).css('display', 'block');
                }

                if ($($rgDataDiv).scrollLeft() + $($rgDataDiv).innerWidth() >= $($rgDataDiv)[0].scrollWidth) {
                    $(".right-scroll-event_" + id).css('display', 'none');
                    if (!isJqGrid) {
                        $($rgDataDiv).scrollLeft($($rgDataDiv)[0].scrollWidth + 50);
                    }
                } else {
                    $(".right-scroll-event_" + id).css('display', 'block');
                }
                return this;
            };

            this.each(function() {
                var $rgDataDiv = this;
                var topDivHeight = $($rgDataDiv).height();
                
                applyUniqueId($rgDataDiv);

                if (isJqGrid) {
                    $($rgDataDiv).removeClass("ui-jqgrid").removeClass("ui-jqgrid-bdiv");
                    $($rgDataDiv).css("overflow", "scroll");
                } else {
                    $($rgDataDiv).parent(".RadGrid").css("position", "relative");
                }
                //$($rgDataDiv).css("box-sizing", "border-box");
                //$($rgDataDiv).css("float", "left");
                

                $($rgDataDiv).prepend("<div class='no-selection nav-top-scroll top-scroll-event_" + itemNumber + "' data-id='" + itemNumber + "'></div>");
                $($rgDataDiv).prepend("<div class='no-selection nav-bottom-scroll bottom-scroll-event_" + itemNumber + "' data-id='" + itemNumber + "'></div>");
                $($rgDataDiv).prepend("<div class='no-selection nav-left-scroll left-scroll-event_" + itemNumber + "' data-id='" + itemNumber + "' style='height:" + topDivHeight + "px;'></div>");
                $($rgDataDiv).prepend("<div class='no-selection nav-right-scroll right-scroll-event_" + itemNumber + "' data-id='" + itemNumber + "' style='height:" + topDivHeight + "px;'></div>");

                $($rgDataDiv).bind('scroll', function () {
                    $rgDataDiv = $($rgDataDiv).ApplyCustomeScroll_LeftScrollStatus();
                    $rgDataDiv = $($rgDataDiv).ApplyCustomeScroll_TopScrollStatus();
                });

                $rgDataDiv = $($rgDataDiv).ApplyCustomeScroll_TopScrollStatus();
                $rgDataDiv = $($rgDataDiv).ApplyCustomeScroll_LeftScrollStatus();

                bindScrollEvents();

                itemNumber++;
            });
            
            return this;
        };
    }(jQuery));
});