﻿function reportValidationError(jqXHR, textStatus, errorThrown) {
    if (jqXHR.status && jqXHR.status == 401) {
        window.location.href = "/Account/Login.aspx";
    }
    else if (jqXHR.status && jqXHR.status == 400) {
        jAlert(jqXHR.responseText, title);
    }
}

function End_Loading_Toppage_Search(Control, Found) {
    if (Control) {
        if (Found) {
            $(Control).removeClass("browse-button-Validation-Loading");
            $(Control).addClass("browse-button-Validation-Correct");
            setTimeout(function () {
                $(Control).removeClass("browse-button-Validation-Correct");
                $(Control).addClass("searchsubmitbutton");
            }, 900);
        }
        else {
            $(Control).removeClass("browse-button-Validation-Loading");
            $(Control).addClass("browse-button-Validation-InCorrect");
            setTimeout(function () {
                $(Control).removeClass("browse-button-Validation-InCorrect");
                $(Control).addClass("searchsubmitbutton");
            }, 900);
        }
    }
}

function setTooltipforAirport(AirportIcaoId,TooltipProp,textbox,airporttype) {
    if (!IsNullOrEmpty(AirportIcaoId)) {
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/AirportDetail_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'AirportIcaoId': AirportIcaoId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.flag == 0) {
                    if (airporttype != undefined) {
                        (airporttype == "DEPT") ? setDepAirportDetail(jsonObj.AirportData) : setArrivalAirportDetail(jsonObj.AirportData);
                    }
                    var tooltipString;
                    tooltipString = "ICAO : " + (jsonObj.AirportData.IcaoID)
                        + "\n" + "City : " + (jsonObj.AirportData.CityName)
                        + "\n" + "State/Province : " + (jsonObj.AirportData.StateName)
                        + "\n" + "Country : " + (jsonObj.AirportData.CountryName)
                        + "\n" + "Airport : " + (jsonObj.AirportData.AirportName)
                        + "\n" + "DST Region : " + (jsonObj.AirportData.DSTRegionCD)
                        + "\n" + "UTC+/- : " + (jsonObj.AirportData.OffsetToGMT)
                        + "\n" + "Longest Runway : " + (jsonObj.AirportData.LongestRunway)
                        + "\n" + "IATA : " + (jsonObj.AirportData.Iata);
                    TooltipProp(tooltipString);
                    tooltipString = "";
                    if (jsonObj.AirportData.Alerts != '' && jsonObj.AirportData.Alerts != null) {
                        $(textbox).css("color", "red");
                        tooltipString = "Alerts : \n" + jsonObj.AirportData.Alerts;
                    } else {
                        $(textbox).css("color", "black");
                    }

                    if (jsonObj.AirportData.GeneralNotes != '' && jsonObj.AirportData.GeneralNotes != null) {
                        tooltipString += tooltipString == '' ? "Notes : \n" + jsonObj.AirportData.GeneralNotes : "\n\nNotes : \n" + jsonObj.AirportData.GeneralNotes;
                    }
                    $(textbox).attr("title", tooltipString);
                }
                else {
                    TooltipProp("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        TooltipProp("");
    }
}

function Client_Validate_Retrieve(ClientTextBox, ClientLabel, ClientHidden, ClientCVLabel, pfClientId, btn) {
    var ClientCD = $.trim(ClientTextBox());
    
    if (!IsNullOrEmpty(ClientCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Client_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'ClientCD':ClientCD}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == 0) {
                    ClientLabel("");
                    $(ClientCVLabel).html("Client Code Does Not Exist");
                    if (pfClientId() == null || IsNullOrEmpty(pfClientId())) {
                        ClientHidden("0");
                        pfClientId(null);
                    }
                    End_Loading(btn, false);
                }
                else {
                    End_Loading(btn, true);
                    ClientTextBox(htmlDecode(jsonObj.txtValue));
                    ClientLabel(htmlDecode(jsonObj.lblValue));
                    ClientHidden(htmlDecode(jsonObj.hdValue));
                    pfClientId(htmlDecode(jsonObj.hdValue));
                    $(ClientCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {        
        $(ClientCVLabel).html("");
        ClientHidden("0");
        ClientLabel("");
        pfClientId(null);
    }
}

function Client_Validate_Retrieve_KO(clientVM, clientID, clientErrorLabel, btn) {
    clientVM.ClientCD(clientVM.ClientCD().toUpperCase());
    var clientCD = clientVM.ClientCD();
    if (!IsNullOrEmpty(clientCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/Client_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'ClientCD': clientCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var clientErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, clientVM);
                    removeUnwantedPropertiesKOViewModel(clientVM);
                    clientID(clientVM.ClientID());
                    End_Loading(btn, true);
                }
                else {
                    clientErrorText = "Client Code Does Not Exist";
                    resetKOViewModel(clientVM, ["ClientID", "ClientCD"]);
                    clientID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(clientErrorLabel)) {
                    if (!clientErrorLabel.match('^#'))
                        clientErrorLabel = '#' + clientErrorLabel;
                    $(clientErrorLabel).text(clientErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(clientVM, ["ClientID"]);
        clientVM.ClientID(0);
        clientID("");
        if (!clientErrorLabel.match('^#'))
            clientErrorLabel = '#' + clientErrorLabel;
        $(clientErrorLabel).text("");
    }
}

function TailNo_Validate_Retrieve(TailNoTextbox, TailNoHidden, pfFleetId, TailnNoCVLabel, HomebaseTextbox, HomebaseLabel, HomebaseHidden, hdairportHomeBase, HomebaseCVLabel, TypeTextbox, TypeLabel, TypeHidden, pfType, TypeCVLabel, rfvType, EMTextbox, EMLabel, EMHidden, EMCVLabel, pfEMId, hdCalculateLeg, btn, typeBtn, typetb) {
    var TailNum = $.trim(TailNoTextbox());
    if (!IsNullOrEmpty(TailNum)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/TailNo_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'TailNum':TailNum}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.TailNum.flag == 0) {
                    End_Loading(btn, false);
                    $(TailnNoCVLabel).html("Tail No. Does Not Exist");
                    TailNoHidden("0");
                    pfFleetId(null);
                    if (typeof (TypeTextbox) !== undefined) {
                        TypeTextbox("");
                        TypeLabel("");
                        TypeHidden("0");
                        pfType(null);
                    }
                    if (typeof (EMTextbox) !== undefined) {
                        EMTextbox("");
                        EMLabel("");
                        EMHidden("0");
                        pfEMId(null);
                    }
                    getTripExceptionGrid("reload");
                }
                else {
                    End_Loading(btn, true);
                    if (hdCalculateLeg != undefined && hdCalculateLeg != null) {
                        $(hdCalculateLeg).val("True");
                    }
                    $(TailnNoCVLabel).html("");
                    TailNoTextbox(htmlDecode(jsonObj.TailNum.txtValue));
                    TailNoHidden(htmlDecode(jsonObj.TailNum.hdValue));
                    self.Preflight.PreflightMain.Fleet.MaximumPassenger(jsonObj.TailNum.Other.MaxPax);
                    pfFleetId(htmlDecode(jsonObj.TailNum.hdValue));
                    if (typeof (HomebaseTextbox) !== undefined && jsonObj.HomeBase.flag != null && jsonObj.HomeBase.flag == "1") {
                        HomebaseTextbox(htmlDecode(jsonObj.HomeBase.txtValue));
                        HomebaseLabel(htmlDecode(jsonObj.HomeBase.lblValue));
                        HomebaseHidden(htmlDecode(jsonObj.HomeBase.hdValue));
                        hdairportHomeBase(htmlDecode(jsonObj.HomeBase.Other.HomebaseAirportID));
                        $(HomebaseCVLabel).html("");
                        setTooltipforAirport(HomebaseTextbox(), self.Preflight.PreflightMain.HomebaseTooltip, document.getElementById("tbHomeBase"));
                    }
                    if (typeof (TypeTextbox) !== undefined && jsonObj.Type.flag != null && jsonObj.Type.flag == "1") {
                        TypeTextbox(htmlDecode(jsonObj.Type.txtValue));
                        TypeLabel(htmlDecode(jsonObj.Type.lblValue));
                        TypeHidden(htmlDecode(jsonObj.Type.hdValue));
                        pfType(htmlDecode(jsonObj.Type.hdValue));
                        $(TypeCVLabel).html("");
                        $(rfvType).hide();
                    }
                    if (typeof (EMTextbox) !== undefined && jsonObj.EmergencyContact.flag != null && jsonObj.EmergencyContact.flag == "1") {
                        EMTextbox(htmlDecode(jsonObj.EmergencyContact.txtValue));
                        EMLabel(htmlDecode(jsonObj.EmergencyContact.lblValue));
                        pfEMId(htmlDecode(jsonObj.EmergencyContact.hdValue));
                        $(EMCVLabel).html("");
                        EMHidden(htmlDecode(jsonObj.EmergencyContact.hdValue));
                    }
                    SavePreflightMain();
                    self.getTripExceptionGrid("reload");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            },
            complete: function ()
            {
                if (self.EditMode())
                    UpdateAllLegsCalculation();
            }
        });
    }
    else {
        End_Loading(btn, false);
        $(TailnNoCVLabel).html("");
        TailNoTextbox("");
        TailNoHidden("0");
        pfFleetId(null);
        TypeTextbox("");
        TypeLabel("");
        TypeHidden("0");
        pfType(null);
        $(TypeCVLabel).html("");EMTextbox("");
        EMLabel("");
        EMHidden("0");
        pfEMId(null);
        $(EMCVLabel).html("");
        $(typetb).removeAttr("disabled");
        $(typeBtn).removeClass("browse-button-disabled").addClass("browse-button");
        $(typeBtn).removeAttr("disabled");
        self.getTripExceptionGrid("reload");
        if (self.EditMode())
            UpdateAllLegsCalculation();

    }
}



function AircraftType_Validate_Retrieve(TypeTextBox, TypeHidden, TypeCVLabel, TypeLabel, rfvType, hdCalculateLeg, pfType, btn) {
    var AricraftCD = $.trim(TypeTextBox());
    if (!IsNullOrEmpty(AricraftCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/AircraftType_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'TypeCD':AricraftCD}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    End_Loading(btn, false);
                    TypeLabel("");
                    $(TypeCVLabel).html("Aircraft Type Does Not Exist");
                    TypeHidden("0");
                    pfType(null);
                    self.getTripExceptionGrid("reload");
                }
                else {
                    End_Loading(btn, true);
                    if (hdCalculateLeg != undefined && hdCalculateLeg != null) {
                        $(hdCalculateLeg).val("True");
                    }
                    TypeTextBox(htmlDecode(jsonObj.txtValue));
                    TypeLabel(htmlDecode(jsonObj.lblValue));
                    TypeHidden(htmlDecode(jsonObj.hdValue));
                    pfType(htmlDecode(jsonObj.hdValue));
                    $(rfvType).hide();
                    $(TypeCVLabel).html("");
                    self.getTripExceptionGrid("reload");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        TypeLabel("");
        $(TypeCVLabel).html("");
        TypeHidden("0");
        pfType(null);
        self.getTripExceptionGrid("reload");
    }
}

function EmergencyContact_Validate_Retrieve(EMTextBox, EMHidden, EMLabel, EMCVLabel, pfEMId, btn) {
    var EMContact = $.trim(EMTextBox());
    if (!IsNullOrEmpty(EMContact)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/EmergencyContact_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'ContactCD':EMContact}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0") {
                    End_Loading(btn, false);
                    EMLabel("");
                    $(EMCVLabel).html("Invalid Emergency Contact Code");
                    if (IsNullOrEmpty(pfEMId()) || pfEMId() == null) {
                        EMHidden("0");
                        pfEMId(null);
                    }
                }
                else {
                    End_Loading(btn, true);
                    EMTextBox(htmlDecode(jsonObj.txtValue));
                    EMLabel(htmlDecode(jsonObj.lblValue));
                    EMHidden(htmlDecode(jsonObj.hdValue));
                    pfEMId(htmlDecode(jsonObj.hdValue));
                    $(EMCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        EMLabel("");
        $(EMCVLabel).html("");
        EMHidden("0");
        pfEMId(null);
    }
}

function AircraftType_Validate_Retrieve_KO(aircraftVM, aircraftID, aircraftErrorLabel, btn) {
    aircraftVM.AircraftCD(aircraftVM.AircraftCD().toUpperCase());
    var aircraftCD = aircraftVM.AircraftCD();
    if (!IsNullOrEmpty(aircraftCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/AircraftType_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'aircraftCD': aircraftCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var aircraftErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, aircraftVM);
                    aircraftID(aircraftVM.AircraftID());
                    End_Loading(btn, true);
                }
                else {
                    aircraftErrorText = "Invalid AirCraftType Code";
                    resetKOViewModel(aircraftVM, ["AircraftID", "AircraftCD"]);
                    aircraftID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(aircraftErrorLabel)) {
                    if (!aircraftErrorLabel.match('^#'))
                        aircraftErrorLabel = '#' + aircraftErrorLabel;
                    $(aircraftErrorLabel).text(aircraftErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(aircraftVM);
        aircraftID("");
        End_Loading(btn, false);
        if (!aircraftErrorLabel.match('^#'))
            aircraftErrorLabel = '#' + aircraftErrorLabel;
        $(aircraftErrorLabel).text("");
    }
}

function CrewDutyType_Validate_Retrieve_KO(crewDutyTypeVM, dutyTypeID, dutyTypeErrorLabel, btn) {
    crewDutyTypeVM.DutyTypeCD(crewDutyTypeVM.DutyTypeCD().toUpperCase());
    var dutyTypeCD = crewDutyTypeVM.DutyTypeCD();
    if (!IsNullOrEmpty(dutyTypeCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/CrewDutyType_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'dutyTypeCD': dutyTypeCD }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var dutytypeErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, crewDutyTypeVM);
                    dutyTypeID(crewDutyTypeVM.DutyTypeID());
                    End_Loading(btn, true);
                }
                else {
                    dutytypeErrorText = "Invalid Duty Type Code";
                    resetKOViewModel(crewDutyTypeVM, ["DutyTypeCD", "DutyTypeID"]);
                    dutyTypeID("")
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(dutyTypeErrorLabel)) {
                    if (!dutyTypeErrorLabel.match('^#'))
                        dutyTypeErrorLabel = '#' + dutyTypeErrorLabel;
                    $(dutyTypeErrorLabel).text(dutytypeErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(crewDutyTypeVM);
        dutyTypeID("");
        End_Loading(btn, false);
        if (!dutyTypeErrorLabel.match('^#'))
            dutyTypeErrorLabel = '#' + dutyTypeErrorLabel;
        $(dutyTypeErrorLabel).text("");
        return false;
    }
}

function Homebase_Validate_Retrieve_KO(homebaseVM, homebaseID, homebaseErrorLabel, btn) {
    homebaseVM.HomebaseCD(homebaseVM.HomebaseCD().toUpperCase());
    var Homebase = homebaseVM.HomebaseCD();
    if (!IsNullOrEmpty(Homebase)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/Homebase_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'BaseCD': Homebase }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var homebaseErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, homebaseVM);
                    homebaseID(homebaseVM.HomebaseID());
                    End_Loading(btn, true);
                }
                else {
                    homebaseErrorText = "Invalid Home Base Code";
                    homebaseVM.BaseDescription("");
                    homebaseVM.HomebaseID(0);
                    homebaseID(homebaseVM.HomebaseID());
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(homebaseErrorLabel)) {
                    if (!homebaseErrorLabel.match('^#'))
                        homebaseErrorLabel = '#' + homebaseErrorLabel;
                    $(homebaseErrorLabel).text(homebaseErrorText);
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
                End_Loading(btn, false);
            }
        });
    }
    else {
        if (!IsNullOrEmptyOrUndefined(homebaseErrorLabel)) {
            if (!homebaseErrorLabel.match('^#'))
                homebaseErrorLabel = '#' + homebaseErrorLabel;
            $(homebaseErrorLabel).text("");
        }
        homebaseVM.HomebaseCD("");
        homebaseVM.BaseDescription("");
        homebaseVM.HomebaseID(0);
        homebaseID(null);
        
        return false;
    }
}

function CrewCode_Validate_Retrieve(CrewTextBox, CrewLabel, CrewHidden, CrewCVLabel, releId, btn) {
    var CrewCode = $.trim(CrewTextBox());
    if (!IsNullOrEmpty(CrewCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/CrewCode_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'CrewCD': CrewCode }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = result.d.Result;
                if (jsonObj.flag == "0") {
                    End_Loading(btn, false);
                    CrewLabel("");
                    $(CrewCVLabel).html("Invalid Crew Code");
                      CrewHidden("0");
                      releId(null);
                }
                else {
                    End_Loading(btn, true);
                    CrewTextBox(htmlDecode(jsonObj.txtValue));
                    CrewLabel(htmlDecode(jsonObj.lblValue));
                    CrewHidden(htmlDecode(jsonObj.hdValue));
                    releId(htmlDecode(jsonObj.hdValue));
                    $(CrewCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        CrewLabel("");
        $(CrewCVLabel).html("");
        CrewHidden("0");
        releId(null);
    }
}

function Homebase_Validate_Retrieve(HomebaseTextbox, HomebaseLabel, HomebaseHidden, HomebaseCVLabel, hdOldHomebase, hdairportHomeBase, hdCalculateLeg, btn) {
    var Homebase = $.trim(HomebaseTextbox());
    if (!IsNullOrEmpty(Homebase)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/Homebase_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'BaseCD':Homebase}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0"  ) {
                    End_Loading(btn, false);
                    HomebaseLabel("");
                    $(HomebaseCVLabel).html("Homebase Code Does Not Exist");
                    HomebaseHidden(null);
                }
                else {
                    End_Loading(btn, true);
                    HomebaseTextbox(htmlDecode(jsonObj.txtValue));
                    HomebaseLabel(htmlDecode(jsonObj.lblValue));
                    HomebaseHidden(htmlDecode(jsonObj.hdValue));
                    hdairportHomeBase(jsonObj.Other.HomebaseAirportID);
                    setTooltipforAirport(HomebaseTextbox(), self.Preflight.PreflightMain.HomebaseTooltip, document.getElementById("tbHomeBase"));
                    var isOldHomebase = $(hdOldHomebase).val();
                    if (isOldHomebase != HomebaseHidden() && IsNullOrEmpty(isOldHomebase) == false)
                        $(hdCalculateLeg).val("True");
                    else if (IsNullOrEmpty(isOldHomebase) && jsonObj.Other.LegCnt != "0")
                        $(hdCalculateLeg).val("True");
                    $(HomebaseCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        HomebaseLabel("");
        $(HomebaseCVLabel).html("");
        HomebaseHidden(null);
    }
}

function Airport_Validate_Retrieve_KO(airportVM, airportID, airportErrorLabel, btn) {
    airportVM.IcaoID(airportVM.IcaoID().toUpperCase());
    var icao = airportVM.IcaoID();
    if (!IsNullOrEmpty(icao)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/Airport_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'ICAO': icao }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var airportErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, airportVM);
                    airportID(airportVM.AirportID());
                    airportVM.tooltipInfo(htmlDecode(airportVM.tooltipInfo()));
                    End_Loading(btn, true);
                }
                else {
                    airportErrorText = "ICAO Does Not Exist";
                    resetKOViewModel(airportVM,["IcaoID"]);
                    airportID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(airportErrorLabel)) {
                    if (!airportErrorLabel.match('^#'))
                        airportErrorLabel = '#' + airportErrorLabel;
                    $(airportErrorLabel).text(airportErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(airportVM);
        airportID("");
        End_Loading(btn, false);
        if (!airportErrorLabel.match('^#'))
            airportErrorLabel = '#' + airportErrorLabel;
        $(airportErrorLabel).text("");
    }
}

function Requestor_Validate_Retrieve(ReqTextBox, ReqLabel, ReqHidden, ReqCVLabel, ReqPhoneNo, reqId, DeptTextBox, DeptLabel, DeptHidden, DeptCVLabel, deptId, AuthTextBox, AuthLabel, AuthHidden, AuthCVLabel, authId, btn, reqPhonetb) {
    var Req = $.trim(ReqTextBox());
    if (!IsNullOrEmpty(Req)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Requestor_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'ReqCD':Req}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.Reqestor.flag == "0" ) {
                    End_Loading(btn, false);
                    ReqLabel("");
                    $(ReqCVLabel).html("Passenger/Requestor Code Does Not Exist");
                    if (reqId() == null || IsNullOrEmpty(reqId())) {
                        ReqHidden("0");
                        reqId(null);
                        if (ReqPhoneNo != null && ReqPhoneNo != '' && ReqPhoneNo != undefined) {
                            ReqPhoneNo("");
                        }
                        DeptTextBox("");
                        DeptLabel("");
                        DeptHidden("0");
                        deptId(null);
                        $(DeptCVLabel).val("");
                        AuthTextBox("");
                        AuthLabel("");
                        AuthHidden("0");
                        authId(null);
                    }
                    $(AuthCVLabel).val("");
                }
                else {
                    End_Loading(btn, true);
                    ReqTextBox(htmlDecode(jsonObj.Reqestor.txtValue));
                    ReqLabel(htmlDecode(jsonObj.Reqestor.lblValue));
                    ReqHidden(htmlDecode(jsonObj.Reqestor.hdValue));
                    reqId(htmlDecode(jsonObj.Reqestor.hdValue));
                    if (ReqPhoneNo != null && ReqPhoneNo != '' && ReqPhoneNo != undefined) {
                        $(reqPhonetb).removeAttr("readonly");
                        ReqPhoneNo(htmlDecode(jsonObj.ReqPhoneNumber));
                        $(reqPhonetb).attr("readonly", true);
                    }

                    $(ReqCVLabel).html("");

                    if (typeof (DeptTextBox) != undefined && jsonObj.Department.flag != null && jsonObj.Department.flag == "1") {
                        $(DeptCVLabel).val("");
                        DeptTextBox(htmlDecode(jsonObj.Department.txtValue));
                        DeptLabel(htmlDecode(jsonObj.Department.lblValue));
                        DeptHidden(htmlDecode(jsonObj.Department.hdValue));
                        deptId(htmlDecode(jsonObj.Department.hdValue));
                    }
                    else {
                        DeptTextBox("");
                        DeptLabel("");
                        DeptHidden("0");
                        $(DeptCVLabel).val("");
                        deptId(null);
                    }

                    if (typeof (AuthTextBox) != undefined && jsonObj.Authorization.flag != null && jsonObj.Department.flag == "1" && jsonObj.Authorization.flag == "1") {
                        $(AuthCVLabel).val("");
                        AuthTextBox(htmlDecode(jsonObj.Authorization.txtValue));
                        AuthLabel(htmlDecode(jsonObj.Authorization.lblValue));
                        AuthHidden(htmlDecode(jsonObj.Authorization.hdValue));
                        authId(htmlDecode(jsonObj.Authorization.hdValue));
                    }
                    else {
                        AuthTextBox("");
                        AuthLabel("");
                        AuthHidden("0");
                        $(AuthCVLabel).val("");
                        authId(null);
                    }
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        ReqLabel("");
        $(ReqCVLabel).html("");
        ReqHidden("0");
        reqId(null);
        if (ReqPhoneNo != null && ReqPhoneNo != '' && ReqPhoneNo != undefined) {
            ReqPhoneNo("");
        }
        DeptTextBox("");
        DeptLabel("");
        DeptHidden("0");
        deptId(null);
        $(DeptCVLabel).val("");
        AuthTextBox("");
        AuthLabel("");
        AuthHidden("0");
        authId(null);
        $(AuthCVLabel).val("");
        End_Loading(btn, false);
    }
}

function Department_Validate_Retrieve(DeptTextBox, DeptLabel, DeptHidden, DeptCVLabel, deptId, AuthTextBox, AuthLabel, AuthHidden, authId, btn) {
    var DeptCD = $.trim(DeptTextBox());
    if (!IsNullOrEmpty(DeptCD)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Department_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'DeptCD':DeptCD,'DeptId':0}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    DeptLabel("");
                    if (deptId() == null || IsNullOrEmpty(deptId())) {
                        AuthLabel("");
                        AuthTextBox("");
                        AuthHidden("0");
                        authId(null);
                        DeptHidden("0");
                        deptId(null);
                    }

                    $(DeptCVLabel).html("Invalid Department Code");
                    End_Loading(btn, false);
                }
                else {
                    End_Loading(btn, true);
                    DeptTextBox(htmlDecode(jsonObj.txtValue));
                    DeptLabel(htmlDecode(jsonObj.lblValue));
                    DeptHidden(htmlDecode(jsonObj.hdValue));
                    deptId(htmlDecode(jsonObj.hdValue));
                    AuthLabel("");
                    AuthTextBox("");
                    AuthHidden("0");
                    authId(null);
                    $(DeptCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        DeptLabel("");
        $(DeptCVLabel).html("");
        DeptHidden("0");
        deptId(null);
        AuthLabel("");
        AuthTextBox("");
        AuthHidden("0");
        authId(null);
        End_Loading(btn, false);
    }
}

function Department_Validate_Retrieve_KO(departmentVM, departmentID, departmentErrorLabel, btn, DepartmentAuthorizationVM,authorizationLabel)
{
    departmentVM.DepartmentCD(departmentVM.DepartmentCD().toUpperCase());
    var departCD = departmentVM.DepartmentCD();
    if (!IsNullOrEmpty(departCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/Department_Validate_Retrieve_KO",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'departmentCd': departCD, 'departmentId': departmentID() }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var ErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, departmentVM);
                    departmentID(departmentVM.DepartmentID());
                    End_Loading(btn, true);
                }
                else {
                    ErrorText = "Invalid Department Code.";
                    resetKOViewModel(departmentVM, ["DepartmentCD", "DepartmentID"]);
                    End_Loading(btn, false);
                }
                resetKOViewModel(DepartmentAuthorizationVM, ["AuthorizationID"]);
                
                if (!IsNullOrEmptyOrUndefined(authorizationLabel)) {
                    if (!authorizationLabel.match('^#'))
                        authorizationLabel = '#' + authorizationLabel;
                    $(authorizationLabel).text("");
                }

                if (!IsNullOrEmptyOrUndefined(departmentErrorLabel)) {
                    if (!departmentErrorLabel.match('^#'))
                        departmentErrorLabel = '#' + departmentErrorLabel;
                    $(departmentErrorLabel).text(ErrorText);
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);                
                reportValidationError(jqXHR, textStatus, errorThrown);
                return true;
            }
        });
    }
    else {
        resetKOViewModel(departmentVM, ["DepartmentID"]);
        resetKOViewModel(DepartmentAuthorizationVM, ["AuthorizationID"]);
        $(departmentErrorLabel).text("");
        
    }
}

function Authorization_Validate_Retrieve(AuthTextBox, AuthLabel, AuthHidden, AuthCVLabel, DepartHidden, authId, btn) {
    var authCD = $.trim(AuthTextBox());
    var departId = $.trim(DepartHidden().toString());
    if (authCD != null && authCD != '' && departId != 0 && departId != null && departId != '') {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Authorization_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'AuthCD': authCD, 'AuthID': 0, 'DeptID': departId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0") {
                    End_Loading(btn, false);
                    AuthLabel("");
                    $(AuthCVLabel).html("Invalid Authorization Code");
                    if (authId() == null || IsNullOrEmpty(authId())) {
                        AuthHidden("0");
                        authId(null);
                    }
                }
                else {
                    End_Loading(btn, true);
                    AuthTextBox(htmlDecode(jsonObj.txtValue));
                    AuthLabel(htmlDecode(jsonObj.lblValue));
                    AuthHidden(htmlDecode(jsonObj.hdValue));
                    authId(htmlDecode(jsonObj.hdValue));
                    $(AuthCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        AuthTextBox("");
        AuthLabel("");
        $(AuthCVLabel).html("");
        AuthHidden("0");
        authId(null);
        if (departId == 0 || departId == null || departId == '') {
            jAlert("Please enter the Department Code before entering the Authorization Code.","Preflight Main");
        }
    }
}

function Authorization_Validate_Retrieve_OK(authorizationVM, authorizationId, authorizationErrorLabel, btn,departmentID,departmentCD)
{
    authorizationVM.AuthorizationCD(authorizationVM.AuthorizationCD().toUpperCase());
    var authorizationCD = authorizationVM.AuthorizationCD();
    if (!IsNullOrEmpty(authorizationCD)) {
        Start_Loading(btn);
        $.ajax({
            async: false,
            url: "/Views/Transactions/CommonValidator.aspx/Authorization_Validate_Retrieve_KO",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'authorizationCd': authorizationCD,'authorizationId': authorizationId() ,'departmentId': departmentID() }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var ErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, authorizationVM);
                    authorizationId(authorizationVM.AuthorizationID())
                    End_Loading(btn, true);
                }
                else {
                    ErrorText = "Invalid Authorization Code.";
                    resetKOViewModel(authorizationVM, ["AuthorizationID", "AuthorizationCD"]);
                    authorizationId(null)
                    End_Loading(btn, false);
                }
                
                if (!IsNullOrEmptyOrUndefined(authorizationErrorLabel)) {
                    if (!authorizationErrorLabel.match('^#'))
                        authorizationErrorLabel = '#' + authorizationErrorLabel;
                    $(authorizationErrorLabel).text(ErrorText);
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
                return true;
            }
        });
    }
    else {
        resetKOViewModel(authorizationVM, ["AuthorizationID"]);
        $(authorizationErrorLabel).text("");        
    }
}

function Account_Validate_Retrieve(AccTextBox, AccLabel, AccHidden, AccCVLabel, accountId, btn) {
 
    var AccountCode = $.trim(AccTextBox());
    if (!IsNullOrEmpty(AccountCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Account_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'AccountCD':AccountCode}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    End_Loading(btn, false);
                    AccLabel("");
                    $(AccCVLabel).html("Account No. Does Not Exist");
                    if (accountId() == null || IsNullOrEmpty(accountId())) {
                        AccHidden("0");
                        accountId(null);
                    }
                }
                else {
                    End_Loading(btn, true);
                    AccTextBox(htmlDecode(jsonObj.txtValue));
                    AccLabel(htmlDecode(jsonObj.lblValue));
                    AccHidden(htmlDecode(jsonObj.hdValue));
                    accountId(htmlDecode(jsonObj.hdValue));
                    $(AccCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        AccLabel("");
        $(AccCVLabel).html("");
        AccHidden("0");
        accountId(null);
    }
}

function Account_Validate_Retrieve_KO(accountVM, accountId, accountErrorLabel, btn) {
    accountVM.AccountNum(accountVM.AccountNum().toUpperCase());
    var accountNum = accountVM.AccountNum();
    if (!IsNullOrEmpty(accountNum)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Account_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ AccountNumber: accountNum }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var accountErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, accountVM);
                    accountId(accountVM.AccountID());
                    End_Loading(btn, true);
                }
                else {
                    accountErrorText = "Invalid Account Number";
                    resetKOViewModel(accountVM, ["AccountID", "AccountNum"]);
                    accountId("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(accountErrorLabel)) {
                    if (!accountErrorLabel.match('^#'))
                        accountErrorLabel = '#' + accountErrorLabel;
                    $(accountErrorLabel).text(accountErrorText);
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        if (!accountErrorLabel.match('^#'))
            accountErrorLabel = '#' + accountErrorLabel;
        $(accountErrorLabel).text("");
        resetKOViewModel(accountVM,["AccountID"]);
        accountId("");        
    }
}
function CQCustomer_Validate_Retrieve(tbCustomer, lbCustomer, hdnCustomer, cvlbCustomer, CQCustId, btn) {
    var customerCD = $.trim(tbCustomer());
    Start_Loading(btn);
    if (!IsNullOrEmpty(customerCD)) {
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/CQCustomer_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({'CQCustomerCD':customerCD}),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    End_Loading(btn, false);
                    lbCustomer("");
                    $(cvlbCustomer).html("Invalid Charter Customer Code");
                    if (CQCustId() == null || IsNullOrEmpty(CQCustId())) {
                        hdnCustomer("0");
                        CQCustId(null);
                    }
                }
                else {
                    End_Loading(btn, true);
                    tbCustomer(htmlDecode(jsonObj.txtValue));
                    lbCustomer(htmlDecode(jsonObj.lblValue));
                    hdnCustomer(htmlDecode(jsonObj.hdValue));
                    CQCustId(htmlDecode(jsonObj.hdValue));
                    $(cvlbCustomer).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        lbCustomer("");
        $(cvlbCustomer).html("");
        hdnCustomer("0");
        CQCustId(null);
    }
}

function Dispatcher_Validate_Retrieve(DispatchTextBox, DispatchLabel, DispatchPhoneNo, DispatchCVLabel, disUsername, btn) {
    var Dispatch = $.trim(DispatchTextBox());
    if (!IsNullOrEmpty(Dispatch)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/Dispatcher_Validate_Retrieve",
            type: "POST",
            dataType: "json",
            data: "{DispatcherCD:'" + Dispatch + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    End_Loading(btn, false);
                    DispatchLabel("");
                    $(DispatchCVLabel).html("Dispatcher Code Does Not Exist");
                    DispatchPhoneNo("");
                    disUsername("");
                }
                else {
                    End_Loading(btn, true);
                    DispatchTextBox(htmlDecode(jsonObj.txtValue));
                    DispatchLabel(htmlDecode(jsonObj.lblValue));
                    DispatchPhoneNo(htmlDecode(jsonObj.hdValue));
                    disUsername(htmlDecode(jsonObj.txtValue));
                    $(DispatchCVLabel).html("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        DispatchLabel("");
        disUsername("");
        $(DispatchCVLabel).html("");
        DispatchPhoneNo("");
    }
}

function HotelValidation(AirportID, HotelCode, hdnHotelchanged, lbcvHotelCode, tbHotelName, hdnHotelID, tbHotelRate, tbHotelPhone, tbHotelFax, tbAddr1, tbAddr2, tbAddr3, tbCity, tbState, tbMetro, hdnMetroID, tbCtry, hdnCtryID, tbPost, btn) {
    $(lbcvHotelCode).val("");
    if (!IsNullOrEmpty(HotelCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/HotelValidation",
            type: "POST",
            dataType: "json",
            data: "{HotelCD:'" + $.trim(HotelCode()) + "',AirportID:'" + AirportID() + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = result.d.Result;
                var returnResult = verifyReturnedResult(result);
                // alert(jsonObj.flag);
                var data = jsonObj;
                if (returnResult == true && result.d.Success == true && data && data.flag == "1") {
                    ResetHotelControls();
                    $(hdnHotelchanged).val("true");
                    $(lbcvHotelCode).val("");
                    tbHotelName(htmlDecode(data.lblValue.toUpperCase()));
                    hdnHotelID(htmlDecode(data.hdValue.toString()));

                    HotelCode(htmlDecode(data.txtValue.toString()));
                    tbHotelRate(htmlDecode(data.Other.NegociatedRate));

                    tbHotelPhone(htmlDecode(data.Other.PhoneNum));
                    tbHotelFax(htmlDecode(data.Other.FaxNum));
                    tbAddr1(htmlDecode(data.Other.Addr1));
                    tbAddr2(htmlDecode(data.Other.Addr2));
                    tbAddr3(htmlDecode(data.Other.Addr3));
                    tbCity(htmlDecode(data.Other.CityName));
                    tbState(htmlDecode(data.Other.StateName));
                    if (data.Other.MetroCD != null) {
                        tbMetro(htmlDecode(data.Other.MetroCD));
                        hdnMetroID(htmlDecode(data.Other.MetroID));
                    }
                    if (data.Other.CountryCD != null) {
                        tbCtry(htmlDecode(data.Other.CountryCD));
                        hdnCtryID(htmlDecode(data.Other.CountryID));
                    }
                    tbPost(htmlDecode(data.Other.PostalZipCD));
                    End_Loading(btn, true);
                } else {
                    $(lbcvHotelCode).html("Hotel Code Does Not Exist");
                    var hotelcode = HotelCode();
                    ResetHotelControls();
                    HotelCode(hotelcode);
                    End_Loading(btn, false);
                }
                return jsonObj;
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        ResetHotelControls();
        End_Loading(btn, false);
        return null;

    }
}

function CountryValidation(CountryTextBox, CountryHidden, CountryCVLabel, btn) {
    var CountryCode = CountryTextBox();
    if (!IsNullOrEmpty(CountryCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/CountryValidation",
            type: "POST",
            dataType: "json",
            data: "{CountryCD:'" + CountryCode + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(CountryCVLabel).html("Country No. Does Not Exist");
                    CountryHidden("");
                    End_Loading(btn, false);
                }
                else {
                    CountryTextBox(htmlDecode(jsonObj.txtValue));
                    CountryHidden(htmlDecode(jsonObj.hdValue));
                    $(CountryCVLabel).html("");
                    End_Loading(btn, true);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        $(CountryCVLabel).html("");
        CountryHidden("");
        End_Loading(btn, false);
    }
}

function MetroValidation(MetroTextBox, MetroHidden, MetroCVLabel, btn) {
    var MetroCode = MetroTextBox();
    if (!IsNullOrEmpty(MetroCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/MetroValidation",
            type: "POST",
            dataType: "json",
            data: "{MetroCD:'" + MetroCode + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(MetroCVLabel).html("Metro No. Does Not Exist");
                    MetroHidden("");
                    End_Loading(btn, false);
                }
                else {
                    MetroTextBox(htmlDecode(jsonObj.txtValue));
                    MetroHidden(htmlDecode(jsonObj.hdValue));
                    $(MetroCVLabel).html("");
                    End_Loading(btn, true);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        $(MetroCVLabel).html("");
        MetroHidden("");
        End_Loading(btn, false);
    }
}

function TransportValidation(TransCode, AirportID, hdnTransAirID, hdnTransportID, tbTransCode, tbTransName, tbTransPhone, tbTransFax, tbTransRate, lbTransCode, lbcvTransCode, btnTrans, ErrorMsg) {

    if (!IsNullOrEmpty(TransCode)) {
        Start_Loading(btnTrans);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/TransportValidation",
            type: "POST",
            dataType: "json",
            data: "{TransportCD:'" + TransCode + "',AirportID:'" + AirportID + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(lbcvTransCode).html(ErrorMsg);
                    tbTransName("");
                    $(lbTransCode).html("");
                    hdnTransportID("");
                    tbTransPhone("");
                    tbTransFax("");
                    tbTransRate("");
                    End_Loading(btnTrans, false);
                }
                else {
                    tbTransCode(htmlDecode(jsonObj.txtValue));
                    tbTransName(htmlDecode(jsonObj.lblValue));
                    $(lbTransCode).html(htmlDecode(jsonObj.lblValue));
                    hdnTransportID(htmlDecode(jsonObj.hdValue));
                    tbTransPhone(htmlDecode(jsonObj.Other.PhoneNum));
                    tbTransFax(htmlDecode(jsonObj.Other.FaxNum));
                    tbTransRate(htmlDecode(jsonObj.Other.NegotiatedRate));
                    $(lbcvTransCode).html("");
                    End_Loading(btnTrans, true);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btnTrans, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }

        });
    }
    else {
        $(lbTransCode).val("");
        tbTransName("");
        tbTransPhone("");
        tbTransFax("");
        tbTransRate("");
        $(lbcvTransCode).html("");
        End_Loading(btnTrans, false);
    }
}

function FBOValidation(airportid, FBOCode, tbFBOCode, hdFBOID, lbcvFBOCode, lblName, lblPhone1, lblPhone2, lblFax, lblCity, lblState, lblZipPostalCode, lblUnicom, lblArinc, lblAddr1, lblAddr2, lblAddr3, lblEmail, tbFBOComments, tbFBOConfirmation, ErrorMsg, btnFBOCode, hdnLeg, alertMsg, isDeparture, Check, PreflightFBOId, State) {

    if (!IsNullOrEmpty(FBOCode)) {
        Start_Loading(btnFBOCode);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/FBOValidation",
            type: "POST",
            dataType: "json",
            data: "{FBOCD:'" + FBOCode + "',hdnLeg:'" + hdnLeg + "',AirportID:'" + airportid() + "',isDeparture:'" + isDeparture + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(lbcvFBOCode).html(ErrorMsg);
                    hdFBOID(0);
                    lblName(null);
                    lblPhone1(null);
                    lblPhone2(null);
                    lblFax(null);
                    lblCity(null);
                    lblState(null);
                    lblZipPostalCode(null);
                    lblAddr1(null);
                    lblAddr2(null);
                    lblAddr3(null);
                    lblEmail(null);
                    lblUnicom(null);
                    lblArinc(null);

                    tbFBOComments("");
                    tbFBOConfirmation("");
                    End_Loading(btnFBOCode, false);
                }
                else {
                    $(lbcvFBOCode).html("");

                    hdFBOID(htmlDecode(jsonObj.hdValue));
                    lblName(htmlDecode(jsonObj.lblValue));
                    lblPhone1(htmlDecode(jsonObj.Other.PhoneNUM1));
                    lblPhone2(htmlDecode(jsonObj.Other.PhoneNUM2));
                    lblFax(htmlDecode(jsonObj.Other.FaxNum));
                    lblCity(htmlDecode(jsonObj.Other.CityNum));
                    lblState(htmlDecode(jsonObj.Other.StateNum));
                    lblZipPostalCode(htmlDecode(jsonObj.Other.PostalZipCD));
                    lblAddr1(htmlDecode(jsonObj.Other.Addr1));
                    lblAddr2(htmlDecode(jsonObj.Other.Addr2));
                    lblAddr3(htmlDecode(jsonObj.Other.Addr3));
                    lblEmail(htmlDecode(jsonObj.Other.EmailAddress));
                    lblUnicom(htmlDecode($.trim(jsonObj.Other.UNICOM)));
                    lblArinc(htmlDecode($.trim(jsonObj.Other.ARINC)));
                    tbFBOComments("");
                    tbFBOConfirmation("");
                    if (PreflightFBOId() != null && PreflightFBOId() > 0) {
                        State(2);
                    } else {
                        State(1);
                    }
                    tbFBOCode(htmlDecode(jsonObj.txtValue));

                    if (Check) {
                        if (jsonObj.Other.Confirm == "true") {
                            if (isDeparture) {
                                jConfirm(alertMsg, "Confirmation!", confirmPreviousFBOCallBackFn);
                            }
                            else {
                                jConfirm(alertMsg, "Confirmation!", confirmNextFBOCallBackFn);
                            }
                        }
                    }
                    tbFBOComments("");
                    tbFBOConfirmation("");
                    End_Loading(btnFBOCode, true);
                }

            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btnFBOCode, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        hdFBOID("");
        $(lbcvFBOCode).html("");
        lblName("");
        lblPhone1("");
        lblPhone2("");
        lblFax("");
        lblCity("");
        lblState("");
        lblZipPostalCode("");
        lblAddr1("");
        lblAddr2("");
        lblAddr3("");
        lblEmail("");
        lblUnicom("");
        lblArinc("");


        tbFBOComments("");
        tbFBOConfirmation("");
        End_Loading(btnFBOCode, false);
    }
}

function FBO_Validate_Retrieve_KO(fboVM, airportId, fboID, fboErrorLabel, btn) {
    fboVM.FBOCD(fboVM.FBOCD().toUpperCase());
    var fboCD = fboVM.FBOCD();
    if (!IsNullOrEmpty(fboCD) && !IsNullOrEmpty(airportId)) {
        var param = { FBO: fboCD,
            AirportId : airportId()
        };
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/FBO_Validate_Retrieve_VM",
            type: "POST",
            dataType: "json",
            data: JSON.stringify(param),
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                var fboErrorText = "";
                if (returnResult == true && response.d.Success == true) {
                    ko.mapping.fromJS(response.d.Result, {}, fboVM);
                    fboID(fboVM.FBOID());
                    End_Loading(btn, true);
                }
                else {
                    fboErrorText = "FBO Code Does Not Exist";
                    resetKOViewModel(fboVM, ["FBOCD"]);
                    fboID("");
                    End_Loading(btn, false);
                }
                if (!IsNullOrEmptyOrUndefined(fboErrorLabel)) {
                    if (!fboErrorLabel.match('^#'))
                        fboErrorLabel = '#' + fboErrorLabel;
                    $(fboErrorLabel).text(fboErrorText);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                return true;
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        resetKOViewModel(fboVM);
        fboID("");
        End_Loading(btn, false);
        if (!fboErrorLabel.match('^#'))
            fboErrorLabel = '#' + fboErrorLabel;
        $(fboErrorLabel).text("");
    }
}

function CaterValidation(CaterCode, AirportID, hdCaterID, tbCaterCode, tbCaterName, tbCaterPhone, tbCaterFax, tbCaterCost, tbContactName, lbcvCaterCode, ErrorMsg, btn, PreflightCateringId, State) {
    if (!IsNullOrEmpty(CaterCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/CommonValidator.aspx/CateringValidation",
            type: "POST",
            dataType: "json",
            data: "{CateringCD:'" + CaterCode + "',AirportID:'" + AirportID() + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0") {
                    $(lbcvCaterCode).html(ErrorMsg);
                    hdCaterID("");
                    tbCaterName("");
                    tbCaterPhone("");
                    tbCaterFax("");
                    tbCaterCost("");
                    tbContactName("");
                    End_Loading(btn, false);
                }
                else {
                    $(lbcvCaterCode).html("");
                    hdCaterID(htmlDecode(jsonObj.hdValue));
                    tbCaterCode(htmlDecode(jsonObj.txtValue));
                    tbCaterName(htmlDecode(jsonObj.lblValue));
                    tbCaterPhone(htmlDecode(jsonObj.Other.PhoneNum));
                    tbCaterFax(htmlDecode(jsonObj.Other.FaxNum));
                    tbCaterCost(htmlDecode(jsonObj.Other.NegotiatedRate));
                    tbContactName(htmlDecode(jsonObj.Other.ContactName));
                    if (PreflightCateringId() != null && PreflightCateringId() > 0) {
                        State(2);
                    } else {
                        State(1);
                    }
                    End_Loading(btn, true);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }

        });
    }
    else {
        $(lbcvCaterCode).html("");        
        hdCaterID("");
        tbCaterName("");
        tbCaterPhone("");
        tbCaterFax("");
        tbCaterCost("");
        tbContactName("");
        End_Loading(btn, false);
    }
}

function CrewGroupValidation(tbCrewGroup, label, hdCrewGroup, btn) {

    if (typeof (tbCrewGroup) != undefined && tbCrewGroup != '' && tbCrewGroup != null) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/CrewGroupValidation",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'CrewGroupCD': tbCrewGroup }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.Result == "true") {
                    End_Loading(btn, true);
                    $(label).text("");
                    $(hdCrewGroup).val(htmlDecode(jsonObj.CrewGroupID));
                    Filter_CheckedChanged();
                }
                else {
                    End_Loading(btn, false);
                    $(label).text("Invalid Crew Group");
                    $(hdCrewGroup).val("");
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading(btn, false);
        $(label).html("");
        $(hdCrewGroup).val("");
        Filter_CheckedChanged();
    }
}

function CrewCodeValidation(CrewCode, CrewID, _airportId, hdCrewGroup, btn, AddCrews) {

    if (typeof (CrewCode) != undefined && CrewCode != '' && CrewCode != null) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/CrewCodeValidation",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'CrewCD': CrewCode, 'CrewID': CrewID, 'hdnCrewGroupID': hdCrewGroup, '_airportId': _airportId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.Results == "true") {
                    End_Loading_Toppage_Search(btn, true);
                    AddCrews(jsonObj.Crews);
                }
                else {
                    End_Loading_Toppage_Search(btn, false);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading_Toppage_Search(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading_Toppage_Search(btn, false);
    }
}

function PaxCodeValidation(PaxCode, btn, AddPaxs) {

    if (typeof (PaxCode) != undefined && PaxCode != '' && PaxCode != null) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            url: "/Views/Transactions/PreFlightValidator.aspx/PaxCodeValidation",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'PaxCD': PaxCode }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (returnResult == true && result.d.Success == true && jsonObj.Results == "true") {
                    End_Loading_Toppage_Search(btn, true);
                    var paxCodes = PaxCode.split(',');
                    AddPaxs(paxCodes);
                }
                else {
                    End_Loading_Toppage_Search(btn, false);
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading_Toppage_Search(btn, false);
                reportValidationError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    else {
        End_Loading_Toppage_Search(btn, false);
    }
}

function setAlertforAirport(AirportData, textbox, airporttype) {
    if (AirportData != null) {
        if (airporttype != undefined) {
            (airporttype == "DEPT") ? setDepAirportDetail(AirportData) : setArrivalAirportDetail(AirportData);
        }
        var tooltipString = "";
        if (AirportData.Alerts() != '' && AirportData.Alerts() != null) {
            $(textbox).css("color", "red");
            tooltipString = "Alerts : \n" + AirportData.Alerts();
        } else {
            $(textbox).css("color", "black");
        }
        
        if (AirportData.GeneralNotes() != '' && AirportData.GeneralNotes() != null) {
            tooltipString += tooltipString == '' ? "Notes : \n" + AirportData.GeneralNotes() : "\n\nNotes : \n" + AirportData.GeneralNotes();
        }
        $(textbox).attr("title", tooltipString);
    }
}

function clearAirportElements(textbox, airporttype) {
    if (airporttype != undefined) {
        (airporttype == "DEPT") ? setDepAirportDetail(null) : setArrivalAirportDetail(null);
    }
    $(textbox).css("color", "black");
    $(textbox).attr("title", '');
}
