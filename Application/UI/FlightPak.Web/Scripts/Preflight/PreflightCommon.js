﻿var alertMessageHotelSave = "Please save or cancel hotel information.";
var title = "Preflight";
function openWinShared(radWin)
{
    var url = '';
    if (radWin == "rdCopyTrip") {
        url = '/Views/Transactions/Preflight/CopyTrip.aspx';
    }
    if (radWin == "rdMoveTrip") {
        url = '/Views/Transactions/Preflight/MoveTrip.aspx';
    }
    if (radWin == "rdHistory") {
        if(self.TripID()>0) {
            url="/Views/Transactions/History.aspx?FromPage="+"Preflight";
        } else {
            jAlert("Please select a trip", title+' - Trip History');
            return;
        }
    }

    if (radWin == "rdSIFL") {
        url = '/Views/Transactions/Preflight/PreflightSIFL.aspx';
    }
    if (radWin == "rdPreflightEMAIL") {
        url = '/Views/Transactions/Preflight/EmailTrip.aspx';
    }
   
    if (url != "")
        var oWnd = radopen(url, radWin);

}

function reportPreflightError(jqXHR, textStatus, errorThrown)
{
    if (jqXHR.status && jqXHR.status == 401) {
        window.location.href = "/Account/Login.aspx";
    }
    else if (jqXHR.status && jqXHR.status == 400) {
        jAlert(jqXHR.responseText, title);
    }
}

function CreateJqGridLoader(grid) {
    if ($(grid).find(".jqgrid-loader").length <= 0) {
        $(grid).append($(".divloader")[0].outerHTML);
        $(grid).append("<div class='jqgrid-loader-wrapper' style='position: absolute;height: " + $(grid).height() + "px;background: rgba(255, 255, 255, 0.498039);width: 100%;top: 0;'></div>");
        $(grid).css("position:relative");
        $(grid).find(".divloader").removeClass("divloader").addClass("jqgrid-loader");
    }
}

function ShowJqGridLoader(grid) {
    $(grid).find(".jqgrid-loader-wrapper").css("display", "block");
    $(grid).find(".jqgrid-loader").css("display", "block");
}

function HideJqGridLoader(grid) {
    $(grid).find(".jqgrid-loader-wrapper").css("display", "none");
    $(grid).find(".jqgrid-loader").css("display", "none");
}

function MaintainModelStatusBeforePageLeave(ctrl,calledFrom) {
    // Find which is current page
    var tabName = getQuerystring("seltab", "PreFlight");
    if (tabName == "PreFlight") {
        if (self.EditMode() == true) {
            self.Preflight.PreflightMain.IsViewModelEdited = true;
            SavePreflightMain();
        }
        $("#tbPreflight").addClass("tabSelected");
        return true;
    }
    else if (tabName == "Legs") {
        if (self.EditMode() == true) {
            SaveCurrentLegData();
        }
        $("#tbLegs").addClass("tabSelected");
        return true;
    }
    else if (tabName == "Crew") {
        if (self.EditMode() == true) {
            if (self.EnableHotel() == true) {
                jAlert(alertMessageHotelSave, "Preflight");
                return false;
            }
            var isCrewAssigned = CheckCrewDutyAssigned();
            if (isCrewAssigned && self.IsCrewAssignedConfirm()==false) {
                ValidateCrewAssignment(function () {
                    if (calledFrom == "save") {
                        ctrl();
                    }
                    else if (calledFrom == "next") {
                        window.location = ctrl;
                    }
                    else if (calledFrom == "redirect")
                        window.location = $(ctrl).attr("href");

                    });
                    return false;
            }
            else
                return true;

            
        }
        else
            return true;
        
    }
    else if (tabName == "Pax") {
        if (self.EditMode() == true) {
            if (self.EnablePaxHotelMode() == true) {
                jAlert(alertMessageHotelSave, "Preflight");
                return false;            
            }
            if (self.IsPaxInfoWarningConfirm() == false) {
                warningInPaxInfoSelection(function () {
                    if (calledFrom == "save") {
                        ctrl();
                    }
                    else if (calledFrom == "next") {
                        window.location = ctrl;
                    }
                    else if (calledFrom == "redirect")
                        window.location = $(ctrl).attr("href");
                   
                });
                return false;
            } else {
                return true;
            }            
        }
        return true;
    }
    else if (tabName == "Logistics") {
        if (self.EditMode()) {
            SaveLogisticsViewModel(false);
            return true;
        } else {
            return true;
        }
        return true;
    }
    else if (tabName == "Reports") {
        return true;
    }
    return false;
}

function InitializeDefaultSelectedTabFeature() {
    ko.bindingHandlers.DefaultTabSelected = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called when the binding is first applied to an element
            // Set up any initial state, event handlers, etc. here
            if (viewModel.LegNUM() == 1) {
                $(element).click();
            }
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called once when the binding is first applied to an element,
            // and again whenever any observables/computeds that are accessed change
            // Update the DOM element based on the supplied values here.
        }
    };    
}
function InitializeDateFormatExtender() {

    ko.extenders.dateFormat = function (target, format) {
        //create a writable computed observable to intercept writes to our observable
        
        var result = ko.pureComputed({
            read: target,  //always return the original observables value
            write: function (newValue) {
                if (newValue != null && newValue.toString().toLowerCase() == "invalid date") {
                    newValue = null;
                }
                var valueToWrite = newValue;
                if (newValue != null) {
                    if (newValue.toString().indexOf("/Date") == -1) {
                        valueToWrite = moment(newValue, 'YYYY-MM-DD').format('YYYY-MM-DD');
                    }
                    else {
                        valueToWrite = moment(newValue).format('YYYY-MM-DD');
                    }
                    if (valueToWrite.toString().toLowerCase() == "invalid date") {
                        valueToWrite = moment(newValue, self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format('YYYY-MM-DD');
                    }
                }
                target(valueToWrite);
            }
        }).extend({ notify: 'always' });
        //initialize with current value to make sure it is rounded appropriately
        result(target());
        //return the new computed observable
        return result;
    };
}

function InitializeTransportRateFormatterBindingHandler() {
    ko.bindingHandlers.transportRateElement = {
        init: function (element, valueAccessor) {
            var rateValue = valueAccessor();
            if (isNaN(rateValue())) {
                rateValue("0");
            }
            $(element).focus(function () {
                if (self.EditMode() && isNaN(rateValue()) == false) {
                    $(element).val(rateValue());
                }
                else {
                    rateValue(parseFloat($(element).val()).toFixed(2));
                    if (isNaN(rateValue())) {
                        rateValue("0");
                    }
                    $(element).val(formatRateValue(rateValue()));
                }
            });
            $(element).blur(function () {
                rateValue(parseFloat($(element).val()).toFixed(2));
                if (isNaN(rateValue())) {
                    rateValue("0");
                }
                if (parseFloat(rateValue()) >= 1000) {
                    jAlert("Invalid format, Required format is ###.##", title)
                    rateValue("0");
                    $(element).val(formatRateValue(rateValue()));
                } else {
                    $(element).val(formatRateValue(rateValue()));
                }
            });
        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            if (isNaN(value())) {
                value("0");
            }
            if (isNaN(value()) == false) {
                $(element).focus();
                $(element).blur();
            }
        }
    };
}

(function (jQuery) {
    jQuery.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

function setAlertTextToYesNo() {
    $.alerts.okButton = "Yes";
    $.alerts.cancelButton = "No";
}
function setAlertTextToOkCancel() {
    $.alerts.okButton = "OK";
    $.alerts.cancelButton = "Cancel";
}