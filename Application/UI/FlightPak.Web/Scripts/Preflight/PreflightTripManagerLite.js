﻿
var isodateformat = 'YYYY-MM-DD';
var lastRevisionNo = null;
var self = this;
var ShortLegSummary = "#SortSummaryDetail";
var DetailedLegSummary = "#DetailedLegSummary";
var titleConfirm = "Confirmation!";
var PreflightMainPropertyNamesList = "TripStatus,FlightNUM,ClientRequestDT,TripDescription,DispatchNUM,Notes,TripSheetNotes,CancelDescription,IsPrivate,TailNum,FleetID";
var flagIsInitialLoad = false;
var isRequestComeFromCalendar;
var PreflightViewModel = function () {

    if ($.cookie('Trip') == "Updated")
    {
        ShowSuccessMessage();
        $.removeCookie('Trip');
    }
    self.EditMode = ko.observable(false);
    self.TripSearch = ko.observable();
    self.TripID = ko.observable();
    self.Preflight ={};
    self.UserPrincipal = {};
    flagIsInitialLoad = true;
    isRequestComeFromCalendar = getQuerystring("IsCalendar", 0);
    GetACurrentTrip();

    self.EditMode.subscribe(function (newValue) {
        if (newValue == false) {
            $("#tbType").attr("disabled", "disabled");
            $("#btnType").addClass("browse-button-disabled").removeClass("browse-button");
            $("#btnType").attr("disabled", "disabled");
        }
    });

    self.TripSearch.subscribe(function (newValue) {
        var TripNum = newValue;
        if (!fnVerifyInt64MaxLimit(newValue)) return false;
        if(self.EditMode()==false&&!IsNullOrEmpty(TripNum)) {
            RequestStart();
            $.ajax({
                async:true,
                url:"/Views/Transactions/Preflight/PreflightTripManager.aspx/TripSearch",
                type:"POST",
                dataType:"json",
                data:JSON.stringify({'TripNum':TripNum}),
                contentType:"application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj.flag == true) {
                        var Trip = jsonObj.Trip;
                        self.UserPrincipal = jsonObj.Principal;
                        self.TripID(Trip.TripId);
                        ko.mapping.fromJS(Trip, self.Preflight);
                        $("[id$='lbTripSearchMessage']").text("");
                        FSSDateParsing();
                        collapseItem(false);
                        $("#preflightTripViewerLink").attr("href", "~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PreTSTripSheetMain.xml&tripnum=" + ConvertNullToEmptyString(self.Preflight.PreflightMain.TripNUM()) + "&tripid=" + ConvertNullToEmptyString(self.Preflight.TripId()))
                        refreshTrip();
                    } else {
                        $("[id$='lbTripSearchMessage']").text("Trip No Does Not Exist");
                    }
                },
                error:function(jqXHR,textStatus,errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR,textStatus,errorThrown);
                },
                complete:function() { ResponseEnd(); }
            });
        } else {
            $("[id$='lbTripSearchMessage']").text("");
            if (!IsNullOrEmpty(TripNum)) {
                jAlert("Trip Unsaved, please Save or Cancel Trip", "Preflight");
            }
        }

    });
    self.btNewTrip_Click = function (element, event) {
        RequestStart();
        $("#btnNewTrip").prop('disabled', "disabled");
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetAFreshPreflightTrip",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    window.location="/views/Transactions/Preflight/PreflightMain.aspx?seltab=PreFlight";
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#btnNewTrip").prop('disabled', "");
                ResponseEnd();
                reportPreflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function() { ResponseEnd(); }
        });
    };
    self.btnEdit_Click = function (element, event) {
        $("#preflightTripViewerLink").attr("href", "javascript:alertToSaveTripBfrPreview()");
        $('#tdSuccessMessage').fadeOut();


        if (self.TripID() <= 0) {
            PreflightEditbuttonClick();
        } else {
            var params = "method=IsTripUpdated&apiType=fss&tripId=" + self.TripID() + "&lastUpdateDatetime=" + self.Preflight.PreflightMain.LastUpdTSStr();
            RequestStart();
            $.ajax({
                async: true,
                url: "/Views/Utilities/GetApi.aspx?" + params,
                type: "GET",
                dataType: "json",
                data: {},
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    ResponseEnd();
                    verifyReturnedResultForJqgrid(result);
                    if (result == false)
                        PreflightEditbuttonClick();
                    else {
                        jAlert("This trip has been updated by another user. Please reload the trip.", title);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#btnEditTrip,#btnEditFlightLog").prop('disabled', false);
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }


    };
    self.btnCancel_Click = function () {
        setAlertTextToYesNo();
        jConfirm("Are you sure want to Cancel?", "Confirmation!", confirmTripCancelCallBackFn);
        setAlertTextToOkCancel();
    };
    self.btnSave_Click = function (element, event) {
        var thisBtn = $("#btnSave");
        var preflight = ko.mapping.toJS(self.Preflight);        
        var result = MaintainModelStatusBeforePageLeave(self.btnSave_Click,"save");
        if (result == false)
        {
            return;
        }
        if (IsNullOrEmpty(self.Preflight.PreflightMain.RevisionNUM()))
            self.Preflight.PreflightMain.RevisionNUM(0);
        if (lastRevisionNo != null && self.Preflight.PreflightMain.RevisionNUM() < lastRevisionNo) {
            jAlert("Revision No. already exists and the new No. must be greater than " + lastRevisionNo, title);
            return;
        }
        if (preflight != null && preflight != '') {
            $("#btnSave").prop('disabled', "disabled");
            RequestStart();
            $.ajax({
                async: true,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightTrip",
                type: "POST",
                dataType: "json",
                data: {},
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    var IsSaved = jsonObj.IsSaved;
                    if (returnResult == true && result.d.Success == true && IsSaved == true) {
                        var Trip = jsonObj.Trip;
                        self.TripID(Trip.TripId);
                        self.Preflight.PreflightMain.RevisionNUM(Trip.PreflightMain.RevisionNUM == null ? 0 : Trip.PreflightMain.RevisionNUM);
                        self.EditMode(Trip.PreflightMain.EditMode);
                        ko.mapping.fromJS(Trip, self.Preflight);
                        if (jsonObj.Msg == "Trip has been saved successfully") {
                            $.cookie('Trip', 'Updated');
                            //window.location.reload();
                            var reloadUrl = removeQueryStringParam("IsCalendar", window.location.href);
                            window.location = reloadUrl;
                        }                       
                        else {
                            jAlert(jsonObj.Msg, "Trip Alert", refreshTrip);
                        }

                    }
                    else if (result.d.Success == false && result.d.StatusCode == 401) {
                        return;
                    }
                    else {
                        $("#btnSave").prop('disabled', "");
                        getTripExceptionGrid("reload");
                        $(thisBtn).removeClass("ui_nav_disable");
                        $(thisBtn).addClass("ui_nav");
                        jAlert(jsonObj.Msg, "Trip Alert");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#btnSave").prop('disabled', "");
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
        }
    };
    self.btnDelete_Click = function (element, event) {
        jConfirm("You are about to delete all data for this TRIP. All data for this TRIP will be lost. Are you sure you want to do this?", "Confirmation!", function (arg) {
            if (arg == true) {
                var preflight = ko.mapping.toJS(self.Preflight);
                if (preflight != null && preflight != '') {
                    $("#btnDeleteTrip").prop('disabled', "disabled");
                    RequestStart(null, null);
                    $.ajax({
                        async: true,
                        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/DeleteTrip",
                        type: "POST",
                        dataType: "json",
                        data: {},
                        contentType: "application/json; charset=utf-8",
                        success: function (result) {
                            var returnResult = verifyReturnedResult(result);
                            if (returnResult == true && result.d.Success == true) {
                                var jsonObj = result.d.Result;
                                self.TripID(jsonObj.TripId);
                                self.EditMode(jsonObj.PreflightMain.EditMode);
                                ko.mapping.fromJS(jsonObj, self.Preflight);
                                refreshTrip();
                            }
                            else if (result.d.Success == false && result.d.StatusCode == 405)
                            {
                                jAlert("You don\'t have permission to delete trip", "Trip Alert");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $("#btnDeleteTrip").prop('disabled', "");
                            ResponseEnd();
                            reportPreflightError(jqXHR, textStatus, errorThrown);
                        },
                        complete: function () { ResponseEnd(); }
                    });
                }
            }
        });
    };

    self.btLogTrip_Click = function () {
        $.ajax({
            async: false,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/IsTripValidForLogging",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    var jsonObj = result.d.Result;
                    if (jsonObj == false) {
                        jAlert("Tail Number Is Required To Log Tripsheet", title);
                        return;
                    }
                    else {
                        setAlertTextToYesNo();
                        jConfirm("Are you sure you want to create a Flight Log?", titleConfirm, function (r) {
                            if (r) {
                                RequestStart();
                                $.ajax({
                                    async: true,
                                    url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/IsTripAlreadyLogged",
                                    type: "POST",
                                    dataType: "json",
                                    data: {},
                                    contentType: "application/json; charset=utf-8",
                                    success: function (results) {
                                        var rtnResult = verifyReturnedResult(results);
                                        if (rtnResult == true && results.d.Success == true) {
                                            var jObj = results.d.Result;
                                            var IsAlreadyCreated = jObj.Result;
                                            if(IsAlreadyCreated==2) {
                                                setAlertTextToYesNo();
                                                jConfirm("The selected Tripsheet is already logged in Postflight. Selecting YES will remove all data previously entered per leg in this log.", titleConfirm, function (rYes) {
                                                    if (rYes) {
                                                        createPostflightLog();
                                                    }
                                                });
                                                setAlertTextToOkCancel();
                                            }
                                            else {
                                                createPostflightLog();
                                            }
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        reportPreflightError(jqXHR, textStatus, errorThrown);
                                    },
                                    complete: function () { ResponseEnd(); }
                                });

                            }
                        });
                        setAlertTextToOkCancel();
                    }
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () {  }

        });
    };
    self.btnTripDescOk_Click = function () {
        var revDesc = $('#tbDescription').val();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/UpdateRevisionDescription",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'RevDesc': revDesc }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                closeTripDescription();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () {}
        });
    };
    function createPostflightLog()
    {
        setAlertTextToOkCancel();
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/LogTrip",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var retResult = verifyReturnedResult(result);
                if (retResult == true && result.d.Success == true) {
                    var returnResult = result.d.Result;
                    if (returnResult.Result == "0") {
                        jAlert("Please select a trip to be logged", title);
                        return;
                    }
                    if (returnResult.Result != "1") {
                        jAlert("Error on creating new flight log", title);
                        return;
                    }
                    setAlertTextToOkCancel();
                    var poLogId = returnResult.POLogId;
                    var message = "Trip Logged into Postflight, Click OK to View Postflight Log No:" + poLogId;
                    Preflight.PreflightMain.LogNum(poLogId);
                    jConfirm(message, titleConfirm, function (r) {
                        if (r) {
                            window.location.href = "/Views/Transactions/PostFlight/PostFlightLegs.aspx?seltab=legs&fromPage=retrieve";
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ResponseEnd();
                reportPreflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { ResponseEnd(); }
        });
    }

    function setClientSetting() {
        var btn = document.getElementById("btnClientCode");
        if (self.UserPrincipal._clientCd != null && self.UserPrincipal._clientCd != '' && self.TripID()==0 && self.EditMode()==true) {
            self.Preflight.PreflightMain.Client.ClientCD(self.UserPrincipal._clientCd);
            var clienttext=self.Preflight.PreflightMain.Client.ClientCD;
            var clientlbl=self.Preflight.PreflightMain.Client.ClientDescription;
            var clienthdn=self.Preflight.PreflightMain.Client.ClientID;
            var pfClientId=self.Preflight.PreflightMain.ClientID;
            var clientcvlbl = document.getElementById("lbcvClientCode");
            $("#btnClientCode").prop('disabled', true);
            $("#tbClientCode").prop('readonly', true);
            Client_Validate_Retrieve(clienttext,clientlbl,clienthdn,clientcvlbl,pfClientId,btn);
        }
    }

    function SetDispatcherSetting() {
        var btn = document.getElementById("btnDispatcher");
        //if (self.UserPrincipal._IsAutomaticDispatchNum != null && self.UserPrincipal._IsAutomaticDispatchNum != '' && self.EditMode() == true) {
        //    $("#tbDispatchNumber").prop('disabled', true);
        //}
        if (!IsNullOrEmpty(self.UserPrincipal._IsAutoDispat) && self.UserPrincipal._IsAutoDispat == true && !IsNullOrEmpty(self.UserPrincipal._isDispatcher) && self.UserPrincipal._isDispatcher == true && self.TripID() == 0 && self.EditMode() == true) {
            self.Preflight.PreflightMain.DispatcherUserName(self.UserPrincipal._name);
            var distext = self.Preflight.PreflightMain.DispatcherUserName;
            var dislbl = self.Preflight.PreflightMain.UserMaster.DispatcherDesc;
            var disphonelbl = self.Preflight.PreflightMain.UserMaster.PhoneNum;
            var discvlbl = document.getElementById("lbcvDispatcher");
            var disUsername = self.Preflight.PreflightMain.DispatcherUserName;
            Dispatcher_Validate_Retrieve(distext, dislbl, disphonelbl, discvlbl, disUsername, btn);
        }
    }
    function GetACurrentTrip() {
        var isAsync = false;
        var Preflight = $("[id$='hdnPreflightMainInitializationObject']").val();
        var UserPrincipal = $("[id$='hdnUserPrincipalInitializationObject']").val();
        if (IsNullOrEmpty(Preflight) == true && IsNullOrEmpty(UserPrincipal) == true) {
            isAsync=false;
        } else {
            isAsync = true;
            var PreflightJsonObj = JSON.parse(Preflight);
            self.UserPrincipal = JSON.parse(UserPrincipal);
            self.Preflight = ko.mapping.fromJS(PreflightJsonObj);
            self.EditMode(false);
        }
        $.ajax({
            async: isAsync,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetACurrentTrip",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    var jsonObj=result.d.Result;
                    var Trip=jsonObj.Trip;
                    if(Trip.PreflightLegs.length>0) {
                        collapseItem(false);
                    } else {
                        collapseItem(true);
                    }
                    self.UserPrincipal=jsonObj.Principal;
                    self.TripID(Trip.TripId);
                    if(self.Preflight.PreflightMain==undefined) {
                        self.Preflight=ko.mapping.fromJS(Trip);
                    } else {
                        ko.mapping.fromJS(Trip,self.Preflight);
                    }
                    lastRevisionNo = self.Preflight.PreflightMain.RevisionNUM();
                    //Check Trip is selected from Calendar
                    if (isRequestComeFromCalendar == 1) {
                       CheckTripIsLocked();
                    }
                    else {
                       self.EditMode(Trip.PreflightMain.EditMode);
                    }
                    EnableDisableRevisionNo();
                    setTooltipforAirport(self.Preflight.PreflightMain.HomeBaseAirportICAOID(),self.Preflight.PreflightMain.HomebaseTooltip,document.getElementById("tbHomeBase"));
                    setClientSetting();
                    SetDispatcherSetting();
                    FSSDateParsing();
                    getTripExceptionGrid("reload");
                    if(self.Preflight!=null&&self.Preflight.PreflightMain!=null) {
                        $("#preflightTripViewerLink").attr("href","/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PreTSTripSheetMain.xml&tripnum="+ConvertNullToEmptyString(self.Preflight.PreflightMain.TripNUM())+"&tripid="+ConvertNullToEmptyString(self.Preflight.TripId()));
                    }
                    if(self.EditMode())
                    {
                        $("#preflightTripViewerLink").attr("href", "javascript:alertToSaveTripBfrPreview()");
                    }
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                $("#preflightTripViewerLink").attr("href", "#");
            reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function refreshTrip() {
        window.location.reload(true);
    }

    function confirmTripCancelCallBackFn(arg) {
        if (arg == true) {
            $("#preflightTripViewerLink").attr("href", "~/Views/Reports/TripSheetReportViewer.aspx?xmlFilename=PreTSTripSheetMain.xml&tripnum=" + ConvertNullToEmptyString(self.Preflight.PreflightMain.TripNUM()) + "&tripid=" + ConvertNullToEmptyString(self.Preflight.TripId()));
            //RequestStart(null, null);
            $.ajax({
                async:true,
                url:"/Views/Transactions/Preflight/PreflightTripManager.aspx/CancelATrip",
                type:"POST",
                dataType:"json",
                data:{},
                contentType:"application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        var jsonObj = result.d.Result;
                        self.TripID(jsonObj.TripId);
                        self.EditMode(jsonObj.PreflightMain.EditMode);
                        ko.mapping.fromJS(jsonObj, self.Preflight);

                        AfterCancelCallComplete(arg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                },
                complete:function() { ResponseEnd(); }
            });
        } else {
            self.EditMode(true);
        }        
    }

    self.BrowseBtn = ko.pureComputed(function() {
        return this.EditMode() ? "browse-button" : "browse-button-disabled";
    }, self);
    self.CopyIcon = ko.pureComputed(function() {
        return this.EditMode() ? "copy-icon" : "copy-icon-disabled";
    }, self);
    self.CharterRateBtn = ko.pureComputed(function () {
        if(Preflight.PreflightMain.Fleet != null) {
            if (this.EditMode() == true && Preflight.PreflightMain.Fleet.FleetID() > 0) {
                $("#btnTailNoChargeHistory").prop("disabled",false);
                return "charter-rate-button";
                
            } else {
                $("#btnTailNoChargeHistory").prop("disabled", true);
                return "charter-rate-disable-button";
            }
        } else {
            $("#btnTailNoChargeHistory").prop("disabled", true);
            return "charter-rate-disable-button";
        }
    }, self);
    self.NoteIcon = ko.pureComputed(function() {
        return this.EditMode() ? "note-icon" : "note-icon-disable";
    }, self);
    self.HistoryIcon = ko.pureComputed(function() {
        return self.TripID() ? "history-icon" : "history-icon-disable";
    }, self);
    self.HotelSaveIcon = ko.pureComputed(function () {
        return this.EditMode() && self.EnableHotel() ? "ui_nav" : "ui_nav_disable";
    }, self);
    
    ko.bindingHandlers.PreflightHeaderBtnStatus = {
        init: function (element, valueAccessor, allBindings) {            
        },
        update: function (element, valueAccessor, allBindings) {
            if (valueAccessor()) {
                $(element).removeClass("ui_nav");
                $(element).addClass("ui_nav_disable");
                $(element).prop('disabled', 'disabled');
            } else {
                $(element).prop('disabled',"");
            }
        }
    };

    ko.bindingHandlers.PreflightHeader = {
        init: function (element, valueAccessor, allBindings) {
            var tripid = valueAccessor();
            var mode = allBindings.get('mode');
            var btn = allBindings.get('btn');
            PreflightHeaderBtn(element, tripid, mode, btn);
        },
        update: function (element, valueAccessor, allBindings) {
            var tripid = valueAccessor();
            var mode = allBindings.get('mode');
            var btn = allBindings.get('btn');
            PreflightHeaderBtn(element, tripid, mode, btn);
        }
    };
    function PreflightHeaderBtn(element, tripid, mode, btn) {
        if (tripid == null) tripid = 0;
        if (tripid == 0 && mode == false && btn == "New") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        }
        else if (tripid != 0 && mode == true && btn == "Save") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        } else if (tripid != 0 && mode == true && btn == "Cancel") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        } else if (tripid == 0 && mode == true && btn == "Save") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        } else if (tripid == 0 && mode == true && btn == "Cancel") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        }
        else if (tripid == 0 && mode == true && btn == "APIS") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        }
        else if (tripid != 0 && mode == false && btn != "Save" && btn != "Cancel") {
            $(element).addClass("ui_nav");
            $(element).removeClass("ui_nav_disable");
            $(element).prop("disabled", false);
        } else {
            $(element).addClass("ui_nav_disable");
            $(element).removeClass("ui_nav");
            $(element).prop("disabled", true);
        }
    }

    ko.bindingHandlers.PreflightFooter={
        init: function (element, valueAccessor, allBindings) {
            var tripid = valueAccessor();
            var mode = allBindings.get('mode');
            var btn = allBindings.get('btn');
            PreflightFooterBtn(element,tripid,mode,btn);
        },
        update: function (element, valueAccessor, allBindings) {
            var tripid = valueAccessor();
            var mode = allBindings.get('mode');
            var btn = allBindings.get('btn');
            PreflightFooterBtn(element, tripid, mode, btn);
        }
    };
    function PreflightFooterBtn(element, tripid, mode, btn) {
        if (tripid == null) tripid = 0;
        if (tripid == 0 && mode == false) {
            $(element).addClass("button-disable");
            $(element).removeClass("button");
            $(element).prop("disabled", true);
        } else if (tripid != 0 && mode == false && (btn == "Delete" || btn == "Edit" || btn == "Next")) {
            $(element).addClass("button");
            $(element).removeClass("button-disable");
            $(element).prop("disabled", false);
        } else if ((tripid != 0 || tripid == 0) && mode == true && (btn == "Cancel" || btn == "Save" || btn == "Next")) {
            $(element).addClass("button");
            $(element).removeClass("button-disable");
            $(element).prop("disabled", false);
        } else {
            $(element).addClass("button-disable");
            $(element).removeClass("button");
            $(element).prop("disabled", true);
        }
    }

    InitializeDatepickerBindingHandler();

    InitializeTimepickerBindingHandler();

    InitializeFormatSingleDecimalBindingHandler();

    ko.bindingHandlers.formatCurrency = {
        init: function (element, valueAccessor) {
            var val = valueAccessor();
            var num=IsNullOrEmpty(val()) ? 0 : val();
            var p = Number(num).toFixed(2).split(".");
            var formatedString= "$" + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
                return num + (i && !(i % 3) ? "," : "") + acc;
            }, "") + "." + p[1];
            $(element).val(formatedString);
        },
        update: function (element, valueAccessor) {
            var val = valueAccessor();
            var num = IsNullOrEmpty(val()) ? 0 : val();
            var p = Number(num).toFixed(2).split(".");
            var formatedString = "$" + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
                return num + (i && !(i % 3) ? "," : "") + acc;
            }, "") + "." + p[1];
            $(element).val(formatedString);
        }
    };
    //Preflight main page Setting
    //START
    ko.bindingHandlers.AirTypetbEnableDisable = {
        init: function (element, valueAccessor) {
            var tailnum = ko.utils.unwrapObservable(valueAccessor());
            if (IsNullOrEmpty(tailnum) == true && self.EditMode() == false) {
                $(element).prop("disabled", true);
            }else if (IsNullOrEmpty(tailnum)) {
                $(element).prop("disabled", false);
            }else if (!IsNullOrEmpty(tailnum)) {
                $(element).attr("disabled", "disabled");
            } else if (IsNullOrEmpty(tailnum) == true && self.EditMode() == true) {
                $(element).removeAttr("disabled");
            } else {
                $(element).attr("disabled", "disabled");
            }
        },
        update: function (element, valueAccessor) {
            var tailnum = ko.utils.unwrapObservable(valueAccessor());
            if (IsNullOrEmpty(tailnum) == true && self.EditMode() == false) {
                $(element).prop("disabled", true);
            }else if (IsNullOrEmpty(tailnum)) {
                $(element).prop("disabled", false);
            }else if (!IsNullOrEmpty(tailnum)) {
                $(element).prop("disabled", true);
            } else if (IsNullOrEmpty(tailnum) == true && self.EditMode() == true) {
                $(element).removeAttr("disabled");
            } else {
                $(element).attr("disabled", "disabled");
            }
        }
    };
    ko.bindingHandlers.AirTypeBtnEnableDisable = {
        init: function (element, valueAccessor) {
            var tailnum = ko.utils.unwrapObservable(valueAccessor());
            if (IsNullOrEmpty(tailnum) == true && self.EditMode() == false) {
                $(element).removeClass("browse-button").addClass("browse-button-disabled");
                $(element).prop("disabled", true);
            }else if (IsNullOrEmpty(tailnum)==true) {
                $(element).removeClass("browse-button-disabled").addClass("browse-button");
                $(element).prop("disabled", false);
            }else if (!IsNullOrEmpty(tailnum)) {
                $(element).removeClass("browse-button").addClass("browse-button-disabled");
                $(element).attr("disabled", "disabled");
            } else if (IsNullOrEmpty(tailnum) == true && self.EditMode() == true) {
                $(element).addClass("browse-button").removeClass("browse-button-disabled");
                $(element).removeAttr("disabled");
            } else {
                $(element).removeClass("browse-button").addClass("browse-button-disabled");
                $(element).attr("disabled", "disabled");
            }
        },
        update: function (element, valueAccessor) {
            var tailnum = ko.utils.unwrapObservable(valueAccessor());
            if (IsNullOrEmpty(tailnum) == true && self.EditMode() == false) {
                $(element).removeClass("browse-button").addClass("browse-button-disabled");
                $(element).prop("disabled", true);
            } else if (IsNullOrEmpty(tailnum)) {
                $(element).removeClass("browse-button-disabled").addClass("browse-button");
                $(element).prop("disabled", false);
            } else if (!IsNullOrEmpty(tailnum)) {
                $(element).removeClass("browse-button").addClass("browse-button-disabled");
                $(element).attr("disabled", "disabled");
            } else if (IsNullOrEmpty(tailnum) == true && self.EditMode() == true) {
                $(element).addClass("browse-button").removeClass("browse-button-disabled");
                $(element).removeAttr("disabled");
            } else {
                $(element).removeClass("browse-button").addClass("browse-button-disabled");
                $(element).attr("disabled", "disabled");
            }
        }
    };
    ko.bindingHandlers.CQCustomerEnableDisable = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            var binding = allBindingsAccessor();
            var CQCheckFor=binding.CQCheckFor;
            var mode = valueAccessor();
            EnableDisableCQCustomer(element, mode,CQCheckFor);
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            var binding = allBindingsAccessor();
            var CQCheckFor=binding.CQCheckFor;
            var mode = valueAccessor();
            EnableDisableCQCustomer(element, mode,CQCheckFor);
        }
    };
    function EnableDisableCQCustomer(element, mode,CQCheckFor) {
        var DepartmentID=CQCheckFor=="Main" ? self.Preflight.PreflightMain.DepartmentID() : self.CurrentLegData.Department.DepartmentID();
        if (IsNullOrEmpty(self.UserPrincipal._DepartmentID) == false && self.UserPrincipal._DepartmentID == DepartmentID && mode() == true) {
            $(element).removeAttr("disabled");
            $("[id$='btCQcustomerPopup']").addClass("browse-button").removeClass("browse-button-disabled").removeAttr("disabled");
            $("[id$='btCQCustomer_AllLegs']").removeAttr("disabled").addClass("copy-icon").removeClass("copy-icon-disabled");

            $("[id$='tbAuthorization']").attr("disabled", "disabled");
            $("[id$='btnAuth']").addClass("browse-button-disabled").removeClass("browse-button").attr("disabled", "disabled");
            $("[id$='btnAuthorizationAllLegs']").addClass("copy-icon-disabled").removeClass("copy-icon").attr("disabled", "disabled");
        } else {
            $(element).attr("disabled", "disabled");
            $("[id$='btCQcustomerPopup']").removeClass("browse-button").addClass("browse-button-disabled").attr("disabled", "disabled");
            $("[id$='btCQCustomer_AllLegs']").addClass("copy-icon-disabled").removeClass("copy-icon").attr("disabled", "disabled");

            if (mode() == true) {
                $("[id$='tbAuthorization']").removeAttr("disabled");
                $("[id$='btnAuth']").addClass("browse-button").removeClass("browse-button-disabled").removeAttr("disabled");
                $("[id$='btnAuthorizationAllLegs']").removeAttr("disabled").addClass("copy-icon").removeClass("copy-icon-disabled");
            }
        }
    }
    self.Preflight.PreflightMain.FleetID.subscribe(function (newValue) {
        if (!IsNullOrEmpty(self.Preflight.PreflightMain.Fleet.TailNum()) && !IsNullOrEmpty(newValue) && newValue != 0) {
            var methodParam = { 'TailNum': self.Preflight.PreflightMain.Fleet.TailNum(), 'FleetID': newValue };
            if (!IsNullOrEmpty(self.Preflight.PreflightMain.Fleet.TailNum()) && !IsNullOrEmpty(newValue) && newValue != 0) {
                $.ajax({
                    async: true,
                    url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/UpdateFleetInfoAfterTailNumChange",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(methodParam),
                    contentType: "application/json; charset=utf-8",
                    success: function(result) {
                        var returnResult = verifyReturnedResult(result);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        reportPreflightError(jqXHR, textStatus, errorThrown);
                    }
                });
            }
        }
     });
    //END 



    // Save call on any correct value change

    ko.watch(self.Preflight.PreflightMain, { depth: 1, tagFields: true }, function (parents, child, item) {

        var callToSaveAllow = false;

        if (jQuery.inArray(child._fieldName, PreflightMainPropertyNamesList.split(",")) != -1 && self.Preflight.PreflightMain != undefined) {
            callToSaveAllow = true;
        }

        if (child._fieldName == "CrewID" && self.Preflight.PreflightMain.Crew != undefined && self.Preflight.PreflightMain.Crew.CrewCD() != undefined && (self.Preflight.PreflightMain.Crew.CrewCD().length == 0 || Number(self.Preflight.PreflightMain.CrewID()) > 0) && isNaN(Number(self.Preflight.PreflightMain.CrewID())) == false) {
            callToSaveAllow = true;
        }

        if (child._fieldName == "EmergencyContactID" && self.Preflight.PreflightMain.EmergencyContact != undefined && self.Preflight.PreflightMain.EmergencyContact.EmergencyContactCD() != undefined && (Preflight.PreflightMain.EmergencyContact.EmergencyContactCD().length == 0 || Number(self.Preflight.PreflightMain.EmergencyContactID()) > 0) && isNaN(Number(self.Preflight.PreflightMain.EmergencyContactID())) == false) {
            callToSaveAllow = true;
        }
        if (child._fieldName == "ClientID" && self.Preflight.PreflightMain.Client != undefined && self.Preflight.PreflightMain.Client.ClientCD() != undefined &&
            (self.Preflight.PreflightMain.Client.ClientCD().length == 0 || Number(self.Preflight.PreflightMain.ClientID()) > 0) &&
            isNaN(Number(self.Preflight.PreflightMain.ClientID())) == false) {
            callToSaveAllow = true;
        }

        if (child._fieldName == "PassengerRequestorID" && self.Preflight.PreflightMain.Passenger != undefined && self.Preflight.PreflightMain.Passenger.PassengerRequestorCD() != undefined &&
            (self.Preflight.PreflightMain.Passenger.PassengerRequestorCD().length == 0 || Number(self.Preflight.PreflightMain.PassengerRequestorID()) > 0) &&
            isNaN(Number(self.Preflight.PreflightMain.PassengerRequestorID())) == false) {
            callToSaveAllow = true;
        }

        if (child._fieldName == "AccountID" && self.Preflight.PreflightMain.Account != undefined && self.Preflight.PreflightMain.Account.AccountNum() != undefined &&
            (self.Preflight.PreflightMain.Account.AccountNum().length == 0 || Number(self.Preflight.PreflightMain.AccountID()) > 0) &&
            isNaN(Number(self.Preflight.PreflightMain.AccountID())) == false) {
            callToSaveAllow = true;
        }

        if (child._fieldName == "DepartmentID" && self.Preflight.PreflightMain.Department != undefined &&
            self.Preflight.PreflightMain.Department.DepartmentCD() != undefined &&
            (self.Preflight.PreflightMain.Department.DepartmentCD().length == 0 || Number(self.Preflight.PreflightMain.DepartmentID()) > 0) &&
            isNaN(Number(self.Preflight.PreflightMain.DepartmentID())) == false) {
            callToSaveAllow = true;
        }
        if (child._fieldName == "CQCustomerID" && self.Preflight.PreflightMain.CQCustomer != undefined &&
            self.Preflight.PreflightMain.CQCustomer.CQCustomerCD() != undefined &&
            (self.Preflight.PreflightMain.CQCustomer.CQCustomerCD().length == 0 || Number(self.Preflight.PreflightMain.CQCustomerID()) > 0) &&
            isNaN(Number(self.Preflight.PreflightMain.CQCustomerID())) == false) {
            callToSaveAllow = true;
        }

        if (child._fieldName == "DispatcherUserName" && self.Preflight.PreflightMain.DispatcherUserName != undefined &&
            self.Preflight.PreflightMain.DispatcherUserName() != undefined &&
            self.Preflight.PreflightMain.DispatcherUserName().length > 0) {
            callToSaveAllow = true;
        }

        setTimeout(function () {
            if (callToSaveAllow == true && self.EditMode() == true && flagIsInitialLoad == false) {
                SavePreflightMain();
            }
        }, 500);
    });

    flagIsInitialLoad = false;
};
ko.subscribable.fn['Trimmed'] = function () {
    return ko.computed({
        read: function () {
            return $.trim(this());
        },
        write: function (value) {
            this($.trim(value));
            this.valueHasMutated();
        },
        owner: this
    });
};
function AfterCancelCallComplete(arg)
{
    if (self.TripID != null && self.TripID() == 0 && arg == true) {
        self.TripID(null);
        window.location = "/views/Transactions/Preflight/PreflightMain.aspx?seltab=PreFlight";
    } else if (self.TripID != null && self.TripID() > 0 && arg == true ) {
        window.location.reload(true);
    }
    $("#Exception").jqGrid('GridUnload');
}
function SavePreflightMain() {
    self.Preflight.PreflightMain.Passenger.LastUpdTS = ko.observable(self.Preflight.PreflightMain.Passenger.LastUpdTS()).extend({ dateFormat: isodateformat });
    self.Preflight.PreflightMain.LastUpdTS = ko.observable(self.Preflight.PreflightMain.LastUpdTS()).extend({ dateFormat: isodateformat });    
    self.Preflight.PreflightMain.EstDepartureDT = ko.observable(self.Preflight.PreflightMain.EstDepartureDT()).extend({ dateFormat: isodateformat });
    self.Preflight.PreflightMain.RequestDT = ko.observable(self.Preflight.PreflightMain.RequestDT()).extend({ dateFormat: isodateformat });

    var preflight = ko.mapping.toJS(self.Preflight);
    $.ajax({
        async: false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightMain",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'pfMain': preflight.PreflightMain }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
        },error: function (jqXHR, textStatus, errorThrown) {
        ResponseEnd();
        reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function FSSDateParsing() {
    self.Preflight.PreflightMain.LastUpdTS = ko.observable(self.Preflight.PreflightMain.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
}

function getTripExceptionGrid(arg) {
    if (self.Preflight.PreflightMain.EstDepartureDT() != '')
        self.Preflight.PreflightMain.EstDepartureDT = ko.observable(self.Preflight.PreflightMain.EstDepartureDT()).extend({ dateFormat: isodateformat });
    if (self.Preflight.PreflightMain.ClientEstDepartureDT() != '')
        self.Preflight.PreflightMain.ClientEstDepartureDT = ko.observable(self.Preflight.PreflightMain.ClientEstDepartureDT()).extend({ dateFormat: isodateformat });

    if (self.Preflight.PreflightMain.EstArrivalDT() != '')

        self.Preflight.PreflightMain.EstArrivalDT = ko.observable(self.Preflight.PreflightMain.EstArrivalDT()).extend({ dateFormat: isodateformat });

    if (self.Preflight.PreflightMain.RequestDT() != '')
        self.Preflight.PreflightMain.RequestDT = ko.observable(self.Preflight.PreflightMain.RequestDT()).extend({ dateFormat: isodateformat });
    if (self.Preflight.PreflightMain.ClientRequestDT() != '')
        self.Preflight.PreflightMain.ClientRequestDT = ko.observable(self.Preflight.PreflightMain.ClientRequestDT()).extend({ dateFormat: isodateformat });
    if (self.Preflight.PreflightMain.LastUpdTS() != '')
        self.Preflight.PreflightMain.LastUpdTS = ko.observable(self.Preflight.PreflightMain.LastUpdTS()).extend({ dateFormat: isodateformat });
    if (self.Preflight.PreflightMain.Passenger.LastUpdTS() != '')
        self.Preflight.PreflightMain.Passenger.LastUpdTS = ko.observable(self.Preflight.PreflightMain.Passenger.LastUpdTS()).extend({ dateFormat: isodateformat });
    if(self.Preflight.PreflightMain.BeginningGMTDT()!='')
        self.Preflight.PreflightMain.BeginningGMTDT=ko.observable(self.Preflight.PreflightMain.BeginningGMTDT()).extend({dateFormat:isodateformat.toUpperCase()});
    if(self.Preflight.PreflightMain.EndingGMTDT()!='')
        self.Preflight.PreflightMain.EndingGMTDT=ko.observable(self.Preflight.PreflightMain.EndingGMTDT()).extend({dateFormat:isodateformat.toUpperCase()});
    if (self.Preflight.PreflightMain.APISSubmit() != '')
        self.Preflight.PreflightMain.APISSubmit = ko.observable(self.Preflight.PreflightMain.APISSubmit()).extend({ dateFormat: isodateformat.toUpperCase() });

    var preflight = ko.mapping.toJS(self.Preflight);
    $.ajax({
        async: true,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/TripException",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'trip': preflight.PreflightMain }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                var jsonObj = result.d.Result;
                if (arg == "init")
                    BindExceptionGrid(jsonObj.ExceptionList);
                else {
                    $("#Exception").jqGrid('GridUnload');
                    BindExceptionGrid(jsonObj.ExceptionList);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
        ResponseEnd();
        reportPreflightError(jqXHR, textStatus, errorThrown);
    }
    });
}
function setSeverityImage(cellvalue, options, rowObject) {
    var cellValueInt = parseInt(cellvalue);
    if (cellValueInt == 1) {
        return "<center><img src='/App_Themes/Default/images/info_red.gif' /></center>";
    }
    else {
        return "<center><img src='/App_Themes/Default/images/info_yellow.gif' /></center>";
    }
}
function BindExceptionGrid(data) {
    jQuery("#Exception").jqGrid({
        data: data,
        datatype: "local",
        height: 'auto',
        autowidth: false,
        shrinkToFit: true,
        width: 306,
        rowNum:500,
        colModel: [
            { name: 'Severity', index: 'Severity', width:30, formatter: setSeverityImage },
            { name: 'ExceptionDescription', index: 'ExceptionDescription', width: 280 },
            { name: 'SubModuleGroup', index: 'SubModuleGroup', width: 140, summaryType: setExceptionGroupHeader },
            { name: 'SubModuleID', width: 140, hidden: true }
        ],
        viewrecords: true,
        sortname: 'SubModuleID',
        sortorder: 'asc',
        grouping: true,
        groupingView: {
            groupField: ['SubModuleID'],
            groupColumnShow: [true],
            groupText: ['{SubModuleGroup}'],
            groupCollapse: false
        },
        loadComplete: function () {
            if (jQuery('#Exception').getGridParam("records") == 0) {
                $('#gbox_Exception').css('height', '0px');
                $('#gbox_Exception').css('overflow', 'hidden');
            }
            $(this).jqGrid('hideCol', ["SubModuleGroup"]);
            $(this).jqGrid('hideCol', ["SubModuleID"]);
        }
    });
}
function setExceptionGroupHeader(val, name, record) {
    if (!IsNullOrEmptyOrUndefined(record))
        return "Group: " + record[name];
    else return "";
}

function InitializeSortSummaryDetail() {
    jQuery(ShortLegSummary).jqGrid({
        url:'/Views/Transactions/PreFlightValidator.aspx/Summary_Validate_Retrieve',
        mtype: 'POST',
        datatype: "json",
        cache:false,
        async:true,
        rowNum:500,
        loadonce:true,
        ajaxGridOptions:{contentType:'application/json; charset=utf-8'},
        serializeGridData:function(postData) {
            if(postData._search==undefined||postData._search==false) {
                if(postData.filters===undefined) postData.filters=null;
            }
            postData._search=undefined;
            postData.nd=undefined;
            postData.size=undefined;
            postData.page=undefined;
            postData.sort=undefined;
            postData.dir=undefined;
            postData.filters=undefined;
            postData.DateFormat=self.UserPrincipal._ApplicationDateFormat;
            postData.TenMin=self.UserPrincipal._TimeDisplayTenMin==null ? 1 : self.UserPrincipal._TimeDisplayTenMin;
            postData.ElapseTM=self.UserPrincipal._ElapseTMRounding;
            return JSON.stringify(postData);
        },
        height:'auto',
        width: 238,
        autowidth: false,
        shrinkToFit: true,
        colNames: ['Departure', 'From', 'To', 'Arrival'],
        colModel:[
            {
                name: 'DepartDate', index: 'DepartDate', width: 170,
                cellattr: function (rowId, val, rawObject)
                { return ' title="' + rawObject.DepartDateTime + '"'; }
            },
            {
                name: 'DepartAir', index: 'DepartAir', width: 120,
                cellattr: function (rowId, val, rawObject)
                { return ' title="' + rawObject.DepartAirCityName + '"'; }
            },
            {
                name: 'ArrivalAir', index: 'ArrivalAir', width: 120,
                cellattr: function (rowId, val, rawObject)
                { return ' title="' + rawObject.ArrivalAirCityName + '"'; }
            },
            {
                name: 'ArrivalDate', index: 'ArrivalDate', width: 170,
                cellattr: function (rowId, val, rawObject)
                { return ' title="' + rawObject.ArrivalDateTime + '"'; }
            }
        ],
        viewrecords: false,
        loadComplete: function () {
            if(jQuery(ShortLegSummary).getGridParam("records")==0) {
                $(ShortLegSummary + ' tbody').html("<div style='padding-left:5px;font: 11px/16px Arial,Helvetica,sans-serif !important;'>No records found</div>");
            }
        }
    });
}
function InitializeDetailedLegSummary() {
    jQuery(DetailedLegSummary).jqGrid({
        url: '/Views/Transactions/PreFlightValidator.aspx/Summary_Validate_Retrieve',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: true,
        rowNum:500,
        loadonce: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8'},
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }
            postData._search = undefined;
            postData.nd = undefined;
            postData.size = undefined;
            postData.page = undefined;
            postData.sort = undefined;
            postData.dir = undefined;
            postData.filters = undefined;
            postData.DateFormat = self.UserPrincipal._ApplicationDateFormat;
            postData.TenMin = self.UserPrincipal._TimeDisplayTenMin == null ? 1 : self.UserPrincipal._TimeDisplayTenMin;
            postData.ElapseTM = self.UserPrincipal._ElapseTMRounding;
            return JSON.stringify(postData);
        },
        autowidth: false,
        width:727,
        shrinkToFit: false,
        colNames: ['Legnum','Leg', 'Depart Date/Time', 'Dep ICAO', 'Arr ICAO', 'Arrive Date/Time', 'TA', 'ETE', 'Approx. Time', 'OS', 'Dept.', 'Auth.', 'Cat.', 'End Day', 'Duty Err.', 'Flight Cost'],
        colModel: [
            { name: 'LegNum', index: 'LegNum', width: 40 ,hidden:true},
            { name: 'LegNum', index: 'LegNum',width:40},
            { name: 'DepartDateTime', index: 'DepartDateTime', width:110},
            { name: 'DepartAir', index: 'DepartAir',width:60 },
            { name: 'ArrivalAir', index: 'ArrivalAir', width: 60 },
            { name: 'ArrivalDateTime', index: 'ArrivalDateTime', width: 110 },
            { name: 'TA', index: 'TA' ,width:25},
            { name: 'ETE', index: 'ETE', width: 40 },
            { name: 'ApproxTime', index: 'ApproxTime', formatter: "checkbox", formatoptions: { disabled: true }, width: 50 },
            { name: 'OS', index: 'OS', formatter: "checkbox", formatoptions: { disabled: true }, width: 27 },
            { name: 'Dept', index: 'Dept', width: 80 },
            { name: 'Auth', index: 'Auth',width:80 },
            { name: 'Cat', index: 'Cat', width: 60 },
            { name: 'EOD', index: 'EOD', formatter: "checkbox", formatoptions: { disabled: true }, width: 40 },
            {
                name: 'DutyErr', index: 'DutyErr', width: 40,
                cellattr: function (rowId, cellValue, rawObject, cm, rdata) {
                    var style = "";
                    if(cellValue!=null&&cellValue!='')
                        style='color:red';
                    else
                        style = 'color:black';
                    return "Style='" + style + "'";
                }
            },
            { name: 'FlightCost', index: 'FlightCost', width: 90 }
        ],
        viewrecords: false,
        loadComplete: function () {
            if (jQuery(DetailedLegSummary).getGridParam("records") == 0) {
                $(DetailedLegSummary + ' tbody').html("<div style='padding-left:5px;font: 11px/16px Arial,Helvetica,sans-serif !important;'>No records found</div>");
            }

            var tableHeader = $("#gbox_DetailedLegSummary").find('.ui-jqgrid-hbox').css("position", "relative");
            $("#gbox_DetailedLegSummary").find('.ui-jqgrid-bdiv').bind('jsp-scroll-x', function (event, scrollPositionX, isAtLeft, isAtRight) {
                tableHeader.css('right', scrollPositionX);
            }).bind(
				'jsp-scroll-y',
				function (event, scrollPositionY, isAtTop, isAtBottom) {

				}
			).jScrollPane({
			    scrollbarWidth: 15,
			    scrollbarMargin: 0
			});
            $(".jspHorizontalBar").css("height", "10px");



        }
    });
}

function OnTripSearchClose(oWnd, args) {
    if (args != null) {
        var arg = args.get_argument();
        if (arg) {
            self.TripSearch(arg.TripNUM);
            if (self.EditMode() == true) {
                $("[id$='SearchBox']").val("");
            }
        }
    }
}
   

function alertToSaveTripBfrPreview() {
    jAlert("Trip Unsaved, please Save or Cancel Trip", "Preflight");
}

function CheckTripIsLocked() {
    var tripid = self.TripID();
    $.ajax({
        async: true,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SetAEditLock",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'TripID': tripid }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            var lock = result.d.Result;
            if (returnResult == true && result.d.Success == true && lock.IsLocked == "True") {
                self.EditMode(true);
                self.Preflight.PreflightMain.EditMode(true);
            } else {
                jAlert(lock.LockMsg, "Preflight");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}

function PreflightEditbuttonClick()
{
    var thisBtn = $("#btnEditTrip");
    var tripid = self.TripID();
    if (tripid != null && tripid != '' && tripid != 0) {
        $("#btnEditTrip").prop('disabled', "disabled");
        RequestStart();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SetAEditLock",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'TripID': tripid }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var lock = result.d.Result;
                if (returnResult == true && result.d.Success == true && lock.IsLocked == "True") {
                    self.EditMode(true);
                    self.Preflight.PreflightMain.EditMode(true);
                    EnableDisableRevisionNo();
                    HideSuccessMessage()
                    if (UserPrincipal._IsOpenHistory == true) {
                        openWinShared("rdHistory");
                        return false;
                    }
                } else {
                    $("#btnEditTrip").prop('disabled', "");
                    $(thisBtn).removeClass("ui_nav_disable");
                    $(thisBtn).addClass("ui_nav");
                    jAlert(lock.LockMsg, "Preflight");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#btnEditTrip").prop('disabled', "disabled");
                ResponseEnd();
                reportPreflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { ResponseEnd(); }
        });
    }
}
