﻿var self = this;
var LegPropertyNamesList = "FlightPurpose,IsScheduledServices,IsPrivate,IsApproxTM,DepartComments,FlightNUM,USCrossing,CrewDutyRulesID";
var PreflightLegViewModel = function () {
    self.Legs = ko.observableArray().extend({ notify: 'always' });
    self.CurrentLegData = ko.observable().extend({ notify: 'always' });
    self.CurrentLegNum = ko.observable();
    self.CurrentLegNum.subscribe(function (newValue) {
        FormatLegDates();
        var legNum = Number(newValue) - 1;
        self.CurrentLegData.LastUpdTS = ko.observable(self.CurrentLegData.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
        self.CurrentLegData.IsLegEdited=true;
        var LegData = ko.mapping.toJS(self.CurrentLegData);
        
        if (LegData.ClientID == null) {
            delete LegData.Client;
        }
        ClearViewModelChildren(LegData);
        
        if (legNum >= 0) {
            RequestStart();
            $.ajax({
                async:true,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePrevLegAndReturnCurrentLegData",
                type:"POST",
                dataType:"json",
                data: JSON.stringify({ 'LegIndex': legNum, 'PreviousLegData': LegData }),
                contentType:"application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        collapseItem(false);
                        var jsonObj = result.d.Result;
                        if (jsonObj != null) {
                            if (self.CurrentLegData.LegNUM == undefined) {
                                self.CurrentLegData = ko.mapping.fromJS(jsonObj);
                            } else {
                                ko.mapping.fromJS(jsonObj, self.CurrentLegData);
                            }
                            FormatLegDates();
                            isDepartureOrArrivalConfirmed();
                            setTooltipforAirport(self.CurrentLegData.DepartureAirport.IcaoID(), self.CurrentLegData.DepartureAirport.tooltipInfo, document.getElementById("tbDepart"), "DEPT");
                            setTooltipforAirport(self.CurrentLegData.ArrivalAirport.IcaoID(), self.CurrentLegData.ArrivalAirport.tooltipInfo, document.getElementById("tbArrival"));
                            PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(), "CrewDutyRule");
                        }
                    }
                    self.getTripExceptionGrid("reload");
                }, error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () {
                    ResponseEnd();
                    checklegnotes();
                }
            });
        }
    });
    self.LegTab_Click = function (Leg) {
        RemoveAllErrorLabels();
        self.CurrentLegNum(Leg.LegNUM());
        $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $("#Leg" + Leg.LegNUM()).removeClass("tabStripItem").addClass("tabStripItemSelected");
        jQuery(DetailedLegSummary).jqGrid('GridUnload');
        InitializeDetailedLegSummary();
    };
    self.IsHaveLeg = ko.observable();
    
    self.DutyHoursAlert = ko.pureComputed(function () {
        var str=self.CurrentLegData.CrewDutyAlert()==null ? "" : self.CurrentLegData.CrewDutyAlert();
        return str.indexOf("D") >= 0 ? "infored" : "infoash";
    }, self);
    self.FlightHoursAlert = ko.pureComputed(function () {
        var str = self.CurrentLegData.CrewDutyAlert() == null ? "" : self.CurrentLegData.CrewDutyAlert();
        return str.indexOf("F") >= 0 ? "infored" : "infoash";
    }, self);
    self.RestHoursAlert = ko.pureComputed(function () {
        var str = self.CurrentLegData.CrewDutyAlert() == null ? "" : self.CurrentLegData.CrewDutyAlert();
        return str.indexOf("R") >= 0 ? "infored" : "infoash";
    }, self);
    self.CrewDutyRuleTitle = ko.pureComputed(function () {
        if (self.CurrentLegData.CrewDutyRule != null) {
            return "Duty Day Start  : " + (self.CurrentLegData.CrewDutyRule.DutyDayBeingTM())
                                   + "\n" + "Duty Day End    : " + (self.CurrentLegData.CrewDutyRule.DutyDayEndTM())
                                   + "\n" + "Max. Duty Hrs. Allowed     : " + (self.CurrentLegData.CrewDutyRule.MaximumDutyHrs())
                                   + "\n" + "Max. Flight Hrs. Allowed     : " + (self.CurrentLegData.CrewDutyRule.MaximumFlightHrs())
                                   + "\n" + "Min. Fixed Rest : " + (self.CurrentLegData.CrewDutyRule.MinimumRestHrs());
        }
        return "";
    }, self).extend({ notify: 'always' });
    
    self.Status = ko.pureComputed(function () {
        switch(self.Preflight.PreflightMain.TripStatus()) {
        case "W":
            return "W-Worksheet";
        case "T":
            return "T-Tripsheet";
        case "U":
            return "U-Unfulfilled";
        case "X":
            return "X-Cancelled";
        case "H":
            return "H-Hold";
        default:
            return ""; 
        }
    }, self);
   
    function InitializePreflghtLegs() {
        RequestStart();
        self.getTripExceptionGrid("reload");
        var isAsync;
        var LegInitializa = $("[id$='hdnPreflightLegInitializationObject']").val();
        $("[id$='hdnPreflightLegInitializationObject']").val("");
        if (IsNullOrEmpty(LegInitializa) == true) {
            isAsync = false;
        } else {
            isAsync = true;
            var LegJsonObj = JSON.parse(LegInitializa);
            self.Legs = ko.mapping.fromJS(LegJsonObj.Leg);
            self.CurrentLegData = ko.mapping.fromJS(LegJsonObj.CurrentLeg);
            self.EditMode(LegJsonObj.EditMode);
            self.IsHaveLeg(LegJsonObj.IsHaveLeg);
            FormatLegDates();
            isDepartureOrArrivalConfirmed();
            if (!IsNullOrEmpty(self.CurrentLegData.ArrivalAirport.IcaoID()) && !IsNullOrEmpty(self.CurrentLegData.DepartureAirport.IcaoID())) {
                if(self.TripID() > 0) {
                    PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(),"CrewDutyRule");
                }
            } else {
                PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(),"CrewDutyRule");
            }
            InitializeSortSummaryDetail();
            InitializeDetailedLegSummary();
            var OldHomebaseIdVal = self.Preflight.PreflightMain.OldHomebaseID();
            var NewHomebaseIdVal = self.Preflight.PreflightMain.HomebaseID();
            if (!IsNullOrEmpty(OldHomebaseIdVal) && OldHomebaseIdVal != NewHomebaseIdVal) {
                self.Preflight.PreflightMain.OldHomebaseID(NewHomebaseIdVal);
                PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(), "Airport");
            }
            ResponseEnd();
        }
        if (isAsync == false) {
            RequestStart();
            $.ajax({
                async:isAsync,
                url:"/Views/Transactions/Preflight/PreflightTripManager.aspx/InitializePreflghtLegs",
                type:"POST",
                dataType:"json",
                data: JSON.stringify({ 'selectedLegNum': '0' }),
                contentType:"application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        var jsonObj = result.d.Result;
                        if (jsonObj != null) {
                            self.Legs = ko.mapping.fromJS(jsonObj.Leg);
                            self.CurrentLegData = ko.mapping.fromJS(jsonObj.CurrentLeg);
                            self.IsHaveLeg(jsonObj.IsHaveLeg);
                            FormatLegDates();
                            $("[id$='DepAirpotObj']").val(JSON.stringify(jsonObj.CurrentLeg.DepartureAirport));
                            $("[id$='ArriAirpotObj']").val(JSON.stringify(jsonObj.CurrentLeg.ArrivalAirport));
                            if (!IsNullOrEmpty(self.CurrentLegData.ArrivalAirport.IcaoID()) && !IsNullOrEmpty(self.CurrentLegData.DepartureAirport.IcaoID()))
                                PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(), "Airport");
                            else { PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(), "CrewDutyRule"); }
                            InitializeSortSummaryDetail();
                            InitializeDetailedLegSummary();
                            var OldHomebaseIdVal = self.Preflight.PreflightMain.OldHomebaseID();
                            var NewHomebaseIdVal = self.Preflight.PreflightMain.HomebaseID();
                            if (!IsNullOrEmpty(OldHomebaseIdVal) && OldHomebaseIdVal != NewHomebaseIdVal) {
                                self.Preflight.PreflightMain.OldHomebaseID(NewHomebaseIdVal);
                                PreflightCalculation_Validate_Retrieve(self.CurrentLegData.IsDepartureConfirmed(), "Airport");
                            }
                        }
                    }
                },
                error:function(jqXHR,textStatus,errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR,textStatus,errorThrown);
                },
                complete:function() { ResponseEnd(); }
            });
        }
    }
    
    function APISSetting() {
        if (self.UserPrincipal._IsAPISSupport == true) {
            $("#lbCBPConfirmNo").show();
            $("#tbCBPConfirmNumber").show();
        } else {
            $("#lbCBPConfirmNo").hide();
            $("#tbCBPConfirmNumber").hide();
        }
    }
    function HomebaseSetting() {
        var HomebaseIcaoId = self.Preflight.PreflightMain.HomeBaseAirportICAOID();
        if (HomebaseIcaoId != null && HomebaseIcaoId != '') {
            $.ajax({
                async: false,
                url: "/Views/Transactions/CommonValidator.aspx/AirportDetail_Validate_Retrieve",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'AirportIcaoId': HomebaseIcaoId }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true  && jsonObj != null && jsonObj.flag == 0) {
                        var airport = jsonObj.AirportData;
                        $("#hdnHomeBaseCountry").val(airport.CountryID);
                        $("#hdHomebaseAirport").val(airport.AirportID);
                        setHomebaseAirportDetail(airport);
                        
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
    function AuthorizationSetting() {
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/AuthorizationForLeg",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    var jsonObj = result.d.Result;
                    if (jsonObj != null) {
                        if (jsonObj.Leg == false) window.location = "/Views/ErrorPages/AccessDenied.aspx";
                        if (jsonObj.Next == false) $("#btnNext").remove();
                        if (jsonObj.Delete == false) $("#btnDeleteTrip").remove();
                        if (jsonObj.Add1 == false) $("#btnAddLeg1").remove();
                        if (jsonObj.Add2 == false) $("#btnAddLeg").remove();
                        if (jsonObj.Insert1 == false) $("#btnInsertLeg").remove();
                        if (jsonObj.Insert2 == false) $("#btnInsertLeg1").remove();
                        if (jsonObj.Delete == false) $("#btnDeleteLeg").remove();
                        if (jsonObj.Delete2 == false) $("#btnDeleteLeg1").remove();
                        if (jsonObj.Checklist == false) $("[id$='lnkbtnChecklist']").remove();
                        if (jsonObj.Outbound == false) $("[id$='lnkbtnOutbound']").remove();
                        if (jsonObj.LegNotes == false) $("[id$='pnlLegNotes']").remove();
                        if (jsonObj.Save == false) $("#btnSave").remove();
                        if (jsonObj.Cancel == false) $("#btnCancel").remove();
                    }
                }
            }, error: function (jqXHR, textStatus, errorThrown) {
                ResponseEnd();
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
    function setClientSetting() {
        var btn = document.getElementById("btnClient");
        if (self.UserPrincipal._clientCd != null && self.UserPrincipal._clientCd != '' && self.TripID() == 0 && self.EditMode() == true) {
            $("#btnClient").prop('disabled', true);
            $("#tbClient").prop('readonly', true);
        }
    }
    function NogoSetting() {
        if (self.Preflight.PreflightMain.Aircraft.AircraftID() != null && self.Preflight.PreflightMain.Aircraft.AircraftID() != '') {
            var id=self.Preflight.PreflightMain.Aircraft.AircraftID();
            $.ajax({
                async: false,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetAAircraft",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'aircraftId': id }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        var jsonObj = result.d.Result;
                        if (jsonObj != null && jsonObj.flag == 0) {
                            setAircraftDetail(jsonObj.Aircraft);
                            if (jsonObj.Aircraft.IsFixedRotary == "R") {
                                $("#tbNogo").remove();
                                $("#lbNogo").remove();
                            }
                        }
                    } 
                }, error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
    NogoSetting();
    HomebaseSetting();
    InitializePreflghtLegs();
    if (self.TripID() == 0 && self.CurrentLegData.ArrivalAirport.AirportID() == 0) {
        setClientSetting();
    }
    AuthorizationSetting();
    
    APISSetting();
    setTooltipforAirport(self.CurrentLegData.DepartureAirport.IcaoID(), self.CurrentLegData.DepartureAirport.tooltipInfo, document.getElementById("tbDepart"));
    setTooltipforAirport(self.CurrentLegData.ArrivalAirport.IcaoID(), self.CurrentLegData.ArrivalAirport.tooltipInfo, document.getElementById("tbArrival"));
    ko.watch(self.CurrentLegData, { depth: 1, tagFields: true }, function (parents, child, item) {
        if (jQuery.inArray(child._fieldName, LegPropertyNamesList.split(",")) != -1) {
            setTimeout(function() {
                if (self.EditMode() == true) {
                    SaveCurrentLegData();
                    UpdateCurrentLegTripEntityState(self.CurrentLegData.LegNUM());
                }
            }, 1000);
        }
    });


    self.copyCrewDutyRuleToAllLeg = function () {
        if (!IsNullOrEmpty(self.CurrentLegData.CrewDutyRule.CrewDutyRuleCD())) {
            setAlertTextToYesNo();
            jConfirm("Copy Crew Duty rules To All Legs?",
                "Confirmation!",
                function (arg) {
                    if (arg) {
                        var ajaxParam = "{'LegNUM':" + self.CurrentLegData.LegNUM() + "}";
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "PreflightTripManager.aspx/CopyCrewDutyRuleToAllLegs",
                            data: ajaxParam,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                reportPreflightError(xhr, textStatus, errorThrown);
                            }
                        });
                    }
                });
            setAlertTextToOkCancel();
        }
    }; // End of copyCrewDutyRuleToAllLeg

    self.copyCrewNotesToAllLeg = function () {
        if (!IsNullOrEmpty(self.CurrentLegData.CrewNotes())) {
            setAlertTextToYesNo();
            jConfirm("Copy Crew Notes To All Legs?",
                "Confirmation!",
                function (arg) {
                    if (arg) {
                        var ajaxParam = "{'LegNUM':" + self.CurrentLegData.LegNUM() + " , 'crewNotes' :'" + self.CurrentLegData.CrewNotes() + "'}";
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "PreflightTripManager.aspx/CopyCrewNotesToAllLeg",
                            data: ajaxParam,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                reportPreflightError(xhr, textStatus, errorThrown);
                            }
                        });
                    }
                });
            setAlertTextToOkCancel();
        }
    }; // End of copyCrewNotesToAllLeg


    self.copyPaxNotesToAllLeg = function () {
        if (!IsNullOrEmpty(self.CurrentLegData.Notes())) {
            setAlertTextToYesNo();
            jConfirm("Copy PAX Notes To All Legs?",
                "Confirmation!",
                function (arg) {
                    if (arg) {
                        var ajaxParam = "{'LegNUM':" + self.CurrentLegData.LegNUM() + " , 'paxNotes' :'" + self.CurrentLegData.Notes() + "'}";
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "PreflightTripManager.aspx/CopyPaxNotesToAllLeg",
                            data: ajaxParam,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                            },
                            error: function (xhr, textStatus, errorThrown) {
                                reportPreflightError(xhr, textStatus, errorThrown);
                            }
                        });
                    }
                });
            setAlertTextToOkCancel();
        }
    }; // End of copyPaxNotesToAllLeg
    setTimeout(function() {
        $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $("#Leg" + self.CurrentLegData.LegNUM()).removeClass("tabStripItem").addClass("tabStripItemSelected");
    }, 500);
};

function UpdateCurrentLegTripEntityState(legNum) {
    var ajaxParam = "{'legNum':" + legNum + "}";
    $.ajax({
        async: true,
        type: "POST",
        url: "PreflightTripManager.aspx/UpdateCurrentLegTripEntityState",
        data: ajaxParam,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });
}
function isDepartureOrArrivalConfirmed() {
    self.CurrentLegData.IsDepartureConfirmed(self.CurrentLegData.IsDepartureConfirmed()==null ? true : self.CurrentLegData.IsDepartureConfirmed());
    if(self.CurrentLegData.IsDepartureConfirmed()) {
        $("#lbDepartureDate").addClass("time_anchor_active").removeClass("time_anchor");
        $("#lbArrivalDate").addClass("time_anchor").removeClass("time_anchor_active");
    } else {
        $("#lbDepartureDate").addClass("time_anchor").removeClass("time_anchor_active");
        $("#lbArrivalDate").addClass("time_anchor_active").removeClass("time_anchor");
    }
}

function AddLeg_Click() {
    FormatLegDates();
    self.CurrentLegData.IsLegEdited = true;
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    var LegNum = self.CurrentLegData.LegNUM();

    ClearViewModelChildren(LegData);

    RequestStart();
    $.ajax({
        async: true,
        cache:false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/AddNewLeg",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'LegNum': LegNum, 'CurrentLeg': LegData }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                var jsonObj = result.d.Result;
                if (jsonObj != null) {
                    if (self.CurrentLegData.LegNUM == undefined) {
                        self.CurrentLegData = ko.mapping.fromJS(jsonObj.NewLeg);
                        self.Legs = ko.mapping.fromJS(jsonObj.Legs);
                        collapseItem(false);
                    } else {
                        ko.mapping.fromJS(jsonObj.NewLeg, self.CurrentLegData);
                        self.Legs.push(ko.mapping.fromJS(jsonObj.NewLeg));
                        collapseItem(false);
                    }
                    setTooltipforAirport(self.CurrentLegData.DepartureAirport.IcaoID(), self.CurrentLegData.DepartureAirport.tooltipInfo, document.getElementById("tbDepart"), "DEPT");
                    setTooltipforAirport(self.CurrentLegData.ArrivalAirport.IcaoID(), self.CurrentLegData.ArrivalAirport.tooltipInfo, document.getElementById("tbArrival"));
                    isDepartureOrArrivalConfirmed();
                    PreflightCalculation_Validate_Retrieve(true, "CrewDutyRule");
                    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                    $("#Leg" + jsonObj.NewLeg.LegNUM).removeClass("tabStripItem").addClass("tabStripItemSelected");
                }
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () {
            ResponseEnd();
            self.getTripExceptionGrid("reload");
        }
    });
}

function InsertLeg_Click() {
    FormatLegDates();
    var legNum = Number(self.CurrentLegData.LegNUM());
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    ClearViewModelChildren(LegData);

    self.getTripExceptionGrid("reload");
    setAlertTextToYesNo();
    jConfirm("Add same crew/position to inserted leg?", "Confirmation!", function (arg) {
            RequestStart();
            $.ajax({
                async: true,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/InsertNewLeg",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'CurrentLegNum': legNum, 'CurrentLeg': LegData, 'CopyCurrentLegCrew': arg }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        var jsonObj = result.d.Result;
                        if (jsonObj != null) {
                            ko.mapping.fromJS(jsonObj.NewLeg, self.CurrentLegData);
                            ko.mapping.fromJS(jsonObj.Legs, self.Legs);
                            isDepartureOrArrivalConfirmed();
                            $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                            $("#Leg" + jsonObj.NewLeg.LegNUM).removeClass("tabStripItem").addClass("tabStripItemSelected");
                            isInsertedLeg = true;
                        }
                    }
                }, error: function (jqXHR, textStatus, errorThrown) {
                    ResponseEnd();
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                },
                complete: function () { ResponseEnd(); }
            });
    });
    setAlertTextToOkCancel();
}

function DeleteLeg_Click() {
    var legNum = Number(self.CurrentLegData.LegNUM());
    RequestStart();
    $.ajax({
        async: true,
        cache:false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/DeleteLeg",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'CurrentLegNum': legNum }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult == true && result.d.Success == true) {
                var jsonObj = result.d.Result;
                if (jsonObj.Legs != undefined && jsonObj.NewLeg != undefined && jsonObj.Flag == true) {
                    RemoveAllErrorLabels();

                    ko.mapping.fromJS(jsonObj.NewLeg, self.CurrentLegData);
                    ko.mapping.fromJS(jsonObj.Legs, self.Legs);
                    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                    $("#Leg" + jsonObj.NewLeg.LegNUM).removeClass("tabStripItem").addClass("tabStripItemSelected");
                    jQuery(ShortLegSummary).jqGrid('GridUnload');
                    jQuery(DetailedLegSummary).jqGrid('GridUnload');
                    InitializeSortSummaryDetail();
                    InitializeDetailedLegSummary();
                }
            }
            self.getTripExceptionGrid("reload");
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}

function SaveCurrentLegData() {
    var legNum = self.CurrentLegData.LegNUM();
    self.CurrentLegData.IsLegEdited = true;

    
    FormatLegDates();
    isDepartureOrArrivalConfirmed();
    var LegData = ko.mapping.toJS(self.CurrentLegData);
    
    ClearViewModelChildren(LegData);

    $.ajax({
        async: false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SaveCurrentLegData",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'LegNum': legNum, 'CurrentLeg': LegData }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
        }, error: function (jqXHR, textStatus, errorThrown) {
            ResponseEnd();
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}

ko.bindingHandlers.MathRound={
    init:function(element,valueAccessor,allBindings) {
        var dataSource=valueAccessor();
        var DecimalPlace=allBindings.get("e");
        var val=+(Math.round(dataSource()+"e+"+DecimalPlace)+"e-"+DecimalPlace);
        valueAccessor(val);
    },
    update:function(element,valueAccessor,allBindings) {
        var dataSource=valueAccessor();
        var DecimalPlace=allBindings.get("e");
        var val=(+(Math.round(dataSource()+"e+"+DecimalPlace)+"e-"+DecimalPlace)).toString();
        if(val=="NaN") {
            val="0.0";
        } else {
            var i=val.toLowerCase().indexOf(".");
            if(i<0)
                val+=".0";
        }
        dataSource(val);
        ko.bindingHandlers.text.update(element,valueAccessor);
    }
};

function FormatLegDates() {
    if (self.CurrentLegData.PreflightTripOutbounds() != null && self.CurrentLegData.PreflightTripOutbounds().length > 0) {
        for (var i = 0; i < self.CurrentLegData.PreflightTripOutbounds().length; i++) {
            self.CurrentLegData.PreflightTripOutbounds()[i].LastUpdTS = ko.observable(self.CurrentLegData.PreflightTripOutbounds()[i].LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
        }
    }
    self.CurrentLegData.LastUpdTS = ko.observable(self.CurrentLegData.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.CurrentLegData.NextGMTDTTM = ko.observable(self.CurrentLegData.NextGMTDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.CurrentLegData.NextHomeDTTM = ko.observable(self.CurrentLegData.NextHomeDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.CurrentLegData.NextLocalDTTM = ko.observable(self.CurrentLegData.NextLocalDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    if (self.CurrentLegData.Passenger != undefined)
        self.CurrentLegData.Passenger.LastUpdTS = ko.observable(self.CurrentLegData.Passenger.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });

    if (self.CurrentLegData.DepartureAirport.DayLightSavingEndDT() != null) {
        self.CurrentLegData.DepartureAirport.DayLightSavingEndDT(moment(self.CurrentLegData.DepartureAirport.DayLightSavingEndDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.DepartureAirport.DayLightSavingStartDT() != null) {
        self.CurrentLegData.DepartureAirport.DayLightSavingStartDT(moment(self.CurrentLegData.DepartureAirport.DayLightSavingStartDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT() != null) {
        self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT(moment(self.CurrentLegData.ArrivalAirport.DayLightSavingEndDT()).format("MM/DD/YYYY"));
    }
    if (self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT() != null) {
        self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT(moment(self.CurrentLegData.ArrivalAirport.DayLightSavingStartDT()).format("MM/DD/YYYY"));
    }
    
    if (self.CurrentLegData.ArrivalDTTMLocal() != null) {
        self.CurrentLegData.ArrivalDTTMLocal = ko.observable(self.CurrentLegData.ArrivalDTTMLocal()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    }
    if (self.CurrentLegData.ArrivalGreenwichDTTM() != null) {
        self.CurrentLegData.ArrivalGreenwichDTTM = ko.observable(self.CurrentLegData.ArrivalGreenwichDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    }
    if (self.CurrentLegData.HomeArrivalDTTM() != null) {
        self.CurrentLegData.HomeArrivalDTTM = ko.observable(self.CurrentLegData.HomeArrivalDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    }
    
    if (self.CurrentLegData.DepartureDTTMLocal() != null) {
        self.CurrentLegData.DepartureDTTMLocal = ko.observable(self.CurrentLegData.DepartureDTTMLocal()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    }
    if (self.CurrentLegData.DepartureGreenwichDTTM() != null) {
        self.CurrentLegData.DepartureGreenwichDTTM = ko.observable(self.CurrentLegData.DepartureGreenwichDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    }
    if (self.CurrentLegData.HomeDepartureDTTM() != null) {
        self.CurrentLegData.HomeDepartureDTTM = ko.observable(self.CurrentLegData.HomeDepartureDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    }
}