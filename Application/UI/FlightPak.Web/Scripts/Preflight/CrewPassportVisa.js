﻿var CrewPassportVisa;
$(function () {
    CrewPassportVisa = function () {
        var passportVisa = null;
        passportVisa = new CrewPassportVisaViewModel(CrewID);
        return {
            passportVisa: this.passportVisa,
        };
    };
    ko.applyBindings(new CrewPassportVisa(), document.getElementById('divGridPanel'));
});
var self = this;
var CrewPassportVisaViewModel = function (CrewID) {
    if (CrewID != null && CrewID != "") {
        self.CrewID = ko.observable();
        self.EditMode = ko.observable();
        self.Leg = ko.observable();
        self.CrewName = ko.observable();
        
        self.EditMode.subscribe(function (status) {
     
            if (status == false) {
                $("#lnkInitEdit").hide();
                $("#btnSubmitButton").hide();
                $("#lbtnInitInsert").hide();
                $("#LinkButton1").hide();
                $("#btnVisaSubmit").hide();
                $("#LinkButton2").hide();
                
            }
            else
            {
                $("#lnkInitEdit").show();
                $("#lbtnInitInsert").show();
                $("#LinkButton1").show();
                $("#LinkButton2").show();
                setButtonStatus(jqgridPassportId, "#btnSubmitButton");
                //setButtonStatus(jqgridVisaId, "#btnVisaSubmit");
            }

        });

        self.CrewID.subscribe(function (id) {
            BindPassportGrid(id);
            BindVisaGrid(id);

            $.ajax({
                async: true,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/TripStatus",
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        var tStatus = result.d.Result;
                        if (tStatus)
                        { self.EditMode(true); }
                    }
                }
            });
           
             
        });

        self.btnEdit_Click = function () {
        };
        self.EditMode(false);
        self.CrewID(CrewID);
    }
}
function setButtonStatus(gridid, buttonid)
{
    var recs = parseInt($(gridid).jqGrid('getGridParam', 'reccount'));
    if (isNaN(recs) || recs == 0) {
        $(buttonid).hide();
    }
    else {
        $(buttonid).show();

    }
 }

function ReloadCrewPassportGrid() {
    $(jqgridPassportId).jqGrid().trigger('reloadGrid');
}

function BindPassportGrid(id) {
    jQuery(jqgridPassportId).jqGrid({
        datatype: "json",
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CrewPassportGrid',
        mtype: 'POST',
        cache: false,
        async: false,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            if (typeof postData.filters === "undefined") {
                postData.filters = "";
            }
            postData.crewID = id;
            return JSON.stringify(postData);
        },
        colNames: ['Passport No.', 'Choice', 'Expiration Date', 'Issuing City/Authority', 'Issue Date', 'Nationality', 'Passport ID', 'CrewID'],
        colModel: [
            { name: 'PassportNum', index: 'PassportNum', frozen: true, sortable: false, search: false },
            {
                name: 'Choice', index: 'Choice', frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == true) {
                        return '<input id="chkcompleted" disabled="disabled" type="checkbox" checked="checked" />';
                    } else { return '<input id="chkcompleted" disabled="disabled" type="checkbox"  />'; }
                }
            },
            { name: 'PassportExpiryDT', index: 'PassportExpiryDT', frozen: true, sortable: true, search: false},
            { name: 'IssueCity', index: 'IssueCity', frozen: true, sortable: false, search: false },
            { name: 'IssueDT', index: 'IssueDT', frozen: true, sortable: false, search: false},
            { name: 'CountryCD', index: 'CountryCD', frozen: true, sortable: false, search: false },
            { name: 'PassportID', index: 'PassportID', frozen: true, sortable: false, search: false, hidden: true },
            { name: 'CrewID', index: 'CrewID', frozen: true, sortable: false, search: false, hidden: true }
        ],
        scrollOffset: 0,
        ondblClickRow: function (rowId) {
            //returnToParent();
        },
        onSelectRow: function (id) {
                    
        },
        gridComplete: function () {
           
        }
    });
}

function BindVisaGrid(id)
{
    jQuery(jqgridVisaId).jqGrid({
        datatype: "json",
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CrewVisaGrid',
        mtype: 'POST',
        cache: false,
        async: false,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            if (typeof postData.filters === "undefined") {
                postData.filters = "";
            }
            postData.crewID = id;
            return JSON.stringify(postData);
        },
        colNames: ['Country', 'Visa No.', 'Expiration Date', 'Issuing City/Authority', 'Issue Date', 'Notes', 'VisaID'],
        colModel: [
            { name: 'CountryCD', index: 'CountryCD', frozen: true, sortable: false, search: false },
            { name: 'VisaNum', index: 'VisaNum', frozen: true, sortable: false, search: false },
            {
                name: 'ExpiryDT', index: 'ExpiryDT', frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                    return moment(cellvalue).format($("#hdApplicationDateFormat").val());
                }
            },
            { name: 'IssuePlace', index: 'IssuePlace', frozen: true, sortable: false, search: false },
            { name: 'IssueDate', index: 'IssueDate', frozen: true, sortable: false, search: false },
            { name: 'Notes', index: 'Notes', frozen: true, sortable: false, search: false },
            { name: 'VisaID', index: 'VisaID', frozen: true, sortable: false, search: false, hidden: true },
        ],
        scrollOffset: 0,
        ondblClickRow: function (rowId) {
        },
        onSelectRow: function (id) {
        },
        gridComplete: function () {
           
        }
    });
}

