﻿
/*** START CODE: PAX_GRID **/
/***** This file is for  Preflight Mocule, PAX Tab -> Logistics Pax Grid related to Hotel details **/

var jqGridPaxLegEntityGrid = "#PaxLegEntityGrid";
var jqGridPaxHotelListGrid = "#PaxHotelListGrid";
var jqgridIsHotelSelected = false;
var IsNoPAXSelected = false;
var titleConfirm = "Confirmation!";
var title = 'Preflight PAX';
var isAfterGetTransportCall = false;

var PropertyNamesList = "DepartCode,ArrivalCode,ArrivalConfirmation,ArrivalComments,DepartComments,DepartConfirmation,SelectedDepartTransportStatus,SelectedArrivalTransportStatus";
function reloadPaxGrid() {
    $(jqGridPaxLegEntityGrid).jqGrid('clearGridData');
    $(jqGridPaxLegEntityGrid).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');    
}

function reloadPaxGridLocally() {
    viewModel.PAXCode("None");
    $("#chkNoPAX").prop("checked", false);
    $(jqGridPaxLegEntityGrid).trigger('reloadGrid');
}

function InitializePaxLegEntityGrid() {    

        jQuery(jqGridPaxLegEntityGrid).jqGrid({
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/InitializePreflightPAXLegEntiryGrid',
            mtype: 'POST',
            datatype: "json",
            cache: false,
            async: true,
            loadonce:true,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                if (postData._search == undefined || postData._search == false) {
                    if (postData.filters === undefined) postData.filters = null;
                }
                postData.LegNUM = $("#hdnSelectedLegNum").val();
                return JSON.stringify(postData);
            },
            height: 260,
            width: 323,
            autowidth: true,
            shrinkToFit: false,            
            multiselect: true,
            colNames: ['PaxID', 'PAX Code', 'OrderNUM', 'PassengerFirstName', 'PassengerMiddleName', 'Name'],
            colModel: [
                { name: 'PassengerRequestorID', index: 'PassengerRequestorID', width: 60, hidden: true },
                 { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: 40, key: true, sortable: false },
                 { name: 'OrderNUM', index: 'OrderNUM', hidden: true },
                 { name: 'PassengerFirstName', index: 'PassengerFirstName', hidden: true },
                 { name: 'PassengerMiddleName', index: 'PassengerMiddleName', hidden: true },
                 {
                     name: 'PassengerLastName', index: 'PassengerLastName', sortable: true, width: 225, search: false, formatter: function (cellvalue, options, rowObject) {
                         return GetValidatedValue(rowObject, "PassengerLastName") + ", " + GetValidatedValue(rowObject, "PassengerFirstName") + " " +
                             GetValidatedValue(rowObject, "PassengerMiddleName");
                     }
                 }
            ],
            beforeSelectRow: function(rowid, e)
            {
                if (($("#hdnHotelMode").val() == "true" || self.EnablePaxHotelMode() == true ) && !self.viewModel.NoPAX()) {                    
                        return true;
                } else {
                    return false;
                }
            },
            onSelectRow: function (id, status) {
                
                if (self.EnablePaxHotelMode() == true) {
                    if (IsNoPAXSelected) {
                        $(jqGridPaxLegEntityGrid).setSelection(id, false);
                    }

                    var rowData = $(this).getRowData(id);
                    var selectedRows = $(jqGridPaxLegEntityGrid).jqGrid('getGridParam', 'selarrrow');
                    if (IsNullOrEmpty(selectedRows))
                        selectedRows = "";

                    var totalRec = $(jqGridPaxLegEntityGrid).jqGrid('getGridParam', 'records');

                    if (selectedRows.length == totalRec) {
                        viewModel.PAXCode("All");
                        viewModel.NoPAX(false);
                    } else {
                        viewModel.PAXCode(selectedRows.toString());
                    }
                } else {                    
                    $(jqGridPaxLegEntityGrid).setSelection(id, status);
                }
            },
            onSelectAll: function (ids, status) {
                if (self.EnablePaxHotelMode() == true) {
                    ids = $(jqGridPaxLegEntityGrid).jqGrid('getDataIDs');
                    if (status && IsNoPAXSelected) {
                        for (var i = 0; i <= ids.length; i++) {
                            $(jqGridPaxLegEntityGrid).setSelection(ids[i], false);
                        }
                        viewModel.PAXCode("None");
                    }
                    if (status && !IsNoPAXSelected) {
                        viewModel.PAXCode("All");
                        viewModel.NoPAX(false);
                    }
                    else
                        viewModel.PAXCode("");
                }
            },
            loadComplete: function () {
                
                setPaxAsperHotelSelection();
                var gridid = jqGridPaxLegEntityGrid.substr(1);                
                $("#jqgh_" + gridid + "_cb").find("span").remove();
                $("#jqgh_" + gridid + "_cb").find("br").remove();
                

                $("#jqgh_" + gridid + "_cb").prepend("<span>Select All</span><br/>");
                $("#jqgh_" + gridid + "_cb").css("height", "32px");
                $(jqGridPaxLegEntityGrid + "_cb").css("width", "40px");
                $(jqGridPaxLegEntityGrid + "_cb").css("font-size", "10px");
                $(jqGridPaxLegEntityGrid + " tbody tr").children().first("td").css("width", "40px");

                $(jqGridPaxLegEntityGrid + " input[type=checkbox]").click(function () {
                    if (($("#hdnHotelMode").val() == "true" || self.EnablePaxHotelMode() == true) && !self.viewModel.NoPAX()) {
                        return true;
                    } else {
                        return false;
                    }
                });


            },
            gridComplete: function ()
            {
                if (self.EnablePaxHotelMode() == false) {
                    $("#gbox_PaxLegEntityGrid input[type=checkbox]").attr("disabled", "disabled");
                    $("#gbox_PaxLegEntityGrid input[type=checkbox]").attr("disabled", "disabled");
                }
                //self.EnablePaxHotelMode(false);
            }
        });
        
}

var RoomDescription = function (title) {
    this.RoomDescr = title;    
};
/***** END CODE :PAX_GRID **/

/****START: Knockout Binding *****/
var self = this;

var PaxViewModel = function () {
    
    self.viewModel = {};
    self.Rooms = ko.observable();
    self.RoomDescription = ko.observable();
    self.PaxInfo = {};  // @Mahmoud  you can get your viewmodel data in this object.
    self.PaxHotelSaveIcon = ko.pureComputed(function () {
        return this.EditMode() && self.EnablePaxHotelMode() ? "ui_nav" : "ui_nav_disable";
    }, self);
    self.IsPaxInfoWarningConfirm=ko.observable(true);

    self.BrowseBtnPaxHotel = ko.pureComputed(function () {
        return this.EditMode() && self.EnablePaxHotelMode() ? "browse-button" : "browse-button-disabled";
    }, self);
    self.Legs = {};
    self.Transport = {};
    self.PaxTransportCurrentLeg = ko.observable("1");
    self.PaxHotelCurrentLeg = ko.observable("1");
    self.PaxHotelrecordEditClick = function () {        
        
        document.getElementById("lbcvHotelCode").innerHTML = "";
        if (parseInt($(jqGridPaxHotelListGrid).getGridParam("reccount")) < 1) {
            jAlert("No hotels found for edition. Please add hotel!", title, null);
        } else {
            self.IsPaxHotelEdit(true);
            self.EnablePaxHotelMode(true);
            self.viewModel.HotelCodeEdit(self.viewModel.HotelCode());
        }
    };
    self.PaxHotelrecordAddClick = function () {
        document.getElementById("lbcvHotelCode").innerHTML = "";
        viewModel.PAXCode("");        
        reloadPaxGridLocally();
        self.viewModel.NoPAX(false);
        viewModel.Status("Select");
        
        $(jqGridPaxHotelListGrid).jqGrid('resetSelection');
        self.EnablePaxHotelMode(true);        
        ResetHotelControls();        
        self.viewModel.HotelCodeEdit("");
        self.IsPaxHotelEdit(false);
        if (!IsNullOrEmpty(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].ArrivalLocalDate())) {
            viewModel.ClientDateIn(moment(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].ArrivalLocalDate(), self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format("YYYY-MM-DD"));
        } else {
            viewModel.ClientDateIn("");
        }

        if (self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg())] && !IsNullOrEmpty(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg())].DepartureLocalDate())) {
            viewModel.ClientDateOut(moment(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg())].DepartureLocalDate(), self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format("YYYY-MM-DD"));
        } else {
            viewModel.ClientDateOut("");
        }


        self.viewModel.isArrivalHotel(true);
        self.viewModel.isDepartureHotel(false);
    };
    self.PaxHotelrecordDeleteClick = function () {
        if (parseInt($(jqGridPaxHotelListGrid).getGridParam("reccount")) < 1) {
            jAlert("No hotels found for deletion. Please add hotel!", title, null);
            return;
        }
        if (self.EnablePaxHotelMode() == false) {
            //delete from list     
            setAlertTextToYesNo();
            jConfirm("Are you sure you want to delete this record?", titleConfirm, function (r) {
                if (r) {                    
                    DeleteHotelFromLeg();
                    ResetHotelControls();
                    btnCancelHotelClick();
                }
            });
            setAlertTextToOkCancel();
        }
       
    };
    self.valueChangeCall = function () { isAfterGetTransportCall = false; };
    self.DepartTransportCodeChange = function () { isAfterGetTransportCall = false; fireOnChangeTransCode(); };
    self.ArrivalTransportCodeChange = function () { isAfterGetTransportCall = false; fireOnChangeArriveTransCode(); };
    self.LegtabClick = function (legItem, event) {
        
        if ($(event.currentTarget).parent().attr("id") == "legTabhotel") {

            if (self.EnablePaxHotelMode() == true) {
                jAlert(alertMessageHotelSave, "Preflight");
                return false;
            }

            $("#legTabhotel").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
            $(event.currentTarget).addClass("tabStripItemSelected");


            self.PaxHotelCurrentLeg(legItem.LegNUM());
            self.PaxTransportCurrentLeg(legItem.LegNUM());

            $("#hdnSelectedLegNum").val(legItem.LegNUM());
            reloadPaxGridLocally();            
            self.viewModel.ClientDateIn(null);
            self.viewModel.ClientDateOut(null);
            ResetHotelControls();            
            self.IsPaxHotelEdit(false);
            reloadPaxHotelListGrid();
            reloadPaxGrid();
            viewModel.LegNUM(legItem.LegNUM());
            
        } else {
            document.getElementById("lbcvTransCode").innerHTML = "";
            document.getElementById("lbcvArriveTransCode").innerHTML = "";
            
            $("#legTabTransport").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
            $(event.currentTarget).addClass("tabStripItemSelected");

            self.PaxHotelCurrentLeg(legItem.LegNUM());
            self.PaxTransportCurrentLeg(legItem.LegNUM());

            ResetTransportControls();
            $("#hdnSelectedTransportLegNum").val(legItem.LegNUM());
            GetTransportDetails(legItem.LegNUM());
            viewModel.LegNUM(legItem.LegNUM());


        }
    };
    self.EnablePaxHotelMode = ko.observable(false);
    self.EnablePaxHotelMode.subscribe(function (newValue) {
        if(newValue)
            $("#gbox_PaxLegEntityGrid input[type=checkbox]").removeAttr("disabled");
        else
            $("#gbox_PaxLegEntityGrid input[type=checkbox]").attr("disabled", "disabled");

    });

    self.HotelCodeChange = function () {
        fireOnChange();
    };
    self.countryChange=function() {
        fireOnChangeCountry();
    };
    self.metroChange=function() {
        fireOnChangeMetro();
    };

    self.EnablePaxTransport = ko.computed(function () {
        return self.EditMode() && !self.EnablePaxHotelMode();
    }, this);

    var jsonSchema = JSON.parse(document.getElementById("hdnPreflightPaxInitializationObject").value);
    self.viewModel = ko.mapping.fromJS(jsonSchema.Hotel);
    self.Transport = ko.mapping.fromJS(jsonSchema.Transport);

    self.IsPaxHotelEdit = ko.observable(false);
    
    ko.watch(self.Transport, { depth: 1, tagFields: true }, function (parents, child, item) {
        
        if (jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 && ( !IsNullOrEmpty(self.Transport.DepartCode()) || !IsNullOrEmpty(self.Transport.ArrivalCode()))) {
            setTimeout(function () {
                if (!isAfterGetTransportCall  && self.EditMode()==true) {
                    SaveTransportDetails(self.PaxTransportCurrentLeg());
                }
            }, 500);            
        }
    });
    // Calling below method for Pax Logistics and Transport related binding properies.
    InitilizeViewMInitilizePaxLogisticsViewModels();
    
};

function ResetTransportControls()
{
    var jsonSchema = JSON.parse(document.getElementById("hdnPreflightPaxInitializationObject").value);
    ko.mapping.fromJS(jsonSchema.Transport,self.Transport);
}

function InitilizeViewMInitilizePaxLogisticsViewModels() {
    GetLegsList();
    $("#hdnSelectedLegNum").val(currentPreflightLeg);
    // Initilize Hotel Details
    GetHotelDetailByLegNumber(currentPreflightLeg);
    // Fill Trip Grid
    InitializeDetailedLegSummary();
    InitializeSortSummaryDetail();

    self.PaxTransportCurrentLeg(currentPreflightLeg);
    self.PaxHotelCurrentLeg(currentPreflightLeg);
    
    InitializePaxLegEntityGrid(); 
    InitializePaxHotelListGrid($("#hdnSelectedLegNum").val());
    
    self.viewModel.isArrivalHotel.extend({ notify: 'always' });
    self.viewModel.isArrivalHotel.subscribe(function (newValue) {

        if (newValue == true) {
            self.viewModel.isDepartureHotel(false);            
        }
        else {
            self.viewModel.isDepartureHotel(true);            
        }
        if (self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg())] != undefined && !IsNullOrEmpty(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg())].DepartureLocalDate()))
            viewModel.ClientDateOut(moment(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg())].DepartureLocalDate(), self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format("YYYY-MM-DD"));
        else
            viewModel.ClientDateOut(null);
        
        if (!IsNullOrEmptyOrUndefined(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].ArrivalLocalDate()))
            viewModel.ClientDateIn(moment(self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].ArrivalLocalDate(), self.UserPrincipal._ApplicationDateFormat.toUpperCase()).format("YYYY-MM-DD"));
        else
            viewModel.ClientDateIn("");
    });
    self.IsPaxHotelEdit.extend({ notify: 'always' });
    self.IsPaxHotelEdit.subscribe(function (newValue) {
        if (newValue == false) {
            self.viewModel.HotelCodeEdit("");
        }
    });

    setTimeout(function() {
        $("#legTabhotel").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $("#Leg" + currentPreflightLeg).removeClass("tabStripItem").addClass("tabStripItemSelected");

        $("#legTabTransport").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $("#Leg" + currentPreflightLeg).removeClass("tabStripItem").addClass("tabStripItemSelected");
    }, 300);
}


function GetHotelDetailByLegNumber(legNum,isAsync) {
    if (legNum == undefined) {
        legNum = 1;
    }
    if (isAsync == undefined)
        isAsync = true;

    var paramters = {};
    paramters.LegNum = legNum;
    paramters.hotelCode = $("#hdnSelectedHotelID").val();
    if (self.viewModel.HotelCodeEdit!=undefined)
        self.viewModel.HotelCodeEdit("");

    $.ajax({
        async: isAsync,
        type: "POST",
        url: "PreflightTripManager.aspx/GetPreflightLegPaxHotelByLegNumHotelCode",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (self.viewModel.HotelCode == undefined)
                    self.viewModel = ko.mapping.fromJS(response.d.Result);
                else
                    ko.mapping.fromJS(response.d.Result, self.viewModel);

                if (self.viewModel.PAXCode() == "None" || viewModel.NoPAX() == true || self.viewModel.PAXCode() == "") {
                    $("#chkNoPAX").prop("checked", true);
                } else
                    $("#chkNoPAX").prop("checked", false);
                RetriveRoomType();
                RetriveRoomDescription();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}


function saveHotelToTripSession() {

    
    
    var lbcvHotelCode = $('#lbcvHotelCode').html();
    var tbHotelName = self.viewModel.PreflightHotelName();
    var hdnHotelID = self.viewModel.HotelID();
    
    var tbCtry = self.viewModel.CountryCode;
    var hdnCtryID = self.viewModel.CountryID;
    
    var tbMetro = self.viewModel.Metro;
    var MetroID = self.viewModel.MetroID;
    
    if (!IsNullOrEmpty(lbcvHotelCode)) {
        jAlert(lbcvHotelCode,title, null);
        return;
    }
    else if (IsNullOrEmpty(tbHotelName)) {
        jAlert("Hotel Code should not be Empty!",title, null);
        return;
    }
    else if (IsNullOrEmpty(tbHotelName) && IsNullOrEmpty(hdnHotelID)) {
        jAlert("Hotel Code should not be Empty!", title, null);
        return;
    }
    else {
        if (IsNullOrEmpty(hdnCtryID()) && !IsNullOrEmpty(tbCtry())) {
            tbCtry("");
            hdnCtryID(null);
        }
        if (IsNullOrEmpty(MetroID()) && !IsNullOrEmpty(tbMetro())) {
            tbMetro("");
            MetroID(null);
        }
        $("#lbcvCtry").html("");
        $("#lbcvMetro").html("");
        //Hotel Save code
        self.EnablePaxHotelMode(false);
        viewModel.LegNUM(self.PaxHotelCurrentLeg());
        var paraObject = ko.toJS(viewModel);
        if (paraObject.ClientDateIn != null && paraObject.ClientDateIn != "")
            paraObject.DateIn = moment(paraObject.ClientDateIn).format(isodateformat);
        else
            paraObject.DateIn = null;
        if (paraObject.ClientDateOut != null && paraObject.ClientDateOut != "")
            paraObject.DateOut = moment(paraObject.ClientDateOut).format(isodateformat);
        else
            paraObject.DateOut = null;
        paraObject=removeUnwantedPropertiesKOViewModel(paraObject);
        

        var param = "{'HotelDetail':" + JSON.stringify(paraObject) + "}";
        $.ajax({
            type: "POST",
            url: "PreflightTripManager.aspx/SavePreflightLegPaxHotelByLegNum",
            data: param,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    var result = response.d.Result;
                    if (result == "1") {
                        self.EnablePaxHotelMode(false);
                        reloadPaxGridLocally();
                        //changePaxHotelControlMode(false);
                        $("#hdnSelectedHotelID").val(viewModel.HotelCode());
                        reloadPaxHotelListGrid();
                        GetHotelDetailByLegNumber(viewModel.LegNUM());
                        $(jqGridPaxHotelListGrid).jqGrid("setSelection", viewModel.HotelCode());
                    }
                    self.IsPaxHotelEdit(false);
                    self.viewModel.HotelCodeEdit("");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                self.viewModel.HotelCodeEdit("");
                reportPreflightError(jqXHR, textStatus, errorThrown);
                var jsonSchema = JSON.parse(document.getElementById("hdnPreflightPaxInitializationObject").value);
                self.viewModel = ko.mapping.fromJS(jsonSchema.Hotel);
            }
        });
        
    }
}

function RetriveRoomType() {
    $.ajax({
        type: "POST",
        url: "PreflightTripManager.aspx/RetrieveRoomType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                self.Rooms(response.d.Result);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function RetriveRoomDescription() {
    $.ajax({
        type: "POST",
        url: "PreflightTripManager.aspx/RetrieveRoomDescription",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                self.RoomDescription(response.d.Result);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}
function GetTransportDetails(legNum,isAsync) {
    if (legNum == undefined) {
        legNum = 1;
    }
    if (isAsync == undefined)
        isAsync = true;
    
    RequestStart();

    $.ajax({
        async: true,
        type: "POST",
        url: "PreflightTripManager.aspx/GetPassengerLegTransportViewModelByLegNum",
        data: '{LegNum: "' + legNum + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (self.Transport.DepartureAirportId == undefined) {
                    self.Transport = ko.mapping.fromJS(response.d.Result);
                }
                else {
                    ko.mapping.fromJS(response.d.Result, self.Transport);
                }
                isAfterGetTransportCall = true;
                ResponseEnd();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function paxTabMainModulesChange(ctrl)
{
    if (self.EnablePaxHotelMode() == true) {
        jAlert(alertMessageHotelSave, "Preflight");
        return false;
    }
    
    if ($(ctrl).attr("id") != "ulPaxInfo") {
        warningInPaxInfoSelection(function () {
            $(ctrl).parent().find("li").removeClass("tabStripItemSelected");
            $(ctrl).addClass("tabStripItemSelected");

            $("#pnlPaxLogistics").css("display", "block");
            $("#pnlPax").css("display", "none");
            RequestStart();
            ResponseEnd();
            reloadPaxGrid();

            //Calendar context menu click
            var subTabNameHotel = getQuerystring("subTab", "PreFlight");
            var subTabNameTran = getQuerystring("legHotel", "PreFlight");
            var reqLegNum = getQuerystring("legNo", "1");
            if (subTabNameHotel == "1") {
                if (subTabNameTran != "1") {
                    setTimeout(function () { $("#legTabhotel li:eq(" + (reqLegNum - 1) + ")").click(); }, 300);
                }
                else {
                    paxLogisticsModuleChange('2');
                    setTimeout(function () { $("#legTabTransport li:eq(" + (reqLegNum - 1) + ")").click(); }, 100);
                }
            }

        });        
    } else
    {
        $(ctrl).parent().find("li").removeClass("tabStripItemSelected");
        $(ctrl).addClass("tabStripItemSelected");

        $("#pnlPax").css("display", "block");
        $("#pnlPaxLogistics").css("display", "none");
    }

    if ($("#pnlPaxLogistics").css("display") == "block") {        
        reloadPaxGridLocally();
        ResetHotelControls();
        self.IsPaxHotelEdit(false);
        reloadPaxHotelListGrid();
    }
  
}
function paxLogisticsModuleChange(moduleId) {
    
    if (moduleId == 1) {        
        $("#radPgeViewTransport").hide();
        $("#rdPgeViewHotel").show();
        $("#ulTransportation").removeClass("tabSelected");
        $("#ulHotel").addClass("tabSelected");
        $("#legTabhotel li:eq(" + ( parseInt(self.PaxHotelCurrentLeg()) - 1) + ")").click();

    } else {
        if (self.EnablePaxHotelMode() == true) {
            jAlert(alertMessageHotelSave, "Preflight");
            return false;
        }
        $("#radPgeViewTransport").show();
        $("#rdPgeViewHotel").hide();
        $("#ulTransportation").addClass("tabSelected");
        $("#ulHotel").removeClass("tabSelected");

        $("#legTabTransport li:eq(" + (parseInt(self.PaxTransportCurrentLeg()) - 1) + ")").click();
    }
}
/****END: Knockout Binding *****/




/********START: Pax Hotel Grid  *************/

function InitializePaxHotelListGrid(legNum) {
    $.extend(jQuery.jgrid.defaults, {
        prmNames: {
            page: "page", rows: "size", order: "dir", sort: "sort"
        }
    });

    jQuery(jqGridPaxHotelListGrid).jqGrid({
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/InitializePreflightPAXHotelListGrid',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: true,
        loadonce:true,
        multiselectWidth: 100,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }

            postData.LegNum = self.PaxHotelCurrentLeg();
            return JSON.stringify(postData);
        },
        height: "auto",
        width: 716,
        autowidth: false,
        shrinkToFit: false,
        multiselect: false,
        pgbuttons: false,
        pginput: false,
        sortname: 'HotelID',
        sortorder: "desc",
        colNames: ['HotelID','Hotel Code', 'Hotel Name', 'Arr.<br/>/Dep.', 'PAX Code', 'Address', 'City', 'State<br/>/Province', 'Country', 'Phone No.', 'LegNUM'],
        colModel: [
            { name: 'HotelID', index: 'HotelID', hidden: true, sortable: true },
             { name: 'HotelCode', index: 'HotelCode', width: 50, key: true,sorttype:"int" },
             { name: 'PreflightHotelName', index: 'PreflightHotelName', width: 130,  sortable: true },
             { name: 'isArrivalHotel', index: 'isArrivalHotel', width: 40, search: false,sortable:false, formatter: arrivalDepartureFmatter },
             { name: 'PAXCode', index: 'PAXCode', width: 50, search: false, sortable: true },
             { name: 'Address1', index: 'Address1', width: 120, sortable: true, search: false },
             { name: 'CityName', index: 'CityName', width: 70, sortable: true, search: false },
             { name: 'StateProvince', index: 'StateProvince', width: 70, sortable: true, search: false },
             { name: 'CountryCode', index: 'CountryCode', width: 70, sortable: true, search: false },
             { name: 'PhoneNum1', index: 'PhoneNum1', width: 71, sortable: true, search: false },
             { name: 'LegNUM', index: 'LegNUM', hidden: true }
        ],
        onSelectRow: function (id, status) {         
            
            if (status == true || $(jqGridPaxHotelListGrid).getGridParam("reccount") == 1) {
                
                var rowData = $(this).getRowData(id);
                $("#hdnSelectedHotelID").val(rowData['HotelCode']); 
                GetHotelDetailByLegNumber(rowData['LegNUM'],false);                
                
                $("#hdnHotelMode").val("true");

                
                if (viewModel.PAXCode != null && viewModel.PAXCode()!=null && viewModel.PAXCode().length >= 0) {
                    var i;
                    var ids = $(jqGridPaxLegEntityGrid).jqGrid('getDataIDs');
                    
                    var selectedRows = $(jqGridPaxLegEntityGrid).jqGrid('getGridParam', 'selarrrow');
                    if (IsNullOrEmpty(selectedRows))
                        selectedRows = "";

                    while (selectedRows.length > 0) {
                        $(jqGridPaxLegEntityGrid).jqGrid('setSelection', selectedRows[0], false);
                    }

                    if (viewModel.PAXCode() == "All") {
                        for (i = 0; i < ids.length; i++) {                            
                            $(jqGridPaxLegEntityGrid).jqGrid('setSelection', ids[i],true);
                        }
                    }
                    else if (viewModel.PAXCode()=="" || viewModel.PAXCode() == "None") {
                        viewModel.NoPAX(true);
                    }
                    else {
                        var rows = viewModel.PAXCode().split(',');                        
                        $.each(rows, function (index, value) {
                            $(jqGridPaxLegEntityGrid).jqGrid('setSelection', value, true);
                        });
                    }
                }
                $("#hdnHotelMode").val("false");
            }
        },
        loadComplete: function () {
            if (IsNullOrEmpty(viewModel.HotelCode())) {
                    var top_rowid = $(jqGridPaxHotelListGrid).getDataIDs()[0];
                    $(jqGridPaxHotelListGrid).jqGrid("setSelection", top_rowid);                    
            }else
                $(jqGridPaxHotelListGrid).jqGrid("setSelection", viewModel.HotelCode());
        }
    });

}

function setPaxAsperHotelSelection() {
    if (viewModel.PAXCode() != undefined) {
        var ids = $(jqGridPaxLegEntityGrid).jqGrid('getDataIDs');

        if (viewModel.PAXCode() == "All") {
            for (i = 0; i < ids.length; i++) {
                $(jqGridPaxLegEntityGrid).jqGrid('setSelection', ids[i], true);
            }
        }
        else if (viewModel.PAXCode() == "None") {
            viewModel.NoPAX(true);
        }
        else {
            var rows = viewModel.PAXCode().split(',');
            $.each(rows, function (index, value) {
                $(jqGridPaxLegEntityGrid).jqGrid('setSelection', value, true);
            });
        }
    }
}

function arrivalDepartureFmatter(cellvalue, options, rowObject) {    
    if (cellvalue == false)
        return "D";
    else
        return "A";
}

function DeleteHotelFromLeg() {
    //(int legNum,string hotelCode,string isArrival)
    var params = {};
    params.legNum = $("#hdnSelectedLegNum").val();
    params.hotelCode = $("#hdnSelectedHotelID").val();;
    params.isArrival = viewModel.isArrivalHotel();

    $.ajax({
        async: false,
        type: "POST",
        url: "PreflightTripManager.aspx/DeletePreflightLegPaxHotelByLegNum",
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                reloadPaxHotelListGrid();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function reloadHotelGridLocally()
{
    $(jqGridPaxHotelListGrid).trigger('reloadGrid');
    var top_rowid = $(jqGridPaxHotelListGrid).getDataIDs()[0];
    $(jqGridPaxHotelListGrid).jqGrid("setSelection", top_rowid);
}
function reloadPaxHotelListGrid() {
    $(jqGridPaxHotelListGrid).jqGrid('clearGridData');    
    $(jqGridPaxHotelListGrid).setGridParam({ datatype: 'json', page: 1 }).trigger('reloadGrid');
}

function changePaxHotelControlMode(mode) {
    self.EnablePaxHotelMode(mode);
    reloadPaxGridLocally();

}


function SaveTransportDetails(legNum)
{
    if (legNum == undefined)
        legNum = self.PaxTransportCurrentLeg();
    
    Transport.ArrivalAirportId(self.Legs()[self.PaxTransportCurrentLeg() - 1].ArrivalAirport.AirportID());
    Transport.DepartureAirportId(self.Legs()[self.PaxTransportCurrentLeg() - 1].DepartureAirport.AirportID());
    
    var paraDeparture = ko.toJS(self.Transport);
    paraDeparture = removeUnwantedPropertiesKOViewModel(paraDeparture);

    var ajaxParam = "{'legNum':" + legNum + ",'paxTransport':" + JSON.stringify(paraDeparture) + "}";

    $.ajax({
        async: true,
        type: "POST",
        url: "PreflightTripManager.aspx/SavePaxLogisticsTransport",
        data: ajaxParam,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });
}

/**********END-PAX Hotel Grid *****************/
