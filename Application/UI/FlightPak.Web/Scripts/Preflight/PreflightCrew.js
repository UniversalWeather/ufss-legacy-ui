﻿/******** Javascript Model & jqgrid Code for Logistics *******/
var lastSelHotel;
var lastSelLegNum;
var jqGridCrewHotelListGrid = "#CrewHotelListGrid";
var jqGridCrewLegHotelSelectionGrid = "#dgCrewLegHotelSelectionGrid";
var IsNoCrewSelected = false;
var title = "Preflight Crew";
var titleConfirm = "Confirmation!";
var noCrewChecked = false;
var isAfterGetTransportCall = false;

var PropertyNamesList = "DepartCode,DepartName,ArrivalCode,ArrivalName,ArrivalConfirmation,ArrivalComments,DepartComments,DepartConfirmation,SelectedDepartTransportStatus,SelectedArrivalTransportStatus";

$(function () {
    $("#chkNoCrew").change(function () {
        noCrewChecked = $(this).is(':checked');
        if(noCrewChecked == true)
        {
            jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('resetSelection');
            $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).hide();
            showSelectAll(false);
        }
        else
        {
            $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).show();
            showSelectAll(true);
            if (self.CrewHotelEditMode() == true)
                $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).removeAttr('disabled');
            else
                $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).attr("disabled", true);
        }
    });

    $("#SearchBoxCrew").keypress(function (e) {
        if (e.keyCode == 13) {
            CrewCodeValidate();
            return false;
        }
    });

    $("#SearchBoxCrew").blur(function () {
        CrewCodeValidate();
        return false;
    });

});


var CrewViewModel = function () {
    self.CrewsList = {};
    self.Legs = {};
    self.BrowseBtnCrewHotel = ko.pureComputed(function () {
        return this.EditMode() && self.EnableHotel() ? "browse-button" : "browse-button-disabled";
    }, self);

    self.CrewHotelEditMode = ko.observable(false);
    self.CrewHotelRooms = ko.observable();
    self.RoomDescription = ko.observable();
    self.CurrentCrewHotel = {};
    self.CurrentCrewHotelLegNum = ko.observable(1);
    self.CrewHotelDepartureAirportId = ko.observable();
    self.CrewHotelArrivalAirportId = ko.observable();
    self.IsCrewAssignedConfirm = ko.observable(false);
    self.EnableHotel = ko.computed(function () {
        return self.EditMode() && self.CrewHotelEditMode();
    }, this);
    self.EnableCrewTransport = ko.computed(function () {
        return self.EditMode() && !self.CrewHotelEditMode();
    }, this);
    self.CrewHotelLegtabClick = function (legItem, event) {

        if (self.EnableHotel() == true) {
            jAlert(alertMessageHotelSave, "Preflight");
            return false;
        }
        $("#legTabhotel").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");        
        $(event.currentTarget).addClass("tabStripItemSelected");

        self.CurrentCrewHotelLegNum(legItem.LegNUM());
        self.CurrentCrewTransportLegNum(legItem.LegNUM());

        self.CrewHotelDepartureAirportId(legItem.DepartureAirport.AirportID());
        self.CrewHotelArrivalAirportId(legItem.ArrivalAirport.AirportID());
        LoadPreflightLegCrewHotelSelectedCrew(legItem.LegNUM());
        InitializeLegCrewViewModels(legItem.LegNUM());
        modifySelectionPreflightLegCrewHotelSelectedCrewViewModelEntryGrid();
        $("#lbcvHotelCode").html("");
        $("#lbcvCtry").text("");
        $("#lbcvMetro").text("");
        self.CrewHotelEditMode(false);
        self.CurrentCrewHotel.HotelCodeEdit(null);
    };
    self.CrewHotelAddClick = function () {
        self.CurrentCrewHotel.HotelCodeEdit(null);
        self.CrewHotelEditMode(true);
        self.ResetCurrentCrewHotel();
        $("#chkNoCrew").prop("checked", false);
        $("#chkNoCrew").trigger('change');
        noCrewChecked = false;
        jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('resetSelection');
        self.CurrentCrewHotel.LegNUM(self.CurrentCrewHotelLegNum());        
        addNewHotel(self.CurrentCrewHotelLegNum(), true);
        
    };

    self.CrewHotelEditClick = function () {
        var selectedRowId = jQuery(jqGridCrewHotelListGrid).jqGrid('getGridParam', 'selrow');
        if (!IsNullOrEmptyOrUndefined(selectedRowId)) {
            var hotelCode = jQuery(jqGridCrewHotelListGrid).jqGrid('getCell', selectedRowId, 'HotelCode');            
            self.CrewHotelEditMode(true);
            self.CurrentCrewHotel.LegNUM(self.CurrentCrewHotelLegNum());
            GetPreflightCrewHotelDetailsByLegNum(self.CurrentCrewHotelLegNum(), hotelCode);
            refreshNoCrew();
            self.CurrentCrewHotel.HotelCodeEdit(hotelCode);
        }
        else {
            jAlert("Please select a hotel to edit.", title);
        }
    };
    self.CrewHotelDeleteClick = function () {
        var selectedRowId = jQuery(jqGridCrewHotelListGrid).jqGrid('getGridParam', 'selrow');
        if (!IsNullOrEmptyOrUndefined(selectedRowId)) {
            setAlertTextToYesNo();
            jConfirm("Are you sure you want to delete this record?", titleConfirm, function (r) {
                if (r) {
                    self.CurrentCrewHotel.LegNUM(self.CurrentCrewHotelLegNum());
                    var rowData = jQuery(jqGridCrewHotelListGrid).jqGrid('getRowData', selectedRowId);
                    var preflightHotelListId = rowData['PreflightHotelListID'];
                    DeleteCrewHotel(self.CurrentCrewHotelLegNum(), self.CurrentCrewHotel.HotelCode(), preflightHotelListId);
                }
            });
            setAlertTextToOkCancel();
        }
        else {
            jAlert("Please select a hotel to delete.", title);
        }
    };

    self.CurrentCrewTransport = {};
    self.CurrentCrewTransportLegNum = ko.observable(1);
    self.CrewTransportLegtabClick = function (legItem, event) {

        $("#legTabTransport").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        
        $(event.currentTarget).addClass("tabStripItemSelected");
        self.CurrentCrewTransportLegNum(legItem.LegNUM());
        self.CurrentCrewHotelLegNum(legItem.LegNUM());

        GetPreflightCrewTransportDetailsByLegNum(legItem.LegNUM());        
        
        $("#lbcvTransCode").text("");
        $("#lbcvArriveTransCode").text("");
    };

    self.ResetCurrentCrewHotel = function (blacklist) {
        resetKOViewModel(self.CurrentCrewHotel, blacklist);
    },
    
    
    self.EnableHotel.subscribe(function (newValue) {
        if (newValue) {
            $("tr.jqgrow > td > input.cbox", jQuery(jqGridCrewLegHotelSelectionGrid)).removeAttr("disabled");
            $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).show();
        }
        else {
            $("tr.jqgrow > td > input.cbox", jQuery(jqGridCrewLegHotelSelectionGrid)).attr("disabled", "disabled");
            $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).hide();
        }

    });

    GetLegsList();
    self.CrewHotelDepartureAirportId(self.Legs()[0].DepartureAirport.AirportID());
    self.CrewHotelArrivalAirportId(self.Legs()[0].ArrivalAirport.AirportID());
    InitializeCrewObjectsVM();
    InitializeCrewViewModels();
    RetriveCrewHotelRoomTypes();
    RetriveRoomDescription();
    
    self.getTripExceptionGrid("reload");
};


function InitializeCrewViewModels() {
    GetLegsList();
    self.CurrentCrewHotelLegNum(currentPreflightLeg);
    self.CurrentCrewTransportLegNum(currentPreflightLeg);
    
    self.CrewHotelDepartureAirportId(self.Legs()[0].DepartureAirport.AirportID());
    self.CrewHotelArrivalAirportId(self.Legs()[0].ArrivalAirport.AirportID());
    InitializeLegCrewViewModels(self.CurrentCrewHotelLegNum());
    
    InitializeSortSummaryDetail();
    InitializeDetailedLegSummary();

    $("#legTabhotel").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $("#Leg" + currentPreflightLeg).removeClass("tabStripItem").addClass("tabStripItemSelected");
    
    $("#legTabTransport").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $("#Leg" + currentPreflightLeg).removeClass("tabStripItem").addClass("tabStripItemSelected");
    
}

function InitializeLegCrewViewModels(legNumber)
{
    RefreshCrewHotelGrid(legNumber);
    self.CurrentCrewHotelLegNum(legNumber);
    GetPreflightCrewHotelDetailsByLegNum(legNumber);
}

function GetPreflightCrewHotelDetailsByLegNum(legNum, hotelCode) {
    if (IsNullOrEmptyOrUndefined(legNum)) {
        legNum = 1;
    }
    var paramters = {};
    paramters.LegNum = legNum;
    if (IsNullOrEmptyOrUndefined(hotelCode))
        paramters.HotelCode = '';
    else
        paramters.HotelCode = hotelCode;

    $.ajax({
        async: false,
        cache: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetPreflightLegCrewHotelByLegNumHotelCode",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (self.CurrentCrewHotel.HotelCode == undefined) {
                    self.CurrentCrewHotel = ko.mapping.fromJS(response.d.Result);
                }
                else {
                    ko.mapping.fromJS(response.d.Result, self.CurrentCrewHotel);
                }
                lastSelLegNum = legNum;
                setTimeout(function () {
                    modifySelectionPreflightLegCrewHotelSelectedCrewViewModelEntryGrid();
                }, 300);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function RefreshCrewHotelGrid(legNum, selHotelCode,force) {
    if (IsNullOrEmptyOrUndefined(legNum)) {
        legNum = 1;
    }
    if (IsNullOrEmptyOrUndefined(force) == true || force == false) {
        if (lastSelLegNum == legNum) {
            return;
        }
    }
    jQuery(jqGridCrewHotelListGrid).jqGrid('GridUnload');
    jQuery(jqGridCrewHotelListGrid).jqGrid({
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/RetrievePreflightCrewHotelListGrid',
        mtype: 'POST',
        datatype: "json",
        async: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            postData.LegNum = legNum;
            return JSON.stringify(postData);
        },
        height: "auto",
        width: 716,
        loadonce: true,
        autowidth: false,
        shrinkToFit: false,
        multiselect: false,
        pgbuttons: false,
        pginput: false,
        colNames: ['LegNUM','Hotel<br />Code', 'Hotel Name', 'Arr./<br />Dep.', 'Crew Code', 'Address', 'City', 'State/<br/>Province', 'Country', 'Phone No.', 'HotelID', 'PreflightHotelListID'],
        colModel: [
            { name: 'LegNUM', index: 'LegNUM', hidden: true },
                { name: 'HotelCode', index: 'HotelCode', width: 50, key: true },
                { name: 'PreflightHotelName', index: 'PreflightHotelName', width: 120, key: true },
                { name: 'isArrivalHotel', index: 'isArrivalHotel', width: 40, search: false, formatter: arrivalDepartureFormatter },
                { name: 'CrewCode', index: 'CrewCode', width: 50, search: false },
                { name: 'Address1', index: 'Address1', width: 130, search: false },
                { name: 'CityName', index: 'CityName', width: 70, search: false },
                { name: 'StateName', index: 'StateName', width: 70, search: false },
                { name: 'CountryCode', index: 'CountryCode', width: 70, search: false },
                { name: 'PhoneNum1', index: 'PhoneNum1', width: 71, search: false },
                { name: 'HotelID', index: 'HotelID', hidden: true },
                
                { name: 'PreflightHotelListID', index: 'PreflightHotelListID', hidden: true }
        ],

        gridComplete: function () {           
            selectCrewHotel(selHotelCode);
        },
        onSelectRow: function (id) {
            if (self.CrewHotelEditMode() == false) {
                var hotelCode = jQuery(jqGridCrewHotelListGrid).jqGrid('getCell', id, 'HotelCode');
                jQuery(jqGridCrewHotelListGrid).restoreRow(lastSelHotel);
                lastSelHotel = id;
                if (!IsNullOrEmptyOrUndefined(legNum)) {
                    GetPreflightCrewHotelDetailsByLegNum(legNum, hotelCode);
                }
            }
            else
            {
                jAlert(alertMessageHotelSave, "Preflight");
                return false;
            }
        }
    });

}

function selectCrewHotel(selHotelCode)
{
    var ids = jQuery(jqGridCrewHotelListGrid).jqGrid("getDataIDs");
    if (ids.length > 0) {
        if (IsNullOrEmptyOrUndefined(selHotelCode)) {
            jQuery(jqGridCrewHotelListGrid).jqGrid("setSelection", ids[0]);
        }
        else {
            for (i = 0; i < ids.length; i++) {
                var rowData = jQuery(jqGridCrewHotelListGrid).jqGrid('getRowData', ids[i]);

                if (rowData['HotelCode'] == selHotelCode) {
                    jQuery(jqGridCrewHotelListGrid).jqGrid('setSelection', ids[i]);
                } //if
            } //for
        }
    }
}

function GetPreflightCrewTransportDetailsByLegNum(legNum, isasync)
{
    if (IsNullOrEmptyOrUndefined(legNum)) {
        legNum = 1;
    }
    if (isasync == undefined)
        isasync = false;
    var paramters = {};
    paramters.LegNum = legNum;
    $.ajax({
        async: isasync,
        cache: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetCrewLegTransportViewModelByLegNum",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                isAfterGetTransportCall = true;
                if (self.CurrentCrewTransport.DepartCode == undefined) {
                    self.CurrentCrewTransport = ko.mapping.fromJS(response.d.Result);
                }
                else {
                    ko.mapping.fromJS(response.d.Result, self.CurrentCrewTransport);
                }
                self.CurrentCrewTransport.ArrivalAirportId(self.Preflight.PreflightLegs()[self.CurrentCrewTransportLegNum() - 1].ArriveICAOID());
                self.CurrentCrewTransport.DepartureAirportId(self.Preflight.PreflightLegs()[self.CurrentCrewTransportLegNum() - 1].DepartICAOID());

                setTimeout(function () {
                    isAfterGetTransportCall = false;
                }, 1000);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });
}

function isArrivalSelected() {
    var arrSelVal = $('input[name="rbArriveDepart"]:checked').val();
    if (arrSelVal === "false"   ) {
        return "1";
    }
    else {
        return "0";
    }
}

function RetriveCrewHotelRoomTypes() {
    $.ajax({
        type: "POST",
        url: "PreflightTripManager.aspx/RetrieveRoomType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                self.CrewHotelRooms(response.d.Result);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });
}

function RetriveRoomDescription() {
    $.ajax({
        type: "POST",
        url: "PreflightTripManager.aspx/RetrieveRoomDescription",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                self.RoomDescription(response.d.Result);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });
}

function SaveCrewHotel() {
    
    var tbCtry = self.CurrentCrewHotel.CountryCode;
    var hdnCtryID = self.CurrentCrewHotel.CountryID;

    var tbMetro = self.CurrentCrewHotel.MetroCD;
    var MetroID = self.CurrentCrewHotel.MetroID;

    if (IsNullOrEmptyOrUndefined(self.CurrentCrewHotel.HotelCode()) || IsNullOrEmptyOrUndefined(self.CurrentCrewHotel.HotelID())) {
        jAlert("Hotel Code should not be Empty!", title);
        return;
    }
    else if (!IsNullOrEmptyOrUndefined($('#lbcvHotelCode').html())) {
        jAlert("Hotel Code should not be Empty!", title);
        return;
    }
    if (IsNullOrEmptyOrUndefined(hdnCtryID()) && !IsNullOrEmptyOrUndefined(tbCtry())) {
        tbCtry("");
        hdnCtryID(null);
    }
    if (IsNullOrEmptyOrUndefined(MetroID()) && !IsNullOrEmptyOrUndefined(tbMetro())) {
        tbMetro("");
        MetroID(null);
    }
    $("#lbcvCtry").html("");
    $("#lbcvMetro").html("");
    var selRowIds = jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('getGridParam', 'selarrrow');
    var selectedRowsLength = 0;
    if (!IsNullOrEmptyOrUndefined(selRowIds)) {
        selectedRowsLength = selRowIds.length;
        self.CurrentCrewHotel.CrewIDList = ko.observableArray();
        for (selindex = 0; selindex < selectedRowsLength; selindex++) {
            var rowData = jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('getRowData', selRowIds[selindex]);
            self.CurrentCrewHotel.CrewIDList.push(rowData['CrewID']);
        }
    }
    else
    {
        self.CurrentCrewHotel.CrewIDList = ko.observableArray();
        self.CurrentCrewHotel.CrewIDList.removeAll();
    }

    var crewHotelObject = ko.toJS(self.CurrentCrewHotel);
    
    crewHotelObject.PreflightHotelListID = crewHotelObject.PreflightHotelListID == null ? 0 : crewHotelObject.PreflightHotelListID;
    crewHotelObject.HotelIdentifier = crewHotelObject.HotelIdentifier == null ? 0 : crewHotelObject.HotelIdentifier;
    crewHotelObject.IsDeleted = crewHotelObject.IsDeleted == null ? false : crewHotelObject.IsDeleted;
    crewHotelObject.crewPassengerType = "C";

    crewHotelObject.isArrivalHotel = $("#rdArrival").is(":checked");
    crewHotelObject.isDepartureHotel = $("#rdDeparture").is(":checked");
    if (crewHotelObject.ClientDateIn != null && crewHotelObject.ClientDateIn != "")
        crewHotelObject.DateIn = moment(crewHotelObject.ClientDateIn).format(isodateformat);
    else
        crewHotelObject.DateIn = null;
    if (crewHotelObject.ClientDateOut != null && crewHotelObject.ClientDateOut != "")
        crewHotelObject.DateOut = moment(crewHotelObject.ClientDateOut).format(isodateformat);
    else
        crewHotelObject.DateOut = null;
    crewHotelObject.LegNUM = self.CurrentCrewHotelLegNum();
    crewHotelObject = removeUnwantedPropertiesKOViewModel(crewHotelObject);
    self.CurrentCrewHotel.LegNUM(self.CurrentCrewHotelLegNum());
    crewHotelObject.State=1;
    var crewhotelstring = JSON.stringify(crewHotelObject);
    var param = "{'crewHotel':" + JSON.stringify(crewHotelObject) + "}";

    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightCrewHotel",
        contentType: "application/json; charset=utf-8",
        data: param,
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                RefreshCrewHotelGrid(crewHotelObject.LegNUM, crewHotelObject.HotelCode, true);
                self.CrewHotelEditMode(false);
                self.CurrentCrewHotel.HotelCodeEdit(null);
            }
        },
        complete: function (jqXHR, textStatus) {
            self.CrewHotelEditMode(false);
            self.CurrentCrewHotel.HotelCodeEdit(null);
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);            
        }
    });

}

function DeleteCrewHotel(legNum, hotelCode, preflightHotelListID) {
    self.CrewHotelEditMode(false);
    self.CurrentCrewHotel.HotelCodeEdit(null);
    var paramters = {};
    paramters.LegNUM = legNum;
    paramters.PreflightHotelListID = preflightHotelListID;
    if (IsNullOrEmptyOrUndefined(hotelCode))
        paramters.CrewHotelCode = '';
    else
        paramters.CrewHotelCode = hotelCode;

    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/DeletePreflightCrewHotel",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(paramters),
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                self.ResetCurrentCrewHotel();
                jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('resetSelection');
                RefreshCrewHotelGrid(legNum, hotelCode, true);
            }
        },
        complete: function (jqXHR, textStatus) {
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });

}

function CancelCrewHotel()
{
    $("#lbcvHotelCode").html("");
    $("#lbcvCtry").html("");
    $("#lbcvMetro").html("");
    self.CrewHotelEditMode(false);
    self.CurrentCrewHotel.HotelCodeEdit(null);
    jQuery(jqGridCrewLegHotelSelectionGrid).trigger('reloadGrid');
    if(self.CurrentCrewHotel.HotelCode() != '')
    {
        GetPreflightCrewHotelDetailsByLegNum(self.CurrentCrewHotelLegNum(), self.CurrentCrewHotel.HotelCode());
    }
    refreshNoCrew();
}

function refreshNoCrew()
{
    if (!IsNullOrEmptyOrUndefined(self.CurrentCrewHotel.CrewCode()) && self.CurrentCrewHotel.CrewCode().toLowerCase() == "none") {
        noCrewChecked = true;
        $("#chkNoCrew").prop("checked", true);
    } else {
        noCrewChecked = false;
        $("#chkNoCrew").prop("checked", false);
    }
    $("#chkNoCrew").trigger('change');
}

function modifySelectionPreflightLegCrewHotelSelectedCrewViewModelEntryGrid()
{
    var grid = jQuery(jqGridCrewLegHotelSelectionGrid);
    if ($(grid).find('tr').length <= 0) return;
    jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('resetSelection');
    
    var rowIds = jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('getDataIDs');
    refreshNoCrew();
    if (!IsNullOrEmptyOrUndefined(self.CurrentCrewHotel.CrewIDList()) && !IsNullOrEmptyOrUndefined(rowIds)) {
        var arrLen = self.CurrentCrewHotel.CrewIDList().length;        
            
        for (icrew = 0 ; icrew < arrLen; icrew++) {
            var crewvalue = self.CurrentCrewHotel.CrewIDList()[icrew];
            var strCrewValue = '' + crewvalue;
            for (j = 0; j < rowIds.length; j++) {
                var rowData = jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('getRowData', rowIds[j]);
                if (rowData['CrewID'] == strCrewValue) {
                    jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('setSelection', rowIds[j]);
                    break;
                } //if
            } //for
        }

        if (arrLen == rowIds.length) {
            $("#cb_dgCrewLegHotelSelectionGrid").prop("checked", true);
        }

    }
}

function LoadPreflightLegCrewHotelSelectedCrew(legNum) {
    var MultiSelectFlag = true;
    jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('GridUnload');
    jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid({
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/LoadPreflightLegCrewHotelSelectedCrew',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: false,
        multiselectWidth: 40,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            postData.LegNum = legNum;
            return JSON.stringify(postData);
        },
        height: 354,
        width: 323,
        autowidth: true,
        shrinkToFit: false,
        multiselect: true,
        loadonce:true,
        colNames: ['CrewID', 'Crew Code', 'OrderNUM', 'Name', 'Lname', 'Mname'],
        colModel: [
            { name: 'CrewID', index: 'CrewID', width: 60, key: true, hidden: true },
             { name: 'CrewCode', index: 'CrewCode', width: 40},
             { name: 'OrderNUM', index: 'OrderNUM', hidden: true },
             { name: 'CrewFirstName', index: 'CrewFirstName', formatter: CrewNameFormatter, width: 240, search: false },
             { name: 'CrewLastName', index: 'CrewLastName', hidden: true },
             { name: 'CrewMiddleName', index: 'CrewMiddleName', hidden: true }
        ],
        beforeSelectRow: function (rowid, e) {
            if (self.CrewHotelEditMode() == true) {
                return true;
            } else {
                return false;
            }
        },
        loadComplete: function () {
            var gridid = jqGridCrewLegHotelSelectionGrid.substr(1);
            $("#jqgh_" + gridid + "_cb").find("span").remove();
            $("#jqgh_" + gridid + "_cb").find("br").remove();
            $("#jqgh_" + gridid + "_cb").prepend("<span>Select All</span><br/>");
            $("#jqgh_" + gridid + "_cb").css("height", "32px");
            $(jqGridCrewLegHotelSelectionGrid + "_cb").css("width", "40px");
            $(jqGridCrewLegHotelSelectionGrid + "_cb").css("font-size", "10px");
            $(jqGridCrewLegHotelSelectionGrid + " tbody tr").children().first("td").css("width", "40px");



            if (self.CurrentCrewHotel.CrewIDList() != undefined) {
                $.each(self.CurrentCrewHotel.CrewIDList(), function (index, value) {
                    $(jqGridCrewLegHotelSelectionGrid).jqGrid('setSelection', value, true);
                });
            }
            $("tr.jqgrow > td > input.cbox", jQuery(jqGridCrewLegHotelSelectionGrid)).attr("disabled", "disabled");
            $("#cb_" + jQuery(jqGridCrewLegHotelSelectionGrid)[0].id).hide();
        },
        onSelectRow: function (id, status) {
            if (noCrewChecked == false) {
                var selRowIds = jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('getGridParam', 'selarrrow');
                var recCount = jQuery(jqGridCrewLegHotelSelectionGrid).getGridParam("reccount");
                if (selRowIds.length >= recCount) {
                    for (var i = 0; i < selRowIds.length; i++) {
                        $("#jqg_dgCrewLegHotelSelectionGrid_" + selRowIds[i] + '.cbox').attr('checked', true);
                    }
                }
            }
            else
            {
                jQuery(jqGridCrewLegHotelSelectionGrid).jqGrid('resetSelection');
            }
        }
    });

}

function arrivalDepartureFormatter(cellvalue, options, rowObject) {
    var arrival = 'A';
    if (IsNullOrEmptyOrUndefined(rowObject["isArrivalHotel"]) == true || rowObject["isArrivalHotel"] != true)
        arrival = 'D';
    return arrival;
}

function CrewNameFormatter(cellvalue, options, rowObject) {
    var name = '';
    if (IsNullOrEmptyOrUndefined(rowObject["CrewLastName"]) == false)
        name = rowObject["CrewLastName"];
    if (IsNullOrEmptyOrUndefined(rowObject["CrewFirstName"]) == false)
        name = name + ' ' + rowObject["CrewFirstName"];
    if (IsNullOrEmptyOrUndefined(rowObject["CrewMiddleName"]) == false )
        name = name + ' ' + rowObject["CrewMiddleName"];
    name = name.replace(/null/g, "");
    return name;
}

function SaveCrewTransportation()
{
    if (self.CurrentCrewTransportLegNum() <= 0 || self.EditMode() != true)
        return;

    self.CurrentCrewTransport.ArrivalAirportId(self.Preflight.PreflightLegs()[self.CurrentCrewTransportLegNum() - 1].ArriveICAOID());
    self.CurrentCrewTransport.DepartureAirportId(self.Preflight.PreflightLegs()[self.CurrentCrewTransportLegNum() - 1].DepartICAOID());

    var crewTransportObject = ko.toJS(self.CurrentCrewTransport);
    crewTransportObject.LegNUM = self.CurrentCrewTransportLegNum();
    crewTransportObject = removeUnwantedPropertiesKOViewModel(crewTransportObject);
    self.CurrentCrewTransport.LegNUM(self.CurrentCrewTransportLegNum());
    crewTransportObject.crewPassengerType = "C";
    
    var param = "{'crewTransportation':" + JSON.stringify(crewTransportObject) + "}";

    $.ajax({
        type: "POST",
        cache: false,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightCrewTransportation",
        contentType: "application/json; charset=utf-8",
        data: param,
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
        },
        complete: function (jqXHR, textStatus) {
        },
        error: function (xhr, textStatus, errorThrown) {
            reportPreflightError(xhr, textStatus, errorThrown);
        }
    });
}


function crewLogisticsModuleChange(moduleId) {
    if (moduleId == 1) {
        $("#radPgeViewTransport").hide();
        $("#rdPgeViewHotel").show();
        $("#ulTransportation").removeClass("tabSelected");
        $("#ulHotel").addClass("tabSelected");
        
        $("#legTabhotel li:eq(" + (parseInt(self.CurrentCrewHotelLegNum()) - 1) + ")").click();

        RefreshCrewHotelGrid(self.CurrentCrewHotelLegNum());
    }
    else {
        if (self.EnableHotel() == true) {
            jAlert(alertMessageHotelSave, "Preflight");
            return false;
        }
        
        $("#radPgeViewTransport").show();
        $("#rdPgeViewHotel").hide();
        $("#ulTransportation").addClass("tabSelected");
        $("#ulHotel").removeClass("tabSelected");
        $("#legTabTransport li:eq(" + (parseInt(self.CurrentCrewTransportLegNum()) - 1) + ")").click();

    }
}

function crewTabModuleChange(moduleId) {
    if (moduleId == 1) {
        if (self.EnableHotel() == true) {
            jAlert(alertMessageHotelSave, "Preflight");
            return false;
        }
        $("#pnlCrewLogistics").hide();
        $("#pnlCrew").show();
        $("#crewInfo").addClass("tabStripItemSelected");
        $("#crewLogistics").removeClass("tabStripItemSelected");
        $("#crewLogistics").addClass("tabStripItem");
    }
    else {
        if (self.EditMode() == true) {
            window.Save_CrewInfo();
        }
        InitializeCrewViewModels();
        GetPreflightCrewTransportDetailsByLegNum(self.CurrentCrewTransportLegNum());
        LoadPreflightLegCrewHotelSelectedCrew(self.CurrentCrewTransportLegNum());
        $("#pnlCrewLogistics").show();
        $("#pnlCrew").hide();
        $("#crewInfo").removeClass("tabStripItemSelected");
        $("#crewInfo").addClass("tabStripItem");
        $("#crewLogistics").addClass("tabStripItemSelected");


        //Calendar context menu click
        var subTabNameHotel = getQuerystring("subTab", "PreFlight");
        var subTabNameTran = getQuerystring("legHotel", "PreFlight");
        var reqLegNum = getQuerystring("legNo", "1");
        if (subTabNameHotel == "1") {
            if (subTabNameTran != "1") {
                setTimeout(function () { $("#legTabhotel li:eq(" + (reqLegNum - 1) + ")").click(); }, 300);
            }
            else {
                crewLogisticsModuleChange('2');
                setTimeout(function () { $("#legTabTransport li:eq(" + (reqLegNum - 1) + ")").click(); }, 100);
            }
        }

    }
}



function CrewTransportValidation(TransCode, AirportID, crewTransport, isArrival, btnTrans, ErrorMsg, lbTransCode, lbcvTransCode) {
    lbcvTransCode = "#" + lbcvTransCode;
    lbTransCode = "#" + lbTransCode;
    if (!IsNullOrEmpty(TransCode)) {
        Start_Loading(btnTrans);
        $.ajax({
            async: true,
            cache: false,
            url: "/Views/Transactions/CommonValidator.aspx/TransportValidation",
            type: "POST",
            dataType: "json",
            data: "{TransportCD:'" + TransCode + "',AirportID:'" + AirportID + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(lbcvTransCode).html(ErrorMsg);
                    $(lbTransCode).val("");
                    if (isArrival) {
                        crewTransport.ArrivalCode(TransCode);
                        crewTransport.ArrivalName("");
                        crewTransport.ArrivalPhone("");
                        crewTransport.ArrivalFax("");
                        crewTransport.ArrivalRate("");
                        crewTransport.ArrivalTransportId(0);
                    }
                    else {
                        crewTransport.DepartCode(TransCode);
                        crewTransport.DepartName("");
                        crewTransport.DepartPhone("");
                        crewTransport.DepartFax("");
                        crewTransport.DepartRate("");
                        crewTransport.DepartTransportId(0);
                    }
                    End_Loading(btnTrans, false);
                }
                else {
                    $(lbTransCode).val(htmlDecode(jsonObj.lblValue));
                    $(lbcvTransCode).html("");
                    if (isArrival) {
                        crewTransport.ArrivalCode(TransCode);
                        crewTransport.ArrivalName(htmlDecode(jsonObj.lblValue));
                        crewTransport.ArrivalPhone(htmlDecode(jsonObj.Other.PhoneNum));
                        crewTransport.ArrivalFax(htmlDecode(jsonObj.Other.FaxNum));
                        crewTransport.ArrivalRate(htmlDecode(jsonObj.Other.NegotiatedRate));
                        crewTransport.ArrivalTransportId(htmlDecode(jsonObj.hdValue));
                    }
                    else {
                        crewTransport.DepartCode(TransCode);
                        crewTransport.DepartName(htmlDecode(jsonObj.lblValue));
                        crewTransport.DepartPhone(htmlDecode(jsonObj.Other.PhoneNum));
                        crewTransport.DepartFax(htmlDecode(jsonObj.Other.FaxNum));
                        crewTransport.DepartRate(htmlDecode(jsonObj.Other.NegotiatedRate));
                        crewTransport.DepartTransportId(htmlDecode(jsonObj.hdValue));
                    }
                    End_Loading(btnTrans, true);
                }
                isAfterGetTransportCall = false;
            }

        });
    }
    else {
        if (isArrival) {
            crewTransport.ArrivalCode(TransCode);
            crewTransport.ArrivalName("");
            crewTransport.ArrivalPhone("");
            crewTransport.ArrivalFax("");
            crewTransport.ArrivalRate("");
            crewTransport.ArrivalTransportId(0);
        } else {
            crewTransport.DepartCode("");
            crewTransport.DepartName("");
            crewTransport.DepartPhone("");
            crewTransport.DepartFax("");
            crewTransport.DepartRate("");
            crewTransport.DepartTransportId(0);
        }
        $(lbcvTransCode).html("");
        isAfterGetTransportCall = false;
        End_Loading(btnTrans, false);
    }
}

function CrewHotelValidation(AirportID, HotelCode, crewHotel, lbcvHotelCode, btn) {
    $(lbcvHotelCode).html("");
    crewHotel.HotelCode(HotelCode);
    if (IsNullOrEmptyOrUndefined(crewHotel) || IsNullOrEmptyOrUndefined(HotelCode)) {
        self.ResetCurrentCrewHotel("isArrivalHotel");
        return;
    }
    Start_Loading(btn);
    if (!IsNullOrEmpty(HotelCode)) {
        $.ajax({
            async: false,
            cache: false,
            url: "/Views/Transactions/CommonValidator.aspx/HotelValidation",
            type: "POST",
            dataType: "json",
            data: "{HotelCD:'" + $.trim(HotelCode) + "',AirportID:'" + AirportID + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var jsonObj = result.d.Result;
                var returnResult = verifyReturnedResult(result);
                var data = jsonObj;
                if (returnResult == true && result.d.Success == true && data && data.flag == "1") {
                    crewHotel.Rate(htmlDecode(data.Other.NegociatedRate));
                    crewHotel.PreflightHotelName(htmlDecode(data.lblValue.toUpperCase()));
                    crewHotel.HotelID(htmlDecode(data.hdValue.toString()));
                    crewHotel.PhoneNum1(htmlDecode(data.Other.PhoneNum));
                    crewHotel.FaxNUM(htmlDecode(data.Other.FaxNum));
                    crewHotel.Address1(htmlDecode(data.Other.Addr1));
                    crewHotel.Address2(htmlDecode(data.Other.Addr2));
                    crewHotel.Address3(htmlDecode(data.Other.Addr3));
                    crewHotel.CityName(htmlDecode(data.Other.CityName));
                    crewHotel.StateName(htmlDecode(data.Other.StateName));
                    crewHotel.CountryCode(htmlDecode(data.Other.CountryCD));
                    crewHotel.CountryID(htmlDecode(data.Other.CountryID));
                    crewHotel.MetroID(htmlDecode(data.Other.MetroID));
                    crewHotel.MetroCD(htmlDecode(data.Other.MetroCD));
                    crewHotel.PostalCode(htmlDecode(data.Other.PostalZipCD));
                    End_Loading(btn, true);
                }
                else {
                    $(lbcvHotelCode).html("Hotel Code Does Not Exist");
                    self.ResetCurrentCrewHotel(["HotelCode", "isArrivalHotel", "DateIn", "DateOut", "ClientDateIn", "ClientDateOut"]);
                    End_Loading(btn, false);
                }
                return jsonObj;
            }
        });
    }
    else {
        self.ResetCurrentCrewHotel("isArrivalHotel", "DateIn", "DateOut", "ClientDateOut", "ClientDateIn");
        End_Loading(btn, false);
        return null;
    }
}

function CrewHotelCountryValidation(crewHotel, CountryCode, CountryCVLabel, btn) {
    if (!IsNullOrEmpty(CountryCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            cache: false,
            url: "/Views/Transactions/CommonValidator.aspx/CountryValidation",
            type: "POST",
            dataType: "json",
            data: "{CountryCD:'" + CountryCode + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(CountryCVLabel).html("Country No. Does Not Exist");
                    crewHotel.CountryID("");
                    End_Loading(btn, false);
                }
                else {
                    crewHotel.CountryCode(htmlDecode(jsonObj.txtValue));
                    crewHotel.CountryID(htmlDecode(jsonObj.hdValue));
                    $(CountryCVLabel).html("");
                    End_Loading(btn, true);
                }
            }
        });
    }
    else {
        $(CountryCVLabel).html("");
        CountryHidden("");
        End_Loading(btn, false);
    }
}

function CrewHotelMetroValidation(crewHotel, MetroCode, MetroCVLabel, btn) {
    if (!IsNullOrEmpty(MetroCode)) {
        Start_Loading(btn);
        $.ajax({
            async: true,
            cache: false,
            url: "/Views/Transactions/CommonValidator.aspx/MetroValidation",
            type: "POST",
            dataType: "json",
            data: "{MetroCD:'" + MetroCode + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                var jsonObj = result.d.Result;
                if (!(returnResult == true && result.d.Success == true) || jsonObj.flag == "0" ) {
                    $(MetroCVLabel).html("Metro Code Does Not Exist");
                    crewHotel.MetroID("");
                    End_Loading(btn, false);
                }
                else {
                    crewHotel.MetroCD(htmlDecode(jsonObj.txtValue));
                    crewHotel.MetroID(htmlDecode(jsonObj.hdValue));
                    $(MetroCVLabel).html("");
                    End_Loading(btn, true);
                }
            }
        });
    }
    else {
        $(MetroCVLabel).html("");
        crewHotel.MetroID("");
        End_Loading(btn, false);
    }
}

function showSelectAll(show)
{
    var gridid = jqGridCrewLegHotelSelectionGrid.substr(1);
    if (show == true) {
        $("#jqgh_" + gridid + "_cb").find("span").remove();
        $("#jqgh_" + gridid + "_cb").find("br").remove();


        $("#jqgh_" + gridid + "_cb").prepend("<span>Select All</span><br/>");
        $("#jqgh_" + gridid + "_cb").css("height", "32px");
        $(jqGridCrewLegHotelSelectionGrid + "_cb").css("width", "42px");
        $(jqGridCrewLegHotelSelectionGrid + "_cb").css("font-size", "10px");
        $(jqGridCrewLegHotelSelectionGrid + " tbody tr").children().first("td").css("width", "42px");
    }
    else
    {
        $("#jqgh_" + gridid + "_cb").find("span").remove();
        $("#jqgh_" + gridid + "_cb").find("br").remove();
    }
}

function addNewHotel(legNumber, isArrival) {
    var paramters = {};
    paramters.legNum = legNumber;
    paramters.isArrival = isArrival;
    $.ajax({
        async: false,
        cache: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/AddNewHotel",
        data: JSON.stringify(paramters),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (self.CurrentCrewHotel.HotelCode == undefined) {
                    self.CurrentCrewHotel = ko.mapping.fromJS(response.d.Result);
                }
                else {
                    ko.mapping.fromJS(response.d.Result, self.CurrentCrewHotel);
                }
                var optionRoomTypes = $('#ddltypeofRoomCrewHotel option');
                var result = $.map(optionRoomTypes, function (option) {
                    if (option.text == "Select")
                        return option.value;
                });
                self.CurrentCrewHotel.RoomTypeID(result);
            }
        },
        failure: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

