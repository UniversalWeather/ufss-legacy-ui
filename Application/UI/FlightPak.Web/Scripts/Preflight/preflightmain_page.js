﻿
$(document).ready(function () {
    InitializeSortSummaryDetail();
    InitializeDetailedLegSummary();
});

function DepartDateValidation() {
    var dtDate = document.getElementById("tbDepartDate");
    var hdDate = document.getElementById("hdDepartDate");
    var showlegPopup = ValidationDeaprtDate(dtDate, hdDate);
    SavePreflightMain();
}

function ClientCodeValidation() {
    var btn = document.getElementById("btnClientCode");
    var clienttext = self.Preflight.PreflightMain.Client.ClientCD;
    var clientlbl = self.Preflight.PreflightMain.Client.ClientDescription;
    var clienthdn = self.Preflight.PreflightMain.Client.ClientID;
    var pfClientId = self.Preflight.PreflightMain.ClientID;
    var clientcvlbl = document.getElementById("lbcvClientCode");
    Client_Validate_Retrieve(clienttext, clientlbl, clienthdn, clientcvlbl, pfClientId, btn);
}
function fnValidateAuth() {

    if (IsNullOrEmpty(self.Preflight.PreflightMain.DepartmentID())) {
        jAlert("Please enter the Department Code before entering the Authorization Code.", "PreflightMain");
    }
    else {
        openWin('radAuthorizationPopup');
    }
}

function TailNumValidation() {

    var btn = document.getElementById("btnTailNo");
    var hdCalculateLeg = document.getElementById("hdCalculateLeg");

    var tailnumtext = self.Preflight.PreflightMain.Fleet.TailNum;
    var tailnumcvlbl = document.getElementById("lbcvTailNo");
    var tailnumhdn = self.Preflight.PreflightMain.Fleet.FleetID;
    var pfFleetId = self.Preflight.PreflightMain.FleetID;

    var homebasetext = self.Preflight.PreflightMain.HomeBaseAirportICAOID;
    var homebaselbl = self.Preflight.PreflightMain.Company.BaseDescription;
    var homebasehdn = self.Preflight.PreflightMain.HomebaseID;
    var homebasecvlbl = document.getElementById("lbcvHomeBase");
    var hdairportHomeBase = self.Preflight.PreflightMain.HomebaseAirportID;

    var typetext = self.Preflight.PreflightMain.Aircraft.AircraftCD;
    var typetb = document.getElementById("tbType");
    var typelbl = self.Preflight.PreflightMain.Aircraft.AircraftDescription;
    var typehdn = self.Preflight.PreflightMain.Aircraft.AircraftID;
    var typecvlbl = document.getElementById("lbcvType");
    var rfvType = document.getElementById("rfvType");
    var pfType = self.Preflight.PreflightMain.AircraftID;

    var EMtext = self.Preflight.PreflightMain.EmergencyContact.EmergencyContactCD;
    var EMlbl = self.Preflight.PreflightMain.EmergencyContact.EmergencyContactDesc;
    var EMhdn = self.Preflight.PreflightMain.EmergencyContact.EmergencyContactID;
    var EMcvlbl = document.getElementById("lbcvEmergencyContact");
    var pfEMId = self.Preflight.PreflightMain.EmergencyContactID;

    var typeBtn = document.getElementById("btnType");
    TailNo_Validate_Retrieve(tailnumtext, tailnumhdn, pfFleetId, tailnumcvlbl, homebasetext, homebaselbl, homebasehdn, hdairportHomeBase, homebasecvlbl, typetext, typelbl, typehdn, pfType, typecvlbl, rfvType, EMtext, EMlbl, EMhdn, EMcvlbl, pfEMId, hdCalculateLeg, btn, typeBtn, typetb);
    setTimeout(function () {
        if (self.EditMode() == true) {
            Preflight_ReCalculation_Validate_Retrieve(self.Preflight.PreflightMain.Fleet.TailNum());
        }
    }, 1500);
    

}

function AirTypeValidation() {
    var btn = document.getElementById("btnType");
    var hdCalculateLeg = document.getElementById("hdCalculateLeg");
    var typetext = self.Preflight.PreflightMain.Aircraft.AircraftCD;
    var typelbl = self.Preflight.PreflightMain.Aircraft.AircraftDescription;
    var typehdn = self.Preflight.PreflightMain.Aircraft.AircraftID;
    var pfType = self.Preflight.PreflightMain.AircraftID;
    var typecvlbl = document.getElementById("lbcvType");
    var rfvType = document.getElementById("rfvType");
    
    self.Preflight.PreflightMain.Fleet.TailNum("");
    self.Preflight.PreflightMain.Fleet.FleetID("0");
    self.Preflight.PreflightMain.FleetID(null);
    
    AircraftType_Validate_Retrieve(typetext, typehdn, typecvlbl, typelbl, rfvType, hdCalculateLeg, pfType, btn);
    
    setTimeout(function () {
        if (self.EditMode() == true) {
            UpdateAllLegsCalculation();
        }
    }, 1500);

}

function ContactValidation() {
    var btn = document.getElementById("btnEmergencyContact");
    var EMtext = self.Preflight.PreflightMain.EmergencyContact.EmergencyContactCD;
    var EMlbl = self.Preflight.PreflightMain.EmergencyContact.EmergencyContactDesc;
    var EMhdn = self.Preflight.PreflightMain.EmergencyContact.EmergencyContactID;
    var EMcvlbl = document.getElementById("lbcvEmergencyContact");
    var pfEMId = self.Preflight.PreflightMain.EmergencyContactID;
    EmergencyContact_Validate_Retrieve(EMtext, EMhdn, EMlbl, EMcvlbl, pfEMId, btn);
}

function HomebaseValidator() {
    var btn = document.getElementById("btnHomeBase");
    var hdCalculateLeg = document.getElementById("hdCalculateLeg");
    var hdOldHomebase = document.getElementById("hdOldHomeBase");
    var homebasetext = self.Preflight.PreflightMain.HomeBaseAirportICAOID;
    var homebaselbl = self.Preflight.PreflightMain.Company.BaseDescription;
    var homebasehdn = self.Preflight.PreflightMain.HomebaseID;
    var hdairportHomeBase = self.Preflight.PreflightMain.HomebaseAirportID;
    var homebasecvlbl = document.getElementById("lbcvHomeBase");
    var oldHomebaseValue = $("#hdOldHomeBase").val();
    Homebase_Validate_Retrieve(homebasetext, homebaselbl, homebasehdn, homebasecvlbl, hdOldHomebase, hdairportHomeBase, hdCalculateLeg, btn);
    if (homebasehdn != oldHomebaseValue) {
        var params = { HomeBaseCD:  homebasetext() };
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/UpdateLegTimes_ForHomebase",
            type: "POST",
            dataType: "json",
            data: JSON.stringify(params),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                //This call would redirect the user to the login page if there is an authentication error
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true) {
                    //We are good, nothing to do basically
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            },
            complete: function () { }
        });
    }
}

function CrewCodeValidator() {
    var btn = document.getElementById("btnReleasedBy");
    var reletext = self.Preflight.PreflightMain.Crew.CrewCD;
    var relelbl = self.Preflight.PreflightMain.Crew.ReleasedbyDesc;
    var relehdn = self.Preflight.PreflightMain.Crew.CrewID;
    var relecvlbl = document.getElementById("lbcvReleasedby");
    var releId = self.Preflight.PreflightMain.CrewID;
    CrewCode_Validate_Retrieve(reletext, relelbl, relehdn, relecvlbl, releId, btn);
}

function CQCustomerValidator() {
    var btn = document.getElementById("btCQcustomerPopup");
    var tbCQCustomer = self.Preflight.PreflightMain.CQCustomer.CQCustomerCD;
    var lbCQCustomer = self.Preflight.PreflightMain.CQCustomer.CQCustomerName;
    var hdCQCustomer = self.Preflight.PreflightMain.CQCustomer.CQCustomerID;
    var CQCustId = self.Preflight.PreflightMain.CQCustomerID;
    var lbcvCQCustomer = document.getElementById("lbcvCQCustomer");
    CQCustomer_Validate_Retrieve(tbCQCustomer, lbCQCustomer, hdCQCustomer, lbcvCQCustomer, CQCustId, btn);
}

function RequestorValidator() {
    var btn = document.getElementById("btnName");

    var reqtext = self.Preflight.PreflightMain.Passenger.PassengerRequestorCD;
    var reqlbl = self.Preflight.PreflightMain.Passenger.RequestorDesc;
    var reqhdn = self.Preflight.PreflightMain.Passenger.PassengerRequestorID;
    var reqcvlbl = document.getElementById("lbcvRequestor");
    var reqId = self.Preflight.PreflightMain.PassengerRequestorID;
    var reqphone = self.Preflight.PreflightMain.Passenger.AdditionalPhoneNum;
    var reqPhonetb = document.getElementById("tbPhone");

    var depttext = self.Preflight.PreflightMain.Department.DepartmentCD;
    var deptlbl = self.Preflight.PreflightMain.Department.DepartmentName;
    var depthdn = self.Preflight.PreflightMain.Department.DepartmentID;
    var deptId = self.Preflight.PreflightMain.DepartmentID;
    var deptcvlbl = document.getElementById("lbcvDepartment");

    var authtext = self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD;
    var authlbl = self.Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription;
    var authhdn = self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationID;
    var authId = self.Preflight.PreflightMain.AuthorizationID;
    var authcvlbl = document.getElementById("lbcvAuthorization");
    Requestor_Validate_Retrieve(reqtext, reqlbl, reqhdn, reqcvlbl, reqphone, reqId, depttext, deptlbl, depthdn, deptcvlbl, deptId, authtext, authlbl, authhdn, authcvlbl, authId, btn, reqPhonetb);
}

function AccountValidator() {
    var btn = document.getElementById("btnAccountNo");
    var accounttext = self.Preflight.PreflightMain.Account.AccountNum;
    var accountlbl = self.Preflight.PreflightMain.Account.AccountDescription;
    var accounthdn = self.Preflight.PreflightMain.Account.AccountID;
    var accountId = self.Preflight.PreflightMain.AccountID;
    var accountcvlbl = document.getElementById("lbcvAccountNo");
    Account_Validate_Retrieve(accounttext, accountlbl, accounthdn, accountcvlbl, accountId, btn);
}

function DepartmentValidator() {
    var btn = document.getElementById("btnDepartment");
    var depttext = self.Preflight.PreflightMain.Department.DepartmentCD;
    var deptlbl = self.Preflight.PreflightMain.Department.DepartmentName;
    var depthdn = self.Preflight.PreflightMain.Department.DepartmentID;
    var deptId = self.Preflight.PreflightMain.DepartmentID;
    var deptcvlbl = document.getElementById("lbcvDepartment");
    var authtext = self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD;
    var authlbl = self.Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription;
    var authhdn = self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationID;
    var authId = self.Preflight.PreflightMain.AuthorizationID;
    Department_Validate_Retrieve(depttext, deptlbl, depthdn, deptcvlbl, deptId, authtext, authlbl, authhdn, authId, btn);
}

function AuthorizationValidator() {
    var btn = document.getElementById("btnAuth");
    var authtext = self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD;
    var authlbl = self.Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription;
    var authhdn = self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationID;
    var authcvlbl = document.getElementById("lbcvAuthorization");
    var depthdn = self.Preflight.PreflightMain.Department.DepartmentID;
    var authId = self.Preflight.PreflightMain.AuthorizationID;
    Authorization_Validate_Retrieve(authtext, authlbl, authhdn, authcvlbl, depthdn, authId, btn);
}

function DispatcherValidator() {
    var btn = document.getElementById("btnDispatcher");
    var distext = self.Preflight.PreflightMain.DispatcherUserName;
    var dislbl = self.Preflight.PreflightMain.UserMaster.DispatcherDesc;
    var disphonelbl = self.Preflight.PreflightMain.UserMaster.PhoneNum;
    var discvlbl = document.getElementById("lbcvDispatcher");
    var disUsername = self.Preflight.PreflightMain.DispatcherUserName;
    Dispatcher_Validate_Retrieve(distext, dislbl, disphonelbl, discvlbl, disUsername, btn);
}

function pageLoad() {
    initializeTripMain();
    var deptId = document.getElementById("hdDepartment").value;
    var authId = document.getElementById("tbAuthorization").value;
    if (deptId != '' && authId == '') {
        setAuthorization(deptId);
    }
}

function openFleetChargeHistory() {
    var oWnd = radopen("/Views/Settings/Fleet/FleetChargeHistory.aspx?IsPopup=&FromPage=preflight&FleetID=" + document.getElementById("hdTailNo").value + "&TailNum=" + document.getElementById("tbTailNo").value, "rdFleetChargeHistory");
}

function confirmCopyCallBackFn(arg) {
    if (arg == true) {
        document.getElementById('btnCopyYes').click();
    }
    else {
        document.getElementById('btnCopyNo').click();
    }
}

function confirmDateCallBackFn(arg) {
    if (arg == true) {        
        var dtDate = document.getElementById("tbDepartDate");
        SavePreflightMain();
        ChangeLegDate(dtDate,self.UserPrincipal._ApplicationDateFormat);
    }
}

function openWin(url, value, radWin) {
    var oWnd = radopen(url + value, radWin);
}

function openWin(radWin) {

    var url = '';
    if (radWin == "radCompanyMasterPopup") {
        url = '../../Settings/Company/CompanyMasterPopup.aspx?HomeBase=' + document.getElementById('tbHomeBase').value;
    }
    else if (radWin == "rdClientCodePopup") {
        url = '../../Settings/Company/ClientCodePopup.aspx?ClientCD=' + document.getElementById('tbClientCode').value;
    }
    else if (radWin == "rdClientCodePopupBootStrap") {
        var originurl = window.location.href;
        var splitURL = originurl.split('//');
        var clientCodePopupURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/Company/ClientCodePopupBootStrap.aspx?ClientCD=" + document.getElementById('tbClientCode').value + "&ParentPage=PreflightMain";
        SetHtmlControl('PreflightMain', document.getElementById("tbClientCode"));
        OpenWithBootStrapPopup(clientCodePopupURL, '#ModalClientCode', '#BodyClientCode', 'iFrameClientCode');
    }
    else if (radWin == "radFleetProfilePopup") {
       url = "../../Settings/Fleet/FleetProfilePopup.aspx?FromPage=preflight&TailNumber=" + document.getElementById('tbTailNo').value;
    }
    else if (radWin == "radFleetProfilePopupBootStrap") {
        var originurl = window.location.href;
        var splitURL = originurl.split('//');
        var fleetProfilePopupURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/Fleet/FleetProfilePopupBootStrap.aspx?FromPage=preflight&TailNumber=" + document.getElementById('tbTailNo').value;
        OpenWithBootStrapPopup(fleetProfilePopupURL, '#ModalFleetProfile', '#BodyFleetProfile', 'iFrameFleetProfile');
    }
    else if (radWin == "radCQCPopups") {
        url = "/Views/CharterQuote/CharterQuoteCustomerPopup.aspx?Code=" + document.getElementById("tbCQCustomer").value;
    }
    else if (radWin == "rdType") {
            var AircraftCD = document.getElementById('tbType').value;
            var AircraftHdn = document.getElementById("hdType").value;
            if (AircraftHdn == null || AircraftHdn == '' || AircraftHdn == undefined) {
                AircraftCD = '';
            }

            url = "../../Settings/Fleet/AircraftPopup.aspx?AircraftCD=" + AircraftCD + "&tailNoAirCraftID=" + AircraftHdn;
    }
    else if (radWin == "rdTypeBootStrap") {
        var FleetId = self.Preflight.PreflightMain.Fleet.FleetID();
        if (FleetId == "0" || FleetId == null || FleetId == '') {
            var AircraftCD = document.getElementById('tbType').value;
            var AircraftHdn = document.getElementById("hdType").value;
            if (AircraftHdn == null || AircraftHdn == '' || AircraftHdn == undefined) {
                AircraftCD = '';
            }
            var originurl = window.location.href;
            var splitURL = originurl.split('//');
            var aircraftPopupURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/Fleet/AircraftPopupBootStrap.aspx?AircraftCD=" + AircraftCD + "&tailNoAirCraftID=" + AircraftHdn + "&ParentPage=PreflightMain";
            SetHtmlControl('PreflightMain', document.getElementById("tbType"));
            OpenWithBootStrapPopup(aircraftPopupURL, '#ModalAircraftPopup', '#BodyAircraftPopup', 'iFrameAircraftPopup');
        }
    }
    else if (radWin == "radCrewRosterPopup") {
        url = "../../Settings/People/CrewRosterPopup.aspx?CrewCD=" + document.getElementById('tbReleasedby').value;
    }
    else if (radWin == "rdEmergencyContact") {
        var EMContactCD = document.getElementById('tbEmergencyContact').value;
        var EMContactHdn = document.getElementById('hdEmergencyContact').value;
        if (EMContactHdn == null || EMContactHdn == '' || EMContactHdn == undefined) {
            EMContactCD = '';
        }
        url = "../../Settings/People/EmergencyContactPopup.aspx?EmergencyContactCD=" + EMContactCD;
    }
    else if (radWin == "rdEmergencyContactBootStrap") {
        var originurl = window.location.href;
        var splitURL = originurl.split('//');
        var EMContactCD = document.getElementById('tbEmergencyContact').value;
        var EMContactHdn = document.getElementById('hdEmergencyContact').value;
        if (EMContactHdn == null || EMContactHdn == '' || EMContactHdn == undefined) {
            EMContactCD = '';
        }
        var AircraftEmergencyPopupURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/People/EmergencyContactPopup.aspx?EmergencyContactCD=" + EMContactCD;
        OpenWithBootStrapPopup(AircraftEmergencyPopupURL, '#ModalAircraftEmergency', '#BodyAircraftEmergency', 'iFrameAircraftEmergency');
    }
    else if (radWin == "radAccountMasterPopup") {
        url = "../../Settings/Company/AccountMasterPopup.aspx?AccountNum=" + document.getElementById('tbAccountNo').value;
    }
    else if (radWin == "radDepartmentPopup") {
        url = "../../Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=" + document.getElementById('tbDepartment').value + "&ClientID=" + document.getElementById("hdnclientID").value;
    }
    else if (radWin == "radAuthorizationPopup") {
        var deptId = self.Preflight.PreflightMain.Department.DepartmentID();
        if (deptId != "0" && deptId != null && deptId != '') {
            var authCD = document.getElementById('tbAuthorization').value;
            var authValid = document.getElementById('hdAuthorization').value;
            if (authValid == '' || authValid == null || authValid == undefined) {
                authCD = '';
            }
            url = "../../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=" + authCD + "&deptId=" + document.getElementById("hdDepartment").value + "&ClientID=" + document.getElementById("hdnclientID").value;
        }
        else {
            jAlert("Please enter the Department Code before entering the Authorization Code.", "Preflight Main", function () {
                $("#tbDepartment").focus();
            });
        }
    }
    else if (radWin == "rdDispatcher") {
        url = "../../Settings/People/DispatcherPopup.aspx?UserName=" + document.getElementById('tbDispatcher').value;
    }
    else if (radWin == "rdDispatcherPopupBootStrap") {
        var originurl = window.location.href;
        var splitURL = originurl.split('//');
        var DispatcherPopupURL = splitURL[0] + "//" + window.location.host + "/Views/Settings/People/DispatcherPopup.aspx?UserName=" + document.getElementById('tbDispatcher').value;
        OpenWithBootStrapPopup(DispatcherPopupURL, '#ModalDispatcher', '#BodyDispatcher', 'iFrameDispatcher');
    }
    else if (radWin == "radPaxInfoPopup") {
        url = "../../Settings/People/PassengerRequestorsPopup.aspx?PassengerRequestorCD=" + document.getElementById('tbRequestor').value + "&ShowRequestor=" + document.getElementById("hdShowRequestor").value;
    }
    else if (radWin == "RadRetrievePopup") {
        url = '../../Transactions/Preflight/PreflightSearch.aspx';
    }

    if (url != "")
        var oWnd = radopen(url, radWin);

}

function ShowAirportInfoPopup(Airportid) {
    window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
    return false;
}

function ConfirmClose(WinName) {
    var oManager = GetRadWindowManager();
    var oWnd = oManager.GetWindowByName(WinName);
    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
    CloseButton.onclick = function () {
        CurrentWinName = oWnd.Id;
        jConfirm("Are you sure you want to close the window?", "Confirmation!", confirmCallBackFn);
    };
}

function OnClientClose(oWnd, args) {
    var combo = $find("tbHomeBase");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            GetTripStatusOnHomeBaseClose(arg.HomebaseID);           
            self.Preflight.PreflightMain.HomeBaseAirportICAOID(arg.HomeBase);
            self.Preflight.PreflightMain.Company.BaseDescription(arg.BaseDescription);
            if (self.Preflight.PreflightMain.HomebaseID() != "") {
                document.getElementById("hdOldHomeBase").value = self.Preflight.PreflightMain.HomebaseID();
            }
            self.Preflight.PreflightMain.HomebaseID(arg.HomebaseID);
            HomebaseValidator();
            if (arg.HomeBase != null)
                document.getElementById("lbcvHomeBase").innerHTML = "";
        }
        else {
            self.Preflight.PreflightMain.HomeBaseAirportICAOID("");
            self.Preflight.PreflightMain.Company.BaseDescription("");
            self.Preflight.PreflightMain.HomebaseID(null);
            combo.clearSelection();
        }
    }
}

function GetTripStatusOnHomeBaseClose(homeBaseId) {
    $.ajax({
        async: true,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetUpdatedTrip",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({ 'homeBaseId': homeBaseId }),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            var returnResult = verifyReturnedResult(result);
            if (returnResult) {
                self.Preflight.PreflightMain.TripStatus(result.d.Result.Principal._TripMGRDefaultStatus);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#btnNewTrip").prop('disabled', "");
            ResponseEnd();
            reportPreflightError(jqXHR, textStatus, errorThrown);
        },
        complete: function () { ResponseEnd(); }
    });
}

function OnClientCodeClose(oWnd, args) {
    var combo = $find("tbClientCode");
    document.getElementById("tbClientCode").focus();
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Client.ClientCD(arg.ClientCD);
            self.Preflight.PreflightMain.Client.ClientDescription(arg.ClientDescription);
            self.Preflight.PreflightMain.Client.ClientID(arg.ClientID);
            self.Preflight.PreflightMain.ClientID(arg.ClientID);
            ClientCodeValidation();
            if (arg.ClientCD != null)
                document.getElementById("lbcvClientCode").innerHTML = "";
        }
        else {
            self.Preflight.PreflightMain.Client.ClientCD("");
            self.Preflight.PreflightMain.Client.ClientDescription("");
            self.Preflight.PreflightMain.Client.ClientID("0");
            self.Preflight.PreflightMain.ClientID(null);
            combo.clearSelection();
        }
    }
}

function OnClientTailNoClose(oWnd, args) {
    var combo = $find("tbTailNo");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Fleet.TailNum(arg.TailNum);
            if (self.Preflight.PreflightMain.Aircraft != undefined && self.Preflight.PreflightMain.Aircraft.AircraftCD != undefined)
                self.Preflight.PreflightMain.Aircraft.AircraftCD(arg.AirCraftTypeCode);

            if (document.getElementById("hdTailNo").value != "") {
                document.getElementById("hdOldTailNo").value = self.Preflight.PreflightMain.Fleet.FleetID();
            }
            self.Preflight.PreflightMain.Fleet.FleetID(arg.FleetId);
            TailNumValidation();
        }
        else {
            self.Preflight.PreflightMain.Fleet.TailNum("");
            self.Preflight.PreflightMain.Fleet.FleetID("0");
            self.Preflight.PreflightMain.FleetID(null);
            combo.clearSelection();
        }
    }
}

function OnClientDeptClose(oWnd, args) {
    var combo = $find("tbDepartment");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Department.DepartmentCD(arg.DepartmentCD);
            self.Preflight.PreflightMain.Department.DepartmentName(arg.DepartmentName);
            self.Preflight.PreflightMain.Department.DepartmentID(arg.DepartmentID);
            self.Preflight.PreflightMain.DepartmentID(arg.DepartmentID);
            self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD("");
            self.Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription("");
            self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationID("0");
            self.Preflight.PreflightMain.AuthorizationID(null);
            DepartmentValidator();
            if (arg.DepartmentCD != null)
                document.getElementById("lbcvDepartment").innerHTML = "";
        }
        else {
            self.Preflight.PreflightMain.Department.DepartmentCD("");
            self.Preflight.PreflightMain.Department.DepartmentName("");
            self.Preflight.PreflightMain.Department.DepartmentID("0");
            self.Preflight.PreflightMain.DepartmentID(null);
            combo.clearSelection();
        }
    }
}

function setAuthorization(deptId) {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Views/Utilities/GetApi.aspx?apiType=fss&method=authorization&_search=false&nd=1408618208051&size=20&page=1&filters=%5B%5D&departmentId=" + deptId + "&showInactive=true&orders=",
        dataType: "json",
        success: function (data) {
            verifyReturnedResultForJqgrid(data);
            var jsonObj = JSON.parse(JSON.stringify(data));
            document.getElementById("tbAuthorization").value = jsonObj["results"][0]["AuthorizationCD"];
            document.getElementById("hdAuthorization").value = jsonObj["results"][0]["AuthorizationID"];
            document.getElementById("lbAuthorization").innerHTML = jsonObj["results"][0]["DeptAuthDescription"];
        }
    });
}

function OnClientAccountClose(oWnd, args) {
    var combo = $find("tbAccountNo");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Account.AccountNum(arg.AccountNum);
            self.Preflight.PreflightMain.Account.AccountDescription(arg.AccountDescription);
            self.Preflight.PreflightMain.Account.AccountID(arg.AccountID);
            self.Preflight.PreflightMain.AccountID(arg.AccountID);
            if (arg.AccountNum != null)
                document.getElementById("lbcvAccountNo").innerHTML = "";
            AccountValidator();
        }
        else {
            self.Preflight.PreflightMain.Account.AccountNum("");
            self.Preflight.PreflightMain.Account.AccountDescription("");
            self.Preflight.PreflightMain.Account.AccountID("0");
            self.Preflight.PreflightMain.AccountID(null);
            combo.clearSelection();
        }
    }
}

function OnClientTypeClose(oWnd, args) {
    var combo = $find("tbType");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Aircraft.AircraftCD(arg.AircraftCD);
            self.Preflight.PreflightMain.Aircraft.AircraftDescription(arg.AircraftDescription);
            self.Preflight.PreflightMain.Aircraft.AircraftID(arg.AircraftID);
            self.Preflight.PreflightMain.AircraftID(arg.AircraftID);
            AirTypeValidation();
            if (arg.AircraftCD != null)
                document.getElementById("lbcvType").innerHTML = "";
            getTripExceptionGrid("reload");
        }
        else {
            self.Preflight.PreflightMain.Aircraft.AircraftCD("");
            self.Preflight.PreflightMain.Aircraft.AircraftDescription("");
            self.Preflight.PreflightMain.Aircraft.AircraftID("0");
            self.Preflight.PreflightMain.AircraftID(null);
            combo.clearSelection();
            getTripExceptionGrid("reload");
        }
    }
}

function OnClientEmergencyContactClose(oWnd, args) {
    var combo = $find("tbEmergencyContact>");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.EmergencyContact.EmergencyContactCD(arg.EmergencyContactCD);
            self.Preflight.PreflightMain.EmergencyContact.EmergencyContactDesc(arg.FirstName);
            self.Preflight.PreflightMain.EmergencyContact.EmergencyContactID(arg.EmergencyContactID);
            self.Preflight.PreflightMain.EmergencyContactID(arg.EmergencyContactID);
            if (arg.EmergencyContactCD != null)
                document.getElementById("lbcvEmergencyContact").innerHTML = "";
            ContactValidation();
        }
        else {
            self.Preflight.PreflightMain.EmergencyContact.EmergencyContactCD("");
            self.Preflight.PreflightMain.EmergencyContact.EmergencyContactDesc("");
            self.Preflight.PreflightMain.EmergencyContact.EmergencyContactID("0");
            self.Preflight.PreflightMain.EmergencyContactID(null);
            combo.clearSelection();
        }
    }
}

function OnClientReleasedClose(oWnd, args) {
    var combo = $find("tbReleasedby");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Crew.CrewCD(arg.CrewCD);
            self.Preflight.PreflightMain.Crew.ReleasedbyDesc(arg.CrewName);
            self.Preflight.PreflightMain.Crew.CrewID(arg.CrewID);
            self.Preflight.PreflightMain.CrewID(arg.CrewID);
            CrewCodeValidator();
            if (arg.CrewCD != null)
                document.getElementById("lbcvReleasedby").innerHTML = "";
        }
        else {
            self.Preflight.PreflightMain.Crew.CrewCD("");
            self.Preflight.PreflightMain.Crew.ReleasedbyDesc("");
            self.Preflight.PreflightMain.Crew.CrewID("0");
            self.Preflight.PreflightMain.CrewID(null);
            combo.clearSelection();
        }
    }
}

function OnClientNameClose(oWnd, args) {
    var combo = $find("tbRequestor");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.Passenger.PassengerRequestorCD(arg.PassengerRequestorCD);
            self.Preflight.PreflightMain.Passenger.RequestorDesc(arg.PassengerName);
            self.Preflight.PreflightMain.Passenger.PassengerRequestorID(arg.PassengerRequestorID);

            if (arg.PassengerRequestorCD != null)
                document.getElementById("lbcvRequestor").innerHTML = "";
            RequestorValidator();
        }
        else {
            self.Preflight.PreflightMain.Passenger.PassengerRequestorCD("");
            self.Preflight.PreflightMain.Passenger.AdditionalPhoneNum("");
            self.Preflight.PreflightMain.Passenger.RequestorDesc("");
            self.Preflight.PreflightMain.Passenger.PassengerRequestorID("0");
            combo.clearSelection();
        }
    }
}

function OnClientAuthClose(oWnd, args) {
    var combo = $find("tbAuthorization");
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD(arg.AuthorizationCD);
            self.Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription(arg.DeptAuthDescription);
            self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationID(arg.AuthorizationID);
            self.Preflight.PreflightMain.AuthorizationID(arg.AuthorizationID);
            AuthorizationValidator();
            if (arg.AuthorizationCD != null)
                document.getElementById("lbcvAuthorization").innerHTML = "";
        }
        else {
            self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationCD("");
            self.Preflight.PreflightMain.DepartmentAuthorization.DeptAuthDescription("");
            self.Preflight.PreflightMain.DepartmentAuthorization.AuthorizationID("0");
            self.Preflight.PreflightMain.AuthorizationID(null);
            combo.clearSelection();
        }
    }
}

function OnClientCloseCharterQuotCustomerPopup(oWnd, args) {
    var combo = $find("tbCQCustomer");
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.Preflight.PreflightMain.CQCustomer.CQCustomerCD(arg.CQCustomerCD);
            CQCustomerValidator();
        }
        else {
            self.Preflight.PreflightMain.CQCustomer.CQCustomerCD("");
            self.Preflight.PreflightMain.CQCustomer.CQCustomerName("");
            self.Preflight.PreflightMain.CQCustomer.CQCustomerID("0");
            combo.clearSelection();
        }
    }
}

function OnClientDispatcherClose(oWnd, args) {
    var combo = $find("tbDispatcher>");
    var btnDispatcher = $('#btnDispatcher');
    Start_Loading(btnDispatcher);
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.Preflight.PreflightMain.DispatcherUserName(arg.UserName);
            self.Preflight.PreflightMain.UserMaster.DispatcherDesc(arg.FullName);
            self.Preflight.PreflightMain.UserMaster.PhoneNum(arg.PhoneNum);
            self.Preflight.PreflightMain.DispatcherUserName(arg.UserName);
            if (arg.UserName != null)
                document.getElementById("lbcvDispatcher").innerHTML = "";
            End_Loading(btnDispatcher,true);
        }
        else {
            self.Preflight.PreflightMain.DispatcherUserName("");
            self.Preflight.PreflightMain.UserMaster.DispatcherDesc("");
            self.Preflight.PreflightMain.UserMaster.PhoneNum("");
            self.Preflight.PreflightMain.DispatcherUserName("");
            combo.clearSelection();
            End_Loading(btnDispatcher,false);
        }
    }
    else {
        End_Loading(btnDispatcher, false);
    }
}

function maxLength(field, maxChars) {
    if (field.value.length >= maxChars) {
        event.returnValue = false;
        return false;
    }
}

function maxLengthPaste(field, maxChars) {
    event.returnValue = false;
    if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
        return false;
    }
    event.returnValue = true;
}

function ValidateDate(control) {
    if (control.value != "") {
        var controlValue = $(control).datepicker('getDate');
        if (moment(new Date(controlValue)).isValid() == false) {
            jAlert("Please enter/select Date between 01/01/1900 and 12/31/2100", "PreflightMain");
            control.value = "";
        }
    }
    return false;
}

function OnClientFleetChargeHistory(oWnd, args) {
    TailNumValidation();
}

/* Copy to all Leg functions */
/* START */

function confirmCopyPrivateToAllLeg() {
    jConfirm("Copy Private To All Legs?", "Confirmation!", copyPrivateToAllLeg);
}

function copyPrivateToAllLeg(arg) {
    if (arg) {
        var isPrivate = IsNullOrEmpty(self.Preflight.PreflightMain.IsPrivate()) ? false : self.Preflight.PreflightMain.IsPrivate();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyPrivateToAllLeg",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'isPrivate': isPrivate }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading_Toppage_Search(btn, false);
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function confirmCopyRequestorToAllLeg() {
    jConfirm("Copy Requestor To All Legs?", "Confirmation!", copyRequestorToAllLeg);
}

function copyRequestorToAllLeg(arg) {
    if (arg) {
        self.Preflight.PreflightMain.Passenger.LastUpdTS(checkedDateValue(self.Preflight.PreflightMain.Passenger.LastUpdTS()));
        var pax = ko.mapping.toJS(self.Preflight.PreflightMain.Passenger);
        var paxId = self.Preflight.PreflightMain.Passenger.PassengerRequestorID();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyRequestorToAllLeg",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'pax': pax, 'paxid': paxId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
            }, error: function (jqXHR, textStatus, errorThrown) {
                End_Loading_Toppage_Search(btn, false);
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function confirmCopyDepartmentToAllLeg() {
    jConfirm("Copy Department To All Legs?", "Confirmation!", copyDepartmentToAllLeg);
}

function copyDepartmentToAllLeg(arg) {
    if (arg) {
        var Department = ko.mapping.toJS(self.Preflight.PreflightMain.Department);
        var DepartmentId = self.Preflight.PreflightMain.Department.DepartmentID();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyDepartmentToAllLeg",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'dept': Department, 'deptId': DepartmentId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function confirmCopyAuthorizationToAllLeg() {
    jConfirm("Warning,Both the current Department and Authorization will be copied to All Legs?", "Confirmation!", copyAuthorizationToAllLeg);
}

function copyAuthorizationToAllLeg(arg) {
    if (arg) {
        var Department = ko.mapping.toJS(self.Preflight.PreflightMain.Department);
        var DepartmentId = self.Preflight.PreflightMain.Department.DepartmentID();
        var Authorization = ko.mapping.toJS(self.Preflight.PreflightMain.DepartmentAuthorization);
        var AuthorizationId = self.Preflight.PreflightMain.AuthorizationID();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyAuthorizationToAllLeg",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'dept': Department, 'deptId': DepartmentId, 'auth': Authorization, 'authId': AuthorizationId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function confirmCopyCQCustomerToAllLeg() {
    jConfirm("Warning,Both the current Department and Charter Customer will be copied to All Legs?", "Confirmation!", copyCQCustomerToAllLeg);
}

function copyCQCustomerToAllLeg(arg) {
    if (arg) {
        var Department = ko.mapping.toJS(self.Preflight.PreflightMain.Department);
        var DepartmentId = self.Preflight.PreflightMain.Department.DepartmentID();
        var CQCustomer = ko.mapping.toJS(Preflight.PreflightMain.CQCustomer);
        var CQCustomerId = self.Preflight.PreflightMain.CQCustomerID();
        if (CQCustomerId != undefined && CQCustomerId != null){
            $.ajax({
                async: true,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyCQCustomerToAllLeg",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'dept': Department, 'deptId': DepartmentId, 'CQCustomer': CQCustomer, 'CQCustomerId': CQCustomerId }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
}

function confirmCopyAccountToAllLeg() {
    jConfirm("Copy Account To All Legs?", "Confirmation!", copyAccountToAllLeg);
}

function copyAccountToAllLeg(arg) {
    if (arg) {
        var account = ko.mapping.toJS(Preflight.PreflightMain.Account);
        var accountId = Preflight.PreflightMain.AccountID();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyAccountToAllLeg",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'account': account, 'accountId': accountId }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function confirmCopyTripPurposeToAllLeg() {
    jConfirm("Copy Trip Purpose To All Legs?", "Confirmation!", copyTripPurposeToAllLeg);
}

function copyTripPurposeToAllLeg(arg) {
    if (arg) {
        var tripPurpose = Preflight.PreflightMain.TripDescription();
        $.ajax({
            async: true,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/copyTripPurposeToAllLeg",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'TripPurpose': tripPurpose }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
            }, error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}
/* END */
/*----------------------Start close Bootstrap popup on close button*/
$(document).on('click', '.ModalFleetProfile', function () {
    ManuallyCloseBootstrapPopup("#ModalFleetProfile");
});
$(document).on('click', '.ModalClientCode', function () {
    ManuallyCloseBootstrapPopup("#ModalClientCode");
});
$(document).on('click', '.ModalFleetProfileCatalog', function () {
    ManuallyCloseBootstrapPopup("#ModalFleetProfileCatalog");
});
/*---------------------End close Bootstrap popup on close button*/

/*----------------------------------Start setting key value pair for page and control*/
var pageControlArray = new Object;
function SetHtmlControl(pageName,control) {
    pageControlArray.pageName = control;
}
function SetHtmlControlValue(pageName, value) {
    $(pageControlArray.pageName).val(value);
    $(pageControlArray.pageName).change();
}
/*--------------------------------End setting key value pair for page and control*/

/*----------------------------------Start setting call back function of parent page*/
var pageFunctionArray = new Object;
function SetFunction(pageName, functionName) {
    pageFunctionArray.pageName = functionName;
}
function CallParentPageFunction(pageName) {
    pageFunctionArray.pageName();
}
/*--------------------------------End Start setting call back function of parent page*/


