﻿var IsPaxInfoGridLoded = false;
var agt = navigator.userAgent.toLowerCase();
var PaxSelectionjqgridTableId = '#PaxSelectionID';
var PaxSelection = "#PaxSelection";
var hdnPassPax = "";
var hdnAssignPassLeg = "";
var hdnInsert = "";
var PaxSelectionLegs;
var userdata = new Array();
var isItFireByMainPurposeDropDown = false;
var LegPaxCodeFlightPurposeList = {};
var PaxsSummaryjqgridTableId = '#PaxsSummaryID';
var sortFlag = true;
var LegsisDeadCategoryList = {};
var alertTitle="Preflight";

var ColNames = ['Code', 'Name', 'Code', 'OrderNUM', 'PassengerRequestorID', 'Notes', 'PassengerAlert', 'BillingCode'];
var ColModel = [{
    name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: isWindowsSafari() || isMacSafari() ? 40 : 32, frozen: true, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
        return '<a  style="cursor: pointer;" onclick="return ShowPaxDetailsPopup(\'' + rowObject["PassengerRequestorID"] + '\')" >' + htmlEncode(cellvalue) + '</a>';
    },
    unformat: function (cellvalue, options, cell) {
        return cellvalue;
    }
}, { name: 'PaxName', index: 'PaxName', width: 180, frozen: true, sortable: false, search: false },
            { name: 'PassengerRequestorCD', index: 'PassengerRequestorCD', width: 100, sortable: false, search: false, hidden: true, key: true },
            { name: 'OrderNUM', index: 'OrderNUM', width: 100, sortable: false, search: false, hidden: true },
            { name: 'PassengerRequestorID', index: 'PassengerRequestorID', width: 55, sortable: false, search: false, hidden: true },
            { name: 'Notes', index: 'Notes', width: 55, sortable: false, search: false, hidden: true },
            { name: 'PassengerAlert', index: 'PassengerAlert', width: 55, sortable: false, search: false, hidden: true },
            { name: 'Billing', index: 'Billing', width: 55, sortable: false, search: false, hidden: true }
];



$(document).ready(function () {
    if (isWindowsSafari()) {
        $('body').attr('class', 'winsaf');
    }
    else if (isMacSafari()) {
        $('body').attr('class', 'macsaf');
    }


    if ($('#tbHotelCode').is(':disabled')) {
        $('#tbHotelRate').attr('disabled', true);
    }
});
function btnCancelHotelClick() {
    
    self.viewModel.ClientDateIn(null);
    self.viewModel.ClientDateOut(null);
    document.getElementById("lbcvHotelCode").innerHTML = "";
    document.getElementById("lbcvCtry").innerHTML = "";
    document.getElementById("lbcvMetro").innerHTML = "";
    var selRowId = $(jqGridPaxHotelListGrid).jqGrid('getGridParam', 'selrow');
    if (IsNullOrEmpty($("#hdnSelectedHotelID").val()) && IsNullOrEmpty(selRowId)) {
        var top_rowid = $(jqGridPaxHotelListGrid).getDataIDs()[0];
        $(jqGridPaxHotelListGrid).jqGrid("setSelection", top_rowid);
        $("#hdnSelectedHotelID").val(top_rowid);
        selRowId = top_rowid;
    }
    self.EnablePaxHotelMode(false);
    if (selRowId != null && IsNullOrEmpty($("#hdnSelectedHotelID").val()))
        $("#hdnSelectedHotelID").val(selRowId);

    $(jqGridPaxHotelListGrid).jqGrid("setSelection",$("#hdnSelectedHotelID").val());
    
    self.IsPaxHotelEdit(false);
    self.viewModel.HotelCodeEdit("");
    
}
function clearDepartTransportation() {
    Transport.DepartureAirportId("");
    Transport.DepartTransportID(0);

    Transport.DepartCode("");
    Transport.DepartName("");
    Transport.DepartFax("");
    Transport.DepartRate("");
    Transport.DepartPhone("");

    Transport.DepartRate("0.00");
    $("lbTransCode").html('');

}
function clearArriveTransportation() {
    Transport.ArrivalAirportId("");
    Transport.ArriveTransportID(0);
    Transport.ArrivalCode("");
    Transport.ArrivalName("");
    Transport.ArrivalPhone("");
    Transport.ArrivalFax("");
    Transport.ArrivalRate("");
    $("#lbArriveTransCode").html('');

}

function openWin(radWin) {
    var url = '';

    if (radWin == "radHotelPopup") {
        var radioArrival = document.getElementById("rdArrival"); //Cleint ID of RadioButtonList
        var hotelCode = document.getElementById('tbHotelCode').value;
        if (radioArrival.checked) {
            url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].ArriveICAOID() + '&HotelCD=' + hotelCode;
        }
        else {
            url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].DepartICAOID() + '&HotelCD=' + hotelCode;
        }

    }
    if (radWin == "rdTransportPopup") {
        url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + self.Preflight.PreflightLegs()[(self.PaxTransportCurrentLeg() - 1)].DepartICAOID() + '&TransportCD=' + Transport.DepartCode();
    }
    if (radWin == "rdArriveTransportPopup") {
        url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + self.Preflight.PreflightLegs()[(self.PaxTransportCurrentLeg() - 1)].ArriveICAOID() + '&TransportCD=' + Transport.ArrivalCode();
    }

    if (radWin == "RadRetrievePopup") {
        url = '/Views/Transactions/Preflight/PreflightSearch.aspx';
    }

    if (radWin == "rdChecklist") {
        url = '../../Transactions/Preflight/PreflightChecklist.aspx';
    }

    if (radWin == "rdOutBoundInstructions") {
        url = '../../Transactions/Preflight/PreflightOutboundInstruction.aspx';
    }

    if (radWin == "rdMetroPopup") {
        url = '/Views/Settings/Company/MetroMasterPopup.aspx?MetroCD=' + document.getElementById('tbMetro').value;
    }
    if (radWin == "rdCtryPopup") {
        url = '/Views/Settings/Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('tbCtry').value;
    }
    if (url != '' && radWin != "rdChecklist" && radWin != "rdOutBoundInstructions") {
        var oWnd = radopen(url, radWin);
    }
    else if (radWin == "rdOutBoundInstructions") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Outbound Instructions - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Outbound Instructions - New Tripsheet");
        }
    }
    else if (radWin == "rdChecklist") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Checklist - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Checklist - New Tripsheet");
        }
    }
}


function OnTextFocus(txtPax) {
    document.getElementById("hdnPAXPurposeOnFocus").value = txtPax.value;

}

var isPaxSummaryExpand = false;
var isPaxLogisticsExpand = false;


function pnlPaxSummaryIsCollapse() {
    isPaxSummaryExpand = false;
}

function validateButtonList() {
    if (document.getElementById('tbHotelCode').value != "" || document.getElementById('tbHotelName').value != "") {
        var arrivalCheckbox = document.getElementById("rdArrival"); //Cleint ID of RadioButtonList
        var departureCheckbox = document.getElementById("rdDeparture"); //Cleint ID of RadioButtonList

        var Checkdvalue;
        if (arrivalCheckbox.checked) {
            Checkdvalue = arrivalCheckbox;
        } else {
            Checkdvalue = departureCheckbox;
        }

        if (document.getElementById('hdnrbArriveDepartPrev').value != Checkdvalue.value) {
            document.getElementById('hdnrbArriveDepartPrev').value = Checkdvalue.value;
            setAlertTextToYesNo();
            jConfirm('Changing Arrival or Departure will clear hotel information. Do you want to continue?', 'Confirmation!', confirmrbArrivalDepartCallBackFn);
            setAlertTextToOkCancel();
        }
    }
}
function confirmrbArrivalDepartCallBackFn(arg) {
    var arrivalCheckbox = document.getElementById("rdArrival"); //Cleint ID of RadioButtonList
    var departureCheckbox = document.getElementById("rdDeparture"); //Cleint ID of RadioButtonList

    var Checkdvalue;
    if (arrivalCheckbox.checked) {
        Checkdvalue = arrivalCheckbox;
    } else {
        Checkdvalue = departureCheckbox;
    }

    if (arg == true) {
        document.getElementById('hdnrbArriveDepartPrev').value = Checkdvalue.value;

        viewModel.HotelCode("");
        viewModel.Rate("0.00");
        viewModel.PreflightHotelName("");
        viewModel.PhoneNum1("");
        viewModel.FaxNUM("");
        viewModel.NoofRooms("");
        viewModel.NoofBeds("");
        viewModel.DateIn("");
        viewModel.DateOut("");

        viewModel.Address1("");
        viewModel.Address2("");
        viewModel.Address3("");
        viewModel.Metro("");
        viewModel.StateProvince("");
        viewModel.CityName("");
        viewModel.PostalCode("");
        viewModel.CountryCode("");

        viewModel.MetroID("");
        self.viewModel.CountryID("");
        self.viewModel.HotelID("");

        document.getElementById('lbcvMetro').innerHTML = "";
        document.getElementById('lbcvCtry').innerHTML = "";
        document.getElementById('lbcvHotelCode').innerHTML = "";

        if (document.getElementById('hdnrbArriveDepartPrev').value == "0") {
            viewModel.DateIn(document.getElementById('hdnArrivalDate').value);
            viewModel.DateOut(document.getElementById('hdnNextDeptDate').value);
        }
        else {
            viewModel.DateIn("");
            viewModel.DateOut("");

        }

    }
    else {
        if (Checkdvalue.value == "false")
            document.getElementById('hdnrbArriveDepartPrev').value = "true";
        else
            document.getElementById('hdnrbArriveDepartPrev').value = "false";

        var arrivalCheckbox = document.getElementById("rdArrival"); //Cleint ID of RadioButtonList
        var departureCheckbox = document.getElementById("rdDeparture"); //Cleint ID of RadioButtonList

        var Checkdvalue;
        if (arrivalCheckbox.value == document.getElementById('hdnrbArriveDepartPrev').value) {
            arrivalCheckbox.checked = true;
        } else {
            departureCheckbox.checked = true;
        }
    }
}

function fireOnChangeCountry() {

    document.getElementById("lbcvCtry").innerHTML = "";
    var Ctrycode = self.viewModel.CountryCode();
    self.viewModel.CountryCode(document.getElementById("tbCtry").value);
    var tbCtry = self.viewModel.CountryCode;
    var lbcvCtry = document.getElementById("lbcvCtry");
    var hdnCtryID = self.viewModel.CountryID;

    var btnCtry = document.getElementById("btnCtry");
    CountryValidation(tbCtry, hdnCtryID, lbcvCtry, btnCtry);

    return true;
}

function fireOnChangeMetro() {

    document.getElementById("lbcvMetro").innerHTML = "";
    var Metrocode = self.viewModel.Metro();
    self.viewModel.Metro(document.getElementById("tbMetro").value);
    var tbMetro = self.viewModel.Metro;
    var lbcvMetro = document.getElementById("lbcvMetro");
    var MetroID = self.viewModel.MetroID;
    var btnMetro = document.getElementById("btnMetro");
    MetroValidation(tbMetro, MetroID, lbcvMetro, btnMetro);

    return true;
}


function fireOnChange() {
    document.getElementById("lbcvHotelCode").innerHTML = "";
    var hcode = document.getElementById("tbHotelCode").value;

    FetchHotelDetailbyHotelCD();

    return true;
}
function FetchHotelDetailbyHotelCD() {

    var hotelCode = viewModel.HotelCode;
    
    if (IsNullOrEmpty(hotelCode()))
    {        
        ResetHotelControls();
        return;
    }
    var airportid;
    var rdArrivalRadio = document.getElementById("rdArrival");

    if (rdArrivalRadio.checked) {
        airportid = self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg()-1)].ArriveICAOID; //document.getElementById("hdnArrivalICao").value;
    } else {
        airportid = self.Preflight.PreflightLegs()[(self.PaxHotelCurrentLeg() - 1)].DepartICAOID;//document.getElementById("hdnDepartIcao").value;
    }

    var hdnHotelchanged = document.getElementById("hdnHotelchanged");
    var lbcvHotelCode = document.getElementById("lbcvHotelCode");
    var tbHotelName = viewModel.PreflightHotelName;
    var HotelID = self.viewModel.HotelID;
    var tbHotelRate = self.viewModel.Rate;;
    var tbHotelPhone = self.viewModel.PhoneNum1;
    var tbHotelFax = self.viewModel.FaxNUM;
    var tbAddr1 = viewModel.Address1;
    var tbAddr2 = viewModel.Address2;
    var tbAddr3 = viewModel.Address3;
    var tbCity = viewModel.CityName;
    var tbState = viewModel.StateProvince;
    var tbMetro = viewModel.Metro;
    var MetroID = self.viewModel.MetroID;
    var tbCtry = viewModel.CountryCode;
    var hdnCtryID = self.viewModel.CountryID;
    var tbPost = viewModel.PostalCode;
    var btnCode = document.getElementById("btnCode");
    var maxCapAmt = viewModel.MaxCapAmount(0);
    $('#lbcvCtry').html("");
    $('#lbcvMetro').html("");
    HotelValidation(airportid, hotelCode, hdnHotelchanged, lbcvHotelCode, tbHotelName, HotelID, tbHotelRate, tbHotelPhone, tbHotelFax, tbAddr1, tbAddr2, tbAddr3, tbCity, tbState, tbMetro, MetroID, tbCtry, hdnCtryID, tbPost, btnCode)

}
function ResetHotelControls() {
    self.viewModel.HotelID("");
    self.viewModel.HotelCode("");
    self.viewModel.HotelID("");
    self.viewModel.PreflightHotelName("");
    self.viewModel.Rate("0.00");
    self.viewModel.MaxCapAmount(0);
    //radnumerictextbox.set_value("");
    
    self.viewModel.PhoneNum1("");
    self.viewModel.FaxNUM("");
    self.viewModel.Address1("");
    self.viewModel.Address2("");
    self.viewModel.Address3("");
    self.viewModel.CityName("");
    self.viewModel.StateProvince("");
    self.viewModel.Metro("");
    self.viewModel.CountryCode("");
    self.viewModel.MetroID("");
    self.viewModel.CountryID("");
    self.viewModel.PostalCode("");
    
    self.viewModel.IsBookNight(false);
    self.viewModel.IsEarlyCheckIn(false);
    self.viewModel.IsEarlyCheckIn(false);
    self.viewModel.IsSmoking(false);
    self.viewModel.IsRequestOnly(false);
    self.viewModel.ClubCard("");
    self.viewModel.RoomTypeID(null);
    
    self.viewModel.RoomDescription("Select");
    self.viewModel.IsRateCap(false);
    self.viewModel.ConfirmationStatus("");
    self.viewModel.NoofBeds("");
    self.viewModel.Comments("");
    
    self.viewModel.PreflightHotelListID(0);

 
}

function OnClientHotelClose(oWnd, args) {
    //get the transferred arguments

    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
                        

            self.viewModel.HotelID(arg.HotelID);
            //document.getElementById("tbHotelCode").value = arg.HotelCD;

            self.viewModel.HotelCode(arg.HotelCD);
            self.viewModel.PreflightHotelName(arg.Name);

                        
            if (arg.NegociatedRate != undefined && arg.NegociatedRate != "&nbsp;") {
                            
                self.viewModel.Rate(arg.NegociatedRate);
            }

            if (arg.HotelCD != null)
                document.getElementById("lbcvHotelCode").innerHTML = "";
            fireOnChange();
        }
        else {
                        

            self.viewModel.HotelID("");

            document.getElementById("tbHotelCode").value = "";

            self.viewModel.PreflightHotelName("");
            self.viewModel.Rate("0.00");
            self.viewModel.PhoneNum1("");
            self.viewModel.FaxNUM("");
        }
    }
}
function OnClientMetroClose(oWnd, args) {
    var combo = $find("tbMetro");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.viewModel.MetroID(arg.MetroID);
            document.getElementById("tbMetro").value = arg.MetroCD;
            if (arg.MetroCD != null)
                document.getElementById("lbcvMetro").innerHTML = "";
            fireOnChangeMetro();
        }
        else {
            self.viewModel.MetroID("");
            document.getElementById("tbMetro").value = "";
        }
    }
}
function OnClientCtryClose(oWnd, args) {
    var combo = $find("tbCtry");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.viewModel.CountryID(arg.CountryID);
            document.getElementById("tbCtry").value = arg.CountryCD;
            if (arg.CountryCD != null)
                document.getElementById("lbcvCtry").innerHTML = "";
            fireOnChangeCountry();
        }
        else {
            self.viewModel.CountryID("");
            document.getElementById("tbCtry").value = "";
        }
    }
}

function OnClientArriveTransportClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            Transport.ArrivalAirportId(self.Legs()[self.PaxTransportCurrentLeg() - 1].ArrivalAirport.AirportID());
            Transport.ArriveTransportID(arg.TransportID);
            Transport.ArrivalCode(arg.TransportCD);
            Transport.ArrivalName(arg.TransportationVendor);

            if (arg.NegotiatedRate != undefined && arg.NegotiatedRate != "&nbsp;")
                Transport.ArrivalRate(arg.NegotiatedRate);
            if (arg.TransportCD != null)
                document.getElementById("lbcvArriveTransCode").innerHTML = "";
            fireOnChangeArriveTransCode();
            isAfterGetTransportCall = false;
        }
        else {
            Transport.ArrivalAirportId("");
            Transport.ArriveTransportID(0);
            document.getElementById("lbArriveTransCode").innerHTML = "";
            Transport.ArrivalCode("");
            Transport.ArrivalName("");
            Transport.ArrivalPhone("");
            Transport.ArrivalFax("");
            Transport.ArrivalRate("");
        }
    }
}

function OnClientTransportClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            Transport.DepartureAirportId(self.Legs()[self.PaxTransportCurrentLeg() - 1].DepartureAirport.AirportID());
            Transport.DepartTransportID(arg.TransportID);
            Transport.DepartCode(arg.TransportCD);
            Transport.DepartName(arg.TransportationVendor);

            if (arg.NegotiatedRate != undefined && arg.NegotiatedRate != "&nbsp;")
                Transport.DepartRate(arg.NegotiatedRate);
            if (arg.TransportCD != null)
                document.getElementById("lbcvTransCode").innerHTML = "";
            fireOnChangeTransCode();
            isAfterGetTransportCall = false;
        }
        else {
            Transport.DepartAirportId(null);
            Transport.DepartTransportID(0);
            document.getElementById("lbTransCode").innerHTML = "";
            
            Transport.DepartCode("");
            Transport.DepartName("");

            
            Transport.DepartPhone("");
            Transport.DepartFax("");
            Transport.DepartRate("0.00");
        }
    }
}
function fireOnChangeTransCode() {
    if (IsNullOrEmpty($.trim(self.Transport.DepartCode()))) {
        self.Transport.DepartCode("");
        self.Transport.DepartName("");
        self.Transport.DepartPhone("");
        self.Transport.DepartFax("");
        self.Transport.DepartRate("");
        self.Transport.DepartureAirportId(null);
        self.Transport.DepartTransportID(0);
        document.getElementById("lbcvTransCode").innerHTML = "";
        SaveTransportDetails(self.PaxTransportCurrentLeg());
        return;
    }

    var hdnTransAirID = Transport.DepartAirportId;
    var hdnTransportID = Transport.DepartTransportID;
    var tbTransCode = Transport.DepartCode;
    var tbTransName = Transport.DepartName;
    var tbTransPhone = Transport.DepartPhone;
    var tbTransFax = Transport.DepartFax;
    var tbTransRate = Transport.DepartRate;
    var lbTransCode = document.getElementById("lbTransCode");
    var lbcvTransCode = document.getElementById("lbcvTransCode");
    var btnTransport = document.getElementById("btnTransport");
    var airportid = self.Preflight.PreflightLegs()[(self.PaxTransportCurrentLeg() - 1)].DepartICAOID();
    var TransCode = Transport.DepartCode();
    var ErrorMsg = "Depart Transport Code Does Not Exist";
    TransportValidation(TransCode, airportid, hdnTransAirID, hdnTransportID, tbTransCode, tbTransName, tbTransPhone, tbTransFax, tbTransRate, lbTransCode, lbcvTransCode, btnTransport, ErrorMsg);
}
function fireOnChangeArriveTransCode() {
    if (IsNullOrEmpty($.trim(self.Transport.ArrivalCode()))) {
        self.Transport.ArrivalCode("");
        self.Transport.ArrivalName("");
        self.Transport.ArrivalPhone("");
        self.Transport.ArrivalFax("");
        self.Transport.ArrivalRate("");
        self.Transport.ArrivalAirportId(null);
        self.Transport.ArriveTransportID(0);
        document.getElementById("lbcvArriveTransCode").innerHTML = "";
        SaveTransportDetails(self.PaxTransportCurrentLeg());
        return;
    }

    var hdnArriveTransAirID = Transport.ArrivalAirportId;
    var hdnArriveTransportID = Transport.ArriveTransportID;
    var tbArriveTransCode = Transport.ArrivalCode;
    var tbArriveTransName = Transport.ArrivalName;;
    var tbArrivePhone = Transport.ArrivalPhone;
    var tbArriveFax = Transport.ArrivalFax;
    var tbArriveRate = Transport.ArrivalRate;
    var lbArriveTransCode = document.getElementById("lbArriveTransCode");
    var lbcvArriveTransCode = document.getElementById("lbcvArriveTransCode");
    var btnArriveTrans = document.getElementById("btnArriveTrans");
    var airportid = self.Preflight.PreflightLegs()[(self.PaxTransportCurrentLeg() - 1)].ArriveICAOID();
    var TransCode = Transport.ArrivalCode();

    var ErrorMsg = "Arrival Transport Code Does Not Exist";
    TransportValidation(TransCode, airportid, hdnArriveTransAirID, hdnArriveTransportID, tbArriveTransCode, tbArriveTransName, tbArrivePhone, tbArriveFax, tbArriveRate, lbArriveTransCode, lbcvArriveTransCode, btnArriveTrans, ErrorMsg);
}
function NoPAX_OnCheckedChanged(ctrl) {
    if (ctrl.checked == true) {
        IsNoPAXSelected = true;

        $("#cb_PaxLegEntityGrid").prop("checked", 'false');
        $("#cb_PaxLegEntityGrid").trigger("click");
    } else {
        IsNoPAXSelected = false;
    }
    return true;
}

//  Pax Selection Usercontrol javascript


$(document).ready(function () {
    GetPaxLegs(false);

    self.EditMode.subscribe(function (newValue) {
        EnableDisableControlsofGrid(newValue);
    });

    $.extend(jQuery.jgrid.defaults, {
        prmNames: {
            page: "page", rows: "size", order: "dir", sort: "sort"
        }
    });

    $("#Submit1").click(function () {
        PaxCodeValidate();
        return false;
    });

    $("#SearchBoxPax").keypress(function (e) {
        if (e.keyCode == 13) {
            PaxCodeValidate();
            return false;
        }
    });

    $("#SearchBoxPax").blur(function () {
        var paxCds = $("#SearchBoxPax").val().split(',');
        AddPassenger(paxCds,false);
        return false;
    });

    $('#ddlFlightPurpose').on('change', function () {
        var SelectedValue = this.value;
        $(this).val("0");
        isItFireByMainPurposeDropDown = true;
        var paxids = $(PaxSelectionjqgridTableId).getGridParam("selarrrow");
        if (paxids.length > 0) {
            for (var i = 0; i < paxids.length; i++) {
                var rowData = jQuery(PaxSelectionjqgridTableId).getRowData(paxids[i]);
                var rowid = rowData["PassengerRequestorID"];
                var controls = $("select[row-id=" + rowid + "][disabled!=disabled]").val(SelectedValue);

            }
            $(this).val("0");
            Save_PaxInfo();
            isItFireByMainPurposeDropDown = false;
        } else {
            jAlert("Please select record.", "Preflight - PaxInfo");
            $(this).val("0");
        }
    });

});

function SaveOneFlightpurposeChange(ctrl) {
    if (!isItFireByMainPurposeDropDown) {

        var params = {};
        params.PaxCode = $(ctrl).attr("rowcode");
        params.legNum = $(ctrl).attr("LegNUM");
        params.FlightPurposeID = $(ctrl).val();
        ShowJqGridLoader($("#gbox_PaxSelectionID"));
        $.ajax({
            cache:false,
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePaxInfoOneFlightPurposeByLegNPax',
            async: true,
            data: JSON.stringify(params),
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                var returnResult = verifyReturnedResult(rowData);
                if (returnResult == true && rowData.d.Success == true) {
                    //setTimeout(function () {
                        HideJqGridLoader($("#gbox_PaxSelectionID"));
                        GetPaxLegs(true);
                        Save_Validation_PaxInfo();
                    //}, 2000);
                } else {
                    HideJqGridLoader($("#gbox_PaxSelectionID"));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //setTimeout(function () {
                    HideJqGridLoader($("#gbox_PaxSelectionID"));
                //}, 2000);
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }
}

function EnableDisableControlsofGrid(enable) {
    if (enable) {        
        $("#gview_PaxSelectionID input:not(.alwaysdisabled),#gview_PaxSelectionID select").removeAttr("disabled");
        $("#gbox_PaxsSummaryID input:not(.alwaysdisabled)").removeAttr("disabled");

    } else {
        $("#gview_PaxSelectionID input:not(.alwaysdisabled),#gview_PaxSelectionID select").attr("disabled", "disabled");
        $("#gbox_PaxsSummaryID input:not(.alwaysdisabled)").attr("disabled", "disabled");
    }

    $.each(LegsisDeadCategoryList, function (index, value) {
        if (value == true) {
            //$("#gview_PaxSelectionID select[leg-id=" + index + "]").attr("disabled", "disabled");
            $("#gview_PaxSelectionID select[leg-id=" + index + "]").val("0");
        }
    });

}
function PaxCodeValidate() {
    var PaxCD = document.getElementById("SearchBoxPax").value;
    var btn = document.getElementById("Submit1");
    PaxCodeValidation(PaxCD, btn, AddPassenger);
}

function Save_PaxInfo(arg) {

    if (arg != undefined && arg == false) {
        return false;
    }

    ShowJqGridLoader($("#gbox_PaxSelectionID"));
    OrderNumbers();
    UpdateModel_Legs();
    var dataArray = $(PaxSelectionjqgridTableId).jqGrid('getGridParam', 'data');
    if (dataArray != undefined) {
        for (var i = 0; i < dataArray.length; i++) {
            delete dataArray[i].PaxName;
        }

        $.each(dataArray, function () {
            this.PassengerAlert = replaceSpecialChars(this.PassengerAlert);
            this.Notes = replaceSpecialChars(this.Notes);
        });
    }
    var myJsonString = "'" + JSON.stringify(dataArray) + "'";
    var params = '{GridJson: ' + myJsonString + '}';
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePaxInfoGrid',
        async: false,
        data: params,
        contentType: 'application/json',
        success: function (rowData, textStatus, xhr) {
            var returnResult = verifyReturnedResult(rowData);
            if (returnResult == true && rowData.d.Success == true) {                
                PaxSelectionLegs = rowData.d.Result.results;
                GetPaxLegs();
                reloadPaxSelectionGrid();                                
            }
            HideJqGridLoader($("#gbox_PaxSelectionID"));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            setTimeout(function () {
                HideJqGridLoader($("#gbox_PaxSelectionID"));
            }, 2000);
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function Save_AfterWarningShow(callbackMethod)
{
    //SaveGridtoSessionAfterPaxWarning
    
    ShowJqGridLoader($("#gbox_PaxSelectionID"));
    OrderNumbers();
    UpdateModel_Legs();
    var dataArray = $(PaxSelectionjqgridTableId).jqGrid('getGridParam', 'data');

    var myJsonString = "'" + JSON.stringify(dataArray) + "'";
    var params = '{GridJson: ' + myJsonString + '}';
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SaveGridtoSessionAfterPaxWarning',
        async: true,
        data: params,
        contentType: 'application/json',
        success: function (rowData, textStatus, xhr) {
            var returnResult = verifyReturnedResult(rowData);
            if (returnResult == true && rowData.d.Success == true) {
                GetPaxLegs();
                reloadPaxSelectionGrid();
                HideJqGridLoader($("#gbox_PaxSelectionID"));
                callbackMethod();
            } else {
                HideJqGridLoader($("#gbox_PaxSelectionID"));
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            setTimeout(function () {
                HideJqGridLoader($("#gbox_PaxSelectionID"));
            }, 1000);
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function Save_Validation_PaxInfo() {
    var flag = false;
    var totalLegs = self.Legs().length;
    var legWiseNotFlightSelected = new Array();
    self.IsPaxInfoWarningConfirm(true);
    $(PaxSelectionjqgridTableId).find('select').each(function () {
        var value = $(this).val();
        if (value == "0") {            
            if (legWiseNotFlightSelected[$(this).attr("rowcode")] == undefined)
                legWiseNotFlightSelected[$(this).attr("rowcode")] = 0;

            legWiseNotFlightSelected[$(this).attr("rowcode")] = legWiseNotFlightSelected[$(this).attr("rowcode")] + 1;
            if (legWiseNotFlightSelected[$(this).attr("rowcode")] == totalLegs) {
                self.IsPaxInfoWarningConfirm(false);
                flag = true;
            }
        }
    });

    return flag;
}

function warningInPaxInfoSelection(callBackFunctionOnPositive)
{
    if (Save_Validation_PaxInfo() == false || EditMode()==false) {
        self.IsPaxInfoWarningConfirm(true);
        return callBackFunctionOnPositive();
        
    }
    setAlertTextToYesNo();
    
    jConfirm("Warning - Pax Not Assigned To Any Legs And Will Not Be Saved. Continue Without Saving Your Entry", "Preflight - PaxInfo", function (reply) {        
        if (reply) {
            Save_AfterWarningShow(function () {
                self.IsPaxInfoWarningConfirm(true);
                callBackFunctionOnPositive();
            });

        }
    });
    setAlertTextToOkCancel();
}

function OrderNumbers() {
    myGrid = $(PaxSelectionjqgridTableId);
    var dataArray = myGrid.jqGrid('getGridParam', 'data');
    for (var i = 0; i < dataArray.length; i++) {
        dataArray[i].OrderNUM = i + 1;
        myGrid.jqGrid('setCell', dataArray[i].PaxID, 'OrderNUM', i + 1);
    }
}

function UpdateModel_Legs() {

    $(PaxSelectionjqgridTableId).find('.chkNotified').each(function () {
        var rowid = $(this).attr("row-id")
        var value = $(this).attr('checked');
        $(PaxSelectionjqgridTableId).jqGrid('getLocalRow', rowid)["IsNotified"] = value;
    });
    $(PaxSelectionjqgridTableId).find('select').each(function () {

        var codeow = $(this).attr("rowcode");
        var rowid = $(this).attr("id").replace("ddl", "");
        var columnName = $(this).attr("leg-id");
        var value = $(this).val();


        var rowData = $(PaxSelectionjqgridTableId).jqGrid('getLocalRow', codeow);
        rowData[columnName] = value;
        $(PaxSelectionjqgridTableId).jqGrid('setRowData', codeow, rowData);
    });
}

function AddPassenger(paxCDs,isPassengerids) {
    if (!IsNullOrEmpty(paxCDs)) {
        var params = {};
        params.passengerRequestorIdentifiers = paxCDs;

        if (isPassengerids == undefined)
            isPassengerids = false;

        params.IsPassengerIds = isPassengerids;
        if (hdnInsert == "true") {
            params.isAddOrInsert = "Insert";
            var myGrid=$(PaxSelectionjqgridTableId);
            var selRowId=myGrid.jqGrid('getGridParam','selrow');
            if(selRowId!=undefined) {
                var orderNo=myGrid.jqGrid('getCell',selRowId,'OrderNUM');
                params.InsertAt = orderNo;
            } else
                params.InsertAt = 0;
        } else {
            params.isAddOrInsert="Add";
            params.InsertAt = 0;
        }
        ShowJqGridLoader($("#gbox_PaxSelectionID"));
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/AddNewPax',
            async: true,
            data: JSON.stringify(params),
            contentType: 'application/json',
            success: function (data) {
                var returnResult = verifyReturnedResult(data);
                if (returnResult == true && data.d.Success == true) {
                    $("#lblMsg").text(data.d.Result.Message);
                    if (data.d.Result.Added > 0) {
                        GetPaxLegs();
                        reloadPaxSelectionGrid();
                        Save_Validation_PaxInfo();
                        reloadPaxGrid();
                    }
                    setTimeout(function () {
                        $("#lblMsg").html("");
                        $("#SearchBoxPax").val("");
                    }, 1000);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                HideJqGridLoader($("#gbox_PaxSelectionID"));
                reportPreflightError(jqXHR, textStatus, errorThrown);
                setscrollpax();
            }
        }).done(function () {
            if (self.Preflight.PreflightMain.Company.IsEnableTSA() == false) {
                HideJqGridLoader($("#gbox_PaxSelectionID"));
                setscrollpax();
            }
        });

    }
}

function GetPaxLegs(isAsync) {
    LegPaxCodeFlightPurposeList = {};
    if (isAsync == undefined)
        isAsync = true;
    $.ajax({
        cache:false,
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/TripPaxSelectionLegs',
        async: false,
        contentType: 'application/json',
        success: function (rowData, textStatus, xhr) {
            PaxSelectionLegs = rowData.d.results;
            PaxPurposeList = rowData.d.PurposeList;
            LegsisDeadCategoryList = {};
            var width = 0;
            if (PaxSelectionLegs.length == 3) {
                width = 0;
            }
            else if (PaxSelectionLegs.length == 2) {
                width = 0;
            }
            else if (PaxSelectionLegs.length == 1) {
                width = 0;
                if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1) { width += 0; }
            }
            $('#ddlFlightPurpose').find('option').remove().end().append('<option value="0">All Legs</option>').append('<option value="0">Select</option>').val('0');

            $.each(PaxPurposeList, function (index, PaxPurposeVale) {
                $("#ddlFlightPurpose").append("<option  value='" + PaxPurposeVale + "' >" + index + "</option>");
            });
            if (self.Preflight.PreflightMain.Company.IsEnableTSA() == true) {
                ColNames.push("Status");
                ColModel.push({
                    name: "Status", title: "Status", index: "Status", width: 100,  hidden: true,
                    formatter: function (cellvalue, options, rowObject) {
                        var a = "";
                        var PassengerRequestorID = rowObject["PassengerRequestorID"];
                        var params = JSON.stringify({ passengerRequestorID: PassengerRequestorID });
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CheckTSA',
                            async: false,
                            data: params,
                            contentType: 'application/json',
                            success: function (rowData, textStatus, xhr) {
                                var returnResult = verifyReturnedResult(rowData);
                                if (returnResult == true && rowData.d.Success == true) {
                                    if (rowData.d.Result.hasOwnProperty("Status"))
                                        a = rowData.d.Result.Status + '|' + rowData.d.Result.BackColor;
                                    else
                                        a = " | ";
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                reportPreflightError(jqXHR, textStatus, errorThrown);
                            }
                        });
                        return a;
                    }
                });
            }
            $.each(PaxSelectionLegs, function (index, Leg) {

                var LegNum = "Leg" + Leg.LegNUM;
                var LegName = LegNum + ":     " + Leg.LegDepart;
                var TotalPax = 0;
                var ReservationAvailable = 0;
                var LegPaxList = Leg.PaxList;
                var SeatTotal = 0;
                var TotalAvail = 0;
                var BlockedSeats = 0;
                var PassengerTotalWithPurpose = 0;

                LegPaxCodeFlightPurposeList[LegNum] = LegPaxList;


                if (Leg.PassengerTotal) {
                    TotalPax = Leg.PassengerTotal;
                }
                if (Leg.SeatTotal) {
                    SeatTotal = Leg.SeatTotal;
                }
                if (Leg.ReservationAvailable) {
                    ReservationAvailable = Leg.ReservationAvailable;
                    TotalAvail = SeatTotal - TotalPax;
                }
                if (LegPaxList) {                    
                    if (Object.keys(LegPaxList).length >= 0) {
                        PassengerTotalWithPurpose = Object.keys(LegPaxList).length;
                        if ((TotalPax - PassengerTotalWithPurpose) > 0) {
                            BlockedSeats = TotalPax - PassengerTotalWithPurpose;
                        }
                    }
                }
                var TotalPaxBooked = "PAX booked:   <span style='color:#444444;font-family:Arial;font-size:11px;' class='small_text' id='lblLeg" + Leg.LegNUM + "p' defaultvalue='" + Leg.PassengerTotal + "' >" + Leg.PassengerTotal + "</span>"
                var TotalSeatsAvail = "Seats Avail:   <span style='color:#444444;font-family:Arial;font-size:11px;' class='small_text' id='lblLeg" + Leg.LegNUM + "a' defaultvalue='" + Leg.ReservationAvailable + "'>" + Leg.ReservationAvailable + "</span>"
                var fitwidthdivholder = $('<div></div>');
                var paddingcount = 5;
                if (isWindowsSafari() || isMacSafari())
                    paddingcount += 10;
                if (Leg.LegDepart.trim().length<8)
                    paddingcount += 6;


                $(fitwidthdivholder).css("display", "inline-block");
                
                $(fitwidthdivholder).html(TotalPaxBooked + "<br/>" + TotalSeatsAvail + "<br/>" + LegName);
                $('body').append(fitwidthdivholder);
                var fitWidth = fitwidthdivholder.width();
                $(fitwidthdivholder).remove();
                updatePaxBookedAndAvailableValues(Leg.LegNUM, Leg.PassengerTotal, Leg.ReservationAvailable);

                userdata[LegNum] = " <input type='text' onkeypress='return fnAllowNumeric(this,event)' style='color:#444444;font-family:Arial;font-size:12px;width:60px;margin-left:5px' value='" + BlockedSeats + "' id='lblLeg" + Leg.LegNUM + "f' defaultvalue='" + BlockedSeats + "' onblur=blockSeatValueChange(this,'" + Leg.LegNUM + "','lblLeg" + Leg.LegNUM + "p','lblLeg" + Leg.LegNUM + "a','" + BlockedSeats + "') ' />";
                ColNames.push(TotalPaxBooked + "<br/>" + TotalSeatsAvail + "<br/>" + LegName);
                ColModel.push({
                    name: LegNum, title: false, index: LegNum, width: (fitWidth + paddingcount + width), frozen: true, sortable: false, search: false,align:'center',classes:'removepad',cellLayout:0,
                    formatter: function (cellvalue, options, rowObject) {
                        if (cellvalue) {
                            if (cellvalue.indexOf("input") > 0)
                            { return cellvalue; }
                        }
                        var PaxList = LegPaxCodeFlightPurposeList[LegNum];
                        var isDeadCategory = LegsisDeadCategoryList[LegNum];
                        var FlightPurposeID = "";
                        FlightPurposeID = PaxList[rowObject["PassengerRequestorCD"]]
                                                
                        

                        var Html = "<select onchange='SaveOneFlightpurposeChange(this)';  rowcode='" + rowObject["PassengerRequestorCD"] + "'  row-id='" + rowObject["PassengerRequestorID"] + "' LegNUM='" + Leg.LegNUM + "' leg-id='" + LegNum + "'  id='ddlLeg" + LegNum + "'  >";
                        Html += "<option value='0'>Select</option>";
                        $.each(PaxPurposeList, function (index, PaxPurposeVale) {
                            if (FlightPurposeID == PaxPurposeVale) {
                                Html += "<option selected='selected' value='" + PaxPurposeVale + "' >" + index + "</option>";
                            } else {
                                Html += "<option  value='" + PaxPurposeVale + "' >" + index + "</option>";
                            }
                        });
                        Html += "</select>";
                        return Html;
                    }
                }); 
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function blockSeatValueChange(ctrl, legnum, paxbookedseatid, availableseatid, defaultBlockValue) {
    if(ctrl.value==""){
        ctrl.value=0;
    }
    if(defaultBlockValue=="" || defaultBlockValue==null)
        defaultBlockValue=0;

    var requireChangeSeats = parseInt(ctrl.value) - parseInt($(ctrl).attr('defaultvalue'));
    var paxbooked = parseInt($("#" + paxbookedseatid).attr("defaultvalue")) + requireChangeSeats;
    var availableSeats = parseInt($("#" + availableseatid).attr("defaultvalue")) - requireChangeSeats;
    $("#" + paxbookedseatid).html(paxbooked);
    $("#" + availableseatid).html(availableSeats);

    if (paxbooked == null || ctrl.value == null)
        return;

    var params = JSON.stringify({ legNum: legnum, PaxBooked: paxbooked, BlockedSeatCount: parseInt(ctrl.value) });
    ShowJqGridLoader($("#gbox_PaxSelectionID"));
    $(ctrl).attr('defaultvalue', ctrl.value);
    $("#" + paxbookedseatid).attr("defaultvalue", paxbooked);
    $("#" + availableseatid).attr("defaultvalue", availableSeats);

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePaxInfoBlockedSeatsChange',
        async: true,
        data: params,
        contentType: 'application/json',
        success: function (rowData, textStatus, xhr) {
            var returnResult = verifyReturnedResult(rowData);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            HideJqGridLoader($("#gbox_PaxSelectionID"));
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    }).done(function () {
        HideJqGridLoader($("#gbox_PaxSelectionID"));
    });


}

function updatePaxBookedAndAvailableValues(legnum, passengerTotal, availableTotal) {
    $("#lblLeg" + legnum + "p").html(passengerTotal);
    $("#lblLeg" + legnum + "a").html(availableTotal);

    $("#lblLeg" + legnum + "p").attr("defaultvalue", passengerTotal);
    $("#lblLeg" + legnum + "a").attr("defaultvalue", availableTotal);

}

$(document).ready(function () {

    ShowJqGridLoader($("#gbox_PaxSelectionID"));
    var iswinsafari = isWindowsSafari();

    jQuery(PaxSelectionjqgridTableId).jqGrid({
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/PaxSelectionGrid',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: false,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }
            postData._search = undefined;
            postData.nd = undefined;
            postData.size = undefined;
            postData.page = undefined;
            postData.sort = undefined;
            postData.dir = undefined;
            postData.filters = undefined;

            return JSON.stringify(postData);
        },
        height: 200,
        width: 700,
        autowidth: false,
        shrinkToFit: false,
        rowNum: 10000,
        multiselect: true,
        pager: "#pg_gridPagerPaxSelection",
        pgbuttons: false,
        viewrecords: false,
        pgtext: "",
        pginput: false,
        colNames: ColNames,
        colModel: ColModel,
        cmTemplate: { resizable: !iswinsafari },
        loadui: 'disable',
        userdata: userdata,
        footerrow: true,
        sortname:"OrderNUM",
        userDataOnFooter: true,
        ondblClickRow: function (rowId) {
            var rowData = jQuery(this).getRowData(rowId);
        },
        beforeSelectRow: function (rowid, e) {
            if (self.EditMode())
                return true;
            else
                return false;
        },
        beforeRequest: function () {
            CreateJqGridLoader($("#gbox_PaxSelectionID"));
            ShowJqGridLoader($("#gbox_PaxSelectionID"));
        },
        gridComplete: function () {
            //UpdateModel_Legs();
        },
        loadComplete: function (rowData) {
            //JqgridCreateSelectAllOption(true, PaxSelectionjqgridTableId);
            JqgridCreateSelectAllOptionFroze();
            EnableDisableControlsofGrid(self.EditMode());            
            LoadPaxSummaryExpand();
            paxGridFrozenColAlign();

            if (IsPaxInfoGridLoded == false) {
                paxGridFrozenColAlign();
                setscrollpax();
            } else {
                paxGridFrozenColAlignAfercolAdd();
                setscrollpax();
            }

            Save_Validation_PaxInfo();
            var trNo = 0;
            $("#PaxSelectionID").find('tr').each(function () {
                if ($(this).find("td[aria-describedby='PaxSelectionID_Status']").length>0) {
                    $($("#PaxSelectionID_frozen").find('tr')[trNo]).find("td[aria-describedby='PaxSelectionID_PaxName']").css("background-color", $(this).find("td[aria-describedby='PaxSelectionID_Status']").html().split('|')[1]);
                    $($("#PaxSelectionID_frozen").find('tr')[trNo]).find("td[aria-describedby='PaxSelectionID_PaxName']").attr("title", $(this).find("td[aria-describedby='PaxSelectionID_Status']").html().split('|')[0]);
                    $($("#PaxSelectionID_frozen").find('tr')[trNo]).find("td[aria-describedby='PaxSelectionID_PaxName']").css("border-color","grey" );
                    $(this).find("td[aria-describedby='PaxSelectionID_PaxName']").css("background-color", $(this).find("td[aria-describedby='PaxSelectionID_Status']").html().split('|')[1]);
                    $(this).find("td[aria-describedby='PaxSelectionID_PaxName']").attr("title", $(this).find("td[aria-describedby='PaxSelectionID_Status']").html().split('|')[0]);
                    $(this).find("td[aria-describedby='PaxSelectionID_PaxName']").css("border-color", "grey");
                }
                trNo++;
            });
            HideJqGridLoader($("#gbox_PaxSelectionID"));
        }
    });
    $(PaxSelectionjqgridTableId).jqGrid('setFrozenColumns');
//    $(PaxSelectionjqgridTableId).jqGrid('filterToolbar', { autosearch: true, defaultSearch: 'cn' });
    $(PaxSelectionjqgridTableId).jqGrid('navGrid', '#pg_gridPagerPaxSelection', { resize: false, add: false, search: false, del: false, refresh: false, edit: false });
    $(PaxSelectionjqgridTableId).jqGrid('footerData', 'set', userdata);
    $(PaxSelectionjqgridTableId).jqGrid('navButtonAdd', "#pg_gridPagerPaxSelection", {
        caption: "", title: "Delete Selected Pax(s)", buttonicon: "ui-icon-trash",
        onClickButton: function () {
            
            var myGrid = $(PaxSelectionjqgridTableId);
            var selRowIds = myGrid.jqGrid('getGridParam', 'selarrrow');
            if (selRowIds.length > 0) {

                setAlertTextToYesNo();
                jConfirm("Are you sure you want to delete this record?", "Confirmation", function (r) {

                    if (r) {
                        var myGrid = $(PaxSelectionjqgridTableId);
                        var selRowIds = myGrid.jqGrid('getGridParam', 'selarrrow');
                        var paxCDs = new Array();
                        for (var i = 0; i < selRowIds.length; i++) {                            
                            paxCDs.push(selRowIds[i]);
                        }
                        ShowJqGridLoader($("#gbox_PaxSelectionID"));
                        var params = {}; params.passengerRequestorCDs = paxCDs;
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/DeletePax',
                            async: true,
                            data: JSON.stringify(params),
                            contentType: 'application/json',
                            success: function (rowData, textStatus, xhr) {
                                var returnResult = verifyReturnedResult(rowData);
                                if (returnResult == true && rowData.d.Success == true) {
                                    GetPaxLegs();
                                    reloadPaxSelectionGrid();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                HideJqGridLoader($("#gbox_PaxSelectionID"));
                                reportPreflightError(jqXHR, textStatus, errorThrown);
                            }
                        }).done(function () {
                            HideJqGridLoader($("#gbox_PaxSelectionID"));
                        });


                    }

                });
                setAlertTextToOkCancel();
            } else {
                jAlert("Please select the record to delete!", alertTitle);
            }
            
            return;

        }
    });
    $(PaxSelectionjqgridTableId).jqGrid('navButtonAdd', "#pg_gridPagerPaxSelection", {
        caption: "", title: "Insert Pax", buttonicon: "ui-icon-insert-file",
        onClickButton: function () {
            ShowPaxInfoPopup('insert');
            return;
        }
    });
    $(PaxSelectionjqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });//.trigger('reloadGrid');
});

function setscrollpax() {

    if (isWindowsSafari()) {

        if ($('#PaxSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

            $('#PaxSelection div[class="ui-jqgrid-bdiv"]').scroll(function () {
                if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                    $('#PaxSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                    $('#PaxSelection .ui-jqgrid-ftable').eq(0).css('padding-right', '17px');
                } else {

                    $('#PaxSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');

                }
            });
        }

    }
    else if (isMacSafari()) {


        if ($('#PaxSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

            $('#PaxSelection div[class="ui-jqgrid-bdiv"]').scroll(function () {
                if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                    $('#PaxSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                    $('#PaxSelection .ui-jqgrid-ftable').eq(0).css('padding-right', '17px');
                } else {

                    $('#PaxSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');

                }
            });
        }

    }
    else {

        if ($('#PaxSelection div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

            $('#PaxSelection .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
            $('#PaxSelection .ui-jqgrid-ftable').eq(0).css('padding-right', '17px');
        }
    }
}


function paxGridFrozenColAlign() {
    if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1) {
        var mainWidth = $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width();

        if (PaxSelectionLegs.length <=3) {
            mainWidth += 2;
            $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv").width(mainWidth);
            $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width(mainWidth);
            $(".preflightpax_selectionlist div.frozen-bdiv, .preflightpax_information div.frozen-bdiv").width(mainWidth + 1);
            $("#PaxSelectionID_frozen").width(mainWidth + 1);
        }
        if (PaxSelectionLegs.length > 3) {
            mainWidth += 3;
            $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv").width(mainWidth);
            $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width(mainWidth);
            $(".preflightpax_selectionlist div.frozen-bdiv, .preflightpax_information div.frozen-bdiv").width(mainWidth + 1);
            $("#PaxSelectionID_frozen").width(mainWidth + 1);
        }
        IsPaxInfoGridLoded = true;
    }
}

function paxGridFrozenColAlignAfercolAdd() {
    if (agt.indexOf("safari") != -1 && agt.indexOf("chrome") == -1) {
        var mainWidth = $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width();
        if (PaxSelectionLegs.length > 3) {
            mainWidth += 3;
            $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv").width(mainWidth);
            $(".preflightpax_selectionlist .frozen-div.ui-state-default.ui-jqgrid-hdiv>table").width(mainWidth);
            $(".preflightpax_selectionlist div.frozen-bdiv, .preflightpax_information div.frozen-bdiv").width(mainWidth + 1);
            $("#PaxSelectionID_frozen").width(mainWidth + 1);
        }


        IsPaxInfoGridLoded = true;
    }
}

function JqgridCreateSelectAllOptionFroze() {
    $(PaxSelection).find("#jqgh_PaxSelectionID_cb").find("span").remove();
    var colWidth = "35px";
    if (isWindowsSafari() || isMacSafari())
        colWidth = "42px";

    //$("#gview_PaxSelectionID input[type=checkbox]").css("width", "50px");
    $(PaxSelection).find(".frozen-div table thead tr").find("#jqgh_PaxSelectionID_cb").find("span").remove();
    $(PaxSelection).find(".frozen-div table thead tr").find("#jqgh_PaxSelectionID_cb").prepend("<span> Select <br/> All <br/></span>");
    $(PaxSelection).find(".frozen-div table thead tr").children().first("td").css("width", colWidth);
    

    $(PaxSelection).find("#PaxSelectionID_cb").css("width", colWidth);
   // $(PaxSelection).find(".jqgfirstrow td:first-child,td[aria-describedby='PaxSelectionID_cb']").css("width", "50px");
    $(PaxSelection).find(".jqgfirstrow td:first-child,td[aria-describedby='PaxSelectionID_cb']").attr("style", "width: " + colWidth + ";text-align: center!important;");
    $(PaxSelection).find("#PaxSelectionID_frozen_cb").css("width", colWidth);
    $(PaxSelection).find("#PaxSelectionID_frozen tbody tr").children().first("td").css("width", colWidth);

    $(PaxSelection).find("#gview_PaxSelectionID").append("<div id='fixedBlockSeats' style='width:265px !important;'> Blocked Seats</div>");
}
// this function is used to display the aircraft type popup 
function ShowPaxRosterPopup(ParameterCall) {
    
    if (ParameterCall == 'insert') {
        document.getElementById('hdnInsert').value = "true";
    }
    else {
        document.getElementById('hdnInsert').value = "false";
        var oWnd = radopen("/Views/Transactions/Preflight/PreflightPaxRosterPopup.aspx?PaxID=" + document.getElementById('hdnPaxInfoId').value + "&PaxCD=" + document.getElementById('hdnPaxCD').value + "", "radPaxRosterPopup"); // Added for Preflight Pax Tab
        return false;
    }
}

// this function is used to display the PaxInfo popup 
function ShowPaxInfoPopup(ParameterCall) {
    if (ParameterCall == 'insert') {
        hdnInsert = "true";
    }
    else
        hdnInsert = "false";

    var oWnd = radopen("/Views/Transactions/Preflight/PaxInfoPopupJqgrid.aspx?IsUIReports=true", "radPaxInfoPopup"); // Added for Preflight Pax Tab

    //var oWnd = radopen("/Views/Settings/People/PassengerRequestorsPopup.aspx", "radPaxInfoPopup"); // Added for Preflight Pax Tab
    oWnd.Close = function () {
        document.getElementById("SearchBoxPax").value = "Popup";
        PaxCodeValidate();
    };

    return false;
}
function OnClientClosePaxInfoPopup(oWnd, args) {
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg.PassengerRequestorID.length > 0) {
            var paxIds = arg.PassengerRequestorID.split(',');
            AddPassenger(paxIds,true);
        }
    }
}

// this function is used to display the details of a particular PAX
function ShowPaxDetailsPopup(Paxid) {
    if (self.EditMode() == true)
    {
        var oWnd = window.radopen("/Views/Settings/People/PassengerCatalog.aspx?IsPopup=&OpenFrom=preflightPax&PassengerRequestorID=" + Paxid, "rdPaxPage");
        oWnd.add_close(OnPassengerCatalogPopup);
    }
    else {
        window.radopen("/Views/Transactions/Preflight/PreflightPassengerPopup.aspx?PaxID=" + Paxid, "rdPaxPage");
    }
    reloadPaxSelectionGrid();
    
    return false;
}

function OnPassengerCatalogPopup(oWnd, args) {
    reloadPaxSelectionGrid();
}

function CheckTSAClick() {
    var paxids = $(PaxSelectionjqgridTableId).getGridParam("selarrrow");
    
    if (paxids.length > 0) {
        var rowData = jQuery(PaxSelectionjqgridTableId).getRowData(paxids[0]);
        var PassengerRequestorID = rowData["PassengerRequestorID"];

        var params = JSON.stringify({ passengerRequestorID: PassengerRequestorID });
        ShowJqGridLoader($("#gbox_PaxSelectionID"));

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/CheckTSA',
            async: true,
            data: params,
            contentType: 'application/json',
            success: function (rowData, textStatus, xhr) {
                var returnResult = verifyReturnedResult(rowData);
                if (returnResult == true && rowData.d.Success == true) {
                    if (rowData.d.Result.hasOwnProperty("Status"))
                        $("#tbStaus").val(rowData.d.Result.Status);

                    if (rowData.d.Result.hasOwnProperty("BackColor")) {
                        $("#tbStaus").css("background-color", rowData.d.Result.BackColor);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {                
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        }).done(function () {
            HideJqGridLoader($("#gbox_PaxSelectionID"));
        });



    } else {
        jAlert("Please select the record.", "Preflight - Trip Alert!");
    }

}

function reloadPaxSelectionGrid() {
    $(PaxSelectionjqgridTableId).jqGrid('setGridParam', {
        datatype: 'json'
    }).trigger('reloadGrid');
}

// Pax Summary Usercontrol javascript

$(document).ready(function () {

    $.extend(jQuery.jgrid.defaults, {
        prmNames: {
            page: "page", rows: "size", order: "dir", sort: "sort"
        }
    });
});

$(document).ready(function () {
    jQuery(PaxsSummaryjqgridTableId).jqGrid({
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/PaxSummaryGrid',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }
            postData._search = undefined;
            postData.nd = undefined;
            postData.size = undefined;
            postData.page = undefined;
            postData.sort = undefined;
            postData.dir = undefined;
            postData.filters = undefined;

            return JSON.stringify(postData);
        },
        height: 200,
        width: 700,
        autowidth: false,
        shrinkToFit: false,
        rowNum: 10000,
        multiselect: false,
        pager: "#pg_gridPagerPaxsSummary",
        pgbuttons: false,
        viewrecords: false,
        pgtext: "",
        pginput: false,
        datatype: "local",
        colNames: ['PassengerID', 'Code', 'Name', 'Leg No.', 'Street', 'City', 'State', 'Postal', 'Passport', 'Billing Info', 'Nationality', 'Expiration Date', 'Choice', 'Issue Date'],
        colModel: [
                    { name: 'PassengerID', index: 'PassengerID', key: true, hidden: true, sortable: false },
                    {
                        name: 'PaxCode', index: 'PaxCode', width: 80, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                            return '<span ID="lnkPaxCode" >' + cellvalue + '</span>';
                        }
                    },
                   { name: 'PaxName', index: 'PaxName', width: 150, sortable: false, search: false },
                   { name: 'LegNUM', index: 'LegNUM', width: 60, sortable: true, search: false, firstsortorder: 'asc' },
                   {
                       name: 'Street', index: 'Street', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                           var onchange = "onchange='UpdateSummary(this);'";
                           return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;" ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["PassengerID"] + '" leg-no="' + rowObject["LegNUM"] + '" col="Street"  data-bind="enable: EditMode"/>';
                       }
                   },
                   {
                       name: 'CityName', index: 'CityName', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                           var onchange = "onchange='UpdateSummary(this);'";
                           return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"  ' + onchange + '  value="' + ((cellvalue == null || cellvalue == 'undefined') ? "" : cellvalue) + '" row-id="' + rowObject["PassengerID"] + '" leg-no="' + rowObject["LegNUM"] + '" col="City"  data-bind="enable: EditMode"/>';
                       }
                   },
                   {
                       name: 'StateName', index: 'StateName', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                           var onchange = "onchange='UpdateSummary(this);'";
                           return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"    ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["PassengerID"] + '" leg-no="' + rowObject["LegNUM"] + '" col="State"  data-bind="enable: EditMode"/>';
                       }
                   },
                   {
                       name: 'PostalZipCD', index: 'PostalZipCD', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                           var onchange = "onchange='UpdateSummary(this);'";
                           return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"   ' + onchange + '  value="' + cellvalue + '" row-id="' + rowObject["PassengerID"] + '" leg-no="' + rowObject["LegNUM"] + '" col="Postal"  data-bind="enable: EditMode"/>';
                       }
                   },

                   {
                       name: 'Passport', index: 'Passport', width: 130, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                           var ShowPaxPassport = "return ShowPaxPassportPopup('" + rowObject["PassengerID"] + "','" + rowObject["LegNUM"] + "','" + rowObject["PaxName"] + "');";
                           var Content = "";
                           Content = Content + '<input type="hidden" id="hdnVisaID" value="' + rowObject["VisaID"] + '">';
                           Content = Content + '<input type="hidden" id="hdnPassID" value="' + rowObject["PassengerID"] + '">';
                           Content = Content + '<input type="hidden" id="hdnPassNum" value="' + rowObject["PassportNum"] + '">';
                           Content = Content + '<input type="hidden" id="hdnPaxName" value="' + rowObject["PaxName"] + '">';
                           Content = Content + '<input type="hidden" id="hdnLegNum" value="' + rowObject["LegNUM"] + '">';
                           Content = Content + '<input type="text" style="font-family:Arial;font-size:12px;"  class="alwaysdisabled aspNetDisabled tdLabel70" disabled="disabled" id="tbPassport' + rowObject["PassengerID"] + rowObject["LegNUM"] + '" value="' + ((rowObject["PassportNum"] == null) ? '' : rowObject["PassportNum"]) + '"  data-bind="enable: EditMode">';
                           Content = Content + '<a style="display:inline-block;height:16px;width:16px;" href="#" class="browse-button" id="lnkPassport" onclick="' + ShowPaxPassport + '" data-bind="enable: EditMode,css: BrowseBtn"></a>';
                           return Content;
                       }
                   },
                   {
                       name: 'Billing', index: 'Billing', width: 120, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                           var onchange = "onchange='UpdateSummary(this);'";
                           var value = "";
                           if (cellvalue) {
                               value = cellvalue;
                           }
                           return '<input type="text" style="font-family:Arial;font-size:12px;width:95%;"   ' + onchange + '  value="' + value + '" row-id="' + rowObject["PassengerID"] + '" leg-no="' + rowObject["LegNUM"] + '" col="Billing"  data-bind="enable: EditMode"/>';
                       }
                   },
                   { name: 'Nation', index: 'Nation', width: 80, sortable: false, search: false },
                   { name: 'PassportExpiryDT', index: 'PassportExpiryDT', width: 100, sortable: false, search: false },
                   {
                       name: 'Choice1', index: 'Choice1', width: 50, sortable: false, search: false, formatoptions: { disabled: true },
                       formatter: function (cell, options, rowObject) {
                           return "<input type='checkbox' checked='" + rowObject["CrewID"] + "' offval='no' disabled='disabled' class='alwaysdisabled'>";
                       }
                   },
                  { name: 'IssueDT', index: 'IssueDT', width: 100, sortable: false, search: false }

        ],
        ondblClickRow: function (rowId) {
            var rowData = jQuery(this).getRowData(rowId);
        },
        onSelectRow: function (id) {
            var rowData = $(this).getRowData(id);
        },
        gridComplete: function () {
            setTimeout(function () {
                if (sortFlag) {
                    //DisplayRows();
                    $(PaxsSummaryjqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
                    $(PaxsSummaryjqgridTableId).jqGrid("sortGrid", "Leg", true, "asc");
                    sortFlag = false;
                }
            }, 1000);
        },
        loadComplete: function (rowData) {
            $('#gbox_PaxsSummaryID input[value="null"]').val("");
            EnableDisableControlsofGrid(self.EditMode());

            if (isWindowsSafari()) {

                if ($('#PaxsSummary div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                    $('#PaxsSummary div[class="ui-jqgrid-bdiv"]').scroll(function () {
                        if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                            $('#PaxsSummary .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                        } else {

                            $('#PaxsSummary .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');

                        }
                    });
                }

            }
            else if (isMacSafari()) {


                if ($('#PaxsSummary div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                    $('#PaxsSummary div[class="ui-jqgrid-bdiv"]').scroll(function () {
                        if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                            $('#PaxsSummary .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                        } else {

                            $('#PaxsSummary .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');

                        }
                    });
                }

            }
            else {
                
                if ($('#PaxsSummary div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                    $('#PaxsSummary .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                }
            }



        }
    });

});

// this function is used to display the Pax passport visa type popup 
function ShowPaxPassportPopup(PassengerID, Leg, PaxName) {
    //window.radopen("/Views/Transactions/Preflight/PaxPassportVisa.aspx?PassengerID=" + PassengerID, "rdPaxPassport");
    window.radopen("/Views/Transactions/Preflight/PaxPassportVisa.aspx?PassengerRequestorID=" + PassengerID + "&Leg=" + Leg + "&PaxName=" + PaxName, "rdPaxPassport");
    hdnPassPax = PassengerID;
    hdnAssignPassLeg = Leg;
    return false;
}

function OnClientVisaClose(oWnd, args) {
    var arg = args.get_argument();
    if (arg != null) {
        SendSaveSummary(hdnAssignPassLeg, hdnPassPax, "PassportID", arg.passportID);
        SendSaveSummary(hdnAssignPassLeg, hdnPassPax, "passportNum", arg.passportNum);
        $("#tbPassport" + arg.PassengerID + hdnAssignPassLeg).val(arg.passportNum);
    }
    return false;
}
function DisplayRows() {
    myGrid = $(PaxsSummaryjqgridTableId);
    var dataArray = myGrid.jqGrid('getGridParam', 'data');
    for (var i = 0; i < dataArray.length; i++) {
        if (dataArray[i].Display == "False" || dataArray[i].Display == null) {
            myGrid.jqGrid('delRowData', dataArray[i].PassengerID);
        }
    }
}

function UpdateSummary(control) {
    var LegNumber = control.getAttribute("leg-no");
    var PassengerID = control.getAttribute("row-id");
    var col = control.getAttribute("col");
    var val = control.value;
    SendSaveSummary(LegNumber, PassengerID, col, val);
}

function SendSaveSummary(LegNumber, PassengerID, col, val) {
    $("#UpdateLoading").show();
    var params = '{ColName: "' + col + '",Value: "' + val + '",PassengerID: "' + PassengerID + '",LegNumber: ' + LegNumber + '}';
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePaxSummaryCells',
        async: false,
        data: params,
        contentType: 'application/json',
        success: function (rowData, textStatus, xhr) {
            var returnResult = verifyReturnedResult(rowData);
            if (returnResult == true && rowData.d.Success == true) {
                PaxSelectionLegs = rowData.d.Result.results;
                $("#UpdateLoading").hide();
                setTimeout(function () {
                    $("#UpdateLoading").hide();
                }, 2000);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            setTimeout(function () {
                $("#UpdateLoading").hide();
            }, 2000);
            reportPreflightError(jqXHR, textStatus, errorThrown);
        }
    });
}

function LoadPaxSummaryExpand() {    
    sortFlag = true;
    $(PaxsSummaryjqgridTableId).setGridParam({ datatype: 'json' }).trigger('reloadGrid');
}
