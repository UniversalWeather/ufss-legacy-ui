﻿$(document).ready(function () {
    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $(".tabStrip li").eq(0).removeClass("tabStripItem").addClass("tabStripItemSelected");
    var dgPower = document.getElementById("dgPowerSetting");
    PowerSettingGrid_Validate_Retrieve(dgPower);
    $("#rmtbLocaltime").mask("99:99").change(function () { self.CurrentLegData.DepartureLocalTime($(this).val()); });
    $("#rmtUtctime").mask("99:99").change(function () { self.CurrentLegData.DepartureGreenwichTime($(this).val()); });
    $("#rmtHomeTime").mask("99:99").change(function () { self.CurrentLegData.HomeDepartureLocal($(this).val()); });
    $("#rmtArrivalTime").mask("99:99").change(function () { self.CurrentLegData.ArrivalLocalTime($(this).val()); });
    $("#rmtArrivalUtctime").mask("99:99").change(function () { self.CurrentLegData.ArrivalGreenwichTime($(this).val()); });
    $("#rmtArrivalHomeTime").mask("99:99").change(function () { self.CurrentLegData.HomeArrivalTime($(this).val()); });
    $(".textDatePicker").datepicker({
        showOn: 'input',
        changeMonth: true,
        changeYear: true,
        dateFormat: self.UserPrincipal._ApplicationDateFormat.toLowerCase().replace(/yyyy/g, "yy")
    });

    $("#tbLocalDate").click(function () { $("#tbLocalDate").datepicker('show'); });
    $("#tbUtcDate").click(function () { $("#tbUtcDate").datepicker('show'); });
    $("#tbHomeDate").click(function () { $("#tbHomeDate").datepicker('show'); });
    $("#tbArrivalDate").click(function () { $("#tbArrivalDate").datepicker('show'); });
    $("#tbArrivalUtcDate").click(function () { $("#tbArrivalUtcDate").datepicker('show'); });
    $("#tbArrivalHomeDate").click(function () { $("#tbArrivalHomeDate").datepicker('show'); });

    $("#rmtbLocaltime").click(function () { $("#rmtbLocaltime").timepicker('show'); });
    $("#rmtUtctime").click(function () { $("#rmtUtctime").timepicker('show'); });
    $("#rmtHomeTime").click(function () { $("#rmtHomeTime").timepicker('show'); });
    $("#rmtArrivalTime").click(function () { $("#rmtArrivalTime").timepicker('show'); });
    $("#rmtArrivalUtctime").click(function () { $("#rmtArrivalUtctime").timepicker('show'); });
    $("#rmtArrivalHomeTime").click(function () { $("#rmtArrivalHomeTime").timepicker('show'); });
    checklegnotes();
});
$(document).on("click", ".tabStripItem", function () {
    $(".tabStrip").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
    $(this).removeClass("tabStripItem").addClass("tabStripItemSelected");
});

var currentTextBox = null;
var currentDatePicker = null;

function DateTimeChanged(data, event) {
    var time = null;
    var changedValue = null;
    var currenttextboxId = event.target.id;
    var isDepartOrArr = null;
    var isPostback;
    switch (currenttextboxId) {
        case "tbLocalDate":
            time = $("#rmtbLocaltime").val();
            changedValue = "Local";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "rmtbLocaltime");
            break;
        case "rmtbLocaltime":
            time = $("#rmtbLocaltime").val();
            changedValue = "LocalTime";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "rmtbLocaltime");
            break;
        case "tbUtcDate":
            time = $("#rmtUtctime").val();
            changedValue = "UTC";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "rmtUtctime");
            break;
        case "rmtUtctime":
            time = $("#rmtUtctime").val();
            changedValue = "UTCTime";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "rmtUtctime");
            break;
        case "tbHomeDate":
            time = $("#rmtHomeTime").val();
            changedValue = "Home";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "rmtHomeTime");
            break;
        case "rmtHomeTime":
            time = $("#rmtHomeTime").val();
            changedValue = "HomeTime";
            isDepartOrArr = "Dep";
            isPostback = timeValidation(time, "rmtHomeTime");
            break;
        case "tbArrivalDate":
            time = $("#rmtArrivalTime").val();
            changedValue = "Local";
            isDepartOrArr = "Arr";
            isPostback = timeValidation(time, "rmtArrivalTime");
            break;
        case "rmtArrivalTime":
            time = $("#rmtArrivalTime").val();
            changedValue = "LocalTime";
            isDepartOrArr = "Arr";
            isPostback = timeValidation(time, "rmtArrivalTime");
            break;
        case "tbArrivalUtcDate":
            time = $("#rmtArrivalUtctime").val();
            changedValue = "UTC";
            isDepartOrArr = "Arr";
            isPostback = timeValidation(time, "rmtArrivalUtctime");
            break;
        case "rmtArrivalUtctime":
            time = $("#rmtArrivalUtctime").val();
            changedValue = "UTCTime";
            isDepartOrArr = "Arr";
            isPostback = timeValidation(time, "rmtArrivalUtctime");
            break;
        case "tbArrivalHomeDate":
            time = $("#rmtArrivalHomeTime").val();
            changedValue = "Home";
            isDepartOrArr = "Arr";
            isPostback = timeValidation(time, "rmtArrivalHomeTime");
            break;
        case "rmtArrivalHomeTime":
            time = $("#rmtArrivalHomeTime").val();
            changedValue = "HomeTime";
            isDepartOrArr = "Arr";
            isPostback = timeValidation(time, "rmtArrivalHomeTime");
            break;
        default:
            break;
    }
    if (isPostback) {
        $("#divTimeValidateMessage").text("");
        $("#divTimeValidateMessage").hide();
        GMT_Validate_Retrieve(changedValue, time, isDepartOrArr);
    }
}

function timeValidation(val, textboxId) {
    var strInvalidTimeFormatMessage = "Invalid format, Required format is HH:MM.";
    var strInvalidHoursMessage = "Hours entry must be between 0 and 23 period";
    var strInvalidMinuteMessage = "Minute entry must be between 0 and 59 period";
    $("#divTimeValidateMessage").text("");
    $("#divTimeValidateMessage").hide();
    var postbackval = true;
    var hourvalue = "00";
    var minvalue = "00";
    var count = 0;
    if (IsNullOrEmpty(val))
        postbackval = false;
    if (val.indexOf(".") >= 0) {
        $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
        $("#divTimeValidateMessage").text(strInvalidTimeFormatMessage);
        $("#divTimeValidateMessage").show();
        postbackval = false;
    }
    else if (val.indexOf(":") >= 0) {
        var strarr = val.split(":");
        if (strarr[0].length <= 2) {
            if (Math.abs(strarr[0]) > 23) {
                $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
                $("#divTimeValidateMessage").text(strInvalidHoursMessage);
                $("#divTimeValidateMessage").show();
                postbackval = false;
            } else {
                hourvalue = "00" + strarr[0];
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            }
        }
        else {
            $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
            $("#divTimeValidateMessage").text(strInvalidTimeFormatMessage);
            $("#divTimeValidateMessage").show();
            postbackval = false;
        }
        if (postbackval) {
            if (strarr[1].length <= 2) {
                if (Math.abs(strarr[1]) > 59) {
                    $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
                    $("#divTimeValidateMessage").text(strInvalidMinuteMessage);
                    $("#divTimeValidateMessage").show();
                    hourvalue = "00";
                    minvalue = "00";
                    postbackval = false;
                } else {
                    minvalue = "00" + strarr[1];
                    minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                }
            } else {
                $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
                $("#divTimeValidateMessage").text(strInvalidMinuteMessage);
                $("#divTimeValidateMessage").show();
                postbackval = false;
            }
        }
    }
    else {
        if (val.length <= 2) {
            hourvalue = "00" + val;
            hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
        }
        else {
            $("#" + textboxId).val(hourvalue + ":" + minvalue).focus();
            $("#divTimeValidateMessage").text(strInvalidTimeFormatMessage);
            $("#divTimeValidateMessage").show();
            postbackval = false;
        }
    }
    return postbackval;
}

function departureChanged() {
    var btn = document.getElementById("btnClosestIcao");
    var depart = CurrentLegData.DepartureAirport.IcaoID;
    var TimeDisplayTenMin = self.UserPrincipal._TimeDisplayTenMin;
    var LegId = document.getElementById("hdnLeg").value;
    var LegTabStrip = $find("rtsLegs");
    DepartureIcao_Validate_Retrieve(depart, TimeDisplayTenMin, self.CurrentLegData.LegNUM, btn);
}

function ArrivalChanged() {
    var btn = document.getElementById("btnArrival");
    var arrival = self.CurrentLegData.ArrivalAirport.IcaoID;
    var TimeDisplayTenMin = self.UserPrincipal._TimeDisplayTenMin;
    ArrivalIcao_Validate_Retrieve(arrival, TimeDisplayTenMin, self.CurrentLegData.LegNUM, btn);
    return false;
}

function RemoveTimeValidationMessage() {
    $("#divTimeValidateMessage").text("");
    $("#divTimeValidateMessage").hide();
}

function RequestorValidator() {
    var btn = document.getElementById("btnRequestor");
    var reqtext = self.CurrentLegData.Passenger.PassengerRequestorCD;
    var reqlbl = self.CurrentLegData.Passenger.RequestorDesc;
    var reqhdn = self.CurrentLegData.Passenger.PassengerRequestorID;
    var reqcvlbl = document.getElementById("lbcvRequestor");
    var reqId = self.CurrentLegData.PassengerRequestorID;
    self.CurrentLegData.PassengerRequestorID(null);
    var reqphone = document.getElementById(null);

    var depttext = self.CurrentLegData.Department.DepartmentCD;
    var deptlbl = self.CurrentLegData.Department.DepartmentName;
    var depthdn = self.CurrentLegData.Department.DepartmentID;
    var deptId = self.CurrentLegData.DepartmentID;
    var deptcvlbl = document.getElementById("lbcvDepartment");

    var authtext = self.CurrentLegData.DepartmentAuthorization.AuthorizationCD;
    var authlbl = self.CurrentLegData.DepartmentAuthorization.DeptAuthDescription;
    var authhdn = self.CurrentLegData.DepartmentAuthorization.AuthorizationID;
    var authId = self.CurrentLegData.AuthorizationID;
    var authcvlbl = document.getElementById("lbcvAuthorization");

    Requestor_Validate_Retrieve(reqtext, reqlbl, reqhdn, reqcvlbl, reqphone, reqId, depttext, deptlbl, depthdn, deptcvlbl, deptId, authtext, authlbl, authhdn, authcvlbl, authId, btn);
}

function AccountValidator() {
    var btn = document.getElementById("btnAccountNo");
    var accounttext = self.CurrentLegData.Account.AccountNum;
    var accountlbl = self.CurrentLegData.Account.AccountDescription;
    var accounthdn = self.CurrentLegData.Account.AccountID;
    var accountcvlbl = document.getElementById("lbcvAccountNo");
    var accountId = self.CurrentLegData.AccountID;
    self.CurrentLegData.AccountID(null);
    Account_Validate_Retrieve(accounttext, accountlbl, accounthdn, accountcvlbl, accountId, btn);
}

function DepartmentValidator() {
    var btn = document.getElementById("btnDepartment");
    var depttext = self.CurrentLegData.Department.DepartmentCD;
    var deptlbl = self.CurrentLegData.Department.DepartmentName;
    var depthdn = self.CurrentLegData.Department.DepartmentID;
    var deptId = self.CurrentLegData.DepartmentID;
    self.CurrentLegData.DepartmentID(null);
    var deptcvlbl = document.getElementById("lbcvDepartment");

    var authtext = self.CurrentLegData.DepartmentAuthorization.AuthorizationCD;
    var authlbl = self.CurrentLegData.DepartmentAuthorization.DeptAuthDescription;
    var authhdn = self.CurrentLegData.DepartmentAuthorization.AuthorizationID;
    var authId = self.CurrentLegData.AuthorizationID;
    Department_Validate_Retrieve(depttext, deptlbl, depthdn, deptcvlbl, deptId, authtext, authlbl, authhdn, authId, btn);
}

function AuthorizationValidator() {
    var btn = document.getElementById("btnAuthorization");
    var authtext = self.CurrentLegData.DepartmentAuthorization.AuthorizationCD;
    var authlbl = self.CurrentLegData.DepartmentAuthorization.DeptAuthDescription;
    var authhdn = self.CurrentLegData.DepartmentAuthorization.AuthorizationID;
    var authcvlbl = document.getElementById("lbcvAuthorization");
    var depthdn = self.CurrentLegData.Department.DepartmentID;
    var authId = self.CurrentLegData.AuthorizationID;
    self.CurrentLegData.AuthorizationID(null);
    Authorization_Validate_Retrieve(authtext, authlbl, authhdn, authcvlbl, depthdn, authId, btn);
}

function ClientCodeValidation() {
    var btn = document.getElementById("btnClient");
    var clienttext = self.CurrentLegData.Client.ClientCD;
    var clientlbl = self.CurrentLegData.Client.ClientDescription;
    var clienthdn = self.CurrentLegData.Client.ClientID;
    var pfClientId = self.CurrentLegData.ClientID;
    self.CurrentLegData.ClientID(null);
    var clientcvlbl = document.getElementById("lbcvClient");
    Client_Validate_Retrieve(clienttext, clientlbl, clienthdn, clientcvlbl, pfClientId, btn);
}

function fnValidateAuth() {

    if (IsNullOrEmpty(self.CurrentLegData.Department.DepartmentCD())) {
        jAlert("Please enter the Department Code before entering the Authorization Code.", "Preflightleg");
    }
    else {
        openWin('rdAuth');
    }
}

function CategoryValidation() {
    var category = self.CurrentLegData.FlightCatagory.FlightCatagoryCD;
    self.CurrentLegData.FlightCategoryID(null);
    var LegNum = self.CurrentLegData.LegNUM();
    var btn = document.getElementById("btnCategory");
    Category_Validate_Retrieve(category, LegNum, btn);
}

function CrewDutyRuleValidation() {
    var tbCrewRule = self.CurrentLegData.CrewDutyRule.CrewDutyRuleCD;
    self.CurrentLegData.CrewDutyRulesID(null);
    var btn = document.getElementById("btnCrewRules");
    CrewDutyRule_Validate_Retrieve(tbCrewRule, btn);
}

function TASValidation() {
    var isDepartureConfirmed = true;
    if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
        isDepartureConfirmed = false;
    FlagForTimeChange = 1;
    PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Bias");
}

function PowersettingValidation() {
    var tbPower = self.CurrentLegData.PowerSetting;
    var tbAircraft = self.Preflight.PreflightMain.Aircraft.AircraftID;
    var TimeDisplayTenMin = self.UserPrincipal._TimeDisplayTenMin;
    var btnPowerSeting = $('#btnPower');
    PowerSetting_Validate_Retrieve(tbPower, tbAircraft, TimeDisplayTenMin, btnPowerSeting);
}

function CQCustomerValidator() {
    var btn = document.getElementById("btCQcustomerPopup");
    var tbCQCustomer = self.CurrentLegData.CQCustomer.CQCustomerCD;
    var lbCQCustomer = self.CurrentLegData.CQCustomer.CQCustomerName;
    var hdCQCustomer = self.CurrentLegData.CQCustomer.CQCustomerID;
    var CQCustId = self.CurrentLegData.CQCustomerID;
    self.CurrentLegData.CQCustomerID(null);
    var lbcvCQCustomer = document.getElementById("lbcvCQCustomer");
    CQCustomer_Validate_Retrieve(tbCQCustomer, lbCQCustomer, hdCQCustomer, lbcvCQCustomer, CQCustId, btn);
}

function confirmDeadHeadLegCallBackFn(arg) {
    var LegNum = self.CurrentLegData.LegNUM;
    if (arg == true) {
        CategoryChange_PassengersDelete(LegNum, true);
    }
    else {
        CategoryChange_PassengersDelete(LegNum, false);
    }
}

function confirmDeleteLegCallBackFn(arg) {
    if (arg == true) {
        document.getElementById('btnDeleteLegYes').click();
    }
    else {
        document.getElementById('btnDeleteLegNo').click();
    }
}

function confirmCopyCallBackFn(arg) {
    if (arg == true) {
        document.getElementById('btnCopyYes').click();
    }
    else {
        document.getElementById('btnCopyNo').click();
    }
}

function openWin(url, value, radWin) {
    var oWnd = radopen(url + value, radWin);
}

function openWinAirport(type) {
    var url = '';
    if (type == "DEPART") {
        url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbDepart').value;
        var oWnd = radopen(url, 'radAirportPopup');
    }
    else {
        url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbArrival').value;
        var oWnd = radopen(url, 'radArrivalAirportPopup');
    }

}

function openWin(radWin) {
    var url = '';
    if (radWin == "radDepartAirportPopup") {
        url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbDepart').value;
    }
    else if (radWin == "radArrivalAirportPopup") {
        url = '../../Settings/Airports/AirportMasterPopup.aspx?IcaoID=' + document.getElementById('tbArrival').value;
    }
    else if (radWin == "rdPower") {
        url = '../../Settings/Fleet/AircraftPopup.aspx?PowerSetting=' + document.getElementById('tbPower').value;
    }
    else if (radWin == "radCrewDutyRulesPopup") {
        url = "../../Settings/People/CrewDutyRulesPopup.aspx?CrewDutyRuleCD=" + document.getElementById('tbCrewRules').value;
    }
    else if (radWin == "radPaxInfoPopup") {
        url = "../../Settings/People/PassengerRequestorsPopup.aspx?PassengerRequestorCD=" + document.getElementById('tbRequestor').value + "&ShowRequestor=" + document.getElementById("hdShowRequestor").value;
    }
    else if (radWin == "rdDeptPopup") {
        url = "../../Settings/Company/DepartmentAuthorizationPopup.aspx?DepartmentCD=" + document.getElementById('tbDepartment').value;
    }
    else if (radWin == "radCQCPopups") {
        url = "/Views/CharterQuote/CharterQuoteCustomerPopup.aspx?Code=" + document.getElementById("tbCQCustomer").value;
    }
    else if (radWin == "radAccountMasterPopup") {
        url = "../../Settings/Company/AccountMasterPopup.aspx?AccountNum=" + document.getElementById('tbAccountNo').value;
    }
    else if (radWin == "rdAuth") {
        if (document.getElementById("hdDepartment").value != "") {
            url = "../../Settings/Company/AuthorizationPopup.aspx?AuthorizationCD=" + document.getElementById('tbAuthorization').value + "&deptId=" + document.getElementById("hdDepartment").value;
        }
        else {
            var OAlert = jAlert("Please enter the Department Code before entering the Authorization Code.", "Preflight Leg");
            OAlert.add_close(function () { document.getElementById("tbDepartment").focus(); });
        }
    }
    else if (radWin == "rdClientCodePopup") {
        url = "/Views/Settings/Company/ClientCodePopup.aspx?ClientCD=" + document.getElementById('tbClient').value;
    }
    else if (radWin == "radFlightCategoryPopup") {
        url = "/Views/Settings/Fleet/FlightCategoryPopup.aspx?FlightCatagoryCD=" + document.getElementById('tbCategory').value;
    }
    else if (radWin == "RadRetrievePopup") {
        url = '../../Transactions/Preflight/PreflightSearch.aspx';
    }
    else if (radWin == "rdChecklist") {
        url = '../../Transactions/Preflight/PreflightChecklist.aspx';
    }
    else if (radWin == "rdOutBoundInstructions") {
        url = '../../Transactions/Preflight/PreflightOutboundInstruction.aspx';
    }
    if (url != "" && radWin != "rdOutBoundInstructions" && radWin != "rdChecklist") {
        var oWnd = radopen(url, radWin);
    }
    else if (radWin == "rdOutBoundInstructions") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Outbound Instructions - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Outbound Instructions - New Tripsheet");
        }
    } else if (radWin == "rdChecklist") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Checklist - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Checklist - New Tripsheet");
        }
    } else {
        if (radWin == "rdPowerSetting")
            var oWnd = radopen("", radWin);
        if (radWin == "rdTOBias")
            var oWnd = radopen("", radWin);
        if (radWin == "rdLandBias")
            var oWnd = radopen("", radWin);
    }
}

function ConfirmClose(WinName) {
    var oManager = GetRadWindowManager();
    var oWnd = oManager.GetWindowByName(WinName);
    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
    CloseButton.onclick = function () {
        CurrentWinName = oWnd.Id;
        jConfirm("Are you sure you want to close the window?", "Confirmation!", confirmCallBackFn);
    };
}

function OnClientICAOClose1(oWnd, args) {
    var combo = $find("tbDepart");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.DepartureAirport.IcaoID(arg.ICAO.toUpperCase());
            self.CurrentLegData.DepartureAirport.AirportName(arg.AirportName.toUpperCase());
            self.CurrentLegData.DepartureAirport.AirportID(arg.AirportID);
            if (arg.ICAO != null)
                document.getElementById("lbcvDepart").innerHTML = "";
            departureChanged();
        }
        else {
            self.CurrentLegData.DepartureAirport.IcaoID("");
            self.CurrentLegData.DepartureAirport.AirportName("");
            self.CurrentLegData.DepartureAirport.AirportID("");
            combo.clearSelection();
        }
    }
}

function OnClientICAOClose2(oWnd, args) {
    var combo = $find("tbArrival");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.ArrivalAirport.IcaoID(arg.ICAO.toUpperCase());
            self.CurrentLegData.ArrivalAirport.AirportName(arg.AirportName.toUpperCase());
            self.CurrentLegData.ArrivalAirport.AirportID(arg.AirportID);
            if (arg.ICAO != null)
                document.getElementById("lbcvArrival").innerHTML = "";
            ArrivalChanged();
        }
        else {
            self.CurrentLegData.ArrivalAirport.IcaoID("");
            self.CurrentLegData.ArrivalAirport.AirportName("");
            self.CurrentLegData.ArrivalAirport.AirportID("");
            combo.clearSelection();
        }
    }
}

function OnClientCodeClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.Client.ClientCD(arg.ClientCD);
            self.CurrentLegData.Client.ClientDescription(arg.ClientDescription);
            self.CurrentLegData.Client.ClientID(arg.ClientID);
            self.CurrentLegData.ClientID(arg.ClientID);
            if (arg.ClientCD != null)
                document.getElementById("lbcvClient").innerHTML = "";
            ClientCodeValidation();
        }
        else {
            self.CurrentLegData.Client.ClientCD("");
            self.CurrentLegData.Client.ClientDescription("");
            self.CurrentLegData.Client.ClientID("");
        }
    }
}

function OnClientDeptClose(oWnd, args) {
    var combo = $find("tbDepartment");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.Department.DepartmentCD(arg.DepartmentCD);
            self.CurrentLegData.Department.DepartmentName(arg.DepartmentName);
            self.CurrentLegData.Department.DepartmentID(arg.DepartmentID);
            self.CurrentLegData.DepartmentID(arg.DepartmentID);
            self.CurrentLegData.DepartmentAuthorization.AuthorizationCD("");
            self.CurrentLegData.DepartmentAuthorization.DeptAuthDescription("");
            self.CurrentLegData.DepartmentAuthorization.AuthorizationID("");
            if (arg.DepartmentCD != null)
                document.getElementById("lbcvDepartment").innerHTML = "";
            DepartmentValidator();
        }
        else {
            self.CurrentLegData.Department.DepartmentCD("");
            self.CurrentLegData.Department.DepartmentName("");
            self.CurrentLegData.Department.DepartmentID("");
            self.CurrentLegData.DepartmentID("");
            combo.clearSelection();
        }
    }
}

function OnClientCloseCharterQuotCustomerPopup(oWnd, args) {
    var combo = $find("tbCQCustomer");
    //get the transferred arguments
    var arg = args.get_argument();

    if (arg !== null) {
        if (arg) {
            self.CurrentLegData.CQCustomer.CQCustomerCD(arg.CQCustomerCD);
            CQCustomerValidator();
        }
        else {
            self.CurrentLegData.CQCustomer.CQCustomerCD("");
            self.CurrentLegData.CQCustomer.CQCustomerName("");
            self.CurrentLegData.CQCustomer.CQCustomerID("");
            combo.clearSelection();
        }
    }
}

function OnClientAccountClose(oWnd, args) {
    var combo = $find("tbAccountNo");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.Account.AccountNum(arg.AccountNum);
            self.CurrentLegData.Account.AccountDescription(arg.AccountDescription);
            self.CurrentLegData.Account.AccountID(arg.AccountID);
            self.CurrentLegData.AccountID(arg.AccountID);
            if (arg.AccountNum != null)
                document.getElementById("lbcvAccountNo").innerHTML = "";
            AccountValidator();
        }
        else {
            self.CurrentLegData.Account.AccountNum("");
            self.CurrentLegData.Account.AccountDescription("");
            self.CurrentLegData.Account.AccountID("");
            self.CurrentLegData.AccountID("");
            combo.clearSelection();
        }
    }
}

function OnClientAuthClose(oWnd, args) {
    var combo = $find("tbAuthorization");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.DepartmentAuthorization.AuthorizationCD(arg.AuthorizationCD);
            self.CurrentLegData.DepartmentAuthorization.DeptAuthDescription(arg.DeptAuthDescription);
            self.CurrentLegData.DepartmentAuthorization.AuthorizationID(arg.AuthorizationID);
            self.CurrentLegData.AuthorizationID(arg.AuthorizationID);
            if (arg.AuthorizationCD != null)
                document.getElementById("lbcvAuthorization").innerHTML = "";
            AuthorizationValidator();
        }
        else {
            self.CurrentLegData.DepartmentAuthorization.AuthorizationCD("");
            self.CurrentLegData.DepartmentAuthorization.DeptAuthDescription("");
            self.CurrentLegData.DepartmentAuthorization.AuthorizationID("");
            self.CurrentLegData.AuthorizationID("");
            combo.clearSelection();
        }
    }
}

function OnClientCrewRuleClose(oWnd, args) {
    var combo = $find("tbCrewRules");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            CurrentLegData.CrewDutyRule.CrewDutyRuleCD(arg.CrewDutyRuleCD);
            CurrentLegData.FedAviationRegNUM(arg.FedAviatRegNum);
            CurrentLegData.CrewDutyRulesID(arg.CrewDutyRulesID);
            if (arg.CrewDutyRuleCD != null)
                document.getElementById("lbcvCrewRules").innerText = "";
            CrewDutyRuleValidation();
        }
        else {
            CurrentLegData.CrewDutyRule.CrewDutyRuleCD("");
            CurrentLegData.FedAviationRegNUM("");
            CurrentLegData.CrewDutyRulesID("");
            combo.clearSelection();
        }
    }
}

function OnClientRqstrClose(oWnd, args) {
    var combo = $find("tbRequestor");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.Passenger.PassengerRequestorCD(arg.PassengerRequestorCD);
            self.CurrentLegData.Passenger.RequestorDesc(arg.PassengerName);
            self.CurrentLegData.Passenger.PassengerRequestorID(arg.PassengerRequestorID);
            self.CurrentLegData.PassengerRequestorID(arg.PassengerRequestorID)
            if (arg.PassengerRequestorCD != null)
                document.getElementById("lbcvRequestor").innerHTML = "";
            RequestorValidator();
        }
        else {
            self.CurrentLegData.Passenger.PassengerRequestorCD("");
            self.CurrentLegData.Passenger.RequestorDesc("");
            self.CurrentLegData.Passenger.PassengerRequestorID("");
            combo.clearSelection();
        }
    }
}

function CloseRadWindow(arg) {
    GetRadWindow().Close();
}

function OnClientCategoryClose(oWnd, args) {
    var combo = $find("tbCategory");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            self.CurrentLegData.FlightCatagory.FlightCatagoryCD(arg.FlightCatagoryCD);
            self.CurrentLegData.FlightCategoryID(arg.FlightCategoryID);
            if (arg.FlightCatagoryCD != null)
                document.getElementById("lbcvCategory").innerHTML = "";
            CategoryValidation();
        }
        else {
            self.CurrentLegData.FlightCatagory.FlightCatagoryCD("");
            self.CurrentLegData.FlightCategoryID("");
            combo.clearSelection();
        }
    }
}

function OnClientPowerClose(oWnd, args) {
    var combo = $find("tbPower");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg != null) {
        if (arg) {
            document.getElementById("tbPower").value = arg.PowerSetting;
            document.getElementById("hdPower").value = arg.AircraftID;
            if (arg.PowerSetting != null)
                document.getElementById("lbcvPower").innerHTML = "";
            PowersettingValidation();
        }
        else {
            document.getElementById("tbPower").value = "";
            document.getElementById("hdPower").value = "";
            combo.clearSelection();
        }
    }
}

function fnTextCount(text, long) {
    var maxlength = new Number(long);
    if (text.value.length >= maxlength) {
        //                    text.value = text.value.substring(0, maxlength);
        return false;
    }
}

function ShowAirportInfoPopup(Airportid) {
    window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
    return false;
}

function fnchecknumericformat(myfield) {
    if (myfield.value != "") {
        if (myfield.value.indexOf(".") >= 0) {
            if (myfield.value.indexOf(".") < myfield.value.length - 1) {
                var strarr = myfield.value.split(".");
                if (strarr[0].length > 2) {
                    jAlert("Invalid format. Required Format is NN.N");
                    myfield.value = "";
                    myfield.focus();
                    return false;
                }
                if (strarr[1].length > 1) {
                    jAlert("Invalid format. Required Format is NN.N");
                    myfield.value = "";
                    myfield.focus();
                    return false;
                }
            }
        }
        else {
            if (myfield.value.length > 2 && myfield.value.indexOf(".") < 0) {
                jAlert("Invalid format. Required Format is NN.N");
                myfield.value = "";
                myfield.focus();
                return false;
            }
        }
    }
    return true;
}

function checknumericValue() {
    var postbackval = true;
    var val = self.CurrentLegData.OverrideValue;
    if (val().length > 0) {
        if (val().indexOf(".") >= 0) {
            if (val().indexOf(".") > 0) {
                var strarr = val().split(".");
                if (strarr[0].length > 2) {
                    jAlert("Invalid format, Required format is NN.N ", "Preflight Leg", function () { val("0"); $("#tbOverride").focus(); });
                    postbackval = false;

                }
                if (strarr[1].length > 1) {
                    jAlert("Invalid format, Required format is NN.N ", "Preflight Leg", function () { val("0"); $("#tbOverride").focus(); });
                    postbackval = false;
                }
            }
        }
        else {
            if (val().length > 2) {
                jAlert("Invalid format, Required format is NN.N ", "Preflight Leg", function () { val("0"); $("#tbOverride").focus(); });
                postbackval = false;
            }
        }
    }

    if (postbackval) {
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        //FlagForTimeChange = 1;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "CrewDutyRule");
    }
    return postbackval;

}

function fnAllowNumericAndOneDecimal(fieldname, myfield, e) {
    var key;
    var keychar;
    var allowedString = "0123456789" + ".";

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    if (keychar == ".") {

        if (myfield.value.indexOf(keychar) > -1) {
            return false;
        }
    }

    if (fieldname == "override") {
        if (myfield.value.length > 0) {
            if (myfield.value.indexOf(".") >= 0) {
                if (myfield.value.indexOf(".") < myfield.value.length - 1) {
                    var strarr = myfield.value.split(".");
                    if (strarr[0].length >= 2)
                        return false;
                    if (strarr[1].length > 0 && strarr[1].length == 1)
                        return false;
                }
            }
            else {
                if (myfield.value.length >= 2 && keychar != ".")
                    return false;
            }
        }
    }
    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
        // numeric values and allow slash("/")
    else if ((allowedString).indexOf(keychar) > -1)
        return true;
    else
        return false;
}

function maxLength(field, maxChars) {
    if (field.value.length >= maxChars) {
        event.returnValue = false;
        return false;
    }
}

function maxLengthPaste(field, maxChars) {
    event.returnValue = false;
    if ((field.value.length + window.clipboardData.getData("Text").length) > maxChars) {
        return false;
    }
    event.returnValue = true;
}

function checkNegativeValue() {
    var postbackval = true;
    var val = CurrentLegData.WindsBoeingTable;
    if (val().length > 0) {

        if (val().indexOf("-") > 0) {
            postbackval = false;
            jAlert("Invalid format, Required format is (-)### ", "Preflight Leg", function () { $("#tbWind").focus(); val("0.0"); });
        }
        else {
            var strarr = val().split("-");
            if (strarr.length > 2) {
                postbackval = false;
                jAlert("Invalid format, Required format is (-)### ", "Preflight Leg", function () { $("#tbWind").focus(); val("0.0"); });
                val("0.0");
            }
        }

    }
    if (postbackval == true) {
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        FlagForTimeChange = 1;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Wind");
    }
    return postbackval;
}

function CheckTenthorHourMinValuebias() {
    var postbackval = true;
    var val = self.CurrentLegData.StrTakeoffBIAS;
    var companyprofileSetting = document.getElementById('hdTimeDisplayTenMin').value;
    if (companyprofileSetting == 1) {
        var count = 0;
        var strarr = val();
        for (var i = 0; i < strarr.length; ++i) {
            if (strarr[i] == ".")
                count++;
        }
        if (count > 1) {
            jAlert("Invalid format, Required format is NN.N", "", function () {
                $("#tbBias").focus();
                val("0.0");
            });
            postbackval = false;
        }
        if (val().length > 0) {
            if (val().indexOf(":") >= 0) {
                jAlert("Invalid format, Required format is NN.N", "", function () {
                    $("#tbBias").focus();
                    val("0.0");
                });
                postbackval = false;
            } else if (val().indexOf(".") >= 0) {
                var strarr = val().split(".");

                if (strarr[0].length > 2) {
                    jAlert("Invalid format, Required format is NN.N", "", function () {
                        $("#tbBias").focus();
                        val("0.0");
                    });
                    postbackval = false;
                }

                if (strarr[1].length > 1) {
                    jAlert("Invalid format, Required format is NN.N", "", function () {
                        $("#tbBias").focus();
                        val("0.0");
                    });
                    postbackval = false;
                }
                if (postbackval) {
                    if (strarr[0].length == 0)
                        val("0" + val());
                }
                if (postbackval) {
                    if (strarr[1].length == 0)
                        val(val() + "0");
                }
            } else {
                if (val().length > 2) {
                    jAlert("Invalid format, Required format is NN.N", "", function () {
                        $("#tbBias").focus();
                        val("0.0");
                    });
                    postbackval = false;
                } else {
                    val(val() + ".0");
                }
            }
        }


    } else {
        var hourvalue = "00";
        var minvalue = "00";
        var count = 0;
        if (val().indexOf(".") >= 0) {
            jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                $("#tbBias").focus();
            });
            postbackval = false;
        }
            //check for colon
        else if (val().indexOf(":") >= 0) {
            strarr = val().split(":");
            if (strarr.length > 2) {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    $("#tbBias").focus();
                });
                postbackval = false;
            }


            // hour validation
            if (strarr[0].length <= 2) {
                hourvalue = "00" + strarr[0];
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            } else {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    $("#tbBias").focus();
                    val("00:00");
                });
                postbackval = false;

            }
            // hour validation
            if (postbackval) {
                //minute Validation
                if (strarr[1].length <= 2) {
                    if (Math.abs(strarr[1]) > 59) {
                        jAlert("Minute entry must be between 0 and 59", "Preflight Leg", function () {
                            $("#tbBias").focus();
                        });
                        hourvalue = "00";
                        minvalue = "00";
                        postbackval = false;
                    } else {
                        minvalue = "00" + strarr[1];
                        minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);
                    }
                } else {
                    jAlert("Minute entry must be between 0 and 59", "Preflight Leg", function () {
                        $("#tbBias").focus();
                    });
                    hourvalue = "00";
                    minvalue = "00";
                    postbackval = false;
                }
                //minute Validation
            }
            //check for colon
        } else {
            if (val().length <= 2) {
                hourvalue = "00" + val();
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            } else {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    $("#tbBias").focus();
                    val("00:00");
                });
                postbackval = false;
            }
        }

        val(hourvalue + ":" + minvalue);
    }

    if (postbackval) {
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        FlagForTimeChange = 1;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Bias");
    }
    return postbackval;
}

function CheckTenthorHourMinValueLandbias() {
    var postbackval = true;
    var companyprofileSetting = document.getElementById('hdTimeDisplayTenMin').value;
    var val = self.CurrentLegData.StrLandingBias;
    if (companyprofileSetting == 1) {
        var count = 0;
        var strarr = val();
        for (var i = 0; i < strarr.length; ++i) {
            if (strarr[i] == ".")
                count++;
        }
        if (count > 1) {
            jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                $("#tbLandBias").focus();
                val("0.0");
            });
            postbackval = false;
        }
        if (val().length > 0) {
            if (val().indexOf(":") >= 0) {
                jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                    $("#tbLandBias").focus();
                    val("0.0");
                });
                postbackval = false;
            }
            else if (val().indexOf(".") >= 0) {
                var strarr = val().split(".");

                if (strarr[0].length > 2) {
                    jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                        $("#tbLandBias").focus();
                        val("0.0");
                    });
                    postbackval = false;
                }

                if (strarr[1].length > 1) {
                    jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                        $("#tbLandBias").focus();
                        val("0.0");
                    });
                    postbackval = false;
                }
                if (postbackval) {
                    if (strarr[0].length == 0)
                        val("0" + val());
                }
                if (postbackval) {
                    if (strarr[1].length == 0)
                        val(val() + "0");
                }
            }
            else {
                if (val().length > 2) {
                    jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                        $("#tbLandBias").focus();
                        val("0.0");
                    });
                    postbackval = false;
                }
                else {
                    val(val() + ".0");
                }
            }
        }


    }
    else {
        var hourvalue = "00";
        var minvalue = "00";
        var count = 0;
        if (val().indexOf(".") >= 0) {
            jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                $("#tbLandBias").focus();
            });
            postbackval = false;
        }
            //check for colon
        else if (val().indexOf(":") >= 0) {

            var strarr = val().split(":");
            // hour validation
            if (strarr[0].length <= 2) {
                hourvalue = "00" + strarr[0];
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            }
            else {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    val("00:00");
                    $("#tbLandBias").focus();
                });
                postbackval = false;

            }
            // hour validation
            if (postbackval) {
                //minute Validation
                if (strarr[1].length <= 2) {
                    if (Math.abs(strarr[1]) > 59) {
                        jAlert("Minute entry must be between 0 and 59.", "Preflight Leg", function () {
                            $("#tbLandBias").focus();
                        });
                        hourvalue = "00";
                        minvalue = "00";
                        postbackval = false;
                    } else {
                        minvalue = "00" + strarr[1];
                        minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                    }
                } else {
                    jAlert("Minute entry must be between 0 and 59.", "Preflight Leg", function () {
                        $("#tbLandBias").focus();
                    });
                    postbackval = false;
                }
                //minute Validation
            }
            //check for colon
        }
        else {
            if (val().length <= 2) {
                hourvalue = "00" + val();
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            }
            else {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    $("#tbLandBias").focus();
                    val("00:00");
                });
                postbackval = false;
            }
        }
        val(hourvalue + ":" + minvalue);
    }
    if (postbackval) {
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        FlagForTimeChange = 1;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "Bias");
    }
    return postbackval;
}

function CheckTenthorHourMinValueEte() {
    var postbackval = true;
    var val = CurrentLegData.StrElapseTM;
    var companyprofileSetting = document.getElementById('hdTimeDisplayTenMin').value;
    if (companyprofileSetting == 1) {
        var count = 0;
        strarr = val();
        for (var i = 0; i < strarr.length; ++i) {
            if (strarr[i] == ".")
                count++;
        }
        if (count > 1) {
            jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                val("0.0");
                $("#tbETE").focus();
            });
            postbackval = false;
        }
        if (val().length > 0) {
            if (val().indexOf(":") >= 0) {
                jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                    val("0.0");
                    $("#tbETE").focus();
                });
                postbackval = false;
            }
            else if (val().indexOf(".") >= 0) {
                strarr = val().split(".");

                if (strarr[0].length > 2) {
                    jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                        val("0.0");
                        $("#tbETE").focus();
                    });
                    postbackval = false;
                }

                if (strarr[1].length > 1) {
                    jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                        val("0.0");
                        $("#tbETE").focus();
                    });
                    postbackval = false;
                }
                if (postbackval) {
                    if (strarr[0].length == 0)
                        val("0" + val());
                }
                if (postbackval) {
                    if (strarr[1].length == 0)
                        val(val() + "0");
                }
            }
            else {
                if (val().length > 2) {
                    jAlert("Invalid format, Required format is NN.N", "Preflight Leg", function () {
                        val("0.0");
                        $("#tbETE").focus();
                    });
                    postbackval = false;
                }
                else {
                    val(val() + ".0");
                }
            }
        }


    }
    else {
        var hourvalue = "00";
        var minvalue = "00";
        var count = 0;
        if (val().indexOf(".") >= 0) {
            jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                $("#tbETE").focus();
            });
            postbackval = false;
        }
            //check for colon
        else if (val().indexOf(":") >= 0) {

            var strarr = val().split(":");
            // hour validation
            if (strarr[0].length <= 2) {
                hourvalue = "00" + strarr[0];
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            }
            else {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    $("#tbETE").focus();
                });
                postbackval = false;

            }
            // hour validation
            if (postbackval) {
                //minute Validation
                if (strarr[1].length <= 2) {
                    if (Math.abs(strarr[1]) > 59) {
                        jAlert("Minute entry must be between 0 and 59", "Preflight Leg", function () {
                            $("#tbETE").focus();
                        });
                        hourvalue = "00";
                        minvalue = "00";
                        postbackval = false;
                    } else {
                        minvalue = "00" + strarr[1];
                        minvalue = minvalue.substring(minvalue.length - 2, minvalue.length);

                    }
                } else {
                    jAlert("Minute entry must be between 0 and 59", "Preflight Leg", function () {
                        $("#tbETE").focus();
                    });
                    postbackval = false;
                }
                //minute Validation
            }
            //check for colon
        }
        else {
            if (val().length <= 2) {
                hourvalue = "00" + val();
                hourvalue = hourvalue.substring(hourvalue.length - 2, hourvalue.length);
            }
            else {
                jAlert("Invalid format, Required format is HH:MM.", "Preflight Leg", function () {
                    $("#tbETE").focus();
                });
                postbackval = false;
            }
        }

        val(hourvalue + ":" + minvalue);

    }

    val(DefaultEteResult(val(), companyprofileSetting));
    if (postbackval) {
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "ETE");
    }
    return postbackval;
}

function ValidateDate(control) {
    var MinDate = new Date("01/01/1900");
    var MaxDate = new Date("12/31/2100");
    var SelectedDate;
    if (control.value != "") {
        SelectedDate = new Date(control.value);
        if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
            jAlert("Please enter/select Date between 01/01/1900 and 12/31/2100");
            control.value = "";
            return false;
        }
    }
    return true;
}

function ClosePowerSettingPopup() {
    $("[id$='rdPowerSetting']").css('display', 'none');
    $(".TelerikModalOverlay").css('display', 'none');
    return false;
}

function pageLoad() {
    $("input[name$='rblistWindReliability']").change(function () {
        FlagForTimeChange = 1;
        self.CurrentLegData.StrWindReliability($(this).val());
        var isDepartureConfirmed = true;
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "WindReliability");
    });
    $("input[id$='chkEndOfDuty']").change(function () {
        var isDepartureConfirmed = true;
        self.CurrentLegData.IsDutyEnd($(this).prop('checked'));
        if ($("[id$='lbDepartureDate']").hasClass("time_anchor"))
            isDepartureConfirmed = false;
        PreflightCalculation_Validate_Retrieve(isDepartureConfirmed, "CrewDutyRule");
    });
}

function setFocusOnTopOfScreen(focusEle) {
    var focusElement = $(focusEle);
    $(focusElement).focus();
    $('html, body').animate({ scrollTop: $(focusElement).offset().top - 50 }, 'slow');
}

function AddLeg() {

    RemoveAllErrorLabels();
    AddLeg_Click();
}

function InsertLeg() {
    RemoveAllErrorLabels();
    InsertLeg_Click();
}

function DeleteLeg() {
    if (self.Legs().length > 1) {
        setAlertTextToYesNo();
        jConfirm('Delete Trip Leg?', 'Confirmation !', function (arg) {
            if (arg) DeleteLeg_Click();
        });
        setAlertTextToOkCancel();
    } else {
        jAlert("Cannot Delete - Trip should have atleast one leg", "Trip Alert");
    }
}

function confirmCopyFlightCategoryToAllLeg() {
    jConfirm("Copy Flight Category To All Legs?", "Confirmation!", CopyFlightCategoryToAllLeg);
}

function CopyFlightCategoryToAllLeg(arg) {
    if (arg) {
        var flightCategoryCD = self.CurrentLegData.FlightCatagory.FlightCatagoryCD();
        var flightCategoryID = self.CurrentLegData.FlightCategoryID();
        if (flightCategoryCD != null) {
            $.ajax({
                async: true,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/CopyFlightCategoryToAllLeg",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'flightCategoryCD': flightCategoryCD, 'flightCategoryID': flightCategoryID, 'legNum': self.CurrentLegData.LegNUM() }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                }, error: function (jqXHR, textStatus, errorThrown) {
                    reportPreflightError(jqXHR, textStatus, errorThrown);
                }
            });
        }
    }
}

function RemoveAllErrorLabels()
{
    document.getElementById("lbcvClient").innerHTML = "";
    document.getElementById("lbcvDepart").innerHTML = "";
    document.getElementById("lbcvArrival").innerHTML = "";
    document.getElementById("lbcvCategory").innerHTML = "";
    document.getElementById("lbcvPower").innerHTML = "";
    document.getElementById("lbcvCrewRules").innerHTML = "";
    document.getElementById("lbcvRequestor").innerHTML = "";
    document.getElementById("lbcvDepartment").innerHTML = "";
    document.getElementById("lbcvAuthorization").innerHTML = "";
    document.getElementById("lbcvAccountNo").innerHTML = "";
    document.getElementById("lbcvCQCustomer").innerHTML = "";    
}

// Method will delete child object from Leg if relevant ID is not set
function ClearViewModelChildren(CurrentLeg)
{
    
    if (IsNullOrEmpty(CurrentLeg.ClientID)) {
        delete CurrentLeg.Client;
    }
    if (IsNullOrEmpty(CurrentLeg.ArrivalAirport.AirportID)) {
        delete CurrentLeg.ArrivalAirport;
    }
    if (IsNullOrEmpty(CurrentLeg.DepartureAirport.AirportID)) {
        delete CurrentLeg.DepartureAirport;
    }
    
    if (IsNullOrEmpty(CurrentLeg.FlightCategoryID)) { 
        delete CurrentLeg.FlightCatagory;
    }

    if (IsNullOrEmpty(CurrentLeg.CrewDutyRulesID)) {
        delete CurrentLeg.CrewDutyRule;
    }    

    if (IsNullOrEmpty(CurrentLeg.PassengerRequestorID)) {
        delete CurrentLeg.Passenger;
    }

    if (IsNullOrEmpty(CurrentLeg.DepartmentID)) {
        delete CurrentLeg.Department;
    }
    
    if (IsNullOrEmpty(CurrentLeg.AuthorizationID)) {
        delete CurrentLeg.DepartmentAuthorization;
    }
    
    if (IsNullOrEmpty(CurrentLeg.AccountID)) {
        delete CurrentLeg.Account;
    }    
    if (IsNullOrEmpty(CurrentLeg.CQCustomerID)) {
        delete CurrentLeg.CQCustomer;
    }
}