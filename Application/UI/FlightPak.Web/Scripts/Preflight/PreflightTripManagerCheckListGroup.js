﻿var TripManagerCheckListGroup;
$(function () {
    TripManagerCheckListGroup = function () {
        var CheckListGroup = null;
        CheckListGroup = new PeflightTripManagerCheckListGroupViewModel();
        return {
            CheckListGroup: this.CheckListGroup,
        };
    };

    ko.applyBindings(new TripManagerCheckListGroup(), document.getElementById('OuterDiv'));
});


var self = this;
jqgridTableId = '#dgTripManager';
var IsInActive = false;
var PeflightTripManagerCheckListGroupViewModel = function () {
    self.IsInActive = ko.observable();
    self.tripManagerCheckListGroup = ko.observable();
    self.IsInActive.subscribe(function (status) {
        jQuery(jqgridTableId).jqGrid({
            datatype: "json",
            url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/GetAllTripManagerCheckListGroup',
            mtype: 'POST',
            cache: false,
            async: false,
            ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
            serializeGridData: function (postData) {
                if (typeof postData.filters === "undefined") {
                    postData.filters = "";
                }
                postData.IsInActive = status;//$("#chkSearchActiveOnly").prop('checked');
                return JSON.stringify(postData);
            },
            colNames: ['CheckGroupID', 'Code', 'Description'],
            colModel: [
                { name: 'CheckGroupID', index: 'CheckGroupID', hidden: true, frozen: true, sortable: true, search: true },
                { name: 'CheckGroupCD', index: 'CheckGroupCD', width: 150, frozen: true, sortable: true, search: true },
                { name: 'CheckGroupDescription', index: 'CheckGroupDescription', width: 250, frozen: true, sortable: true, search: true }

            ],
            scrollOffset: 0,
            ignoreCase:true,
            ondblClickRow: function () {
                returnToParent();
            }
        });
        $("#dgTripManager").jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
        $("#dgTripManager").jqGrid('filterToolbar', {
            searchResult: true,
            defaultSearch: 'cn',
            searchOnEnter: false,
        });
    });
    self.btnSearch_Click = function () {       
        $(jqgridTableId).jqGrid('GridUnload');
        self.IsInActive($("#chkSearchActiveOnly").prop('checked'));
    };
    self.IsInActive(false);
}

