﻿var LegOutbound;
var selectedRowData = null;
var isodateformat = 'YYYY-MM-DD';
var jqgridTableId = '#gridLegSummary';
var legNum = 1;
$(function () {
    LegOutbound = function () {
        var outbound = null;
        outbound = new PreflightLegOutboundInstructionViewModel(1);
        return {
            outbound: this.outbound,
        };
    };
    ko.applyBindings(new LegOutbound(), document.getElementById('OuterDiv'));
});
var self = this;

var PreflightLegOutboundInstructionViewModel = function (newValue) {
    self.LegNum = ko.observable();
    self.EditMode = ko.observable(false);
    self.CurrentLegOutBoundData = ko.observable();
    var DateFormat;
   

    self.LegNum.subscribe(function (legNum) {
        self.EditMode(false);
        $.ajax({
            async: false,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetLegOutboundInstruction",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'LegNum': legNum }),
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    var jsonObj = result.d.Result;
                    if (jsonObj != null) {
                        if (self.CurrentLegOutBoundData.LegNUM == undefined) {
                            self.CurrentLegOutBoundData = ko.mapping.fromJS(jsonObj.CurrentLegOutBoundData);
                            DateFormat = jsonObj.CurrentLegOutBoundData.UserPrincipal._ApplicationDateFormat.toUpperCase();
                            FormatDateProperties(jsonObj.CurrentLegOutBoundData.UserPrincipal._ApplicationDateFormat.toUpperCase());
                        } else {
                            ko.mapping.fromJS(jsonObj.CurrentLegOutBoundData, self.CurrentLegOutBoundData);
                            DateFormat = jsonObj.CurrentLegOutBoundData.UserPrincipal._ApplicationDateFormat.toUpperCase();
                            FormatDateProperties(jsonObj.CurrentLegOutBoundData.UserPrincipal._ApplicationDateFormat.toUpperCase());
                        }
                    }
                    var isCalender = getQuerystring("IsCalender", 0);
                    if (!jsonObj.EditMode && isCalender == 0) {
                        $("#recordEdit").hide();
                    }
                    else {
                        $("#recordEdit").show();
                    }
                }
            },
            failure: function (response) {
                jAlert(response.d, "Preflight-Outbound");
            }
        });
    });
    self.btnEdit_Click=function() {
        self.EditMode(true);
    };
    self.btnSave_Click = function () {
        var isCalender = getQuerystring("IsCalender", 0);
        if(self.CurrentLegOutBoundData.PreflightTripOutbounds()!=null){
            for(var i=0;i<self.CurrentLegOutBoundData.PreflightTripOutbounds().length;i++) {
                self.CurrentLegOutBoundData.PreflightTripOutbounds()[i].LastUpdTS = ko.observable(self.CurrentLegOutBoundData.PreflightTripOutbounds()[i].LastUpdTS()).extend({ dateFormat: isodateformat }); 
            }
        }
        var preflightLegOutBoundIns = ko.mapping.toJS(self.CurrentLegOutBoundData);
        if (preflightLegOutBoundIns != null && preflightLegOutBoundIns != '') {
            $.ajax({
                dataType: "json",
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightLegOutbound",
                type: 'POST',               
                data: JSON.stringify({ 'preflightLegOutBoundIns': preflightLegOutBoundIns}),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    if (returnResult == true && result.d.Success == true) {
                        var jsonObj = result.d.Result;
                        ko.mapping.fromJS(jsonObj.CurrentLegOutBoundData, self.CurrentLegOutBoundData);
                        self.EditMode(false);
                        if (isCalender == 1)
                            OISavePreflightTrip();
                    }
                },
                error: function (data) {
                }
               
            });
        }
    };
    self.btnCancel_Click = function () {
        self.EditMode(false);
    };
    self.LegNum(newValue);
    ko.extenders.dateFormat = function (target, format) {
        //create a writable computed observable to intercept writes to our observable
        if (target() == null) {
            return target();
        }
        var result = ko.pureComputed({
            read: target,  //always return the original observables value
            write: function (newValue) {
                if (newValue != null) {
                    valueToWrite = moment(newValue).format(format);
                    target(valueToWrite);
                }
            }
        }).extend({ notify: 'always' });
        result(target());
        return result;
    };

    ko.bindingHandlers.numeric = {
        init: function (element, valueAccessor) {
            $(element).on("keydown", function (event) {
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                    (event.keyCode >= 35 && event.keyCode <= 39)) {
                    return;
                }
                else {
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
        }
    };
    ko.bindingHandlers.trimedValue = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            $(element).on("change", function () {
                var observable = valueAccessor();
                var trimedValue = $.trim($(this).val());
                observable($(this).val());
                observable(trimedValue);
            });
        },
        update: function (element, valueAccessor) {
            var value = ko.utils.unwrapObservable(valueAccessor());
            var trimedValue = $.trim(value);
            $(element).val(trimedValue);
        }
    };

    function FormatDateProperties(ApplicationDateFormat) {
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.DepartureDTTMLocal()))
            self.CurrentLegOutBoundData.DepartureDTTMLocal = ko.observable(self.CurrentLegOutBoundData.DepartureDTTMLocal()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.ArrivalDTTMLocal()))
            self.CurrentLegOutBoundData.ArrivalDTTMLocal = ko.observable(self.CurrentLegOutBoundData.ArrivalDTTMLocal()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.ArrivalGreenwichDTTM()))
            self.CurrentLegOutBoundData.ArrivalGreenwichDTTM = ko.observable(self.CurrentLegOutBoundData.ArrivalGreenwichDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.DepartureGreenwichDTTM()))
            self.CurrentLegOutBoundData.DepartureGreenwichDTTM = ko.observable(self.CurrentLegOutBoundData.DepartureGreenwichDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.HomeArrivalDTTM()))
            self.CurrentLegOutBoundData.HomeArrivalDTTM = ko.observable(self.CurrentLegOutBoundData.HomeArrivalDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.HomeDepartureDTTM()))
            self.CurrentLegOutBoundData.HomeDepartureDTTM = ko.observable(self.CurrentLegOutBoundData.HomeDepartureDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.NextGMTDTTM()))
            self.CurrentLegOutBoundData.NextGMTDTTM = ko.observable(self.CurrentLegOutBoundData.NextGMTDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.NextHomeDTTM()))
            self.CurrentLegOutBoundData.NextHomeDTTM = ko.observable(self.CurrentLegOutBoundData.NextHomeDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.NextLocalDTTM()))
            self.CurrentLegOutBoundData.NextLocalDTTM = ko.observable(self.CurrentLegOutBoundData.NextLocalDTTM()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.LastUpdTS()))
            self.CurrentLegOutBoundData.LastUpdTS = ko.observable(self.CurrentLegOutBoundData.LastUpdTS()).extend({ dateFormat: isodateformat });
        if (!IsNullOrEmptyOrUndefined(self.CurrentLegOutBoundData.Passenger.LastUpdTS()))
            self.CurrentLegOutBoundData.Passenger.LastUpdTS = ko.observable(self.CurrentLegOutBoundData.Passenger.LastUpdTS()).extend({ dateFormat: isodateformat });
    }
}

function GetLegDetail(legNumber) {
    self.LegNum(legNumber);
}

$(document).ready(function () {
    UserIdentity(PopupLoad);
    $.extend(jQuery.jgrid.defaults, {
        prmNames: {
            page: "page", rows: "size", order: "dir", sort: "sort"
        }
    });

    jQuery(jqgridTableId).jqGrid({
        datatype: "json",
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/TripLegsDetail',
        mtype: 'POST',
        cache: false,
        async: false,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        colNames: ['LegID', 'Leg', 'Departure', 'From', 'To', 'Arrival'],
        colModel: [
            { name: 'LegID', index: 'LegID', hidden: true, frozen: true, sortable: true, search: true },
            { name: 'LegNUM', index: 'LegNUM', width: 40, frozen: true, sortable: true, search: true },
            { name: 'DepartureDTTMLocal', index: 'DepartureDTTMLocal', width: 150, frozen: true, sortable: true, search: true },
            { name: 'DepartICAOID', index: 'DepartICAOID', width: 88, frozen: true, sortable: true, search: true },
            { name: 'ArriveICAOID', index: 'ArriveICAOID', width: 88, frozen: true, sortable: true, search: true },
            { name: 'ArrivalDTTMLocal', index: 'ArrivalDTTMLocal', width: 150, frozen: true, sortable: true, search: true }
        ],
        scrollOffset: 0,
        ondblClickRow: function (rowId) {
            GetLegDetail(rowId);
        },
        onSelectRow: function (id) {
            GetLegDetail(id);
            //document.getElementById("hdnLegNUM").value = id - 1;
        },
        gridComplete: function () {
            var top_rowid = $('#gridLegSummary tr:nth-child(2)').attr('id');
            $("#gridLegSummary").setSelection(top_rowid, true);
        }
    });
    $("#gridLegSummary").jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
            
});

function PopupLoad(Identity) {
    if (Identity._fpSettings._RptTabOutbdInstLab1 != null) {
        $('[id$="lblOutboundInstruction1"]').text(Identity._fpSettings._RptTabOutbdInstLab1).show();
        $('[id$="tbOutboundInstruction1"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction1"]').text('').hide();
        $('[id$="tbOutboundInstruction1"]').hide();
    }

    if (Identity._fpSettings._RptTabOutbdInstLab2 != null) {
        $('[id$="lblOutboundInstruction2"]').text(Identity._fpSettings._RptTabOutbdInstLab2).show();
        $('[id$="tbOutboundInstruction2"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction2"]').text('').hide();
        $('[id$="tbOutboundInstruction2"]').hide();
    }

    if (Identity._fpSettings._RptTabOutbdInstLab3 != null) {
        $('[id$="lblOutboundInstruction3"]').text(Identity._fpSettings._RptTabOutbdInstLab3).show();
        $('[id$="tbOutboundInstruction3"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction3"]').text('').hide();
        $('[id$="tbOutboundInstruction3"]').hide();
    }
    if (Identity._fpSettings._RptTabOutbdInstLab4 != null) {
        $('[id$="lblOutboundInstruction4"]').text(Identity._fpSettings._RptTabOutbdInstLab4).show();
        $('[id$="tbOutboundInstruction4"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction4"]').text('').hide();
        $('[id$="tbOutboundInstruction4"]').hide();
    }

    if (Identity._fpSettings._RptTabOutbdInstLab5 != null) {
        $('[id$="lblOutboundInstruction5"]').text(Identity._fpSettings._RptTabOutbdInstLab5).show();
        $('[id$="tbOutboundInstruction5"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction5"]').text('').hide();
        $('[id$="tbOutboundInstruction5"]').hide();
    }

    if (Identity._fpSettings._RptTabOutbdInstLab6 != null) {
        $('[id$="lblOutboundInstruction6"]').text(Identity._fpSettings._RptTabOutbdInstLab6).show();
        $('[id$="tbOutboundInstruction6"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction6"]').text('').hide();
        $('[id$="tbOutboundInstruction6"]').hide();
    }

    if (Identity._fpSettings._RptTabOutbdInstLab7 != null) {
        $('[id$="lblOutboundInstruction7"]').text(Identity._fpSettings._RptTabOutbdInstLab7).show();
        $('[id$="tbOutboundInstruction7"]').show();
    }
    else {
        $('[id$="lblOutboundInstruction7"]').text('').hide();
        $('[id$="tbOutboundInstruction7"]').hide();
    }
}

function OISavePreflightTrip() {
    $.ajax({
        async: true,
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightTrip",
        type: "POST",
        dataType: "json",
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (result) {
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        complete: function () {
        }
    });
}
