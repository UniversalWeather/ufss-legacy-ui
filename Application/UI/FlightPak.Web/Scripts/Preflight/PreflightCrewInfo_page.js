﻿var ispnlCrewAvailibilityExpand = true;
var tableHtml = ' <table id="CrewsAvailabilityGridID" class="table table-striped table-hover table-bordered preflight_crew_availability_grid"></table>';

function checknumericValue(myfield) {
    var postbackval = true;
    if (myfield.value.length > 0) {
        if (myfield.value.indexOf(".") >= 0) {
            if (myfield.value.indexOf(".") > 0) {
                var strarr = myfield.value.split(".");
                if (strarr[0].length > 3) {
                    alert("Invalid format, Required format is ###.## ");
                    myfield.value = "0.00";
                    myfield.focus();
                    postbackval = false;

                }
                if (strarr[1].length > 2) {
                    alert("Invalid format, Required format is ###.## ");
                    myfield.value = "0.00";
                    myfield.focus();
                    postbackval = false;
                }
            }
        }
        else {
            if (myfield.value.length > 3) {
                alert("Invalid format, Required format is ###.## ");
                myfield.value = "0.00";
                myfield.focus();
                postbackval = false;
            }
        }
    }

    return postbackval;
}

function focusMaxcap() {
    if (!IsNullOrEmpty($("#tbMaxcapamt").val())) {
        $("#tbMaxcapamt").val($("#tbMaxcapamt").val().replace(new RegExp(',', 'g'), '').replace('$', ''));
    }
}

function blurMaxcap() {
    if (!IsNullOrEmpty($("#tbMaxcapamt").val())) {
        var num = $("#tbMaxcapamt").val().replace(new RegExp(',', 'g'), '').replace('$', '');
        var p = Number(num).toFixed(2).split(".");
        var formatedString = "$" + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
            return num + (i && !(i % 3) ? "," : "") + acc;
        }, "") + "." + p[1];
        $("#tbMaxcapamt").val(formatedString);
    }
}

function checkMaxcap() {
    var postbackval = true;
    var val = CurrentCrewHotel.MaxCapAmount;
    val($("#tbMaxcapamt").val());
    if (val().length > 0) {
        if (val().indexOf(".") >= 0) {
                var strarr = val().split(".");
                if (strarr.length == 2) {
                    if (strarr[0].length > 7) {
                        val("0.00");
                        jAlert("Invalid format, Required format is #######.## ", "Preflight Crew Info", function() {
                            $("#tbMaxcapamt").focus();
                        });
                        postbackval = false;
                    }
                } else {
                    val("0.00");
                    jAlert("Invalid format, Required format is #######.## ", "Preflight Crew Info", function () {
                        $("#tbMaxcapamt").focus();
                    });
                }
        }
    }
    return postbackval;
}

function checkRadNumericCapValue(sender, eventArgs) {
    var value = sender.get_value().toString();
    if (sender.get_textBoxValue().length > 0) {
        if (value.indexOf('.') >= 0) {
            if (value.indexOf('.') > 0) {
                var strarr = value.split('.');
                if (strarr[0].length > 7) {
                    alert("Invalid format, Required format is #######.## ");
                    sender.set_value('0.00');
                    sender.focus();
                    sender.selectAllText();
                }
                if (strarr[1].length > 2) {
                    alert("Invalid format, Required format is #######.## ");
                    sender.set_value('0.00');
                    sender.focus();
                    sender.selectAllText();
                }
            }
        }
        else {
            if ((sender.get_textBoxValue().length > 7) && (value != "0")) {
                alert("Invalid format, Required format is #######.## ");
                sender.set_value('0.00');
                sender.focus();
                sender.selectAllText();
            }
        }
    }
}

function validateButtonList() {
    var arrivalSel = isArrivalSelected();
    jConfirm("Changing Arrival or Departure will clear hotel information. Do you want to continue?", "Confirmation", function (r) {
        if (r) {
            self.ResetCurrentCrewHotel(["isArrivalHotel"]);
            if (arrivalSel === "1") {
                self.CurrentCrewHotel.isArrivalHotel(false);
                self.CurrentCrewHotel.isDepartureHotel(true);
            } else {
                self.CurrentCrewHotel.isArrivalHotel(true);
                self.CurrentCrewHotel.isDepartureHotel(false);
            }

            addNewHotel(self.CurrentCrewHotelLegNum(), self.CurrentCrewHotel.isArrivalHotel());
        }
        else {
            if (arrivalSel === "1") {
                self.CurrentCrewHotel.isArrivalHotel(true);
                self.CurrentCrewHotel.isDepartureHotel(false);
            } else {
                self.CurrentCrewHotel.isArrivalHotel(false);
                self.CurrentCrewHotel.isDepartureHotel(true);
            }

        }
    });
}

function openWinCrew(radWin) {
    var url = '';
    if (radWin == "rdCrewGroupPopup") {
        url = '/Views/Settings/People/CrewGroupPopup.aspx?CrewGroupCD=' + document.getElementById('tbCrewGroup').value;
    }
    if (radWin == "radHotelPopup") {
        var arrivalSel = isArrivalSelected();
        var hotelCode=document.getElementById('tbHotelCode').value;
        
        if (arrivalSel == "0") {
            url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('hdnArrivalICao').value + (hotelCode==undefined|| hotelCode=='' ?'': '&HotelCD=' + document.getElementById('tbHotelCode').value);
        }
        else {
            url = '/Views/Settings/Logistics/HotelPopup.aspx?IcaoID=' + document.getElementById('hdnDepartIcao').value + (hotelCode == undefined || hotelCode == '' ? '' : '&HotelCD=' + document.getElementById('tbHotelCode').value);
        }

    }
    if (radWin == "rdTransportPopup") {
        url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + self.Preflight.PreflightLegs()[parseInt(self.CurrentCrewTransportLegNum()) - 1].DepartICAOID() + '&TransportCD=' + document.getElementById('tbTransCode').value;
    }
    if (radWin == "rdArriveTransportPopup") {
        url = '/Views/Settings/Logistics/TransportCatalogPopup.aspx?IcaoID=' + self.Preflight.PreflightLegs()[parseInt(self.CurrentCrewTransportLegNum())-1].ArriveICAOID() + '&TransportCD=' + document.getElementById('tbArriveTransCode').value;
    }
    if (radWin == "RadRetrievePopup") {
        url = '/Views/Transactions/Preflight/PreflightSearch.aspx';
    }
    if (radWin == "rdChecklist") {
        url = '../../Transactions/Preflight/PreflightChecklist.aspx';
    }
    if (radWin == "rdOutBoundInstructions") {
        url = '../../Transactions/Preflight/PreflightOutboundInstruction.aspx';
    }
    if (radWin == "rdMetroPopup") {
        url = '/Views/Settings/Company/MetroMasterPopup.aspx?MetroCD=' + document.getElementById('tbMetro').value;
    }
    if (radWin == "rdCtryPopup") {
        url = '/Views/Settings/Company/CountryMasterPopup.aspx?CountryCD=' + document.getElementById('tbCtry').value;
    }
    if (url != "" && radWin != "rdOutBoundInstructions" && radWin != "rdChecklist") {
        var oWnd = radopen(url, radWin);
    }
    else if (radWin == "rdOutBoundInstructions"){
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Outbound Instructions - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Outbound Instructions - New Tripsheet");
        }
    }
    else if (radWin == "rdChecklist") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Checklist - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Checklist - New Tripsheet");
        }
    }

}

function AdjustRadWidow() {
    try {
        var manager = GetRadWindowManager();
        var oWindow = manager.GetActiveWindow();

    }
    catch (err) {
        var oWindow = GetRadWindow();
    }
    setTimeout(function () { oWindow.autoSize(true); if ($telerik.isChrome || $telerik.isSafari) ChromeSafariFix(oWindow); }, 10);
}
//fix for Chrome/Safari due to absolute positioned popup not counted as part of the content page layout
function ChromeSafariFix(oWindow) {
    var iframe = oWindow.get_contentFrame();
    var w = $('.box1').width();
    var h = $('.jqgrid').height();
    var elementwidth = $('.ui-jqgrid-bdiv').width();
    var elementheight = $('.ui-jqgrid-bdiv').outerHeight();

    setTimeout(function () {

        oWindow.set_width(w);
        oWindow.center();
    }, 310);
}

function ConfirmClose(WinName) {
    var oManager = GetRadWindowManager();
    var oWnd = oManager.GetWindowByName(WinName);
    //Find the Close button on the page and attach to the 
    //onclick event
    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
    CloseButton.onclick = function () {
        CurrentWinName = oWnd.Id;
        //radconfirm is non-blocking, so you will need to provide a callback function
        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
    }
}

function OnClientHotelClose(oWnd, args) {
    //get the transferred arguments
    var lbcvHotelCode = document.getElementById("lbcvHotelCode");
    $(lbcvHotelCode).val("");
    var btn=$('#btnCode');
    var arg = args.get_argument();
    Start_Loading(btn);
    if(arg!==null) {
        if(arg) {
            self.CurrentCrewHotel.HotelID(arg.HotelID);
            self.CurrentCrewHotel.HotelCode(arg.HotelCD);
            self.CurrentCrewHotel.AirportID(arg.AirportID);
            self.CurrentCrewHotel.PreflightHotelName(arg.Name);
            self.CurrentCrewHotel.Rate(arg.NegociatedRate);
            if(arg.HotelID!="") {
                FetchHotelDetailbyHotelID();
            } else
                if(arg.HotelCD!="") {
                    FetchHotelDetailbyHotelCD();
                } else {
                    self.ResetCurrentCrewHotel(["isArrivalHotel"]);
                }
            End_Loading(btn,true);
        } else {
            self.ResetCurrentCrewHotel(["isArrivalHotel"]);
            End_Loading(btn,false);
        }
    } else {
        End_Loading(btn, false);
    }
}

function OnClientMetroClose(oWnd, args) {
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.CurrentCrewHotel.MetroCD(arg.MetroCD);
            self.CurrentCrewHotel.MetroID(arg.MetroID);

            if (arg.MetroCD != null)
                document.getElementById("lbcvMetro").innerHTML = "";

            fireOnChangeMetro();
        }
        else {
            self.CurrentCrewHotel.MetroCD("");
            self.CurrentCrewHotel.MetroID("");
        }
    }
}

function OnClientCtryClose(oWnd, args) {

    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            self.CurrentCrewHotel.CountryCode(arg.CountryCD);
            self.CurrentCrewHotel.CountryID(arg.CountryID);
            if (arg.CountryCD != null)
                document.getElementById("lbcvCtry").innerHTML = "";
            fireOnChangeCountry();
        }
        else {
            self.CurrentCrewHotel.CountryCode("");
            self.CurrentCrewHotel.CountryID("");
        }
    }
}

function OnClientTransportClose(oWnd, args) {
    var combo = $find("tbTransCode");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnTransport");
            Start_Loading(btn);
            self.CurrentCrewTransport.DepartCode(arg.TransportCD);
            self.CurrentCrewTransport.DepartName(arg.TransportationVendor);
            self.CurrentCrewTransport.DepartPhone(arg.PhoneNum);
            self.CurrentCrewTransport.DepartFax(arg.FaxNum);
            self.CurrentCrewTransport.DepartRate(arg.NegotiatedRate);            
            self.CurrentCrewTransport.DepartTransportId(arg.TransportID);            
            
            if (arg.TransportCD != null)
                document.getElementById("lbcvTransCode").innerHTML = "";

            End_Loading(btn, true);
        }
        else {
            self.CurrentCrewTransport.DepartCode("");
            self.CurrentCrewTransport.DepartName("");
            self.CurrentCrewTransport.DepartPhone("");
            self.CurrentCrewTransport.DepartFax("");
            self.CurrentCrewTransport.DepartRate("");
            self.CurrentCrewTransport.DepartureAirportId(null);
            self.CurrentCrewTransport.DepartTransportId(0);
        }
    }
}

function OnClientArriveTransportClose(oWnd, args) {
    var combo = $find("tbArriveTransCode");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnArriveTrans");
            Start_Loading(btn);
            self.CurrentCrewTransport.ArrivalCode(arg.TransportCD);
            self.CurrentCrewTransport.ArrivalName(arg.TransportationVendor);
            self.CurrentCrewTransport.ArrivalPhone(arg.PhoneNum);
            self.CurrentCrewTransport.ArrivalFax(arg.FaxNum);
            self.CurrentCrewTransport.ArrivalRate(arg.NegotiatedRate);            
            self.CurrentCrewTransport.ArrivalTransportId(arg.TransportID);

            if (arg.TransportCD != null)
                document.getElementById("lbcvArriveTransCode").innerHTML = "";

            End_Loading(btn, true);
        }
        else {
            self.CurrentCrewTransport.ArrivalCode("");
            self.CurrentCrewTransport.ArrivalName("");
            self.CurrentCrewTransport.ArrivalPhone("");
            self.CurrentCrewTransport.ArrivalFax("");
            self.CurrentCrewTransport.ArrivalRate("");
            self.CurrentCrewTransport.ArrivalAirportId(null);
            self.CurrentCrewTransport.ArrivalTransportId(0);
        }
    }
}

function ValidateCrewAssignment(callBackFunction) {
    setAlertTextToYesNo();
   jConfirm('Warning - Crew Not Assigned To Any Legs And Will Not Be Saved. Continue Without Saving Your Entry', 'Preflight - CrewInfo', function (reply) {
       if (reply) {
           self.IsCrewAssignedConfirm(true);
            callBackFunction();
        }
   });
   setAlertTextToOkCancel();
}
function CheckCrewDutyAssigned()
{
    var isCrewAssign = false;
    var rowSelect = true;
    var rowid;
    var nextRowId = "";
    var value = "";
    $("#CrewsSelectionID").find('select').each(function () {
        
        rowid = $(this).attr("id").replace("ddl", "");
        
        if (nextRowId == "")
            nextRowId = rowid;
        if (rowSelect == true && nextRowId != rowid) {
            isCrewAssign = true;
            nextRowId = rowid;

        }
        else if (rowSelect == false && nextRowId != rowid)
        {
            rowSelect = true;
            nextRowId = rowid;
        }
       value = $(this).val();
        if (value != "0" ) {
            rowSelect = false;
            
        }
    });

    if (rowSelect == true && nextRowId == rowid) {
        isCrewAssign = true;
    }
    window.Save_CrewInfo("save_on_crew");

    return isCrewAssign;
}
function ImgbtnSelectedCrewlistButtonClick() {
    if (selectedCrewsList.length > 0) {
        PopulateCrewSelectionGrid(selectedCrewsList);
        $(jqgridTableId).jqGrid('resetSelection');
        selectedCrewsList = new Array();
    }
    return false;
};

function PopulateCrewSelectionGrid(selectedCrews) {
    var oArg = new Object();
    var Crews = new Array();
    for (rowId = 0; rowId < selectedCrews.length; rowId++) {
        var dtNewAssign = new Object();
        dtNewAssign.CrewID = selectedCrews[rowId].CrewID;
        dtNewAssign.CrewCD = $(selectedCrews[rowId].CrewCD).text();
        dtNewAssign.CrewName = selectedCrews[rowId].CrewName;
        dtNewAssign.IsNotified = false;
        dtNewAssign.OrderNUM = 0;
        dtNewAssign.City = selectedCrews[rowId].CityName;
        dtNewAssign.State = selectedCrews[rowId].StateName;
        dtNewAssign.Postal = selectedCrews[rowId].PostalZipCD;
        dtNewAssign.AddlNotes = selectedCrews[rowId].Notes2;
        dtNewAssign.PassportID = "";
        dtNewAssign.VisaID = "";
        dtNewAssign.Passport = "";
        dtNewAssign.PassportExpiryDT = null;
        dtNewAssign.Notes = selectedCrews[rowId].Notes;
        dtNewAssign.Nation = "";
        dtNewAssign.IsStatus = selectedCrews[rowId].IsStatus;
        dtNewAssign.Street = selectedCrews[rowId].Addr1;
        Crews.push(dtNewAssign);
    }
    oArg.val = Crews;
    if (oArg) {
        AddCrews(oArg.val);
        document.getElementById('hdnCrewInfo').value = oArg.val;
    }
}

function OnRowSelecting(sender, args) {
}

function FetchHotelDetailbyHotelCD() {
    var airportid;
    var arrivalSel = isArrivalSelected();

    if (arrivalSel == "0")
        airportid = document.getElementById("hdnArrivalICao").value;
    else
        airportid = document.getElementById("hdnDepartIcao").value;


    var lbcvHotelCode = document.getElementById("lbcvHotelCode");
    var btnCode = document.getElementById("btnCode");
    var hcode = document.getElementById("tbHotelCode").value;

    $('#lbcvCtry').html("");
    $('#lbcvMetro').html("");

    CrewHotelValidation(airportid, hcode, self.CurrentCrewHotel, lbcvHotelCode, btnCode);

}

function FetchHotelDetailbyHotelID() {
    document.getElementById("lbcvHotelCode").innerHTML = "";
    try {
        $("#divloading").show();
        var hotelid = document.getElementById("hdnHotelID").value;
        var data = GetHotelDetailByHotelID(hotelid);
        if (data.success == "true") {
            self.CurrentCrewHotel.HotelID(hotelid);
            self.CurrentCrewHotel.HotelCode(GetValidatedValue(data.d, "HotelCD"));
            self.CurrentCrewHotel.PreflightHotelName(GetValidatedValue(data.d,"Name"));
            self.CurrentCrewHotel.Rate(GetValidatedValue(data.d, "NegociatedRate"));
            self.CurrentCrewHotel.PhoneNum1(GetValidatedValue(data.d, "PhoneNum"));
            self.CurrentCrewHotel.FaxNUM(GetValidatedValue(data.d, "FaxNum"));
            self.CurrentCrewHotel.Address1(GetValidatedValue(data.d, "Addr1"));
            self.CurrentCrewHotel.Address2(GetValidatedValue(data.d, "Addr2"));
            self.CurrentCrewHotel.Address3(GetValidatedValue(data.d, "Addr3"));
            self.CurrentCrewHotel.CityName(GetValidatedValue(data.d, "CityName"));
            self.CurrentCrewHotel.StateName(GetValidatedValue(data.d, "StateName"));

            if (data.d.MetroCD != null) {
                self.CurrentCrewHotel.MetroCD(GetValidatedValue(data.d, "MetroCD"));
                self.CurrentCrewHotel.MetroID(GetValidatedValue(data.d, "MetroID"));
            }
            if (data.d.CountryCD != null) {
                self.CurrentCrewHotel.CountryCode(GetValidatedValue(data.d, "CountryCD"));
                self.CurrentCrewHotel.CountryID(GetValidatedValue(data.d, "CountryID"));
            }            
            self.CurrentCrewHotel.PostalCode(GetValidatedValue(data.d, "PostalZipCD"));
        }
        else {
            self.ResetCurrentCrewHotel(["isArrivalHotel"]);
            document.getElementById("lbcvHotelCode").innerHTML = "Hotel Code Does Not Exist -pg";
            document.getElementById("tbHotelCode").focus();
        }
        $("#divloading").hide();
    }
    catch(ex)
    {
        $("#divloading").hide();
    }
}

function fireOnChange() {
    document.getElementById("lbcvHotelCode").innerHTML = "";
    var hcode = document.getElementById("tbHotelCode").value;
    FetchHotelDetailbyHotelCD();

    return true;
}

function fireOnChangeCountry() {
    document.getElementById("lbcvCtry").innerHTML = "";
    var Ctrycode = document.getElementById("tbCtry").value;
    var lbcvCtry = document.getElementById("lbcvCtry");
    var btnCtry = document.getElementById("btnCtry");
    CrewHotelCountryValidation(self.CurrentCrewHotel, Ctrycode, lbcvCtry, btnCtry);

    return true;
}

function fireOnChangeMetro() {
    document.getElementById("lbcvMetro").innerHTML = "";
    var Metrocode = document.getElementById("tbMetro").value;
    var lbcvMetro = document.getElementById("lbcvMetro");
    var btnMetro = document.getElementById("btnMetro");
    CrewHotelMetroValidation(self.CurrentCrewHotel, Metrocode, lbcvMetro, btnMetro);

    return true;
}

function fireOnChangeTransCode() {
    
    
    var btnTransport = document.getElementById("btnTransport");
    var airportid = document.getElementById("hdnTransAirID").value;
    var TransCode = document.getElementById("tbTransCode").value;
    var lbTransCode = document.getElementById("lbTransCode");
    var lbcvTransCode = document.getElementById("lbcvTransCode");
    
    if (IsNullOrEmpty(TransCode)) {
        self.CurrentCrewTransport.DepartCode("");
        self.CurrentCrewTransport.DepartName("");
        self.CurrentCrewTransport.DepartPhone("");
        self.CurrentCrewTransport.DepartFax("");
        self.CurrentCrewTransport.DepartRate("");
        self.CurrentCrewTransport.DepartureAirportId(null);
        self.CurrentCrewTransport.DepartTransportId(0);
        SaveCrewTransportation();
    }

    var ErrorMsg = "Depart Transport Code Does Not Exist";
    CrewTransportValidation(TransCode, airportid, self.CurrentCrewTransport, false, btnTransport, ErrorMsg, "lbTransCode", "lbcvTransCode");
}

function fireOnChangeArriveTransCode() {
    var btnArriveTrans = document.getElementById("btnArriveTrans");
    var airportid = document.getElementById("hdnArriveTransAirID").value;
    var TransCode = document.getElementById("tbArriveTransCode").value;
    
    if (IsNullOrEmpty(TransCode)) {
        self.CurrentCrewTransport.ArrivalCode("");
        self.CurrentCrewTransport.ArrivalName("");
        self.CurrentCrewTransport.ArrivalPhone("");
        self.CurrentCrewTransport.ArrivalFax("");
        self.CurrentCrewTransport.ArrivalRate("");
        self.CurrentCrewTransport.ArrivalAirportId(null);
        self.CurrentCrewTransport.ArrivalTransportId(0);
        SaveCrewTransportation();
    }

    var ErrorMsg = "Arrival Transport Code Does Not Exist";
    CrewTransportValidation(TransCode, airportid, self.CurrentCrewTransport, true, btnArriveTrans, ErrorMsg, "lbArriveTransCode", "lbcvArriveTransCode");
}

function pnlCrewAvailabilityExpand() {
    ispnlCrewAvailibilityExpand = true;
    $("#tdOfCrewsAvailabilityTable").html(tableHtml);
}
function pnlCrewAvailabilityCollapse() {
    ispnlCrewAvailibilityExpand = false;
    $(jqgridTableId).jqGrid("GridUnload");
}
function pnlCrewAvailabilityAnimationEnd() {
    if (!ispnlCrewAvailibilityExpand) {
        $("#tdOfCrewsAvailabilityTable").html(tableHtml);
    } else {
        $(jqgridTableId).jqGrid("GridUnload");
        $("#tdOfCrewsAvailabilityTable").html(tableHtml);
        InitializeCrewAvailibilityGrid();
    }
}


