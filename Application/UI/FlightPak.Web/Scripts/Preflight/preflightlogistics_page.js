﻿
var isDepartCatering = false;
var ispnlFuelExpand = true;
var ispnlCateringExpand = true;
var ispnlFBOHandlerExpand = true;
var tableHtml = '<table id="LogisticsFuelGrid" class="table table-striped table-hover table-bordered preflightlogistic_fuelgrid"></table>';
$(document).ready(function () {
    var DFBOCode = document.getElementById("tbDepartFBOCode").value;
    if (!IsNullOrEmpty(DFBOCode) && self.EditMode() != undefined && self.EditMode() != false) {
        fireOnChangeDepartFBOCode(false);
    }

    var AFBOCode = document.getElementById("tbArriveFBOCode").value;
    
    if (!IsNullOrEmpty(AFBOCode) && self.EditMode() != undefined && self.EditMode()!=false) {
        fireOnChangeArriveFBOCode(false);
    }
});
function pnlFuelExpand()
{
    ispnlFuelExpand = true;    
    $("#tdOfFuelTable").html(tableHtml);
}
function pnlFuelCollapse()
{
    ispnlFuelExpand = false;
    $(jqGridLogisticsEntityGrid).jqGrid("GridUnload");
}
function pnlFuelAnimationEnd()
{    
    if (!ispnlFuelExpand) {
        $("#tdOfFuelTable").html(tableHtml);
    } else {
        $(jqGridLogisticsEntityGrid).jqGrid("GridUnload");
        $("#tdOfFuelTable").html(tableHtml);
        InitializeLogisticsFuelGrid();
    }
}
function pnlCateringExpand() {
    ispnlCateringExpand = true;
}
function pnlCateringCollapse() {
    ispnlCateringExpand = false;
}
function pnlFBOHandlerExpand() {
    ispnlFBOHandlerExpand = true;
}
function pnlFBOHandlerCollapse() {
    ispnlFBOHandlerExpand = false;
}


function ShowAirportInfoPopup(Airportid) {
    window.radopen("/Views/Transactions/Preflight/PreflightAirportInfo.aspx?AirportID=" + Airportid, "rdAirportPage");
    return false;
}
function EnterToTab(sender, event) {
    if ((event.keyCode == 13) && (browserName().toLowerCase() == "chrome")) {
        ajaxManager.ajaxRequest("tbEstimatedGallons_TextChanged");
        if (event.preventDefault) {
            event.preventDefault();
        }
        else {
            event.returnValue = false;
        }

    }
}

function openFBOWin(type) {
    var url = '';
    if (type == "DEPART") {
        url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('hdDepartIcaoID').value + '&FBOID=' + document.getElementById('hdDepartFBOID').value + '&FBOCD=' + document.getElementById('tbDepartFBOCode').value;
        var oWnd = radopen(url, 'RadFBOCodePopupDepart');
    }
    else {
        url = '/Views/Settings/Logistics/FBOPopup.aspx?IcaoID=' + document.getElementById('hdArriveIcaoID').value + '&FBOID=' + document.getElementById('hdArriveFBOID').value + '&FBOCD=' + document.getElementById('tbArriveFBOCode').value;
        var oWnd = radopen(url, 'RadFBOCodePopup');
    }

}
function openCateringWin(type) {
    var url = '';
    if (type == "DEPART") {
        url = '/Views/Settings/Logistics/CaterPopup.aspx?IcaoID=' + document.getElementById('hdDepartIcaoID').value + '&CateringCD=' + document.getElementById('tbDepartCaterCode').value;
        var oWnd = radopen(url, 'radCateringPopup');
        isDepartCatering = true;
        oWnd.remove_close(OnClientCaterClose2)
        oWnd.add_close(OnClientCaterClose1);
    }
    else {
        url = '/Views/Settings/Logistics/CaterPopup.aspx?IcaoID=' + document.getElementById('hdArriveIcaoID').value + '&CateringCD=' + document.getElementById('tbArriveCaterCode').value;
        var oWnd = radopen(url, 'radCateringPopup');
        isDepartCatering = false;
        oWnd.remove_close(OnClientCaterClose1)
        oWnd.add_close(OnClientCaterClose2);
    }

}



function openWin(radWin) {

    var url = '';


    if (radWin == "rdChecklist") {
        url = '../../Transactions/Preflight/PreflightChecklist.aspx';
    }
    else if (radWin == "rdOutBoundInstructions") {
        url = '../../Transactions/Preflight/PreflightOutboundInstruction.aspx';
    }
    if (radWin == "RadRetrievePopup") {
        url = '../../Transactions/Preflight/PreflightSearch.aspx';
    }
    if (url != '' && radWin != "rdChecklist" && radWin != "rdOutBoundInstructions") {
        var oWnd = radopen(url, radWin);
    }
    else if (radWin == "rdOutBoundInstructions") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Outbound Instructions - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Outbound Instructions - New Tripsheet");
        }
    }
    else if (radWin == "rdChecklist") {
        var oWnd = radopen(url, radWin);
        if (self.TripID() != 0 && self.TripID() != null) {
            oWnd.set_title("Checklist - Trip Number: " + self.Preflight.PreflightMain.TripNUM() + " And Tail Number: " + self.Preflight.PreflightMain.Fleet.TailNum());
        } else {
            oWnd.set_title("Checklist - New Tripsheet");
        }
    }
}

function ConfirmClose(WinName) {
    var oManager = GetRadWindowManager();
    var oWnd = oManager.GetWindowByName(WinName);
    //Find the Close button on the page and attach to the 
    //onclick event
    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
    CloseButton.onclick = function () {
        CurrentWinName = oWnd.Id;
        //radconfirm is non-blocking, so you will need to provide a callback function
        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
    }
}

function OnClientFBOClose1(oWnd, args) {
    var combo = $find("#tbDepartFBOCode");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            document.getElementById("tbDepartFBOCode").value = arg.FBOCD;
            isAfterGetDepartFBOCall = false;
            fireOnChangeDepartFBOCode(true);
        }
        else {
            document.getElementById("hdDeptFBOAirID").value = "";
            document.getElementById("tbDepartFBOCode").value = "";
            document.getElementById("hdDepartFBOID").value = "";
            document.getElementById("lblInputDepartName").innerHTML = "";
            document.getElementById("lblInputDepartPhone1").innerHTML = "";
            document.getElementById("lblInputDepartPhone2").innerHTML = "";
            document.getElementById("lblInputDepartFax").innerHTML = "";
            document.getElementById("lblInputDepartCity").innerHTML = "";
            document.getElementById("lblInputDepartState").innerHTML = "";
            document.getElementById("lblInputDepartZipPostalCode").innerHTML = "";
            document.getElementById("lblDepartName").innerHTML = "";
            document.getElementById("lblDepartPhone1").innerHTML = "";
            document.getElementById("lblDepartPhone2").innerHTML = "";
            document.getElementById("lblDepartFax").innerHTML = "";
            document.getElementById("lblDepartCity").innerHTML = "";
            document.getElementById("lblDepartState").innerHTML = "";
            document.getElementById("lblDepartZipPostalCode").innerHTML = "";
            document.getElementById("lblInputUnicom").innerHTML = "";
            document.getElementById("lblInputArinc").innerHTML = "";
            combo.clearSelection();
        }
    }
}

function OnClientFBOClose2(oWnd, args) {
    var combo = $find("#tbArriveFBOCode");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            document.getElementById("tbArriveFBOCode").value = arg.FBOCD;
            if (arg.FBOCD != null)
                document.getElementById("lbcvArriveFBOCode").innerHTML = "";

            
            isAfterGetArrivalFBOCall = false;
            fireOnChangeArriveFBOCode(true);
            
        }
        else {
            document.getElementById("hdArrFBOAirID").value = "";
            document.getElementById("tbArriveFBOCode").value = "";
            document.getElementById("hdArriveFBOID").value = "";
            document.getElementById("lblInputArriveName").innerHTML = "";
            document.getElementById("lblInputArrivePhone1").innerHTML = "";
            document.getElementById("lblInputArrivePhone2").innerHTML = "";
            document.getElementById("lblInputArriveFax").innerHTML = "";
            document.getElementById("lblInputArriveCity").innerHTML = "";
            document.getElementById("lblInputArriveState").innerHTML = "";
            document.getElementById("lblInputArriveZipPostalCode").innerHTML = "";
            document.getElementById("lblArriveName").innerHTML = "";
            document.getElementById("lblArrivePhone1").innerHTML = "";
            document.getElementById("lblArrivePhone2").innerHTML = "";
            document.getElementById("lblArriveFax").innerHTML = "";
            document.getElementById("lblArriveCity").innerHTML = "";
            document.getElementById("lblArriveState").innerHTML = "";
            document.getElementById("lblArriveZipPostalCode").innerHTML = "";
            document.getElementById("lblInputArriveUnicom").innerHTML = "";
            document.getElementById("lblInputArriveArinc").innerHTML = "";
            combo.clearSelection();
        }
    }
}

function RemoveconfirmCommentCallBackFn(arg) {
    var paraLogistics = ko.mapping.toJS(Logistics);
    paraLogistics = removeUnwantedPropertiesKOViewModel(paraLogistics);

    delete paraLogistics.PreflightLeg.PreflightTripOutbounds;

    $.ajax({
        async: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/btnDepartRetainCommentConfirmClick",
        data: JSON.stringify({ 'preLogistics': paraLogistics, "isYesClick": arg }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (arg == false) {
                    Logistics.PreflightDepFboListViewModel.Comments("");
                    Logistics.PreflightDepFboListViewModel.ConfirmationStatus("");
                }
                $("#lbcvDepartFBOCode").text(response.d.Result.fbocodemsg);

                if (response.d.Result.open.length > 0) {
                    if (response.d.Result.open.length > 1)
                        confirmPreviousFBOCallBackFnConfirmBoxOpen();
                }
            }
        },
        failure: function (response) {
            jAlert(response.d, "Preflight");
        }
    });

}

function RemoveArriveconfirmCommentCallBackFn(arg) {
    var paraLogistics = ko.mapping.toJS(Logistics);
    paraLogistics = removeUnwantedPropertiesKOViewModel(paraLogistics);
    delete paraLogistics.PreflightLeg.PreflightTripOutbounds;
    $.ajax({
        async: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/btnArrivalRetainCommentYes_Click",
        data: JSON.stringify({ 'preLogistics': paraLogistics, "isYesClick": arg }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (arg == false) {
                    Logistics.PreflightArrFboListViewModel.Comments("");
                    Logistics.PreflightArrFboListViewModel.ConfirmationStatus("");
                }
                $("#lbcvArriveFBOCode").text(response.d.Result.fbocodemsg);

                if (response.d.Result.open.length > 0) {
                    if (response.d.Result.open.length > 1)
                        confirmNextFBOCallBackFnConfirmBoxOpen();
                }
            }
        },
        failure: function (response) {
            jAlert(response.d, "Preflight");
        }
    });

}


function RemoveCaterConfirmCommentCallBackFn(arg) {
    var paraLogistics = ko.mapping.toJS(Logistics);
    paraLogistics = removeUnwantedPropertiesKOViewModel(paraLogistics);
    delete paraLogistics.PreflightLeg.PreflightTripOutbounds;
    $.ajax({
        async: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/btnDepartCaterRetainCommentYes_Click",
        data: JSON.stringify({ 'preLogistics': paraLogistics, "isYesClick": arg }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (arg == false) {
                    Logistics.DepartLogisticsCatering.CateringComments("");
                    Logistics.DepartLogisticsCatering.CateringConfirmation("");
                }
                $("#lbcvDepartCaterCode").text(response.d.Result.cateringcodemsg);
            }
        },
        failure: function (response) {
            jAlert(response.d, "Preflight");
        }
    });

}

function RemoveArriveCaterConfirmCommentCallBackFn(arg) {               
    var paraLogistics = ko.mapping.toJS(Logistics);
    paraLogistics = removeUnwantedPropertiesKOViewModel(paraLogistics);
    delete paraLogistics.PreflightLeg.PreflightTripOutbounds;
    $.ajax({
        async: false,
        type: "POST",
        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/btnArrivalCaterRetainCommentYes_Click",
        data: JSON.stringify({ 'preLogistics': paraLogistics, "isYesClick": arg }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var returnResult = verifyReturnedResult(response);
            if (returnResult == true && response.d.Success == true) {
                if (arg == false) {
                    Logistics.ArrivalLogisticsCatering.CateringComments("");
                    Logistics.ArrivalLogisticsCatering.CateringConfirmation("");
                }
                $("#lbcvArriveCaterCode").text(response.d.Result.cateringcodemsg);
            }
        },
        failure: function (response) {
            jAlert(response.d, "Preflight");
        }
    });
}

function confirmNextFBOCallBackFnConfirmBoxOpen() {
    jConfirm("Do you want to use the current Arrival FBO to update the next leg’s Departure FBO?", "Confirmation!", confirmNextFBOCallBackFn);
}

function confirmNextFBOCallBackFn(arg) {
    if (arg == true) {
        //Call btnNextFBOYesClick
        var paraLogistics = ko.mapping.toJS(Logistics);
        paraLogistics = removeUnwantedPropertiesKOViewModel(paraLogistics);
        delete paraLogistics.PreflightLeg.PreflightTripOutbounds;
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/btnNextFBOYesClick",
            data: JSON.stringify({ 'preLogistics': paraLogistics }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success:function(response) {
                var returnResult = verifyReturnedResult(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {                
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });

    }
}
function confirmPreviousFBOCallBackFnConfirmBoxOpen() {
    jConfirm("Do you want to update the prior leg’s Arrival FBO?", "Confirmation!", confirmPreviousFBOCallBackFn);
}
function confirmPreviousFBOCallBackFn(arg) {
    if (arg == true) {
        //call btnPreviousFBOYesClick
        var paraLogistics = ko.mapping.toJS(Logistics);
        paraLogistics = removeUnwantedPropertiesKOViewModel(paraLogistics);
        delete paraLogistics.PreflightLeg.PreflightTripOutbounds;
        $.ajax({
            async: true,
            type: "POST",
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/btnPreviousFBOYesClick",
            data: JSON.stringify({ 'preLogistics': paraLogistics }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var returnResult = verifyReturnedResult(response);
            },
            error: function (jqXHR, textStatus, errorThrown) {                
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });

    }
}

function fireOnChangeDepartFBOCode(Check) {
    var FBOCode = document.getElementById("tbDepartFBOCode").value;
    FBOCode = $.trim(FBOCode);

    var hdnLeg = Logistics.PreflightLeg.LegNUM();
    Logistics.PreflightDepFboListViewModel.AirportID(document.getElementById("hdDepartIcaoID").value);
    var airportid = Logistics.PreflightDepFboListViewModel.AirportID;
    var hdDepartFBOID = Logistics.PreflightDepFboListViewModel.FBOID;
    var tbDepartFBOCode = Logistics.PreflightDepFboListViewModel.FBOCD;
    var lbcvFBOCode = document.getElementById("lbcvDepartFBOCode");
    var lblDepartName = Logistics.PreflightDepFboListViewModel.PreflightFBOName;
    var lblDepartPhone1 = Logistics.PreflightDepFboListViewModel.PhoneNum1;
    var lblDepartPhone2 = Logistics.PreflightDepFboListViewModel.PhoneNum2;
    var lblDepartFax = Logistics.PreflightDepFboListViewModel.FaxNUM;
    var lblDepartCity = Logistics.PreflightDepFboListViewModel.CityName;
    var lblDepartState = Logistics.PreflightDepFboListViewModel.StateName;
    var lblDepartZipPostalCode = Logistics.PreflightDepFboListViewModel.PostalZipCD;
    var lblUnicom = Logistics.PreflightDepFboListViewModel.UNICOM;
    var lblArinc = Logistics.PreflightDepFboListViewModel.ARINC;
    var lblDepartAddr1 = Logistics.PreflightDepFboListViewModel.Addr1;
    var lblDepartAddr2 = Logistics.PreflightDepFboListViewModel.Addr2;
    var lblDepartAddr3 = Logistics.PreflightDepFboListViewModel.Addr3;
    var lblDepartEmail = Logistics.PreflightDepFboListViewModel.EmailAddress;
    var tbDepartFBOComments = Logistics.PreflightDepFboListViewModel.Comments;
    var tbDepartFBOConfirmation = Logistics.PreflightDepFboListViewModel.ConfirmationStatus;
    var State = Logistics.PreflightDepFboListViewModel.State;
    var PreflightFBOId=Logistics.PreflightDepFboListViewModel.PreflightFBOID;
    var btnDepartFBOCode = document.getElementById("btnDepartFBOCode");

    var tbDepartFBOCommentsValue = tbDepartFBOComments();
    var tbDepartFBOConfirmationValue = tbDepartFBOConfirmation();
    var lblInputDepartName = lblDepartName();


    if (Check == true && IsNullOrEmpty(lblInputDepartName) && (!IsNullOrEmpty(tbDepartFBOConfirmationValue) || !IsNullOrEmpty(tbDepartFBOCommentsValue))) {
        jConfirm("Do you want to retain your Confirmation and Comments?", "Confirmation!", RemoveconfirmCommentCallBackFn);
    }
    else {
        var ErrorMsg = "FBO/Handler Code Does Not Exist";
        var alertMsg = "Do you want to update the prior leg’s Departure FBO?";
        FBOValidation(airportid, FBOCode, tbDepartFBOCode, hdDepartFBOID, lbcvFBOCode, lblDepartName, lblDepartPhone1, lblDepartPhone2, lblDepartFax, lblDepartCity, lblDepartState, lblDepartZipPostalCode, lblUnicom, lblArinc, lblDepartAddr1, lblDepartAddr2, lblDepartAddr3, lblDepartEmail, tbDepartFBOComments, tbDepartFBOConfirmation, ErrorMsg, btnDepartFBOCode, hdnLeg, alertMsg, true, Check, PreflightFBOId, State);
    }
}
function fireOnChangeArriveFBOCode(Check) {
    var FBOCode = document.getElementById("tbArriveFBOCode").value;
    FBOCode = $.trim(FBOCode);
    var hdnLeg = Logistics.PreflightLeg.LegNUM();

    Logistics.PreflightArrFboListViewModel.AirportID(document.getElementById("hdArriveIcaoID").value);
    var airportid = Logistics.PreflightArrFboListViewModel.AirportID;
    var hdArriveFBOID = Logistics.PreflightArrFboListViewModel.FBOID;
    var tbArriveFBOCode = Logistics.PreflightArrFboListViewModel.FBOCD;
    var lbcvFBOCode = document.getElementById("lbcvArriveFBOCode");
    var lblArriveName = Logistics.PreflightArrFboListViewModel.PreflightFBOName;
    var lblArrivePhone1 = Logistics.PreflightArrFboListViewModel.PhoneNum1;
    var lblArrivePhone2 = Logistics.PreflightArrFboListViewModel.PhoneNum2;
    var lblArriveFax = Logistics.PreflightArrFboListViewModel.FaxNUM;
    var lblArriveCity = Logistics.PreflightArrFboListViewModel.CityName;
    var lblArriveState = Logistics.PreflightArrFboListViewModel.StateName;
    var lblArriveZipPostalCode = Logistics.PreflightArrFboListViewModel.PostalZipCD;
    var lblUnicom = Logistics.PreflightArrFboListViewModel.UNICOM;
    var lblArinc = Logistics.PreflightArrFboListViewModel.ARINC;
    var lblArriveAddr1 = Logistics.PreflightArrFboListViewModel.Addr1;
    var lblArriveAddr2 = Logistics.PreflightArrFboListViewModel.Addr2;
    var lblArriveAddr3 = Logistics.PreflightArrFboListViewModel.Addr3;
    var lblArriveEmail = Logistics.PreflightArrFboListViewModel.EmailAddress;
    var tbArriveFBOComments = Logistics.PreflightArrFboListViewModel.Comments;
    var tbArriveFBOConfirmation = Logistics.PreflightArrFboListViewModel.ConfirmationStatus;
    var btnArriveFBOCode = document.getElementById("btnArriveFBOCode");
    var PreflightFBOId = self.Logistics.PreflightArrFboListViewModel.PreflightFBOID;
    var State=self.Logistics.PreflightArrFboListViewModel.State;

    var tbArriveFBOCommentsValue = tbArriveFBOComments();
    var tbArriveFBOConfirmationValue = tbArriveFBOConfirmation();
    var lblInputArriveName = document.getElementById("lblInputArriveName").value;


    if (Check == true && IsNullOrEmpty(lblInputArriveName) && (!IsNullOrEmpty(tbArriveFBOConfirmationValue) || !IsNullOrEmpty(tbArriveFBOCommentsValue))) {
        jConfirm("Do you want to retain your Confirmation and Comments?", "Confirmation!", RemoveArriveconfirmCommentCallBackFn);
    }
    else {
        var ErrorMsg = "FBO/Handler Code Does Not Exist";
        var alertMsg = "Do you want to use the current Arrival FBO to update the next leg’s Departure FBO?";
        FBOValidation(airportid,FBOCode,tbArriveFBOCode,hdArriveFBOID,lbcvFBOCode,lblArriveName,lblArrivePhone1,lblArrivePhone2,lblArriveFax,lblArriveCity,lblArriveState,lblArriveZipPostalCode,lblUnicom,lblArinc,lblArriveAddr1,lblArriveAddr2,lblArriveAddr3,lblArriveEmail,tbArriveFBOComments,tbArriveFBOConfirmation,ErrorMsg,btnArriveFBOCode,hdnLeg,alertMsg,false,Check,PreflightFBOId,State);
    }
}
function fireOnChangeDepartCaterCode() {

    var DepartCaterCode = document.getElementById("tbDepartCaterCode").value;
    Logistics.DepartLogisticsCatering.AirportID(document.getElementById("hdDepartIcaoID").value);
    var airportid = Logistics.DepartLogisticsCatering.AirportID;
    var hdDepartCaterID = Logistics.DepartLogisticsCatering.CateringID;
    var tbDepartCaterCode = Logistics.DepartLogisticsCatering.CateringCD;
    var tbDepartCaterName = Logistics.DepartLogisticsCatering.ContactName;
    var tbDepartCaterPhone = Logistics.DepartLogisticsCatering.ContactPhone;
    var tbDepartCaterFax = Logistics.DepartLogisticsCatering.ContactFax;
    var tbDepartCaterCost = Logistics.DepartLogisticsCatering.Cost;
    var tbDepartContactName = Logistics.DepartLogisticsCatering.CateringContactName;
    var tbDepartCaterConfirmation = Logistics.DepartLogisticsCatering.CateringConfirmation;
    var tbDepartCaterComments = Logistics.DepartLogisticsCatering.CateringComments;
    var lbcvDepartCaterCode = document.getElementById("lbcvDepartCaterCode");
    var PreflightCateringId = self.Logistics.DepartLogisticsCatering.PreflightCateringID;
    var State=self.Logistics.DepartLogisticsCatering.State;
    var btn = document.getElementById("btnDepartCaterCode");
    var ErrorMsg = "Catering Code Does Not Exist";
    if (tbDepartCaterName() && (tbDepartCaterConfirmation() || tbDepartCaterComments())) {
        CaterValidation(DepartCaterCode, airportid, hdDepartCaterID, tbDepartCaterCode, tbDepartCaterName, tbDepartCaterPhone, tbDepartCaterFax, tbDepartCaterCost, tbDepartContactName, lbcvDepartCaterCode, ErrorMsg, btn, PreflightCateringId, State);
        jConfirm("Do you want to retain your Confirmation and Comments?", "Confirmation!", RemoveCaterConfirmCommentCallBackFn);
    }
    else {
        CaterValidation(DepartCaterCode, airportid, hdDepartCaterID, tbDepartCaterCode, tbDepartCaterName, tbDepartCaterPhone, tbDepartCaterFax, tbDepartCaterCost, tbDepartContactName, lbcvDepartCaterCode, ErrorMsg, btn, PreflightCateringId, State);
    }
}
function fireOnChangeArriveCaterCode() {
    var ArriveCaterCode = document.getElementById("tbArriveCaterCode").value;
    
    Logistics.ArrivalLogisticsCatering.AirportID(document.getElementById("hdArriveIcaoID").value);
    var airportid = Logistics.ArrivalLogisticsCatering.AirportID;
    var tbArriveCaterCode = Logistics.ArrivalLogisticsCatering.CateringCD;
    var hdArriveCaterID = Logistics.ArrivalLogisticsCatering.CateringID;
    var tbArriveCaterCode = Logistics.ArrivalLogisticsCatering.CateringCD;
    var tbArriveCaterName = Logistics.ArrivalLogisticsCatering.ContactName;
    var tbArriveCaterPhone = Logistics.ArrivalLogisticsCatering.ContactPhone;
    var tbArriveCaterFax = Logistics.ArrivalLogisticsCatering.ContactFax;
    var tbArriveCaterCost = Logistics.ArrivalLogisticsCatering.Cost;
    var tbArriveContactName = Logistics.ArrivalLogisticsCatering.CateringContactName;
    var tbArriveCaterConfirmation = Logistics.ArrivalLogisticsCatering.CateringConfirmation;
    var tbArriveCaterComments = Logistics.ArrivalLogisticsCatering.CateringComments;
    var lbcvArriveCaterCode = document.getElementById("lbcvArriveCaterCode");
    var btn = document.getElementById("btnArriveCaterCode");
    var PreflightCateringId = self.Logistics.DepartLogisticsCatering.PreflightCateringID;
    var State = self.Logistics.DepartLogisticsCatering.State;
    
    var ErrorMsg = "Catering Code Does Not Exist";
    if (tbArriveCaterName() && (tbArriveCaterConfirmation() || tbArriveCaterComments())) {
        CaterValidation(ArriveCaterCode, airportid, hdArriveCaterID, tbArriveCaterCode, tbArriveCaterName, tbArriveCaterPhone, tbArriveCaterFax, tbArriveCaterCost, tbArriveContactName, lbcvArriveCaterCode, ErrorMsg, btn, PreflightCateringId, State);
        jConfirm("Do you want to retain your Confirmation and Comments?", "Confirmation!", RemoveArriveCaterConfirmCommentCallBackFn);
    }
    else {
        CaterValidation(ArriveCaterCode, airportid, hdArriveCaterID, tbArriveCaterCode, tbArriveCaterName, tbArriveCaterPhone, tbArriveCaterFax, tbArriveCaterCost, tbArriveContactName, lbcvArriveCaterCode, ErrorMsg, btn, PreflightCateringId,State);
    }
}


function OnClientCaterClose1(oWnd, args) {

    if (isDepartCatering == false) {
        OnClientCaterClose2(oWnd, args);
        return;
    }

    var combo = $find("tbDepartCaterCode");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnDepartCaterCode");
            Start_Loading(btn);
            Logistics.DepartLogisticsCatering.CateringID(arg.CateringID);
            Logistics.DepartLogisticsCatering.CateringCD(arg.CateringCD);
            Logistics.DepartLogisticsCatering.AirportID(arg.AirportID);
            Logistics.DepartLogisticsCatering.ContactName(arg.CateringVendor);
            Logistics.DepartLogisticsCatering.CateringContactName(arg.ContactName);
            Logistics.DepartLogisticsCatering.ContactFax(arg.FaxNum);
            Logistics.DepartLogisticsCatering.ContactPhone(arg.PhoneNum);
            Logistics.DepartLogisticsCatering.Cost(arg.NegotiatedRate);
            //            fireOnChangeDepartCaterCode();
            isAfterGetDepartCaterCall = false;
            End_Loading(btn, true);
            if (Logistics.DepartLogisticsCatering.ContactName() && (Logistics.DepartLogisticsCatering.CateringConfirmation() || Logistics.DepartLogisticsCatering.CateringComments())) {
                jConfirm("Do you want to retain your Confirmation and Comments?", "Confirmation!", RemoveCaterConfirmCommentCallBackFn);
            }
        }
        else {
            Logistics.DepartLogisticsCatering.CateringID("");
            Logistics.DepartLogisticsCatering.CateringCD("");
            Logistics.DepartLogisticsCatering.CateringContactName("");
            Logistics.DepartLogisticsCatering.ContactName("");
            Logistics.DepartLogisticsCatering.ContactFax("");
            Logistics.DepartLogisticsCatering.ContactPhone("");
            Logistics.DepartLogisticsCatering.Cost("");


            document.getElementById("hdDeptCaterAirID").value = "";
            document.getElementById("hdDepartCaterID").value = "";

            combo.clearSelection();
        }
    }
}

function OnClientCaterClose2(oWnd, args) {
    if (isDepartCatering == true) {
        OnClientCaterClose1(oWnd, args);
        return;
    }
    var combo = $find("tbArriveCaterCode");
    //get the transferred arguments
    var arg = args.get_argument();
    if (arg !== null) {
        if (arg) {
            var btn = document.getElementById("btnArriveCaterCode");
            Start_Loading(btn);
            Logistics.ArrivalLogisticsCatering.CateringCD(arg.CateringCD);
            Logistics.ArrivalLogisticsCatering.CateringID(arg.CateringID);
            Logistics.ArrivalLogisticsCatering.AirportID(arg.AirportID);
            Logistics.ArrivalLogisticsCatering.ContactName(arg.CateringVendor);
            Logistics.ArrivalLogisticsCatering.ContactPhone(arg.PhoneNum);
            Logistics.ArrivalLogisticsCatering.ContactFax(arg.FaxNum);
            Logistics.ArrivalLogisticsCatering.Cost(arg.NegotiatedRate);
            Logistics.ArrivalLogisticsCatering.CateringContactName(arg.ContactName);
            //            fireOnChangeArriveCaterCode();
            isAfterGetArrivalCaterCall = false;
            End_Loading(btn, true);
            if (Logistics.ArrivalLogisticsCatering.ContactName() && (Logistics.ArrivalLogisticsCatering.CateringConfirmation() || Logistics.ArrivalLogisticsCatering.CateringComments())) {
                jConfirm("Do you want to retain your Confirmation and Comments?", "Confirmation!", RemoveArriveCaterConfirmCommentCallBackFn);
            }
        }
        else {
            Logistics.ArrivalLogisticsCatering.CateringCD("");
            Logistics.ArrivalLogisticsCatering.CateringID("");
            Logistics.ArrivalLogisticsCatering.CateringContactName("");
            Logistics.ArrivalLogisticsCatering.ContactName("");
            Logistics.ArrivalLogisticsCatering.ContactFax("");
            Logistics.ArrivalLogisticsCatering.ContactPhone("");
            Logistics.ArrivalLogisticsCatering.Cost("");
            combo.clearSelection();
        }
    }
}
