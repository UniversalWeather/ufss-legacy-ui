﻿$(document).ready(function(){
    
});

/******** Javascript Model & jqgrid Code for Logistics *******/

var jqGridLogisticsEntityGrid = "#LogisticsFuelGrid";
var PropertyNamesList = "ConfirmationStatus,Comments,FBOCD,CateringComments,CateringConfirmation,CateringCD,CateringContactName,CateringID,FBOID,Status";
var isAfterGetDepartFBOCall = false;
var isAfterGetArrivalFBOCall = false;
var isAfterGetArrivalCaterCall = false;
var isAfterGetDepartCaterCall = false;
var LogisticsXhr = null,FuelGridXhr=null;
var timeoutToFireSaveCall = 500;
var lastUpdated = new Date((new Date()) - (60000));;



var LogisticsViewModel = function () {
    self.Logistics = {};    
    self.Legs = {};
    self.DepartLegDateLocal = ko.observable();
    self.ArrivalLegDateLocal = ko.observable();
    self.LegtabClick = function (legItem, event) {

        SaveLogisticsViewModel();
        document.getElementById("lbcvArriveFBOCode").innerHTML = "";
        document.getElementById("lbcvDepartFBOCode").innerHTML = "";
        document.getElementById("lbcvDepartCaterCode").innerHTML = "";
        document.getElementById("lbcvArriveCaterCode").innerHTML = "";

        $("#legTabLogistics").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
        $(event.currentTarget).addClass("tabStripItemSelected");


        $("#hdnSelectedLegNum").val(legItem.LegNUM());        
        Logistics.LegNUM(legItem.LegNUM());
        self.CurrentLogisticsLegNum(legItem.LegNUM());

        self.DepartLegDateLocal(moment(self.Preflight.PreflightLegs()[self.CurrentLogisticsLegNum() - 1].DepartureDTTMLocal()).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase()));
        self.ArrivalLegDateLocal(moment(self.Preflight.PreflightLegs()[self.CurrentLogisticsLegNum() - 1].ArrivalDTTMLocal()).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase()));

        if (FuelGridXhr && FuelGridXhr.readyState != 4) {
            FuelGridXhr.abort();
        }
        //$("#hdnUpdateClick").val("true");
        GetPreflightLogisticsDetailsByLegNum(legItem.LegNUM());
        reloadFuelGrid();

    };
    
    self.EstFuelQTYChangeCall = function () {
        reloadFuelGrid();
        EstimateGallonTextChange(true);
    };
    self.fuelTypeRadioClick = function () {
        if (Logistics.FuelAmountType() == "1") {
            Logistics.LblEstimateType("Imperial Gallons");
        } else if (Logistics.FuelAmountType() == "2") {
            Logistics.LblEstimateType("Liters");
        }else{
            Logistics.LblEstimateType("U.S. Gallons");
        }

        if (!IsNullOrEmpty(Logistics.EstimatedCost())) {
            self.EstFuelQTYChangeCall();
        }
        return true;
    };

    self.ButtonDisableCss = ko.pureComputed(function () {
        return self.EditMode() ? "button" : "button-disable";
    }, self);

    self.CateringArrivalChange = function () {
        isAfterGetArrivalCaterCall = false;
        fireOnChangeArriveCaterCode();
    };
    self.CateringDepartChange = function () {
        isAfterGetDepartCaterCall = false;
        fireOnChangeDepartCaterCode();
    };
    self.ArrivalFBOChange = function () {
        if (self.EditMode()) {
            isAfterGetArrivalFBOCall = false;            
        }
        fireOnChangeArriveFBOCode(true);
    };
    self.DepartFBOChange = function () {
        if (self.EditMode()) {
            isAfterGetDepartFBOCall = false;            
        }
        fireOnChangeDepartFBOCode(true);
    };

    self.CaterCommentOrConfirmationChange = function () { isAfterGetArrivalCaterCall = false; isAfterGetDepartCaterCall = false; timeoutToFireSaveCall = 0;};
    self.FBOCommentOrConfirmationChange = function () { isAfterGetArrivalFBOCall = false; isAfterGetDepartFBOCall = false; timeoutToFireSaveCall = 0; };

    self.CurrentLogisticsLegNum = ko.observable(1);

    var jsonSchema = JSON.parse(document.getElementById("hdnPreflightLogisticsInitializationObject").value);
    document.getElementById("hdnPreflightLogisticsInitializationObject").value = "";

    self.Logistics = ko.mapping.fromJS(jsonSchema.Logistics);
    self.Logistics.PreflightDepFboListViewModel = ko.mapping.fromJS(jsonSchema.FBO);
    self.Logistics.PreflightArrFboListViewModel = ko.mapping.fromJS(jsonSchema.FBO);
    self.Logistics.DepartLogisticsCatering = ko.mapping.fromJS(jsonSchema.Catering);
    self.Logistics.ArrivalLogisticsCatering = ko.mapping.fromJS(jsonSchema.Catering);

    ko.watch(self.Logistics.PreflightDepFboListViewModel, { depth: 1, tagFields: true }, function (parents, child, item) {
        var latestUpdate = new Date();
        var diff = Math.floor((latestUpdate - lastUpdated) / 600);

        if (jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 && diff>1) {
            setTimeout(function () {
                if (!isAfterGetDepartFBOCall && self.EditMode() == true) {
                    lastUpdated = new Date();
                    SaveLogisticsViewModel()
                }
            }, timeoutToFireSaveCall);
            timeoutToFireSaveCall = 500;
        }
    });

    ko.watch(self.Logistics.PreflightArrFboListViewModel, { depth: 1, tagFields: true }, function (parents, child, item) {

        var latestUpdate = new Date();
        var diff = Math.floor((latestUpdate - lastUpdated) / 600);

        if (jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 && diff > 1) {
            setTimeout(function () {
                if (!isAfterGetArrivalFBOCall && self.EditMode() == true) {
                    lastUpdated = new Date();
                    SaveLogisticsViewModel()
                }
            }, timeoutToFireSaveCall);
            timeoutToFireSaveCall = 500;
        }
    });


    ko.watch(self.Logistics.DepartLogisticsCatering, { depth: 1, tagFields: true }, function (parents, child, item) {

        var latestUpdate = new Date();
        var diff = Math.floor((latestUpdate - lastUpdated) / 600);

        if (jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 && diff > 1) {
            setTimeout(function () {
                if (!isAfterGetDepartCaterCall && self.EditMode() == true) {
                    lastUpdated = new Date();
                    SaveLogisticsViewModel()
                }
            }, timeoutToFireSaveCall);
            timeoutToFireSaveCall = 500;
        }
    });

    ko.watch(self.Logistics.ArrivalLogisticsCatering, { depth: 1, tagFields: true }, function (parents, child, item) {
        var latestUpdate = new Date();
        var diff = Math.floor((latestUpdate - lastUpdated) / 600);

        if (jQuery.inArray(child._fieldName, PropertyNamesList.split(",")) != -1 && diff > 1) {
            setTimeout(function () {
                if (!isAfterGetArrivalCaterCall && self.EditMode() == true) {
                    lastUpdated = new Date();
                    SaveLogisticsViewModel()
                }
            }, timeoutToFireSaveCall);
            timeoutToFireSaveCall = 500;
        }
    });

    FormatDatePropertiesLogistics();
    InitilizeLogisticsViewModels();
};

function InitilizeLogisticsViewModels() {


    GetLegsList();
    GetPreflightLogisticsDetailsByLegNum(currentPreflightLeg);

    InitializeSortSummaryDetail();
    InitializeDetailedLegSummary();

}

function reloadFuelGrid() {
    
    $(jqGridLogisticsEntityGrid).jqGrid('clearGridData');    
    $(jqGridLogisticsEntityGrid).trigger("reloadGrid");
}

function GetPreflightLogisticsDetailsByLegNum(legNum) {

    RequestStart();

    Logistics.PreflightDepFboListViewModel.Status("0");
    Logistics.PreflightArrFboListViewModel.Status("0");
    Logistics.DepartLogisticsCatering.Status("0");
    Logistics.ArrivalLogisticsCatering.Status("0");


    if (legNum == undefined) {
        legNum = 1;
      
    }
    var paramters = {};
    paramters.legNum = legNum;


    if (LogisticsXhr && LogisticsXhr.readyState != 4) {
        LogisticsXhr.abort();
    }

    LogisticsXhr=$.ajax({
                        async: true,
                        type: "POST",
                        url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetPreflightLogisticsByLegNum",
                        data: JSON.stringify(paramters),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var returnResult = verifyReturnedResult(response);
                            if (returnResult == true && response.d.Success == true) {
                                if (self.Logistics.PreflightLeg == undefined) {
                                    self.Logistics = ko.mapping.fromJS(response.d.Result);
                                    FormatDatePropertiesLogistics();
                                }
                                else {
                                    ko.mapping.fromJS(response.d.Result, self.Logistics);
                                    FormatDatePropertiesLogistics();
                                }
                                isAfterGetDepartFBOCall = true;
                                isAfterGetArrivalFBOCall = true;
                                isAfterGetArrivalCaterCall = true;
                                isAfterGetDepartCaterCall = true;
                                InitializeLogisticsFuelGrid();
                                
                                $("#legTabLogistics").find("li").removeClass("tabStripItemSelected").addClass("tabStripItem");
                                $("#Leg" + legNum).removeClass("tabStripItem").addClass("tabStripItemSelected");
                                
                            }
                            ResponseEnd();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            ResponseEnd();
                            reportPreflightError(jqXHR, textStatus, errorThrown);
                        }
                    });
}

function FormatDatePropertiesLogistics() {

    if (!IsNullOrEmptyOrUndefined(self.Preflight.PreflightLegs()[self.CurrentLogisticsLegNum() - 1].DepartureDTTMLocal()))
        self.DepartLegDateLocal(moment(self.Preflight.PreflightLegs()[self.CurrentLogisticsLegNum() - 1].DepartureDTTMLocal()).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase()));
    else
        self.DepartLegDateLocal("");

    if (!IsNullOrEmptyOrUndefined(self.Preflight.PreflightLegs()[self.CurrentLogisticsLegNum() - 1].ArrivalDTTMLocal()))
        self.ArrivalLegDateLocal(moment(self.Preflight.PreflightLegs()[self.CurrentLogisticsLegNum() - 1].ArrivalDTTMLocal()).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase()));
    else
        self.ArrivalLegDateLocal("");

    self.Logistics.PreflightLeg.DepartureDTTMLocal = ko.observable(self.Logistics.PreflightLeg.DepartureDTTMLocal()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.ArrivalDTTMLocal = ko.observable(self.Logistics.PreflightLeg.ArrivalDTTMLocal()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.ArrivalGreenwichDTTM = ko.observable(self.Logistics.PreflightLeg.ArrivalGreenwichDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.DepartureGreenwichDTTM = ko.observable(self.Logistics.PreflightLeg.DepartureGreenwichDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.HomeArrivalDTTM = ko.observable(self.Logistics.PreflightLeg.HomeArrivalDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.HomeDepartureDTTM = ko.observable(self.Logistics.PreflightLeg.HomeDepartureDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.NextGMTDTTM = ko.observable(self.Logistics.PreflightLeg.NextGMTDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.NextHomeDTTM = ko.observable(self.Logistics.PreflightLeg.NextHomeDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.NextLocalDTTM = ko.observable(self.Logistics.PreflightLeg.NextLocalDTTM()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.LastUpdTS = ko.observable(self.Logistics.PreflightLeg.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.ArrivalAirport.DayLightSavingStartDT = ko.observable(self.Logistics.PreflightLeg.ArrivalAirport.DayLightSavingStartDT()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.ArrivalAirport.DayLightSavingEndDT = ko.observable(self.Logistics.PreflightLeg.ArrivalAirport.DayLightSavingEndDT()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });

    self.Logistics.PreflightLeg.DepartureAirport.DayLightSavingStartDT = ko.observable(self.Logistics.PreflightLeg.ArrivalAirport.DayLightSavingStartDT()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
    self.Logistics.PreflightLeg.DepartureAirport.DayLightSavingEndDT = ko.observable(self.Logistics.PreflightLeg.ArrivalAirport.DayLightSavingEndDT()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });


}

function InitializeLogisticsFuelGrid() {
    var cnt = 1;
    var iswinsafari = isWindowsSafari();
    jQuery(jqGridLogisticsEntityGrid).jqGrid({
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/GetLogisticsFuelInfo',
        mtype: 'POST',
        datatype: "json",
        cache: false,
        async: true,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            if (postData._search == undefined || postData._search == false) {
                if (postData.filters === undefined) postData.filters = null;
            }
           
            postData.isEditMode = self.EditMode();
            postData.isUpdateButtonClick = $("#hdnUpdateClick").val() == undefined ? false : $("#hdnUpdateClick").val();
            postData.legNum = $("#hdnSelectedLegNum").val() == undefined ? "1" : $("#hdnSelectedLegNum").val();
            $("#hdnUpdateClick").val("false");
            return JSON.stringify(postData);
        },
        width:700,
        height: 270,
        sortable: false,
        shrinkToFit: false,
        cmTemplate: { resizable: !iswinsafari },
        gridview: true,        
        colNames: ["id","FuelVendorID","Vendor", "FBO", "Best Price","tempBestPrice", "Price As Of", "Price 1", "Range 1", "Price 2", "Range 2", "Price 3", "Range 3", "Price 4", "Range 4",
            "Price 5", "Range 5", "Price 6", "Range 6", "Price 7", "Range 7", "Price 8", "Range 8", "Price 9", "Range 9", "Price 10",
            "Range 10", "Price 11", "Range 11", "Price 12", "Range 12", "Price 13", "Range 13", "Price 14", "Range 14", "Price 15", "Range 15",
            "Price 16", "Range 16", "Price 17", "Range 17", "Price 18", "Range 18", "Price 19", "Range 19", "Price 20", "Range 20", "Price 21", "Range 21"],
        colModel: [
            {
                name: 'Id', index: 'Id', width: 60, key: true, hidden: true, frozen: true
            },
            { name: 'FuelVendorID', index: 'FuelVendorID', width: 80,  hidden: true, frozen: true },
            { name: 'VendorName', index: 'VendorName', width: 120, frozen: true, sortable: false },
            { name: 'FBOName', index: 'FBOName', width: 180, frozen: true, sortable: false },
            { name: 'BestPrice', index: 'BestPrice', width: 55, frozen: true, sortable: false },
            { name: 'BestPrice', index: 'BestPrice', width: 55, frozen: true, sortable: false,hidden:true },
            { name: 'EffectiveDT', index: 'EffectiveDT', width: 80, sortable: false, formatter: function (cellvalue, options, rowObject) { return moment(cellvalue).format(self.UserPrincipal._ApplicationDateFormat.toUpperCase()); } },
            { name: 'Price1', index: 'Price1', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange1', index: 'StartRange1', width: 70, sortable: false },
            { name: 'Price2', index: 'Price2', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange2', index: 'StartRange2', width: 70, sortable: false },
            { name: 'Price3', index: 'Price3', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange3', index: 'StartRange3', width: 70, sortable: false },
            { name: 'Price4', index: 'Price4', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange4', index: 'StartRange4', width: 70, sortable: false },
            { name: 'Price5', index: 'Price5', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange5', index: 'StartRange5', width: 70, sortable: false },
            { name: 'Price6', index: 'Price6', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange6', index: 'StartRange6', width: 70, sortable: false },
            { name: 'Price7', index: 'Price7', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange7', index: 'StartRange7', width: 70, sortable: false },
            { name: 'Price8', index: 'Price8', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange8', index: 'StartRange8', width: 70, sortable: false },
            { name: 'Price9', index: 'Price9', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange9', index: 'StartRange9', width: 70, sortable: false },
            { name: 'Price10', index: 'Price10', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange10', index: 'StartRange10', width: 70, sortable: false },
            { name: 'Price11', index: 'Price11', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange11', index: 'StartRange11', width: 70, sortable: false },
            { name: 'Price12', index: 'Price12', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange12', index: 'StartRange12', width: 70, sortable: false },
            { name: 'Price13', index: 'Price13', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange13', index: 'StartRange13', width: 70, sortable: false },
            { name: 'Price14', index: 'Price14', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange14', index: 'StartRange14', width: 70, sortable: false },
            { name: 'Price15', index: 'Price15', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange15', index: 'StartRange15', width: 70, sortable: false },
            { name: 'Price16', index: 'Price16', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange16', index: 'StartRange16', width: 70, sortable: false },
            { name: 'Price17', index: 'Price17', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange17', index: 'StartRange17', width: 70, sortable: false },
            { name: 'Price18', index: 'Price18', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange18', index: 'StartRange18', width: 70, sortable: false },
            { name: 'Price19', index: 'Price19', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange19', index: 'StartRange19', width: 70, sortable: false },
            { name: 'Price20', index: 'Price20', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange20', index: 'StartRange20', width: 70, sortable: false },
            { name: 'Price21', index: 'Price21', width: 50, sortable: false, formatter: currencyFormatter, formatoptions: { decimalPlaces: 4, prefix: "$" } },
            { name: 'StartRange21', index: 'StartRange21', width: 70, sortable: false }
        ],
        loadBeforeSend: function (xhr, settings)
        {
            FuelGridXhr = xhr;
        },
        onSelectRow: function (id, status) {
            var rowData = $(this).getRowData(id);

        },
        loadComplete: function () {
            if (!IsNullOrEmpty(Logistics.PreflightLeg.EstFuelQTY())) {
                EstimateGallonTextChange(false);
            }
            $('#tdOfFuelTable').append('<span id="widthTest" />');
            gridName = this.id;
            var columnNames = $("#" + gridName).jqGrid('getGridParam', 'colModel');
            var thisWidth;
            for (var itm = 0, itmCount = columnNames.length; itm < itmCount; itm++) {
                var curObj = $('[aria-describedby=' + gridName + '_' + columnNames[itm].name + ']');
                if ((columnNames[itm].name.indexOf('Price') > -1 || columnNames[itm].name.indexOf('Range') > -1) && columnNames[itm].name != 'BestPrice') {
                    var thisCell = $('#' + gridName + '_' + columnNames[itm].name + ' div');
                    $('#widthTest').html(thisCell[0].innerText).css({
                        'font-family': thisCell.css('font-family'),
                        'font-size': thisCell.css('font-size'),
                        'font-weight': thisCell.css('font-weight')
                    });
                    var maxWidth = Width =$('#widthTest').width()+5;
                    for (var itm2 = 0, itm2Count = curObj.length; itm2 < itm2Count; itm2++) {
                        var thisCell = $(curObj[itm2]);
                        $('#widthTest').html(thisCell.html()).css({
                            'font-family': thisCell.css('font-family'),
                            'font-size': thisCell.css('font-size'),
                            'font-weight': thisCell.css('font-weight')
                        });
                        thisWidth = $('#widthTest').width();
                        if (thisWidth > maxWidth) 
                            maxWidth = thisWidth;                    
                    }
                    maxWidth = ((isWindowsSafari() || isMacSafari()) ? (maxWidth + 15) : (maxWidth + 5));
                    $('[aria-describedby=' + gridName + '_' + columnNames[itm].name + ']').width(maxWidth);
                    var tdNo = $('[aria-describedby=' + gridName + '_' + columnNames[itm].name + ']').index();
                    $($('[aria-describedby=' + gridName + '_' + columnNames[itm].name + ']').parent().siblings()[0].getElementsByTagName('td')[tdNo]).css("width", maxWidth);
                    $('#' + gridName + '_' + columnNames[itm].name).width(maxWidth);
                }
            }
            $('#widthTest').remove();
            if (isWindowsSafari())
            {
                $('#LogisticsFuelGrid_frozen tbody tr td:nth-child(5)').css('width', '81px !important');
                $(".frozen-div .ui-jqgrid-htable #LogisticsFuelGrid_BestPrice").css("width", "81px !important");
                $('#LogisticsFuelGrid_BestPrice.ui-state-default.ui-th-column.ui-th-ltr:even').each(function () {
                    if ($(this).width() <= 17)
                    {
                        $(this).css('width', '81px !important');
                    }
                });
                if ($('.preflight-custom-grid-fuel div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                    $('.preflight-custom-grid-fuel div[class="ui-jqgrid-bdiv"]').scroll(function () {
                        if ($(this).scrollLeft() + $(this).innerWidth()-12 >= $(this)[0].scrollWidth) {
                            
                            $('.preflight_logistic_fuel_wrapper .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                        } else {
                            $('.preflight_logistic_fuel_wrapper .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');
                           
                        }
                    });
                }
               
            }
            else if (isMacSafari()) {
                $('#LogisticsFuelGrid_frozen tbody tr td:nth-child(5)').attr('style', $('#LogisticsFuelGrid_frozen tbody tr td:nth-child(5)').attr('style')+';width:60px !important;');
                $(".frozen-div .ui-jqgrid-htable #LogisticsFuelGrid_BestPrice:first").attr("style", $(".frozen-div .ui-jqgrid-htable #LogisticsFuelGrid_BestPrice").attr('style') + ";width:60px!important;");
                $('#LogisticsFuelGrid_BestPrice.ui-state-default.ui-th-column.ui-th-ltr:even').each(function () {
                    if ($(this).width() <= 17) {
                        $(this).attr('style', $(this).attr('style') + ';width:60px!important;');
                    }
                });

                if ($('.preflight-custom-grid-fuel div[class="ui-jqgrid-bdiv"]').hasScrollBar()) {

                    $('.preflight-custom-grid-fuel div[class="ui-jqgrid-bdiv"]').scroll(function () {
                        if ($(this).scrollLeft() + $(this).innerWidth() - 12 >= $(this)[0].scrollWidth) {

                            $('.preflight_logistic_fuel_wrapper .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                        } else {
                            $('.preflight_logistic_fuel_wrapper .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');
                        }
                    });
                }
            }
            else {
                $('.preflight-custom-grid-fuel div[class="ui-jqgrid-bdiv"]').scroll(function () {
                    if ($(this).scrollLeft() + $(this).innerWidth()-7  >= $(this)[0].scrollWidth) {

                        $('.preflight_logistic_fuel_wrapper .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '17px');
                    } else {
                        $('.preflight_logistic_fuel_wrapper .box1 .ui-jqgrid .ui-jqgrid-htable').eq(0).css('padding-right', '0px');
                    }
                });
            }

        }
    });

    jQuery(jqGridLogisticsEntityGrid).jqGrid('setFrozenColumns');
    $(".frozen-div").css("overflow", "hidden");

    
}

function currencyFormatter(cellValue, options, rowObject) {
 
    if(cellValue!=null)  {        
        return $.fn.fmatter.call(this, "currency", cellValue, options)        
    }
    else
        return "";
}

function EstimateGallonTextChange(showConfirmForFuelEstimateUpdate) {
        //var Trip = HttpContext.Current.Session[WebSessionKeys.CurrentPreflightTrip];
        var btn = $("#btnEstimateGallonsLoading");
        btn.css("display", "block");
        btn.show();

        var preLogistics = self.Logistics;

        var EstFuelQty = preLogistics.PreflightLeg.EstFuelQTY();

        preLogistics.VendorName("");
        //karthik
        preLogistics.FuelVendorText("");
        preLogistics.EstimatedCost("");

        //decimal.TryParse(preLogistics.PreflightLeg.EstFuelQTY.ToString(), out EstFuelQty);

        EstFuelQty = GetGallonConvertedValue(EstFuelQty,preLogistics);

        if (EstFuelQty > 0)
        {
            //   Trip.EstFuelQTY = EstFuelQty;
            //preLogistics.PreflightLeg.EstFuelQTY(EstFuelQty);
        }
        else
        {
            //Trip.EstFuelQTY = 0;
            preLogistics.PreflightLeg.EstFuelQTY(0);
        }
    
        var dgFuel = $(jqGridLogisticsEntityGrid).jqGrid('getRowData');
        var fuelRows = $(jqGridLogisticsEntityGrid).jqGrid('getDataIDs');

 

        if (fuelRows.length < 1) {
            btn.hide();
        }
        //Hashtable hashFuelGrid = new Hashtable();
        if (EstFuelQty >= 0)
        {
            // Reset whole Grid to initial color and state


            if (!IsNullOrEmpty(preLogistics.PreflightLeg.EstFuelQTY()))
            {
                // Call service        
                var Gallontocheck = 0;
                Gallontocheck=preLogistics.PreflightLeg.EstFuelQTY();
            
                Gallontocheck = GetGallonConvertedValue(Gallontocheck,preLogistics);

                var doRegularCalculation = false;

                for (i = 0; i < fuelRows.length; i++)
                {
                    var rowData = $(jqGridLogisticsEntityGrid).jqGrid('getRowData', fuelRows[i]);
                    if(!IsNullOrEmpty(rowData["StartRange1"]) && rowData["StartRange1"]!="0-0" && !IsNullOrEmpty("StartRange2")){
                        doRegularCalculation = true;
                        break;
                    }                
                }

                if (!doRegularCalculation)
                {
                
                    if (Gallontocheck > 0)
                    {    // Call Service
                        var BestFBOPriceList =null;

                        var paramters={};                    
                        paramters.departureAirportID=preLogistics.PreflightLeg.DepartureAirport.AirportID();
                        paramters.gallon=0;
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetFBOBestPrice",
                            data: JSON.stringify(paramters),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                                if (returnResult == true && response.d.Success == true && response.d.Result.ReturnFlag == true) {
                                    BestFBOPriceList = response.d.Result.results;

                                    if (BestFBOPriceList != null && BestFBOPriceList.length > 0) {
                                        var BestFuelList = BestFBOPriceList;

                                        if (BestFBOPriceList.length > 0) {
                                            var FirstbestOrice = 0;
                                            if (BestFuelList != null && BestFuelList.length > 0) {
                                                for (var jBFL = 0; jBFL < BestFuelList.length; jBFL++) {
                                                    var fpkFuel = BestFuelList[jBFL];
                                                    var startrange = "0-0";
                                                    for (var iBFL = 0; iBFL < dgFuel.length; iBFL++) {
                                                        var dataItem = dgFuel[iBFL];
                                                        var rowData = $(jqGridLogisticsEntityGrid).jqGrid('getRowData', fuelRows[iBFL]);
                                                        var rowid = rowData["Id"];
                                                        if (
                                                            (fpkFuel.FuelVendorID != null && dataItem.FuelVendorID != null && dataItem.FuelVendorID == fpkFuel.FuelVendorID) && (dataItem.FBOName == fpkFuel.FBOName)
                                                            ||
                                                            (dataItem.VendorName == "UWA") && (dataItem.FBOName == fpkFuel.FBOName)
                                                            ) {

                                                            for (var i = 1; i <= 21; i++) {
                                                                var tbprice = dataItem["Price" + i];
                                                                //tbprice = tbprice.substr(1);


                                                                var StartRange = dataItem["StartRange" + i];
                                                                if (tbprice != null) {
                                                                    tbprice = tbprice.replace("$", "");

                                                                    var price = parseFloat(tbprice);
                                                                    if (!IsNullOrEmpty(price)) {
                                                                        //toPrecision use for precision point
                                                                        if (price.toFixed(2) == fpkFuel.UnitPrice.toFixed(2) && StartRange == startrange) {

                                                                            var gridPrice = parseFloat(tbprice);
                                                                            if (true) {

                                                                                jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "Price" + i, "", "fc_ylwtextbox");
                                                                                jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "StartRange" + i, "", { 'background': '#EBF229' });

                                                                                if (gridPrice > 0) {
                                                                                    if (FirstbestOrice == 0) {

                                                                                        jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "BestPrice", "", "fc_grntextbox");

                                                                                        if (!IsNullOrEmpty(tbprice)) {
                                                                                            dataItem.BestPrice = parseFloat(tbprice);
                                                                                            setFrozenColumnsBestPriceValue(rowid, dataItem.BestPrice);
                                                                                            jQuery(jqGridLogisticsEntityGrid + "_frozen").setCell(rowid, "BestPrice", "$" + dataItem.BestPrice, "fc_grntextbox");
                                                                                            jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "BestPrice", "$" + dataItem.BestPrice, "fc_grntextbox");
                                                                                        }
                                                                                        preLogistics.VendorName(dataItem.VendorName);

                                                                                        //call service
                                                                                        var paramters = {};
                                                                                        paramters.fuelVendorID = dataItem.FuelVendorID;
                                                                                        paramters.hdDepartIcaoID = preLogistics.PreflightLeg.DepartureAirport.AirportID();
                                                                                        paramters.fboName = dataItem.FBOName;

                                                                                        $.ajax({
                                                                                            async: true,
                                                                                            type: "POST",
                                                                                            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetFuelVendorTextForLogisticsFuel",
                                                                                            data: JSON.stringify(paramters),
                                                                                            contentType: "application/json; charset=utf-8",
                                                                                            dataType: "json",
                                                                                            success: function (response) {
                                                                                                var returnResult = verifyReturnedResult(response);
                                                                                                if (returnResult == true && response.d.Success == true) {
                                                                                                    if (response.d.Result.flag == true) {
                                                                                                        preLogistics.FuelVendorText(response.d.Result.results);
                                                                                                    }
                                                                                                }
                                                                                            },
                                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                                btn.hide();
                                                                                                reportPreflightError(jqXHR, textStatus, errorThrown);
                                                                                            }
                                                                                        });
                                                                                        preLogistics.EstimatedCost("$" + parseFloat(Gallontocheck * fpkFuel.UnitPrice).toFixed(2));
                                                                                        FirstbestOrice++;

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (showConfirmForFuelEstimateUpdate == false)
                                        EstFuelQty = 0;

                                    AfterEstimateGallonExecutionComplete(EstFuelQty, preLogistics);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                btn.hide();
                                reportPreflightError(jqXHR, textStatus, errorThrown);                                
                            }                        
                        });
                        
                    
                    }
                }                               
                else
                {
                    //"For Best Price Check"
                    if (Gallontocheck > 0)
                    {
                        var BestFBOPriceList =null;

                        var paramters={};                    
                        paramters.departureAirportID=preLogistics.PreflightLeg.DepartureAirport.AirportID();
                        paramters.gallon=Gallontocheck;
                        $.ajax({
                            async: true,
                            type: "POST",
                            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetFBOBestPrice",
                            data: JSON.stringify(paramters),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var returnResult = verifyReturnedResult(response);
                                if (returnResult == true && response.d.Success == true && response.d.Result.ReturnFlag == true) {
                                    BestFBOPriceList = response.d.Result.results;

                                    if (BestFBOPriceList != null && BestFBOPriceList.length > 0) {
                                        var BestFuelList = BestFBOPriceList;

                                        if (BestFBOPriceList.length > 0) {
                                            var FirstbestOrice = 0;
                                            if (BestFuelList != null && BestFuelList.length > 0) {
                                                for (var jBFL = 0; jBFL < BestFuelList.length; jBFL++) {
                                                    var fpkFuel = BestFuelList[jBFL];

                                                    var startrange = "";
                                                    startrange = fpkFuel.GallonFrom.toString().substr(0, (fpkFuel.GallonFrom.toString().indexOf(".") > 0 ? fpkFuel.GallonFrom.toString().indexOf(".") : fpkFuel.GallonFrom.toString().length));
                                                    if (startrange == "") {
                                                        startrange = fpkFuel.GallonFrom.toString();
                                                    }

                                                    if (startrange != "")
                                                        startrange = startrange + "-" + fpkFuel.GallonTO.toString().substring(0, (fpkFuel.GallonTO.toString().indexOf(".") > 0 ? fpkFuel.GallonTO.toString().indexOf(".") : fpkFuel.GallonTO.toString().length));
                                                    for (var iBFL = 0; iBFL < dgFuel.length; iBFL++) {
                                                        var dataItem = dgFuel[iBFL];
                                                        var rowData = $(jqGridLogisticsEntityGrid).jqGrid('getRowData', fuelRows[iBFL]);
                                                        var rowid = rowData["Id"];
                                                        if (
                                                            (fpkFuel.FuelVendorID != null && dataItem.FuelVendorID != null && dataItem.FuelVendorID == fpkFuel.FuelVendorID) && (dataItem.FBOName == fpkFuel.FBOName)
                                                            ||
                                                            (dataItem.VendorName == "UWA") && (dataItem.FBOName == fpkFuel.FBOName)
                                                            ) {

                                                            for (var i = 1; i <= 21; i++) {
                                                                var tbprice = dataItem["Price" + i];


                                                                var StartRange = dataItem["StartRange" + i];
                                                                if (tbprice != null) {
                                                                    tbprice = tbprice.replace("$", "");

                                                                    var price = parseFloat(tbprice);
                                                                    if (!IsNullOrEmpty(price)) {
                                                                        //toPrecision use for precision point
                                                                        if (price.toFixed(2) == fpkFuel.UnitPrice.toFixed(2) && StartRange == startrange) {

                                                                            var gridPrice = parseFloat(tbprice);
                                                                            if (true) {

                                                                                jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "Price" + i, "", "fc_ylwtextbox");
                                                                                jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "StartRange" + i, "", { 'background': '#EBF229' });

                                                                                //dataItem["Price" + i].CssClass = "fc_ylwtextbox";
                                                                                //dataItem["StartRange" + i].BackColor = System.Drawing.Color.Yellow;

                                                                                if (gridPrice > 0) {
                                                                                    if (FirstbestOrice == 0) {
                                                                                        jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "BestPrice", "", "fc_grntextbox");
                                                                                        if (!IsNullOrEmpty(tbprice)) {
                                                                                            dataItem.BestPrice = parseFloat(tbprice);
                                                                                            jQuery(jqGridLogisticsEntityGrid).setCell(rowid, "BestPrice", "$" + dataItem.BestPrice, "fc_grntextbox");
                                                                                            jQuery(jqGridLogisticsEntityGrid + "_frozen").setCell(rowid, "BestPrice", "$" + dataItem.BestPrice, "fc_grntextbox");
                                                                                            setFrozenColumnsBestPriceValue(rowid, dataItem.BestPrice);
                                                                                        }
                                                                                        preLogistics.VendorName(dataItem.VendorName);

                                                                                        //call service
                                                                                        var paramters = {};
                                                                                        paramters.fuelVendorID = dataItem.FuelVendorID;
                                                                                        paramters.hdDepartIcaoID = preLogistics.PreflightLeg.DepartureAirport.AirportID();
                                                                                        paramters.fboName = dataItem.FBOName;

                                                                                        $.ajax({
                                                                                            async: true,
                                                                                            type: "POST",
                                                                                            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetFuelVendorTextForLogisticsFuel",
                                                                                            data: JSON.stringify(paramters),
                                                                                            contentType: "application/json; charset=utf-8",
                                                                                            dataType: "json",
                                                                                            success: function (response) {
                                                                                                var returnResult = verifyReturnedResult(response);
                                                                                                if (returnResult == true && response.d.Success == true) {
                                                                                                    if (response.d.Result.flag == true) {
                                                                                                        preLogistics.FuelVendorText(response.d.Result.results);
                                                                                                    }
                                                                                                }
                                                                                            },
                                                                                            error: function (jqXHR, textStatus, errorThrown) {
                                                                                                btn.hide();
                                                                                                reportPreflightError(jqXHR, textStatus, errorThrown);
                                                                                            }
                                                                                        });
                                                                                        preLogistics.EstimatedCost("$" + parseFloat(Gallontocheck * fpkFuel.UnitPrice).toFixed(2));
                                                                                        FirstbestOrice++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (showConfirmForFuelEstimateUpdate == false)
                                        EstFuelQty = 0;

                                    AfterEstimateGallonExecutionComplete(EstFuelQty, preLogistics);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                btn.hide();
                                reportPreflightError(jqXHR, textStatus, errorThrown);                                
                            }                        
                        }).done(function () {
                            btn.hide();
                        });

                   
                    }
                }            
            }
        }    
    }

function AfterEstimateGallonExecutionComplete(EstFuelQty, preLogistics) {
        SaveLogisticsViewModel();        
        $("#btnEstimateGallonsLoading").hide();
    }

function GetGallonConvertedValue(EstFuelQty, preflightLogistics)
    {
        var usGal = 0.264172; // 1 liter = 0.264172 US Gal
        var impGal = 1.20095; // 1 Imp Gal = 1.20095 US Gal

        if (preflightLogistics.FuelAmountType() == "1") // 1= Imperial Gallons
        {
            EstFuelQty = EstFuelQty * impGal;
        }
        else if (preflightLogistics.FuelAmountType() == "2") // 2 = Liters
        {
            EstFuelQty = EstFuelQty * usGal;
        }
        return EstFuelQty;
    }

function btnUpdateClick() {
        $("#hdnUpdateClick").val("true");
        reloadFuelGrid();
    }

function SaveLogisticsViewModel(isAsync) {

        if (isAsync == undefined)
            isAsync = true;

        self.Logistics.PreflightLeg.Passenger.LastUpdTS = ko.observable(Logistics.PreflightLeg.Passenger.LastUpdTS()).extend({ dateFormat: self.UserPrincipal._ApplicationDateFormat.toUpperCase() });
        var paraObject = ko.mapping.toJS(Logistics);
        paraObject = removeUnwantedPropertiesKOViewModel(paraObject);

        delete paraObject.PreflightLeg.PreflightTripOutbounds;


        $.ajax({
            async: isAsync,
            type: "POST",
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightLogisticsModel",
            data: JSON.stringify({ 'preflightLogistics': paraObject }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {                
                var returnResult = verifyReturnedResult(response);
                if (returnResult == true && response.d.Success == true) {
                    if (response.d.Result == "1") {
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                reportPreflightError(jqXHR, textStatus, errorThrown);
            }
        });
    }

function setFrozenColumnsBestPriceValue(rowid,bestPriceValue)
    {
        var frozenBestPrice = $(jqGridLogisticsEntityGrid + "_frozen tr[id=" + rowid + "]").find("td[aria-describedby=LogisticsFuelGrid_BestPrice]");
        frozenBestPrice.html(""); frozenBestPrice.attr("class", "fc_grntextbox");
        frozenBestPrice.html("$" + bestPriceValue);
        frozenBestPrice.attr("class", "fc_grntextbox");
    }
