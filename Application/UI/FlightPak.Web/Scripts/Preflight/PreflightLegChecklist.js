﻿var LegChecklist;
var selectedRowData = null;
var jqgridTableId = '#gridChecklistLegSummary';
var legNum = 1;
var jqgridChecklistTableId = '#dgTripChecklist';
$(function () {
    LegChecklist = function () {
        var checklist = null;
        checklist = new PreflightLegChecklistViewModel(1);
        return {
            checklist: this.checklist,
        };
    };
    ko.applyBindings(new LegChecklist(), document.getElementById('DivExternalForm'));
});



var self = this;

var PreflightLegChecklistViewModel = function (legNum) {
    self.LegNum = ko.observable().extend({ notify: 'always' });
    self.EditMode = ko.observable(false);
    self.CurrentLegChecklist = ko.observable();
    self.ChecklistGroupID = ko.observable().extend({ notify: 'always' });
    self.CheckGroupDescription = ko.observable();
  
    
    self.EditMode.subscribe(function (status) {
        if (status == false) {
            $("#btnChecklistGroup").addClass("browse-button-disabled").removeClass("browse-button");
            $("#btnChecklistGroup").attr("disabled", "disabled");       
        }
    });
  
    self.LegNum.subscribe(function (legNum) {
        
        $.ajax({
            async: false,
            url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetChecklist",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({ 'LegNum': legNum }),
            contentType: "application/json; charset=utf-8",
            success: function (result)
            {
                var returnResult = verifyReturnedResult(result);
                if (returnResult == true && result.d.Success == true) {
                    var jsonObj = result.d.Result;
                    if (jsonObj != null) {
                        if (self.CurrentLegChecklist.LegNum == undefined) {
                            if (jsonObj.CurrentLegChecklist != null) {
                                if (jsonObj.CurrentLegChecklist.length > 0) {
                                    jQuery(jqgridChecklistTableId).jqGrid('clearGridData');
                                    self.CurrentLegChecklist = jsonObj.CurrentLegChecklist;

                                    TripChecklist(self.CurrentLegChecklist);
                                    for (var row = 0; row <= self.CurrentLegChecklist.length; row++)
                                        jQuery(jqgridChecklistTableId).jqGrid('addRowData', row + 1, self.CurrentLegChecklist[row]);

                                    if (self.CurrentLegChecklist[self.LegNum() - 1] != undefined) {
                                        self.CheckGroupDescription = self.CurrentLegChecklist[self.LegNum() - 1].CheckGroupDescription;
                                        $('#tbChecklistGroup').val(self.CurrentLegChecklist[self.LegNum() - 1].CheckGroupDescription)
                                    }
                                }
                                else { SetBlankGridLayout(); }

                            }
                            else { SetBlankGridLayout(); }

                        }
                        $('.txtDisableEnable,.chkDisableEnable').attr("disabled", "disabled");

                    }
                    else {
                        SetBlankGridLayout();
                    }

                    if (!jsonObj.EditMode) {
                        $("#recordEdit").hide();
                        $("#btnSave").hide();
                        $("#btnCancel").hide();
                        $("#btnChecklistGroup").addClass("browse-button-disabled").removeClass("browse-button");
                    }
                    else {
                        $("#btnChecklistGroup").addClass("browse-button-disabled").removeClass("browse-button");
                    }
                }
            }
         
        });
     
    });

    self.ChecklistGroupID.subscribe(function (checkListGroupId) {
        
        var legNum = ko.mapping.toJS(self.LegNum);
        var checklistGroupDesc= ko.mapping.toJS(self.CheckGroupDescription);
        if (checkListGroupId!= undefined) {
            $.ajax({
                async: false,
                url: "/Views/Transactions/Preflight/PreflightTripManager.aspx/GetCheckListByChecklistGroupID",
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ 'ChecklistGroupID': checkListGroupId, 'CheckListGroupDesc': checklistGroupDesc, 'LegNum': legNum }),
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var returnResult = verifyReturnedResult(result);
                    var jsonObj = result.d.Result;
                    if (returnResult == true && result.d.Success == true && jsonObj != null && jsonObj.CurrentLegChecklist.length > 0) {
                        jQuery(jqgridChecklistTableId).jqGrid('GridUnload');
                        self.CurrentLegChecklist = jsonObj.CurrentLegChecklist;

                        self.CheckGroupDescription = checklistGroupDesc;
                        TripChecklist(self.CurrentLegChecklist);
                        for (var row = 0; row <= self.CurrentLegChecklist.length; row++) {
                            jQuery(jqgridChecklistTableId).jqGrid('addRowData', row + 1, self.CurrentLegChecklist[row]);
                        }

                    }
                    else {
                        self.CurrentLegChecklist = jsonObj.CurrentLegChecklist;
                        self.CheckGroupDescription = "";
                        SetBlankGridLayout();


                    }
                }
            });
        }

    });

   function SetBlankGridLayout()
    {
       jQuery(jqgridChecklistTableId).jqGrid('clearGridData');
       $('#tbChecklistGroup').val('');
       $(jqgridChecklistTableId + ' tbody').html("<div style='padding-left:5px;font: 11px/16px Arial,Helvetica,sans-serif !important;'>No records found</div>");
      
    }
   
   self.btnSave_Click = function () {
       var totalRecprds = $(jqgridChecklistTableId).jqGrid('getGridParam', 'reccount');
       if(totalRecprds<=0) {
           jAlert("No checklist items associated with this group. Select a Trip Checklist Group which has associated items.");
           
           return false;
       }
       UpdateChecklist();    
        var currentLegChecklist = ko.mapping.toJS(self.CurrentLegChecklist);
        var legNum = ko.mapping.toJS(self.LegNum);
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/SavePreflightChecklist',
                async: false,
                data: JSON.stringify({ 'preflightLegCheckList': currentLegChecklist, 'LegNum': legNum }),
                contentType: 'application/json',
                success: function (rowData, textStatus, xhr) {
                    var returnResult = verifyReturnedResult(rowData);
                    if (returnResult == true && rowData.d.Success == true) {
                        self.EditMode(false);
                        $('.txtDisableEnable,.chkDisableEnable').attr("disabled", "disabled");
                    }
                },
                error: function (xhr, status, error) {
                    alert("Error on Saving", "Preflight - Checklist");
                }
            });
        
            self.LegNum(legNum);
    };
    self.btnEdit_Click = function () {
        
            self.EditMode(true);
            $('.txtDisableEnable,.chkDisableEnable').removeAttr("disabled");
            $("#btnChecklistGroup").addClass("browse-button").removeClass("browse-button-disabled");
      
    };
    self.btnCancel_Click = function () {
        self.EditMode(false);
        $('.txtDisableEnable,.chkDisableEnable').attr("disabled", "disabled");
    };
    self.LegNum(legNum);
    ko.extenders.dateFormat = function (target, format) {

        //create a writable computed observable to intercept writes to our observable
        if (target() == null) {
            return target();
        }
        var result = ko.pureComputed({
            read: target,  //always return the original observables value
            write: function (newValue) {
                if (newValue != null) {
                    valueToWrite = moment(newValue).format(format);
                    target(valueToWrite);
                }
            }
        }).extend({ notify: 'always' });
        result(target());
        return result;
    };
    function FormatDateProperties(ApplicationDateFormat) {
        self.CurrentLegChecklist.LastUpdTS = ko.observable(self.CurrentLegChecklist.LastUpdTS()).extend({ dateFormat: ApplicationDateFormat });
    }
    $('body').on('change', '.txtComment', function () {
        var CheckListID=Number($(this).attr("row-Id"));
        for (var i = 0; i < self.CurrentLegChecklist.length; i++) {
            if(self.CurrentLegChecklist[i].CheckListID==CheckListID&&self.CurrentLegChecklist[i].PreflightCheckListID!=0) {
                self.CurrentLegChecklist[i].State=2;
            } else if(self.CurrentLegChecklist[i].PreflightCheckListID==0) {
                    self.CurrentLegChecklist[i].State=1;
                } else {
                    self.CurrentLegChecklist[i].State = 0;
                }
        }
    });
    $('body').on('change', '.chkcompleted', function () {
        var CheckListID = Number($(this).attr("row-Id"));
        for (var i = 0; i < self.CurrentLegChecklist.length; i++) {
            if (self.CurrentLegChecklist[i].CheckListID == CheckListID && self.CurrentLegChecklist[i].PreflightCheckListID != 0) {
                self.CurrentLegChecklist[i].State = 2;
            } else if (self.CurrentLegChecklist[i].PreflightCheckListID == 0) {
                self.CurrentLegChecklist[i].State = 1;
            }
        }
    });

    function UpdateChecklist() {
        var row = 0;
        $(jqgridChecklistTableId).find('.chkcompleted').each(function () {
            var rowid = $(this).attr("row-id");
            if (self.CurrentLegChecklist[row].CheckListID == rowid) {
                if ($(this).is(':checked')) {
                       self.CurrentLegChecklist[row].IsCompleted = true;
                }
                else {
                    self.CurrentLegChecklist[row].IsCompleted = false;
                }
                }
                row++;
            });
        
        row = 0;
        $(jqgridChecklistTableId).find('.txtComment').each(function () {
            var rowid = $(this).attr("row-id");
            var value = $(this).val();
            if (self.CurrentLegChecklist[row].CheckListID == rowid) {
                self.CurrentLegChecklist[row].ComponentDescription = value;
            }
            row++;
        });

    }
}


function BindTripChecklist(legNum) {
    self.LegNum(legNum);
}
function BindTripChecklistByCheckListGroupID(checklistGroupId, checklistGroupDesc)
{    
    self.CheckGroupDescription = checklistGroupDesc;
    self.ChecklistGroupID(checklistGroupId);
}
function TripChecklist(Data) {
    jQuery(jqgridChecklistTableId).jqGrid({
        data: JSON.stringify(Data),
        datatype: "json",
        cache: false,
        async: false,
        width: 660,
        colNames: ['CheckListID','Code', 'Description', 'Comment', 'Completed', 'Checklist Group', 'CheckGroupID'],
        colModel: [
            { name: 'CheckListID', index: 'CheckListID', key: true, hidden: true, sortable: false, search: false },
            { name: 'ComponentCD', dataField: 'ComponentCD', index: 'ComponentCD', width: 50, sortable: false, search: false },
            { name: 'CheckListDescription', dataField: 'CheckListDescription', index: 'CheckListDescription', width: 125, sortable: false, search: false },
            {
                name: 'ComponentDescription', index: 'ComponentDescription', width: 220, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {
                    if (cellvalue == null) {
                        return '<input id="txtComment" class="txtComment txtDisableEnable" type="text" row-id="' + rowObject["CheckListID"] + '" value="' + '' + '" style="width:100%"/>';
                    }
                    else { return '<input id="txtComment" class="txtComment txtDisableEnable" type="text" row-id="' + rowObject["CheckListID"] + '" value="' + cellvalue + '" style="width:100%"/>'; }
                }
            },
            {
                name: 'IsCompleted', index: 'IsCompleted', width: 75, sortable: false, search: false, formatter: function (cellvalue, options, rowObject) {

                    if (cellvalue == true) {
                        return '<input id="chkcompleted" class="chkcompleted chkDisableEnable" row-id="' + rowObject["CheckListID"] + '" type="checkbox" checked="checked" />';
                    } else { return '<input id="chkcompleted" class="chkcompleted chkDisableEnable" row-id="' + rowObject["CheckListID"] + '" type="checkbox"  />'; }
                }
            },
            { name: 'CheckGroupDescription', index: 'CheckGroupDescription', width: 150, sortable: false, search: false },
            { name: 'CheckGroupID', index: 'CheckGroupID', hidden: true, sortable: false, search: false }
           
        ],
        height:'auto',
        scrollOffset: 0,
        loadComplete: function () {         
            var objHeader = $("table[aria-labelledby=gbox_" + jqgridChecklistTableId + "] tr[role=rowheader] th");

            for (var i = 0; i < objHeader.length; i++) {
                var col = $("table[id=" + jqgridChecklistTableId + "] td[aria-describedby=" + objHeader[i].id + "]");
                var width = col.outerWidth();
                $(objHeader[i]).css("width", width);
            }
        }
    });
    $(jqgridChecklistTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });

    
}




$(document).ready(function () {
    jQuery(jqgridTableId).jqGrid({
        datatype: "json",
        url: '/Views/Transactions/Preflight/PreflightTripManager.aspx/TripLegsDetail',
        mtype: 'POST',
        cache: false,
        async: false,
        ajaxGridOptions: { contentType: 'application/json; charset=utf-8', cache: false },
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        width: 660,
        colNames: ['LegID', 'Legs', 'Depart Date/Time', 'Depart ICAO', 'Arrive ICAO', 'Arrival Date/Time'],
        colModel: [
            { name: 'LegID', index: 'LegID', hidden: true, sortable: true, search: false },
            { name: 'LegNUM', index: 'LegNUM',key:true, width: 50, sortable: true, search: false },
            { name: 'DepartureDTTMLocal', index: 'DepartureDTTMLocal', width: 165, sortable: true, search: false },
            { name: 'DepartICAOID', index: 'DepartICAOID', width: 110, sortable: true, search: false },
            { name: 'ArriveICAOID', index: 'ArriveICAOID', width: 110, sortable: true, search: false },
            { name: 'ArrivalDTTMLocal', index: 'ArrivalDTTMLocal', width: 165, sortable: true, search: false }
        ],
        scrollOffset: 0,
        ondblClickRow: function (rowId) {
            BindTripChecklist(rowId);
        },
        onSelectRow: function (id) {
            BindTripChecklist(id);
        },
        gridComplete: function () {
            var top_rowid = $('#gridChecklistLegSummary tr:nth-child(2)').attr('id');
            $(jqgridTableId).setSelection(top_rowid, true);
        }
    });

    $(jqgridTableId).jqGrid('setGridParam', { datatype: 'Local', loadonce: true });
});