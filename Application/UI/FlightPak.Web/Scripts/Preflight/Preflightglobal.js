﻿var Trip;
$(function () {
    Trip = function () {

        InitializeDateFormatExtender();
        InitializeDefaultSelectedTabFeature();
        InitializeTransportRateFormatterBindingHandler();
        InitializeCostFormatterBindingHandler();

        var currentTab = getQuerystring("seltab", "PreFlight");

        var main = new PreflightViewModel();

        var leg = null;
        if (currentTab == "Legs") {
            leg=new PreflightLegViewModel();
        }

        var crew = null;
        if (currentTab == "Crew") {
            crew = new CrewViewModel();
        }

        var pax = null;
        if (currentTab == "Pax") {
            pax = new PaxViewModel();
        }
        var logistics = null;
        if (currentTab == "Logistics") {
            logistics = new LogisticsViewModel();
        }
        return {
            main: this.main,
            leg:this.leg,
            pax: this.pax,
            logistics: this.logistics
        };
    };
    ko.applyBindings(new Trip());
});

function RequestStart() {
    $("#LoadingSpinner").height(($("[id$='DivExternalForm']").height() - 45) + "px");
    $("#LoadingSpinner").show();
}

function ResponseEnd() {
    $("#LoadingSpinner").hide();
}

