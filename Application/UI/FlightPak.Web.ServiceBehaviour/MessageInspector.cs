﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.Net;

namespace FlightPak.Web.ServiceBehaviour
{
    public class MessageInspector : IClientMessageInspector
    {

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply,
            object correlationState)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + ";" + "AfterReceiveReply");
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + ";" + request.Headers.Action);
            string sessionid = HttpContext.Current.Session.SessionID;

            //Key - SessionIdKey, Value (actual user sessionid) - sessionid
            MessageHeader header = MessageHeader.CreateHeader("SessionIdKey", "FlightPak.Web", sessionid);
            request.Headers.Add(header);
            return null;
        }
    }
}