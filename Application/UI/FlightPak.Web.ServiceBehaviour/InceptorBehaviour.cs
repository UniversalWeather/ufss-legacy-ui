﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;

namespace FlightPak.Web.ServiceBehaviour
{
    public class InterceptorBehaviour : BehaviorExtensionElement, IEndpointBehavior
    {
        public override System.Type BehaviorType
        {
            get { return typeof(InterceptorBehaviour); }
        }

        protected override object CreateBehavior()
        {
            return new InterceptorBehaviour();
        }

        public void AddBindingParameters(ServiceEndpoint endpoint,
               System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint,
               System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new MessageInspector());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint,
               System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {

        }

        public void Validate(ServiceEndpoint endpoint)
        {

        }
    }
}