﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerSetup.aspx.cs"
    Inherits="FlightPak.Web.Admin.CustomerSetup" MasterPageFile="~/Site.Master" EnableEventValidation="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
        <%--<Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js">
            </asp:ScriptReference>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js">
            </asp:ScriptReference>
        </Scripts>--%>
    </telerik:RadScriptManager>
    <div class="art-setting-customersupport">
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript" src="../../../Scripts/Common.js"></script>
            <script type="text/javascript">
                var currentTextBox = null;
                var currentDatePicker = null;

                //This method is called to handle the onclick and onfocus client side events for the texbox
                function showPopup(sender, e) {
                    //this is a reference to the texbox which raised the event
                    //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

                    currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

                    //this gets a reference to the datepicker, which will be shown, to facilitate
                    //the selection of a date
                    var datePicker = $find("<%= RadDatePicker1.ClientID %>");

                    //this variable is used to store a reference to the date picker, which is currently
                    //active
                    currentDatePicker = datePicker;

                    //this method first parses the date, that the user entered or selected, and then
                    //sets it as a selected date to the picker
                    datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

                    //the code lines below show the calendar, which is used to select a date. The showPopup
                    //function takes three arguments - the x and y coordinates where to show the calendar, as
                    //well as its height, derived from the offsetHeight property of the textbox
                    var position = datePicker.getElementPosition(currentTextBox);
                    datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
                }

                //this handler is used to set the text of the TextBox to the value of selected from the popup
                function dateSelected(sender, args) {
                    if (currentTextBox != null) {
                        //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
                        //value of the picker
                        currentTextBox.value = args.get_newValue();
                    }
                }

                //this function is used to parse the date entered or selected by the user
                function parseDate(sender, e) {
                    if (currentDatePicker != null) {
                        var date = currentDatePicker.get_dateInput().parseDate(sender.value);
                        var dateInput = currentDatePicker.get_dateInput();
                        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
                        sender.value = formattedDate;
                    }
                }

                function OnClientCloseAirportMasterPopup(oWnd, args) {
                    var combo = $find("<%= tbHomebase.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            alert(arg.AirportID);
                            document.getElementById("<%=tbHomebase.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdnHomebaseId.ClientID%>").value = arg.AirportId;

                        }
                        else {
                            document.getElementById("<%=tbHomebase.ClientID%>").value = "";
                            document.getElementById("<%=hdnHomebaseId.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }


                function openwindows() {
                    var url = "AirportMasterPopup.aspx?IcaoID=" + document.getElementById("<%=tbHomebase.ClientID%>").value;
                    var oWnd = radopen(url, 'radAirportCatalog');
                }
                function openWin(url, value, radWin) {

                    var oWnd = radopen(url + value, radWin);
                }
                function ConfirmClose(WinName) {
                    var oManager = GetRadWindowManager();
                    var oWnd = oManager.GetWindowByName(WinName);
                    //Find the Close button on the page and attach to the 
                    //onclick event
                    var CloseButton = document.getElementById("CloseButton" + oWnd.Id);
                    CloseButton.onclick = function () {
                        CurrentWinName = oWnd.Id;
                        //radconfirm is non-blocking, so you will need to provide a callback function
                        radconfirm("Are you sure you want to close the window?", confirmCallBackFn);
                    }
                }

                function OnClientCloseAirportMasterPopup(oWnd, args) {
                    var combo = $find("<%= tbHomebase.ClientID %>");
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg !== null) {
                        if (arg) {
                            document.getElementById("<%=tbHomebase.ClientID%>").value = arg.ICAO;
                            document.getElementById("<%=hdnHomebaseId.ClientID%>").value = arg.AirportId;

                        }
                        else {
                            document.getElementById("<%=tbHomebase.ClientID%>").value = "";
                            document.getElementById("<%=hdnHomebaseId.ClientID%>").value = "";
                            combo.clearSelection();
                        }
                    }
                }

                function GetDimensions(sender, args) {
                    var bounds = sender.getWindowBounds();
                    return;
                }
                function GetRadWindow() {
                    var oWindow = null;
                    if (window.radWindow) oWindow = window.radWindow;
                    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                    return oWindow;
                }
                function tbDate_OnKeyDown(sender, event) {
                    if (event.keyCode == 9) {
                        var datePicker = $find("<%= RadDatePicker1.ClientID %>");
                        datePicker.hidePopup();
                        return true;
                    }
                }
       
           
            </script>
        </telerik:RadCodeBlock>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            <Windows>
                <telerik:RadWindow ID="radAirportCatalog" runat="server" OnClientResize="GetDimensions"
                    OnClientClose="OnClientCloseAirportMasterPopup" AutoSize="true" KeepInScreenBounds="true"
                    ReloadOnShow="true" Modal="true" Behaviors="close" VisibleStatusbar="false" NavigateUrl="AirportMasterPopup.aspx"
                    Height="500px" Width="750px">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
        <telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" runat="server">
            <ClientEvents OnDateSelected="dateSelected" />
            <DateInput ID="DateInput1" DateFormat="MM/dd/yyyy" runat="server">
            </DateInput>
        </telerik:RadDatePicker>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCustomerSetup" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgCustomerSetup">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomerSetup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="chkAll">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCustomerSetup" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbEmail">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbHomebase">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <div id="globalNav_admn">
            <span class="head-title-nav">Customer Setup</span>
        </div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="tblspace_10">
                    </div>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
                    <telerik:RadGrid ID="dgCustomerSetup" runat="server" AllowSorting="true" OnItemCreated="dgCustomerSetup_ItemCreated"
                        Visible="true" OnNeedDataSource="dgCustomerSetup_BindData" OnItemCommand="dgCustomerSetup_ItemCommand"
                        OnUpdateCommand="dgCustomerSetup_UpdateCommand" OnInsertCommand="dgCustomerSetup_InsertCommand"
                        OnDeleteCommand="dgCustomerSetup_DeleteCommand" AutoGenerateColumns="false" Height="348px"
                        PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCustomerSetup_SelectedIndexChanged"
                        OnItemDataBound="dgCustomerSetup_ItemDataBound" AllowFilteringByColumn="true"
                        PagerStyle-AlwaysVisible="true" EnableViewState="true">
                        <MasterTableView DataKeyNames="CustomerID,CustomerName,PrimaryFirstName,PrimaryMiddleName,PrimaryLastName,PrimaryEmailID,SecondaryFirstName,SecondaryMiddleName,SecondaryLastName,SecondaryEmailID,IsAll,IsAPIS,IsATS,IsMapping,IsUVFuel,IsUVTriplink,UVTripLinkName,UVTripLinkAccountNumber,ApisID,ApisPassword ,LicenseNumber,LicenseType,LicenseStartDate,UserCount,AircraftCount,MaintainenceExpiryDate,IsUWASupportAccess,IsActiveLicense,LastUpdUID,LastUpdTS,UserName,CustomerLicenseID,IcaoID,HomebaseAirportID"
                            CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="CustomerID" HeaderText="Customer ID" AutoPostBackOnFilter="true"
                                    ShowFilterIcon="false" HeaderStyle-Width="230px" AllowFiltering="true" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CustomerName" HeaderText="Customer Name" ShowFilterIcon="false"
                                    HeaderStyle-Width="400px" AllowFiltering="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LicenseType" HeaderText="License Type" ShowFilterIcon="false"
                                    HeaderStyle-Width="292px" AllowFiltering="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; float: left; clear: both;">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                        ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                </div>
                                <div style="padding: 5px 5px; float: right;">
                                    <asp:Label ID="lbLastUpdatedUser" runat="server"></asp:Label>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table class="border-box" width="100%">
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel160">
                                        <asp:CheckBox Text="UWA Support Access" ID="chkUWASupportAccess" runat="server" />
                                    </td>
                                    <td>
                                        <asp:CheckBox Text="Active License" ID="chkActiveLicense" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbCustomerID" runat="server" Text="Customer ID"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:TextBox ID="tbCustomerID" CssClass="tdLabel200" runat="server" ReadOnly="true"
                                            ValidationGroup="save"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel150">
                                        <span class="mnd_text">
                                            <asp:Label ID="lbCustomerName" runat="server" Text="Customer Name"></asp:Label></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbCustomerName" runat="server" CssClass="tdLabel200" ValidationGroup="save"
                                            onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbCustomerID"
                                            Display="Dynamic" runat="server" Text="CustomerID is Required" CssClass="alert-text"
                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                    </td>
                                    <td class="tdLabel150">
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="tbCustomerName"
                                            Display="Dynamic" runat="server" Text="Customer Name is Required" CssClass="alert-text"
                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbLicenseType" runat="server" Text="License Type"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:DropDownList runat="server" CssClass="text60" ID="CmbLicense">
                                            <asp:ListItem Text="PR" Value="PR"></asp:ListItem>
                                            <asp:ListItem Text="EV" Value="EV"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="tdLabel150">
                                        <span class="mnd_text">
                                            <asp:Label ID="lbLicenseNumber" runat="server" Text="License No."></asp:Label></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbLicenseNumber" runat="server" ValidationGroup="save"></asp:TextBox>
                                        <asp:HiddenField ID="HdLicenseID" runat="server"></asp:HiddenField>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                    </td>
                                    <td class="tdLabel250">
                                    </td>
                                    <td class="tdLabel150">
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tbLicenseNumber"
                                            Display="Dynamic" ErrorMessage="License No is required" ValidationGroup="save"
                                            CssClass="alert-text"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbLicenseStartDate" runat="server" Text="License Start Date"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:TextBox ID="tbLicenseStartDate" runat="server" ValidationGroup="save" CssClass="text80"
                                            onKeyPress="return fnNotAllowKeys(this, event,'/')" onclick="showPopup(this, event);"
                                            onfocus="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel150">
                                        <asp:Label ID="lbMaintainenceExpiryDate" runat="server" Text="Support Renewal Date"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbMaintainenceExpiryDate" runat="server" ValidationGroup="save"
                                            CssClass="text80" onKeyPress="return fnNotAllowKeys(this, event,'/')" onclick="showPopup(this, event);"
                                            onfocus="showPopup(this, event);" MaxLength="10" onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbUserCount" runat="server" Text="User Count"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:TextBox ID="tbUserCount" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel150">
                                        <span class="mnd_text">
                                            <asp:Label ID="lbHomeBase" runat="server" Text="Home Base"></asp:Label></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="tbHomebase" CssClass="tdLabel200" runat="server" MaxLength="5" ValidationGroup="save"
                                            OnTextChanged="tbHomeBase_TextChanged" AutoPostBack="true"></asp:TextBox>
                                        <button id="Button1" runat="server" onclick="javascript:openwindows();return false;"
                                            class="browse-button" causesvalidation="false">
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:RegularExpressionValidator ID="revUserCount" runat="server" ErrorMessage="Enter only numeric"
                                            Display="Dynamic" ControlToValidate="tbUserCount" CssClass="alert-text" ValidationExpression="^\d+$"
                                            ValidationGroup="save"></asp:RegularExpressionValidator>
                                    </td>
                                    <td class="tdLabel150">
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfqhome" runat="server" ControlToValidate="tbHomebase"
                                            Display="Dynamic" ErrorMessage="Home Base is Required" ValidationGroup="save"
                                            CssClass="alert-text"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="rfvHomebaseInvalid" runat="server" ControlToValidate="tbHomebase"
                                            ErrorMessage="Home Base does not exist" Display="Dynamic" CssClass="alert-text"
                                            ValidationGroup="save" SetFocusOnError="true" OnServerValidate="rfvHomebaseInvalid_ServerValidate"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                        <asp:Label ID="lbAircraftcount" runat="server" Text="Aircraft Count"></asp:Label>
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:TextBox ID="tbAircraftCount" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                    </td>
                                    <td class="tdLabel150">
                                        <span class="mnd_text">
                                            <asp:Label ID="Label1" runat="server" Text="Initial User Name"></asp:Label></span>
                                    </td>
                                    <td>
                                        <span class="mnd_text">
                                            <asp:TextBox ID="tbIntUserName" runat="server" CssClass="tdLabel200" ValidationGroup="save"></asp:TextBox></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel140">
                                    </td>
                                    <td class="tdLabel250">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter only numeric"
                                            Display="Dynamic" ControlToValidate="tbAircraftCount" CssClass="alert-text" ValidationExpression="^\d+$"
                                            ValidationGroup="save"></asp:RegularExpressionValidator>
                                    </td>
                                    <td class="tdLabel150">
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvUserName" ValidationGroup="save" runat="server"
                                            Display="Dynamic" ControlToValidate="tbIntUserName" ErrorMessage="RequiredFieldValidator"
                                            CssClass="alert-text">Initial User Name is Required</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <div class="tblspace_10">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="tdLabel390">
                                        <fieldset>
                                            <legend>Primary Contact</legend>
                                            <asp:Panel runat="server" ID="pnlPrimanyContact" text="Primary Contact">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel120">
                                                                        <span class="mnd_text">
                                                                            <asp:Label ID="lbFirstName" runat="server" Text="First Name"></asp:Label></span>
                                                                    </td>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbFirstName" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbFirstName"
                                                                                        CssClass="alert-text" runat="server" ID="RegularExpressionValidator5" ErrorMessage="Enter valid First Name"
                                                                                        ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>
                                                                                    <asp:RequiredFieldValidator ID="rfqFirstname" runat="server" ControlToValidate="tbFirstName"
                                                                                        Display="Dynamic" ValidationGroup="save" ErrorMessage="RequiredFieldValidator"
                                                                                        CssClass="alert-text">First Name is Required</asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel120">
                                                                        <asp:Label ID="lbMiddleName" runat="server" Text="Middle Name"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbMiddleName" CssClass="tdLabel200" runat="server" ValidationGroup="save"
                                                                                        onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <%--<asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbMiddleName"
                                                                                        CssClass="alert-text" runat="server" ID="RegularExpressionValidator6" ErrorMessage="Enter valid Middle Name"
                                                                                        ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>--%>
                                                                                    <%--<asp:RequiredFieldValidator ID="rfqMiddlleName" runat="server" ControlToValidate="tbMiddleName"
                                                                                        Display="Dynamic" ValidationGroup="save" ErrorMessage="RequiredFieldValidator"
                                                                                        CssClass="alert-text">Middle Name is Required</asp:RequiredFieldValidator>--%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel120">
                                                                        <span class="mnd_text">
                                                                            <asp:Label ID="lbLastName" runat="server" Text="Last Name"></asp:Label></span>
                                                                    </td>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbLastName" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbLastName"
                                                                                        CssClass="alert-text" runat="server" ID="RegularExpressionValidator7" ErrorMessage="Enter valid Last Name"
                                                                                        ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>
                                                                                    <asp:RequiredFieldValidator ID="rfqLastName" ValidationGroup="save" runat="server"
                                                                                        Display="Dynamic" ControlToValidate="tbLastName" ErrorMessage="RequiredFieldValidator"
                                                                                        CssClass="alert-text">Last Name is Required</asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="tdLabel120">
                                                                        <span class="mnd_text">
                                                                            <asp:Label ID="lbEmail" runat="server" Text="Primary E-mail"></asp:Label></span>
                                                                    </td>
                                                                    <td>
                                                                        <table cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="tbEmail" CssClass="tdLabel200" runat="server" ValidationGroup="save"
                                                                                        AutoPostBack="true" OnTextChanged="tbEmail_TextChanged" CausesValidation="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbEmail"
                                                                                        CssClass="alert-text" runat="server" ID="RegularExpressionValidator8" ErrorMessage="Enter valid E-mail"
                                                                                        ValidationExpression="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" EnableClientScript="true"></asp:RegularExpressionValidator>
                                                                                    <asp:CustomValidator ID="tbEmailInvalid" runat="server" ControlToValidate="tbEmail"
                                                                                        ErrorMessage="Unique E-mail id required" Display="Dynamic" CssClass="alert-text"
                                                                                        ValidationGroup="save" SetFocusOnError="true" OnServerValidate="tbEmail_ServerValidate"
                                                                                        EnableClientScript="true"></asp:CustomValidator>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="save" runat="server"
                                                                                        Display="Dynamic" ControlToValidate="tbEmail" ErrorMessage="RequiredFieldValidator"
                                                                                        CssClass="alert-text" EnableClientScript="true">E-mail is Required</asp:RequiredFieldValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </fieldset>
                                    </td>
                                    <td valign="top">
                                        <fieldset>
                                            <legend>Secondary Contact</legend>
                                            <asp:Panel runat="server" ID="pnlSecondaryContact" text="Secondary Contact">
                                                <table>
                                                    <tr>
                                                        <td class="tdLabel140">
                                                            <asp:Label ID="lbFirstName1" runat="server" Text="First Name"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbFirstName1" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbFirstName1"
                                                                CssClass="alert-text" runat="server" ID="reFirstName" ErrorMessage="Enter valid First Name"
                                                                ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            <asp:Label ID="lbMiddleName1" runat="server" Text="Middle Name"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbMiddleName1" CssClass="tdLabel200" runat="server" ValidationGroup="save"
                                                                onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                                            <%--<asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbMiddleName1"
                                                                CssClass="alert-text" runat="server" ID="RegularExpressionValidator2" ErrorMessage="Enter valid Middle Name"
                                                                ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            <asp:Label ID="lbLastName1" runat="server" Text="Last Name"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbLastName1" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbLastName1"
                                                                CssClass="alert-text" runat="server" ID="RegularExpressionValidator3" ErrorMessage="Enter valid Last Name"
                                                                ValidationExpression="([a-zA-Z]{3,30}\s*)+"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tdLabel100">
                                                            <asp:Label ID="lbEmail1" runat="server" Text="Secondary E-mail"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="tbEmail1" CssClass="tdLabel200" runat="server" ValidationGroup="save"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ValidationGroup="save" Display="Dynamic" ControlToValidate="tbEmail1"
                                                                CssClass="alert-text" runat="server" ID="RegularExpressionValidator4" ErrorMessage="Enter valid E-mail"
                                                                ValidationExpression="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="tdLabel390">
                                        <fieldset>
                                            <legend>Modules</legend>
                                            <div style="width: 100%; height: 142px; overflow: auto;">
                                                <asp:GridView runat="server" ID="dgModule" Width="100%" AutoGenerateColumns="false"
                                                    DataKeyNames="ModuleID" Height="150px">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="HdCust" runat="server" Value='<%# Convert.ToString(Eval("CustomerID")) %>' />
                                                                <asp:HiddenField ID="HdAccessId" runat="server" Value='<%# Convert.ToString(Eval("CustomerModuleAccessID")) %>' />
                                                                <asp:CheckBox ID="chkModule" Checked='<%# Convert.ToBoolean(Eval("CanAccess")) %>'
                                                                    runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Modules">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbModules" runat="server" Text='<%# Eval("ModuleName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Evalution Expiration Date">
                                                            <ItemTemplate>
                                                                <asp:TextBox runat="server" ID="tbEvalutionExpiryDate" Text='<%# Eval("ExpiryDate") == null ? "" : ModuleDate(Eval("ExpiryDate").ToString()) %>'
                                                                    ValidationGroup="save" CssClass="text80" onKeyPress="return fnNotAllowKeys(this, event,'/')"
                                                                    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" MaxLength="10"
                                                                    onkeydown="return tbDate_OnKeyDown(this, event);"></asp:TextBox>
                                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ErrorMessage="Enter Valid Date"
                                                        Display="Dynamic" ControlToValidate="tbEvalutionExpiryDate" CssClass="alert-text" ValidationExpression="^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$"
                                                        ValidationGroup="save"></asp:RegularExpressionValidator>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </fieldset>
                                    </td>
                                    <td valign="top" class="tdLabel150">
                                        <fieldset>
                                            <legend>Web Services</legend>
                                            <asp:Panel runat="server" ID="pnlWebService">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox runat="server" Text="All" ID="chkAll" AutoPostBack="true" OnCheckedChanged="chkAll_OnCheckedChanged" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox runat="server" Text="APIS" ID="chkAPIS" AutoPostBack="true" OnCheckedChanged="chkOthers_OnCheckedChanged" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox runat="server" Text="ATS/TS3" ID="chkATSTS3" AutoPostBack="true" OnCheckedChanged="chkOthers_OnCheckedChanged"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox runat="server" Text="Mapping" ID="chkMapping" AutoPostBack="true" OnCheckedChanged="chkOthers_OnCheckedChanged"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox runat="server" Text="UWA Fuel" ID="chkUVFuel" AutoPostBack="true" OnCheckedChanged="chkOthers_OnCheckedChanged"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox runat="server" Text="UVtriplink" ID="chkUvtriplink" AutoPostBack="true" OnCheckedChanged="chkOthers_OnCheckedChanged"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </fieldset>
                                    </td>
                                    <td>
                                    </td>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td class="tdLabel150">
                                                    <asp:Label ID="lbUvtriplink" runat="server" Text="UVtriplink Name"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbUvtriplinkName" CssClass="tdLabel40" runat="server" ValidationGroup="save"
                                                        onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel150">
                                                    <asp:Label ID="lbApisID" runat="server" Text="APIS ID"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbApisID" CssClass="tdLabel40" runat="server" ValidationGroup="save"
                                                        onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel150">
                                                    <asp:Label ID="lbAccountNumber" runat="server" Text="UVtriplink Account No."></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbAccountNumber" CssClass="tdLabel40" runat="server" ValidationGroup="save"
                                                        onKeyPress="return fnAllowAlpha(this, event,'.')"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tdLabel150">
                                                    <asp:Label ID="lbApisPassword" runat="server" Text="APIS Password"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="tbApisPassword" CssClass="tdLabel40" runat="server" ></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdLabel390">
                                          <fieldset style="margin-top: 5px">
                                            <legend>FuelVendor</legend>
                                              <div style="width: 100%; height: 142px; overflow: auto;">
                                                <asp:GridView runat="server" ID="dgFuelVendor"  Width="100%" AutoGenerateColumns="false"
                                                    DataKeyNames="VendorCD,VendorName" Height="150px">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="HdFuelVendorID" runat="server" Value='<%# Convert.ToString(Eval("FuelVendorID")) %>' />
                                                                <asp:HiddenField ID="HdCustomerFuelVendorAccessID" runat="server" Value='<%# Convert.ToString(Eval("CustomerFuelVendorAccessID")) %>' />
                                                                <asp:CheckBox ID="chkVendorAccess" runat="server" Checked='<%# Eval("CanAccess")!=null && Convert.ToBoolean(Eval("CanAccess")) %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vendor CD">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbVendorCD" runat="server" Text='<%# Eval("VendorCD") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Vendor Name">
                                                            <ItemTemplate>
                                                               <asp:Label ID="lbVendorName" runat="server" Text='<%# Eval("VendorName") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                          </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveChanges" Text="Save" runat="server" CssClass="button" ValidationGroup="save"
                                OnClick="btnSaveChanges_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="button" OnClick="btnCancel_Click"
                                CausesValidation="false" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnHomebaseId" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
