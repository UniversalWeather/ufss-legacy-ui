﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HelpManager.aspx.cs" Inherits="FlightPak.Web.Admin.Help.HelpManager"
    MasterPageFile="Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <div class="art-setting-customersupport">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableCdn="true" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
                        function ShowSuccessMessage() {
                            $(document).ready(function () {
                                $("#tdSuccessMessage").css("display", "inline");
                                $('#tdSuccessMessage').delay(60000).fadeOut(60000);
                            });
                        }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
        </Windows>
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(true);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(false);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    <div id="DivExternalForm" runat="server" class="dgpExternalForm">
        <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
            <table width="100%" cellpadding="0" cellspacing="0">                
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td class="tdLabel100">
                                    Category
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCategory" runat="server" OnSelectedIndexChanged="ddlCategory_OnSelectedIndexChanged"
                                        AutoPostBack="true">
                                        <%--<asp:ListItem Text="Company" Value="Company" />
                                        <asp:ListItem Text="Fleet" Value="Fleet" />
                                        <asp:ListItem Text="People" Value="People" />
                                        <asp:ListItem Text="Logistics" Value="Logistics" />
                                        <asp:ListItem Text="Administration" Value="Administration" />
                                        <asp:ListItem Text="Preflight" Value="Preflight" />
                                        <asp:ListItem Text="Scheduling Calendar" Value="Scheduling Calendar" />
                                        <asp:ListItem Text="Postflight" Value="Postflight" />
                                        <asp:ListItem Text="Corporate Request" Value="Corporate Request" />
                                        <asp:ListItem Text="Glossary" Value="Glossary" />--%>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Topic
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTopic" runat="server" OnSelectedIndexChanged="ddlTopic_OnSelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <telerik:RadEditor ID="RadEditor1" runat="server" ToolsFile="DBFileBrowserContentProvider.xml">
                                        <Content>
                                        </Content>
                                    </telerik:RadEditor>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>  
                 <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td align="left" class="tdLabel160">
                                    <div id="tdSuccessMessage" class="success_msg">
                                        Record saved successfully.</div>
                                </td>
                                <%--<td align="right">
                                    <div class="mandatory">
                                        <span>Bold</span> Indicates required field</div>
                                </td>--%>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>               
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" class="tblButtonArea-nw">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="save"
                                        OnClick="btnSave_Click" />
                                    <asp:HiddenField ID="hdnSave" runat="server" />
                                    <asp:HiddenField ID="hdnHelpID" runat="server" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" CausesValidation="false"
                                        OnClick="btnCancel_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </div>
</asp:Content>
