﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Text;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Admin;
using FlightPak.Web.Admin.AdminService;
namespace FlightPak.Web.Admin.Help
{
    public partial class HelpManager : BasePage
    {
        #region "VARIABLE DECLARATIONS"
        private ExceptionManager exManager;
        private string ModuleNameConstant = ModuleNameConstants.Utilities.Bookmark;
        //private string ViewPermission = Permission.Utilities.ViewBookmark;
        //private string AddPermission = Permission.Utilities.AddBookmark;
        //private string EditPermission = Permission.Utilities.EditBookmark;
        //private string DeletePermission = Permission.Utilities.DeleteBookmark;
        //private string EntitySetName = EntitySet.Utilities.Bookmark;
        #endregion "VARIABLE DECLARATIONS"
        #region "PAGE EVENTS"
        protected void Page_Load(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (!IsPostBack)
                        {
                            RadEditor1.ImageManager.ContentProviderTypeName = typeof(DBContentProvider).AssemblyQualifiedName;

                            RadEditor1.ImageManager.ViewPaths = new string[] { "ROOT", "ROOT" };
                            RadEditor1.ImageManager.UploadPaths = new string[] { "ROOT", "ROOT" };
                            RadEditor1.ImageManager.DeletePaths = new string[] { "ROOT", "ROOT" };
                            BindCatalog();
                            BindTopic();
                            BindContent();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "PAGE EVENTS"
        #region "AJAX EVENTS"
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (e.Initiator.ID.IndexOf("btnSave", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = DivExternalForm;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "AJAX EVENTS"
        #region "USER DEFINED METHODS"
        private void BindCatalog()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<GetHelpContent> GetHelpContentList = new List<GetHelpContent>();
                HelpContent oGetHelpContent = new HelpContent();                
                oGetHelpContent.IsDeleted = false;
                using (FlightPak.Web.Admin.AdminService.AdminServiceClient AdminService = new FlightPak.Web.Admin.AdminService.AdminServiceClient())
                {
                    var HelpContentInfo = AdminService.GetHelpContent(oGetHelpContent);
                    if (HelpContentInfo.ReturnFlag == true)
                    {
                        GetHelpContentList = HelpContentInfo.EntityList;
                    }
                    ddlCategory.DataSource = GetHelpContentList.GroupBy(x => x.Category).Select(grp => grp.First());
                    ddlCategory.DataTextField = "Category";
                    ddlCategory.DataValueField = "Category";
                    ddlCategory.DataBind();
                }
            }
        }
        private void BindTopic()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<GetHelpContent> GetHelpContentList = new List<GetHelpContent>();
                HelpContent oGetHelpContent = new HelpContent();
                oGetHelpContent.Category = ddlCategory.SelectedValue.ToString();                
                oGetHelpContent.IsDeleted = false;
                using (FlightPak.Web.Admin.AdminService.AdminServiceClient AdminService = new FlightPak.Web.Admin.AdminService.AdminServiceClient())
                {
                    var HelpContentInfo = AdminService.GetHelpContent(oGetHelpContent);
                    if (HelpContentInfo.ReturnFlag == true)
                    {
                        GetHelpContentList = HelpContentInfo.EntityList;
                    }
                    ddlTopic.DataSource = GetHelpContentList;
                    ddlTopic.DataTextField = "Topic";
                    ddlTopic.DataValueField = "Topic";
                    ddlTopic.DataBind();
                }
            }
        }
        private void BindContent()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<GetHelpContent> GetHelpContentList = new List<GetHelpContent>();
                HelpContent oGetHelpContent = new HelpContent();
                oGetHelpContent.Category = ddlCategory.SelectedValue.ToString();
                oGetHelpContent.Topic = ddlTopic.SelectedValue.ToString();
                oGetHelpContent.IsDeleted = false;
                using (FlightPak.Web.Admin.AdminService.AdminServiceClient AdminService = new FlightPak.Web.Admin.AdminService.AdminServiceClient())
                {
                    var HelpContentInfo = AdminService.GetHelpContent(oGetHelpContent);
                    if (HelpContentInfo.ReturnFlag == true)
                    {
                        GetHelpContentList = HelpContentInfo.EntityList;
                    }
                    if (GetHelpContentList.Count > 0)
                    {
                        hdnHelpID.Value = GetHelpContentList[0].Identifier;
                        RadEditor1.Content = GetHelpContentList[0].Content;
                    }
                }
            }
        }
        private void Save()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                HelpContent oHelpContent = new HelpContent();
                if (!string.IsNullOrEmpty(hdnHelpID.Value))
                {
                    oHelpContent.Identifier = hdnHelpID.Value;
                }
                oHelpContent.Category = ddlCategory.SelectedValue;
                oHelpContent.Topic = ddlTopic.SelectedValue;
                oHelpContent.Content = RadEditor1.Content;
                oHelpContent.IsInActive = false;
                oHelpContent.IsDeleted = false;
                using (FlightPak.Web.Admin.AdminService.AdminServiceClient AdminService = new FlightPak.Web.Admin.AdminService.AdminServiceClient())
                {
                    var result = AdminService.UpdateHelpContent(oHelpContent);
                    if (result.ReturnFlag)
                    {
                        ShowSuccessMessage();
                    }
                }
            }
        }
        private void SaveImage()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                
            }
        }
        #endregion "USER DEFINED METHODS"
        #region "CONTROL EVENTS"
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Save();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindContent();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void ddlCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindTopic();
                        BindContent();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        protected void ddlTopic_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        BindContent();
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstant);
                }
            }
        }
        #endregion "CONTROL EVENTS"

    }
}