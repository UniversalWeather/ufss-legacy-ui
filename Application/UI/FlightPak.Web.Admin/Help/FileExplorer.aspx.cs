﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Text;
using System.Collections;
//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Web.Admin;
using FlightPak.Web.Admin.AdminService;
namespace FlightPak.Web.Views.Help
{
    public partial class FileExplorer : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            RadFileExplorer1.Configuration.ContentProviderTypeName = typeof(DBContentProvider).AssemblyQualifiedName;

            RadFileExplorer1.Configuration.ViewPaths = new string[] { "ROOT", "ROOT" };
            RadFileExplorer1.Configuration.UploadPaths = new string[] { "ROOT", "ROOT" };
            RadFileExplorer1.Configuration.DeletePaths = new string[] { "ROOT", "ROOT" };

            //RadFileExplorer1.Configuration.ViewPaths = new string[] { "ROOT/Images/Nature/Fruits", "ROOT/Images/Nature/Animals" };
            //RadFileExplorer1.Configuration.UploadPaths = new string[] { "ROOT/Images/Nature/Fruits", "ROOT/Images/Nature/Animals" };
            //RadFileExplorer1.Configuration.DeletePaths = new string[] { "ROOT/Images/Nature/Fruits", "ROOT/Images/Nature/Animals" };
        }

    }
}