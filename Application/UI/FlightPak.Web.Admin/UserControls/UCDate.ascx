﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UCDate.ascx.cs" Inherits="FlightPak.Web.UserControls.UCDate" %>
<script type="text/javascript">
    var currentTextBox = null;
    var currentDatePicker = null;

    //This method is called to handle the onclick and onfocus client side events for the texbox
    function showPopup(sender, e) {
        //this is a reference to the texbox which raised the event
        //see the methods exposed through the $telerik static client library here - http://www.telerik.com/help/aspnet-ajax/telerik-static-client-library.html

        currentTextBox = sender.tagName == "INPUT" ? sender : $telerik.getPreviousHtmlNode(sender);

        //this gets a reference to the datepicker, which will be shown, to facilitate
        //the selection of a date
        var datePicker = $find("<%= RadDatePicker1.ClientID %>");

        //this variable is used to store a reference to the date picker, which is currently
        //active
        currentDatePicker = datePicker;

        //this method first parses the date, that the user entered or selected, and then
        //sets it as a selected date to the picker
        datePicker.set_selectedDate(currentDatePicker.get_dateInput().parseDate(currentTextBox.value));

        //the code lines below show the calendar, which is used to select a date. The showPopup
        //function takes three arguments - the x and y coordinates where to show the calendar, as
        //well as its height, derived from the offsetHeight property of the textbox
        var position = datePicker.getElementPosition(currentTextBox);
        datePicker.showPopup(position.x, position.y + currentTextBox.offsetHeight);
    }

    //this handler is used to set the text of the TextBox to the value of selected from the popup
    function dateSelected(sender, args) {
        if (currentTextBox != null) {
            //currentTextBox is the currently selected TextBox. Its value is set to the newly selected
            //value of the picker
            currentTextBox.value = args.get_newValue();
        }
    }
    function ValidateDate(control) {
        var MinDate = new Date("01/01/1900");
        var MaxDate = new Date("12/31/2100");
        var SelectedDate;
        if (control.value != "") {
            SelectedDate = new Date(control.value);
            if ((SelectedDate < MinDate) || (SelectedDate > MaxDate)) {
                radalert("Please enter / select Date between 01/01/1900 and 12/31/2100", 400, 100, "Date");
                control.value = "";
            }
        }
        return false;
    }
    function tbDate_OnKeyDown(sender, event) {
        if (event.keyCode == 9) {
            var datePicker = $find("<%= RadDatePicker1.ClientID %>");
            datePicker.hidePopup();
            return true;
        }
    }
</script>
<telerik:RadDatePicker ID="RadDatePicker1" Style="display: none;" MinDate="01/01/1900"
    MaxDate="12/31/2100" runat="server">
    <ClientEvents OnDateSelected="dateSelected" />
    <DateInput ID="DateInput1" runat="server">
    </DateInput>
    <Calendar ID="Calendar1" runat="server" ShowRowHeaders="false">
        <SpecialDays>
            <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BorderColor="#ea9b0a" ItemStyle-BackColor="#ea9b0a" ItemStyle-BorderStyle="Solid">
            </telerik:RadCalendarDay>
        </SpecialDays>
    </Calendar>
</telerik:RadDatePicker>
<asp:TextBox ID="tbDate" runat="server" CssClass="text80" onKeyPress="return fnAllowNumericAndChar(this, event,'/')" onkeydown="return tbDate_OnKeyDown(this, event);"
    onclick="showPopup(this, event);" onfocus="showPopup(this, event);" onblur="parseDate(this, event); ValidateDate(this);"
    ClientIDMode="Static" MaxLength="10"></asp:TextBox>
