﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web.UI;

namespace FlightPak.Web.Admin
{
    public class BasePage : System.Web.UI.Page
    {
        /// <summary>
        /// All the ASP.net Pages in FlightPak should inherit this class and use WCFCall to make all WCF calls.
        /// There are two purpose for this method.
        /// 1. To pass SessionId as WCF Custom Message Header
        /// 2. To retry in case of WCF Call failure. FlightPak does lots of WCF calls to talk Application layer.
        /// We cannot afford for WCF call failure, when fails we should retry before logging to the log file and 
        /// give up the call.
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public Object WCFCall(Func<Object> func)  // WCFCall with retry and delay
        {
            //todo - // please write logic to read from config file.
            int maxRetries = 3; // please write logic to read from config file.
            int delayInMilliseconds = 2; // please write logic to read from config file.

            Object returnValue = default(Object);
            int numTries = 0;
            bool succeeded = false;
            while (numTries < maxRetries)
            {
                //todo - We should catch some specific exceptions; such as SQL Exceptions
                //When SQL Exception accours then there is no point in RETRY, should log and return NULL
                try
                {
                    returnValue = func();
                    succeeded = true;

                }
                catch (Exception ex)
                {
                    //Manually Handled
                    //todo
                    //Write code to Log to the error log file / tracing

                    //((ReturnValue<Object>)returnValue).ErrorMessage = ex.Message;

                }
                finally
                {
                    numTries++;
                }

                if (succeeded)
                {
                    //Todo - Please log to the log file as success
                    return returnValue;
                }

                //Delay for configured Milliseconds, before retry the WCF call.
                System.Threading.Thread.Sleep(delayInMilliseconds);
            }

            //Todo - Please log to the log file, if the code comes here, then its returnning NULL to the
            //Called application. Could be Critical.
            return default(Object);
        }

        public void ProcessErrorMessage(Exception ex, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            //string AlertMsg = "alert('" + ex.Message + "', 360, 50, '" + ModuleName + "');";
            string AlertMsg = "jAlert('" + ex.Message.Replace("'", "") + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }

        public void ProcessErrorMessage(string ex, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            //string AlertMsg = "alert('" + ex.Message + "', 360, 50, '" + ModuleName + "');";
            string AlertMsg = "jAlert('" + ex.Replace("'", "") + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }

        public void ProcessErrorMessageNew(Exception ex, string ModuleName)
        {
            //Manish 
            string AlertMsg = "jAlert('" + ex.Message + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertscript", AlertMsg, true);
        }

        public void ShowAlert(string message, string ModuleName)
        {
            //Vishwa
            // Radalert has been changed to javascript alert
            string AlertMsg = "radalert('" + message + "', 360, 50, '" + ModuleName + "');";
            //string AlertMsg = "radalert('" + message + "', '" + ModuleName + "');";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", AlertMsg, true);
        }
        protected void ShowSuccessMessage()
        {
            //string AlertMsg = "alert('" + "Test" + "');";
            //ScriptManager.(this, this.GetType(), "radalert", AlertMsg, true);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
        }
    }
}