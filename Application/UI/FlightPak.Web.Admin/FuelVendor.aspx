﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FuelVendor.aspx.cs" Inherits="FlightPak.Web.Admin.FuelVendor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/Common.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jqgrid/jquery-1.11.0.min.js") %>"></script>
        <script type="text/javascript" src="<% =ResolveClientUrl("~/Scripts/jquery.alerts.js") %>"></script>
        <script type="text/javascript">
            function ShowSuccessMessage() {
                $(document).ready(function () {
                    $("#tdSuccessMessage").css("display", "inline");
                    $('#tdSuccessMessage').delay(60000).fadeOut(60000);
                });
            }
            
            function CloseAdd_UpdateFuelVendorRadWindow() {
                var oWnd = GetRadWindow();
                var dialogB = oWnd.get_windowManager().getWindowByName("radWinFuelVendorPopup");
                dialogB.get_contentFrame().contentWindow.OnClientCloseFuelVendorAddWindow();
                oWnd.close();
            }

            $(document).ready(function () {
                $('#art-contentLayout_popup').css("width", "600px");
                $('#art-sheet').css("width", "620px");
                $('#art-sheet-body').css("width", "620px");
                $('#art-sheet-body').css("position", "fixed");
                $('#art-sheet-body').css("padding", "0px 0px 0px 5px");
            });
        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
   <div id="art-main">
        <div id="art-sheet" class="art-Sheet">
            <div id="art-sheet-body" class="art-Sheet-body">
                <div id="art-contentLayout_popup" class="art-contentLayout_popup">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="headerText">
                                <div class="tab-nav-top">
                                    <span class="head-title">Fuel Vendors</span> <span class="tab-nav-icons"><a href="../../Help/ViewHelp.aspx?Screen=FuelVendorHelp"
                                        target="_blank" title="Help" class="help-icon"></a></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" class="border-box" runat="server" id="tblFormInput">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel80">
                                            <asp:CheckBox runat="server" ID="chkInactive" Text="Inactive" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120" valign="top">
                                            <span class="mnd_text">Fuel Vendor Code</span>
                                        </td>
                                        <td class="tdLabel120" valign="top">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbCode" runat="server" CssClass="text50" MaxLength="5" ValidationGroup="save"
                                                            AutoPostBack="true"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnFuelVendorID" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Code is Required"
                                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="save"></asp:CustomValidator>
                                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ErrorMessage="Code is Required"
                                                            Display="Dynamic" SetFocusOnError="true" ControlToValidate="tbCode" CssClass="alert-text"
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td class="tdLabel80" valign="top">
                                            <span class="mnd_text">Name</span>
                                        </td>
                                        <td>
                                        </td>
                                        <td class="tdLabel120" valign="top">
                                            <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbName" ValidationGroup="save" runat="server" CssClass="text200"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfvName" Display="Dynamic" runat="server" CssClass="alert-text"
                                                            ControlToValidate="tbName" ValidationGroup="save">Name is Required.</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120" valign="top">
                                            <span class="mnd_text">Description</span>
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbDescription" ValidationGroup="save" runat="server" CssClass="text400"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:RequiredFieldValidator ID="rfvDescription" CssClass="alert-text" runat="server"
                                                            ControlToValidate="tbDescription" Display="Dynamic" ErrorMessage="Description is Required."
                                                            ValidationGroup="save"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120" valign="top">
                                            Server Information
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbServerInformation" runat="server" CssClass="text400" MaxLength="200"
                                                            onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                       
                        <tr style="display: none;">
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120" valign="top">
                                            File Location
                                        </td>
                                        <td>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbfilelocation" runat="server" CssClass="text400"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="tdLabel120" valign="top">
                                        </td>
                                        <td>  
                                            <asp:HiddenField ID="hdnIsFormatChanged" ClientIDMode="Static" Value="false" runat="server" />
                                            <asp:HiddenField ID="hdnIframeSuccess" ClientIDMode="Static" Value="" runat="server" />
                                            <asp:HiddenField ID="hdnMappedColumnsIDs" Value="" runat="server" />
                                            <asp:HiddenField ID="hdnIsColumnsMappedCorrect" Value="false" runat="server" />
                                            <asp:HiddenField ID="hdnUploadedFileName" runat="server" />                                       
                                            <asp:Label ID="lblFilename" ClientIDMode="Static" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                                            
                    </table>
                    <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSave" ValidationGroup="save" Text="Save" runat="server" CssClass="button" OnClick="btnSave_OnClick" />
                                <asp:Button ID="btnCancel" CausesValidation="false" Text="Cancel" runat="server" OnClick="btnCancel_Click"
                                    CssClass="button"/>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hdnSave" runat="server" />
                    <asp:HiddenField ID="hdnDate" runat="server" />
                    <asp:HiddenField ID="hdnRedirect" runat="server" /> 
                </div>
                <div class="cleared">
                </div>
            </div>
            
        </div>
    </div>
    </form>
</body>
</html>
