﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AirportMasterPopup.aspx.cs"
    Inherits="FlightPak.Web.Admin.AirportMasterPopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var oArg = new Object();
            var grid = $find("<%= dgAirport.ClientID %>");

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function returnToParent() {
                //create the argument that will be returned to the parent page
                oArg = new Object();
                grid = $find("<%= dgAirport.ClientID %>");
                var MasterTable = grid.get_masterTableView();
                var selectedRows = MasterTable.get_selectedItems();
                var SelectedMain = false;
                for (var i = 0; i < selectedRows.length; i++) {
                    var row = selectedRows[i];
                    var cell1 = MasterTable.getCellByColumnUniqueName(row, "IcaoID");
                    var cell2 = MasterTable.getCellByColumnUniqueName(row, "AirportID");

                }
                if (selectedRows.length > 0) {
                    oArg.ICAO = cell1.innerHTML;
                    oArg.AirportId = cell2.innerHTML;
                }
                else {
                    oArg.ICAO = "";
                    oArg.AirportId = "";

                }
                var oWnd = GetRadWindow();
                if (oArg) {
                    oWnd.close(oArg);
                }
            }

            function Close() {
                GetRadWindow().Close();
            }
        </script>
    </telerik:RadCodeBlock>
    <div class="divGridPanel">
        <asp:ScriptManager ID="scr1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="dgAirport">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgAirport" />
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgAirport" runat="server" AllowMultiRowSelection="true" AllowSorting="true"
            OnNeedDataSource="dgAirport_BindData" AutoGenerateColumns="false" Height="400px"
            PageSize="10" AllowPaging="true" Width="900px">
            <MasterTableView DataKeyNames="AirportID,IcaoID,CityName,StateName,CountryName,AirportName,IsInActive,LongestRunway,UWAID,IsEntryPort"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="AirportID" HeaderText="AirportID" Display="false"
                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IcaoID" HeaderText="ICAO" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CityName" HeaderText="City" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StateName" HeaderText="State" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CountryName" HeaderText="Country" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AirportName" HeaderText="Airport" AutoPostBackOnFilter="true"
                        CurrentFilterFunction="Contains" ShowFilterIcon="false">
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="IsInActive" HeaderText="IsInactive">
                    </telerik:GridBoundColumn>--%>
                    <%--<telerik:GridBoundColumn DataField="LongestRunway" HeaderText="LongestRunway">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="UWAID" HeaderText="UWAID">
                    </telerik:GridBoundColumn>--%>
                    <%--<telerik:GridBoundColumn DataField="IsEntryPort" HeaderText="IsEntryPort">
                    </telerik:GridBoundColumn>--%>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; text-align: right;">
                        <button id="btnSubmit" onclick="returnToParent(); return false;" class="button">
                            Ok</button>
                        <!--<button id="btnCancel" onclick="Close();" class="button">
                            Cancel</button>-->
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
                <ClientEvents OnRowDblClick="returnToParent" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
    </div>
    </form>
</body>
</html>
