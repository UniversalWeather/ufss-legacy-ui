﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightPak.Web.Admin.AdminService;
using FlightPak.Web.Admin.GridHelpers;
using FlightPak.Web.Admin.GridHelpers.CoreApiFormatters;

namespace FlightPak.Web.Admin
{
    public partial class FuelVendorPopup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Hashtable GetFuelVendorByFilter(int page, int rows, string sidx, string sord, bool _search, string filters, bool isInActive)
        {
            var data = new Hashtable();
            List<Filter> listFilters = null;
            var fuelVendorList = new List<FlightPak.Web.Admin.AdminService.FuelVendor>();
            if (!string.IsNullOrEmpty(filters))
            {
                listFilters = jqGridFilterHelper.GetCoreApiCompatibleFilters(filters);
            }
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                using (var fuelVendorService = new AdminServiceClient())
                {
                    var fuelVendorServiceInfo = fuelVendorService.GetFuelVendor();
                    if (fuelVendorServiceInfo.ReturnFlag && fuelVendorServiceInfo.EntityList.Count > 0)
                    {
                        fuelVendorList = isInActive == false ? fuelVendorServiceInfo.EntityList.Where(s => s.IsDeleted == false && s.IsInActive == isInActive).ToList() : fuelVendorServiceInfo.EntityList.Where(s => s.IsDeleted == false).ToList();
                    }
                }
            }
            if (listFilters != null && listFilters.Count > 0)
            {
                foreach (var filter in listFilters)
                {
                    switch (filter.Field.ToLower())
                    {
                        case "vendorcd":
                            fuelVendorList = fuelVendorList.Where(v => v.VendorCD.StartsWith(filter.Value)).ToList();
                            break;
                        case "vendorname":
                            fuelVendorList = fuelVendorList.Where(v => v.VendorName.StartsWith(filter.Value)).ToList();
                            break;
                        case "vendordescription":
                            fuelVendorList = fuelVendorList.Where(v => v.VendorDescription.StartsWith(filter.Value)).ToList();
                            break;
                    }
                }
            }
            data["results"] = fuelVendorList.Where(x => x.IsDeleted == false).Select(s => new { s.FuelVendorID, s.VendorCD, s.VendorName, s.VendorDescription, s.IsDeleted, s.IsInActive }).ToList<Object>();
            data["meta"] = new PagingMetaData() { Page = page, Size = rows, page_count = Convert.ToInt32(Math.Ceiling(Math.Ceiling(Convert.ToDecimal(fuelVendorList.Count) / rows))), total_items = fuelVendorList.Count };
            return data;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> DeleteFuelVendor(long fuelVendorId, string fuelVendorCode)
        {
            var oFSSOperationResult = new FSSOperationResult<Hashtable>();
            var ret = new Hashtable();
            try
            {
                using (var fuelVendorService = new AdminServiceClient())
                {
                    FlightPak.Web.Admin.AdminService.FuelVendor fuelVendor;
                    if (fuelVendorId!=0)
                    {
                        var deleteReturnValue = fuelVendorService.DeleteFuelVendor(fuelVendorId);
                        if (deleteReturnValue.ReturnFlag)
                        {
                            ret.Add("ResponseCode", 1);
                            oFSSOperationResult.Result = ret;
                            oFSSOperationResult.StatusCode = HttpStatusCode.OK;
                        }
                    }
                    else
                    {
                        ret.Add("ResponseCode", 2);
                        oFSSOperationResult.Result = ret;
                        oFSSOperationResult.StatusCode = HttpStatusCode.OK;
                    }
                }
            }
            catch (Exception ex)
            {
                var exceptionList = new StringBuilder();
                exceptionList.AppendLine("Message : " + ex.Message);
                if (ex.InnerException != null) exceptionList.AppendLine("InnerException : " + ex.InnerException.Message.ToString());
                oFSSOperationResult.ErrorsList.Add(exceptionList.ToString());
                oFSSOperationResult.StatusCode = HttpStatusCode.InternalServerError;
            }
            return oFSSOperationResult;
        }
    }
}