﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MaintainGroupAccess.aspx.cs" Inherits="FlightPak.Web.Admin.MaintainGroupAccess" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="App_Themes/Default/style.css" rel="Stylesheet" type="text/css" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="ScriptManager1" runat="server">  
        </asp:ScriptManager> 
     <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainGroupAccess" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="dgMaintainGroupAccess">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dgMaintainGroupAccess" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="clrFilters">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
    <table style="width: 100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <div class="tab-nav-top">
                    <span class="head-title">Maintain Groups and Access</span> <span class="tab-nav-icons"><a
                        href="#" class="help-icon"></a></span>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
    <telerik:RadGrid ID="dgMaintainGroupAccess" runat="server" AllowSorting="true" OnItemCreated="dgMaintainGroupAccess_ItemCreated"
        Visible="true" OnNeedDataSource="dgMaintainGroupAccess_BindData" OnItemCommand="dgMaintainGroupAccess_ItemCommand"
        OnUpdateCommand="dgMaintainGroupAccess_UpdateCommand" OnInsertCommand="dgMaintainGroupAccess_InsertCommand"
        OnDeleteCommand="dgMaintainGroupAccess_DeleteCommand" AutoGenerateColumns="false"
        Height="330px" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgMaintainGroupAccess_SelectedIndexChanged"
        OnItemDataBound="dgMaintainGroupAccess_ItemDataBound" AllowFilteringByColumn="true"
        PagerStyle-AlwaysVisible="true">
        <MasterTableView DataKeyNames=""
            CommandItemDisplay="Bottom">
            <Columns>
                <telerik:GridBoundColumn DataField="GroupCD" HeaderText="Group Code">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="GroupDescription" HeaderText="Group Description">
                </telerik:GridBoundColumn>                 
            </Columns>
            <CommandItemTemplate>
                <div style="padding: 5px 5px; float: left; clear: both;">
                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                        ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                </div>
                <div style="padding: 5px 5px; float: right;">
                    <asp:Label ID="lbLastUpdatedGroup" runat="server"></asp:Label>
                </div>
            </CommandItemTemplate>
        </MasterTableView>
        <ClientSettings EnablePostBackOnRowClick="true">
            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
            <Selecting AllowRowSelect="true" />
        </ClientSettings>
    </telerik:RadGrid>
    <table>
         <tr>
                       <td class="tdLabel100">
                        <b>
                            <asp:Label ID="lbGroupCode" runat="server" Text="Group Code"></asp:Label></b>
                       </td>
                       <td>
                           <asp:TextBox ID="tbGroupCode" runat="server"  ValidationGroup="save" ></asp:TextBox>
                       </td>                      
                      </tr>

                       <tr>
                       <td class="tdLabel100">
                        <b>
                            <asp:Label ID="lbDesc" runat="server" Text="Description"></asp:Label></b>
                       </td>
                       <td>
                           <asp:TextBox ID="tbDesc" runat="server"  ValidationGroup="save" ></asp:TextBox>
                       </td>                      
                      </tr>
                      </table>

   
      
        <!-- content start -->
       
        <telerik:RadGrid ID="RadGrid1"  runat="server" PageSize="20"
            AllowSorting="True" AllowPaging="True" ShowGroupPanel="True" AutoGenerateColumns="False"
            GridLines="None">
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView Width="100%" GroupLoadMode="Client" TableLayout="Fixed">
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Module" FieldName="Module"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="Module"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                 <telerik:GridBoundColumn SortExpression="ContactTitle" HeaderText="Contact Title"
                        HeaderButtonType="TextButton" DataField="">
                    </telerik:GridBoundColumn>
                  <telerik:GridCheckBoxColumn UniqueName="GridCheckBoxView" DataField="" HeaderText="View" FooterText="CheckBox column footer" /> 
                  <telerik:GridCheckBoxColumn UniqueName="GridCheckBoxAdd" DataField="" HeaderText="Add" FooterText="CheckBox column footer" /> 
                  <telerik:GridCheckBoxColumn UniqueName="GridCheckBoxEdit" DataField="" HeaderText="Edit" FooterText="CheckBox column footer" /> 
                  <telerik:GridCheckBoxColumn UniqueName="GridCheckBoxDelete" DataField="" HeaderText="Delete" FooterText="CheckBox column footer" /> 


                    
                </Columns>
            </MasterTableView>
            <ClientSettings AllowGroupExpandCollapse="True" ReorderColumnsOnClient="True" AllowDragToGroup="True"
                AllowColumnsReorder="True">
            </ClientSettings>
             <GroupingSettings ShowUnGroupButton="true" />
        </telerik:RadGrid>
        <br />
<asp:Button ID="btnPostBack" CssClass="button" runat="server" Text="Refresh page"
Width="120px"></asp:Button>
       
      

    
    </div>
    </form>
</body>
</html>
