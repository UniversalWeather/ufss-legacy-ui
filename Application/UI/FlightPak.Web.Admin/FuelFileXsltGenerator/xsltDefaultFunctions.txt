<xsl:template name="HeaderTemplate">
    <xsl:if test="$ICAO"><xsl:value-of select="'ICAO'"/></xsl:if>
    <xsl:if test="$IATA"><xsl:value-of select="',IATA'"/></xsl:if>
    <xsl:if test="$FBO"><xsl:value-of select="',FBO'"/></xsl:if>
    <xsl:if test="$EFFECTIVEDATE"><xsl:value-of select="',EFFECTIVEDATE'"/></xsl:if>
    <xsl:if test="$NOTE"><xsl:value-of select="',NOTE'"/></xsl:if>
    <xsl:value-of select="',LOW'"/>
    <xsl:value-of select="',HIGH'"/>
    <xsl:value-of select="',PRICE'"/>
</xsl:template>
<xsl:template name="tokenizeRange">
    <xsl:param name="tokenizeString"/>
    <xsl:variable name="tokenizeDelimiter" select="'-'"/>
    <xsl:choose>
        <xsl:when test="contains($tokenizeString, $tokenizeDelimiter)">
            <xsl:value-of select="substring-before($tokenizeString,$tokenizeDelimiter)"/><xsl:value-of select="$delimiter"/>
            <xsl:value-of select="substring-after($tokenizeString,$tokenizeDelimiter)"/><xsl:value-of select="$delimiter"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:variable name="newRange" select="concat($tokenizeString,'-99999')"/>
            <xsl:value-of select="substring-before($newRange,$tokenizeDelimiter)"/><xsl:value-of select="$delimiter"/>
            <xsl:value-of select="substring-after($newRange,$tokenizeDelimiter)"/><xsl:value-of select="$delimiter"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="fuelInfo">
    <xsl:variable name="currNode" select="." />
    <xsl:if test="$ICAO">
        <xsl:value-of select="$currNode/child::*[name()=$ICAO]"/><xsl:value-of select="$delimiter"/>
    </xsl:if>
    <xsl:if test="$IATA">
        <xsl:value-of select="$currNode/child::*[name()=$IATA]"/><xsl:value-of select="$delimiter"/>
    </xsl:if>
    <xsl:if test="$FBO">
        <xsl:value-of select="$currNode/child::*[name()=$FBO]"/><xsl:value-of select="$delimiter"/>
    </xsl:if>
    <xsl:if test="$EFFECTIVEDATE">
        <xsl:value-of select="$currNode/child::*[name()=$EFFECTIVEDATE]"/><xsl:value-of select="$delimiter"/>
    </xsl:if>
    <xsl:if test="$NOTE">
        <xsl:value-of select="$currNode/child::*[name()=$NOTE]"/><xsl:value-of select="$delimiter"/>
    </xsl:if>
</xsl:template>