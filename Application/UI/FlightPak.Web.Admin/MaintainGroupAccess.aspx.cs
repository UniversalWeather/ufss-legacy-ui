﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FlightPak.Web.Admin
{
    public partial class MaintainGroupAccess : System.Web.UI.Page
    {
        private List<string> listAircraftCodes = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Grid Control could be ajaxified when the page is initially loaded.
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgMaintainGroupAccess, dgMaintainGroupAccess, RadAjaxLoadingPanel1);
            // Store the clientID of the grid to reference it later on the client
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgMaintainGroupAccess.ClientID));
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
            {
                e.Updated = dgMaintainGroupAccess;
            }
        }


        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemCreated(object sender, GridItemEventArgs e)
        {
            //if (e.Item is GridCommandItem)
            //{
            //    //Added based on UWA requirement.
            //    LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
            //    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

            //    LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
            //    RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
            //}
        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            //FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
            //if (objService.GetAircraftDuty().ReturnFlag == true)
            //{
            //    dgMaintainGroupAccess.DataSource = objService.GetAircraftDuty().EntityList;
            //}

            //Session.Remove("DutyCodes");
            //listAircraftCodes = new List<string>();
            //foreach (AircraftDuty objEntity in objService.GetAircraftDuty().EntityList)
            //{
            //    listAircraftCodes.Add(objEntity.AircraftDutyCD.Trim().ToLower());
            //    Session["DutyCodes"] = listAircraftCodes;
            //}
        }

        /// <summary>
        /// Datagrid Item Command for Aircraft Type Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //switch (e.CommandName)
            //{
            //    case RadGrid.EditCommandName:
            //        e.Canceled = true;
            //        e.Item.Selected = true;

            //        pnlExternalForm.Visible = true;
            //        hdnSave.Value = "Update";

            //        GridDataItem item = (GridDataItem)Session["SelectedItem"];
            //        string dutyCode = item.GetDataKeyValue("AircraftDutyCD").ToString();
            //        tbCode.Text = dutyCode;
            //        tbCode.ReadOnly = true;
            //        tbCode.BackColor = System.Drawing.Color.LightGray;

            //        if (dutyCode.Trim() == "F" || dutyCode.Trim() == "G" || dutyCode.Trim() == "R")
            //        {
            //            tbDescription.ReadOnly = true;
            //            tbDescription.BackColor = System.Drawing.Color.LightGray;
            //        }

            //        if (item.GetDataKeyValue("AircraftDutyDescription") != null)
            //        {
            //            tbDescription.Text = item.GetDataKeyValue("AircraftDutyDescription").ToString();
            //        }
            //        else
            //        {
            //            tbDescription.Text = string.Empty;
            //        }
            //        if (item.GetDataKeyValue("ForeGrndCustomColor") != null)
            //        {
            //            System.Drawing.Color foreColor;
            //            tbForeColor.Text = item.GetDataKeyValue("ForeGrndCustomColor").ToString();
            //            hdnForeColor.Value = item.GetDataKeyValue("ForeGrndCustomColor").ToString();
            //            foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
            //            rcpForeColor.SelectedColor = foreColor;
            //        }
            //        else
            //        {
            //            tbForeColor.Text = string.Empty;
            //        }
            //        if (item.GetDataKeyValue("BackgroundCustomColor") != null)
            //        {
            //            System.Drawing.Color backColor;
            //            tbBackColor.Text = item.GetDataKeyValue("BackgroundCustomColor").ToString();
            //            hdnBackColor.Value = item.GetDataKeyValue("BackgroundCustomColor").ToString();
            //            backColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
            //            rcpBackColor.SelectedColor = backColor;
            //        }
            //        else
            //        {
            //            tbBackColor.Text = string.Empty;
            //        }
            //        if (item.GetDataKeyValue("IsCalendarEntry") != null)
            //        {
            //            chkQuickCalendarEntry.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsCalendarEntry").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsCalendarEntry").ToString(), CultureInfo.CurrentCulture);
            //        }
            //        else
            //        {
            //            chkQuickCalendarEntry.Checked = false;
            //        }
            //        if (item.GetDataKeyValue("IsAircraftStandby") != null)
            //        {
            //            chkAircraftStandBy.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsAircraftStandby").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsAircraftStandby").ToString(), CultureInfo.CurrentCulture);
            //        }
            //        else
            //        {
            //            chkAircraftStandBy.Checked = false;
            //        }

            //        if (item.GetDataKeyValue("DefaultStartTM") != null)
            //        {
            //            tbStart.Text = item.GetDataKeyValue("DefaultStartTM").ToString();
            //        }
            //        else
            //        {
            //            tbStart.Text = "00:00";
            //        }
            //        if (item.GetDataKeyValue("DefualtEndTM") != null)
            //        {
            //            tbEnd.Text = item.GetDataKeyValue("DefualtEndTM").ToString();
            //        }
            //        else
            //        {
            //            tbEnd.Text = "00:00";
            //        }

            //        dgMaintainGroupAccess.Rebind();
            //        GridEnable(false, true, false);
            //        EnableForm(true);
            //        break;
            //    case RadGrid.InitInsertCommandName:
            //        e.Canceled = true;
            //        dgMaintainGroupAccess.SelectedIndexes.Clear();
            //        GridEnable(true, false, false);
            //        DisplayInsertForm();
            //        dgMaintainGroupAccess.Rebind();
            //        break;
            //    case "Filter":
            //        foreach (GridColumn column in e.Item.OwnerTableView.Columns)
            //        {
            //            column.CurrentFilterValue = string.Empty;
            //            column.CurrentFilterFunction = GridKnownFunction.NoFilter;
            //        }
            //        break;
            //    default:
            //        break;
            //}
        }

        /// <summary>
        /// Update Command for Aircraft Duty Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_UpdateCommand(object source, GridCommandEventArgs e)
        {
            //e.Canceled = true;

            //try
            //{
            //    if (Session["SelectedItem"] != null)
            //    {
            //        btnCancel.Visible = true;
            //        btnSaveChanges.Visible = true;

            //        if (Convert.ToDouble(tbStart.Text, CultureInfo.CurrentCulture) > Convert.ToDouble(tbEnd.Text, CultureInfo.CurrentCulture))
            //        {
            //            string alertMsg = "radalert('Start time should be lesser than end time', 360, 50, 'Aircraftduty Type Catalog');";
            //            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
            //        }
            //        else
            //        {
            //            FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
            //            FlightPakMasterService.AircraftDuty objAirDutyType = new FlightPakMasterService.AircraftDuty();

            //            objAirDutyType.AircraftDutyCD = tbCode.Text;
            //            objAirDutyType.AircraftDutyDescription = tbDescription.Text;
            //            objAirDutyType.BackgroundCustomColor = hdnBackColor.Value;
            //            objAirDutyType.ForeGrndCustomColor = hdnForeColor.Value;
            //            objAirDutyType.DefaultStartTM = tbStart.Text;
            //            objAirDutyType.DefualtEndTM = tbEnd.Text;
            //            objAirDutyType.IsAircraftStandby = chkAircraftStandBy.Checked;
            //            objAirDutyType.IsCalendarEntry = chkQuickCalendarEntry.Checked;
            //            objService.UpdateAircraftDuty(objAirDutyType);

            //            e.Item.OwnerTableView.Rebind();
            //            e.Item.Selected = true;
            //            //pnlExternalForm.Visible = false;

            //            dgMaintainGroupAccess.Rebind();
            //        }
            //    }
            //}
            //catch (System.NullReferenceException exc)
            //{
            //    e.Canceled = true;
            //    throw exc;
            //}
            //catch (System.ArgumentOutOfRangeException exc)
            //{
            //    e.Canceled = true;
            //    throw exc;
            //}
            //catch (System.InvalidCastException exc)
            //{
            //    e.Canceled = true;
            //    throw exc;
            //}
        }


        /// <summary>
        /// Insert Command for Passenger Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_InsertCommand(object source, GridCommandEventArgs e)
        {
            //try
            //{
            //    btnSaveChanges.Visible = true;
            //    btnCancel.Visible = true;

            //    e.Canceled = true;
            //    if (CheckAllReadyExist())
            //    {
            //        string alertMsg = "radalert('Unique Aircraft Duty Type Code is required', 380, 50, 'Aircraft Duty Type');";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
            //    }
            //    else if (Convert.ToDouble(tbStart.Text, CultureInfo.CurrentCulture) > Convert.ToDouble(tbEnd.Text, CultureInfo.CurrentCulture))
            //    {
            //        string alertMsg = "radalert('Start time should be lesser than end time', 360, 50, 'Aircraftduty Type Catalog');";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
            //    }
            //    else
            //    {
            //        FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
            //        FlightPakMasterService.AircraftDuty objAirDutyType = new FlightPakMasterService.AircraftDuty();
            //        objAirDutyType.AircraftDutyCD = tbCode.Text;
            //        objAirDutyType.AircraftDutyDescription = tbDescription.Text;
            //        objAirDutyType.BackgroundCustomColor = hdnBackColor.Value;
            //        objAirDutyType.ForeGrndCustomColor = hdnForeColor.Value;
            //        objAirDutyType.DefaultStartTM = tbStart.Text;
            //        objAirDutyType.DefualtEndTM = tbEnd.Text;
            //        objAirDutyType.IsAircraftStandby = chkAircraftStandBy.Checked;
            //        objAirDutyType.IsCalendarEntry = chkQuickCalendarEntry.Checked;

            //        objService.AddAircraftDuty(objAirDutyType);
            //        //pnlExternalForm.Visible = false;

            //        dgMaintainGroupAccess.Rebind();
            //        DisplayInsertForm();
            //    }
            //}
            //catch (System.NullReferenceException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //}
            //catch (System.ArgumentOutOfRangeException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //}
            //catch (System.InvalidCastException exc)
            //{
            //    e.Canceled = true;
            //    e.Item.OwnerTableView.IsItemInserted = false;
            //}
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_DeleteCommand(object source, GridCommandEventArgs e)
        {
            //if (Session["SelectedItem"] != null)
            //{
            //    GridDataItem item = (GridDataItem)Session["SelectedItem"];
            //    string dutyCode = item.GetDataKeyValue("AircraftDutyCD").ToString();
            //    if (dutyCode.Trim() == "F" || dutyCode.Trim() == "G" || dutyCode.Trim() == "R")
            //    {
            //        string alertMsg = "radalert('R, F and G Codes Are Reserved For Internal Use by FlightPak.', 380, 50, 'Aircraft Duty Type');";
            //        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
            //    }
            //    else
            //    {
            //        FlightPakMasterService.MasterCatalogServiceClient objService = new FlightPakMasterService.MasterCatalogServiceClient();
            //        FlightPakMasterService.AircraftDuty objAirDutyType = new FlightPakMasterService.AircraftDuty();
            //        objAirDutyType.AircraftDutyCD = item.GetDataKeyValue("AircraftDutyCD").ToString();
            //        objAirDutyType.IsDeleted = true;
            //        objService.DeleteAircraftDuty(objAirDutyType);

            //        DisplayInsertForm();
            //    }
            //}
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GridDataItem item = (GridDataItem)dgMaintainGroupAccess.SelectedItems[0];
            //Session["SelectedItem"] = item;
            //dgMaintainGroupAccess.Rebind();
            //item.Selected = true;
            //Label lbLastUpdatedGroup;

            //lbLastUpdatedGroup = (Label)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedGroup");
            //if (item.GetDataKeyValue("LastGroupID") != null)
            //{
            //    lbLastUpdatedGroup.Text = "Last Updated Group: " + item.GetDataKeyValue("LastGroupID").ToString();
            //}
            //else
            //{
            //    lbLastUpdatedGroup.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("LastUptTS") != null)
            //{
            //    lbLastUpdatedGroup.Text = lbLastUpdatedGroup.Text + " Date: " + item.GetDataKeyValue("LastUptTS").ToString();
            //}
            //ReadOnlyForm();
            //GridEnable(true, true, true);
        }

        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            //pnlExternalForm.Visible = true;
            //hdnSave.Value = "Save";

            //tbCode.Text = string.Empty;
            //tbDescription.Text = string.Empty;
            //tbStart.Text = "00:00";
            //tbEnd.Text = "00:00";
            //chkQuickCalendarEntry.Checked = false;
            //chkAircraftStandBy.Checked = false;
            //tbForeColor.Text = string.Empty;
            //hdnForeColor.Value = string.Empty;
            //tbBackColor.Text = string.Empty;
            //hdnBackColor.Value = string.Empty;

            //rcpBackColor.SelectedColor = System.Drawing.Color.White;
            //rcpForeColor.SelectedColor = System.Drawing.Color.White;

            //tbCode.ReadOnly = false;
            //tbCode.BackColor = System.Drawing.Color.White;
            //tbDescription.ReadOnly = false;
            //tbDescription.BackColor = System.Drawing.Color.White;

            //EnableForm(true);
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Aircraft Duty Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            //if (hdnSave.Value == "Update")
            //{
            //    (dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
            //}
            //else
            //{
            //    (dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
            //}
            //dgMaintainGroupAccess.Rebind();
            //GridEnable(true, true, true);
        }


        /// <summary>
        /// Cancel Aircraft Duty Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //pnlExternalForm.Visible = false;
            //Session["SelectedItem"] = null;

            //dgMaintainGroupAccess.Rebind();

            //btnSaveChanges.Visible = false;
            //btnCancel.Visible = false;
            //GridEnable(true, true, true);
        }


        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgMaintainGroupAccess_ItemDataBound(object sender, GridItemEventArgs e)
        {
            //if (e.Item is GridDataItem)
            //{
            //    GridDataItem item = (GridDataItem)e.Item;
            //    TableCell cell = (TableCell)item["AircraftDutyCD"];
            //    TableCell cell1 = (TableCell)item["AircraftDutyDescription"];
            //    cell.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
            //    cell1.BackColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BackgroundCustomColor"), CultureInfo.CurrentCulture));
            //    cell.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
            //    cell1.ForeColor = System.Drawing.Color.FromName(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ForeGrndCustomColor"), CultureInfo.CurrentCulture));
            //}
        }


        /// <summary>
        /// Function to Check if Aircraft Duty Type Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyExist()
        {
            bool returnVal = false;
            //listAircraftCodes = (List<string>)Session["DutyCodes"];
            //if (listAircraftCodes != null && listAircraftCodes.Contains(tbCode.Text.ToString().Trim().ToLower()))
            //    return true;
            return returnVal;
        }

        private void GridEnable(bool add, bool edit, bool delete)
        {
            LinkButton insertCtl, delCtl, editCtl;
            insertCtl = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
            delCtl = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
            editCtl = (LinkButton)dgMaintainGroupAccess.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
            if (add)
                insertCtl.Enabled = true;
            else
                insertCtl.Enabled = false;
            if (delete)
            {
                delCtl.Enabled = true;
                delCtl.OnClientClick = "javascript:return ProcessDelete();";

            }
            else
            {
                delCtl.Enabled = false;
                delCtl.OnClientClick = string.Empty;
            }
            if (edit)
            {
                editCtl.Enabled = true;
                editCtl.OnClientClick = "javascript:return ProcessUpdate();";
            }
            else
            {
                editCtl.Enabled = false;
                editCtl.OnClientClick = string.Empty;
            }
        }

        protected void ReadOnlyForm()
        {

            //GridDataItem item = (GridDataItem)Session["SelectedItem"];

            //tbCode.Text = item.GetDataKeyValue("AircraftDutyCD").ToString();
            //if (item.GetDataKeyValue("AircraftDutyDescription") != null)
            //{
            //    tbDescription.Text = item.GetDataKeyValue("AircraftDutyDescription").ToString();
            //}
            //else
            //{
            //    tbDescription.Text = string.Empty;
            //}

            //if (item.GetDataKeyValue("ForeGrndCustomColor") != null)
            //{
            //    System.Drawing.Color foreColor;
            //    tbForeColor.Text = item.GetDataKeyValue("ForeGrndCustomColor").ToString();
            //    hdnForeColor.Value = item.GetDataKeyValue("ForeGrndCustomColor").ToString();
            //    foreColor = System.Drawing.Color.FromName(Convert.ToString(tbForeColor.Text, CultureInfo.CurrentCulture));
            //    rcpForeColor.SelectedColor = foreColor;
            //}
            //else
            //{
            //    tbForeColor.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("BackgroundCustomColor") != null)
            //{
            //    System.Drawing.Color backColor;
            //    tbBackColor.Text = item.GetDataKeyValue("BackgroundCustomColor").ToString();
            //    hdnBackColor.Value = item.GetDataKeyValue("BackgroundCustomColor").ToString();
            //    backColor = System.Drawing.Color.FromName(Convert.ToString(tbBackColor.Text, CultureInfo.CurrentCulture));
            //    rcpBackColor.SelectedColor = backColor;
            //}
            //else
            //{
            //    tbBackColor.Text = string.Empty;
            //}
            //if (item.GetDataKeyValue("IsCalendarEntry") != null)
            //{
            //    chkQuickCalendarEntry.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsCalendarEntry").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsCalendarEntry").ToString(), CultureInfo.CurrentCulture);
            //}
            //else
            //{
            //    chkQuickCalendarEntry.Checked = false;
            //}
            //if (item.GetDataKeyValue("IsAircraftStandby") != null)
            //{
            //    chkAircraftStandBy.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsAircraftStandby").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsAircraftStandby").ToString(), CultureInfo.CurrentCulture);
            //}
            //else
            //{
            //    chkAircraftStandBy.Checked = false;
            //}

            //if (item.GetDataKeyValue("DefaultStartTM") != null)
            //{
            //    tbStart.Text = item.GetDataKeyValue("DefaultStartTM").ToString().Trim();
            //}
            //else
            //{
            //    tbStart.Text = "00:00";
            //}
            //if (item.GetDataKeyValue("DefualtEndTM") != null)
            //{
            //    tbEnd.Text = item.GetDataKeyValue("DefualtEndTM").ToString().Trim();
            //}
            //else
            //{
            //    tbEnd.Text = "00:00";
            //}

            //EnableForm(false);
        }


        protected void EnableForm(bool enable)
        {
            //tbCode.Enabled = enable;
            //tbDescription.Enabled = enable;
            //tbStart.Enabled = enable;
            //tbEnd.Enabled = enable;
            //chkQuickCalendarEntry.Enabled = enable;
            //chkAircraftStandBy.Enabled = enable;
            //tbForeColor.Enabled = enable;
            //tbBackColor.Enabled = enable;
            //rcpForeColor.Enabled = enable;
            //rcpBackColor.Enabled = enable;

            //btnCancel.Visible = enable;
            //btnSaveChanges.Visible = enable;
        }
    }
}