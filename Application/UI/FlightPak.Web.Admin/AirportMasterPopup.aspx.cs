﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.Admin;

namespace FlightPak.Web.Admin
{
    public partial class AirportMasterPopup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Added for Reassign the Selected Value and highlight the specified row in the Grid
            try
            {
                if (Request.QueryString["IcaoID"] != null)
                {
                    string IcaoID = Request.QueryString["IcaoID"];

                    if (IcaoID != null || IcaoID != string.Empty)
                    {
                        dgAirport.Rebind();

                        foreach (GridDataItem item in dgAirport.MasterTableView.Items)
                        {
                            if (item["IcaoID"].Text == IcaoID)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }
            }
            catch (System.NullReferenceException ex)
            { //Manually Handled
            }

        }

        /// <summary>
        /// Bind Passenger Group Catalog Data into Grid
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Grid Event Argument</param>
        protected void dgAirport_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (AdminService.AdminServiceClient AdminService = new AdminService.AdminServiceClient())
            {
                Int64 UwaCustomerId = AdminService.GetUWACustomerId();
                var ObjRetVal = AdminService.GetAirportMasterInfo(UwaCustomerId);
                if (ObjRetVal.ReturnFlag == true)
                {
                    dgAirport.DataSource = ObjRetVal.EntityList;
                }
            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Ajax Creating Event</param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            e.Updated = dgAirport;
        }
    }
}

