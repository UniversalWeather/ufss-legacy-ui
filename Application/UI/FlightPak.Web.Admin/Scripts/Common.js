﻿/*
* <summary>
* Get the querystring value by providing Query Id
* </summary>
* <param name="key">A string contains the querystring key</param>
* <param name="defaultVal">Object which get returns when there is not key</param>
**/
function getQuerystring(key, defaultVal) {
    if (defaultVal == null) {
        defaultVal = "";
    }
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null) {
        return defaultVal;
    }
    else {
        return qs[1];
    }
}

/*
* <summary>
* Get the Selected Value from the Radio Button
* </summary>
* <param name="radioObj">Object which has been passed</param>
**/
function getCheckedValue(radioObj) {
    if (!radioObj)
        return "";
    var radioLength = radioObj.length;
    if (radioLength == undefined)
        if (radioObj.checked)
            return radioObj.value;
        else
            return "";
for (var i = 0; i < radioLength; i++) {
    if (radioObj[i].checked) {
        return radioObj[i].value;
    }
}
return "";
}

/*
* <summary>
* Function to allow only a Alphabet Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnAllowAlpha(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // alpha characters
    else if (("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ").indexOf(keychar) > -1)
        return true;
    else
        return false;
}


/*
* <summary>
* Function to allow only a Alphabet and Numeric Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnAllowAlphaNumeric(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // alpha and numric characters
    else if (("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ").indexOf(keychar) > -1)
        return true;
    else
        return false;
}
/*
* <summary>
* Function not to any Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnNotAllowKeys(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return false;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // no keys
    else if (("").indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to allow only a Numeric Values
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
* <param name="dec">Decimal Point to be passed(optional)</param>
**/
function fnAllowNumeric(myfield, e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // numbers
    else if (("0123456789").indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to allow Numeric with customized Characters (i.e., for Date, Phone etc. Eg: 10/12/2011, 123-456-7890)
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
* <param name="char">Character you need to allow to be passed</param>
**/
function fnAllowNumericAndChar(myfield, e, char) {
    var key;
    var keychar;
    var allowedString = "0123456789" + char;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // numeric values and allow slash("/")
    else if ((allowedString).indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to allow Alphabets, Numeric and customized Characters (i.e., for Color Specs, #FC10EE)
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
* <param name="char">Character you need to allow to be passed</param>
**/
function fnAllowAlphaNumericAndChar(myfield, e, char) {
    var key;
    var keychar;
    var allowedString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" + char;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // numeric values and allow slash("/")
    else if ((allowedString).indexOf(keychar) > -1)
        return true;
    else
        return false;
}

/*
* <summary>
* Function to Ask for Confirmation, if you select any row to delete.
* </summary>
**/
function ProcessDelete(customMsg) {

    //get reference to the client grid object using the global window variable set in the user control's Page_Load server handler
    var grid = $find(window['gridId']);

    var msg = 'Are you sure you want to delete this record?';

    if (customMsg != null) {
        msg = customMsg;
    }

    if (grid.get_masterTableView().get_selectedItems().length > 0) {

        radconfirm(msg, callBackFn, 330, 100, '', 'Delete');
        return false;
        //        if (confirm(msg)) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
    }
    else {
        radalert('Please select a record from the above table', 330, 100, "Delete", "");
        return false;
    }
}
function callBackFn(confirmed) {
    if (confirmed) {
        var grid = $find(window['gridId']);
        grid.get_masterTableView().fireCommand("DeleteSelected");
    }
}

/*
* <summary>
* To check whether the row is selected in grid before update
* </summary>
**/
function ProcessUpdate() {
    var grid = $find(window['gridId']);
    if (grid.get_masterTableView().get_selectedItems().length == 0) {
        radalert('Please select a record from the above table', 330, 100, "Edit", "");
        return false;
    }
}

/*
* <summary>
* Function to check if any special characters are available, while copy from any source.
* If any special characters found, it will automatically removed.
* </summary>
**/
function RemoveSpecialChars(elementRef) {
    var checkValue = new String(elementRef.value);
    var newValue = '';
    var isExists = false;

    for (var i = 0; i < checkValue.length; i++) {
        var currentChar = checkValue.charAt(i);

        if ((currentChar != '`') && (currentChar != '~') && (currentChar != '!') && (currentChar != '^') && (currentChar != '&') && (currentChar != '*')
                && (currentChar != '(') && (currentChar != ')') && (currentChar != '+') && (currentChar != '=') && (currentChar != '{') && (currentChar != '}')
                && (currentChar != '[') && (currentChar != ']') && (currentChar != '?') && (currentChar != '/') && (currentChar != '<') && (currentChar != '>')
                && (currentChar != ':') && (currentChar != ';') && (currentChar != '|') && (currentChar != '#') && (currentChar != '%') && (currentChar != '\\')
                && (currentChar != '\"') && (currentChar != '\'') && (currentChar != '$') && (currentChar != '-') && (currentChar != '_') && (currentChar != '@')) {
            newValue += currentChar;
        }

        if (checkValue.indexOf(currentChar) > -1) {
            isExists = true;
        }
    }

    elementRef.value = newValue;

}

//Disable right click script
//visit http://www.rainbow.arch.scriptmania.com/scripts/
var message = "Sorry, right-click has been disabled";
///////////////////////////////////
function clickIE() { if (document.all) { (message); return false; } }
function clickNS(e) {
    if
(document.layers || (document.getElementById && !document.all)) {
        if (e.which == 2 || e.which == 3) { (message); return false; }
    }
}
//To disable right click comment in the below code
//if (document.layers)
//{ document.captureEvents(Event.MOUSEDOWN); document.onmousedown = clickNS; }
//else { document.onmouseup = clickNS; document.oncontextmenu = clickIE; }
//document.oncontextmenu = new Function("return false")


//this function is used to parse the date entered or selected by the user
function parseDate(sender, e) {
    if (currentDatePicker != null) {
        var date = currentDatePicker.get_dateInput().parseDate(sender.value);
        var dateInput = currentDatePicker.get_dateInput();
        var formattedDate = dateInput.get_dateFormatInfo().FormatDate(date, dateInput.get_displayDateFormat());
        sender.value = formattedDate;
    }
}


/*
* <summary>
* Function to allow Phone format
* </summary>
* <param name="myfield">Control Id to be passed</param>
* <param name="e">Event to be passed</param>
**/
function fnAllowPhoneFormat(myfield, e) {
    //    var key;
    //    var keychar;
    //    //add the character and special character to 'spchar' that need to be allowed for phone format
    //    var spchar = "EeXxTtNn -+()#,"; 
    //    var allowedString = "0123456789" + spchar;
    //    if (window.event)
    //        key = window.event.keyCode;
    //    else if (e)
    //        key = e.which;
    //    else
    //        return true;
    //    keychar = String.fromCharCode(key);
    //    // control keys
    //    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
    //        return true;
    //    // numeric values and allow slash("/")
    //    else if ((allowedString).indexOf(keychar) > -1)
    //        return true;
    //    else
    //        return false;
    return true;
}

function RemoveSpecialCharsInPhone(elementRef) {
    //    var checkValue = new String(elementRef.value);
    //    var newValue = '';
    //    var isExists = false;
    //    var spchar = "EeXxTtNn -+()#,";
    //    var allowedString = "0123456789" + spchar;

    //    for (var i = 0; i < checkValue.length; i++) {
    //        var currentChar = checkValue.charAt(i);
    //        
    //        if (allowedString.indexOf(currentChar) != -1) {
    //            newValue += currentChar;
    //        }

    //        if (checkValue.indexOf(currentChar) > -1) {
    //            isExists = true;
    //        }
    //    }

    //    elementRef.value = newValue;
    return true;

}


function PrintWindow(content) {
    var printWindow = window.open('', 'PrintWindow');
    printWindow.document.write('<html><head><title>Print Window - Universal Weather and Aviation</title>');
    printWindow.document.write('</head><body ><img src=\'');
    printWindow.document.write(content.src);
    printWindow.document.write('\' /></body></html>');
    printWindow.document.close();
    printWindow.print();
}

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function browserName() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf("msie") != -1) return 'Internet Explorer';
    if (agt.indexOf("chrome") != -1) return 'Chrome';
    if (agt.indexOf("opera") != -1) return 'Opera';
    if (agt.indexOf("firefox") != -1) return 'Firefox';
    if (agt.indexOf("mozilla/5.0") != -1) return 'Mozilla';
    if (agt.indexOf("netscape") != -1) return 'Netscape';
    if (agt.indexOf("safari") != -1) return 'Safari';
    if (agt.indexOf("staroffice") != -1) return 'Star Office';
    if (agt.indexOf("webtv") != -1) return 'WebTV';
    if (agt.indexOf("beonex") != -1) return 'Beonex';
    if (agt.indexOf("chimera") != -1) return 'Chimera';
    if (agt.indexOf("netpositive") != -1) return 'NetPositive';
    if (agt.indexOf("phoenix") != -1) return 'Phoenix';
    if (agt.indexOf("skipstone") != -1) return 'SkipStone';
    if (agt.indexOf('\/') != -1) {
        if (agt.substr(0, agt.indexOf('\/')) != 'mozilla') {
            return navigator.userAgent.substr(0, agt.indexOf('\/'));
        }
        else return 'Netscape';
    } else if (agt.indexOf(' ') != -1)
        return navigator.userAgent.substr(0, agt.indexOf(' '));
    else return navigator.userAgent;
}

/*
* <summary>
* Function to check weather given value is not null, empty or undefined
* </summary>
* <param name="value">Control value to be passed</param>
**/
function IsNullOrEmpty(value) {
    return (value == null || value == "");
}

function verifyReturnedResultForJqgrid(fssOperationResult) {
    fssOperationResult = htmlDecode(fssOperationResult);

    if (fssOperationResult != undefined && fssOperationResult != null && fssOperationResult != "") {
        try {
            if (typeof fssOperationResult == "string") {
                fssOperationResult = JSON.parse(fssOperationResult);
            }
            if (fssOperationResult.StatusCode == 401) {
                parent.window.location.href = "/Account/Login.aspx";
            }
        }
        catch (ex) {
        }
    }
}

function GetRadWindow() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}

function popupwindow(url, title, w, h, grid) {
    var oWindow = GetRadWindow();
    openSizedWindow(url, oWindow, w, h, grid);
    return true;
}

function openSizedWindow(page, radwindow, w, h, grid, centerPopup) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var parentRadWindowManager = parentPage.GetRadWindowManager();
    var oWnd2 = parentRadWindowManager.open(page, radwindow);
    oWnd2.setSize(w, h);
    try { oWnd2.add_pageLoad(OnPageLoadComplete); } catch (e) { }
    oWnd2.argument = grid;

    try { oWnd2.add_close(OnPageClosed); } catch (e) { }
    oWnd2.set_modal(true);
    oWnd2.set_visibleStatusbar(true);
    try { oWnd2.moveTo(oWnd2.center(), 0); } catch (e) { }
    oWnd2.set_status('');
    if (centerPopup) {
        oWnd2.center();
    }
}

function OnPageLoadComplete(sender, eventArgs) {
    sender.remove_pageLoad();
    setTimeout(function () {
        sender.set_status('');
    }, 0);
    setTimeout(function () {
        sender.setActive(true);
    }, 0);
}

function GetRadWindowPopup() {
    var oWindow = null;
    if (window.radWindow) oWindow = window.radWindow;
    else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
    return oWindow;
}

function OnPageClosed(sender, eventArgs) {
    sender.remove_close();
    if (sender.argument != undefined && sender.argument != null) {
        if (sender.argument.indexOf("#") >= 0) {
            $(sender.argument).trigger('reloadGrid');
        }
        else {
            $('#' + sender.argument).trigger('reloadGrid');
        }
    }
}

function showMessageBox(message, title) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var oManager = parentPage.GetRadWindowManager();
    var width = 300;
    var height = 120;

    if (message.length > 300) {
        width = 430;
        height = 280;
    }

    if (message.length > 1300) {
        width = 550;
        height = 400;
    }
    oManager.radalert(message, width, height, title);
}

function showConfirmPopup(message, callbackfn, popupTitle) {
    var parentPage = GetRadWindowPopup().BrowserWindow;
    var oManager = parentPage.GetRadWindowManager();
    oManager.radconfirm(message, callbackfn, 330, 100, '', popupTitle);
}

function AjaxRequestStart_IE8Fix() {
    if ($telerik.isIE8) {
        document.documentElement.focus();
    }
}