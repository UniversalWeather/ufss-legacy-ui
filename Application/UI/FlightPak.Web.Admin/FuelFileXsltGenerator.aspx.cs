﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.Admin.AdminService;
using FlightPak.Web.Admin.GridHelpers;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Newtonsoft.Json;
using Telerik.Web.UI;

namespace FlightPak.Web.Admin
{
    public partial class FuelFileXsltGenerator : BasePage
    {
        #region "Variable Declaration"
        private ExceptionManager _exManager;
        #endregion

        #region "Page Eventes"
        protected void Page_Load(object sender, EventArgs e)
        {
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgFuelFileXsltParser, dgFuelFileXsltParser, RadAjaxLoadingPanel1);
            //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveXSLTFile, dgFuelFileXsltParser, RadAjaxLoadingPanel1);
            //RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(btnSaveXSLTFile, DivExternalForm, RadAjaxLoadingPanel1);
            // Store the clientID of the grid to reference it later on the client
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgFuelFileXsltParser.ClientID));
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['windowManagerId'] = '{0}';", RadWindowManager1.ClientID));
            if (!IsPostBack)
            {
                SetFormControlMode(false);
                DefaultSelection(true);
            }
            UploadCSVFile();
        }
        #endregion

        #region "Grid Eventes"
        protected void dgFuelFileXsltParser_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new AdminService.AdminServiceClient())
                        {
                            List<FuelFileXsltParser> xsltParserList = new List<FuelFileXsltParser>();
                            var xsltParserInfo = client.GetAllXsltParser();
                            if (xsltParserInfo.ReturnFlag)
                            {
                                xsltParserList = xsltParserInfo.EntityList.Where(x => x.IsDeleted == false).ToList();
                                dgFuelFileXsltParser.DataSource = xsltParserList;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void dgFuelFileXsltParser_OnItemCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                if ((Session[FuelFileConstants.XsltParserID] != null) && dgFuelFileXsltParser.SelectedItems.Count > 0)
                                {
                                    e.Canceled = true;
                                    rfvForCsvFile.Enabled = false;
                                    txtXsltFileName.Focus();
                                    DisplayEditForm();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                rfvForCsvFile.Enabled = true;
                                SetFormControlMode(true);
                                txtXsltFileName.Focus();
                                DisplayInsertForm();
                                break;
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void dgFuelFileXsltParser_OnInsertCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        e.Canceled = true;
                        using (var client = new AdminService.AdminServiceClient())
                        {
                            FuelFileXsltParser oFuelFileXsltParser = new FuelFileXsltParser();
                            oFuelFileXsltParser = GetFuelFileXsltParser(oFuelFileXsltParser);
                            ReturnValueOfFuelFileXsltParser result = new ReturnValueOfFuelFileXsltParser();
                            if (hdnSave.Value == "Insert")
                            {
                                result = client.AddFuelFileXsltParser(oFuelFileXsltParser);
                            }
                            if (result.ReturnFlag)
                            {
                                var oFuelFileData = (FuelFileData)HttpContext.Current.Session[FuelFileConstants.FuelFileData];
                                var xsltParserList = client.GetAllXsltParser();
                                int xsltParserId = xsltParserList.EntityList.Where(a => a.IsDeleted == false && a.IsInActive == false).OrderByDescending(x => x.LastUpdTS).FirstOrDefault().FuelFileXsltParserID;
                                HttpContext.Current.Session[FuelFileConstants.XsltParserID] = xsltParserId;
                                List<FuelCsvFileFields> csvFildes = new List<FuelCsvFileFields>();
                                Regex removeWhitespacePattern = new Regex(@"\s+");
                                var headers = Utility.FetchPreviewRecordsInSession(oFuelFileData.UploadFileData, 1)[0].ConvertAll(a => removeWhitespacePattern.Replace(a.ToLower().Trim(), "").Replace('/', '_')).ToList();
                                var retFuelFileCsvHeaders = client.AddFuelFileCsvHeaders(headers, xsltParserId);
                                if (retFuelFileCsvHeaders.ReturnFlag)
                                {
                                    if (!string.IsNullOrEmpty(hdnFuelVendor.Value) && !string.IsNullOrEmpty(txtFuelVendor.Text))
                                    {

                                        FuelSavedFileFormat oFuelSavedFileFormat = new FuelSavedFileFormat { FuelFileXsltParserID = xsltParserId, FuelVendorID = Convert.ToInt64(hdnFuelVendor.Value) };
                                        var retFuelSavedFileFormat = client.AddFuelSavedFileFormat(oFuelSavedFileFormat);
                                    }
                                    HttpContext.Current.Session[FuelFileConstants.FuelFileData] = null;
                                    hdnSave.Value = string.Empty;
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
                                    DefaultSelection(true);
                                }
                            }
                            else
                            {
                                ProcessErrorMessage(result.ErrorMessage, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void dgFuelFileXsltParser_OnUpdateCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session[FuelFileConstants.XsltParserID] != null)
                        {
                            using (var client = new AdminService.AdminServiceClient())
                            {
                                ReturnValueOfFuelFileXsltParser result = new ReturnValueOfFuelFileXsltParser();
                                AdminService.FuelFileXsltParser oFuelFileXsltParser = new AdminService.FuelFileXsltParser();
                                oFuelFileXsltParser = GetFuelFileXsltParser(oFuelFileXsltParser);
                                if (hdnSave.Value == "Update")
                                {
                                    result = client.UpdateFuelFileXsltParser(oFuelFileXsltParser);
                                }
                                if (result.ReturnFlag)
                                {
                                    int xsltParserId = Convert.ToInt32(Session[FuelFileConstants.XsltParserID]);
                                    if (HttpContext.Current.Session[FuelFileConstants.FuelFileData] != null)
                                    {
                                        var oFuelFileData = (AdminService.FuelFileData)HttpContext.Current.Session[FuelFileConstants.FuelFileData];
                                        var retDeletField = client.DeleteOldCsvFieldsBasedOnXsltParserId(xsltParserId);
                                        if (retDeletField.ReturnFlag)
                                        {
                                            var csvFildes = new List<FuelCsvFileFields>();
                                            Regex removeWhitespacePattern = new Regex(@"\s+");
                                            var headers = Utility.FetchPreviewRecordsInSession(oFuelFileData.UploadFileData, 1)[0].ConvertAll(a => removeWhitespacePattern.Replace(a.ToLower().Trim(), "").Replace('/', '_')).ToList();
                                            client.AddFuelFileCsvHeaders(headers, xsltParserId);
                                        }
                                    }
                                    if (Session[FuelFileConstants.FuelVendor] != null)
                                    {
                                        var oFuelSavedFileFormat = (FuelSavedFileFormat)Session[FuelFileConstants.FuelVendor];
                                        if (oFuelSavedFileFormat.FuelFileXsltParserID != Convert.ToInt64(hdnFuelVendor.Value))
                                        {
                                            oFuelSavedFileFormat = new FuelSavedFileFormat
                                            {
                                                FuelSavedFileFormatID = oFuelSavedFileFormat.FuelSavedFileFormatID,
                                                FuelVendorID = Convert.ToInt64(hdnFuelVendor.Value),
                                                FuelFileXsltParserID = xsltParserId
                                            };
                                            client.UpdateFuelSavedFileFormat(oFuelSavedFileFormat);
                                            Session[FuelFileConstants.FuelVendor] = null;
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(hdnFuelVendor.Value))
                                        {
                                            var oFuelSavedFileFormat = new FuelSavedFileFormat();
                                            oFuelSavedFileFormat.FuelFileXsltParserID = xsltParserId;
                                            oFuelSavedFileFormat.FuelVendorID = Convert.ToInt64(hdnFuelVendor.Value);
                                            client.AddFuelSavedFileFormat(oFuelSavedFileFormat);
                                        }
                                    }
                                    HttpContext.Current.Session[FuelFileConstants.FuelFileData] = null;
                                    hdnSave.Value = string.Empty;
                                    DefaultSelection(true);
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowSuccessMessage", "ShowSuccessMessage();", true);
                                }
                                else
                                {
                                    ProcessErrorMessage(result.ErrorMessage, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                                }
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void dgFuelFileXsltParser_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                if (hdnSave.Value == "Update")
                {
                    string confirmMessage = "Do you want to save changes to the current record?";
                    rwmUpdateConfirmation.RadConfirm(confirmMessage, "UpdateConfirmCallBackFn", 330, 110, null, "Confirmation!");
                }
                else
                {
                    try
                    {
                        _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        _exManager.Process(() =>
                        {
                            GridDataItem item = dgFuelFileXsltParser.SelectedItems[0] as GridDataItem;
                            Session[FuelFileConstants.XsltParserID] = item["FuelFileXsltParserID"].Text;
                            ReadOnlyForm();
                        }, FlightPak.Common.Constants.Policy.UILayer);
                    }
                    catch (Exception ex)
                    {
                        ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                    }
                }
            }
        }
        protected void dgFuelFileXsltParser_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        e.Canceled = true;
                        if (Session[FuelFileConstants.XsltParserID] != null)
                        {
                            using (var client = new AdminService.AdminServiceClient())
                            {
                                AdminService.FuelFileXsltParser oFuelFileXsltParser = new AdminService.FuelFileXsltParser();
                                GridDataItem item = dgFuelFileXsltParser.SelectedItems[0] as GridDataItem;
                                oFuelFileXsltParser = GetFuelFileXsltParser(oFuelFileXsltParser);
                                oFuelFileXsltParser.FuelFileXsltParserID = Convert.ToInt32(item.GetDataKeyValue("FuelFileXsltParserID"));
                                client.DeleteFuelFileXsltParser(oFuelFileXsltParser);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                HttpContext.Current.Session[FuelFileConstants.FuelFileData] = null;
                                Session[FuelFileConstants.XsltParserID] = null;
                                hdnSave.Value = string.Empty;
                                DefaultSelection(false);
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void dgFuelFileXsltParser_OnItemCreated(object sender, GridItemEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (e.Item is GridCommandItem)
                        {
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Account);
                }
            }
        }
        #endregion

        #region "Private Function"
        protected void SetFormControlMode(bool enable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                if (txtXsltFileName.Text.ToLower() == FuelFileConstants.XSLTFileNameForErrorLog.ToLower())
                {
                    chkIsActive.Enabled = false;
                    txtXsltFileName.Enabled = false;
                    txtXsltDescription.Enabled = false;
                    uploadedCsvFile.Enabled = false;
                    txtFuelVendor.Enabled = false;
                    btnFuelVendor.Enabled = false;
                    btnFuelVendor.CssClass = "browse-button-disabled";
                    rfvFuelVendor.Enabled = false;
                }
                else
                {
                    chkIsActive.Enabled = enable;
                    txtXsltFileName.Enabled = enable;
                    txtXsltDescription.Enabled = enable;
                    uploadedCsvFile.Enabled = enable;
                    txtFuelVendor.Enabled = enable;
                    btnFuelVendor.Enabled = enable;
                    btnFuelVendor.CssClass = enable ? "browse-button" : "browse-button-disabled";
                    rfvFuelVendor.Enabled = enable;
                }
                btnSaveXSLTFile.Visible = enable;
                btnCancel.Visible = enable;
                hdnFormMode.Value = Convert.ToString(enable);
            }
        }
        protected void DisplayInsertForm()
        {
            hdnSave.Value = "Insert";
            ClearForm();
            SetFormControlMode(true);
        }
        protected void ClearForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                chkIsActive.Checked = false;
                txtXsltFileName.Text = string.Empty;
                txtXsltDescription.Text = string.Empty;
                hdnXsltContent.Value = string.Empty;
                hdnXsltFunctionContent.Value = string.Empty;
                hdnCsvData.Value = string.Empty;
                hdnAllCsvData.Value = string.Empty;
                txtFuelVendor.Text = string.Empty;
                hdnFuelVendor.Value = string.Empty;
                lblFuelVendorDescription.Text = string.Empty;
                lblCSVFilename.Text = string.Empty;
            }
        }
        protected void ReadOnlyForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                ClearForm();
                LoadControlData();
                SetFormControlMode(false);
            }
        }
        protected void LoadControlData()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (Session[FuelFileConstants.XsltParserID] != null)
                        {
                            Session[FuelFileConstants.FuelVendor] = null;
                            int xsltParserId = Convert.ToInt32(Session[FuelFileConstants.XsltParserID]);
                            FuelFileXsltParser parser = new FuelFileXsltParser();
                            using (var client = new AdminService.AdminServiceClient())
                            {
                                var xsltParserInfo = client.GetXsltParserById(xsltParserId);
                                if (xsltParserInfo.ReturnFlag && xsltParserInfo.EntityList.Count > 0)
                                {
                                    parser = xsltParserInfo.EntityList.FirstOrDefault();
                                    chkIsActive.Checked = parser.IsInActive.Value;
                                    txtXsltFileName.Text = parser.XsltFileName;
                                    txtXsltDescription.Text = parser.XsltFileDescription;
                                    hdnXsltParserId.Value = Convert.ToString(parser.FuelFileXsltParserID);
                                    hdnXsltContent.Value = HttpUtility.HtmlEncode(parser.XsltFileContain);
                                    hdnXsltFunctionContent.Value = HttpUtility.HtmlEncode(parser.XsltFunctionContain);
                                    hdnAllCsvData.Value = parser.CsvData;
                                    hdnCsvData.Value = GetCsvDataAsJson(parser.CsvData, 3);
                                    if (parser.FuelSavedFileFormat != null && parser.FuelSavedFileFormat.Count > 0)
                                    {
                                        var oFuelSavedFileFormat = parser.FuelSavedFileFormat.FirstOrDefault(s => s.FuelVendor.IsDeleted == false && s.FuelVendor.IsInActive == false);
                                        if (oFuelSavedFileFormat != null && oFuelSavedFileFormat.FuelVendorID != null)
                                        {
                                            Session[FuelFileConstants.FuelVendor] = oFuelSavedFileFormat;
                                            hdnFuelVendor.Value = Convert.ToString(oFuelSavedFileFormat.FuelVendor.FuelVendorID);
                                            txtFuelVendor.Text = oFuelSavedFileFormat.FuelVendor.VendorCD;
                                            lblFuelVendorDescription.Text = oFuelSavedFileFormat.FuelVendor.VendorDescription;
                                        }
                                    }
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        private void DefaultSelection(bool BindDataSwitch)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BindDataSwitch))
            {
                if (BindDataSwitch)
                {
                    dgFuelFileXsltParser.Rebind();
                }
                if (dgFuelFileXsltParser.MasterTableView.Items.Count > 0)
                {
                    if (Session[FuelFileConstants.XsltParserID] == null)
                    {
                        dgFuelFileXsltParser.SelectedIndexes.Add(0);
                        Session[FuelFileConstants.XsltParserID] = dgFuelFileXsltParser.Items[0].GetDataKeyValue("FuelFileXsltParserID").ToString();
                    }

                    if (dgFuelFileXsltParser.SelectedIndexes.Count == 0)
                        dgFuelFileXsltParser.SelectedIndexes.Add(0);

                    ReadOnlyForm();
                }
                else
                {
                    ClearForm();
                    SetFormControlMode(false);
                }
            }
        }
        private FuelFileXsltParser GetFuelFileXsltParser(FuelFileXsltParser oFuelFileXsltParser)
        {
            if (hdnSave.Value == "Update" && !string.IsNullOrEmpty(hdnXsltParserId.Value))
            {
                oFuelFileXsltParser.FuelFileXsltParserID = Convert.ToInt32(hdnXsltParserId.Value);
                Session[FuelFileConstants.XsltParserID] = hdnXsltParserId.Value;
            }
            if (HttpContext.Current.Session[FuelFileConstants.FuelFileData] != null)
            {
                var oFuelFileData = (AdminService.FuelFileData)HttpContext.Current.Session[FuelFileConstants.FuelFileData];
                oFuelFileXsltParser.CsvData = ReadLinesFromCSVData(oFuelFileData.UploadFileData, 5);
            }
            else
            {

                oFuelFileXsltParser.CsvData = hdnAllCsvData.Value;
            }
            oFuelFileXsltParser.XsltFileName = txtXsltFileName.Text;
            oFuelFileXsltParser.XsltFileDescription = txtXsltDescription.Text;
            oFuelFileXsltParser.XsltFileContain = HttpUtility.HtmlDecode(hdnXsltContent.Value);
            oFuelFileXsltParser.XsltFunctionContain = HttpUtility.HtmlDecode(hdnXsltFunctionContent.Value);
            oFuelFileXsltParser.IsDeleted = false;
            oFuelFileXsltParser.IsInActive = chkIsActive.Checked;
            return oFuelFileXsltParser;
        }
        private void DisplayEditForm()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Update";
                LoadControlData();
                SetFormControlMode(true);
            }
        }
        private bool IsValidXsltContent()
        {
            try
            {
                string xsltContain = string.Format("{0}{1}{2}{3}", FuelFileConstants.FirstXSLTPart, HttpUtility.HtmlDecode(hdnXsltContent.Value), HttpUtility.HtmlDecode(hdnXsltFunctionContent.Value), FuelFileConstants.LastXSLTPart);
                var xslt = new XslTransform();
                using (XmlReader xmlreader = XmlReader.Create(new StringReader(xsltContain)))
                {
                    xslt.Load(xmlreader);
                    return true;
                }
            }
            catch (Exception ex)
            {
                var exceptionList = new StringBuilder();
                exceptionList.AppendLine("Message : " + ex.Message);
                if (ex.InnerException != null) exceptionList.AppendLine("InnerException : " + ex.InnerException.Message.ToString());
                customValidatorForXsltContent.ErrorMessage = exceptionList.Replace("\n", "<br/>").ToString();
                return false;
            }
        }
        private bool ValidateUniqueXsltFileName()
        {
            bool isValid = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new AdminService.AdminServiceClient())
                        {
                            var xsltParserInfo = client.GetAllXsltParser();
                            int counter = 0;
                            if (xsltParserInfo.ReturnFlag && xsltParserInfo.EntityList.Count > 0)
                            {
                                if (hdnSave.Value == "Update")
                                {
                                    int xsltParserId = Convert.ToInt32(Session[FuelFileConstants.XsltParserID]);
                                    counter = xsltParserInfo.EntityList.Count(x => x.IsDeleted == false && x.FuelFileXsltParserID != xsltParserId && x.XsltFileName.Trim() == txtXsltFileName.Text.Trim());
                                }
                                else
                                {
                                    counter = xsltParserInfo.EntityList.Count(x => x.IsDeleted == false && x.XsltFileName.Trim() == txtXsltFileName.Text.Trim());
                                }
                            }
                            isValid = counter == 0;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
            return isValid;
        }
        public static FlightPak.Web.Admin.AdminService.FuelFileData InitializeFuelFileData(FlightPak.Web.Admin.AdminService.FuelFileData fuelData)
        {
            fuelData.FBO = -1;
            fuelData.Vendor = -1;
            fuelData.Text -= -1;
            fuelData.Price = -1;
            fuelData.Low = -1;
            fuelData.ICAO = -1;
            fuelData.IATA = -1;
            fuelData.High = -1;
            fuelData.EffectiveDate = -1;

            return fuelData;
        }
        #endregion

        #region "Control Event"
        protected void imgDownloadXsltFile_OnClick(object sender, ImageClickEventArgs e)
        {
            Response.Clear();
            string xsltContain = string.Format("{0}{1}{2}{3}", FuelFileConstants.FirstXSLTPart, HttpUtility.HtmlDecode(hdnXsltContent.Value), HttpUtility.HtmlDecode(hdnXsltFunctionContent.Value), FuelFileConstants.LastXSLTPart);
            string xsltFileName = string.Format("UWA-{0}.xslt", string.IsNullOrEmpty(txtXsltFileName.Text) ? DateTime.Now.ToString("yyyy-MMM-dd-HHmmss") : txtXsltFileName.Text);
            Response.ContentType = "text/xml";
            Response.AddHeader("content-disposition", "attachment; filename=" + xsltFileName);
            Response.Write(xsltContain);
            Response.End();
        }
        protected void imgDownloadCSVFile_OnClick(object sender, ImageClickEventArgs e)
        {
            string csvdata;
            if (HttpContext.Current.Session[FuelFileConstants.FuelFileData] != null)
            {
                var oFuelFileData = (AdminService.FuelFileData)HttpContext.Current.Session[FuelFileConstants.FuelFileData];
                csvdata = oFuelFileData.UploadFileData;
            }
            else
            {
                csvdata = hdnParsedCSV.Value;
            }
            Response.Clear();
            string xsltFileName = string.Format("UWA-{0}.csv", string.IsNullOrEmpty(txtXsltFileName.Text) ? DateTime.Now.ToString("yyyy-MMM-dd-HHmmss") : txtXsltFileName.Text);
            Response.ContentType = "text/csv";
            Response.AddHeader("content-disposition", "attachment; filename=" + xsltFileName);
            Response.Write(csvdata);
            Response.End();
        }
        protected void txtFuelVendor_OnTextChanged(object sender, EventArgs e)
        {
            var fuelVendorList = new List<AdminService.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        using (var client = new AdminService.AdminServiceClient())
                        {
                            if (!string.IsNullOrWhiteSpace(txtFuelVendor.Text))
                            {
                                var fuelVendorServiceInfo = client.GetFuelVendor();
                                if (fuelVendorServiceInfo.ReturnFlag && fuelVendorServiceInfo.EntityList.Count > 0)
                                {
                                    fuelVendorList = fuelVendorServiceInfo.EntityList.Where(s => s.IsDeleted == false && s.VendorCD.ToLower().Trim() == txtFuelVendor.Text.ToLower().Trim()).ToList();
                                    if (fuelVendorList.Count > 0)
                                    {
                                        hdnFuelVendor.Value = Convert.ToString(fuelVendorList[0].FuelVendorID);
                                        txtFuelVendor.Text = Convert.ToString(fuelVendorList[0].VendorCD).Trim();
                                        lblFuelVendorDescription.Text = Convert.ToString(fuelVendorList[0].VendorName).Trim();
                                        CustomValidatorFuelVendor.IsValid = true;
                                    }
                                    else
                                    {
                                        CustomValidatorFuelVendor.IsValid = false;
                                        lblFuelVendorDescription.Text = string.Empty;
                                        hdnFuelVendor.Value = string.Empty;
                                    }
                                }
                                else
                                {
                                    CustomValidatorFuelVendor.IsValid = false;
                                    lblFuelVendorDescription.Text = string.Empty;
                                    hdnFuelVendor.Value = string.Empty;
                                }
                            }
                            else
                            {
                                lblFuelVendorDescription.Text = string.Empty;
                                hdnFuelVendor.Value = string.Empty;
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        hdnSave.Value = "";
                        DefaultSelection(false);
                        HttpContext.Current.Session[FuelFileConstants.FuelFileData] = null;
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void btnSaveXSLTFile_OnClick(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        bool isValidPage = true;
                        if (!IsValidXsltContent())
                        {
                            isValidPage = false;
                            customValidatorForXsltContent.IsValid = false;
                        }
                        if (!ValidateUniqueXsltFileName())
                        {
                            isValidPage = false;
                            customValidatorXsltFileNameUnique.IsValid = false;
                        }
                        if (isValidPage)
                        {
                            (dgFuelFileXsltParser.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(hdnSave.Value == "Update" ? RadGrid.UpdateCommandName : RadGrid.PerformInsertCommandName, string.Empty);
                            dgFuelFileXsltParser.Rebind();
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        #endregion

        #region "Web Methods"
        /// <summary>
        /// Get Preview data from uploded CSV file
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static Dictionary<string, object> GetUploadedCSVData()
        {
            var result = new Dictionary<string, object>();
            string csvjsonstring = string.Empty;
            if (HttpContext.Current.Session[FuelFileConstants.FuelFileData] != null)
            {
                var oFuelFileData = (AdminService.FuelFileData)HttpContext.Current.Session[FuelFileConstants.FuelFileData];
                csvjsonstring = GetCsvDataAsJson(oFuelFileData.UploadFileData, 3);
            }
            if (!string.IsNullOrEmpty(csvjsonstring))
            {
                result["IsHaveDataToDisplay"] = true;
                result["CsvFileData"] = csvjsonstring;
            }
            else
            {
                result["IsHaveDataToDisplay"] = false;
            }
            return result;
        }

        /// <summary>
        /// Transform CSV file data using XSLT
        /// </summary>
        /// <param name="xsltContain">XSLT file string</param>
        /// <param name="storedcsvData">If Records is in edit mode and csv file is not upload then we can show parsed data with csv data which is stored in database</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static FSSOperationResult<Hashtable> TransformXSLT(string xsltContain, string xsltFunctionContain, string storedcsvData)
        {
            var oFSSOperationResult = new FSSOperationResult<Hashtable>();
            var ret = new Hashtable();
            var oFuelFileData = new FuelFileData();
            string fileDataInJson;
            try
            {
                xsltContain = string.Format("{0}{1}{2}{3}", FuelFileConstants.FirstXSLTPart, xsltContain, xsltFunctionContain, FuelFileConstants.LastXSLTPart);
                List<List<string>> fileReadRecords;
                if (HttpContext.Current.Session[FuelFileConstants.FuelFileData] != null)
                {
                    oFuelFileData = (AdminService.FuelFileData)HttpContext.Current.Session[FuelFileConstants.FuelFileData];
                    fileReadRecords = Utility.FetchPreviewRecordsInSession(oFuelFileData.UploadFileData, 50);
                }
                else
                {
                    fileReadRecords = Utility.FetchPreviewRecordsInSession(storedcsvData, -1);
                }
                var removeWhitespacePattern = new Regex(@"\s+");
                string[] headers = fileReadRecords[0].ConvertAll(a => removeWhitespacePattern.Replace(a.ToLower().Trim(), "").Replace('/', '_')).ToArray();
                fileReadRecords.RemoveAt(0);
                var xmlDoc = new XmlDocument();
                XmlNode rootNode = xmlDoc.CreateElement("fuelData");
                foreach (var record in fileReadRecords)
                {
                    XmlNode fuelInfo = xmlDoc.CreateElement("fuelInfo");
                    for (int count = 0; count < headers.Length; count++)
                    {
                        XmlNode fuelDetails = xmlDoc.CreateElement(headers[count]);
                        fuelDetails.InnerText = record[count];
                        fuelInfo.AppendChild(fuelDetails);
                    }
                    rootNode.AppendChild(fuelInfo);
                }
                xmlDoc.AppendChild(rootNode);

                string xmlInput = xmlDoc.InnerXml;
                string xsltInput = xsltContain;
                string csvOutput;
                var doc = new XPathDocument(new StringReader(xmlInput));
                var xslt = new XslTransform();
                using (XmlReader xmlreader = XmlReader.Create(new StringReader(xsltInput)))
                {
                    xslt.Load(xmlreader);
                    using (var sw = new StringWriter())
                    {
                        xslt.Transform(doc, null, sw);
                        csvOutput = sw.ToString();
                    }
                }
                if (HttpContext.Current.Session[FuelFileConstants.FuelFileData] != null)
                {
                    List<Dictionary<string, string>> parsedCSV = ConvertCSVFormatToDictionaryList(csvOutput);
                    fileDataInJson = ReadFileDataAsJson(parsedCSV);
                }
                else
                {
                    fileDataInJson = ReadFileDataAsJson(ConvertCSVFormatToDictionaryList(csvOutput));
                }

                if (!string.IsNullOrEmpty(fileDataInJson))
                {
                    ret["IsHaveDataToDisplay"] = true;
                    ret["ParsedCsvFileData"] = fileDataInJson;
                    ret["ParsedCSV"] = csvOutput;
                    oFSSOperationResult.Result = ret;
                    oFSSOperationResult.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    ret["IsHaveDataToDisplay"] = false;
                    oFSSOperationResult.Result = ret;
                    oFSSOperationResult.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                var exceptionList = new StringBuilder();
                exceptionList.AppendLine("Message : " + ex.Message);
                if (ex.InnerException != null) exceptionList.AppendLine("InnerException : " + ex.InnerException.Message.ToString());
                oFSSOperationResult.ErrorsList.Add(exceptionList.ToString());
                oFSSOperationResult.StatusCode = HttpStatusCode.InternalServerError;
            }
            return oFSSOperationResult;
        }
        #endregion

        #region "Upload Functions"
        /// <summary>
        /// Upload CSV File and Save it Details in Session
        /// </summary>
        private void UploadCSVFile()
        {
            byte[] fileData = null;
            string filename = string.Empty;
            if (uploadedCsvFile.PostedFile != null)
            {
                if (uploadedCsvFile.PostedFile.FileName.Length > 0 && IsCsvFile(uploadedCsvFile.PostedFile.FileName))
                {
                    int fileLength = Convert.ToInt32(uploadedCsvFile.FileContent.Length);
                    filename = uploadedCsvFile.PostedFile.FileName;
                    fileData = new byte[uploadedCsvFile.FileContent.Length];
                    using (var streamReader = uploadedCsvFile.FileContent)
                    {
                        streamReader.Read(fileData, 0, fileLength);
                    }
                }
                if (fileData != null && fileData.Length > 0)
                {
                    bool result = SaveCsvFile(filename, fileData);
                    lblCSVFilename.Text = filename;
                    btnTransformXml.Enabled = true;
                    hdnCsvData.Value = string.Empty;
                    hdnAllCsvData.Value = string.Empty;
                }
                else
                {
                    customValidatorForCsvFile.Validate();
                }
            }
        }
        /// <summary>
        /// Check Uploaded file is CSV or not
        /// </summary>
        /// <param name="filePath">File path with it name and extension</param>
        /// <returns></returns>
        private bool IsCsvFile(string filePath)
        {
            return filePath != null &&
                filePath.EndsWith(".csv", StringComparison.Ordinal);
        }
        /// <summary>
        /// Save File Data in Session
        /// </summary>
        /// <param name="name">Name of the File</param>
        /// <param name="data">Uploded file data in byte form</param>
        /// <returns></returns>
        private bool SaveCsvFile(string name, byte[] data)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                bool isSaved = false;
                //Handle methods throguh exception manager with return flag
                _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                _exManager.Process(() =>
                {
                    rfvForCsvFile.Enabled = false;
                    string folderPath = string.Format("{0}{1}", Server.MapPath(Page.ResolveUrl("~")), "Uploads\\FuelUpload");
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    string filename = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd-HH-mm-ss fff").Replace(" ", "-") + ".csv";
                    string filepath = folderPath + @"\" + filename;
                    File.WriteAllBytes(filepath, data);
                    var fuelData = new AdminService.FuelFileData();
                    fuelData = InitializeFuelFileData(fuelData);
                    fuelData.FilePath = Request.Url.Scheme + @"://" + Request.Url.Authority + @"/" + "Uploads\\FuelUpload".Replace('\\', '/') + @"/" + filename;
                    fuelData.FileName = filename;
                    fuelData.BaseFileName = name;
                    fuelData.FullPhysicalFilePath = folderPath + @"\" + filename;
                    using (var objReader = new StreamReader(File.OpenRead(filepath)))
                    {
                        fuelData.UploadFileData = objReader.ReadToEnd();
                    }
                    Session[FuelFileConstants.FuelFileData] = fuelData;
                    hdnCsvData.Value = GetCsvDataAsJson(fuelData.UploadFileData, 3);
                    DeleteCSVFileAfterDataSavedInSession();
                    isSaved = true;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return isSaved;
            }
        }
        /// <summary>
        /// Delete temp created CSV File on server after it Data is reading and Save in Seesion
        /// </summary>
        private void DeleteCSVFileAfterDataSavedInSession()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (Session[FuelFileConstants.FuelFileData] != null)
                        {
                            var objFuelFileData = (FuelFileData)Session[FuelFileConstants.FuelFileData];
                            if (File.Exists(objFuelFileData.FullPhysicalFilePath))
                            {
                                if (Path.GetFileName(objFuelFileData.FullPhysicalFilePath) == objFuelFileData.FileName)
                                    File.Delete(objFuelFileData.FullPhysicalFilePath);
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuleFileXSLTGenerator);
                }
            }
        }
        /// <summary>
        /// Custom validator Method for CSV file upload control
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void ValidateUploadedCsvFile(object source, ServerValidateEventArgs args)
        {
            if (uploadedCsvFile.PostedFile.FileName.Length > 0)
            {
                if (Path.GetExtension(uploadedCsvFile.PostedFile.FileName) == ".csv")
                {
                    args.IsValid = true;
                }
                else
                {
                    customValidatorForCsvFile.ErrorMessage = FuelFileConstants.ErrorMessageWrongFileTypeUpload;
                    args.IsValid = false;
                }
            }
            else
            {
                customValidatorForCsvFile.ErrorMessage = FuelFileConstants.ErrorMessageNoFileSelected;
                args.IsValid = false;
            }
        }
        #endregion

        #region "Static Method"
        public static List<Dictionary<string, string>> ConvertCSVFormatToDictionaryList(string csvData)
        {
            var parsedCSV = new List<Dictionary<string, string>>();
            var csvRecords = Utility.FetchPreviewRecordsInSession(csvData, -1);
            //Get all headers and convert all in lowercase and trim it
            Regex removeWhitespacePattern = new Regex(@"\s+");
            string[] headers = csvRecords[0].ConvertAll(a => removeWhitespacePattern.Replace(a.Trim(), "")).ToArray();
            csvRecords.RemoveAt(0);
            foreach (var records in csvRecords)
            {
                Dictionary<string, string> csvRec = new Dictionary<string, string>();
                for (int count = 0; count < headers.Count(); count++)
                {
                    csvRec.Add(headers[count], records[count]);
                }
                parsedCSV.Add(csvRec);
            }

            return parsedCSV;
        }
        public static string ReadFileDataAsJson(List<Dictionary<string, string>> fileData)
        {
            var fdata = ReadFileAsIndexNumber(fileData, 10);
            List<string> rowList = new List<string>();
            List<List<string>> masterList = new List<List<string>>();

            rowList.Add("FBO");
            rowList.Add("ICAO");
            rowList.Add("IATA");
            rowList.Add("Effective Date");
            rowList.Add("Gallon From");
            rowList.Add("Gallon To");
            rowList.Add("Price");

            masterList.Add(rowList);

            for (int i = 0; i < fdata.Count; i++)
            {
                var oneFuelRow = fdata[i];
                rowList = new List<string>();
                rowList.Add(oneFuelRow.FBO);
                rowList.Add(oneFuelRow.ICAO ?? "");
                rowList.Add(oneFuelRow.IATA ?? "");
                rowList.Add(oneFuelRow.EffectiveDate.Value.ToShortDateString());
                rowList.Add(oneFuelRow.GallonFrom.ToString() ?? "");
                rowList.Add(oneFuelRow.GallonTo.ToString() ?? "");
                rowList.Add(oneFuelRow.Price.ToString() ?? "");
                masterList.Add(rowList);
            }
            string jsondata = JsonConvert.SerializeObject(masterList);
            return jsondata;
        }
        public static List<FlightpakFuelModal> ReadFileAsIndexNumber(List<Dictionary<string, string>> parsedCSV, int readNoOfRecords)
        {
            List<FlightpakFuelModal> readTotalFile = new List<FlightpakFuelModal>();
            int cnt = 0;

            foreach (var item in parsedCSV)
            {

                if (cnt < readNoOfRecords)
                {
                    var objFlightpak = new FlightpakFuelModal();

                    if (item.ContainsKey("ICAO"))
                    {
                        objFlightpak.ICAO = Convert.ToString(item["ICAO"]);
                    }
                    if (item.ContainsKey("IATA"))
                    {
                        objFlightpak.IATA = Convert.ToString(item["IATA"]);
                    }
                    if (item.ContainsKey("FBO"))
                    {
                        objFlightpak.FBO = Convert.ToString(item["FBO"].Replace("\"", ""));
                    }
                    if (item.ContainsKey("EFFECTIVEDATE"))
                    {
                        DateTime tempdate = new DateTime();
                        if ((DateTime.TryParse(Convert.ToString(item["EFFECTIVEDATE"]), out tempdate)))
                        {
                            objFlightpak.EffectiveDate = tempdate;
                        }
                    }
                    if (item.ContainsKey("LOW"))
                    {
                        objFlightpak.GallonFrom = string.IsNullOrEmpty(item["LOW"]) ? (int?)null : Convert.ToInt32(item["LOW"]);
                    }
                    if (item.ContainsKey("HIGH"))
                    {
                        objFlightpak.GallonTo = string.IsNullOrEmpty(item["HIGH"]) ? (int?)null : Convert.ToInt32(item["HIGH"]);
                    }
                    if (item.ContainsKey("PRICE"))
                    {
                        objFlightpak.Price = string.IsNullOrEmpty(item["PRICE"]) ? (decimal?)null : Convert.ToDecimal(item["PRICE"]);
                    }
                    if (item.ContainsKey("NOTE"))
                    {
                        objFlightpak.NoteText = Convert.ToString(item["NOTE"]);
                    }
                    if (HaveRequiredColumnsFound(objFlightpak))
                    {
                        readTotalFile.Add(objFlightpak);
                        cnt++;
                    }
                }
            }
            return readTotalFile;
        }
        public static string ReadLinesFromCSVData(string csvfiledata, int numberoflines)
        {
            StringBuilder dataReader = new StringBuilder();
            string line;
            int linecounter = 0;
            if (!string.IsNullOrEmpty(csvfiledata))
            {
                using (StringReader sr = new StringReader(csvfiledata))
                {
                    while ((line = sr.ReadLine()) != null && linecounter != numberoflines)
                    {
                        if (!string.IsNullOrEmpty(line))
                        {
                            dataReader.AppendLine(line);
                            linecounter++;
                        }
                    }
                }
            }
            return dataReader.ToString();
        }
        public static string GetCsvDataAsJson(string csvfiledata, int numberofrecords)
        {
            var masterList = new List<List<string>>();
            var fileReadRecords = Utility.FetchPreviewRecordsInSession(csvfiledata, numberofrecords);
            Regex removeWhitespacePattern = new Regex(@"\s+");
            string[] headers = fileReadRecords[0].ConvertAll(a => removeWhitespacePattern.Replace(a.ToLower().Trim(), "").Replace('/', '_')).ToArray();
            fileReadRecords.RemoveAt(0);
            masterList.Add(headers.ToList());
            foreach (var csvRec in fileReadRecords)
            {
                var rowList = new List<string>();
                for (int i = 0; i < headers.Count(); i++)
                {
                    rowList.Add(csvRec[i]);
                }
                masterList.Add(rowList);
            }
            return JsonConvert.SerializeObject(masterList);
        }
        public static bool HaveRequiredColumnsFound(FlightpakFuelModal oFlightpakFuelModal)
        {
            if ((!string.IsNullOrEmpty(oFlightpakFuelModal.FBO) || !string.IsNullOrEmpty(oFlightpakFuelModal.Vendor))
                && (!string.IsNullOrEmpty(oFlightpakFuelModal.ICAO) || !string.IsNullOrEmpty(oFlightpakFuelModal.IATA))
                && oFlightpakFuelModal.Price != null && oFlightpakFuelModal.Price > -1
                && oFlightpakFuelModal.EffectiveDate != null
                && oFlightpakFuelModal.GallonFrom != null && oFlightpakFuelModal.GallonFrom > -1
                && oFlightpakFuelModal.GallonTo != null && oFlightpakFuelModal.GallonTo > -1)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}