﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.Admin;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.Admin.AdminService;
//For Tracing and Exceptioon Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Admin
{
    public partial class CountryCatalog : BasePage  
    {
        private bool IsEmptyCheck = true;
        private bool CountryCatalogPageNavigated = false;
        private string strCountryId = "";
        private ExceptionManager exManager;
        protected void Page_Load(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // Grid Control could be ajaxified when the page is initially loaded.

                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCountry, dgCountry, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCountry.ClientID));
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);                    
                   
                }
            }

        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            //Handle methods throguh exception manager with return flag
            if (BindDataSwitch)
            {
                dgCountry.Rebind();
            }
            if (dgCountry.MasterTableView.Items.Count > 0)
            {
                dgCountry.SelectedIndexes.Add(0);
                Session["SelectedCountryID"] = dgCountry.Items[0].GetDataKeyValue("CountryID").ToString();
                ReadOnlyForm();
            }
            else
            {
                ClearForm();
                EnableForm(false);
            }
            // GridEnable(true, true, true);

        }
        /// <summary>
        /// To select the selected item
        /// </summary>
        private void SelectItem()
        {
            if (Session["SelectedCountryID"] != null)
            {
                //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                string ID = Session["SelectedCountryID"].ToString();
                foreach (GridDataItem item in dgCountry.MasterTableView.Items)
                {
                    if (item.GetDataKeyValue("CountryID").ToString().Trim() == ID)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            CountryCatalogPageNavigated = true;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_PreRender(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (CountryCatalogPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    //throw ex;
                    
                }
            }


        }
        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_ItemCreated(object sender, GridItemEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton lbtnEditButton = ((e.Item as GridCommandItem).FindControl("lbtnInitEdit") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnEditButton, divExternalForm, RadAjaxLoadingPanel1);
                            LinkButton lbtnInsertButton = ((e.Item as GridCommandItem).FindControl("lbtnInitInsert") as LinkButton);
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(lbtnInsertButton, divExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                   // throw ex;
                }
            }


        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }
            }
        }
        /// <summary>
        /// Bind Country Catalog Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_BindData(object sender, GridNeedDataSourceEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        using (AdminService.AdminServiceClient AdminService = new AdminService.AdminServiceClient())
                        {
                            var objCountryVal = AdminService.GetCountryMasterList();
                            if (objCountryVal.ReturnFlag == true)
                            {
                                dgCountry.DataSource = objCountryVal.EntityList;
                            }
                            Session["CountryCodes"] = objCountryVal.EntityList.ToList();
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    //throw ex;
                }
            }

        }
        /// <summary>
        /// Item Command for Country Catalog Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_ItemCommand(object sender, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedCountryID"] != null)
                                {
                                    DisplayEditForm();
                                    //GridEnable(false, true, false);
                                    tbDescription.Focus();
                                    SelectItem();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                DisplayInsertForm();
                                tbCode.Focus();
                                //GridEnable(true, false, false);
                                break;
                            case "Filter":
                                foreach (GridColumn Column in e.Item.OwnerTableView.Columns)
                                {
                                    Column.CurrentFilterValue = string.Empty;
                                    Column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                   // throw ex;
                }
            }

        }
        /// <summary>
        /// Update Command for Country Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_UpdateCommand(object source, GridCommandEventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        e.Canceled = true;

                        //Handle methods throguh exception manager with return flag
                        e.Canceled = true;
                        if (Session["SelectedCountryID"] != null)
                        {
                            using (AdminService.AdminServiceClient objCountryService = new AdminService.AdminServiceClient())
                            {
                                var RetVal = objCountryService.UpdateCountryMaster(GetItems());
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                                if (RetVal.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    //throw ex;
                }
            }


        }
        /// <summary>
        /// Function to Check if Country Type Code Already Exists
        /// </summary>
        /// <returns></returns>
        private Boolean CheckAllReadyExist()
        {
            bool ReturnFlag = false;
            List<Country> CountryCodeList = new List<Country>();
            CountryCodeList = ((List<Country>)Session["CountryCodes"]).Where(x => x.CountryCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<Country>();
            if (CountryCodeList.Count != 0)
            {
                cvCode.IsValid = false;
                tbCode.Focus();
                ReturnFlag = true;
            }
            return ReturnFlag;
        }
        /// <summary>
        /// To enable the items in the grid
        /// </summary>
        /// <param name="Add"></param>
        /// <param name="Edit"></param>
        /// <param name="Delete"></param>
        ///Commenting this method Since there is no Add,Edit ,Delete functionality for this catalog.Always view.
        private void GridEnable(bool Add, bool Edit, bool Delete)
        {
            LinkButton lbtnInsertCtl = (LinkButton)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitInsert");
            LinkButton lbtnDelCtl = (LinkButton)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnDelete");
            LinkButton lbtnEditCtl = (LinkButton)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbtnInitEdit");
            if (Add)
            {
                lbtnInsertCtl.Enabled = true;
            }
            else
            {
                lbtnInsertCtl.Enabled = false;
            }
            if (Edit)
            {
                lbtnEditCtl.Enabled = true;
                lbtnEditCtl.OnClientClick = "javascript:return ProcessUpdate();";
            }
            else
            {
                lbtnEditCtl.Enabled = false;
                lbtnEditCtl.OnClientClick = string.Empty;
            }
            if (Delete)
            {
                lbtnDelCtl.Enabled = true;
                lbtnDelCtl.OnClientClick = "javascript:return ProcessDelete();";
            }
            else
            {
                lbtnDelCtl.Enabled = false;
                lbtnDelCtl.OnClientClick = string.Empty;
            }
        }
        /// <summary>
        /// To Display the read only form
        /// </summary>
        protected void ReadOnlyForm()
        {
            GridDataItem Item = dgCountry.SelectedItems[0] as GridDataItem;
            tbCode.Text = Convert.ToString(Item.GetDataKeyValue("CountryCD"));//.ToString();
            if (Item.GetDataKeyValue("CountryName") != null)
            {
                tbDescription.Text = Item.GetDataKeyValue("CountryName").ToString();
            }
            else
            {
                tbDescription.Text = string.Empty;
            }
            if (Item.GetDataKeyValue("ISOCD") != null)
            {
                tbIsoCode.Text = Item.GetDataKeyValue("ISOCD").ToString();
            }
            else
            {
                tbIsoCode.Text = string.Empty;
            }
            Label lbLastUpdatedUser = (Label)dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
            if (Item.GetDataKeyValue("LastUpdUID") != null)
            {
                // lbLastUpdatedUser.Text = "Last Updated User: " + Item.GetDataKeyValue("LastUpdUID").ToString();
            }
            else
            {
                lbLastUpdatedUser.Text = string.Empty;
            }
            if (Item.GetDataKeyValue("LastUpdDT") != null)
            {//sujitha
                 lbLastUpdatedUser.Text = lbLastUpdatedUser.Text + " Date: " + Item.GetDataKeyValue("LastUpdDT").ToString();
            }
            Item.Selected = true;
            EnableForm(false);
        }
        /// <summary>
        ///Enable the controls in the form
        /// </summary>
        /// <param name="Enable"></param>
        protected void EnableForm(bool Enable)
        {
            if (hdnSave.Value == "Update" && Enable == true)
            {
                tbCode.Enabled = false;
            }
            else
            {
                tbCode.Enabled = Enable;
            }
            tbDescription.Enabled = Enable;
            tbIsoCode.Enabled = false;
            btnCancel.Visible = Enable;
            btnSaveChanges.Visible = Enable;
        }
        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_DeleteCommand(object source, GridCommandEventArgs e)
        {



            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag

                        using (AdminService.AdminServiceClient CountryService = new AdminService.AdminServiceClient())
                        {
                            AdminService.Country CountryData = new AdminService.Country();
                            string Code = Session["SelectedCountryID"].ToString().Trim();
                            string CountryID = "";
                            foreach (GridDataItem Item in dgCountry.MasterTableView.Items)
                            {
                                if (Item["CountryID"].Text.Trim() == Code)
                                {
                                    CountryID = Item["CountryID"].Text.Trim().Replace("&nbsp;", "");
                                    break;
                                }
                            }
                            CountryData.CountryID = Convert.ToInt64(CountryID);

                            CountryData.IsDeleted = true;
                            CountryService.DeleteCountryMaster(CountryData);
                            e.Item.OwnerTableView.Rebind();
                            e.Item.Selected = true;
                            ClearForm();
                            DefaultSelection(true);
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                   // throw ex;
                }
            }



        }
        /// <summary>
        /// Update Command for Country Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCountry_InsertCommand(object source, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (AdminService.AdminServiceClient objCountryService = new AdminService.AdminServiceClient())
                            {
                                var RetVal = objCountryService.AddCountryMaster(GetItems());
                                dgCountry.Rebind();
                                DefaultSelection(false);
                                if (RetVal.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    //throw ex;
                }
            }


        }
        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCountry_SelectedIndexChanged(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        if (btnSaveChanges.Visible == false)
                        {
                            GridDataItem item = dgCountry.SelectedItems[0] as GridDataItem;
                            Session["SelectedCountryID"] = item.GetDataKeyValue("CountryID");
                            if (btnSaveChanges.Visible == false)
                            {
                                ReadOnlyForm();
                            }
                        }
                        //  GridEnable(true, true, true);

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    //throw ex;
                }
            }

        }
        /// <summary>
        /// Clear the Form
        /// </summary>
        protected void ClearForm()
        {
            tbCode.Text = string.Empty;
            tbDescription.Text = string.Empty;
            tbIsoCode.Text = string.Empty;
        }
        /// <summary>
        /// Display Insert  form when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            hdnSave.Value = "Save";
            tbCode.Text = string.Empty;
            tbDescription.Text = string.Empty;
            ClearForm();
            dgCountry.Rebind();
            EnableForm(true);
        }
        /// <summary>
        /// Command Event Trigger for Save or Update Country Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (IsEmptyCheck)
                        {
                            if (hdnSave.Value == "Update")
                            {
                                (dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                            }
                            else
                            {
                                (dgCountry.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.Country);
                    //throw ex;
                }
            }


        }
        /// <summary>
        /// Cancel Country Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {

            //Handle methods throguh exception manager with return flag
            DefaultSelection(false);

        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {

            //Handle methods throguh exception manager with return flag
            if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
            {
                e.Updated = dgCountry;
            }

        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private Country GetItems()
        {
            AdminService.Country CountryType = new AdminService.Country();
            CountryType.CountryName = tbDescription.Text;
            CountryType.CountryCD = tbCode.Text;
            CountryType.LastUpdDT = DateTime.UtcNow;
            string Code = Session["SelectedCountryID"].ToString();
            if (hdnSave.Value == "Update")
            {
                foreach (GridDataItem Item in dgCountry.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("CountryID").ToString().Trim() == Code.Trim())
                    {
                        if (Item.GetDataKeyValue("CountryID") != null)
                        {
                            CountryType.CountryID = Convert.ToInt64(Item.GetDataKeyValue("CountryID"));
                        }
                        break;
                    }
                }
            }
            else
            {
                CountryType.CountryID = 0;
            }
            CountryType.IsDeleted = false;
            //CountryType.UPDTDATE = System.DateTime.Now;
            return CountryType;
        }
        /// <summary>
        /// To display the edit form
        /// </summary>
        protected void DisplayEditForm()
        {
            if (Session["SelectedCountryID"] != null)
            {
                strCountryId = "";
                foreach (GridDataItem Item in dgCountry.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("CountryID").ToString().Trim() == Session["SelectedCountryID"].ToString().Trim())
                    {
                        if ((Item).GetDataKeyValue("CountryCD") != null)
                        {
                            tbCode.Text = (Item).GetDataKeyValue("CountryCD").ToString();
                        }
                        if ((Item).GetDataKeyValue("CountryName") != null)
                        {
                            tbDescription.Text = (Item).GetDataKeyValue("CountryName").ToString();
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }
                        if ((Item).GetDataKeyValue("ISOCD") != null)
                        {
                            tbIsoCode.Text = (Item).GetDataKeyValue("ISOCD").ToString();
                        }
                        else
                        {
                            tbIsoCode.Text = string.Empty;
                        }
                        break;
                    }
                }
                EnableForm(true);
            }
        }
        /// <summary>
        /// To check unique Code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Code_TextChanged(object sender, EventArgs e)
        {

            //Handle methods throguh exception manager with return flag
            if (tbCode.Text != null && tbCode.Text.Trim() != string.Empty)
            {
                using (AdminService.AdminServiceClient Service = new AdminService.AdminServiceClient())
                {
                    var RetVal = Service.GetCountryMasterList().EntityList.Where(x => x.CountryCD.Trim().ToUpper() == (tbCode.Text.ToString().ToUpper().Trim()));
                    if (RetVal.Count() > 0 && RetVal != null)
                    {
                        cvCode.IsValid = false;
                        tbCode.Focus();
                    }
                }
            }

        }
    }
}
