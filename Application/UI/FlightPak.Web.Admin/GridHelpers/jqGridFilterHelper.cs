﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using FlightPak.Web.Admin.GridHelpers.CoreApiFormatters;

namespace FlightPak.Web.Admin.GridHelpers
{
    public class jqGridFilterHelper
    {
        public static List<Filter> GetCoreApiCompatibleFilters(String filters)
        {
            List<Filter> filtersList = new List<Filter>();
            if (!String.IsNullOrEmpty(filters))
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                jqGridSearchFilter searchFilter = serializer.Deserialize<jqGridSearchFilter>(filters);

                foreach (jqGridSearchFilterItem item in searchFilter.rules)
                {
                    Filter searchItem = new Filter();
                    switch (item.op)
                    {
                        case "eq":
                            searchItem.Op = Operator.Eq;
                            break;
                        case "cn":
                            searchItem.Op = Operator.Li;
                            break;
                        case "bw":
                            searchItem.Op = Operator.Sw;
                            break;
                        case "ew":
                            searchItem.Op = Operator.Ew;
                            break;
                        case "ne":
                            searchItem.Op = Operator.Ne;
                            break;
                        case "lt":
                            searchItem.Op = Operator.Lt;
                            break;
                        case "le":
                            searchItem.Op = Operator.Lte;
                            break;
                        case "gt":
                            searchItem.Op = Operator.Gt;
                            break;
                        case "ge":
                            searchItem.Op = Operator.Gte;
                            break;
                        default:
                            searchItem.Op = Operator.Sw;
                            break;
                    }
                    searchItem.Field = item.field;
                    searchItem.Value = item.data;

                    filtersList.Add(searchItem);
                }
            }
            return filtersList;
        }
    }
}