﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Admin.GridHelpers.CoreApiFormatters
{
    public enum FieldType
    {
        StringType = 1,
        NumericType = 2
    }
}