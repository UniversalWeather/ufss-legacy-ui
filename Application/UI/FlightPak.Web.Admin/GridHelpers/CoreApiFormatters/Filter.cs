﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Admin.GridHelpers.CoreApiFormatters
{
    public class Filter
    {
        public string Field { get; set; }
        public string Value { get; set; }
        public Operator Op { get; set; }
        public FieldType? Type { get; set; }
    }
}