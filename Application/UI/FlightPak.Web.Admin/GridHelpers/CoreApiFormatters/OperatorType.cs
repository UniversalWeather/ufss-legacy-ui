﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Web.Admin.GridHelpers.CoreApiFormatters
{
    public enum Operator
    {

        Eq = 1, // =

        Gt = 2, //>

        Lt = 3, //<

        Lte = 4, //<=

        Gte = 5, //>=

        Ne = 6, //<>

        Li = 7, //LIke

        Sw = 8, //Starts with

        Ew = 9 //Ends with
    }
}