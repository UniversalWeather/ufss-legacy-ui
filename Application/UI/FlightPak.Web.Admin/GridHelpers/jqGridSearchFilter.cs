﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightPak.Web.Admin.GridHelpers
{
    public class jqGridSearchFilter
    {
        public string groupOp { get; set; }
        public List<jqGridSearchFilterItem> rules { get; set; }
    }
}
