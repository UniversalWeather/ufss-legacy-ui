﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;

namespace FlightPak.Web.Admin.GridHelpers
{
    public class PagingMetaData
    {
        [DataMember(Name = "total_items")]
        [JsonProperty(PropertyName = "total_items")]
        public int total_items { get; set; }
        [DataMember(Name = "size")]
        [JsonProperty(PropertyName = "size")]
        public int Size { get; set; }
        [DataMember(Name = "page ")]
        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }
        [DataMember(Name = "page_count")]
        [JsonProperty(PropertyName = "page_count")]
        public int page_count { get; set; }
        [DataMember(Name = "next_page")]
        [JsonProperty(PropertyName = "next_page")]
        public string NextPage { get; set; }
        [DataMember(Name = "previous_page")]
        [JsonProperty(PropertyName = "previous_page")]
        public string PreviousPage { get; set; }
    }
}