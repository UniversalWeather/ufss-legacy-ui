﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace FlightPak.Web.Admin.GridHelpers
{
    public class FSSOperationResult<T> where T : new()
    {
        public FSSOperationResult()
        {
            Result = new T();
            ResultList = new List<T>();
            ErrorsList = new List<string>();
        }
        public bool Success { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public List<String> ErrorsList { get; set; }
        public T Result { get; set; }
        public List<T> ResultList { get; set; }

        
    }
}