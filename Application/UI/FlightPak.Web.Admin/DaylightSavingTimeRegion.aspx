﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DaylightSavingTimeRegion.aspx.cs"
    Inherits="FlightPak.Web.Admin.DaylightSavingTimeRegion" MasterPageFile="~/Site.Master" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<%@ Register TagPrefix="uc" TagName="DatePicker" Src="~/UserControls/UCDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="Scripts/Common.js">
    </script>
    <script type="text/javascript">
        function validateHourAndMinute(oSrc, args) {
            var array = args.Value.split(":");
            if (array[0] > 23 && array[1] > 59) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23; Minute entry must be 0-59";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23; Minute entry must be 0-59";
                }
                if (oSrc.id.indexOf("cvStartTime") != -1) {
                    setTimeout("document.getElementById('<%=rmtbStartTime.ClientID%>').focus()", 0);
                }
                else if (oSrc.id.indexOf("cvFinishTime") != -1) {
                    setTimeout("document.getElementById('<%=rmtbFinishTime.ClientID%>').focus()", 0);
                }
                return false;
            }
            else if ((array[0] > 23)) {
                args.IsValid = false;
                if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                    oSrc.textContent = "Hour entry must be 0-23";
                } else {
                    oSrc.innerText = "Hour entry must be 0-23";
                }
                if (oSrc.id.indexOf("cvStartTime") != -1) {
                    setTimeout("document.getElementById('<%=rmtbStartTime.ClientID%>').focus()", 0);
                }
                else if (oSrc.id.indexOf("cvFinishTime") != -1) {
                    setTimeout("document.getElementById('<%=rmtbFinishTime.ClientID%>').focus()", 0);
                }
                return false;
            }
            else {
                if (array[1] > 59) {
                    args.IsValid = false;
                    if ((browserName().toLowerCase() == "firefox") || (browserName().toLowerCase() == "mozilla")) {
                        oSrc.textContent = "Minute entry must be 0-59";
                    } else {
                        oSrc.innerText = "Minute entry must be 0-59";
                    }
                    if (oSrc.id.indexOf("cvStartTime") != -1) {
                        setTimeout("document.getElementById('<%=rmtbStartTime.ClientID%>').focus()", 0);
                    }
                    else if (oSrc.id.indexOf("cvFinishTime") != -1) {
                        setTimeout("document.getElementById('<%=rmtbFinishTime.ClientID%>').focus()", 0);
                    }
                    return false;
                }
            }
        }
 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="art-setting-customersupport">
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgDaylightSavingTimeRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="clrFilters" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgDaylightSavingTimeRegion">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgDaylightSavingTimeRegion" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" />
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div id="globalNav_admn">
                        <span class="head-title-nav">DST Region</span>
                    </div>
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="nav-space">
                    </div>
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr style="display: none;">
                <td>
                    <uc:DatePicker ID="ucDatePicker" runat="server"></uc:DatePicker>
                </td>
            </tr>
        </table>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgDaylightSavingTimeRegion" runat="server" AllowSorting="true"
                        Height="348px" OnItemCreated="dgDaylightSavingTimeRegion_ItemCreated" Visible="true"
                        OnNeedDataSource="dgDaylightSavingTimeRegion_BindData" OnItemCommand="dgDaylightSavingTimeRegion_ItemCommand"
                        OnUpdateCommand="dgDaylightSavingTimeRegion_UpdateCommand" OnInsertCommand="dgDaylightSavingTimeRegion_InsertCommand"
                        OnDeleteCommand="dgDaylightSavingTimeRegion_DeleteCommand" AutoGenerateColumns="false"
                        PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgDaylightSavingTimeRegion_SelectedIndexChanged"
                        AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" OnPreRender="dgDaylightSavingTimeRegion_PreRender"
                        OnPageIndexChanged="dgDaylightSavingTimeRegion_PageIndexChanged">
                        <MasterTableView DataKeyNames="DSTRegionID,DSTRegionCD,DSTRegionName,StartDT,EndDT,OffSet,CityList,LastUpdUID,LastUpdTS"
                            CommandItemDisplay="Bottom">
                            <Columns>
                                <telerik:GridBoundColumn DataField="DSTRegionCD" HeaderText="DST Code" AllowFiltering="true"
                                    HeaderStyle-Width="230px" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DSTRegionName" HeaderText="Description" AllowFiltering="true"
                                    HeaderStyle-Width="400px" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="true">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CityList" HeaderText="City" AllowFiltering="true"
                                    HeaderStyle-Width="292px" CurrentFilterFunction="Contains" ShowFilterIcon="false"
                                    AutoPostBackOnFilter="true">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <CommandItemTemplate>
                                <div style="padding: 5px 5px; float: left;">
                                    <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                    <asp:LinkButton ID="lnkInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                                        ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt=""   src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                        runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;"  alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                </div>
                                <div>
                                    <asp:Label runat="server" ID="lbLastUpdatedUser" CssClass="last-updated-text"></asp:Label>
                                </div>
                            </CommandItemTemplate>
                        </MasterTableView>
                        <ClientSettings EnablePostBackOnRowClick="true">
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            <Selecting AllowRowSelect="true" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <div id="DivExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="True">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table class="border-box">
                    <tr>
                        <td valign="top">
                            <span class="mnd_text">DST Code</span>
                        </td>
                        <td class="tdLabel200" valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbCode" runat="server" MaxLength="3" CssClass="text50" OnTextChanged="tbCode_textchange"
                                            ValidationGroup="Save" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                                        Display="Dynamic" CssClass="alert-text" SetFocusOnError="true" ErrorMessage="Code is Required"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvDstCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique DST Code is Required"
                                                        Display="Dynamic" CssClass="alert-text" ValidationGroup="Save" SetFocusOnError="true"></asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdLabel130" valign="top">
                            <span class="mnd_text">Description</span>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbDescription" runat="server" MaxLength="40" CssClass="text180"
                                            ValidationGroup="Save"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                            ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true"
                                            ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tdLabel60">
                            <asp:Label ID="lbStart" runat="server" Text="Start"></asp:Label>
                        </td>
                        <td valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="padright_5" valign="top">
                                        <uc:DatePicker ID="ucStartDate" runat="server" />
                                    </td>
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <telerik:RadMaskedTextBox ID="rmtbStartTime" runat="server" SelectionOnFocus="SelectAll"
                                                        ValidationGroup="save" Mask="<0..99>:<0..99>" CssClass="RadMaskedTextBox50">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvStartTime" runat="server" ValidationGroup="Save" ControlToValidate="rmtbStartTime"
                                                        CssClass="alert-text" ClientValidationFunction="validateHourAndMinute">
                                                    </asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tdLabel60" valign="top">
                            <asp:Label ID="lbEnd" runat="server" Text="End"></asp:Label>
                        </td>
                        <td valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="padright_5" valign="top">
                                        <uc:DatePicker ID="ucFinishedDate" runat="server" />
                                    </td>
                                    <td valign="top">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <telerik:RadMaskedTextBox ID="rmtbFinishTime" runat="server" SelectionOnFocus="SelectAll"
                                                        ValidationGroup="save" Mask="<0..99>:<0..99>" CssClass="RadMaskedTextBox50">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CustomValidator ID="cvFinishTime" runat="server" ValidationGroup="Save" ControlToValidate="rmtbFinishTime"
                                                        CssClass="alert-text" ClientValidationFunction="validateHourAndMinute">
                                                    </asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel130" valign="top">
                            <asp:Label ID="lbUTCOffset" runat="server" Text="UTC Offset"></asp:Label>
                        </td>
                        <td>
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbutcoffset" runat="server" MaxLength="6" CssClass="text60" Text="00.00"
                                            onKeyPress="return fnAllowNumericAndChar(this, event,'.+-')">
                                        </asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RangeValidator ID="rvUTC" runat="server" ControlToValidate="tbutcoffset" ValidationGroup="Save"
                                            CssClass="alert-text" Text="Values should be between -14 to 14" Display="Dynamic"
                                            Type="Double" MaximumValue="14" MinimumValue="-14"></asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%--<asp:RegularExpressionValidator ID="revutcoffset" runat="server" Display="Dynamic"
                                            ErrorMessage="Invalid Format" ControlToValidate="tbutcoffset" CssClass="alert-text"
                                            ValidationExpression="^[-+]?\d+(\.\d\d)$" ValidationGroup="save"></asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLabel130" valign="top">
                            <asp:Label ID="lbCityList" runat="server" Text="City List"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="tbCityList" runat="server" MaxLength="60" CssClass="text180"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveChanges" Text="Save" runat="server" OnClick="SaveChanges_Click"
                                ValidationGroup="Save" CssClass="button" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" runat="server" OnClick="Cancel_Click" CssClass="button"
                                CausesValidation="false" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                            <asp:HiddenField ID="hdnStartDate" runat="server" />
                            <asp:HiddenField ID="hdnFinishedDate" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
