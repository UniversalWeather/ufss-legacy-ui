﻿using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.Admin.AdminService;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Web.Admin
{
    public partial class FuelVendor : BasePage
    {
        #region "Variable Declaration"
        private ExceptionManager _exManager;
        #endregion

        #region "Page Event"
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsPopUp)
                {
                    if (IsAdd)
                    {
                        InitInsertCommand();
                    } else
                    {
                        EditCommand();
                    }
                }
            }
        }
        #endregion

        #region "Property"
        public bool IsPopUp
        {
            get
            {
                if (Request.QueryString["IsPopup"] == string.Empty || Request.QueryString["IsPopup"] == "Add" || Request.QueryString["IsPopup"] == "View")
                    return true;
                return false;
            }
        }
        public bool IsAdd
        {
            get
            {
                if (Request.QueryString["IsPopup"] == "Add")
                    return true;
                return false;
            }
        }
        #endregion

        #region "Private Function"
        private void InitInsertCommand()
        {
            DisplayInsertForm();
        }
        private void EditCommand()
        {
            if (Request.QueryString["VendorId"] != null)
            {
                Session["FuelVendorID"] = Convert.ToInt64(Request.QueryString["VendorId"]);
                DisplayEditForm();
            }
        }
        private void DisplayEditForm()
        {
            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                //ddlCode.Enabled = false;
                hdnSave.Value = "Update";
                hdnRedirect.Value = "";
                LoadControlData();
                EnableForm(true);
                tbCode.Enabled = false;
            }
        }
        private void LoadControlData()
        {
            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                if (Session["FuelVendorID"] != null)
                {
                    using (var client = new AdminServiceClient())
                    {
                        long fuelVendorId = Convert.ToInt64(Session["FuelVendorID"].ToString().Trim());
                        var retFuelVendorInfo = client.GetFuelVendorInfoByFilter(fuelVendorId, "");
                        if (retFuelVendorInfo.ReturnFlag && retFuelVendorInfo.EntityInfo != null)
                        {
                            var fuelVendorInfo = retFuelVendorInfo.EntityInfo;
                            tbCode.Text = fuelVendorInfo.VendorCD.ToUpper();
                            hdnFuelVendorID.Value = fuelVendorInfo.FuelVendorID.ToString();
                            tbName.Text = fuelVendorInfo.VendorName;
                            if (fuelVendorInfo.VendorDescription != null)
                            {
                                tbDescription.Text = fuelVendorInfo.VendorDescription;
                            }
                            if (fuelVendorInfo.ServerInfo != null)
                            {
                                tbServerInformation.Text = fuelVendorInfo.ServerInfo;
                            }
                            if (fuelVendorInfo.FileLocation != null)
                            {
                                tbfilelocation.Text = fuelVendorInfo.FileLocation;
                            }
                            chkInactive.Checked = fuelVendorInfo.IsInActive != null && Convert.ToBoolean(fuelVendorInfo.IsInActive.ToString(), CultureInfo.CurrentCulture);
                        }
                    }
                }
            }
        }
        private void EnableForm(bool enable)
        {
            using (Common.Tracing.Tracer.NewTraceInputs(enable))
            {
                tbCode.Enabled = enable;
                if (IsPopUp)
                {
                    tbDescription.Enabled = enable;
                    tbName.Enabled = enable;
                }
                else
                {
                    tbDescription.Enabled = false;
                    tbName.Enabled = false;
                }
                tbfilelocation.Enabled = enable;
                tbServerInformation.Enabled = enable;
                btnSave.Visible = enable;
                btnCancel.Visible = enable;
                chkInactive.Enabled = enable;
            }
        }
        private void SaveUpdatedReocrd()
        {
            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                            if ((Session["FuelVendorID"] != null))
                            {
                                using (var fuelVendorService = new AdminServiceClient())
                                {
                                    var oFuelVendor = new AdminService.FuelVendor();
                                    oFuelVendor = GetItems(oFuelVendor);
                                    ReturnValueOfFuelVendor result = hdnSave.Value == "Save" ? fuelVendorService.AddFuelVendor(oFuelVendor) : fuelVendorService.UpdateFuelVendor(oFuelVendor);
                                    if (result.ReturnFlag )
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "Close Browser Window", "CloseAdd_UpdateFuelVendorRadWindow();", true);
                                    }
                                    else
                                    {
                                        ProcessErrorMessage(result.ErrorMessage, ModuleNameConstants.Database.FuelVendor);
                                    }
                                }
                            }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        private void SaveInsertedRecords()
        {
            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                try
                {
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        bool isValidate = true;
                        if (CheckAlreadyExist())
                        {
                            cvCode.IsValid = false;
                            isValidate = false;
                        }
                        if (tbCode.Text.Trim() == "")
                        {
                            rfvCode.IsValid = false;
                            isValidate = false;
                        }
                        if (tbName.Text.Trim() == "")
                        {
                            rfvName.IsValid = false;
                            isValidate = false;
                        }
                        if (tbDescription.Text.Trim() == "")
                        {
                            rfvDescription.IsValid = false;
                            isValidate = false;
                        }
                        if (isValidate)
                        {
                            using (var client = new AdminServiceClient())
                            {
                                var oFuelVendor = new AdminService.FuelVendor();
                                oFuelVendor = GetItems(oFuelVendor);
                                ReturnValueOfFuelVendor result = hdnSave.Value == "Save" ? client.AddFuelVendor(oFuelVendor) : client.UpdateFuelVendor(oFuelVendor);
                                if (result.ReturnFlag)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "Close Browser Window", "CloseAdd_UpdateFuelVendorRadWindow();", true);
                                }
                                else
                                {
                                    ProcessErrorMessage(result.ErrorMessage, ModuleNameConstants.Database.FuelVendor);
                                }
                            }
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        protected void DisplayInsertForm()
        {
            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                hdnSave.Value = "Save";
                tbCode.Enabled = true;
                ClearForm();
                EnableForm(true);
            }
        }
        protected void ClearForm()
        {
            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                tbCode.Text = string.Empty;
                hdnFuelVendorID.Value = string.Empty;
                tbDescription.Text = string.Empty;
                tbfilelocation.Text = string.Empty;
                tbName.Text = string.Empty;
                tbServerInformation.Text = string.Empty;
                chkInactive.Checked = false;
            }
        }
        private bool CheckAlreadyExist()
        {

            using (Common.Tracing.Tracer.NewTraceInputs())
            {
                bool returnVal = false;
                //Handle methods throguh exception manager with return flag
                _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                _exManager.Process(() =>
                {

                    using (var objservice = new AdminServiceClient())
                    {
                        var objRetVal = objservice.GetFuelVendor().EntityList.Where(x => x.VendorCD.Trim().ToUpper().Equals(tbCode.Text.ToString().Trim().ToUpper()));
                        if (objRetVal.Any())
                        {
                            cvCode.IsValid = false;
                            tbCode.Focus();
                            returnVal = true;
                        }
                        else
                        {
                            tbName.Focus();
                        }
                    }
                }, Policy.UILayer);
                return returnVal;
            }
        }
        private AdminService.FuelVendor GetItems(AdminService.FuelVendor objFuelVendor)
        {
            using (Common.Tracing.Tracer.NewTraceInputs(objFuelVendor))
            {
                if (hdnSave.Value == "Update")
                {
                    if (!string.IsNullOrEmpty(hdnFuelVendorID.Value))
                    {
                        objFuelVendor.FuelVendorID = Convert.ToInt64(hdnFuelVendorID.Value);
                        Session["FuelVendorID"] = hdnFuelVendorID.Value;
                        Session["VendorName"] = tbName.Text;
                    }
                }
                objFuelVendor.VendorCD = tbCode.Text.Trim();
                if (!string.IsNullOrEmpty(tbName.Text))
                {
                    objFuelVendor.VendorName = tbName.Text;
                }
                if (!string.IsNullOrEmpty(tbDescription.Text))
                {
                    objFuelVendor.VendorDescription = tbDescription.Text;
                }
                if (!string.IsNullOrEmpty(tbServerInformation.Text))
                {
                    objFuelVendor.ServerInfo = tbServerInformation.Text;
                }
                if (hdnUploadedFileName.Value.Length > 0)
                {
                    objFuelVendor.BaseFileName = hdnUploadedFileName.Value.Trim();
                    objFuelVendor.FileLocation = "N/A";
                }
                else
                {
                    objFuelVendor.BaseFileName = "N/A";
                    objFuelVendor.FileLocation = "N/A";
                }

                objFuelVendor.FldSep = ",";
                objFuelVendor.IsHeaderRow = false;
                objFuelVendor.VendorDateFormat = "MM/DD/YYYY";
                objFuelVendor.IsDeleted = false;
                objFuelVendor.UWAUpdates = false;
                objFuelVendor.IsInActive = chkInactive.Checked;
                return objFuelVendor;
            }
        }
        #endregion

        #region "Control Events"
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            using (Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    _exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    _exManager.Process(() =>
                    {
                        if (IsPopUp)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Close Browser Window", "CloseAdd_UpdateFuelVendorRadWindow();", true);
                        }
                    }, Policy.UILayer);
                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.FuelVendor);
                }
            }
        }
        protected void btnSave_OnClick(object sender, EventArgs e)
        {
            if (hdnSave.Value == "Update")
            {
                SaveUpdatedReocrd();
            } else
            {
                SaveInsertedRecords();
            }
        }
        #endregion



    }
}