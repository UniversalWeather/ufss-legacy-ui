﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FuelVendorPopup.aspx.cs" Inherits="FlightPak.Web.Admin.FuelVendorPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Scripts/jqgrid/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" />
    <link href="/Scripts/jqgrid/jqgrid.min.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="/Scripts/jqgrid/jquery-1.11.0.min.js"></script>
    <script src="/Scripts/jqgrid/i18n/grid.locale-en.js" type="text/javascript"></script>
    <script type="text/javascript" src="/Scripts/jqgrid/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/Scripts/Common.js"></script>
    <style type="text/css">       
        body {
            background: none !important;
            height:100%;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">
    <div>
           <div class="jqgrid">
                            <div>
                                <table class="box1">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <div class="account_headingtop">
                                            <input type="checkbox" name="Active" value="Display Inactive" id="chkActiveOnly"/>
                                                Display Inactive
                                            <input id="btnSearch" class="button" value="Search" type="submit" name="btnSearch" onclick="reloadPageSize(); return false;" />
                                            </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table id="gridFuelVendor" style="width: 981px !important;" class="table table-striped table-hover table-bordered"></table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="grid_icon">
                                                <div role="group" id="pg_gridPager"></div>
                                                <a title="Add" class="add-icon-grid" href="#" id="recordAdd"></a>
                                                <a id="recordEdit" title="Edit" class="edit-icon-grid" href="#"></a>
                                                <a id="recordDelete" title="Delete" class="delete-icon-grid" href="#"></a>
                                                <span style="color: #555;font-size: 0.9em;font-weight: normal;left: 19px;margin-left: 0;margin-top: -25px;position: absolute;">Page Size:</span>
                                                <input style="font-size: 0.8em;height: 14px;left: 10px;margin-left: 65px;margin-top: -29px;position: absolute;width: 30px;" id="rowNum" type="text" value="20" maxlength="5" />
                                                <input id="btnChange" style="color: #555;cursor: pointer;font-size: 11px !important;height: 22px;left: 9px;margin-left: 107px;margin-top: -28px;position: absolute;text-align: center;width: 60px;-webkit-appearance: none;-moz-border-radius: 0px;-webkit-border-radius: 0px;border-radius: 0px;" 
                                                    value="Change" type="submit" name="btnChange" onclick="reloadPageSize(); return false;" />
                                            </div>
                                            <div style="padding: 5px 5px; text-align: right;">
                                                <input id="btnSubmit" class="button okButton" value="OK"  type="button"/>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
    </div>
    </form>
</body>
    <script type="text/javascript">
        var JQGridTableId = "#gridFuelVendor";
        var vendorId;
        var vendorCode;
        $(document).ready(function () {
            
            $("#btnSubmit").click(function () {
                var selr = $(JQGridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(JQGridTableId).getRowData(selr);
                if (rowData['FuelVendorID'] == undefined) {
                    showMessageBox('Please select a catering.', "Fuel Vendor");
                } else {
                    returnToParent(rowData);
                }
                return false;
            });

            jQuery(JQGridTableId).jqGrid({
                url: "/FuelVendorPopup.aspx/GetFuelVendorByFilter",
                mtype: "POST",
                datatype: "json",
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                serializeGridData: function (postData) {
                    if (postData._search == undefined || postData._search == false) {
                        if (postData.filters === undefined) postData.filters = null;
                    }
                    postData.isInActive = $("#chkActiveOnly").is(':checked') ? true : false;
                    return JSON.stringify(postData);
                },
               
                height: 250,
                width: 510,
                viewrecords: true,
                shrinkToFit: true,
                rowNum: $("#rowNum").val(),
                multiselect: false,
                pager: "#pg_gridPager",
                colNames: ['FuelVendorID', 'VendorCD', 'VendorName', 'VendorDescription', 'IsInActive', 'IsDeleted'],
                colModel: [
                    { name: 'FuelVendorID', index: 'FuelVendorID', key: true, hidden: true },
                    { name: 'VendorCD', index: 'VendorCD' },
                    { name: 'VendorName', index: 'VendorName' },
                    { name: 'VendorDescription', index: 'VendorDescription' },
                    { name: 'IsInActive', index: 'IsInActive', formatter: "checkbox", classes: "grid-checkbox", formatoptions: { disabled: true }, search: false },
                    { name: 'IsDeleted', index: 'IsDeleted', hidden: true }
                ],
                ondblClickRow: function (rowId) {
                    var rowData = jQuery(this).getRowData(rowId); 
                    returnToParent(rowData);
                },
                onSelectRow: function (id) {
                        
                },
            });
            $(JQGridTableId).jqGrid('filterToolbar', { defaultSearch: 'bw', searchOnEnter: false, stringResult: true });
            
            $("#recordAdd").click(function () {
                var widthDoc = $(document).width();
                popupwindow("/FuelVendor.aspx?IsPopup=Add", "Add Fuel Vendor", widthDoc + 200, 650, JQGridTableId, 'FuelVendorAddWindow', true);
                return false;
            });
            
            $("#recordEdit").click(function () {
                var selr = $(JQGridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(JQGridTableId).getRowData(selr);
                vendorId = rowData['FuelVendorID'];
                var widthDoc = $(document).width();
                if (vendorId != undefined && vendorId != '' && vendorId != 0) {
                    popupwindow("/FuelVendor.aspx?IsPopup=&VendorId=" + vendorId, "Edit Vendor", widthDoc + 200, 650, JQGridTableId, 'FuelVendorUpdateWindow', true);
                }
                else {
                    showMessageBox('Please select an Fuel Vendor.', "Fuel Vendor");
                }
                return false;
            });
            
            $("#recordDelete").click(function () {
                var selr = $(JQGridTableId).jqGrid('getGridParam', 'selrow');
                var rowData = $(JQGridTableId).getRowData(selr);
                vendorId = rowData['FuelVendorID'];
                vendorCode = rowData['VendorCD'];
                var widthDoc = $(document).width();

                if (IsNullOrEmpty(vendorId) == false && vendorId != 0) {
                    showConfirmPopup('Are you sure you want to delete this record?', deleteConfirmCallBackFn, 'Confirmation');
                }
                else {
                    showMessageBox('Please select an Fuel Vendor.', "Fuel Vendor");
                }
                return false;
            });
            
            function deleteConfirmCallBackFn(arg) {
                if (arg) {
                    $.ajax({
                        async: true,
                        type: 'POST',
                        url: '/FuelVendorPopup.aspx/DeleteFuelVendor',
                        data: JSON.stringify({ "fuelVendorId": vendorId, "fuelVendorCode": vendorCode }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            response = response.d;
                            vendorId = "";
                            vendorCode = "";
                            if (response.StatusCode == 200 && response.Result.ResponseCode==1) {
                                OnClientCloseFuelVendorAddWindow();
                            } else if (response.StatusCode == 200 && response.Result.ResponseCode == 2) {
                                showMessageBox("Not able to delete. Please try agine !", "Fuel Vendor");
                            } else if (response.StatusCode == 200 && response.Result.ResponseCode == 3) {
                                showMessageBox(response.Result.LockMessage, "Fuel Vendor");
                            }else if (response.StatusCode == 500 && response.ErrorsList.length > 0) {
                                showMessageBox(response.ErrorsList[0], "Fuel File XSLT Generator");
                            }
                        }
                    });
                }
            }
        });
        function reloadPageSize() {
            var myGrid = $(JQGridTableId);
            var currentValue = $("#rowNum").val();
            myGrid.setGridParam({ rowNum: currentValue });
            myGrid.trigger('reloadGrid');
        }
        function OnClientCloseFuelVendorAddWindow() {
            $(JQGridTableId).trigger('reloadGrid');
        }
        
        function returnToParent(rowData) {
            var oArg = new Object();
            oArg.FuelVendorID = rowData["FuelVendorID"];
            oArg.VendorCD = rowData["VendorCD"];
            oArg.VendorName = rowData["VendorName"];
            oArg.VendorDescription = rowData["VendorDescription"];

            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }
        
        function popupwindow(url, title, w, h, grid) {
            var oWindow = GetRadWindow();
            openSizedWindow(url, oWindow, w, h, grid);
            return true;
        }
        function openSizedWindow(page, radwindow, w, h, grid, centerPopup) {
            var parentPage = GetRadWindowPopup().BrowserWindow;
            var parentRadWindowManager = parentPage.GetRadWindowManager();
            var oWnd2 = parentRadWindowManager.open(page, radwindow);
            oWnd2.setSize(w, h);
            try { oWnd2.add_pageLoad(OnPageLoadComplete); } catch (e) { }
            oWnd2.argument = grid;

            try { oWnd2.add_close(OnPageClosed); } catch (e) { }
            oWnd2.set_modal(true);
            oWnd2.set_visibleStatusbar(true);
            try { oWnd2.moveTo(oWnd2.center(), 0); } catch (e) { }
            oWnd2.set_status('');
            if (centerPopup) {
                oWnd2.center();
            }
        }
        
        function OnPageLoadComplete(sender, eventArgs) {
            sender.remove_pageLoad();
            setTimeout(function () {
                sender.set_status('');
            }, 0);
            setTimeout(function () {
                sender.setActive(true);
            }, 0);
        }

        function GetRadWindowPopup() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
        
        function OnPageClosed(sender, eventArgs) {
            sender.remove_close();
            if (sender.argument != undefined && sender.argument != null) {
                if (sender.argument.indexOf("#") >= 0) {
                    $(sender.argument).trigger('reloadGrid');
                }
                else {
                    $('#' + sender.argument).trigger('reloadGrid');
                }
            }
        }
        

        
        function showMessageBox(message, title) {
            var parentPage = GetRadWindowPopup().BrowserWindow;
            var oManager = parentPage.GetRadWindowManager();
            var width = 300;
            var height = 120;

            if (message.length > 300) {
                width = 430;
                height = 280;
            }

            if (message.length > 1300) {
                width = 550;
                height = 400;
            }
            oManager.radalert(message, width, height, title);
        }
        
        function showConfirmPopup(message, callbackfn, popupTitle) {
            var parentPage = GetRadWindowPopup().BrowserWindow;
            var oManager = parentPage.GetRadWindowManager();
            oManager.radconfirm(message, callbackfn, 330, 100, '', popupTitle);
        }
        
    </script>
</html>

