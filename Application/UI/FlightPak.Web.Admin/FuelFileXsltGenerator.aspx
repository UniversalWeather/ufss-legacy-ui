﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FuelFileXsltGenerator.aspx.cs" Inherits="FlightPak.Web.Admin.FuelFileXsltGenerator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="/Scripts/Common.js" ></script>
 <style type="text/css" media="screen">
        #XSLTEditor,#XSLTFunctionEditor {border: 1px solid #ddd;height: 550px;}
        #XSLTEditor div *{font-family: monospace !important;}
        #XSLTFunctionEditor div *{font-family: monospace !important;}
        .FuelTable {margin:10px 0 0 0;padding:0;width:100%;border:1px solid #cccccc;font-family: Arial, Helvetica, sans-serif!important;-moz-border-radius-bottomleft:4px;-webkit-border-bottom-left-radius:4px;border-bottom-left-radius:4px;-moz-border-radius-bottomright:4px;-webkit-border-bottom-right-radius:4px;border-bottom-right-radius:4px;-moz-border-radius-topright:4px;-webkit-border-top-right-radius:4px;border-top-right-radius:4px;-moz-border-radius-topleft:4px;-webkit-border-top-left-radius:4px;border-top-left-radius:4px;border-collapse: collapse !important;}
        .FuelTable table{border-collapse: collapse;border-spacing: 0;width:100%;height:100%;margin:0;padding:0;}
        .FuelTable tr:last-child td:last-child {-moz-border-radius-bottomright:4px;-webkit-border-bottom-right-radius:4px;border-bottom-right-radius:4px;}
        .FuelTable table tr:first-child td:first-child {-moz-border-radius-topleft:4px;-webkit-border-top-left-radius:4px;border-top-left-radius:4px;}
        .FuelTable table tr:first-child td:last-child {-moz-border-radius-topright:4px;-webkit-border-top-right-radius:4px;border-top-right-radius:4px;}
        .FuelTable tr:last-child td:first-child{-moz-border-radius-bottomleft:4px;-webkit-border-bottom-left-radius:4px;border-bottom-left-radius:4px;}
        .FuelTable tr:nth-child(odd){ background-color:#f5f5f5; }
        .FuelTable tr:nth-child(even){ background-color:#ffffff; }
        .FuelTable td{vertical-align:middle;border:1px solid #cccccc;border-width:0 1px 1px 0;text-align:center;padding:6px;font-size:13px;font-family:Arial;font-weight:normal;color:#8b8b8b;}
        .FuelTable tr:last-child td{border-width:0 1px 0 0;}
        .FuelTable tr td:last-child{border-width:0 0 1px 0;}
        .FuelTable tr:last-child td:last-child{border-width:0 0 0 0;}
        .FuelTable tr:first-child td{background:-o-linear-gradient(bottom, #fff4d5 5%, #fff4d5 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fff4d5), color-stop(1, #fff4d5) );background:-moz-linear-gradient( center top, #fff4d5 5%, #fff4d5 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#fff4d5", endColorstr="#fff4d5");	background: -o-linear-gradient(top,#fff4d5,fff4d5);background-color:#fff4d5;border:0px solid #cccccc;text-align:center;border-width:0px 0px 1px 1px;font-size:12px;font-family:Helvetica;font-weight:bold;color:#8b8b8b;}
        .FuelTable tr:first-child:hover td{background:-o-linear-gradient(bottom, #fff4d5 5%, #fff4d5 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #fff4d5), color-stop(1, #fff4d5) );background:-moz-linear-gradient( center top, #fff4d5 5%, #fff4d5 100% );filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#fff4d5", endColorstr="#fff4d5");	background: -o-linear-gradient(top,#fff4d5,fff4d5);background-color:#fff4d5;}
        .FuelTable tr:first-child td:first-child{border-width:0 0 1px 0;}
        .FuelTable tr:first-child td:last-child{border-width:0 0 1px 1px;}
        .custom-alert-text {font: 12px/16px Arial, Helvetica, San-serif!important;color: #DD4B39 !important;font-weight: bold !important;display: block}

        #EditorTab{ list-style: none;margin: 0;padding: 0}
        #EditorTab li{float:left}
        .currentTab{background-color: #ffffff;border-left: silver solid 1px;border-top: silver solid 1px;border-right: silver solid 1px;padding: 4px 0 4px 0;}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<telerik:RadAjaxManager id="RadAjaxManager1" runat="server" EnableAJAX="True" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
        <ClientEvents OnRequestStart="AjaxRequestStart_IE8Fix" />
         <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dgFuelFileXsltParser">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="DivExternalForm">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnFuelVendor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="DivExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
         </AjaxSettings>
</telerik:RadAjaxManager>
<telerik:radwindowmanager id="RadWindowManager1" runat="server">
    <Windows>
            <telerik:RadWindow ID="radWinFuelVendorPopup" runat="server" OnClientResizeEnd="GetDimensions"
                OnClientClose="OnClientClose" AutoSize="true" KeepInScreenBounds="true" Modal="true"
                Behaviors="Move,Close" VisibleStatusbar="false">
            </telerik:RadWindow>
   </Windows>
</telerik:radwindowmanager>
<telerik:RadWindowManager ID="rwmUpdateConfirmation" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close(1);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Yes]##</span></span></a>
                    <a onclick="$find('{0}').close(2);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[No]##</span></span></a>
                    <a onclick="$find('{0}').close(3);" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[Cancel]##</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
</telerik:RadWindowManager>
<telerik:radajaxloadingpanel id="RadAjaxLoadingPanel1" runat="server" skin="Sunset"></telerik:radajaxloadingpanel>
    <div id="globalNav_admn">
            <span class="head-title-nav">Fuel File XSLT Generator</span>
    </div>
    <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="tblspace_10">
                    </div>
                </td>
            </tr>
        </table>
    <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="RadGrid1" runat="server"  autogeneratecolumns="false" pagesize="10" allowpaging="true" pagerstyle-alwaysvisible="true"
                    OnNeedDataSource="dgFuelFileXsltParser_OnNeedDataSource"
                    OnItemCommand="dgFuelFileXsltParser_OnItemCommand"
                    OnInsertCommand="dgFuelFileXsltParser_OnInsertCommand"
                    OnUpdateCommand="dgFuelFileXsltParser_OnUpdateCommand"
                    OnDeleteCommand="dgFuelFileXsltParser_OnDeleteCommand"
                    OnItemCreated="dgFuelFileXsltParser_OnItemCreated"
                    OnSelectedIndexChanged="dgFuelFileXsltParser_OnSelectedIndexChanged">
                    <MasterTableView DataKeyNames="FuelFileXsltParserID,XsltFileName,XsltFileDescription,XsltFileContain,IsInActive,IsDeleted,LastUpdUID,LastUpdTS"
                        ClientDataKeyNames="XsltFileName,XsltFileDescription" CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="FuelFileXsltParserID" HeaderText="FuelFileXsltParserID" Display="false"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="XsltFileName" HeaderText="Xslt FileName" AutoPostBackOnFilter="false"
                                        ShowFilterIcon="false" HeaderStyle-Width="140px" FilterControlWidth="120px" FilterDelay="500"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="XsltFileDescription" HeaderText="Description" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                        ShowFilterIcon="false" HeaderStyle-Width="540px" FilterControlWidth="520px" FilterDelay="500"></telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  FooterStyle-HorizontalAlign="Center"  DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                        HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" ItemStyle-CssClass="MakeItCenter">
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                        <CommandItemTemplate>
                                    <div style="padding: 5px 5px; float: left; clear: both;">
                                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                            CommandName="InitInsert" ><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                            runat="server" CommandName="DeleteSelected" ToolTip="Delete" ><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" CssClass="last-updated-text" runat="server"></asp:Label>
                                    </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true" />
                   </ClientSettings>
                </telerik:RadGrid>
            </td>
          </tr>
    </table>
<div id="DivExternalForm" runat="server" class="ExternalForm">
    <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="dgFuelFileXsltParser" runat="server"  autogeneratecolumns="false" pagesize="10" allowpaging="true" pagerstyle-alwaysvisible="true"
                    OnNeedDataSource="dgFuelFileXsltParser_OnNeedDataSource"
                    OnItemCommand="dgFuelFileXsltParser_OnItemCommand"
                    OnInsertCommand="dgFuelFileXsltParser_OnInsertCommand"
                    OnUpdateCommand="dgFuelFileXsltParser_OnUpdateCommand"
                    OnDeleteCommand="dgFuelFileXsltParser_OnDeleteCommand"
                    OnItemCreated="dgFuelFileXsltParser_OnItemCreated"
                    OnSelectedIndexChanged="dgFuelFileXsltParser_OnSelectedIndexChanged">
                    <MasterTableView DataKeyNames="FuelFileXsltParserID,XsltFileName,XsltFileDescription,XsltFileContain,IsInActive,IsDeleted,LastUpdUID,LastUpdTS"
                        ClientDataKeyNames="XsltFileName,XsltFileDescription" CommandItemDisplay="Bottom">
                        <Columns>
                            <telerik:GridBoundColumn DataField="FuelFileXsltParserID" HeaderText="FuelFileXsltParserID" Display="false"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="XsltFileName" HeaderText="Xslt FileName" AutoPostBackOnFilter="false"
                                        ShowFilterIcon="false" HeaderStyle-Width="140px" FilterControlWidth="120px" FilterDelay="500"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="XsltFileDescription" HeaderText="Description" AutoPostBackOnFilter="false" CurrentFilterFunction="StartsWith"
                                        ShowFilterIcon="false" HeaderStyle-Width="540px" FilterControlWidth="520px" FilterDelay="500"></telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  FooterStyle-HorizontalAlign="Center"  DataField="IsInActive" HeaderText="Inactive" CurrentFilterFunction="EqualTo"
                                        HeaderStyle-Width="80px" ShowFilterIcon="false" AutoPostBackOnFilter="true" AllowFiltering="false" ItemStyle-CssClass="MakeItCenter">
                            </telerik:GridCheckBoxColumn>
                        </Columns>
                        <CommandItemTemplate>
                                    <div style="padding: 5px 5px; float: left; clear: both;">
                                        <asp:LinkButton ID="lnkInitInsert" runat="server" ToolTip="Add" CssClass="add-icon-grid"
                                            CommandName="InitInsert" ><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkInitEdit" runat="server" ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>"  /></asp:LinkButton>
                                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                                            runat="server" CommandName="DeleteSelected" ToolTip="Delete" ><img style="border:0px;vertical-align:middle;" alt="Delete" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lbLastUpdatedUser" CssClass="last-updated-text" runat="server"></asp:Label>
                                    </div>
                        </CommandItemTemplate>
                    </MasterTableView>
                    <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="true" />
                   </ClientSettings>
                </telerik:RadGrid>
            </td>
          </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                            <div id="tdSuccessMessage" class="success_msg">Record saved successfully.</div>
                        </td>
                        <td align="right">
                            <div class="mandatory"><span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
        </table>
        <asp:HiddenField ID="hdnXsltContent" runat="server"/>
        <asp:HiddenField ID="hdnXsltFunctionContent" runat="server"/>
        <asp:HiddenField ID="hdnCsvData" runat="server"/>
        <asp:HiddenField ID="hdnAllCsvData" runat="server"/>
        <asp:HiddenField ID="hdnFormMode" runat="server"/>
        <asp:HiddenField ID="hdnSave" runat="server"/>
        <asp:HiddenField ID="hdnXsltParserId" runat="server"/>
        <asp:HiddenField ID="hdnParsedCSV" runat="server"/>
        <table class="border-box" width="100%">
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tdLabel160">   
                                <asp:CheckBox ID="chkIsActive" Checked="false" Text="IsInActive" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tdLabel130">
                                <span class="mnd_text">
                                    <asp:Label ID="lblXSLTFilename" runat="server" Text="XSLT File name"></asp:Label>
                                </span>
                            </td>
                            <td>
                               <asp:TextBox ID="txtXsltFileName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                 <asp:CustomValidator runat="server" id="customValidatorXsltFileNameUnique" Display="Dynamic" ErrorMessage="Unique XSLT File name is Required." ValidationGroup="Save" CssClass="alert-text"  />
                                 <asp:RequiredFieldValidator ID="rfvXsltFileName" Display="Dynamic" runat="server" CssClass="alert-text" ValidationGroup="Save" ControlToValidate="txtXsltFileName" ErrorMessage="Xslt file name is Required"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
              </tr>
              <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tdLabel130">
                               <asp:Label ID="lblXSLTDescription" runat="server" Text="XSLT Description"></asp:Label>
                            </td>
                            <td>
                               <asp:TextBox ID="txtXsltDescription" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
              </tr>
              <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tdLabel130">
                                <span class="mnd_text">
                                    <asp:Label ID="lblFuelVendor" runat="server" Text="Fuel Vendor"></asp:Label>
                                </span>
                            </td>
                            <td>
                               <asp:TextBox ID="txtFuelVendor" runat="server" AutoPostBack="true" OnTextChanged="txtFuelVendor_OnTextChanged"></asp:TextBox>
                               <asp:HiddenField ID="hdnFuelVendor" runat="server" />
                               <asp:Button ID="btnFuelVendor" runat="server" OnClientClick="javascript:openFuelVendorWin();return false;" CssClass="browse-button" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Label ID="lblFuelVendorDescription" CssClass="input_no_bg" runat="server" Text=""></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvFuelVendor" runat="server" ValidationGroup="Save" CssClass="alert-text" Display="Dynamic" ControlToValidate="txtFuelVendor" ErrorMessage="Fuel Vendor is Required"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="CustomValidatorFuelVendor" CssClass="alert-text" Display="Dynamic" ControlToValidate="txtFuelVendor" runat="server" ErrorMessage="Invalid Fuel vendor"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
              </tr>
              <tr>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="tdLabel130">
                                <span class="mnd_text">
                                    <asp:Label ID="lblSelectCSVFile" runat="server" Text="Select CSV File"></asp:Label>
                                </span>
                            </td>
                            <td>
                                <asp:FileUpload ID="uploadedCsvFile" onchange="this.form.submit()" runat="server"/>
                                <asp:Label ID="lblCSVFilename" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:CustomValidator runat="server" id="customValidatorForCsvFile" CssClass="alert-text" ControlToValidate="uploadedCsvFile" OnServerValidate="ValidateUploadedCsvFile" ErrorMessage="Upload Fail. Please upload CSV format file." />
                                <asp:RequiredFieldValidator ID="rfvForCsvFile" ValidationGroup="Save" CssClass="alert-text" ControlToValidate="uploadedCsvFile" runat="server" Enabled="False" ErrorMessage="Please upload CSV File"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
              </tr>
         </table>
        <div id="UplodedFileView" style="display: none;margin-top:10px;font:bold 12px Arial, Helvetica, sans-serif !important">
            Uploaded File View:
            <div style="overflow-x: overlay;border-right: 1px solid #cccccc;border-left: 1px solid #cccccc;border-bottom: 1px solid #cccccc;">
                <table id="uploadedCSVDataView" style="margin-top: 0 !important" class="FuelTable">
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div style="margin-top:10px">
            <label style="font-weight: bold;font:bold 12px Arial, Helvetica, sans-serif !important">XSLT File :</label>
            <div style="float:right;"><asp:ImageButton ID="imgDownloadXsltFile" OnClick="imgDownloadXsltFile_OnClick" ToolTip="Download XSLT file" ImageUrl="/App_Themes/Default/images/export_report_icon.png" runat="server" /></div>
            <asp:CustomValidator runat="server" id="customValidatorForXsltContent" Display="Dynamic" ValidationGroup="Save" CssClass="custom-alert-text"  />
            <div class="UI-panel-bar">
                <div id="ctl00_ctl00_MainContent_PreflightHeader_rtPreFlight" class="UI-bar UI-bar-Simple UI-bar-top-Simple">
                    <div class="tabLevel tabLevel1">
                        <ul class="tab-layout">
                            <li  class="tab-content" style="width: 125px;">
                                <a href="javascript:void(0)" id="tabXSLTEditor" class="panel-tab tabAfter tabSelected">
                                    <span class="tabOut">
                                        <span class="tabIn">
                                            <span class="tabTxt">XSLT File</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li  class="tab-content" style="width: 125px;">
                                <a href="javascript:void(0)" id="tabXSLTFunctionEditor" class="panel-tab tabAfter">
                                    <span class="tabOut">
                                        <span class="tabIn">
                                            <span class="tabTxt">XSLT Function File</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
            <div id="XSLTEditor"></div>
            <div id="XSLTFunctionEditor" style="display: none"></div>
        </div>
        <div style="text-align: center; float: left; width: 100%; padding: 5px 0  5px 0;">
            <asp:ImageButton ID="btnTransformXml" ImageUrl="/App_Themes/Default/images/data_mover.png" ToolTip="Transform CSV using above XSLT File"  OnClientClick="return false;" runat="server" />          
        </div>
        <div style="clear: both"></div>
        <div style="float: right">
            <asp:Button ID="btnSaveXSLTFile" class="button" runat="server" ValidationGroup="Save" OnClick="btnSaveXSLTFile_OnClick" Text="Save XSLT File" />
            <asp:Button ID="btnCancel" class="button" runat="server" OnClick="btnCancel_OnClick" Text="Cancel" />
        </div>    
        <div style="clear: both"></div>
        <div id="ParsedCsvFileDataView" style="display: none;margin-top: 10px">
            <label style="float: left;margin-bottom: 5px;font:bold 12px Arial, Helvetica, sans-serif !important">Parsed csv file data</label>
            <div style="float:right;"><asp:ImageButton ID="imgDownloadCSVFile" OnClick="imgDownloadCSVFile_OnClick" ToolTip="Download CSV file" ImageUrl="/App_Themes/Default/images/export_report_icon.png" runat="server" /></div>
            <table id="fueldata" class="FuelTable"><tbody></tbody></table>
        </div>
</div>

<script src="/Scripts/Syntax-Highlighter/ace.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/Scripts/Syntax-Highlighter/ext-language_tools.js"></script>

<telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
    <script type="text/javascript">
        var langTools = ace.require("ace/ext/language_tools");
        var UploadedCSVDataTable = "#uploadedCSVDataView";
        var FuelDataTable = "#fueldata";
        var XSLTeditor;
        var XSLTFunctionEditor;
        
        function ShowSuccessMessage() {
            $(document).ready(function () {
                $("#tdSuccessMessage").css("display", "inline");
                $('#tdSuccessMessage').delay(60000).fadeOut(60000);
            });
        }

        $.ajax({
            async: true,
            url: "/FuelFileXsltGenerator.aspx/GetUploadedCSVData",
            type: "POST",
            dataType: "json",
            data: {},
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                result = result.d;
                if (result.IsHaveDataToDisplay) {
                    $("#UplodedFileView").show();
                    AddRowToTable(UploadedCSVDataTable, result.CsvFileData);
                } else {
                    var csvData = $("#<%=hdnCsvData.ClientID%>").val();
                    AddRowToTable(UploadedCSVDataTable, csvData);
                }
            }
        });

        function pageLoad() {
            XSLTeditor = ace.edit("XSLTEditor");
            XSLTeditor.setTheme("ace/theme/chrome");
            XSLTeditor.getSession().setMode("ace/mode/xml");
            XSLTeditor.setFontSize(13);
            XSLTeditor.setOption("showPrintMargin", false);
            XSLTeditor.setOptions({
                enableBasicAutocompletion: true
            });


            XSLTFunctionEditor = ace.edit("XSLTFunctionEditor");
            XSLTFunctionEditor.setTheme("ace/theme/chrome");
            XSLTFunctionEditor.getSession().setMode("ace/mode/xml");
            XSLTFunctionEditor.setFontSize(13);
            XSLTFunctionEditor.setOption("showPrintMargin", false);
            XSLTFunctionEditor.setOptions({
                enableBasicAutocompletion: true
            });

            //LOAD DEFAULT XSLT
            $("<div />").load("/FuelFileXsltGenerator/xsltDefaultLayout.txt", function (responseTxt, statusTxt, xhr) {
                var haveEditorSession = $("#<%=hdnXsltContent.ClientID%>").val();
                if (statusTxt == "success" && IsNullOrEmpty(haveEditorSession)) {
                    XSLTeditor.setValue(responseTxt);
                } else {
                    XSLTeditor.setValue(htmlDecode(haveEditorSession));
                }
            });

            //LOAD DEFAULT XSLT FUNCTION
            $("<div />").load("/FuelFileXsltGenerator/xsltDefaultFunctions.txt", function (responseTxt, statusTxt, xhr) {
                var haveEditorSession = $("#<%=hdnXsltFunctionContent.ClientID%>").val();
                if (statusTxt == "success" && IsNullOrEmpty(haveEditorSession)) {
                    XSLTFunctionEditor.setValue(responseTxt);
                } else {
                    XSLTFunctionEditor.setValue(htmlDecode(haveEditorSession));
                }
            });
            // Set Edit mode for the Xslt editor
            setXsltEditorMode($("#<%=hdnFormMode.ClientID%>").val());
            setCsvView();
            
            XSLTeditor.getSession().on('change', function (e) {
                var editortext = XSLTeditor.getValue();
                $("#<%=hdnXsltContent.ClientID%>").val(htmlEncode(editortext));
            });

            XSLTFunctionEditor.getSession().on('change', function (e) {
                var editortext = XSLTFunctionEditor.getValue();
                $("#<%=hdnXsltFunctionContent.ClientID%>").val(htmlEncode(editortext));
            });
            
            $(function () {
                $("#tabXSLTEditor").click(function () {
                    $("#XSLTEditor").show();
                    $("#tabXSLTEditor").addClass("tabSelected");
                    $("#XSLTFunctionEditor").hide();
                    $("#tabXSLTFunctionEditor").removeClass("tabSelected");
                });

                $("#tabXSLTFunctionEditor").click(function () {
                    $("#XSLTFunctionEditor").show();
                    $("#tabXSLTFunctionEditor").addClass("tabSelected");
                    $("#XSLTEditor").hide();
                    $("#tabXSLTEditor").removeClass("tabSelected");
                });
                
                $("#<%= btnTransformXml.ClientID%>").click(function () {
                    var storedcsvData = $("#<%=hdnAllCsvData.ClientID%>").val();
                    $.ajax({
                        async: true,
                        url: "/FuelFileXsltGenerator.aspx/TransformXSLT",
                        type: "POST",
                        dataType: "json",
                        data: JSON.stringify({ 'xsltContain': XSLTeditor.getValue(), 'xsltFunctionContain': XSLTFunctionEditor.getValue(), 'storedcsvData': storedcsvData }),
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            response = response.d;
                            if (response.StatusCode == 200 && response.Result.IsHaveDataToDisplay) {
                                $("#ParsedCsvFileDataView").show();
                                $("#<%= hdnParsedCSV.ClientID%>").val(response.Result.ParsedCSV);
                                AddRowToTable(FuelDataTable, response.Result.ParsedCsvFileData);
                            } else if (response.StatusCode == 500 && response.ErrorsList.length > 0) {
                                jAlert(response.ErrorsList[0], "Fuel File XSLT Generator");
                            }
                        }
                    });
                });
            });
        }
        function UpdateConfirmCallBackFn(arg) {
            if (arg === 1) {
                document.getElementById('<%=btnSaveXSLTFile.ClientID%>').click();
            }
            else if (arg === 2) {
                document.getElementById('<%=btnCancel.ClientID%>').click();
            }
            //for blocking redirection
            return false;
        }


        function openFuelVendorWin() {
            var oManager = $find("<%= RadWindowManager1.ClientID %>");
            var oWnd = oManager.open("/FuelVendorPopup.aspx", "radWinFuelVendorPopup");
        }
        
        function OnClientClose(oWnd, args) {
            //get the transferred arguments
            var arg = args.get_argument();
            if (arg !== null && arg !== undefined) {
                $("#<%=hdnFuelVendor.ClientID%>").val(arg.FuelVendorID);
                $("#<%=txtFuelVendor.ClientID%>").val(arg.VendorCD);
                window.__doPostBack("txtFuelVendor", "TextChanged");
            }
        }
        function GetDimensions(sender, args) {
            var bounds = sender.getWindowBounds();
            return;
        }

        function AddRowToTable(tabelId, jsondata) {
            $(tabelId + ' > tbody').html("");
            var jsonObj = { "response": eval(jsondata) };
            $.each(jsonObj.response, function (i, d) {
                var row;
                var td;
                if (i == 0) {
                    row = '<tr>';
                    td = '<td class="text-center">';
                } else {
                    row = '<tr class="text-center">';
                    td = '<td class="text-center">';
                }
                $.each(d, function (j, e) {
                    row += td + e + '</td>';
                });
                row += '</tr>';
                $(tabelId + ' tbody').append(row);
            });
        }

        function setCsvView() {
            if (!IsNullOrEmpty($("#<%=hdnCsvData.ClientID%>").val())) {
                $("#UplodedFileView").show();
                var csvData = $("#<%=hdnCsvData.ClientID%>").val();
                AddRowToTable(UploadedCSVDataTable, csvData);
            }
        }

        function setXsltEditorMode(enable) {
            if (enable == "True") {
                // FALSE for make it editable
                XSLTeditor.setReadOnly(false);
                XSLTFunctionEditor.setReadOnly(false);
            } else {
                // TRUE for make it read only
                XSLTeditor.setReadOnly(true);
                XSLTFunctionEditor.setReadOnly(true);
            }
        }

    </script>
</telerik:RadCodeBlock>
</asp:Content>
