﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountryCatalog.aspx.cs"
    Inherits="FlightPak.Web.Admin.CountryCatalog" MasterPageFile="~/Site.Master" %>

<%@ Import Namespace="FlightPak.Common.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../../../Scripts/Common.js"></script>
    <script type="text/ecmascript"></script>  
    <script type="text/javascript">
        function CheckTxtBox(control) {


            var txtCode = document.getElementById("<%=tbCode.ClientID%>").value;
            var txtDesc = document.getElementById("<%=tbDescription.ClientID%>").value;

            if (txtCode == "") {
                if (control == 'code') {
                    ValidatorEnable(document.getElementById('<%=rfvCode.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbCode.ClientID%>').focus()", 0);
                    //document.getElementById('tbCode').setFocus();
                } return false;
            }

            if (txtDesc == "") {
                if (control == 'desc') {
                    ValidatorEnable(document.getElementById('<%=rfvDescription.ClientID%>'));
                    setTimeout("document.getElementById('<%=tbDescription.ClientID%>').focus()", 0);
                    //document.getElementById('tbDescription').setFocus();
                } return false;
            }
        }        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <div class="art-setting-customersupport">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div id="globalNav_admn">
                        <span class="head-title-nav">Country</span>
                    </div>
                </td>
            </tr>
        </table>
        <table style="width: 100%;" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left">
                    <div class="nav-space">
                    </div>
                </td>
            </tr>
        </table>
         <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxSettingCreating="RadAjaxManager1_AjaxSettingCreating">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="btnSaveChanges">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnCancel">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="dgCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="dgCountry">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="dgCountry" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="divExternalForm" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="clrFilters">
                    <UpdatedControls>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="tbCode">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="tbCode" />
                        <telerik:AjaxUpdatedControl ControlID="cvCode" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Sunset" />
        <telerik:RadGrid ID="dgCountry" runat="server" AllowSorting="true" OnItemCreated="dgCountry_ItemCreated"
            OnNeedDataSource="dgCountry_BindData" OnItemCommand="dgCountry_ItemCommand" OnUpdateCommand="dgCountry_UpdateCommand"
            OnInsertCommand="dgCountry_InsertCommand" OnPreRender="dgCountry_PreRender" OnPageIndexChanged="dgCountry_PageIndexChanged"
            AutoGenerateColumns="false" PageSize="10" AllowPaging="true" OnSelectedIndexChanged="dgCountry_SelectedIndexChanged"
            AllowFilteringByColumn="true" PagerStyle-AlwaysVisible="true" Height="348px"
            OnDeleteCommand="dgCountry_DeleteCommand">
            <MasterTableView DataKeyNames="CountryID,CountryCD,CountryName,ISOCD,LastUpdUID,LastUpdDT"
                CommandItemDisplay="Bottom">
                <Columns>
                    <telerik:GridBoundColumn DataField="CountryCD" HeaderText="Country Code" AutoPostBackOnFilter="true"
                        HeaderStyle-Width="230px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CountryName" HeaderText="Description" AutoPostBackOnFilter="true"
                        HeaderStyle-Width="400px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ISOCD" HeaderText="ISO Code" AutoPostBackOnFilter="true"
                        HeaderStyle-Width="292px" ShowFilterIcon="false" CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CountryID" HeaderText="Country Code" AutoPostBackOnFilter="true"
                        ShowFilterIcon="false" CurrentFilterFunction="Contains" Display="false">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemTemplate>
                    <div style="padding: 5px 5px; float: left; clear: both;">
                        <asp:LinkButton ID="lbtnInitInsert" runat="server" ToolTip="Add" CommandName="InitInsert"><img style="border:0px;vertical-align:middle;" alt="Add" src="<%=ResolveClientUrl("~/App_Themes/Default/images/AddRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lbtnInitEdit" runat="server" OnClientClick="javascript:return ProcessUpdate();"
                            ToolTip="Edit" CommandName="Edit"><img style="border:0px;vertical-align:middle;" alt="Edit" src="<%=ResolveClientUrl("~/App_Themes/Default/images/EditRecord.png") %>" /></asp:LinkButton>
                        <asp:LinkButton ID="lnkDelete" OnClientClick="javascript:return ProcessDelete();"
                            runat="server" CommandName="DeleteSelected" ToolTip="Delete"><img style="border:0px;vertical-align:middle;"  alt="" src="<%=ResolveClientUrl("~/App_Themes/Default/images/delete.png") %>" /></asp:LinkButton>
                    </div>
                    <div>
                        <asp:Label ID="lbLastUpdatedUser" runat="server" CssClass="last-updated-text"></asp:Label>
                    </div>
                </CommandItemTemplate>
            </MasterTableView>
            <ClientSettings EnablePostBackOnRowClick="true">
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <Selecting AllowRowSelect="true" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" />
        </telerik:RadGrid>
        <div id="divExternalForm" runat="server" class="ExternalForm">
            <asp:Panel ID="pnlExternalForm" runat="server" Visible="true">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="tdLabel160">
                        </td>
                        <td align="right">
                            <div class="mandatory">
                                <span>Bold</span> Indicates required field</div>
                        </td>
                    </tr>
                </table>
                <table class="border-box">
                    <tr>
                        <td valign="top" class="tdLabel100">
                            <span class="mnd_text">Country Code</span>
                        </td>
                        <td class="tdLabel200" valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbCode" AutoPostBack="true" OnTextChanged="Code_TextChanged" runat="server"
                                            MaxLength="2" CssClass="text50" TabIndex="1"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvCode" runat="server" ValidationGroup="Save" ControlToValidate="tbCode"
                                            Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Country Code is Required.</asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvCode" runat="server" ControlToValidate="tbCode" ErrorMessage="Unique Country Code is Required"
                                            Display="Dynamic" CssClass="alert-text" ValidationGroup="Save"></asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" class="tdLabel80">
                            <asp:Label ID="lbISOCode" Text="ISO Code" runat="server"></asp:Label>
                        </td>
                        <td align="left" valign="top">
                            <asp:TextBox ID="tbIsoCode" runat="server" MaxLength="3"  CssClass="tdLabel70"
                                onKeyPress="return fnAllowAlphaNumeric(this, event)"></asp:TextBox>
                            <asp:HiddenField ID="hdnHomeBase" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="tdLabel80">
                            <span class="mnd_text">Description</span>
                        </td>
                        <td valign="top">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="tbDescription" runat="server" MaxLength="60" CssClass="text180"
                                            TabIndex="2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ValidationGroup="Save"
                                            ControlToValidate="tbDescription" Display="Dynamic" CssClass="alert-text" SetFocusOnError="true">Description is Required.</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" class="tblButtonArea">
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveChanges" Text="Save" ValidationGroup="Save" runat="server"
                                TabIndex="3" OnClick="SaveChanges_Click" CssClass="button" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" Text="Cancel" CausesValidation="false" runat="server"
                                TabIndex="4" OnClick="Cancel_Click" CssClass="button" />
                            <asp:HiddenField ID="hdnSave" runat="server" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
