﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;

namespace FlightPak.Web.Admin
{
    public partial class TrialCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //using (AdminService.AdminServiceClient client = new AdminService.AdminServiceClient())
            //{
            //    StringBuilder sb = new StringBuilder();
            //    using (StreamReader sr = new StreamReader("E:\\TrialCustomer.xml"))
            //    {
            //        String line;
            //        // Read and display lines from the file until the end of 
            //        // the file is reached.
            //        while ((line = sr.ReadLine()) != null)
            //        {
            //            sb.AppendLine(line);
            //        }
            //    }
            //    string allines = sb.ToString();
            //    string returnvalue = client.ProcessTrialCustomer(10027, allines);
            //}
            if (!IsPostBack)
            {
                using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                {
                    CustomerComboBox.DataSource = objService.GetCustomerList().EntityList.Where(x => x.LicenseType == "EV").ToList();
                    CustomerComboBox.DataTextField = "CustomerName";
                    CustomerComboBox.DataValueField = "CustomerId";
                    CustomerComboBox.DataBind();
                }
            }
        }
        protected void btnSaveChanges_OnClick(object sender, EventArgs e)
        {

            ProcessTrialCustomer();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Save", "btnSaveChanges_OnClientClick(false);", true);


        }

        private void ProcessTrialCustomer()
        {
            if (TSANoFlyFile.HasFile)
            {
                using (AdminService.AdminServiceClient client = new AdminService.AdminServiceClient())
                {
                    StringBuilder sb = new StringBuilder();
                    using (StreamReader sr = new StreamReader(TSANoFlyFile.FileContent))
                    {
                        String line;
                        // Read and display lines from the file until the end of 
                        // the file is reached.
                        while ((line = sr.ReadLine()) != null)
                        {
                            sb.AppendLine(line);
                        }
                    }
                    string allines = sb.ToString();
                    lbUploadStatus.Text = System.Web.HttpUtility.HtmlEncode( client.ProcessTrialCustomer(Convert.ToInt64(CustomerComboBox.SelectedValue), allines));
                }
            }
        }
        protected void btnCancel_OnClick(object sender, EventArgs e)
        {

            lbUploadStatus.Text = string.Empty;
            CustomerComboBox.SelectedIndex = 0;
            TSANoFlyFile = new FileUpload();

        }
    }
}