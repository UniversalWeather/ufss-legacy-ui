﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Web.Admin;
using System.Data;
using FlightPak.Web.Admin.AdminService;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using FlightPak.Common;
using System.Text.RegularExpressions;

namespace FlightPak.Web.Admin
{
    public partial class CustomerSetup : BasePage
    {
        private List<string> listCustomerCodes = new List<string>();
        System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");
        private ExceptionManager exManager;

        protected void Page_Load(object sender, EventArgs e)
        {
            // Grid Control could be ajaxified when the page is initially loaded.
            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgCustomerSetup, dgCustomerSetup, RadAjaxLoadingPanel1);
            // Store the clientID of the grid to reference it later on the client
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgCustomerSetup.ClientID));
            if (!IsPostBack)
            {
                dgCustomerSetup.Rebind();

                if (dgCustomerSetup.MasterTableView.Items.Count > 0)
                {
                    Session["CustomerSelectedItem"] = (GridDataItem)dgCustomerSetup.Items[0];
                    ReadOnlyForm();
                    GridEnable(true, true, true);
                    dgCustomerSetup.SelectedIndexes.Add(0);
                }
                BindModuleGridValue();
                BindFuelVendorGridValue();

            }
        }

        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {
            if (e.Initiator.ID.IndexOf("btnSaveChanges") > -1)
            {
                e.Updated = dgCustomerSetup;
            }
        }

        /// <summary>
        /// Grid Command Item Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridCommandItem)
            {
                //Added based on UWA requirement.
                LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);

                LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
            }
        }

        /// <summary>
        /// Bind Customer Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_BindData(object sender, GridNeedDataSourceEventArgs e)
        {
            using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
            {
                if (objService.GetCustomerList().ReturnFlag == true)
                {

                    dgCustomerSetup.DataSource = objService.GetCustomerList().EntityList;
                }

                Session.Remove("CustomerCodes");
                listCustomerCodes = new List<string>();
                foreach (AdminService.GetAllCustomersLicence objEntity in objService.GetCustomerList().EntityList)
                {
                    listCustomerCodes.Add(objEntity.CustomerID.ToString().Trim().ToLower());
                    Session["CustomerCodes"] = listCustomerCodes;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string ModuleDate(object date)
        {
            string ModuleExpDate = string.Empty;
            if (date != null && (date.ToString().Contains("1900") || date.ToString().Contains("0001")))
            {
                ModuleExpDate = "";

            }
            else if (date != null)
            {
                ModuleExpDate = date.ToString();

                ModuleExpDate = Convert.ToString(Convert.ToDateTime(date).ToString("MM/dd/yyyy"));
            }

            return ModuleExpDate;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void BindModuleGridValue()
        {
            using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
            {
                if (tbCustomerID.Text.Length > 0)
                {
                    var objModuleAccess = objService.GetCustomerModuleAccessList(Convert.ToInt64(tbCustomerID.Text)).EntityList;
                    dgModule.DataSource = objModuleAccess;
                    dgModule.DataBind();

                }
                else
                {
                    var objModuleAccess = objService.GetCustomerModuleAccessList(0).EntityList;
                    dgModule.DataSource = objModuleAccess;
                    dgModule.DataBind();
                }
            }
        }

        /// <summary>
        /// Datagrid Item Command for Customer Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case RadGrid.EditCommandName:
                    e.Canceled = true;
                    e.Item.Selected = true;
                    DisplayEditForm();
                    GridEnable(false, true, false);
                    EnableForm(true);
                    tbHomebase.Enabled = false;
                    tbIntUserName.Enabled = false;
                    tbFirstName.Enabled = false;
                    tbMiddleName.Enabled = false;
                    tbLastName.Enabled = false;
                    rfqhome.Enabled = false;
                    RequiredFieldValidator3.Enabled = false;
                    RegularExpressionValidator8.Enabled = false;
                    RegularExpressionValidator5.Enabled = false;
                    rfqFirstname.Enabled = false;
                    RegularExpressionValidator7.Enabled = false;
                    rfqLastName.Enabled = false;
                    rfvUserName.Enabled = false;
                    tbEmail.Enabled = false;
                    Button1.Disabled = true;
                    tbCustomerName.Focus();
                    break;
                case RadGrid.InitInsertCommandName:
                    e.Canceled = true;
                    rfqhome.Enabled = true;
                    RequiredFieldValidator3.Enabled = true;
                    RegularExpressionValidator8.Enabled = true;
                    RegularExpressionValidator5.Enabled = true;
                    rfqFirstname.Enabled = true;
                    RegularExpressionValidator7.Enabled = true;
                    rfqLastName.Enabled = true;
                    RequiredFieldValidator3.Enabled = true;
                    RegularExpressionValidator8.Enabled = true;
                    RegularExpressionValidator5.Enabled = true;
                    rfqFirstname.Enabled = true;

                    RegularExpressionValidator7.Enabled = true;
                    rfqLastName.Enabled = true;
                    tbEmail.Enabled = true;
                    Button1.Disabled = false;
                    dgCustomerSetup.SelectedIndexes.Clear();
                    GridEnable(true, false, false);
                    DisplayInsertForm();
                    dgCustomerSetup.Rebind();

                    BindModuleGridValue();
                    BindFuelVendorGridValue();
                    tbCustomerName.Focus();
                    break;
                case "Filter":
                    foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                    {
                        column.CurrentFilterValue = string.Empty;
                        column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Update Command for Customer Type Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_UpdateCommand(object source, GridCommandEventArgs e)
        {
            e.Canceled = true;
            try
            {
                if (!CheckCanEdit())
                {
                    string alertMsg = "radalert('Maximum users has been created. You cannot decrease user count at this time. Please inactivate some users to decrease user count!');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                }
                else if (!CheckCanEditFleet())
                {
                    string alertMsg = "radalert('Maximum aircarfts has been created. You cannot decrease aircraft count at this time. Please inactivate some aircrafts to decrease aircraft count!');";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                }
                else if (Session["CustomerSelectedItem"] != null)
                {
                    btnCancel.Visible = true;
                    btnSaveChanges.Visible = true;
                    using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                    {
                        long homebaseId = 0;
                        if (CmbLicense.SelectedValue == "PR" && (tbLicenseNumber.Text == "" || tbLicenseNumber.Text.Length == 0))
                        {
                            string alertMsg = "radalert('LicenseNumber is Required', 360, 50, 'Customer setup');";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        }
                        //else if (!long.TryParse(hdnHomebaseId.Value, out homebaseId) && homebaseId == 0)
                        //{
                        //    string alertMsg = "radalert('Invalid home base', 360, 50, 'Customer setup');";
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        //}
                        else
                        {
                            if (validateModules())
                            {
                                AdminService.Customer objCustomer = new AdminService.Customer();
                                AdminService.CustomerLicense objCustomerLicence = new AdminService.CustomerLicense();
                                AdminService.Company objCompany = new AdminService.Company();
                                AdminService.UserMaster objUserMaster = new AdminService.UserMaster();

                                objCustomer.CustomerLicense = new List<AdminService.CustomerLicense>();
                                //objCustomer.Company = new List<AdminService.Company>();
                                //objCustomer.UserMaster = new List<AdminService.UserMaster>();


                                ////user Master Records
                                //objUserMaster.CustomerID = Convert.ToInt32(tbCustomerID.Text); ;
                                //objUserMaster.UserName = tbIntUserName.Text;
                                //objUserMaster.FirstName = tbFirstName.Text;
                                //objUserMaster.MiddleName = tbMiddleName.Text;
                                //objUserMaster.LastName = tbLastName.Text;

                                ////Company Records
                                //objCompany.CustomerID = Convert.ToInt32(tbCustomerID.Text); ;
                                //objCompany.HomebaseCD = tbIntUserName.Text;
                                objCompany.HomebaseAirportID = homebaseId;


                                objCustomer.CustomerID = Convert.ToInt64(tbCustomerID.Text);
                                objCustomer.CustomerName = tbCustomerName.Text;
                                objCustomer.PrimaryFirstName = tbFirstName.Text;
                                objCustomer.PrimaryMiddleName = tbMiddleName.Text;
                                objCustomer.PrimaryLastName = tbLastName.Text;
                                objCustomer.PrimaryEmailID = tbEmail.Text;
                                objCustomer.SecondaryFirstName = tbFirstName1.Text;
                                objCustomer.SecondaryMiddleName = tbMiddleName1.Text;
                                objCustomer.SecondaryLastName = tbLastName1.Text;
                                objCustomer.SecondaryEmailID = tbEmail1.Text;
                                objCustomer.IsAll = chkAll.Checked;
                                objCustomer.IsAPIS = chkAPIS.Checked;
                                objCustomer.IsATS = chkATSTS3.Checked;
                                objCustomer.IsMapping = chkMapping.Checked;
                                objCustomer.IsUVFuel = chkUVFuel.Checked;
                                objCustomer.IsUVTripLink = chkUvtriplink.Checked;
                                objCustomer.UVTripLinkName = tbUvtriplinkName.Text;
                                objCustomer.UVTripLinkAccountNumber = tbAccountNumber.Text;
                                objCustomer.ApisID = tbApisID.Text;
                                objCustomer.ApisPassword = tbApisPassword.Text;
                                objCustomerLicence.CustomerID = Convert.ToInt64(tbCustomerID.Text); ;
                                objCustomerLicence.LicenseNumber = tbLicenseNumber.Text;
                                objCustomerLicence.LicenseType = CmbLicense.SelectedItem.Text.ToString();
                                objCustomer.IsDeleted = false;
                                if (tbLicenseStartDate.Text != "" || tbLicenseStartDate.Text.Length != 0)
                                {
                                    objCustomerLicence.LicenseStartDate = Convert.ToDateTime(tbLicenseStartDate.Text);
                                }

                                if (tbUserCount.Text != "" || tbUserCount.Text.Length != 0)
                                {
                                    objCustomerLicence.UserCount = Convert.ToInt32(tbUserCount.Text);
                                }
                                if (tbAircraftCount.Text != "" || tbAircraftCount.Text.Length != 0)
                                {
                                    objCustomerLicence.AircraftCount = Convert.ToInt32(tbAircraftCount.Text);
                                }
                                if (tbMaintainenceExpiryDate.Text != "" || tbMaintainenceExpiryDate.Text.Length != 0)
                                {
                                    objCustomerLicence.MaintainenceExpiryDate = Convert.ToDateTime(tbMaintainenceExpiryDate.Text);
                                }


                                if (HdLicenseID.Value != "")
                                {
                                    objCustomerLicence.CustomerLicenseID = Convert.ToInt64(HdLicenseID.Value);
                                }
                                else
                                {
                                    objCustomerLicence.CustomerLicenseID = 1;
                                }


                                objCustomerLicence.IsUWASupportAccess = chkUWASupportAccess.Checked;
                                objCustomerLicence.IsActiveLicense = chkActiveLicense.Checked;
                                objCustomerLicence.IsDeleted = false;

                                objCustomer.CustomerName = tbCustomerName.Text;
                                objCustomer.CustomerModuleAccess = new List<AdminService.CustomerModuleAccess>();
                                int ival = 1;
                                foreach (GridViewRow gvr in dgModule.Rows)
                                {
                                    AdminService.CustomerModuleAccess objModule = new AdminService.CustomerModuleAccess();
                                    //ival is the Dummy value not used in Db change \
                                    HiddenField hdAccessId = (HiddenField)gvr.FindControl("HdAccessId");
                                    objModule.CustomerModuleAccessID = Convert.ToInt64(hdAccessId.Value);

                                    //////
                                    CheckBox cb = (CheckBox)gvr.FindControl("chkModule");
                                    objModule.CanAccess = cb.Checked;
                                    HiddenField hdCust = (HiddenField)gvr.FindControl("HdCust");
                                    // objModule.CustomerID = Guid.Parse(hdCust.Value);
                                    objModule.CustomerID = Convert.ToInt64(tbCustomerID.Text);
                                    if (hdCust.Value.ToUpper() != "0")
                                    {
                                        objModule.LastUpdTS = DateTime.Today;
                                    }
                                    objModule.ModuleID = Convert.ToInt64(dgModule.DataKeys[gvr.RowIndex].Value);
                                    TextBox Expdate = (TextBox)gvr.FindControl("tbEvalutionExpiryDate");

                                    if (cb.Checked)
                                    {
                                        if (Expdate.Text != "" || Expdate.Text.Length == 0)
                                        {
                                            objModule.ExpiryDate = Convert.ToDateTime(Expdate.Text.Trim());
                                        }
                                    }
                                    objModule.IsDeleted = false;

                                    objCustomer.CustomerModuleAccess.Add(objModule);

                                    ival++;
                                }

                                var oListCustomerFuelVendorAccess = new List<CustomerFuelVendorAccess>();
                                foreach (GridViewRow gvr in dgFuelVendor.Rows)
                                {
                                    CustomerFuelVendorAccess oCustomerFuelVendorAccess = new CustomerFuelVendorAccess();
                                    CheckBox cb = (CheckBox)gvr.FindControl("chkVendorAccess");
                                    if (cb.Checked)
                                    {
                                        HiddenField hdFuelVendorID = (HiddenField) gvr.FindControl("HdFuelVendorID");
                                        HiddenField customerFuelVendorAccessID = (HiddenField)gvr.FindControl("HdCustomerFuelVendorAccessID");
                                        if (!string.IsNullOrWhiteSpace(customerFuelVendorAccessID.Value))
                                        {
                                            oCustomerFuelVendorAccess.CustomerFuelVendorAccessID = Convert.ToInt64(customerFuelVendorAccessID.Value);
                                        }
                                        oCustomerFuelVendorAccess.CanAccess = cb.Checked;
                                        oCustomerFuelVendorAccess.FuelVendorID = Convert.ToInt64(hdFuelVendorID.Value);
                                        oCustomerFuelVendorAccess.IsDeleted = false;
                                        oCustomerFuelVendorAccess.IsInActive = false;
                                        oCustomerFuelVendorAccess.LastUpdUID = null;
                                        oCustomerFuelVendorAccess.LastUpdTS = DateTime.UtcNow;
                                        oListCustomerFuelVendorAccess.Add(oCustomerFuelVendorAccess);
                                    } else
                                    {
                                        HiddenField customerFuelVendorAccessID = (HiddenField)gvr.FindControl("HdCustomerFuelVendorAccessID");
                                        if (!string.IsNullOrWhiteSpace(customerFuelVendorAccessID.Value))
                                        {
                                            HiddenField hdFuelVendorID = (HiddenField) gvr.FindControl("HdFuelVendorID");
                                            oCustomerFuelVendorAccess.CustomerFuelVendorAccessID = Convert.ToInt64(customerFuelVendorAccessID.Value);
                                            oCustomerFuelVendorAccess.CanAccess = cb.Checked;
                                            oCustomerFuelVendorAccess.FuelVendorID = Convert.ToInt64(hdFuelVendorID.Value);
                                            oCustomerFuelVendorAccess.IsDeleted = false;
                                            oCustomerFuelVendorAccess.IsInActive = false;
                                            oCustomerFuelVendorAccess.LastUpdUID = null;
                                            oCustomerFuelVendorAccess.LastUpdTS = DateTime.UtcNow;
                                            oListCustomerFuelVendorAccess.Add(oCustomerFuelVendorAccess);
                                        }
                                    }
                                }
                                objCustomer.CustomerFuelVendorAccess = oListCustomerFuelVendorAccess;
                                objCustomer.CustomerLicense.Add(objCustomerLicence);
                                objService.UpdateCustomer(objCustomer, dgModule.Rows.Count);
                                //e.Item.OwnerTableView.Rebind();
                                //e.Item.Selected = true;
                                dgCustomerSetup.Rebind();
                                BindModuleGridValue();
                                ReadOnlyForm();
                                if (dgCustomerSetup.MasterTableView.Items.Count > 0)
                                {
                                    dgCustomerSetup.SelectedIndexes.Add(0);
                                    Session["CustomerSelectedItem"] = (GridDataItem)dgCustomerSetup.Items[0];
                                    PopulateData();
                                }
                                BindFuelVendorGridValue();
                            }

                        }
                    }
                }
            }
            catch (System.NullReferenceException exc)
            {
                //Manually Handled
                e.Canceled = true;
                throw exc;
            }
            catch (System.ArgumentOutOfRangeException exc)
            {
                //Manually Handled
                e.Canceled = true;
                throw exc;
            }
            catch (System.InvalidCastException exc)
            {
                //Manually Handled
                e.Canceled = true;
                throw exc;
            }
        }

        /// <summary>
        /// Insert Command for Customer Group Catalog Grid
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_InsertCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                btnSaveChanges.Visible = true;
                btnCancel.Visible = true;

                e.Canceled = true;
                using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                {
                    long homebaseId = 0;
                    if (CmbLicense.SelectedValue == "PR" && (tbLicenseNumber.Text == "" || tbLicenseNumber.Text.Length == 0))
                    {
                        string alertMsg = "radalert('LicenseNumber is Required', 360, 50, 'Customer setup');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    }
                    else if ((objService.ValidateUser(tbIntUserName.Text).EntityList.Count() > 0))
                    {
                        string alertMsg = "radalert('User name already exist', 360, 50, 'Customer setup');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    }
                    else if (!long.TryParse(hdnHomebaseId.Value, out homebaseId) && homebaseId == 0)
                    {
                        string alertMsg = "radalert('Invalid home base', 360, 50, 'Customer setup');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                    }

                    else
                    {
                        if (validateModules())
                        {
                            AdminService.Customer objCustomer = new AdminService.Customer();
                            AdminService.CustomerLicense objCustomerLicence = new AdminService.CustomerLicense();
                            AdminService.Company objCompany = new AdminService.Company();
                            AdminService.UserMaster objUserMaster = new AdminService.UserMaster();

                            objCustomer.CustomerLicense = new List<AdminService.CustomerLicense>();
                            objCustomer.Company = new List<AdminService.Company>();
                            objCustomer.UserMaster = new List<AdminService.UserMaster>();

                            //user Master Records
                            objUserMaster.CustomerID = Convert.ToInt64(tbCustomerID.Text); ;
                            objUserMaster.UserName = tbIntUserName.Text;
                            objUserMaster.FirstName = tbFirstName.Text;
                            objUserMaster.MiddleName = tbMiddleName.Text;
                            objUserMaster.LastName = tbLastName.Text;
                            objUserMaster.EmailID = tbEmail.Text;

                            //Initially these fields should be true
                            objUserMaster.IsPrimaryContact = true;
                            objUserMaster.IsSystemAdmin = true;
                            objUserMaster.IsActive = true;
                            objUserMaster.IsUserLock = false;

                            //Company Records
                            objCompany.CustomerID = Convert.ToInt64(tbCustomerID.Text);

                            // By Vishwa
                            //objCompany.HomebaseCD = tbHomebase.Text;


                            objCompany.HomebaseAirportID = homebaseId;

                            //Customer Records
                            objCustomer.CustomerID = Convert.ToInt64(tbCustomerID.Text); ;
                            objCustomer.CustomerUID = Guid.NewGuid();
                            objCustomer.CustomerName = tbCustomerName.Text;
                            objCustomer.PrimaryFirstName = tbFirstName.Text;
                            objCustomer.PrimaryMiddleName = tbMiddleName.Text;
                            objCustomer.PrimaryLastName = tbLastName.Text;
                            objCustomer.PrimaryEmailID = tbEmail.Text;
                            objCustomer.SecondaryFirstName = tbFirstName1.Text;
                            objCustomer.SecondaryMiddleName = tbMiddleName1.Text;
                            objCustomer.SecondaryLastName = tbLastName1.Text;
                            objCustomer.SecondaryEmailID = tbEmail1.Text;
                            objCustomer.IsAll = chkAll.Checked;
                            objCustomer.IsAPIS = chkAPIS.Checked;
                            objCustomer.IsATS = chkATSTS3.Checked;
                            objCustomer.IsMapping = chkMapping.Checked;
                            objCustomer.IsUVFuel = chkUVFuel.Checked;
                            objCustomer.IsUVTripLink = chkUvtriplink.Checked;
                            objCustomer.UVTripLinkName = tbUvtriplinkName.Text;
                            objCustomer.UVTripLinkAccountNumber = tbAccountNumber.Text;
                            objCustomer.ApisID = tbApisID.Text;
                            objCustomer.ApisPassword = tbApisPassword.Text;
                            objCustomerLicence.LicenseNumber = tbLicenseNumber.Text;
                            objCustomerLicence.LicenseType = CmbLicense.SelectedItem.Text.ToString();
                            if (tbLicenseStartDate.Text != "" || tbLicenseStartDate.Text.Length != 0)
                            {
                                objCustomerLicence.LicenseStartDate = Convert.ToDateTime(tbLicenseStartDate.Text);
                            }
                            if (tbUserCount.Text != "" || tbUserCount.Text.Length != 0)
                            {
                                objCustomerLicence.UserCount = Convert.ToInt32(tbUserCount.Text);
                            }
                            if (tbAircraftCount.Text != "" || tbAircraftCount.Text.Length != 0)
                            {
                                objCustomerLicence.AircraftCount = Convert.ToInt32(tbAircraftCount.Text);
                            }
                            if (tbMaintainenceExpiryDate.Text != "" || tbMaintainenceExpiryDate.Text.Length != 0)
                            {
                                objCustomerLicence.MaintainenceExpiryDate = Convert.ToDateTime(tbMaintainenceExpiryDate.Text);
                            }
                            objCustomerLicence.CustomerID = 0;
                            if (HdLicenseID.Value != "")
                            {
                                objCustomerLicence.CustomerLicenseID = Convert.ToInt64(HdLicenseID.Value);
                            }
                            else
                            {
                                objCustomerLicence.CustomerLicenseID = 1;
                            }


                            objCustomerLicence.IsUWASupportAccess = chkUWASupportAccess.Checked;
                            objCustomerLicence.IsActiveLicense = chkActiveLicense.Checked;
                            objCustomerLicence.IsDeleted = false;


                            objCustomer.CustomerName = tbCustomerName.Text;
                            objCustomer.CustomerModuleAccess = new List<AdminService.CustomerModuleAccess>();
                            foreach (GridViewRow gvr in dgModule.Rows)
                            {
                                AdminService.CustomerModuleAccess objModule = new AdminService.CustomerModuleAccess();
                                CheckBox cb = (CheckBox)gvr.FindControl("chkModule");
                                objModule.ModuleID = Convert.ToInt64(dgModule.DataKeys[gvr.RowIndex].Value);
                                objModule.CanAccess = cb.Checked;
                                TextBox Expdate = (TextBox)gvr.FindControl("tbEvalutionExpiryDate");
                                if (cb.Checked)
                                {
                                    if (Expdate.Text != "" || Expdate.Text.Length == 0)
                                    {
                                        objModule.ExpiryDate = Convert.ToDateTime(Expdate.Text);
                                    }
                                }
                                objModule.IsDeleted = false;
                                objModule.LastUpdTS = DateTime.UtcNow;
                                objCustomer.CustomerModuleAccess.Add(objModule);
                            }

                            var oListCustomerFuelVendorAccess = new List<CustomerFuelVendorAccess>();
                            foreach (GridViewRow gvr in dgFuelVendor.Rows)
                            {
                                CustomerFuelVendorAccess oCustomerFuelVendorAccess=new CustomerFuelVendorAccess();
                                CheckBox cb = (CheckBox)gvr.FindControl("chkVendorAccess");
                                if (cb.Checked)
                                {
                                    HiddenField hdFuelVendorID = (HiddenField) gvr.FindControl("HdFuelVendorID");
                                    oCustomerFuelVendorAccess.CanAccess = cb.Checked;
                                    oCustomerFuelVendorAccess.FuelVendorID = Convert.ToInt64(hdFuelVendorID.Value);
                                    oCustomerFuelVendorAccess.IsDeleted = false;
                                    oCustomerFuelVendorAccess.IsInActive = false;
                                    oCustomerFuelVendorAccess.LastUpdUID = null;
                                    oCustomerFuelVendorAccess.LastUpdTS = DateTime.UtcNow;
                                    oListCustomerFuelVendorAccess.Add(oCustomerFuelVendorAccess);
                                }
                            }
                            objCustomer.CustomerFuelVendorAccess = oListCustomerFuelVendorAccess;
                            objCustomer.UserMaster.Add(objUserMaster);
                            objCustomer.CustomerLicense.Add(objCustomerLicence);
                            objCustomer.Company.Add(objCompany);
                            objService.AddCustomer(objCustomer);

                            //e.Item.OwnerTableView.Rebind();
                            //e.Item.Selected = true;
                            dgCustomerSetup.Rebind();
                            BindModuleGridValue();
                            DisplayInsertForm();
                            ReadOnlyForm();
                            if (dgCustomerSetup.MasterTableView.Items.Count > 0)
                            {
                                dgCustomerSetup.SelectedIndexes.Add(0);
                                Session["CustomerSelectedItem"] = (GridDataItem)dgCustomerSetup.Items[0];
                                PopulateData();
                            }
                            BindFuelVendorGridValue();
                        }
                    }
                }
            }
            catch (System.NullReferenceException exc)
            {
                //Manually Handled
                e.Canceled = true;
                e.Item.OwnerTableView.IsItemInserted = false;
            }
            catch (System.ArgumentOutOfRangeException exc)
            {
                //Manually Handled
                e.Canceled = true;
                e.Item.OwnerTableView.IsItemInserted = false;
            }
            catch (System.InvalidCastException exc)
            {
                //Manually Handled
                e.Canceled = true;
                e.Item.OwnerTableView.IsItemInserted = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool validateModules()
        {
            bool retval = true;
            foreach (GridViewRow gvr in dgModule.Rows)
            {
                CheckBox cb = (CheckBox)gvr.FindControl("chkModule");
                bool canAccess = cb.Checked;
                DateTime dt;
                int moduleid;

                TextBox Expdate = (TextBox)gvr.FindControl("tbEvalutionExpiryDate");
                if (canAccess)
                {
                    if (Expdate.Text == "" || Expdate.Text.Length == 0)
                    {
                        string alertMsg = "radalert('Evaluation Expiry date are Missing for the Modules .Please fill it', 360, 50, 'Customer setup');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        retval = false;
                        break;
                    }
                    else
                    {

                        //dt = Convert.ToDateTime(String.Format("{0:MM/dd/yyyy}", Expdate.Text));
                        // dt = Convert.ToDateTime(Expdate.Text);
                        // dt = (CultureInfo.InvariantCulture,"MM/dd/yyyy", Expdate.Text);
                        dt = DateTime.ParseExact(Expdate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);

                        moduleid = Convert.ToInt32(dgModule.DataKeys[gvr.RowIndex].Value);
                    }
                }

            }

            return retval;
        }

        /// <summary>
        /// Command for Deleting value in the Database
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_DeleteCommand(object source, GridCommandEventArgs e)
        {
            if (Session["CustomerSelectedItem"] != null)
            {
                GridDataItem item = (GridDataItem)Session["CustomerSelectedItem"];
                using (AdminService.AdminServiceClient objService = new AdminService.AdminServiceClient())
                {
                    AdminService.Customer objCustomers = new AdminService.Customer();
                    objCustomers.CustomerID = Convert.ToInt64(item.GetDataKeyValue("CustomerID").ToString());
                    objCustomers.IsDeleted = true;
                    objCustomers.CustomerName = string.Empty;
                    objService.DeleteCustomer(objCustomers);
                    DisplayInsertForm();
                }
            }
        }

        /// <summary>
        /// Bind Selected Item in the Session for Update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridDataItem item = (GridDataItem)dgCustomerSetup.SelectedItems[0];
            if (btnSaveChanges.Visible == false)
            {

                Session["CustomerSelectedItem"] = item;
                dgCustomerSetup.Rebind();
                PopulateData();
                BindModuleGridValue();
                item.Selected = true;
                Label lbLastUpdatedUser;

                lbLastUpdatedUser = (Label)dgCustomerSetup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
                if (item.GetDataKeyValue("LastUserID") != null)
                {
                    lbLastUpdatedUser.Text = "Last Updated User: " + item.GetDataKeyValue("LastUserID").ToString();
                }
                else
                {
                    lbLastUpdatedUser.Text = string.Empty;
                }
                if (item.GetDataKeyValue("LastUptTS") != null)
                {
                    lbLastUpdatedUser.Text = lbLastUpdatedUser.Text + " Date: " + item.GetDataKeyValue("LastUptTS").ToString();
                }
                ReadOnlyForm();
                GridEnable(true, true, true);
                BindFuelVendorGridValue();

            }
            item.Selected = true;
        }

        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {
            hdnSave.Value = "Save";
            ClearForm();
            tbCustomerID.Enabled = false;
            //tbCustomerID.BackColor = System.Drawing.Color.White;
            EnableForm(true);
            BindModuleGridValue();
            tbCustomerID.Text = "0";
            chkActiveLicense.Enabled = true;
        }

        /// <summary>
        /// Command Event Trigger for Save or Update Customer Type Catalog Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveChanges_Click(object sender, EventArgs e)
        {
            if (hdnSave.Value == "Update")
            {
                (dgCustomerSetup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
            }
            else
            {
                (dgCustomerSetup.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
            }
            //dgCustomerSetup.Rebind();
            //if (dgCustomerSetup.MasterTableView.Items.Count > 0)
            //{
            //    dgCustomerSetup.SelectedIndexes.Add(0);
            //}
            //BindModuleGridValue();
            //GridEnable(true, true, true);
        }

        /// <summary>
        /// Cancel Customer Type Catalog Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            //pnlExternalForm.Visible = false;
            Session["CustomerSelectedItem"] = null;


            btnSaveChanges.Visible = false;
            btnCancel.Visible = false;
            GridEnable(true, true, true);
            BindModuleGridValue();
            
            EnableForm(false);
            dgCustomerSetup.Rebind();
            if (dgCustomerSetup.MasterTableView.Items.Count > 0)
            {
                dgCustomerSetup.SelectedIndexes.Add(0);
                Session["CustomerSelectedItem"] = (GridDataItem)dgCustomerSetup.Items[0];
                PopulateData();
            }
            BindFuelVendorGridValue();
            //ClearForm();
        }

        /// <summary>
        /// Binding the colors for grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgCustomerSetup_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
        }

        /// <summary>
        /// Function to Check if Customer Type Code Alerady Exists
        /// </summary>
        /// <returns></returns>
        private bool CheckAllReadyExist()
        {
            bool returnVal = false;
            listCustomerCodes = (List<string>)Session["CustomerCodes"];
            if (listCustomerCodes != null && listCustomerCodes.Equals(tbCustomerID.Text.ToString().Trim().ToLower()))
                return true;
            return returnVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="add"></param>
        /// <param name="edit"></param>
        /// <param name="delete"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {

            LinkButton insertCtl, delCtl, editCtl;
            insertCtl = (LinkButton)dgCustomerSetup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
            delCtl = (LinkButton)dgCustomerSetup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
            editCtl = (LinkButton)dgCustomerSetup.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
            if (add)
                insertCtl.Enabled = true;
            else
                insertCtl.Enabled = false;
            if (delete)
            {
                delCtl.Enabled = true;
                delCtl.OnClientClick = "javascript:return ProcessDelete();";

            }
            else
            {
                delCtl.Enabled = false;
                delCtl.OnClientClick = string.Empty;
            }
            if (edit)
            {
                editCtl.Enabled = true;
                editCtl.OnClientClick = "javascript:return ProcessUpdate();";
            }
            else
            {
                editCtl.Enabled = false;
                editCtl.OnClientClick = string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected void ReadOnlyForm()
        {
            PopulateData();
            hdnSave.Value = "Update";
            dgCustomerSetup.Rebind();

            BindModuleGridValue();
            EnableForm(false);
        }

        /// <summary>
        /// 
        /// </summary>
        protected void PopulateData()
        {
            GridDataItem item = (GridDataItem)Session["CustomerSelectedItem"];
            string CustomerID = item.GetDataKeyValue("CustomerID").ToString();
            tbCustomerID.Text = CustomerID;
            tbCustomerID.ReadOnly = true;
            tbCustomerID.BackColor = System.Drawing.Color.LightGray;

            if (item.GetDataKeyValue("CustomerName") != null)
            {
                tbCustomerName.Text = item.GetDataKeyValue("CustomerName").ToString();
            }
            else
            {
                tbCustomerName.Text = string.Empty;
            }
            if (item.GetDataKeyValue("PrimaryFirstName") != null)
            {
                tbFirstName.Text = item.GetDataKeyValue("PrimaryFirstName").ToString();
            }
            else
            {
                tbFirstName.Text = string.Empty;
            }
            if (item.GetDataKeyValue("PrimaryMiddleName") != null)
            {
                tbMiddleName.Text = item.GetDataKeyValue("PrimaryMiddleName").ToString();
            }
            else
            {
                tbMiddleName.Text = string.Empty;
            }
            if (item.GetDataKeyValue("PrimaryLastName") != null)
            {
                tbLastName.Text = item.GetDataKeyValue("PrimaryLastName").ToString();
            }
            else
            {
                tbLastName.Text = string.Empty;
            }

            if (item.GetDataKeyValue("PrimaryEmailID") != null)
            {
                tbEmail.Text = item.GetDataKeyValue("PrimaryEmailID").ToString();
            }
            else
            {
                tbEmail.Text = string.Empty;
            }

            if (item.GetDataKeyValue("SecondaryFirstName") != null)
            {
                tbFirstName1.Text = item.GetDataKeyValue("SecondaryFirstName").ToString();
            }
            else
            {
                tbFirstName1.Text = string.Empty;
            }

            if (item.GetDataKeyValue("SecondaryMiddleName") != null)
            {
                tbMiddleName1.Text = item.GetDataKeyValue("SecondaryMiddleName").ToString();
            }
            else
            {
                tbMiddleName1.Text = string.Empty;
            }

            if (item.GetDataKeyValue("SecondaryLastName") != null)
            {
                tbLastName1.Text = item.GetDataKeyValue("SecondaryLastName").ToString();
            }
            else
            {
                tbLastName1.Text = string.Empty;
            }

            if (item.GetDataKeyValue("SecondaryEmailID") != null)
            {
                tbEmail1.Text = item.GetDataKeyValue("SecondaryEmailID").ToString();
            }
            else
            {
                tbEmail1.Text = string.Empty;
            }

            //if (item.GetDataKeyValue("SecondaryEmailID") != null)
            //{
            //    tbEmail1.Text = item.GetDataKeyValue("SecondaryEmailID").ToString();
            //}
            //else
            //{
            //    tbEmail1.Text = string.Empty;
            //}

            if (item.GetDataKeyValue("IsAll") != null)
            {
                chkAll.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsAll").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsAll").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkAll.Checked = false;
            }

            if (item.GetDataKeyValue("IsAPIS") != null)
            {
                chkAPIS.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsAPIS").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsAPIS").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkAPIS.Checked = false;
            }


            if (item.GetDataKeyValue("IsATS") != null)
            {
                chkATSTS3.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsATS").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsATS").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkATSTS3.Checked = false;
            }

            if (item.GetDataKeyValue("IsMapping") != null)
            {
                chkMapping.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsMapping").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsMapping").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkMapping.Checked = false;
            }

            if (item.GetDataKeyValue("IsUVFuel") != null)
            {
                chkUVFuel.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsUVFuel").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsUVFuel").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkUVFuel.Checked = false;
            }

            if (item.GetDataKeyValue("IsUVTriplink") != null)
            {
                chkUvtriplink.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsUVTriplink").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsUVTriplink").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkUvtriplink.Checked = false;
            }

            if (item.GetDataKeyValue("UVTripLinkName") != null)
            {
                tbUvtriplinkName.Text = item.GetDataKeyValue("UVTripLinkName").ToString();
            }
            else
            {
                tbUvtriplinkName.Text = string.Empty;
            }

            if (item.GetDataKeyValue("UVTripLinkAccountNumber") != null)
            {
                tbAccountNumber.Text = item.GetDataKeyValue("UVTripLinkAccountNumber").ToString();
            }
            else
            {
                tbAccountNumber.Text = string.Empty;
            }


            if (item.GetDataKeyValue("ApisID") != null)
            {
                tbApisID.Text = item.GetDataKeyValue("ApisID").ToString();
            }
            else
            {
                tbApisID.Text = string.Empty;
            }

            if (item.GetDataKeyValue("ApisPassword") != null)
            {
                tbApisPassword.Text = item.GetDataKeyValue("ApisPassword").ToString();
            }
            else
            {
                tbApisPassword.Text = string.Empty;
            }



            //if (item.GetDataKeyValue("ApisPassword") != null)
            //{
            //    tbApisPassword.Text = item.GetDataKeyValue("ApisPassword").ToString();
            //}
            //else
            //{
            //    tbApisPassword.Text = string.Empty;
            //}

            if (item.GetDataKeyValue("LicenseNumber") != null)
            {
                tbLicenseNumber.Text = item.GetDataKeyValue("LicenseNumber").ToString();
            }
            else
            {
                tbLicenseNumber.Text = string.Empty;
            }

            //if (item.GetDataKeyValue("LicenseNumber") != null)
            //{
            //    tbLicenseNumber.Text = item.GetDataKeyValue("LicenseNumber").ToString();
            //}
            //else
            //{
            //    tbLicenseNumber.Text = string.Empty;
            //}

            if (item.GetDataKeyValue("LicenseType") != null)
            {
                CmbLicense.SelectedValue = item.GetDataKeyValue("LicenseType").ToString();
            }
            else
            {
                CmbLicense.SelectedIndex = 0;
            }

            if (item.GetDataKeyValue("LicenseStartDate") != null)
            {

                tbLicenseStartDate.Text = Convert.ToDateTime(item.GetDataKeyValue("LicenseStartDate").ToString()).ToString("MM/dd/yyyy");
            }
            else
            {
                tbLicenseStartDate.Text = string.Empty;
            }

            if (item.GetDataKeyValue("UserCount") != null)
            {
                tbUserCount.Text = item.GetDataKeyValue("UserCount").ToString();
            }
            else
            {
                tbUserCount.Text = string.Empty;
            }

            if (item.GetDataKeyValue("AircraftCount") != null)
            {
                tbAircraftCount.Text = item.GetDataKeyValue("AircraftCount").ToString();
            }
            else
            {
                tbAircraftCount.Text = string.Empty;
            }

            if (item.GetDataKeyValue("MaintainenceExpiryDate") != null)
            {

                tbMaintainenceExpiryDate.Text = Convert.ToDateTime(item.GetDataKeyValue("MaintainenceExpiryDate").ToString()).ToString("MM/dd/yyyy");
            }
            else
            {
                tbMaintainenceExpiryDate.Text = string.Empty;
            }
            if (item.GetDataKeyValue("IsUWASupportAccess") != null)
            {
                chkUWASupportAccess.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsUWASupportAccess").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsUWASupportAccess").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkUWASupportAccess.Checked = false;
            }

            if (item.GetDataKeyValue("IsActiveLicense") != null)
            {
                chkActiveLicense.Checked = Convert.ToBoolean(item.GetDataKeyValue("IsActiveLicense").ToString() == "&nbsp;" ? "False" : item.GetDataKeyValue("IsActiveLicense").ToString(), CultureInfo.CurrentCulture);
            }
            else
            {
                chkActiveLicense.Checked = false;
            }

            if (item.GetDataKeyValue("UserName") != null)
            {
                tbIntUserName.Text = Convert.ToString(item.GetDataKeyValue("UserName").ToString());
            }
            else
            {
                tbIntUserName.Text = "";
            }

            if (item.GetDataKeyValue("IcaoID") != null)
            {
                tbHomebase.Text = Convert.ToString(item.GetDataKeyValue("IcaoID").ToString());
            }
            else
            {
                tbHomebase.Text = "";
            }
            if (item.GetDataKeyValue("HomebaseAirportID") != null)
            {
                hdnHomebaseId.Value = Convert.ToString(item.GetDataKeyValue("HomebaseAirportID").ToString());
            }
            else
            {
                hdnHomebaseId.Value = "";
            }

            if (item.GetDataKeyValue("CustomerLicenseID") != null)
            {
                HdLicenseID.Value = Convert.ToString(item.GetDataKeyValue("CustomerLicenseID").ToString());
            }
            else
            {
                HdLicenseID.Value = "";
            }
            if (chkAll.Checked)
            {
                chkAPIS.Checked = chkATSTS3.Checked = chkMapping.Checked = chkUVFuel.Checked = chkUvtriplink.Checked = chkAll.Checked;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            //tbCustomerID.Enabled = enable;
            //tbCustomerName.Enabled = enable;
            //tbFirstName.Enabled = enable;
            //tbMiddleName.Enabled = enable;
            //tbLastName.Enabled = enable;
            //tbEmail.Enabled = enable;
            //tbFirstName1.Enabled = enable;
            //tbMiddleName1.Enabled = enable;
            //tbLastName1.Enabled = enable;
            //tbEmail1.Enabled = enable;
            //chkAll.Enabled = enable;
            //chkAPIS.Enabled = enable;
            //chkATSTS3.Enabled = enable;
            //chkMapping.Enabled = enable;
            //chkUVFuel.Enabled = enable;
            //chkUvtriplink.Enabled = enable;
            //tbUvtriplinkName.Enabled = enable;
            //tbAccountNumber.Enabled = enable;
            //tbApisID.Enabled = enable;
            //tbApisPassword.Enabled = enable;
            //tbCustomerID.Enabled = enable;
            //tbLicenseNumber.Enabled = enable;
            //CmbLicense.Enabled = enable;
            //tbLicenseStartDate.Enabled = enable;
            //tbUserCount.Enabled = enable;
            //tbAircraftCount.Enabled = enable;
            //tbMaintainenceExpiryDate.Enabled = enable;
            //chkUWASupportAccess.Enabled = enable;
            //chkActiveLicense.Enabled = enable;
            //tbCustomerID.Enabled = enable;
            //tbCustomerName.Enabled = enable;
            //tbFirstName.Enabled = enable;
            //tbMiddleName.Enabled = enable;
            //tbLastName.Enabled = enable;
            //tbEmail.Enabled = enable;
            //tbFirstName1.Enabled = enable;
            //tbMiddleName1.Enabled = enable;
            //tbLastName1.Enabled = enable;
            //tbEmail1.Enabled = enable;
            //chkAll.Enabled = enable;
            //chkAPIS.Enabled = enable;
            //chkATSTS3.Enabled = enable;
            //chkMapping.Enabled = enable;
            //chkUVFuel.Enabled = enable;
            //chkUvtriplink.Enabled = enable;
            //tbUvtriplinkName.Enabled = enable;
            //tbAccountNumber.Enabled = enable;
            //tbApisID.Enabled = enable;
            //tbApisPassword.Enabled = enable;
            //tbCustomerID.Enabled = enable;
            //tbLicenseNumber.Enabled = enable;
            //CmbLicense.Enabled = enable;
            //tbLicenseStartDate.Enabled = enable;
            //tbUserCount.Enabled = enable;
            //tbAircraftCount.Enabled = enable;
            //tbMaintainenceExpiryDate.Enabled = enable;
            //chkUWASupportAccess.Enabled = enable;
            //chkActiveLicense.Enabled = enable;
            //dgModule.Enabled = enable;
            //tbHomebase.Enabled = enable;
            //tbIntUserName.Enabled = enable;
            //btnCancel.Visible = enable;
            //btnSaveChanges.Visible = enable;
            //Button1.Disabled = !enable;

            tbCustomerID.Enabled = enable;
            tbCustomerName.Enabled = enable;
            tbFirstName.Enabled = enable;
            tbMiddleName.Enabled = enable;
            tbLastName.Enabled = enable;
            tbEmail.Enabled = enable;
            tbFirstName1.Enabled = enable;
            tbMiddleName1.Enabled = enable;
            tbLastName1.Enabled = enable;
            tbEmail1.Enabled = enable;
            chkAll.Enabled = enable;
            chkAPIS.Enabled = enable;
            chkATSTS3.Enabled = enable;
            chkMapping.Enabled = enable;
            chkUVFuel.Enabled = enable;
            chkUvtriplink.Enabled = enable;
            tbUvtriplinkName.Enabled = enable;
            tbAccountNumber.Enabled = enable;
            tbApisID.Enabled = enable;
            tbApisPassword.Enabled = enable;
            tbCustomerID.Enabled = enable;
            tbLicenseNumber.Enabled = enable;
            CmbLicense.Enabled = enable;
            tbLicenseStartDate.Enabled = enable;
            tbUserCount.Enabled = enable;
            tbAircraftCount.Enabled = enable;
            tbMaintainenceExpiryDate.Enabled = enable;
            chkUWASupportAccess.Enabled = enable;
            chkActiveLicense.Enabled = enable;
            dgModule.Enabled = enable;
            tbHomebase.Enabled = enable;
            tbIntUserName.Enabled = enable;
            btnCancel.Visible = enable;
            btnSaveChanges.Visible = enable;
            Button1.Disabled = !enable;
            dgFuelVendor.Enabled = enable;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void DisplayEditForm()
        {
            PopulateData();

        }

        /// <summary>
        /// To Clear the Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClearForm()
        {
            //tbCustomerID.Text = string.Empty;
            //tbCustomerName.Text = string.Empty;
            //tbFirstName.Text = string.Empty;
            //tbMiddleName.Text = string.Empty;
            //tbLastName.Text = string.Empty;
            //tbEmail.Text = string.Empty;
            //tbFirstName1.Text = string.Empty;
            //tbMiddleName1.Text = string.Empty;
            //tbLastName1.Text = string.Empty;
            //tbEmail1.Text = string.Empty;
            //chkAll.Checked = false;
            //chkAPIS.Checked = false;
            //chkATSTS3.Checked = false;
            //chkMapping.Checked = false;
            //chkUVFuel.Checked = false;
            //chkUvtriplink.Checked = false;
            //tbUvtriplinkName.Text = string.Empty;
            //tbAccountNumber.Text = string.Empty;
            //tbApisID.Text = string.Empty;
            //tbApisPassword.Text = string.Empty;
            //tbCustomerID.Text = string.Empty;
            //tbLicenseNumber.Text = string.Empty;
            //tbLicenseStartDate.Text = string.Empty;
            //tbUserCount.Text = string.Empty;
            //tbAircraftCount.Text = string.Empty;
            //tbMaintainenceExpiryDate.Text = string.Empty;
            //chkUWASupportAccess.Checked = false;
            //chkActiveLicense.Checked = true;
            //tbCustomerID.Text = string.Empty;
            //tbCustomerName.Text = string.Empty;
            //tbFirstName.Text = string.Empty;
            //tbMiddleName.Text = string.Empty;
            //tbLastName.Text = string.Empty;
            //tbEmail.Text = string.Empty;
            //tbFirstName1.Text = string.Empty;
            //tbMiddleName1.Text = string.Empty;
            //tbLastName1.Text = string.Empty;
            //tbEmail1.Text = string.Empty;
            //chkAll.Checked = false;
            //chkAPIS.Checked = false;
            //chkATSTS3.Checked = false;
            //chkMapping.Checked = false;
            //chkUVFuel.Checked = false;
            //chkUvtriplink.Checked = false;
            //tbUvtriplinkName.Text = string.Empty;
            //tbAccountNumber.Text = string.Empty;
            //tbApisID.Text = string.Empty;
            //tbApisPassword.Text = string.Empty;
            //tbCustomerID.Text = string.Empty;
            //tbLicenseNumber.Text = string.Empty;
            ////Ramesh: Commented "PR" to fix issue in selecting EV after a new customer is created.
            ////CmbLicense.SelectedItem.Text = "PR";
            //CmbLicense.SelectedIndex = 0;
            //tbLicenseStartDate.Text = string.Empty;
            //tbUserCount.Text = string.Empty;
            //tbAircraftCount.Text = string.Empty;
            //tbMaintainenceExpiryDate.Text = string.Empty;
            //tbHomebase.Text = string.Empty;
            //tbIntUserName.Text = string.Empty;
            //BindModuleGridValue();

            tbCustomerID.Text = string.Empty;
            tbCustomerName.Text = string.Empty;
            tbFirstName.Text = string.Empty;
            tbMiddleName.Text = string.Empty;
            tbLastName.Text = string.Empty;
            tbEmail.Text = string.Empty;
            tbFirstName1.Text = string.Empty;
            tbMiddleName1.Text = string.Empty;
            tbLastName1.Text = string.Empty;
            tbEmail1.Text = string.Empty;
            chkAll.Checked = false;
            chkAPIS.Checked = false;
            chkATSTS3.Checked = false;
            chkMapping.Checked = false;
            chkUVFuel.Checked = false;
            chkUvtriplink.Checked = false;
            tbUvtriplinkName.Text = string.Empty;
            tbAccountNumber.Text = string.Empty;
            tbApisID.Text = string.Empty;
            tbApisPassword.Text = string.Empty;
            tbCustomerID.Text = string.Empty;
            tbLicenseNumber.Text = string.Empty;
            tbLicenseStartDate.Text = string.Empty;
            tbUserCount.Text = string.Empty;
            tbAircraftCount.Text = string.Empty;
            tbMaintainenceExpiryDate.Text = string.Empty;
            chkUWASupportAccess.Checked = false;
            chkActiveLicense.Checked = true;
            //Ramesh: Commented "PR" to fix issue in selecting EV after a new customer is created.
            //CmbLicense.SelectedItem.Text = "PR";
            CmbLicense.SelectedIndex = 0;
            tbHomebase.Text = string.Empty;
            tbIntUserName.Text = string.Empty;
            BindModuleGridValue();

        }
        protected void chkAll_OnCheckedChanged(object sender, EventArgs e)
        {
            chkAPIS.Checked = chkATSTS3.Checked = chkMapping.Checked = chkUVFuel.Checked = chkUvtriplink.Checked = chkAll.Checked;
        }
        protected void chkOthers_OnCheckedChanged(object sender, EventArgs e)
        {
            chkAll.Checked = chkAPIS.Checked && chkATSTS3.Checked && chkMapping.Checked && chkUVFuel.Checked && chkUvtriplink.Checked;
        }
        protected void tbEmail_TextChanged(object sender, EventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient client = new AdminServiceClient())
                        {
                            var user = client.GetUserByEMailId(tbEmail.Text).EntityList;

                            if (user != null && (user.Where(y => y.CustomerID != Convert.ToInt64(tbCustomerID.Text)).Count() > 0 || user.Where(x => x.UserName.Trim().ToUpper() != tbIntUserName.Text.Trim().ToUpper()).Count() > 0))
                            {
                                tbEmailInvalid.IsValid = false;
                                tbEmail.Focus();
                            }

                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }

        protected void tbEmail_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        using (AdminService.AdminServiceClient client = new AdminServiceClient())
                        {
                            var user = client.GetUserByEMailId(tbEmail.Text).EntityList;
                            if (user != null && (user.Where(y => y.CustomerID != Convert.ToInt64(tbCustomerID.Text)).Count() > 0 || user.Where(x => x.UserName.Trim().ToUpper() != tbIntUserName.Text.Trim().ToUpper()).Count() > 0))
                            {
                                e.IsValid = false;
                                tbEmail.Focus();
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }
        private bool CheckCanEdit()
        {
            using (AdminServiceClient client = new AdminServiceClient())
            {
                if (string.IsNullOrEmpty(tbUserCount.Text))
                    return true;
                var UsedUserCount = client.GetUsedUserCount(Convert.ToInt64(tbCustomerID.Text));
                return UsedUserCount <= Convert.ToInt32(tbUserCount.Text);
            }
        }
        private bool CheckCanEditFleet()
        {
            using (AdminServiceClient client = new AdminServiceClient())
            {
                if (string.IsNullOrEmpty(tbAircraftCount.Text))
                    return true;
                var UsedAircraftCount = client.GetAircraftUsedCount(Convert.ToInt64(tbCustomerID.Text));
                return UsedAircraftCount <= Convert.ToInt32(tbAircraftCount.Text);
            }
        }

        /// <summary>
        /// To validate Home base
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void tbHomeBase_TextChanged(object sender, EventArgs e)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnHomebaseId.Value = "";
                        if (!string.IsNullOrEmpty(tbHomebase.Text))
                        {
                            using (AdminService.AdminServiceClient AdminService = new AdminService.AdminServiceClient())
                            {
                                Int64 UwaCustomerId = AdminService.GetUWACustomerId();
                                var ObjRetVal = AdminService.GetAirportMasterInfo(UwaCustomerId);
                                if (ObjRetVal.ReturnFlag)
                                {

                                    var airportList = ObjRetVal.EntityList.Where(x => x.IcaoID.ToString().ToUpper().Trim().Equals(tbHomebase.Text.ToUpper().Trim())).ToList();
                                    if (airportList != null && airportList.Count > 0)
                                    {
                                        hdnHomebaseId.Value = airportList[0].AirportID.ToString();
                                        tbHomebase.Text = airportList[0].IcaoID;
                                        rfvHomebaseInvalid.Text = "";
                                        tbAircraftCount.Focus();
                                    }
                                    else
                                    {
                                        hdnHomebaseId.Value = "";
                                        rfvHomebaseInvalid.IsValid = false;
                                        tbHomebase.Focus();
                                    }
                                }
                                else
                                {
                                    hdnHomebaseId.Value = "";
                                    rfvHomebaseInvalid.IsValid = false;
                                    tbHomebase.Focus();
                                }
                            }
                        }
                        else
                        {
                            hdnHomebaseId.Value = "";
                            rfvHomebaseInvalid.IsValid = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }

        }

        protected void rfvHomebaseInvalid_ServerValidate(object sender, ServerValidateEventArgs e)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        hdnHomebaseId.Value = "";
                        if (!string.IsNullOrEmpty(tbHomebase.Text))
                        {
                            using (AdminService.AdminServiceClient AdminService = new AdminService.AdminServiceClient())
                            {
                                Int64 UwaCustomerId = AdminService.GetUWACustomerId();
                                var ObjRetVal = AdminService.GetAirportMasterInfo(UwaCustomerId);
                                if (ObjRetVal.ReturnFlag)
                                {

                                    var airportList = ObjRetVal.EntityList.Where(x => x.IcaoID.ToString().ToUpper().Trim().Equals(tbHomebase.Text.ToUpper().Trim())).ToList();
                                    if (airportList != null && airportList.Count > 0)
                                    {
                                        hdnHomebaseId.Value = airportList[0].AirportID.ToString();
                                        tbHomebase.Text = airportList[0].IcaoID;
                                        rfvHomebaseInvalid.Text = "";
                                        tbAircraftCount.Focus();
                                    }
                                    else
                                    {
                                        hdnHomebaseId.Value = "";
                                        rfvHomebaseInvalid.IsValid = false;
                                        tbHomebase.Focus();
                                    }
                                }
                                else
                                {
                                    hdnHomebaseId.Value = "";
                                    rfvHomebaseInvalid.IsValid = false;
                                    tbHomebase.Focus();
                                }
                            }
                        }
                        else
                        {
                            hdnHomebaseId.Value = "";
                            rfvHomebaseInvalid.IsValid = false;
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.UserMaster);
                }
            }
        }

        private void BindFuelVendorGridValue()
        {
            using (var fuelVendorService = new AdminServiceClient())
            {
                var fuelVendorServiceInfo = fuelVendorService.GetCustomerFuelVendorAccessList(Convert.ToInt64(tbCustomerID.Text));
                if (fuelVendorServiceInfo.ReturnFlag && fuelVendorServiceInfo.EntityList.Count > 0)
                {
                    List<CustomerFuelVendorAccessList> oCustomerFuelVendorAccessList = fuelVendorServiceInfo.EntityList.ToList();
                    dgFuelVendor.DataSource = oCustomerFuelVendorAccessList;
                    dgFuelVendor.DataBind();
                }
            }
        }
    }

}
