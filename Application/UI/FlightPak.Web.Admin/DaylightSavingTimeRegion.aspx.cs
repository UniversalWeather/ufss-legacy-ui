﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Web.Admin.AdminService;
//For Tracing and Exceptioon Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Web.Admin
{
    public partial class DaylightSavingTimeRegion : BasePage
    {
        private bool IsEmptyCheck = true;
        private bool DSTPageNavigated = false;
        private string strDstID = "";
        string DateFormat;
        private ExceptionManager exManager;
        private List<string> DSTRegionCode = new List<string>();
        protected void Page_Load(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        // Set Logged-in User Name
                        //FPPrincipal identity = (FPPrincipal)Session[Session.SessionID];
                        //if (identity != null && identity.Identity._fpSettings._ApplicationDateFormat != null)
                        //{
                        //    DateFormat = identity.Identity._fpSettings._ApplicationDateFormat.ToString().Trim();
                        //}
                        //else
                        //{
                        DateFormat = "MM/dd/yyyy";
                        //}
                        ((RadDatePicker)ucDatePicker.FindControl("RadDatePicker1")).DateInput.DateFormat = DateFormat;
                        // Grid Control could be ajaxified when the page is initially loaded.
                        RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(dgDaylightSavingTimeRegion, dgDaylightSavingTimeRegion, RadAjaxLoadingPanel1);
                        // Store the clientID of the grid to reference it later on the client
                        RadAjaxManager.GetCurrent(Page).ResponseScripts.Add(string.Format(CultureInfo.CurrentCulture, "window['gridId'] = '{0}';", dgDaylightSavingTimeRegion.ClientID));
                        if (!IsPostBack)
                        {
                            //To check the page level access.
                            DefaultSelection(true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// Default Selection on First GridRow Select
        /// </summary>
        private void DefaultSelection(bool BindDataSwitch)
        {
            if (BindDataSwitch)
            {
                dgDaylightSavingTimeRegion.Rebind();
            }
            if (dgDaylightSavingTimeRegion.MasterTableView.Items.Count > 0)
            {
                dgDaylightSavingTimeRegion.SelectedIndexes.Add(0);
                Session["SelectedDSTRegionID"] = dgDaylightSavingTimeRegion.Items[0].GetDataKeyValue("DSTRegionID").ToString();
                ReadOnlyForm();
            }
            else
            {
                ClearForm();
                EnableForm(false);
            }
            //GridEnable(true, true, true);
        }
        /// <summary>
        /// To change the page index on paging        
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_PageIndexChanged(object source, GridPageChangedEventArgs e)
        {

            //Handle methods throguh exception manager with return flag
            DSTPageNavigated = true;

        }
        /// <summary>
        /// PreRender
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_PreRender(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (hdnSave.Value != "Save")
                        {
                            SelectItem();
                        }
                        else if (DSTPageNavigated)
                        {
                            SelectItem();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// To set focus to the empty field on save button click
        /// </summary>
        /// <param name="group"></param>
        public override void Validate(string group)
        {
            base.Validate(group);
            // get the first validator that failed
            var validator = GetValidators(group)
            .OfType<BaseValidator>()
            .FirstOrDefault(v => !v.IsValid);
            // set the focus to the control
            // that the validator targets
            if (validator != null)
            {
                Control target = validator
                .NamingContainer
                .FindControl(validator.ControlToValidate);
                if (target != null)
                {
                    target.Focus();
                    IsEmptyCheck = false;
                }
            }
        }
        /// <summary>
        /// To highlight the selected item 
        /// </summary>        
        private void SelectItem()
        {
            if (Session["SelectedDSTRegionID"] != null)
            {
                //GridDataItem items = (GridDataItem)Session["SelectedItem"];
                //string code = items.GetDataKeyValue("DelayTypeCD").ToString();
                string ID = Session["SelectedDSTRegionID"].ToString();
                foreach (GridDataItem item in dgDaylightSavingTimeRegion.MasterTableView.Items)
                {
                    if (item.GetDataKeyValue("DSTRegionID").ToString().Trim() == ID)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
            else
            {
                DefaultSelection(false);
            }
        }
        /// <summary>
        /// Datagrid Item Created for DayLightSavingTimeRegion Insert and edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_ItemCreated(object sender, GridItemEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (e.Item is GridCommandItem)
                        {
                            //Added based on UWA requirement.
                            LinkButton editButton = (e.Item as GridCommandItem).FindControl("lnkInitEdit") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(editButton, DivExternalForm, RadAjaxLoadingPanel1);
                            LinkButton insertButton = (e.Item as GridCommandItem).FindControl("lnkInitInsert") as LinkButton;
                            RadAjaxManager.GetCurrent(Page).AjaxSettings.AddAjaxSetting(insertButton, DivExternalForm, RadAjaxLoadingPanel1);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// Bind DayLightSavingTimeRegion Data into Grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_BindData(object sender, GridNeedDataSourceEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        using (AdminService.AdminServiceClient objDSTService = new AdminService.AdminServiceClient())
                        {
                            var objDstVal = objDSTService.GetDSTRegionList();
                            if (objDstVal.ReturnFlag == true)
                            {
                                dgDaylightSavingTimeRegion.DataSource = objDstVal.EntityList;
                            }
                            Session["DstCD"] = objDstVal.EntityList.ToList();
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// Datagrid Item Command for DayLightSavingTimeRegion Insert and Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_ItemCommand(object sender, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        switch (e.CommandName)
                        {
                            case RadGrid.EditCommandName:
                                e.Canceled = true;
                                e.Item.Selected = true;
                                hdnSave.Value = "Update";
                                if (Session["SelectedDSTRegionID"] != null)
                                {
                                    DisplayEditForm();
                                    // GridEnable(false, true, false);
                                    tbDescription.Focus();
                                    SelectItem();
                                }
                                break;
                            case RadGrid.InitInsertCommandName:
                                e.Canceled = true;
                                tbCode.Focus();
                                dgDaylightSavingTimeRegion.SelectedIndexes.Clear();
                                DisplayInsertForm();
                                // GridEnable(true, false, false);
                                dgDaylightSavingTimeRegion.Rebind();
                                break;
                            case "UpdateEdited":
                                dgDaylightSavingTimeRegion_UpdateCommand(sender, e);
                                break;
                            case "Filter":
                                foreach (GridColumn column in e.Item.OwnerTableView.Columns)
                                {
                                    column.CurrentFilterValue = string.Empty;
                                    column.CurrentFilterFunction = GridKnownFunction.NoFilter;
                                }
                                break;
                            default:
                                break;
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }

        }
        /// <summary>
        /// Update Command for updating the values of DayLightSavingTimeRegion  in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_UpdateCommand(object source, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (Session["SelectedDSTRegionID"] != null)
                        {
                            using (AdminService.AdminServiceClient objDstService = new AdminService.AdminServiceClient())
                            {
                                var RetVal = objDstService.UpdateDSTRegion(GetItems());
                                ///////Update Method UnLock
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                //  GridEnable(true, true, true);
                                DefaultSelection(true);
                                if (RetVal.ReturnFlag == true)
                                {
                                    //ShowSuccessMessage();
                                }
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// DaylightSavingTimeRegion Insert Command for inserting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_InsertCommand(object source, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        e.Canceled = true;
                        if (CheckAllReadyExist())
                        {
                            cvDstCode.IsValid = false;
                            tbCode.Focus();
                        }
                        else
                        {
                            using (AdminService.AdminServiceClient objDSTRegionService = new AdminService.AdminServiceClient())
                            {
                                var RetVal = objDSTRegionService.AddDSTRegion(GetItems());
                                dgDaylightSavingTimeRegion.Rebind();
                                DefaultSelection(false);
                                if (RetVal.ReturnFlag == true)
                                {
                                    // ShowSuccessMessage();
                                }
                            }
                        }

                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }

        }
        /// <summary>
        /// Save and Update DayLightSavingTimeRegion Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveChanges_Click(object sender, EventArgs e)
        {

            //Handle methods throguh exception manager with return flag
            if (IsEmptyCheck)
            {
                bool IsValidDateCheck = true;
                string StartDateTime;
                string EndDateTime;
                string alertMsg;
                hdnStartDate.Value = (((TextBox)ucStartDate.FindControl("tbDate")).Text);
                hdnFinishedDate.Value = (((TextBox)ucFinishedDate.FindControl("tbDate")).Text);
                StartDateTime = rmtbStartTime.TextWithLiterals;
                EndDateTime = rmtbFinishTime.TextWithLiterals;

                if (!string.IsNullOrEmpty(hdnStartDate.Value) || !string.IsNullOrEmpty(hdnFinishedDate.Value))
                {
                    if (string.IsNullOrEmpty(hdnStartDate.Value) || string.IsNullOrEmpty(hdnFinishedDate.Value))
                    {
                        alertMsg = "radalert('Please enter both Start and End Date', 360, 50, 'DaylightSavingTimeRegion');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        IsValidDateCheck = false;
                    }
                }


                if ((IsValidDateCheck) && (!string.IsNullOrEmpty(hdnStartDate.Value)) && (StartDateTime != "") && (EndDateTime != "") && (!string.IsNullOrEmpty(hdnFinishedDate.Value)))
                {

                    hdnStartDate.Value = string.Format("{0} {1}", hdnStartDate.Value, StartDateTime, CultureInfo.InvariantCulture);
                    hdnFinishedDate.Value = string.Format("{0} {1}", hdnFinishedDate.Value, EndDateTime, CultureInfo.InvariantCulture);

                    DateTime DateSource = DateTime.MinValue;
                    IFormatProvider yyyymmddFormat = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DateSource = DateTime.Parse((hdnStartDate.Value).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
                    DateTime DateSource1 = DateTime.MinValue;
                    IFormatProvider yyyymmddFormat1 = new System.Globalization.CultureInfo(CultureInfo.CurrentCulture.Name, true);
                    DateSource1 = DateTime.Parse((hdnFinishedDate.Value).ToString(yyyymmddFormat), System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
                    // if ((Convert.ToDateTime(hdnStartDate.Value, CultureInfo.InvariantCulture)) > (Convert.ToDateTime(hdnFinishedDate.Value, CultureInfo.InvariantCulture)))
                    if (DateSource > DateSource1)
                    {
                        alertMsg = "radalert('Start date  should be lesser than end date', 360, 50, 'DaylightSavingTimeRegion');";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "radalert", alertMsg, true);
                        IsValidDateCheck = false;
                    }
                }
                if (IsValidDateCheck)
                {
                    if (hdnSave.Value == "Update")
                    {
                        (dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.UpdateCommandName, string.Empty);
                    }
                    else
                    {
                        (dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0] as GridCommandItem).FireCommandEvent(RadGrid.PerformInsertCommandName, string.Empty);
                    }
                }
            }

        }
        /// <summary>
        /// Cancel DayLightSavingTimeRegion  Form Informations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {

            //Handle methods throguh exception manager with return flag
            DefaultSelection(true);
            Session["SelectedDSTRegionID"] = null;

        }
        /// <summary>
        ///  DaylightSavingTimeRegion Delete Command for deleting value in table
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_DeleteCommand(object source, GridCommandEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(source, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (Session["SelectedDSTRegionID"] != null)
                        {
                            using (AdminService.AdminServiceClient objDstService = new AdminService.AdminServiceClient())
                            {
                                AdminService.DSTRegion DstType = new AdminService.DSTRegion();
                                string Code = Session["SelectedDSTRegionID"].ToString();
                                strDstID = "";
                                foreach (GridDataItem Item in dgDaylightSavingTimeRegion.MasterTableView.Items)
                                {
                                    if (Item.GetDataKeyValue("DSTRegionID").ToString().Trim() == Code.Trim())
                                    {
                                        if (Item.GetDataKeyValue("DSTRegionCD") != null)
                                        {
                                            DstType.DSTRegionCD = Item.GetDataKeyValue("DSTRegionCD").ToString().Trim();
                                        }
                                        if (Item.GetDataKeyValue("DSTRegionID") != null)
                                        {
                                            strDstID = Item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                                        }
                                        break;
                                    }
                                }
                                DstType.DSTRegionID = Convert.ToInt64(strDstID);
                                DstType.IsDeleted = true;
                                //Lock will happen from UI
                                objDstService.DeleteDSTRegion(DstType);
                                e.Item.OwnerTableView.Rebind();
                                e.Item.Selected = true;
                                DefaultSelection(true);
                            }
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// Bind Selected Item in the Session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgDaylightSavingTimeRegion_SelectedIndexChanged(object sender, EventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (btnSaveChanges.Visible == false)
                        {
                            GridDataItem item = dgDaylightSavingTimeRegion.SelectedItems[0] as GridDataItem;
                            if (item.GetDataKeyValue("DSTRegionID") != null)
                            {
                                Session["SelectedDSTRegionID"] = item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                            }
                            if (btnSaveChanges.Visible == false)
                            {
                                ReadOnlyForm();
                            }
                            //GridEnable(true, true, true);
                        }
                    }, FlightPak.Common.Constants.Policy.UILayer);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// AjaxSettingCreating event occurs just before an AjaxSetting is added to the RadAjaxManager AjaxSettings collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAjaxManager1_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sender, e))
            {
                try
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        if (e.Initiator.ID.IndexOf("btnSaveChanges", StringComparison.Ordinal) > -1)
                        {
                            e.Updated = dgDaylightSavingTimeRegion;
                        }
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);

                }
                catch (Exception ex)
                {
                    //The exception will be handled, logged and replaced by our custom exception. 
                    ProcessErrorMessage(ex, ModuleNameConstants.Database.DSTRegion);
                }
            }


        }
        /// <summary>
        /// Method to fetch the values from controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private DSTRegion GetItems()
        {
            decimal checkforDecimal;
            AdminService.DSTRegion objDSTRegion = new AdminService.DSTRegion();
            //Handle methods throguh exception manager with return flag
            AdminService.DSTRegion DstServiceType = new AdminService.DSTRegion();
            DstServiceType.DSTRegionCD = tbCode.Text;
            DstServiceType.DSTRegionName = tbDescription.Text;
            DstServiceType.CityList = tbCityList.Text.Trim();
            DstServiceType.LastUpdTS = DateTime.UtcNow;
            if (hdnSave.Value == "Update")
            {
                //GridDataItem Item = (GridDataItem)dgMetroCity.SelectedItems[0];
                if (Session["SelectedDSTRegionID"] != null)
                {
                    DstServiceType.DSTRegionID = Convert.ToInt64(Session["SelectedDSTRegionID"].ToString());
                }
            }
            else
            {
                DstServiceType.DSTRegionID = 0;
            }
            if (!string.IsNullOrEmpty(hdnStartDate.Value))
            {
                DstServiceType.StartDT = FormatDate(hdnStartDate.Value, DateFormat);// Convert.ToDateTime(hdnStartDate.Value);
            }
            if (!string.IsNullOrEmpty(hdnFinishedDate.Value))
            {
                DstServiceType.EndDT = FormatDate(hdnFinishedDate.Value, DateFormat);//Convert.ToDateTime(hdnFinishedDate.Value);
            }
            if (tbutcoffset.Text != null && tbutcoffset.Text != string.Empty)
                DstServiceType.OffSet = Convert.ToDecimal(tbutcoffset.Text);
            //decimal.TryParse(tbutcoffset.Text.Substring(0, tbutcoffset.Text.IndexOf(".")) + "." + tbutcoffset.Text.Substring(tbutcoffset.Text.IndexOf(".") + 1, 2), out checkforDecimal) ? checkforDecimal : 0;
            DstServiceType.IsDeleted = false;
            return DstServiceType;
        }
        protected void tbCode_textchange(object sender, EventArgs e)
        {
            CheckAllReadyExist();
        }
        /// <summary>
        /// Display Insert  form, when click on Add Button
        /// </summary>
        protected void DisplayInsertForm()
        {

            hdnSave.Value = "Save";
            ClearForm();
            dgDaylightSavingTimeRegion.Rebind();
            EnableForm(true);
        }
        /// <summary>
        /// To check whether the code is unique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private bool CheckAllReadyExist()
        {
            bool returnval = false;
            //Handle methods throguh exception manager with return flag
            List<DSTRegion> DSTRegionList = new List<DSTRegion>();
            DSTRegionList = ((List<DSTRegion>)Session["DstCD"]).Where(x => x.DSTRegionCD.ToString().ToUpper().Trim().Equals(tbCode.Text.ToString().ToUpper().Trim())).ToList<DSTRegion>();
            if (DSTRegionList.Count != 0)
            {
                returnval = true;
                cvDstCode.IsValid = false;
                tbCode.Focus();
            }
            else
            {
                tbDescription.Focus();
            }
            return returnval;
        }
        /// <summary>
        /// Display Edit form, when click on Add Button
        /// </summary>
        protected void DisplayEditForm()
        {
            if (Session["SelectedDSTRegionID"] != null)
            {
                strDstID = "";
                foreach (GridDataItem Item in dgDaylightSavingTimeRegion.MasterTableView.Items)
                {
                    if (Item.GetDataKeyValue("DSTRegionID").ToString().Trim() == Session["SelectedDSTRegionID"].ToString().Trim())
                    {
                        if (Item.GetDataKeyValue("DSTRegionCD") != null)
                        {
                            tbCode.Text = Item.GetDataKeyValue("DSTRegionCD").ToString().Trim();
                        }
                        if (Item.GetDataKeyValue("DSTRegionID") != null)
                        {
                            strDstID = Item.GetDataKeyValue("DSTRegionID").ToString().Trim();
                        }
                        if (Item.GetDataKeyValue("DSTRegionName") != null)
                        {
                            tbDescription.Text = Item.GetDataKeyValue("DSTRegionName").ToString().Trim();
                        }
                        else
                        {
                            tbDescription.Text = string.Empty;
                        }
                        if ((Item.GetDataKeyValue("StartDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
                        {
                            string startDate = Item.GetDataKeyValue("StartDT").ToString();
                            TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                            string[] startDateTime = startDate.Split(' ');
                            string[] startTime;

                            if (startDate.Contains("AM") || startDate.Contains("PM"))
                            {
                                //tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDate)).Substring(0, 11).Trim();// String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", startDate).Substring(0, 10);
                                tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDateTime[0].ToString())).Trim();

                                if (startDateTime[1].Contains("AM"))
                                {
                                    startTime = startDateTime[2].Split(':');

                                    if (startTime[0] == "12")
                                    {
                                        rmtbStartTime.TextWithLiterals = "00:" + startTime[1];
                                    }
                                    else
                                    {
                                        rmtbStartTime.TextWithLiterals = startTime[0] + ":" + startTime[1];
                                    }
                                }
                                else if (startDateTime[1].Contains("PM"))
                                {
                                    startTime = startDateTime[2].Split(':');

                                    if (startTime[0] == "12")
                                    {
                                        rmtbStartTime.TextWithLiterals = "12:" + startTime[1];
                                    }
                                    else
                                    {
                                        rmtbStartTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(startTime[0]) + 12) + ":" + startTime[1];
                                    }
                                }
                                else if (startDateTime[2].Contains("AM"))
                                {
                                    startTime = startDateTime[1].Split(':');

                                    if (startTime[0] == "12")
                                    {
                                        rmtbStartTime.TextWithLiterals = "00:" + startTime[1];
                                    }
                                    else
                                    {
                                        rmtbStartTime.TextWithLiterals = startTime[0] + ":" + startTime[1];
                                    }
                                }
                                else if (startDateTime[2].Contains("PM"))
                                {
                                    startTime = startDateTime[1].Split(':');

                                    if (startTime[0] == "12")
                                    {
                                        rmtbStartTime.TextWithLiterals = "12:" + startTime[1];
                                    }
                                    else
                                    {
                                        rmtbStartTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(startTime[0]) + 12) + ":" + startTime[1];
                                    }
                                }
                            }
                            else
                            {
                                startTime = startDateTime[1].Split(':');

                                tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDateTime[0].ToString())).Trim();
                                rmtbStartTime.TextWithLiterals = startTime[0] + ":" + startTime[1];
                            }
                        }
                        else
                        {
                            TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                            tbStartDate.Text = string.Empty;
                            rmtbStartTime.Text = "00:00";
                        }
                        //if ((Item.GetDataKeyValue("StartDT") != null) && (string.IsNullOrEmpty(DateFormat)))
                        //{
                        //    string startDate = Item.GetDataKeyValue("StartDT").ToString();
                        //    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                        //    tbStartDate.Text = Convert.ToDateTime(startDate).ToString().Substring(0, 11);// String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", startDate).Substring(0, 10);
                        //    rmtbStartTime.Text = startDate.Substring(9, 2) + ":" + startDate.Substring(12, 2);
                        //}
                        //else
                        //{
                        //    TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                        //    tbStartDate.Text = string.Empty;
                        //    rmtbStartTime.Text = string.Empty;
                        //}
                        if ((Item.GetDataKeyValue("EndDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
                        {
                            string endDate = Item.GetDataKeyValue("EndDT").ToString();
                            TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                            string[] endDateTime = endDate.Split(' ');
                            string[] endTime;

                            if (endDate.Contains("AM") || endDate.Contains("PM"))
                            {
                                //tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDate)).Substring(0, 11).Trim();//String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", enddate).Substring(0, 10);                
                                tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDateTime[0].ToString())).Trim();

                                if (endDateTime[1].Contains("AM"))
                                {
                                    endTime = endDateTime[2].Split(':');

                                    if (endTime[0] == "12")
                                    {
                                        rmtbFinishTime.TextWithLiterals = "00:" + endTime[1];
                                    }
                                    else
                                    {
                                        rmtbFinishTime.TextWithLiterals = endTime[0] + ":" + endTime[1];
                                    }
                                }
                                else if (endDateTime[1].Contains("PM"))
                                {
                                    endTime = endDateTime[2].Split(':');

                                    if (endTime[0] == "12")
                                    {
                                        rmtbFinishTime.TextWithLiterals = "12:" + endTime[1];
                                    }
                                    else
                                    {
                                        rmtbFinishTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(endTime[0]) + 12) + ":" + endTime[1];
                                    }
                                }
                                else if (endDateTime[2].Contains("AM"))
                                {
                                    endTime = endDateTime[1].Split(':');

                                    if (endTime[0] == "12")
                                    {
                                        rmtbFinishTime.TextWithLiterals = "00:" + endTime[1];
                                    }
                                    else
                                    {
                                        rmtbFinishTime.TextWithLiterals = endTime[0] + ":" + endTime[1];
                                    }
                                }
                                else if (endDateTime[2].Contains("PM"))
                                {
                                    endTime = endDateTime[1].Split(':');

                                    if (endTime[0] == "12")
                                    {
                                        rmtbFinishTime.TextWithLiterals = "12:" + endTime[1];
                                    }
                                    else
                                    {
                                        rmtbFinishTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(endTime[0]) + 12) + ":" + endTime[1];
                                    }
                                }
                            }
                            else
                            {
                                endTime = endDateTime[1].Split(':');

                                tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDateTime[0].ToString())).Trim();
                                rmtbFinishTime.TextWithLiterals = endTime[0] + ":" + endTime[1];
                            }
                        }
                        else
                        {
                            TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                            tbEndDate.Text = string.Empty;
                            rmtbFinishTime.Text = "00:00";
                        }
                        //if ((Item.GetDataKeyValue("EndDT") != null) && (string.IsNullOrEmpty(DateFormat)))
                        //{
                        //    string enddate = Item.GetDataKeyValue("EndDT").ToString();
                        //    TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                        //    tbEndDate.Text = Convert.ToDateTime(enddate).ToString().Substring(0, 11);//String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", enddate).Substring(0, 10);
                        //    rmtbFinishTime.Text = enddate.Substring(9, 2) + ":" + enddate.Substring(12, 2);
                        //}
                        //else
                        //{
                        //    TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                        //    tbEndDate.Text = string.Empty;
                        //    rmtbFinishTime.Text = string.Empty;
                        //}
                        if (Item.GetDataKeyValue("OffSet") != null)
                        {
                            tbutcoffset.Text = Item.GetDataKeyValue("OffSet").ToString();
                        }
                        else
                            tbutcoffset.Text = "00.00";
                        if (Item.GetDataKeyValue("CityList") != null)
                        {
                            tbCityList.Text = Item.GetDataKeyValue("CityList").ToString();
                        }
                        else
                            tbCityList.Text = string.Empty;
                        break;
                    }
                }
                EnableForm(true);
            }
        }
        /// <summary>
        /// to Enable the form based on condition
        /// </summary>
        /// <param name="enable"></param>
        protected void EnableForm(bool enable)
        {
            if (hdnSave.Value == "Update" && enable == true)
            {
                tbCode.Enabled = false;
            }
            else
            {
                tbCode.Enabled = enable;
            }
            tbDescription.Enabled = enable;
            TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
            tbStartDate.Enabled = enable;
            rmtbStartTime.Enabled = enable;
            TextBox tbFinishedDate = (TextBox)ucFinishedDate.FindControl("tbDate");
            tbFinishedDate.Enabled = enable;
            rmtbFinishTime.Enabled = enable;
            tbCityList.Enabled = enable;
            tbutcoffset.Enabled = enable;
            btnCancel.Visible = enable;
            btnSaveChanges.Visible = enable;
        }
        /// <summary>
        /// To clear the form
        /// </summary>
        protected void ClearForm()
        {
            tbCode.Text = string.Empty;
            tbDescription.Text = string.Empty;
            tbCityList.Text = string.Empty;
            (((TextBox)ucStartDate.FindControl("tbDate")).Text) = string.Empty;
            rmtbStartTime.Text = "0000";
            (((TextBox)ucFinishedDate.FindControl("tbDate")).Text) = string.Empty;
            rmtbFinishTime.Text = "0000";
            tbutcoffset.Text = "00.00";
        }
        /// <summary>
        /// To display as read only
        /// </summary>
        protected void ReadOnlyForm()
        {
            GridDataItem Item = dgDaylightSavingTimeRegion.SelectedItems[0] as GridDataItem;
            tbCode.Text = Convert.ToString(Item.GetDataKeyValue("DSTRegionCD"));//.ToString();
            if (Item.GetDataKeyValue("DSTRegionName") != null)
            {
                tbDescription.Text = Item.GetDataKeyValue("DSTRegionName").ToString();
            }
            else
            {
                tbDescription.Text = string.Empty;
            }

            //Start Date
            if ((Item.GetDataKeyValue("StartDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
            {
                string startDate = Item.GetDataKeyValue("StartDT").ToString();

                TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                string[] startDateTime = startDate.Split(' ');
                string[] startTime;

                if (startDate.Contains("AM") || startDate.Contains("PM"))
                {
                    //tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDate)).Substring(0, 11).Trim();// String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", startDate).Substring(0, 10);
                    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDateTime[0].ToString())).Trim();

                    if (startDateTime[1].Contains("AM"))
                    {
                        startTime = startDateTime[2].Split(':');

                        if (startTime[0] == "12")
                        {
                            rmtbStartTime.TextWithLiterals = "00:" + startTime[1];
                        }
                        else
                        {
                            rmtbStartTime.TextWithLiterals = startTime[0] + ":" + startTime[1];
                        }
                    }
                    else if (startDateTime[1].Contains("PM"))
                    {
                        startTime = startDateTime[2].Split(':');

                        if (startTime[0] == "12")
                        {
                            rmtbStartTime.TextWithLiterals = "12:" + startTime[1];
                        }
                        else
                        {
                            rmtbStartTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(startTime[0]) + 12) + ":" + startTime[1];
                        }
                    }
                    else if (startDateTime[2].Contains("AM"))
                    {
                        startTime = startDateTime[1].Split(':');

                        if (startTime[0] == "12")
                        {
                            rmtbStartTime.TextWithLiterals = "00:" + startTime[1];
                        }
                        else
                        {
                            rmtbStartTime.TextWithLiterals = startTime[0] + ":" + startTime[1];
                        }
                    }
                    else if (startDateTime[2].Contains("PM"))
                    {
                        startTime = startDateTime[1].Split(':');

                        if (startTime[0] == "12")
                        {
                            rmtbStartTime.TextWithLiterals = "12:" + startTime[1];
                        }
                        else
                        {
                            rmtbStartTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(startTime[0]) + 12) + ":" + startTime[1];
                        }
                    }
                }
                else
                {
                    startTime = startDateTime[1].Split(':');

                    tbStartDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(startDateTime[0].ToString())).Trim();
                    rmtbStartTime.TextWithLiterals = startTime[0] + ":" + startTime[1];
                }
            }
            else
            {
                TextBox tbStartDate = (TextBox)ucStartDate.FindControl("tbDate");
                tbStartDate.Text = string.Empty;
                rmtbStartTime.Text = "00:00";
            }

            //End Date           
            if ((Item.GetDataKeyValue("EndDT") != null) && (!string.IsNullOrEmpty(DateFormat)))
            {
                string endDate = Item.GetDataKeyValue("EndDT").ToString();

                TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                string[] endDateTime = endDate.Split(' ');
                string[] endTime;

                if (endDate.Contains("AM") || endDate.Contains("PM"))
                {
                    //tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDate)).Substring(0, 11).Trim();//String.Format(CultureInfo.InvariantCulture, "{0:" + DateFormat + "}", enddate).Substring(0, 10);                
                    tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDateTime[0].ToString())).Trim();

                    if (endDateTime[1].Contains("AM"))
                    {
                        endTime = endDateTime[2].Split(':');

                        if (endTime[0] == "12")
                        {
                            rmtbFinishTime.TextWithLiterals = "00:" + endTime[1];
                        }
                        else
                        {
                            rmtbFinishTime.TextWithLiterals = endTime[0] + ":" + endTime[1];
                        }
                    }
                    else if (endDateTime[1].Contains("PM"))
                    {
                        endTime = endDateTime[2].Split(':');

                        if (endTime[0] == "12")
                        {
                            rmtbFinishTime.TextWithLiterals = "12:" + endTime[1];
                        }
                        else
                        {
                            rmtbFinishTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(endTime[0]) + 12) + ":" + endTime[1];
                        }
                    }
                    else if (endDateTime[2].Contains("AM"))
                    {
                        endTime = endDateTime[1].Split(':');

                        if (endTime[0] == "12")
                        {
                            rmtbFinishTime.TextWithLiterals = "00:" + endTime[1];
                        }
                        else
                        {
                            rmtbFinishTime.TextWithLiterals = endTime[0] + ":" + endTime[1];
                        }
                    }
                    else if (endDateTime[2].Contains("PM"))
                    {
                        endTime = endDateTime[1].Split(':');

                        if (endTime[0] == "12")
                        {
                            rmtbFinishTime.TextWithLiterals = "12:" + endTime[1];
                        }
                        else
                        {
                            rmtbFinishTime.TextWithLiterals = Convert.ToString(Convert.ToInt32(endTime[0]) + 12) + ":" + endTime[1];
                        }
                    }
                }
                else
                {
                    endTime = endDateTime[1].Split(':');

                    tbEndDate.Text = String.Format("{0: " + DateFormat + "}", Convert.ToDateTime(endDateTime[0].ToString())).Trim();
                    rmtbFinishTime.TextWithLiterals = endTime[0] + ":" + endTime[1];
                }
            }
            else
            {
                TextBox tbEndDate = (TextBox)ucFinishedDate.FindControl("tbDate");
                tbEndDate.Text = string.Empty;
                rmtbFinishTime.Text = "00:00";
            }

            if (Item.GetDataKeyValue("OffSet") != null)
            {
                tbutcoffset.Text = Item.GetDataKeyValue("OffSet").ToString();
            }
            else
                tbutcoffset.Text = "00.00";
            if (Item.GetDataKeyValue("CityList") != null)
            {
                tbCityList.Text = Item.GetDataKeyValue("CityList").ToString();
            }
            else
                tbCityList.Text = string.Empty;
            Label lbLastUpdatedUser;
            lbLastUpdatedUser = (Label)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lbLastUpdatedUser");
            if (Item.GetDataKeyValue("LastUpdUID") != null)
            {
                // lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode("Last Updated User: " + Convert.ToString(Item.GetDataKeyValue("LastUpdUID")));//.ToString();
            }
            else
            {
                lbLastUpdatedUser.Text = string.Empty;
            }
            if (Item.GetDataKeyValue("LastUpdTS") != null)
            {
                //To retrieve back the UTC date in Homebase format
                lbLastUpdatedUser.Text = System.Web.HttpUtility.HtmlEncode(lbLastUpdatedUser.Text + " Date: " + Item.GetDataKeyValue("LastUpdTS").ToString());
            }
            EnableForm(false);
        }
        /// <summary>
        /// To find the control and make it visible false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnable(bool add, bool edit, bool delete)
        {
            LinkButton lbtninsertCtl, lbtndelCtl, lbtneditCtl;
            lbtninsertCtl = (LinkButton)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitInsert");
            lbtndelCtl = (LinkButton)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkDelete");
            lbtneditCtl = (LinkButton)dgDaylightSavingTimeRegion.MasterTableView.GetItems(GridItemType.CommandItem)[0].FindControl("lnkInitEdit");
            if (add)
            {
                lbtninsertCtl.Enabled = true;
            }
            else
            {
                lbtninsertCtl.Enabled = false;
            }
            if (delete)
            {
                lbtndelCtl.Enabled = true;
                lbtndelCtl.OnClientClick = "javascript:return ProcessDelete();";
            }
            else
            {
                lbtndelCtl.Enabled = false;
                lbtndelCtl.OnClientClick = string.Empty;
            }
            lbtneditCtl.Visible = true;
            if (edit)
            {
                lbtneditCtl.Enabled = true;
                lbtneditCtl.OnClientClick = "javascript:return ProcessUpdate();";
            }
            else
            {
                lbtneditCtl.Enabled = false;
                lbtneditCtl.OnClientClick = string.Empty;
            }
        }
        public DateTime FormatDate(string Date, string Format)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Date, Format))
            {
                Date = Date.TrimStart();
                string[] DateAndTime = Date.Replace("  ", " ").Split(' ');
                string[] SplitDate = new string[3];
                string[] SplitFormat = new string[3];
                if (Format.Contains("/"))
                {
                    SplitFormat = Format.Split('/');
                }
                else if (Format.Contains("-"))
                {
                    SplitFormat = Format.Split('-');
                }
                else if (Format.Contains("."))
                {
                    SplitFormat = Format.Split('.');
                }
                if (DateAndTime[0].Contains("/"))
                {
                    SplitDate = DateAndTime[0].Split('/');
                }
                else if (DateAndTime[0].Contains("-"))
                {
                    SplitDate = DateAndTime[0].Split('-');
                }
                else if (DateAndTime[0].Contains("."))
                {
                    SplitDate = DateAndTime[0].Split('.');
                }
                int dd = 0, mm = 0, yyyy = 0;
                for (int Index = 0; Index < SplitFormat.Count(); Index++)
                {
                    if (SplitFormat[Index].ToLower().Contains("d"))
                    {
                        dd = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("m"))
                    {
                        mm = Convert.ToInt16(SplitDate[Index]);
                    }
                    if (SplitFormat[Index].ToLower().Contains("y"))
                    {
                        yyyy = Convert.ToInt16(SplitDate[Index]);
                    }
                }
                int hh = 0, min = 0, ss = 0;
                string[] TimeSplit = new string[3];
                if (!string.IsNullOrEmpty(DateAndTime[1]))
                {
                    TimeSplit = DateAndTime[1].Split(':');
                    hh = Convert.ToInt32(TimeSplit[0]);
                    min = Convert.ToInt32(TimeSplit[1]);
                    //ss = Convert.ToInt32(TimeSplit[2]);
                }
                return new DateTime(yyyy, mm, dd, hh, min, ss);
                //return (yyyy + "/" + mm + "/" + dd);
            }
        }
    }
}