﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlightPak.Web.PreflightService;
using FlightPak.Web.ViewModels;
using FlightPak.Web.Views.Transactions.PostFlight;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class Postflight_PreflightRetrieveTest
    {
        public enum DutyBasisForTest { Scheduled = 2, BlockHours = 1 };

        #region DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Home_FixedWing
        //FPK Log - 88
        [TestMethod]
        public void DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Home_FixedWing()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 1;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.HomeDepartureDTTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.HomeArrivalDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();

            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);
            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            #endregion Data Setups

            decimal daysAway = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "F", upVM);

            Assert.AreEqual(1, daysAway);
        }
        #endregion

        #region DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Home_FixedWing_auto
        //FPK Log - 88
        [TestMethod]
        public void DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Home_FixedWing_auto()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 1;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.HomeDepartureDTTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.HomeArrivalDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();

            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);
            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            #endregion Data Setups
            decimal daysAway = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "F", upVM);

            Assert.AreEqual(1, daysAway);
        }
        #endregion

        #region DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Greenwich_FixedWing

        public void DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Greenwich_FixedWing()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 2;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureGreenwichDTTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.ArrivalGreenwichDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();

            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);
            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            #endregion Data Setups

            decimal daysAway = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "F", upVM);

            Assert.AreEqual(1, daysAway);
        }
        #endregion

        #region DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Local_RotaryWing
        //FPK Log - 88
        [TestMethod]
        public void DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Local_RotaryWing()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 1;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureDTTMLocal = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.ArrivalDTTMLocal = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();

            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);
            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            #endregion Data Setups

            decimal daysAway = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "R", upVM);

            Assert.AreEqual(1, daysAway);
        }
        #endregion

        #region DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Greenwich_RotaryWing
        //FPK Log - 88
        [TestMethod]
        public void DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled_Greenwich_RotaryWing()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 2;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureGreenwichDTTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.ArrivalGreenwichDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();

            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);
            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            #endregion Data Setups

            decimal daysAway = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "R", upVM);

            Assert.AreEqual(1, daysAway);
        }
        #endregion

        #region DaysAway_Scenario_4_One_Leg_TwoCrews_NextDay_Duty_Basis_Scheduled_Local_Rotary
        //FPK Log - won't be different from single leg single crew scenario - use Log 91 from FPK
        [TestMethod]
        public void DaysAway_Scenario_4_One_Leg_TwoCrews_NextDay_Duty_Basis_Scheduled_Local_Rotary()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 1;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureDTTMLocal = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.ArrivalDTTMLocal = new DateTime(2015, 10, 5, 01, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();

            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);

            PreflightCrewList plc2 = new PreflightCrewList();
            plc2.CrewID = 2;
            pl1.PreflightCrewLists.Add(plc2);

            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            #endregion Data Setups

            decimal daysAwayLeg1_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "R", upVM);
            decimal daysAwayLeg1_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 1, "R", upVM);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(1, daysAwayLeg1_Crew2);
        }
        #endregion

        #region DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3_Home_Fixed
        //FPK Log - 95
        [TestMethod]
        public void DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3_Home_Fixed()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 1;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1; 
            pl1.HomeDepartureDTTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.HomeArrivalDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);

            PreflightLeg pl2 = new PreflightLeg();
            pl2.LegNUM = 2;
            pl2.HomeDepartureDTTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.HomeArrivalDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            pl2.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc2 = new PreflightCrewList();
            plc2.CrewID = 1;
            pl2.PreflightCrewLists.Add(plc2);

            PreflightLeg pl3 = new PreflightLeg();
            pl3.LegNUM = 3;
            pl3.HomeDepartureDTTM = new DateTime(2015, 10, 5, 15, 50, 0);
            pl3.HomeArrivalDTTM = new DateTime(2015, 10, 5, 18, 10, 0);

            pl3.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc3 = new PreflightCrewList();
            plc3.CrewID = 1;
            pl3.PreflightCrewLists.Add(plc3);

            PreflightCrewList plc31 = new PreflightCrewList();
            plc31.CrewID = 2;
            pl3.PreflightCrewLists.Add(plc31);



            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();
            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            trip.PreflightLegs.Add(pl2);
            trip.PreflightLegs.Add(pl3);
            #endregion Data Setups

            decimal daysAwayLeg1_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "F", upVM);
            decimal daysAwayLeg2_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 2, "F", upVM);
            decimal daysAwayLeg3_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 3, "F", upVM);
            decimal daysAwayLeg3_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 3, "F", upVM);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
            Assert.AreEqual(0, daysAwayLeg3_Crew1);
            Assert.AreEqual(1, daysAwayLeg3_Crew2);
        }
        #endregion

        #region DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3_Greenwich_Fixed
        //FPK Log - 95
        [TestMethod]
        public void DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3_Greenwich_Fixed()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogFixed = 2;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.HomeDepartureDTTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.HomeArrivalDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);

            PreflightLeg pl2 = new PreflightLeg();
            pl2.LegNUM = 2;
            pl2.HomeDepartureDTTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.HomeArrivalDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            pl2.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc2 = new PreflightCrewList();
            plc2.CrewID = 1;
            pl2.PreflightCrewLists.Add(plc2);

            PreflightLeg pl3 = new PreflightLeg();
            pl3.LegNUM = 3;
            pl3.HomeDepartureDTTM = new DateTime(2015, 10, 5, 15, 50, 0);
            pl3.HomeArrivalDTTM = new DateTime(2015, 10, 5, 18, 10, 0);

            pl3.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc3 = new PreflightCrewList();
            plc3.CrewID = 1;
            pl3.PreflightCrewLists.Add(plc3);

            PreflightCrewList plc31 = new PreflightCrewList();
            plc31.CrewID = 2;
            pl3.PreflightCrewLists.Add(plc31);



            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();
            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            trip.PreflightLegs.Add(pl2);
            trip.PreflightLegs.Add(pl3);
            #endregion Data Setups

            decimal daysAwayLeg1_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "F", upVM);
            decimal daysAwayLeg2_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 2, "F", upVM);
            decimal daysAwayLeg3_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 3, "F", upVM);
            decimal daysAwayLeg3_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 3, "F", upVM);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
            Assert.AreEqual(0, daysAwayLeg3_Crew1);
            Assert.AreEqual(1, daysAwayLeg3_Crew2);
        }
        #endregion

        #region DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled_Local_Rotary
        //FPK Log - 96
        [TestMethod]
        public void DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled_Local_Rotary()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogRotary = 1;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureDTTMLocal = new DateTime(2015, 9, 28, 2, 0, 0);
            pl1.ArrivalDTTMLocal = new DateTime(2015, 9, 29, 1, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);

            PreflightLeg pl2 = new PreflightLeg();
            pl2.LegNUM = 2;
            pl2.DepartureDTTMLocal = new DateTime(2015, 9, 29, 1, 28, 0);
            pl2.ArrivalDTTMLocal = new DateTime(2015, 9, 30, 2, 10, 0);

            pl2.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc2 = new PreflightCrewList();
            plc2.CrewID = 1;
            pl2.PreflightCrewLists.Add(plc2);

            PreflightLeg pl3 = new PreflightLeg();
            pl3.LegNUM = 3;
            pl3.DepartureDTTMLocal = new DateTime(2015, 10, 1, 5, 50, 0);
            pl3.ArrivalDTTMLocal = new DateTime(2015, 10, 3, 1, 10, 0);

            pl3.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc3 = new PreflightCrewList();
            plc3.CrewID = 1;
            pl3.PreflightCrewLists.Add(plc3);

            PreflightCrewList plc31 = new PreflightCrewList();
            plc31.CrewID = 2;
            pl3.PreflightCrewLists.Add(plc31);

            PreflightLeg pl4 = new PreflightLeg();
            pl4.LegNUM = 4;
            pl4.DepartureDTTMLocal = new DateTime(2015, 10, 3, 6, 50, 0);
            pl4.ArrivalDTTMLocal = new DateTime(2015, 10, 4, 18, 10, 0);

            pl4.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc41 = new PreflightCrewList();
            plc41.CrewID = 2;
            pl4.PreflightCrewLists.Add(plc41);


            PreflightLeg pl5 = new PreflightLeg();
            pl5.LegNUM = 5;
            pl5.DepartureDTTMLocal = new DateTime(2015, 10, 4, 19, 20, 0);
            pl5.ArrivalDTTMLocal = new DateTime(2015, 10, 4, 22, 10, 0);

            pl5.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc5 = new PreflightCrewList();
            plc5.CrewID = 1;
            pl5.PreflightCrewLists.Add(plc5);

            PreflightCrewList plc51 = new PreflightCrewList();
            plc51.CrewID = 2;
            pl5.PreflightCrewLists.Add(plc51);


            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            trip.PreflightLegs.Add(pl2);
            trip.PreflightLegs.Add(pl3);
            trip.PreflightLegs.Add(pl4);
            trip.PreflightLegs.Add(pl5);
            #endregion Data Setups
            
            decimal daysAwayLeg1_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "R", upVM);
            decimal daysAwayLeg2_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 2, "R", upVM);
            decimal daysAwayLeg3_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 3, "R", upVM);
            decimal daysAwayLeg3_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 3, "R", upVM);
            decimal daysAwayLeg4_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 4, "R", upVM);
            decimal daysAwayLeg5_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 5, "R", upVM);
            decimal daysAwayLeg5_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 5, "R", upVM);

            Assert.AreEqual(2, daysAwayLeg1_Crew1);
            Assert.AreEqual(2, daysAwayLeg2_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew2);

            Assert.AreEqual(2, daysAwayLeg4_Crew2);
            Assert.AreEqual(1, daysAwayLeg5_Crew1);
            Assert.AreEqual(0, daysAwayLeg5_Crew2);
        }
        #endregion 

        #region DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled_UTC_Rotary
        //FPK Log - 96
        [TestMethod]
        public void DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled_UTC_Rotary()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogRotary = 2;
            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureGreenwichDTTM = new DateTime(2015, 9, 28, 2, 0, 0);
            pl1.ArrivalGreenwichDTTM = new DateTime(2015, 9, 29, 1, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);

            PreflightLeg pl2 = new PreflightLeg();
            pl2.LegNUM = 2;
            pl2.DepartureGreenwichDTTM = new DateTime(2015, 9, 29, 1, 28, 0);
            pl2.ArrivalGreenwichDTTM = new DateTime(2015, 9, 30, 2, 10, 0);

            pl2.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc2 = new PreflightCrewList();
            plc2.CrewID = 1;
            pl2.PreflightCrewLists.Add(plc2);

            PreflightLeg pl3 = new PreflightLeg();
            pl3.LegNUM = 3;
            pl3.DepartureGreenwichDTTM = new DateTime(2015, 10, 1, 5, 50, 0);
            pl3.ArrivalGreenwichDTTM = new DateTime(2015, 10, 3, 1, 10, 0);

            pl3.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc3 = new PreflightCrewList();
            plc3.CrewID = 1;
            pl3.PreflightCrewLists.Add(plc3);

            PreflightCrewList plc31 = new PreflightCrewList();
            plc31.CrewID = 2;
            pl3.PreflightCrewLists.Add(plc31);

            PreflightLeg pl4 = new PreflightLeg();
            pl4.LegNUM = 4;
            pl4.DepartureGreenwichDTTM = new DateTime(2015, 10, 3, 6, 50, 0);
            pl4.ArrivalGreenwichDTTM = new DateTime(2015, 10, 4, 18, 10, 0);

            pl4.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc41 = new PreflightCrewList();
            plc41.CrewID = 2;
            pl4.PreflightCrewLists.Add(plc41);


            PreflightLeg pl5 = new PreflightLeg();
            pl5.LegNUM = 5;
            pl5.DepartureGreenwichDTTM = new DateTime(2015, 10, 4, 19, 20, 0);
            pl5.ArrivalGreenwichDTTM = new DateTime(2015, 10, 4, 22, 10, 0);

            pl5.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc5 = new PreflightCrewList();
            plc5.CrewID = 1;
            pl5.PreflightCrewLists.Add(plc5);

            PreflightCrewList plc51 = new PreflightCrewList();
            plc51.CrewID = 2;
            pl5.PreflightCrewLists.Add(plc51);


            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            trip.PreflightLegs.Add(pl2);
            trip.PreflightLegs.Add(pl3);
            trip.PreflightLegs.Add(pl4);
            trip.PreflightLegs.Add(pl5);
            #endregion Data Setups

            decimal daysAwayLeg1_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "R", upVM);
            decimal daysAwayLeg2_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 2, "R", upVM);
            decimal daysAwayLeg3_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 3, "R", upVM);
            decimal daysAwayLeg3_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 3, "R", upVM);
            decimal daysAwayLeg4_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 4, "R", upVM);
            decimal daysAwayLeg5_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 5, "R", upVM);
            decimal daysAwayLeg5_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 5, "R", upVM);

            Assert.AreEqual(2, daysAwayLeg1_Crew1);
            Assert.AreEqual(2, daysAwayLeg2_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew2);

            Assert.AreEqual(2, daysAwayLeg4_Crew2);
            Assert.AreEqual(1, daysAwayLeg5_Crew1);
            Assert.AreEqual(0, daysAwayLeg5_Crew2);
        }
        #endregion 

        #region DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled_UTC_Rotary_auto
        //FPK Log - 96
        [TestMethod]
        public void DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled_UTC_Rotary_auto()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;
            upVM._LogRotary = 2;
            upVM._IsAutoPopulated = true;
            upVM._TaxiTime = (new TimeSpan(0, 5, 0)).ToString();

            PreflightMain trip = new PreflightMain();

            #region Data Setups
            PreflightLeg pl1 = new PreflightLeg();
            pl1.LegNUM = 1;
            pl1.DepartureGreenwichDTTM = new DateTime(2015, 9, 28, 2, 0, 0);
            pl1.ArrivalGreenwichDTTM = new DateTime(2015, 9, 29, 1, 10, 0);

            pl1.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc1 = new PreflightCrewList();
            plc1.CrewID = 1;
            pl1.PreflightCrewLists.Add(plc1);

            PreflightLeg pl2 = new PreflightLeg();
            pl2.LegNUM = 2;
            pl2.DepartureGreenwichDTTM = new DateTime(2015, 9, 29, 1, 28, 0);
            pl2.ArrivalGreenwichDTTM = new DateTime(2015, 9, 30, 2, 10, 0);

            pl2.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc2 = new PreflightCrewList();
            plc2.CrewID = 1;
            pl2.PreflightCrewLists.Add(plc2);

            PreflightLeg pl3 = new PreflightLeg();
            pl3.LegNUM = 3;
            pl3.DepartureGreenwichDTTM = new DateTime(2015, 10, 1, 5, 50, 0);
            pl3.ArrivalGreenwichDTTM = new DateTime(2015, 10, 3, 1, 10, 0);

            pl3.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc3 = new PreflightCrewList();
            plc3.CrewID = 1;
            pl3.PreflightCrewLists.Add(plc3);

            PreflightCrewList plc31 = new PreflightCrewList();
            plc31.CrewID = 2;
            pl3.PreflightCrewLists.Add(plc31);

            PreflightLeg pl4 = new PreflightLeg();
            pl4.LegNUM = 4;
            pl4.DepartureGreenwichDTTM = new DateTime(2015, 10, 3, 6, 50, 0);
            pl4.ArrivalGreenwichDTTM = new DateTime(2015, 10, 4, 18, 10, 0);

            pl4.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc41 = new PreflightCrewList();
            plc41.CrewID = 2;
            pl4.PreflightCrewLists.Add(plc41);


            PreflightLeg pl5 = new PreflightLeg();
            pl5.LegNUM = 5;
            pl5.DepartureGreenwichDTTM = new DateTime(2015, 10, 4, 19, 20, 0);
            pl5.ArrivalGreenwichDTTM = new DateTime(2015, 10, 4, 22, 10, 0);

            pl5.PreflightCrewLists = new System.Collections.Generic.List<PreflightCrewList>();
            PreflightCrewList plc5 = new PreflightCrewList();
            plc5.CrewID = 1;
            pl5.PreflightCrewLists.Add(plc5);

            PreflightCrewList plc51 = new PreflightCrewList();
            plc51.CrewID = 2;
            pl5.PreflightCrewLists.Add(plc51);


            if (trip.PreflightLegs != null)
                trip.PreflightLegs.Clear();

            trip.PreflightLegs = new System.Collections.Generic.List<PreflightLeg>();

            trip.PreflightLegs.Add(pl1);
            trip.PreflightLegs.Add(pl2);
            trip.PreflightLegs.Add(pl3);
            trip.PreflightLegs.Add(pl4);
            trip.PreflightLegs.Add(pl5);
            #endregion Data Setups
            decimal daysAwayLeg1_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 1, "R", upVM);
            decimal daysAwayLeg2_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 2, "R", upVM);
            decimal daysAwayLeg3_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 3, "R", upVM);
            decimal daysAwayLeg3_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 3, "R", upVM);
            decimal daysAwayLeg4_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 4, "R", upVM);
            decimal daysAwayLeg5_Crew1 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 1, 5, "R", upVM);
            decimal daysAwayLeg5_Crew2 = PreflightSearchRetrieve.DaysAwayCalculation(trip, 2, 5, "R", upVM);

            Assert.AreEqual(2, daysAwayLeg1_Crew1);
            Assert.AreEqual(2, daysAwayLeg2_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew2);

            Assert.AreEqual(2, daysAwayLeg4_Crew2);
            Assert.AreEqual(1, daysAwayLeg5_Crew1);
            Assert.AreEqual(0, daysAwayLeg5_Crew2);
        }
        #endregion 


    }
}
