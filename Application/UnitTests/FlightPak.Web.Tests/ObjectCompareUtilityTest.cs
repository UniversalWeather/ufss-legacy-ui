﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using FakeItEasy.Configuration;
using FlightPak.Business.Common;
using FlightPak.Data.Preflight;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeItEasy;
using System.Text;
using FlightPak.Common;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class ObjectCompareUtilityTest
    {
        private FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain> GetTripObjectForTest1()
        {
            var oldTrip = A.Fake<FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain>>();
            var prefMain = new FlightPak.Data.Preflight.PreflightMain();
            var prefMainLeg = new FlightPak.Data.Preflight.PreflightLeg();
            var prefMainLegCrew = new FlightPak.Data.Preflight.PreflightCrewList();

            prefMain.TripID = 10002104323;
            prefMain.AircraftID = 10002418;
            prefMain.CustomerID = 10002;
            prefMain.EstDepartureDT = Convert.ToDateTime("12/16/2014");
            prefMain.FlightCost = 0;
            prefMain.HomebaseID = 10002179596;
            prefMain.IsDeleted = false;
            prefMain.IsPrivate = false;
            prefMain.IsTripCopied = false;
            prefMain.LastUpdTS = Convert.ToDateTime("12/16/2014");
            prefMain.LastUpdUID = "Anil Singh";
            prefMain.Notes = "";
            prefMain.PreviousNUM = 0;
            prefMain.RecordType = "T";
            prefMain.RequestDT = Convert.ToDateTime("12/16/2014");
            prefMain.RevisionDescriptioin = "";
            prefMain.TripDescription = "";
            prefMain.TripID = 10002104520;
            prefMain.TripNUM = 8306;
            prefMain.TripStatus = "T";
            prefMain.TripSheetNotes = "";
            prefMain.AllowTripToNavigate = false;
            prefMain.AllowTripToSave = false;
            prefMain.CancelDescription = "";

            prefMain.Company = new FlightPak.Data.Preflight.Company();
            prefMain.Company.HomebaseID = 10002179596;
            prefMain.Company.HomebaseAirportID = 1000238714;
            prefMain.Company.AircraftBasis = 1;
            prefMain.Company.AircraftBlockFlight = 1;
            prefMain.Company.Approach = 180;
            prefMain.Company.ApproachMinimum = 6;
            prefMain.Company.CompanyName = "Test Company";
            prefMain.Company.CrewDutyID = 10002196735;

            prefMainLeg.LegID = 10002104521;
            prefMainLeg.ArriveICAOID = 1000239046;
            prefMainLeg.CheckGroup = "DOM";
            prefMainLeg.CrewDutyRulesID = 10002196735;
            prefMainLeg.CustomerID = 10002;
            prefMainLeg.DepartICAOID = 1000238714;
            prefMainLeg.DepartureDTTMLocal = Convert.ToDateTime("12/16/2014");
            prefMainLeg.DepartureGreenwichDTTM = Convert.ToDateTime("12/16/2014");
            prefMainLeg.Distance = 100;

            prefMainLegCrew.CrewID = 10002208844;
            prefMainLegCrew.LegID = 10002104521;
            prefMainLegCrew.PreflightCrewListID = 10002104522;
            prefMainLegCrew.CrewLastName = "Singh";
            prefMainLegCrew.CrewFirstName = "Anil";
            prefMainLegCrew.CrewMiddleName = "A";
            prefMainLegCrew.DutyTYPE = "A";

            prefMain.PreflightLegs.Add(prefMainLeg);
            prefMainLeg.PreflightCrewLists.Add(prefMainLegCrew);

            oldTrip.EntityList = new List<FlightPak.Data.Preflight.PreflightMain>();
            oldTrip.EntityList.Add(prefMain);
            oldTrip.ReturnFlag = true;

            return oldTrip;
        }
        private FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain> GetTripObjectForTest2()
        {
            var newTrip = A.Fake<FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain>>();
            newTrip.EntityList = new List<FlightPak.Data.Preflight.PreflightMain>();
            var prefMainRet = new FlightPak.Data.Preflight.PreflightMain();
            var prefMainLegRet = new FlightPak.Data.Preflight.PreflightLeg();
            var prefMainLegCrewRet = new FlightPak.Data.Preflight.PreflightCrewList();

            prefMainRet.TripID = 10002104323;
            prefMainRet.AircraftID = 10002418;
            prefMainRet.CustomerID = 10002;
            prefMainRet.EstDepartureDT = Convert.ToDateTime("12/16/2014");
            prefMainRet.FlightCost = 0;
            prefMainRet.HomebaseID = 10002179596;
            prefMainRet.IsDeleted = false;
            prefMainRet.IsPrivate = false;
            prefMainRet.IsTripCopied = false;
            prefMainRet.LastUpdTS = Convert.ToDateTime("12/16/2014");
            prefMainRet.LastUpdUID = "Anil Singh";
            prefMainRet.Notes = "";
            prefMainRet.PreviousNUM = 0;
            prefMainRet.RecordType = "T";
            prefMainRet.RequestDT = Convert.ToDateTime("12/16/2014");
            prefMainRet.RevisionDescriptioin = "";
            prefMainRet.TripDescription = "";
            prefMainRet.TripID = 10002104520;
            prefMainRet.TripNUM = 8306;
            prefMainRet.TripStatus = "T";
            prefMainRet.TripSheetNotes = "";
            prefMainRet.AllowTripToNavigate = false;
            prefMainRet.AllowTripToSave = false;
            prefMainRet.CancelDescription = "";

            prefMainRet.Company = new FlightPak.Data.Preflight.Company();
            prefMainRet.Company.HomebaseID = 10002179596;
            prefMainRet.Company.HomebaseAirportID = 1000238714;
            prefMainRet.Company.AircraftBasis = 1;
            prefMainRet.Company.AircraftBlockFlight = 1;
            prefMainRet.Company.Approach = 180;
            prefMainRet.Company.ApproachMinimum = 6;
            prefMainRet.Company.CompanyName = "Test Company";
            prefMainRet.Company.CrewDutyID = 10002196735;

            prefMainLegRet.LegID = 10002104521;
            prefMainLegRet.ArriveICAOID = 1000239046;
            prefMainLegRet.CheckGroup = "DOM";
            prefMainLegRet.CrewDutyRulesID = 10002196735;
            prefMainLegRet.CustomerID = 10002;
            prefMainLegRet.DepartICAOID = 1000238714;
            prefMainLegRet.DepartureDTTMLocal = Convert.ToDateTime("12/16/2014");
            prefMainLegRet.DepartureGreenwichDTTM = Convert.ToDateTime("12/16/2014");
            prefMainLegRet.Distance = 100;
            prefMainLegCrewRet.CrewID = 10002208844;
            prefMainLegCrewRet.LegID = 10002104521;
            prefMainLegCrewRet.PreflightCrewListID = 10002104522;
            prefMainLegCrewRet.CrewLastName = "Singh";
            prefMainLegCrewRet.CrewFirstName = "Anil";
            prefMainLegCrewRet.CrewMiddleName = "A";
            prefMainLegCrewRet.DutyTYPE = "S";
            prefMainRet.PreflightLegs.Add(prefMainLegRet);
            prefMainLegRet.PreflightCrewLists.Add(prefMainLegCrewRet);

            newTrip.EntityList = new List<FlightPak.Data.Preflight.PreflightMain>();
            newTrip.EntityList.Add(prefMainRet);
            newTrip.ReturnFlag = true;

            return newTrip;
        }
        private FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain> GetTripObjectForTest3()
        {
            var newTrip = A.Fake<FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain>>();
            newTrip.EntityList = new List<FlightPak.Data.Preflight.PreflightMain>();
            var prefMainRet = new FlightPak.Data.Preflight.PreflightMain();

            prefMainRet.TripID = 10002104323;
            prefMainRet.AircraftID = 10002418;
            prefMainRet.CustomerID = 10002;
            prefMainRet.EstDepartureDT = Convert.ToDateTime("12/16/2014");
            prefMainRet.FlightCost = 0;
            prefMainRet.HomebaseID = 10002179596;
            prefMainRet.IsDeleted = false;
            prefMainRet.IsPrivate = false;
            prefMainRet.IsTripCopied = false;
            prefMainRet.LastUpdTS = Convert.ToDateTime("12/16/2014");
            prefMainRet.LastUpdUID = "Anil Singh";
            prefMainRet.Notes = "";
            prefMainRet.PreviousNUM = 0;
            prefMainRet.RecordType = "T";
            prefMainRet.RequestDT = Convert.ToDateTime("12/16/2014");
            prefMainRet.RevisionDescriptioin = "";
            prefMainRet.TripDescription = "";
            prefMainRet.TripID = 10002104520;
            prefMainRet.TripNUM = 8306;
            prefMainRet.TripStatus = "T";
            prefMainRet.TripSheetNotes = "";
            prefMainRet.AllowTripToNavigate = false;
            prefMainRet.AllowTripToSave = false;
            prefMainRet.CancelDescription = "";

            prefMainRet.Company = new FlightPak.Data.Preflight.Company();
            prefMainRet.Company.HomebaseID = 10002179596;
            prefMainRet.Company.HomebaseAirportID = 1000238714;
            prefMainRet.Company.AircraftBasis = 1;
            prefMainRet.Company.AircraftBlockFlight = 1;
            prefMainRet.Company.Approach = 180;
            prefMainRet.Company.ApproachMinimum = 6;
            prefMainRet.Company.CompanyName = "Test Company";
            prefMainRet.Company.CrewDutyID = 10002196735;

            newTrip.EntityList = new List<FlightPak.Data.Preflight.PreflightMain>();
            newTrip.EntityList.Add(prefMainRet);
            newTrip.ReturnFlag = true;

            return newTrip;
        }

        [TestMethod]
        public void Test_CompareObjects()
        {
            var oldTrip = GetTripObjectForTest1();
            var newTrip = GetTripObjectForTest2();

            var ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };

            var containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG", "CREW", "COMPANY", "PASSENGER", "FLEET", "AIRCRAFT", "AIRPORT1", 
                                            "ACCOUNT", "USERMASTER", "DEPARTMENT", "DEPARTMENTAUTHORIZATION", "FLIGHTCATAGORY", "CREWDUTYRULE", 
                                            "PREFLIGHTFBOLISTS", "PREFLIGHTFBOLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTHOTELLISTS", 
                                            "PREFLIGHTHOTELLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTCATERINGDETAILS", "PREFLIGHTCATERINGDETAIL", 
                                            "PREFLIGHTPASSENGERLISTS", "PREFLIGHTPASSENGERLIST", "CREWPASSENGERPASSPORTS", "CREWPASSENGERPASSPORT", "FLIGHTPURPOSE", 
                                            "PREFLIGHTCREWLISTS", "PREFLIGHTCREWLIST", "FBO","TRANSPORT","HOTEL","CATERING","PREFLIGHTTRIPOUTBOUNDS","PREFLIGHTTRIPOUTBOUND",
                                            "PREFLIGHTCHECKLISTS","PREFLIGHTCHECKLIST","PREFLIGHTFUELS","PREFLIGHTFUEL" };

            const bool vActual = false;
            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0], ignoreListProp, containListProp);
            Assert.AreEqual(vActual, vExpected);
        }

        [TestMethod]
        [Description(@"Test case for Compare object type")]
        public void Test_CompareObjects_ComperObjectType()
        {
            var oldTrip = A.Fake<FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain>>();
            var newTrip = A.Fake<FlightPak.Business.Common.ReturnValue<FlightPak.Data.Preflight.PreflightMain>>();
           
            oldTrip = GetTripObjectForTest1();
            newTrip = GetTripObjectForTest2();

            var ignoreListProp = new string[] {};
            var containListProp = new string[] {};

            const bool vActual = false;
            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0].PreflightLegs, ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);

        }
     
        [TestMethod]
        [Description(@"Test case for ignore list of object's fields and property.")]
        public void Test_CompareObjects_CheckIgnoreList()
        {
            ClassA A1 = new ClassA();
            ClassA B1 = new ClassA();

            var ignoreListProp = new string[] { "_NAME", "NAME", "<NAME>K__BACKINGFIELD" };
            var containListProp = new string[] {  };

            const bool vActual = true;
            bool vExpected = FlightPak.Common.Utility.CompareObjects(A1, B1, ignoreListProp, containListProp);
            Assert.IsTrue(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for contain list of object's fields and property.")]
        public void Test_CompareObjects_CheckContainList()
        {
            ClassA A1 = new ClassA();
            ClassA B1 = new ClassA();

            A1.Name = "Test Name A";
            B1.Name = "Test Name B";

            var ignoreListProp = new string[] { "<NAME>K__BACKINGFIELD", "CHARS" };
            var containListProp = new string[] { "NAME"};

            bool vExpected = FlightPak.Common.Utility.CompareObjects(A1, B1, ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for source field are null.")]
        public void Test_CompareObjects_CheckSourceFieldNull()
        {
            ClassA A1 = new ClassA();
            ClassA B1 = new ClassA();

            A1.Name = null;
            B1.Name = "Test Name B";

            var ignoreListProp = new string[] {  };
            var containListProp = new string[] {  };

            bool vExpected = FlightPak.Common.Utility.CompareObjects(A1, B1, ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for compare field are null.")]
        public void Test_CompareObjects_CheckCompareFieldNull()
        {
            ClassA A1 = new ClassA();
            ClassA B1 = new ClassA();

            A1.Name = null;
            B1.Name = null;

            var ignoreListProp = new string[] { };
            var containListProp = new string[] { };

            bool vExpected = FlightPak.Common.Utility.CompareObjects(A1, B1, ignoreListProp, containListProp);
            Assert.IsTrue(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for same field value are different.")]
        public void Test_CompareObjects_CheckFieldValueDifferent()
        {
            var a1 = new ClassA();
            var b1 = new ClassA();

            a1.Name = "Test Name A";
            b1.Name = "Test Name B";

            var ignoreListProp = new string[] { };
            var containListProp = new string[] { };

            bool vExpected = FlightPak.Common.Utility.CompareObjects(a1, b1, ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for source property is null.")]
        public void Test_CompareObjects_CheckSourceProNull()
        {
            var oldTrip = GetTripObjectForTest1();
            var newTrip = GetTripObjectForTest2();

            var ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };

            var containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG", "CREW", "COMPANY", "PASSENGER", "FLEET", "AIRCRAFT", "AIRPORT1", 
                                            "ACCOUNT", "USERMASTER", "DEPARTMENT", "DEPARTMENTAUTHORIZATION", "FLIGHTCATAGORY", "CREWDUTYRULE", 
                                            "PREFLIGHTFBOLISTS", "PREFLIGHTFBOLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTHOTELLISTS", 
                                            "PREFLIGHTHOTELLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTCATERINGDETAILS", "PREFLIGHTCATERINGDETAIL", 
                                            "PREFLIGHTPASSENGERLISTS", "PREFLIGHTPASSENGERLIST", "CREWPASSENGERPASSPORTS", "CREWPASSENGERPASSPORT", "FLIGHTPURPOSE", 
                                            "PREFLIGHTCREWLISTS", "PREFLIGHTCREWLIST", "FBO","TRANSPORT","HOTEL","CATERING","PREFLIGHTTRIPOUTBOUNDS","PREFLIGHTTRIPOUTBOUND",
                                            "PREFLIGHTCHECKLISTS","PREFLIGHTCHECKLIST","PREFLIGHTFUELS","PREFLIGHTFUEL" };

            newTrip.EntityList[0].Aircraft = new Aircraft();
            newTrip.EntityList[0].Aircraft.AircraftID = 1002;

            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0], ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }
        
        [TestMethod]
        [Description(@"Test case for same property value are different.")]
        public void Test_CompareObjects_CheckSourceProValueDifferent()
        {
            var oldTrip = GetTripObjectForTest1();
            var newTrip = GetTripObjectForTest2();

            var ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };

            var containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG", "CREW", "COMPANY", "PASSENGER", "FLEET", "AIRCRAFT", "AIRPORT1", 
                                            "ACCOUNT", "USERMASTER", "DEPARTMENT", "DEPARTMENTAUTHORIZATION", "FLIGHTCATAGORY", "CREWDUTYRULE", 
                                            "PREFLIGHTFBOLISTS", "PREFLIGHTFBOLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTHOTELLISTS", 
                                            "PREFLIGHTHOTELLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTCATERINGDETAILS", "PREFLIGHTCATERINGDETAIL", 
                                            "PREFLIGHTPASSENGERLISTS", "PREFLIGHTPASSENGERLIST", "CREWPASSENGERPASSPORTS", "CREWPASSENGERPASSPORT", "FLIGHTPURPOSE", 
                                            "PREFLIGHTCREWLISTS", "PREFLIGHTCREWLIST", "FBO","TRANSPORT","HOTEL","CATERING","PREFLIGHTTRIPOUTBOUNDS","PREFLIGHTTRIPOUTBOUND",
                                            "PREFLIGHTCHECKLISTS","PREFLIGHTCHECKLIST","PREFLIGHTFUELS","PREFLIGHTFUEL" };

            newTrip.EntityList[0].Aircraft = new Aircraft();
            newTrip.EntityList[0].Aircraft.AircraftID = 1002;

            oldTrip.EntityList[0].Aircraft = new Aircraft();
            oldTrip.EntityList[0].Aircraft.AircraftID = 1003;

            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0], ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for Entity collection.")]
        public void Test_CompareObjects_CheckSourceEntityCollection()
        {
            var oldTrip = GetTripObjectForTest1();
            var newTrip = GetTripObjectForTest2();

            var ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };

            var containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG", "CREW", "COMPANY", "PASSENGER", "FLEET", "AIRCRAFT", "AIRPORT1", 
                                            "ACCOUNT", "USERMASTER", "DEPARTMENT", "DEPARTMENTAUTHORIZATION", "FLIGHTCATAGORY", "CREWDUTYRULE", 
                                            "PREFLIGHTFBOLISTS", "PREFLIGHTFBOLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTHOTELLISTS", 
                                            "PREFLIGHTHOTELLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTCATERINGDETAILS", "PREFLIGHTCATERINGDETAIL", 
                                            "PREFLIGHTPASSENGERLISTS", "PREFLIGHTPASSENGERLIST", "CREWPASSENGERPASSPORTS", "CREWPASSENGERPASSPORT", "FLIGHTPURPOSE", 
                                            "PREFLIGHTCREWLISTS", "PREFLIGHTCREWLIST", "FBO","TRANSPORT","HOTEL","CATERING","PREFLIGHTTRIPOUTBOUNDS","PREFLIGHTTRIPOUTBOUND",
                                            "PREFLIGHTCHECKLISTS","PREFLIGHTCHECKLIST","PREFLIGHTFUELS","PREFLIGHTFUEL" };


            var prefMainLeg = new FlightPak.Data.Preflight.PreflightLeg();
            prefMainLeg.LegID = 10002104521;
            prefMainLeg.ArriveICAOID = 1000239046;
            prefMainLeg.CheckGroup = "DOM";
            prefMainLeg.CrewDutyRulesID = 10002196735;
            prefMainLeg.CustomerID = 10002;
            prefMainLeg.DepartICAOID = 1000238714;
            prefMainLeg.DepartureDTTMLocal = Convert.ToDateTime("12/16/2014");
            prefMainLeg.DepartureGreenwichDTTM = Convert.ToDateTime("12/16/2014");
            prefMainLeg.Distance = 100;
            newTrip.EntityList[0].PreflightLegs.Add(prefMainLeg);


            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0], ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for Entity collection data changes.")]
        public void Test_CompareObjects_CheckSourceEntityCollectionDataChanges()
        {
            var oldTrip = GetTripObjectForTest1();
            var newTrip = GetTripObjectForTest1();

            var ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };
            var containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG"};

            foreach (PreflightLeg leg in newTrip.EntityList[0].PreflightLegs)
            {
                leg.LegID = 10002104522;
            }

            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0], ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }

        [TestMethod]
        [Description(@"Test case for Entity collection source data null.")]
        public void Test_CompareObjects_CheckSourceEntityCollectionSourceNull()
        {
            var oldTrip = GetTripObjectForTest3();
            var newTrip = GetTripObjectForTest3();

            var ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };
            var containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG" };

            var prefMainLeg = new FlightPak.Data.Preflight.PreflightLeg();
            prefMainLeg.LegID = 10002104521;
            prefMainLeg.ArriveICAOID = 1000239046;
            prefMainLeg.CheckGroup = "DOM";
            prefMainLeg.CrewDutyRulesID = 10002196735;
            prefMainLeg.CustomerID = 10002;
            prefMainLeg.DepartICAOID = 1000238714;
            prefMainLeg.DepartureDTTMLocal = Convert.ToDateTime("12/16/2014");
            prefMainLeg.DepartureGreenwichDTTM = Convert.ToDateTime("12/16/2014");
            prefMainLeg.Distance = 100;
            newTrip.EntityList[0].PreflightLegs.Add(prefMainLeg);

            bool vExpected = FlightPak.Common.Utility.CompareObjects(oldTrip.EntityList[0], newTrip.EntityList[0], ignoreListProp, containListProp);
            Assert.IsFalse(vExpected);
        }
    }

    public  class ClassA
    {
        public string Name { get; set; }
        public  void MethodA()
        {
            var strTest = "this is test method";
        }
    }

    public  class ClassB 
    {
        public  void MethodB()
        {
            var strTest = "this is test method";
        }
    }
}
