﻿using System;
using FlightPak.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class CsvDataValidationTest
    {
        [TestMethod]
        public void NoInvalidChar()
        {
            string CsvFileContent = @"Yes,AEH,Abeche,,Air Total - AEH,1,99999,,6.3653,8/5/2013
                                           Yes,AGA,Agadir,,Air Total - AGA,1,99999,,3.6379,8/5/2013
                                           Yes,AGP,Malaga,,Cepsa - AGP,1,99999,,3.613,8/5/2013
                                           Yes,AGU,Aguascalientes,,ICCS - Aguascalientes,1,99999,,4.1306,7/23/2013
                                           Yes,ALC,Alicante,,Cepsa - ALC,1,99999,,3.526,8/5/2013
                                           Yes,ALF,Alta,,Statoil - ALF,1,99999,,5.8645,8/1/2013
                                           Yes,AMD,Ahmedabad,,Bharat Petroleum - AMD,1,99999,,3.6102,8/1/2013
                                           Yes,ARN,Stockholm,,Stockholm Fuelling - ARN,1,99999,,3.3945,8/1/2013
                                           Yes,ASK,Yamoussoukro,,Air Total - ASK,1,99999,,4.8894,8/5/2013
                                           Yes,ASW,Aswan,,MISR Petroleum - ASW,1,99999,,3.9358,8/5/2013
                                           Yes,ATH,Sparta,,Signature Flight Support,1,99999,,4.0308,8/5/2013
                                           Yes,AUR,Aurillac,,Air Total - AUR,1,99999,,4.7533,8/5/2013
                                           Yes,AYT,Antalya,,ExxonMobil - AYT,1,99999,,3.2954,8/6/2013";

            Assert.IsTrue(Utility.ValidateCsvContent(CsvFileContent).IsValid);
        }

        [TestMethod]
        public void AllInvalidChar()
        {
            string CsvFileContent = @"¢é±Œ,ù¾†ý,Ú¾,ã÷ú¾,ÿôÑ,Ž¶ø,üúöß,ù,á¥ÚçÓ,¢é±Œ,¼ÌìÝ,¢ñ¼ÌìÝ,¢é±Œ";
            CsvValidationResult objCsvValidationResult = Utility.ValidateCsvContent(CsvFileContent);
            Assert.IsFalse(objCsvValidationResult.IsValid);
            Assert.IsTrue(objCsvValidationResult.InvalidPercentage > 75);
        }

        [TestMethod]
        public void IsValidEmptyString()
        {
            string CsvFileContent = @",,,,,,,,,,,,";
            CsvValidationResult objCsvValidationResult = Utility.ValidateCsvContent(CsvFileContent);
            Assert.IsTrue(objCsvValidationResult.IsValid);
            Assert.IsTrue(objCsvValidationResult.InvalidPercentage == 0);
        }

        [TestMethod]
        public void InvalidCharaterMarginLessThenTwenty()
        {
            string CsvFileContent = "TestCustomer,,+ˆÓy-õ,ÁÁÔjµ,Êòì_y,2015/01/31,1,299,4.032,n)—Ë–?,979-836-5462,Yes,Yes";
            CsvValidationResult objCsvValidationResult = Utility.ValidateCsvContent(CsvFileContent, 19);
            Assert.IsFalse(objCsvValidationResult.IsValid);
            Assert.IsTrue(objCsvValidationResult.InvalidPercentage < 20);
        }

        [TestMethod]
        public void InvalidCharaterMarginGreaterThenTwenty()
        {
            string CsvFileContent = "TestCustomer,,+ˆÓy-Óy-Óy-õ,ÁÁÔjµ,Êòì_y,2015/01/31,1,299,4.032,n)——Ë–?—Ë–?Ë–?,979-836-5462,Yes,Yes";

            CsvValidationResult objCsvValidationResult = Utility.ValidateCsvContent(CsvFileContent, 20);
            Assert.IsFalse(objCsvValidationResult.IsValid);
            Assert.IsTrue(objCsvValidationResult.InvalidPercentage > 20);
        }

        [TestMethod]
        public void InvalidCharaterMarginEqualToTwenty()
        {
            string CsvFileContent = "TestCustomer,,+ˆÓy-Óy-Óy-õ,ÁÁÔjµ,òì_y,2015/01/31,1,299,4.032,n)–?—Ë–?Ë–?,979-836-5462,Yes,Yes";

            CsvValidationResult objCsvValidationResult = Utility.ValidateCsvContent(CsvFileContent, 20);
            Assert.IsFalse(objCsvValidationResult.IsValid);
            Assert.IsTrue(objCsvValidationResult.InvalidPercentage == 20);
        }
    }
}
