﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using FlightPak.Web.BusinessLite;
using FlightPak.Web.Framework.Constants;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Web.ViewModels;
using FlightPak.Web.ViewModels.Postflight;
using FlightPak.Web.Views.Transactions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlightPak.Web.PostflightService;
using Omu.ValueInjecter;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class PostflightLegTest
    {
        private PostflightLogViewModel GetPostflightLogForTestWithLeg1()
        {
            PostflightLogViewModel poLog = new PostflightLogViewModel();
            poLog.PostflightMain = new PostflightMainViewModel();

            PostflightLegViewModel poLeg1 = new PostflightLegViewModel();

            poLeg1.LegNUM = 1;
            poLeg1.ArriveICAOID = 1000745890;
            poLeg1.DepartICAOID = 1000743415;
            poLeg1.ScheduledDate = "2015-10-14";
            poLeg1.ScheduledTime = "05:00";
            poLeg1.OutDate = "2015-10-14";
            poLeg1.OutTime = "05:00";
            poLeg1.BlockOut = "05:00";
            poLeg1.TimeOff = "05:00";
            poLeg1.TimeOn = "10:00";
            poLeg1.InDate = "2015-10-14";
            poLeg1.InTime = "10:00";
            poLeg1.BlockIN = "10:00";

            poLeg1.IsDeleted = false;
            poLeg1.State = TripEntityState.Modified;
            poLeg1.TimeDisplayTenMin = 1;
            poLeg1.HoursMinutesCONV = 2;
            poLeg1.DutyTYPE = 2;
            poLeg1.Distance = 5151;
            poLeg1.ConvertedDistance = 5151;
            poLeg1.CrewDutyRule = null;
            poLeg1.CrewCurrency = "91";
            poLeg1.IsKilometer = false;
            poLog.PostflightLegs[0] = (PostflightLegViewModel)poLog.PostflightLegs[0].InjectFrom(poLeg1);

            return poLog;
        }
        private PostflightLogViewModel GetPostflightLogForTestWithLeg2()
        {
            PostflightLogViewModel poLog = new PostflightLogViewModel();
            poLog.PostflightMain = new PostflightMainViewModel();

            PostflightLegViewModel poLeg1 = new PostflightLegViewModel();

            poLeg1.LegNUM = 1;
            poLeg1.ArriveICAOID = 1000745890;
            poLeg1.DepartICAOID = 1000743415;
            poLeg1.ScheduledDate = "2015-10-14";
            poLeg1.ScheduledTime = "05:00";
            poLeg1.OutDate = "2015-10-14";
            poLeg1.OutTime = "05:00";
            poLeg1.BlockOut = "05:00";
            poLeg1.TimeOff = "05:00";
            poLeg1.TimeOn = "10:00";
            poLeg1.InDate = "2015-10-14";
            poLeg1.InTime = "10:00";
            poLeg1.BlockIN = "10:00";

            poLeg1.IsDeleted = false;
            poLeg1.State = TripEntityState.Modified;
            poLeg1.TimeDisplayTenMin = 1;
            poLeg1.HoursMinutesCONV = 2;
            poLeg1.DutyTYPE = 2;
            poLeg1.Distance = 5151;
            poLeg1.ConvertedDistance = 5151;
            poLeg1.CrewDutyRule = null;
            poLeg1.CrewCurrency = "91";
            poLeg1.IsKilometer = false;

            PostflightLegViewModel poLeg2 = new PostflightLegViewModel();

            poLeg2.LegNUM = 2;
            poLeg2.ArriveICAOID = 1000745890;
            poLeg2.DepartICAOID = 1000743415;
            poLeg2.ScheduledDate = "2015-10-14";
            poLeg2.ScheduledTime = "10:00";
            poLeg2.OutDate = "2015-10-14";
            poLeg2.OutTime = "10:00";
            poLeg2.BlockOut = "10:00";
            poLeg2.TimeOff = "10:00";
            poLeg2.TimeOn = "15:00";
            poLeg2.InDate = "2015-10-14";
            poLeg2.InTime = "15:00";
            poLeg2.BlockIN = "15:00";

            poLeg2.IsDeleted = false;
            poLeg2.State = TripEntityState.Modified;
            poLeg2.TimeDisplayTenMin = 1;
            poLeg2.HoursMinutesCONV = 2;
            poLeg2.DutyTYPE = 2;
            poLeg2.Distance = 5151;
            poLeg2.ConvertedDistance = 5151;
            poLeg2.CrewDutyRule = null;
            poLeg2.CrewCurrency = "91";
            poLeg2.IsKilometer = false;

            poLog.PostflightLegs[0] = (PostflightLegViewModel)poLog.PostflightLegs[0].InjectFrom(poLeg1);
            poLog.PostflightLegs.Add(poLeg2);

            return poLog;
        }
        private PostflightLogViewModel GetPostflightLogForTestWithLeg3()
        {
            PostflightLogViewModel poLog = new PostflightLogViewModel();
            poLog.PostflightMain = new PostflightMainViewModel();

            PostflightLegViewModel poLeg1 = new PostflightLegViewModel();

            poLeg1.LegNUM = 1;
            poLeg1.ArriveICAOID = 1000745890;
            poLeg1.DepartICAOID = 1000743415;
            poLeg1.ScheduledDate = "2015-10-14";
            poLeg1.ScheduledTime = "05:00";
            poLeg1.OutDate = "2015-10-14";
            poLeg1.OutTime = "05:00";
            poLeg1.BlockOut = "05:00";
            poLeg1.TimeOff = "05:00";
            poLeg1.TimeOn = "10:00";
            poLeg1.InDate = "2015-10-14";
            poLeg1.InTime = "10:00";
            poLeg1.BlockIN = "10:00";

            poLeg1.IsDeleted = false;
            poLeg1.State = TripEntityState.Modified;
            poLeg1.TimeDisplayTenMin = 1;
            poLeg1.HoursMinutesCONV = 2;
            poLeg1.DutyTYPE = 2;
            poLeg1.Distance = 5151;
            poLeg1.ConvertedDistance = 5151;
            poLeg1.CrewDutyRule = null;
            poLeg1.CrewCurrency = "91";
            poLeg1.IsKilometer = false;

            PostflightLegViewModel poLeg2 = new PostflightLegViewModel();

            poLeg2.LegNUM = 2;
            poLeg2.ArriveICAOID = 1000745890;
            poLeg2.DepartICAOID = 1000743415;
            poLeg2.ScheduledDate = "2015-10-14";
            poLeg2.ScheduledTime = "10:00";
            poLeg2.OutDate = "2015-10-14";
            poLeg2.OutTime = "10:00";
            poLeg2.BlockOut = "10:00";
            poLeg2.TimeOff = "10:00";
            poLeg2.TimeOn = "15:00";
            poLeg2.InDate = "2015-10-14";
            poLeg2.InTime = "15:00";
            poLeg2.BlockIN = "15:00";

            poLeg2.IsDeleted = false;
            poLeg2.State = TripEntityState.Modified;
            poLeg2.TimeDisplayTenMin = 1;
            poLeg2.HoursMinutesCONV = 2;
            poLeg2.DutyTYPE = 2;
            poLeg2.Distance = 5151;
            poLeg2.ConvertedDistance = 5151;
            poLeg2.CrewDutyRule = null;
            poLeg2.CrewCurrency = "91";
            poLeg2.IsKilometer = false;

            PostflightLegViewModel poLeg3 = new PostflightLegViewModel();

            poLeg3.LegNUM = 3;
            poLeg3.ArriveICAOID = 1000745890;
            poLeg3.DepartICAOID = 1000743415;
            poLeg3.ScheduledDate = "2015-10-14";
            poLeg3.ScheduledTime = "16:00";
            poLeg3.OutDate = "2015-10-14";
            poLeg3.OutTime = "17:00";
            poLeg3.BlockOut = "17:00";
            poLeg3.TimeOff = "17:00";
            poLeg3.TimeOn = "21:00";
            poLeg3.InDate = "2015-10-14";
            poLeg3.InTime = "22:00";
            poLeg3.BlockIN = "22:00";

            poLeg3.IsDeleted = false;
            poLeg3.State = TripEntityState.Modified;
            poLeg3.TimeDisplayTenMin = 1;
            poLeg3.HoursMinutesCONV = 2;
            poLeg3.DutyTYPE = 2;
            poLeg3.Distance = 5151;
            poLeg3.ConvertedDistance = 5151;
            poLeg3.CrewDutyRule = null;
            poLeg3.CrewCurrency = "91";
            poLeg3.IsKilometer = false;

            poLog.PostflightLegs[0] = (PostflightLegViewModel)poLog.PostflightLegs[0].InjectFrom(poLeg1);
            poLog.PostflightLegs.Add(poLeg2);
            poLog.PostflightLegs.Add(poLeg3);

            return poLog;
        }

        [TestMethod]
        public void Test_DutyHourCalculation()
        {
            int LegNUM = 0;
            DateTime vScheduledTM = new DateTime();
            DateTime vOutboundDTTM = new DateTime();
            DateTime vInboundDTTM = new DateTime();

            IDBCatalogsDataLoader iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal) 1.5,
                        DutyDayEndTM = (decimal) 0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            PostflightLogViewModel pfViewModel = GetPostflightLogForTestWithLeg2();

            LegNUM = 1;
            vScheduledTM = (DateTime) pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime) pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime) pfViewModel.PostflightLegs[0].InboundDTTM;

            Hashtable actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Hashtable assume = new Hashtable();
            
            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "06:30";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

            LegNUM = 2;
            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[1].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[1].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[1].InboundDTTM;
            Hashtable actual2 = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Hashtable assume2 = new Hashtable();

            assume2["DutyEnd"] = "00:30:00";
            assume2["DutyHoursStyle"] = "gray";
            assume2["DutyBegin"] = "01:30:00";
            assume2["TotalHours"] = "12:00";

            Assert.AreEqual(assume2["DutyEnd"], actual2["DutyEnd"]);
            Assert.AreEqual(assume2["DutyHoursStyle"], actual2["DutyHoursStyle"]);
            Assert.AreEqual(assume2["DutyBegin"], actual2["DutyBegin"]);
            Assert.AreEqual(assume2["TotalHours"], actual2["TotalHours"]);

        }

        #region Simple test case for cundition of object null and empty with no leg and single leg

        [TestMethod]
        [Description("Test case for Postflight view model is null And Postflight Leg view model is null And Postflight Legs are not found")]
        public void Test_DutyHourCalculation_LogAndLegIsNull()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            IDBCatalogsDataLoader iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();

            PostflightLogViewModel pfViewModel = null;
            Hashtable actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Assert.AreEqual(0, actual.Count);

            pfViewModel = new PostflightLogViewModel();
            pfViewModel.PostflightLegs = null;
            actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Assert.AreEqual(0, actual.Count);

            pfViewModel = new PostflightLogViewModel();
            pfViewModel.PostflightLegs.RemoveAt(0); 
            actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Assert.AreEqual(0, actual.Count);

        }

        [TestMethod]
        [Description("Test case for current Postflight leg view model is null")]
        public void Test_DutyHourCalculation_CurrentLegIsNull()
        {
            int LegNUM = 2;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            IDBCatalogsDataLoader iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();

            PostflightLogViewModel pfViewModel = new PostflightLogViewModel();
            Hashtable actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Hashtable assume = new Hashtable();

            assume["DutyEnd"] = "00:00:00";
            assume["DutyBegin"] = "00:00:00";
            assume["TotalHours"] = "00:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);
        }

        [TestMethod]
        [Description("Test case for current Postflight leg CrewCurrency is null or empty")]
        public void Test_DutyHourCalculation_CurrentLeg_CrewCurrencyIsNullOrEmpty()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            IDBCatalogsDataLoader iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();

            PostflightLogViewModel pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = null;
            
            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            Hashtable actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Hashtable assume = new Hashtable();

            assume["DutyEnd"] = "00:00:00";
            assume["DutyHoursStyle"] = "red";
            assume["DutyBegin"] = "00:00:00";
            assume["TotalHours"] = "05:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

            pfViewModel.PostflightLegs[0].CrewCurrency = string.Empty;
            Hashtable actual2 = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Hashtable assume2 = new Hashtable();

            assume2["DutyEnd"] = "00:00:00";
            assume2["DutyHoursStyle"] = "red";
            assume2["DutyBegin"] = "00:00:00";
            assume2["TotalHours"] = "05:00";

            Assert.AreEqual(assume2["DutyEnd"], actual2["DutyEnd"]);
            Assert.AreEqual(assume2["DutyHoursStyle"], actual2["DutyHoursStyle"]);
            Assert.AreEqual(assume2["DutyBegin"], actual2["DutyBegin"]);
            Assert.AreEqual(assume2["TotalHours"], actual2["TotalHours"]);

        }

        [TestMethod]
        [Description("Test case for current Postflight leg CrewDutyRule is null or empty")]
        public void Test_DutyHourCalculation_CurrentLeg_CrewDutyRuleIsNullOrEmpty()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            IDBCatalogsDataLoader iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();

            PostflightLogViewModel pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "WRONGCODE";

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            Hashtable actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            Hashtable assume = new Hashtable();

            assume["DutyEnd"] = "00:00:00";
            assume["DutyHoursStyle"] = "red";
            assume["DutyBegin"] = "00:00:00";
            assume["TotalHours"] = "05:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 1 : User principal Duty Basis is Block Out")]
        public void Test_DutyHourCalculation_CurrentLeg_1_DutyBasis_BlockOut()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
               .Returns(value:
                   new CrewDutyRuleViewModel()
                   {
                       CrewDutyRuleCD = "91",
                       CrewDutyRulesID = 100071011,
                       DutyDayBeingTM = (decimal)1.5,
                       DutyDayEndTM = (decimal)0.5,
                       MaximumDutyHrs = 18,
                       MaximumFlightHrs = 10,
                       MinimumRestHrs = 6,
                       CrewDutyRulesDescription = "FAR 91 CREW RULES",
                       FedAviatRegNum = "91"
                   });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "91";

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "07:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 1 : User principal Duty Basis is Scheduled")]
        public void Test_DutyHourCalculation_CurrentLeg_1_DutyBasis_Scheduled()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
               .Returns(value:
                   new CrewDutyRuleViewModel()
                   {
                       CrewDutyRuleCD = "91",
                       CrewDutyRulesID = 100071011,
                       DutyDayBeingTM = (decimal)1.5,
                       DutyDayEndTM = (decimal)0.5,
                       MaximumDutyHrs = 18,
                       MaximumFlightHrs = 10,
                       MinimumRestHrs = 6,
                       CrewDutyRulesDescription = "FAR 91 CREW RULES",
                       FedAviatRegNum = "91"
                   });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.ScheduledDep;
            var pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "91";
            pfViewModel.PostflightLegs[0].ScheduledTime = "04:00";

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "08:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 1 : Duty start End is zero")]
        public void Test_DutyHourCalculation_CurrentLeg_1_DutyStartEndIsZero()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
               .Returns(value:
                   new CrewDutyRuleViewModel()
                   {
                       CrewDutyRuleCD = "91",
                       CrewDutyRulesID = 100071011,
                       DutyDayBeingTM = (decimal)0,
                       DutyDayEndTM = (decimal)0,
                       MaximumDutyHrs = 18,
                       MaximumFlightHrs = 10,
                       MinimumRestHrs = 6,
                       CrewDutyRulesDescription = "FAR 91 CREW RULES",
                       FedAviatRegNum = "91"
                   });
            var upVM = new UserPrincipalViewModel();
            var pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "91";
            pfViewModel.PostflightLegs[0].ScheduledTime = "04:00";

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:00:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "00:00:00";
            assume["TotalHours"] = "05:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 1 : Check Start and End time with datetime minimum value and StartTime < Endtime.")]
        public void Test_DutyHourCalculation_CurrentLeg_1_CheckStartAndEndTime()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
               .Returns(value:
                   new CrewDutyRuleViewModel()
                   {
                       CrewDutyRuleCD = "91",
                       CrewDutyRulesID = 100071011,
                       DutyDayBeingTM = (decimal)1.0,
                       DutyDayEndTM = (decimal)0.5,
                       MaximumDutyHrs = 18,
                       MaximumFlightHrs = 10,
                       MinimumRestHrs = 6,
                       CrewDutyRulesDescription = "FAR 91 CREW RULES",
                       FedAviatRegNum = "91"
                   });
            var upVM = new UserPrincipalViewModel();
            var pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "91";
            pfViewModel.PostflightLegs[0].ScheduledTime = "04:00";

            pfViewModel.PostflightLegs[0].ScheduledTM = DateTime.MinValue;
            pfViewModel.PostflightLegs[0].OutboundDTTM = DateTime.MinValue;
            pfViewModel.PostflightLegs[0].InboundDTTM = DateTime.MinValue;

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:00:00";
            assume["TotalHours"] = "00:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 1 : Check max Duty Hours is less then total duty hours.")]
        public void Test_DutyHourCalculation_CurrentLeg_1_CheckMaxDutyHours_LessThen_TotalDutyHours()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
               .Returns(value:
                   new CrewDutyRuleViewModel()
                   {
                       CrewDutyRuleCD = "91",
                       CrewDutyRulesID = 100071011,
                       DutyDayBeingTM = (decimal)1.0,
                       DutyDayEndTM = (decimal)0.5,
                       MaximumDutyHrs = 10,
                       MaximumFlightHrs = 10,
                       MinimumRestHrs = 6,
                       CrewDutyRulesDescription = "FAR 91 CREW RULES",
                       FedAviatRegNum = "91"
                   });
            var upVM = new UserPrincipalViewModel();
            var pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "91";
            pfViewModel.PostflightLegs[0].InTime = "20:00";

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "red";
            assume["DutyBegin"] = "01:00:00";
            assume["TotalHours"] = "16:30";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 1 : Current leg is 1 and Crew currency & crew duty rule are not empty.")]
        public void Test_DutyHourCalculation_CurrentLeg_1_CrewCurrency_And_CrewDutyRule_NotNullAndEmpty()
        {
            int LegNUM = 1;
            var vScheduledTM = new DateTime(2015, 10, 14);
            var vOutboundDTTM = new DateTime(2015, 10, 14);
            var vInboundDTTM = new DateTime(2015, 10, 14);
            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
               .Returns(value:
                   new CrewDutyRuleViewModel()
                   {
                       CrewDutyRuleCD = "91",
                       CrewDutyRulesID = 100071011,
                       DutyDayBeingTM = (decimal)1.0,
                       DutyDayEndTM = (decimal)0.5,
                       MaximumDutyHrs = 10,
                       MaximumFlightHrs = 10,
                       MinimumRestHrs = 6,
                       CrewDutyRulesDescription = "FAR 91 CREW RULES",
                       FedAviatRegNum = "91"
                   });
            var upVM = new UserPrincipalViewModel();
            var pfViewModel = GetPostflightLogForTestWithLeg1();
            pfViewModel.PostflightLegs[0].CrewCurrency = "91";

            vScheduledTM = (DateTime)pfViewModel.PostflightLegs[0].ScheduledTM;
            vOutboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].OutboundDTTM;
            vInboundDTTM = (DateTime)pfViewModel.PostflightLegs[0].InboundDTTM;

            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:00:00";
            assume["TotalHours"] = "06:30";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }
        #endregion

        #region Test case with 2 leg 
        [TestMethod]
        [Description("Leg 2 and Crew Currency is null and empty")]
        public void Test_DutyHourCalculation_CurrentLeg_2_CrewCurrencyIsEmpty()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal)1.5,
                        DutyDayEndTM = (decimal)0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg2();

            var poLeg =  new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[1]);

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = string.Empty;
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:00:00";
            assume["DutyHoursStyle"] = "red";
            assume["DutyBegin"] = "00:00:00";
            assume["TotalHours"] = "10:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 2 and crew duty rule is null and empty")]
        public void Test_DutyHourCalculation_CurrentLeg_2_CrewDutyRuleIsNullAndEmpty()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg2();

            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[1]);

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "EMPTYCREWCURRENCY";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:00:00";
            assume["DutyHoursStyle"] = "red";
            assume["DutyBegin"] = "00:00:00";
            assume["TotalHours"] = "10:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 2 and User principal Duty Basis is Block Out")]
        public void Test_DutyHourCalculation_CurrentLeg_2_DutyBasis_BlockOut()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal) 1.5,
                        DutyDayEndTM = (decimal) 0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg2();

            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[1]);
            poLeg.OutTime = "11:00";

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "91";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "12:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 2 and User principal Duty Basis is Scheduled ")]
        public void Test_DutyHourCalculation_CurrentLeg_2_DutyBasis_Scheduled()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal)1.5,
                        DutyDayEndTM = (decimal)0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.ScheduledDep;
            var pfViewModel = GetPostflightLogForTestWithLeg2();

            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[1]);
            poLeg.OutTime = "11:00";

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "91";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "12:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 2 and User principal Duty Basis is Block and IsDutyEnd is true for Leg 1")]
        public void Test_DutyHourCalculation_CurrentLeg_2_DutyBasis_BlockOut_IsDutyEnd_True()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal)1.5,
                        DutyDayEndTM = (decimal)0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg2();

            pfViewModel.PostflightLegs[0].IsDutyEnd = true;

            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[1]);
            poLeg.OutTime = "11:00";

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "91";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "07:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 2 and User principal Duty Basis is Scheduled ")]
        public void Test_DutyHourCalculation_CurrentLeg_2_DutyBasis_Scheduled_IsDutyEnd_True()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal)1.5,
                        DutyDayEndTM = (decimal)0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.ScheduledDep;
            var pfViewModel = GetPostflightLogForTestWithLeg2();
            pfViewModel.PostflightLegs[0].IsDutyEnd = true;
            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[1]);
            poLeg.OutTime = "11:00";
            poLeg.InTime = "16:00";

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "91";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "07:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }
        #endregion

        #region Test case with 3 leg
        [TestMethod]
        [Description("Leg 3 IsDutyEnd is true for Leg 1")]
        public void Test_DutyHourCalculation_CurrentLeg_3_IsDutyEnd_True_Leg1()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal)1.5,
                        DutyDayEndTM = (decimal)0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg3();

            pfViewModel.PostflightLegs[0].IsDutyEnd = true;

            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[2]);
            
            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "91";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "14:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }

        [TestMethod]
        [Description("Leg 3 IsDutyEnd is true for Leg 2")]
        public void Test_DutyHourCalculation_CurrentLeg_3_IsDutyEnd_True_Leg2()
        {
            int LegNUM = 0;
            var vScheduledTM = new DateTime();
            var vOutboundDTTM = new DateTime();
            var vInboundDTTM = new DateTime();

            var iDataLoader = A.Fake<IDBCatalogsDataLoader>();
            A.CallTo(() => iDataLoader.GetCrewDutyRulesInfo_FromFilter_I(0, "91"))
                .Returns(value:
                    new CrewDutyRuleViewModel()
                    {
                        CrewDutyRuleCD = "91",
                        CrewDutyRulesID = 100071011,
                        DutyDayBeingTM = (decimal)1.5,
                        DutyDayEndTM = (decimal)0.5,
                        MaximumDutyHrs = 18,
                        MaximumFlightHrs = 10,
                        MinimumRestHrs = 6,
                        CrewDutyRulesDescription = "FAR 91 CREW RULES",
                        FedAviatRegNum = "91"
                    });
            var upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = WebConstants.POCrewDutyBasis.BlockOut;
            var pfViewModel = GetPostflightLogForTestWithLeg3();

            pfViewModel.PostflightLegs[1].IsDutyEnd = true;

            var poLeg = new PostflightLegViewModel();
            poLeg = (PostflightLegViewModel)poLeg.InjectFrom(pfViewModel.PostflightLegs[2]);

            LegNUM = (int)poLeg.LegNUM;
            vScheduledTM = (DateTime)poLeg.ScheduledTM;
            vOutboundDTTM = (DateTime)poLeg.OutboundDTTM;
            vInboundDTTM = (DateTime)poLeg.InboundDTTM;
            pfViewModel.PostflightLegs[1].CrewCurrency = "91";
            var actual = PostFlightValidator.PODutyHourCalculation(LegNUM, vScheduledTM, vOutboundDTTM, vInboundDTTM, upVM, pfViewModel, iDataLoader);
            var assume = new Hashtable();

            assume["DutyEnd"] = "00:30:00";
            assume["DutyHoursStyle"] = "gray";
            assume["DutyBegin"] = "01:30:00";
            assume["TotalHours"] = "07:00";

            Assert.AreEqual(assume["DutyEnd"], actual["DutyEnd"]);
            Assert.AreEqual(assume["DutyHoursStyle"], actual["DutyHoursStyle"]);
            Assert.AreEqual(assume["DutyBegin"], actual["DutyBegin"]);
            Assert.AreEqual(assume["TotalHours"], actual["TotalHours"]);

        }
        #endregion

    }
}
