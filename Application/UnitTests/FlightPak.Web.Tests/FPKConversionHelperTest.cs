﻿using System;
using System.Collections.Generic;
using System.Data;
using FlightPak.Data.Preflight;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeItEasy;
using FlightPak.Web.AuthenticationService;
using FlightPak.Web.Framework.Helpers;
using FlightPak.Business.Common;
using FlightPak.Web.PreflightService;
using Omu.ValueInjecter;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class FPKConversionHelperTest
    {
        [TestMethod]
        public void Test_GetDecimalFormattedTime_String_ValidTime1()
        {
            var fpSettings = A.Fake<FPSettings>();
            fpSettings._TimeDisplayTenMin = 1;
            Decimal timeformatted = FPKConversionHelper.GetDecimalFormattedTime(fpSettings, "04:12");
            Assert.AreEqual((decimal)4.2, timeformatted);
        }
        [TestMethod]
        public void Test_GetDecimalFormattedTime_String_ValidTime2()
        {
            var fpSettings = A.Fake<FPSettings>();
            fpSettings._TimeDisplayTenMin = 1;
            Decimal timeformatted = FPKConversionHelper.GetDecimalFormattedTime(fpSettings, "04:17");
            Assert.AreEqual((decimal)4.2, timeformatted);
        }
        [TestMethod]
        public void Test_GetDecimalFormattedTime_String_ValidTime3()
        {
            var fpSettings = A.Fake<FPSettings>();
            fpSettings._TimeDisplayTenMin = 1;
            Decimal timeformatted = FPKConversionHelper.GetDecimalFormattedTime(fpSettings, "04:18");
            Assert.AreEqual((decimal)4.3, timeformatted);
        }
        [TestMethod]
        public void Test_ConvertMinToTenths()
        {
            PrivateType pt = new PrivateType(typeof(FPKConversionHelper));
            String strActual = (String)pt.InvokeStatic("ConvertMinToTenths", new object[] { "04:10", true, 2 });
            String strExpected = "4.2";
            Assert.AreEqual(strExpected, strActual);
        }


       
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_Test1()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("2.3833", 2);
            String strExpected = "02:22";
            Assert.AreEqual(strExpected, strActual);            
        }
        
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_Test2()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("0.8543", 2);
            String strExpected = "00:51";
            Assert.AreEqual(strExpected, strActual);
        }
       
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_Test3()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("0.1182", 2);
            String strExpected = "00:07";
            Assert.AreEqual(strExpected, strActual);
        }
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_Test4()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("6.8569", 2);
            String strExpected = "06:51";
            Assert.AreEqual(strExpected, strActual);
        }
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_Test5()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("6.21996", 2);
            String strExpected = "06:13";
            Assert.AreEqual(strExpected, strActual);
        }
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_Test6()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("15.8992", 2);
            String strExpected = "15:53";
            Assert.AreEqual(strExpected, strActual);
        }
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_TenthToTenth_Test1()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("2.3833", 1);
            String strExpected = "2.4";
            Assert.AreEqual(strExpected, strActual);
        }

        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_TenthToTenth_Test2()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("0.8543", 1);
            String strExpected = "0.9";
            Assert.AreEqual(strExpected, strActual);
        }

        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_TenthToTenth_Test3()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("0.1182", 1);
            String strExpected = "0.1";
            Assert.AreEqual(strExpected, strActual);
        }
        [TestMethod]
        public void ConvertTenthsToMins_NewMethod_TenthToTenth_Test4()
        {
            FPKConversionHelper oo = new FPKConversionHelper();
            String strActual = oo.Preflight_ConvertTenthsToMins("0.000", 1);
            String strExpected = "0.0";
            Assert.AreEqual(strExpected, strActual);
        }

    }
}
