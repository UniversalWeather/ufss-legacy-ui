﻿using System;
using System.Collections.Generic;
using FakeItEasy;
using FlightPak.Web.CalculationService;
using FlightPak.Web.Views.Transactions.Preflight;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlightPak.Web.Framework.Helpers.Validators;
using FlightPak.Web.Views.Transactions;


namespace FlightPak.Web.Tests
{
    [TestClass]
    public class PreflightValidatorTest
    {    
        [TestMethod]
        public void Real_Test_PrefightCalculation_Validate_Retrieve()
        {
            IPreflightUtils PreUtilities = A.Fake<IPreflightUtils>();
            // Depart Airport
            DepartAirportDetails deptObj=new DepartAirportDetails();
            deptObj.deplatdeg = 29.0;
            deptObj.deplatmin = 10.8;
            deptObj.lcdeplatdir = "N";
            deptObj.deplngdeg = 81.0;
            deptObj.deplngmin = 3.5;
            deptObj.lcdeplngdir = "W";
            deptObj.depIsDayLightSaving = true;
            deptObj.depDayLightSavingStartDT = new DateTime(2015, 3, 8, 00, 00, 00);
            deptObj.depDayLightSavingEndDT = new DateTime(2015, 11, 1, 00, 00, 00); ;
            deptObj.depDaylLightSavingStartTM = "07:00";
            deptObj.depDayLightSavingEndTM = "06:00";
            deptObj.depOffsetToGMT = -5;
            deptObj.lnDepartWindZone = 881;
            deptObj.DepAirportTakeoffBIAS = 0.0;
            deptObj.DepCD = "KDAB";
            deptObj.DepID = "100004346";

            //Arrival Airport
            ArrivalAirportDetails arrObj=new ArrivalAirportDetails();
            arrObj.arrlatdeg = 29.0;
            arrObj.arrlatmin = 38.7;
            arrObj.lcArrLatDir = "N";
            arrObj.arrlngdeg = 95.0;
            arrObj.arrlngmin = 16.7;
            arrObj.lcarrlngdir = "W";
            arrObj.arrIsDayLightSaving = true;
            arrObj.arrDayLightSavingStartDT = new DateTime(2015, 3, 8, 00, 00, 00); ;
            arrObj.arrDayLightSavingEndDT = new DateTime(2015, 11, 1, 00, 00, 00); ;
            arrObj.arrDaylLightSavingStartTM = "02:00";
            arrObj.arrDayLightSavingEndTM = "02:00";
            arrObj.arrOffsetToGMT = -6;
            arrObj.lnArrivWindZone = 884;
            arrObj.ArrAirportLandingBIAS = 0.2;
            arrObj.ArrivalCD = "KHOU";
            arrObj.ArrivalID = "10003181481";


            //Homebase Airport
            HomebaseAirportDetails homeobj=new HomebaseAirportDetails();
            homeobj.homeIsDayLightSaving = true;
            homeobj.homeDayLightSavingStartDT = new DateTime(2015, 3, 8, 00, 00, 00);
            homeobj.homeDayLightSavingEndDT = new DateTime(2015, 11, 1, 00, 00, 00); 
            homeobj.homeDaylLightSavingStartTM = "02:00";
            homeobj.homeDayLightSavingEndTM = "02:00";
            homeobj.homeOffsetToGMT = -6;
            homeobj.HomebaseCD = "KHOU";
            homeobj.HomebaseId = "10003181481";

            //Aircraft Details
            AircraftDetails aircraftObj=new AircraftDetails();
            aircraftObj.PowerSettings1TakeOffBias = 0.2;
            aircraftObj.PowerSettings1LandingBias = 0.2;
            aircraftObj.PowerSettings1TrueAirSpeed = 472.0;
            aircraftObj.PowerSettings2TakeOffBias = 0.2;
            aircraftObj.PowerSettings2LandingBias = 0.2;
            aircraftObj.PowerSettings2TrueAirSpeed = 466.0;
            aircraftObj.PowerSettings3TakeOffBias = 0.2;
            aircraftObj.PowerSettings3LandingBias = 0.2;
            aircraftObj.PowerSettings3TrueAirSpeed = 490.0;
            aircraftObj.IsFixedRotary = "F";
            aircraftObj.aircraftWindAltitude = 41000.0;
            aircraftObj.ChargeRate = 0.0;
            aircraftObj.ChargeUnit = "H";
            aircraftObj.Wind = 21.0;
            aircraftObj.TAS = "472";


            decimal TenMin = 1;
            decimal ElapseTimeRounding = 1;
            bool Kilo = false;
            int WindRelibility=3;
            string localdate = "10/16/2014";
            string DateForamte="MM/dd/yyyy";
            ICalculationService client = A.Fake<ICalculationService>();
            double _expectedete, _wind, _landBias, _tas, _bias, _miles = 0;

            decimal ExpectedMiles = 743;

            A.CallTo<decimal>(() => PreUtilities.CalculateDistance(Kilo, deptObj.deplatdeg, deptObj.deplatmin, deptObj.lcdeplatdir, deptObj.deplngdeg,
              deptObj.deplngmin, deptObj.lcdeplngdir, arrObj.arrlatdeg, arrObj.arrlatmin, arrObj.lcArrLatDir,
              arrObj.arrlngdeg, arrObj.arrlngmin, arrObj.lcarrlngdir)).Returns(743);


            decimal ActualMiles = PreUtilities.CalculateDistance(Kilo, deptObj.deplatdeg, deptObj.deplatmin, deptObj.lcdeplatdir, deptObj.deplngdeg,
                deptObj.deplngmin, deptObj.lcdeplngdir, arrObj.arrlatdeg, arrObj.arrlatmin, arrObj.lcArrLatDir,
                arrObj.arrlngdeg, arrObj.arrlngmin, arrObj.lcarrlngdir);
            String strExpected = "1";
            Assert.AreEqual(ExpectedMiles, ActualMiles);

            Dictionary<string,string> expectedBias=new Dictionary<string, string>();
            expectedBias.Add("Bias", "0.2");
            expectedBias.Add("LandBias", "0.4");
            expectedBias.Add("TAS", "472");

            A.CallTo<Dictionary<string, string>>(() => PreUtilities.CalculateBias("1", aircraftObj.PowerSettings1TakeOffBias, aircraftObj.PowerSettings1LandingBias, aircraftObj.PowerSettings1TrueAirSpeed,
             aircraftObj.PowerSettings2TakeOffBias, aircraftObj.PowerSettings2LandingBias, aircraftObj.PowerSettings2TrueAirSpeed,
             aircraftObj.PowerSettings3TakeOffBias, aircraftObj.PowerSettings3LandingBias, aircraftObj.PowerSettings3TrueAirSpeed,
             deptObj.DepAirportTakeoffBIAS, arrObj.ArrAirportLandingBIAS, TenMin, out _bias, out _landBias, out _tas)).Returns(expectedBias);



            Dictionary<string, string> actaulBias = PreUtilities.CalculateBias("1", aircraftObj.PowerSettings1TakeOffBias, aircraftObj.PowerSettings1LandingBias, aircraftObj.PowerSettings1TrueAirSpeed,
                aircraftObj.PowerSettings2TakeOffBias, aircraftObj.PowerSettings2LandingBias, aircraftObj.PowerSettings2TrueAirSpeed,
                aircraftObj.PowerSettings3TakeOffBias, aircraftObj.PowerSettings3LandingBias, aircraftObj.PowerSettings3TrueAirSpeed,
                deptObj.DepAirportTakeoffBIAS, arrObj.ArrAirportLandingBIAS, TenMin, out _bias, out _landBias,out _tas);
            
            double ExpectedWind = -54;

            A.CallTo<double>(() => PreUtilities.CalculateWind(aircraftObj.aircraftWindAltitude, WindRelibility, deptObj.deplatdeg, deptObj.deplatmin,
               deptObj.lcdeplatdir, deptObj.deplngdeg, deptObj.deplngmin, deptObj.lcdeplngdir, deptObj.lnDepartWindZone, arrObj.arrlatdeg,
               arrObj.arrlatmin, arrObj.lcArrLatDir, arrObj.arrlngdeg, arrObj.arrlngmin, arrObj.lcarrlngdir, arrObj.lnArrivWindZone, localdate, client, DateForamte)).Returns(-54);


            double actualWind=PreUtilities.CalculateWind(aircraftObj.aircraftWindAltitude,WindRelibility,deptObj.deplatdeg,deptObj.deplatmin,
                deptObj.lcdeplatdir,deptObj.deplngdeg,deptObj.deplngmin,deptObj.lcdeplngdir,deptObj.lnDepartWindZone,arrObj.arrlatdeg,
                arrObj.arrlatmin,arrObj.lcArrLatDir,arrObj.arrlngdeg,arrObj.arrlngmin,arrObj.lcarrlngdir,arrObj.lnArrivWindZone,localdate,client,DateForamte);

            //Assert.AreEqual(ExpectedWind,actualWind);
            
            _expectedete=2.4;
            _wind = ExpectedWind;
            _landBias = Convert.ToDouble(actaulBias["LandBias"]);
            _tas = Convert.ToDouble(actaulBias["TAS"]);
            _bias = Convert.ToDouble(actaulBias["Bias"]);
            _miles = Convert.ToDouble(ExpectedMiles);
            //double _actualete=PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles,aircraftObj.IsFixedRotary, TenMin, ElapseTimeRounding);
            double _actualete;
            PreflightUtils PreUtilitiesObj = new PreflightUtils(); // To Test Read EteCalculation method

            double.TryParse(PreUtilitiesObj.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, aircraftObj.IsFixedRotary, TenMin, ElapseTimeRounding), out _actualete);

            Assert.AreEqual(_expectedete,_actualete);

        }
        [TestMethod]
        public void Test_PrefightCalculation_Validate_Retrieve()
        {
            IPreflightUtils PreUtilities=A.Fake<IPreflightUtils>();
            // Depart Airport
            DepartAirportDetails deptObj=new DepartAirportDetails();
            deptObj.deplatdeg = 29.0;
            deptObj.deplatmin = 59.1;
            deptObj.lcdeplatdir = "N";
            deptObj.deplngdeg = 95.0;
            deptObj.deplngmin = 20.5;
            deptObj.lcdeplngdir = "W";
            deptObj.depIsDayLightSaving = true;
            deptObj.depDayLightSavingStartDT = new DateTime(2014, 3, 9, 00, 00, 00);
            deptObj.depDayLightSavingEndDT = new DateTime(2014, 11, 2, 00, 00, 00); ;
            deptObj.depDaylLightSavingStartTM = "02:00";
            deptObj.depDayLightSavingEndTM = "02:00";
            deptObj.depOffsetToGMT = -6;
            deptObj.lnDepartWindZone = 884;
            deptObj.DepAirportTakeoffBIAS = 0.0;

            //Arrival Airport
            ArrivalAirportDetails arrObj=new ArrivalAirportDetails();
            arrObj.arrlatdeg = 23.0;
            arrObj.arrlatmin = 35.4;
            arrObj.lcArrLatDir = "S";
            arrObj.arrlngdeg = 46.0;
            arrObj.arrlngmin = 15.7;
            arrObj.lcarrlngdir = "W";
            arrObj.arrIsDayLightSaving = true;
            arrObj.arrDayLightSavingStartDT = new DateTime(1899, 12, 30, 00, 00, 00); ;
            arrObj.arrDayLightSavingEndDT = new DateTime(1899, 12, 30, 00, 00, 00); ;
            arrObj.arrDaylLightSavingStartTM = null;
            arrObj.arrDayLightSavingEndTM = null;
            arrObj.arrOffsetToGMT = Convert.ToDecimal(5.5);
            arrObj.lnArrivWindZone = 1594;
            arrObj.ArrAirportLandingBIAS = 0.0;

            //Homebase Airport
            HomebaseAirportDetails homeobj=new HomebaseAirportDetails();
            homeobj.homeIsDayLightSaving = true;
            homeobj.homeDayLightSavingStartDT = new DateTime(2014, 3, 9, 0, 0, 0);
            homeobj.homeDayLightSavingEndDT = new DateTime(2014, 11, 2, 0, 0, 0);
            homeobj.homeDaylLightSavingStartTM = "02:00";
            homeobj.homeDayLightSavingEndTM = "02:00";
            homeobj.homeOffsetToGMT = -6;

            //Aircraft Details
            AircraftDetails aircraftObj=new AircraftDetails();
            aircraftObj.PowerSettings1TakeOffBias = 0.167;
            aircraftObj.PowerSettings1LandingBias = 0.167;
            aircraftObj.PowerSettings1TrueAirSpeed = 460.0;
            aircraftObj.PowerSettings2TakeOffBias = 0.167;
            aircraftObj.PowerSettings2LandingBias = 0.167;
            aircraftObj.PowerSettings2TrueAirSpeed = 450.0;
            aircraftObj.PowerSettings3TakeOffBias = 0.167;
            aircraftObj.PowerSettings3LandingBias = 0.167;
            aircraftObj.PowerSettings3TrueAirSpeed = 475.0;
            aircraftObj.IsFixedRotary = "F";
            aircraftObj.aircraftWindAltitude = 41000.0;
            aircraftObj.ChargeRate = 1000.0;
            aircraftObj.ChargeUnit = "H";

            decimal TenMin = 1;
            decimal ElapseTimeRounding = 1;
            bool Kilo = false;
            int WindRelibility=3;
            string localdate = "12/12/2014";
            string DateForamte="MM/dd/yyyy";
            ICalculationService client = A.Fake<ICalculationService>();
            double _expectedete, _wind, _landBias, _tas, _bias, _miles = 0;
            
            decimal ExpectedMiles = 4280;

            A.CallTo<decimal>(()=> PreUtilities.CalculateDistance(Kilo, deptObj.deplatdeg, deptObj.deplatmin, deptObj.lcdeplatdir, deptObj.deplngdeg,
                deptObj.deplngmin, deptObj.lcdeplngdir, arrObj.arrlatdeg, arrObj.arrlatmin, arrObj.lcArrLatDir,
                arrObj.arrlngdeg, arrObj.arrlngmin, arrObj.lcarrlngdir)).Returns(4280);

            var ActualMiles = PreUtilities.CalculateDistance(Kilo, deptObj.deplatdeg, deptObj.deplatmin, deptObj.lcdeplatdir, deptObj.deplngdeg,
                deptObj.deplngmin, deptObj.lcdeplngdir, arrObj.arrlatdeg, arrObj.arrlatmin, arrObj.lcArrLatDir,
                arrObj.arrlngdeg, arrObj.arrlngmin, arrObj.lcarrlngdir);
            
            Assert.AreEqual(ExpectedMiles, ActualMiles);

            Dictionary<string,string> expectedBias=new Dictionary<string, string>();
            expectedBias.Add("Bias", "0.2");
            expectedBias.Add("LandBias", "0.2");
            expectedBias.Add("TAS", "460");

            A.CallTo<Dictionary<string, string>>(() => PreUtilities.CalculateBias("1", aircraftObj.PowerSettings1TakeOffBias, aircraftObj.PowerSettings1LandingBias, aircraftObj.PowerSettings1TrueAirSpeed,
                aircraftObj.PowerSettings2TakeOffBias, aircraftObj.PowerSettings2LandingBias, aircraftObj.PowerSettings2TrueAirSpeed,
                aircraftObj.PowerSettings3TakeOffBias, aircraftObj.PowerSettings3LandingBias, aircraftObj.PowerSettings3TrueAirSpeed,
                deptObj.DepAirportTakeoffBIAS, arrObj.ArrAirportLandingBIAS, TenMin, out _bias, out _landBias, out _tas)).Returns(expectedBias);


            Dictionary<string, string> actaulBias = PreUtilities.CalculateBias("1", aircraftObj.PowerSettings1TakeOffBias, aircraftObj.PowerSettings1LandingBias, aircraftObj.PowerSettings1TrueAirSpeed,
                aircraftObj.PowerSettings2TakeOffBias, aircraftObj.PowerSettings2LandingBias, aircraftObj.PowerSettings2TrueAirSpeed,
                aircraftObj.PowerSettings3TakeOffBias, aircraftObj.PowerSettings3LandingBias, aircraftObj.PowerSettings3TrueAirSpeed,
                deptObj.DepAirportTakeoffBIAS, arrObj.ArrAirportLandingBIAS, TenMin, out _bias, out _landBias,out _tas);
            
            double ExpectedWind = -4;

            A.CallTo<double>(() => PreUtilities.CalculateWind(aircraftObj.aircraftWindAltitude, WindRelibility, deptObj.deplatdeg, deptObj.deplatmin,
                deptObj.lcdeplatdir, deptObj.deplngdeg, deptObj.deplngmin, deptObj.lcdeplngdir, deptObj.lnDepartWindZone, arrObj.arrlatdeg,
                arrObj.arrlatmin, arrObj.lcArrLatDir, arrObj.arrlngdeg, arrObj.arrlngmin, arrObj.lcarrlngdir, arrObj.lnArrivWindZone, localdate, client, DateForamte)).Returns(-4);


            double actualWind=PreUtilities.CalculateWind(aircraftObj.aircraftWindAltitude,WindRelibility,deptObj.deplatdeg,deptObj.deplatmin,
                deptObj.lcdeplatdir,deptObj.deplngdeg,deptObj.deplngmin,deptObj.lcdeplngdir,deptObj.lnDepartWindZone,arrObj.arrlatdeg,
                arrObj.arrlatmin,arrObj.lcArrLatDir,arrObj.arrlngdeg,arrObj.arrlngmin,arrObj.lcarrlngdir,arrObj.lnArrivWindZone,localdate,client,DateForamte);

            //Assert.AreEqual(ExpectedWind,actualWind);
            
            _expectedete=9.8;
            _wind = ExpectedWind;
            _landBias = Convert.ToDouble(actaulBias["LandBias"]);
            _tas = Convert.ToDouble(actaulBias["TAS"]);
            _bias = Convert.ToDouble(actaulBias["Bias"]);
            _miles = Convert.ToDouble(ExpectedMiles);
            //double _actualete=PreUtilities.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles,aircraftObj.IsFixedRotary, TenMin, ElapseTimeRounding);
            PreflightUtils PreUtilitiesObj = new PreflightUtils(); // To Test Read EteCalculation method
            double _actualete;
            double.TryParse(PreUtilitiesObj.GetIcaoEte(_wind, _landBias, _tas, _bias, _miles, aircraftObj.IsFixedRotary, TenMin, ElapseTimeRounding), out _actualete);
            Assert.AreEqual(_expectedete,_actualete);
        }
    }
}
