﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightPak.Web.Tests
{
    public class TestUtils
    {
        public static bool HashTables(Hashtable htExpected, Hashtable htActual)
        {
            int destination_has_all = 1; //
            foreach (DictionaryEntry de in htExpected)
            {
                if (!htActual.ContainsKey(de.Key) || htActual[de.Key].ToString() != htExpected[de.Key].ToString())
                {
                    destination_has_all = 0;// Destination is missing something
                    break;
                }
            }
            return destination_has_all == 1;
        }

        public static bool DictionaryTest(Dictionary<string,string> Expexted,Dictionary<string,string> Actual)
        {
            int destination_has_all = 1;
            foreach (KeyValuePair<string, string> de in Expexted)
            {
                if(!Actual.ContainsKey(de.Key) || Actual[de.Key].ToString() !=Expexted[de.Key].ToString())
                {
                    destination_has_all = 0;
                    break;
                }
            }
            return destination_has_all == 1;
        }

    }
}
