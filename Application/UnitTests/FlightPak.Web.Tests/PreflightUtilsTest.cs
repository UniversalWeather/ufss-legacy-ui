﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlightPak.Web.Views.Transactions.Preflight;
using FakeItEasy;
using FlightPak.Web.CalculationService;
using FlightPak.Web.Framework.Helpers;
using System.Globalization;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class PreflightUtilsTest
    {
        //[TestMethod]
        //public void Test_CalculateLegDateTime_Valid1()
        //{
        //    Hashtable expectedHash = new Hashtable();
        //    ICalculationService client = A.Fake<ICalculationService>();
        //    DateTime departureTime = new DateTime(2014, 12, 10, 10, 0, 0);
        //    expectedHash.Add("DepartureLocalDate", "12/10/2014");
        //    expectedHash.Add("DepartureUtcDate", "12/10/2014");
        //    expectedHash.Add("DepartureHomeDate", "12/10/2014");

        //    expectedHash.Add("LocalDepartureTime", "10:00");
        //    expectedHash.Add("UtcDepartureTime", "14:00");
        //    expectedHash.Add("HomeDepartureTime", "00:00");

        //    expectedHash.Add("LocalArrivalTime", "06:30");
        //    expectedHash.Add("UTCArrivalTime", "00:00");
        //    expectedHash.Add("HomeArrivalTime", "00:00");

        //    A.CallTo(() => client.GetGMT(1111, departureTime, true, false)).Returns(departureTime.AddHours(4));
        //    Hashtable actual = new PreflightUtils().CalculateLegDateTime(departureTime, "10:00", "00:00", "00:00", "06:30",
        //        "00:00", "00:00", "MM/dd/yyyy", "1111", true, client);

        //    Assert.IsTrue(TestUtils.HashTables(expectedHash, actual));
        //}

        [TestMethod]
        public void Tes_CalculationTimeAdjustment_Valid1()
        {
            String expected = "12:34";
            String actual = MiscUtils.CalculationTimeAdjustment("12:34");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Tes_CalculationTimeAdjustment_Valid2()
        {
            String expected = "12:03";
            String actual = MiscUtils.CalculationTimeAdjustment("12:3");
            Assert.AreEqual(expected, actual);
        }
        
        [TestMethod]
        public void Tes_CalculationTimeAdjustment_InValid_1()
        {
            String expected = "12:00";
            String actual = MiscUtils.CalculationTimeAdjustment("12");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_GetQtr_Valid1()
        {
            int expected = 1;
            // Edited to build 1072 fail, this test in not acurate depending on the month of the year will fail
            DateTime thisDate1 = new DateTime(2015, 1, 10);
            PrivateObject pt = new PrivateObject(new PreflightUtils());
            int actual = (int)pt.Invoke("GetQtr", new object[] { thisDate1 });
            Assert.AreEqual(expected,actual);
        }

        [TestMethod]
        public void Test_GetQtr_InValid1()
        {
            int expected = 12;
            PrivateObject pt = new PrivateObject(new PreflightUtils());
            int actual = (int)pt.Invoke("GetQtr", new object[] { DateTime.Now });
            Assert.AreNotEqual(expected, actual);
        }

        [TestMethod]
        public void Test_CalculateDistance_Valid1()
        {
            //Dept ICAO : AA15
            //Arrival ICAO : CZML
            decimal expected = 6524;
            decimal actual = new PreflightUtils().CalculateDistance(false, 2, 3.5, "S", 3, 4.5, "W", 51, 44.2, "N", 121, 20, "W");
            Assert.AreEqual(expected,actual);
        }

        [TestMethod]
        public void Test_CalculateDistance_Valid2()
        {
            //Returns in Miles
            //Dept ICAO : LECO
            //Arrival ICAO : SNLI
            decimal expected = 4268;
            decimal actual = new PreflightUtils().CalculateDistance(false, 43, 18.1, "N", 8, 22.6, "W", 19, 9.3, "S", 45, 29.7, "W");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_CalculateDistance_Valid3()
        {
            // Returns in KM
            //Dept ICAO : CYXX
            //Arrival ICAO : OEAB
            decimal expected = 6685;
            decimal actual = new PreflightUtils().CalculateDistance(true, 49, 1.5, "N", 122, 21.6, "W", 18, 14.4, "N", 42, 39.4, "E");
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_CalculateBias_Valid1()
        {
            //Aircraft : B-707-300
            Dictionary<string, string> expected = new Dictionary<string, string>();
            expected.Add("Bias", "0.2");
            expected.Add("LandBias", "0.2");
            expected.Add("TAS", "460");
            double _bias, _landBias, _tas;
            Dictionary<string, string> actual = new PreflightUtils().CalculateBias("1", 0.167, 0.167, 460.0, 0.167,
                0.167, 450.0, 0.167, 0.167, 475.0, 0.0, 0.0, 1, out _bias, out _landBias, out _tas);
            Assert.IsTrue(TestUtils.DictionaryTest(expected, actual));
        }

        [TestMethod]
        public void Test_FSSParseDateTime_Valid1()
        {
            //British, French, DMY
            DateTime? expecteddt = DateTime.ParseExact("20/01/2015", "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string date = "20/01/2015";
            DateTime? actualdt = null;
            actualdt = actualdt.FSSParseDateTime(date, "dd/MM/yyyy");
            Assert.AreEqual<DateTime?>(expecteddt, actualdt);
        }

        [TestMethod]
        public void Test_FSSParseDateTime_Valid2()
        {
            //German
            DateTime? expecteddt = DateTime.ParseExact("20.01.2015", "dd.MM.yyyy", CultureInfo.InvariantCulture);
            string date = "20.01.2015";
            DateTime? actualdt = null;
            actualdt = actualdt.FSSParseDateTime(date, "dd.MM.yyyy");
            Assert.AreEqual<DateTime?>(expecteddt, actualdt);
        }

        [TestMethod]
        public void Test_FSSParseDateTime_Valid3()
        {
            //ANSI
            DateTime? expecteddt = DateTime.ParseExact("2015.01.20", "yyyy.MM.dd", CultureInfo.InvariantCulture);
            string date = "2015.01.20";
            DateTime? actualdt = null;
            actualdt = actualdt.FSSParseDateTime(date, "yyyy.MM.dd");
            Assert.AreEqual<DateTime?>(expecteddt, actualdt);
        }

        [TestMethod]
        public void Test_FSSParseDateTime_Valid4()
        {
            //Italian
            DateTime? expecteddt = DateTime.ParseExact("20-01-2015", "dd-MM-yyyy", CultureInfo.InvariantCulture);
            string date = "20-01-2015";
            DateTime? actualdt = null;
            actualdt = actualdt.FSSParseDateTime(date, "dd-MM-yyyy");
            Assert.AreEqual<DateTime?>(expecteddt, actualdt);
        }

        [TestMethod]
        public void Test_FSSParseDateTime_Valid5()
        {
            //Japan, Taiwan, YMD
            DateTime? expecteddt = DateTime.ParseExact("2015/01/20", "yyyy/MM/dd", CultureInfo.InvariantCulture);
            string date = "2015/01/20";
            DateTime? actualdt = null;
            actualdt = actualdt.FSSParseDateTime(date, "yyyy/MM/dd");
            Assert.AreEqual<DateTime?>(expecteddt, actualdt);
        }

        [TestMethod]
        public void Test_FSSParseDateTime_Valid6()
        {
            //MM/dd/yyyy
            DateTime? expecteddt = DateTime.ParseExact("01/20/2015", "MM/dd/yyyy", CultureInfo.InvariantCulture);
            string date = "01/20/2015";
            DateTime? actualdt = null;
            actualdt = actualdt.FSSParseDateTime(date, "MM/dd/yyyy");
            Assert.AreEqual<DateTime?>(expecteddt, actualdt);
        }
    }
}
