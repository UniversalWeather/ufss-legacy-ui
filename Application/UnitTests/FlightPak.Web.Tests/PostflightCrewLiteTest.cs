﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FlightPak.Web.ViewModels.MasterData;
using FlightPak.Web.ViewModels.Postflight;
using FlightPak.Web.ViewModels;
using FlightPak.Web.BusinessLite.Postflight;

namespace FlightPak.Web.Tests
{
    [TestClass]
    public class PostflightCrewLiteTest
    {
        public enum DutyBasisForTest { Scheduled = 2, BlockOut = 1 };

        #region DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled
        //FPK Log - 88
        [TestMethod]
        public void DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);
            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);

            Assert.AreEqual(1, daysAwayLeg1);
        }
        #endregion DaysAway_Scenario_1_One_Leg_OneCrew_SameDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_2_One_Leg_TwoCrews_SameDay_Duty_Basis_Scheduled
        //FPK Log - 91
        [TestMethod]
        public void DaysAway_Scenario_2_One_Leg_TwoCrews_SameDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 2;
            pl1.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg1_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 1);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(1, daysAwayLeg1_Crew2);
        }
        #endregion DaysAway_Scenario_2_One_Leg_TwoCrews_SameDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_2_One_Leg_TwoCrews_SameDay_Duty_Basis_Scheduled
        //FPK Log - 91
        [TestMethod]
        public void DaysAway_Scenario_2_1_One_Leg_TwoCrews_SameDay_Duty_Basis_Blockout()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.BlockOut;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 2;
            pl1.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg1_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 1);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(1, daysAwayLeg1_Crew2);
        }
        #endregion

        #region DaysAway_Scenario_3_One_Leg_OneCrew_NextDay_Duty_Basis_Scheduled
        //FPK Log - 92
        [TestMethod]
        public void DaysAway_Scenario_3_One_Leg_OneCrew_NextDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 6, 01, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);
            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);

            Assert.AreEqual(2, daysAwayLeg1);
        }
        #endregion DaysAway_Scenario_3_One_Leg_OneCrew_NextDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_4_One_Leg_TwoCrews_NextDay_Duty_Basis_Scheduled
        //FPK Log - won't be different from single leg single crew scenario - use Log 91 from FPK
        [TestMethod]
        public void DaysAway_Scenario_4_One_Leg_TwoCrews_NextDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 01, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 2;
            pl1.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg1_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 1);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(1, daysAwayLeg1_Crew2);
        }
        #endregion DaysAway_Scenario_4_One_Leg_TwoCrews_NextDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_5_Two_Legs_OneCrew_SameDay_Duty_Basis_Scheduled
        //FPK Log - 93
        [TestMethod]
        public void DaysAway_Scenario_5_Two_Legs_OneCrew_SameDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 5, 13, 5, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
        }
        #endregion DaysAway_Scenario_5_Two_Legs_OneCrew_SameDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_5_1_Two_Legs_OneCrew_SameDay_Duty_Basis_Scheduled
        //FPK Log - 93
        [TestMethod]
        public void DaysAway_Scenario_5_1_Two_Legs_OneCrew_SameDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.BlockOut;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 5, 13, 5, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
        }
        #endregion

        #region DaysAway_Scenario_6_Two_Legs_Two_Crews_SameDay_Duty_Basis_Scheduled
        //FPK Log - 93
        [TestMethod]
        public void DaysAway_Scenario_6_Two_Legs_Two_Crews_SameDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegCrewViewModel plc11 = new PostflightLegCrewViewModel(upVM);
            plc11.CrewID = 2;
            pl1.PostflightLegCrews.Add(plc11);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 5, 13, 5, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            PostflightLegCrewViewModel plc21 = new PostflightLegCrewViewModel(upVM);
            plc21.CrewID = 2;
            pl2.PostflightLegCrews.Add(plc21);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg1_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg2_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 2);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(1, daysAwayLeg1_Crew2);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew2);
        }
        #endregion DaysAway_Scenario_6_Two_Legs_Two_Crews_SameDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_7_Two_Legs_OneCrew_NextDay_Duty_Basis_Scheduled
        //FPK Log - 94
        [TestMethod]
        public void DaysAway_Scenario_7_Two_Legs_OneCrew_NextDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 8, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 8, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 6, 1, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 6, 1, 28, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 6, 1, 50, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 7, 01, 0, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);

            Assert.AreEqual(2, daysAwayLeg1_Crew1);
            Assert.AreEqual(2, daysAwayLeg2_Crew1);
        }
        #endregion DaysAway_Scenario_7_Two_Legs_OneCrew_NextDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_8_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled
        //FPK Log - 95
        [TestMethod]
        public void DaysAway_Scenario_8_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegCrewViewModel plc11 = new PostflightLegCrewViewModel(upVM);
            plc11.CrewID = 2;
            pl1.PostflightLegCrews.Add(plc11);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 5, 13, 5, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            PostflightLegViewModel pl3 = new PostflightLegViewModel();
            pl3.LegNUM = 3;
            pl3.ScheduledTM = new DateTime(2015, 10, 5, 15, 50, 0);
            pl3.OutboundDTTM = new DateTime(2015, 10, 5, 15, 55, 0);
            pl3.InboundDTTM = new DateTime(2015, 10, 5, 18, 10, 0);

            PostflightLegCrewViewModel plc3 = new PostflightLegCrewViewModel(upVM);
            plc3.CrewID = 1;
            pl3.PostflightLegCrews.Add(plc3);


            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            TripLog.PostflightLegs.Add(pl3);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg1_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg3_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 3);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(1, daysAwayLeg1_Crew2);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
            Assert.AreEqual(0, daysAwayLeg3_Crew1);
        }
        #endregion DaysAway_Scenario_8_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled

        #region DaysAway_Scenario_9_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L2
        //FPK Log - 95
        [TestMethod]
        public void DaysAway_Scenario_9_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L2()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 5, 13, 5, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            PostflightLegCrewViewModel plc21 = new PostflightLegCrewViewModel(upVM);
            plc21.CrewID = 2;
            pl2.PostflightLegCrews.Add(plc21);


            PostflightLegViewModel pl3 = new PostflightLegViewModel();
            pl3.LegNUM = 3;
            pl3.ScheduledTM = new DateTime(2015, 10, 5, 15, 50, 0);
            pl3.OutboundDTTM = new DateTime(2015, 10, 5, 15, 55, 0);
            pl3.InboundDTTM = new DateTime(2015, 10, 5, 18, 10, 0);

            PostflightLegCrewViewModel plc3 = new PostflightLegCrewViewModel(upVM);
            plc3.CrewID = 1;
            pl3.PostflightLegCrews.Add(plc3);


            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            TripLog.PostflightLegs.Add(pl3);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg2_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 2);
            decimal daysAwayLeg3_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 3);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
            Assert.AreEqual(1, daysAwayLeg2_Crew2);
            Assert.AreEqual(0, daysAwayLeg3_Crew1);
        }
        #endregion DaysAway_Scenario_9_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L2

        #region DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3
        //FPK Log - 95
        [TestMethod]
        public void DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 5, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 5, 13, 0, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 5, 13, 5, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 5, 15, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            PostflightLegViewModel pl3 = new PostflightLegViewModel();
            pl3.LegNUM = 3;
            pl3.ScheduledTM = new DateTime(2015, 10, 5, 15, 50, 0);
            pl3.OutboundDTTM = new DateTime(2015, 10, 5, 15, 55, 0);
            pl3.InboundDTTM = new DateTime(2015, 10, 5, 18, 10, 0);

            PostflightLegCrewViewModel plc3 = new PostflightLegCrewViewModel(upVM);
            plc3.CrewID = 1;
            pl3.PostflightLegCrews.Add(plc3);

            PostflightLegCrewViewModel plc31 = new PostflightLegCrewViewModel(upVM);
            plc31.CrewID = 2;
            pl3.PostflightLegCrews.Add(plc31);



            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            TripLog.PostflightLegs.Add(pl3);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg3_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 3);
            decimal daysAwayLeg3_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 3);

            Assert.AreEqual(1, daysAwayLeg1_Crew1);
            Assert.AreEqual(0, daysAwayLeg2_Crew1);
            Assert.AreEqual(0, daysAwayLeg3_Crew1);
            Assert.AreEqual(1, daysAwayLeg3_Crew2);
        }
        #endregion DaysAway_Scenario_10_Three_Legs_Two_Crews_FirstLeg_BothCrews_SameDay_Duty_Basis_Scheduled_L3

        #region DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled
        //FPK Log - 96
        [TestMethod]
        public void DaysAway_Scenario_11_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 9, 28, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 9, 28, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 9, 29, 1, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 9, 29, 1, 28, 0);
            pl2.OutboundDTTM = new DateTime(2015, 9, 29, 1, 40, 0);
            pl2.InboundDTTM = new DateTime(2015, 9, 30, 2, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            PostflightLegViewModel pl3 = new PostflightLegViewModel();
            pl3.LegNUM = 3;
            pl3.ScheduledTM = new DateTime(2015, 10, 1, 5, 50, 0);
            pl3.OutboundDTTM = new DateTime(2015, 10, 1, 5, 55, 0);
            pl3.InboundDTTM = new DateTime(2015, 10, 3, 1, 10, 0);

            PostflightLegCrewViewModel plc3 = new PostflightLegCrewViewModel(upVM);
            plc3.CrewID = 1;
            pl3.PostflightLegCrews.Add(plc3);

            PostflightLegCrewViewModel plc31 = new PostflightLegCrewViewModel(upVM);
            plc31.CrewID = 2;
            pl3.PostflightLegCrews.Add(plc31);

            PostflightLegViewModel pl4 = new PostflightLegViewModel();
            pl4.LegNUM = 4;
            pl4.ScheduledTM = new DateTime(2015, 10, 3, 6, 50, 0);
            pl4.OutboundDTTM = new DateTime(2015, 10, 3, 6, 55, 0);
            pl4.InboundDTTM = new DateTime(2015, 10, 4, 18, 10, 0);

            PostflightLegCrewViewModel plc41 = new PostflightLegCrewViewModel(upVM);
            plc41.CrewID = 2;
            pl4.PostflightLegCrews.Add(plc41);


            PostflightLegViewModel pl5 = new PostflightLegViewModel();
            pl5.LegNUM = 5;
            pl5.ScheduledTM = new DateTime(2015, 10, 4, 19, 20, 0);
            pl5.OutboundDTTM = new DateTime(2015, 10, 4, 19, 25, 0);
            pl5.InboundDTTM = new DateTime(2015, 10, 4, 22, 10, 0);

            PostflightLegCrewViewModel plc5 = new PostflightLegCrewViewModel(upVM);
            plc5.CrewID = 1;
            pl5.PostflightLegCrews.Add(plc5);

            PostflightLegCrewViewModel plc51 = new PostflightLegCrewViewModel(upVM);
            plc51.CrewID = 2;
            pl5.PostflightLegCrews.Add(plc51);


            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            TripLog.PostflightLegs.Add(pl3);
            TripLog.PostflightLegs.Add(pl4);
            TripLog.PostflightLegs.Add(pl5);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg3_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 3);
            decimal daysAwayLeg3_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 3);
            decimal daysAwayLeg4_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 4);
            decimal daysAwayLeg5_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 5);
            decimal daysAwayLeg5_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 5);

            Assert.AreEqual(2, daysAwayLeg1_Crew1);
            Assert.AreEqual(2, daysAwayLeg2_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew2);

            Assert.AreEqual(2, daysAwayLeg4_Crew2);
            Assert.AreEqual(1, daysAwayLeg5_Crew1);
            Assert.AreEqual(0, daysAwayLeg5_Crew2);
        }
        #endregion

        #region DaysAway_Scenario_11_1_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Blockout
        //FPK Log - 96
        [TestMethod]
        public void DaysAway_Scenario_11_1_Three_Legs_Two_Crews_FirstLeg_BothCrews_NextDay_Duty_Basis_Blockout()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.BlockOut;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 9, 28, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 9, 28, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 9, 29, 1, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 9, 29, 1, 28, 0);
            pl2.OutboundDTTM = new DateTime(2015, 9, 29, 1, 40, 0);
            pl2.InboundDTTM = new DateTime(2015, 9, 30, 2, 10, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            PostflightLegViewModel pl3 = new PostflightLegViewModel();
            pl3.LegNUM = 3;
            pl3.ScheduledTM = new DateTime(2015, 10, 1, 5, 50, 0);
            pl3.OutboundDTTM = new DateTime(2015, 10, 1, 5, 55, 0);
            pl3.InboundDTTM = new DateTime(2015, 10, 3, 1, 10, 0);

            PostflightLegCrewViewModel plc3 = new PostflightLegCrewViewModel(upVM);
            plc3.CrewID = 1;
            pl3.PostflightLegCrews.Add(plc3);

            PostflightLegCrewViewModel plc31 = new PostflightLegCrewViewModel(upVM);
            plc31.CrewID = 2;
            pl3.PostflightLegCrews.Add(plc31);

            PostflightLegViewModel pl4 = new PostflightLegViewModel();
            pl4.LegNUM = 4;
            pl4.ScheduledTM = new DateTime(2015, 10, 3, 6, 50, 0);
            pl4.OutboundDTTM = new DateTime(2015, 10, 3, 6, 55, 0);
            pl4.InboundDTTM = new DateTime(2015, 10, 4, 18, 10, 0);

            PostflightLegCrewViewModel plc41 = new PostflightLegCrewViewModel(upVM);
            plc41.CrewID = 2;
            pl4.PostflightLegCrews.Add(plc41);


            PostflightLegViewModel pl5 = new PostflightLegViewModel();
            pl5.LegNUM = 5;
            pl5.ScheduledTM = new DateTime(2015, 10, 4, 19, 20, 0);
            pl5.OutboundDTTM = new DateTime(2015, 10, 4, 19, 25, 0);
            pl5.InboundDTTM = new DateTime(2015, 10, 4, 22, 10, 0);

            PostflightLegCrewViewModel plc5 = new PostflightLegCrewViewModel(upVM);
            plc5.CrewID = 1;
            pl5.PostflightLegCrews.Add(plc5);

            PostflightLegCrewViewModel plc51 = new PostflightLegCrewViewModel(upVM);
            plc51.CrewID = 2;
            pl5.PostflightLegCrews.Add(plc51);


            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            TripLog.PostflightLegs.Add(pl3);
            TripLog.PostflightLegs.Add(pl4);
            TripLog.PostflightLegs.Add(pl5);
            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg3_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 3);
            decimal daysAwayLeg3_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 3);
            decimal daysAwayLeg4_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 4);
            decimal daysAwayLeg5_Crew1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 5);
            decimal daysAwayLeg5_Crew2 = pfcrewlite.DaysAwayCalculation(TripLog, 2, 5);

            Assert.AreEqual(2, daysAwayLeg1_Crew1);
            Assert.AreEqual(2, daysAwayLeg2_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew1);
            Assert.AreEqual(3, daysAwayLeg3_Crew2);

            Assert.AreEqual(2, daysAwayLeg4_Crew2);
            Assert.AreEqual(1, daysAwayLeg5_Crew1);
            Assert.AreEqual(0, daysAwayLeg5_Crew2);
        }
        #endregion

        #region DaysAway_Scenario_12_Four_Legs_Duty_Basis_Scheduled
        [TestMethod]
        public void DaysAway_Scenario_12_Four_Legs_Duty_Basis_Scheduled()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (int)DutyBasisForTest.Scheduled;

            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 5, 2, 0, 0);
            pl1.OutboundDTTM = new DateTime(2015, 10, 5, 2, 5, 0);
            pl1.InboundDTTM = new DateTime(2015, 10, 7, 12, 10, 0);

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 7, 13, 33, 0);
            pl2.OutboundDTTM = new DateTime(2015, 10, 7, 13, 38, 0);
            pl2.InboundDTTM = new DateTime(2015, 10, 7, 20, 40, 0);

            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);


            PostflightLegViewModel pl3 = new PostflightLegViewModel();
            pl3.LegNUM = 3;
            pl3.ScheduledTM = new DateTime(2015, 10, 7, 21, 03, 0);
            pl3.OutboundDTTM = new DateTime(2015, 10, 7, 21, 10, 0);
            pl3.InboundDTTM = new DateTime(2015, 10, 8, 2, 45, 0);

            PostflightLegCrewViewModel plc3 = new PostflightLegCrewViewModel(upVM);
            plc3.CrewID = 1;
            pl3.PostflightLegCrews.Add(plc3);


            PostflightLegViewModel pl4 = new PostflightLegViewModel();
            pl4.LegNUM = 4;
            pl4.ScheduledTM = new DateTime(2015, 10, 8, 3, 3, 0);
            pl4.OutboundDTTM = new DateTime(2015, 10, 8, 3, 10, 0);
            pl4.InboundDTTM = new DateTime(2015, 10, 8, 12, 20, 0);

            PostflightLegCrewViewModel plc4 = new PostflightLegCrewViewModel(upVM);
            plc4.CrewID = 1;
            pl4.PostflightLegCrews.Add(plc4);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            TripLog.PostflightLegs.Add(pl3);
            TripLog.PostflightLegs.Add(pl4);

            #endregion Data Setups

            PostflightCrewLite pfcrewlite = new PostflightCrewLite(upVM);
            decimal daysAwayLeg1 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 1);
            decimal daysAwayLeg2 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 2);
            decimal daysAwayLeg3 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 3);
            decimal daysAwayLeg4 = pfcrewlite.DaysAwayCalculation(TripLog, 1, 4);

            Assert.AreEqual(3, daysAwayLeg1);
            Assert.AreEqual(0, daysAwayLeg2);
            Assert.AreEqual(1, daysAwayLeg3);
            Assert.AreEqual(0, daysAwayLeg4);
        }
        #endregion DaysAway_Scenario_12_Four_Legs_Duty_Basis_Scheduled

        #region RemainOverNight Test Cases

        #region RemainOverNight_Scenerio_1_TripLog_IsNULL
        [TestMethod]
        public void RemainOverNight_Scenerio_1_TripLog_IsNULL()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = null;

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RON = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);

            Assert.AreEqual(0, RON);
        }
        #endregion RemainOverNight_Scenerio_1_TripLog_IsNULL

        #region RemainOverNight_Scenerio_2_Log_With_OneLeg_NoCrew_Assigned_to_Leg
        [TestMethod]
        public void RemainOverNight_Scenerio_2_Log_With_OneLeg_NoCrew_Assigned_to_Leg()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 02, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 02, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 04, 15, 00);
            pl1.PostflightLegCrews = null;

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RON = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);

            Assert.AreEqual(0, RON);
        }
        #endregion RemainOverNight_Scenerio_2_Log_With_OneLeg_NoCrew_Assigned_to_Leg

        #region RemainOverNight_Scenerio_3__OneLeg_OneCrew_Assigned_to_Leg
        [TestMethod]
        public void RemainOverNight_Scenerio_3__OneLeg_OneCrew_Assigned_to_Leg()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 02, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 02, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 04, 15, 00);
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RON = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);

            Assert.AreEqual(0, RON);
        }
        #endregion RemainOverNight_Scenerio_3__OneLeg_OneCrew_Assigned_to_Leg

        #region RemainOverNight_Scenerio_4_OneLeg_With_IsDutyEnd_True_Crew_Assigned_to_FirstLeg
        [TestMethod]
        public void RemainOverNight_Scenerio_4_OneLeg_With_IsDutyEnd_True_Crew_Assigned_to_FirstLeg()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 02, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 02, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 04, 15, 00);
            pl1.IsDutyEnd = true;

            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);
            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RON = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);

            Assert.AreEqual(0, RON);
        }
        #endregion RemainOverNight_Scenerio_4_OneLeg_With_IsDutyEnd_True_Crew_Assigned_to_FirstLeg

        #region RemainOverNight_Scenerio_5_Two_Legs_With_IsDutyEnd_False_On_Both_Legs_Same_Crew_to_Both_legs
        [TestMethod]
        public void RemainOverNight_Scenerio_5_Two_Legs_With_IsDutyEnd_False_On_Both_Legs_Same_Crew_to_Both_legs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 02, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 02, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 04, 15, 00);
            pl1.IsDutyEnd = false;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 12, 21, 00, 00);
            pl2.OutboundDTTM = new DateTime(2015, 10, 12, 21, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 12, 23, 15, 00);
            pl2.IsDutyEnd = false;
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);
            Assert.AreEqual(0, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_5_Two_Legs_With_IsDutyEnd_False_On_Both_Legs_Same_Crew_to_Both_legs

        #region RemainOverNight_Scenerio_6_Two_Legs_Consecutive_Day_IsDutyEnd_true_for_FirstLeg_SameCrew_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_6_Two_Legs_Consecutive_Day_IsDutyEnd_true_for_FirstLeg_SameCrew_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 17, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 23, 55, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 13, 00, 05, 00);
            pl2.OutboundDTTM = new DateTime(2015, 10, 13, 00, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 13, 03, 20, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);
            Assert.AreEqual(1, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_6_Two_Legs_Consecutive_Day_IsDutyEnd_true_for_FirstLeg_SameCrew_Bothlegs

        #region RemainOverNight_Scenerio_7_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day2_IsDutyEnd_True_For_FirstLeg_SameCrew_On_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_7_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day2_IsDutyEnd_True_For_FirstLeg_SameCrew_On_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 11, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 10, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 13, 06, 05, 00);
            pl2.OutboundDTTM = new DateTime(2015, 10, 13, 06, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 13, 09, 00, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);
            Assert.AreEqual(1, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_7_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day2_IsDutyEnd_True_For_FirstLeg_SameCrew_On_Bothlegs

        #region RemainOverNight_Scenerio_8_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_SameCrew_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_8_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_SameCrew_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 11, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 10, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 14, 02, 05, 00);
            pl2.OutboundDTTM = new DateTime(2015, 10, 14, 02, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 14, 07, 20, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);
            Assert.AreEqual(2, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_8_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_SameCrew_Bothlegs

        #region RemainOverNight_Scenerio_9_Two_Legs_Leg1_StartsOn_day1_leg2_ScheduleTM_OnDay2_Leg2_OutDTTM_OnDay3_IsDutyEnd_true_for_FirstLeg_DutyBasis_Set_To_Blockout_SameCrew_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_9_Two_Legs_Leg1_StartsOn_day1_leg2_ScheduleTM_OnDay2_Leg2_OutDTTM_OnDay3_IsDutyEnd_true_for_FirstLeg_DutyBasis_Set_To_Blockout_SameCrew_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (decimal)DutyBasisForTest.BlockOut;
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 17, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 23, 55, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 13, 00, 05, 00);
            pl2.OutboundDTTM = new DateTime(2015, 10, 14, 00, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 14, 03, 20, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);
            Assert.AreEqual(2, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_9_Two_Legs_Leg1_StartsOn_day1_leg2_ScheduleTM_OnDay2_Leg2_OutDTTM_OnDay3_IsDutyEnd_true_for_FirstLeg_DutyBasis_Set_To_Blockout_SameCrew_Bothlegs

        #region RemainOverNight_Scenerio_10_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day2_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_Scheduled_SameCrew_On_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_10_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day2_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_Scheduled_SameCrew_On_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (decimal)DutyBasisForTest.Scheduled;
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 11, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 10, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 13, 06, 05, 00);
            pl2.OutboundDTTM = new DateTime(2015, 10, 13, 06, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 13, 09, 00, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);
            Assert.AreEqual(1, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_10_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day2_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_Scheduled_SameCrew_On_Bothlegs

        #region RemainOverNight_Scenerio_11_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_Scheduled_ScheduledTM_null_OnSecondLeg_SameCrew_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_11_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_Scheduled_ScheduledTM_null_OnSecondLeg_SameCrew_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = 2;
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 11, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 10, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = null;
            pl2.OutboundDTTM = new DateTime(2015, 10, 14, 02, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 14, 07, 20, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);

            Assert.AreEqual(0, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_11_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_Scheduled_ScheduledTM_null_OnSecondLeg_SameCrew_Bothlegs

        #region RemainOverNight_Scenerio_12_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_BlockOut_OutBoundDTTM_null_OnSecondLeg_SameCrew_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_12_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_BlockOut_OutBoundDTTM_null_OnSecondLeg_SameCrew_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            upVM._DutyBasis = (decimal)DutyBasisForTest.BlockOut;
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 11, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 10, 15, 00);
            pl1.InboundDTTM = new DateTime(2015, 10, 12, 17, 15, 00);
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 14, 02, 05, 00);
            pl2.OutboundDTTM = null;
            pl2.InboundDTTM = new DateTime(2015, 10, 14, 07, 20, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);

            Assert.AreEqual(0, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_12_Two_Legs__Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_DutyBasis_Set_To_BlockOut_OutBoundDTTM_null_OnSecondLeg_SameCrew_Bothlegs

        #region RemainOverNight_Scenerio_13_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_InBoundDTTM_null_OnFirstLeg_SameCrew_Bothlegs
        [TestMethod]
        public void RemainOverNight_Scenerio_13_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_InBoundDTTM_null_OnFirstLeg_SameCrew_Bothlegs()
        {
            UserPrincipalViewModel upVM = new UserPrincipalViewModel();
            PostflightLogViewModel TripLog = new PostflightLogViewModel();

            #region Data Setups
            PostflightLegViewModel pl1 = new PostflightLegViewModel();
            pl1.LegNUM = 1;
            pl1.ScheduledTM = new DateTime(2015, 10, 12, 11, 00, 00);
            pl1.OutboundDTTM = new DateTime(2015, 10, 12, 10, 15, 00);
            pl1.InboundDTTM = null;
            pl1.IsDutyEnd = true;
            PostflightLegCrewViewModel plc1 = new PostflightLegCrewViewModel(upVM);
            plc1.CrewID = 1;
            pl1.PostflightLegCrews.Add(plc1);

            PostflightLegViewModel pl2 = new PostflightLegViewModel();
            pl2.LegNUM = 2;
            pl2.ScheduledTM = new DateTime(2015, 10, 14, 02, 05, 00); ;
            pl2.OutboundDTTM = new DateTime(2015, 10, 14, 02, 15, 00);
            pl2.InboundDTTM = new DateTime(2015, 10, 14, 07, 20, 00);
            PostflightLegCrewViewModel plc2 = new PostflightLegCrewViewModel(upVM);
            plc2.CrewID = 1;
            pl2.PostflightLegCrews.Add(plc2);

            if (TripLog.PostflightLegs != null)
                TripLog.PostflightLegs.Clear();

            TripLog.PostflightLegs.Add(pl1);
            TripLog.PostflightLegs.Add(pl2);
            #endregion Data Setups

            PostflightCrewLite pfCrewLite = new PostflightCrewLite(upVM);
            decimal RONLeg1 = pfCrewLite.RONCalculation(1, 1, upVM, TripLog);
            decimal RONLeg2 = pfCrewLite.RONCalculation(1, 2, upVM, TripLog);

            Assert.AreEqual(0, RONLeg1);
            Assert.AreEqual(0, RONLeg2);
        }
        #endregion RemainOverNight_Scenerio_13_Two_Legs_Leg1_Starts_On_Day1_and_Leg2_Starts_On_Day3_IsDutyEnd_True_For_FirstLeg_InBoundDTTM_null_OnFirstLeg_SameCrew_Bothlegs

        #endregion
    }
}
