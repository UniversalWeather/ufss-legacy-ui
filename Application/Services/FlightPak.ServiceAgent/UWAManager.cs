﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using FlightPak.Business.Common;
using FlightPak.Data.Preflight;
using System.Data;
using FlightPak.Framework.Encryption;
using UWA.Adapters;
using UWA.Common.Shared;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

using System.Net;
using System.Xml.Linq;
using UWA.Entities.Fuel;

namespace FlightPak.ServiceAgents
{
    public partial class UWAManager
    {
        private ExceptionManager exManager;
        PreflightDataModelContainer containerPF = null;
        FlightPak.Data.MasterCatalog.MasterDataContainer container = null;
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        #region  ATS

        #region TSS Submit

        public ReturnValue<List<string>> TSSSubmit(PreflightMain Trip, string RequesterAppname, string UserName)
        {
            ReturnValue<List<string>> objMain = new ReturnValue<List<string>>();
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<List<string>>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        #region Mapping Entities to UWA Service Entities

                        #region Trip Level Entity Mapping

                        Data.MasterCatalog.MasterDataContainer container = null;
                        container = new Data.MasterCatalog.MasterDataContainer();
                        var UWATrip = new UWA.Entities.ATS.Trip(RequesterAppname);

                        // /*Hard Coded for testing Purpose. */

                        //UWATrip.CustomerNumber = "32590";
                        //UWATrip.UserName = "rveldurthi";
                        //UWATrip.Registry = "N110UV";
                        //UWATrip.SchedulingSoftware.UserName = "rveldurthi";
                        //UWATrip.SchedulingSoftware.Email = "SOMEONE@SOMEONE.COM";
                        //UWATrip.RequestOrigin = RequesterAppname;                    

                        Data.Preflight.PreflightDataModelContainer containerpf = null;
                        containerpf = new Data.Preflight.PreflightDataModelContainer();
                        FlightPak.Data.Preflight.GetInterfaceCredentials ICredentials = new Data.Preflight.GetInterfaceCredentials();
                        ICredentials = containerpf.GetInterfaceCredentials(Trip.CustomerID, UserName.Trim()).SingleOrDefault();
                        if (ICredentials != null)
                        {
                            //UWATrip.CustomerNumber = Trip.CustomerID.ToString();
                            //UWATrip.UserName = (ICredentials.CompanyUserID == null ? string.Empty : ICredentials.CompanyUserID.Trim());
                            UWATrip.CustomerNumber = ICredentials.CustomerNumber == null ? string.Empty : ICredentials.CustomerNumber.Trim();
                            UWATrip.UserName = (ICredentials.UserName == null ? string.Empty : ICredentials.UserName.Trim());
                            UWATrip.Registry = Trip.Fleet.TailNum == null ? string.Empty : Trip.Fleet.TailNum;
                            UWATrip.SchedulingSoftware.Email = ICredentials.CompanyUserEmail == null ? string.Empty : ICredentials.CompanyUserEmail.Trim();
                            //UWATrip.SchedulingSoftware.UserName = (ICredentials.CompanyUserID == null ? string.Empty : ICredentials.CompanyUserID.Trim());
                            UWATrip.SchedulingSoftware.UserName = (ICredentials.UserName == null ? string.Empty : ICredentials.UserName.Trim());
                            UWATrip.RequestOrigin = RequesterAppname;

                        }

                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                        {
                            if (Trip.PreflightLegs.First().UWALs != null && Trip.PreflightLegs.First().UWALs.Count > 0)
                            {

                                if (Trip.PreflightLegs.First().UWALs.First().NotamTYPE != null && Trip.PreflightLegs.First().UWALs.First().NotamTYPE == 0)
                                    UWATrip.NOTAMSType = UWA.Entities.ATS.NOTAMSTypeEnum.None;
                                else if (Trip.PreflightLegs.First().UWALs.First().NotamTYPE != null && Trip.PreflightLegs.First().UWALs.First().NotamTYPE == 1)
                                    UWATrip.NOTAMSType = UWA.Entities.ATS.NOTAMSTypeEnum.Plain;
                                else if (Trip.PreflightLegs.First().UWALs.First().NotamTYPE != null && Trip.PreflightLegs.First().UWALs.First().NotamTYPE == 2)
                                    UWATrip.NOTAMSType = UWA.Entities.ATS.NOTAMSTypeEnum.Standard;

                                if (Trip.PreflightLegs.First().UWALs.First().NotamLegSettings != null && Trip.PreflightLegs.First().UWALs.First().NotamLegSettings == 0)
                                    UWATrip.SendNOTAMS = UWA.Entities.ATS.SendNOTAMSEnum.None;
                                else if (Trip.PreflightLegs.First().UWALs.First().NotamLegSettings != null && Trip.PreflightLegs.First().UWALs.First().NotamLegSettings == 1)
                                    UWATrip.SendNOTAMS = UWA.Entities.ATS.SendNOTAMSEnum.DOM;
                                else if (Trip.PreflightLegs.First().UWALs.First().NotamLegSettings != null && Trip.PreflightLegs.First().UWALs.First().NotamLegSettings == 2)
                                    UWATrip.SendNOTAMS = UWA.Entities.ATS.SendNOTAMSEnum.International;
                                else if (Trip.PreflightLegs.First().UWALs.First().NotamLegSettings != null && Trip.PreflightLegs.First().UWALs.First().NotamLegSettings == 3)
                                    UWATrip.SendNOTAMS = UWA.Entities.ATS.SendNOTAMSEnum.All;

                                if (Trip.PreflightLegs.First().UWALs.First().FlightLegSettings == 0)
                                    UWATrip.FlightFollow.LegSettings = UWA.Entities.ATS.SendNOTAMSEnum.None;
                                else if (Trip.PreflightLegs.First().UWALs.First().FlightLegSettings == 1)
                                    UWATrip.FlightFollow.LegSettings = UWA.Entities.ATS.SendNOTAMSEnum.DOM;
                                else if (Trip.PreflightLegs.First().UWALs.First().FlightLegSettings == 2)
                                    UWATrip.FlightFollow.LegSettings = UWA.Entities.ATS.SendNOTAMSEnum.International;
                                else if (Trip.PreflightLegs.First().UWALs.First().FlightLegSettings == 3)
                                    UWATrip.FlightFollow.LegSettings = UWA.Entities.ATS.SendNOTAMSEnum.All;

                                if (Trip.PreflightLegs.First().UWALs.First().IsFax != null && Trip.PreflightLegs.First().UWALs.First().IsFax == true)
                                {
                                    UWATrip.FlightFollow.ByFax = (bool)(Trip.PreflightLegs.First().UWALs.First().IsFax == null ? false : Trip.PreflightLegs.First().UWALs.First().IsFax);
                                    UWATrip.FlightFollow.FaxNumber = (Trip.PreflightLegs.First().UWALs.First().FaxDeliveryAddress == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().FaxDeliveryAddress);
                                    UWATrip.FlightFollow.FaxRecipient = (Trip.PreflightLegs.First().UWALs.First().FaxAttention == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().FaxAttention);
                                }
                                if (Trip.PreflightLegs.First().UWALs.First().IsEmail != null && Trip.PreflightLegs.First().UWALs.First().IsEmail == true)
                                {
                                    UWATrip.FlightFollow.ByEmail = (bool)(Trip.PreflightLegs.First().UWALs.First().IsEmail == null ? false : Trip.PreflightLegs.First().UWALs.First().IsEmail);
                                    UWATrip.FlightFollow.EmailAddress = (Trip.PreflightLegs.First().UWALs.First().EmailAddress == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().EmailAddress);
                                    UWATrip.FlightFollow.EmailRecipient = (Trip.PreflightLegs.First().UWALs.First().EmailAttention == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().EmailAttention);
                                }
                                if (Trip.PreflightLegs.First().UWALs.First().IsPhone != null && Trip.PreflightLegs.First().UWALs.First().IsPhone == true)
                                {
                                    UWATrip.FlightFollow.ByPhone = (bool)(Trip.PreflightLegs.First().UWALs.First().IsPhone == null ? false : Trip.PreflightLegs.First().UWALs.First().IsPhone);
                                    UWATrip.FlightFollow.PhoneNumber = (Trip.PreflightLegs.First().UWALs.First().PhoneDeliveryAddress == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().PhoneDeliveryAddress);
                                    UWATrip.FlightFollow.PhoneRecipient = (Trip.PreflightLegs.First().UWALs.First().PhoneAttention == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().PhoneAttention);
                                }
                                if (Trip.PreflightLegs.First().UWALs.First().IsSITAAIRINC != null && Trip.PreflightLegs.First().UWALs.First().IsSITAAIRINC == true)
                                {
                                    UWATrip.FlightFollow.BySITA = (bool)(Trip.PreflightLegs.First().UWALs.First().IsSITAAIRINC == null ? false : Trip.PreflightLegs.First().UWALs.First().IsSITAAIRINC);
                                    UWATrip.FlightFollow.SITANumber = (Trip.PreflightLegs.First().UWALs.First().SITAAIRINCDeliveryAddress == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().SITAAIRINCDeliveryAddress);
                                    UWATrip.FlightFollow.SITARecipient = (Trip.PreflightLegs.First().UWALs.First().SITAAIRINCDeliveryAttention == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().SITAAIRINCDeliveryAttention);
                                }

                                var preTrip1 = UWATrip.AddPreTripDetails(Trip.PreflightLegs.First().UWALs.First().Pretrip1Location);
                                preTrip1.Address = Trip.PreflightLegs.First().UWALs.First().PretripDelivery1Address;
                                preTrip1.Method = Trip.PreflightLegs.First().UWALs.First().Pretrip1DeliveryMethod;
                                preTrip1.Recipient = Trip.PreflightLegs.First().UWALs.First().Pretrip1Attention;

                                var preTrip2 = UWATrip.AddPreTripDetails(Trip.PreflightLegs.First().UWALs.First().Pretrip2Location);
                                preTrip2.Address = Trip.PreflightLegs.First().UWALs.First().PretripDelivery2Address;
                                preTrip2.Method = Trip.PreflightLegs.First().UWALs.First().Pretrip2DeliveryMethod;
                                preTrip2.Recipient = Trip.PreflightLegs.First().UWALs.First().Pretrip2Attention;

                                var preTrip3 = UWATrip.AddPreTripDetails(Trip.PreflightLegs.First().UWALs.First().Pretrip3Location);
                                preTrip3.Address = Trip.PreflightLegs.First().UWALs.First().PretripDelivery3Address;
                                preTrip3.Method = Trip.PreflightLegs.First().UWALs.First().Pretrip3DeliveryMethod;
                                preTrip3.Recipient = Trip.PreflightLegs.First().UWALs.First().Pretrip3Attention;

                                var preTrip4 = UWATrip.AddPreTripDetails(Trip.PreflightLegs.First().UWALs.First().Pretrip4Location);
                                preTrip4.Address = Trip.PreflightLegs.First().UWALs.First().PretripDelivery4Address;
                                preTrip4.Method = Trip.PreflightLegs.First().UWALs.First().Pretrip4DeliveryMethod;
                                preTrip4.Recipient = Trip.PreflightLegs.First().UWALs.First().Pretrip4Attention;

                                UWATrip.Requester = Convert.ToString(Trip.PreflightLegs.First().UWALs.First().TripRequestBy);

                                /* We are handling the services in the Leg Level. But here the Mapping is Under Trip Level. Need to Check with Business*/

                                if (Trip.PreflightLegs.First().UWALs.First().FARAllLegs != null && Trip.PreflightLegs.First().UWALs.First().FARAllLegs == 1)
                                    UWATrip.FARPart = UWA.Entities.ATS.FARPartTypeEnum.FP91;
                                else if (Trip.PreflightLegs.First().UWALs.First().FARAllLegs != null && Trip.PreflightLegs.First().UWALs.First().FARAllLegs == 2)
                                    UWATrip.FARPart = UWA.Entities.ATS.FARPartTypeEnum.FP121;
                                else if (Trip.PreflightLegs.First().UWALs.First().FARAllLegs != null && Trip.PreflightLegs.First().UWALs.First().FARAllLegs == 5)
                                    UWATrip.FARPart = UWA.Entities.ATS.FARPartTypeEnum.FP129;
                                else if (Trip.PreflightLegs.First().UWALs.First().FARAllLegs != null && Trip.PreflightLegs.First().UWALs.First().FARAllLegs == 4)
                                    UWATrip.FARPart = UWA.Entities.ATS.FARPartTypeEnum.FP135;
                                else if (Trip.PreflightLegs.First().UWALs.First().FARAllLegs != null && Trip.PreflightLegs.First().UWALs.First().FARAllLegs == 3)
                                    UWATrip.FARPart = UWA.Entities.ATS.FARPartTypeEnum.FP125;
                                UWATrip.SpecialInstructions = (Trip.PreflightLegs.First().UWALs.First().SpecialInstructions == null ? string.Empty : Trip.PreflightLegs.First().UWALs.First().SpecialInstructions);

                                // UWATrip.History = ""; //check
                            }
                        }
                        if (Trip.UWATSSMains != null && Trip.UWATSSMains.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Trip.UWATSSMains.First().UWATripID))
                                UWATrip.TripNumber = (ulong)(Convert.ToInt64(Trip.UWATSSMains.First().UWATripID)); //Check
                        }
                        #endregion

                        #region Leg Level Entity Mapping
                        foreach (PreflightLeg uwaPFLeg in Trip.PreflightLegs)
                        {

                            var leg = UWATrip.AddLeg((uwaPFLeg.Airport1.IcaoID == null ? string.Empty : uwaPFLeg.Airport1.IcaoID), (uwaPFLeg.Airport.IcaoID == null ? string.Empty : uwaPFLeg.Airport.IcaoID));

                            if (uwaPFLeg.DepartureGreenwichDTTM != null)
                                leg.DepartureDateUTC = (DateTime)(uwaPFLeg.DepartureGreenwichDTTM);
                            if (uwaPFLeg.ArrivalGreenwichDTTM != null)
                                leg.ArrivalDateUTC = (DateTime)(uwaPFLeg.ArrivalGreenwichDTTM);
                            if (uwaPFLeg.DepartureDTTMLocal != null)
                                leg.DepartureDateLocal = (DateTime)(uwaPFLeg.DepartureDTTMLocal);
                            if (uwaPFLeg.ArrivalDTTMLocal != null)
                                leg.ArrivalDateLocal = (DateTime)(uwaPFLeg.ArrivalDTTMLocal);

                            var crewcaptain = uwaPFLeg.PreflightCrewLists.Where(c => c.DutyTYPE == "P").FirstOrDefault();
                            if (crewcaptain != null)
                            {
                                string Crewname = Convert.ToString(crewcaptain.CrewLastName) + ", " + Convert.ToString(crewcaptain.CrewFirstName) + " " + Convert.ToString(crewcaptain.CrewMiddleName);
                                leg.Captain = Crewname;
                            }
                            var crewsecondcommand = uwaPFLeg.PreflightCrewLists.Where(c => c.DutyTYPE == "S").FirstOrDefault();
                            if (crewsecondcommand != null)
                            {
                                string Crewname = Convert.ToString(crewsecondcommand.CrewLastName) + " " + Convert.ToString(crewsecondcommand.CrewFirstName) + " " + Convert.ToString(crewsecondcommand.CrewMiddleName);
                                leg.SecondInCommand = Crewname;
                            }

                            #region UWA Services Page Data
                            if (uwaPFLeg.UWALs.Count > 0)
                            {
                                leg.IsUWAHandling = (bool)(uwaPFLeg.UWALs.First().IsHandlingAllLegs == null ? false : uwaPFLeg.UWALs.First().IsHandlingAllLegs);
                                leg.IsHomeBaseFlightFollowing = (bool)(uwaPFLeg.UWALs.First().IsHomebaseFlightFollowing == null ? false : uwaPFLeg.UWALs.First().IsHomebaseFlightFollowing);
                                leg.IsUWACatering = (bool)(uwaPFLeg.UWALs.First().IsCateringAllLegs == null ? false : uwaPFLeg.UWALs.First().IsCateringAllLegs);
                                leg.CateringOrder = (uwaPFLeg.UWALs.First().CateringDetail == null ? string.Empty : uwaPFLeg.UWALs.First().CateringDetail);
                                leg.IsPlaneWideBody = (bool)(uwaPFLeg.UWALs.First().IsWideBodyHandling == null ? false : uwaPFLeg.UWALs.First().IsWideBodyHandling);
                                leg.NeedPassengerSecurity = (bool)(uwaPFLeg.UWALs.First().IsPassengerSecurityAllLegs == null ? false : uwaPFLeg.UWALs.First().IsPassengerSecurityAllLegs);
                                leg.NeedAircraftSecurity = (bool)(uwaPFLeg.UWALs.First().IsAircraftSecurityAllLegs == null ? false : uwaPFLeg.UWALs.First().IsAircraftSecurityAllLegs);
                                leg.NeedPassengerVisas = (bool)(uwaPFLeg.UWALs.First().IsPassengerVisaAllLegs == null ? false : uwaPFLeg.UWALs.First().IsPassengerVisaAllLegs);

                                if (uwaPFLeg.FuelLoad != null)
                                    leg.FlightPlan.FuelAmount = Convert.ToInt32(uwaPFLeg.FuelLoad);

                                if ((uwaPFLeg.UWALs.First().FuelOnArrivalDeparture) != null && uwaPFLeg.UWALs.First().FuelOnArrivalDeparture == 1)
                                    leg.FlightPlan.FuelTime = UWA.Entities.ATS.FlightPlanFuelTimeEnum.AtArrival;
                                else if ((uwaPFLeg.UWALs.First().FuelOnArrivalDeparture) != null && uwaPFLeg.UWALs.First().FuelOnArrivalDeparture == 2)
                                    leg.FlightPlan.FuelTime = UWA.Entities.ATS.FlightPlanFuelTimeEnum.AtDeparture;
                            }
                            #endregion

                            ////Crewvisa missing //check  


                            if (uwaPFLeg.UWALs.Count > 0)
                            {
                                leg.Fuel.IsRequested = (bool)(uwaPFLeg.UWALs.First().IsUVAirFuel == null ? false : uwaPFLeg.UWALs.First().IsUVAirFuel);
                                leg.Fuel.IsQuoteRequested = (bool)(uwaPFLeg.UWALs.First().IsUVAirFuelQuoteRequired == null ? false : uwaPFLeg.UWALs.First().IsUVAirFuelQuoteRequired);
                                leg.Fuel.Ispecial = (bool)(uwaPFLeg.UWALs.First().IsPristRequired == null ? false : uwaPFLeg.UWALs.First().IsPristRequired);
                                leg.Fuel.Instructions = (uwaPFLeg.UWALs.First().SpecialInstructions == null ? string.Empty : uwaPFLeg.UWALs.First().FuelSpecialInstruction);
                            }


                            leg.CrewCount = uwaPFLeg.PreflightCrewLists.Count;

                            #region Handling services
                            if (uwaPFLeg.UWASpServs.Count > 0)
                            {
                                foreach (UWASpServ uwaSp in uwaPFLeg.UWASpServs)
                                {
                                    if (uwaSp.LegID == uwaPFLeg.LegID)
                                    {
                                        var handling = leg.AddHandlingService();
                                        handling.Names = (uwaSp.UWASpServDescription == null ? string.Empty : uwaSp.UWASpServDescription);
                                        handling.Instructions = (uwaSp.UWASpServInstruction == null ? string.Empty : uwaSp.UWASpServInstruction);
                                    }
                                }

                            }

                            #endregion

                            #region permits
                            if (uwaPFLeg.UWALs.Count > 0)
                            {
                                if (uwaPFLeg.UWALs.First().IsLandingPermitsAllLegs != null && uwaPFLeg.UWALs.First().IsLandingPermitsAllLegs == true)
                                    leg.Permits.LandingArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Yes;
                                else
                                    leg.Permits.LandingArrangedBy = UWA.Entities.ATS.ArrangedByEnum.No;

                                leg.Permits.Number = (uwaPFLeg.UWALs.First().ClientArrangeLandingPermitNUM == null ? string.Empty : uwaPFLeg.UWALs.First().ClientArrangeLandingPermitNUM);

                                if (uwaPFLeg.UWALs.First().IsOverflightPermitAllLegs != null && uwaPFLeg.UWALs.First().IsOverflightPermitAllLegs == true)
                                    leg.Permits.OverFlightArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Yes;
                                else
                                    leg.Permits.OverFlightArrangedBy = UWA.Entities.ATS.ArrangedByEnum.No;

                                if (uwaPFLeg.UWAPermits.Count > 0)
                                {
                                    foreach (UWAPermit uwaPer in uwaPFLeg.UWAPermits)
                                    {
                                        var per = leg.Permits.AddOverflightPermit(uwaPer.Country);
                                        per.Date = (DateTime)uwaPer.ValidDate;
                                        per.Number = uwaPer.PermitNumber;
                                    }
                                }

                                if (uwaPFLeg.UWALs.First().AirportDepartureSlotsArrangement != null && uwaPFLeg.UWALs.First().AirportDepartureSlotsArrangement == 1)
                                    leg.Permits.AROSlotsDeparture = UWA.Entities.ATS.ArrangedByEnum.Universal;
                                else
                                    leg.Permits.AROSlotsDeparture = UWA.Entities.ATS.ArrangedByEnum.Client;

                                if (uwaPFLeg.UWALs.First().AirportArrivalSlotArrange != null && uwaPFLeg.UWALs.First().AirportArrivalSlotArrange == 1)
                                    leg.Permits.AROSlotsArrival = UWA.Entities.ATS.ArrangedByEnum.Universal;
                                else
                                    leg.Permits.AROSlotsArrival = UWA.Entities.ATS.ArrangedByEnum.Client;


                            }
                            #endregion

                            #region Crew List
                            if (uwaPFLeg.PreflightCrewLists.Count > 0)
                            {
                                foreach (PreflightCrewList uwaPFCrewLst in uwaPFLeg.PreflightCrewLists)
                                {

                                    leg.CrewHotel.NumberOfRooms = (byte)(uwaPFCrewLst.NoofRooms == null ? 0 : uwaPFCrewLst.NoofRooms);
                                    leg.CrewHotel.CardNumber = (uwaPFCrewLst.ClubCard == null ? string.Empty : uwaPFCrewLst.ClubCard);
                                    leg.CrewHotel.IsRateCapped = (bool)(uwaPFCrewLst.IsRateCap == null ? false : uwaPFCrewLst.IsRateCap);
                                    leg.CrewHotel.MaximumNightlyRate = (decimal)(uwaPFCrewLst.MaxCapAmount == null ? 0 : uwaPFCrewLst.MaxCapAmount);
                                    ////Crew
                                    if (uwaPFCrewLst.PreflightCrewHotelLists.Count > 0)
                                    {
                                        foreach (PreflightCrewHotelList uwaPFHotelLst in uwaPFCrewLst.PreflightCrewHotelLists)
                                        {
                                            if (string.IsNullOrEmpty(uwaPFHotelLst.EarlyCheckInOption))
                                                leg.CrewHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum.None);
                                            else
                                            {
                                                if (uwaPFHotelLst.EarlyCheckInOption == "R")
                                                    leg.CrewHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum.RequestOnly);
                                                else if (uwaPFHotelLst.EarlyCheckInOption == "B")
                                                    leg.CrewHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum.BookPriorNight);
                                            }

                                            if (string.IsNullOrEmpty(uwaPFHotelLst.HotelArrangedBy))
                                                leg.CrewHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.No;
                                            else
                                            {
                                                if (uwaPFHotelLst.HotelArrangedBy.ToUpper() == "NONE")
                                                    leg.CrewHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.No;
                                                else if (uwaPFHotelLst.HotelArrangedBy.ToUpper() == "UWA")
                                                    leg.CrewHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Universal;
                                                else if (uwaPFHotelLst.HotelArrangedBy.ToUpper() == "CLIENT")
                                                    leg.CrewHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Client;

                                            }

                                            var guest = leg.CrewHotel.AddGuest();
                                            guest.Name = Convert.ToString(uwaPFCrewLst.CrewLastName) + ", " + Convert.ToString(uwaPFCrewLst.CrewFirstName) + " " + Convert.ToString(uwaPFCrewLst.CrewMiddleName);
                                            if (uwaPFHotelLst.DateIn != null)
                                                guest.CheckInDate = (DateTime)(uwaPFHotelLst.DateIn);
                                            if (uwaPFHotelLst.DateOut != null)
                                                guest.CheckOutDate = (DateTime)(uwaPFHotelLst.DateOut);
                                            guest.HotelName = (uwaPFHotelLst.PreflightHotelName == null ? string.Empty : uwaPFHotelLst.PreflightHotelName);
                                            guest.BedCount = (short)(uwaPFHotelLst.NoofBeds == null ? 0 : (Convert.ToInt32(uwaPFHotelLst.NoofBeds) == 0 ? 1 : uwaPFHotelLst.NoofBeds));

                                            if (uwaPFHotelLst.RoomTypeID != null)
                                            {
                                                var CrewRoomType = container.RoomType.Where(r => r.RoomTypeID == uwaPFHotelLst.RoomTypeID).FirstOrDefault();
                                                if (CrewRoomType != null)
                                                {
                                                    if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeSingle) || (CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeSingleRoom))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Single;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDB) || (CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDOUBLE))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Double;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDOUBLEFORTWO))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.DoubleForTwo;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeAPARTMENT))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Apartment;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeBUNGALOW))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Bungalow;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeCABIN))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Cabin;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeSUITE))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Suite;
                                                    else if ((CrewRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeOTHER))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Other;
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(uwaPFHotelLst.RoomDescription))
                                            {
                                                if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescAMBASSADOR)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Ambassador;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescDELUXE)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Deluxe;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescEX)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Ex;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescEXECUTIVE)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Executive;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescFOURPOSTER)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.FourPoster;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescONEBEDROOM)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.OneRoom;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescOT)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.OT;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescOTHER)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Other;
                                            }

                                            guest.MaximumNightlyRate = (decimal)(uwaPFCrewLst.MaxCapAmount == null ? 0 : uwaPFCrewLst.MaxCapAmount);
                                            guest.RoomNumber = (uwaPFCrewLst.NoofRooms == null ? 0 : uwaPFCrewLst.NoofRooms).ToString();
                                            guest.CardNumber = (uwaPFCrewLst.ClubCard == null ? string.Empty : uwaPFCrewLst.ClubCard);

                                            guest.RequireEarlyCheckIn = (bool)(uwaPFHotelLst.IsEarlyCheckIn == null ? false : uwaPFHotelLst.IsEarlyCheckIn);
                                            guest.AllowSmoking = (bool)(uwaPFHotelLst.IsSmoking == null ? false : uwaPFHotelLst.IsSmoking);
                                            guest.Comments = (uwaPFHotelLst.SpecialInstructions == null ? string.Empty : uwaPFHotelLst.SpecialInstructions);

                                        }
                                    }
                                    else
                                        leg.CrewHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum)0;
                                }
                            }
                            else
                                leg.CrewHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum)0;

                            #endregion

                            #region Passenger List
                            //Passenger
                            if (uwaPFLeg.PreflightPassengerLists.Count > 0)
                            {
                                foreach (PreflightPassengerList uwaPFPaxLst in uwaPFLeg.PreflightPassengerLists)
                                {
                                    leg.PassengerHotel.NumberOfRooms = (byte)(uwaPFPaxLst.NoofRooms == null ? 0 : uwaPFPaxLst.NoofRooms);
                                    leg.PassengerHotel.CardNumber = (uwaPFPaxLst.ClubCard == null ? string.Empty : uwaPFPaxLst.ClubCard);
                                    leg.PassengerHotel.IsRateCapped = (bool)(uwaPFPaxLst.IsRateCap == null ? false : uwaPFPaxLst.IsRateCap);
                                    leg.PassengerHotel.MaximumNightlyRate = (int)(uwaPFPaxLst.MaxCapAmount == null ? 0 : uwaPFPaxLst.MaxCapAmount);

                                    if (uwaPFPaxLst.PreflightPassengerHotelLists.Count > 0)
                                    {
                                        foreach (PreflightPassengerHotelList uwaPFHotelLst in uwaPFPaxLst.PreflightPassengerHotelLists)
                                        {
                                            var guest = leg.PassengerHotel.AddGuest();
                                            if (string.IsNullOrEmpty(uwaPFHotelLst.EarlyCheckInOption))
                                                leg.PassengerHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum.None);
                                            else
                                            {
                                                if (uwaPFHotelLst.EarlyCheckInOption == "R")
                                                    leg.PassengerHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum.RequestOnly);
                                                else if (uwaPFHotelLst.EarlyCheckInOption == "B")
                                                    leg.PassengerHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum.BookPriorNight);
                                            }

                                            guest.Name = Convert.ToString(uwaPFPaxLst.PassengerLastName) + ", " + Convert.ToString(uwaPFPaxLst.PassengerFirstName) + " " + Convert.ToString(uwaPFPaxLst.PassengerMiddleName);
                                            if (uwaPFHotelLst.DateIn != null)
                                                guest.CheckInDate = (DateTime)(uwaPFHotelLst.DateIn);
                                            if (uwaPFHotelLst.DateOut != null)
                                                guest.CheckOutDate = (DateTime)(uwaPFHotelLst.DateOut);
                                            guest.HotelName = (uwaPFHotelLst.PreflightHotelName == null ? string.Empty : uwaPFHotelLst.PreflightHotelName);
                                            guest.BedCount = (short)(uwaPFHotelLst.NoofBeds == null ? 0 : (uwaPFHotelLst.NoofBeds == 0 ? 1 : uwaPFHotelLst.NoofBeds));

                                            if (uwaPFHotelLst.RoomTypeID != null)
                                            {
                                                var PaxRoomType = container.RoomType.Where(r => r.RoomTypeID == uwaPFHotelLst.RoomTypeID).FirstOrDefault();
                                                if (PaxRoomType != null)
                                                {
                                                    if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeSingle) || (PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeSingleRoom))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Single;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDB) || (PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDOUBLE))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Double;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDOUBLEFORTWO))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.DoubleForTwo;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeAPARTMENT))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Apartment;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeBUNGALOW))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Bungalow;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeCABIN))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Cabin;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeSUITE))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Suite;
                                                    else if ((PaxRoomType.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeOTHER))
                                                        guest.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Other;
                                                }
                                            }

                                            guest.RoomNumber = (uwaPFPaxLst.NoofRooms == null ? 0 : uwaPFPaxLst.NoofRooms).ToString();
                                            guest.CardNumber = (uwaPFPaxLst.ClubCard == null ? string.Empty : uwaPFPaxLst.ClubCard);
                                            guest.MaximumNightlyRate = (int)(uwaPFPaxLst.MaxCapAmount == null ? 0 : uwaPFPaxLst.MaxCapAmount);

                                            if (!string.IsNullOrEmpty(uwaPFHotelLst.RoomDescription))
                                            {
                                                if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescAMBASSADOR)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Ambassador;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescDELUXE)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Deluxe;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescEX)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Ex;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescEXECUTIVE)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Executive;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescFOURPOSTER)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.FourPoster;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescONEBEDROOM)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.OneRoom;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescOT)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.OT;
                                                else if (uwaPFHotelLst.RoomDescription.ToUpper() == FlightPak.Common.ModuleNameConstants.Preflight.PreflightATSRoomTypeDescOTHER)
                                                    guest.RoomDescription = UWA.Entities.ATS.HotelRoomDescriptionEnum.Other;
                                            }

                                            guest.RequireEarlyCheckIn = (bool)(uwaPFHotelLst.IsEarlyCheckIn == null ? false : uwaPFHotelLst.IsEarlyCheckIn);
                                            guest.AllowSmoking = (bool)(uwaPFHotelLst.IsSmoking == null ? false : uwaPFHotelLst.IsSmoking);
                                            guest.Comments = (uwaPFHotelLst.SpecialInstructions == null ? string.Empty : uwaPFHotelLst.SpecialInstructions);

                                            if (string.IsNullOrEmpty(uwaPFHotelLst.HotelArrangedBy))
                                                leg.PassengerHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.No;
                                            else
                                            {
                                                if (uwaPFHotelLst.HotelArrangedBy.ToUpper() == "NONE")
                                                    leg.PassengerHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.No;
                                                else if (uwaPFHotelLst.HotelArrangedBy.ToUpper() == "UWA")
                                                    leg.PassengerHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Universal;
                                                else if (uwaPFHotelLst.HotelArrangedBy.ToUpper() == "CLIENT")
                                                    leg.PassengerHotel.HotelsArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Client;

                                            }

                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion

                        #endregion

                        #region Hard Codeded Values for Testing
                        //var trip = new UWA.Entities.ATS.Trip("uwafpktest");
                        //trip.CustomerNumber = "32590";
                        //trip.UserName = "rveldurthi";
                        //trip.Registry = "N110UV";
                        //trip.SchedulingSoftware.Email = "copara@univ-wea.com";
                        //trip.NOTAMSType = UWA.Entities.ATS.NOTAMSTypeEnum.Plain;
                        //trip.SendNOTAMS = UWA.Entities.ATS.SendNOTAMSEnum.All;

                        //trip.FlightFollow.ByEmail = true;
                        //trip.FlightFollow.EmailAddress = "jmiller@gmail.com";
                        //trip.FlightFollow.EmailRecipient = "John Miller";

                        //var preTrip11 = trip.AddPreTripDetails("Kokomo");
                        //preTrip11.Address = "8787 Tallyho Road, Houston, TX";
                        //preTrip11.Method = "Fax";
                        //preTrip11.Recipient = "Beach Boys";

                        //var preTrip12 = trip.AddPreTripDetails("Aruba");
                        //preTrip12.Address = "111 Holiday Road, Jamaica";
                        //preTrip12.Method = "Email";
                        //preTrip12.Recipient = "Lindsey Buckingham";

                        //#region First leg
                        //var leg1 = trip.AddLeg("KIAH", "KPHX");
                        //leg1.DepartureDateUTC = DateTime.Now.AddDays(5);
                        //leg1.ArrivalDateUTC = DateTime.Now.AddDays(6);
                        //leg1.DepartureDateLocal = leg1.DepartureDateUTC.ToLocalTime();
                        //leg1.ArrivalDateLocal = leg1.ArrivalDateUTC.ToLocalTime();

                        //leg1.Captain = "Kirk, James Tiberius";

                        //// Weather
                        //leg1.Weather.AlternateAirportICAO = "KDEV";
                        //leg1.Weather.IsStandardBrief = true;
                        //leg1.Weather.StandardDeliveryDate = DateTime.Now.AddDays(3);
                        //leg1.Weather.MaximumFlightlevel = "2900";

                        //leg1.NeedAircraftSecurity = true;
                        //leg1.FlightPlan.FuelAmount = 5;
                        //leg1.FlightPlan.FuelTime = UWA.Entities.ATS.FlightPlanFuelTimeEnum.AtArrival;

                        //leg1.CrewCount = 3;

                        //leg1.Permits.LandingArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Universal;
                        //leg1.Permits.OverFlightArrangedBy = UWA.Entities.ATS.ArrangedByEnum.Client;
                        //var ovrFPermit = leg1.Permits.AddOverflightPermit("Russia");
                        //ovrFPermit.Date = DateTime.Now.AddDays(60);
                        //ovrFPermit.Number = "ABC65676";

                        //leg1.CrewHotel.Request = (UWA.Entities.ATS.TravelerHotelRequestEnum)0;

                        //#region Handling Service for Leg1
                        //var service1 = leg1.AddHandlingService();
                        //service1.Instructions = "Feed Mr. Tubbles";
                        //service1.Names = "Pet care";

                        //var service2 = leg1.AddHandlingService();
                        //service2.Instructions = "Fluff my pillow";
                        //service2.Names = "Custom Comfort";
                        //#endregion

                        //#region Hotel guest - Captain
                        //var guest1 = leg1.CrewHotel.AddGuest();
                        //guest1.Name = "Kirk, James T.";
                        //guest1.CheckInDate = DateTime.Now.AddDays(29);
                        //guest1.CheckOutDate = DateTime.Now.AddDays(32);
                        //guest1.HotelName = "Motel 6";
                        //guest1.BedCount = 1;
                        //guest1.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Cabin;
                        //guest1.MaximumNightlyRate = new decimal(49.99);
                        //#endregion

                        //#region Hotel guest - passenger 1
                        //var guest2 = leg1.PassengerHotel.AddGuest();
                        //guest2.Name = "Phoenix, Joaquin";
                        //guest2.CheckInDate = DateTime.Now.AddDays(30);
                        //guest2.CheckOutDate = DateTime.Now.AddDays(32);
                        //guest2.HotelName = "Days Inn";
                        //guest2.BedCount = 1;
                        //guest2.RoomType = UWA.Entities.ATS.HotelRoomTypeEnum.Bungalow;

                        //#endregion
                        //#endregion

                        #endregion

                        #region Write to XML to check the Data
                        //To check the data is populated correctly.

                        //string atsXml = UWATrip.ToString();
                        ////string atsXml = trip.ToString();
                        //var doc = new System.Xml.XmlDocument();
                        //doc.LoadXml(atsXml);
                        //var atsWriter = new System.Xml.XmlTextWriter(@"c:\temp\ssw_submission_Real.xml", null);
                        //atsWriter.Formatting = System.Xml.Formatting.Indented;
                        //doc.Save(atsWriter);
                        #endregion

                        #region Submitting to UWA Services

                        List<string> lst = new List<string>();
                        var atsCredentials = new UWA.Adapters.UserCredentials();
                        var atsClient = new ATS(UWATrip);

                        var result = atsClient.SubmitTrip();
                        if (!result.HasError)
                        {
                            if (result.Data != 0)
                            {
                                lst.Add(result.Data.ToString());
                                lst.Add(result.DataTimeStamp.ToString());
                                objMain.EntityInfo = lst;
                                objMain.ReturnFlag = true;
                            }
                        }
                        else
                        {
                            objMain.ErrorMessage = result.ErrorMessage;
                            objMain.ReturnFlag = false;
                        }

                        //Used for Dummy Return for complete flow.
                        //lst.Add("123456");
                        //objMain.EntityInfo = lst;
                        //objMain.ReturnFlag = true;


                        #endregion
                    }, FlightPak.Common.Constants.Policy.UWAServiceLayer);

                }


                return objMain;
            }, FlightPak.Common.Constants.Policy.DataLayer);

        }

        #endregion

        #region TSS Check Status
        public ReturnValue<List<string>> TSSCheckStatus(string UWATripID, string RequesterAppname)
        {
            ReturnValue<List<string>> objMain = null;
            List<string> lst = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<List<string>>>(() =>
            {

                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UWATripID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        objMain = new ReturnValue<List<string>>();
                        lst = new List<string>();
                        //var trip = new UWA.Entities.ATS.Trip("FlightPak2");
                        var trip = new UWA.Entities.ATS.Trip(RequesterAppname);
                        var atsCredentials = new UWA.Adapters.UserCredentials();

                        Int64 UWAtrip = 0;
                        if (Int64.TryParse(UWATripID, out UWAtrip))
                        {
                            //Code to handle if its not valid uwatrip 
                        }

                        var atsClient = new ATS(UWAtrip);
                        var tripStatusResult = atsClient.GetTripStatus();
                        if (tripStatusResult.HasError)
                        {
                            objMain.ErrorMessage = tripStatusResult.ErrorMessage;
                            objMain.ReturnFlag = false;
                        }
                        else
                        {
                            //lst.Add(result.Data.ToString());

                            lst.Add(string.Format("TripID: {0} ", tripStatusResult.Data.TripNumber));

                            for (int ii = 0; ii <= tripStatusResult.Data.Count - 1; ii++)
                            {
                                //Leg level status
                                UWA.Entities.ATS.TripLegStatus leg = tripStatusResult.Data[ii];

                                lst.Add(string.Format("Leg No: {0} ", leg.LegNumber));

                                lst.Add(string.Format("Leg Schedule Id ", leg.ScheduleID));

                                lst.Add(string.Format("Leg Level Service Statues "));

                                //All service requested at Leg level
                                for (int llstatus = 0; llstatus <= leg.Statuses.Count() - 1; llstatus++)
                                {

                                    lst.Add(string.Format(" Service Name {0}: {1} ", leg.Statuses[llstatus].ServiceName, leg.Statuses[llstatus].Status));
                                }

                                //Leg Origin
                                UWA.Entities.ATS.TripLegEndPoint origin = leg.Origin;


                                lst.Add(string.Format(" ICAO {0} - {1} ", origin.ICAO, origin.EndPointType.ToString()));

                                //service requested at origin ICAO level
                                for (int oss = 0; oss <= origin.Statuses.Count() - 1; oss++)
                                {

                                    lst.Add(string.Format(" Service Name {0}: {1} ", origin.Statuses[oss].ServiceName, origin.Statuses[oss].Status));
                                }

                                //Leg Destination
                                UWA.Entities.ATS.TripLegEndPoint destination = leg.Destination;


                                lst.Add(string.Format(" ICAO {0} - {1} ", destination.ICAO, destination.EndPointType.ToString()));

                                //service requested at destination ICAO level
                                for (int dss = 0; dss <= destination.Statuses.Count() - 1; dss++)
                                {

                                    lst.Add(string.Format(" Service Name {0}: {1} ", destination.Statuses[dss].ServiceName, destination.Statuses[dss].Status));
                                }
                            }
                            objMain.EntityInfo = lst;
                            objMain.ReturnFlag = true;
                        }

                        ////Used for Dummy Return for complete flow.
                        //lst.Add("123456");
                        //objMain.EntityInfo = lst;
                        //objMain.ReturnFlag = true;

                    }, FlightPak.Common.Constants.Policy.UWAServiceLayer);

                }

                return objMain;

            }, FlightPak.Common.Constants.Policy.DataLayer);
        }
        #endregion

        #endregion

        #region EAPIS

        public ReturnValue<List<string>> EAPISSubmit(PreflightMain prefMain, PreflightLeg prefLeg, string sBound, string Domainname, string UserName)
        {
            ReturnValue<List<string>> objMain = null;
            Data.MasterCatalog.MasterDataContainer container = null;
            PreflightDataModelContainer containerPF = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<List<string>>>(() =>
            {

                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(prefMain, prefLeg, sBound, Domainname, UserName))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        objMain = new ReturnValue<List<string>>();
                        container = new Data.MasterCatalog.MasterDataContainer();
                        containerPF = new PreflightDataModelContainer();

                        #region Mapping Entities to UWA Service Entities
                        var EAPISTrip = new UWA.Entities.EAPIS.Manifest((UWA.Entities.EAPIS.ItineraryTypeEnum)Enum.Parse(typeof(UWA.Entities.EAPIS.ItineraryTypeEnum), sBound)); // need to be modified ***

                        #region Transaction Node

                        Data.Preflight.PreflightDataModelContainer containerpf = null;
                        containerpf = new Data.Preflight.PreflightDataModelContainer();
                        FlightPak.Data.Preflight.GetInterfaceCredentials ICredentials = new Data.Preflight.GetInterfaceCredentials();
                        ICredentials = containerpf.GetInterfaceCredentials(prefMain.CustomerID, UserName.Trim()).SingleOrDefault();
                        if (ICredentials != null)
                        {
                            // as per the instructions of Balaje & ravi, We are just passing 7 Character from the APIS Username as sender ID.
                            //EAPISTrip.Transaction.SenderID = (ICredentials.APISUserName == null ? string.Empty : ICredentials.APISUserName.ToString().Trim().Substring(0, 7));
                            //Above line commented by Ramesh and condition changed
                            EAPISTrip.Transaction.SenderID = (string.IsNullOrEmpty(ICredentials.APISUserName) ? string.Empty : ICredentials.APISUserName.ToString().Trim().Substring(0, 7));
                        }
                        //GetEmergencyContactNode -- Data   
                        if (prefMain.Fleet != null)
                        {
                            if (prefMain.EmergencyContact != null)
                            {
                                EAPISTrip.Transaction.EmergencyContact.FirstName = (prefMain.EmergencyContact.FirstName == null ? string.Empty : prefMain.EmergencyContact.FirstName.ToString());
                                EAPISTrip.Transaction.EmergencyContact.MiddleName = (prefMain.EmergencyContact.MiddleName == null ? string.Empty : prefMain.EmergencyContact.MiddleName.ToString());
                                EAPISTrip.Transaction.EmergencyContact.LastName = (prefMain.EmergencyContact.LastName == null ? string.Empty : prefMain.EmergencyContact.LastName.ToString());
                                EAPISTrip.Transaction.EmergencyContact.ContactInfo.PhoneNumber = (prefMain.EmergencyContact.PhoneNum == null ? string.Empty : prefMain.EmergencyContact.PhoneNum.ToString());
                                EAPISTrip.Transaction.EmergencyContact.ContactInfo.FaxNumber = (prefMain.EmergencyContact.FaxNum == null ? string.Empty : prefMain.EmergencyContact.FaxNum.ToString());
                                //Changes for removing SetEmailAddress in UWA.Service.Adapters
                                if (prefMain.EmergencyContact.EmailAddress != null && prefMain.EmergencyContact.EmailAddress != string.Empty)
                                    EAPISTrip.Transaction.EmergencyContact.ContactInfo.Email.Address = prefMain.EmergencyContact.EmailAddress;
                                else
                                    EAPISTrip.Transaction.EmergencyContact.ContactInfo.Email.Address = string.Empty;
                            }
                            EAPISTrip.Transaction.Itinerary.ArrivalLocation.ICAONotes = (prefMain.Fleet.Notes == null ? string.Empty : prefMain.Fleet.Notes);
                        }
                        //GetItineraryNode -- Data For Departure Details            
                        EAPISTrip.Transaction.Itinerary.DepartureLocation.ICAO = (prefLeg.DepartICAOID == null ? string.Empty : prefLeg.DepartICAOID.ToString());

                        if (prefLeg.Airport1 != null)
                            EAPISTrip.Transaction.Itinerary.DepartureLocation.City = (prefLeg.Airport1.CityName == null ? string.Empty : prefLeg.Airport1.CityName);
                        else
                            EAPISTrip.Transaction.Itinerary.DepartureLocation.City = string.Empty;

                        FlightPak.Data.MasterCatalog.Country country = new Data.MasterCatalog.Country();

                        EAPISTrip.Transaction.Itinerary.DepartureLocation.State = (prefLeg.Airport1.StateName == null ? string.Empty : prefLeg.Airport1.StateName);
                        if (prefLeg.Airport1.CountryID != null)
                            country = container.Country.Where(p => p.CountryID == prefLeg.Airport1.CountryID).SingleOrDefault();
                        if (country != null)
                            EAPISTrip.Transaction.Itinerary.DepartureLocation.CountryCode = (country.CountryCD == null ? string.Empty : country.CountryCD);

                        if (prefLeg.DepartureDTTMLocal != null)
                            EAPISTrip.Transaction.Itinerary.DepartureDate = (DateTime)(prefLeg.DepartureDTTMLocal); //Check the time format                    
                        EAPISTrip.Transaction.Itinerary.BorderCrossing = (prefLeg.USCrossing == null ? string.Empty : prefLeg.USCrossing);


                        EAPISTrip.Transaction.Itinerary.CompleteItinerary[0] = ""; //check .
                        EAPISTrip.Transaction.Itinerary.CompleteItinerary[1] = ""; //check .
                        EAPISTrip.Transaction.Itinerary.CompleteItinerary[2] = ""; //check .
                        EAPISTrip.Transaction.Itinerary.CompleteItinerary[3] = ""; //check .
                        EAPISTrip.Transaction.Itinerary.CompleteItinerary[4] = ""; //check .

                        //GetItineraryNode -- Data For Arrival Details  
                        EAPISTrip.Transaction.Itinerary.ArrivalLocation.ICAO = (prefLeg.ArriveICAOID == null ? string.Empty : prefLeg.ArriveICAOID.ToString());
                        if (prefLeg.Airport != null)
                        {
                            EAPISTrip.Transaction.Itinerary.ArrivalLocation.City = (prefLeg.Airport.CityName == null ? string.Empty : prefLeg.Airport.CityName);
                            EAPISTrip.Transaction.Itinerary.ArrivalLocation.State = (prefLeg.Airport.StateName == null ? string.Empty : prefLeg.Airport.StateName);
                            if (prefLeg.Airport.CountryID != null)
                                country = container.Country.Where(p => p.CountryID == prefLeg.Airport.CountryID).SingleOrDefault();
                            if (country != null)
                                EAPISTrip.Transaction.Itinerary.ArrivalLocation.CountryCode = (country.CountryCD == null ? string.Empty : country.CountryCD);
                        }
                        else
                        {
                            EAPISTrip.Transaction.Itinerary.ArrivalLocation.City = string.Empty;
                            EAPISTrip.Transaction.Itinerary.ArrivalLocation.State = string.Empty;
                            EAPISTrip.Transaction.Itinerary.ArrivalLocation.CountryCode = string.Empty;
                        }

                        if (prefLeg.ArrivalDTTMLocal != null)
                            EAPISTrip.Transaction.Itinerary.ArrivalDate = (DateTime)(prefLeg.ArrivalDTTMLocal); //Check the time format 

                        //GetAircraftNode -- Data
                        if (prefMain.Fleet != null)
                        {
                            EAPISTrip.Transaction.Aircraft.TailNumber = (prefMain.Fleet.TailNum == null ? string.Empty : prefMain.Fleet.TailNum);
                            EAPISTrip.Transaction.Aircraft.Type = (prefMain.Aircraft.AircraftDescription == null ? string.Empty : prefMain.Aircraft.AircraftDescription);

                            EAPISTrip.Transaction.Aircraft.CallSign = (prefMain.Fleet.TailNum == null ? string.Empty : prefMain.Fleet.TailNum);
                            FlightPak.Data.MasterCatalog.FleetPair fleetPair = new Data.MasterCatalog.FleetPair();

                            Int64 fleetID = (Int64)prefMain.FleetID;
                            fleetPair = container.FleetPair.Where(x => x.FleetID == fleetID).FirstOrDefault();

                            if (fleetPair != null)
                            {
                                EAPISTrip.Transaction.Aircraft.Color = (fleetPair.AircraftColor1 == null ? string.Empty : fleetPair.AircraftColor1);
                                EAPISTrip.Transaction.Aircraft.CBPDecalNumber = (fleetPair.BuyAircraftAdditionalFeeDOM == null ? string.Empty : fleetPair.BuyAircraftAdditionalFeeDOM);  // check with anoop.

                                //GetAircraftOwnerNode -- Data : In DBF O_ is operator 
                                if (!string.IsNullOrEmpty(fleetPair.OwnerLesse))
                                {
                                    EAPISTrip.Transaction.Aircraft.Owner.OperatorType = UWA.Entities.EAPIS.AircraftOperatorTypeEnum.Company;
                                    EAPISTrip.Transaction.Aircraft.Owner.CompanyName = (fleetPair.OwnerLesse == null ? string.Empty : fleetPair.OwnerLesse);
                                }
                                else
                                {
                                    EAPISTrip.Transaction.Aircraft.Owner.OperatorType = UWA.Entities.EAPIS.AircraftOperatorTypeEnum.Person;
                                    EAPISTrip.Transaction.Aircraft.Owner.LastName = (fleetPair.PLastName == null ? string.Empty : fleetPair.PLastName);
                                    EAPISTrip.Transaction.Aircraft.Owner.FirstName = (fleetPair.PFirstName == null ? string.Empty : fleetPair.PFirstName);
                                    EAPISTrip.Transaction.Aircraft.Owner.MiddleName = (fleetPair.PMiddlename == null ? string.Empty : fleetPair.PMiddlename);
                                }
                                EAPISTrip.Transaction.Aircraft.Owner.Address.Line1 = (fleetPair.Addr1 == null ? string.Empty : fleetPair.Addr1);
                                EAPISTrip.Transaction.Aircraft.Owner.Address.Line2 = (fleetPair.Addr2 == null ? string.Empty : fleetPair.Addr2);
                                EAPISTrip.Transaction.Aircraft.Owner.Address.City = (fleetPair.CityName == null ? string.Empty : fleetPair.CityName);
                                EAPISTrip.Transaction.Aircraft.Owner.Address.StateOrProvince = (fleetPair.StateName == null ? string.Empty : fleetPair.StateName);
                                EAPISTrip.Transaction.Aircraft.Owner.Address.PostalCode = (fleetPair.PostalZipCD == null ? string.Empty : fleetPair.PostalZipCD);
                                if (prefLeg.Airport.CountryID != null)
                                    country = container.Country.Where(p => p.CountryID == fleetPair.CountryID).SingleOrDefault();
                                if (country != null)
                                    EAPISTrip.Transaction.Aircraft.Owner.Address.Country = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                EAPISTrip.Transaction.Aircraft.Owner.ContactInfo.PhoneNumber = (fleetPair.PhoneNum == null ? string.Empty : fleetPair.PhoneNum);
                                EAPISTrip.Transaction.Aircraft.Owner.ContactInfo.FaxNumber = (fleetPair.FaxNum == null ? string.Empty : fleetPair.FaxNum);
                                //Changes for removing SetEmailAddress in UWA.Service.Adapters
                                if (fleetPair.EmailAddress != null && fleetPair.EmailAddress != string.Empty)

                                    EAPISTrip.Transaction.Aircraft.Owner.ContactInfo.Email.Address = fleetPair.EmailAddress;
                                else
                                    EAPISTrip.Transaction.Aircraft.Owner.ContactInfo.Email.Address = string.Empty;
                                //GetAircraftOperatorNode -- Data : In DBF O_ is operator 
                                if (!string.IsNullOrEmpty(fleetPair.OwnerLesse))
                                {
                                    EAPISTrip.Transaction.Aircraft.Operator.OperatorType = UWA.Entities.EAPIS.AircraftOperatorTypeEnum.Company;
                                    EAPISTrip.Transaction.Aircraft.Operator.CompanyName = (fleetPair.OwnerLesse == null ? string.Empty : fleetPair.OwnerLesse);
                                }
                                else
                                {
                                    EAPISTrip.Transaction.Aircraft.Operator.OperatorType = UWA.Entities.EAPIS.AircraftOperatorTypeEnum.Person;
                                    EAPISTrip.Transaction.Aircraft.Operator.LastName = (fleetPair.OLastName == null ? string.Empty : fleetPair.OLastName);
                                    EAPISTrip.Transaction.Aircraft.Operator.FirstName = (fleetPair.OFirstName == null ? string.Empty : fleetPair.OFirstName);
                                    EAPISTrip.Transaction.Aircraft.Operator.MiddleName = (fleetPair.OMiddleName == null ? string.Empty : fleetPair.OMiddleName);
                                }
                                EAPISTrip.Transaction.Aircraft.Operator.Address.Line1 = (fleetPair.Address1 == null ? string.Empty : fleetPair.Address1);
                                EAPISTrip.Transaction.Aircraft.Operator.Address.Line2 = (fleetPair.Address2 == null ? string.Empty : fleetPair.Address2);
                                EAPISTrip.Transaction.Aircraft.Operator.Address.City = (fleetPair.OCityName == null ? string.Empty : fleetPair.OCityName);
                                EAPISTrip.Transaction.Aircraft.Operator.Address.StateOrProvince = (fleetPair.OStateName == null ? string.Empty : fleetPair.OStateName);
                                EAPISTrip.Transaction.Aircraft.Operator.Address.PostalCode = (fleetPair.ZipCD == null ? string.Empty : fleetPair.ZipCD);
                                if (prefLeg.Airport.CountryID != null)
                                    country = container.Country.Where(p => p.CountryID == fleetPair.OCountryID).SingleOrDefault();
                                if (country != null)
                                    EAPISTrip.Transaction.Aircraft.Operator.Address.Country = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                EAPISTrip.Transaction.Aircraft.Operator.ContactInfo.PhoneNumber = (fleetPair.OPhoneNum == null ? string.Empty : fleetPair.OPhoneNum);
                                EAPISTrip.Transaction.Aircraft.Operator.ContactInfo.FaxNumber = (fleetPair.OFaxNum == null ? string.Empty : fleetPair.OFaxNum);
                                //Changes for removing SetEmailAddres in UWA.ServiceAdapters
                                EAPISTrip.Transaction.Aircraft.Operator.ContactInfo.Email.Address = fleetPair.OEmailAddress == null ? string.Empty : fleetPair.OEmailAddress;
                            }
                        }

                        #endregion

                        #region "Crew Details Mapping"
                        if (prefLeg.PreflightCrewLists != null)
                        {
                            foreach (PreflightCrewList crewLst in prefLeg.PreflightCrewLists)
                            {
                                if (crewLst.Crew != null)
                                {
                                    var vCrew = EAPISTrip.FlightManifest.AddCrew(true);

                                    vCrew.Document1.Type = UWA.Entities.EAPIS.DocumentTypeEnum.Passport;
                                    if (crewLst.PassportID != null)
                                    {
                                        CrewPassengerPassport crewPassPassport = containerPF.CrewPassengerPassports.Where(p => p.PassportID == crewLst.PassportID).FirstOrDefault();
                                        if (crewPassPassport != null)
                                        {
                                            vCrew.Document1.Number = (crewPassPassport.PassportNum == null ? string.Empty : Crypting.Decrypt(crewPassPassport.PassportNum));
                                            if (crewPassPassport.PassportExpiryDT != null)
                                                vCrew.Document1.ExpirationDate = (DateTime)(crewPassPassport.PassportExpiryDT);
                                            country = container.Country.Where(p => p.CountryID == crewPassPassport.CountryID).SingleOrDefault();
                                            if (country != null)
                                                vCrew.Document1.CountryCode = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                        }
                                    }
                                    if (crewLst.Crew.PilotLicense1 != null && crewLst.Crew.PilotLicense1 != string.Empty)
                                    {
                                        vCrew.Document2.Type = UWA.Entities.EAPIS.DocumentTypeEnum.PilotLicense;
                                        vCrew.Document2.Number = (crewLst.Crew.PilotLicense1 == null ? string.Empty : crewLst.Crew.PilotLicense1);
                                        if ((crewLst.Crew.License1ExpiryDT) != null)
                                            vCrew.Document2.ExpirationDate = (DateTime)crewLst.Crew.License1ExpiryDT;
                                        vCrew.Document2.CountryCode = (crewLst.Crew.LicenseCountry1 == null ? string.Empty : crewLst.Crew.LicenseCountry1);
                                    }
                                    //Permanent address is from the Crew Table
                                    if (crewLst.CrewID != null)
                                    {
                                        FlightPak.Data.MasterCatalog.Crew crew = new Data.MasterCatalog.Crew();
                                        crew = container.Crew.Where(c => c.CrewID == crewLst.CrewID).First();
                                        if (crew != null)
                                        {
                                            vCrew.LastName = (crew.LastName == null ? string.Empty : crew.LastName);
                                            vCrew.FirstName = (crew.FirstName == null ? string.Empty : crew.FirstName);
                                            vCrew.MiddleName = (crew.MiddleInitial == null ? string.Empty : crew.MiddleInitial);
                                            if (crew.BirthDT != null)
                                                vCrew.BirthDate = ((DateTime)crew.BirthDT);
                                            vCrew.ContactInfo.PhoneNumber = (crew.PhoneNum == null ? string.Empty : crew.PhoneNum);

                                            if (crew.Gender != null && crew.Gender == "M")
                                                vCrew.Gender = UWA.Entities.GenderEnum.Male;
                                            else if (crew.Gender != null && crew.Gender == "F")
                                                vCrew.Gender = UWA.Entities.GenderEnum.Female;

                                            if (crew.Citizenship != null)
                                                country = container.Country.Where(p => p.CountryID == crew.Citizenship).SingleOrDefault();
                                            if (country != null)
                                                vCrew.CountryCitizenship = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                            if (crew.Country != null)
                                                vCrew.CountryResidence = crew.Country.CountryCD == null ? string.Empty : crew.Country.CountryCD;

                                            vCrew.CityBirth = (crew.CityOfBirth == null ? string.Empty : crew.CityOfBirth);
                                            vCrew.StateProvinceBirth = (crew.StateofBirth == null ? string.Empty : crew.StateofBirth);
                                            vCrew.PermanentAddress.Line1 = (crew.Addr1 == null ? string.Empty : crew.Addr1);
                                            vCrew.PermanentAddress.Line2 = (crew.Addr2 == null ? string.Empty : crew.Addr2);
                                            vCrew.PermanentAddress.Line2 = (crew.Addr3 == null ? vCrew.PermanentAddress.Line2 : vCrew.PermanentAddress.Line2 + " " + crew.Addr3);
                                            vCrew.PermanentAddress.City = (crew.CityName == null ? string.Empty : crew.CityName);
                                            vCrew.PermanentAddress.StateOrProvince = (crew.StateName == null ? string.Empty : crew.StateName);
                                            vCrew.PermanentAddress.PostalCode = (crew.PostalZipCD == null ? string.Empty : crew.PostalZipCD);
                                            if (crew.CountryID != null)
                                                country = container.Country.Where(p => p.CountryID == crew.CountryID).SingleOrDefault();
                                            if (country != null)
                                            {
                                                vCrew.PermanentAddress.Country = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                            }

                                            if (crew.CountryOfBirth != null)
                                                country = container.Country.Where(p => p.CountryID == crew.CountryOfBirth).First();
                                            if (country != null)
                                                vCrew.CountryBirth = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                        }
                                    }

                                    //this is from the PreflightCrewlist- for Address in US
                                    vCrew.AddressWhileInUS.Line1 = (crewLst.Crew.Addr1 == null ? string.Empty : crewLst.Crew.Addr1);
                                    vCrew.AddressWhileInUS.Line2 = (crewLst.Crew.Addr2 == null ? string.Empty : crewLst.Crew.Addr2);
                                    vCrew.AddressWhileInUS.Line2 = (crewLst.Crew.Addr3 == null ? vCrew.AddressWhileInUS.Line2 : vCrew.AddressWhileInUS.Line2 + " " + crewLst.Crew.Addr3);
                                    vCrew.AddressWhileInUS.City = (crewLst.Crew.CityName == null ? string.Empty : crewLst.Crew.CityName);
                                }
                            }
                        }
                        #endregion

                        #region "Passenger Details Mapping"
                        if (prefLeg.PreflightPassengerLists != null)
                        {
                            foreach (PreflightPassengerList passLst in prefLeg.PreflightPassengerLists)
                            {
                                if (passLst.Passenger != null)
                                {
                                    var vPass = EAPISTrip.FlightManifest.AddPassenger();
                                    vPass.Document1.Type = UWA.Entities.EAPIS.DocumentTypeEnum.Passport;
                                    if (passLst.PassportID != null)
                                    {
                                        CrewPassengerPassport crewPassPassport = containerPF.CrewPassengerPassports.Where(p => p.PassportID == passLst.PassportID).First();
                                        vPass.Document1.Number = (crewPassPassport.PassportNum == null ? string.Empty : Crypting.Decrypt(crewPassPassport.PassportNum));
                                        if (crewPassPassport.PassportExpiryDT != null)
                                            vPass.Document1.ExpirationDate = (DateTime)(crewPassPassport.PassportExpiryDT);
                                        country = container.Country.Where(p => p.CountryID == crewPassPassport.CountryID).SingleOrDefault();
                                        if (country != null)
                                            vPass.Document1.CountryCode = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                    }
                                    if (passLst.VisaID != null)
                                    {
                                        FlightPak.Data.MasterCatalog.CrewPassengerVisa passvisa = new Data.MasterCatalog.CrewPassengerVisa();
                                        passvisa = container.CrewPassengerVisa.Where(p => p.VisaID == passLst.VisaID).First();
                                        if (passvisa != null)
                                        {
                                            vPass.Document2.Type = UWA.Entities.EAPIS.DocumentTypeEnum.USAlienRegCard;
                                            vPass.Document2.Number = (passvisa.VisaNum == null ? string.Empty : Crypting.Decrypt(passvisa.VisaNum));
                                            if (passvisa.ExpiryDT != null)
                                                vPass.Document2.ExpirationDate = (DateTime)(passvisa.ExpiryDT);
                                            if (passvisa.CountryID != null)
                                                country = container.Country.Where(p => p.CountryID == passvisa.CountryID).SingleOrDefault();
                                            if (country != null)
                                                vPass.Document2.CountryCode = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                        }
                                    }

                                    vPass.AddressWhileInUS.Line1 = (passLst.Street == null ? string.Empty : passLst.Street);
                                    vPass.AddressWhileInUS.City = (passLst.CityName == null ? string.Empty : passLst.CityName);
                                    if (passLst.PassengerID != null)
                                    {
                                        FlightPak.Data.MasterCatalog.Passenger pass = new Data.MasterCatalog.Passenger();
                                        pass = container.Passenger.Where(p => p.PassengerRequestorID == passLst.PassengerID).First();
                                        if (pass != null)
                                        {
                                            vPass.LastName = (pass.LastName == null ? string.Empty : pass.LastName);
                                            vPass.FirstName = (pass.FirstName == null ? string.Empty : pass.FirstName);
                                            vPass.MiddleName = (pass.MiddleInitial == null ? string.Empty : pass.MiddleInitial);
                                            if (pass.DateOfBirth != null)
                                                vPass.BirthDate = (DateTime)pass.DateOfBirth;
                                            vPass.ContactInfo.PhoneNumber = (pass.PhoneNum == null ? string.Empty : pass.PhoneNum);

                                            if (pass.Gender != null && pass.Gender == "M")
                                                vPass.Gender = UWA.Entities.GenderEnum.Male;
                                            else if (pass.Gender != null && pass.Gender == "F")
                                                vPass.Gender = UWA.Entities.GenderEnum.Female;

                                            if (pass.CountryOfResidenceID != null)
                                                country = container.Country.Where(p => p.CountryID == pass.CountryOfResidenceID).SingleOrDefault();
                                            if (country != null)
                                            {
                                                vPass.CountryResidence = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                            }
                                            if (pass.CountryID != null)
                                                country = container.Country.Where(p => p.CountryID == pass.CountryID).SingleOrDefault();
                                            if (country != null)
                                            {
                                                vPass.CountryCitizenship = (country.CountryCD == null ? string.Empty : country.CountryCD);
                                            }
                                        }
                                    }
                                    //Physical Address - AddressWhileInUS 

                                }
                            }
                        }
                        #endregion

                        #endregion

                        #region Hard Codeded Values for Testing
                        //EAPISTrip.Transaction.SenderID = "87654321";
                        //EAPISTrip.Transaction.EmergencyContact.FirstName = "Johnny";
                        //EAPISTrip.Transaction.EmergencyContact.LastName = "Depp";
                        //EAPISTrip.Transaction.EmergencyContact.Gender = UWA.Entities.GenderEnum.Male;
                        //EAPISTrip.Transaction.EmergencyContact.ContactInfo.SetEmailAddress("johnnydepp@yahoo.com");

                        //EAPISTrip.Transaction.Itinerary.DepartureLocation.ICAO = "KJFK";
                        //EAPISTrip.Transaction.Itinerary.DepartureLocation.City = "New York";
                        //EAPISTrip.Transaction.Itinerary.DepartureLocation.CountryCode = "USA";
                        //EAPISTrip.Transaction.Itinerary.CompleteItinerary[0] = "KLAG";
                        //EAPISTrip.Transaction.Itinerary.CompleteItinerary[1] = "KPHX";

                        //EAPISTrip.Transaction.Itinerary.ArrivalDate = DateTime.Now.Subtract(TimeSpan.FromDays(4));
                        //EAPISTrip.Transaction.Itinerary.ArrivalLocation.ICAO = "KIAH";
                        //EAPISTrip.Transaction.Itinerary.ArrivalLocation.City = "Houston";
                        //EAPISTrip.Transaction.Itinerary.ArrivalLocation.State = "TX";
                        //EAPISTrip.Transaction.Itinerary.ArrivalLocation.ICAONotes = "NO goddamn parking.";

                        //EAPISTrip.Transaction.Aircraft.Color = "TEAL";

                        //EAPISTrip.Transaction.Aircraft.Operator.OperatorType = UWA.Entities.EAPIS.AircraftOperatorTypeEnum.Person;
                        //EAPISTrip.Transaction.Aircraft.Operator.CompanyName = "SKY HIGH, INC.";
                        //EAPISTrip.Transaction.Aircraft.Operator.ContactInfo.SetEmailAddress("skyhigh@yahoo.com");
                        //EAPISTrip.Transaction.Aircraft.Operator.Address.Line1 = "2700 Barbary Lane";
                        //EAPISTrip.Transaction.Aircraft.Operator.Address.Line2 = "Suite 40";
                        //EAPISTrip.Transaction.Aircraft.Operator.Address.City = "New York";
                        //EAPISTrip.Transaction.Aircraft.Operator.Address.Country = "USA";
                        //EAPISTrip.Transaction.Aircraft.Operator.ContactInfo.FaxNumber = "713-222-1508";

                        //EAPISTrip.Transaction.Aircraft.Owner.OperatorType = UWA.Entities.EAPIS.AircraftOperatorTypeEnum.Company;
                        //EAPISTrip.Transaction.Aircraft.Owner.FirstName = "Sanjay";
                        //EAPISTrip.Transaction.Aircraft.Owner.LastName = "Gupta";
                        //EAPISTrip.Transaction.Aircraft.Owner.MiddleName = "Mahatma";
                        //EAPISTrip.Transaction.Aircraft.Owner.CompanyName = "India Airlines";
                        //EAPISTrip.Transaction.Aircraft.Owner.ContactInfo.SetEmailAddress("sgupta@yahoo.com");
                        //EAPISTrip.Transaction.Aircraft.Owner.Address.Line1 = "100 New Dehli Blvd";
                        //EAPISTrip.Transaction.Aircraft.Owner.Address.City = "Mumbai";
                        //EAPISTrip.Transaction.Aircraft.Owner.Address.Country = "IND";
                        //EAPISTrip.Transaction.Aircraft.Owner.ContactInfo.PhoneNumber = "44-766-222-9000";

                        //EAPISTrip.Transaction.EmergencyContact.ContactInfo.PhoneNumber = "223423424";
                        //EAPISTrip.Transaction.Itinerary.ArrivalLocation.CountryCode = "US";
                        //EAPISTrip.Transaction.Itinerary.DepartureLocation.State = "TX";
                        //EAPISTrip.Transaction.Aircraft.TailNumber = "FR546456";
                        //EAPISTrip.Transaction.Aircraft.Type = "Flight";
                        //EAPISTrip.Transaction.Aircraft.Operator.FirstName = "John";
                        //EAPISTrip.Transaction.Aircraft.Operator.LastName = "Duo";
                        //EAPISTrip.Transaction.Aircraft.Operator.ContactInfo.PhoneNumber = "567454374567";



                        //var crew1 = EAPISTrip.FlightManifest.AddCrew(true);
                        //crew1.Document1.Type = UWA.Entities.EAPIS.DocumentTypeEnum.SENTRI;
                        //crew1.Document1.Number = "5555";
                        //crew1.Document1.CountryCode = "AUS";
                        //crew1.Document1.ExpirationDate = DateTime.Now.AddYears(3);

                        //crew1.Document2.Type = UWA.Entities.EAPIS.DocumentTypeEnum.Passport;
                        //crew1.Document2.Number = "1234567890";
                        //crew1.Document2.CountryCode = "UK";
                        //crew1.Document2.ExpirationDate = DateTime.Now.AddYears(2);

                        //crew1.FirstName = "Kenzo";
                        //crew1.LastName = "Suzuki";
                        //crew1.Gender = UWA.Entities.GenderEnum.Male;
                        //crew1.PermanentAddress.Country = "JPN";
                        //crew1.PermanentAddress.City = "Tokyo";
                        //crew1.PermanentAddress.Line1 = "1083 Hisho Lane";
                        //crew1.PermanentAddress.StateOrProvince = "TX";
                        //crew1.BirthDate = DateTime.Now.AddYears(-49);
                        //crew1.CityBirth = "Tokyo";
                        //crew1.CountryBirth = "KOR";
                        //crew1.CountryCitizenship = "JPN";
                        //crew1.CountryResidence = "JPN";
                        //crew1.AddressWhileInUS.Line1 = "Ridge view st";
                        //crew1.AddressWhileInUS.City = "Edison";
                        //crew1.ContactInfo.PhoneNumber = "1235324234345";


                        //var pax1 = EAPISTrip.FlightManifest.AddPassenger();
                        //pax1.Gender = UWA.Entities.GenderEnum.Female;
                        //pax1.FirstName = "Olga";
                        //pax1.MiddleName = "Laffson";
                        //pax1.LastName = "Schmidt";
                        //pax1.CountryResidence = "NOR";

                        //pax1.AddressWhileInUS.City = "Dallas";
                        //pax1.AddressWhileInUS.Line1 = "4988 Main Street";
                        //pax1.AddressWhileInUS.StateOrProvince = "TX";
                        //pax1.CountryResidence = "USA";
                        //pax1.CountryCitizenship = "NOR";

                        //pax1.Document1.Type = UWA.Entities.EAPIS.DocumentTypeEnum.USAlienRegCard;
                        //pax1.Document1.Number = "10200300";
                        //pax1.Document1.CountryCode = "SA";
                        //pax1.Document1.ExpirationDate = DateTime.Now.AddYears(8);

                        //pax1.Document2.Type = UWA.Entities.EAPIS.DocumentTypeEnum.Passport;
                        //pax1.Document2.Number = "98765434";
                        //pax1.Document2.CountryCode = "SA";
                        //pax1.Document2.ExpirationDate = DateTime.Now.AddYears(1);

                        //pax1.BirthDate = DateTime.Now.AddYears(-15);
                        //pax1.ContactInfo.PhoneNumber = "6758678678";

                        //var pax2 = EAPISTrip.FlightManifest.AddPassenger();
                        //pax2.Gender = UWA.Entities.GenderEnum.Male;
                        //pax2.FirstName = "Jason";
                        //pax2.MiddleName = "Montgomery";
                        //pax2.LastName = "Granger";
                        //pax2.CountryResidence = "CAN";
                        //pax2.BirthDate = DateTime.Now.AddYears(-26);
                        //pax2.CountryCitizenship = "USA";
                        //pax2.AddressWhileInUS.Line1 = "Pine place";
                        //pax2.AddressWhileInUS.City = "Orange valley";

                        //pax2.Document1.Type = UWA.Entities.EAPIS.DocumentTypeEnum.MilitaryIDCard;
                        //pax2.Document1.Number = "98765434";
                        //pax2.Document1.CountryCode = "SA";
                        //pax2.Document1.ExpirationDate = DateTime.Now.AddYears(1);

                        //pax2.Document2.Type = UWA.Entities.EAPIS.DocumentTypeEnum.Passport;
                        //pax2.Document2.Number = "98765434";
                        //pax2.Document2.CountryCode = "SA";
                        //pax2.Document2.ExpirationDate = DateTime.Now.AddYears(1);


                        //pax2.ContactInfo.PhoneNumber = "6758678678";



                        #endregion

                        #region Write to XML to check the Data
                        //To check the data is populated correctly.

                        //string atsXml = EAPISTrip.ToString();
                        //var doc = new System.Xml.XmlDocument();
                        //doc.LoadXml(atsXml);
                        //var atsWriter = new System.Xml.XmlTextWriter(@"c:\temp\EAPIS.xml", null);
                        //atsWriter.Formatting = System.Xml.Formatting.Indented;
                        //doc.Save(atsWriter);

                        #endregion

                        #region Submit to EAPIS

                        //CBP username / password and UWA registered username / password
                        string cbpusername = "", cbppassword = "", username = "", password = "";
                        if (ICredentials != null)
                        {
                            cbpusername = Convert.ToString(ICredentials.CBPUserName == null ? string.Empty : ICredentials.CBPUserName).Trim();
                            cbppassword = Convert.ToString(ICredentials.CBPPassword == null ? string.Empty : ICredentials.CBPPassword).Trim();
                            username = Convert.ToString(ICredentials.APISUserName == null ? string.Empty : ICredentials.APISUserName).Trim();
                            password = Convert.ToString(ICredentials.APISPassword == null ? string.Empty : ICredentials.APISPassword).Trim();
                        }
                        //var credentials = new UWA.Adapters.EAPISCredentials("APGA9786", "14STERLING!", Guid.Empty, Domainname, "uwafpktest", "password");
                        var credentials = new UWA.Adapters.EAPISCredentials(cbpusername, cbppassword, Guid.Empty, Domainname, username, password);
                        UWA.Adapters.EAPIS client = new UWA.Adapters.EAPIS(credentials, EAPISTrip);
                        var result = client.ValidateManifest();
                        List<string> lst = new List<string>();
                        if (!result.HasError)
                        {
                            if (result.Data)
                            {
                                //if (prefLeg.UWAID == null && prefLeg.UWAID == string.Empty)
                                if (string.IsNullOrEmpty(prefLeg.UWAID.Trim()))
                                {
                                    var returnValue = client.SubmitManifest();
                                    lst.Add(returnValue.InfoMessage);
                                    lst.Add(returnValue.DataTimeStamp.ToString());
                                    objMain.EntityInfo = lst;
                                    objMain.ReturnFlag = true;
                                }
                                else
                                {
                                    var returnValue = client.UpdateManifest();
                                    lst.Add(Convert.ToString(returnValue.InfoMessage) == string.Empty ? Convert.ToString(prefLeg.UWAID.Trim()) : returnValue.InfoMessage);
                                    lst.Add(returnValue.DataTimeStamp.ToString());
                                    objMain.EntityInfo = lst;
                                    objMain.ReturnFlag = true;
                                }
                            }
                        }
                        else
                        {
                            objMain.ErrorMessage = result.ErrorMessage;
                            if (EAPISTrip.ValidationErrors.Length > 0)
                            {
                                foreach (string error in EAPISTrip.ValidationErrors)
                                {
                                    lst.Add(error);
                                }
                                objMain.EntityInfo = lst;
                            }
                            objMain.ReturnFlag = false;
                        }

                        #endregion
                    }, FlightPak.Common.Constants.Policy.UWAServiceLayer);
                }

                return objMain;
            }, FlightPak.Common.Constants.Policy.DataLayer);
        }

        #endregion

        #region FUEL

        public ReturnValue<PreflightFuel> GetFuelPrice(List<string> ICAOID, string username, string password)
        {
            ReturnValue<PreflightFuel> objMain = null;
            List<PreflightFuel> uwaPFFuelLst = null;
            PreflightFuel uwaPFFuel = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<PreflightFuel>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ICAOID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        uwaPFFuelLst = new List<PreflightFuel>();
                        objMain = new ReturnValue<PreflightFuel>();
                        username = username.Trim();
                        String urlAuthentcateUser = String.Format(@"{0}GetLegalFlag?login={1}",ConfigurationManager.AppSettings["FuelServiceUrl"], username);
                        String urlFuelEstimation = String.Format(@"{0}CreateFuelEstimate?fmt=xml&user={1}&source={2}&icao={3}&UvAirCardNumber={4}&registry={5}&estimateType={6}&favoriteList={7}&searchBy={8}&quantity={9}",ConfigurationManager.AppSettings["FuelServiceUrl"] , username, "UFSS", ICAOID.First(), "0", "0", "0", "0", "CODE", "0");
                        String authenticateResult = CallWebserviceViaUrl(urlAuthentcateUser); // This return "Y" for valid RU3 users.

                        if (authenticateResult.Equals("\"Y\""))
                        {
                            var fueldata = CallWebserviceViaUrl(urlFuelEstimation);

                            if (!String.IsNullOrEmpty(fueldata))
                            {
                                var ds = GetPricesParser(fueldata);
                                if (ds != null)
                                {
                                    var fuelprices = MapTables(ds);
                                    if (fuelprices != null)
                                    {
                                        foreach (UWA.Entities.Fuel.Airport airport in fuelprices.Airports)
                                        {
                                            foreach (UWA.Entities.Fuel.Provider provider in airport.Providers)
                                            {
                                                foreach (UWA.Entities.Fuel.Type type in provider.FuelTypes)
                                                {
                                                    foreach (UWA.Entities.Fuel.Price price in type.Prices)
                                                    {
                                                        uwaPFFuel = new PreflightFuel();
                                                        uwaPFFuel.FBOName = provider.Name;
                                                        uwaPFFuel.GallonFrom = price.MinimumGallons;
                                                        uwaPFFuel.GallonTo = price.MaximumGallons;
                                                        uwaPFFuel.EffectiveDT = price.EffectiveDate;
                                                        //uwaPFFuel.UnitPrice = price.UVPrice;
                                                        uwaPFFuel.UnitPrice = price.Total;
                                                        //uwaPFFuel.UWAFuelInterfaceRequestID = UserCredentials
                                                        uwaPFFuelLst.Add(uwaPFFuel);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    objMain.EntityList = uwaPFFuelLst;
                                    objMain.ReturnFlag = true;
                                }
                            }
                        }
                       
                    }, FlightPak.Common.Constants.Policy.UWAServiceLayer);

                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.DataLayer);
        }


        private String CallWebserviceViaUrl(String methodUrl)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                byte[] buffer = null;
                var data = UWA.Common.Shared.Net.DownloadData(methodUrl);
                if (data.Data != null)
                {
                    buffer = data.Data;
                    return buffer.GetString();
                }
                return String.Empty;
            }, FlightPak.Common.Constants.Policy.UWAServiceLayer);
        }

        /// <summary>
        /// Parses the XML from GetFuelPrices.
        /// </summary>
        public DataSet GetPricesParser(string xmlData)
        {
            try
            {
                const string SummaryTableName = "Summary";
                const string AirportsTableName = "Airports";
                const string ProvidersTableName = "Providers";
                const string FuelsTableName = "Fuels";
                const string PricesTableName = "Prices";

                DataSet dataSet;
                DataSet ds = new System.Data.DataSet();
                ds.ReadXml(new StringReader(xmlData));

                // Rename the foreign key columns
                ds.Tables["FboSDC"].Columns["FBOs_ID"].ColumnName = "AirportID";
                ds.Tables["FuelSDC"].Columns["Fuels_ID"].ColumnName = "ProviderID";
                ds.Tables["PriceSDC"].Columns["Prices_ID"].ColumnName = "FuelID";

                // Copy only the tables we need
                dataSet = new DataSet();
                dataSet.Tables.Add(ds.Tables["FuelPriceDataSDC"].Copy());
                dataSet.Tables.Add(ds.Tables["AirportSDC"].Copy());
                dataSet.Tables.Add(ds.Tables["FboSDC"].Copy());
                dataSet.Tables.Add(ds.Tables["FuelSDC"].Copy());
                dataSet.Tables.Add(ds.Tables["PriceSDC"].Copy());

                // Rename some tables
                dataSet.Tables["FuelPriceDataSDC"].TableName = SummaryTableName;
                dataSet.Tables["AirportSDC"].TableName = AirportsTableName;
                dataSet.Tables["FboSDC"].TableName = ProvidersTableName;
                dataSet.Tables["FuelSDC"].TableName = FuelsTableName;
                dataSet.Tables["PriceSDC"].TableName = PricesTableName;

                dataSet.Tables[SummaryTableName].PrimaryKey = new DataColumn[0];
                dataSet.Tables[SummaryTableName].Columns.Remove("FuelPriceDataSDC_ID");
                dataSet.Tables[AirportsTableName].Columns.Remove("Airports_ID");
                dataSet.Tables[AirportsTableName].Columns["AirportSDC_Id"].ColumnName = "AirportID";
                dataSet.Tables[ProvidersTableName].Columns["FboSDC_Id"].ColumnName = "ProviderID";
                dataSet.Tables[FuelsTableName].Columns["FuelSDC_Id"].ColumnName = "FuelID";

                // Set up new data relations.
                dataSet.Relations.Add("AirportToProvider", dataSet.Tables[AirportsTableName].Columns["AirportID"],
                    dataSet.Tables[ProvidersTableName].Columns["AirportID"]);
                dataSet.Relations.Add("ProviderToFuel", dataSet.Tables[ProvidersTableName].Columns["ProviderID"],
                    dataSet.Tables[FuelsTableName].Columns["ProviderID"]);
                dataSet.Relations.Add("FuelToPrice", dataSet.Tables[FuelsTableName].Columns["FuelID"],
                    dataSet.Tables[PricesTableName].Columns["FuelID"]);

                return dataSet;
            }
            catch (Exception ec)
            {
                return null;
            }
        }

        /// <summary>
        /// Method for parsing Dataset to Entiry classes
        /// </summary>
        /// <param name="ds">Dataset of Fuel FBO and prices</param>
        /// <returns></returns>
        private Prices MapTables(DataSet ds)
        {
            #region Create the FuelEstimates
            // Create the FuelEstimates object by looping through the incoming dataset.
            var summary = new Prices();
            DataTable dtSummary = ds.Tables["Summary"];
            DataTable dtAirports = ds.Tables["Airports"];
            DataTable dtProviders = ds.Tables["Providers"];
            DataTable dtFuels = ds.Tables["Fuels"];
            DataTable dtPrices = ds.Tables.Contains("Prices") ? ds.Tables["Prices"] : null;

            foreach (DataRow drSummary in dtSummary.Rows)
            {
                summary.Comment = UWA.Common.Shared.Utility.GetString(drSummary["Comment"]);
                summary.EffectiveDate = Convert.ToDateTime(drSummary["EffectiveDate"]);
                summary.EstimateID = UWA.Common.Shared.Utility.GetInt64(drSummary["EstimateId"]);
                summary.ID = new Guid(drSummary["Id"].ToString());
                summary.MNumber = UWA.Common.Shared.Utility.GetString(drSummary["Mnumber"]);
                summary.ResponseType = Convert.ToString(drSummary["ResponseType"]);
                summary.SequenceNumber = UWA.Common.Shared.Utility.GetInt64(drSummary["SequenceNumber"]);
                summary.StatusCode = Convert.ToString(drSummary["StatusCode"]);
                summary.SuccessfulICAO = UWA.Common.Shared.Utility.GetInt32(drSummary["SuccessfulICAO"]);
                summary.AreTaxesAndFeesIncluded = Convert.ToBoolean(drSummary["TaxesFeesIncluded"]);
                summary.TripNumber = UWA.Common.Shared.Utility.GetInt64(drSummary["Tnumber"]);
                summary.TripScheduleID = UWA.Common.Shared.Utility.GetInt64(drSummary["TripScheduleId"]);
                summary.UrlSource = UWA.Common.Shared.Utility.GetString(drSummary["UrlSource"]);

                #region Add Airports
                var listAirports = new List<UWA.Entities.Fuel.Airport>(dtAirports.Rows.Count);
                foreach (DataRow drAirport in dtAirports.Rows)
                {
                    var objAirport = new UWA.Entities.Fuel.Airport();
                    objAirport.Name = UWA.Common.Shared.Utility.GetString(drAirport["AirportName"]);
                    objAirport.City = UWA.Common.Shared.Utility.GetString(drAirport["City"]);
                    objAirport.CityNotes = UWA.Common.Shared.Utility.GetString(drAirport["CityNotes"]);
                    objAirport.Country = UWA.Common.Shared.Utility.GetString(drAirport["Country"]);
                    objAirport.CountryCode = UWA.Common.Shared.Utility.GetString(drAirport["CountryCode"]);
                    objAirport.CountryNotes = UWA.Common.Shared.Utility.GetString(drAirport["CountryNotes"]);
                    objAirport.ErrorMessage = UWA.Common.Shared.Utility.GetString(drAirport["ErrorMessage"]);
                    objAirport.IATA = UWA.Common.Shared.Utility.GetString(drAirport["IATA"]);
                    objAirport.ICAO = UWA.Common.Shared.Utility.GetString(drAirport["ICAO"]);
                    objAirport.ICAONotes = UWA.Common.Shared.Utility.GetString(drAirport["ICAONotes"]);
                    objAirport.ID = new Guid(drAirport["Id"].ToString());
                    objAirport.State = UWA.Common.Shared.Utility.GetString(drAirport["State"]);
                    objAirport.IsCountryVatExempt = UWA.Common.Shared.Utility.GetString(drAirport["VatExemptCountry"]).ToUpper() == "Y";
                    listAirports.Add(objAirport);

                    #region Add Providers
                    // Add providers in a loop, but be sure to match by airport
                    var providerView = dtProviders.DefaultView.RowFilter = "AirportID = " + drAirport["AirportID"].ToString();
                    var listProviders = new List<Provider>(dtProviders.DefaultView.Count);
                    foreach (DataRowView drvProvider in dtProviders.DefaultView)
                    {
                        var objProvider = new Provider();
                        objProvider.ErrorMessage = UWA.Common.Shared.Utility.GetString(drvProvider["ErrorMessage"]);
                        objProvider.Notes = UWA.Common.Shared.Utility.GetString(drvProvider["FBONotes"]);
                        objProvider.Name = UWA.Common.Shared.Utility.GetString(drvProvider["Name"]);
                        objProvider.HasIDCard = UWA.Common.Shared.Utility.GetString(drvProvider["IDCard"]).ToUpper() == "Y";
                        objProvider.ID = new Guid(drvProvider["Id"].ToString());
                        objProvider.Link = UWA.Common.Shared.Utility.GetString(drvProvider["Link"]);
                        objProvider.OnRequest = UWA.Common.Shared.Utility.GetString(drvProvider["OnRequest"]);
                        objProvider.OnRequestNotes = UWA.Common.Shared.Utility.GetString(drvProvider["OnRequestNotes"]);
                        objProvider.UpliftDate = UWA.Common.Shared.Utility.GetDateTimeValue(drvProvider["UpliftDate"]).GetValueOrDefault();
                        listProviders.Add(objProvider);

                        #region Add Fuel Types
                        // Add fuel types in a loop, but be sure to match by Provider
                        var fuelsView = dtFuels.DefaultView.RowFilter = "ProviderID = " + drvProvider["ProviderID"].ToString();
                        var listFuels = new List<UWA.Entities.Fuel.Type>(dtFuels.DefaultView.Count);
                        foreach (DataRowView drvFuel in dtFuels.DefaultView)
                        {
                            var objFuel = new UWA.Entities.Fuel.Type();
                            objFuel.ID = new Guid(drvFuel["Id"].ToString());
                            objFuel.LocationID = UWA.Common.Shared.Utility.GetInt64(drvFuel["FuelLocationID"]);
                            objFuel.Name = UWA.Common.Shared.Utility.GetString(drvFuel["Type"]);
                            listFuels.Add(objFuel);

                            #region Add fuel prices, mathching by fuel type, BUT ONLY IF THE TABLE EXISTS
                            if (dtPrices == null)
                            {
                                continue;
                            }
                            else
                            {
                                var pricesView = dtPrices.DefaultView.RowFilter = "FuelID = " + drvFuel["FuelID"].ToString();
                                var listPrices = new List<Price>(dtPrices.DefaultView.Count);
                                foreach (DataRowView drvPrice in dtPrices.DefaultView)
                                {
                                    var objPrice = new Price();
                                    objPrice.ID = new Guid(drvPrice["Id"].ToString());
                                    if (dtPrices.Columns.Contains("AdditionalFees"))
                                    {
                                        objPrice.AdditionalFees = UWA.Common.Shared.Utility.GetString(drvPrice["AdditionalFees"]);
                                    }
                                    else
                                    {
                                        objPrice.AdditionalFees = string.Empty;
                                    }
                                    objPrice.BasePerGallon = UWA.Common.Shared.Utility.GetDecimal(drvPrice["BasePerGal"]);
                                    objPrice.BasePrice = UWA.Common.Shared.Utility.GetDecimal(drvPrice["BasePrice"]);
                                    objPrice.ClientPrice = UWA.Common.Shared.Utility.GetDecimal(drvPrice["ClientPrice"]);
                                    objPrice.EffectiveDate = UWA.Common.Shared.Utility.GetDateTimeValue(drvPrice["EffectiveDate"]).GetValueOrDefault();
                                    objPrice.ErrorMessage = UWA.Common.Shared.Utility.GetString(drvPrice["ErrorMessage"]);
                                    objPrice.FuelTax = UWA.Common.Shared.Utility.GetDecimal(drvPrice["FuelTax"]);
                                    objPrice.MaximumGallons = UWA.Common.Shared.Utility.GetInt32(drvPrice["MaxGal"]);
                                    objPrice.MinimumGallons = UWA.Common.Shared.Utility.GetInt32(drvPrice["MinGal"]);
                                    objPrice.PostedPerGallon = UWA.Common.Shared.Utility.GetDecimal(drvPrice["PostedPerGal"]);
                                    objPrice.SalesTax = UWA.Common.Shared.Utility.GetDecimal(drvPrice["SalesTax"]);
                                    objPrice.IsSpecialRate = UWA.Common.Shared.Utility.GetString(drvPrice["SpecialRateFlag"]).ToUpper() == "Y";
                                    objPrice.SpecialRateID = UWA.Common.Shared.Utility.GetInt32(drvPrice["SpecialRateId"]);
                                    if (drvPrice.Row.Table.Columns.Contains("TaxBreakdowns"))
                                    {
                                        objPrice.TaxBreakdowns = UWA.Common.Shared.Utility.GetString(drvPrice["TaxBreakdowns"]);
                                    }
                                    objPrice.Tier = UWA.Common.Shared.Utility.GetInt16(drvPrice["Tier"]);
                                    objPrice.Total = UWA.Common.Shared.Utility.GetDecimal(drvPrice["Total"]);
                                    objPrice.UVMargin = UWA.Common.Shared.Utility.GetDecimal(drvPrice["UVMargin"]);
                                    objPrice.UVPrice = UWA.Common.Shared.Utility.GetDecimal(drvPrice["UVPrice"]);
                                    listPrices.Add(objPrice);
                                }
                                objFuel.Prices = listPrices.ToArray();
                            }

                            #endregion
                        }
                        objProvider.FuelTypes = listFuels.ToArray();
                        #endregion
                    }
                    objAirport.Providers = listProviders.ToArray();
                    #endregion
                }
                summary.Airports = listAirports.ToArray();
                #endregion
            }
            #endregion

            return summary;
        }

        #endregion



        #region Submit to EAPIS Company Profile

        public ReturnValue<List<string>> CompanyEAPISSubmit(Int64 CustomerID, string DomainName, string UserName, string SenderID, string Password)
        {
            ReturnValue<List<string>> objMain = new ReturnValue<List<string>>();
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<List<string>>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, UserName, SenderID, Password))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Data.Preflight.PreflightDataModelContainer containerpf = null;
                        containerpf = new Data.Preflight.PreflightDataModelContainer();
                        FlightPak.Data.Preflight.GetInterfaceCredentials ICredentials = new Data.Preflight.GetInterfaceCredentials();
                        ICredentials = containerpf.GetInterfaceCredentials(CustomerID, UserName.Trim()).SingleOrDefault();
                        string cbpusername = "", cbppassword = "", username = "", password = "";
                        if (ICredentials != null)
                        {
                            cbpusername = Convert.ToString(ICredentials.CBPUserName.Trim());
                            cbppassword = Convert.ToString(ICredentials.CBPPassword.Trim());
                            username = Convert.ToString(ICredentials.APISUserName.Trim());
                            password = Convert.ToString(ICredentials.APISPassword.Trim());
                        }
                        var credentials = new UWA.Adapters.EAPISCredentials(cbpusername, cbppassword, Guid.Empty, DomainName, username, password);
                        UWA.Adapters.EAPIS client = new UWA.Adapters.EAPIS(credentials);
                        client.UpdateCBPCredentials(SenderID, Password);


                    }, FlightPak.Common.Constants.Policy.UWAServiceLayer);

                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.DataLayer);
        }
           
    }

}
        #endregion
