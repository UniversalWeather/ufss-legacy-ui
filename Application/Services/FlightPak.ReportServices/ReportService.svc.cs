﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ReportServices.wsReportService2005;
using System.Configuration;
using ReportServices.Base;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Xml;
using System.IO;
using System.Web;

namespace ReportServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ReportService : BaseService, IReportService
    {
        private ExceptionManager exManager;

        #region Render report from the server

        public byte[] GetReport(string filepath)
        {
            byte[] reportDefinition = null; //Holds the Original RDL byte array.

            try
            {
                wsReportService2005.ReportingService2005 rs = new wsReportService2005.ReportingService2005();

                rs.Credentials = GetCredentials();

                //Retrieving the list of all the items deployed in the Report Server.
                wsReportService2005.CatalogItem[] catalogitems = rs.ListChildren("/", true);

                //Looping through each and every item.
                foreach (wsReportService2005.CatalogItem item in catalogitems)
                {
                    string strname = item.Name;
                    string strtype = item.Type + " " + item.CreatedBy + " " + item.CreationDate + " " + item.Description + " " + item.ExecutionDate + " " + item.ID + " " + item.ModifiedBy + " " + item.ModifiedDate + " " + item.Path + " " + item.Size;
                    //We're concerned only about the reports and thats were the processing starts.
                    //if (ItemTypeEnum.Report == item.Type)

                    if (item.Path.ToLower() == filepath.ToLower())
                    {
                        string reportPath = item.Path; //Path of the report that is currently being processed.
                        reportDefinition = rs.GetReportDefinition(reportPath);
                        break; // break foreach loop
                    }
                }

            }
            catch (Exception ex)
            {
                //  Console.WriteLine("Exception occured: " + ex.Message.ToString());
            }

            return reportDefinition;

        }

        public string uploadReport(string filepath, byte[] newReport)
        {
            string result = "0";

            try
            {

                wsReportService2005.ReportingService2005 rs = new wsReportService2005.ReportingService2005();
                rs.Credentials = GetCredentials();


                XmlDocument doc = new XmlDocument();

                wsReportService2005.Warning[] warning = rs.SetReportDefinition(filepath, newReport);

                result = "1";

            }
            catch (Exception a1)
            { }


            return result;
        }

        #endregion


        #region GetCredentials
        public System.Net.ICredentials GetCredentials()
        {
            //--------------------------------------------------
            ////temp code - to be removed once the proper authentication for reporting service is done
            //string PwString = "password";
            //char[] PasswordChars = PwString.ToCharArray();
            //System.Security.SecureString Password = new System.Security.SecureString();
            //foreach (char c in PasswordChars) 
            //    Password.AppendChar(c);
            //System.Net.ICredentials cred = (System.Net.ICredentials)new System.Net.NetworkCredential("sudhakar.j", Password, "gavsin");
            //----------------------------


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<System.Net.ICredentials>(() =>
            {
                System.Net.ICredentials cred = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<System.Net.ICredentials>(() =>
                    {
                        if (Properties.Settings.Default.IsIntegratedSecurity)
                        {
                            return System.Net.CredentialCache.DefaultCredentials;
                        }
                        else
                        {
                            //Credentials from Config
                            #region Get credentials from Config file

                            string strAccount, strDomain, strPWD = string.Empty;

                            //----------------------------
                            bool boolIsDomain = false;
                            boolIsDomain = Properties.Settings.Default.IsDomain;
                            if (!boolIsDomain)
                            {
                                //Don't read domain name from config
                                strAccount = Properties.Settings.Default.ReportServerAccount;
                                strDomain = "";
                                strPWD = Properties.Settings.Default.ReportServerPwd;

                                char[] PasswordChars = strPWD.ToCharArray();
                                System.Security.SecureString Password = new System.Security.SecureString();
                                foreach (char c in PasswordChars) Password.AppendChar(c);
                                cred = (System.Net.ICredentials)new System.Net.NetworkCredential(strAccount, Password, strDomain);

                                return cred;
                            }
                            else
                            {
                                strAccount = Properties.Settings.Default.ReportServerAccount;
                                strDomain = Properties.Settings.Default.ReportServerDomain;
                                strPWD = Properties.Settings.Default.ReportServerPwd;

                                char[] PasswordChars = strPWD.ToCharArray();
                                System.Security.SecureString Password = new System.Security.SecureString();
                                foreach (char c in PasswordChars) Password.AppendChar(c);
                                cred = (System.Net.ICredentials)new System.Net.NetworkCredential(strAccount, Password, strDomain);


                            }

                            #endregion

                        }
                        return cred;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

    }
}
