﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using FlightPak.Framework.Encryption;
using System.IO;
using System.Collections;
using System.Xml;
using System.Security;
using FlightPak.Business.Report;

namespace ReportServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportsSecurableService" in code, svc and config file together.
    [ServiceBehavior]
    [XmlSerializerFormat]
    public class ReportsSecurableService : IReportsSecurableService
    {
        public ReportResponse GetDecryptedData(ReportRequest request)
        {
            return new DecryptionManager().GetDecryptedData(request);
        }

        public ReportResponse GetPartialDecryptedData(ReportPartialRequest request)
        {
            return new DecryptionManager().GetPartialDecryptedData(request);
        }
       
    }


    


}
