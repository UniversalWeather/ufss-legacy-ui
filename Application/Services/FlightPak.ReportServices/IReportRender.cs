﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ReportServices.wsReportExecution2005;

namespace ReportServices
{
    [ServiceContract]
    public interface IReportRender
    {
        [OperationContract]
        Byte[] GetReportData(string strReportPath, string strRptFormat, List<ReportParameter> rptUserParameters);

        [OperationContract]
        List<ReportParameter> GetReportParameters(string strReportPath);
    }

    [DataContract]
    public class ReportParameter
    {
        [DataMember]
        public String ParamName { get; set; }

        [DataMember]
        public String ParamValue { get; set; }

        [DataMember]
        public String ParamType { get; set; }
    }
}
