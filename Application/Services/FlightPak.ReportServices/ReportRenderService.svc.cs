﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ReportServices.wsReportExecution2005;
using System.Configuration;
using ReportServices.Base;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace ReportServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ReportRenderService : BaseService, IReportRender
    {
        private ExceptionManager exManager;
        #region GetParamValue
        /// <summary>
        /// Find and return the param value from param collection for the given param name
        /// </summary>
        /// <param name="strParamName">Parameter name for which parameter value to be extracted</param>
        /// <param name="rptUserParameters">Collection of report parameters</param>
        /// <returns>Param value from param collection for the given param name</returns>
        public string GetParamValue(string strParamName, List<ReportParameter> rptUserParameters)
        {


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<string>(() =>
                    {
                        var matchParam = (from prm in rptUserParameters
                                          where prm.ParamName == strParamName
                                          select prm).Single<ReportParameter>();

                        if (matchParam != null)
                        {
                            return matchParam.ParamValue;
                        }
                        else
                        {
                            throw new Exception("Data not supplied for the parameter " + strParamName);
                        }
                        return string.Empty;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

            //return (matchParam != null ? matchParam.ParamValue : "");
        }
        #endregion

        #region Get Report tParameters from the report server
        /// <summary>
        /// Retrieve the report paramters from the report server and returns parameter collection
        /// </summary>
        /// <param name="strReportPath">Complete report path</param>
        /// <returns>List of parameters for the specific report</returns>
        public List<ReportParameter> GetReportParameters(string strReportPathOrig)
        {
            List<ReportParameter> ListPrams = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<List<ReportParameter>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strReportPathOrig))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<List<ReportParameter>>(() =>
                    {
                        //Getting reports route folder name
                        string strReportPath = "/" + Properties.Settings.Default.SSRSReportsFolder + strReportPathOrig;
                        

                        //ReportParameterResponse response = new ReportParameterResponse();
                        ListPrams = new List<ReportParameter>();

                        ReportExecutionService rs = new ReportExecutionService();
                        //rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        rs.Credentials = GetCredentials();

                        string historyID = null;
                        ExecutionInfo execInfo = new ExecutionInfo();

                        //Retrieving report details from the ReportServer for the given report name
                        execInfo = rs.LoadReport(strReportPath, historyID);

                        //IF this report requires any parameters
                        //if (execInfo.ParametersRequired)
                        //{
                        wsReportExecution2005.ReportParameter[] rptServerParameters = execInfo.Parameters;
                        if (rptServerParameters.Length > 0)
                        {
                            foreach (wsReportExecution2005.ReportParameter rp in rptServerParameters)
                            {
                                //to return the list of report parameters to the client
                                ListPrams.Add(new ReportParameter { ParamName = rp.Name, ParamValue = "", ParamType = rp.Type.ToString() });
                            }
                        }
                        //}

                        //response.ReportParameters = ListPrams;
                        //return response;
                        return ListPrams;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }
        #endregion

        #region Render report from the server
        public Byte[] GetReportData(string strReportPathOrig, string strRptFormat, List<ReportParameter> rptUserParameters)
        {
            // Render arguments
            Byte[] reportStream = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Byte[]>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strReportPathOrig, strRptFormat, rptUserParameters))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<Byte[]>(() =>
                    {
                        //Getting reports route folder name
                        string strReportPath = "/" + Properties.Settings.Default.SSRSReportsFolder + strReportPathOrig;
                        
                        ReportExecutionService rs = new ReportExecutionService();
                        //rs.Credentials = System.Net.CredentialCache.DefaultCredentials;
                        rs.Credentials = GetCredentials();

                        //rs.Url = ConfigurationManager.AppSettings["urlReportExecution2005"].ToString();


                        //strReportPath = @"/FlightPakReports/RptDBAccounts";

                        string historyID = null;
                        string devInfo = @"<DeviceInfo><Toolbar>True</Toolbar></DeviceInfo>";

                        string encoding;
                        string mimeType;
                        string extension;
                        Warning[] warnings = null;
                        string[] streamIDs = null;
                        string SessionId = string.Empty;
                        ExecutionInfo execInfo = new ExecutionInfo();
                        ExecutionHeader execHeader = new ExecutionHeader();


                        rs.ExecutionHeaderValue = execHeader;
                        //Retrieve report details
                        execInfo = rs.LoadReport(strReportPath, historyID);

                        //set parameters for the report if requied
                        //if (execInfo.ParametersRequired)
                        //{
                        if (rptUserParameters == null || rptUserParameters.Count < 1)
                            throw new Exception("Parameters are required for this report");

                        wsReportExecution2005.ReportParameter[] rptServerParameters = execInfo.Parameters;
                        if (rptServerParameters.Length > 0)
                        {
                            ParameterValue[] setParameters = new ParameterValue[rptServerParameters.Length];
                            int intTemp = 0;
                            foreach (wsReportExecution2005.ReportParameter rp in rptServerParameters)
                            {
                                // assigning report parameters

                                setParameters[intTemp] = new ParameterValue();
                                setParameters[intTemp].Name = rp.Name;
                                setParameters[intTemp].Value = GetParamValue(rp.Name, rptUserParameters);

                                intTemp++;
                            }
                            rs.SetExecutionParameters(setParameters, "");
                        }
                        //}

                        SessionId = rs.ExecutionHeaderValue.ExecutionID;
                        rs.Timeout = 1200000;
                        reportStream = rs.Render(strRptFormat, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);

                        return reportStream;

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region GetCredentials
        public System.Net.ICredentials GetCredentials()
        {
            //--------------------------------------------------
            ////temp code - to be removed once the proper authentication for reporting service is done
            //string PwString = "password";
            //char[] PasswordChars = PwString.ToCharArray();
            //System.Security.SecureString Password = new System.Security.SecureString();
            //foreach (char c in PasswordChars) 
            //    Password.AppendChar(c);
            //System.Net.ICredentials cred = (System.Net.ICredentials)new System.Net.NetworkCredential("sudhakar.j", Password, "gavsin");
            //----------------------------


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<System.Net.ICredentials>(() =>
            {
                System.Net.ICredentials cred = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<System.Net.ICredentials>(() =>
                    {
                        if (Properties.Settings.Default.IsIntegratedSecurity)
                        {
                            return System.Net.CredentialCache.DefaultCredentials;
                        }
                        else
                        {
                            //Credentials from Config
                            #region Get credentials from Config file

                            string strAccount, strDomain, strPWD = string.Empty;

                            //----------------------------
                            bool boolIsDomain = false;
                            boolIsDomain = Properties.Settings.Default.IsDomain;
                            if (!boolIsDomain)
                            {
                                //Don't read domain name from config
                                strAccount = Properties.Settings.Default.ReportServerAccount;
                                strDomain = "";
                                strPWD = Properties.Settings.Default.ReportServerPwd;

                                char[] PasswordChars = strPWD.ToCharArray();
                                System.Security.SecureString Password = new System.Security.SecureString();
                                foreach (char c in PasswordChars) Password.AppendChar(c);
                                cred = (System.Net.ICredentials)new System.Net.NetworkCredential(strAccount, Password, strDomain);

                                return cred;
                            }
                            else
                            {
                                strAccount = Properties.Settings.Default.ReportServerAccount;
                                strDomain = Properties.Settings.Default.ReportServerDomain;
                                strPWD = Properties.Settings.Default.ReportServerPwd;

                                char[] PasswordChars = strPWD.ToCharArray();
                                System.Security.SecureString Password = new System.Security.SecureString();
                                foreach (char c in PasswordChars) Password.AppendChar(c);
                                cred = (System.Net.ICredentials)new System.Net.NetworkCredential(strAccount, Password, strDomain);


                            }

                            #endregion

                        }
                        return cred;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

    }
}
