﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.ServiceModel.Web;
using FlightPak.Business.Report;

namespace ReportServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReportsSecurableService" in both code and config file together.
    [ServiceContract(Namespace = "http://localhost/UWA/ReportsSecurableService")]
    public interface IReportsSecurableService
    {
        [OperationContract]
        ReportResponse GetDecryptedData(ReportRequest request);
        [OperationContract]
        ReportResponse GetPartialDecryptedData(ReportPartialRequest request);
    }
}
