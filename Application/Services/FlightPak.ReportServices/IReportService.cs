﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ReportServices.wsReportService2005;
using System.Xml;



namespace ReportServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReportService" in both code and config file together.
    [ServiceContract]
    public interface IReportService
    {
        [OperationContract]
        byte[] GetReport(string filePath);

        [OperationContract]
        string uploadReport(string filePath, byte[] newReport);
    }

    [DataContract]
    public class ReportServiceData
    {
        [DataMember]
        public byte[] newReport { get; set; }
    }

}
