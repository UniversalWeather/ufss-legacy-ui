﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Business.Common;
using System.Data.Objects.DataClasses;
using FlightPak.Services.Utility;
using System.Collections.ObjectModel;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICommonService" in both code and config file together.
    [ServiceContract]
    public interface ICommonService
    {
        [OperationContract]
        void LockCustomer();
        [OperationContract]
        void UnlockCustomer();
        [OperationContract]
        ReturnValue<EntityObject> Lock(string entitySetName, Int64 primaryKey);
        [OperationContract]
        ReturnValue<EntityObject> UnLock(string entitySetName, Int64 primaryKey);
        [OperationContract]
        Nullable<Int64> GetSequenceNumber(string ModuleName);
        [OperationContract]
        ReturnValue<Data.Common.GetAllLocks> GetAllLocks();
        [OperationContract]
        ReturnValue<Data.Common.GetAllLocks> DeleteLock(Int64 FPLockID);
        [OperationContract]
        ReturnValue<Data.Admin.GetAllLoggedUsersByCustomerId> GetAllLoggedInUsers(bool includeCurrentUser);
        [OperationContract]
        void LogOutAllUsers(string UserNames);

        #region ScheduleCalendar Default View

        [OperationContract]
        string GetUserDefaultCalendarView();

        [OperationContract]
        bool UpdateCalendarDefaultView(string viewMode);

        # endregion
        #region Security
        [OperationContract]
        void LogOut();
        #endregion

        #region System Administration

        [OperationContract]
        bool ChangeCrew(long oldCrewID, long newCrewID, string crewCD);

        [OperationContract]
        bool ChangeAirport(string oldIcaoId, string newIcaoId);

        [OperationContract]
        bool ChangeFleet(long oldFleetId, string newTailNum, DateTime? effectiveDTTM);

        [OperationContract]
        bool ChangePassenger(long oldPassengerID, long newPassengerID, string paxCD);

        [OperationContract]
        void RetrievePreflightTrip(long tripId);

        [OperationContract]
        bool ChangeAccountNumber(long oldAccountID, long newAccountID, string oldAccountNum, string newAccountNum);

        [OperationContract]
        ReturnValue<Data.Common.GetCrewPAXCleanUpList> GetCrewPAXCleanUpList(string orderBy, bool showInactive, DateTime reportTime);

        [OperationContract]
        ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp> GetInactivateCrewPAXForCleanUp(string orderBy, bool showInactive, DateTime reportTime);

        [OperationContract]
        bool ChangeCQCustomerCD(long oldCQCUstomerID, string newCQCUstomerCD, DateTime? effectiveDTTM);
        #endregion


        #region Utilities - User Preferences

        [OperationContract]
        ReturnValue<Data.Common.GetUserPreferenceUtilitiesList> GetUserPreferenceList(Int64 userGroupID);

        [OperationContract]
        bool AddUserPreference(string categoryName, string subCategoryName, string keyName, string keyValue);

        [OperationContract]
        void DeleteUserPreference();

        #endregion

        #region Recent Activity Manager
        [OperationContract]
        Business.Common.ReturnValue<Data.Common.GetRecentActivityPreflight> GetRecentActivityPreflight(int MaxRecordCount);
        [OperationContract]
        Business.Common.ReturnValue<Data.Common.GetRecentActivityPostflight> GetRecentActivityPostflight(int MaxRecordCount);
        [OperationContract]
        Business.Common.ReturnValue<Data.Common.GetRecentActivityCorporateRequest> GetRecentActivityCorporateRequest(int MaxRecordCount);
        [OperationContract]
        Business.Common.ReturnValue<Data.Common.GetRecentActivityCharterQuote> GetRecentActivityCharterQuote(int MaxRecordCount);
        #endregion

        #region "User Preference - Bookmarks"
        [OperationContract]
        Business.Common.ReturnValue<Data.Common.GetBookmark> GetBookmark(Data.Common.UserPreference oUserPreference);
        [OperationContract]
        Business.Common.ReturnValue<Data.Common.UserPreference> AddBookmark(Data.Common.UserPreference oUserPreference);
        #endregion "User Preference - Bookmarks"

        [OperationContract]
        Wrappers.SCWrapper GetSchedulingCalendarModal();
        //bool homebaseOnly, DateTime startDate, DateTime endDate, FilterCriteria filterCriteria,
        //    Collection<string> fleetTreeInput, bool retrievePaxDetails, bool isStandByCrew, bool showRecordStartsInDateRange,
        //    FlightPak.Services.Utility.VendorOption vendorOption, Collection<string> fleetIds, string homebaseId
        [OperationContract]
        Wrappers.SCPlannerWrapper GetPlannerModal(bool homebaseOnly);
    }


}
