﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Xml;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPreflightService" in both code and config file together.
    [ServiceContract(Namespace = "http://myservice.com")] // This will be changed are moved to web.config file - Sathish
    public interface IAdminService
    {

        #region "Customer Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Customer> AddCustomer(Data.Admin.Customer Customer);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Customer> UpdateCustomer(Data.Admin.Customer customer, int customerModuleCount);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Customer> DeleteCustomer(Data.Admin.Customer Customer);


        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetAllCustomersLicence> GetCustomerList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetAllAirport> GetAirportMasterInfo(Int64 customerID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> ValidateUser(string userName);
        [OperationContract]
        Int64 GetUWACustomerId();
        [OperationContract]
        Int64? GetActualUserCount(Int64 customerId);
        [OperationContract]
        Int64? GetAircraftUsedCount(Int64 customerId);
        #endregion

        #region "UserGroup Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> AddUserGroup(Data.Admin.UserGroup userGroup);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> UpdateUserGroup(Data.Admin.UserGroup userGroup, List<Data.Admin.UserGroupPermission> dtUpdatePermission, int maintainGroupModuleCount);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> DeleteUserGroup(Data.Admin.UserGroup userGroup);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupLock(Data.Admin.UserGroup usergroup);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetUserGroupPermission> GetUserGroupPermission(Int64 UserGroupID, Int64 CustomerID);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupList(Int64 CustomerID);

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupByGroupCode(string userGroupCode);

        #endregion

        #region "User Master Manager"
        [OperationContract]
        Int64? GetUsedUserCount(Int64 customerId);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> AddUserMaster(Data.Admin.UserMaster UserMaster, string password, bool isClientCode = false);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> UpdateUserMaster(Data.Admin.UserMaster UserMaster, int UserGroupMappingCount, bool isClientCode = false);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> DeleteUserMaster(Data.Admin.UserMaster UserMaster);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> GetUserMasterLock(Data.Admin.UserMaster UserMaster);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetAllUserMasterResult> GetUserMasterList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> ResetPassword(string userName, string answer, string hashPassword);
        [OperationContract]
        bool SendLockedEmailToUser(string userName);
        [OperationContract]
        bool ChangePassword(string oldPassword, string newPassword);
        [OperationContract]
        bool ChangePasswordWithPeriod(string oldPassword, string newPassword, int resetPeriod);
        [OperationContract]
        bool CheckResetpasswordPeriod(string emailId);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetUserMasterClient> GetUserMasterClient();

        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetPasswordHistory> GetPasswordHistory(string UserName);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> GetUserByUserName(string username);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> ResetPasswordWithoutAuth(string userName, string answer, string hashPassword, string emailId);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetUserByEmailId> GetUserByEMailId(string emailId);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserMaster> LockOrUnlockUser(string userName, bool isUserLock);

        #endregion

        //#region "Customer Licence"

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.CustomerLicence> AddCustomerLicence(Data.Admin.CustomerLicence Customer);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.CustomerLicence> UpdateCustomerLicence(Data.Admin.CustomerLicence customer);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.CustomerLicence> DeleteCustomerLicence(Data.Admin.CustomerLicence Customer);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.CustomerLicence> GetCustomerLicenceLock(Data.Admin.CustomerLicence Customer);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.CustomerLicence> GetCustomerLicenceList();

        //#endregion

        #region "Customer Module Accesses"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetCustomerModuleAccesses> GetCustomerModuleAccessList(Int64 CustomerID);


        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.Customer> AddCustomerModuleAccess(Data.Admin.Customer Customer);
        //#endregion

        //#region "Customer Module"
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.GetAllModules> GetModuleList();



        #endregion

        #region "User Group Mappings "
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupAvailableList(string UserName, Int64 CustomerID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupSelectedList(string UserName, Int64 CustomerID);
        #endregion

        //#region "UserGroup"
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> AddUserGroup(Data.Admin.UserGroup userGroup);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> UpdateUserGroup(Data.Admin.UserGroup userGroup, int maintainGroupModuleCount);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> DeleteUserGroup(Data.Admin.UserGroup UserGroup);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupLock(Data.Admin.UserGroup UserGroup);
        ////[OperationContract]
        ////Business.Common.ReturnValue<Data.Admin.GetUserGroupPermissions> GetUserGroupPermissionsList(string UserGroupCD, int CustomerID);
        //#endregion



        //#region "Customer Manager"
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> AddUserGroup(Data.Admin.UserGroup userGroup);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> UpdateUserGroup(Data.Admin.UserGroup userGroup);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> DeleteUserGroup(Data.Admin.UserGroup Customer);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupLock(Data.Admin.UserGroup usergroup);

        //    [OperationContract]
        //Business.Common.ReturnValue<Data.Admin.GetUserGroupPermissions> GetUserGroupPermissionsList(string UserGroupCD, Guid CustomerID);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupList(int CustomerID);



        //#endregion

        #region "DSTRegionMaster"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.DSTRegion> AddDSTRegion(Data.Admin.DSTRegion DSTRegionType);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.DSTRegion> GetDSTRegionList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.DSTRegion> UpdateDSTRegion(Data.Admin.DSTRegion DSTRegionType);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.DSTRegion> DeleteDSTRegion(Data.Admin.DSTRegion DSTRegionType);
        #endregion

        #region "Country Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Country> AddCountryMaster(Data.Admin.Country Country);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Country> UpdateCountryMaster(Data.Admin.Country Country);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Country> GetCountryMasterList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.Country> DeleteCountryMaster(Data.Admin.Country Country);
        #endregion

        #region "Help Content"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpContent> AddHelpContent(Data.Admin.HelpContent oHelpContent);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpContent> DeleteHelpContent(Data.Admin.HelpContent oHelpContent);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpContent> UpdateHelpContent(Data.Admin.HelpContent oHelpContent);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpContent> GetHelpContentList(Data.Admin.HelpContent oHelpContent);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.GetHelpContent> GetHelpContent(Data.Admin.HelpContent oHelpContent);
        #endregion "Help Content"

        #region "Help Items"
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpItem> AddHelpItems(Data.Admin.HelpItem oHelpItem);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpItem> DeleteHelpItems(Data.Admin.HelpItem oHelpItem);
        [OperationContract]
        Business.Common.ReturnValue<Data.Admin.HelpItem> GetHelpList(Data.Admin.HelpItem oHelpItem);
        [OperationContract]
        bool UpdateHelpItems(string strColumn, string strValue, string ItemID);
        #endregion "Help Items"

        #region Trial Customer
        [OperationContract]
        string ProcessTrialCustomer(long CustomerId, string document);
        #endregion
        #region "Fuel File XSLT Parser"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> AddFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> UpdateFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> DeleteFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetAllXsltParser();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> AddFuelFileCsvHeaders(string[] headers, int xsltParserId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetXsltParserById(int xsltParserId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> DeleteOldCsvFieldsBasedOnXsltParserId(int xsltParserId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> AddFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> UpdateFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendor();

        [OperationContract]
        Business.Common.ReturnValue<FuelFileData> CheckFileDataByDataformat(FuelFileData objFuelFileData);

        #endregion
        #region "Fuel Vendor Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> AddFuelVendor(Data.MasterCatalog.FuelVendor fuelVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> UpdateFuelVendor(Data.MasterCatalog.FuelVendor fuelVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> DeleteFuelVendor(long? fuelVendorId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorInfoByFilter(long? fuelVendorId, string fuelVendorCode);

        #endregion
        #region "Customer Fuel Vendor Access"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccessList> GetCustomerFuelVendorAccessList(long? customerId);

        #endregion
    }
}