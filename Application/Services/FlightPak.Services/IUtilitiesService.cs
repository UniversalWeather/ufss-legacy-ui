﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUtilitiesService" in both code and config file together.
    [ServiceContract(Namespace = "http://myservice.com")] // This will be changed are moved to web.config file - Sathish
    public interface IUtilitiesService
    {
        #region "Airport Pairs"
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPair> AddAirportPairs(Data.Utilities.AirportPair oAirportPairs);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPair> UpdateAirportPairs(Data.Utilities.AirportPair oAirportPairs);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPair> DeleteAirportPairs(Data.Utilities.AirportPair oAirportPairs);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPair> GetAirportPairsList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetAirportPair> GetAirportPairs(Data.Utilities.AirportPair oAirportPairs);
        #endregion

        #region "Airport Pairs Details"
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> AddAirportPairsDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> UpdateAirportPairsDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> DeleteAirportPairsDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> GetAirportPairDetailList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetail> GetAirportPairDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber> GetAirportPairDetailByPairNumber(Data.Utilities.AirportPairDetail oAirportPairsDetail);
        #endregion

        #region "World Winds"
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetWorldWindAirRoutes> GetWorldWindAirRoutes(Int64 DepartureAirportID, Int64 ArrivalAirportID);
        #endregion

        #region "Itinerary Plan"
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> AddItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> DeleteItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> UpdateItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> GetItineraryPlanList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetItineraryPlan> GetItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> UpdateItineraryPlanTransfer(Data.Utilities.ItineraryPlan oItineraryPlan);
        #endregion

        #region "Itinerary Plan Details"
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> AddItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> DeleteItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> UpdateItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> GetItineraryPlanDetailList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetail> GetItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID> GetItineraryPlanDetailByItineraryPlanID(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail);
        #endregion

        #region "World Clock"
        [OperationContract]
        Boolean UpdateWorldClock(string AirportID, bool IsWorldClock);

        [OperationContract]
        Boolean DeleteWorldClock(long AirportID);

        [OperationContract]
        Boolean AddWorldClock(long AirportID, bool IsWorldClock);

        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetAllAirportForWorldClock> GetAllAirportForWorldClock();
        #endregion

        #region Locator
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetLocatorAirport> GetAllLocatorAirport(Int64 airportID, string icaoID, string iata, string countryCD, Int64 countryID, decimal longestRunway, string cityName, Int64 metroID, string metroCD, int milesFrom, string stateName, string airportName, bool isHeliport, bool isEntryPort, bool isInActive);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetLocatorCustom> GetAllLocatorCustom(Int64 airportID, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string name, int milesFrom);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetLocatorHotel> GetAllLocatorHotel(Int64 airportID, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string hotelName, int milesFrom);
        [OperationContract]
        Business.Common.ReturnValue<Data.Utilities.GetLocatorVendor> GetAllLocatorVendor(Int64 airportid, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string name, int milesFrom, Int64 aircraftID, string aircraftCD);
        #endregion


    }
}
