﻿using System;
using System.ServiceModel;
using FlightPak.Business.Common;
using FlightPak.Business.CharterQuote;
using FlightPak.Data.CharterQuote;
using System.Configuration;
using FlightPak.Services.Utility;
using FlightPak.Business.Preflight.ScheduleCalendarEntities;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace FlightPak.Services
{

    public class CharterQuoteService : BaseService,ICharterQuoteService
    {
        private ExceptionManager exManager;
        /// <summary>
        /// Method Get Charter Quote Request by CQMainID
        /// </summary>
        /// <param name="CQMainID">Pass CQMainID</param>
        /// <returns>Charter Quote Request Object</returns>
        public ReturnValue<CQFile> GetCQRequestByID(Int64 CQFileID, bool isExpressQuote)
        {
            ReturnValue<Data.CharterQuote.CQFile> objMain = null;
            CharterQuoteManager CQMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, isExpressQuote))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.GetCQRequestByID(CQFileID, isExpressQuote);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Method Get Charter Quote Request by CQFileNum
        /// </summary>
        /// <param name="CQMainID">Pass CQMainID</param>
        /// <returns>Charter Quote Request Object</returns>
        public ReturnValue<CQFile> GetCQRequestByFileNum(Int64 CQFileNum, bool isExpressQuote)
        {
            ReturnValue<Data.CharterQuote.CQFile> objMain = null;
            CharterQuoteManager CQMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileNum, isExpressQuote))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.GetCQRequestByFileNum(CQFileNum, isExpressQuote);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Method to Save Charter Quote Request 
        /// </summary>
        /// <param name="CQFile">CQFile/param>
        /// <returns>Charter Quote File Request Object</returns>
        public ReturnValue<Data.CharterQuote.CQFile> SaveFileRequest(CQFile cqFile)
        {
            ReturnValue<Data.CharterQuote.CQFile> objMain = null;
            CharterQuoteManager CQMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqFile))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.UpdateFile(cqFile);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #region "Quote Report"
        /// <summary>
        /// Method Get Quote report  by CQMainID
        /// </summary>
        /// <param name="CQMainID">Pass CQMainID</param>
        /// <returns>Quote Report Object</returns>
        public ReturnValue<CQQuoteMain> GetCQQuoteMainByCQMainID(Int64 CQMainID)
        {
            ReturnValue<Data.CharterQuote.CQQuoteMain> objMain = null;
            CharterQuoteManager CQMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQQuoteMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.GetCQQuoteMainByCQMainID(CQMainID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        /// <summary>
        /// Method to Save  Quote Report 
        /// </summary>
        /// <param name="CQFile">CQQuoteMain/param>
        /// <returns>Quote Report Object</returns>
        public ReturnValue<Data.CharterQuote.CQQuoteMain> SaveQuoteReport(CQQuoteMain cqQuoteMain)
        {
            ReturnValue<Data.CharterQuote.CQQuoteMain> objMain = null;
            CharterQuoteManager CQMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQQuoteMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqQuoteMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.UpdateCQQouteMain(cqQuoteMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion


        #region "Invoice Report"
        /// <summary>
        /// Method Get Invoice report  by CQMainID
        /// </summary>
        /// <param name="CQMainID">Pass CQMainID</param>
        /// <returns>Invoice Report Object</returns>
        public ReturnValue<CQInvoiceMain> GetCQInvoiceMainByCQMainID(Int64 CQMainID)
        {
            ReturnValue<Data.CharterQuote.CQInvoiceMain> objMain = null;
            CharterQuoteManager CQMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQInvoiceMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.GetCQInvoiceMainByCQMainID(CQMainID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        /// <summary>
        /// Method to Save Invoice Report 
        /// </summary>
        /// <param name="CQFile">CQInvoiceMain/param>
        /// <returns>Invoice Report Object</returns>
        public ReturnValue<Data.CharterQuote.CQInvoiceMain> SaveInvoiceReport(CQInvoiceMain cqInvoiceMain)
        {
            ReturnValue<Data.CharterQuote.CQInvoiceMain> objMain = null;
            CharterQuoteManager CQMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQInvoiceMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqInvoiceMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.UpdateCQInvoiceMain(cqInvoiceMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion


        #region "CharterQuote Pax"
        public ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList> GetAllCharterQuoteAvailablePaxList()
        {
            Business.Common.ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList> objPax = null;
            CharterQuoteManager CharterQuoteMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CharterQuoteMngr = new CharterQuoteManager();
                        objPax = CharterQuoteMngr.GetAllCharterQuoteAvailablePaxList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPax;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        public ReturnValue<View_CharterQuoteMainList> GetAllCQMainList(Int64 homebaseID, DateTime EstDepartDate, bool expressQuote, Int64 leadSourceID, string customerType, Int64 salesPersonID)
        {
            CharterQuoteManager CqMngr = null;

            Business.Common.ReturnValue<Data.CharterQuote.View_CharterQuoteMainList> objMain = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<View_CharterQuoteMainList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseID, EstDepartDate, expressQuote, leadSourceID, customerType, salesPersonID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CqMngr = new CharterQuoteManager();
                        objMain = CqMngr.GetAllCQMainList(homebaseID, EstDepartDate, expressQuote, leadSourceID, customerType, salesPersonID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }


        public ReturnValue<ExceptionTemplate> GetCQBusinessErrorsList()
        {
            CharterQuoteValidationManager CqMngr = null;

            Business.Common.ReturnValue<Data.CharterQuote.ExceptionTemplate> objException = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<ExceptionTemplate>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    objException = new ReturnValue<ExceptionTemplate>();
                    CqMngr = new CharterQuoteValidationManager();
                    objException = CqMngr.GetCQBusinessErrorsList((long)CQBusinessErrorModule.CharterQuote);
                }
                return objException;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }
        public ReturnValue<CQException> GetQuoteExceptionList(Int64 cqMainID)
        {
            CharterQuoteManager CqMngr = null;

            Business.Common.ReturnValue<Data.CharterQuote.CQException> objValidateTrip = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQException>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    objValidateTrip = new ReturnValue<CQException>();
                    CqMngr = new CharterQuoteManager();
                    objValidateTrip = CqMngr.GetQuoteExceptionList(cqMainID);
                }
                return objValidateTrip;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public ReturnValue<CQException> ValidateQuoteforMandatoryExcep(CQMain cqMain)
        {
            CharterQuoteManager CqMngr = null;

            Business.Common.ReturnValue<Data.CharterQuote.CQException> objValidateTrip = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQException>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    objValidateTrip = new ReturnValue<CQException>();
                    CqMngr = new CharterQuoteManager();
                    objValidateTrip = CqMngr.ValidateQuoteforMandatoryExcep(cqMain);
                }
                return objValidateTrip;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        //public ReturnValue<Data.CharterQuote.CQLostBusiness> GetLostBusinessInfo()
        //{
        //    //Handle methods throguh exception manager with return flag
        //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //    return exManager.Process<ReturnValue<Data.CharterQuote.CQLostBusiness>>(() =>
        //    {
        //        Business.Common.ReturnValue<Data.CharterQuote.CQLostBusiness> retValue = null;
        //        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                FlightPak.Business.CharterQuote.CharterQuoteManager objLostBusiness = new FlightPak.Business.CharterQuote.CharterQuoteManager();
        //                retValue = objLostBusiness.GetLostBusinessInfo();
        //            }, FlightPak.Common.Constants.Policy.BusinessLayer);
        //        }
        //        return retValue;
        //    }, FlightPak.Common.Constants.Policy.ServiceLayer);
        //}

        public ReturnValue<CQFile> DeleteFile(Int64 cqFileID)
        {
            Business.Common.ReturnValue<CQFile> objFile = null;
            CharterQuoteManager CharterQuoteMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqFileID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CharterQuoteMngr = new CharterQuoteManager();
                        objFile = CharterQuoteMngr.DeleteFile(cqFileID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFile;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<Data.CharterQuote.CQHistory> GetHistory(long CQFileID)
        {
            Business.Common.ReturnValue<Data.CharterQuote.CQHistory> objCQMains = null;
            CharterQuoteManager charterQuoteMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CharterQuote.CQHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        charterQuoteMngr = new CharterQuoteManager();
                        objCQMains = charterQuoteMngr.GetHistory(CQFileID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCQMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #region "Copy Quote"

        public ReturnValue<CQFile> CopyQuoteDetails(Int64 CQFileID, Int64 CQMainID)
        {

            Business.Common.ReturnValue<CQFile> objMain = null;
            CharterQuoteManager CqMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, CQMainID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CqMngr = new CharterQuoteManager();
                        objMain = CqMngr.GetCQCopyQuoteDetail(CQFileID, CQMainID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion


        #region "Express Quote"
        public ReturnValue<CQFile> CopyExpressQuoteDetails(Int64 CQFileID)
        {

            Business.Common.ReturnValue<CQFile> objMain = null;
            CharterQuoteManager CqMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CqMngr = new CharterQuoteManager();
                        objMain = CqMngr.GetCQExpressCopyQuoteDetail(CQFileID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region "Copy CQ to Preflight"
        public ReturnValue<CQFile> CopyCQToPreflight(Int64 CQFileID, Int64 CQMainID, Boolean IsAutoDispatch, string TripStatus, Boolean IsRevision)
        {

            Business.Common.ReturnValue<CQFile> objMain = null;
            CharterQuoteManager CqMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CQFile>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, CQMainID, IsAutoDispatch, TripStatus, IsRevision))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CqMngr = new CharterQuoteManager();
                        objMain = CqMngr.GetCQQuotetoPreflight(CQFileID, CQMainID, IsAutoDispatch, TripStatus, IsRevision);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        public void DeleteQuoteImageFiles(Int64 CQMainID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                CharterQuoteManager charterQuoteMngr = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        charterQuoteMngr = new CharterQuoteManager();
                        charterQuoteMngr.DeleteQuoteImageFiles(CQMainID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #region "Charter Quote Reports"

        public ReturnValue<GetQuoteByFileID> GetQuoteByFileID(Int64 CQFileID)
        {
            ReturnValue<Data.CharterQuote.GetQuoteByFileID> objMain = null;
            CharterQuoteManager CQMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetQuoteByFileID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.GetQuoteByFileID(CQFileID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public ReturnValue<GetCQLegsByQuoteID> GetCQLegsByQuoteID(Int64 CQMainID)
        {
            ReturnValue<Data.CharterQuote.GetCQLegsByQuoteID> objMain = null;
            CharterQuoteManager CQMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetCQLegsByQuoteID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMngr = new CharterQuoteManager();
                        objMain = CQMngr.GetCQLegsByQuoteID(CQMainID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.CharterQuote.GetCQReportDetail> GetCQReportDetail()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.GetCQReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.GetCQReportDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.CharterQuote.CharterQuoteManager CQManager = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        ReturnValue = CQManager.GetCQReportDetail();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.CharterQuote.GetCQReportHeader> GetCQHeaderList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.GetCQReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.GetCQReportHeader> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.CharterQuote.CharterQuoteManager CompanyCatalog = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        ReturnValue = CompanyCatalog.GetCQReportHeaderList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> AddCQReportDetail(Data.CharterQuote.CQReportDetail oCQReportDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportDetail))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oCQReportDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCQReportDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.CharterQuote.CharterQuoteManager objCQReportDetail = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        retValue = objCQReportDetail.Add(oCQReportDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> UpdateCQReportDetail(Data.CharterQuote.CQReportDetail oCQReportDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportDetail))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oCQReportDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCQReportDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.CharterQuote.CharterQuoteManager objCQReportDetail = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        retValue = objCQReportDetail.Update(oCQReportDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> DeleteCQReportDetail(Data.CharterQuote.CQReportDetail oCQReportDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportDetail))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oCQReportDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCQReportDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.CharterQuote.CharterQuoteManager objCQReportDetail = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        retValue = objCQReportDetail.Delete(oCQReportDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> AddCQReportHeader(Data.CharterQuote.CQReportHeader oCQReportHeader)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oCQReportHeader.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCQReportHeader.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.CharterQuote.CharterQuoteManager objCQReportHeader = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        retValue = objCQReportHeader.Add(oCQReportHeader);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> UpdateCQReportHeader(Data.CharterQuote.CQReportHeader oCQReportHeader)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oCQReportHeader.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCQReportHeader.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.CharterQuote.CharterQuoteManager objCQReportHeader = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        retValue = objCQReportHeader.Update(oCQReportHeader);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> DeleteCQReportHeader(Data.CharterQuote.CQReportHeader oCQReportHeader)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oCQReportHeader.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCQReportHeader.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.CharterQuote.CharterQuoteManager objCQReportHeader = new FlightPak.Business.CharterQuote.CharterQuoteManager();
                        retValue = objCQReportHeader.Delete(oCQReportHeader);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        /// <summary>
        /// Method for Getting FileWarehouse Details by ID
        /// Fix for #2983
        /// </summary>
        /// <param name="WareHouseID">Pass FileWarehouseID</param>
        /// <returns>Returns FileWarehouse Details List</returns>
        public ReturnValue<Data.CharterQuote.GetFileWarehouseByID> GetFileWarehouseByID(Int64 WareHouseID)
        {
            CharterQuoteManager CqMngr = null;
            Business.Common.ReturnValue<Data.CharterQuote.GetFileWarehouseByID> objFile = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetFileWarehouseByID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(WareHouseID))
                {
                    objFile = new ReturnValue<GetFileWarehouseByID>();
                    CqMngr = new CharterQuoteManager();
                    objFile = CqMngr.GetFileWarehouseByID(WareHouseID);
                }
                return objFile;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
    }
}
