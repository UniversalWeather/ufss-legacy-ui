﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Configuration;
namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UtilitiesService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class UtilitiesService : BaseService, IUtilitiesService
    {
        private ExceptionManager exManager;

        #region "Airport Pairs"
        public Business.Common.ReturnValue<Data.Utilities.AirportPair> AddAirportPairs(Data.Utilities.AirportPair oAirportPairs)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPair>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPair> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairs.CustomerID = UserPrincipal.Identity.CustomerID;
                        oAirportPairs.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.AirportPairManager AirportPairManager = new FlightPak.Business.Utilities.AirportPairManager();
                        rtnAirportPairs = AirportPairManager.Add(oAirportPairs);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.AirportPair> DeleteAirportPairs(Data.Utilities.AirportPair oAirportPairs)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPair>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPair> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairs.CustomerID = UserPrincipal.Identity.CustomerID;
                        oAirportPairs.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.AirportPairManager AirportPairManager = new FlightPak.Business.Utilities.AirportPairManager();
                        rtnAirportPairs = AirportPairManager.Delete(oAirportPairs);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.AirportPair> UpdateAirportPairs(Data.Utilities.AirportPair oAirportPairs)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPair>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPair> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairs.CustomerID = UserPrincipal.Identity.CustomerID;
                        oAirportPairs.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.AirportPairManager AirportPairManager = new FlightPak.Business.Utilities.AirportPairManager();
                        rtnAirportPairs = AirportPairManager.Update(oAirportPairs);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.AirportPair> GetAirportPairsList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPair>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPair> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.AirportPairManager AirportPairManager = new FlightPak.Business.Utilities.AirportPairManager();
                        rtnAirportPairs = AirportPairManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetAirportPair> GetAirportPairs(Data.Utilities.AirportPair oAirportPairs)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetAirportPair>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetAirportPair> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairs.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.Utilities.AirportPairManager AirportPairManager = new FlightPak.Business.Utilities.AirportPairManager();
                        rtnAirportPairs = AirportPairManager.GetAirportPairs(oAirportPairs);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Airport Pairs Details"
        public Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> AddAirportPairsDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPairDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairsDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oAirportPairsDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.AirportPairsDetailManager AirportPairsDetailManager = new FlightPak.Business.Utilities.AirportPairsDetailManager();
                        rtnAirportPairs = AirportPairsDetailManager.Add(oAirportPairsDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> DeleteAirportPairsDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPairDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairsDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oAirportPairsDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.AirportPairsDetailManager AirportPairsDetailManager = new FlightPak.Business.Utilities.AirportPairsDetailManager();
                        rtnAirportPairs = AirportPairsDetailManager.Delete(oAirportPairsDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> UpdateAirportPairsDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPairDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairsDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oAirportPairsDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.AirportPairsDetailManager AirportPairsDetailManager = new FlightPak.Business.Utilities.AirportPairsDetailManager();
                        rtnAirportPairs = AirportPairsDetailManager.Update(oAirportPairsDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> GetAirportPairDetailList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.AirportPairDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.AirportPairDetail> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.AirportPairsDetailManager AirportPairsDetailManager = new FlightPak.Business.Utilities.AirportPairsDetailManager();
                        rtnAirportPairs = AirportPairsDetailManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetail> GetAirportPairDetail(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetail> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairsDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.Utilities.AirportPairsDetailManager AirportPairsDetailManager = new FlightPak.Business.Utilities.AirportPairsDetailManager();
                        rtnAirportPairs = AirportPairsDetailManager.GetAirportPairsDetail(oAirportPairsDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber> GetAirportPairDetailByPairNumber(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber> rtnAirportPairs = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oAirportPairsDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.Utilities.AirportPairsDetailManager AirportPairsDetailManager = new FlightPak.Business.Utilities.AirportPairsDetailManager();
                        rtnAirportPairs = AirportPairsDetailManager.GetAirportPairDetailByPairNumber(oAirportPairsDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnAirportPairs;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "World Winds"
        public Business.Common.ReturnValue<Data.Utilities.GetWorldWindAirRoutes> GetWorldWindAirRoutes(Int64 DepartureAirportID, Int64 ArrivalAirportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetWorldWindAirRoutes>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetWorldWindAirRoutes> rtnWorldWindAirRoutes = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.WorldWindManager WorldWindManager = new FlightPak.Business.Utilities.WorldWindManager();
                        rtnWorldWindAirRoutes = WorldWindManager.GetWorldWindAirRoutes(DepartureAirportID, ArrivalAirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnWorldWindAirRoutes;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Itinerary Plan"
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> AddItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlan>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> rtnItineraryPlan = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlan.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlan.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanManager ItineraryPlanManager = new FlightPak.Business.Utilities.ItineraryPlanManager();
                        rtnItineraryPlan = ItineraryPlanManager.Add(oItineraryPlan);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlan;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> DeleteItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlan>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> rtnItineraryPlan = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlan.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlan.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanManager ItineraryPlanManager = new FlightPak.Business.Utilities.ItineraryPlanManager();
                        rtnItineraryPlan = ItineraryPlanManager.Delete(oItineraryPlan);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlan;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> UpdateItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlan>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> rtnItineraryPlan = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlan.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlan.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanManager ItineraryPlanManager = new FlightPak.Business.Utilities.ItineraryPlanManager();
                        rtnItineraryPlan = ItineraryPlanManager.Update(oItineraryPlan);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlan;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> GetItineraryPlanList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlan>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> rtnItineraryPlan = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.ItineraryPlanManager ItineraryPlanManager = new FlightPak.Business.Utilities.ItineraryPlanManager();
                        rtnItineraryPlan = ItineraryPlanManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlan;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetItineraryPlan> GetItineraryPlan(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetItineraryPlan>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetItineraryPlan> rtnItineraryPlan = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlan.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.Utilities.ItineraryPlanManager ItineraryPlanManager = new FlightPak.Business.Utilities.ItineraryPlanManager();
                        rtnItineraryPlan = ItineraryPlanManager.GetItineraryPlans(oItineraryPlan);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlan;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> UpdateItineraryPlanTransfer(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlan>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlan> rtnItineraryPlan = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlan.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlan.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanManager ItineraryPlanManager = new FlightPak.Business.Utilities.ItineraryPlanManager();
                        rtnItineraryPlan = ItineraryPlanManager.UpdateItineraryPlanTransfer(oItineraryPlan);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlan;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Itinerary Plan Details"
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> AddItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> rtnItineraryPlanDetail = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlanDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlanDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanDetailManager ItineraryPlanDetailManager = new FlightPak.Business.Utilities.ItineraryPlanDetailManager();
                        rtnItineraryPlanDetail = ItineraryPlanDetailManager.Add(oItineraryPlanDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlanDetail;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> DeleteItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> rtnItineraryPlanDetail = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlanDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlanDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanDetailManager ItineraryPlanDetailManager = new FlightPak.Business.Utilities.ItineraryPlanDetailManager();
                        rtnItineraryPlanDetail = ItineraryPlanDetailManager.Delete(oItineraryPlanDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlanDetail;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> UpdateItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> rtnItineraryPlanDetail = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlanDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oItineraryPlanDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.Utilities.ItineraryPlanDetailManager ItineraryPlanDetailManager = new FlightPak.Business.Utilities.ItineraryPlanDetailManager();
                        rtnItineraryPlanDetail = ItineraryPlanDetailManager.Update(oItineraryPlanDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlanDetail;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> GetItineraryPlanDetailList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.ItineraryPlanDetail> rtnItineraryPlanDetail = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.ItineraryPlanDetailManager ItineraryPlanDetailManager = new FlightPak.Business.Utilities.ItineraryPlanDetailManager();
                        rtnItineraryPlanDetail = ItineraryPlanDetailManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlanDetail;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetail> GetItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetail> rtnItineraryPlanDetail = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlanDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.Utilities.ItineraryPlanDetailManager ItineraryPlanDetailManager = new FlightPak.Business.Utilities.ItineraryPlanDetailManager();
                        rtnItineraryPlanDetail = ItineraryPlanDetailManager.GetItineraryPlanDetail(oItineraryPlanDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlanDetail;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID> GetItineraryPlanDetailByItineraryPlanID(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID> rtnItineraryPlanDetail = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oItineraryPlanDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.Utilities.ItineraryPlanDetailManager ItineraryPlanDetailManager = new FlightPak.Business.Utilities.ItineraryPlanDetailManager();
                        rtnItineraryPlanDetail = ItineraryPlanDetailManager.GetItineraryPlanDetailByItineraryPlanID(oItineraryPlanDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnItineraryPlanDetail;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "World Winds"
        public Boolean AddWorldClock(long AirportID, bool IsWorldClock)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Boolean>(() =>
            {
                Boolean retValue = false;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID, IsWorldClock))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.WorldClockManager objClientCode = new FlightPak.Business.Utilities.WorldClockManager();
                        retValue = objClientCode.AddWorldClock(AirportID, IsWorldClock);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Boolean UpdateWorldClock(string AirportID, bool IsWorldClock)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Boolean>(() =>
            {
                Boolean retValue = false;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID, IsWorldClock))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.WorldClockManager objClientCode = new FlightPak.Business.Utilities.WorldClockManager();
                        retValue = objClientCode.UpdateWorldClock(AirportID, IsWorldClock);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Boolean DeleteWorldClock(long AirportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Boolean>(() =>
            {
                Boolean retValue = false;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.WorldClockManager objClientCode = new FlightPak.Business.Utilities.WorldClockManager();
                        retValue = objClientCode.DeleteWorldClock(AirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Utilities.GetAllAirportForWorldClock> GetAllAirportForWorldClock()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetAllAirportForWorldClock>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetAllAirportForWorldClock> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.WorldClockManager objClientCode = new FlightPak.Business.Utilities.WorldClockManager();
                        ReturnValue = objClientCode.GetAllAirportForWorldClock();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Locator"
        public Business.Common.ReturnValue<Data.Utilities.GetLocatorAirport> GetAllLocatorAirport(Int64 airportID, string icaoID, string iata, string countryCD, Int64 countryID, decimal longestRunway, string cityName, Int64 metroID, string metroCD, int milesFrom, string stateName, string airportName, bool isHeliport, bool isEntryPort, bool isInActive)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetLocatorAirport>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetLocatorAirport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(airportID, icaoID, iata, countryCD, countryID, longestRunway, cityName, metroID, metroCD, milesFrom, stateName, airportName, isHeliport, isEntryPort, isInActive))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.LocatorManager objLoc = new FlightPak.Business.Utilities.LocatorManager();
                        ReturnValue = objLoc.GetAllLocatorAirport(airportID, icaoID, iata, countryCD, countryID, longestRunway, cityName, metroID, metroCD, milesFrom, stateName, airportName, isHeliport, isEntryPort, isInActive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetLocatorHotel> GetAllLocatorHotel(Int64 airportID, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string hotelName, int milesFrom)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetLocatorHotel>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetLocatorHotel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, hotelName, milesFrom))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.LocatorManager objLoc = new FlightPak.Business.Utilities.LocatorManager();
                        ReturnValue = objLoc.GetAllLocatorHotel(airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, hotelName, milesFrom);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetLocatorVendor> GetAllLocatorVendor( Int64 airportid, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string name, int milesFrom, Int64 aircraftID, string aircraftCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetLocatorVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetLocatorVendor> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( airportid, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom, aircraftID, aircraftCD))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                       FlightPak.Business.Utilities.LocatorManager objLoc = new FlightPak.Business.Utilities.LocatorManager();
                       ReturnValue = objLoc.GetAllLocatorVendor(airportid, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom, aircraftID, aircraftCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Utilities.GetLocatorCustom> GetAllLocatorCustom( Int64 airportID, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string name, int milesFrom)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Utilities.GetLocatorCustom>>(() =>
            {
                Business.Common.ReturnValue<Data.Utilities.GetLocatorCustom> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Utilities.LocatorManager objLoc = new FlightPak.Business.Utilities.LocatorManager();
                        ReturnValue = objLoc.GetAllLocatorCustom(airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion




    }
}
