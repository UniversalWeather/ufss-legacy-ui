﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Data.CorporateRequest;
using FlightPak.Business.Common;
using FlightPak.Services.Utility;
using System.Collections.ObjectModel;
using System.Data;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICorporateRequestService" in both code and config file together.
    [ServiceContract]
    public interface ICorporateRequestService
    {
        #region "Request"
        [OperationContract]
        ReturnValue<CRMain> GetRequest(Int64 RequestID);
        [OperationContract]
        ReturnValue<CRMain> Add(CRMain Request);
        [OperationContract]
        ReturnValue<CRMain> Update(CRMain Request);
        [OperationContract]
        ReturnValue<CRMain> Delete(Int64 RequestID);
        #endregion

        #region "Search"
        [OperationContract]
        ReturnValue<View_CorporateRequestMainList> GetAllCorporateRequestMainList(Int64 homebaseID, Int64 clientID, string tripSheet, string worksheetstr, string hold, string cancelled, string schedServ, string Unfulfilled, bool isLog, DateTime EstDepartDate, string homebaseIDCSVStr, Int64 travelID);
        [OperationContract]
        ReturnValue<View_CorporateRequestMainList> GetAllDeletedCorporateRequestMainList();
        [OperationContract]
        ReturnValue<CRMain> GetRequestByTripNum(Int64 CRTripNUM);
        #endregion


        #region "Copy Request"
        [OperationContract]
        ReturnValue<CRMain> CopyRequestDetails(DateTime DepartDate, long crMainID, long customerID, bool IsLogisticsToBeCopied, bool IsPaxToBeCopied, bool IsLegsToBeCopied, bool IsAllToBeCopied);
        #endregion

        #region "Exception list"
        [OperationContract]
        ReturnValue<CRException> ValidateRequestforMandatoryExceptions(CRMain Request);
        [OperationContract]
        ReturnValue<ExceptionTemplate> GetExceptionTemplateList();
        [OperationContract]
        ReturnValue<CRException> GetRequestExceptionList(Int64 CRMainID);
        #endregion

        #region "Retrieve Pax"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList> GetAllCorporateRequestAvailablePaxList();
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.CrewPassengerPassport> GetPassengerPassportByPassportID(Int64 PassportId);
        #endregion


        #region "Change Queue"
        [OperationContract]
        ReturnValue<GetAllCRMain> GetCRChangeQueue();
        [OperationContract]
        ReturnValue<CRMain> QueueTransfer(CRMain oCRMain);
        [OperationContract]
        ReturnValue<CRMain> DenyRequest(CRMain oCRMain);
        [OperationContract]
        ReturnValue<CRMain> AcceptRequest(CRMain oCRMain);
        [OperationContract]
        ReturnValue<CRLeg> ChangePrivate(CRLeg oCRMain);
        [OperationContract]
        ReturnValue<CRMain> ApproveRequest(CRMain oCRMain);
        #endregion


        #region "Submit Request"
        [OperationContract]
        ReturnValue<CRMain> UpdateCRMainCorpStatus(CRMain oCRMain);
        #endregion

        #region "Cancel Request"
        [OperationContract]
        ReturnValue<CRMain> CancelRequest(CRMain oCRMain);
        #endregion

        #region History
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.CRHistory> GetCRHistory(long CustomerId, long CRMainId);
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.CRHistory> UpdateCRHistory(CRHistory History);
        [OperationContract]
        ReturnValue<CRHistory> AddCRHistory(CRHistory oCRHistory);
        #endregion

        #region "Acknowledge Request"
        [OperationContract]
        ReturnValue<CRMain> RequestAcknowledge(long requestID, long tripID, long customerID, string lastUpdUID, DateTime lastUpdTS);
        #endregion

        #region "Travelcoordinator"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.TravelCoordinator> GetTravelcoordinatorbyID(Int64 TravelCoordinatorID);
        #endregion

        #region "Fleet"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.Fleet> GetFleetbyID(Int64 FleetID);
        #endregion


        #region "Aircraft"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.Aircraft> GetAircraftbyID(Int64 AircraftID);
        #endregion


        #region "Passenger"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.Passenger> GetPassengerbyID(Int64 PassengerRequestorID);

        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR> GetCRPaxbyPaxID(Int64 TravelID);

        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.GetAllFleetForCR> GetCRFleet(Int64 TravelID);
        #endregion


        #region "Department"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.Department> GetDepartmentbyID(Int64 DepartmentID);
        #endregion

        #region "Authorization"
        [OperationContract]
        Business.Common.ReturnValue<Data.CorporateRequest.DepartmentAuthorization> GetAuthorizationbyID(Int64 AuthorizationID, Int64 DepartmentID);
        #endregion

    }
}
