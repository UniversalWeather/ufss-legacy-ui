﻿using System;
using System.ServiceModel;
using FlightPak.Business.Common;
using FlightPak.Business.CorporateRequest;
using FlightPak.Data.CorporateRequest;
using System.Configuration;
using FlightPak.Services.Utility;
using FlightPak.Business.Preflight.ScheduleCalendarEntities;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CorporateRequestService" in code, svc and config file together.
    public class CorporateRequestService : ICorporateRequestService
    {
        private ExceptionManager exManager;

        #region "Request"

        public ReturnValue<CRMain> GetRequest(Int64 RequestID)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            CorporateRequestManager CorporateRequestMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RequestID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.GetRequest(RequestID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CRMain> Add(CRMain Request)
        {
            Business.Common.ReturnValue<CRMain> objMain = null;
            CorporateRequestManager CorporateRequestMngr = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Request))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.UpdateRequest(Request);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CRMain> Update(CRMain Request)
        {
            Business.Common.ReturnValue<CRMain> objMain = null;
            CorporateRequestManager CorporateRequestMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Request))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.UpdateRequest(Request);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<CRMain> Delete(Int64 RequestID)
        {
            Business.Common.ReturnValue<CRMain> objMain = null;
            CorporateRequestManager CorporateRequestMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RequestID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.DeleteRequest(RequestID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "Copy Request"

        public ReturnValue<CRMain> CopyRequestDetails(DateTime DepartDate, long tripID, long customerID, bool IsLogisticsToBeCopied, bool IsPaxToBeCopied, bool IsLegsToBeCopied, bool IsAllToBeCopied)
        {

            Business.Common.ReturnValue<CRMain> objMain = null;
            CorporateRequestManager CorpRequestMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, tripID, customerID, IsLogisticsToBeCopied,  IsPaxToBeCopied,  IsLegsToBeCopied,  IsAllToBeCopied))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequestMngr = new CorporateRequestManager();
                        objMain = CorpRequestMngr.CopyRequestDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied,  IsPaxToBeCopied,  IsLegsToBeCopied,  IsAllToBeCopied);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "Exception"
        public ReturnValue<Data.CorporateRequest.CRException> ValidateRequestforMandatoryExceptions(CRMain Request)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.CRException> objValidateTrip = new ReturnValue<CRException>();
            CorporateRequestManager CorpRequestMngr = new CorporateRequestManager();
            objValidateTrip = CorpRequestMngr.ValidateRequestforMandatoryExcep(Request);
            return objValidateTrip;
        }

        public ReturnValue<ExceptionTemplate> GetExceptionTemplateList()
        {
            Business.Common.ReturnValue<ExceptionTemplate> objExceptionList = new ReturnValue<ExceptionTemplate>();
            CorporateRequestManager CorpRequestMngr = new CorporateRequestManager();
            objExceptionList = CorpRequestMngr.GetExceptionTemplateList();
            return objExceptionList;
        }

        public ReturnValue<CRException> GetRequestExceptionList(Int64 CRMainID)
        {
            Business.Common.ReturnValue<CRException> objExceptionList = new ReturnValue<CRException>();
            CorporateRequestManager CorpRequestMngr = new CorporateRequestManager();
            objExceptionList = CorpRequestMngr.GetRequestExceptionList(CRMainID);
            return objExceptionList;
        }
        #endregion

        #region "Search"
        public ReturnValue<CRMain> GetRequestByTripNum(Int64 CRTripNUM)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;

            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CRTripNUM))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();
                        objMain = CorpMngr.GetRequestByTripNum(CRTripNUM);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }

        public ReturnValue<View_CorporateRequestMainList> GetAllCorporateRequestMainList(Int64 homebaseID, Int64 clientID, string tripSheet, string worksheetstr, string hold, string cancelled, string schedServ, string Unfulfilled, bool isLog, DateTime EstDepartDate, string homebaseIDCSVStr, Int64 travelID)
        {
            CorporateRequestManager CorpMngr = null;

            Business.Common.ReturnValue<Data.CorporateRequest.View_CorporateRequestMainList> objMain = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<View_CorporateRequestMainList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, homebaseIDCSVStr, travelID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();
                        objMain = CorpMngr.GetAllCorporateRequestMainList(homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, homebaseIDCSVStr, travelID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }
        public ReturnValue<View_CorporateRequestMainList> GetAllDeletedCorporateRequestMainList()
        {
            CorporateRequestManager CorpMngr = null;

            Business.Common.ReturnValue<Data.CorporateRequest.View_CorporateRequestMainList> objMain = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<View_CorporateRequestMainList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();
                        objMain = CorpMngr.GetAllDeletedCorporateRequestMainList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region "Retrieve Pax"

        public ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList> GetAllCorporateRequestAvailablePaxList()
        {
            Business.Common.ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList> objPax = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();
                        objPax = CorpMngr.GetAllCorporateRequestAvailablePaxList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPax;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.CorporateRequest.CrewPassengerPassport> GetPassengerPassportByPassportID(Int64 PassportId)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.CrewPassengerPassport> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.CrewPassengerPassport>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassportId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetPassengerPassportByPassportID(PassportId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Change Queue"

        public ReturnValue<GetAllCRMain> GetCRChangeQueue()
        {
            ReturnValue<Data.CorporateRequest.GetAllCRMain> objMain = null;
            CorporateRequestManager CorporateRequestMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetAllCRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.GetCRChangeQueue();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CRMain> QueueTransfer(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.QueueTransfer(oCRMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public ReturnValue<CRMain> DenyRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.DenyRequest(oCRMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public ReturnValue<CRMain> AcceptRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.AcceptRequest(oCRMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CRLeg> ChangePrivate(CRLeg oCRLeg)
        {
            ReturnValue<Data.CorporateRequest.CRLeg> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRLeg>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRLeg))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.ChangePrivate(oCRLeg);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CRMain> ApproveRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.ApproveRequest(oCRMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Submit Request"
        public ReturnValue<CRMain> UpdateCRMainCorpStatus(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.UpdateCRMainCorpStatus(oCRMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Cancel Request"
        public ReturnValue<CRMain> CancelRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.CancelRequest(oCRMain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region History
        public ReturnValue<Data.CorporateRequest.CRHistory> GetCRHistory(long CustomerId, long CRMainId)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.CRHistory> objCorpMains = null;
            CorporateRequestManager CorporateRequestMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.CRHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, CRMainId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();
                        objCorpMains = CorporateRequestMngr.GetCRHistory(CustomerId, CRMainId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CRHistory> UpdateCRHistory(CRHistory history)
        {
            Business.Common.ReturnValue<CRHistory> objMain = null;
            CorporateRequestManager CorporateRequestMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(history))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestMngr = new CorporateRequestManager();

                        objMain = CorporateRequestMngr.UpdateCRHistory(history);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        public ReturnValue<CRHistory> AddCRHistory(CRHistory oCRHistory)
        {
            ReturnValue<Data.CorporateRequest.CRHistory> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRHistory))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorporateRequestManager CorporateRequestMngr = new CorporateRequestManager();
                        objMain = CorporateRequestMngr.AddCRHistory(oCRHistory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region "Acknowledge Request"
        public ReturnValue<CRMain> RequestAcknowledge(long requestID, long tripID, long customerID, string lastUpdUID, DateTime lastUpdTS)
        {

            Business.Common.ReturnValue<CRMain> objMain = null;
            CorporateRequestManager CorpRequestMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CRMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(requestID, tripID, customerID, lastUpdUID, lastUpdTS))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpRequestMngr = new CorporateRequestManager();
                        objMain = CorpRequestMngr.RequestAcknowledge(requestID, tripID, customerID, lastUpdUID, lastUpdTS);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "Travelcoordinator"
        public ReturnValue<Data.CorporateRequest.TravelCoordinator> GetTravelcoordinatorbyID(Int64 TravelCoordinatorID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.TravelCoordinator> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.TravelCoordinator>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinatorID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetTravelcoordinatorbyID(TravelCoordinatorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fleet"
        public ReturnValue<Data.CorporateRequest.Fleet> GetFleetbyID(Int64 FleetID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.Fleet> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.Fleet>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetFleetbyID(FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region "Aircraft"
        public ReturnValue<Data.CorporateRequest.Aircraft> GetAircraftbyID(Int64 AircraftID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.Aircraft> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.Aircraft>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetAircraftbyID(AircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region "Passenger"
        public ReturnValue<Data.CorporateRequest.Passenger> GetPassengerbyID(Int64 PassengerRequestorID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.Passenger> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.Passenger>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetPassengerbyID(PassengerRequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR> GetCRPaxbyPaxID(Int64 TravelID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetPassengerByPassengerRequestorIDforCR(TravelID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.CorporateRequest.GetAllFleetForCR> GetCRFleet(Int64 TravelID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.GetAllFleetForCR> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.GetAllFleetForCR>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetAllFleetforCR(TravelID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion


        #region "Department"
        public ReturnValue<Data.CorporateRequest.Department> GetDepartmentbyID(Int64 DepartmentID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.Department> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.Department>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetDepartmentbyID(DepartmentID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Authorization"
        public ReturnValue<Data.CorporateRequest.DepartmentAuthorization> GetAuthorizationbyID(Int64 AuthorizationID, Int64 DepartmentID)
        {
            Business.Common.ReturnValue<Data.CorporateRequest.DepartmentAuthorization> objCorpMains = null;
            CorporateRequestManager CorpMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.CorporateRequest.DepartmentAuthorization>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CorpMngr = new CorporateRequestManager();

                        objCorpMains = CorpMngr.GetAuthorizationbyID(AuthorizationID,DepartmentID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCorpMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
    }
}
