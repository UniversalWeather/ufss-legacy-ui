﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Business.Common;
using System.Data.Objects.DataClasses;
using FlightPak.Services.Utility;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Business.Admin;
using FlightPak.Business.Preflight.ScheduleCalendarEntities;
using System.Collections.ObjectModel;
using FlightPak.Business.Preflight;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CommonService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class CommonService : BaseService, ICommonService
    {
        private ExceptionManager exManager;
        public ReturnValue<EntityObject> Lock(string entitySetName, Int64 primaryKey)
        {
            return new LockManager<EntityObject>().Lock(entitySetName, primaryKey);
        }
        public ReturnValue<EntityObject> UnLock(string entitySetName, Int64 primaryKey)
        {
            return new LockManager<EntityObject>().UnLock(entitySetName, primaryKey);
        }

        public Nullable<Int64> GetSequenceNumber(string ModuleName)
        {
            return new SequenceNumberManager().GetSequenceNumber(ModuleName);
        }

        public ReturnValue<Data.Common.GetAllLocks> GetAllLocks()
        {
            return new LockDetails<Data.Common.GetAllLocks>().GetAllLocks();
        }

        public ReturnValue<Data.Common.GetAllLocks> DeleteLock(Int64 FPLockID)
        {
            return new LockDetails<Data.Common.GetAllLocks>().DeleteLock(FPLockID);

        }

        public ReturnValue<Data.Admin.GetAllLoggedUsersByCustomerId> GetAllLoggedInUsers(bool includeCurrentUser)
        {
            return new FlightPak.Business.Admin.UtilitiesManager().GetAllLoggedInUsers(includeCurrentUser);
        }

        public void LogOutAllUsers(string UserNames)
        {
            new FlightPak.Business.Admin.UtilitiesManager().LogOutAllUsers(UserNames);
        }

        public void LockCustomer()
        { new UtilitiesManager().LockCustomer(); }
        public void UnlockCustomer()
        { new UtilitiesManager().UnlockCustomer(); }

        #region ScheduleCalendar Default View


        public string GetUserDefaultCalendarView()
        {
            try
            {
                return new SystemPreferenceManager().GetUserDefaultCalendarView();
            }
            catch (System.Exception exc)
            {
                //Manually Handled
                ServiceFault ex = new ServiceFault();
                ex.Title = "Common Service - Function:GetUserDefaultCalendarView()";
                ex.InnerException = exc.InnerException != null ? exc.InnerException.StackTrace : null;
                ex.ExceptionMessage = exc.InnerException != null ? exc.InnerException.Message : exc.Message;
                ex.StackTrace = exc.StackTrace;
                throw new FaultException<ServiceFault>(ex, ex.ExceptionMessage);
            }
        }

        /// <summary>
        /// To update Calendar Default view - selected by User
        /// </summary>
        /// <param name="viewMode">calendar View</param>
        /// <returns>true if success, else false</returns>
        public bool UpdateCalendarDefaultView(string viewMode)
        {
            try
            {
                return new SystemPreferenceManager().UpdateCalendarDefaultView(viewMode);
            }
            catch (System.Exception exc)
            {
                //Manually Handled
                ServiceFault ex = new ServiceFault();
                ex.Title = "Common Service - Function:UpdateCalendarDefaultView()";
                ex.InnerException = exc.InnerException != null ? exc.InnerException.StackTrace : null;
                ex.ExceptionMessage = exc.InnerException != null ? exc.InnerException.Message : exc.Message;
                ex.StackTrace = exc.StackTrace;
                throw new FaultException<ServiceFault>(ex, ex.ExceptionMessage);
            }
        }

        # endregion

        #region Security
        // Since Memory cache is per instance, to clear cache in MasterServices we are making this call
        public void LogOut()
        { }
        #endregion

        #region System Administration

        public bool ChangeCrew(long oldCrewID, long newCrewID, string crewCD)
        {
            return new SystemAdminManager().ChangeCrew(oldCrewID, newCrewID, crewCD);
        }

        public bool ChangeAirport(string oldIcaoId, string newIcaoId)
        {
            return new SystemAdminManager().ChangeAirport(oldIcaoId, newIcaoId);
        }

        public bool ChangeFleet(long oldFleetId, string newTailNum, DateTime? effectiveDTTM)
        {
            return new SystemAdminManager().ChangeFleet(oldFleetId, newTailNum, effectiveDTTM);
        }

        public bool ChangePassenger(long oldPassengerID, long newPassengerID, string paxCD)
        {
            return new SystemAdminManager().ChangePassenger(oldPassengerID, newPassengerID, paxCD);
        }

        public void RetrievePreflightTrip(long tripId)
        {
            new SystemAdminManager().RetrievePreflightTrip(tripId);
        }

        public bool ChangeAccountNumber(long oldAccountID, long newAccountID, string oldAccountNum, string newAccountNum)
        {
            return new SystemAdminManager().ChangeAccountNumber(oldAccountID, newAccountID, oldAccountNum, newAccountNum);
        }

        public Business.Common.ReturnValue<Data.Common.GetCrewPAXCleanUpList> GetCrewPAXCleanUpList(string orderBy, bool showInactive, DateTime reportTime)
        {
            Business.Common.ReturnValue<Data.Common.GetCrewPAXCleanUpList> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetCrewPAXCleanUpList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderBy, showInactive, reportTime))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Common.SystemAdminManager objCrewPAXCleanUpList = new FlightPak.Business.Common.SystemAdminManager();
                        retValue = objCrewPAXCleanUpList.GetCrewPAXCleanUpList(orderBy, showInactive, reportTime);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp> GetInactivateCrewPAXForCleanUp(string orderBy, bool showInactive, DateTime reportTime)
        {
            Business.Common.ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(orderBy, showInactive, reportTime))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Common.SystemAdminManager objInactivateCrewPAXForCleanUp = new FlightPak.Business.Common.SystemAdminManager();
                        retValue = objInactivateCrewPAXForCleanUp.GetInactivateCrewPAXForCleanUp(orderBy, showInactive, reportTime);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public bool ChangeCQCustomerCD(long oldCQCUstomerID, string newCQCUstomerCD, DateTime? effectiveDTTM)
        {
            return new SystemAdminManager().ChangeCQCustomerCD(oldCQCUstomerID, newCQCUstomerCD, effectiveDTTM);
        }
        #endregion

        #region Utilities - User Preferences
        public ReturnValue<Data.Common.GetUserPreferenceUtilitiesList> GetUserPreferenceList(Int64 userGroupID)
        {
            Business.Common.ReturnValue<Data.Common.GetUserPreferenceUtilitiesList> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetUserPreferenceUtilitiesList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroupID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Common.SystemPreferenceManager objMgr = new SystemPreferenceManager();
                        retValue = objMgr.GetUserPreferenceList(userGroupID);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public bool AddUserPreference(string categoryName, string subCategoryName, string keyName, string keyValue)
        {
            return new SystemPreferenceManager().AddUserPreference(categoryName, subCategoryName, keyName, keyValue);
        }

        public void DeleteUserPreference()
        {
            new SystemPreferenceManager { }.DeleteUserPreference();
        }

        #endregion

        #region Recent Activities
        public Business.Common.ReturnValue<Data.Common.GetRecentActivityPreflight> GetRecentActivityPreflight(int MaxRecordCount)
        {
            Business.Common.ReturnValue<Data.Common.GetRecentActivityPreflight> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetRecentActivityPreflight>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MaxRecordCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Common.RecentActivityManager objRecentActivityManager = new FlightPak.Business.Common.RecentActivityManager();
                        retValue = objRecentActivityManager.GetRecentActivityPreflight(MaxRecordCount);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Common.GetRecentActivityPostflight> GetRecentActivityPostflight(int MaxRecordCount)
        {
            Business.Common.ReturnValue<Data.Common.GetRecentActivityPostflight> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetRecentActivityPostflight>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MaxRecordCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Common.RecentActivityManager objRecentActivityManager = new FlightPak.Business.Common.RecentActivityManager();
                        retValue = objRecentActivityManager.GetRecentActivityPostflight(MaxRecordCount);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Common.GetRecentActivityCorporateRequest> GetRecentActivityCorporateRequest(int MaxRecordCount)
        {
            Business.Common.ReturnValue<Data.Common.GetRecentActivityCorporateRequest> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetRecentActivityCorporateRequest>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MaxRecordCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Common.RecentActivityManager objRecentActivityManager = new FlightPak.Business.Common.RecentActivityManager();
                        retValue = objRecentActivityManager.GetRecentActivityCorporateRequest(MaxRecordCount);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Common.GetRecentActivityCharterQuote> GetRecentActivityCharterQuote(int MaxRecordCount)
        {
            Business.Common.ReturnValue<Data.Common.GetRecentActivityCharterQuote> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetRecentActivityCharterQuote>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MaxRecordCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Common.RecentActivityManager objRecentActivityManager = new FlightPak.Business.Common.RecentActivityManager();
                        retValue = objRecentActivityManager.GetRecentActivityCharterQuote(MaxRecordCount);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "User Preference - Bookmarks"
        public Business.Common.ReturnValue<Data.Common.GetBookmark> GetBookmark(Data.Common.UserPreference oUserPreference)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.GetBookmark>>(() =>
            {
                Business.Common.ReturnValue<Data.Common.GetBookmark> rtnUserPreference = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oUserPreference.CustomerID = UserPrincipal.Identity.CustomerID;
                        oUserPreference.UserName = UserPrincipal.Identity.Name;
                        FlightPak.Business.Common.SystemPreferenceManager SystemPreferenceManager = new FlightPak.Business.Common.SystemPreferenceManager();
                        rtnUserPreference = SystemPreferenceManager.GetUserPreference(oUserPreference);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnUserPreference;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Common.UserPreference> AddBookmark(Data.Common.UserPreference oUserPreference)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Common.UserPreference>>(() =>
            {
                Business.Common.ReturnValue<Data.Common.UserPreference> rtnUserPreference = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oUserPreference.CustomerID = UserPrincipal.Identity.CustomerID;
                        oUserPreference.UserName = UserPrincipal.Identity.Name;
                        FlightPak.Business.Common.SystemPreferenceManager SystemPreferenceManager = new FlightPak.Business.Common.SystemPreferenceManager();
                        rtnUserPreference = SystemPreferenceManager.AddBookmark(oUserPreference);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnUserPreference;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "User Preference - Bookmarks"

        public Wrappers.SCWrapper GetSchedulingCalendarModal()
        {
            //bool homebaseOnly, DateTime startDate, DateTime endDate, FilterCriteria filterCriteria,
            //Collection<string> fleetTreeInput, bool retrievePaxDetails, bool isStandByCrew, bool showRecordStartsInDateRange,
            //FlightPak.Services.Utility.VendorOption vendorOption, Collection<string> fleetIds, string homebaseId
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Wrappers.SCWrapper>(() =>
            {
                Wrappers.SCWrapper scWrapper = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        scWrapper = new Wrappers.SCWrapper();

                        scWrapper.flightCategory = new FlightPak.Business.MasterCatalog.FlightCategoryManager().GetList();
                        scWrapper.aircraftDuty = new FlightPak.Business.MasterCatalog.AircraftDutyTypeManager().GetList();
                        scWrapper.crewDutyType = new FlightPak.Business.MasterCatalog.CrewDutyTypeManager().GetList();
                        scWrapper.userDefaultCalendarView = new SystemPreferenceManager().GetUserDefaultCalendarView();
                        //scWrapper.fleetTreeResult = new FlightPak.Business.Preflight.ScheduleCalendarManager().GetFleetInfoForFleetTree(homebaseOnly);
                        //FilterEntity filterEntity = ConvertToBusinessEntity(filterCriteria);
                        //scWrapper.fleetCalendarDataResult = new FlightPak.Business.Preflight.ScheduleCalendarManager().GetFleetCalendarData(startDate, endDate, filterEntity, fleetTreeInput, retrievePaxDetails, isStandByCrew, showRecordStartsInDateRange);
                        //scWrapper.fleetResourceTypeResult = new FlightPak.Business.Preflight.ScheduleCalendarManager().GetFleetResourceType(vendorOption.ToString(), fleetIds, homebaseOnly, homebaseId);
                        scWrapper.userSettingAvailable = new ScheduleCalendarManager().IsUserSettingsAvailable();
                        scWrapper.retrieveSystemDefaultSettings =  new ScheduleCalendarManager().RetrieveSystemDefaultSettings();
                        scWrapper.scheduleCalenderPreferencesAvailable = new ScheduleCalendarManager().IsScheduleCalenderPreferencesAvailable("View_Monthly");                        
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }
                return scWrapper;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Wrappers.SCPlannerWrapper GetPlannerModal(bool homebaseOnly)
        {  
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Wrappers.SCPlannerWrapper>(() =>
            {
                Wrappers.SCPlannerWrapper scWrapper = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseOnly))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        scWrapper = new Wrappers.SCPlannerWrapper();
                        scWrapper.fleetTreeResult = new FlightPak.Business.Preflight.ScheduleCalendarManager().GetFleetInfoForFleetTree(homebaseOnly);
                        scWrapper.crewTreeResult = new FlightPak.Business.Preflight.ScheduleCalendarManager().GetCrewInfoForCrewTree(homebaseOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }
                return scWrapper;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
    }
}
