﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data;
using FlightPak.Business.Admin;

namespace FlightPak.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class AdminService : IAdminService
    {
        private ExceptionManager exManager;
        #region "Customer Manager"
        public Business.Common.ReturnValue<Data.Admin.Customer> AddCustomer(Data.Admin.Customer Customer)
        {
            Business.Common.ReturnValue<Data.Admin.Customer> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Customer>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Customer))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager CustomerBusiness = new FlightPak.Business.Admin.CustomerManager();
                        List<FlightPak.Data.MasterCatalog.CustomerFuelVendorAccess> oCustomerFuelVendorAccessList = new List<CustomerFuelVendorAccess>();
                        oCustomerFuelVendorAccessList.AddRange(Customer.CustomerFuelVendorAccess);
                        retValue = CustomerBusiness.Add(Customer);
                        if (oCustomerFuelVendorAccessList.Count > 0)
                        {
                            var objCustomerFuelVendorAccessManager = new FlightPak.Business.MasterCatalog.CustomerFuelVendorAccessManager();
                            objCustomerFuelVendorAccessManager.AddCustomerFuelVendorAccess(oCustomerFuelVendorAccessList, Customer.CustomerID);
                        }
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }
        public Business.Common.ReturnValue<Data.Admin.Customer> UpdateCustomer(Data.Admin.Customer customer, int customerModuleCount)
        {
            Business.Common.ReturnValue<Data.Admin.Customer> retValue = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Customer>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customer, customerModuleCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        List<FlightPak.Data.MasterCatalog.CustomerFuelVendorAccess> oCustomerFuelVendorAccessList = new List<CustomerFuelVendorAccess>();
                        oCustomerFuelVendorAccessList.AddRange(customer.CustomerFuelVendorAccess);
                        retValue = objCustomer.Update(customer, customerModuleCount);
                        if (oCustomerFuelVendorAccessList.Count > 0)
                        {
                            var objCustomerFuelVendorAccessManager = new FlightPak.Business.MasterCatalog.CustomerFuelVendorAccessManager();
                            objCustomerFuelVendorAccessManager.UpdateCustomerFuelVendorAccess(oCustomerFuelVendorAccessList, customer.CustomerID);
                        }
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.UserMaster> ValidateUser(string userName)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.ValidateUser(userName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.Customer> DeleteCustomer(Data.Admin.Customer Customer)
        {
            Business.Common.ReturnValue<Data.Admin.Customer> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Customer>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Customer))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.Delete(Customer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.Customer> GetCustomerLock(Data.Admin.Customer Customer)
        {
            Business.Common.ReturnValue<Data.Admin.Customer> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Customer>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Customer))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.GetLock(Customer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.GetAllCustomersLicence> GetCustomerList()
        {
            Business.Common.ReturnValue<Data.Admin.GetAllCustomersLicence> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetAllCustomersLicence>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.GetAllAirport> GetAirportMasterInfo(Int64 customerID)
        {
            Business.Common.ReturnValue<Data.Admin.GetAllAirport> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetAllAirport>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.GetAirportList(customerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Int64 GetUWACustomerId()
        {
            Int64 retValue = 0;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int64>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.GetUWACustomerId();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Int64? GetActualUserCount(Int64 customerId)
        {
            Int64? retValue = 0;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int64?>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomer = new FlightPak.Business.Admin.CustomerManager();
                        retValue = objCustomer.GetActualUserCount(customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Int64? GetAircraftUsedCount(Int64 customerId)
        {
            Int64? AircraftUsedCount = 0;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int64?>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerManager objCustomerManager = new FlightPak.Business.Admin.CustomerManager();
                        AircraftUsedCount = objCustomerManager.GetAircraftUsedCount(customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return AircraftUsedCount;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Customer Module Accesses"
        public Business.Common.ReturnValue<Data.Admin.GetCustomerModuleAccesses> GetCustomerModuleAccessList(Int64 CustomerID)
        {
            Business.Common.ReturnValue<Data.Admin.GetCustomerModuleAccesses> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetCustomerModuleAccesses>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CustomerModuleAccessesManager objCustomer = new FlightPak.Business.Admin.CustomerModuleAccessesManager();
                        retValue = objCustomer.GetList(CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region "User Manager"
        public Int64? GetUsedUserCount(Int64 customerId)
        {
            Int64? retValue = 0;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int64?>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objCustomer = new FlightPak.Business.Admin.UserManager();
                        retValue = objCustomer.GetUsedUserCount(customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.UserMaster> AddUserMaster(Data.Admin.UserMaster UserMaster, string password, bool isClientCode = false)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserMaster, password))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.Add(UserMaster, password, isClientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            bool returnvalue = false;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldPassword, newPassword))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        returnvalue = objUserMaster.ChangePassword(oldPassword, newPassword);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return returnvalue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }


        public bool ChangePasswordWithPeriod(string oldPassword, string newPassword, int resetPeriod)
        {
            bool returnvalue = false;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldPassword, newPassword, resetPeriod))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        returnvalue = objUserMaster.ChangePasswordWithPeriod(oldPassword, newPassword, resetPeriod);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return returnvalue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }


        public bool CheckResetpasswordPeriod(string emailId)
        {
            bool returnvalue = false;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emailId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        returnvalue = objUserMaster.CheckResetpasswordPeriod( emailId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return returnvalue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }
        public Business.Common.ReturnValue<Data.Admin.UserMaster> UpdateUserMaster(Data.Admin.UserMaster UserMaster, int UserGroupMappingCount, bool isClientCode = false)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserMaster, UserGroupMappingCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.Update(UserMaster, UserGroupMappingCount, isClientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.UserMaster> DeleteUserMaster(Data.Admin.UserMaster UserMaster)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserMaster))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.Delete(UserMaster);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.UserMaster> GetUserMasterLock(Data.Admin.UserMaster UserMaster)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserMaster))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.GetLock(UserMaster);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.GetAllUserMasterResult> GetUserMasterList()
        {
            Business.Common.ReturnValue<Data.Admin.GetAllUserMasterResult> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetAllUserMasterResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Admin.UserMaster> ResetPassword(string userName, string answer, string hashPassword)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, answer, hashPassword))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.ResetPassword(userName, answer, hashPassword);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public bool SendLockedEmailToUser(string userName)
        {
            bool retValue = false;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.SendLockedEmailToUser(userName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        public Business.Common.ReturnValue<Data.Admin.GetUserMasterClient> GetUserMasterClient()
        {
            Business.Common.ReturnValue<Data.Admin.GetUserMasterClient> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetUserMasterClient>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.GetUserMasterClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        public Business.Common.ReturnValue<Data.Admin.GetPasswordHistory> GetPasswordHistory(string UserName)
        {
            Business.Common.ReturnValue<Data.Admin.GetPasswordHistory> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetPasswordHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.GetPasswordHistory(UserName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Admin.UserMaster> GetUserByUserName(string username)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(username))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.GetUserByUserName(username);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Admin.UserMaster> ResetPasswordWithoutAuth(string userName, string answer, string hashPassword, string emailId)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, answer, hashPassword, emailId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.ResetPasswordWithoutAuth(userName, answer, hashPassword, emailId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Admin.GetUserByEmailId> GetUserByEMailId(string emailId)
        {
            Business.Common.ReturnValue<Data.Admin.GetUserByEmailId> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetUserByEmailId>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emailId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.GetUserByEMailId(emailId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.UserMaster> LockOrUnlockUser(string userName, bool isUserLock)
        {
            Business.Common.ReturnValue<Data.Admin.UserMaster> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserMaster>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, isUserLock))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserManager objUserMaster = new FlightPak.Business.Admin.UserManager();
                        retValue = objUserMaster.LockOrUnlockUser(userName, isUserLock);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "User Group Mappings"
        public Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupAvailableList(string UserName, Int64 CustomerID)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName, CustomerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupMappingManager objUserMaster = new FlightPak.Business.Admin.UserGroupMappingManager();
                        retValue = objUserMaster.GetUserGroupAvailableList(UserName, CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupSelectedList(string UserName, Int64 CustomerID)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName, CustomerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupMappingManager objUserMaster = new FlightPak.Business.Admin.UserGroupMappingManager();
                        retValue = objUserMaster.GetUserGroupSelectedList(UserName, CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region "UserGroup"

        public Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupByGroupCode(string userGroupCode)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroupCode))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        retValue = new Business.Common.ReturnValue<Data.Admin.UserGroup>();
                        FlightPak.Business.Admin.UserGroupManager objUserGroup = new FlightPak.Business.Admin.UserGroupManager();
                        retValue = objUserGroup.GetUserGroupByGroupCode(userGroupCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Admin.UserGroup> AddUserGroup(Data.Admin.UserGroup userGroup)
        {

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Admin.UserGroupManager objUserGroup = new FlightPak.Business.Admin.UserGroupManager();
                        retValue = objUserGroup.Add(userGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        public Business.Common.ReturnValue<Data.Admin.UserGroup> UpdateUserGroup(Data.Admin.UserGroup userGroup, List<Data.Admin.UserGroupPermission> dtUpdatePermission, int maintainGroupModuleCount)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroup, maintainGroupModuleCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupManager objUserGroup = new FlightPak.Business.Admin.UserGroupManager();
                        retValue = objUserGroup.Update(userGroup, dtUpdatePermission, maintainGroupModuleCount);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Admin.UserGroup> DeleteUserGroup(Data.Admin.UserGroup userGroup)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroup))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupManager objUserGroup = new FlightPak.Business.Admin.UserGroupManager();
                        retValue = objUserGroup.Delete(userGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupLock(Data.Admin.UserGroup usergroup)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(usergroup))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupManager objUserGroup = new FlightPak.Business.Admin.UserGroupManager();
                        retValue = objUserGroup.GetLock(usergroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Admin.GetUserGroupPermission> GetUserGroupPermission(Int64 UserGroupID, Int64 CustomerID)
        {
            Business.Common.ReturnValue<Data.Admin.GetUserGroupPermission> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetUserGroupPermission>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupID, CustomerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupPermissionManager objUserGroupPermission = new FlightPak.Business.Admin.UserGroupPermissionManager();
                        retValue = objUserGroupPermission.GetList(UserGroupID, CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        public Business.Common.ReturnValue<Data.Admin.UserGroup> GetUserGroupList(Int64 CustomerID)
        {
            Business.Common.ReturnValue<Data.Admin.UserGroup> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.UserGroup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.UserGroupManager objUserGroup = new FlightPak.Business.Admin.UserGroupManager();
                        retValue = objUserGroup.GetList(CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        #endregion

        #region "Country Manager"
        /// <summary>
        /// To Add the values to the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.Admin.Country> AddCountryMaster(Data.Admin.Country Country)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CountryManager objCountry = new FlightPak.Business.Admin.CountryManager();
                        retValue = objCountry.Add(Country);


                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// To Update the values in the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.Admin.Country> UpdateCountryMaster(Data.Admin.Country Country)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CountryManager objCountry = new FlightPak.Business.Admin.CountryManager();
                        retValue = objCountry.Update(Country);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// To delete the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.Admin.Country> DeleteCountryMaster(Data.Admin.Country Country)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CountryManager objCountry = new FlightPak.Business.Admin.CountryManager();
                        retValue = objCountry.Delete(Country);
                        return retValue;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                    return retValue;
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }
        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.Admin.Country> GetCountryMasterList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.CountryManager objCountry = new FlightPak.Business.Admin.CountryManager();
                        retValue = objCountry.GetList();

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "DSTRegion Manager"
        public Business.Common.ReturnValue<Data.Admin.DSTRegion> AddDSTRegion(Data.Admin.DSTRegion dSTRegionType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dSTRegionType.LastUpdTS = System.DateTime.UtcNow;
                        FlightPak.Business.Admin.DSTManager objDSTRegionType = new FlightPak.Business.Admin.DSTManager();
                        retValue = objDSTRegionType.Add(dSTRegionType);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.DSTRegion> UpdateDSTRegion(Data.Admin.DSTRegion dSTRegionType)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.DSTManager objDSTRegionType = new FlightPak.Business.Admin.DSTManager();
                        retValue = objDSTRegionType.Update(dSTRegionType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.DSTRegion> DeleteDSTRegion(Data.Admin.DSTRegion dSTRegionType)
        {

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.DSTManager objDSTRegionType = new FlightPak.Business.Admin.DSTManager();
                        retValue = objDSTRegionType.Delete(dSTRegionType);
                        return retValue;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                    return retValue;
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.Admin.DSTRegion> GetDSTRegionList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //Handle methods throguh exception manager with return flag
                        FlightPak.Business.Admin.DSTManager objDSTRegionType = new FlightPak.Business.Admin.DSTManager();
                        retValue = objDSTRegionType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Help Content"
        public Business.Common.ReturnValue<Data.Admin.HelpContent> AddHelpContent(Data.Admin.HelpContent oHelpContent)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpContent>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpContent> rtnHelpContent = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpContent = HelpManager.Add(oHelpContent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpContent;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.HelpContent> DeleteHelpContent(Data.Admin.HelpContent oHelpContent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpContent>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpContent> rtnHelpContent = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpContent = HelpManager.Delete(oHelpContent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpContent;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.HelpContent> UpdateHelpContent(Data.Admin.HelpContent oHelpContent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpContent>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpContent> rtnHelpContent = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpContent = HelpManager.Update(oHelpContent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpContent;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.HelpContent> GetHelpContentList(Data.Admin.HelpContent oHelpContent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpContent>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpContent> rtnHelpContent = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpContent = HelpManager.GetList(oHelpContent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpContent;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.GetHelpContent> GetHelpContent(Data.Admin.HelpContent oHelpContent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.GetHelpContent>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.GetHelpContent> rtnHelpContent = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpContent = HelpManager.GetHelpContent(oHelpContent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpContent;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Help Content"

        #region "Help Items"
        public Business.Common.ReturnValue<Data.Admin.HelpItem> AddHelpItems(Data.Admin.HelpItem oHelpItem)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpItem>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpItem> rtnHelpItems = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpItems = HelpManager.Add(oHelpItem);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpItems;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.HelpItem> DeleteHelpItems(Data.Admin.HelpItem oHelpItem)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpItem>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpItem> rtnHelpItem = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpItem = HelpManager.Delete(oHelpItem);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpItem;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.Admin.HelpItem> GetHelpList(Data.Admin.HelpItem oHelpItem)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Admin.HelpItem>>(() =>
            {
                Business.Common.ReturnValue<Data.Admin.HelpItem> rtnHelpItems = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Admin.HelpManager HelpManager = new FlightPak.Business.Admin.HelpManager();
                        rtnHelpItems = HelpManager.GetList(oHelpItem);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return rtnHelpItems;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public bool UpdateHelpItems(string strColumn, string strValue, string ItemID)
        {
            bool returnValue = false;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HelpManager helpManager = new HelpManager();
                        returnValue = helpManager.UpdateHelpItems(strColumn, strValue, ItemID);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return returnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Help Items"

        #region Trial Customer
        public string ProcessTrialCustomer(long CustomerId, string document)
        {
            string returnValue = string.Empty;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TrialCustomerManager trialCustomerManager = new TrialCustomerManager();
                        returnValue = trialCustomerManager.ProcessTrialCustomer(CustomerId, document);
                        
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return returnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fuel File XSLT Parser"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> AddFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //oFuelFileXsltParser.LastUpdUID = UserPrincipal.Identity.Name;
                        //oFuelFileXsltParser.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFuelFileXsltParser.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.Add(oFuelFileXsltParser);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> UpdateFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //oFuelFileXsltParser.LastUpdUID = UserPrincipal.Identity.Name;
                        //oFuelFileXsltParser.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFuelFileXsltParser.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.Update(oFuelFileXsltParser);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> DeleteFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //oFuelFileXsltParser.LastUpdUID = UserPrincipal.Identity.Name;
                        //oFuelFileXsltParser.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFuelFileXsltParser.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.Delete(oFuelFileXsltParser);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetAllXsltParser()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> objFuelFileXsltParser = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager fuelfilexsltparserManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        objFuelFileXsltParser = fuelfilexsltparserManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFuelFileXsltParser;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> AddFuelFileCsvHeaders(string[] headers, int xsltParserId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(headers))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<Data.MasterCatalog.FuelCsvFileFields> csvFields = new List<FuelCsvFileFields>();
                        foreach (var item in headers)
                        {
                            Data.MasterCatalog.FuelCsvFileFields newField = new Data.MasterCatalog.FuelCsvFileFields();
                            newField.FieldTitle = item;
                            newField.FuelFileXsltParserID = xsltParserId;
                            newField.IsInActive = true;
                            newField.IsDeleted = false;
                            newField.LastUpdTS = DateTime.Now;
                            //newField.LastUpdUID = UserPrincipal.Identity.Name;
                            csvFields.Add(newField);
                        }
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager objManager = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        retValue = objManager.AddBulkCSVFields(csvFields);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetXsltParserById(int xsltParserId)
        {
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> objFuelFileXsltParser = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager fuelfilexsltparserManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        objFuelFileXsltParser = fuelfilexsltparserManager.GetXsltParserByID(xsltParserId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFuelFileXsltParser;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> DeleteOldCsvFieldsBasedOnXsltParserId(int xsltParserId)
        {
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> objFuelCsvFileFields = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager oFuelCsvFileFieldsManage = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        objFuelCsvFileFields = oFuelCsvFileFieldsManage.DeleteOldCsvFieldsBasedOnXsltParserId(xsltParserId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFuelCsvFileFields;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> AddFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelSavedFileFormat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //oFuelSavedFileFormat.LastUpdUID = UserPrincipal.Identity.Name;
                        //oFuelSavedFileFormat.LastUpdTS = DateTime.Now;
                        oFuelSavedFileFormat.IsDeleted = false;
                        oFuelSavedFileFormat.IsInActive = false;
                        //oFuelSavedFileFormat.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.Add(oFuelSavedFileFormat);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> UpdateFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelSavedFileFormat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //oFuelSavedFileFormat.LastUpdUID = UserPrincipal.Identity.Name;
                        oFuelSavedFileFormat.LastUpdTS = DateTime.Now;
                        oFuelSavedFileFormat.IsDeleted = false;
                        oFuelSavedFileFormat.IsInActive = false;
                        //oFuelSavedFileFormat.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.Update(oFuelSavedFileFormat);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendor()
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.GetAllFuelVendorList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<FuelFileData> CheckFileDataByDataformat(FuelFileData objFuelFileData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<FuelFileData>>(() =>
            {
                Business.Common.ReturnValue<FuelFileData> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFileData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.CheckFileDataByDataformat(objFuelFileData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fuel Vendor Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> AddFuelVendor(Data.MasterCatalog.FuelVendor fuelVendor)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendor))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.Add(fuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> UpdateFuelVendor(Data.MasterCatalog.FuelVendor fuelVendor)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendor))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.Update(fuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }

                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> DeleteFuelVendor(long? fuelVendorId)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendorId))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        Data.MasterCatalog.FuelVendor fuelVendor = objFuelVendor.GetFuelVendorByFuelVendorFilter(fuelVendorId, "").EntityInfo;
                        fuelVendor.IsDeleted = true;
                        retValue = objFuelVendor.Delete(fuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorInfoByFilter(long? fuelVendorId, string fuelVendorCode)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendorId, fuelVendorCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.GetFuelVendorByFuelVendorFilter(fuelVendorId, fuelVendorCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }

                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Customer Fuel Vendor Access"
        public Business.Common.ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccessList> GetCustomerFuelVendorAccessList(long? customerId)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccessList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccessList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerId))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        var objCustomerFuelVendorAccessManager = new FlightPak.Business.MasterCatalog.CustomerFuelVendorAccessManager();
                        retValue = objCustomerFuelVendorAccessManager.GetCustomerFuelVendorAccessList(customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
    }
}
