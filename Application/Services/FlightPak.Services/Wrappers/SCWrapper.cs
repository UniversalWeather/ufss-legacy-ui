﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using FlightPak.Business.Common;
using FlightPak.Data.Preflight;

namespace FlightPak.Services.Wrappers
{
    public class SCWrapper
    {
       public FlightPak.Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> flightCategory;
       public Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> aircraftDuty;
       public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> crewDutyType;
       public string userDefaultCalendarView;
       //public ReturnValue<Data.Preflight.FleetTreeResult> fleetTreeResult;
       //public ReturnValue<FleetCalendarDataResult> fleetCalendarDataResult;
       //public ReturnValue<Data.Preflight.FleetResourceTypeResult> fleetResourceTypeResult;
       public bool userSettingAvailable;
       public string retrieveSystemDefaultSettings;

       public bool scheduleCalenderPreferencesAvailable;
       public string retrieveScheduleCalenderPreferences;
       
    }

    public class SCPlannerWrapper
    {
        public ReturnValue<Data.Preflight.FleetTreeResult> fleetTreeResult;
        public ReturnValue<Data.Preflight.CrewTreeResult> crewTreeResult;
    }
}
