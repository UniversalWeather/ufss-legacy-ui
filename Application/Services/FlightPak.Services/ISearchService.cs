﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISearchService" in both code and config file together.
    [ServiceContract]
    public interface ISearchService
    {
        #region Simple Search
        [OperationContract]
        Business.Common.ReturnValue<Data.Search.SearchResult> SimpleSearch(string SearchStr, string Module, int PageNumber, int PageSize);
        [OperationContract]
        Business.Common.ReturnValue<Data.Search.FlightPakHelpContentSearch> HelpContentSearch(string SearchStr);
        #endregion
    }
}
