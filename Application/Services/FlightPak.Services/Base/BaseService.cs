﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightPak.Services
{
    public class BaseService
    {        
        protected FlightPak.Framework.Security.FPPrincipal UserPrincipal
        {
            get { return (FlightPak.Framework.Security.FPPrincipal)System.Threading.Thread.CurrentPrincipal; }
        }
    }
}