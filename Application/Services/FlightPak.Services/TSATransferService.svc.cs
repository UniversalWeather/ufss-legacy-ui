﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Services;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TSATransferService" in code, svc and config file together.
    public class TSATransferService : BaseService, ITSATransferService
    {
        private ExceptionManager exManager;
        private const string Key = "GetResult";
        public string GetData()
        {
            return "You entered:";
        }



        public FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Metro> GetMetroCityList()
        {
            //ObjectCache cache = MemoryCache.Default;

            //if (cache.Contains(Key))
            //    return (FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Metro>)cache.Get(Key);
            //else
            //{
            FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Metro> objMetroCityType = new FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Metro>();
            FlightPak.Data.MasterCatalog.MasterDataContainer objContainer = new FlightPak.Data.MasterCatalog.MasterDataContainer();

            //objMetroCityType.EntityList = objContainer.GetAllMetroCity().ToList();
            //objMetroCityType.ReturnFlag = true;

            //CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            //cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(2.0);
            //cache.Add(Key, objMetroCityType, cacheItemPolicy);

            return objMetroCityType;
            //}
        }

        public FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.FileWarehouse> AddFWHType(FlightPak.Data.MasterCatalog.FileWarehouse FileWH)
        {
            FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.FileWarehouse> retValue = null;

            FlightPak.Business.MasterCatalog.FileWarehouseManager objFWH = new FlightPak.Business.MasterCatalog.FileWarehouseManager();
            retValue = objFWH.Add(FileWH);

            return retValue;
        }

        public FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Customer> AddTSA(FlightPak.Data.MasterCatalog.Customer TSAData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Customer>>(() =>
            {
                FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Customer> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TSAData.CustomerName = UserPrincipal.Identity.Name;
                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        TSAData.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.AddTSA(TSAData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSANoFly> DeleteTSANoFly(FlightPak.Data.MasterCatalog.TSANoFly TSAData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSANoFly>>(() =>
            {
                FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSANoFly> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        TSAData.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.DeleteTSANOFly(TSAData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSASelect> DeleteTSASelecte(FlightPak.Data.MasterCatalog.TSASelect TSAData)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSASelect>>(() =>
            {
                FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSASelect> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;

                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        TSAData.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.DeleteTSASelect(TSAData);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSAClear> DeleteTSANClear(FlightPak.Data.MasterCatalog.TSAClear TSAData)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSAClear>>(() =>
            {
                FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSAClear> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;

                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        TSAData.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.DeleteTSAClear(TSAData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

    }
}
