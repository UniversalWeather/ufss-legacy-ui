using System.Web.Services.Description;
using FlightPak.Business.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Data.MasterCatalog;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMasterCatalogService" in both code and config file together.
    [ServiceContract(Namespace = "http://myservice.com")] // This will be changed are moved to web.config file - Sathish
    public interface IMasterCatalogService
    {
        #region "Accounts Manager"
          #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAccountWithFilters> GetAccountWithFilters(string AccountNum, long AccountID, string FetchHomebaseOnly, bool FetchInactiveOnly);
          #endregion

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Account> AddAccount(Data.MasterCatalog.Account account);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Account> UpdateAccount(Data.MasterCatalog.Account account);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Account> DeleteAccount(Data.MasterCatalog.Account account);
        //[OperationContract]
        // Business.Common.ReturnValue<Data.MasterCatalog.Account> GetAccountLock(Data.MasterCatalog.Account account);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Account> GetAccountList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAccounts> GetAllAccountList();
        #endregion
        #region "Passenger Group Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> AddPaxGroup(Data.MasterCatalog.PassengerGroup paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> UpdatePaxGroup(Data.MasterCatalog.PassengerGroup paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> DeletePaxGroup(Data.MasterCatalog.PassengerGroup paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> GetPaxGroupLock(Data.MasterCatalog.PassengerGroup paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> GetPaxGroupList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupList> GetPaxGroupListInfo();
        #endregion
        #region "Aircraft Type Catalog"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftWithFilters(string AircraftCD, long AircraftID, bool FetchInactiveOnly);
        #endregion

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetList();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAircraft> GetAircraftList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftByAircraftID(Int64 AircraftID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> AddAircraft(Data.MasterCatalog.Aircraft aircraft);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> UpdateAircraft(Data.MasterCatalog.Aircraft aircraft);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> DeleteAircraft(Data.MasterCatalog.Aircraft aircraft);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftLock(Data.MasterCatalog.Aircraft aircraft);
        [OperationContract]
        Int64? GetAircraftUsedCount();
        [OperationContract]
        Int64? GetAircraftActualCount();
        [OperationContract]
        Int32 GetPreflightLegAircraftDuty(string airtcraftDutyCD);
        #endregion
        #region "DelayType"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DelayType> GetAllDelayTypeWithFilters(long DelayTypeID, string DelayTypeCD, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DelayType> AddDelayType(Data.MasterCatalog.DelayType delayType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DelayType> GetDelayTypeList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DelayType> UpdateDelayType(Data.MasterCatalog.DelayType delayType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DelayType> DeleteDelayType(Data.MasterCatalog.DelayType delayType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DelayType> GetDelayTypeLock(Data.MasterCatalog.DelayType delayType);
        #endregion
        #region "FlightCategory"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters> GetAllFlightCategoryWithFilters(long ClientID, long FlightCategoryID,string FlightCategoryCD, bool FetchInactiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> AddFlightCategory(Data.MasterCatalog.FlightCatagory flightCategory);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> UpdateFlightCategory(Data.MasterCatalog.FlightCatagory flightCategory);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> DeleteFlightCategory(Data.MasterCatalog.FlightCatagory flightCategory);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> GetFlightCategoryLock(Data.MasterCatalog.FlightCatagory flightCategory);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> GetFlightCategoryList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategories> GetAllFlightCategoryList();
        /// <summary>
        /// Added by Scheduling calendar team to get FlightCategory Entity by FlightCategory Code.
        /// </summary>
        /// <param name="flightpurpose"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult> GetFlightCategoryByCategoryCD(string flightCategory);
        #endregion
        #region "flightpurpose"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters> GetAllFlightPurposesWithFilters(long ClientID, long FlightPurposeID, string FlightPurposeCD, bool FetchDefaultPurposeOnly, bool FetchPersonalTravelOnly, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> AddFlightPurpose(Data.MasterCatalog.FlightPurpose flightpurpose);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> UpdateFlightPurpose(Data.MasterCatalog.FlightPurpose flightpurpose);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> DeleteFlightPurpose(Data.MasterCatalog.FlightPurpose flightpurpose);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposeLock(Data.MasterCatalog.FlightPurpose flightpurpose);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposeList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposes> GetAllFlightPurposeList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposebyFlightPurposeID(Int64 FlightPurposeID);
        #endregion
        #region Worldwind and WindStat
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.WorldWind> GetWorldWindInfo(long DepartZoneAirprt, long ArrivZoneAirprt, string Quartr, string Alt);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.WindStat> GetWindStatInfo(string Hemisphere, string Quadrilateral, string Altitude, string Quarter);
        #endregion Worldwind and WindStat
        #region "FleetGroupMaster"                  
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> AddFleetType(Data.MasterCatalog.FleetGroup fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> GetFleetList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> UpdateFleetType(Data.MasterCatalog.FleetGroup fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> DeleteFleetType(Data.MasterCatalog.FleetGroup fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> GetFleetTypeLock(Data.MasterCatalog.FleetGroup fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetGroup> GetFleetGroup();
        #endregion
        #region "DSTRegionMaster"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> AddDSTRegion(Data.MasterCatalog.DSTRegion DSTRegionType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> GetDSTRegionList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> UpdateDSTRegion(Data.MasterCatalog.DSTRegion DSTRegionType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> DeleteDSTRegion(Data.MasterCatalog.DSTRegion DSTRegionType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> GetDSTRegionTypeLock(Data.MasterCatalog.DSTRegion DSTRegionType);
        #endregion
        #region "MetroCityMaster"
         #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Metro> GetMetroWithFilters(long MetroId, string MetroCD);
          #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Metro> AddMetroCity(Data.MasterCatalog.Metro MetroCityType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Metro> GetMetroCityList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Metro> UpdateMetroCity(Data.MasterCatalog.Metro MetroCityType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Metro> DeleteMetroCity(Data.MasterCatalog.Metro MetroCityType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Metro> GetMetroCityTypeLock(Data.MasterCatalog.Metro MetroCityType);
        #endregion
        #region "Country Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Country> GetCountryWithFilters(string CountryCD, long CountryId);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Country> AddCountryMaster(Data.MasterCatalog.Country Country);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Country> UpdateCountryMaster(Data.MasterCatalog.Country Country);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Country> GetCountryMasterLock(Data.MasterCatalog.Country Country);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Country> GetCountryMasterList();
        #endregion
        #region "Aircraft Duty Type Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> AddAircraftDuty(Data.MasterCatalog.AircraftDuty aircraftDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> UpdateAircraftDuty(Data.MasterCatalog.AircraftDuty aircraftDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> DeleteAircraftDuty(Data.MasterCatalog.AircraftDuty aircraftDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> GetAircraftDutyLock(Data.MasterCatalog.AircraftDuty aircraftDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> GetAircraftDuty();
        #endregion
        #region "EmergencyContactMaster"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> GetAllEmergencyContactWithFilters(long EmergencyContactID, string EmergencyContactCD, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> AddEmergencyType(Data.MasterCatalog.EmergencyContact emergencytype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> GetEmergencyList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> UpdateEmergencyType(Data.MasterCatalog.EmergencyContact emergencytype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> DeleteEmergencyType(Data.MasterCatalog.EmergencyContact emergencytype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> GetEmergencyTypeLock(Data.MasterCatalog.EmergencyContact emergencytype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetEmergencyContact> GetEmergencyListInfo();
        #endregion
        #region "Company Manager"

        #region For Performance Optimization
         /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyWithFilters> GetCompanyWithFilters(string HomebaseCD, long HomebaseId, bool FetchInactiveOnly,bool IsDeleted);
        #endregion
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Company> AddCompanyMaster(Data.MasterCatalog.Company CompanyData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Company> UpdateCompanyMaster(Data.MasterCatalog.Company CompanyData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Company> DeleteCompanyMaster(Data.MasterCatalog.Company CompanyData);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Company> GetCompanyMasterLock(Data.MasterCatalog.Company CompanyData);
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Company> GetCompanyMasterList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMaster> GetCompanyMasterListInfo();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyID> GetCompanyID();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripPrivacySettings> GetTripPrivacySettings();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Customer> ReadTSACSVFile(Data.MasterCatalog.Customer oTSAData, string TSAType, string TSAFileUrl);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Customer> SetTSAstatus(Data.MasterCatalog.Customer TSAData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSANoFly> DeleteTSANoFly(Data.MasterCatalog.TSANoFly TSAData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSASelect> DeleteTSASelecte(Data.MasterCatalog.TSASelect TSAData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSAClear> DeleteTSANClear(Data.MasterCatalog.TSAClear TSAData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId> GetListInfoByHomeBaseId(Int64 HomeBaseID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Company> UpdateAPISInterface(Int64 HomebaseID, string UserNameCBP, string UserPasswordCBP, string Domain, string APISUrl, bool IsAPISSupport, bool IsAPISAlert);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> GetTripSheetReportHeaderList();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQMessage> GetCQMessage(string MessageTYPE, Int64 HomebaseID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQMessage> AddCqMessage(Data.MasterCatalog.CQMessage CQMessageData);

        [OperationContract]
        void DeleteCQMessage(string MessageTYPE, Int64 HomebaseID);
        #endregion
        #region "Client Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> AddClientMaster(Data.MasterCatalog.Client client);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> UpdateClientMaster(Data.MasterCatalog.Client client);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> DeleteClientMaster(Data.MasterCatalog.Client client);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientMasterLock(Data.MasterCatalog.Client client);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientMasterList();

        #endregion
        #region "Airport Manager"
        #region For Performance Optimization
        /// <summary>
        /// Interface to fetch a single Airport Record
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters> GetAirportForGridWithFilters(long AirportID, string ICAOID, long RunwayOnly, bool IsDisplayInctive);

        /// <summary>
        /// Interface to fetch a single Airport Record
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter> GetAirportForGridPagingWithFilter(long AirportID, string ICAOID, long RunwayOnly, bool IsDisplayInctive, int StartRowIndex, int PageSize, string filterValue, string SortExpression, string SortOrder, string SelectedICAOID);


        #endregion
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Airport> AddAirport(Data.MasterCatalog.Airport AirportData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Airport> UpdateAirport(Data.MasterCatalog.Airport AirportData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Airport> DeleteAirport(Data.MasterCatalog.Airport AirportData);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Airport> GetAirportLock(Data.MasterCatalog.Airport AirportData);
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Airport> GetAirportList();


        /// <summary>
        /// Interface to fetch a single Airport Record
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAirportByAirportID(Int64 AirportID);

        /// <summary>
        /// Interface to fetch a single Airport Record
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAirportByAirportICaoID(string ICaoID);


        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByCityName> GetAirportByCityName(string CityName);

        ///// <summary>
        ///// Interface to fetch records from table
        ///// </summary>
        ///// <returns>ReturnValue<Data.MasterCatalog.Airport></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.Airport> GetAirportInfo(Data.MasterCatalog.Airport AirportData);
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAllAirportList();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount> GetAirportPositionAndCount(bool IsDisplayInctive, long AirportId, bool selectLastModified);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging> GetAllAirportListForGridCustomPaging(bool IsDisplayInctive, decimal MinimumRunWay, int Start, int End, string Sort, string IcaoID, string Iata, string CityName, string StateName, string CountryCD, string CountryName, string AirportName, string OffsetToGMT, string Filter);
        
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid> GetAllAirportListForGrid(bool IsDisplayInctive);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit> GetAllAirportListForGridLimit(int Limit, bool IsDisplayInctive);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount> GetAllAirportListForGridCount(bool IsDisplayInctive);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor> GetAirportForFuelVendor();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO> GetAirportByAirportIDFBO(Int64 AirportID);

        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.Airport> UpdateWorldClock(string AirportID, bool IsWorldClock); 

        #endregion
        #region "Vendor Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> AddVendorMaster(Data.MasterCatalog.Vendor vendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> UpdateVendorMaster(Data.MasterCatalog.Vendor vendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> DeleteVendorMaster(Data.MasterCatalog.Vendor vendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetVendorMasterLock(Data.MasterCatalog.Vendor vendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetVendorMaster();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor> GetVendorInfo();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetVendorMasterInfo(Data.MasterCatalog.Vendor vendor);
        #endregion
        #region "Vendor Contact Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> AddVendorContactMaster(Data.MasterCatalog.VendorContact vendorContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> UpdateVendorContactMaster(Data.MasterCatalog.VendorContact vendorContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> DeleteVendorContactMaster(Data.MasterCatalog.VendorContact vendorContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> GetVendorContactMasterLock(Data.MasterCatalog.VendorContact vendorContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> GetVendorContactMaster();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorContact> GetVendorContactMasterInfo(Data.MasterCatalog.GetAllVendorContact vendorContact);
        #endregion
        #region "Catering Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters> GetAllCateringByAirportIDWithFilters(long AirportID, long CateringID, string CateringCD, bool FetchChoiceOnly, bool FetchActiveOnly);
        #endregion
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CateringService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Catering></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Catering> AddCatering(Data.MasterCatalog.Catering CateringData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CateringService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Catering></returns>        
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Catering> UpdateCatering(Data.MasterCatalog.Catering CateringData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CateringService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Catering></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Catering> DeleteCatering(Data.MasterCatalog.Catering CateringData);

        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Catering></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID> GetAllCateringByAirportID(long airportID);

        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Catering></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD> GetCateringByAirportIDANDCateringCD(long airportID, string CateringCD);

        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Catering></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Catering> GetCateringList();
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.GetAllCatering></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCatering> GetCateringListInfo();
        /// <summary>
        /// To fetch records based on cateringid
        /// </summary>
        /// <param name="CateringID"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID> GetAllCateringByCateringID(long CateringID);
        #endregion
        #region "Transport Manager"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="TransportService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Transport></returns>
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters> GetTransportByAirportIDWithFilters(long AirportID, long TransportID, string TransportCD, bool IsChoice, bool IsInActive);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Transport> AddTransport(Data.MasterCatalog.Transport TransportData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="TransportService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Transport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Transport> UpdateTransport(Data.MasterCatalog.Transport TransportData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="TransportService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Transport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Transport> DeleteTransport(Data.MasterCatalog.Transport TransportData);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Transport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Transport> GetTransportList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportID> GetTransportByAirportID(long AirportID);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportID> GetTransportByTransportID(long TransportID);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportCode> GetTransportByTransportCode(long AirportID, string Transportcd);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.GetAllTransport></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllTransport> GetTransportListInfo();
        #endregion
        #region "FleetGroupOrderDetailMaster"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder> AddFleetGroupOrderType(Data.MasterCatalog.FleetGroupOrder fleetgrouporderttype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetLsavailableFleet> GetLsavailableList(Int64 FleetGroupID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetLsselectedFleet> GetLsselectedList(Int64 FleetGroupID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder> DeleteFleetGroupOrderType(Data.MasterCatalog.FleetGroupOrder fleetgrouporderttype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetGroupID> GetFleetGroupID(string FleetGroupCD);
        #endregion
        #region "TravelCoordinator Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> AddTravelCoordinatorMaster(Data.MasterCatalog.TravelCoordinator travelCoordinator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> UpdateTravelCoordinatorMaster(Data.MasterCatalog.TravelCoordinator travelCoordinator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> DeleteTravelCoordinatorMaster(Data.MasterCatalog.TravelCoordinator travelCoordinator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorMasterLock(Data.MasterCatalog.TravelCoordinator travelCoordinator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorMaster();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorMasterInfo(Data.MasterCatalog.TravelCoordinator travelCoordinator);
        #endregion
        #region "Exchange Rate"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> GetExchangeRate();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> UpdateExchangeRate(Data.MasterCatalog.ExchangeRate ExchangeRateData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> DeleteExchangeRate(Data.MasterCatalog.ExchangeRate ExchangeRateData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> GetExchangeRateLock(Data.MasterCatalog.ExchangeRate ExchangeRateData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> AddExchangeRate(Data.MasterCatalog.ExchangeRate ExchangeRateData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> GetExchangeRateInfo(Data.MasterCatalog.ExchangeRate ExchangeRateData);
        #endregion "Exchange Rate"
        #region "Passenger Group Order Detail"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPaxAvailableList> GetPaxAvailableList(Int64 customerId, Int64 paxCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPaxSelectedList> GetPaxSelectedList(Int64 customerId, Int64 paxCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder> AddPaxGroupOrder(Data.MasterCatalog.PassengerGroupOrder paxGroupOrder);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder> DeletePaxGroupOrder(Data.MasterCatalog.PassengerGroupOrder paxGroupOrder);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID> GetPassengerGroupbyGroupID(Int64 PassengerGroupID);
        #endregion
        #region "Crew Duty Rules Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> GetCrewDutyRulesWithFilters(long CrewDutyRuleId, string CrewDutyRuleCD, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> AddCrewDutyRule(Data.MasterCatalog.CrewDutyRules CrewDutyRule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> UpdateCrewDutyRule(Data.MasterCatalog.CrewDutyRules CrewDutyRule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> DeleteCrewDutyRule(Data.MasterCatalog.CrewDutyRules CrewDutyRule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> GetCrewDutyRuleList();
        #endregion
        #region "Passenger Additional Info"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> AddPassengerAdditionalInfo(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> GetPassengerAdditionalInfoList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> UpdatePassengerAdditionalInfo(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> DeletePassengerAdditionalInfo(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> GetPassengerAdditionalInfoLock(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo);
        #endregion
        #region "Department Manager"

        #region "Department Group Manager"
        #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters> GetDepartmentByWithFilters(string DepartmentCD, long DepartmentID, long ClientID, bool FetchInactiveOnly);
        #endregion

        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> AddDepartment(Data.MasterCatalog.Department DepartmentData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> UpdateDepartment(Data.MasterCatalog.Department DepartmentData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> DeleteDepartment(Data.MasterCatalog.Department DepartmentData);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDepartmentLock(Data.MasterCatalog.Department DepartmentData);
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDepartmentList();
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartments> GetAllDepartmentList();
        /// <summary>
        ///  Added by Scheduling calendar team to get Department Entity by Department Code.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult> GetDepartmentByDepartmentCD(string departmentCode);
        /// <summary>
        /// Method to fetch Department record by DepartmentCD
        /// </summary>
        /// <param name="DepartmentCD"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD> GetAllDeptByDeptCD(string DepartmentCD);
        #endregion
        #region "Department Authorization Manager"
         #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
         [OperationContract]
          Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationWithFilters(long ClientId, long DepartmentId, long LongAuthorizationId, string AuthorizationCD, bool FetchActiveOnly);
        #endregion


        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> AddDepartmentAuthorization(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> UpdateDepartmentAuthorization(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> DeleteDepartmentAuthorization(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationLock(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData);
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationList();
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetAllDeptAuthorizationList();
        #endregion
        #region "GetAllDeptAuth Manager"
        ///// <summary>
        ///// Interface to insert records into table
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> AddGetAllDeptAuth(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorizationData);
        ///// <summary>
        ///// Interface to update records in table
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> UpdateGetAllDeptAuth(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorizationData);
        ///// <summary>
        ///// Interface to update records as delete in table
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> DeleteGetAllDeptAuth(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorizationData);
        ///// <summary>
        ///// Interface to lock data in table when editing
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetAllDeptAuthock(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorizationData);
        ///// <summary>
        ///// Interface to fetch all records from table
        ///// </summary>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetAllDeptAuthorizationList();
        #endregion
        #region "GetAllAirport Manager"
        ///// <summary>
        ///// Interface to insert records into table
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> AddGetAllAirport(Data.MasterCatalog.GetAllAirport AirportData);
        ///// <summary>
        ///// Interface to update records in table
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> UpdateGetAllAirport(Data.MasterCatalog.GetAllAirport AirportData);
        ///// <summary>
        ///// Interface to update records as delete in table
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> DeleteGetAllAirport(Data.MasterCatalog.GetAllAirport AirportData);
        ///// <summary>
        ///// Interface to lock data in table when editing
        ///// </summary>
        ///// <param name="DepartmentAuthorizationData"></param>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAllAirporthock(Data.MasterCatalog.GetAllAirport AirportData);
        ///// <summary>
        ///// Interface to fetch all records from table
        ///// </summary>
        ///// <returns></returns>
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAllAirportList();
        #endregion
        #region "PaymentType Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetAllPaymentTypeWithFilters(long PaymentTypeID, string PaymenttypeCD, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetPaymentType();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> UpdatePaymentType(Data.MasterCatalog.PaymentType paymentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> DeletePaymentType(Data.MasterCatalog.PaymentType paymentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetPaymentTypeLock(Data.MasterCatalog.PaymentType paymentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> AddPaymentType(Data.MasterCatalog.PaymentType paymentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetPaymentTypeInfo(Data.MasterCatalog.PaymentType paymentType);
        #endregion
        #region "Crew Manager"
        #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list based on parameters
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>        
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters> GetAllCrewWithFilters(long ClientID, bool FetchActiveOnly, bool IsRotaryWing, bool IsFixedWing, string ICAOID, long CrewGroupId, long CrewId, string CrewCD);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewforDisplay> GetCrewforDisplay(long CrewId);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> UpdateCrewDisplay(long CrewID, bool IsDisplay);

        

        /// <summary>
        /// Get Other Crew Duty log Info based on CrewID and AircraftID
        /// </summary>
        /// <param name="CrewID"></param>
        /// <param name="AircraftID"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog> GetOtherCrewDutyLog(long CrewId, long AircraftID);
         #endregion
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> AddCrew(Data.MasterCatalog.Crew Crew);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> UpdateCrew(Data.MasterCatalog.Crew Crew);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> DeleteCrew(Data.MasterCatalog.Crew Crew);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetCrewLock(Data.MasterCatalog.Crew Crew);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrew> GetCrewList();
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewForGrid> GetAllCrewForGrid();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewByCrewID> GetCrewbyCrewId(Int64 CrewID);
        /// <summary>
        /// Interface to fetch records from table based on parameters
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetCrewInfo(); //Data.MasterCatalog.Crew Crew
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours> GetCurrentFltHoursInfo(Int64 CrewID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency> GetAllCrewCurrency(Int64 CrewID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewID> GetCrewID(string CrewCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew> GetCrewGroupBasedCrewInfo(Int64 CrewGroupID);


        #endregion
        #region "Crew Roster Additional Info"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="crewRosterAddInfo"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> AddCrewRosterAddInfo(Data.MasterCatalog.CrewInformation crewRosterAddInfo);



        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> AddforAllCrewMembers(Data.MasterCatalog.CrewInformation crewRosterAddInfo);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> UpdateforAllCrewMembers(Data.MasterCatalog.CrewInformation crewRosterAddInfo);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> GetCrewRosterAddInfoList();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="crewRosterAddInfo"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> UpdateCrewRosterAddInfo(Data.MasterCatalog.CrewInformation crewRosterAddInfo);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="crewRosterAddInfo"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> DeleteCrewRosterAddInfo(Data.MasterCatalog.CrewInformation crewRosterAddInfo);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="crewRosterAddInfo"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> GetCrewRosterAddInfoLock(Data.MasterCatalog.CrewInformation crewRosterAddInfo);
        #endregion
       
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="DeptGroupData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> AddDeptGroup(Data.MasterCatalog.DepartmentGroup DeptGroupData);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="DeptGroupData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> UpdateDeptGroup(Data.MasterCatalog.DepartmentGroup DeptGroupData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="DeptGroupData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> DeleteDeptGroup(Data.MasterCatalog.DepartmentGroup DeptGroupData);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="DeptGroupData"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> GetDeptGroupLock(Data.MasterCatalog.DepartmentGroup DeptGroupData);
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> GetDeptGroupList();
        /// <summary>
        /// Interface to fetch all records from table
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup> GetDeptGroupInfo();
        #endregion
        #region "Department Group Order Detail"
        /// <summary>
        /// Interface to fetch available records from table
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="DepartmentCode"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDeptAvailableList(Int64 CustomerId, Int64 DepartmentCode);
        /// <summary>
        /// Interface to fetch selected records from table
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="DepartmentCode"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDeptSelectedList(Int64 CustomerId, Int64 DepartmentCode);
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="deptGroupOrder"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> AddDeptGroupOrder(Data.MasterCatalog.DepartmentGroupOrder deptGroupOrder);
        /// <summary>
        /// Interface to update records as deleted in table
        /// </summary>
        /// <param name="deptGroupOrder"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> DeleteDeptGroupOrder(Data.MasterCatalog.DepartmentGroupOrder deptGroupOrder);
        #endregion
        #region "FuelLocator Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters> GetFuelLocatorWithFilters(long FuelLocatorId, string FuelLocatorCD, long ClientId, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> GetFuelLocator();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> UpdateFuelLocator(Data.MasterCatalog.FuelLocator fuelLocator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> DeleteFuelLocator(Data.MasterCatalog.FuelLocator fuelLocator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> GetFuelLocatorLock(Data.MasterCatalog.FuelLocator fuelLocator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> AddFuelLocator(Data.MasterCatalog.FuelLocator fuelLocator);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFuelLocator> GetFuelLocatorInfo();
        #endregion
        #region "Fleet Profile Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters> GetAllFleetWithFilters(long ClientID, bool FetchActiveOnly, string IsFixedRotary, string ICAOID, long VendorID, string TailNum, long FleetID);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Fleet> AddFleetProfileType(Data.MasterCatalog.Fleet fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Fleet> GetFleetProfileList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Fleet> UpdateFleetProfileType(Data.MasterCatalog.Fleet fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Fleet> DeleteFleetProfileType(Data.MasterCatalog.Fleet fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Fleet> GetFleetProfileTypeLock(Data.MasterCatalog.Fleet fleettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleet> GetFleet();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetID> GetFleetID(string TailNum, string SerialNum);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetIDInfo> GetFleetIDInfo(Int64 FleetID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetSearch> GetFleetSearch(bool IsInActive, string IsFixedRotary, Int64 HomeBaseID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForPopup> GetAllFleetForPopupList();
        #endregion

        #region "Fleet New Charter Rate Manager"
        /// <summary>
        /// interface for AddFleetNewCharterRate
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> AddFleetNewCharterRate(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate);


        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> UpdateCharterNewRateTax(Int64 FleetID, Int64 AircraftID, bool DailyUsageTax, bool LandingFeeTax, string LastupdateID, DateTime LastUpTS, Decimal MinimumDailyRequirement, Decimal StandardCrewIntl, Decimal StandardCrewDOM, Int64 CQCustomerID, Decimal MarginalPercentage);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAll(Int64 NewAircraftID, Int64 OldAircraftID);


        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAllByFleet(Int64 NewAircraftID, Int64 OldAircraftID);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAllByCQ(Int64 NewAircraftID, Int64 OldAircraftID);

        /// <summary>
        /// interface for UpdateFleetNewCharterRate
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> UpdateFleetNewCharterRate(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate);

        /// <summary>
        /// interface for GetAllFleetNewCharterRate
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate> GetAllFleetNewCharterRate();

        /// <summary>
        /// interface for GetFleetNewCharterRateLock
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> GetFleetNewCharterRateLock(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate);

        /// <summary>
        /// interface for DeleteFleetNewCharter
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> DeleteFleetNewCharter(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate);

        /// <summary>
        /// interface for GetFleetNewCharterByFleetNewCharterID
        /// </summary>
        /// <param name="FleetNewCharterRateID"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID> GetFleetNewCharterByFleetNewCharterID(Int64 FleetNewCharterRateID);
        #endregion

        #region "FBO Manager"
        #region For Performance Optimization
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AirportID"></param>
        /// <param name="FBOID"></param>
        /// <param name="FBOCD"></param>
        /// <param name="FetchChoiceOnly"></param>
        /// <param name="FetchUVAIROnly"></param>
        /// <param name="FetchActiveOnly"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters> GetFBOByAirportWithFilters(long AirportID, long FBOID, string FBOCD, bool FetchChoiceOnly, bool FetchUVAIROnly, bool FetchActiveOnly);
        #endregion
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.FBO></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FBO> GetFBOList();
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="FBOService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FBO></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FBO> UpdateFBO(Data.MasterCatalog.FBO FBOData);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="FBOService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FBO></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FBO> DeleteFBO(Data.MasterCatalog.FBO FBOData);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="FBOService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FBO></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FBO> GetFBOLock(Data.MasterCatalog.FBO FBOData);
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="FBOService"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FBO></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FBO> AddFBO(Data.MasterCatalog.FBO FBOData);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.FBO></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBO> GetFBOInfo();

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID> GetAllFBOByAirportID(long airportID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD> GetFBOByAirportIDAndFBOCD(long airportID, string fboCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID> GetAllFBOByFBOID(long FBOID);
        #endregion
        #region "CrewGroupManager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters> GetCrewGroupWithFilters(long CrewGroupID, string CrewGroupCD, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> AddCrewType(Data.MasterCatalog.CrewGroup Crewtype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> GetCrewGroupList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> UpdateCrewType(Data.MasterCatalog.CrewGroup Crewtype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> DeleteCrewType(Data.MasterCatalog.CrewGroup Crewtype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> GetCrewTypeLock(Data.MasterCatalog.CrewGroup Crewtype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewGroup> GetCrewGroupInfo();
        #endregion
        #region "CrewGroupOrderDetailManager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder> AddCrewGroupOrderType(Data.MasterCatalog.CrewGroupOrder crewgrouporderttype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetcrewavailableList(Int64 CustomerID, Int64 GroupCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetcrewselectedList(Int64 CustomerID, Int64 GroupCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder> DeleteCrewGroupOrderType(Data.MasterCatalog.CrewGroupOrder crewgrouporderttype);
        #endregion
        #region "Budget Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Budget> AddBudget(Data.MasterCatalog.Budget budgettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Budget> GetBudgetList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllBudget> GetBudgetListInfo();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Budget> UpdateBudget(Data.MasterCatalog.Budget budgettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Budget> DeleteBudget(Data.MasterCatalog.Budget budgettype);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Budget> GetBudgetLock(Data.MasterCatalog.Budget budgettype);
        #endregion
        #region "Crew Check List"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> AddCrewChecklist(Data.MasterCatalog.CrewCheckList crewChecklist);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> AddChecklistforAllCrewMembers(Data.MasterCatalog.CrewCheckList crewChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> UpdateChecklistforAllCrewMembers(Data.MasterCatalog.CrewCheckList crewChecklist);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> GetCrewChecklistList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> UpdateCrewChecklist(Data.MasterCatalog.CrewCheckList crewChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> DeleteCrewChecklist(Data.MasterCatalog.CrewCheckList crewChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> GetCrewChecklistLock(Data.MasterCatalog.CrewCheckList crewChecklist);
        #endregion
        #region "Crew Duty Type Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> AddCrewDutyType(Data.MasterCatalog.CrewDutyType crewDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> UpdateCrewDutyType(Data.MasterCatalog.CrewDutyType crewDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> DeleteCrewDutyType(Data.MasterCatalog.CrewDutyType crewDuty);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> GetCrewDutyTypeList();
        #endregion
        #region "Hotel Manager"
        #region For Performance Optimization
         [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters> GetHotelsByAirportIDWithFilters(long AirportID, long HotelID, string HotelCD, bool FetchactiveOnly, bool FetchIsCrewOnly, bool FetchIsPaxOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Hotel> AddHotel(Data.MasterCatalog.Hotel hotel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Hotel> GetHotelList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Hotel> UpdateHotel(Data.MasterCatalog.Hotel hotel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Hotel> DeleteHotel(Data.MasterCatalog.Hotel hotel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Hotel> GetHotelLock(Data.MasterCatalog.Hotel hotel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllHotel> GetHotelInfo();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportID> GetHotelsByAirportID(long airportID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelID> GetHotelByHotelID(long hotelID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelCode> GetHotelByHotelCode(long airportID, string hotelCd);
        #endregion
        #region "Fleet Profile Information"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> AddFleetProfileInformation(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> UpdateFleetProfileInformation(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> DeleteFleetProfileInformation(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> GetFleetProfileInformationList();
        #endregion
        #region "Payable Vendor Manager"
        [OperationContract]
        #region For Performance Optimization
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters> GetAllVendorWithFilters(string VendorType, long VendorID, string VendorCD, string FetchHomebaseOnly, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> AddPayableVendorMaster(Data.MasterCatalog.Vendor payableVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> UpdatePayableVendorMaster(Data.MasterCatalog.Vendor payableVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> DeletePayableVendorMaster(Data.MasterCatalog.Vendor payableVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetPayableVendorMasterLock(Data.MasterCatalog.Vendor payableVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetPayableVendorMaster();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor> GetPayableVendorInfo();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetPayableVendorMasterInfo(Data.MasterCatalog.Vendor payableVendor);
        #endregion
        #region "Crew Roster Additional Info - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> AddCrewDefinition(Data.MasterCatalog.CrewDefinition CrewDefinition);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="crewDefinition"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> UpdateCrewDefinition(Data.MasterCatalog.CrewDefinition CrewDefinition);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> DeleteCrewDefinition(Data.MasterCatalog.CrewDefinition CrewDefinition);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewDefinition> GetCrewDefinitionList(Data.MasterCatalog.GetCrewDefinition CrewDefinition);
        #endregion
        #region "Crew Rating - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> AddCrewRating(Data.MasterCatalog.CrewRating CrewRating);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> UpdateCrewRating(Data.MasterCatalog.CrewRating CrewRating);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> DeleteCrewRating(Data.MasterCatalog.CrewRating CrewRating);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewRating> GetCrewRatingList(Data.MasterCatalog.GetCrewRating CrewRating);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember> GetCalculateCrewRating(Int64 AircraftCD, Int64 CrewID);
        #endregion
        #region "Crew Checklist Date - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> AddCrewCheckListDate(Data.MasterCatalog.CrewCheckListDetail CrewChecklist);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> UpdateCrewCheckListDate(Data.MasterCatalog.CrewCheckListDetail CrewChecklist);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> DeleteCrewCheckListDate(Data.MasterCatalog.CrewCheckListDetail CrewChecklist);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewCheckListDate> GetCrewCheckListDate(Data.MasterCatalog.GetCrewCheckListDate CrewChecklist);
        #endregion
        #region "Crew Aircraft Assigned - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> AddCrewAircraftAssigned(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> UpdateCrewAircraftAssigned(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> DeleteCrewAircraftAssigned(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> GetAircraftAssignedList(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> GetSelectedAircraftAssignedList(Data.MasterCatalog.GetAllCrewAircraftAssign CrewAircraftAssigned);
        #endregion
        #region "Crew Passport - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> AddCrewPassport(Data.MasterCatalog.CrewPassengerPassport CrewPassport);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> UpdateCrewPassport(Data.MasterCatalog.CrewPassengerPassport CrewPassport);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> DeleteCrewPassport(Data.MasterCatalog.CrewPassengerPassport CrewPassport);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetCrewPassportList(Data.MasterCatalog.CrewPassengerPassport CrewPassport);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPassport> GetCrewPassportListInfo(Data.MasterCatalog.CrewPassengerPassport CrewPassport);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPassportbyPassportId> GetPassportbyPassportId(Int64 PassportID, Boolean IsCrewPassport);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetPassengerPassportByPassengerRequestorID(Int64 PassengerRequestorID);
        #endregion
        #region "Crew Passenger Visa - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> AddCrewPaxVisa(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> UpdateCrewPaxVisa(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> DeleteCrewPaxVisa(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> GetCrewPaxVisaList(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa> GetCrewVisaListInfo(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa);
        #endregion
        #region "Sifl Rate Catalog"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FareLevel></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> AddFareLevel(Data.MasterCatalog.FareLevel SiflFareLevel);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.FareLevel></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> GetFareLevel();
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FareLevel></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> UpdateFareLevel(Data.MasterCatalog.FareLevel SiflFareLevel);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FareLevel></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> DeleteFareLevel(Data.MasterCatalog.FareLevel SiflFareLevel);
        /// <summary>
        /// Interface to lock data in table when editing
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.FareLevel></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> GetFareLevelLock(Data.MasterCatalog.FareLevel SiflFareLevel);

        /// <summary>
        /// Check SiflRate in the Specified Dates
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CheckSiflDate> CheckSiflDate(DateTime StartDate, DateTime EndDate);
        #endregion
        #region "Client Code Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientWithFilters(Int64 ClientID, string ClientCD, bool IsActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> AddClientCode(Data.MasterCatalog.Client clientCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientCodeList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> UpdateClientCode(Data.MasterCatalog.Client clientCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> DeleteClientCode(Data.MasterCatalog.Client clientCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientCodeLock(Data.MasterCatalog.Client clientCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Client> UpdateClientCodeAll(Int64 ClientID, string UserMaster, string Fleet, string Crew, string Passenger, string Department, string FlightCategory, string FlightPurpose, string FuelLocator);
        /// <summary>
        ///  Added by Scheduling calendar team to get Client Entity by Client Code.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ClientByClientCDResult> GetClientByClientCD(string clientCode);
        #endregion

        #region "Crew Checklist Date - Sub Section of Crew Roster"
        /// <summary>
        /// Interface to insert records into table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> AddPaxCheckListDate(Data.MasterCatalog.PassengerCheckListDetail CrewChecklist);
        /// <summary>
        /// Interface to update records in table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> UpdatePaxCheckListDate(Data.MasterCatalog.PassengerCheckListDetail CrewChecklist);
        /// <summary>
        /// Interface to update records as delete in table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> DeletePaxCheckListDate(Data.MasterCatalog.PassengerCheckListDetail CrewChecklist);
        /// <summary>
        /// Interface to fetch records from table
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPaxChecklistDate> GetPaxCheckListDate(Data.MasterCatalog.GetPaxChecklistDate CrewChecklist);
        #endregion
        #region "Runway Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Runway> AddRunwayMaster(Data.MasterCatalog.Runway runway);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Runway> UpdateRunwayMaster(Data.MasterCatalog.Runway runway);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Runway> DeleteRunwayMaster(Data.MasterCatalog.Runway runway);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayMasterLock(Data.MasterCatalog.Runway runway);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayMaster();
        /// <summary>
        /// Interface to fetch a single Airport Record
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Runway></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayByAirportID(Int64 AirportID);
        #endregion
        #region "TripManagerCheckListGroup Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetAllTripmanagerChecklistGroupWithFilters(long CheckGroupID, string CheckGroupCD, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> AddTripManagerChecklistGroup(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> UpdateTripManagerChecklistGroup(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> DeleteTripManagerChecklistGroup(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetTripManagerChecklistGroupList();
        #endregion
        #region "TripManagerCheckList Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters> GetTripManagerCheckListWithFilters(long CheckListID, string CheckListCD, long CheckGroupID, bool IsInactive);
        #endregion

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> AddTripManagerChecklist(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> UpdateTripManagerChecklist(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> DeleteTripManagerChecklist(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> GetTripManagerChecklistList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList> GetTripManagerChecklistListInfo();
        #endregion
        #region "Fleet Component"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> AddFleetComponentType(Data.MasterCatalog.FleetComponent fleetcomponentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> GetFleetComponentList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> UpdateFleetComponentType(Data.MasterCatalog.FleetComponent fleetcomponentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> DeleteFleetComponentType(Data.MasterCatalog.FleetComponent fleetcomponentType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> GetFleetComponentLock(Data.MasterCatalog.FleetComponent fleetcomponentType);
        #endregion
        #region "Fleet Chargerate History"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> AddFleetChargeRateType(Data.MasterCatalog.FleetChargeRate fleetchargerateType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> GetFleetChargeRateList(Int64 FleetID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> CalculateChargeRate(Data.MasterCatalog.FleetChargeRate fleetChargeRate);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> UpdateFleetChargeRateType(Data.MasterCatalog.FleetChargeRate fleetchargerateType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> DeleteFleetChargeRateType(Data.MasterCatalog.FleetChargeRate fleetchargerateType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> GetFleetChargeRateLock(Data.MasterCatalog.FleetChargeRate fleetchargerateType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists> GetFleetChargeRateExists(DateTime BeginRateDT, DateTime EndRateDT, Int64 FleetID, Int64 FleetChargeRateID);
        #endregion
        #region "CustomerAddress"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> AddCustomerAddress(Data.MasterCatalog.CustomAddress customerAddress);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCustomerAddress> GetCustomerAddress();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> UpdateCustomerAddress(Data.MasterCatalog.CustomAddress customerAddress);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> DeleteCustomerAddress(Data.MasterCatalog.CustomAddress customerAddress);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> GetCustomerAddressLock(Data.MasterCatalog.CustomAddress customerAddress);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCustomerID> GetCustomerAddressID();
        #endregion
        #region "Fleet Forecast"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> AddFleetForecastType(Data.MasterCatalog.FleetForeCast fleetforecastType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> GetFleetForecastList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> UpdateFleetForecastType(Data.MasterCatalog.FleetForeCast fleetforecastType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> DeleteFleetForecastType(Data.MasterCatalog.FleetForeCast fleetforecastType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> GetFleetForecastLock(Data.MasterCatalog.FleetForeCast fleetforecastType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForecast> GetFleetForecastInfo(Int64 FleetID, Int32 year);
        #endregion
        #region "Passenger Type Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllPassenger> GetPassengerList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> AddPassengerType(Data.MasterCatalog.Passenger passenger);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> UpdatePassengerType(Data.MasterCatalog.Passenger passenger);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> DeletePassengerType(Data.MasterCatalog.Passenger passenger);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> GetPassengerLock(Data.MasterCatalog.Passenger passenger);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID> GetPassengerbyPassengerRequestorID(long PassengerRequestorID);
        #endregion
        #region "ConversionTable Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> AddConversionTable(Data.MasterCatalog.ConversionTable conversionTable);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> UpdateConversionTable(Data.MasterCatalog.ConversionTable conversionTable);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> DeleteConversionTable(Data.MasterCatalog.ConversionTable conversionTable);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> GetConversionTableList();
        #endregion
        #region "FlightLogGet Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> AddGetAllFlightLog(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> UpdateGetAllFlightLog(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> DeleteGetAllFlightLog(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetAllFlightLogList(Data.MasterCatalog.TSFlightLog TSFlightLog);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLog> GetAllPilotLogList(Data.MasterCatalog.TSFlightLog TSFlightLog);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> ResetFlightLog(Data.MasterCatalog.TSFlightLog TSFlightLog);
        #endregion
        #region "DefinitionLog Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> AddTSDefinitionLog(Data.MasterCatalog.TSDefinitionLog DefinitionData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> UpdateTSDefinitionLog(Data.MasterCatalog.TSDefinitionLog DefinitionData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> DeleteTSDefinitionLog(Data.MasterCatalog.TSDefinitionLog DefinitionData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> GetTSDefinitionLogList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLogReset> GetPilotLogReset();
        #endregion
        #region "FlightLog Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> AddTSFlightLog(Data.MasterCatalog.TSFlightLog FlightLogData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> UpdateTSFlightLog(Data.MasterCatalog.TSFlightLog FlightLogData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> DeleteTSFlightLog(Data.MasterCatalog.TSFlightLog FlightLogData);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> GetTSFlightLogList();
        #endregion
        #region "Travel Coordinator Requestor Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> AddTravelCoordinatorRequestor(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> UpdateTravelCoordinatorRequestor(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> DeleteTravelCoordinatorRequestor(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestor();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestorSelectedList(Int64 travelCoordCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestorInfo(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTravelCoordinator> GetTravelCoordinatorList();
        #endregion
        #region "Travel Coordinator Fleet Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> AddTravelCoordinatorFleet(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> UpdateTravelCoordinatorFleet(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleetSelectedList(Int64 travelCoordCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> DeleteTravelCoordinatorFleet(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleet();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleetInfo(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet);
        #endregion
        #region "Passenger Requestor Manager"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters> GetAllPassengerWithFilters(long ClientId, long  CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result> GetAllPassengerTelerikPagedGrid(long ClientId, long CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId, int start, int end, string sort, string PassengerName, string DepartmentCD, string DepartmentName, string PhoneNum);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> AddPassengerRequestor(Data.MasterCatalog.Passenger paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> UpdatePassengerRequestor(Data.MasterCatalog.Passenger paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> DeletePassengerRequestor(Data.MasterCatalog.Passenger paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> GetPassengerRequestorLock(Data.MasterCatalog.Passenger paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> GetPassengerRequestorList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.Passenger> UpdatePassportForFutureTrip(Int64 PassportID, Int64 PassengerID, Int64 CrewID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerAssociate> GetPassengerAssociate();
        /// <summary>
        /// Added by Scheduling calendar team to get PassengerRequestor Entity by PassengerRequestor Code.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerRequestorResult> GetPassengerRequestorByPassengerRequestorCD(string requestorCode);

        #endregion
        #region "Passenger Additional Information"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> AddPassengerAddlInfo(Data.MasterCatalog.PassengerAdditionalInfo paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> UpdatePassengerAddlInfo(Data.MasterCatalog.PassengerAdditionalInfo paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> DeletePassengerAddlInfo(Data.MasterCatalog.PassengerAdditionalInfo paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetPassengerAddlInfoLock(Data.MasterCatalog.PassengerAdditionalInfo paxGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetPassengerAddlInfoList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetPassengerAddlInfoListReqID(Int64 RequestorID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> AddforAllPaxMembers(Data.MasterCatalog.PassengerInformation crewRosterAddInfo);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> UpdateforAllPaxMembers(Data.MasterCatalog.PassengerInformation crewRosterAddInfo);

        #endregion
        #region "Passenger Passport Information"
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.PassengerPassportInfo> AddPassengerPassportInfo(Data.MasterCatalog.PassengerPassportInfo paxGroup);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.PassengerPassportInfo> UpdatePassengerPassportInfo(Data.MasterCatalog.PassengerPassportInfo paxGroup);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.PassengerPassportInfo> DeletePassengerPassportInfo(Data.MasterCatalog.PassengerPassportInfo paxGroup);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.PassengerPassportInfo> GetPassengerPassportInfoLock(Data.MasterCatalog.PassengerPassportInfo paxGroup);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.MasterCatalog.PassengerPassportInfo> GetPassengerPassportInfoList();
        #endregion
        #region "Fuel Vendor Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendor();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFuelVendorText> GetfuelVendorText(Int64 FuelVendorID, decimal GallonFrom, decimal GallonTo, Int64 DepartureICAO, string FBOName);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> UpdateFuelVendor(Data.MasterCatalog.FuelVendor FuelVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> DeleteFuelVendor(Data.MasterCatalog.FuelVendor FuelVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorLock(Data.MasterCatalog.FuelVendor FuelVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> AddFuelVendor(Data.MasterCatalog.FuelVendor FuelVendor);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorInfoByFilter(long? fuelVendorId, string fuelVendorCode);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> DeleteFlightpakFuel(Data.MasterCatalog.FlightpakFuel FlightpakFuel);
        [OperationContract]
        long AddFuelVendorBulk(Data.MasterCatalog.FuelVendor FuelVendor);
        #endregion
        #region "Fleet Engine Airframe Info"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> AddFleetInformationType(Data.MasterCatalog.FleetInformation fleetinformationType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> GetFleetInformationTypeLock(Data.MasterCatalog.FleetInformation fleetinformationType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> GetFleetInformation(Int64 tailNum);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> UpdateFleetInformation(Data.MasterCatalog.FleetInformation FleetInfo);
        #endregion
        #region "Fleet International Data"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> AddFleetInternationalType(Data.MasterCatalog.FleetPair fleetinternationalType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> GetFleetInternationalTypeLock(Data.MasterCatalog.FleetPair fleetinternationalType);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> GetFleetPairList(Int64 tailNum);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> UpdateFleetinternationaldata(Data.MasterCatalog.FleetPair fleetinternationaldata);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetPair> GetFleetPair(Int64 FleetID);
        #endregion
        #region "FileWarehouseManager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> AddFWHType(Data.MasterCatalog.FileWarehouse FileWH);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> GetFileWarehouseList(string RecordType, Int64 RecordID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> UpdateFWHType(Data.MasterCatalog.FileWarehouse FileWH);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> DeleteFWHType(Data.MasterCatalog.FileWarehouse FileWH);
        #endregion
        #region "RoomType Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.RoomType> GetRoomList();
        #endregion
        #region "RoomDescription Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.RoomDescription> GetRoomDescriptionList();
        #endregion
        #region "Fleet Profile Definition"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> AddFleetAddlInfo(Data.MasterCatalog.FleetProfileDefinition FleetAI);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> UpdateFleetAddlInfo(Data.MasterCatalog.FleetProfileDefinition FleetAI);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> DeleteFleetAddlInfo(Data.MasterCatalog.FleetProfileDefinition FleetAI);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> GetFleetAddlInfoList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition> GetFleetAdditionalInfoList();
        #endregion
        # region FleetInformationManager
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FleetByTailNumberResult> GetFleetByTailNumber(string tailNumber);
        # endregion
        #region "Crew, Fleet, Passenger, Department, FlightCatagory, FlightPurpose, FuelLocator for Client"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCrewForClient> GetCrewForClient();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFleetForClient> GetFleetForClient();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerForClient> GetPassengerForClient();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentForClient> GetDepartmentForClient();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient> GetFlightCategoryForClient();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient> GetFlightPurposeForClient();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient> GetFuelLocatorForClient();
        #endregion
        #region APIS
        [OperationContract]
        Business.Common.ReturnValue<List<string>> CompanyEAPISSubmit(string SenderID, string Password);
        #endregion
        #region "TripSheetReportHeader"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> AddTripSheetReportHeader(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> UpdateTripSheetReportHeader(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> DeleteTripSheetReportHeader(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetUserGroup> GetUserGroup();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail> GetTripSheetReportDetail();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> AddTripSheetReportDetail(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> UpdateTripSheetReportDetail(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> DeleteTripSheetReportDetail(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetReportSheetDetail> GetReportSheetDetail(Int64 TripSheetReportHeaderID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission> GetReportSheetDetailByUMPermission(Int64 TripSheetReportHeaderID);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> UpdateReportSheetDetail(Int64 ReportSheetHeaderID, List<int> ReportNumber);
        #endregion
        #region "SalesPerson"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> AddSalesPerson(Data.MasterCatalog.SalesPerson salesPerson);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPerson> GetSalesPersonList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> UpdateSalesPerson(Data.MasterCatalog.SalesPerson salesPerson);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> DeleteSalesPerson(Data.MasterCatalog.SalesPerson salesPerson);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> GetSalesPersonLock(Data.MasterCatalog.SalesPerson salesPerson);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPersonID> GetSalesPersonID();
        #endregion
        #region "LeadSource"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> AddLeadSource(Data.MasterCatalog.LeadSource leadSource);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> GetLeadSourceList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> UpdateLeadSource(Data.MasterCatalog.LeadSource leadSource);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> DeleteLeadSource(Data.MasterCatalog.LeadSource leadSource);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> GetLeadSourceLock(Data.MasterCatalog.LeadSource leadSource);
        #endregion
        #region "CQCustomer"
        #region For Performance Optimization
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters> GetCQCustomerWithFilters(long CQCustomerID, string CQCustomerCD, string FetchHomebaseOnly, bool FetchActiveOnly);
        #endregion
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> AddCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> GetCQCustomerList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> UpdateCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> DeleteCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> GetCQCustomerLock(Data.MasterCatalog.CQCustomer cqCustomer);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact> GetCQCustomerListInfo();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomer> GetCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD> GetCQCustomerByCQCustomerCD(Data.MasterCatalog.CQCustomer CQCustomer);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID> GetCQCustomerByCQCustomerID(Data.MasterCatalog.CQCustomer CQCustomer);
        #endregion
        #region "CQCustomerContact"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> AddCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> GetCQCustomerContactList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> UpdateCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> DeleteCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> GetCQCustomerContactLock(Data.MasterCatalog.CQCustomerContact cqCustomerContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContact> GetCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID> GetCQCustomerContactByCQCustomerContactID(Data.MasterCatalog.CQCustomerContact CQCustomerContact);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID> GetCQCustomerContactByCQCustomerID(Data.MasterCatalog.CQCustomerContact CQCustomerContact);
        #endregion
        #region "Fee Group Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> AddFeeGroup(Data.MasterCatalog.FeeGroup FeeGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> GetFeeGroupList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> UpdateFeeGroup(Data.MasterCatalog.FeeGroup FeeGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> DeleteFeeGroup(Data.MasterCatalog.FeeGroup FeeGroup);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> GetFeeGroupLock(Data.MasterCatalog.FeeGroup FeeGroup);
        #endregion
        #region "LostBusiness"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> AddLostBusiness(Data.MasterCatalog.CQLostBusiness lostBusiness);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> GetLostBusinessList();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> UpdateLostBusiness(Data.MasterCatalog.CQLostBusiness lostBusiness);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> DeleteLostBusiness(Data.MasterCatalog.CQLostBusiness lostBusiness);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> GetLostBusinessLock(Data.MasterCatalog.CQLostBusiness lostBusiness);
        #endregion
        #region "Fee Schedule"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> AddFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> UpdateFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> DeleteFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> GetFeeScheduleList(Data.MasterCatalog.FeeSchedule oFeeSchedule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetFeeSchedule> GetFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID> GetFeeScheduleByGroupID(Int64 feeGroupID);
        #endregion "Fee Schedule"
        #region "Flightpak Fuel"

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData> GetFlightpakFuelListByFilter(long? vendorId, string fboName, long? airportIcao, DateTime? effectiveDate, bool? isActive, int pageSize, int pageIndex);
        [OperationContract]
        Business.Common.ReturnValue<FlightpakFuel> GetFlightpakFuelListById(long flightpakFuelId);
        [OperationContract]
        Business.Common.ReturnValue<int> CountNumberOfFlightpakFuelRecordsForVendor(long fuelVendorId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> AddFlightpakFuel(Data.MasterCatalog.FlightpakFuel oFlightpakFuel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> UpdateFlightpakFuel(Data.MasterCatalog.FlightpakFuel oFlightpakFuel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> UpdateToHistoricalFlightpakFuel(Data.MasterCatalog.HistoricalFuelPrice oFlightpakFuel);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> DeleteToHistoricalFlightpakFuel(Data.MasterCatalog.HistoricalFuelPrice oFlightpakFuel);

        #endregion

        #region "Fuel Upload ServiceCalls"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> AddFuelFieldMaster(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> UpdateFuelFieldMaster(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> DeleteFuelFieldMaster(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> GetFuelFieldMasterList(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster);

        [OperationContract]
        Business.Common.ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> AddBulkFlightpakFuel(FuelFileData oFlightpakFuelList);
        [OperationContract]
        Business.Common.ReturnValue<FuelFileData> CheckFileDataByDataformat(FuelFileData objFuelFileData);
        [OperationContract]
        Business.Common.ReturnValue<Dictionary<string, string>> CheckFileFormat(String fileTitlesCSVformat);
        
        /// <summary>
        /// Get Previously saved CSV File format.
        /// </summary>
        /// <param name="objFuelFileData">FuelFileData</param>
        /// <param name="csvColumnsTitle">Comma delimited Columns Title</param>
        /// <returns></returns>
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetPreviousSavedFormatForVendor(FuelFileData objFuelFileData, List<string> csvColumnsTitle);

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> MatcheCSVFormatWithErrorLogForamt(FuelFileData objFuelFileData, List<string> csvColumnsTitle);


        #endregion "Fuel Upload ServiceCalls"

        #region "Fuel File XSLT Parser"

        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> AddFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> UpdateFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> DeleteFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetAllXsltParser();
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> AddFuelFileCsvHeaders(string[] headers, int xsltParserId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetXsltParserById(int xsltParserId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> DeleteOldCsvFieldsBasedOnXsltParserId(int xsltParserId);

        #endregion

        #region "FuelSavedFileFormat"
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> AddFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> UpdateFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> DeleteFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormatebasedOnFuelVendor(long fuelVendorId);
        [OperationContract]
        Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormatebasedOnFuelVendorCD(string fuelVendorCD);

        #endregion

    }
}
