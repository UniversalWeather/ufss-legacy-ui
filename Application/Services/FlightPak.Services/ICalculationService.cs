﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICalculationService" in both code and config file together.
    [ServiceContract]
    public interface ICalculationService
    {
        [OperationContract]
        double GetDistance(Int64 DepartAirportID, Int64 ArriveAirportID);

        [OperationContract]
        List<double> CalcBiasTas(long DepartAirportID, long ArriveAirportID, long AircraftID, string PowerSetting);

        [OperationContract]
        double GetWind(long DepartAirportID, long ArriveAirportID, int _windReliability, long AircraftID, string Quarter);

        [OperationContract]
        double GetWindBasedOnParameter(double aircraftWindAltitude, int _windReliability, string Quarter,
            double deplatdeg, double deplatmin, string lcDepLatDir,
            double deplngdeg, double deplngmin, string lcDepLngDir, long lnDepartZone,
            double arrlatdeg, double arrlatmin, string lcArrLatDir,
            double arrlngdeg, double arrlngmin, string lcArrLngDir, long lnArrivZone
            );

        [OperationContract]
        double GetHeading(double Slope, double DeltaLat, double DeltaLng);

        [OperationContract]
        double GetIcaoEte(double lnwind, double lnLndBias, double lntas, double lnToBias, double lnmiles, DateTime DateVal);

        [OperationContract]
        DateTime GetGMT(long AirportID, DateTime ldDtTm, bool IIFindGMT, bool IIGmt = false);

        [OperationContract]
        string CalcDomIntl(Int64 DepartAirportID, Int64 ArriveAirportID, Int64 HomebaseAirportID);
        
        [OperationContract]
        Business.Common.ReturnValue<Data.Calculations.PreflightHomeBaseSettings> GetPreflightHomeBaseSettings(Int64 HomeBaseID);



        #region "Postflight Calculation Methods"
        [OperationContract]
        Business.Common.ReturnValue<Data.Calculations.GetPOHomeBaseSetting> GetPostflightHomeBaseSetting(Int64 HomeBaseID);

        [OperationContract]
        Business.Common.ReturnValue<Data.Calculations.GetPOFleetChargeRate> GetFleetChargeRate(Int64 FleetID, DateTime CurrentDate);

        [OperationContract]
        Business.Common.ReturnValue<Data.Calculations.GetPOFleetProfile> GetFleetProfile(Int64 FleetID);

        [OperationContract]
        Business.Common.ReturnValue<Data.Calculations.GetPOSIFLRate> GetSIFLRate(DateTime CurrentDate);
        #endregion

    }
}
