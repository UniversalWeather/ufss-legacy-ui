﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Business.Calculations;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CalculationService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]  
    public class CalculationService : ICalculationService
    {
        /// <summary>
        /// Gets the departure AirportID & Arrival AirportID from UI & calculate Miles
        /// </summary>
        /// <param name="DepartAirportID"></param>
        /// <param name="ArriveAirportID"></param>
        /// <returns></returns>
        public double GetDistance(Int64 DepartAirportID, Int64 ArriveAirportID)
        {
            return Business.Calculations.CalculationManager.GetDistance(DepartAirportID, ArriveAirportID);
        }

        /// <summary>
        /// Gets the departure AirportID & Arrival AirportID ,AircraftID & PowerSetting from UI & calculate Bias
        /// </summary>
        /// <param name="DepartAirportID"></param>
        /// <param name="ArriveAirportID"></param>
        /// <param name="AircraftID"></param>
        /// <param name="PowerSetting"></param>
        /// <returns></returns>
        public List<double> CalcBiasTas(long DepartAirportID, long ArriveAirportID, long AircraftID, string PowerSetting)
        {
            return Business.Calculations.CalculationManager.CalcBiasTas(DepartAirportID, ArriveAirportID, AircraftID, PowerSetting);
        }
        /// <summary>
        /// Gets the departure AirportID & Arrival AirportID ,WindReliabilty ,AircraftID & Quarter from UI & calculate Wind
        /// </summary>
        /// <param name="DepartAirportID"></param>
        /// <param name="ArriveAirportID"></param>
        /// <param name="_windReliability"></param>
        /// <param name="AircraftID"></param>
        /// <param name="Quarter"></param>
        /// <returns></returns>
        public double GetWind(long DepartAirportID, long ArriveAirportID, int _windReliability, long AircraftID, string Quarter)
        {
            return Business.Calculations.CalculationManager.GetWind(DepartAirportID, ArriveAirportID, _windReliability, AircraftID, Quarter);
        }

        public double GetWindBasedOnParameter(double aircraftWindAltitude, int _windReliability, string Quarter,
            double deplatdeg, double deplatmin, string lcDepLatDir,
            double deplngdeg, double deplngmin, string lcDepLngDir, long lnDepartZone,
            double arrlatdeg, double arrlatmin, string lcArrLatDir,
            double arrlngdeg, double arrlngmin, string lcArrLngDir, long lnArrivZone
            )
        {
            return Business.Calculations.CalculationManager.GetWindBasedOnParameter(aircraftWindAltitude, _windReliability, Quarter,
            deplatdeg, deplatmin, lcDepLatDir,
            deplngdeg, deplngmin, lcDepLngDir, lnDepartZone,
            arrlatdeg, arrlatmin, lcArrLatDir,
            arrlngdeg, arrlngmin, lcArrLngDir, lnArrivZone);
        }
        /// <summary>
        /// Gets the Slope value,DeltaLat value & DeltaLng value from UI & calculate GetHeading
        /// </summary>
        /// <param name="Slope"></param>
        /// <param name="DeltaLat"></param>
        /// <param name="DeltaLng"></param>
        /// <returns></returns>
        public double GetHeading(double Slope, double DeltaLat, double DeltaLng)
        {
            return Business.Calculations.CalculationManager.GetHeading(Slope, DeltaLat, DeltaLng);
        }
        /// <summary>
        /// Gets the values from UI for Wind ,LandingBias,trueairspeed, TakeoffBias, Miles & DateValue 
        /// </summary>
        /// <param name="lnwind"></param>
        /// <param name="lnLndBias"></param>
        /// <param name="lntas"></param>
        /// <param name="lnToBias"></param>
        /// <param name="lnmiles"></param>
        /// <param name="DateVal"></param>
        /// <returns></returns>
        public double GetIcaoEte(double lnwind, double lnLndBias, double lntas, double lnToBias, double lnmiles, DateTime DateVal)
        {
            return Business.Calculations.CalculationManager.GetIcaoEte(lnwind, lnLndBias, lntas, lnToBias, lnmiles, DateVal);
        }
        /// <summary>
        /// Gets the values from UI for AirportID, Leg's DateTime, GMT status  & calculate GMT
        /// </summary>
        /// <param name="AirportID"></param>
        /// <param name="ldDtTm"></param>
        /// <param name="IIFindGMT"></param>
        /// <param name="IIGmt"></param>
        /// <returns></returns>
        public DateTime GetGMT(long AirportID, DateTime ldDtTm, bool IIFindGMT, bool IIGmt = false)
        {
            return Business.Calculations.CalculationManager.GetGMT(AirportID, ldDtTm, IIFindGMT, IIGmt);
        }
        /// <summary>
        /// Gets the values from UI for departure ICAO id, Arrival ICAO id & HomeBaseICAO id to calculate Domestic or International
        /// </summary>
        /// <param name="DepartIcaoID"></param>
        /// <param name="ArriveIcaoID"></param>
        /// <param name="HomebaseICAOID"></param>
        /// <returns></returns>
        public string CalcDomIntl(Int64 DepartAirportID, Int64 ArriveAirportID, Int64 HomebaseAirportID)
        {
            return Business.Calculations.CalculationManager.CalcDomIntl(DepartAirportID, ArriveAirportID, HomebaseAirportID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="HomeBaseID"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.Calculations.PreflightHomeBaseSettings> GetPreflightHomeBaseSettings(Int64 HomeBaseID)
        {
            Business.Common.ReturnValue<Data.Calculations.PreflightHomeBaseSettings> retVal = null;
            PreflightCalcManager PreflightCalcMngr = new PreflightCalcManager();
            retVal = PreflightCalcMngr.GetHomeBaseSetting(HomeBaseID);
            return retVal;
        }

        #region "Postflight Calculation Methods"

        /// <summary>
        /// Method to Get Homebase Informations
        /// </summary>
        /// <param name="HomeBaseID">Pass Homebase ID</param>
        /// <returns>Returns Homebase Settings List</returns>
        public Business.Common.ReturnValue<Data.Calculations.GetPOHomeBaseSetting> GetPostflightHomeBaseSetting(Int64 HomeBaseID)
        {
            Business.Common.ReturnValue<Data.Calculations.GetPOHomeBaseSetting> retVal = null;
            PostflightCalcManager CalcMgr = new PostflightCalcManager();
            retVal = CalcMgr.GetHomeBaseSetting(HomeBaseID);
            return retVal;
        }

        /// <summary>
        /// Method to Get Fleet Charge Rate Informations
        /// </summary>
        /// <param name="FleetID">Pass Fleet ID</param>
        /// <returns>Returns Fleet Charge Rate List</returns>
        public Business.Common.ReturnValue<Data.Calculations.GetPOFleetChargeRate> GetFleetChargeRate(Int64 FleetID, DateTime CurrentDate)
        {
            Business.Common.ReturnValue<Data.Calculations.GetPOFleetChargeRate> retVal = null;
            PostflightCalcManager CalcMgr = new PostflightCalcManager();
            retVal = CalcMgr.GetFleetChargeRateList(FleetID, CurrentDate);
            return retVal;
        }

        /// <summary>
        /// Method to Get Fleet Profile Informations
        /// </summary>
        /// <param name="FleetID">Pass Fleet ID</param>
        /// <returns>Returns Fleet Profile List</returns>
        public Business.Common.ReturnValue<Data.Calculations.GetPOFleetProfile> GetFleetProfile(Int64 FleetID)
        {
            Business.Common.ReturnValue<Data.Calculations.GetPOFleetProfile> retVal = null;
            PostflightCalcManager CalcMgr = new PostflightCalcManager();
            retVal = CalcMgr.GetFleetProfileList(FleetID);
            return retVal;
        }

        /// <summary>
        /// Method to Get SIFL Rate Informations
        /// </summary>
        /// <param name="currentDate">Pass Current Date</param>
        /// <returns>Returns SIFL Rate List</returns>
        public Business.Common.ReturnValue<Data.Calculations.GetPOSIFLRate> GetSIFLRate(DateTime CurrentDate)
        {
            Business.Common.ReturnValue<Data.Calculations.GetPOSIFLRate> retVal = null;
            PostflightCalcManager CalcMgr = new PostflightCalcManager();
            retVal = CalcMgr.GetSIFLRateList(CurrentDate);
            return retVal;
        }

        #endregion

    }
}
