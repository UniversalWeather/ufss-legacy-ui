﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Data.Preflight;
using FlightPak.Business.Common;
using FlightPak.Services.Utility;
using System.Collections.ObjectModel;
using System.Data;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPreflightService" in both code and config file together.
    [ServiceContract(Namespace = "http://myservice.com")] // This will be changed are moved to web.config file - Sathish
    public interface IPreflightService
    {
        #region Preflight
        [OperationContract]
        Business.Common.ReturnValue<GetAllPreflightList> GetAllPreflightList();

        [OperationContract]
        ReturnValue<View_PreflightMainList> GetAllPreflightMainList(Int64 homebaseID, Int64 clientID, string tripSheet, string worksheetstr, string hold, string cancelled, string schedServ, string Unfulfilled, bool isLog, DateTime EstDepartDate, string homebaseIDCSVStr);
        [OperationContract]
        ReturnValue<View_PreflightMainList> GetAllDeletedPreflightMainList();
        [OperationContract]
        long GetRevisionNum(long tripID);
        [OperationContract]
        ReturnValue<PreflightMain> GetTrip(Int64 TripID);
        [OperationContract]
        ReturnValue<PreflightMain> GetTripByTripNum(Int64 TripNum);
        [OperationContract]
        ReturnValue<PreflightMain> GetList();
        [OperationContract]
        ReturnValue<PreflightMain> Add(PreflightMain Trip);
        [OperationContract]
        ReturnValue<PreflightMain> Update(PreflightMain Trip);
        [OperationContract]
        ReturnValue<PreflightMain> Delete(Int64 TripID);
        [OperationContract]
        ReturnValue<PreflightMain> GetLock(Guid TripID);
        [OperationContract]
        ReturnValue<PreflightMain> UnlockTrip(Guid TripID);
        [OperationContract]
        ReturnValue<PreflightMain> ValidateTrip(PreflightMain Trip);

        #region Optimization
        [OperationContract]
        ReturnValue<IndividualDispatcher> GetDispatcherByUsername(string Username);
        [OperationContract]
        ReturnValue<IndividualPassenger> GetIndividualPassengerByIDOrCD(string passengerRequestorCD, long passengerRequestorID);
        [OperationContract]
        ReturnValue<IndividualCrew> GetCrewBYIDOrCD(string CrewCD, long CrewID);
        [OperationContract]
        ReturnValue<IndividualEmergencyContact> GetEmergencyContactByIDOrCD(string EmergencyContactCD, long EmergencyContactID);
        [OperationContract]
        ReturnValue<IndividualFleet> GetFleetByIDOrTailnum(string Tailnum, long FleetID);
        [OperationContract]
        ReturnValue<IndividualClient> GetClientByIDOrCD(string ClientCD, long ClientID);
        [OperationContract]
        ReturnValue<IndividualCompany> GetCompanyMasterbyHomebaseCDorID(string HomebaseCD, long HomebaseID);
        [OperationContract]
        ReturnValue<IndividualDepartmentAuthorization> GetDepartmentAuthorizationByCDOrID(string DepartmentAuthorizationCD, long DepartmentAuthorizationID);
        [OperationContract]
        ReturnValue<IndividualDepartment> GetDepartmentByCDOrID(string DepartmentCD, long DepartmentID);
        [OperationContract]
        ReturnValue<IndividualAccount> GetAccountByCDOrID(string AccountCD, long AccountID);
        [OperationContract]
        ReturnValue<IndividualAircraft> GetAircraftByCDOrID(string AircraftCD, long AircraftID);
        [OperationContract]
        ReturnValue<GetAllFleetForPopup> GetAllFleetForPopup(bool FetchActiveOnly, string IsFixedRotary, string IcaoID, long VendorID);
        [OperationContract]
        ReturnValue<GetAllPassengerForPopup> GetAllPassengerForPopup(bool FetchActiveOnly, bool IsRequestor, string IcaoID, long CQCustomerID);
        [OperationContract]
        ReturnValue<GetAllCrewForPopup> GetAllCrewForPopup(long ClientID, bool FetchActiveOnly, bool IsRotatory, bool IsFixed, string IcaoID, long CrewGroupID);
        #endregion
        #region "Copy Trip"

        [OperationContract]
        ReturnValue<PreflightMain> CopyTripDetails(DateTime DepartDate, long tripID, long customerID, bool IsLogisticsToBeCopied, bool IsCrewToBeCopied, bool IsPaxToBeCopied, bool IsHistoryToBeCopied);

        #endregion

        #region "MoveTrip"
        [OperationContract]
        ReturnValue<PreflightLeg> MoveTrip(List<PreflightLeg> NewLegList, List<PreflightLeg> OldLegList, Int64 OldTripID, Int64 NewTripID);
        #endregion
        //[OperationContract]
        //ReturnValue<Data.Preflight.Leg> ValidateLegs(List<Data.Preflight.Leg> Legs);
        //[OperationContract]
        //ReturnValue<Data.Preflight.PreFlightTrip> SubmitTrip(Guid TripID, TripAction Action);

        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetPreReportSearch> GetPreReportList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate, DateTime EndDate, Int64 FromTripNUM, Int64 ToTripNUM);
        #endregion

        #region  Dispatcher
        [OperationContract]
        ReturnValue<GetDispatcherList> GetDispatcherList();
        #endregion

        #region Crew
        [OperationContract]
        ReturnValue<Data.Preflight.GetAllCrewDetails> GetCrewListbyCrewIDORCrewCD(DateTime DepartDate, DateTime ArriveDate, long CrewGroupID, long HomeBaseID, long FleetID, long ArrivalAirportID, long CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, long AircraftID, long CrewID, string CrewCD);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetAllCrewDetails> GetCrewList(DateTime DepartDate, DateTime ArriveDate, Int64 CrewGroupID, Int64 HomeBaseID, Int64 FleetID, Int64 ArrivalAirportID, Int64 CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, Int64 AircraftID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetAllCrewDetails> GetAvailableCrewList(DateTime DepartDate, DateTime ArriveDate, Int64 CrewGroupID, Int64 HomeBaseID, Int64 FleetID, Int64 ArrivalAirportID, Int64 CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, Int64 AircraftID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.CrewActivity> GetCrewActivity(Int64 crewID, DateTime DepartDate, bool IsNonLogTripSheetIncludeCrewHIST, string CrewFH, long TripNUM);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetCrewCheckList> GetCrewCheckList(bool IsStatus);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.CrewPassengerPassport> GetPassengerPassportByPassportID(Int64 PassportId);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.CrewActivityDateRange> GetCrewActivityDateRange(Int64 crewID, DateTime FromDate, DateTime ToDate, bool IsNonLogTripSheetIncludeCrewHIST, string CrewFH, long TripNUM);
        #endregion

        #region Airport
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetAirportbyAirportID> GetAirportbyAirportIDs(Int64 AirportID);
        #endregion


        #region Pax
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreFlightAvailablePaxList();
        [OperationContract]
        ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreflightAvailablePaxListByIDORCD(long PassengerRequestorID, string PassengerRequestorCD);
        [OperationContract]
        ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreflightAvailablePaxListForPaxTab();
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary> GetAllPreFlightPaxSummary(long tripID);
        [OperationContract]
        ReturnValue<Data.Preflight.GetPassenger> GetPassengerbyIDOrCD(long PassengerRequestorID, string PassengerRequestorCD);
        #endregion

        #region History
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.PreflightTripHistory> GetHistory(long CustomerId, long TripId);
        [OperationContract]
        Business.Common.ReturnValue<Data.Preflight.PreflightTripHistory> UpdatePreflightTripHistory(PreflightTripHistory History);
        #endregion

        #region ScheduleCalender Advanced Filter

        [OperationContract]
        string RetrieveFilterSettings();
        [OperationContract]
        string RetrieveSystemDefaultSettings();
        [OperationContract]
        bool SaveUserDefaultSettings(string xmlSettings);
        [OperationContract]
        bool IsUserSettingsAvailable();

        #endregion

        #region ScheduleCalender Preferences

        [OperationContract]
        string RetrieveScheduleCalenderPreferences(string calenderType);
        [OperationContract]
        bool SaveScheduleCalanderPreferences(string calenderType, string fleetIDS, string crewIDS, string crewGroupIDS, bool isCrewGroup);
        [OperationContract]
        bool IsScheduleCalenderPreferencesAvailable(string calenderType);

        #endregion

        #region ScheduleCalender Monthly SatSunWeek, NoPastWeek

        [OperationContract]
        string RetrieveScheduleCalenderPreferencesMonthly(string calenderType);
        [OperationContract]
        bool SaveScheduleCalanderPreferencesMonthly(string calenderType, bool satSunWeek, bool noPastWeek);

        #endregion

        #region Schedule Calendar


        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Business.Common.ReturnValue<Data.Preflight.FleetCalendarDataResult> GetFleetCalendarData(DateTime startDate, DateTime endDate, FilterCriteria filterCriteria, Collection<string> fleetTreeInput, bool retrievePaxDetails, bool isStandByCrew, bool showRecordStartsInDateRange);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Business.Common.ReturnValue<Data.Preflight.CrewCalendarDataResult> GetCrewCalendarData(DateTime startDate, DateTime endDate, FilterCriteria filterCriteria, Collection<string> crewTreeInput, bool showRecordStartsInDateRange);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        ReturnValue<Data.Preflight.FleetResourceTypeResult> GetFleetResourceType(FlightPak.Services.Utility.VendorOption vendorOption, Collection<string> fleetIds, bool homebaseOnly, string homebaseId);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        ReturnValue<Data.Preflight.CrewResourceTypeResult> GetCrewResourceType(Collection<string> crewIds, bool homebaseOnly, string homebaseId, bool isFixedCrewOnly, bool isRotaryWingCrewOnly, string calenderType);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        ReturnValue<Data.Preflight.LegendsResult> GetAllLegends();

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Business.Common.ReturnValue<Data.Preflight.FleetPlannerCalendarDataResult> GetFleetPlannerCalendarData(DateTime startDate, short timeintervel, FilterCriteria filterCriteria, Collection<string> fleetTreeInput, bool IsTrip, bool IsHold, bool IsWorkSheet, bool IsCanceled, bool IsUnFulfilled);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Business.Common.ReturnValue<Data.Preflight.CrewPlannerCalendarDataResult> GetCrewPlannerCalendarData(DateTime startDate, short timeintervel, FilterCriteria filterCriteria, Collection<string> crewTreeInput, bool IsTrip, bool IsHold, bool IsWorkSheet, bool IsCanceled, bool IsUnFulfilled);


        #endregion




        #region "Schedule Calendar Entries From Context Menu"

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Business.Common.ReturnValue<Data.Preflight.FleetCalendarEntriesResult> GetFleetCalendarEntries(string timebase, DateTime startdate, DateTime enddate);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        Business.Common.ReturnValue<Data.Preflight.CrewCalendarEntriesResult> GetCrewCalendarEntries(string timebase, DateTime startdate, DateTime enddate);

        //[OperationContract]
        //[FaultContract(typeof(ServiceFault))]
        //Business.Common.ReturnValue<Data.Preflight.TripHistoryResult> GetTripHistoryResult(Int64 TripID);

        #endregion

        #region Tree view
        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        ReturnValue<Data.Preflight.FleetTreeResult> GetFleetInfoForFleetTree(bool homebaseOnly);

        [OperationContract]
        [FaultContract(typeof(ServiceFault))]
        ReturnValue<Data.Preflight.CrewTreeResult> GetCrewInfoForCrewTree(bool homebaseOnly);

        # endregion

        #region "UWA Interface"

        [OperationContract]
        ReturnValue<PreflightMain> GetUWAL(Int64 TripID, bool isUWASubmit);
        [OperationContract]
        ReturnValue<PreflightMain> UpdateUWAL(PreflightMain prefMain);
        [OperationContract]
        ReturnValue<List<string>> TSSSubmit(Int64 TripID, string UWATripID, bool isManualSubmit);
        [OperationContract]
        ReturnValue<List<string>> TSSCheckStatus(string UWATripID);
        [OperationContract]
        ReturnValue<Data.Preflight.GetUWACrewPassportVisa_Result> GetUWACrewPassportVisa(Int64 LegID, Int64 CustomerID);
        [OperationContract]
        ReturnValue<Data.Preflight.GetUWAPassengerPassportVisa_Result> GetUWAPassengerPassportVisa(Int64 LegID, Int64 CustomerID);

        [OperationContract]
        ReturnValue<List<string>> EAPISSubmit(Int64 TripID, List<string> PrefLegLst);
        [OperationContract]
        ReturnValue<PreflightMain> GetAPIS(Int64 TripID);

        [OperationContract]
        ReturnValue<PreflightFuel> GetFuelPrices(List<string> ICAOID, long AirportID);

        [OperationContract(IsOneWay = true)]
        void UpdateAPISException(Int64 TripID, string APISException);

        #endregion

        #region  Fuel
        [OperationContract]
        ReturnValue<FlightPakFuelData> GetFlightPakFuelData(Int64 TripID, Int64 DepatureICAOID);

        [OperationContract]
        ReturnValue<FBOBestPrice_Result> GetFBOBestPrice(Int64 TripID, Int64 DepatureICAOID, decimal Gallon);

        [OperationContract]
        bool UpdatePreflightFuelDetails(Int64 TripID);

        #endregion

        #region "SIFL"

        [OperationContract]
        ReturnValue<GetSIFLPassengerInfo> GetSIFLPassengerInfo(Int64 TripID);

        [OperationContract]
        ReturnValue<GetAllPreflightTripSIFL> GetAllPreflightTripSIFL(Int64 TripID);

        [OperationContract]
        ReturnValue<GetSIFLPassengerEmployeeType> GetSIFLPassengerEmployeeType(Int64 passengerRequestorID);

        [OperationContract]
        ReturnValue<CheckPreflightTripSIFL> CheckPreflightTripSIFL(Int64 passengerRequestorID, Int64 departIcaoID, Int64 arrivalIcaoID);

        [OperationContract]
        ReturnValue<GetPreflightTripSIFLID> GetPreflightTripSIFLID();

        [OperationContract]
        ReturnValue<GetAssociatePassengerID> GetAssociatePassengerID(Int64 PassengerRequestorID);

        [OperationContract]
        ReturnValue<PreflightTripSIFL> AddPreflightTripSIFL(PreflightTripSIFL Sifl);

        [OperationContract]
        ReturnValue<PreflightTripSIFL> UpdatePreflightTripSIFL(PreflightTripSIFL Sifl);

        [OperationContract]
        ReturnValue<PreflightTripSIFL> DeletePreflightTripSIFL(Int64 SiflID);

        [OperationContract]
        ReturnValue<PreflightTripSIFL> DeleteAllPreflightTripSIFL(Int64 TripID);

        #endregion

        #region   #region Validate Entire Trip
        //[OperationContract]
        //ReturnValue<PreflightTripException> ValidateTripPriority(PreflightMain Trip);
        [OperationContract]
        ReturnValue<PreflightTripException> ValidateTripforMandatoryExceptions(PreflightMain Trip);
        [OperationContract]
        ReturnValue<ExceptionTemplate> GetExceptionTemplateList();
        [OperationContract]
        ReturnValue<PreflightTripException> GetTripExceptionList(Int64 TripID);

        #endregion

        #region Update CorporateRequest from Preflight
        [OperationContract]
        ReturnValue<PreflightMain> UpdateCRFromPreflight(PreflightMain Trip);
        #endregion

        #region "Copy File from Cq to preflight"
        [OperationContract]
        ReturnValue<PreflightMain> CopyCQToPreflight(Int64 CQFileID, Int64 CQMainID, Boolean IsAutoDispatch, string TripStatus, Boolean IsRevision);
        #endregion

        #region "Copy Crew Calender Entries"
        [OperationContract]
        ReturnValue<PreflightMain> CopyCrewCalenderDetails(DateTime DepartDate, long TripID, long CustomerId);
        #endregion

        #region "Postflight"
        [OperationContract]
        ReturnValue<Data.Preflight.PostflightMain> GetLogByTripID(long tripID);
        #endregion

        #region Trip Email

        [OperationContract]
        void SendEmailToCrewList(PreflightMain Trip);

        [OperationContract]
        void SendEmailToPaxList(PreflightMain Trip);

        [OperationContract]
        void SendTripDetails(PreflightMain Trip, List<string> EmailList);

        //2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        [OperationContract]
        void SendTripDetailsWithFromEmail(PreflightMain Trip, List<string> EmailList, string FromEmail, string HtmlBody, List<String> attachments);

        [OperationContract]
        void SendItineraryDetailsToOthers(PreflightMain Trip, List<string> EmailList);

        //2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        [OperationContract]
        void SendItineraryDetailsToOthersWithFromEmail(PreflightMain Trip, List<string> EmailList, string FromEmail, string HtmlBody, List<String> attachments);

        [OperationContract]
        void SendItineraryDetailsToPAX(PreflightMain Trip, string EmailID, List<GetPaxItineraryInfo> PaxItineraryList);

        //2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        [OperationContract]
        void SendItineraryDetailsToPAXWithFromEmail(PreflightMain Trip, string EmailID, string FromEmail, string HtmlBody, List<String> attachments);

        [OperationContract]
        List<GetPaxItineraryInfo> GetPassengerItinerary(PreflightMain Trip, Int64? PaxID);

        #endregion
    }
}

