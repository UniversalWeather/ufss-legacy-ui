﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using FlightPak.Business.Common;
using FlightPak.Business.Preflight;
using FlightPak.Business.Postflight;

//For Tracing and Exception Handling
using FlightPak.Common;
using FlightPak.Common.Constants;
using Tracing = FlightPak.Common.Tracing;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PreflightService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class PostflightService : BaseService, IPostflightService
    {
        private ExceptionManager exManager;

        #region "Postflight Trip Manager"

        public Business.Common.ReturnValue<Data.Postflight.GetPostflightList> GetPostflightTrips(Int64 homeBaseID, Int64 clientID, Int64 fleetID, bool isPersonal, bool isCompleted, DateTime startDate)
        {
            Business.Common.ReturnValue<Data.Postflight.GetPostflightList> retVal = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.GetPostflightList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homeBaseID, clientID, fleetID, isPersonal, isCompleted, startDate))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        retVal = PostflightMgr.GetList(homeBaseID, clientID, fleetID, isPersonal, isCompleted, startDate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightMain> GetTrip(Data.Postflight.PostflightMain trip)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightMain> retVal = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.PostflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();

                        retVal = PostflightMgr.GetTrip(trip);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightMain> ValidateTrip(Data.Postflight.PostflightMain trip)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightMain> retVal = null;
            return retVal;
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightMain> Add(Data.Postflight.PostflightMain trip)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightMain> retVal = null;
            return retVal;
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightMain> Update(Data.Postflight.PostflightMain trip)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightMain> objMain = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.PostflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        objMain = PostflightMgr.UpdateTrip(trip);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightMain> DeleteTrip(long POLogID)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightMain> objMain = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.PostflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(POLogID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        objMain = PostflightMgr.DeleteTrip(POLogID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightMain> LogPreflightTrip(Data.Postflight.PostflightMain trip)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightMain> retVal = null;
            return retVal;
        }

        /// <summary>
        /// Method to Get All Passengers List
        /// </summary>
        /// <returns>Passengers List</returns>
        public Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger> GetPaxList()
        {
            Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger> retVal = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        retVal = PostflightMgr.GetPassengerList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        /// <summary>
        /// Method to Get All Passengers List
        /// </summary>
        /// <returns>Passengers List</returns>
        public Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger> GetPaxListByRequestor(List<long> PassengerRequestorIDs)
        {
            Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger> retVal = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorIDs))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        retVal = PostflightMgr.GetPassengerList(PassengerRequestorIDs);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region "Other Crew Duty Log"
        public Business.Common.ReturnValue<Data.Postflight.PostflightSimulatorLog> UpdateOtherCrew(Data.Postflight.PostflightSimulatorLog OtherCrewLog)
        {
            PostflightManager PostflightMgr = new PostflightManager();
            return PostflightMgr.UpdateOtherCrewDutyLog(OtherCrewLog);
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightSimulatorLog> DeleteOtherCrewDuty(long OtherCrewSimulatorID)
        {
            PostflightManager PostflightMgr = new PostflightManager();
            return PostflightMgr.DeleteOtherCrewDutyLog(OtherCrewSimulatorID);
        }

        public Business.Common.ReturnValue<Data.Postflight.GetOtherCrewDutyLog> GetOtherCrewDutyLog()
        {
            Business.Common.ReturnValue<Data.Postflight.GetOtherCrewDutyLog> retVal = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.GetOtherCrewDutyLog>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();

                        retVal = PostflightMgr.GetOtherCrewDutyLog();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Expense Catalog"
        public Business.Common.ReturnValue<Data.Postflight.GetPostflightExpenseList> GetExpList()
        {
            Business.Common.ReturnValue<Data.Postflight.GetPostflightExpenseList> retVal = null;
            PostflightManager PostflightMgr = new PostflightManager();
            retVal = PostflightMgr.GetExpList();
            return retVal;
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightExpense> GetPOExpense(Data.Postflight.PostflightExpense expense)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightExpense> retVal = null;
            PostflightManager PostflightMgr = new PostflightManager();
            retVal = PostflightMgr.GetTripExpense(expense);
            return retVal;
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightExpense> UpdateExpenseCatalog(Data.Postflight.PostflightExpense expense)
        {
            PostflightManager PostflightMgr = new PostflightManager();
            return PostflightMgr.UpdateExpenseCatalog(expense);
        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightExpense> DeleteExpenseCatalog(long PostflightExpenseID)
        {
            PostflightManager PostflightMgr = new PostflightManager();
            return PostflightMgr.DeleteExpenseCatalog(PostflightExpenseID);
        }
        #endregion

        #region "Business Errors"
        public Business.Common.ReturnValue<Data.Postflight.ExceptionTemplate> GetPOBusinessErrorsList(long ModuleID)
        {
            Business.Common.ReturnValue<Data.Postflight.ExceptionTemplate> retVal = null;
            PostflightValidationsManager PostflightMgr = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.ExceptionTemplate>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ModuleID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightValidationsManager();
                        retVal = PostflightMgr.GetPOBusinessErrorsList(ModuleID);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<Data.Postflight.PostflightTripException> ValidateBusinessRules(Data.Postflight.PostflightMain Trip)
        {

            Business.Common.ReturnValue<Data.Postflight.PostflightTripException> retVal = null;
            PostflightValidationsManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.PostflightTripException>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightValidationsManager();
                        retVal = PostflightMgr.ValidateBusinessRules(Trip);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.Postflight.PostflightTripException> DeleteExistingTripExceptions(long POLogID, long POTripExceptionID)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightTripException> retVal = null;
            PostflightManager PostflightMgr = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.PostflightTripException>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(POLogID, POTripExceptionID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        retVal = PostflightMgr.DeleteExistingTripExceptions(POLogID, POTripExceptionID);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "Pax Passport"
        public ReturnValue<Data.Postflight.GetPassportByPassengerID> GetPassportByPassengerID(Int64 PassengerID, string PassportNum)
        {
            Business.Common.ReturnValue<Data.Postflight.GetPassportByPassengerID> objCrew = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Postflight.GetPassportByPassengerID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerID, PassportNum))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        objCrew = PostflightMgr.GetPassportByPassengerID(PassengerID, PassportNum);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region History
        public ReturnValue<Data.Postflight.PostflightLogHistory> GetHistory(long CustomerId, long POLogId)
        {
            Business.Common.ReturnValue<Data.Postflight.PostflightLogHistory> objPostflightMains = null;
            PostflightManager PostflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Postflight.PostflightLogHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, POLogId))
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMngr = new PostflightManager();
                        objPostflightMains = PostflightMngr.GetHistory(CustomerId, POLogId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPostflightMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        public Business.Common.ReturnValue<Data.Postflight.GetPOReportSearch> GetPoReportList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate, DateTime EndDate, Int64 FromLogNum, Int64 ToLogNum)
        {
            Business.Common.ReturnValue<Data.Postflight.GetPOReportSearch> retVal = null;
            PostflightManager PostflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Postflight.GetPOReportSearch>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID,ClientID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromLogNum, ToLogNum))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PostflightMgr = new PostflightManager();
                        retVal = PostflightMgr.GetPoReportList(HomeBaseID,ClientID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromLogNum, ToLogNum);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

    }
}