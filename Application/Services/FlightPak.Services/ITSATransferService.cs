﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FlightPak.Business.Common;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITSATransferService" in both code and config file together.
    [ServiceContract]
    public interface ITSATransferService
    {
        [OperationContract]
        string GetData();

        [OperationContract]
        ReturnValue<FlightPak.Data.MasterCatalog.Metro> GetMetroCityList();

        [OperationContract]
        ReturnValue<FlightPak.Data.MasterCatalog.FileWarehouse> AddFWHType(FlightPak.Data.MasterCatalog.FileWarehouse FileWH);

        [OperationContract]
        FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.Customer> AddTSA(FlightPak.Data.MasterCatalog.Customer TSAData);
        [OperationContract]
        FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSANoFly> DeleteTSANoFly(FlightPak.Data.MasterCatalog.TSANoFly TSAData);
        [OperationContract]
        FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSASelect> DeleteTSASelecte(FlightPak.Data.MasterCatalog.TSASelect TSAData);
        [OperationContract]
        FlightPak.Business.Common.ReturnValue<FlightPak.Data.MasterCatalog.TSAClear> DeleteTSANClear(FlightPak.Data.MasterCatalog.TSAClear TSAData);

    }
}
