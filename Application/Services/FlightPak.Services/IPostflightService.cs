﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPreflightService" in both code and config file together.
    [ServiceContract(Namespace = "http://myservice.com")] // This will be changed are moved to web.config file - Sathish
    public interface IPostflightService
    {
        #region "Postflight Trip Manager"
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.GetPostflightList> GetPostflightTrips(Int64 homeBaseID, Int64 clientID, Int64 fleetID, bool isPersonal, bool isCompleted, DateTime startDate);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightMain> GetTrip(Data.Postflight.PostflightMain trip);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightMain> ValidateTrip(Data.Postflight.PostflightMain trip);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightMain> Add(Data.Postflight.PostflightMain trip);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightMain> Update(Data.Postflight.PostflightMain trip);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightMain> DeleteTrip(long POLogID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightMain> LogPreflightTrip(Data.Postflight.PostflightMain trip);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger> GetPaxList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.POGetAllPassenger> GetPaxListByRequestor(List<long> PassengerRequestorIDs);
        #endregion

        #region "Other Crew Duty Log"
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightSimulatorLog> UpdateOtherCrew(Data.Postflight.PostflightSimulatorLog OtherCrewLog);
        //[OperationContract]
        //Business.Common.ReturnValue<Data.Postflight.PostflightSimulatorLog> GetOtherCrewDutyList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightSimulatorLog> DeleteOtherCrewDuty(long OtherCrewSimulatorID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.GetOtherCrewDutyLog> GetOtherCrewDutyLog();
        #endregion

        #region "Expense Catalog"
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightExpense> GetPOExpense(Data.Postflight.PostflightExpense expense);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.GetPostflightExpenseList> GetExpList();
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightExpense> UpdateExpenseCatalog(Data.Postflight.PostflightExpense expense);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightExpense> DeleteExpenseCatalog(long PostflightExpenseID);
        #endregion

        #region "Business Errors"
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.ExceptionTemplate> GetPOBusinessErrorsList(long ModuleID);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightTripException> ValidateBusinessRules(Data.Postflight.PostflightMain Trip);
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightTripException> DeleteExistingTripExceptions(long POLogID, long POTripExceptionID);
        #endregion

        #region "Pax Passport"

        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.GetPassportByPassengerID> GetPassportByPassengerID(Int64 PassengerID, string PassportNum);

        #endregion

        #region History
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.PostflightLogHistory> GetHistory(long CustomerId, long POLogId);
        #endregion

        // To Get Postflight List for Report
        [OperationContract]
        Business.Common.ReturnValue<Data.Postflight.GetPOReportSearch> GetPoReportList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate, DateTime EndDate, Int64 FromLogNum, Int64 ToLogNum);
    }
}