﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SearchService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class SearchService : ISearchService
    {
        private ExceptionManager exManager;
        #region Simple Search
        public Business.Common.ReturnValue<Data.Search.SearchResult> SimpleSearch(string SearchStr, string Module, int PageNumber, int PageSize)
        {
            Business.Common.ReturnValue<Data.Search.SearchResult> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Search.SearchResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SearchStr, Module, PageNumber, PageSize))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Search.SearchManager searchManager = new FlightPak.Business.Search.SearchManager();
                        retValue = searchManager.SimpleSearch(SearchStr, Module, PageNumber, PageSize);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }
        #endregion
        #region Help Content Search
        public Business.Common.ReturnValue<Data.Search.FlightPakHelpContentSearch> HelpContentSearch(string SearchStr)
        {
            Business.Common.ReturnValue<Data.Search.FlightPakHelpContentSearch> retValue = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Search.FlightPakHelpContentSearch>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SearchStr))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Search.SearchManager searchManager = new FlightPak.Business.Search.SearchManager();
                        retValue = searchManager.HelpContentSearch(SearchStr);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
    }
}
