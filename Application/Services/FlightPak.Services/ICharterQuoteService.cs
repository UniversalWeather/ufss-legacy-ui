﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Data.CharterQuote;
using FlightPak.Business.Common;
using FlightPak.Services.Utility;
using System.Collections.ObjectModel;
using System.Data;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICharterQuoteService" in both code and config file together.
    [ServiceContract]
    public interface ICharterQuoteService
    {
        [OperationContract]
        ReturnValue<CQFile> GetCQRequestByFileNum(Int64 FileNum, bool isExpressQuote);

        [OperationContract]
        ReturnValue<CQFile> GetCQRequestByID(Int64 CQFileID, bool isExpressQuote);

        [OperationContract]
        ReturnValue<Data.CharterQuote.CQFile> SaveFileRequest(CQFile cqFile);

        [OperationContract]
        ReturnValue<CQQuoteMain> GetCQQuoteMainByCQMainID(Int64 CQMainID);
        [OperationContract]
        ReturnValue<Data.CharterQuote.CQQuoteMain> SaveQuoteReport(CQQuoteMain cqQuoteMain);

        [OperationContract]
        ReturnValue<CQInvoiceMain> GetCQInvoiceMainByCQMainID(Int64 CQMainID);
        [OperationContract]
        ReturnValue<Data.CharterQuote.CQInvoiceMain> SaveInvoiceReport(CQInvoiceMain cqQuoteMain);


        #region "CharterQuote Pax"
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList> GetAllCharterQuoteAvailablePaxList();
        #endregion

        [OperationContract]
        ReturnValue<View_CharterQuoteMainList> GetAllCQMainList(Int64 homebaseID, DateTime EstDepartDate, bool expressQuote, Int64 leadSourceID, string customerType, Int64 salesPersonID);
        [OperationContract]
        ReturnValue<ExceptionTemplate> GetCQBusinessErrorsList();
        [OperationContract]
        ReturnValue<CQException> GetQuoteExceptionList(Int64 cqMainID);
        [OperationContract]
        ReturnValue<CQException> ValidateQuoteforMandatoryExcep(CQMain cqMain);
        //[OperationContract]
        //ReturnValue<Data.CharterQuote.CQLostBusiness> GetLostBusinessInfo();

        [OperationContract]
        ReturnValue<CQFile> DeleteFile(Int64 cqFileID);
        [OperationContract]
        ReturnValue<Data.CharterQuote.CQHistory> GetHistory(long CQFileID);

        #region "Copy File"

        [OperationContract]
        ReturnValue<CQFile> CopyQuoteDetails(Int64 CQFileID, Int64 CQMainID);
        #endregion

        #region "Express Copy File"
        [OperationContract]
        ReturnValue<CQFile> CopyExpressQuoteDetails(Int64 CQFileID);
        #endregion

        #region "Copy File from Cq to preflight"
        [OperationContract]
        ReturnValue<CQFile> CopyCQToPreflight(Int64 CQFileID, Int64 CQMainID, Boolean IsAutoDispatch, string TripStatus, Boolean IsRevision);
        #endregion

        [OperationContract]
        void DeleteQuoteImageFiles(Int64 CQMainID);

        #region "Charter Quote Report"
        [OperationContract]
        ReturnValue<GetQuoteByFileID> GetQuoteByFileID(Int64 CQFileID);

        [OperationContract]
        ReturnValue<GetCQLegsByQuoteID> GetCQLegsByQuoteID(Int64 CQMainID);

        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.GetCQReportHeader> GetCQHeaderList();
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.GetCQReportDetail> GetCQReportDetail();
        
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> AddCQReportHeader(Data.CharterQuote.CQReportHeader oCQReportHeader);
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> UpdateCQReportHeader(Data.CharterQuote.CQReportHeader oCQReportHeader);
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.CQReportHeader> DeleteCQReportHeader(Data.CharterQuote.CQReportHeader oCQReportHeader);
        
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> AddCQReportDetail(Data.CharterQuote.CQReportDetail oCQReportDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> UpdateCQReportDetail(Data.CharterQuote.CQReportDetail oCQReportDetail);
        [OperationContract]
        Business.Common.ReturnValue<Data.CharterQuote.CQReportDetail> DeleteCQReportDetail(Data.CharterQuote.CQReportDetail oCQReportDetail);
        #endregion

        [OperationContract]
        ReturnValue<Data.CharterQuote.GetFileWarehouseByID> GetFileWarehouseByID(Int64 WareHouseID);
    }
}
