﻿using System;
using System.ServiceModel;
using FlightPak.Business.Common;
using FlightPak.Business.Preflight;
using FlightPak.Data.Preflight;
using System.Configuration;
using FlightPak.Services.Utility;
using FlightPak.Business.Preflight.ScheduleCalendarEntities;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PreflightService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class PreflightService : BaseService, IPreflightService
    {
        private ExceptionManager exManager;
        #region PreflightTrip
        public ReturnValue<GetAllPreflightList> GetAllPreflightList()
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllPreflightList> objMain = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetAllPreflightList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAllPreflightList();

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<View_PreflightMainList> GetAllPreflightMainList(Int64 homebaseID, Int64 clientID, string tripSheet, string worksheetstr, string hold, string cancelled, string schedServ, string Unfulfilled, bool isLog, DateTime EstDepartDate, string homebaseIDCSVStr)
        {
            PreflightManager PreflightMngr = null;

            Business.Common.ReturnValue<Data.Preflight.View_PreflightMainList> objMain = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<View_PreflightMainList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, homebaseIDCSVStr))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAllPreflightMainList(homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, homebaseIDCSVStr);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }
        public ReturnValue<View_PreflightMainList> GetAllDeletedPreflightMainList()
        {
            PreflightManager PreflightMngr = null;

            Business.Common.ReturnValue<Data.Preflight.View_PreflightMainList> objMain = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<View_PreflightMainList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAllDeletedPreflightMainList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public long GetRevisionNum(long tripID)
        {
            long? RevisionNum = 0;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        RevisionNum = PreflightMngr.GetRevisionNum(tripID, UserPrincipal.Identity.CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return (long)RevisionNum;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public ReturnValue<PreflightMain> GetTripByTripNum(Int64 TripNum)
        {
            ReturnValue<Data.Preflight.PreflightMain> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripNum))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetTripByTripNum(TripNum);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);



        }

        public ReturnValue<PreflightMain> GetTrip(Int64 TripID)
        {
            ReturnValue<Data.Preflight.PreflightMain> objMain = null;
            PreflightManager PreflightMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetTrip(TripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<PreflightMain> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> Add(PreflightMain Trip)
        {
            Business.Common.ReturnValue<PreflightMain> objMain = null;
            PreflightManager PreflightMngr = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.UpdateTrip(Trip);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<PreflightMain> Update(PreflightMain Trip)
        {
            Business.Common.ReturnValue<PreflightMain> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.UpdateTrip(Trip);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<PreflightMain> Delete(Int64 TripID)
        {
            Business.Common.ReturnValue<PreflightMain> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.DeleteTrip(TripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        #region Optimization

        public ReturnValue<IndividualDispatcher> GetDispatcherByUsername(string Username)
        {
            ReturnValue<Data.Preflight.IndividualDispatcher> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualDispatcher>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Username))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetDispatcherByUsername(Username);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<IndividualPassenger> GetIndividualPassengerByIDOrCD(string passengerRequestorCD, long passengerRequestorID)
        {
            ReturnValue<Data.Preflight.IndividualPassenger> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualPassenger>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerRequestorCD, passengerRequestorID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetIndividualPassengerByIDOrCD(passengerRequestorCD, passengerRequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<IndividualCrew> GetCrewBYIDOrCD(string CrewCD, long CrewID)
        {
            ReturnValue<Data.Preflight.IndividualCrew> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualCrew>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewCD, CrewID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetCrewBYIDOrCD(CrewCD, CrewID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<IndividualEmergencyContact> GetEmergencyContactByIDOrCD(string EmergencyContactCD, long EmergencyContactID)
        {
            ReturnValue<Data.Preflight.IndividualEmergencyContact> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualEmergencyContact>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EmergencyContactCD, EmergencyContactID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetEmergencyContactByIDOrCD(EmergencyContactCD, EmergencyContactID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<IndividualFleet> GetFleetByIDOrTailnum(string Tailnum, long FleetID)
        {
            ReturnValue<Data.Preflight.IndividualFleet> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualFleet>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Tailnum, FleetID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetFleetByIDOrTailnum(Tailnum, FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<IndividualClient> GetClientByIDOrCD(string ClientCD, long ClientID)
        {
            ReturnValue<Data.Preflight.IndividualClient> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualClient>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCD, ClientID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetClientByIDOrCD(ClientCD, ClientID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<IndividualDepartment> GetDepartmentByCDOrID(string DepartmentCD, long DepartmentID)
        {
            ReturnValue<Data.Preflight.IndividualDepartment> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualDepartment>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentCD, DepartmentID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetDepartmentByCDOrID(DepartmentCD, DepartmentID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<IndividualAccount> GetAccountByCDOrID(string AccountCD, long AccountID)
        {
            ReturnValue<Data.Preflight.IndividualAccount> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualAccount>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AccountCD, AccountID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAccountByCDorID(AccountCD, AccountID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<IndividualAircraft> GetAircraftByCDOrID(string AircraftCD, long AircraftID)
        {
            ReturnValue<Data.Preflight.IndividualAircraft> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualAircraft>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftCD, AircraftID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAircraftByIDOrCD(AircraftCD, AircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<IndividualCompany> GetCompanyMasterbyHomebaseCDorID(string HomebaseCD, long HomebaseID)
        {
            ReturnValue<Data.Preflight.IndividualCompany> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualCompany>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomebaseCD, HomebaseID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetCompanyMasterbyHomebaseCDorID(HomebaseCD, HomebaseID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<IndividualDepartmentAuthorization> GetDepartmentAuthorizationByCDOrID(string DepartmentAuthorizationCD, long DepartmentAuthorizationID)
        {
            ReturnValue<Data.Preflight.IndividualDepartmentAuthorization> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<IndividualDepartmentAuthorization>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorizationCD, DepartmentAuthorizationID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetDepartmentAuthorizationByCDOrID(DepartmentAuthorizationCD, DepartmentAuthorizationID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<GetAllFleetForPopup> GetAllFleetForPopup(bool FetchActiveOnly, string IsFixedRotary, string IcaoID, long VendorID)
        {
            ReturnValue<Data.Preflight.GetAllFleetForPopup> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetAllFleetForPopup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FetchActiveOnly, IsFixedRotary, IcaoID, VendorID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAllFleetForPopup(FetchActiveOnly, IsFixedRotary, IcaoID, VendorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<GetAllPassengerForPopup> GetAllPassengerForPopup(bool FetchActiveOnly, bool IsRequestor, string IcaoID, long CQCustomerID)
        {
            ReturnValue<Data.Preflight.GetAllPassengerForPopup> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetAllPassengerForPopup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FetchActiveOnly, IsRequestor, IcaoID, CQCustomerID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAllPassengerForPopup(FetchActiveOnly, IsRequestor, IcaoID, CQCustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<GetAllCrewForPopup> GetAllCrewForPopup(long ClientID, bool FetchActiveOnly, bool IsRotatory, bool IsFixed, string IcaoID, long CrewGroupID)
        {
            ReturnValue<Data.Preflight.GetAllCrewForPopup> objMain = null;

            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetAllCrewForPopup>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID, FetchActiveOnly, IsRotatory, IsFixed, IcaoID, CrewGroupID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAllCrewForPopup(ClientID, FetchActiveOnly, IsRotatory, IsFixed, IcaoID, CrewGroupID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion


        #region "Copy Trip"

        public ReturnValue<PreflightMain> CopyTripDetails(DateTime DepartDate, long tripID, long customerID, bool IsLogisticsToBeCopied, bool IsCrewToBeCopied, bool IsPaxToBeCopied, bool IsHistoryToBeCopied)
        {

            Business.Common.ReturnValue<PreflightMain> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, IsHistoryToBeCopied))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.CopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, IsHistoryToBeCopied);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        public ReturnValue<PreflightMain> GetLock(Guid TripID)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> UnlockTrip(Guid TripID)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> ValidateTrip(PreflightMain Trip)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Dispatcher"
        public ReturnValue<GetDispatcherList> GetDispatcherList()
        {
            Business.Common.ReturnValue<GetDispatcherList> objDispatcher = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<GetDispatcherList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objDispatcher = PreflightMngr.GetDispatcherList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objDispatcher;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Crew"
        public ReturnValue<Data.Preflight.GetAllCrewDetails> GetCrewListbyCrewIDORCrewCD(DateTime DepartDate, DateTime ArriveDate, long CrewGroupID, long HomeBaseID, long FleetID, long ArrivalAirportID, long CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, long AircraftID, long CrewID, string CrewCD)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllCrewDetails> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllCrewDetails>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID, CrewID, CrewCD))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objCrew = PreflightMngr.GetCrewListbyCrewIDORCrewCD(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID, CrewID, CrewCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetAllCrewDetails> GetCrewList(DateTime DepartDate, DateTime ArriveDate, Int64 CrewGroupID, Int64 HomeBaseID, Int64 FleetID, Int64 ArrivalAirportID, Int64 CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, Int64 AircraftID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllCrewDetails> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllCrewDetails>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objCrew = PreflightMngr.GetCrewList(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetAllCrewDetails> GetAvailableCrewList(DateTime DepartDate, DateTime ArriveDate, Int64 CrewGroupID, Int64 HomeBaseID, Int64 FleetID, Int64 ArrivalAirportID, Int64 CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, Int64 AircraftID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllCrewDetails> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllCrewDetails>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objCrew = PreflightMngr.GetAvailableCrewList(DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public ReturnValue<Data.Preflight.CrewActivity> GetCrewActivity(Int64 crewID, DateTime DepartDate, bool IsNonLogTripSheetIncludeCrewHIST, string CrewFH, long TripNUM)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewActivity> objPreflightMains = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.CrewActivity>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewID, DepartDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH,  TripNUM))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objPreflightMains = PreflightMngr.GetCrewActivity(crewID, DepartDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH, TripNUM);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPreflightMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.CrewActivityDateRange> GetCrewActivityDateRange(Int64 crewID, DateTime FromDate, DateTime ToDate, bool IsNonLogTripSheetIncludeCrewHIST, string CrewFH, long TripNUM)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewActivityDateRange> objPreflightMains = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.CrewActivityDateRange>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewID, FromDate, ToDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH, TripNUM))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objPreflightMains = PreflightMngr.GetCrewActivityDateRange(crewID, FromDate, ToDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH, TripNUM);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPreflightMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetCrewCheckList> GetCrewCheckList(bool IsStatus)
        {
            Business.Common.ReturnValue<Data.Preflight.GetCrewCheckList> objPreflightMains = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetCrewCheckList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsStatus))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objPreflightMains = PreflightMngr.GetCrewCheckList(IsStatus);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPreflightMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.CrewPassengerPassport> GetPassengerPassportByPassportID(Int64 PassportId)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewPassengerPassport> objPreflightMains = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.CrewPassengerPassport>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassportId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objPreflightMains = PreflightMngr.GetPassengerPassportByPassportID(PassportId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPreflightMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "Airport"

        public ReturnValue<Data.Preflight.GetAirportbyAirportID> GetAirportbyAirportIDs(Int64 AirportID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAirportbyAirportID> objAirport = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAirportbyAirportID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objAirport = PreflightMngr.GetAirportbyAirportIDs(AirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAirport;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Pax"

        public ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreflightAvailablePaxListByIDORCD(long PassengerRequestorID, string PassengerRequestorCD)
        {

            Business.Common.ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objCrew = PreflightMngr.GetPreFlightAvailablePaxListByIDOrCD(PassengerRequestorID, PassengerRequestorCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreFlightAvailablePaxList()
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objCrew = PreflightMngr.GetAllPreFlightAvailablePaxList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);





        }


        public ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreflightAvailablePaxListForPaxTab()
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objCrew = PreflightMngr.GetAllPreflightAvailablePaxListForPaxTab();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objCrew;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);





        }
        public ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary> GetAllPreFlightPaxSummary(long tripID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary> objCrew = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary>>(() =>
                   {
                       using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                       {
                           //Handle methods through exception manager with return flag
                           exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                           exManager.Process(() =>
                           {
                               PreflightMngr = new PreflightManager();

                               objCrew = PreflightMngr.GetAllPreFlightPaxSummary(tripID);
                           }, FlightPak.Common.Constants.Policy.BusinessLayer);
                       }
                       return objCrew;
                   }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region ScheduleCalender Advanced Filter

        /// <summary>
        /// Gets the user specific default advanced filter settings.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>user specific default advanced filter settings. returns string.empty if there is no such user id 
        /// exist</returns>
        ScheduleCalendarManager scheduleCalendarManager = new ScheduleCalendarManager();
        public string RetrieveFilterSettings()
        {

            string settings = string.Empty;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        settings = scheduleCalendarManager.RetrieveFilterSettings();
                        return settings;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return settings;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        /// <summary>
        /// Gets the system default advanced filter settings.
        /// </summary>
        /// <returns>System default advanced filter settings.</returns>
        public string RetrieveSystemDefaultSettings()
        {

            // TO DO : Check the logic to retrieve the default user settings, especially the userID.
            string systemDefaultSettings = string.Empty;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        systemDefaultSettings = scheduleCalendarManager.RetrieveSystemDefaultSettings();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return systemDefaultSettings;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        /// <summary>
        /// Saves the user specific settings. If there is already settings available for the user, then the previous settings
        /// will be overriden with this new settings else new settings will be created.
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="xmlSettings">Advanced filter settings</param>
        /// <returns>True if the save operation is successful.</returns>
        public bool SaveUserDefaultSettings(string xmlSettings)
        {

            bool successFlag = false;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(xmlSettings))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        successFlag = scheduleCalendarManager.SaveUserDefaultSettings(xmlSettings);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return successFlag;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        /// <summary>
        /// Checks if the advanced filter options available for the user
        /// </summary>
        /// <param name="userID">user id</param>
        /// <returns>True if user settings exist else false. </returns>
        public bool IsUserSettingsAvailable()
        {
            bool isUserSettingsAvailable = false;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        isUserSettingsAvailable = scheduleCalendarManager.IsUserSettingsAvailable();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return isUserSettingsAvailable;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        #endregion

        #region ScheduleCalender Preferences

        /// <summary>
        /// Gets the user specific default advanced filter settings.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>user specific default advanced filter settings. returns string.empty if there is no such user id 
        /// exist</returns>

        public string RetrieveScheduleCalenderPreferences(string CalenderType)
        {

            string fleetcrewids = string.Empty;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        fleetcrewids = scheduleCalendarManager.RetrieveScheduleCalenderPreferences(CalenderType);
                        return fleetcrewids;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return fleetcrewids;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        /// <summary>
        /// Saves the user specific settings. If there is already settings available for the user, then the previous settings
        /// will be overriden with this new settings else new settings will be created.
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="xmlSettings">Advanced filter settings</param>
        /// <returns>True if the save operation is successful.</returns>
        public bool SaveScheduleCalanderPreferences(string CalenderType, string fleetIDS, string crewIDS, string crewGroupIDS, bool IsCrewGroup)
        {

            bool successFlag = false;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetIDS + "|" + crewIDS + "|" + crewGroupIDS))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        successFlag = scheduleCalendarManager.SaveScheduleCalanderPreferences(CalenderType, fleetIDS, crewIDS, crewGroupIDS, IsCrewGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return successFlag;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        /// <summary>
        /// Checks if the advanced filter options available for the user
        /// </summary>
        /// <param name="userID">user id</param>
        /// <returns>True if user settings exist else false. </returns>
        public bool IsScheduleCalenderPreferencesAvailable(string CalenderType)
        {
            bool isScheduleCalenderPreferenceAvailable = false;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        isScheduleCalenderPreferenceAvailable = scheduleCalendarManager.IsScheduleCalenderPreferencesAvailable(CalenderType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return isScheduleCalenderPreferenceAvailable;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        #endregion

        #region ScheduleCalender Preferences Monthly

        //Monthly Tab SatSunWeek & NoPastWeek

        public string RetrieveScheduleCalenderPreferencesMonthly(string CalenderType)
        {
            string satSunNoPastWeek = string.Empty;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        satSunNoPastWeek = scheduleCalendarManager.RetrieveScheduleCalenderPreferencesMonthly(CalenderType);
                        return satSunNoPastWeek;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return satSunNoPastWeek;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        /// <summary>
        /// Saves the user specific settings. If there is already settings available for the user, then the previous settings
        /// will be overriden with this new settings else new settings will be created.
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="xmlSettings">Advanced filter settings</param>
        /// <returns>True if the save operation is successful.</returns>
        public bool SaveScheduleCalanderPreferencesMonthly(string CalenderType, bool satSunWeek, bool noPastWeek)
        {

            bool successFlag = false;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(satSunWeek + "|" + noPastWeek))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        successFlag = scheduleCalendarManager.SaveScheduleCalanderPreferencesMonthly(CalenderType, satSunWeek, noPastWeek);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return successFlag;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        #endregion

        #region ScheduleCalendar


        public ReturnValue<FleetCalendarDataResult> GetFleetCalendarData(DateTime startDate, DateTime endDate, FilterCriteria filterCriteria, Collection<string> fleetTreeInput, bool retrievePaxDetails, bool isStandByCrew, bool showRecordStartsInDateRange)
        {

            ReturnValue<FleetCalendarDataResult> retValue = new ReturnValue<FleetCalendarDataResult>();

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FleetCalendarDataResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, endDate, filterCriteria, fleetTreeInput, retrievePaxDetails, isStandByCrew, showRecordStartsInDateRange))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        FilterEntity filterEntity = ConvertToBusinessEntity(filterCriteria);
                        retValue = objSchedule.GetFleetCalendarData(startDate, endDate, filterEntity, fleetTreeInput, retrievePaxDetails, isStandByCrew, showRecordStartsInDateRange);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CrewCalendarDataResult> GetCrewCalendarData(DateTime startDate, DateTime endDate, FilterCriteria filterCriteria, Collection<string> crewTreeInput, bool showRecordStartsInDateRange)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewCalendarDataResult> retValue = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CrewCalendarDataResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, endDate, filterCriteria, crewTreeInput, showRecordStartsInDateRange))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        FilterEntity filterEntity = ConvertToBusinessEntity(filterCriteria);
                        retValue = objSchedule.GetCrewCalendarData(startDate, endDate, filterEntity, crewTreeInput, showRecordStartsInDateRange);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        //To get fleet data based on Vendor option filter and homebase only filter applied...
        public ReturnValue<Data.Preflight.FleetResourceTypeResult> GetFleetResourceType(FlightPak.Services.Utility.VendorOption vendorOption, Collection<string> fleetIds, bool homebaseOnly, string homebaseId)
        {
            Business.Common.ReturnValue<Data.Preflight.FleetResourceTypeResult> fleets = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.FleetResourceTypeResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorOption, fleetIds, homebaseOnly, homebaseId))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        fleets = objSchedule.GetFleetResourceType(vendorOption.ToString(), fleetIds, homebaseOnly, homebaseId);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return fleets;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }


        public ReturnValue<Data.Preflight.CrewResourceTypeResult> GetCrewResourceType(Collection<string> crewIds, bool homebaseOnly, string homebaseId, bool isFixedCrewOnly, bool isRotaryWingCrewOnly, string calenderType)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewResourceTypeResult> crewCodes = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.CrewResourceTypeResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewIds, homebaseOnly, homebaseId))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        crewCodes = objSchedule.GetCrewResourceType(crewIds, homebaseOnly, homebaseId, isFixedCrewOnly, isRotaryWingCrewOnly, calenderType);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return crewCodes;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<Data.Preflight.LegendsResult> GetAllLegends()
        {
            Business.Common.ReturnValue<Data.Preflight.LegendsResult> legends = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.LegendsResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        legends = objSchedule.GetAllLegends();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return legends;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        private FilterEntity ConvertToBusinessEntity(FilterCriteria filterCriteria)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(filterCriteria))
            {

                FilterEntity filterEntity = new FilterEntity();
                filterEntity.AllCrew = filterCriteria.AllCrew;
                filterEntity.Canceled = filterCriteria.Canceled;
                filterEntity.ClientID = filterCriteria.ClientID;
                filterEntity.Client = filterCriteria.Client;
                filterEntity.CrewDutyID = filterCriteria.CrewDutyID;
                filterEntity.CrewDuty = filterCriteria.CrewDuty;
                filterEntity.DepartmentID = filterCriteria.DepartmentID;
                filterEntity.Department = filterCriteria.Department;
                filterEntity.FixedWingCrewOnly = filterCriteria.FixedWingCrewOnly;
                filterEntity.FlightCategoryID = filterCriteria.FlightCategoryID;
                filterEntity.FlightCategory = filterCriteria.FlightCategory;
                filterEntity.Hold = filterCriteria.Hold;
                filterEntity.HomeBaseID = filterCriteria.HomeBaseID;
                filterEntity.HomeBase = filterCriteria.HomeBase;
                filterEntity.HomeBaseOnly = filterCriteria.HomeBaseOnly;
                filterEntity.RequestorID = filterCriteria.RequestorID;
                filterEntity.Requestor = filterCriteria.Requestor;
                filterEntity.RotaryWingCrewCrewOnly = filterCriteria.RotaryWingCrewCrewOnly;
                filterEntity.SchedServ = filterCriteria.SchedServ;
                filterEntity.TimeBase = (FlightPak.Business.Preflight.ScheduleCalendarEntities.TimeBase)filterCriteria.TimeBase;
                filterEntity.Trip = filterCriteria.Trip;
                filterEntity.UnFulfilled = filterCriteria.UnFulfilled;
                filterEntity.VendorsFilter = (FlightPak.Business.Preflight.ScheduleCalendarEntities.VendorOption)filterCriteria.VendorsFilter;
                filterEntity.WorkSheet = filterCriteria.WorkSheet;

                return filterEntity;
            }
        }


        public ReturnValue<FleetPlannerCalendarDataResult> GetFleetPlannerCalendarData(DateTime startDate, short timeintervel, FilterCriteria filterCriteria, Collection<string> fleetTreeInput, bool IsTrip, bool IsHold, bool IsWorkSheet, bool IsCanceled, bool IsUnFulfilled)
        {
            Business.Common.ReturnValue<Data.Preflight.FleetPlannerCalendarDataResult> retValue = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FleetPlannerCalendarDataResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, timeintervel, filterCriteria, fleetTreeInput))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        FilterEntity filterEntity = ConvertToBusinessEntity(filterCriteria);
                        retValue = objSchedule.GetFleetPlannerCalendarData(startDate, timeintervel, filterEntity, fleetTreeInput, IsTrip, IsHold, IsWorkSheet, IsCanceled, IsUnFulfilled);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        public ReturnValue<CrewPlannerCalendarDataResult> GetCrewPlannerCalendarData(DateTime startDate, short timeintervel, FilterCriteria filterCriteria, Collection<string> crewTreeInput, bool IsTrip, bool IsHold, bool IsWorkSheet, bool IsCanceled, bool IsUnFulfilled)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewPlannerCalendarDataResult> retValue = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CrewPlannerCalendarDataResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, timeintervel, filterCriteria, crewTreeInput))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        FilterEntity filterEntity = ConvertToBusinessEntity(filterCriteria);
                        retValue = objSchedule.GetCrewPlannerCalendarData(startDate, timeintervel, filterEntity, crewTreeInput, IsTrip, IsHold, IsWorkSheet, IsCanceled, IsUnFulfilled);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }
        #endregion

        #region "Schedule Calendar Entries From Context Menu"

        public ReturnValue<FleetCalendarEntriesResult> GetFleetCalendarEntries(string timebase, DateTime startdate, DateTime enddate)
        {
            Business.Common.ReturnValue<Data.Preflight.FleetCalendarEntriesResult> retValue = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FleetCalendarEntriesResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timebase, startdate, enddate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        retValue = objSchedule.GetFleetCalendarEntries(timebase.ToString(), startdate, enddate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<CrewCalendarEntriesResult> GetCrewCalendarEntries(string timebase, DateTime startdate, DateTime enddate)
        {
            Business.Common.ReturnValue<Data.Preflight.CrewCalendarEntriesResult> retValue = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CrewCalendarEntriesResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timebase, startdate, enddate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                        retValue = objSchedule.GetCrewCalendarEntries(timebase.ToString(), startdate, enddate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        //public ReturnValue<TripHistoryResult> GetTripHistoryResult(Int64 TripID)
        //{
        //    Business.Common.ReturnValue<Data.Preflight.TripHistoryResult> retValue = null;
        //    //Handle methods throguh exception manager with return flag
        //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //    return exManager.Process<ReturnValue<TripHistoryResult>>(() =>
        //    {
        //        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
        //                retValue = objSchedule.GetTripHistoryResult(TripID);
        //            }, FlightPak.Common.Constants.Policy.BusinessLayer);
        //        }
        //        return retValue;
        //    }, FlightPak.Common.Constants.Policy.ServiceLayer);
        //}     

        #endregion
        #region History
        public ReturnValue<Data.Preflight.PreflightTripHistory> GetHistory(long CustomerId, long TripId)
        {
            Business.Common.ReturnValue<Data.Preflight.PreflightTripHistory> objPreflightMains = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.PreflightTripHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, TripId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objPreflightMains = PreflightMngr.GetHistory(CustomerId, TripId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objPreflightMains;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region Tree view

        public ReturnValue<Data.Preflight.FleetTreeResult> GetFleetInfoForFleetTree(bool homebaseOnly)
        {

            Business.Common.ReturnValue<Data.Preflight.FleetTreeResult> retValue = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FleetTreeResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();

                        retValue = objSchedule.GetFleetInfoForFleetTree(homebaseOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        public ReturnValue<Data.Preflight.CrewTreeResult> GetCrewInfoForCrewTree(bool homebaseOnly)
        {

            Business.Common.ReturnValue<Data.Preflight.CrewTreeResult> retValue = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<CrewTreeResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseOnly))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();

                        retValue = objSchedule.GetCrewInfoForCrewTree(homebaseOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        # endregion

        #region UWA Interface

        #region "TSS"
        public ReturnValue<PreflightMain> GetUWAL(Int64 TripID, bool isUWASubmit)
        {
            ReturnValue<PreflightMain> objMain = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                objMain = new ReturnValue<PreflightMain>();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetUWAL(TripID, isUWASubmit);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<PreflightMain> UpdateUWAL(PreflightMain uwal)
        {
            ReturnValue<PreflightMain> objMain = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                objMain = new ReturnValue<PreflightMain>();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.UpdateUWAL(uwal);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetUWACrewPassportVisa_Result> GetUWACrewPassportVisa(Int64 LegID, Int64 CustomerID)
        {
            ReturnValue<Data.Preflight.GetUWACrewPassportVisa_Result> objMain = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetUWACrewPassportVisa_Result>>(() =>
            {
                objMain = new ReturnValue<GetUWACrewPassportVisa_Result>();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetUWACrewPassportVisa(LegID, CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetUWAPassengerPassportVisa_Result> GetUWAPassengerPassportVisa(Int64 LegID, Int64 CustomerID)
        {
            ReturnValue<Data.Preflight.GetUWAPassengerPassportVisa_Result> objMain = null;
            PreflightManager PreflightMngr = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetUWAPassengerPassportVisa_Result>>(() =>
            {
                objMain = new ReturnValue<GetUWAPassengerPassportVisa_Result>();
                PreflightMngr = new PreflightManager();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        objMain = PreflightMngr.GetUWAPassengerPassportVisa(LegID, CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<List<string>> TSSSubmit(Int64 TripID, string UWATripID, bool isManualSubmit)
        {
            ReturnValue<List<string>> objMain = null;
            PreflightManager PreflightMngr = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<List<string>>>(() =>
            {
                objMain = new ReturnValue<List<string>>();
                PreflightMngr = new PreflightManager();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string InterfaceRequesterAppname = ConfigurationManager.AppSettings["InterfaceRequesterAppname"];
                        objMain = PreflightMngr.TSSSubmit(TripID, UWATripID, isManualSubmit, InterfaceRequesterAppname);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<List<string>> TSSCheckStatus(string UWATripID)
        {
            ReturnValue<List<string>> objMain = null;
            PreflightManager PreflightMngr = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<List<string>>>(() =>
            {
                objMain = new ReturnValue<List<string>>();
                PreflightMngr = new PreflightManager();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string InterfaceRequesterAppname = ConfigurationManager.AppSettings["InterfaceRequesterAppname"];
                        objMain = PreflightMngr.TSSCheckStatus(UWATripID, InterfaceRequesterAppname);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region EAPIS

        public ReturnValue<PreflightMain> GetAPIS(Int64 TripID)
        {
            ReturnValue<PreflightMain> objMain = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                objMain = new ReturnValue<PreflightMain>();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetAPIS(TripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<List<string>> EAPISSubmit(Int64 TripID, List<string> PrefLegLst)
        {
            ReturnValue<List<string>> objMain = null;
            PreflightManager PreflightMngr = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<List<string>>>(() =>
            {
                objMain = new ReturnValue<List<string>>();
                PreflightMngr = new PreflightManager();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        string InterfaceRequesterDomain = ConfigurationManager.AppSettings["InterfaceRequesterDomain"];
                        objMain = PreflightMngr.EAPISSubmit(TripID, PrefLegLst, InterfaceRequesterDomain);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public void UpdateAPISException(Int64 TripID, string APISException)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID, APISException))
            {
                try
                {
                    FlightPak.Business.Preflight.PreflightManager objPreflight = new FlightPak.Business.Preflight.PreflightManager();
                    objPreflight.UpdateAPISException(TripID, APISException);
                }
                catch (System.Exception exc)
                {
                    //Manually handled
                    ServiceFault ex = new ServiceFault();
                    ex.Title = "Function:UpdateAPISException()";
                    ex.InnerException = exc.InnerException != null ? exc.InnerException.StackTrace : null;
                    ex.ExceptionMessage = exc.InnerException != null ? exc.InnerException.Message : exc.Message;
                    ex.StackTrace = exc.StackTrace;
                    throw new FaultException<ServiceFault>(ex, ex.ExceptionMessage);
                }
            }
        }

        #endregion

        #region FUEL

        public ReturnValue<PreflightFuel> GetFuelPrices(List<string> ICAOID, long AirportID)
        {
            ReturnValue<PreflightFuel> objMain = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightFuel>>(() =>
            {
                objMain = new ReturnValue<PreflightFuel>();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        string FuelInterfaceUsername = ConfigurationManager.AppSettings["FuelInterfaceUsername"];
                        string FuelInterfacePassword = ConfigurationManager.AppSettings["FuelInterfacePassword"];
                        if (!string.IsNullOrWhiteSpace(FuelInterfaceUsername) && !string.IsNullOrWhiteSpace(FuelInterfacePassword))
                            objMain = PreflightMngr.GetFuelPrices(ICAOID, AirportID, FuelInterfaceUsername, FuelInterfacePassword);
                        else
                            objMain.ReturnFlag = false;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #endregion

        #region "Fuel"
        public ReturnValue<FlightPakFuelData> GetFlightPakFuelData(Int64 TripID, Int64 DepatureICAOID)
        {
            Business.Common.ReturnValue<FlightPakFuelData> objFuel = null;
            PreflightManager PreflightMngr = new PreflightManager();
            objFuel = PreflightMngr.GetFlightPakFuelData(TripID, DepatureICAOID);
            return objFuel;
        }


        public ReturnValue<FBOBestPrice_Result> GetFBOBestPrice(Int64 TripID, Int64 DepatureICAOID, decimal Gallon)
        {
            Business.Common.ReturnValue<FBOBestPrice_Result> objFuel = null;
            PreflightManager PreflightMngr = new PreflightManager();
            objFuel = PreflightMngr.GetFBOBestPrice(TripID, DepatureICAOID, Gallon);
            return objFuel;

        }


        public bool UpdatePreflightFuelDetails(Int64 TripID)
        {

            bool UpdateDetails = false;
            try
            {
                PreflightManager PreflightMngr = new PreflightManager();

                UpdateDetails = PreflightMngr.UpdatePreflightFuelDetails(TripID);

                return UpdateDetails;
            }
            catch (System.Exception exc)
            {
                //Manually Handled
                UpdateDetails = false;
            }
            return UpdateDetails;
        }

        #endregion

        #region "SIFL"

        public ReturnValue<Data.Preflight.GetSIFLPassengerInfo> GetSIFLPassengerInfo(Int64 tripID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetSIFLPassengerInfo> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetSIFLPassengerInfo>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.GetSIFLPassengerInfo(tripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetAllPreflightTripSIFL> GetAllPreflightTripSIFL(Int64 tripID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAllPreflightTripSIFL> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAllPreflightTripSIFL>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.GetAllPreflightTripSIFL(tripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetSIFLPassengerEmployeeType> GetSIFLPassengerEmployeeType(Int64 passengerRequestorID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetSIFLPassengerEmployeeType> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetSIFLPassengerEmployeeType>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.GetSIFLPassengerEmployeeType(passengerRequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.CheckPreflightTripSIFL> CheckPreflightTripSIFL(Int64 passengerRequestorID, Int64 departIcaoID, Int64 arrivalIcaoID)
        {
            Business.Common.ReturnValue<Data.Preflight.CheckPreflightTripSIFL> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.CheckPreflightTripSIFL>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerRequestorID, departIcaoID, arrivalIcaoID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.CheckPreflightTripSIFL(passengerRequestorID, departIcaoID, arrivalIcaoID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetPreflightTripSIFLID> GetPreflightTripSIFLID()
        {
            Business.Common.ReturnValue<Data.Preflight.GetPreflightTripSIFLID> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetPreflightTripSIFLID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.GetPreflightTripSIFLID();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetPassenger> GetPassengerbyIDOrCD(long PassengerRequestorID, string PassengerRequestorCD)
        {
            Business.Common.ReturnValue<Data.Preflight.GetPassenger> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetPassenger>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.GetPassengerbyIDOrCD(PassengerRequestorID, PassengerRequestorCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<Data.Preflight.GetAssociatePassengerID> GetAssociatePassengerID(Int64 PassengerRequestorID)
        {
            Business.Common.ReturnValue<Data.Preflight.GetAssociatePassengerID> objSIFL = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.Preflight.GetAssociatePassengerID>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objSIFL = PreflightMngr.GetAssociatePassengerID(PassengerRequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objSIFL;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<PreflightTripSIFL> AddPreflightTripSIFL(PreflightTripSIFL SIFL)
        {
            Business.Common.ReturnValue<PreflightTripSIFL> objMain = null;
            PreflightManager PreflightMngr = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightTripSIFL>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SIFL))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objMain = PreflightMngr.UpdatePreflightTripSIFL(SIFL);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<PreflightTripSIFL> UpdatePreflightTripSIFL(PreflightTripSIFL Sifl)
        {
            Business.Common.ReturnValue<PreflightTripSIFL> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightTripSIFL>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Sifl))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objMain = PreflightMngr.UpdatePreflightTripSIFL(Sifl);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<PreflightTripHistory> UpdatePreflightTripHistory(PreflightTripHistory history)
        {
            Business.Common.ReturnValue<PreflightTripHistory> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightTripHistory>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(history))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objMain = PreflightMngr.UpdatePreflightTripHistory(history);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public ReturnValue<PreflightTripSIFL> DeletePreflightTripSIFL(Int64 SIFLID)
        {
            Business.Common.ReturnValue<PreflightTripSIFL> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightTripSIFL>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SIFLID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objMain = PreflightMngr.DeletePreflightTripSIFL(SIFLID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<PreflightTripSIFL> DeleteAllPreflightTripSIFL(Int64 TripID)
        {
            Business.Common.ReturnValue<PreflightTripSIFL> objMain = null;
            PreflightManager PreflightMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightTripSIFL>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();

                        objMain = PreflightMngr.DeleteAllPreflightTripSIFL(TripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region MoveTrip
        public ReturnValue<PreflightLeg> MoveTrip(List<PreflightLeg> NewLegList, List<PreflightLeg> OldLegList, Int64 OldTripID, Int64 NewTripID)
        {
            Business.Common.ReturnValue<PreflightLeg> objMain = null;
            PreflightManager PreflightMngr = null;


            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightLeg>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewLegList, OldLegList, OldTripID, NewTripID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.UpdatePreflightLegForMove(NewLegList, OldLegList, OldTripID, NewTripID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region Validate Entire Trip

        //public ReturnValue<Data.Preflight.PreflightTripException> ValidateTripPriority(PreflightMain Trip)
        //{
        //    Business.Common.ReturnValue<Data.Preflight.PreflightTripException> objValidateTrip = new ReturnValue<PreflightTripException>();
        //    try
        //    {
        //        PreflightManager PreflightMngr = new PreflightManager();
        //        objValidateTrip = PreflightMngr.ValidateTripPriorityExcep(Trip);
        //    }
        //    catch (System.Exception exc)
        //    {
        //    }
        //    return objValidateTrip;
        //}

        public ReturnValue<Data.Preflight.PreflightTripException> ValidateTripforMandatoryExceptions(PreflightMain Trip)
        {
            Business.Common.ReturnValue<Data.Preflight.PreflightTripException> objValidateTrip = new ReturnValue<PreflightTripException>();
            PreflightManager PreflightMngr = new PreflightManager();
            objValidateTrip = PreflightMngr.ValidateTripforMandatoryExcep(Trip);
            return objValidateTrip;
        }

        public ReturnValue<ExceptionTemplate> GetExceptionTemplateList()
        {
            Business.Common.ReturnValue<ExceptionTemplate> objExceptionList = new ReturnValue<ExceptionTemplate>();
            PreflightManager PreflightMngr = new PreflightManager();
            objExceptionList = PreflightMngr.GetExceptionTemplateList();
            return objExceptionList;
        }

        public ReturnValue<PreflightTripException> GetTripExceptionList(Int64 TripID)
        {
            Business.Common.ReturnValue<PreflightTripException> objExceptionList = new ReturnValue<PreflightTripException>();
            PreflightManager PreflightMngr = new PreflightManager();
            objExceptionList = PreflightMngr.GetTripExceptionList(TripID);
            return objExceptionList;
        }
        #endregion

        #region Update CorporateRequest from Preflight
        public ReturnValue<PreflightMain> UpdateCRFromPreflight(PreflightMain Trip)
        {
            PreflightManager PreflightMngr = null;
            Business.Common.ReturnValue<PreflightMain> objMain = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.UpdateCRFromPreflight(Trip);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region "Copy CQ to Preflight"
        public ReturnValue<PreflightMain> CopyCQToPreflight(Int64 CQFileID, Int64 CQMainID, Boolean IsAutoDispatch, string TripStatus, Boolean IsRevision)
        {

            Business.Common.ReturnValue<PreflightMain> objMain = null;
            PreflightManager CqMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, CQMainID, IsAutoDispatch, TripStatus, IsRevision))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CqMngr = new PreflightManager();
                        objMain = CqMngr.GetCQQuotetoPreflight(CQFileID, CQMainID, IsAutoDispatch, TripStatus, IsRevision);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Copy Crew Calender Entries"
        public ReturnValue<PreflightMain> CopyCrewCalenderDetails(DateTime DepartDate, long TripID, long CustomerId)
        {

            Business.Common.ReturnValue<PreflightMain> objMain = null;
            PreflightManager CqMngr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PreflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, TripID, CustomerId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CqMngr = new PreflightManager();
                        objMain = CqMngr.CopyCrewCalenderDetails(DepartDate, TripID, CustomerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region PostFlight
        public ReturnValue<Data.Preflight.PostflightMain> GetLogByTripID(long tripID)
        {
            Business.Common.ReturnValue<Data.Preflight.PostflightMain> objMain = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<PostflightMain>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightManager PreflightMngr = new PreflightManager();
                        objMain = PreflightMngr.GetLogByTripID(tripID);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion

        #region Trip Email

        public void SendEmailToCrewList(PreflightMain Trip)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendEmailToCrewList(Trip);
        }

        public void SendEmailToPaxList(PreflightMain Trip)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendEmailToPaxList(Trip);
        }

        public void SendTripDetails(PreflightMain Trip, List<string> EmailList)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendTripDetails(Trip, EmailList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="EmailList"></param>
        public void SendTripDetailsWithFromEmail(PreflightMain Trip, List<string> EmailList, string FromEmail, string HtmlBody, List<String> attachments)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendTripDetails(Trip, EmailList, FromEmail, HtmlBody, attachments);
        }

        // Fix for PROD-42 (#57,174)
        public void SendItineraryDetailsToOthers(PreflightMain Trip, List<string> EmailList)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendItineraryDetailsToOthers(Trip, EmailList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="EmailList"></param>
        /// <param name="FromEmail"></param>
        /// <param name="HtmlBody"></param>
        public void SendItineraryDetailsToOthersWithFromEmail(PreflightMain Trip, List<string> EmailList, string FromEmail, string HtmlBody, List<String> attachments)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendItineraryDetailsToOthers(Trip, EmailList, FromEmail, HtmlBody, attachments);
        }

        public void SendItineraryDetailsToPAX(PreflightMain Trip, string EmailID, List<GetPaxItineraryInfo> PaxItineraryList)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendItineraryDetailsToPAX(Trip, EmailID, PaxItineraryList);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="EmailID"></param>
        /// <param name="PaxItineraryList"></param>
        /// <param name="FromEmail"></param>
        /// <param name="HtmlBody"></param>
        public void SendItineraryDetailsToPAXWithFromEmail(PreflightMain Trip, string EmailID, string FromEmail, string HtmlBody, List<String> attachments)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            PreflightMngr.SendItineraryDetailsToPAX(Trip, EmailID, FromEmail, HtmlBody, attachments);
        }

        public List<GetPaxItineraryInfo> GetPassengerItinerary(PreflightMain Trip, Int64? PaxID)
        {
            PreflightManager PreflightMngr = new PreflightManager();
            return PreflightMngr.GetPassengerItinerary(Trip, PaxID);
        }

        #endregion

        public Business.Common.ReturnValue<Data.Preflight.GetPreReportSearch> GetPreReportList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate, DateTime EndDate, Int64 FromTripNUM, Int64 ToTripNUM)
        {
            Business.Common.ReturnValue<Data.Preflight.GetPreReportSearch> retVal = null;
            PreflightManager PreflightMgr = null;

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.Preflight.GetPreReportSearch>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID, ClientID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromTripNUM, ToTripNUM))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PreflightMgr = new PreflightManager();
                        retVal = PreflightMgr.GetPreReportList(HomeBaseID, ClientID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromTripNUM, ToTripNUM);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retVal;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
    }
}
