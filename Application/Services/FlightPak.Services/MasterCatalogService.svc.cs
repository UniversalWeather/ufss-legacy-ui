﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Configuration;
using FlightPak.Common.Constants;
using FlightPak.Common;
namespace FlightPak.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MasterCatalogService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class MasterCatalogService : BaseService, IMasterCatalogService
    {
        private ExceptionManager exManager;
        #region "Account Manager"
        #region For Performance Optimization
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAccountWithFilters> GetAccountWithFilters(string AccountNum ,long AccountID ,string FetchHomebaseOnly ,bool FetchInactiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAccountWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAccountWithFilters> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AccountManager accountManager = new FlightPak.Business.MasterCatalog.AccountManager();
                        objAcct = accountManager.GetAccountWithFilters(AccountNum ,AccountID ,FetchHomebaseOnly ,FetchInactiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        public Business.Common.ReturnValue<Data.MasterCatalog.Account> AddAccount(Data.MasterCatalog.Account account)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Account>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Account> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(account))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        account.CustomerID = UserPrincipal.Identity.CustomerID;
                        account.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AccountManager accountManager = new FlightPak.Business.MasterCatalog.AccountManager();
                        objAcct = accountManager.Add(account);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Account> DeleteAccount(Data.MasterCatalog.Account account)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Account>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Account> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(account))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        account.CustomerID = UserPrincipal.Identity.CustomerID;
                        account.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AccountManager accountManager = new FlightPak.Business.MasterCatalog.AccountManager();
                        objAcct = accountManager.Delete(account);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Account> UpdateAccount(Data.MasterCatalog.Account account)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Account>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Account> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(account))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        account.CustomerID = UserPrincipal.Identity.CustomerID;
                        account.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AccountManager accountManager = new FlightPak.Business.MasterCatalog.AccountManager();
                        objAcct = accountManager.Update(account);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Account> GetAccountList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Account>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Account> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AccountManager accountManager = new FlightPak.Business.MasterCatalog.AccountManager();
                        objAcct = accountManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAccounts> GetAllAccountList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAccounts>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAccounts> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AccountManager accountManager = new FlightPak.Business.MasterCatalog.AccountManager();
                        objAcct = accountManager.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Passenger Group Manager"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="PassengerGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> AddPaxGroup(Data.MasterCatalog.PassengerGroup PassengerGroupData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroupData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PassengerGroupData.CustomerID = UserPrincipal.Identity.CustomerID;
                        PassengerGroupData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerGroupManager PassengerGroupManagerCatalog = new FlightPak.Business.MasterCatalog.PassengerGroupManager();
                        ReturnValue = PassengerGroupManagerCatalog.Add(PassengerGroupData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="PassengerGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> UpdatePaxGroup(Data.MasterCatalog.PassengerGroup PassengerGroupData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroupData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PassengerGroupData.CustomerID = UserPrincipal.Identity.CustomerID;
                        PassengerGroupData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerGroupManager PassengerGroupManagerCatalog = new FlightPak.Business.MasterCatalog.PassengerGroupManager();
                        ReturnValue = PassengerGroupManagerCatalog.Update(PassengerGroupData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="PassengerGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> DeletePaxGroup(Data.MasterCatalog.PassengerGroup PassengerGroupData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroupData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        PassengerGroupData.CustomerID = UserPrincipal.Identity.CustomerID;
                        PassengerGroupData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerGroupManager PassengerGroupManagerCatalog = new FlightPak.Business.MasterCatalog.PassengerGroupManager();
                        ReturnValue = PassengerGroupManagerCatalog.Delete(PassengerGroupData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to lock records in table and returns generics
        /// </summary>
        /// <param name="PassengerGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> GetPaxGroupLock(Data.MasterCatalog.PassengerGroup PassengerGroupData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> GetPaxGroupList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerGroupManager PassengerGroupManagerCatalog = new FlightPak.Business.MasterCatalog.PassengerGroupManager();
                        ReturnValue = PassengerGroupManagerCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupList> GetPaxGroupListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupList> ReturnValue = null;
                FlightPak.Business.MasterCatalog.PassengerGroupManager PassengerGroupManagerCatalog = new FlightPak.Business.MasterCatalog.PassengerGroupManager();
                ReturnValue = PassengerGroupManagerCatalog.GetListInfo();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Delay Type Manager"
        #region For Performance Optimization

         
 

        public Business.Common.ReturnValue<Data.MasterCatalog.DelayType> GetAllDelayTypeWithFilters(long DelayTypeID,string DelayTypeCD ,bool FetchActiveOnly )
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DelayType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DelayType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DelayTypeManager objDelayType = new FlightPak.Business.MasterCatalog.DelayTypeManager();
                        retValue = objDelayType.GetAllDelayTypeWithFilters( DelayTypeID, DelayTypeCD ,FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.DelayType> AddDelayType(Data.MasterCatalog.DelayType delayType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DelayType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DelayType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        delayType.CustomerID = UserPrincipal.Identity.CustomerID;
                        delayType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DelayTypeManager objDelayType = new FlightPak.Business.MasterCatalog.DelayTypeManager();
                        retValue = objDelayType.Add(delayType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DelayType> UpdateDelayType(Data.MasterCatalog.DelayType delayType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DelayType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DelayType> retValue = null;
                delayType.CustomerID = UserPrincipal.Identity.CustomerID;
                delayType.LastUpdUID = UserPrincipal.Identity.Name;
                FlightPak.Business.MasterCatalog.DelayTypeManager objDelayType = new FlightPak.Business.MasterCatalog.DelayTypeManager();
                retValue = objDelayType.Update(delayType);
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DelayType> DeleteDelayType(Data.MasterCatalog.DelayType delayType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DelayType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DelayType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        delayType.CustomerID = UserPrincipal.Identity.CustomerID;
                        delayType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DelayTypeManager objDelayType = new FlightPak.Business.MasterCatalog.DelayTypeManager();
                        retValue = objDelayType.Delete(delayType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DelayType> GetDelayTypeLock(Data.MasterCatalog.DelayType delayType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DelayType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DelayType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DelayTypeManager objDelayType = new FlightPak.Business.MasterCatalog.DelayTypeManager();
                        retValue = objDelayType.GetLock(delayType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DelayType> GetDelayTypeList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DelayType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DelayType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DelayTypeManager objDelayType = new FlightPak.Business.MasterCatalog.DelayTypeManager();
                        retValue = objDelayType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Aircraft Manager"

        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftWithFilters(string AircraftCD, long AircraftID, bool FetchInactiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftManager aircraftManager = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = aircraftManager.GetAircraftWithFilters(AircraftCD, AircraftID, FetchInactiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftByAircraftID(Int64 AircraftID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftManager aircraftManager = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = aircraftManager.GetAircraftByAircraftID(AircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAircraft> GetAircraftList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftManager aircraftManager = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = aircraftManager.GetAircraftInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftManager aircraftManager = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = aircraftManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> AddAircraft(Data.MasterCatalog.Aircraft aircraft)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        aircraft.CustomerID = UserPrincipal.Identity.CustomerID;
                        aircraft.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AircraftManager objAircraftType = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = objAircraftType.Add(aircraft);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> UpdateAircraft(Data.MasterCatalog.Aircraft aircraft)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        aircraft.CustomerID = UserPrincipal.Identity.CustomerID;
                        aircraft.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AircraftManager objAircraftType = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = objAircraftType.Update(aircraft);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> DeleteAircraft(Data.MasterCatalog.Aircraft aircraft)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        aircraft.CustomerID = UserPrincipal.Identity.CustomerID;
                        aircraft.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AircraftManager objAircraftType = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = objAircraftType.Delete(aircraft);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftLock(Data.MasterCatalog.Aircraft aircraft)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Aircraft>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Aircraft> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftManager aircraftManager = new FlightPak.Business.MasterCatalog.AircraftManager();
                        retValue = aircraftManager.GetLock(aircraft);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Int64? GetAircraftUsedCount()
        {
            Int64? AircraftUsedCount = 0;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int64?>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objAircraftManager = new FlightPak.Business.MasterCatalog.FleetManager();
                        AircraftUsedCount = objAircraftManager.GetAircraftUsedCount();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return AircraftUsedCount;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Int64? GetAircraftActualCount()
        {
            Int64? AircraftActualCount = 0;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int64?>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objAircraftManager = new FlightPak.Business.MasterCatalog.FleetManager();
                        AircraftActualCount = objAircraftManager.GetAircraftActualCount();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return AircraftActualCount;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion "Aircraft Manager"
        #region "FleetGroupMaster"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> GetFleetList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager fleetGroupManager = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        retValue = fleetGroupManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> AddFleetType(Data.MasterCatalog.FleetGroup fleetgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager fleetGroupManager = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        fleetgroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetgroup.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = fleetGroupManager.Add(fleetgroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> UpdateFleetType(Data.MasterCatalog.FleetGroup fleetgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager fleetGroupManager = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        fleetgroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetgroup.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = fleetGroupManager.Update(fleetgroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> DeleteFleetType(Data.MasterCatalog.FleetGroup fleetgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager fleetGroupManager = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        fleetgroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetgroup.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = fleetGroupManager.Delete(fleetgroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> GetFleetTypeLock(Data.MasterCatalog.FleetGroup fleetgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager fleetGroupManager = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        retValue = fleetGroupManager.GetLock(fleetgroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetGroup> GetFleetGroup()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager objMetroCityType = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        retValue = objMetroCityType.GetFleetGroup();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion "FleetGroupMaster"
        #region "MetroCity Manager"

        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.Metro> GetMetroWithFilters(long MetroId, string MetroCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Metro>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Metro> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.MetroCityManager objMetroCityType = new FlightPak.Business.MasterCatalog.MetroCityManager();
                        retValue = objMetroCityType.GetMetroWithFilters(MetroId,MetroCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.Metro> AddMetroCity(Data.MasterCatalog.Metro metroCityType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Metro>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Metro> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(metroCityType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.MetroCityManager objMetroCityType = new FlightPak.Business.MasterCatalog.MetroCityManager();
                        metroCityType.CustomerID = UserPrincipal.Identity.CustomerID;
                        metroCityType.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objMetroCityType.Add(metroCityType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Metro> UpdateMetroCity(Data.MasterCatalog.Metro metroCityType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Metro>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Metro> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.MetroCityManager objMetroCityType = new FlightPak.Business.MasterCatalog.MetroCityManager();
                        metroCityType.CustomerID = UserPrincipal.Identity.CustomerID;
                        metroCityType.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objMetroCityType.Update(metroCityType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Metro> DeleteMetroCity(Data.MasterCatalog.Metro metroCityType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Metro>>(() =>
                {
                    Business.Common.ReturnValue<Data.MasterCatalog.Metro> retValue = null;
                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(metroCityType))
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            FlightPak.Business.MasterCatalog.MetroCityManager objMetroCityType = new FlightPak.Business.MasterCatalog.MetroCityManager();
                            metroCityType.CustomerID = UserPrincipal.Identity.CustomerID;
                            metroCityType.LastUpdUID = UserPrincipal.Identity.Name;
                            retValue = objMetroCityType.Delete(metroCityType);
                        }, FlightPak.Common.Constants.Policy.BusinessLayer);
                    }
                    return retValue;
                }, FlightPak.Common.Constants.Policy.ServiceLayer);
            }
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Metro> GetMetroCityTypeLock(Data.MasterCatalog.Metro metroCityType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Metro>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Metro> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(metroCityType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.MetroCityManager objMetroCityType = new FlightPak.Business.MasterCatalog.MetroCityManager();
                        retValue = objMetroCityType.GetLock(metroCityType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Metro> GetMetroCityList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Metro>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Metro> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.MetroCityManager objMetroCityType = new FlightPak.Business.MasterCatalog.MetroCityManager();
                        retValue = objMetroCityType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "DSTRegion Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> AddDSTRegion(Data.MasterCatalog.DSTRegion dSTRegionType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // delayType.CustomerID = UserPrincipal.Identity.CustomerID;
                        dSTRegionType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DSTRegionManager objDSTRegionType = new FlightPak.Business.MasterCatalog.DSTRegionManager();
                        retValue = objDSTRegionType.Add(dSTRegionType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> UpdateDSTRegion(Data.MasterCatalog.DSTRegion dSTRegionType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        // delayType.CustomerID = UserPrincipal.Identity.CustomerID;
                        dSTRegionType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DSTRegionManager objDSTRegionType = new FlightPak.Business.MasterCatalog.DSTRegionManager();
                        retValue = objDSTRegionType.Update(dSTRegionType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> DeleteDSTRegion(Data.MasterCatalog.DSTRegion dSTRegionType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        dSTRegionType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DSTRegionManager objDSTRegionType = new FlightPak.Business.MasterCatalog.DSTRegionManager();
                        retValue = objDSTRegionType.Delete(dSTRegionType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> GetDSTRegionTypeLock(Data.MasterCatalog.DSTRegion dSTRegionType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(dSTRegionType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DSTRegionManager objDSTRegionType = new FlightPak.Business.MasterCatalog.DSTRegionManager();
                        retValue = objDSTRegionType.GetLock(dSTRegionType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> GetDSTRegionList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DSTRegion> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DSTRegionManager objDSTRegionType = new FlightPak.Business.MasterCatalog.DSTRegionManager();
                        retValue = objDSTRegionType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Flight Category Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters> GetAllFlightCategoryWithFilters(long ClientID, long FlightCategoryID, string FlightCategoryCD, bool FetchInactiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightCategoryManager objFlightCategory = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        retValue = objFlightCategory.GetAllFlightCategoryWithFilters(ClientID,FlightCategoryID, FlightCategoryCD, FetchInactiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> AddFlightCategory(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        flightCategory.CustomerID = UserPrincipal.Identity.CustomerID;
                        flightCategory.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FlightCategoryManager objFlightCategory = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        retValue = objFlightCategory.Add(flightCategory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> UpdateFlightCategory(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        flightCategory.CustomerID = UserPrincipal.Identity.CustomerID;
                        flightCategory.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FlightCategoryManager objFlightCategory = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        retValue = objFlightCategory.Update(flightCategory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> DeleteFlightCategory(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        flightCategory.CustomerID = UserPrincipal.Identity.CustomerID;
                        flightCategory.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FlightCategoryManager objFlightCategory = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        retValue = objFlightCategory.Delete(flightCategory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> GetFlightCategoryLock(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
            //{
            //    try
            //    {
            //        //Handle methods throguh exception manager with return flag
            //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            //        exManager.Process(() =>
            //        {
            throw new NotImplementedException();
            //        }, FlightPak.Common.Constants.Policy.BusinessLayer);
            //    }
            //    catch (Exception ex)
            //    {
            //        //The exception will be handled, logged and replaced by our custom exception. 
            //        throw ex;
            //    }
            //}
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> GetFlightCategoryList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightCatagory> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightCategoryManager objFlightCategory = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        retValue = objFlightCategory.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategories> GetAllFlightCategoryList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategories>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightCategories> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightCategoryManager FlightCategoryCatalog = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        ReturnValue = FlightCategoryCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Added by Scheduling calendar team to get FlightCategory Entity by FlightCategory Code.
        /// </summary>
        /// <param name="flightCategoryCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult> GetFlightCategoryByCategoryCD(string flightCategoryCode)
        {
            Business.Common.ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult> flightCategory = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightCategoryManager objSchedule = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        flightCategory = objSchedule.GetFlightCategoryByCategoryCD(flightCategoryCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return flightCategory;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }
        #endregion
        #region "Flight Purpose Manager"


        public Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> AddFlightPurpose(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        flightPurpose.CustomerID = UserPrincipal.Identity.CustomerID;
                        flightPurpose.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FlightPurposeManager objFlightPurpose = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        retValue = objFlightPurpose.Add(flightPurpose);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> UpdateFlightPurpose(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        flightPurpose.CustomerID = UserPrincipal.Identity.CustomerID;
                        flightPurpose.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FlightPurposeManager objFlightPurpose = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        retValue = objFlightPurpose.Update(flightPurpose);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> DeleteFlightPurpose(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        flightPurpose.CustomerID = UserPrincipal.Identity.CustomerID;
                        flightPurpose.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FlightPurposeManager objFlightPurpose = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        retValue = objFlightPurpose.Delete(flightPurpose);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposeLock(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
            //{
            //    try
            //    {
            //        //Handle methods throguh exception manager with return flag
            //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            //        exManager.Process(() =>
            //        {
            throw new NotImplementedException();
            //        }, FlightPak.Common.Constants.Policy.BusinessLayer);
            //    }
            //    catch (Exception ex)
            //    {
            //        //The exception will be handled, logged and replaced by our custom exception. 
            //        throw ex;
            //    }
            //}
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposeList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightPurposeManager objFlightPurpose = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        retValue = objFlightPurpose.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposes> GetAllFlightPurposeList()
        {
            Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposes> ReturnValue = null;
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposes>>(() =>
            {

                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightPurposeManager FlightPurposeCatalog = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        ReturnValue = FlightPurposeCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposebyFlightPurposeID(Int64 FlightPurposeID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightPurpose> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightPurposeManager objFlightPurpose = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        retValue = objFlightPurpose.GetFlightPurposebyFlightPurposeID(FlightPurposeID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Country Manager"

        #region For Performance Optimization
        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.MasterCatalog.Country> GetCountryWithFilters(string CountryCD, long CountryId)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CountryManager objCountry = new FlightPak.Business.MasterCatalog.CountryManager();
                        retValue = objCountry.GetCountryWithFilters(CountryCD, CountryId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        /// <summary>
        /// To Add the values to the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.MasterCatalog.Country> AddCountryMaster(Data.MasterCatalog.Country Country)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        Country.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CountryManager objCountry = new FlightPak.Business.MasterCatalog.CountryManager();
                        retValue = objCountry.Add(Country);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// To Update the values in the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.MasterCatalog.Country> UpdateCountryMaster(Data.MasterCatalog.Country Country)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Country.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CountryManager objCountry = new FlightPak.Business.MasterCatalog.CountryManager();
                        retValue = objCountry.Update(Country);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// To delete the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.MasterCatalog.Country> DeleteCountryMaster(Data.MasterCatalog.Country Country)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Country.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CountryManager objCountry = new FlightPak.Business.MasterCatalog.CountryManager();
                        retValue = objCountry.Delete(Country);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Country"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Country> GetCountryMasterLock(Data.MasterCatalog.Country Country)
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Country))
            //{
            //    try
            //    {
            //        //Handle methods throguh exception manager with return flag
            //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            //        exManager.Process(() =>
            //        {
            throw new NotImplementedException();
            //        }, FlightPak.Common.Constants.Policy.BusinessLayer);
            //    }
            //    catch (Exception ex)
            //    {
            //        //The exception will be handled, logged and replaced by our custom exception. 
            //        throw ex;
            //    }
            //}
        }
        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.MasterCatalog.Country> GetCountryMasterList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Country>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Country> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CountryManager objCountry = new FlightPak.Business.MasterCatalog.CountryManager();
                        retValue = objCountry.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Aircraft Duty Type Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> AddAircraftDuty(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        aircraftDuty.CustomerID = UserPrincipal.Identity.CustomerID;
                        aircraftDuty.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AircraftDutyTypeManager objAircraftDuty = new FlightPak.Business.MasterCatalog.AircraftDutyTypeManager();
                        retValue = objAircraftDuty.Add(aircraftDuty);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> UpdateAircraftDuty(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        aircraftDuty.CustomerID = UserPrincipal.Identity.CustomerID;
                        aircraftDuty.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AircraftDutyTypeManager objAircraftDuty = new FlightPak.Business.MasterCatalog.AircraftDutyTypeManager();
                        retValue = objAircraftDuty.Update(aircraftDuty);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> DeleteAircraftDuty(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        aircraftDuty.CustomerID = UserPrincipal.Identity.CustomerID;
                        aircraftDuty.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AircraftDutyTypeManager objAircraftDuty = new FlightPak.Business.MasterCatalog.AircraftDutyTypeManager();
                        retValue = objAircraftDuty.Delete(aircraftDuty);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> GetAircraftDutyLock(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            //using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
            //{
            //    try
            //    {
            //        //Handle methods throguh exception manager with return flag
            //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            //        exManager.Process(() =>
            //        {
            throw new NotImplementedException();
            //        }, FlightPak.Common.Constants.Policy.BusinessLayer);
            //    }
            //    catch (Exception ex)
            //    {
            //        //The exception will be handled, logged and replaced by our custom exception. 
            //        throw ex;
            //    }
            //}
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> GetAircraftDuty()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.AircraftDuty> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftDutyTypeManager objAircraftDuty = new FlightPak.Business.MasterCatalog.AircraftDutyTypeManager();
                        retValue = objAircraftDuty.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Int32 GetPreflightLegAircraftDuty(string airtcraftDutyCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Int32>(() =>
            {
                Int32 retValue = 0;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AircraftDutyTypeManager objAircraftDuty = new FlightPak.Business.MasterCatalog.AircraftDutyTypeManager();
                        retValue = objAircraftDuty.GetPreflightLegAircraftDuty(airtcraftDutyCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "EmergencyMaster"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> GetAllEmergencyContactWithFilters(long EmergencyContactID, string EmergencyContactCD, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        retValue = objEmergency.GetAllEmergencyContactWithFilters( EmergencyContactID,  EmergencyContactCD,  FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        public Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> GetEmergencyList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        retValue = objEmergency.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> AddEmergencyType(Data.MasterCatalog.EmergencyContact emergencyContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencyContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        emergencyContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        emergencyContact.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objEmergency.Add(emergencyContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> UpdateEmergencyType(Data.MasterCatalog.EmergencyContact emergencyContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencyContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        emergencyContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        emergencyContact.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objEmergency.Update(emergencyContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> DeleteEmergencyType(Data.MasterCatalog.EmergencyContact emergencyContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencyContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        emergencyContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        emergencyContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        retValue = objEmergency.Delete(emergencyContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> GetEmergencyTypeLock(Data.MasterCatalog.EmergencyContact emergencyContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.EmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencyContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        retValue = objEmergency.GetLock(emergencyContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetEmergencyContact> GetEmergencyListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetEmergencyContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetEmergencyContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.EmergencyManager objEmergency = new FlightPak.Business.MasterCatalog.EmergencyManager();
                        retValue = objEmergency.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "EmergencyMaster"
        #region "Company Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyWithFilters> GetCompanyWithFilters(string HomebaseCD, long HomebaseId, bool FetchInactiveOnly,bool IsDeleted)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.GetCompanyWithFilters(HomebaseCD, HomebaseId, FetchInactiveOnly, IsDeleted);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Company> AddCompanyMaster(Data.MasterCatalog.Company CompanyData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Company>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CompanyData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CompanyData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.Add(CompanyData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Company> UpdateCompanyMaster(Data.MasterCatalog.Company CompanyData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Company>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CompanyData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CompanyData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.Update(CompanyData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Company> DeleteCompanyMaster(Data.MasterCatalog.Company CompanyData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Company>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CompanyData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CompanyData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.Delete(CompanyData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to lock records in table and returns generics
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Company> GetCompanyMasterLock(Data.MasterCatalog.Company CompanyData)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Company> GetCompanyMasterList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Company>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMaster> GetCompanyMasterListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMaster>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMaster> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyID> GetCompanyID()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCompanyID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager objCrew = new FlightPak.Business.MasterCatalog.CompanyManager();
                        retValue = objCrew.GetCompanyID();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.TripPrivacySettings> GetTripPrivacySettings()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripPrivacySettings>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripPrivacySettings> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager objCrew = new FlightPak.Business.MasterCatalog.CompanyManager();
                        retValue = objCrew.GetTripPrivacySettings();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Get TSAType
        /// </summary>
        /// <param name="oTSAData"></param>
        /// <param name="TSAType"></param>
        /// <param name="DestinationFile"></param>
        /// <returns>null</returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Customer> ReadTSACSVFile(FlightPak.Data.MasterCatalog.Customer oTSAData, string TSAType, string TSAFileUrl)
        {
            //Handle methods throguh exception manager with return flag
            //NOTE: TSAFileUrl- Conatins the CSV file content. Not the file URL. TODO: Change TSAFileUrl parametername to TSAData
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Customer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Customer> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTSAData, TSAType, TSAFileUrl))
                {
                    const string TSAUploadCurrentNo = "TSAUploadCurrentNo";
                    Int64 RecordBatchCount = 25000;
                    string strErrUpload = string.Empty, strErrUploadClear = string.Empty, strErrUploadNoFly = string.Empty, strErrUploadSelect = string.Empty;
                    FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();

                                        if (ConfigurationManager.AppSettings["TSABatchRecordCount"] != null)
                    {
                        RecordBatchCount = Convert.ToInt64(ConfigurationManager.AppSettings["TSABatchRecordCount"].ToString());
                    }
                    Int64 RecordCount = 0;
                    if (TSAType == EntitySet.Database.TSANoFly)
                    {

                        if (TSAFileUrl != string.Empty)
                        {
                            using (System.IO.StringReader reader = new System.IO.StringReader(TSAFileUrl))//This not File URL. This File Content
                            {
                                string lines;
                                string[] columns = null;
                                string unitprice = string.Empty;
                                long countval = 0;

                                var TSASequenceNumber = new SequenceNumberManager().GetSequenceNumber(TSAUploadCurrentNo);
                                long TSASequenceNo = Convert.ToInt64(Convert.ToString(TSASequenceNumber).Remove(0, 5));// Remove customerid from TSANoFlyId
                                FlightPak.Data.MasterCatalog.TSANoFly flightTSANoFly = new FlightPak.Data.MasterCatalog.TSANoFly();
                                long TSANoFlyId = 0;
                               
                                reader.ReadLine();//Read first line
                                while ((lines = reader.ReadLine()) != null)
                                {
                                    string[] validatedcolumns = CompanyCatalog.GetValidatedTSARecord(lines);
                                    columns = validatedcolumns;
                                    TSANoFlyId = TSASequenceNo + countval;

                                    try
                                    {

                                        FlightPak.Data.MasterCatalog.TSANoFly TSANoFlyvar = new FlightPak.Data.MasterCatalog.TSANoFly();
                                        TSANoFlyvar.TSANoFlyID = Convert.ToInt64(string.Format("{0}{1}", UserPrincipal.Identity.CustomerID, TSANoFlyId));
                                        TSANoFlyvar.SequenceID = columns[0];
                                        TSANoFlyvar.Cleared = columns[1];
                                        TSANoFlyvar.FirstName = columns[3];
                                        TSANoFlyvar.MiddleName = columns[4];
                                        TSANoFlyvar.LastName = columns[2];
                                        TSANoFlyvar.TSAType = columns[5];
                                        try
                                        {
                                            TSANoFlyvar.DOB = Convert.ToDateTime(columns[6]);
                                        }
                                        catch (Exception Ex)
                                        {
                                            //Manually Handled
                                            strErrUploadNoFly = string.Format("{0} </BR> In TSA - No Fly Invalid DOB in the SID: {1}", strErrUploadNoFly, TSANoFlyvar.SequenceID);
                                        }

                                        TSANoFlyvar.POB = columns[7].Truncate(50);
                                        TSANoFlyvar.Citizen = columns[8].Truncate(18);
                                        TSANoFlyvar.PPID = columns[9].Truncate(100);
                                        TSANoFlyvar.MIC = columns[10].Truncate(300);
                                        TSANoFlyvar.TSATime = DateTime.Now;
                                        TSANoFlyvar.IsDeleted = false;
                                        countval = countval + 1;
                                        oTSAData.TSANoFly.Add(TSANoFlyvar);


                                        RecordCount = RecordCount + 1;
                                        if (RecordCount == RecordBatchCount)
                                        {
                                            CompanyCatalog.AddTSA(oTSAData);
                                            oTSAData.TSANoFly.Clear();
                                            RecordCount = 0;
                                        }

                                    }
                                    catch (Exception Ex)
                                    {
                                        //Manually Handled
                                        strErrUploadNoFly = string.Format("{0}</BR>In TSA - No Fly Error Occurred In SID {1}", strErrUploadNoFly, columns[0]);
                                    }

                                }

                                CompanyCatalog.AddTSA(oTSAData);
                                oTSAData.TSANoFly.Clear();

                                CompanyCatalog.UpdateSequenceNumber(UserPrincipal.Identity.CustomerID, TSANoFlyId + 1);
                                RecordCount = 0;

                            }
                        }
                    }

                    if (TSAType == EntitySet.Database.TSASelect)
                    {

                        if (TSAFileUrl != string.Empty)
                        {
                            using (System.IO.StringReader reader = new System.IO.StringReader(TSAFileUrl))//This not File URL. This File Content
                            {
                                string lines;
                                string[] columns = null;
                                string unitprice = string.Empty;
                                long countval = 0;
                                var TSASequenceNumber = new SequenceNumberManager().GetSequenceNumber(TSAUploadCurrentNo);
                                long TSASequenceNo = Convert.ToInt64(Convert.ToString(TSASequenceNumber).Remove(0, 5));// Remove customerid from TSASequenceNo
                                long TSASelectId = 0;
                                
                                reader.ReadLine();//Read first line
                                while ((lines = reader.ReadLine()) != null)
                                {
                                    string[] validatedcolumns = CompanyCatalog.GetValidatedTSARecord(lines);
                                    columns = validatedcolumns;
                                    TSASelectId = TSASequenceNo + countval;

                                    try
                                    {
                                        FlightPak.Data.MasterCatalog.TSASelect TTSASelectvar = new FlightPak.Data.MasterCatalog.TSASelect();

                                        TTSASelectvar.TSASelectID = Convert.ToInt64(string.Format("{0}{1}", UserPrincipal.Identity.CustomerID, TSASelectId));
                                        TTSASelectvar.SequenceID = columns[0];
                                        TTSASelectvar.Cleared = columns[1];
                                        TTSASelectvar.FirstName = columns[3];
                                        TTSASelectvar.MiddleName = columns[4];
                                        TTSASelectvar.LastName = columns[2];
                                        TTSASelectvar.TSAType = columns[5];
                                        try
                                        {
                                            TTSASelectvar.DOB = Convert.ToDateTime(columns[6]);
                                        }
                                        catch (Exception Ex)
                                        {
                                            //Manually Handled
                                            strErrUploadNoFly = string.Format("{0} </BR> In TSA - Selectee Invalid DOB in the SID: {1}", strErrUploadSelect, TTSASelectvar.SequenceID);
                                        }


                                        TTSASelectvar.POB = columns[7].Truncate(50);
                                        TTSASelectvar.Citizen = columns[8].Truncate(18);
                                        TTSASelectvar.PPID = columns[9].Truncate(100);
                                        TTSASelectvar.MIC = columns[10].Truncate(300);
                                        TTSASelectvar.TSATime = DateTime.Now;
                                        TTSASelectvar.IsDeleted = false;
                                        countval = countval + 1;

                                        oTSAData.TSASelect.Add(TTSASelectvar);

                                        RecordCount = RecordCount + 1;
                                        if (RecordCount == RecordBatchCount)
                                        {
                                            CompanyCatalog.AddTSA(oTSAData);
                                            oTSAData.TSASelect.Clear();
                                            RecordCount = 0;
                                        }

                                    }
                                    catch (Exception Ex)
                                    {
                                        //Manually Handled
                                        strErrUploadNoFly = string.Format("{0] <Br>In TSA - Selectee Error Occurred In SID: {1}", strErrUploadSelect, columns[0]);
                                    }
                                }

                                CompanyCatalog.AddTSA(oTSAData);
                                oTSAData.TSASelect.Clear();
                                CompanyCatalog.UpdateSequenceNumber(UserPrincipal.Identity.CustomerID, TSASelectId + 1);
                                RecordCount = 0;

                            }
                        }
                    }

                    if (TSAType == EntitySet.Database.TSAClear)
                    {

                        if (TSAFileUrl != string.Empty)
                        {
                            using (System.IO.StringReader reader = new System.IO.StringReader(TSAFileUrl))//This not File URL. This File Content
                            {
                                string lines;
                                string[] columns = null;
                                string unitprice = string.Empty;
                                long countval = 0;
                                var TSASequenceNumber = new SequenceNumberManager().GetSequenceNumber(TSAUploadCurrentNo);
                                long TSASequenceNo = Convert.ToInt64(Convert.ToString(TSASequenceNumber).Remove(0, 5));// Remove customerid from TSANoFlyId
                                long TSACleartId = 0;
                                reader.ReadLine();//Read first line
                                while ((lines = reader.ReadLine()) != null)
                                {
                                    string[] validatedcolumns = CompanyCatalog.GetValidatedTSARecord(lines);
                                    columns = validatedcolumns;
                                    TSACleartId = TSASequenceNo + countval;

                                    try
                                    {
                                        FlightPak.Data.MasterCatalog.TSAClear TTSAclearvar = new FlightPak.Data.MasterCatalog.TSAClear();
                                        TTSAclearvar.TSACleartID = Convert.ToInt64(string.Format("{0}{1}", UserPrincipal.Identity.CustomerID, TSACleartId));
                                        TTSAclearvar.SequenceID = columns[0];
                                        TTSAclearvar.Cleared = columns[1];
                                        TTSAclearvar.FirstName = columns[3];
                                        TTSAclearvar.MiddleName = columns[4];
                                        TTSAclearvar.LastName = columns[2];
                                        TTSAclearvar.TSAType = columns[5];
                                        try
                                        {
                                            TTSAclearvar.DOB = Convert.ToDateTime(columns[6]);
                                        }
                                        catch (Exception Ex)
                                        {
                                            //Manually Handled
                                            strErrUploadNoFly = string.Format("{0} </BR> In TSA - Selectee Invalid DOB in the SID: {1}", strErrUploadClear, TTSAclearvar.SequenceID);
                                        }

                                        TTSAclearvar.POB = columns[7].Truncate(50);
                                        TTSAclearvar.Citizen = columns[8].Truncate(18);
                                        TTSAclearvar.PPID = columns[9].Truncate(100);
                                        TTSAclearvar.MIC = columns[10].Truncate(300);
                                        TTSAclearvar.TSATime = DateTime.Now;
                                        TTSAclearvar.IsDeleted = false;
                                        countval = countval + 1;

                                        oTSAData.TSAClear.Add(TTSAclearvar);

                                        RecordCount = RecordCount + 1;
                                        if (RecordCount == RecordBatchCount)
                                        {
                                            CompanyCatalog.AddTSA(oTSAData);
                                            oTSAData.TSAClear.Clear();
                                            RecordCount = 0;
                                        }

                                    }
                                    catch (Exception Ex)
                                    {
                                        //Manually Handled
                                        strErrUploadNoFly = string.Format("{0] <Br>In TSA - clear Error Occurred In SID: {1}", strErrUploadClear, columns[0]);
                                    }
                                }

                                CompanyCatalog.AddTSA(oTSAData);
                                oTSAData.TSAClear.Clear();
                                CompanyCatalog.UpdateSequenceNumber(UserPrincipal.Identity.CustomerID, TSACleartId);
                                RecordCount = 0;
                            }
                        }
                    }

                    //Common.Utility.DeleteFile(TSAFilelocation, null);
                    strErrUpload = strErrUploadNoFly + strErrUploadSelect + strErrUploadClear;
                    if (strErrUpload != "")
                    {
                        strErrUpload = " <b>Please Correct the Below Items in the TSA Upload </b>" + strErrUpload;//TO Do- Retrun the strErrUpload ASPX client
                    }


                }

                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public Business.Common.ReturnValue<Data.MasterCatalog.Customer> SetTSAstatus(Data.MasterCatalog.Customer TSAData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Customer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Customer> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TSAData.CustomerName = UserPrincipal.Identity.Name;
                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.SetTSAstatus(TSAData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }


        public Business.Common.ReturnValue<Data.MasterCatalog.TSANoFly> DeleteTSANoFly(Data.MasterCatalog.TSANoFly TSAData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSANoFly>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSANoFly> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.DeleteTSANOFly(TSAData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.TSASelect> DeleteTSASelecte(Data.MasterCatalog.TSASelect TSAData)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSASelect>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSASelect> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;

                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.DeleteTSASelect(TSAData);

                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TSAClear> DeleteTSANClear(Data.MasterCatalog.TSAClear TSAData)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSAClear>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSAClear> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSAData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TSAData.CustomerID = UserPrincipal.Identity.CustomerID;

                        TSAData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.DeleteTSAClear(TSAData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId> GetListInfoByHomeBaseId(Int64 HomeBaseID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.GetListInfoByHomeBaseId(HomeBaseID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.Company> UpdateAPISInterface(Int64 HomebaseID, string UserNameCBP, string UserPasswordCBP, string Domain, string APISUrl, bool IsAPISSupport, bool IsAPISAlert)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Company>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Company> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomebaseID, UserNameCBP, UserPasswordCBP, Domain, APISUrl, IsAPISSupport, IsAPISAlert))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager objcompanyCode = new FlightPak.Business.MasterCatalog.CompanyManager();
                        retValue = objcompanyCode.UpdateAPISInterface(HomebaseID, UserNameCBP, UserPasswordCBP, Domain, APISUrl, IsAPISSupport, IsAPISAlert);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> GetTripSheetReportHeaderList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.GetTripSheetReportHeaderList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQMessage> AddCqMessage(Data.MasterCatalog.CQMessage CQMessageData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQMessage>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQMessage> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMessageData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQMessageData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQMessageData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CompanyManager CompanyCatalog = new FlightPak.Business.MasterCatalog.CompanyManager();
                        ReturnValue = CompanyCatalog.Add(CQMessageData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQMessage> GetCQMessage(string MessageTYPE, Int64 HomebaseID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQMessage>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQMessage> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MessageTYPE, HomebaseID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager objcompanyCode = new FlightPak.Business.MasterCatalog.CompanyManager();
                        retValue = objcompanyCode.GetCQMessage(MessageTYPE, HomebaseID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public void DeleteCQMessage(string MessageTYPE, Int64 HomebaseID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {

                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MessageTYPE, HomebaseID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CompanyManager objcompanyCode = new FlightPak.Business.MasterCatalog.CompanyManager();
                        objcompanyCode.DeleteCQMessage(MessageTYPE, HomebaseID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Client Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> AddClientMaster(Data.MasterCatalog.Client client)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(client))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientManager objClient = new FlightPak.Business.MasterCatalog.ClientManager();
                        retValue = objClient.Add(client);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> UpdateClientMaster(Data.MasterCatalog.Client client)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(client))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        client.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ClientManager objClient = new FlightPak.Business.MasterCatalog.ClientManager();
                        retValue = objClient.Update(client);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> DeleteClientMaster(Data.MasterCatalog.Client client)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(client))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        client.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ClientManager objClient = new FlightPak.Business.MasterCatalog.ClientManager();
                        retValue = objClient.Delete(client);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientMasterLock(Data.MasterCatalog.Client client)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientMasterList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientManager objClient = new FlightPak.Business.MasterCatalog.ClientManager();
                        retValue = objClient.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Added by Scheduling calendar team to get Client Entity by Client Code.
        /// </summary>
        /// <param name="clientCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ClientByClientCDResult> GetClientByClientCD(string clientCode)
        {
            Business.Common.ReturnValue<Data.MasterCatalog.ClientByClientCDResult> client = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.ClientByClientCDResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientCodeManager objSchedule = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        client = objSchedule.GetClientByClientCD(clientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return client;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }
        #endregion
        #region "Airport Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters> GetAirportForGridWithFilters(long AirportID, string ICAOID, long RunwayOnly, bool IsDisplayInctive)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportForGridWithFilters(AirportID, ICAOID, RunwayOnly, IsDisplayInctive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter> GetAirportForGridPagingWithFilter(long AirportID, string ICAOID, long RunwayOnly, bool IsDisplayInctive, int StartRowIndex, int PageSize, string filterValue, string SortExpression, string SortOrder, string SelectedICAOID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportForGridPagingWithFilter(AirportID, ICAOID, RunwayOnly, IsDisplayInctive, StartRowIndex, PageSize, filterValue, SortExpression, SortOrder, SelectedICAOID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Airport> AddAirport(Data.MasterCatalog.Airport AirportData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Airport>>(() =>
                {
                    Business.Common.ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            AirportData.CustomerID = UserPrincipal.Identity.CustomerID;
                            AirportData.LastUpdUID = UserPrincipal.Identity.Name;
                            FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                            ReturnValue = AirportCatalog.Add(AirportData);
                        }, FlightPak.Common.Constants.Policy.BusinessLayer);
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.ServiceLayer);
            }
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Airport> UpdateAirport(Data.MasterCatalog.Airport AirportData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Airport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        AirportData.CustomerID = UserPrincipal.Identity.CustomerID;
                        AirportData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.Update(AirportData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="AirportData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Airport> DeleteAirport(Data.MasterCatalog.Airport AirportData)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Airport>>(() =>
                {
                    Business.Common.ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            AirportData.CustomerID = UserPrincipal.Identity.CustomerID;
                            AirportData.LastUpdUID = UserPrincipal.Identity.Name;
                            FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                            ReturnValue = AirportCatalog.Delete(AirportData);
                        }, FlightPak.Common.Constants.Policy.BusinessLayer);
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.ServiceLayer);
            }
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        public Business.Common.ReturnValue<Data.MasterCatalog.Airport> GetAirportLock(Data.MasterCatalog.Airport AirportData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Airport> GetAirportList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Airport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAllAirportList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid> GetAllAirportListForGrid(bool IsDisplayInctive)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetListInfoForGrid(IsDisplayInctive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount> GetAirportPositionAndCount(bool IsDisplayInctive, long AirportId, bool selectLastModified)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportPositionAndCount(IsDisplayInctive, AirportId, selectLastModified);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging> GetAllAirportListForGridCustomPaging(bool IsDisplayInctive, decimal MinimumRunWay, int Start, int End, string Sort, string IcaoID, string Iata, string CityName, string StateName, string CountryCD, string CountryName, string AirportName, string OffsetToGMT, string Filter)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetListInfoForGridCustomPaging(IsDisplayInctive,MinimumRunWay, Start, End, Sort, IcaoID, Iata, CityName, StateName, CountryCD, CountryName, AirportName, OffsetToGMT, Filter);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit> GetAllAirportListForGridLimit(int Limit, bool IsDisplayInctive)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetListInfoForGridLimit(Limit, IsDisplayInctive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount> GetAllAirportListForGridCount(bool IsDisplayInctive)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetListInfoForGridCount(IsDisplayInctive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO> GetAirportByAirportIDFBO(Int64 AirportID)
        {
            //Handle methods through exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportByAirportIDFBO(AirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAirportByAirportICaoID(string ICaoID)
        {
            //Handle methods through exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportByAirportICaoID(ICaoID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByCityName> GetAirportByCityName(string CityName)
        {
            //Handle methods through exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByCityName>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAirportByCityName> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportByCityName(CityName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAirportByAirportID(Int64 AirportID)
        {
            //Handle methods through exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportByAirportID(AirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        ///// <summary>
        ///// Implementation of interface to get records from table and returns list
        ///// </summary>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.Airport> GetAirportInfo(Data.MasterCatalog.Airport AirportData)
        //{
        //    Business.Common.ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
        //    try
        //    {
        //        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
        //        ReturnValue = AirportCatalog.GetList();
        //    }
        //    catch (System.Exception exc)
        //    {
        //        throw exc;
        //    }
        //    return ReturnValue;
        //}

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor> GetAirportForFuelVendor()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                        ReturnValue = AirportCatalog.GetAirportForFuelVendor();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        //public Business.Common.ReturnValue<Data.MasterCatalog.Airport> UpdateWorldClock(string AirportID, bool IsWorldClock)
        //{
        //    //Handle methods throguh exception manager with return flag
        //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //    return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Airport>>(() =>
        //    {
        //        Business.Common.ReturnValue<Data.MasterCatalog.Airport> retValue = null;
        //        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID, IsWorldClock))
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                FlightPak.Business.MasterCatalog.AirportManager objClientCode = new FlightPak.Business.MasterCatalog.AirportManager();
        //                retValue = objClientCode.UpdateWorldClock(AirportID, IsWorldClock);
        //            }, FlightPak.Common.Constants.Policy.BusinessLayer);
        //        }
        //        return retValue;
        //    }, FlightPak.Common.Constants.Policy.ServiceLayer);
        //}
        #endregion
        #region "Vendor Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> AddVendorMaster(Data.MasterCatalog.Vendor vendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        vendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        vendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.VendorManager objVendor = new FlightPak.Business.MasterCatalog.VendorManager();
                        retValue = objVendor.Add(vendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> UpdateVendorMaster(Data.MasterCatalog.Vendor vendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        vendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        vendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.VendorManager objVendor = new FlightPak.Business.MasterCatalog.VendorManager();
                        retValue = objVendor.Update(vendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> DeleteVendorMaster(Data.MasterCatalog.Vendor vendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        vendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        vendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.VendorManager objVendor = new FlightPak.Business.MasterCatalog.VendorManager();
                        retValue = objVendor.Delete(vendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetVendorMasterLock(Data.MasterCatalog.Vendor vendor)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetVendorMaster()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.VendorManager objVendor = new FlightPak.Business.MasterCatalog.VendorManager();
                        retValue = objVendor.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor> GetVendorInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.VendorManager objVendor = new FlightPak.Business.MasterCatalog.VendorManager();
                        retValue = objVendor.GetVendorListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetVendorMasterInfo(Data.MasterCatalog.Vendor vendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.VendorManager objVendor = new FlightPak.Business.MasterCatalog.VendorManager();
                        retValue = objVendor.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region"Vendor Contact Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> AddVendorContactMaster(Data.MasterCatalog.VendorContact vendorContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.VendorContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        vendorContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        vendorContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.VendorContactManager objVendorContact = new FlightPak.Business.MasterCatalog.VendorContactManager();
                        retValue = objVendorContact.Add(vendorContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> UpdateVendorContactMaster(Data.MasterCatalog.VendorContact vendorContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.VendorContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        vendorContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        vendorContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.VendorContactManager objVendorContact = new FlightPak.Business.MasterCatalog.VendorContactManager();
                        retValue = objVendorContact.Update(vendorContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> DeleteVendorContactMaster(Data.MasterCatalog.VendorContact vendorContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.VendorContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        vendorContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        vendorContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.VendorContactManager objVendorContact = new FlightPak.Business.MasterCatalog.VendorContactManager();
                        retValue = objVendorContact.Delete(vendorContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> GetVendorContactMasterLock(Data.MasterCatalog.VendorContact vendorContact)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> GetVendorContactMaster()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.VendorContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.VendorContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.VendorContactManager objVendorContact = new FlightPak.Business.MasterCatalog.VendorContactManager();
                        retValue = objVendorContact.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorContact> GetVendorContactMasterInfo(Data.MasterCatalog.GetAllVendorContact vendorContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorContact>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorContact> retValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorContact))
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.VendorContactManager objVendorContact = new FlightPak.Business.MasterCatalog.VendorContactManager();
                                    retValue = objVendorContact.GetListInfo(Convert.ToInt64(vendorContact.VendorID), vendorContact.VendorType);
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return retValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Catering Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters> GetAllCateringByAirportIDWithFilters(long AirportID, long CateringID, string CateringCD, bool FetchChoiceOnly, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.GetAllCateringByAirportIDWithFilters(AirportID, CateringID, CateringCD, FetchChoiceOnly, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CateringData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Catering> AddCatering(Data.MasterCatalog.Catering CateringData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Catering>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Catering> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CateringData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CateringData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CateringData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.Add(CateringData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CateringData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Catering> DeleteCatering(Data.MasterCatalog.Catering CateringData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Catering>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Catering> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CateringData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CateringData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CateringData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.Delete(CateringData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="HotelData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Catering> UpdateCatering(Data.MasterCatalog.Catering CateringData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Catering>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Catering> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CateringData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CateringData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CateringData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.Update(CateringData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID> GetAllCateringByAirportID(long airportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.GetAllCateringByAirportID(airportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD> GetCateringByAirportIDANDCateringCD(long airportID, string CateringCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.GetCateringByAirportIDANDCateringCD(airportID, CateringCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }



        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCatering> GetCateringListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCatering>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCatering> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Catering> GetCateringList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID> GetAllCateringByCateringID(long CateringID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CateringManager CateringCatalog = new FlightPak.Business.MasterCatalog.CateringManager();
                        ReturnValue = CateringCatalog.GetAllCateringByCateringID(CateringID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Transport Manager"

        #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary> 
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters> GetTransportByAirportIDWithFilters(long AirportID, long TransportID, string TransportCD, bool IsChoice, bool IsInActive)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.GetTransportByAirportIDWithFilters(AirportID, TransportID, TransportCD, IsChoice, IsInActive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="TransportData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Transport> AddTransport(Data.MasterCatalog.Transport TransportData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Transport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Transport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TransportData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TransportData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TransportData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.Add(TransportData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="TransportData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Transport> DeleteTransport(Data.MasterCatalog.Transport TransportData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Transport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Transport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TransportData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TransportData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TransportData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.Delete(TransportData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="TransportData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Transport> UpdateTransport(Data.MasterCatalog.Transport TransportData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Transport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Transport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TransportData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        TransportData.CustomerID = UserPrincipal.Identity.CustomerID;
                        TransportData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.Update(TransportData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllTransport> GetTransportListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllTransport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllTransport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportID> GetTransportByAirportID(long AirportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByAirportID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.GetTransportByAirportID(AirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportID> GetTransportByTransportID(long TransportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.GetTransportByTransportID(TransportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportCode> GetTransportByTransportCode(long AirportID, string Transportcd)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportCode>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTransportByTransportCode> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TransportManager TransportMgr = new FlightPak.Business.MasterCatalog.TransportManager();
                        ReturnValue = TransportMgr.GetTransportByTransportCode(AirportID, Transportcd);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Transport> GetTransportList()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region "Fleet Group Order Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.GetLsavailableFleet> GetLsavailableList(Int64 FleetGroupID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetLsavailableFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetLsavailableFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupOrderManager FleetGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.FleetGroupOrderManager();
                        retValue = FleetGroupOrderDetailManager.GetLsavailableList(FleetGroupID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetLsselectedFleet> GetLsselectedList(Int64 FleetGroupID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetLsselectedFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetLsselectedFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupOrderManager FleetGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.FleetGroupOrderManager();
                        retValue = FleetGroupOrderDetailManager.GetLsselectedList(FleetGroupID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder> AddFleetGroupOrderType(Data.MasterCatalog.FleetGroupOrder fleetGroupOrder)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetGroupOrder))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetGroupOrder.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetGroupOrder.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetGroupOrderManager FleetGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.FleetGroupOrderManager();
                        retValue = FleetGroupOrderDetailManager.Add(fleetGroupOrder);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder> DeleteFleetGroupOrderType(Data.MasterCatalog.FleetGroupOrder fleetGroupOrder)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetGroupOrder> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetGroupOrder))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetGroupOrder.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetGroupOrder.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetGroupOrderManager FleetGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.FleetGroupOrderManager();
                        retValue = FleetGroupOrderDetailManager.Delete(fleetGroupOrder);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetGroupID> GetFleetGroupID(string FleetGroupCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetGroupID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetGroupID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetGroupManager FleetGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.FleetGroupManager();
                        retValue = FleetGroupOrderDetailManager.GetFleetGroupID(FleetGroupCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion "Fleet Group Order Manager"
        #region "TravelCoordinator Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> AddTravelCoordinatorMaster(Data.MasterCatalog.TravelCoordinator travelCoordinator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinator.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinator.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinator.Add(travelCoordinator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> UpdateTravelCoordinatorMaster(Data.MasterCatalog.TravelCoordinator travelCoordinator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinator.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinator.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinator.Update(travelCoordinator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> DeleteTravelCoordinatorMaster(Data.MasterCatalog.TravelCoordinator travelCoordinator)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
                {
                    Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> ReturnValue = null;
                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
                    {
                        //Handle methods throguh exception manager with return flag
                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                        exManager.Process(() =>
                        {
                            travelCoordinator.CustomerID = UserPrincipal.Identity.CustomerID;
                            travelCoordinator.LastUpdUID = UserPrincipal.Identity.Name;
                            FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                            ReturnValue = objTravelCoordinator.Delete(travelCoordinator);
                        }, FlightPak.Common.Constants.Policy.BusinessLayer);
                    }
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.ServiceLayer);
            }
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorMasterLock(Data.MasterCatalog.TravelCoordinator travelCoordinator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinator.GetLock(travelCoordinator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorMaster()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinator.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorMasterInfo(Data.MasterCatalog.TravelCoordinator travelCoordinator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinator.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleetSelectedList(Int64 travelCoordCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordCD))
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
                                    retValue = objTravelCoordinatorFleet.GetTravelCoordinatorFleetSelectedList(travelCoordCD);
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return retValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestorSelectedList(Int64 travelCoordCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordCD))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
                        retValue = objTravelCoordinatorRequestor.GetTravelCoordinatorRequestorSelectedList(travelCoordCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTravelCoordinator> GetTravelCoordinatorList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinator = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinator.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "TravelCoordinator Manager"
        #region "Exchange Rate"
        public Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> AddExchangeRate(Data.MasterCatalog.ExchangeRate ExchangeRateData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRateData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ExchangeRateData.CustomerID = UserPrincipal.Identity.CustomerID;
                        ExchangeRateData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ExchangeRateManager ExchangeRateCatalog = new FlightPak.Business.MasterCatalog.ExchangeRateManager();
                        ReturnValue = ExchangeRateCatalog.Add(ExchangeRateData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> UpdateExchangeRate(Data.MasterCatalog.ExchangeRate ExchangeRateData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRateData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ExchangeRateData.CustomerID = UserPrincipal.Identity.CustomerID;
                        ExchangeRateData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ExchangeRateManager ExchangeRateCatalog = new FlightPak.Business.MasterCatalog.ExchangeRateManager();
                        ReturnValue = ExchangeRateCatalog.Update(ExchangeRateData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> DeleteExchangeRate(Data.MasterCatalog.ExchangeRate ExchangeRateData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRateData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        ExchangeRateData.CustomerID = UserPrincipal.Identity.CustomerID;
                        ExchangeRateData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ExchangeRateManager ExchangeRateCatalog = new FlightPak.Business.MasterCatalog.ExchangeRateManager();
                        ReturnValue = ExchangeRateCatalog.Delete(ExchangeRateData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> GetExchangeRateLock(Data.MasterCatalog.ExchangeRate ExchangeRateData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRateData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ExchangeRateManager ExchangeRateCatalog = new FlightPak.Business.MasterCatalog.ExchangeRateManager();
                        ReturnValue = ExchangeRateCatalog.GetLock(ExchangeRateData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> GetExchangeRate()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ExchangeRateManager ExchangeRateCatalog = new FlightPak.Business.MasterCatalog.ExchangeRateManager();
                        ReturnValue = ExchangeRateCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> GetExchangeRateInfo(Data.MasterCatalog.ExchangeRate ExchangeRate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ExchangeRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ExchangeRateManager ExchangeRateCatalog = new FlightPak.Business.MasterCatalog.ExchangeRateManager();
                        ReturnValue = ExchangeRateCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Exchange Rate"
        #region "Passenger Group Order Detail"
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPaxAvailableList> GetPaxAvailableList(Int64 customerId, Int64 paxCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPaxAvailableList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPaxAvailableList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerId, paxCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerGroupOrderManager PaxGroup = new FlightPak.Business.MasterCatalog.PassengerGroupOrderManager();
                        retValue = PaxGroup.GetAvailableList(paxCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPaxSelectedList> GetPaxSelectedList(Int64 customerId, Int64 paxCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPaxSelectedList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPaxSelectedList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerId, paxCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerGroupOrderManager PaxGroup = new FlightPak.Business.MasterCatalog.PassengerGroupOrderManager();
                        retValue = PaxGroup.GetSelectedList(paxCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder> AddPaxGroupOrder(Data.MasterCatalog.PassengerGroupOrder paxGroupOrder)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxGroupOrder))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerGroupOrderManager PaxGroup = new FlightPak.Business.MasterCatalog.PassengerGroupOrderManager();

                        paxGroupOrder.CustomerID = UserPrincipal.Identity.CustomerID;
                        paxGroupOrder.LastUpdUID = UserPrincipal.Identity.Name;

                        retValue = PaxGroup.Add(paxGroupOrder);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder> DeletePaxGroupOrder(Data.MasterCatalog.PassengerGroupOrder paxGroupOrder)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerGroupOrder> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxGroupOrder))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        paxGroupOrder.CustomerID = UserPrincipal.Identity.CustomerID;
                        paxGroupOrder.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerGroupOrderManager PaxGroup = new FlightPak.Business.MasterCatalog.PassengerGroupOrderManager();
                        retValue = PaxGroup.Delete(paxGroupOrder);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID> GetPassengerGroupbyGroupID(Int64 PassengerGroupID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroupID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerGroupOrderManager PaxGroup = new FlightPak.Business.MasterCatalog.PassengerGroupOrderManager();
                        retValue = PaxGroup.GetPassengerGroupbyGroupID(PassengerGroupID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Duty Rules Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> GetCrewDutyRulesWithFilters(long CrewDutyRuleId, string CrewDutyRuleCD, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDutyRuleCatalog = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                        ReturnValue = CrewDutyRuleCatalog.GetCrewDutyRulesWithFilters(CrewDutyRuleId, CrewDutyRuleCD, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> AddCrewDutyRule(Data.MasterCatalog.CrewDutyRules CrewDutyRuleData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRuleData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDutyRuleData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewDutyRuleData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDutyRuleCatalog = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                        ReturnValue = CrewDutyRuleCatalog.Add(CrewDutyRuleData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> DeleteCrewDutyRule(Data.MasterCatalog.CrewDutyRules CrewDutyRuleData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRuleData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDutyRuleData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewDutyRuleData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDutyRuleCatalog = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                        ReturnValue = CrewDutyRuleCatalog.Delete(CrewDutyRuleData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> UpdateCrewDutyRule(Data.MasterCatalog.CrewDutyRules CrewDutyRuleData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRuleData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDutyRuleData.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewDutyRuleData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDutyRuleCatalog = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                        ReturnValue = CrewDutyRuleCatalog.Update(CrewDutyRuleData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> GetCrewDutyRuleList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDutyRuleCatalog = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                        ReturnValue = CrewDutyRuleCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Passenger Additional Info"
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> AddPassengerAdditionalInfo(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        passengerAdditionalInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        passengerAdditionalInfo.ClientID = UserPrincipal.Identity.ClientId;
                        passengerAdditionalInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager objPassengerAdditionalInfo = new FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager();
                        retValue = objPassengerAdditionalInfo.Add(passengerAdditionalInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> UpdatePassengerAdditionalInfo(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        passengerAdditionalInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        passengerAdditionalInfo.ClientID = UserPrincipal.Identity.ClientId;
                        passengerAdditionalInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager objPassengerAdditionalInfo = new FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager();
                        retValue = objPassengerAdditionalInfo.Update(passengerAdditionalInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> DeletePassengerAdditionalInfo(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        passengerAdditionalInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        passengerAdditionalInfo.ClientID = UserPrincipal.Identity.ClientId;
                        passengerAdditionalInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager objPassengerAdditionalInfo = new FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager();
                        retValue = objPassengerAdditionalInfo.Delete(passengerAdditionalInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> GetPassengerAdditionalInfoLock(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager objPassengerAdditionalInfo = new FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager();
                        retValue = objPassengerAdditionalInfo.GetLock(passengerAdditionalInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> GetPassengerAdditionalInfoList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager objPassengerAdditionalInfo = new FlightPak.Business.MasterCatalog.PassengerAdditionalInfoManager();
                        retValue = objPassengerAdditionalInfo.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Department Manager"

        #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters> GetDepartmentByWithFilters(string DepartmentCD, long DepartmentID, long ClientID, bool FetchInactiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentManager objDepartment = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = objDepartment.GetDepartmentByWithFilters(DepartmentCD, DepartmentID, ClientID, FetchInactiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion



        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> AddDepartment(Data.MasterCatalog.Department DepartmentData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Department>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Department> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentManager DepartmentCatalog = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = DepartmentCatalog.Add(DepartmentData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> UpdateDepartment(Data.MasterCatalog.Department DepartmentData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Department>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Department> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentManager DepartmentCatalog = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = DepartmentCatalog.Update(DepartmentData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> DeleteDepartment(Data.MasterCatalog.Department DepartmentData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Department>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Department> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentManager DepartmentCatalog = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = DepartmentCatalog.Delete(DepartmentData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDepartmentLock(Data.MasterCatalog.Department DepartmentData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDepartmentList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Department>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Department> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentManager objDepartment = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = objDepartment.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartments> GetAllDepartmentList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartments>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartments> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentManager DepartmentCatalog = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = DepartmentCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.ExceptionPolicy);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        ///  Added by Scheduling calendar team to get Department Entity by Department Code.
        /// </summary>
        /// <param name="departmentCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult> GetDepartmentByDepartmentCD(string departmentCode)
        {
            Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult> department = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentManager objSchedule = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        department = objSchedule.GetDepartmentByDepartmentCD(departmentCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return department;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        /// <summary>
        /// Method to fetch Department record by DepartmentCD
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD> GetAllDeptByDeptCD(string DepartmentCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentManager objDepartment = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = objDepartment.GetAllDeptByDeptCD(DepartmentCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        
        #region "GetAllDeptAuth Manager"
        ///// <summary>
        ///// Implementation of interface to insert records in table and returns generics
        ///// </summary>
        ///// <param name="GetAllDeptAuthData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> AddGetAllDeptAuth(Data.MasterCatalog.GetAllDeptAuth GetAllDeptAuthData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Implementation of interface to update records in table and returns generics
        ///// </summary>
        ///// <param name="DepartmentData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> UpdateGetAllDeptAuth(Data.MasterCatalog.GetAllDeptAuth GetAllDeptAuthData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Implementation of interface to update records as deleted in table and returns generics
        ///// </summary>
        ///// <param name="DepartmentData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> DeleteGetAllDeptAuth(Data.MasterCatalog.GetAllDeptAuth GetAllDeptAuthData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Hook for implementing locking functionality
        ///// </summary>
        ///// <param name="DepartmentData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetAllDeptAuthock(Data.MasterCatalog.GetAllDeptAuth GetAllDeptAuthData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Implementation of interface to get records from table and returns list
        ///// </summary>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetAllDeptAuthorizationList()
        //{
        //    Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> ReturnValue = null;
        //    try
        //    {
        //        FlightPak.Business.MasterCatalog.GetAllDeptAuthManager DeptAuthCatalog = new FlightPak.Business.MasterCatalog.GetAllDeptAuthManager();
        //        ReturnValue = DeptAuthCatalog.GetList();
        //    }
        //    catch (System.Exception exc)
        //    {
        //        throw exc;
        //    }
        //    return ReturnValue;
        //    throw new NotImplementedException();
        //}
        #endregion
        #region "GetAllAirport Manager"
        ///// <summary>
        ///// Implementation of interface to insert records in table and returns generics
        ///// </summary>
        ///// <param name="GetAllDeptAuthData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> AddGetAllAirport(Data.MasterCatalog.GetAllAirport GetAllAirport)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Implementation of interface to update records in table and returns generics
        ///// </summary>
        ///// <param name="DepartmentData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> UpdateGetAllAirport(Data.MasterCatalog.GetAllAirport GetAllAirportData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Implementation of interface to update records as deleted in table and returns generics
        ///// </summary>
        ///// <param name="DepartmentData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> DeleteGetAllAirport(Data.MasterCatalog.GetAllAirport GetAllDeptAuthData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Hook for implementing locking functionality
        ///// </summary>
        ///// <param name="DepartmentData"></param>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAllAirporthock(Data.MasterCatalog.GetAllAirport GetAllDeptAuthData)
        //{
        //    throw new NotImplementedException();
        //}
        ///// <summary>
        ///// Implementation of interface to get records from table and returns list
        ///// </summary>
        ///// <returns></returns>
        //public Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> GetAllAirportList()
        //{
        //    Business.Common.ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
        //    try
        //    {
        //        FlightPak.Business.MasterCatalog.AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
        //        ReturnValue = AirportCatalog.GetListInfo();
        //    }
        //    catch (System.Exception exc)
        //    {
        //        throw exc;
        //    }
        //    return ReturnValue;
        //}
        #endregion
        #region "Department Authorization Manager"

        #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationWithFilters(long ClientId, long DepartmentId, long LongAuthorizationId, string AuthorizationCD, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager DepartmentAuthorizationCatalog = new FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager();
                        ReturnValue = DepartmentAuthorizationCatalog.GetDepartmentAuthorizationWithFilters(ClientId, DepartmentId, LongAuthorizationId, AuthorizationCD, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> AddDepartmentAuthorization(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorizationData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentAuthorizationData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentAuthorizationData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager DepartmentAuthorizationCatalog = new FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager();
                        ReturnValue = DepartmentAuthorizationCatalog.Add(DepartmentAuthorizationData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> UpdateDepartmentAuthorization(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorizationData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentAuthorizationData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentAuthorizationData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager DepartmentAuthorizationCatalog = new FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager();
                        ReturnValue = DepartmentAuthorizationCatalog.Update(DepartmentAuthorizationData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> DeleteDepartmentAuthorization(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorizationData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentAuthorizationData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentAuthorizationData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager DepartmentAuthorizationCatalog = new FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager();
                        ReturnValue = DepartmentAuthorizationCatalog.Delete(DepartmentAuthorizationData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        ///  Implementation of interface to lock records in table and returns generics
        /// </summary>
        /// <param name="DepartmentAuthorizationData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationLock(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorizationData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager DepartmentAuthorizationCatalog = new FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager();
                        ReturnValue = DepartmentAuthorizationCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetAllDeptAuthorizationList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllDeptAuth> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager DeptAuthCatalog = new FlightPak.Business.MasterCatalog.DepartmentAuthorizationManager();
                        ReturnValue = DeptAuthCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
            //throw new NotImplementedException();
        }
        #endregion
        #region "PaymentType Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetAllPaymentTypeWithFilters(long PaymentTypeID, string PaymenttypeCD, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.GetAllPaymentTypeWithFilters( PaymentTypeID, PaymenttypeCD, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> AddPaymentType(Data.MasterCatalog.PaymentType paymentType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paymentType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        paymentType.CustomerID = UserPrincipal.Identity.CustomerID;
                        paymentType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.Add(paymentType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> UpdatePaymentType(Data.MasterCatalog.PaymentType paymentType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paymentType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        paymentType.CustomerID = UserPrincipal.Identity.CustomerID;
                        paymentType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.Update(paymentType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> DeletePaymentType(Data.MasterCatalog.PaymentType paymentType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paymentType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        paymentType.CustomerID = UserPrincipal.Identity.CustomerID;
                        paymentType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.Delete(paymentType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetPaymentTypeLock(Data.MasterCatalog.PaymentType paymentType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paymentType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.GetLock(paymentType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetPaymentType()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> GetPaymentTypeInfo(Data.MasterCatalog.PaymentType paymentType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PaymentType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PaymentType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paymentType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PaymentTypeManager objPaymentType = new FlightPak.Business.MasterCatalog.PaymentTypeManager();
                        retValue = objPaymentType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Manager"

        #region For Performance Optimization
          /// <summary>
        /// Implementation of interface to get records from table and returns list based on parameters
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>        
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters> GetAllCrewWithFilters(long ClientID, bool FetchActiveOnly, bool IsRotaryWing, bool IsFixedWing, string ICAOID, long CrewGroupId, long CrewId, string CrewCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewManager = new FlightPak.Business.MasterCatalog.CrewManager();
                        retValue = CrewManager.GetAllCrewWithFilters(ClientID, FetchActiveOnly, IsRotaryWing, IsFixedWing, ICAOID, CrewGroupId, CrewId, CrewCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }



        /// <summary>
        /// Implementation of interface to get records from table and returns list based on parameters
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>        
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewforDisplay> GetCrewforDisplay(long CrewId)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewforDisplay>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewforDisplay> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewManager = new FlightPak.Business.MasterCatalog.CrewManager();
                        retValue = CrewManager.GetCrewforDisplay(CrewId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }


        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> UpdateCrewDisplay(long CrewID, bool IsDisplay)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewManager = new FlightPak.Business.MasterCatalog.CrewManager();
                        retValue = CrewManager.UpdateCrewDisplay(CrewID, IsDisplay);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }




        /// <summary>
        /// Implementation of interface to get records from table and returns list based on parameters
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>        
        public Business.Common.ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog> GetOtherCrewDutyLog(long CrewId, long AircraftID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewManager = new FlightPak.Business.MasterCatalog.CrewManager();
                        retValue = CrewManager.GetOtherCrewDutylog(CrewId, AircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        #endregion
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> AddCrew(Data.MasterCatalog.Crew Crew)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Crew))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Crew.CustomerID = UserPrincipal.Identity.CustomerID;
                        Crew.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.Add(Crew);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> UpdateCrew(Data.MasterCatalog.Crew Crew)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Crew))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Crew.CustomerID = UserPrincipal.Identity.CustomerID;
                        Crew.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.Update(Crew);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> DeleteCrew(Data.MasterCatalog.Crew Crew)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Crew))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Crew.CustomerID = UserPrincipal.Identity.CustomerID;
                        Crew.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.Delete(Crew);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to lock records in table and returns generics
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetCrewLock(Data.MasterCatalog.Crew Crew)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Crew))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetLock(Crew);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrew> GetCrewList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrew>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrew> ReturnValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                                    ReturnValue = CrewMgr.GetCrewList();
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return ReturnValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewForGrid> GetAllCrewForGrid()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewForGrid>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewForGrid> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetAllCrewForGrid();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewByCrewID> GetCrewbyCrewId(Int64 CrewID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewByCrewID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewByCrewID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetCrewByCrewID(CrewID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list based on parameters
        /// </summary>
        /// <param name="Crew"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetCrewInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours> GetCurrentFltHoursInfo(Int64 CrewID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetList(CrewID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency> GetAllCrewCurrency(Int64 CrewID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetCurrency(CrewID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewID> GetCrewID(string CrewCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewCD))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager objCrew = new FlightPak.Business.MasterCatalog.CrewManager();
                        retValue = objCrew.GetCrewID(CrewCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew> GetCrewGroupBasedCrewInfo(Int64 CrewGroupID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewMgr = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewMgr.GetCrewGroupBasedCrewInfo(CrewGroupID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetPassportbyPassportId> GetPassportbyPassportId(Int64 PassportID, Boolean IsCrewPassport)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPassportbyPassportId>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPassportbyPassportId> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewPassportManager CrewPassportManager = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                        retValue = CrewPassportManager.GetPassportbyPassportId(PassportID, IsCrewPassport);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetPassengerPassportByPassengerRequestorID(Int64 PassengerRequestorID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewPassportManager CrewPassportManager = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                        retValue = CrewPassportManager.GetPassengerPassportByPassengerRequestorID(PassengerRequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        #region "Crew Roster Additional Info"
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> AddCrewRosterAddInfo(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.Add(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> AddforAllCrewMembers(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.AddforAllCrew(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> UpdateforAllCrewMembers(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.UpdateforAllCrew(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> UpdateCrewRosterAddInfo(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.Update(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> DeleteCrewRosterAddInfo(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.Delete(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> GetCrewRosterAddInfoLock(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.GetLock(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> GetCrewRosterAddInfoList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewRosterAdditionalInfoManager();
                        retValue = objCrewRosterAddInfo.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "FuelLocator Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> AddFuelLocator(Data.MasterCatalog.FuelLocator fuelLocator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fuelLocator.CustomerID = UserPrincipal.Identity.CustomerID;
                        fuelLocator.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelLocatorManager objFuelLocator = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        retValue = objFuelLocator.Add(fuelLocator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> UpdateFuelLocator(Data.MasterCatalog.FuelLocator fuelLocator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fuelLocator.CustomerID = UserPrincipal.Identity.CustomerID;
                        fuelLocator.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelLocatorManager objFuelLocator = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        retValue = objFuelLocator.Update(fuelLocator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> DeleteFuelLocator(Data.MasterCatalog.FuelLocator fuelLocator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fuelLocator.CustomerID = UserPrincipal.Identity.CustomerID;
                        fuelLocator.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelLocatorManager objFuelLocator = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        retValue = objFuelLocator.Delete(fuelLocator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> GetFuelLocatorLock(Data.MasterCatalog.FuelLocator fuelLocator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelLocatorManager objFuelLocator = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        retValue = objFuelLocator.GetLock(fuelLocator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> GetFuelLocator()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelLocator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelLocatorManager objFuelLocator = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        retValue = objFuelLocator.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFuelLocator> GetFuelLocatorInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFuelLocator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFuelLocator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelLocatorManager objFuelLocator = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        retValue = objFuelLocator.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "FleetProfileManager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters> GetAllFleetWithFilters(long ClientID, bool FetchActiveOnly, string IsFixedRotary, string ICAOID, long VendorID, string TailNum, long FleetID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetAllFleetWithFilters(ClientID, FetchActiveOnly, IsFixedRotary, ICAOID, VendorID, TailNum, FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.Fleet> GetFleetProfileList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Fleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Fleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForPopup> GetAllFleetForPopupList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForPopup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForPopup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetAllFleetForPopupList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Fleet> AddFleetProfileType(Data.MasterCatalog.Fleet fleetProfile)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Fleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Fleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfile))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        fleetProfile.CustomerID = UserPrincipal.Identity.CustomerID;
                        //fleetProfile.ClientID = UserPrincipal.Identity.ClientId;
                        fleetProfile.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleet.Add(fleetProfile);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Fleet> UpdateFleetProfileType(Data.MasterCatalog.Fleet fleetProfile)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Fleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Fleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfile))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        //fleetProfile.ClientID = UserPrincipal.Identity.ClientId;
                        fleetProfile.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetProfile.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleet.Update(fleetProfile);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Fleet> DeleteFleetProfileType(Data.MasterCatalog.Fleet fleetProfile)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Fleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Fleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfile))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //fleetProfile.ClientID = UserPrincipal.Identity.ClientId;
                        fleetProfile.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetProfile.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.Delete(fleetProfile);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleet> GetFleet()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetFleet();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Fleet> GetFleetProfileTypeLock(Data.MasterCatalog.Fleet fleetProfile)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Fleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Fleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfile))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetLock(fleetProfile);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetID> GetFleetID(string TailNum, string SerialNum)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TailNum, SerialNum))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetFleetID(TailNum, SerialNum);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetIDInfo> GetFleetIDInfo(Int64 FleetID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetIDInfo>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetIDInfo> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetFleetIDInfo(FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetSearch> GetFleetSearch(bool IsInActive, string IsFixedRotary, Int64 HomeBaseID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetSearch>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetSearch> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsInActive, IsFixedRotary, HomeBaseID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager objFleet = new FlightPak.Business.MasterCatalog.FleetManager();
                        retValue = objFleet.GetFleetSearch(IsInActive, IsFixedRotary, HomeBaseID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "DepartmentGroup"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="DepartmentGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> AddDeptGroup(Data.MasterCatalog.DepartmentGroup DepartmentGroupData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroupData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentGroupData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentGroupData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentGroupManager DepartmentGroupCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupManager();
                        ReturnValue = DepartmentGroupCatalog.Add(DepartmentGroupData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="DepartmentGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> UpdateDeptGroup(Data.MasterCatalog.DepartmentGroup DepartmentGroupData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroupData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentGroupData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentGroupData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentGroupManager DepartmentGroupCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupManager();
                        ReturnValue = DepartmentGroupCatalog.Update(DepartmentGroupData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="DepartmentGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> DeleteDeptGroup(Data.MasterCatalog.DepartmentGroup DepartmentGroupData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroupData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentGroupData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentGroupData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentGroupManager DepartmentGroupCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupManager();
                        ReturnValue = DepartmentGroupCatalog.Delete(DepartmentGroupData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to lock records in table and returns generics
        /// </summary>
        /// <param name="DepartmentGroupData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> GetDeptGroupLock(Data.MasterCatalog.DepartmentGroup DepartmentGroupData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroup> GetDeptGroupList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup> GetDeptGroupInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentGroupManager DepartmentGroupCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupManager();
                        ReturnValue = DepartmentGroupCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Department Group Order Detail"
        /// <summary>
        /// Implementation of interface to get available records from table based on CustomerId and DepartmentCode and returns list
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="DepartmentCode"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDeptAvailableList(Int64 CustomerId, Int64 DepartmentCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Department>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Department> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, DepartmentCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CustomerId = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager DepartmentGroupOrderCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager();
                        ReturnValue = DepartmentGroupOrderCatalog.GetAvailableList(DepartmentCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get selected records from table based on CustomerId and DepartmentCode and returns list
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="DepartmentCode"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Department> GetDeptSelectedList(Int64 CustomerId, Int64 DepartmentCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Department>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Department> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, DepartmentCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CustomerId = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager DepartmentGroupOrderCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager();
                        ReturnValue = DepartmentGroupOrderCatalog.GetSelectedList(DepartmentCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="DepartmentGroupOrderData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> AddDeptGroupOrder(Data.MasterCatalog.DepartmentGroupOrder DepartmentGroupOrderData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroupOrderData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager DepartmentGroupOrderCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager();
                        DepartmentGroupOrderData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentGroupOrderData.LastUpdUID = UserPrincipal.Identity.Name;
                        ReturnValue = DepartmentGroupOrderCatalog.Add(DepartmentGroupOrderData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="DepartmentGroupOrderData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> DeleteDeptGroupOrder(Data.MasterCatalog.DepartmentGroupOrder DepartmentGroupOrderData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroupOrderData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DepartmentGroupOrderData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DepartmentGroupOrderData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager DepartmentGroupOrderCatalog = new FlightPak.Business.MasterCatalog.DepartmentGroupOrderManager();
                        ReturnValue = DepartmentGroupOrderCatalog.Delete(DepartmentGroupOrderData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "FBO Manager"
        #region For Performance Optimization
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters> GetFBOByAirportWithFilters(long AirportID, long FBOID, string FBOCD, bool FetchChoiceOnly, bool FetchUVAIROnly, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOCatalog = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOCatalog.GetFBOByAirportWithFilters(AirportID, FBOID, FBOCD, FetchChoiceOnly, FetchUVAIROnly, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="FBOData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FBO> AddFBO(Data.MasterCatalog.FBO FBOData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FBOData.CustomerID = UserPrincipal.Identity.CustomerID;
                        FBOData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.Add(FBOData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="FBOData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FBO> UpdateFBO(Data.MasterCatalog.FBO FBOData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FBOData.CustomerID = UserPrincipal.Identity.CustomerID;
                        FBOData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.Update(FBOData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="FBOData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FBO> DeleteFBO(Data.MasterCatalog.FBO FBOData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FBOData.CustomerID = UserPrincipal.Identity.CustomerID;
                        FBOData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.Delete(FBOData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to lock records in table and returns generics
        /// </summary>
        /// <param name="FBOData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FBO> GetFBOLock(Data.MasterCatalog.FBO FBOData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.GetLock(FBOData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID> GetAllFBOByAirportID(long airportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOCatalog = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOCatalog.GetAllFBOByAirportID(airportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD> GetFBOByAirportIDAndFBOCD(long airportID, string fboCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOCatalog = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOCatalog.GetFBOByAirportIDAndFBOCD(airportID, fboCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }




        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FBO> GetFBOList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBO> GetFBOInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBO>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBO> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID> GetAllFBOByFBOID(long FBOID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FBOManager FBOMgr = new FlightPak.Business.MasterCatalog.FBOManager();
                        ReturnValue = FBOMgr.GetAllFBOByFBOID(FBOID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "CrewGroupManager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters> GetCrewGroupWithFilters(long CrewGroupID, string CrewGroupCD, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupManager = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        retValue = CrewGroupManager.GetCrewGroupWithFilters(CrewGroupID, CrewGroupCD, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> GetCrewGroupList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupManager = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        retValue = CrewGroupManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewGroup> GetCrewGroupInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupCatalog = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        ReturnValue = CrewGroupCatalog.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> AddCrewType(Data.MasterCatalog.CrewGroup crewgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupManager = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        crewgroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewgroup.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = CrewGroupManager.Add(crewgroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> UpdateCrewType(Data.MasterCatalog.CrewGroup crewgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupManager = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        crewgroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewgroup.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = CrewGroupManager.Update(crewgroup);
                        return retValue;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> DeleteCrewType(Data.MasterCatalog.CrewGroup crewgroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewgroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewgroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewgroup.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupManager = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        retValue = CrewGroupManager.Delete(crewgroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> GetCrewTypeLock(Data.MasterCatalog.CrewGroup CrewGroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewGroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewGroupManager CrewGroupManager = new FlightPak.Business.MasterCatalog.CrewGroupManager();
                        retValue = CrewGroupManager.GetLock(CrewGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Group Order Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetcrewavailableList(Int64 customerID, Int64 groupCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerID, groupCD))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        customerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.CrewGroupOrderManager CrewGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.CrewGroupOrderManager();
                        retValue = CrewGroupOrderDetailManager.GetCrewavailableList(groupCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Crew> GetcrewselectedList(Int64 CustomerID, Int64 groupCD)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Crew>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Crew> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, groupCD))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.CrewGroupOrderManager CrewGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.CrewGroupOrderManager();
                        retValue = CrewGroupOrderDetailManager.GetCrewselectedList(groupCD);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder> AddCrewGroupOrderType(Data.MasterCatalog.CrewGroupOrder CrewGroupOrder)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder> retValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewGroupOrder))
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.CrewGroupOrderManager CrewGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.CrewGroupOrderManager();
                                    CrewGroupOrder.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CrewGroupOrder.LastUpdUID = UserPrincipal.Identity.Name;
                                    retValue = CrewGroupOrderDetailManager.Add(CrewGroupOrder);
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return retValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder> DeleteCrewGroupOrderType(Data.MasterCatalog.CrewGroupOrder CrewGroupOrder)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewGroupOrder> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewGroupOrder))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewGroupOrder.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewGroupOrder.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewGroupOrderManager CrewGroupOrderDetailManager = new FlightPak.Business.MasterCatalog.CrewGroupOrderManager();
                        retValue = CrewGroupOrderDetailManager.Delete(CrewGroupOrder);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "BudgetGet Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.Budget> AddBudget(Data.MasterCatalog.Budget budgetType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Budget>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Budget> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        budgetType.CustomerID = UserPrincipal.Identity.CustomerID;
                        budgetType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.BudgetManager objBudget = new FlightPak.Business.MasterCatalog.BudgetManager();
                        retValue = objBudget.Add(budgetType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Budget> UpdateBudget(Data.MasterCatalog.Budget budgetType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Budget>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Budget> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        budgetType.CustomerID = UserPrincipal.Identity.CustomerID;
                        budgetType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.BudgetManager objBudget = new FlightPak.Business.MasterCatalog.BudgetManager();
                        retValue = objBudget.Update(budgetType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Budget> DeleteBudget(Data.MasterCatalog.Budget budgetType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Budget>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Budget> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        budgetType.CustomerID = UserPrincipal.Identity.CustomerID;
                        budgetType.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.BudgetManager objBudget = new FlightPak.Business.MasterCatalog.BudgetManager();
                        retValue = objBudget.Delete(budgetType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Budget> GetBudgetLock(Data.MasterCatalog.Budget budgetType)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Budget>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Budget> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.BudgetManager objBudget = new FlightPak.Business.MasterCatalog.BudgetManager();
                        retValue = objBudget.GetLock(budgetType);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Budget> GetBudgetList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Budget>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Budget> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.BudgetManager objBudget = new FlightPak.Business.MasterCatalog.BudgetManager();
                        retValue = objBudget.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllBudget> GetBudgetListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllBudget>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllBudget> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.BudgetManager objBudget = new FlightPak.Business.MasterCatalog.BudgetManager();
                        retValue = objBudget.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Check List"
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> AddCrewChecklist(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewChecklist = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewChecklist.Add(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> UpdateCrewChecklist(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewChecklist = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewChecklist.Update(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> AddChecklistforAllCrewMembers(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewRosterAddInfo.AddChecklistforAllCrew(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> UpdateChecklistforAllCrewMembers(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewRosterAddInfo.UpdateChecklistforAllCrew(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> DeleteCrewChecklist(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewChecklist = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewChecklist.Delete(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> GetCrewChecklistLock(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewChecklist = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewChecklist.GetLock(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> GetCrewChecklistList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewChecklistManager objCrewChecklist = new FlightPak.Business.MasterCatalog.CrewChecklistManager();
                        retValue = objCrewChecklist.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Duty Type Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> AddCrewDutyType(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDuty))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewDuty.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewDuty.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDutyTypeManager objCrewDutyType = new FlightPak.Business.MasterCatalog.CrewDutyTypeManager();
                        retValue = objCrewDutyType.Add(crewDuty);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> DeleteCrewDutyType(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDuty))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewDuty.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewDuty.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDutyTypeManager objCrewDutyType = new FlightPak.Business.MasterCatalog.CrewDutyTypeManager();
                        retValue = objCrewDutyType.Delete(crewDuty);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> UpdateCrewDutyType(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDuty))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewDuty.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewDuty.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDutyTypeManager objCrewDutyType = new FlightPak.Business.MasterCatalog.CrewDutyTypeManager();
                        retValue = objCrewDutyType.Update(crewDuty);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> GetCrewDutyTypeList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDutyType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewDutyTypeManager objCrewDutyType = new FlightPak.Business.MasterCatalog.CrewDutyTypeManager();
                        retValue = objCrewDutyType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Hotel Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters> GetHotelsByAirportIDWithFilters(long AirportID, long HotelID, string HotelCD, bool FetchactiveOnly, bool FetchIsCrewOnly, bool FetchIsPaxOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.GetHotelsByAirportIDWithFilters(AirportID, HotelID, HotelCD, FetchactiveOnly, FetchIsCrewOnly, FetchIsPaxOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="HotelData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Hotel> AddHotel(Data.MasterCatalog.Hotel HotelData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Hotel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Hotel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HotelData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HotelData.CustomerID = UserPrincipal.Identity.CustomerID;
                        HotelData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.Add(HotelData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="HotelData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Hotel> UpdateHotel(Data.MasterCatalog.Hotel HotelData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Hotel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Hotel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HotelData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HotelData.CustomerID = UserPrincipal.Identity.CustomerID;
                        HotelData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.Update(HotelData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="HotelData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Hotel> DeleteHotel(Data.MasterCatalog.Hotel HotelData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Hotel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Hotel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HotelData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        HotelData.CustomerID = UserPrincipal.Identity.CustomerID;
                        HotelData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.Delete(HotelData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="HotelData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Hotel> GetHotelLock(Data.MasterCatalog.Hotel HotelData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Hotel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Hotel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HotelData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.GetLock(HotelData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllHotel> GetHotelInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllHotel>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.GetAllHotel> ReturnValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                                    ReturnValue = HotelCatalog.GetListInfo();
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return ReturnValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportID> GetHotelsByAirportID(long airportID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetHotelsByAirportID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.GetHotelsByAirportID(airportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelID> GetHotelByHotelID(long hotelID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.GetHotelByHotelID(hotelID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelCode> GetHotelByHotelCode(long airportID, string hotelCd)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelCode>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetHotelByHotelCode> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.GetHotelByHotelCode(airportID, hotelCd);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.Hotel> GetHotelList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Hotel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Hotel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.HotelManager HotelCatalog = new FlightPak.Business.MasterCatalog.HotelManager();
                        ReturnValue = HotelCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Fleet Profile Information"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> AddFleetProfileInformation(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfileInformation))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetProfileInformation.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetProfileInformation.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetProfileInformationManager FleetProfileInfoMgr = new FlightPak.Business.MasterCatalog.FleetProfileInformationManager();
                        objAcct = FleetProfileInfoMgr.Add(fleetProfileInformation);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> DeleteFleetProfileInformation(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfileInformation))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetProfileInformation.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetProfileInformation.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetProfileInformationManager FleetProfileInfoMgr = new FlightPak.Business.MasterCatalog.FleetProfileInformationManager();
                        objAcct = FleetProfileInfoMgr.Delete(fleetProfileInformation);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> UpdateFleetProfileInformation(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfileInformation))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetProfileInformation.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetProfileInformation.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetProfileInformationManager FleetProfileInfoMgr = new FlightPak.Business.MasterCatalog.FleetProfileInformationManager();
                        objAcct = FleetProfileInfoMgr.Update(fleetProfileInformation);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> GetFleetProfileInformationList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileInformation> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetProfileInformationManager FleetProfileInfoMgr = new FlightPak.Business.MasterCatalog.FleetProfileInformationManager();
                        objAcct = FleetProfileInfoMgr.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Payable Vendor Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters> GetAllVendorWithFilters(string VendorType, long VendorID, string VendorCD, string FetchHomebaseOnly, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                        retValue = objPayableVendor.GetAllVendorWithFilters(VendorType, VendorID, VendorCD, FetchHomebaseOnly, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> AddPayableVendorMaster(Data.MasterCatalog.Vendor payableVendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        payableVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        payableVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                        retValue = objPayableVendor.Add(payableVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> UpdatePayableVendorMaster(Data.MasterCatalog.Vendor payableVendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        payableVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        payableVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                        retValue = objPayableVendor.Update(payableVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> DeletePayableVendorMaster(Data.MasterCatalog.Vendor payableVendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        payableVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        payableVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                        retValue = objPayableVendor.Delete(payableVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetPayableVendorMasterLock(Data.MasterCatalog.Vendor payableVendor)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetPayableVendorMaster()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                        retValue = objPayableVendor.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor> GetPayableVendorInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.GetAllVendor> retValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                                    retValue = objPayableVendor.GetPayableVendorList();
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return retValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Vendor> GetPayableVendorMasterInfo(Data.MasterCatalog.Vendor payableVendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Vendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Vendor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PayableVendorManager objPayableVendor = new FlightPak.Business.MasterCatalog.PayableVendorManager();
                        retValue = objPayableVendor.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "TripManagerCheckListGroup Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetAllTripmanagerChecklistGroupWithFilters(long CheckGroupID, string CheckGroupCD, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager objTripManagerGroup = new FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager();
                        retValue = objTripManagerGroup.GetAllTripmanagerChecklistGroupWithFilters(CheckGroupID, CheckGroupCD, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripManagerChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> AddTripManagerChecklistGroup(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tripManagerChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        tripManagerChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager objTripManagerGroup = new FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager();
                        retValue = objTripManagerGroup.Add(tripManagerChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripManagerChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> DeleteTripManagerChecklistGroup(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tripManagerChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        tripManagerChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager objTripManagerGroup = new FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager();
                        retValue = objTripManagerGroup.Delete(tripManagerChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tripManagerChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> UpdateTripManagerChecklistGroup(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tripManagerChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        tripManagerChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager objTripManagerGroup = new FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager();
                        retValue = objTripManagerGroup.Update(tripManagerChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetTripManagerChecklistGroupList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager objTripManagerGroup = new FlightPak.Business.MasterCatalog.TripManagerCheckListGroupManager();
                        retValue = objTripManagerGroup.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "TripManagerCheckList Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters> GetTripManagerCheckListWithFilters(long CheckListID, string CheckListCD, long CheckGroupID, bool IsInactive)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripManagerCheckListManager objTripManagerCheckList = new FlightPak.Business.MasterCatalog.TripManagerCheckListManager();
                        retValue = objTripManagerCheckList.GetTripManagerCheckListWithFilters( CheckListID, CheckListCD, CheckGroupID, IsInactive);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> AddTripManagerChecklist(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tripManagerChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        tripManagerChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripManagerCheckListManager objTripManagerCheckList = new FlightPak.Business.MasterCatalog.TripManagerCheckListManager();
                        retValue = objTripManagerCheckList.Add(tripManagerChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> DeleteTripManagerChecklist(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tripManagerChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        tripManagerChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripManagerCheckListManager objTripManagerCheckList = new FlightPak.Business.MasterCatalog.TripManagerCheckListManager();
                        retValue = objTripManagerCheckList.Delete(tripManagerChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> UpdateTripManagerChecklist(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        tripManagerChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        tripManagerChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripManagerCheckListManager objTripManagerCheckList = new FlightPak.Business.MasterCatalog.TripManagerCheckListManager();
                        retValue = objTripManagerCheckList.Update(tripManagerChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> GetTripManagerChecklistList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripManagerCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripManagerCheckListManager objTripManagerCheckList = new FlightPak.Business.MasterCatalog.TripManagerCheckListManager();
                        retValue = objTripManagerCheckList.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList> GetTripManagerChecklistListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripManagerCheckListManager objTripManagerCheckList = new FlightPak.Business.MasterCatalog.TripManagerCheckListManager();
                        retValue = objTripManagerCheckList.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Roster Additional Info - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> AddCrewDefinition(Data.MasterCatalog.CrewDefinition CrewDefinition)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDefinition))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDefinition.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewDefinition.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDefinitionManager CrewDef = new FlightPak.Business.MasterCatalog.CrewDefinitionManager();
                        ReturnValue = CrewDef.Add(CrewDefinition);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> UpdateCrewDefinition(Data.MasterCatalog.CrewDefinition CrewDefinition)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDefinition))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDefinition.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewDefinition.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDefinitionManager CrewDef = new FlightPak.Business.MasterCatalog.CrewDefinitionManager();
                        ReturnValue = CrewDef.Update(CrewDefinition);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> DeleteCrewDefinition(Data.MasterCatalog.CrewDefinition CrewDefinition)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewDefinition> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDefinition))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewDefinition.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewDefinition.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewDefinitionManager CrewDef = new FlightPak.Business.MasterCatalog.CrewDefinitionManager();
                        ReturnValue = CrewDef.Delete(CrewDefinition);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewDefinition"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewDefinition> GetCrewDefinitionList(Data.MasterCatalog.GetCrewDefinition CrewDefinition)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewDefinition> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDefinition))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewDefinitionManager CrewDef = new FlightPak.Business.MasterCatalog.CrewDefinitionManager();
                        ReturnValue = CrewDef.GetListInfo(CrewDefinition);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Rating - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> AddCrewRating(Data.MasterCatalog.CrewRating CrewRating)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewRating>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewRating))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewRating.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewRating.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRatingManager Crew = new FlightPak.Business.MasterCatalog.CrewRatingManager();
                        ReturnValue = Crew.Add(CrewRating);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> UpdateCrewRating(Data.MasterCatalog.CrewRating CrewRating)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewRating>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewRating))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewRating.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewRating.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRatingManager Crew = new FlightPak.Business.MasterCatalog.CrewRatingManager();
                        ReturnValue = Crew.Update(CrewRating);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> DeleteCrewRating(Data.MasterCatalog.CrewRating CrewRating)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewRating>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewRating> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewRating))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewRating.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewRating.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewRatingManager Crew = new FlightPak.Business.MasterCatalog.CrewRatingManager();
                        ReturnValue = Crew.Delete(CrewRating);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewRating"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewRating> GetCrewRatingList(Data.MasterCatalog.GetCrewRating CrewRating)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewRating>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewRating> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewRating))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewRatingManager Crew = new FlightPak.Business.MasterCatalog.CrewRatingManager();
                        ReturnValue = Crew.GetListInfo(CrewRating);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember> GetCalculateCrewRating(Int64 AircraftCD, Int64 CrewID)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftCD, CrewID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewRatingManager Crew = new FlightPak.Business.MasterCatalog.CrewRatingManager();
                        ReturnValue = Crew.GetTypeRating(AircraftCD, CrewID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion
        #region "Crew Checklist Date - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> AddCrewCheckListDate(Data.MasterCatalog.CrewCheckListDetail CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewCheckListDateManager Crew = new FlightPak.Business.MasterCatalog.CrewCheckListDateManager();
                        ReturnValue = Crew.Add(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> UpdateCrewCheckListDate(Data.MasterCatalog.CrewCheckListDetail CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewCheckListDateManager Crew = new FlightPak.Business.MasterCatalog.CrewCheckListDateManager();
                        ReturnValue = Crew.Update(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> DeleteCrewCheckListDate(Data.MasterCatalog.CrewCheckListDetail CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewCheckListDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewCheckListDateManager Crew = new FlightPak.Business.MasterCatalog.CrewCheckListDateManager();
                        ReturnValue = Crew.Delete(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewCheckListDate> GetCrewCheckListDate(Data.MasterCatalog.GetCrewCheckListDate CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewCheckListDate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewCheckListDate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewCheckListDateManager Crew = new FlightPak.Business.MasterCatalog.CrewCheckListDateManager();
                        ReturnValue = Crew.GetListInfo(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Aircraft Assigned - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> AddCrewAircraftAssigned(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewAircraftAssigned))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewAircraftAssigned.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewAircraftAssigned.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager Crew = new FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager();
                        ReturnValue = Crew.Add(CrewAircraftAssigned);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> UpdateCrewAircraftAssigned(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewAircraftAssigned))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewAircraftAssigned.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewAircraftAssigned.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager Crew = new FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager();
                        ReturnValue = Crew.Update(CrewAircraftAssigned);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> DeleteCrewAircraftAssigned(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewAircraftAssigned))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewAircraftAssigned.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewAircraftAssigned.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager Crew = new FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager();
                        ReturnValue = Crew.Delete(CrewAircraftAssigned);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewAircraftAssigned"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> GetAircraftAssignedList(Data.MasterCatalog.CrewAircraftAssigned CrewAircraftAssigned)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewAircraftAssigned))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager Crew = new FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager();
                        ReturnValue = Crew.GetListInfo(CrewAircraftAssigned);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> GetSelectedAircraftAssignedList(Data.MasterCatalog.GetAllCrewAircraftAssign CrewAircraftAssigned)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewAircraftAssigned))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager Crew = new FlightPak.Business.MasterCatalog.CrewAircraftAssignedManager();
                        ReturnValue = Crew.GetListInfo(CrewAircraftAssigned);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Passport - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> AddCrewPassport(Data.MasterCatalog.CrewPassengerPassport CrewPassport)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPassport))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewPassport.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPassport.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewPassportManager Crew = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                        ReturnValue = Crew.Add(CrewPassport);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> UpdateCrewPassport(Data.MasterCatalog.CrewPassengerPassport CrewPassport)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPassport))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewPassport.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPassport.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewPassportManager Crew = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                        ReturnValue = Crew.Update(CrewPassport);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> DeleteCrewPassport(Data.MasterCatalog.CrewPassengerPassport CrewPassport)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPassport))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewPassport.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPassport.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewPassportManager Crew = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                        ReturnValue = Crew.Delete(CrewPassport);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPassport> GetCrewPassportListInfo(Data.MasterCatalog.CrewPassengerPassport CrewPassport)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPassport>>(() =>
                        {
                            Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPassport> ReturnValue = null;
                            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPassport))
                            {
                                //Handle methods throguh exception manager with return flag
                                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                exManager.Process(() =>
                                {
                                    FlightPak.Business.MasterCatalog.CrewPassportManager Crew = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                                    CrewPassport.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CrewPassport.LastUpdUID = UserPrincipal.Identity.Name;
                                    ReturnValue = Crew.GetListInfo(CrewPassport);
                                }, FlightPak.Common.Constants.Policy.BusinessLayer);
                            }
                            return ReturnValue;
                        }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewPassport"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetCrewPassportList(Data.MasterCatalog.CrewPassengerPassport CrewPassport)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerPassport> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPassport))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewPassportManager Crew = new FlightPak.Business.MasterCatalog.CrewPassportManager();
                        CrewPassport.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPassport.LastUpdUID = UserPrincipal.Identity.Name;
                        ReturnValue = Crew.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Crew Passenger Visa - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewPaxVisa"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> AddCrewPaxVisa(Data.MasterCatalog.CrewPassengerVisa CrewPaxVisa)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPaxVisa))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewPaxVisa.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPaxVisa.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewPaxVisaManager Crew = new FlightPak.Business.MasterCatalog.CrewPaxVisaManager();
                        ReturnValue = Crew.Add(CrewPaxVisa);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewPaxVisa"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> UpdateCrewPaxVisa(Data.MasterCatalog.CrewPassengerVisa CrewPaxVisa)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPaxVisa))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewPaxVisa.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPaxVisa.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewPaxVisaManager Crew = new FlightPak.Business.MasterCatalog.CrewPaxVisaManager();
                        ReturnValue = Crew.Update(CrewPaxVisa);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewPaxVisa"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> DeleteCrewPaxVisa(Data.MasterCatalog.CrewPassengerVisa CrewPaxVisa)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPaxVisa))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewPaxVisa.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPaxVisa.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CrewPaxVisaManager Crew = new FlightPak.Business.MasterCatalog.CrewPaxVisaManager();
                        ReturnValue = Crew.Delete(CrewPaxVisa);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewPaxVisa"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa> GetCrewVisaListInfo(Data.MasterCatalog.CrewPassengerVisa CrewPaxVisa)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPaxVisa))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewPaxVisaManager Crew = new FlightPak.Business.MasterCatalog.CrewPaxVisaManager();
                        CrewPaxVisa.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPaxVisa.LastUpdUID = UserPrincipal.Identity.Name;
                        ReturnValue = Crew.GetListInfo(CrewPaxVisa);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewPaxVisa"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> GetCrewPaxVisaList(Data.MasterCatalog.CrewPassengerVisa CrewPaxVisa)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CrewPassengerVisa> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewPaxVisa))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewPaxVisaManager Crew = new FlightPak.Business.MasterCatalog.CrewPaxVisaManager();
                        CrewPaxVisa.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewPaxVisa.LastUpdUID = UserPrincipal.Identity.Name;

                        ReturnValue = Crew.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Sifl Rate Manager"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> AddFareLevel(Data.MasterCatalog.FareLevel SiflFareLevel)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FareLevel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SiflFareLevel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SiflFareLevel.CustomerID = UserPrincipal.Identity.CustomerID;
                        SiflFareLevel.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.SiflRateManager SiflRate = new FlightPak.Business.MasterCatalog.SiflRateManager();
                        ReturnValue = SiflRate.Add(SiflFareLevel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> UpdateFareLevel(Data.MasterCatalog.FareLevel SiflFareLevel)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FareLevel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SiflFareLevel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SiflFareLevel.CustomerID = UserPrincipal.Identity.CustomerID;
                        SiflFareLevel.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.SiflRateManager SiflRate = new FlightPak.Business.MasterCatalog.SiflRateManager();
                        ReturnValue = SiflRate.Update(SiflFareLevel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> DeleteFareLevel(Data.MasterCatalog.FareLevel SiflFareLevel)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FareLevel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SiflFareLevel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        SiflFareLevel.CustomerID = UserPrincipal.Identity.CustomerID;
                        SiflFareLevel.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.SiflRateManager SiflRate = new FlightPak.Business.MasterCatalog.SiflRateManager();
                        ReturnValue = SiflRate.Delete(SiflFareLevel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="SiflFareLevel"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> GetFareLevelLock(Data.MasterCatalog.FareLevel SiflFareLevel)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FareLevel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SiflFareLevel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.SiflRateManager SiflRate = new FlightPak.Business.MasterCatalog.SiflRateManager();
                        ReturnValue = SiflRate.GetLock(SiflFareLevel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> GetFareLevel()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FareLevel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FareLevel> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.SiflRateManager SiflRate = new FlightPak.Business.MasterCatalog.SiflRateManager();
                        ReturnValue = SiflRate.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Check SiflRate in the Specified Dates
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.CheckSiflDate> CheckSiflDate(DateTime StartDate, DateTime EndDate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CheckSiflDate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CheckSiflDate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.SiflRateManager SiflRate = new FlightPak.Business.MasterCatalog.SiflRateManager();
                        ReturnValue = SiflRate.CheckSiflDate(StartDate, EndDate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion
        #region "Client Code Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientWithFilters(Int64 ClientID, string ClientCD, bool IsActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.GetClientWithFilters(ClientID, ClientCD, IsActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        public Business.Common.ReturnValue<Data.MasterCatalog.Client> AddClientCode(Data.MasterCatalog.Client clientCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(clientCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        clientCode.CustomerID = UserPrincipal.Identity.CustomerID;
                        clientCode.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.Add(clientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> UpdateClientCode(Data.MasterCatalog.Client clientCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(clientCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        clientCode.CustomerID = UserPrincipal.Identity.CustomerID;
                        clientCode.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.Update(clientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> UpdateClientCodeAll(Int64 ClientID, string UserMaster, string Fleet, string Crew, string Passenger, string Department, string FlightCategory, string FlightPurpose, string FuelLocator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.UpdateAll(ClientID, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> DeleteClientCode(Data.MasterCatalog.Client clientCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(clientCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        clientCode.CustomerID = UserPrincipal.Identity.CustomerID;
                        clientCode.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.Delete(clientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientCodeLock(Data.MasterCatalog.Client clientCode)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(clientCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.GetLock(clientCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Client> GetClientCodeList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Client>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Client> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ClientCodeManager objClientCode = new FlightPak.Business.MasterCatalog.ClientCodeManager();
                        retValue = objClientCode.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Runway Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> AddRunwayMaster(Data.MasterCatalog.Runway runway)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> UpdateRunwayMaster(Data.MasterCatalog.Runway runway)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> DeleteRunwayMaster(Data.MasterCatalog.Runway runway)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayMasterLock(Data.MasterCatalog.Runway runway)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayMaster()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Runway>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Runway> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.RunwayManager objrunway = new FlightPak.Business.MasterCatalog.RunwayManager();
                        retValue = objrunway.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayMasterInfo()
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Runway> GetRunwayByAirportID(Int64 AirportID)
        {
            //Handle methods through exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Runway>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Runway> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.RunwayManager RunwayCatalog = new FlightPak.Business.MasterCatalog.RunwayManager();
                        ReturnValue = RunwayCatalog.GetRunwayByAirportID(AirportID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Fleet Component manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> GetFleetComponentList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetComponentManager objFleetComponent = new FlightPak.Business.MasterCatalog.FleetComponentManager();
                        retValue = objFleetComponent.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> AddFleetComponentType(Data.MasterCatalog.FleetComponent fleetComponent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetComponent))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetComponentManager objFleetComponent = new FlightPak.Business.MasterCatalog.FleetComponentManager();
                        fleetComponent.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetComponent.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetComponent.Add(fleetComponent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> UpdateFleetComponentType(Data.MasterCatalog.FleetComponent fleetComponent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetComponent))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetComponentManager objFleetComponent = new FlightPak.Business.MasterCatalog.FleetComponentManager();
                        fleetComponent.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetComponent.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetComponent.Update(fleetComponent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> DeleteFleetComponentType(Data.MasterCatalog.FleetComponent fleetComponent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetComponent))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetComponent.LastUpdUID = UserPrincipal.Identity.Name;
                        fleetComponent.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetComponentManager objFleetComponent = new FlightPak.Business.MasterCatalog.FleetComponentManager();
                        retValue = objFleetComponent.Delete(fleetComponent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> GetFleetComponentLock(Data.MasterCatalog.FleetComponent fleetComponent)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetComponent> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetComponent))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetComponentManager objFleetComponent = new FlightPak.Business.MasterCatalog.FleetComponentManager();
                        retValue = objFleetComponent.GetLock(fleetComponent);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "FleetComponentManager"
        #region "Fleet Chargerate History manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> GetFleetChargeRateList(Int64 FleetID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleetChargeRateHistory = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        retValue = objFleetChargeRateHistory.GetList(FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> CalculateChargeRate(Data.MasterCatalog.FleetChargeRate fleetChargeRate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargeRate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetChargeRate.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetChargeRate.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        retValue = objCrewRosterAddInfo.CalculateFleet(fleetChargeRate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> AddFleetChargeRateType(Data.MasterCatalog.FleetChargeRate fleetChargerateHistory)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargerateHistory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleetChargerateHistory = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        fleetChargerateHistory.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetChargerateHistory.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetChargerateHistory.Add(fleetChargerateHistory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> UpdateFleetChargeRateType(Data.MasterCatalog.FleetChargeRate fleetChargerateHistory)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargerateHistory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleetChargerateHistory = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        fleetChargerateHistory.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetChargerateHistory.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetChargerateHistory.Update(fleetChargerateHistory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> DeleteFleetChargeRateType(Data.MasterCatalog.FleetChargeRate fleetChargerateHistory)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargerateHistory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetChargerateHistory.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetChargerateHistory.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleetChargerateHistory = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        retValue = objFleetChargerateHistory.Delete(fleetChargerateHistory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> GetFleetChargeRateLock(Data.MasterCatalog.FleetChargeRate fleetChargerateHistory)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetChargeRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargerateHistory))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleetChargerateHistory = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        retValue = objFleetChargerateHistory.GetLock(fleetChargerateHistory);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists> GetFleetChargeRateExists(DateTime BeginRateDT, DateTime EndRateDT, Int64 FleetID, Int64 FleetChargeRateID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(BeginRateDT, EndRateDT, FleetID, FleetChargeRateID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleet = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                        retValue = objFleet.GetFleetChargeRateExists(BeginRateDT, EndRateDT, FleetID, FleetChargeRateID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Fleet Chargerate History manager"
        #region "Customer Address Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> AddCustomerAddress(Data.MasterCatalog.CustomAddress customerAddress)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        customerAddress.CustomerID = UserPrincipal.Identity.CustomerID;
                        customerAddress.UserName = UserPrincipal.Identity.Name;
                        customerAddress.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CustomerAddressManager objCustomerAddress = new FlightPak.Business.MasterCatalog.CustomerAddressManager();
                        retValue = objCustomerAddress.Add(customerAddress);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> UpdateCustomerAddress(Data.MasterCatalog.CustomAddress customerAddress)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        customerAddress.CustomerID = UserPrincipal.Identity.CustomerID;
                        customerAddress.UserName = UserPrincipal.Identity.Name;
                        customerAddress.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CustomerAddressManager objCustomerAddress = new FlightPak.Business.MasterCatalog.CustomerAddressManager();
                        retValue = objCustomerAddress.Update(customerAddress);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> DeleteCustomerAddress(Data.MasterCatalog.CustomAddress customerAddress)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        customerAddress.CustomerID = UserPrincipal.Identity.CustomerID;
                        customerAddress.UserName = UserPrincipal.Identity.Name;
                        customerAddress.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CustomerAddressManager objCustomerAddress = new FlightPak.Business.MasterCatalog.CustomerAddressManager();
                        retValue = objCustomerAddress.Delete(customerAddress);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetCustomerID> GetCustomerAddressID()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCustomerID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCustomerID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CustomerAddressManager objCrew = new FlightPak.Business.MasterCatalog.CustomerAddressManager();
                        retValue = objCrew.GetCustomerAddressID();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> GetCustomerAddressLock(Data.MasterCatalog.CustomAddress customerAddress)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CustomAddress> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CustomerAddressManager objCustomerAddress = new FlightPak.Business.MasterCatalog.CustomerAddressManager();
                        retValue = objCustomerAddress.GetLock(customerAddress);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCustomerAddress> GetCustomerAddress()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCustomerAddress>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCustomerAddress> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CustomerAddressManager objCustomerAddress = new FlightPak.Business.MasterCatalog.CustomerAddressManager();
                        retValue = objCustomerAddress.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Fleet Forecast manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> GetFleetForecastList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetForecastManager objFleetForecast = new FlightPak.Business.MasterCatalog.FleetForecastManager();
                        retValue = objFleetForecast.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> AddFleetForecastType(Data.MasterCatalog.FleetForeCast fleetForecast)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetForecast))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetForecastManager objFleetForecast = new FlightPak.Business.MasterCatalog.FleetForecastManager();
                        fleetForecast.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetForecast.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetForecast.Add(fleetForecast);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> UpdateFleetForecastType(Data.MasterCatalog.FleetForeCast fleetForecast)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetForecast))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetForecastManager objFleetForecast = new FlightPak.Business.MasterCatalog.FleetForecastManager();
                        fleetForecast.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetForecast.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetForecast.Update(fleetForecast);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> DeleteFleetForecastType(Data.MasterCatalog.FleetForeCast fleetForecast)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetForecast))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetForecast.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetForecast.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetForecastManager objFleetForecast = new FlightPak.Business.MasterCatalog.FleetForecastManager();
                        retValue = objFleetForecast.Delete(fleetForecast);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> GetFleetForecastLock(Data.MasterCatalog.FleetForeCast fleetForecast)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetForeCast> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetForecast))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetForecastManager objFleetForecast = new FlightPak.Business.MasterCatalog.FleetForecastManager();
                        retValue = objFleetForecast.GetLock(fleetForecast);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForecast> GetFleetForecastInfo(Int64 FleetID, Int32 year)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForecast>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetForecast> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID, year))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetForecastManager objfleetForecast = new FlightPak.Business.MasterCatalog.FleetForecastManager();
                        retValue = objfleetForecast.GetListInfo(FleetID, year);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Fleet Forecast manager"
        #region "ConversionTable Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> AddConversionTable(Data.MasterCatalog.ConversionTable conversionTable)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(conversionTable))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        conversionTable.CustomerID = UserPrincipal.Identity.CustomerID;
                        conversionTable.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ConversionTableManager conversionTableManager = new FlightPak.Business.MasterCatalog.ConversionTableManager();
                        retValue = conversionTableManager.Add(conversionTable);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> DeleteConversionTable(Data.MasterCatalog.ConversionTable conversionTable)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(conversionTable))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        conversionTable.CustomerID = UserPrincipal.Identity.CustomerID;
                        conversionTable.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ConversionTableManager conversionTableManager = new FlightPak.Business.MasterCatalog.ConversionTableManager();
                        retValue = conversionTableManager.Delete(conversionTable);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> UpdateConversionTable(Data.MasterCatalog.ConversionTable conversionTable)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(conversionTable))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        conversionTable.CustomerID = UserPrincipal.Identity.CustomerID;
                        conversionTable.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.ConversionTableManager conversionTableManager = new FlightPak.Business.MasterCatalog.ConversionTableManager();
                        retValue = conversionTableManager.Update(conversionTable);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> GetConversionTableList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.ConversionTable> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.ConversionTableManager conversionTableManager = new FlightPak.Business.MasterCatalog.ConversionTableManager();
                        retValue = conversionTableManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "FlightLogGet Manager"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="GetAllFlightLogData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> AddGetAllFlightLog(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="GetAllFlightLogData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> UpdateGetAllFlightLog(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="GetAllFlightLogData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> DeleteGetAllFlightLog(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetAllFlightLogLock(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetAllFlightLogList(Data.MasterCatalog.TSFlightLog TSFlightLog)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSFlightLog))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightLogGetManager FlightLogCatalog = new FlightPak.Business.MasterCatalog.FlightLogGetManager();
                        ReturnValue = FlightLogCatalog.GetAllFlightLog(TSFlightLog);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLog> GetAllPilotLogList(Data.MasterCatalog.TSFlightLog TSFlightLog)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSFlightLog))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightLogGetManager FlightLogCatalog = new FlightPak.Business.MasterCatalog.FlightLogGetManager();
                        ReturnValue = FlightLogCatalog.GetPilotList(TSFlightLog);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> ResetFlightLog(Data.MasterCatalog.TSFlightLog TSFlightLog)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSFlightLog))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightLogManager FlightLogCatalog = new FlightPak.Business.MasterCatalog.FlightLogManager();
                        TSFlightLog.CustomerID = UserPrincipal.Identity.CustomerID;
                        ReturnValue = FlightLogCatalog.ResetFlightLog(TSFlightLog);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetAllFlightLogInfo(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region "FlightLog Manager"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="FlightLogData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> AddTSFlightLog(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="FlightLogData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> UpdateTSFlightLog(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="FlightLogData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> DeleteTSFlightLog(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> GetTSFlightLogList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSFlightLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightLogManager FlightLogCatalog = new FlightPak.Business.MasterCatalog.FlightLogManager();
                        ReturnValue = FlightLogCatalog.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "DefinitionLog Manager"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="DefinitionData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> AddTSDefinitionLog(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DefinitionData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DefinitionData.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.DefinitionLogManager CustomFlightLogCatalog = new FlightPak.Business.MasterCatalog.DefinitionLogManager();
                        ReturnValue = CustomFlightLogCatalog.Add(DefinitionData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="DefinitionData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> UpdateTSDefinitionLog(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DefinitionData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DefinitionData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DefinitionData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DefinitionLogManager CustomFlightLogCatalog = new FlightPak.Business.MasterCatalog.DefinitionLogManager();
                        ReturnValue = CustomFlightLogCatalog.Update(DefinitionData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="DefinitionData"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> DeleteTSDefinitionLog(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DefinitionData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        DefinitionData.CustomerID = UserPrincipal.Identity.CustomerID;
                        DefinitionData.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.DefinitionLogManager CustomFlightLogCatalog = new FlightPak.Business.MasterCatalog.DefinitionLogManager();
                        ReturnValue = CustomFlightLogCatalog.Delete(DefinitionData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLogReset> GetPilotLogReset()
        {
            //Handle methods throguh exception manager with return flag
            Business.Common.ReturnValue<Data.MasterCatalog.GetAllPilotLogReset> retValue = null;
            FlightPak.Business.MasterCatalog.DefinitionLogManager objTSDefinitionLog = new FlightPak.Business.MasterCatalog.DefinitionLogManager();
            retValue = objTSDefinitionLog.GetListInfo();
            return retValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.TSDefinitionLog> GetTSDefinitionLogList()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region "Travel Coordinator Requestor Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> AddTravelCoordinatorRequestor(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinatorRequestor.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinatorRequestor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
                        retValue = objTravelCoordinatorRequestor.Add(travelCoordinatorRequestor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        //public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestorSelectedList(Int64 travelCoordCD)
        //{
        //    Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
        //    try
        //    {
        //        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
        //        retValue = objTravelCoordinatorRequestor.GetTravelCoordinatorRequestorSelectedList(travelCoordCD);
        //    }
        //    catch (System.Exception exc)
        //    {
        //    }
        //    return retValue;
        //}
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> UpdateTravelCoordinatorRequestor(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinatorRequestor.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinatorRequestor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
                        retValue = objTravelCoordinatorRequestor.Update(travelCoordinatorRequestor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> DeleteTravelCoordinatorRequestor(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinatorRequestor.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinatorRequestor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
                        retValue = objTravelCoordinatorRequestor.Delete(travelCoordinatorRequestor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> GetTravelCoordinatorRequestorLock(Data.MasterCatalog.TravelCoordinator travelCoordinator)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinator> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinator))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorManager();
                        retValue = objTravelCoordinatorRequestor.GetLock(travelCoordinator);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestor()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
                        retValue = objTravelCoordinatorRequestor.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestorInfo(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager objTravelCoordinatorRequestor = new FlightPak.Business.MasterCatalog.TravelCoordinatorRequestorManager();
                        retValue = objTravelCoordinatorRequestor.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "TravelCoordinator Requester Manager"
        #region "Passenger Type Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllPassenger> GetPassengerList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllPassenger>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllPassenger> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager Passenger = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        ReturnValue = Passenger.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> AddPassengerType(Data.MasterCatalog.Passenger passenger)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> UpdatePassengerType(Data.MasterCatalog.Passenger passenger)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> DeletePassengerType(Data.MasterCatalog.Passenger passenger)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> GetPassengerLock(Data.MasterCatalog.Passenger passenger)
        {
            throw new NotImplementedException();
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID> GetPassengerbyPassengerRequestorID(long PassengerRequestorID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager Passenger = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        ReturnValue = Passenger.GetPassengerbyPassengerRequestorID(PassengerRequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Travel Coordinator Fleet Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> AddTravelCoordinatorFleet(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinatorFleet.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinatorFleet.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
                        retValue = objTravelCoordinatorFleet.Add(travelCoordinatorFleet);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        //public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleetSelectedList(Int64 travelCoordCD)
        //{
        //    Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
        //    try
        //    {
        //        FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
        //        retValue = objTravelCoordinatorFleet.GetTravelCoordinatorFleetSelectedList(travelCoordCD);
        //    }
        //    catch (System.Exception exc)
        //    {
        //    }
        //    return retValue;
        //}
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> UpdateTravelCoordinatorFleet(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinatorFleet.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinatorFleet.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
                        retValue = objTravelCoordinatorFleet.Update(travelCoordinatorFleet);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> DeleteTravelCoordinatorFleet(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        travelCoordinatorFleet.CustomerID = UserPrincipal.Identity.CustomerID;
                        travelCoordinatorFleet.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
                        retValue = objTravelCoordinatorFleet.Delete(travelCoordinatorFleet);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleet()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
                        retValue = objTravelCoordinatorFleet.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleetInfo(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager objTravelCoordinatorFleet = new FlightPak.Business.MasterCatalog.TravelCoordinatorFleetManager();
                        retValue = objTravelCoordinatorFleet.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Passenger Requestor Manager"

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters> GetAllPassengerWithFilters(long ClientId, long  CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId)
        {
            Business.Common.ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters> passenger = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objSchedule = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        passenger = objSchedule.GetAllPassengerWithFilters(ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return passenger;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        public ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result> GetAllPassengerTelerikPagedGrid(long ClientId, long CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId, int start, int end, string sort, string PassengerName, string DepartmentCD, string DepartmentName, string PhoneNum)
        {
            Business.Common.ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result> passenger = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objSchedule = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        passenger = objSchedule.GetAllPassengerTelerikPagedGrid(ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId, start, end, sort, PassengerName, DepartmentCD, DepartmentName, PhoneNum);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return passenger;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }

        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> AddPassengerRequestor(Data.MasterCatalog.Passenger Passenger)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Passenger>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Passenger> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Passenger.CustomerID = UserPrincipal.Identity.CustomerID;
                        //Passenger.ClientID = UserPrincipal.Identity.ClientId;
                        Passenger.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        retValue = objPaxGroup.Add(Passenger);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> UpdatePassengerRequestor(Data.MasterCatalog.Passenger Passenger)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Passenger>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Passenger> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Passenger.CustomerID = UserPrincipal.Identity.CustomerID;
                        //Passenger.ClientID = UserPrincipal.Identity.ClientId;
                        Passenger.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        retValue = objPaxGroup.Update(Passenger);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> DeletePassengerRequestor(Data.MasterCatalog.Passenger Passenger)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Passenger>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Passenger> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Passenger.CustomerID = UserPrincipal.Identity.CustomerID;
                        //Passenger.ClientID = UserPrincipal.Identity.ClientId;
                        Passenger.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        retValue = objPaxGroup.Delete(Passenger);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> GetPassengerRequestorLock(Data.MasterCatalog.Passenger Passenger)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> GetPassengerRequestorList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Passenger>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Passenger> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        retValue = objPaxGroup.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.Passenger> UpdatePassportForFutureTrip(Int64 PassportID, Int64 PassengerID, Int64 CrewID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.Passenger>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.Passenger> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassportID, PassengerID, CrewID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        retValue = objPaxGroup.UpdatePassportForFutureTrip(PassportID, PassengerID, CrewID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerAssociate> GetPassengerAssociate()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerAssociate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerAssociate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        retValue = objPaxGroup.GetPassengerAssociate();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Added by Scheduling calendar team to get Passenger Entity by Passenger Code.
        /// </summary>
        /// <param name="requestorCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerRequestorResult> GetPassengerRequestorByPassengerRequestorCD(string requestorCode)
        {
            Business.Common.ReturnValue<Data.MasterCatalog.PassengerRequestorResult> passenger = null;


            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.PassengerRequestorResult>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager objSchedule = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        passenger = objSchedule.GetPassengerRequestorByPassengerRequestorCD(requestorCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return passenger;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);


        }
        #endregion
        #region "Passenger Additional Information"
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> AddPassengerAddlInfo(Data.MasterCatalog.PassengerAdditionalInfo Passenger)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Passenger.CustomerID = UserPrincipal.Identity.CustomerID;
                        Passenger.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objPaxGroup.Add(Passenger);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> UpdatePassengerAddlInfo(Data.MasterCatalog.PassengerAdditionalInfo Passenger)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Passenger.CustomerID = UserPrincipal.Identity.CustomerID;
                        Passenger.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objPaxGroup.Update(Passenger);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> AddforAllPaxMembers(Data.MasterCatalog.PassengerInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objCrewRosterAddInfo.AddforAllPax(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> UpdateforAllPaxMembers(Data.MasterCatalog.PassengerInformation crewRosterAddInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        crewRosterAddInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        crewRosterAddInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objCrewRosterAddInfo.UpdateforAllPax(crewRosterAddInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }



        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> DeletePassengerAddlInfo(Data.MasterCatalog.PassengerAdditionalInfo Passenger)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        Passenger.CustomerID = UserPrincipal.Identity.CustomerID;
                        Passenger.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objPaxGroup.Delete(Passenger);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetPassengerAddlInfoLock(Data.MasterCatalog.PassengerAdditionalInfo Passenger)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetPassengerAddlInfoList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objPaxGroup.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetPassengerAddlInfoListReqID(Int64 RequestorID)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerAddlInfoManager objPaxGroup = new FlightPak.Business.MasterCatalog.PassengerAddlInfoManager();
                        retValue = objPaxGroup.GetListBYRequestorID(RequestorID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region "Passenger Checklist Date - Sub Section of Crew Roster"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> AddPaxCheckListDate(Data.MasterCatalog.PassengerCheckListDetail CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerChecklistManager Crew = new FlightPak.Business.MasterCatalog.PassengerChecklistManager();
                        ReturnValue = Crew.Add(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> UpdatePaxCheckListDate(Data.MasterCatalog.PassengerCheckListDetail CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerChecklistManager Crew = new FlightPak.Business.MasterCatalog.PassengerChecklistManager();
                        ReturnValue = Crew.Update(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records as deleted in table and returns generics
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> DeletePaxCheckListDate(Data.MasterCatalog.PassengerCheckListDetail CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CrewChecklist.CustomerID = UserPrincipal.Identity.CustomerID;
                        CrewChecklist.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.PassengerChecklistManager Crew = new FlightPak.Business.MasterCatalog.PassengerChecklistManager();
                        ReturnValue = Crew.Delete(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to get records from table and returns list
        /// </summary>
        /// <param name="CrewChecklist"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPaxChecklistDate> GetPaxCheckListDate(Data.MasterCatalog.GetPaxChecklistDate CrewChecklist)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPaxChecklistDate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPaxChecklistDate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerChecklistManager Crew = new FlightPak.Business.MasterCatalog.PassengerChecklistManager();
                        ReturnValue = Crew.GetListInfo(CrewChecklist);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion


        #region "Fuel Vendor Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> AddFuelVendor(Data.MasterCatalog.FuelVendor FuelVendor)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FuelVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        FuelVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.Add(FuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public long AddFuelVendorBulk(Data.MasterCatalog.FuelVendor FuelVendor)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<long>(() =>
            {
                //Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;
                long FuelVendorID = 0;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FuelVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        FuelVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        FuelVendorID = objFuelVendor.AddBulkFuel(FuelVendor);
                        Data.MasterCatalog.FuelVendor fv = new Data.MasterCatalog.FuelVendor();
                        fv.FuelVendorID = FuelVendorID;
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }
                return FuelVendorID;

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> UpdateFuelVendor(Data.MasterCatalog.FuelVendor FuelVendor)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FuelVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        FuelVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.Update(FuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }

                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> DeleteFuelVendor(Data.MasterCatalog.FuelVendor FuelVendor)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FuelVendor.CustomerID = UserPrincipal.Identity.CustomerID;
                        FuelVendor.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.Delete(FuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorLock(Data.MasterCatalog.FuelVendor FuelVendor)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.GetLock(FuelVendor);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendor()
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {

                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }


                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.GetFuelVendorText> GetfuelVendorText(Int64 FuelVendorID, decimal GallonFrom, decimal GallonTo, Int64 DepartureICAO, string FBOName)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFuelVendorText>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFuelVendorText> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.GetFuelVendorText(FuelVendorID, GallonFrom, GallonTo, DepartureICAO, FBOName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorInfoByFilter(long? fuelVendorId,string fuelVendorCode)
        {

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelVendor> retValue = null;


                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendorId, fuelVendorCode))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.GetFuelVendorByFuelVendorFilter(fuelVendorId, fuelVendorCode);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }

                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        
        #endregion
        #region "Fleet Profile Engine Airframe Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> AddFleetInformationType(Data.MasterCatalog.FleetInformation fleetinformation)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinformation))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInformation objFleetInformation = new FlightPak.Business.MasterCatalog.FleetInformation();
                        fleetinformation.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetinformation.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetInformation.Add(fleetinformation);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> UpdateFleetInformation(Data.MasterCatalog.FleetInformation FleetInfo)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetInfo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInformation objFleetInfo = new FlightPak.Business.MasterCatalog.FleetInformation();
                        FleetInfo.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetInfo.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetInfo.Update(FleetInfo);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> GetFleetInformation(Int64 tailNum)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tailNum))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInformation objFleetInfo = new FlightPak.Business.MasterCatalog.FleetInformation();
                        retValue = objFleetInfo.GetFleetInformation(tailNum);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> GetFleetInformationTypeLock(Data.MasterCatalog.FleetInformation fleetinformation)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetInformation> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinformation))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInformation objFleetInformation = new FlightPak.Business.MasterCatalog.FleetInformation();
                        retValue = objFleetInformation.GetLock(fleetinformation);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Fleet Profile Engine Airframe Manager"
        #region "Fleet Profile International Data Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> GetFleetPairList(Int64 FleetID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetPair>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInternationalDataManager objFleetPair = new FlightPak.Business.MasterCatalog.FleetInternationalDataManager();
                        retValue = objFleetPair.GetFleetPairList(FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> AddFleetInternationalType(Data.MasterCatalog.FleetPair fleetinternationaldata)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetPair>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinternationaldata))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInternationalDataManager objFleetInternational = new FlightPak.Business.MasterCatalog.FleetInternationalDataManager();
                        fleetinternationaldata.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetinternationaldata.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetInternational.Add(fleetinternationaldata);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> UpdateFleetinternationaldata(Data.MasterCatalog.FleetPair fleetinternationaldata)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetPair>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> objAcct = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinternationaldata))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fleetinternationaldata.CustomerID = UserPrincipal.Identity.CustomerID;
                        fleetinternationaldata.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetInternationalDataManager FleetIntlData = new FlightPak.Business.MasterCatalog.FleetInternationalDataManager();
                        objAcct = FleetIntlData.Update(fleetinternationaldata);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objAcct;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> GetFleetInternationalTypeLock(Data.MasterCatalog.FleetPair fleetinternationaldata)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetPair>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetPair> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinternationaldata))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInternationalDataManager objFleetInternational = new FlightPak.Business.MasterCatalog.FleetInternationalDataManager();
                        retValue = objFleetInternational.GetLock(fleetinternationaldata);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetPair> GetFleetPair(Int64 FleetID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetPair>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetPair> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInternationalDataManager objFleetPair = new FlightPak.Business.MasterCatalog.FleetInternationalDataManager();
                        retValue = objFleetPair.GetFleetPair(FleetID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fleet New Charter Rate Manager"
        /// <summary>
        /// Implementation of interface to insert records in table and returns generics
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> AddFleetNewCharterRate(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objFleetInformation = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        FleetNewCharterRate.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetNewCharterRate.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetInformation.Add(FleetNewCharterRate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> UpdateCharterNewRateTax(Int64 FleetID, Int64 AircraftID, bool DailyUsageTax, bool LandingFeeTax, string LastupdateID, DateTime LastUpTS, Decimal MinimumDailyRequirement, Decimal StandardCrewIntl, Decimal StandardCrewDOM, Int64 CQCustomerID, Decimal MarginalPercentage)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID, AircraftID, DailyUsageTax, LandingFeeTax, LastupdateID, LastUpTS, MinimumDailyRequirement, StandardCrewIntl, StandardCrewDOM, CQCustomerID, MarginalPercentage))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        LastupdateID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        retValue = objCrewRosterAddInfo.UpdateCharterTax(FleetID, AircraftID, DailyUsageTax, LandingFeeTax, LastupdateID, LastUpTS, MinimumDailyRequirement, StandardCrewIntl, StandardCrewDOM, CQCustomerID, MarginalPercentage);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAll(Int64 NewAircraftID, Int64 OldAircraftID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewAircraftID, OldAircraftID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //LastupdateID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        retValue = objCrewRosterAddInfo.CopyAll(NewAircraftID, OldAircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAllByFleet(Int64 NewAircraftID, Int64 OldAircraftID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewAircraftID, OldAircraftID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //LastupdateID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        retValue = objCrewRosterAddInfo.CopyAllByFleet(NewAircraftID, OldAircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAllByCQ(Int64 NewAircraftID, Int64 OldAircraftID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewAircraftID, OldAircraftID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //LastupdateID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objCrewRosterAddInfo = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        retValue = objCrewRosterAddInfo.CopyAllByCQ(NewAircraftID, OldAircraftID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// Implementation of interface to update records in table and returns generics
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> UpdateFleetNewCharterRate(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objFleetInfo = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        FleetNewCharterRate.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetNewCharterRate.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFleetInfo.Update(FleetNewCharterRate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get records in table and returns generics
        /// </summary>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate> GetAllFleetNewCharterRate()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objFleetInfo = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        retValue = objFleetInfo.GetFleetNewCharterRateListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to Lock records in table and returns generics
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> GetFleetNewCharterRateLock(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate objFleetInformation = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        retValue = objFleetInformation.GetLock(FleetNewCharterRate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to delete records in table and returns generics
        /// </summary>
        /// <param name="FleetNewCharterRate"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> DeleteFleetNewCharter(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetNewCharterRate> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FleetNewCharterRate.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetNewCharterRate.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate FleetNewCharterRateMgr = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        ReturnValue = FleetNewCharterRateMgr.Delete(FleetNewCharterRate);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Implementation of interface to get  records in table and returns generics
        /// </summary>
        /// <param name="FleetNewCharterRateID"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID> GetFleetNewCharterByFleetNewCharterID(Int64 FleetNewCharterRateID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetNewCharterRate CrewMgr = new FlightPak.Business.MasterCatalog.FleetNewCharterRate();
                        ReturnValue = CrewMgr.GetAllFleetNewCharterByFleetNewCharterID(FleetNewCharterRateID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion "Fleet Profile Engine Airframe Manager"
        #region "FileWarehouseManager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> AddFWHType(Data.MasterCatalog.FileWarehouse FileWH)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FileWH))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FileWarehouseManager objFWH = new FlightPak.Business.MasterCatalog.FileWarehouseManager();
                        FileWH.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFWH.Add(FileWH);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> GetFileWarehouseList(string RecordType, Int64 RecordID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RecordType, RecordID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FileWarehouseManager objFWH = new FlightPak.Business.MasterCatalog.FileWarehouseManager();
                        retValue = objFWH.GetFWHList(RecordType, RecordID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> UpdateFWHType(Data.MasterCatalog.FileWarehouse FileWH)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FileWH))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FileWarehouseManager objFWH = new FlightPak.Business.MasterCatalog.FileWarehouseManager();
                        FileWH.CustomerID = UserPrincipal.Identity.CustomerID;
                        FileWH.LastUpdUID = UserPrincipal.Identity.Name;
                        retValue = objFWH.Update(FileWH);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> DeleteFWHType(Data.MasterCatalog.FileWarehouse FileWH)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FileWarehouse> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FileWH))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FileWH.CustomerID = UserPrincipal.Identity.CustomerID;
                        FileWH.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FileWarehouseManager objFWH = new FlightPak.Business.MasterCatalog.FileWarehouseManager();
                        retValue = objFWH.Delete(FileWH);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "RoomTypeManager"
        /// <summary>
        /// To Get the values from the Room Type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public Business.Common.ReturnValue<Data.MasterCatalog.RoomType> GetRoomList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.RoomType>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.RoomType> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.RoomTypeManager objRoomType = new FlightPak.Business.MasterCatalog.RoomTypeManager();
                        retValue = objRoomType.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "RoomDescription Manager"
        /// <summary>
        /// To get the values from RoomDescription
        /// </summary>
        public Business.Common.ReturnValue<Data.MasterCatalog.RoomDescription> GetRoomDescriptionList()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.RoomDescription>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.RoomDescription> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.RoomDescriptionManager objRoomDescription = new FlightPak.Business.MasterCatalog.RoomDescriptionManager();
                        retValue = objRoomDescription.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Fleet Profile Definition"
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> AddFleetAddlInfo(Data.MasterCatalog.FleetProfileDefinition FleetAI)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetAI))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FleetAI.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetAI.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager objAddInfo = new FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager();
                        retValue = objAddInfo.Add(FleetAI);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> UpdateFleetAddlInfo(Data.MasterCatalog.FleetProfileDefinition FleetAI)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetAI))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FleetAI.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetAI.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager objAddInfo = new FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager();
                        retValue = objAddInfo.Update(FleetAI);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> DeleteFleetAddlInfo(Data.MasterCatalog.FleetProfileDefinition FleetAI)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetAI))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FleetAI.CustomerID = UserPrincipal.Identity.CustomerID;
                        FleetAI.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager objAddInfo = new FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager();
                        retValue = objAddInfo.Delete(FleetAI);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> GetFleetAddlInfoLock(Data.MasterCatalog.FleetProfileDefinition FleetAI)
        {
            throw new NotImplementedException();
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> GetFleetAddlInfoList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FleetProfileDefinition> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager objAddInfo = new FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager();
                        retValue = objAddInfo.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition> GetFleetAdditionalInfoList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager objFleet = new FlightPak.Business.MasterCatalog.FleetAdditionalInfoManager();
                        retValue = objFleet.GetFleetProfileDefinition();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "Worldwind and WinStat"
        /// <summary>
        /// This method is created in Business calculations
        /// </summary>
        /// <param name="DepartZoneAirprt">Airport's DepartureZone </param>
        /// <param name="ArrivZoneAirprt">Airport's ArrivalZone</param>
        /// <param name="Quartr">Quarter</param>
        /// <param name="Alt">Altitude</param>
        /// <returns>World Wind DB Values</returns>
        public ReturnValue<Data.MasterCatalog.WorldWind> GetWorldWindInfo(long DepartZoneAirprt, long ArrivZoneAirprt, string Quartr, string Alt)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.WorldWind>>(() =>
            {
                ReturnValue<Data.MasterCatalog.WorldWind> objWind = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartZoneAirprt, ArrivZoneAirprt, Quartr, Alt))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.WorldWindManager worldWindManager = new FlightPak.Business.MasterCatalog.WorldWindManager();
                        objWind = worldWindManager.GetWorldWindInfo(DepartZoneAirprt, ArrivZoneAirprt, Quartr, Alt);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objWind;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Hemisphere"></param>
        /// <param name="Quadrilateral"></param>
        /// <param name="Altitude"></param>
        /// <param name="Quarter"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.WindStat> GetWindStatInfo(string Hemisphere, string Quadrilateral, string Altitude, string Quarter)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.WindStat>>(() =>
            {
                ReturnValue<Data.MasterCatalog.WindStat> objMain = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Hemisphere, Quadrilateral, Altitude, Quarter))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.WindStatManager windStatManager = new FlightPak.Business.MasterCatalog.WindStatManager();
                        objMain = windStatManager.GetWindStatInfo(Hemisphere, Quadrilateral, Altitude, Quarter);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        # region "FleetInformationManager"

        public ReturnValue<Data.MasterCatalog.FleetByTailNumberResult> GetFleetByTailNumber(string tailNumber)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<Data.MasterCatalog.FleetByTailNumberResult>>(() =>
            {
                ReturnValue<Data.MasterCatalog.FleetByTailNumberResult> fleet = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tailNumber))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetInformation fleetInformationManager = new FlightPak.Business.MasterCatalog.FleetInformation();
                        fleet = fleetInformationManager.GetFleetByTailNumber(tailNumber);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return fleet;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        # endregion
        #region "Crew, Fleet, Passenger, Department, FlightCatagory, FlightPurpose, FuelLocator for Client"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters> GetFuelLocatorWithFilters(long FuelLocatorId, string FuelLocatorCD, long ClientId, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelLocatorManager FuelLocatorManager = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        ReturnValue = FuelLocatorManager.GetFuelLocatorWithFilters(FuelLocatorId, FuelLocatorCD, ClientId, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters> GetAllFlightPurposesWithFilters(long ClientID, long FlightPurposeID, string FlightPurposeCD, bool FetchDefaultPurposeOnly, bool FetchPersonalTravelOnly, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightPurposeManager FlightPurposeManager = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        ReturnValue = FlightPurposeManager.GetAllFlightPurposesWithFilters(ClientID, FlightPurposeID, FlightPurposeCD, FetchDefaultPurposeOnly, FetchPersonalTravelOnly, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCrewForClient> GetCrewForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCrewForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCrewForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CrewManager CrewManager = new FlightPak.Business.MasterCatalog.CrewManager();
                        ReturnValue = CrewManager.GetCrewForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFleetForClient> GetFleetForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFleetForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFleetForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FleetManager FleetManager = new FlightPak.Business.MasterCatalog.FleetManager();
                        ReturnValue = FleetManager.GetFleetForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerForClient> GetPassengerForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetPassengerForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.PassengerRequestorManager PassengerRequestorManager = new FlightPak.Business.MasterCatalog.PassengerRequestorManager();
                        ReturnValue = PassengerRequestorManager.GetPassengerForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentForClient> GetDepartmentForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetDepartmentForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.DepartmentManager DepartmentManager = new FlightPak.Business.MasterCatalog.DepartmentManager();
                        ReturnValue = DepartmentManager.GetDepartmentForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient> GetFlightCategoryForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightCategoryManager FlightCategoryManager = new FlightPak.Business.MasterCatalog.FlightCategoryManager();
                        ReturnValue = FlightCategoryManager.GetFlightCategoryForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient> GetFlightPurposeForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightPurposeManager FlightPurposeManager = new FlightPak.Business.MasterCatalog.FlightPurposeManager();
                        ReturnValue = FlightPurposeManager.GetFlightPurposeForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient> GetFuelLocatorForClient()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelLocatorManager FuelLocatorManager = new FlightPak.Business.MasterCatalog.FuelLocatorManager();
                        ReturnValue = FuelLocatorManager.GetFuelLocatorForClient();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "APIS"
        public ReturnValue<List<string>> CompanyEAPISSubmit(string SenderID, string Password)
        {
            ReturnValue<List<string>> objMain = new ReturnValue<List<string>>();
            FlightPak.Business.MasterCatalog.CompanyManager CompanyMngr = null;

            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<List<string>>>(() =>
            {
                //objMain = new ReturnValue<List<string>>();
                CompanyMngr = new FlightPak.Business.MasterCatalog.CompanyManager();
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        //string InterfaceRequesterDomain = System.Configuration.ConfigurationManager.AppSettings["InterfaceRequesterDomain"];
                        string UserName = UserPrincipal.Identity.Name;
                        string InterfaceRequesterDomain = ConfigurationManager.AppSettings["InterfaceRequesterDomain"];
                        objMain = CompanyMngr.UpdateCBPCredentials(UserName, InterfaceRequesterDomain, SenderID, Password);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objMain;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
        #region "TripSheetReportHeader"
        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> AddTripSheetReportHeader(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oTripSheetReportHeader.CustomerID = UserPrincipal.Identity.CustomerID;
                        oTripSheetReportHeader.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        retValue = objTripSheetReportHeader.Add(oTripSheetReportHeader);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> UpdateTripSheetReportHeader(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oTripSheetReportHeader.CustomerID = UserPrincipal.Identity.CustomerID;
                        oTripSheetReportHeader.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        retValue = objTripSheetReportHeader.Update(oTripSheetReportHeader);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> DeleteTripSheetReportHeader(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportHeader> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oTripSheetReportHeader.CustomerID = UserPrincipal.Identity.CustomerID;
                        oTripSheetReportHeader.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        retValue = objTripSheetReportHeader.Delete(oTripSheetReportHeader);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetUserGroup> GetUserGroup()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetUserGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetUserGroup> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager TripManager = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        ReturnValue = TripManager.GetUserGroup();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail> GetTripSheetReportDetail()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager TripManager = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        ReturnValue = TripManager.GetTripSheetReportDetail();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Service Method to get ReportSheetDetail by UMPermission
        /// </summary>
        /// <param name="TripSheetReportHeaderID"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission> GetReportSheetDetailByUMPermission(Int64 TripSheetReportHeaderID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager TripManager = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        ReturnValue = TripManager.GetReportSheetDetailByUMPermission(TripSheetReportHeaderID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        /// <summary>
        /// Service Method to get ReportSheetDetail
        /// </summary>
        /// <param name="TripSheetReportHeaderID"></param>
        /// <returns></returns>
        public Business.Common.ReturnValue<Data.MasterCatalog.GetReportSheetDetail> GetReportSheetDetail(Int64 TripSheetReportHeaderID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetReportSheetDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetReportSheetDetail> ReturnValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager TripManager = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        ReturnValue = TripManager.GetReportSheetDetail(TripSheetReportHeaderID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return ReturnValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }


        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> UpdateReportSheetDetail(Int64 ReportSheetHeaderID, List<int> ReportNumber)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ReportSheetHeaderID, ReportNumber))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        objTripSheetReportHeader.UpdateTripSheetDetail(ReportSheetHeaderID, ReportNumber);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> AddTripSheetReportDetail(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportDetail))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oTripSheetReportDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oTripSheetReportDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        retValue = objTripSheetReportHeader.Add(oTripSheetReportDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> UpdateTripSheetReportDetail(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportDetail))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oTripSheetReportDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oTripSheetReportDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        retValue = objTripSheetReportHeader.Update(oTripSheetReportDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> DeleteTripSheetReportDetail(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.TripSheetReportDetail> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportDetail))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oTripSheetReportDetail.CustomerID = UserPrincipal.Identity.CustomerID;
                        oTripSheetReportDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager objTripSheetReportHeader = new FlightPak.Business.MasterCatalog.TripsheetReportHeaderManager();
                        retValue = objTripSheetReportHeader.Delete(oTripSheetReportDetail);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Sales Person Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> AddSalesPerson(Data.MasterCatalog.SalesPerson salesPerson)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        salesPerson.CustomerID = UserPrincipal.Identity.CustomerID;
                        salesPerson.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.SalesPersonManager objSalesPerson = new FlightPak.Business.MasterCatalog.SalesPersonManager();
                        retValue = objSalesPerson.Add(salesPerson);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> UpdateSalesPerson(Data.MasterCatalog.SalesPerson salesPerson)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> retValue = null;
                salesPerson.CustomerID = UserPrincipal.Identity.CustomerID;
                salesPerson.LastUpdUID = UserPrincipal.Identity.Name;
                FlightPak.Business.MasterCatalog.SalesPersonManager objSalesPerson = new FlightPak.Business.MasterCatalog.SalesPersonManager();
                retValue = objSalesPerson.Update(salesPerson);
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> DeleteSalesPerson(Data.MasterCatalog.SalesPerson salesPerson)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        salesPerson.CustomerID = UserPrincipal.Identity.CustomerID;
                        salesPerson.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.SalesPersonManager objSalesPerson = new FlightPak.Business.MasterCatalog.SalesPersonManager();
                        retValue = objSalesPerson.Delete(salesPerson);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> GetSalesPersonLock(Data.MasterCatalog.SalesPerson salesPerson)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.SalesPersonManager objSalesType = new FlightPak.Business.MasterCatalog.SalesPersonManager();
                        retValue = objSalesType.GetLock(salesPerson);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        //public Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson> GetSalesPersonList()
        //{
        //    //Handle methods throguh exception manager with return flag
        //    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //    return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.SalesPerson>>(() =>
        //    {
        //        Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPerson> retValue = null;
        //        using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //        {
        //            //Handle methods throguh exception manager with return flag
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //            exManager.Process(() =>
        //            {
        //                FlightPak.Business.MasterCatalog.SalesPersonManager objSalesPerson = new FlightPak.Business.MasterCatalog.SalesPersonManager();
        //                retValue = objSalesPerson.GetList();
        //            }, FlightPak.Common.Constants.Policy.BusinessLayer);
        //        }
        //        return retValue;
        //    }, FlightPak.Common.Constants.Policy.ServiceLayer);
        //}
        public Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPerson> GetSalesPersonList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPerson>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPerson> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.SalesPersonManager objSalesPerson = new FlightPak.Business.MasterCatalog.SalesPersonManager();
                        retValue = objSalesPerson.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPersonID> GetSalesPersonID()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPersonID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetSalesPersonID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.SalesPersonManager objSalesPerson = new FlightPak.Business.MasterCatalog.SalesPersonManager();
                        retValue = objSalesPerson.GetSalesPersonID();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Lead Source Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> AddLeadSource(Data.MasterCatalog.LeadSource leadSource)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.LeadSource>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leadSource))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        leadSource.CustomerID = UserPrincipal.Identity.CustomerID;
                        leadSource.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.LeadSourceManager objLeadSource = new FlightPak.Business.MasterCatalog.LeadSourceManager();
                        retValue = objLeadSource.Add(leadSource);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> UpdateLeadSource(Data.MasterCatalog.LeadSource leadSource)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.LeadSource>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> retValue = null;
                leadSource.CustomerID = UserPrincipal.Identity.CustomerID;
                leadSource.LastUpdUID = UserPrincipal.Identity.Name;
                FlightPak.Business.MasterCatalog.LeadSourceManager objLeadSource = new FlightPak.Business.MasterCatalog.LeadSourceManager();
                retValue = objLeadSource.Update(leadSource);
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leadSource))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> DeleteLeadSource(Data.MasterCatalog.LeadSource leadSource)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.LeadSource>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leadSource))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        leadSource.CustomerID = UserPrincipal.Identity.CustomerID;
                        leadSource.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.LeadSourceManager objLeadSource = new FlightPak.Business.MasterCatalog.LeadSourceManager();
                        retValue = objLeadSource.Delete(leadSource);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> GetLeadSourceLock(Data.MasterCatalog.LeadSource leadSource)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.LeadSource>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leadSource))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.LeadSourceManager objLeadSource = new FlightPak.Business.MasterCatalog.LeadSourceManager();
                        retValue = objLeadSource.GetLock(leadSource);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> GetLeadSourceList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.LeadSource>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.LeadSource> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.LeadSourceManager objLeadSource = new FlightPak.Business.MasterCatalog.LeadSourceManager();
                        retValue = objLeadSource.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "CQCustomer Manager"
        #region For Performance Optimization
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters> GetCQCustomerWithFilters(long CQCustomerID, string CQCustomerCD, string FetchHomebaseOnly, bool FetchActiveOnly)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetCQCustomerWithFilters(CQCustomerID, CQCustomerCD, FetchHomebaseOnly, FetchActiveOnly);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);

                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        #endregion
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> GetCQCustomerLock(Data.MasterCatalog.CQCustomer cqCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqCustomer))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetLock(cqCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> GetCQCustomerList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact> GetCQCustomerListInfo()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetListInfo();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> AddCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomer))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQCustomer.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQCustomer.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.Add(CQCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> UpdateCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomer))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQCustomer.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQCustomer.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.Update(CQCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> DeleteCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomer> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomer))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQCustomer.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQCustomer.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.Delete(CQCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomer> GetCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomer>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomer> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetCQCustomer(CQCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD> GetCQCustomerByCQCustomerCD(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetCQCustomerByCQCustomerCD(CQCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID> GetCQCustomerByCQCustomerID(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerManager objCqCustomer = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                        retValue = objCqCustomer.GetCQCustomerByCQCustomerID(CQCustomer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "CQCustomerContact Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> GetCQCustomerContactLock(Data.MasterCatalog.CQCustomerContact cqCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqCustomerContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.GetLock(cqCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> GetCQCustomerContactList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> AddCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQCustomerContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQCustomerContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.Add(CQCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> UpdateCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQCustomerContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQCustomerContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.Update(CQCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> DeleteCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQCustomerContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        CQCustomerContact.CustomerID = UserPrincipal.Identity.CustomerID;
                        CQCustomerContact.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.Delete(CQCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContact> GetCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContact>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContact> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.GetCQCustomerContact(CQCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID> GetCQCustomerContactByCQCustomerContactID(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.GetCQCustomerContactByCQCustomerContactID(CQCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID> GetCQCustomerContactByCQCustomerID(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.CQCustomerContactManager objCQCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();
                        retValue = objCQCustomerContact.GetCQCustomerContactByCQCustomerID(CQCustomerContact);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fee Group Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> AddFeeGroup(Data.MasterCatalog.FeeGroup FeeGroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FeeGroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FeeGroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        FeeGroup.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FeeGroupManager objFeeGroup = new FlightPak.Business.MasterCatalog.FeeGroupManager();
                        retValue = objFeeGroup.Add(FeeGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> UpdateFeeGroup(Data.MasterCatalog.FeeGroup FeeGroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> retValue = null;
                FeeGroup.CustomerID = UserPrincipal.Identity.CustomerID;
                FeeGroup.LastUpdUID = UserPrincipal.Identity.Name;
                FlightPak.Business.MasterCatalog.FeeGroupManager objFeeGroup = new FlightPak.Business.MasterCatalog.FeeGroupManager();
                retValue = objFeeGroup.Update(FeeGroup);
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FeeGroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> DeleteFeeGroup(Data.MasterCatalog.FeeGroup FeeGroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FeeGroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FeeGroup.CustomerID = UserPrincipal.Identity.CustomerID;
                        FeeGroup.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FeeGroupManager objFeeGroup = new FlightPak.Business.MasterCatalog.FeeGroupManager();
                        retValue = objFeeGroup.Delete(FeeGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> GetFeeGroupLock(Data.MasterCatalog.FeeGroup FeeGroup)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FeeGroup))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FeeGroupManager objFeeGroup = new FlightPak.Business.MasterCatalog.FeeGroupManager();
                        retValue = objFeeGroup.GetLock(FeeGroup);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> GetFeeGroupList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeGroup> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FeeGroupManager objFeeGroup = new FlightPak.Business.MasterCatalog.FeeGroupManager();
                        retValue = objFeeGroup.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Lost Business Manager"
        public Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> AddLostBusiness(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lostBusiness))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lostBusiness.CustomerID = UserPrincipal.Identity.CustomerID;
                        lostBusiness.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.LostBusinessManager objLostBusiness = new FlightPak.Business.MasterCatalog.LostBusinessManager();
                        retValue = objLostBusiness.Add(lostBusiness);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> UpdateLostBusiness(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> retValue = null;
                lostBusiness.CustomerID = UserPrincipal.Identity.CustomerID;
                lostBusiness.LastUpdUID = UserPrincipal.Identity.Name;
                FlightPak.Business.MasterCatalog.LostBusinessManager objLostBusiness = new FlightPak.Business.MasterCatalog.LostBusinessManager();
                retValue = objLostBusiness.Update(lostBusiness);
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lostBusiness))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> DeleteLostBusiness(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lostBusiness))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        lostBusiness.CustomerID = UserPrincipal.Identity.CustomerID;
                        lostBusiness.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.LostBusinessManager objLostBusiness = new FlightPak.Business.MasterCatalog.LostBusinessManager();
                        retValue = objLostBusiness.Delete(lostBusiness);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> GetLostBusinessLock(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lostBusiness))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.LostBusinessManager objLostBusiness = new FlightPak.Business.MasterCatalog.LostBusinessManager();
                        retValue = objLostBusiness.GetLock(lostBusiness);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> GetLostBusinessList()
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.CQLostBusiness> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.LostBusinessManager objLostBusiness = new FlightPak.Business.MasterCatalog.LostBusinessManager();
                        retValue = objLostBusiness.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fee Schedule"
        public Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> AddFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFeeSchedule.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFeeSchedule.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FeeScheduleManager objManager = new FlightPak.Business.MasterCatalog.FeeScheduleManager();
                        retValue = objManager.Add(oFeeSchedule);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> UpdateFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFeeSchedule.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFeeSchedule.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FeeScheduleManager objManager = new FlightPak.Business.MasterCatalog.FeeScheduleManager();
                        retValue = objManager.Update(oFeeSchedule);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> DeleteFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFeeSchedule.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFeeSchedule.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FeeScheduleManager objManager = new FlightPak.Business.MasterCatalog.FeeScheduleManager();
                        retValue = objManager.Delete(oFeeSchedule);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> GetFeeScheduleList(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FeeSchedule> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FeeScheduleManager objManager = new FlightPak.Business.MasterCatalog.FeeScheduleManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetFeeSchedule> GetFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetFeeSchedule>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetFeeSchedule> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FeeScheduleManager objManager = new FlightPak.Business.MasterCatalog.FeeScheduleManager();
                        retValue = objManager.GetFeeSchedule(oFeeSchedule);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID> GetFeeScheduleByGroupID(Int64 feeGroupID)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(feeGroupID))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FeeScheduleManager objManager = new FlightPak.Business.MasterCatalog.FeeScheduleManager();
                        retValue = objManager.GetFeeScheduleByGroupID(feeGroupID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion "Fee Schedule"

        #region "FuelFieldMaster"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> AddFuelFieldMaster(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldMaster))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFieldMaster.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelFieldMasterManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldMasterManager();
                        retValue = objManager.Add(oFuelFieldMaster);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> UpdateFuelFieldMaster(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldMaster))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFieldMaster.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelFieldMasterManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldMasterManager();
                        retValue = objManager.Update(oFuelFieldMaster);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> DeleteFuelFieldMaster(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldMaster))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFieldMaster.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelFieldMasterManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldMasterManager();
                        retValue = objManager.Delete(oFuelFieldMaster);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> GetFuelFieldMasterList(Data.MasterCatalog.FuelFieldMaster oFuelFieldMaster)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldMaster> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldMaster))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFieldMasterManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldMasterManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "FuelSavedFileFormat"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> AddFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelSavedFileFormat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelSavedFileFormat.LastUpdUID = UserPrincipal.Identity.Name;
                        oFuelSavedFileFormat.LastUpdTS = DateTime.Now;
                        oFuelSavedFileFormat.IsDeleted = false;
                        oFuelSavedFileFormat.IsInActive = false;
                        oFuelSavedFileFormat.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.Add(oFuelSavedFileFormat);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> UpdateFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelSavedFileFormat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelSavedFileFormat.LastUpdUID = UserPrincipal.Identity.Name;
                        oFuelSavedFileFormat.LastUpdTS = DateTime.Now;
                        oFuelSavedFileFormat.IsDeleted = false;
                        oFuelSavedFileFormat.IsInActive = false;
                        oFuelSavedFileFormat.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.Update(oFuelSavedFileFormat);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> DeleteFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelSavedFileFormat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelSavedFileFormat.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.Delete(oFuelSavedFileFormat);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormat(Data.MasterCatalog.FuelSavedFileFormat oFuelSavedFileFormat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelSavedFileFormat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetPreviousSavedFormatForVendor(FuelFileData objFuelFileData, List<string> csvColumnsTitle)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFileData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.IsLastSavedFileFormatSameAsUploadedNewFile(csvColumnsTitle,objFuelFileData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormatebasedOnFuelVendor(long fuelVendorId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendorId))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long customerId = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.GetFuelSavedFileFormatebasedOnFuelVendor(fuelVendorId, customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormatebasedOnFuelVendorCD(string fuelVendorCD)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendorCD))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long customerId = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.GetFuelSavedFileFormatebasedOnFuelVendorCD(fuelVendorCD, customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> MatcheCSVFormatWithErrorLogForamt(FuelFileData objFuelFileData, List<string> csvColumnsTitle)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFileData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager objManager = new FlightPak.Business.MasterCatalog.FuelSavedFileFormatManager();
                        retValue = objManager.MatcheCSVFormatWithErrorLogForamt(csvColumnsTitle, objFuelFileData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        

        #endregion

        #region "FuelFieldSynonyms"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> AddFuelFieldSynonym(Data.MasterCatalog.FuelFieldSynonym oFuelFieldSynonym)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldSynonym))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFieldSynonym.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelFieldSynonymManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldSynonymManager();
                        retValue = objManager.Add(oFuelFieldSynonym);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> UpdateFuelFieldSynonym(Data.MasterCatalog.FuelFieldSynonym oFuelFieldSynonym)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldSynonym))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFieldSynonym.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelFieldSynonymManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldSynonymManager();
                        retValue = objManager.Update(oFuelFieldSynonym);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> DeleteFuelFieldSynonym(Data.MasterCatalog.FuelFieldSynonym oFuelFieldSynonym)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldSynonym))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFieldSynonym.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelFieldSynonymManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldSynonymManager();
                        retValue = objManager.Delete(oFuelFieldSynonym);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> GetFuelFieldSynonymList(Data.MasterCatalog.FuelFieldSynonym oFuelFieldSynonym)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFieldSynonym> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFieldSynonym))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFieldSynonymManager objManager = new FlightPak.Business.MasterCatalog.FuelFieldSynonymManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "FueldCsvFileFields"
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> AddFuelCsvFileFields(Data.MasterCatalog.FuelCsvFileFields oFuelCsvFileFields)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelCsvFileFields))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelCsvFileFields.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager objManager = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        retValue = objManager.Add(oFuelCsvFileFields);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> UpdateFuelCsvFileFields(Data.MasterCatalog.FuelCsvFileFields oFuelCsvFileFields)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelCsvFileFields))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelCsvFileFields.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager objManager = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        retValue = objManager.Update(oFuelCsvFileFields);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> DeleteFuelCsvFileFields(Data.MasterCatalog.FuelCsvFileFields oFuelCsvFileFields)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelCsvFileFields))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelCsvFileFields.LastUpdUID = UserPrincipal.Identity.Name;
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager objManager = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        retValue = objManager.Delete(oFuelCsvFileFields);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> GetFuelCsvFileFieldsList(Data.MasterCatalog.FuelCsvFileFields oFuelCsvFileFields)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelCsvFileFields))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager objManager = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

        #region "Fuel Records Bulk Insert"
        public Business.Common.ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> AddBulkFlightpakFuel(FuelFileData oFuelFileData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<List<Data.MasterCatalog.FlightpakFuel>>>(() =>
            {
                Business.Common.ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.AddBulk(oFuelFileData, UserPrincipal.Identity.Name, UserPrincipal.Identity.CustomerID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fuel File Filter"

        public Business.Common.ReturnValue<Dictionary<string, string>> CheckFileFormat(String fileTitlesCSVformat)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Dictionary<string, string>>>(() =>
            {
                Business.Common.ReturnValue<Dictionary<string, string>> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileTitlesCSVformat))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.CheckFileFormat(fileTitlesCSVformat);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<FuelFileData> CheckFileDataByDataformat(FuelFileData objFuelFileData)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<FuelFileData>>(() =>
            {
                Business.Common.ReturnValue<FuelFileData> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFileData))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.CheckFileDataByDataformat(objFuelFileData);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Fuel File XSLT Parser"

        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> AddFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFileXsltParser.LastUpdUID = UserPrincipal.Identity.Name;
                        oFuelFileXsltParser.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFuelFileXsltParser.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.Add(oFuelFileXsltParser);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> UpdateFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFileXsltParser.LastUpdUID = UserPrincipal.Identity.Name;
                        oFuelFileXsltParser.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFuelFileXsltParser.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.Update(oFuelFileXsltParser);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> DeleteFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFuelFileXsltParser.LastUpdUID = UserPrincipal.Identity.Name;
                        oFuelFileXsltParser.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFuelFileXsltParser.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.Delete(oFuelFileXsltParser);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetFuelFileXsltParser(Data.MasterCatalog.FuelFileXsltParser oFuelFileXsltParser)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileXsltParser))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager objManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        retValue = objManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetAllXsltParser()
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> objFuelFileXsltParser = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager fuelfilexsltparserManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        objFuelFileXsltParser = fuelfilexsltparserManager.GetList();
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFuelFileXsltParser;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>  AddFuelFileCsvHeaders(string[] headers, int xsltParserId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(headers))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        List<Data.MasterCatalog.FuelCsvFileFields> csvFields=new List<FuelCsvFileFields>();
                        foreach (var item in headers)
                        {
                            Data.MasterCatalog.FuelCsvFileFields newField = new Data.MasterCatalog.FuelCsvFileFields();
                            newField.FieldTitle = item;
                            newField.FuelFileXsltParserID = xsltParserId;
                            newField.IsInActive = true;
                            newField.IsDeleted = false;
                            newField.LastUpdTS = DateTime.Now;
                            newField.LastUpdUID = UserPrincipal.Identity.Name;
                            csvFields.Add(newField);
                        }
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager objManager = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        retValue = objManager.AddBulkCSVFields(csvFields);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetXsltParserById(int xsltParserId)
        {
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelFileXsltParser> objFuelFileXsltParser = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelFileXsltParserManager fuelfilexsltparserManager = new FlightPak.Business.MasterCatalog.FuelFileXsltParserManager();
                        objFuelFileXsltParser = fuelfilexsltparserManager.GetXsltParserByID(xsltParserId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFuelFileXsltParser;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> DeleteOldCsvFieldsBasedOnXsltParserId(int xsltParserId)
        {
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FuelCsvFileFields> objFuelCsvFileFields = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager oFuelCsvFileFieldsManage = new FlightPak.Business.MasterCatalog.FuelCsvFileFieldsManager();
                        objFuelCsvFileFields = oFuelCsvFileFieldsManage.DeleteOldCsvFieldsBasedOnXsltParserId(xsltParserId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return objFuelCsvFileFields;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion

        #region "Flightpak Fuel"

        public Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData> GetFlightpakFuelListByFilter(long? vendorId, string fboName, long? airportIcaoId, DateTime? effectiveDate, bool? isActive, int pageSize, int pageIndex)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorId, fboName, airportIcaoId, effectiveDate, isActive))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long customerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager oFlightpakFuelManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = oFlightpakFuelManager.GetFlightpakFuelListByFilter(customerID, vendorId, fboName, airportIcaoId, effectiveDate, isActive, pageSize, pageIndex);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> GetFlightpakFuelListById(long flightpakFuelId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightpakFuelId))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long customerId = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager oFlightpakFuelManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = oFlightpakFuelManager.GetFlightpakFuelListById(flightpakFuelId, customerId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<int> CountNumberOfFlightpakFuelRecordsForVendor(long fuelVendorId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<int>>(() =>
            {
                Business.Common.ReturnValue<int> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelVendorId))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        long customerId = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager oFlightpakFuelManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = oFlightpakFuelManager.CountNumberOfFlightpakFuelRecordsForVendor(fuelVendorId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> AddFlightpakFuel(Data.MasterCatalog.FlightpakFuel oFlightpakFuel)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFlightpakFuel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFlightpakFuel.LastUpdUID = UserPrincipal.Identity.Name;
                        oFlightpakFuel.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFlightpakFuel.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.Add(oFlightpakFuel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> UpdateFlightpakFuel(Data.MasterCatalog.FlightpakFuel oFlightpakFuel)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFlightpakFuel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFlightpakFuel.LastUpdUID = UserPrincipal.Identity.Name;
                        oFlightpakFuel.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFlightpakFuel.UpdateDTTM = DateTime.Now;
                        oFlightpakFuel.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.Update(oFlightpakFuel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> DeleteFlightpakFuel(Data.MasterCatalog.FlightpakFuel FlightpakFuel)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.FlightpakFuel> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FlightpakFuel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        FlightpakFuel.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FuelVendorManager objFuelVendor = new FlightPak.Business.MasterCatalog.FuelVendorManager();
                        retValue = objFuelVendor.DeleteFlightpakFuel(FlightpakFuel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> UpdateToHistoricalFlightpakFuel(Data.MasterCatalog.HistoricalFuelPrice oFlightpakFuel)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFlightpakFuel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFlightpakFuel.LastUpdUID = UserPrincipal.Identity.Name;
                        oFlightpakFuel.CustomerID = UserPrincipal.Identity.CustomerID;
                        oFlightpakFuel.LastUpdTS = DateTime.Now;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objManager = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objManager.UpdateToHistoricalFlightpakFuel(oFlightpakFuel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> DeleteToHistoricalFlightpakFuel(Data.MasterCatalog.HistoricalFuelPrice oFlightpakFuel)
        {
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice>>(() =>
            {
                Business.Common.ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> retValue = null;
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFlightpakFuel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        oFlightpakFuel.CustomerID = UserPrincipal.Identity.CustomerID;
                        FlightPak.Business.MasterCatalog.FlightpakFuelManager objFuelVendor = new FlightPak.Business.MasterCatalog.FlightpakFuelManager();
                        retValue = objFuelVendor.DeleteToHistoricalFlightpakFuel(oFlightpakFuel);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);


                }
                return retValue;
            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #endregion

    }
}

