﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace FlightPak.Services.Utility
{
    [DataContract()]
    public class FilterCriteria
    {
          [DataMember()]
        public TimeBase TimeBase { get; set; }

          [DataMember()]
        public string ClientID { get; set; }

          [DataMember()]
          public string Client { get; set; }

          [DataMember()]
          public string RequestorID { get; set; }

          [DataMember()]
        public string Requestor { get; set; }

          [DataMember()]
          public string DepartmentID { get; set; }

          [DataMember()]
         public string Department { get; set; }

          [DataMember()]
          public string FlightCategoryID { get; set; }

          [DataMember()]
        public string FlightCategory { get; set; }

          [DataMember()]
        public string CrewDutyID { get; set; }

          [DataMember()]
          public string CrewDuty { get; set; }

          [DataMember()]
        public string HomeBase { get; set; }

          [DataMember()]
          public string HomeBaseID { get; set; }

          [DataMember()]
        public bool Trip { get; set; }

          [DataMember()]
        public bool WorkSheet { get; set; }
          
        [DataMember()]
        public bool SchedServ { get; set; }

          [DataMember()]
        public bool Canceled { get; set; }
          [DataMember()]
        public bool UnFulfilled { get; set; }

          [DataMember()]
        public bool Hold { get; set; }

          [DataMember()]
        public bool AllCrew { get; set; }

          [DataMember()]
        public bool HomeBaseOnly { get; set; }

          [DataMember()]
        public bool FixedWingCrewOnly { get; set; }

          [DataMember()]
        public bool RotaryWingCrewCrewOnly { get; set; }
          [DataMember()]
        public VendorOption VendorsFilter { get; set; }

    }

    /// <summary>
    /// Time base option
    /// </summary>
    [Serializable]
    public enum TimeBase
    {
        None,
        Local,
        UTC,
        Home
    }

    /// <summary>
    /// Vendor selection option
    /// </summary>
    [Serializable]
    public enum VendorOption
    {
        None,
        IncludeActiveVendors,
        OnlyShowActiveVendors,
        ExcludeActiveVendors
    }
}