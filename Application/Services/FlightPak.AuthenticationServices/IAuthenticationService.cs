﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using FlightPak.Business.Common;

namespace FlightPak.AuthenticationServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAuthenticationService" in both code and config file together.
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        string GetSaltValue(string UserName);
        [OperationContract]
        FlightPak.Framework.Security.FPPrincipal GetUserPrincipal(string SessionID);
        [OperationContract]
        bool Login(string UserName, string Password, Guid SessionId, Int32 InvalidLoginCount);
        [OperationContract]
        string GetUserNameByEmail(string email, string securityQuestion, string securityAnswer);
        [OperationContract]
        void Logout(string SessionId);
        [OperationContract]
        ReturnValue<FlightPak.Framework.Data.UserSecurityHint> AddOrUpdateSecurityHint(FlightPak.Framework.Data.UserSecurityHint userSecurtiyHint);
        [OperationContract]
        ReturnValue<FlightPak.Framework.Data.UserSecurityHint> GetUserSecurityHintByUserName(string userName);
        [OperationContract]
        ReturnValue<FlightPak.Framework.Data.UserSecurityHint> GetUserSecurityHintByEmailId(string EmailId);
        [OperationContract]
        bool CheckAuthenticationByEmailId(string EmailId, string SessionId);
        [OperationContract]
        void LogOutByEmailId(string EmailId, string SessionId);
        //[OperationContract]
        //bool ChangePassword(string UserName, System.Security.SecureString OldPassword, System.Security.SecureString NewPassword);

    }
}
