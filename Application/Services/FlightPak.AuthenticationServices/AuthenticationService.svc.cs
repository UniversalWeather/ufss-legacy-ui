﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using FlightPak.Business.Common;
using FlightPak.Business.Security;

namespace FlightPak.AuthenticationServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AuthenticationService" in code, svc and config file together.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]  
    public class AuthenticationService : IAuthenticationService
    {
        private ExceptionManager exManager;
        public string GetSaltValue(string UserName)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<string>(() =>
                    {
                        FlightPak.Business.Security.LoginManager LoginManager = new Business.Security.LoginManager();
                        return LoginManager.GetSaltValue(UserName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public FlightPak.Framework.Security.FPPrincipal GetUserPrincipal(string SessionID)
        {

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<FlightPak.Framework.Security.FPPrincipal>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SessionID))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<FlightPak.Framework.Security.FPPrincipal>(() =>
                    {
                        FlightPak.Business.Security.LoginManager LoginManager = new Business.Security.LoginManager();
                        return LoginManager.GetUserPrincipal(SessionID);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public bool Login(string UserName, string Password, Guid SessionId, Int32 InvalidLoginCount)
        {

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName, Password, SessionId, InvalidLoginCount))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<bool>(() =>
                    {
                        FlightPak.Business.Security.LoginManager LoginManager = new Business.Security.LoginManager();
                        return LoginManager.Login(UserName, Password, SessionId, InvalidLoginCount);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public string GetUserNameByEmail(string email, string securityQuestion, string securityAnswer)
        {

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<string>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(email, securityAnswer, securityQuestion))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<string>(() =>
                    {
                        FlightPak.Business.Security.SecurityManager userManager = new Business.Security.SecurityManager();
                        return userManager.GetUserNameByEmail(email, securityQuestion, securityAnswer);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }
        //public bool ChangePassword(string UserName, System.Security.SecureString OldPassword, System.Security.SecureString NewPassword)
        //{
        //    throw new NotImplementedException();
        //}

        public void Logout(string SessionId)
        {

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SessionId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Security.LoginManager LoginManager = new Business.Security.LoginManager();
                        LoginManager.LogOut(SessionId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public void LogOutByEmailId(string EmailId, string SessionId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EmailId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        FlightPak.Business.Security.LoginManager LoginManager = new Business.Security.LoginManager();
                        LoginManager.LogOutByEmailId(EmailId, SessionId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);

        }

        public bool CheckAuthenticationByEmailId(string EmailId, string SessionId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<bool>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EmailId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<bool>(() =>
                    {
                        FlightPak.Business.Security.LoginManager loginManager = new Business.Security.LoginManager();
                        return loginManager.CheckAuthenticationByEmailId(EmailId, SessionId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        #region User Security Manager
        public ReturnValue<FlightPak.Framework.Data.UserSecurityHint> AddOrUpdateSecurityHint(FlightPak.Framework.Data.UserSecurityHint userSecurtiyHint)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FlightPak.Framework.Data.UserSecurityHint>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userSecurtiyHint))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<ReturnValue<FlightPak.Framework.Data.UserSecurityHint>>(() =>
                    {
                        SecurityHintManager securityHintManager = new SecurityHintManager();
                        return securityHintManager.AddOrUpdate(userSecurtiyHint);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        public ReturnValue<FlightPak.Framework.Data.UserSecurityHint> GetUserSecurityHintByUserName(string userName)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FlightPak.Framework.Data.UserSecurityHint>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<ReturnValue<FlightPak.Framework.Data.UserSecurityHint>>(() =>
                    {
                        SecurityHintManager securityHintManager = new SecurityHintManager();
                        return securityHintManager.GetUserSecurityHintByUserName(userName);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }

        public ReturnValue<FlightPak.Framework.Data.UserSecurityHint> GetUserSecurityHintByEmailId(string EmailId)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            return exManager.Process<ReturnValue<FlightPak.Framework.Data.UserSecurityHint>>(() =>
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EmailId))
                {
                    //Handle methods through exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<ReturnValue<FlightPak.Framework.Data.UserSecurityHint>>(() =>
                    {
                        SecurityHintManager securityHintManager = new SecurityHintManager();
                        return securityHintManager.GetUserSecurityHintByEmailId(EmailId);
                    }, FlightPak.Common.Constants.Policy.BusinessLayer);
                }

            }, FlightPak.Common.Constants.Policy.ServiceLayer);
        }
        #endregion
    }
}
