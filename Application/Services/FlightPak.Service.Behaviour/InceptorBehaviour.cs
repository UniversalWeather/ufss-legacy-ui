﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;
using System.ServiceModel.Dispatcher;

using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Service.Behaviour
{
    public class InceptorBehaviour : BehaviorExtensionElement, IServiceBehavior
    {
        private ExceptionManager exManager;
        public override Type BehaviorType
        {
            get { return typeof(InceptorBehaviour); }
        }

        protected override object CreateBehavior()
        {
            return new InceptorBehaviour();
        }

        public void AddBindingParameters(ServiceDescription serviceDescription,
               System.ServiceModel.ServiceHostBase serviceHostBase,
               System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints,
               System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyDispatchBehavior(ServiceDescription
               serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    foreach (ChannelDispatcher dispatcher in serviceHostBase.ChannelDispatchers)
                    {
                        foreach (var endpoint in dispatcher.Endpoints)
                        {
                            endpoint.DispatchRuntime.MessageInspectors.Add(new MessageInspector());
                        }
                    }
                }, FlightPak.Common.Constants.Policy.ServiceLayer);

            }

        }

        public void Validate(ServiceDescription serviceDescription,
               System.ServiceModel.ServiceHostBase serviceHostBase)
        {

        }
    }
}