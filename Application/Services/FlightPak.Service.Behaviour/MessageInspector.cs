﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using System.Net;
using System.IO;
using System.ServiceModel;
using System.Security.Principal;
using System.Security;

using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Service.Behaviour
{
    public class MessageInspector : IDispatchMessageInspector
    {
        private ExceptionManager exManager;
        public object AfterReceiveRequest(ref Message request,
                       System.ServiceModel.IClientChannel channel,
                       System.ServiceModel.InstanceContext instanceContext)
        {
            MessageHeaders headers = request.Headers;
            string message = request.ToString();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(request, channel, instanceContext))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<object>(() =>
                {

                    try
                    {

                        string sessionid = headers.GetHeader<string>("SessionIdKey", "FlightPak.Web");
                        bool isLogOutCall = message.ToLower().Contains("icommonservice/logout") && !message.ToLower().Contains("icommonservice/logoutallusers");
                        bool isMembershipCheck = message.ToLower().Contains("iadminservice/getuserbyemailid");

                        Guid _sessionID;
                        if (!Guid.TryParse(sessionid, out _sessionID))
                        {
                            throw new ArgumentException("Invalid Session");
                        }
                        // Vishwa
                        // Check the session id has valid record in Login table
                        // If the session id not valid throw new security exception.
                        FlightPak.Business.Security.SecurityManager secManager = new Business.Security.SecurityManager();
                        if (!secManager.CheckAuthentication(sessionid) && !isLogOutCall && !isMembershipCheck)
                            throw new SecurityException("Login session has expired or logged-in using other browser or other device.");

                        //// TODO Fetch FPPrincipal from Cache using Session ID
                        FlightPak.Framework.Security.FPPrincipal FPPrincipal = new Framework.Security.FPPrincipal(_sessionID);

                        System.Threading.Thread.CurrentPrincipal = FPPrincipal;

                        //Clear Cache Entry for MasterService
                        if (isLogOutCall) //ICommonService/LogOutAllUsers
                        {
                            FlightPak.Business.Security.LoginManager loginManager = new Business.Security.LoginManager();
                            loginManager.LogOut(sessionid);
                        }

                        return null;

                    }

                    catch (Exception)
                    {
                        //Manually Handled
                        return null;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {

        }
    }
}