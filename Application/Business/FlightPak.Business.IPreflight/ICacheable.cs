﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.IPreflight
{
    public interface ICacheable
    {
        void RefreshCache();
    }
}
