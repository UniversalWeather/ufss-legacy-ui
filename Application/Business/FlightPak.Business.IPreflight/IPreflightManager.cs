﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Data.Common;
using FlightPak.Data.Preflight;
using FlightPak.Business.Common;


namespace FlightPak.Business.IPreflight
{
    public interface IPreflightManager
    {
        #region "Trip Main"
        ReturnValue<PreflightMain> GetList();
        ReturnValue<PreflightMain> GetTrip(Int64 TripNumber);
        ReturnValue<PreflightMain> UpdateTrip(PreflightMain trip);
        ReturnValue<PreflightMain> DeleteTrip(Int64 TripNumber);

        #endregion

        #region "Trip Leg"
        ReturnValue<PreflightMain> AddPreflightLeg(PreflightLeg preflightLeg);
        ReturnValue<PreflightMain> UpdatePreflightLeg(PreflightLeg preflightLeg);
        ReturnValue<PreflightMain> DeletePreflightLeg(string LogNumber, string TripNumber, string LegID, string TripLegID);
        //Add more methods as require
        #endregion

        #region "Trip Crew"

        ReturnValue<PreflightMain> DeletePreflightCrew(string LogNumber, string TripNumber, string LegID, string TripLegID);
        ReturnValue<PreflightMain> UpdatePreflightCrew(PreflightCrewList preflightCrewlist);
        ReturnValue<PreflightMain> AddPreflightCrew(PreflightCrewList preflightCrewlist);
        #endregion

        #region "UWA Interface"       
        ReturnValue<PreflightMain> GetUWAL(Int64 TripID, bool isUWASubmit);        
        ReturnValue<PreflightMain> UpdateUWAL(PreflightMain prefMain);
        ReturnValue<List<string>> TSSSubmit(Int64 TripID, string UWATripID, bool isManualSubmit, string RequesterAppname);
        ReturnValue<List<string>> TSSCheckStatus(string UWATripID, string RequesterAppname);

        ReturnValue<PreflightMain> GetAPIS(Int64 TripID);
        ReturnValue<List<string>> EAPISSubmit(Int64 TripID, List<string> PrefLegLst, string Domainname);
        ReturnValue<PreflightFuel> GetFuelPrices(List<string> ICAOID, long AirportID, string userName, string password);

        void UpdateAPISException(Int64 TripID, string APISException);

        #endregion

        
    }
}
