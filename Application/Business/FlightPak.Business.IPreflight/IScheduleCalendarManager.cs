﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Business.Common;
using System.Collections.ObjectModel;

namespace FlightPak.Business.IPreflight
{
    public interface IScheduleCalendarManager
    {

        #region "Schedule Calendar"

        ReturnValue<Data.Preflight.CrewCalendarDataResult> GetFleetCalendarData(DateTime? startDate, DateTime? endDate, Collection<string> fleetTreeInput, Collection<string> crewTreeInput, bool isDetailMode);

        ReturnValue<Data.Preflight.CrewCalendarDataResult> GetCrewCalendarData(DateTime? startDate, DateTime? endDate, Collection<string> fleetTreeInput, Collection<string> crewTreeInput);

        ReturnValue<Data.Preflight.FleetResourceTypeResult> GetFleetResourceType(string vendorFilterInput, Collection<string> fleetIds);

        ReturnValue<Data.Preflight.CrewResourceTypeResult> GetCrewResourceType(Collection<string> crewIds);

        ReturnValue<Data.Preflight.LegendsResult> GetAllLegends();

        ReturnValue<Data.Preflight.FleetPlannerCalendarDataResult> GetFleetPlannerCalendarData(DateTime? startDate, short timeintervel, Collection<string> fleetTreeInput);

        ReturnValue<Data.Preflight.CrewPlannerCalendarDataResult> GetCrewPlannerCalendarData(DateTime? startDate, short timeintervel,  Collection<string> crewTreeInput);
        #endregion

        #region "Schedule Calendar Fleet Entries From Context Menu"
        /// <summary>
        /// Get the Existing Fleets Calender Entries
        /// </summary>
        /// <param name="timebase"></param>
        /// <returns></returns>
        ReturnValue<Data.Preflight.FleetCalendarEntriesResult> GetFleetCalendarEntries(string timebase, DateTime? startDate, DateTime? endDate);
        /// <summary>
        /// Add the Fleet Calendar Entries
        /// </summary>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        void AddFleetCalendarEntries(string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, Int64 clientid, Int64 fleetid);
        /// <summary>
        ///  Update Fleet Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        void UpdateFleetCalenderEntries(Int64 fleetid, string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, Int64 legid, Int64 tripid, Int64 clientid);
        /// <summary>
        /// Delete Fleet Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        void DeleteFleetCalendarEntries(Int64 fleetid, Int64 legid, Int64 tripid);

        #endregion

        #region "Schedule Calendar Crew Entries From Context Menu"
        /// <summary>
        /// Get the Existing Crew Calender Entries
        /// </summary>
        /// <param name="timebase"></param>
        /// <returns></returns>
        ReturnValue<Data.Preflight.CrewCalendarEntriesResult> GetCrewCalendarEntries(string timebase, DateTime? startDate, DateTime? endDate);
        /// <summary>
        /// Add Crew Calendar Entries
        /// </summary>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        void AddCrewCalendarEntries(string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, Int64 clientid, string crewids, Int64 fleetid);
        /// <summary>
        ///  Update Crew Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        void UpdateCrewCalenderEntries(Int64 fleetid, string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, string legid, string tripid, Int64 clientid, Int64 PreviousNum, string crewid);
        /// <summary>
        /// Delete Crew Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        void DeleteCrewCalendarEntries(string legid, string tripid, string crewids);

        #endregion
    }
}
