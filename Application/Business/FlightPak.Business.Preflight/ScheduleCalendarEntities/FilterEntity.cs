﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace FlightPak.Business.Preflight.ScheduleCalendarEntities
{
 
    public class FilterEntity
    {
        
        public TimeBase TimeBase { get; set; }

        public string ClientID { get; set; }
        public string Client { get; set; }

        public string RequestorID { get; set; }
        public string Requestor { get; set; }

         
        public string DepartmentID { get; set; }
        public string Department { get; set; }

           
        public string FlightCategoryID { get; set; }
        public string FlightCategory { get; set; }

           
        public string CrewDutyID { get; set; }
        public string CrewDuty { get; set; }


        public string HomeBaseID { get; set; }
        public string HomeBase { get; set; }

           
        public bool Trip { get; set; }

           
        public bool WorkSheet { get; set; }
          
         
        public bool SchedServ { get; set; }

           
        public bool Canceled { get; set; }
           
        public bool UnFulfilled { get; set; }

           
        public bool Hold { get; set; }

           
        public bool AllCrew { get; set; }

           
        public bool HomeBaseOnly { get; set; }

           
        public bool FixedWingCrewOnly { get; set; }

           
        public bool RotaryWingCrewCrewOnly { get; set; }
           
        public VendorOption VendorsFilter { get; set; }
        
    }

    /// <summary>
    /// Time base option
    /// </summary>
    [Serializable]
    public enum TimeBase
    {
        None,
        Local,
        UTC,
        Home
    }

    /// <summary>
    /// Vendor selection option
    /// </summary>
    [Serializable]
    public enum VendorOption
    {
        None,
        IncludeActiveVendors,
        OnlyShowActiveVendors,
        ExcludeActiveVendors
    }
}