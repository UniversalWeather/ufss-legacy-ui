﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using FlightPak.Data.Preflight;
using FlightPak.Business.Common;
using FlightPak.Business.IPreflight;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.ServiceAgents;
using System.Globalization;
using System.Reflection;
using System.Data.OleDb;
using FlightPak.Common.Tracing;
using FlightPak.Common.Extensions;
using FlightPak.Common;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Collections.ObjectModel;

namespace FlightPak.Business.Preflight
{
    public partial class PreflightManager : BaseManager, IPreflightManager
    {

        #region ExceptionHandling
        List<ExceptionTemplate> TripExceptions = new List<ExceptionTemplate>();
        List<PreflightTripException> PreflightExceptions = new List<PreflightTripException>();
        //List<FlightPak.Business.Common.RulesException> ErrorList = new List<FlightPak.Business.Common.RulesException>();
        #endregion

        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        FlightPak.Common.PreFlight commPreFlight = new FlightPak.Common.PreFlight();

        private ExceptionManager exManager;
        private ReturnValue<PreflightMain> getOldTrip = null;
        #region "Crew Details"
        public ReturnValue<Data.Preflight.CrewActivity> GetCrewActivity(long crewID, DateTime DepartDate, bool IsNonLogTripSheetIncludeCrewHIST, string CrewFH, long TripNUM)
        {
            ReturnValue<Data.Preflight.CrewActivity> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, crewID, DepartDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH, TripNUM))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.CrewActivity>();
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.CrewActivity(CustomerID, ClientId, crewID, DepartDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH, TripNUM).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Preflight.CrewActivityDateRange> GetCrewActivityDateRange(long crewID, DateTime FromDate, DateTime ToDate, bool IsNonLogTripSheetIncludeCrewHIST, string CrewFH, long TripNUM)
        {
            ReturnValue<Data.Preflight.CrewActivityDateRange> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, crewID, FromDate, ToDate, IsNonLogTripSheetIncludeCrewHIST, TripNUM))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.CrewActivityDateRange>();
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.CrewActivityDateRange(CustomerID, ClientId, crewID, FromDate, ToDate, IsNonLogTripSheetIncludeCrewHIST, CrewFH, TripNUM).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Preflight.GetAllCrewDetails> GetCrewList(DateTime DepartDate, DateTime ArriveDate, long CrewGroupID, long HomeBaseID, long FleetID, long ArrivalAirportID, long CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, long AircraftID)
        {

            ReturnValue<Data.Preflight.GetAllCrewDetails> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ClientId, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllCrewDetails>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCrewDetails(CustomerID, ClientId, DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID).ToList();
                        foreach (var item in ret.EntityList)
                        {
                            item.Passport = Crypting.Decrypt(item.Passport);
                        }
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        public ReturnValue<Data.Preflight.GetAllCrewDetails> GetAvailableCrewList(DateTime DepartDate, DateTime ArriveDate, long CrewGroupID, long HomeBaseID, long FleetID, long ArrivalAirportID, long CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, long AircraftID)
        {

            ReturnValue<Data.Preflight.GetAllCrewDetails> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ClientId, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllCrewDetails>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCrewDetails(CustomerID, ClientId, DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Preflight.GetAllCrewDetails> GetCrewListbyCrewIDORCrewCD(DateTime DepartDate, DateTime ArriveDate, long CrewGroupID, long HomeBaseID, long FleetID, long ArrivalAirportID, long CurrentTripID, DateTime MinDepartDate, DateTime MaxArriveDate, long AircraftID, long CrewID, string CrewCD)
        {

            ReturnValue<Data.Preflight.GetAllCrewDetails> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ClientId, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID, CrewID, CrewCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllCrewDetails>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCrewDetailsbyCrewIDorCrewCD(CustomerID, ClientId, DepartDate, ArriveDate, CrewGroupID, HomeBaseID, FleetID, ArrivalAirportID, CurrentTripID, MinDepartDate, MaxArriveDate, AircraftID, CrewID, CrewCD).ToList();
                        foreach (var item in ret.EntityList)
                        {
                            item.Passport = Crypting.Decrypt(item.Passport);
                        }
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        
        public string GetFuelUnitstring(decimal FuelUnitValue)
        {
            string fuelunitstr = "U.S. Gallons";
            //if (FuelUnitValue == -1)
            //    fuelunitstr = "U.S. Gallons";
            if (FuelUnitValue == 0)
                fuelunitstr = "U.S. Gallons";
            else if (FuelUnitValue == 1)
                fuelunitstr = "Imperial Gallons";
            else if (FuelUnitValue == 2)
                fuelunitstr = "Liters";

            return fuelunitstr;

        }

        public ReturnValue<GetCrewCheckList> GetCrewCheckList(bool IsStatus)
        {
            ReturnValue<GetCrewCheckList> ret = new ReturnValue<GetCrewCheckList>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(IsStatus))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetCrewCheckList(CustomerID, ClientId, IsStatus).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }

        public ReturnValue<Data.Preflight.CrewPassengerPassport> GetPassengerPassportByPassportID(Int64 PassportId)
        {
            ReturnValue<Data.Preflight.CrewPassengerPassport> CrewPassport = new ReturnValue<Data.Preflight.CrewPassengerPassport>();
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassportId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.Preflight.PreflightDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    CrewPassport.EntityList = cs.GetPassengerPassportByPassportID(CustomerID, PassportId).ToList();
                    foreach (var item in CrewPassport.EntityList)
                    {
                        item.PassportNum = Crypting.Decrypt(item.PassportNum);
                    }
                    CrewPassport.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassport;
        }
        #endregion

        #region "Airport"

        public ReturnValue<Data.Preflight.GetAirportbyAirportID> GetAirportbyAirportIDs(long AirportID)
        {

            ReturnValue<Data.Preflight.GetAirportbyAirportID> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, AirportID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAirportbyAirportID>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAirportbyAirportID(CustomerID, AirportID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        #endregion

        #region "Trip Main"

        //This method is not needed
        public ReturnValue<GetAllPreflightList> GetAllPreflightList()
        {
            
            ReturnValue<Data.Preflight.GetAllPreflightList> ret = new ReturnValue<Data.Preflight.GetAllPreflightList>();
            PreflightDataModelContainer cs = new PreflightDataModelContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllPreflightList>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetPreflightMainList(CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<View_PreflightMainList> GetAllPreflightMainList(Int64 homebaseID, Int64 clientID, string tripSheet, string worksheetstr, string hold, string cancelled, string schedServ, string Unfulfilled, bool isLog, Nullable<DateTime> EstDepartDate, string homebaseIDCSVStr)
        {
            ReturnValue<Data.Preflight.View_PreflightMainList> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, homebaseIDCSVStr))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        ret = new ReturnValue<Data.Preflight.View_PreflightMainList>();

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        if (clientID == 0)
                            clientID = UserPrincipal.Identity.ClientId == null ? 0 : (long)UserPrincipal.Identity.ClientId;

                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        ret.EntityList = cs.GetAllPreflightMainDetails(CustomerID, homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, isprivateUser, homebaseIDCSVStr).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<View_PreflightMainList> GetAllDeletedPreflightMainList()
        {
            ReturnValue<Data.Preflight.View_PreflightMainList> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        ret = new ReturnValue<Data.Preflight.View_PreflightMainList>();

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllDeletedPreflightMainDetails(CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<PreflightTripException> GetTripExceptionList(Int64 TripID)
        {
            ReturnValue<Data.Preflight.PreflightTripException> ret = new ReturnValue<Data.Preflight.PreflightTripException>();
            PreflightDataModelContainer cs = new PreflightDataModelContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.PreflightTripException>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetTripExceptionList(CustomerID, TripID).ToList();////cs.PreflightTripExceptions.Where(x => (x.TripID == TripID && x.CustomerID == CustomerID)).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<PreflightMain> GetList()
        {
            throw new NotImplementedException();
        }

        public long GetRevisionNum(long tripID, long customerID)
        {
            long? RevisionNum = 0;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripID, customerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    RevisionNum = new long();
                    RevisionNum = 0;
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        RevisionNum = cs.GetRevisionNum(customerID, tripID).FirstOrDefault();
                        RevisionNum = RevisionNum != null ? RevisionNum : 0;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return (long)RevisionNum;
        }

        public ReturnValue<PreflightMain> GetTripByTripNum(Int64 TripNum)
        {
            ReturnValue<PreflightMain> ret = null;
            PreflightMain prefMain = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripNum))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<PreflightMain>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        prefMain = new PreflightMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;


                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        //PreflightMain PreflightMain = new PreflightMain();
                        prefMain = cs.GetTripByTripNUM(CustomerID, TripNum).SingleOrDefault();
                        #region NewCode
                        if (clientID > 0)
                        {
                            if (prefMain != null)
                            {
                                if (prefMain.ClientID != clientID)
                                    prefMain = null;
                            }
                        }

                        if (isprivateUser)
                        {
                            if (prefMain != null)
                            {
                                if ((bool)prefMain.IsPrivate != false)
                                    prefMain = null;
                            }
                        }

                        if (prefMain != null)
                        {
                            LoadTripProperties(ref prefMain, ref cs);
                            ret.EntityList = new List<PreflightMain>();
                            ret.EntityList.Add(prefMain);
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            ret.EntityList = null;
                            ret.ReturnFlag = false;
                        }

                        #endregion
                        #region old code
                        //if (clientID > 0)
                        //{
                        //    ret.EntityList = (from trips in cs.PreflightMains
                        //                      where ((trips.TripNUM == TripNum)
                        //                             && (trips.CustomerID == CustomerID)
                        //                             && (trips.ClientID == clientID)
                        //                             && (trips.IsDeleted == false)
                        //                          // && (trips.IsPrivate == isprivateUser ? false : (bool)trips.IsPrivate)
                        //                             )
                        //                      select trips).ToList();
                        //}
                        //else
                        //{
                        //    ret.EntityList = (from trips in cs.PreflightMains
                        //                      where ((trips.TripNUM == TripNum)
                        //                            && (trips.CustomerID == CustomerID)
                        //                            && (trips.IsDeleted == false)
                        //                          // && (trips.IsPrivate == isprivateUser ? false : (bool)trips.IsPrivate)
                        //                            )
                        //                      select trips).ToList();
                        //}

                        //if (ret.EntityList != null && ret.EntityList.Count > 0)
                        //{
                        //    if (isprivateUser)
                        //    {
                        //        if ((bool)ret.EntityList[0].IsPrivate == false)
                        //        {
                        //            prefMain = ret.EntityList[0];
                        //            LoadTripProperties(ref prefMain, ref cs);
                        //            ret.EntityList[0] = prefMain;
                        //            ret.ReturnFlag = true;
                        //        }
                        //        else
                        //        {
                        //            prefMain = null;
                        //            ret.ReturnFlag = false;
                        //        }

                        //    }
                        //    else
                        //    {
                        //        prefMain = ret.EntityList[0];
                        //        LoadTripProperties(ref prefMain, ref cs);
                        //        ret.EntityList[0] = prefMain;
                        //        ret.ReturnFlag = true;
                        //    }
                        //}
                        //else
                        //{
                        //    prefMain = null;
                        //    ret.ReturnFlag = false;
                        //}
                        #endregion
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<PreflightMain> GetTrip(Int64 TripID)
        {
            ReturnValue<PreflightMain> ret = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer cs = null;
            PreflightMain prefMain = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        prefMain = new PreflightMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;

                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;

                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        prefMain = cs.GetTripByTripID(CustomerID, TripID).SingleOrDefault();
                        #region NewCode
                        if (clientID > 0)
                        {
                            if (prefMain != null)
                            {
                                if (prefMain.ClientID != clientID)
                                    prefMain = null;
                            }
                        }

                        if (isprivateUser)
                        {
                            if (prefMain != null)
                            {
                                if ((bool)prefMain.IsPrivate != false)
                                    prefMain = null;
                            }
                        }

                        if (prefMain != null)
                        {
                            LoadTripProperties(ref prefMain, ref cs);
                            ret.EntityList = new List<PreflightMain>();
                            ret.EntityList.Add(prefMain);
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            ret.EntityList = null;
                            ret.ReturnFlag = false;
                        }



                        #endregion

                        #region oldcode

                        //if (clientID > 0)
                        //{
                        //    ret.EntityList = (from trips in cs.PreflightMains
                        //                      where ((trips.TripID == TripID)
                        //                                && (trips.CustomerID == CustomerID)
                        //                                && (trips.ClientID == clientID)
                        //                                && (trips.IsDeleted == false)
                        //                           && (trips.IsPrivate == isprivateUser? false: (bool)trips.IsPrivate)
                        //                      )
                        //                      select trips).ToList();
                        //}
                        //else
                        //{
                        //    ret.EntityList = (from trips in cs.PreflightMains
                        //                      where ((trips.TripID == TripID)
                        //                          && (trips.CustomerID == CustomerID)
                        //                          && (trips.IsDeleted == false)
                        //                           && (trips.IsPrivate == isprivateUser ? false : (bool)trips.IsPrivate)
                        //                      )
                        //                      select trips).ToList();
                        //}
                        //if (ret.EntityList != null && ret.EntityList.Count > 0)
                        //{

                        //    if (isprivateUser)
                        //    {
                        //        if ((bool)ret.EntityList[0].IsPrivate == false)
                        //        {
                        //            prefMain = ret.EntityList[0];
                        //            LoadTripProperties(ref prefMain, ref cs);
                        //            ret.EntityList[0] = prefMain;
                        //            ret.ReturnFlag = true;
                        //        }
                        //        else
                        //        {
                        //            prefMain = null;
                        //            ret.ReturnFlag = false;
                        //        }

                        //    }
                        //    else
                        //    {
                        //        prefMain = ret.EntityList[0];
                        //        LoadTripProperties(ref prefMain, ref cs);
                        //        ret.EntityList[0] = prefMain;
                        //        ret.ReturnFlag = true;
                        //    }

                        //}
                        //else
                        //{
                        //    prefMain = null;
                        //    ret.ReturnFlag = false;
                        //}
                        #endregion

                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        private void LoadTripProperties(ref PreflightMain prefMain, ref PreflightDataModelContainer cs)
        {
            if (prefMain.AccountID != null) cs.LoadProperty(prefMain, c => c.Account);
            if (prefMain.AircraftID != null) cs.LoadProperty(prefMain, c => c.Aircraft);
            if (prefMain.ClientID != null) cs.LoadProperty(prefMain, c => c.Client);
            if (prefMain.CrewID != null) cs.LoadProperty(prefMain, c => c.Crew);
            if (prefMain.HomebaseID != null) cs.LoadProperty(prefMain, c => c.Company);
            if (prefMain.DepartmentID != null) cs.LoadProperty(prefMain, c => c.Department);
            if (prefMain.AuthorizationID != null) cs.LoadProperty(prefMain, c => c.DepartmentAuthorization);
            if (prefMain.CQCustomerID != null) cs.LoadProperty(prefMain, c => c.CQCustomer);
            if (prefMain.EmergencyContactID != null) cs.LoadProperty(prefMain, c => c.EmergencyContact);
            if (prefMain.FleetID != null) cs.LoadProperty(prefMain, c => c.Fleet);
            if (prefMain.PassengerRequestorID != null) cs.LoadProperty(prefMain, c => c.Passenger);
            if (prefMain.DispatcherUserName != null)
            {
                prefMain.DispatcherUserName = prefMain.DispatcherUserName.Trim();
                string dispatcher = prefMain.DispatcherUserName.Trim();
                // cs.LoadProperty(prefMain, c => c.UserMaster);

                IQueryable<UserMaster> Usermasters = preflightCompiledQuery.getUser(cs, dispatcher, CustomerID);
                UserMaster dispuser = new UserMaster();
                if (Usermasters != null && Usermasters.ToList().Count > 0)
                    dispuser = Usermasters.ToList()[0];

                //UserMaster dispuser = new UserMaster();
                //dispuser = (from user in cs.UserMasters
                //            where user.UserName == dispatcher && user.CustomerID == CustomerID
                //            select user).SingleOrDefault();
                if (dispuser != null)
                {
                    prefMain.UserMaster = dispuser;
                }
            }

            //if (prefMain.UserMaster1 != null) cs.LoadProperty(prefMain, "UserMaster1");

            cs.LoadProperty(prefMain, c => c.PreflightLegs);

            if (prefMain.PreflightLegs != null)
            {
                foreach (PreflightLeg Leg in prefMain.PreflightLegs.ToList())
                {
                    if (Leg.IsDeleted)
                        prefMain.PreflightLegs.Remove(Leg);

                    if (Leg.AccountID != null) cs.LoadProperty(Leg, c => c.Account);
                    if (Leg.ArriveICAOID != null) cs.LoadProperty(Leg, c => c.Airport);
                    if (Leg.DepartICAOID != null) cs.LoadProperty(Leg, c => c.Airport1);
                    if (Leg.ClientID != null) cs.LoadProperty(Leg, c => c.Client);
                    if (Leg.FlightCategoryID != null) cs.LoadProperty(Leg, c => c.FlightCatagory);
                    if (Leg.CrewDutyRulesID != null) cs.LoadProperty(Leg, c => c.CrewDutyRule);
                    if (Leg.DepartmentID != null) cs.LoadProperty(Leg, c => c.Department);
                    if (Leg.AuthorizationID != null) cs.LoadProperty(Leg, c => c.DepartmentAuthorization);
                    if (Leg.CQCustomerID != null) cs.LoadProperty(Leg, c => c.CQCustomer);
                    if (Leg.FBOID != null) cs.LoadProperty(Leg, c => c.FBO);
                    if (Leg.PassengerRequestorID != null) cs.LoadProperty(Leg, c => c.Passenger);
                    //if (Leg!= null) cs.LoadProperty(Leg, "UserMaster");

                    cs.LoadProperty(Leg, c => c.PreflightCrewLists);

                    #region NewCode
                    cs.LoadProperty(Leg, c => c.PreflightHotelLists);
                    Int64 HotelIdentifier = 1;
                    foreach (PreflightHotelList PrefHotel in Leg.PreflightHotelLists)
                    {
                        PrefHotel.HotelIdentifier = HotelIdentifier;
                        HotelIdentifier++;
                        if (PrefHotel.HotelID != null && PrefHotel.HotelID > 0)
                        {
                            List<HotelDetails> dbHotel = new List<HotelDetails>();
                            dbHotel = cs.GetHotelByHotelID((long)PrefHotel.HotelID).ToList();

                            if (dbHotel != null && dbHotel.Count > 0)
                            {
                                PrefHotel.HotelCode = dbHotel[0].HotelCD;
                            }
                        }

                        if (PrefHotel.CountryID != null && PrefHotel.CountryID > 0)
                        {
                            List<CountryDetails> dbCntry = new List<CountryDetails>();
                            dbCntry = cs.GetCountry(string.Empty, (long)PrefHotel.CountryID).ToList();

                            if (dbCntry != null && dbCntry.Count > 0)
                            {
                                PrefHotel.CountryCode = dbCntry[0].CountryCD;
                            }
                        }


                        List<Int64?> CrewIDList = new List<Int64?>();
                        if (PrefHotel.CrewIDList == null)
                            PrefHotel.CrewIDList = new List<Int64?>();
                        #region Get CrewID in PreflightHotelCrewPassengerList
                        CrewIDList = cs.GetHotelCrewOrPAXList(PrefHotel.PreflightHotelListID, CustomerID, "C").ToList();
                        #endregion
                        PrefHotel.CrewIDList = CrewIDList;

                        List<Int64?> PAXIDList = new List<Int64?>();
                        if (PrefHotel.PAXIDList == null)
                            PrefHotel.PAXIDList = new List<Int64?>();

                        #region Get PAXID in PreflightHotelCrewPassengerList
                        PAXIDList = cs.GetHotelCrewOrPAXList(PrefHotel.PreflightHotelListID, CustomerID, "P").ToList();
                        #endregion
                        PrefHotel.PAXIDList = PAXIDList;
                    }
                    #endregion

                    #region CrewHotelList OldCode Needs to be removed
                    foreach (PreflightCrewList prefCrew in Leg.PreflightCrewLists)
                    {
                        cs.LoadProperty(prefCrew, c => c.PreflightCrewHotelLists);
                    }
                    #endregion


                    cs.LoadProperty(Leg, c => c.PreflightPassengerLists);

                    foreach (PreflightPassengerList prefPass in Leg.PreflightPassengerLists)
                    {
                        cs.LoadProperty(prefPass, c => c.PreflightPassengerHotelLists);
                    }

                    cs.LoadProperty(Leg, c => c.PreflightTripSIFLs);

                    cs.LoadProperty(Leg, c => c.PreflightFBOLists);

                    foreach (PreflightFBOList Fbo in Leg.PreflightFBOLists)
                    {
                        cs.LoadProperty(Fbo, c => c.FBO);
                    }

                    cs.LoadProperty(Leg, c => c.PreflightCateringDetails);

                    foreach (PreflightCateringDetail PrefCatDt in Leg.PreflightCateringDetails)
                    {
                        cs.LoadProperty(PrefCatDt, c => c.Catering);
                    }

                    cs.LoadProperty(Leg, c => c.PreflightTransportLists);
                    cs.LoadProperty(Leg, c => c.PreflightTripOutbounds);

                    cs.LoadProperty(Leg, c => c.PreflightCheckLists);

                    foreach (PreflightCheckList PrefChklst in Leg.PreflightCheckLists)
                    {
                        cs.LoadProperty(PrefChklst, c => c.TripManagerCheckList);
                        cs.LoadProperty(PrefChklst.TripManagerCheckList, C => C.TripManagerCheckListGroup);
                    }

                }
            }
        }

        public ReturnValue<PreflightMain> UpdateTrip(PreflightMain trip)
        {
            ReturnValue<PreflightMain> ReturnValue = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new PreflightDataModelContainer())
                        {

                            bool SaveTrip = true;

                            if (trip.RecordType == "T")
                            {
                                base.CheckLock = true;
                                ReturnValue.ReturnFlag = base.Validate(trip, ref ReturnValue.ErrorMessage);

                                if (ReturnValue.ReturnFlag)
                                {

                                    if (trip.PreflightLegs != null && trip.PreflightLegs.Count > 0)
                                    {
                                        foreach (PreflightLeg Leg in trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                                        {
                                            ReturnValue<PreflightLeg> ReturnLegValue = new ReturnValue<PreflightLeg>();
                                            base.CheckLock = false;
                                            ReturnLegValue.ReturnFlag = base.Validate(Leg, ref ReturnLegValue.ErrorMessage);

                                            if (!ReturnLegValue.ReturnFlag)
                                            {
                                                SaveTrip = false;
                                                ReturnLegValue.ErrorMessage = "Leg " + Leg.LegNUM + ": \\n" + ReturnLegValue.ErrorMessage;
                                                ReturnValue.ErrorMessage += ReturnLegValue.ErrorMessage;
                                                break;
                                            }
                                        }
                                    }


                                }
                                else
                                    SaveTrip = false;
                            }

                            if (SaveTrip)
                            {

                                trip.CustomerID = CustomerID;
                                bool updatePreflightFuel = false;

                                #region Exception
                                List<PreflightTripException> ExceptionList = new List<PreflightTripException>();
                                List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                                List<PreflightTripException> eSeverityException = new List<PreflightTripException>();
                                if (trip.RecordType == "T")
                                {
                                    ExceptionList = ValidateTrip(trip);
                                    ExceptionTemplateList = Container.GetExceptionTemplate((long)PreflightExceptionModule.Preflight).ToList(); //Container.ExceptionTemplates.Where(x => x.ModuleID == (long)PreflightExceptionModule.Preflight).ToList();

                                    eSeverityException = (from Exp in ExceptionList
                                                          join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                          where ExpTempl.Severity == 1
                                                          select Exp).ToList();
                                }
                                #endregion

                                if (eSeverityException != null && eSeverityException.Count > 1)
                                {
                                    ReturnValue.ReturnFlag = false;
                                    ReturnValue.ErrorMessage = "Trip has mandatory Exception, Trip not saved!";
                                }
                                else
                                {
                                    #region Exception
                                    trip.PreflightTripExceptions.Clear();

                                    foreach (PreflightTripException PrefException in ExceptionList)
                                    {
                                        PreflightTripException NewExeption = new PreflightTripException();
                                        NewExeption.TripID = trip.TripID;
                                        NewExeption.CustomerID = PrefException.CustomerID;
                                        NewExeption.ExceptionDescription = PrefException.ExceptionDescription;
                                        NewExeption.LastUpdUID = UserPrincipal.Identity.Name;
                                        NewExeption.LastUpdTS = DateTime.UtcNow;
                                        NewExeption.ExceptionTemplateID = PrefException.ExceptionTemplateID;
                                        NewExeption.DisplayOrder = PrefException.DisplayOrder;
                                        trip.PreflightTripExceptions.Add(NewExeption);
                                    }

                                    #region Commented
                                    //objContainer.AddToFPEmailTranLog(new Data.Admin.FPEmailTranLog
                                    //{
                                    //    CustomerID = CustomerID,
                                    //    EmailPrio = true,
                                    //    EmailReqUID = UserName,
                                    //    EmailSentTo = userMaster.EmailID,
                                    //    EmailStatus = "Not Done",
                                    //    LastUPDUID = UserName,
                                    //    EmailBody = "Welcome to Flight Pak Application",
                                    //    EmailSubject = "User Name:" + userMaster.UserName + Environment.NewLine + "Password:" + password
                                    //});
                                    #endregion

                                    #endregion
                                    trip.PreviousNUM = 0;
                                    ReturnValue<PreflightMain> oldTrip = GetTrip(trip.TripID);
                                    getOldTrip = oldTrip;
                                    bool isTripChange = false;
                                    if (trip.isUpdateFuel || (trip.TripID == 0 && trip.EstFuelQTY > 0))
                                        updatePreflightFuel = true;

                                    foreach (PreflightLeg legFuel in trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                                    {
                                        if (trip.TripID == 0){
                                            if(legFuel.EstFuelQTY > 0){
                                                updatePreflightFuel = true;
                                            }
                                        }
                                        else
                                        {
                                            //Get the previous version of the leg and if the Estimated quantity has changed, then we have to update now
                                            PreflightMain oldTripTemp = null;
                                            if (oldTrip != null && oldTrip.EntityList != null)
                                                oldTripTemp = oldTrip.EntityList.FirstOrDefault();
                                            if (oldTripTemp != null)
                                            {
                                                PreflightLeg oldLeg = oldTripTemp.PreflightLegs.Where(ol => ol.LegID == legFuel.LegID).FirstOrDefault();
                                                if (oldLeg == null || oldLeg.EstFuelQTY != legFuel.EstFuelQTY)
                                                {
                                                    updatePreflightFuel = true;
                                                }
                                            }
                                        }
                                    }

                                    UpdateEntity(ref trip, ref Container, ref isTripChange);
                                    Container.SaveChanges();
                                    if (updatePreflightFuel)
                                        Container.UpdatePreflightFuelDetails(trip.TripID, CustomerID, UserPrincipal.Identity.SessionID);

                                    //Unlock
                                    LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                                    lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, trip.TripID);

                                    ReturnValue = GetTrip(trip.TripID);
                                    ReturnValue.EntityInfo = ReturnValue.EntityList[0];

                                    //if (UserPrincipal.Identity.Settings.IsBlackberrySupport != null && (bool)UserPrincipal.Identity.Settings.IsBlackberrySupport)
                                    if (ReturnValue.EntityList[0] != null)
                                    {
                                        if (oldTrip.EntityList == null)
                                        {
                                            SendEmailToCrewList(ReturnValue.EntityList[0]);
                                        }
                                        else
                                        {
                                            string[] ignoreListProp = new string[] { "_LASTUPDUID", "_LASTUPDTS", "LASTUPDUID", "LASTUPDTS" };

                                            string[] containListProp = new string[] { "PREFLIGHTLEGS", "PREFLIGHTLEG", "CREW", "COMPANY", "PASSENGER", "FLEET", "AIRCRAFT", "AIRPORT1", 
                                            "ACCOUNT", "USERMASTER", "DEPARTMENT", "DEPARTMENTAUTHORIZATION", "FLIGHTCATAGORY", "CREWDUTYRULE", 
                                            "PREFLIGHTFBOLISTS", "PREFLIGHTFBOLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTHOTELLISTS", 
                                            "PREFLIGHTHOTELLIST", "PREFLIGHTTRANSPORTLISTS", "PREFLIGHTTRANSPORTLIST", "PREFLIGHTCATERINGDETAILS", "PREFLIGHTCATERINGDETAIL", 
                                            "PREFLIGHTPASSENGERLISTS", "PREFLIGHTPASSENGERLIST", "CREWPASSENGERPASSPORTS", "CREWPASSENGERPASSPORT", "FLIGHTPURPOSE", 
                                            "PREFLIGHTCREWLISTS", "PREFLIGHTCREWLIST", "FBO","TRANSPORT","HOTEL","CATERING","PREFLIGHTTRIPOUTBOUNDS","PREFLIGHTTRIPOUTBOUND",
                                            "PREFLIGHTCHECKLISTS","PREFLIGHTCHECKLIST","PREFLIGHTFUELS","PREFLIGHTFUEL" };

                                            bool areEqual1 = Utility.CompareObjects(oldTrip.EntityList[0], ReturnValue.EntityList[0], ignoreListProp, containListProp);

                                            if (areEqual1 == false && isTripChange == true)
                                            {
                                                ReturnValue.EntityList[0].Mode = TripActionMode.Edit;
                                                ReturnValue.EntityList[0].State= TripEntityState.Modified;
                                                SendEmailToCrewList(ReturnValue.EntityList[0]);
                                            }
                                        }
                                    }
                                    ReturnValue.ReturnFlag = true;
                                }
                            }
                            else
                            {
                                ReturnValue.ReturnFlag = false;
                                ReturnValue.EntityInfo = trip;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Write(String.Format("Error :Preflight Add Trip , will still return:{0}", ex.ToString()), FlightPak.Common.Constants.Policy.DataLayer);
                        //manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                        ReturnValue.EntityInfo = trip;
                    }
                    finally
                    {

                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        #region EMAIL
        //create list of members
        List<PreflightCrewList> deletedCrewList = new List<PreflightCrewList>();
        List<PreflightCrewList> addedCrewList = new List<PreflightCrewList>();
        List<PreflightCrewList> commonCrewList = new List<PreflightCrewList>();
        List<PreflightLeg> deletedLegs = null;
        public void SendEmailToCrewList(PreflightMain Trip)
        {
try{
            List<string> EmailList = new List<string>();
            //new trip added
            if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
            {
                if (getOldTrip != null && getOldTrip.EntityList == null)
                {
                    foreach (PreflightLeg legs in Trip.PreflightLegs)
                    {
                        foreach (PreflightCrewList Crewlist in legs.PreflightCrewLists)
                        {
                           
                            if (Crewlist.CrewID != null)
                            {
                                //add to ADD list
                                addedCrewList.Add(Crewlist);
                            }
                        }
                    }
                }
                else //member deleted or leg deleted or leg updated
                {
                    int oldTripLegCount = getOldTrip.EntityList[0].PreflightLegs.Count();
                    int newTripLegCount = Trip.PreflightLegs.Count();
                    //leg deleted
                    if (newTripLegCount < oldTripLegCount)
                    {
                        deletedLegs = new List<PreflightLeg>();
                        deletedLegs = getOldTrip.EntityList[0].PreflightLegs.Where(x => !Trip.PreflightLegs.Any(l => x.LegID == l.LegID)).ToList();
                        foreach (PreflightLeg leg in deletedLegs)
                        {
                            if (leg != null)
                            {
                                foreach (var crewMember in leg.PreflightCrewLists)
                                {
                                    if (crewMember != null)
                                    {
                                        //add to DELETED list
                                        deletedCrewList.Add(crewMember);
                                    }
                                }
                            }
                        }
                        //check for changes in remaning legs
                        foreach (PreflightLeg tripLeg in Trip.PreflightLegs)
                        {
                            if (tripLeg != null)
                            {
                                checkTripCrewMember(tripLeg, Trip);
                            }
                        }
                    }
                    else// leg added or members updated/deleted
                    {
                        foreach (PreflightLeg leg in Trip.PreflightLegs)
                        {
                            checkTripCrewMember(leg,Trip);
                        }
                    }
                }
                //sort deletedCrewList
                List<long?> tempCrewList = deletedCrewList.Select(x => x.CrewID).Distinct().ToList();
                List<long?> LegIDs=new List<long?>();
                foreach(long crewID in tempCrewList)
                {
                    LegIDs = deletedCrewList.Where(x => x.CrewID == crewID).Select(p => p.LegID).ToList();
                    SendEmailToCew(Trip, crewID, FlightPak.Common.Constants.EmailSubject.UnAssignedTrip, LegIDs);
                }

                //sort commonCrewList
                tempCrewList.Clear();
                LegIDs.Clear();
                tempCrewList = commonCrewList.Select(x => x.CrewID).Distinct().ToList();
                foreach (long crewID in tempCrewList)
                {
                    LegIDs = commonCrewList.Where(x => x.CrewID == crewID).Select(p => p.LegID).ToList();
                    SendEmailToCew(Trip, crewID, FlightPak.Common.Constants.EmailSubject.UpdateTrip, LegIDs);
                }
                //sort addCrewList
                tempCrewList.Clear();  
                LegIDs.Clear();
                tempCrewList = addedCrewList.Select(x=>x.CrewID).Distinct().ToList();
                foreach (long crewID in tempCrewList)
                {
                    LegIDs = addedCrewList.Where(x => x.CrewID == crewID).Select(p => p.LegID).ToList();
                    SendEmailToCew(Trip, crewID,FlightPak.Common.Constants.EmailSubject.AssignedTrip, LegIDs);
                }
            }
            //empty lists
            deletedCrewList.Clear();
            addedCrewList.Clear();
            commonCrewList.Clear();
}
catch(Exception ex){
 Logger.Write(String.Format("Error: In PreflightManager.SendEmailToCrewList(PreflightMain Trip): {0}", ex.ToString()), FlightPak.Common.Constants.Policy.BusinessLayer);
}
        }
        public void checkTripCrewMember(PreflightLeg leg,PreflightMain Trip)
        {
            var getCrewMembers = GetCrewMemberFromTrip(leg.PreflightCrewLists, leg.LegID);
            var deletedCrewMember = getCrewMembers["DeleteCrewMember"];
            var commonCrewMember = getCrewMembers["CommonCrewMember"];
            var addedCrewMember = getCrewMembers["AddedCrewMember"];
            foreach (PreflightCrewList Crewlist in deletedCrewMember)
            {
                if (Crewlist.CrewID != null)
                {
                    //add to DELETED list
                    deletedCrewList.Add(Crewlist);
                }
            }
            foreach (PreflightCrewList Crewlist in addedCrewMember)
            {
                if (Crewlist.CrewID != null)
                {
                    //add to ADD list
                    addedCrewList.Add(Crewlist);
                }
            }
            foreach (PreflightCrewList Crewlist in commonCrewMember)
            {
                if (Crewlist.CrewID != null && (deletedCrewMember.Count > 0 || addedCrewMember.Count > 0))
                {
                    //add to COMMON list
                    commonCrewList.Add(Crewlist);
                }
            }
        }
        public void SendEmailToCew(PreflightMain Trip, long crewId, string action,List<long?> legIDs)
        {
            PreflightDataModelContainer Container;
            string emailType = string.Empty;
            List<string> EmailList = new List<string>();
            using (Container = new PreflightDataModelContainer())
            {
                IQueryable<Crew> Crews = preflightCompiledQuery.getCrew(Container, (long)crewId, CustomerID);
                Crew TripCrew = new Crew();
                if (Crews != null && Crews.ToList().Count > 0)
                    TripCrew = Crews.ToList()[0];
                if (TripCrew != null && TripCrew.IsTripEmail.HasValue && TripCrew.IsTripEmail.Value)
                {
                    string emailid = TripCrew.EmailAddress;
                    if (emailid != null && !string.IsNullOrEmpty(emailid))
                    {
                        emailid = emailid.Trim();
                        if (!EmailList.Contains(emailid))
                        {

                            EmailList.Add(emailid);
                            if (action == FlightPak.Common.Constants.EmailSubject.AssignedTrip)
                            {
                                emailType =FlightPak.Common.Constants.EmailSubject.AssignedTrip;
                                SendTripDetailsByCrew(TripCrew, Trip, emailid, emailType,legIDs);
                            }
                            else if (action == FlightPak.Common.Constants.EmailSubject.UnAssignedTrip)
                            {
                                emailType = FlightPak.Common.Constants.EmailSubject.UnAssignedTrip;
                                SendTripDetailsByCrew(TripCrew, Trip, emailid, emailType,legIDs);
                            }
                            else if (action == FlightPak.Common.Constants.EmailSubject.UpdateTrip)
                            {
                                emailType = FlightPak.Common.Constants.EmailSubject.UpdateTrip;
                                SendTripDetailsByCrew(TripCrew, Trip, emailid, emailType,legIDs);
                            }
                        }
                    }
                }
            }
        }

        //getting deleted,new,old crew members
        public Dictionary<string, List<PreflightCrewList>> GetCrewMemberFromTrip(IEnumerable<PreflightCrewList> crewList, long legId)
        {
            Dictionary<string, List<PreflightCrewList>> emailCrewList = new Dictionary<string, List<PreflightCrewList>>();
            List<PreflightCrewList> deletedCrewMember = new List<PreflightCrewList>();
            List<PreflightCrewList> commonCrewMember = new List<PreflightCrewList>();
            List<PreflightCrewList> addedCrewMember = new List<PreflightCrewList>();
            if (getOldTrip != null && getOldTrip.EntityList != null)
            {
                ReturnValue<PreflightMain> trips = getOldTrip;
                List<PreflightCrewList> getOldCrewMember = new List<PreflightCrewList>();
                PreflightLeg getLeg = trips.EntityList[0].PreflightLegs.FirstOrDefault(p => p.LegID == legId);
                if (getLeg != null)
                {
                    foreach (var crewMember in getLeg.PreflightCrewLists)
                    {
                        if (crewMember != null)
                        {
                            getOldCrewMember.Add(crewMember);
                        }
                    }

                }
                List<PreflightCrewList> newCrewMembers = new List<PreflightCrewList>();
                foreach (var items in crewList)
                {
                    newCrewMembers.Add(items);
                }
                deletedCrewMember = getOldCrewMember.Where(p => !newCrewMembers.Any(l => p.CrewID == l.CrewID)).ToList();
                commonCrewMember = getOldCrewMember.Where(p => newCrewMembers.Any(l => p.CrewID == l.CrewID)).ToList();
                addedCrewMember = newCrewMembers.Where(p => !getOldCrewMember.Any(l => p.CrewID == l.CrewID)).ToList();
            }
            emailCrewList.Add("DeleteCrewMember", deletedCrewMember);
            emailCrewList.Add("CommonCrewMember", commonCrewMember);
            emailCrewList.Add("AddedCrewMember", addedCrewMember);
            return emailCrewList;
        }

        public void SendEmailToPaxList(PreflightMain Trip)
        {
            List<string> EmailList = new List<string>();
            PreflightDataModelContainer Container;
            if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
            {
                foreach (PreflightLeg legs in Trip.PreflightLegs)
                {
                    foreach (PreflightPassengerList Paxlist in legs.PreflightPassengerLists)
                    {
                        if (Paxlist.PassengerID != null)
                        {
                            using (Container = new PreflightDataModelContainer())
                            {
                                IQueryable<Passenger> Passengers = preflightCompiledQuery.getPassenger(Container, (long)Paxlist.PassengerID, CustomerID);
                                Passenger TripPax = new Passenger();
                                if (Passengers != null && Passengers.ToList().Count > 0)
                                    TripPax = Passengers.ToList()[0];
                                if (TripPax != null)
                                {
                                    string emailid = TripPax.EmailAddress;
                                    if (emailid != null && !string.IsNullOrEmpty(emailid))
                                    {
                                        if (!EmailList.Contains(emailid))
                                            EmailList.Add(emailid);
                                    }
                                }
                            }
                        }
                    }
                }

                SendTripDetails(Trip, EmailList);
            }
        }

        public void SendItineraryDetailsToPAX(PreflightMain Trip, string EmailID, List<GetPaxItineraryInfo> PaxItineraryList)
        {
            if (!string.IsNullOrEmpty(EmailID))
            {
                List<string> EmailList = new List<string>();
                EmailList.Add(EmailID);

                StringBuilder htmlEmailbody = new StringBuilder();

                string Emailsubject = "Flight Scheduling Software - Trip No.: " + Trip.TripNUM;

                if (PaxItineraryList.Count > 0)
                {
                    foreach (GetPaxItineraryInfo paxInfo in PaxItineraryList)
                    {
                        htmlEmailbody.Append("<html>");
                        htmlEmailbody.Append("<head>");
                        htmlEmailbody.Append("</head>");
                        htmlEmailbody.Append("<body>");
                        htmlEmailbody.Append("<table align='center' width='800' style='border: #666666 1px solid;' cellpadding='3' cellspacing='0'>");
                        htmlEmailbody.Append("<tr style='border-bottom: 1px solid black;'>");
                        htmlEmailbody.Append("<td bgcolor='#E5F7FF' style='border-bottom: #666666 1px solid;'>");
                        htmlEmailbody.Append("<br />");
                        htmlEmailbody.Append("<table border='0' align='center'>");
                        htmlEmailbody.Append("<tr><td><p style='font-family: arial,  helvetica, sans-serif; font-size: 25px; color: #3366cc;'>" + UserPrincipal.Identity.Settings.CompanyName + "</p></td></tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        htmlEmailbody.Append("<table width='750' style='font: 20px Arial, Helvetica, sans-serif; color: #444;' align='center'>");
                        htmlEmailbody.Append("<tr><td align='center'><strong>PASSENGER TRAVEL ITINERARY</strong></td></tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("<br />");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<b>This Itinerary Has Been Prepared For : </b>" + paxInfo.PassengerNm);
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<b>Trip Dates : </b>");
                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                        {
                            DateTime LegMinDate = DateTime.MinValue;
                            DateTime LegMaxDate = DateTime.MinValue;
                            foreach (PreflightLeg Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                            {
                                if (Leg.DepartureGreenwichDTTM != null)
                                {
                                    if (LegMinDate == DateTime.MinValue)
                                        LegMinDate = (DateTime)Leg.DepartureGreenwichDTTM;

                                    if (LegMinDate > (DateTime)Leg.DepartureGreenwichDTTM)
                                        LegMinDate = (DateTime)Leg.DepartureGreenwichDTTM;
                                }

                                if (Leg.ArrivalGreenwichDTTM != null)
                                {
                                    if (LegMaxDate == DateTime.MinValue)
                                        LegMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;

                                    if (LegMaxDate < (DateTime)Leg.ArrivalGreenwichDTTM)
                                        LegMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;
                                }
                            }
                            htmlEmailbody.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", LegMinDate) + " - " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", LegMaxDate));
                        }

                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<table width='100%' style='border: 1px solid black;'>");
                        htmlEmailbody.Append("<tr style='font-weight: bold;'>");
                        htmlEmailbody.Append("<td>Dep. Date</td>");
                        htmlEmailbody.Append("<td>From</td>");
                        htmlEmailbody.Append("<td>To</td>");
                        htmlEmailbody.Append("<td>Depart</td>");
                        htmlEmailbody.Append("<td>Arrive</td>");
                        htmlEmailbody.Append("<td>FltTime</td>");
                        htmlEmailbody.Append("<td>PAX</td>");
                        htmlEmailbody.Append("</tr>");

                        htmlEmailbody.Append("<tr>");
                        if (paxInfo.DepDate != null)
                            htmlEmailbody.Append("<td>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", paxInfo.DepDate) + "</td>");
                        else
                            htmlEmailbody.Append("<td></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.From + "</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.To + "</td>");
                        if (paxInfo.Depart != null)
                            htmlEmailbody.Append("<td>" + String.Format("{0:t}", paxInfo.Depart) + "</td>");
                        else
                            htmlEmailbody.Append("<td>&nbsp;</td>");
                        if (paxInfo.Arrive != null)
                            htmlEmailbody.Append("<td>" + String.Format("{0:t}", paxInfo.Arrive) + "</td>");
                        else
                            htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.FltTime + "</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TotPax + "</td>");
                        htmlEmailbody.Append("</tr>");

                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='right'>");
                        htmlEmailbody.Append("<table>");
                        if (paxInfo.ArrDate != null)
                        {
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td><b>Local Arrival Date is :</b></td>");
                            htmlEmailbody.Append("<td>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", paxInfo.ArrDate) + "</td>");
                            htmlEmailbody.Append("</tr>");
                        }

                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td><b>*Time Change Enroute is :</b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TCEnrouteis + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td><b>*Time Change from Home Base is :</b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TCHomeBaseis + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<table>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td style='padding-left:40px;'><b>Terminal: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TerminalD + "<br />" + paxInfo.TerminalDPh + "</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TerminalA + "<br />" + paxInfo.TerminalAPh + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Aircraft: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.Aircraft + "&nbsp;" + paxInfo.Aircraft1 + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Captain: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.Captain + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Other Crew: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.OtherCrew + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Flight Information: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.FlightInformation + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</body>");
                        htmlEmailbody.Append("</html>");
                    }

                    SendEmailByList(EmailList, Emailsubject, htmlEmailbody);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="EmailID"></param>
        /// <param name="PaxItineraryList"></param>
        /// <param name="FromEmail"></param>
        /// <param name="HtmlBody"></param>
        public void SendItineraryDetailsToPAX(PreflightMain Trip, string EmailID, string FromEmail, string HtmlBody, List<String> attachments)
        {
            if (!string.IsNullOrEmpty(EmailID))
            {
                List<string> EmailList = new List<string>();
                EmailList.Add(EmailID);

                string Emailsubject = string.Format("Flight Scheduling Software - Trip No.: {0}", Trip.TripNUM);

                StringBuilder htmlEmailbody = new StringBuilder();
                htmlEmailbody.Append(HtmlBody);
                SendEmailByList(EmailList, Emailsubject, htmlEmailbody, FromEmail, attachments);
            }
        }

        /// <summary>
        /// Method to Send PAX Itinerary to specified Email List
        /// </summary>
        /// <param name="Trip">Pass Trip Object</param>
        /// <param name="EmailList">Pass Email Address as List</param>
        public void SendItineraryDetailsToOthers(PreflightMain Trip, List<string> EmailList)
        {
            if (EmailList != null && EmailList.Count > 0)
            {
                StringBuilder htmlEmailbody = new StringBuilder();

                string Emailsubject = "Flight Scheduling Software - Trip No.: " + Trip.TripNUM;

                List<GetPaxItineraryInfo> paxItineraryList = GetPassengerItinerary(Trip, null);

                if (paxItineraryList.Count > 0)
                {
                    foreach (GetPaxItineraryInfo paxInfo in paxItineraryList)
                    {
                        htmlEmailbody.Append("<html>");
                        htmlEmailbody.Append("<head>");
                        htmlEmailbody.Append("</head>");
                        htmlEmailbody.Append("<body>");
                        htmlEmailbody.Append("<table align='center' width='800' style='border: #666666 1px solid;' cellpadding='3' cellspacing='0'>");
                        htmlEmailbody.Append("<tr style='border-bottom: 1px solid black;'>");
                        htmlEmailbody.Append("<td bgcolor='#E5F7FF' style='border-bottom: #666666 1px solid;'>");
                        htmlEmailbody.Append("<br />");
                        htmlEmailbody.Append("<table border='0' align='center'>");
                        htmlEmailbody.Append("<tr><td><p style='font-family: arial,  helvetica, sans-serif; font-size: 25px; color: #3366cc;'>" + UserPrincipal.Identity.Settings.CompanyName + "</p></td></tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        htmlEmailbody.Append("<table width='750' style='font: 20px Arial, Helvetica, sans-serif; color: #444;' align='center'>");
                        htmlEmailbody.Append("<tr><td align='center'><strong>PASSENGER TRAVEL ITINERARY</strong></td></tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("<br />");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<b>This Itinerary Has Been Prepared For : </b>" + paxInfo.PassengerNm);
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<b>Trip Dates : </b>");
                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                        {
                            DateTime LegMinDate = DateTime.MinValue;
                            DateTime LegMaxDate = DateTime.MinValue;
                            foreach (PreflightLeg Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                            {
                                if (Leg.DepartureGreenwichDTTM != null)
                                {
                                    if (LegMinDate == DateTime.MinValue)
                                        LegMinDate = (DateTime)Leg.DepartureGreenwichDTTM;

                                    if (LegMinDate > (DateTime)Leg.DepartureGreenwichDTTM)
                                        LegMinDate = (DateTime)Leg.DepartureGreenwichDTTM;
                                }

                                if (Leg.ArrivalGreenwichDTTM != null)
                                {
                                    if (LegMaxDate == DateTime.MinValue)
                                        LegMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;

                                    if (LegMaxDate < (DateTime)Leg.ArrivalGreenwichDTTM)
                                        LegMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;
                                }
                            }
                            htmlEmailbody.Append(String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", LegMinDate) + " - " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", LegMaxDate));
                        }

                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<table width='100%' style='border: 1px solid black;'>");
                        htmlEmailbody.Append("<tr style='font-weight: bold;'>");
                        htmlEmailbody.Append("<td>Dep. Date</td>");
                        htmlEmailbody.Append("<td>From</td>");
                        htmlEmailbody.Append("<td>To</td>");
                        htmlEmailbody.Append("<td>Depart</td>");
                        htmlEmailbody.Append("<td>Arrive</td>");
                        htmlEmailbody.Append("<td>FltTime</td>");
                        htmlEmailbody.Append("<td>PAX</td>");
                        htmlEmailbody.Append("</tr>");

                        htmlEmailbody.Append("<tr>");
                        if (paxInfo.DepDate != null)
                            htmlEmailbody.Append("<td>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", paxInfo.DepDate) + "</td>");
                        else
                            htmlEmailbody.Append("<td></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.From + "</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.To + "</td>");
                        if (paxInfo.Depart != null)
                            htmlEmailbody.Append("<td>" + String.Format("{0:t}", paxInfo.Depart) + "</td>");
                        else
                            htmlEmailbody.Append("<td>&nbsp;</td>");
                        if (paxInfo.Arrive != null)
                            htmlEmailbody.Append("<td>" + String.Format("{0:t}", paxInfo.Arrive) + "</td>");
                        else
                            htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.FltTime + "</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TotPax + "</td>");
                        htmlEmailbody.Append("</tr>");

                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='right'>");
                        htmlEmailbody.Append("<table>");
                        if (paxInfo.ArrDate != null)
                        {
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td><b>Local Arrival Date is :</b></td>");
                            htmlEmailbody.Append("<td>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", paxInfo.ArrDate) + "</td>");
                            htmlEmailbody.Append("</tr>");
                        }

                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td><b>*Time Change Enroute is :</b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TCEnrouteis + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td><b>*Time Change from Home Base is :</b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TCHomeBaseis + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='left'>");
                        htmlEmailbody.Append("<table>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td style='padding-left:40px;'><b>Terminal: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TerminalD + "<br />" + paxInfo.TerminalDPh + "</td>");
                        htmlEmailbody.Append("<td>" + paxInfo.TerminalA + "<br />" + paxInfo.TerminalAPh + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Aircraft: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.Aircraft + "&nbsp;" + paxInfo.Aircraft1 + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Captain: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.Captain + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Other Crew: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.OtherCrew + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>&nbsp;</td>");
                        htmlEmailbody.Append("<td style='padding-left:50px;'><b>Flight Information: </b></td>");
                        htmlEmailbody.Append("<td>" + paxInfo.FlightInformation + "</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</body>");
                        htmlEmailbody.Append("</html>");
                    }

                    SendEmailByList(EmailList, Emailsubject, htmlEmailbody);
                }
            }
        }

        /// <summary>
        /// Method to Send PAX Itinerary to specified Email List
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip">Pass Trip Object</param>
        /// <param name="EmailList">Pass Email Address as List</param>
        /// <param name="FromEmail">Pass From Email Address as String</param>
        public void SendItineraryDetailsToOthers(PreflightMain Trip, List<string> EmailList, string FromEmail, string HtmlBody, List<String> attachments)
        {
            if (EmailList != null && EmailList.Count > 0)
            {
                string Emailsubject = string.Format("Flight Scheduling Software - Trip No.: {0}" , Trip.TripNUM);

                StringBuilder htmlEmailbody = new StringBuilder();
                htmlEmailbody.Append(HtmlBody);
                SendEmailByList(EmailList, Emailsubject, htmlEmailbody, FromEmail, attachments);
            }
        }

        /// <summary>
        /// Method to Get Passenger Itinerary Details
        /// </summary>
        /// <param name="Trip">Pass Preflight Trip Object</param>
        /// <returns>Returns Passenger Itinerary Details</returns>
        public List<GetPaxItineraryInfo> GetPassengerItinerary(PreflightMain Trip, Int64? PaxID)
        {
            List<GetPaxItineraryInfo> paxItineraryList = new List<GetPaxItineraryInfo>();
            using (PreflightDataModelContainer cs = new PreflightDataModelContainer())
            {
                cs.ContextOptions.LazyLoadingEnabled = false;
                cs.ContextOptions.ProxyCreationEnabled = false;

                if (PaxID != null && PaxID.HasValue && PaxID.Value > 0)
                    paxItineraryList = cs.GetPaxItineraryInfo(UserName, Trip.TripID, CustomerID, PaxID.Value.ToString()).ToList();
                else
                    paxItineraryList = cs.GetPaxItineraryInfo(UserName, Trip.TripID, CustomerID, string.Empty).ToList();
            }
            return paxItineraryList;
        }

        /// <summary>
        /// Method to Send Trip Sheet Details to specified Email List
        /// </summary>
        /// <param name="Trip">Pass Trip Object</param>
        /// <param name="EmailList">Pass Email Address as List</param>
        public void SendTripDetails(PreflightMain Trip, List<string> EmailList)
        {
            if (EmailList != null && EmailList.Count > 0)
            {
                string Emailsubject = "Flight Scheduling Software - Trip No.: " + Trip.TripNUM;

                StringBuilder htmlEmailbody = new StringBuilder();

                htmlEmailbody.Append("<html>");
                htmlEmailbody.Append("<head>");
                htmlEmailbody.Append("</head>");
                htmlEmailbody.Append("<body>");
                htmlEmailbody.Append("<table align='center' width='800' style='border: #666666 1px solid;' cellpadding='0' cellspacing='0'>");
                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td bgcolor='#E5F7FF' style='border-bottom: #666666 1px solid;' ><br>");
                htmlEmailbody.Append("<TABLE border='0' align='center'>");
                htmlEmailbody.Append("<TR>");
                htmlEmailbody.Append("<TD><p style='font-family: arial,  helvetica, sans-serif;font-size: 25px;color: #3366cc;'>");
                htmlEmailbody.Append("TRIPSHEET </p></TD>");
                htmlEmailbody.Append("</TR>");
                htmlEmailbody.Append("</TABLE>");
                htmlEmailbody.Append("<br></td>");
                htmlEmailbody.Append("</tr>");
                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;' ><table width='750' style='font: 12px Arial, Helvetica, sans-serif; color:#444;' align='center'>");
                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td width='20%'><strong>Trip Number</strong></td>");

                htmlEmailbody.Append("<td width='30%'>" + (Trip.TripNUM != null ? Trip.TripNUM.ToString() : string.Empty) + "</td>");
                htmlEmailbody.Append("<td width='20%'><strong>Description</strong></td>");
                htmlEmailbody.Append("<td width='30%'>" + (Trip.TripDescription != null ? Trip.TripDescription : string.Empty) + " </td>");
                htmlEmailbody.Append("</tr>");
                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td><strong>Trip Dates</strong></td>");
                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    DateTime LegMinDate = DateTime.MinValue;
                    DateTime LegMaxDate = DateTime.MinValue;

                    foreach (PreflightLeg Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                    {
                        if (Leg.DepartureGreenwichDTTM != null)
                        {
                            if (LegMinDate == DateTime.MinValue)
                                LegMinDate = (DateTime)Leg.DepartureGreenwichDTTM;

                            if (LegMinDate > (DateTime)Leg.DepartureGreenwichDTTM)
                                LegMinDate = (DateTime)Leg.DepartureGreenwichDTTM;
                        }

                        if (Leg.ArrivalGreenwichDTTM != null)
                        {
                            if (LegMaxDate == DateTime.MinValue)
                                LegMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;

                            if (LegMaxDate < (DateTime)Leg.ArrivalGreenwichDTTM)
                                LegMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;
                        }
                    }

                    htmlEmailbody.Append("<td>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", LegMinDate) + " - " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", LegMaxDate) + "</td>");
                }

                htmlEmailbody.Append("<td><strong>Requestor</strong></td>");
                if (Trip.Passenger != null)
                    htmlEmailbody.Append("<td>" + (Trip.Passenger.PassengerName != null ? Trip.Passenger.PassengerName : string.Empty) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");
                htmlEmailbody.Append("</tr>");
                htmlEmailbody.Append("<tr>");

                htmlEmailbody.Append("<td><strong>Tailnum</strong></td>");

                if (Trip.Fleet != null)
                    htmlEmailbody.Append("<td>" + (Trip.Fleet.TailNum != null ? Trip.Fleet.TailNum : string.Empty) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");

                htmlEmailbody.Append("<td><strong>Contact Phone</strong></td>");
                if (Trip.Passenger != null)
                    htmlEmailbody.Append("<td>" + (Trip.Passenger.PhoneNum != null ? Trip.Passenger.PhoneNum : string.Empty) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");
                htmlEmailbody.Append("</tr>");

                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td><strong>Type</strong></td>");
                if (Trip.Aircraft != null)
                    htmlEmailbody.Append("<td>" + (Trip.Aircraft.AircraftCD != null ? Trip.Aircraft.AircraftCD : string.Empty) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");

                htmlEmailbody.Append("<td><strong>Dispatcher Name</strong></td>");
                if (Trip.UserMaster != null)
                    htmlEmailbody.Append("<td>" + (Trip.UserMaster.FirstName + (string.IsNullOrWhiteSpace(Trip.UserMaster.MiddleName) ? string.Empty : (" " + Trip.UserMaster.MiddleName)) + (string.IsNullOrWhiteSpace(Trip.UserMaster.LastName) ? string.Empty : (" " + Trip.UserMaster.LastName))) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");
                htmlEmailbody.Append("</tr>");

                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td><strong>Released By</strong></td>");
                if (Trip.Crew != null)
                    htmlEmailbody.Append("<td>" + (Trip.Crew.LastName + (Trip.Crew.FirstName != null ? "," + Trip.Crew.FirstName : "") + Trip.Crew.MiddleInitial) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");

                htmlEmailbody.Append("<td><strong>Dispatcher email</strong></td>");
                if (Trip.UserMaster != null)
                    htmlEmailbody.Append("<td>" + (Trip.UserMaster.EmailID != null ? Trip.UserMaster.EmailID : string.Empty) + "</td>");
                else
                    htmlEmailbody.Append("<td>&nbsp;</td>");

                htmlEmailbody.Append("</tr>");
                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td><strong>Flight Number</strong></td>");
                htmlEmailbody.Append("<td>" + (Trip.FlightNUM != null ? Trip.FlightNUM : string.Empty) + "</td>");

                htmlEmailbody.Append("<td><strong>Revision Number</strong></td>");
                htmlEmailbody.Append("<td>" + (Trip.RevisionNUM != null ? Trip.RevisionNUM.ToString() : string.Empty) + "</td>");
                htmlEmailbody.Append("</tr>");
                htmlEmailbody.Append("<tr>");
                htmlEmailbody.Append("<td><strong>Dispatch Number</strong></td>");
                htmlEmailbody.Append("<td>" + (Trip.DispatchNUM != null ? Trip.DispatchNUM.ToString() : string.Empty) + "</td>");
                htmlEmailbody.Append("<td><strong>Trip Status</strong></td>");
                string StatusStr = string.Empty;
                switch (Trip.TripStatus)
                {
                    case "W": StatusStr = "W-Worksheet"; break;
                    case "T": StatusStr = "T-Tripsheet"; break;
                    case "U": StatusStr = "U-Unfulfilled"; break;
                    case "X": StatusStr = "X-Cancelled"; break;
                    case "H": StatusStr = "H-Hold"; break;
                }

                htmlEmailbody.Append("<td>" + StatusStr + "</td>");
                htmlEmailbody.Append("</tr>");
                htmlEmailbody.Append("</table></td>");
                htmlEmailbody.Append("</tr>");

                if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    List<PreflightLeg> prefleg = new List<PreflightLeg>();
                    prefleg = Trip.PreflightLegs.OrderBy(x => x.LegNUM).ToList();
                    if (prefleg.Count > 0)
                    {
                        foreach (PreflightLeg leg in prefleg)
                        {
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td bgcolor='#f5dd88' style='border-bottom: #666666 1px solid;' >");
                            htmlEmailbody.Append("<TABLE border='0' align='center' width='750px'>");
                            htmlEmailbody.Append("<TR>");
                            htmlEmailbody.Append("<TD align='left'><p style='font-family: arial,  helvetica, sans-serif;font-size: 16px;color: #3366cc;'>Leg " + leg.LegNUM + " Information</p></TD>");
                            htmlEmailbody.Append("</TR>");
                            htmlEmailbody.Append("</TABLE>");
                            htmlEmailbody.Append("</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;' ><table width='750' style='font: 12px Arial, Helvetica, sans-serif; color:#444;' align='center'>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>From</strong></td>");
                            if (leg.Airport1 != null)
                                htmlEmailbody.Append("<td colspan='3' >" + (leg.Airport1.IcaoID != null ? leg.Airport1.IcaoID : string.Empty) + " - " + (leg.Airport1.AirportName != null ? leg.Airport1.AirportName : string.Empty) + ", " + (leg.Airport1.CityName != null ? leg.Airport1.CityName : string.Empty) + ", " + (leg.Airport1.StateName != null ? leg.Airport1.StateName : string.Empty) + ", " + (leg.Airport1.CountryName != null ? leg.Airport1.CountryName : string.Empty) + " </td>");
                            else
                                htmlEmailbody.Append("<td colspan='3' >&nbsp;</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>To</strong></td>");
                            if (leg.Airport != null)
                                htmlEmailbody.Append("<td colspan='3' >" + (leg.Airport.IcaoID != null ? leg.Airport.IcaoID : string.Empty) + " - " + (leg.Airport.AirportName != null ? leg.Airport.AirportName : string.Empty) + ", " + (leg.Airport.CityName != null ? leg.Airport.CityName : string.Empty) + ", " + (leg.Airport.StateName != null ? leg.Airport.StateName : string.Empty) + ", " + (leg.Airport.CountryName != null ? leg.Airport.CountryName : string.Empty) + " </td>");
                            else
                                htmlEmailbody.Append("<td colspan='3' >&nbsp;</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td width='20%'class='style1'><strong>Local</strong></td>");
                            if (leg.DepartureDTTMLocal != null)
                                htmlEmailbody.Append("<td width='30%'>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.DepartureDTTMLocal) + " </td>");
                            else
                                htmlEmailbody.Append("<td width='30%'>&nbsp;</td>");
                            htmlEmailbody.Append("<td width='20%' class='style2'><strong>Zulu</strong></td>");
                            if (leg.DepartureGreenwichDTTM != null)
                                htmlEmailbody.Append("<td width='30%'>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.DepartureGreenwichDTTM) + " </td>");
                            else
                                htmlEmailbody.Append("<td width='30%'>&nbsp;</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>Local</strong></td>");
                            if (leg.ArrivalDTTMLocal != null)
                                htmlEmailbody.Append("<td width='30%'>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.ArrivalDTTMLocal) + " </td>");
                            else
                                htmlEmailbody.Append("<td width='30%'>&nbsp;</td>");

                            htmlEmailbody.Append("<td class='style2'><strong>Zulu</strong></td>");
                            if (leg.ArrivalGreenwichDTTM != null)
                                htmlEmailbody.Append("<td width='30%'>" + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.ArrivalGreenwichDTTM) + " </td>");
                            else
                                htmlEmailbody.Append("<td width='30%'>&nbsp;</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>Leg Purpose</strong></td>");
                            htmlEmailbody.Append("<td>" + (leg.FlightPurpose != null ? leg.FlightPurpose : string.Empty) + "</td>");
                            htmlEmailbody.Append("<td class='style2'><strong>Pax</strong></td>");
                            if (leg.PassengerTotal != null)
                                htmlEmailbody.Append("<td>" + leg.PassengerTotal + "</td>");
                            else
                                htmlEmailbody.Append("<td>&nbsp;</td>");

                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>Leg Requestor</strong></td>");
                            if (leg.Passenger != null)
                                htmlEmailbody.Append("<td>" + (leg.Passenger.PassengerName != null ? leg.Passenger.PassengerName : string.Empty) + "</td>");
                            else
                                htmlEmailbody.Append("<td>&nbsp;</td>");

                            htmlEmailbody.Append("<td class='style2'><strong>Distance</strong></td>");
                            if (leg.Distance != null)
                                htmlEmailbody.Append("<td>" + Math.Round((decimal)leg.Distance, 2).ToString() + "</td>");
                            else
                                htmlEmailbody.Append("<td>&nbsp;</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>Department</strong></td>");

                            if (leg.Department != null)
                                htmlEmailbody.Append("<td>" + (leg.Department.DepartmentName != null ? leg.Department.DepartmentName : string.Empty) + "</td>");
                            else
                                htmlEmailbody.Append("<td>&nbsp;</td>");
                            htmlEmailbody.Append("<td class='style2'><strong>ETE</strong></td>");
                            if (leg.ElapseTM != null)
                                htmlEmailbody.Append("<td> " + Math.Round((decimal)leg.ElapseTM, 2).ToString() + " </td>");
                            else
                                htmlEmailbody.Append("<td> </td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>Authorization</strong></td>");
                            if (leg.DepartmentAuthorization != null)
                                htmlEmailbody.Append("<td>" + (leg.DepartmentAuthorization.DeptAuthDescription != null ? leg.DepartmentAuthorization.DeptAuthDescription : string.Empty) + "</td>");
                            else
                                htmlEmailbody.Append("<td>&nbsp;</td>");
                            htmlEmailbody.Append("<td class='style2'><strong>Winds</strong></td>");
                            if (leg.WindsBoeingTable != null)
                                htmlEmailbody.Append("<td> " + Math.Round((decimal)leg.WindsBoeingTable, 2).ToString() + "</td>");
                            else
                                htmlEmailbody.Append("<td></td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td class='style1'><strong>Flight Category</strong></td>");
                            if (leg.FlightCatagory != null)
                                htmlEmailbody.Append("<td> " + (leg.FlightCatagory.FlightCatagoryCD != null ? leg.FlightCatagory.FlightCatagoryCD : string.Empty) + " -" + (leg.FlightCatagory.FlightCatagoryDescription != null ? leg.FlightCatagory.FlightCatagoryDescription : string.Empty) + "</td>");
                            else
                                htmlEmailbody.Append("<td></td>");
                            htmlEmailbody.Append("<td class='style2'><strong>FAR</strong></td>");
                            htmlEmailbody.Append("<td>" + leg.FedAviationRegNUM + "</td>");
                            htmlEmailbody.Append("</tr>");
                            htmlEmailbody.Append("</table></td>");
                            htmlEmailbody.Append("</tr>");
                        }
                    }
                }
                htmlEmailbody.Append("</table>");
                htmlEmailbody.Append("</body>");
                htmlEmailbody.Append("</html>");

                //SendEmailByList(EmailList, Emailsubject, htmlEmailbody);
            }
        }

        private void SendEmailByList(List<string> EmailList, string Emailsubject, StringBuilder htmlEmailbody)
        {
            foreach (string emailadd in EmailList)
            {
                FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                {
                    //Email logic has been modified by Vishwa on 05-02-2013
                    CustomerID = CustomerID,
                    EmailBody = htmlEmailbody.ToString(),
                    EmailReqTime = DateTime.UtcNow,
                    EmailReqUID = UserName,
                    EmailSentTo = emailadd,
                    EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                    EmailSubject = Emailsubject,
                    EmailTmplID = null,
                    EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                    IsBodyHTML = true,
                }, null);
                //EmailManager.SendMail(emailadd, "", htmlEmailbody.ToString(), Emailsubject, true);
                //MailAddressCollection mailcol = new MailAddressCollection();
                //mailcol.Add("prabhud@gavsin.com");
                //EmailManager.SendMeetingRequest(DateTime.Now, DateTime.Now, "test", "", "", "", "", mailcol, true);
            }
        }

        public void SendTripDetailsByCrew(Crew TripCrew, PreflightMain Trip, string emailadd, string emailType, List<long?> LegIDs)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, Trip, emailadd))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    PreflightDataModelContainer cs;
                    var htmlEmailbody = new StringBuilder();
                    if (emailadd != string.Empty)
                    {
                        string tripStatus = "U";
                        if (Trip.State == TripEntityState.Added) tripStatus = "I";

                        string strLegMinDate = string.Empty;
                        string strLegMaxDate = string.Empty;
                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                        {
                            DateTime legMinDate = DateTime.MinValue;
                            DateTime legMaxDate = DateTime.MinValue;

                            foreach (PreflightLeg Leg in Trip.PreflightLegs.Where(x => x.IsDeleted == false).ToList())
                            {
                                if (TripCrew.BlackBerryTM == "L")
                                {
                                    if (Leg.DepartureDTTMLocal != null)
                                    {
                                        if (legMinDate == DateTime.MinValue || legMinDate > (DateTime)Leg.DepartureDTTMLocal) legMinDate = (DateTime)Leg.DepartureDTTMLocal;
                                    }
                                    if (Leg.ArrivalDTTMLocal != null)
                                    {
                                        if (legMaxDate == DateTime.MinValue  || legMaxDate < (DateTime)Leg.ArrivalDTTMLocal) legMaxDate = (DateTime)Leg.ArrivalDTTMLocal;
                                    }
                                }
                                else
                                {
                                    if (Leg.DepartureGreenwichDTTM != null)
                                    {
                                        if (legMinDate == DateTime.MinValue || legMinDate > (DateTime)Leg.DepartureGreenwichDTTM) legMinDate = (DateTime)Leg.DepartureGreenwichDTTM;
                                    }
                                    if (Leg.ArrivalGreenwichDTTM != null)
                                    {
                                        if (legMaxDate == DateTime.MinValue || legMaxDate < (DateTime)Leg.ArrivalGreenwichDTTM) legMaxDate = (DateTime)Leg.ArrivalGreenwichDTTM;
                                    }
                                }
                            }

                            if (TripCrew.IsHomeArrivalTM == true && tripStatus=="I")
                            {
                                strLegMinDate = string.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", legMinDate);
                            }
                            if (TripCrew.IsHomeDEPARTTM == true && tripStatus == "I")
                            {
                                strLegMaxDate = string.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", legMaxDate);
                                if (strLegMinDate != string.Empty) strLegMaxDate = " - " + strLegMaxDate;
                            }

                            if (TripCrew.IsArrivalDepartTime == true && tripStatus == "U")
                            {
                                strLegMinDate = string.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", legMinDate);
                            }
                            else
                            {
                                strLegMinDate = string.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", legMinDate);
                            }

                            if (TripCrew.IsArrivalDepartTime == true && tripStatus == "U")
                            {
                                strLegMaxDate = string.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", legMaxDate);
                                if (strLegMinDate != string.Empty) strLegMaxDate = " - " + strLegMaxDate;
                            }
                            else
                            {
                                strLegMaxDate = string.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", legMaxDate);
                                if (strLegMinDate != string.Empty) strLegMaxDate = " - " + strLegMaxDate;
                            }
                        }
                        string subject = string.Empty;
                        string content = "";
                        if (!string.IsNullOrEmpty(emailType) && emailType.Contains(FlightPak.Common.Constants.EmailSubject.UnAssignedTrip))
                        {
                            subject = FlightPak.Common.Constants.EmailSubject.UnAssignedTrip;
                            content = FlightPak.Common.Constants.EmailSubject.SubjectUnAssignTrip;
                        }
                        if (!string.IsNullOrEmpty(emailType) && emailType.Contains(FlightPak.Common.Constants.EmailSubject.UpdateTrip))
                        {
                            subject = FlightPak.Common.Constants.EmailSubject.UpdateTrip;
                            content =FlightPak.Common.Constants.EmailSubject.SubjectUpdateTrip;
                        }

                        
                        string emailsubject = string.Format("Flight Scheduling Software - {0} Trip No.: {1}",subject , Trip.TripNUM);
                        htmlEmailbody.AppendLine(content);
                        htmlEmailbody.Append("<html>");
                        htmlEmailbody.Append("<head>");
                        htmlEmailbody.Append("</head>");
                        htmlEmailbody.Append("<body>");
                        htmlEmailbody.Append("<table align='center' width='800' style='border: #666666 1px solid;' cellpadding='0' cellspacing='0'>");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td bgcolor='#E5F7FF' style='border-bottom: #666666 1px solid;' >");
                        htmlEmailbody.Append("<table border='0' align='center' style='width: 100%;'>");
                        if (Trip.Company.CompanyName != string.Empty)
                        {
                            htmlEmailbody.Append("<tr>");
                            htmlEmailbody.Append("<td align='center'><p style='font-family: arial,  helvetica, sans-serif;font-size: 20px;color: #3366cc;'>");
                            htmlEmailbody.Append(Trip.Company.CompanyName + " </p></td>");
                            htmlEmailbody.Append("</tr>");
                        }
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td align='center'><p style='font-family: arial,  helvetica, sans-serif;font-size: 20px;color: #3366cc;'>");
                        htmlEmailbody.Append("TRIPSHEET</p></td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");

                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;' >");
                        htmlEmailbody.Append("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color:#444;' align='center' >");
                        htmlEmailbody.Append("<tr>");
                        htmlEmailbody.Append("<td>");

                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Trip No.", Trip.TripNUM != null ? Trip.TripNUM.ToString() : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Description", Trip.TripDescription ?? string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Trip Dates", strLegMinDate + strLegMaxDate));
                        if (TripCrew.IsStatusT == true)
                        {
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Status", Trip.TripStatus));
                        }
                        if (TripCrew.IsRequestPhoneNum == true)
                        {
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Requestor", Trip.Passenger != null ? (Trip.Passenger.PassengerName ?? string.Empty) : string.Empty));
                        }
                        
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Tail No.", Trip.Fleet != null ? (Trip.Fleet.TailNum ?? string.Empty) : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Type", Trip.Aircraft != null ? (Trip.Aircraft.AircraftCD ?? string.Empty) : string.Empty));

                        if (TripCrew.IsRequestPhoneNum == true)
                        {
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Contact Phone", Trip.Passenger != null ? (Trip.Passenger.AdditionalPhoneNum ?? string.Empty) : string.Empty));
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Home Phone", Trip.Passenger != null ? (Trip.Passenger.PhoneNum ?? string.Empty) : string.Empty));
                        }
                        
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Released By", Trip.Crew != null ? Trip.Crew.LastName + (Trip.Crew.FirstName != null ? "," + Trip.Crew.FirstName : string.Empty) + Trip.Crew.MiddleInitial : string.Empty));

                        if ((TripCrew.IsAircraft == true && tripStatus == "U") || tripStatus == "I")
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Aircraft Dom Flight Phone", Trip.Fleet != null ? (Trip.Fleet.FlightPhoneNum ?? string.Empty) : string.Empty));

                        if (TripCrew.IsAccountNum == true)
                        {
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Account No.", Trip.Account != null ? (Trip.Account.AccountNum ?? string.Empty) : string.Empty));
                        }
                        
                        if ((TripCrew.IsAircraft == true && tripStatus == "U") || tripStatus == "I")
                        {
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Flight No.", Trip.FlightNUM ?? string.Empty));
                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Aircraft Intl Flight Phone", Trip.Fleet != null ? (Trip.Fleet.FlightPhoneIntlNum ?? string.Empty) : string.Empty));
                        }
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Dispatch No.", Trip.DispatchNUM ?? string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Dispatcher Name", Trip.UserMaster != null ? (Trip.UserMaster.FirstName + (string.IsNullOrWhiteSpace(Trip.UserMaster.MiddleName) ? string.Empty : (" " + Trip.UserMaster.MiddleName)) + (string.IsNullOrWhiteSpace(Trip.UserMaster.LastName) ? string.Empty : (" " + Trip.UserMaster.LastName))) : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Dispatcher Phone", Trip.UserMaster != null ? (Trip.UserMaster.PhoneNum ?? string.Empty) : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Dispatcher Email", Trip.UserMaster != null ? (Trip.UserMaster.EmailID ?? string.Empty) : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Verification No.", Trip.VerifyNUM ?? string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Revision No.", Trip.RevisionNUM != null ? Trip.RevisionNUM.ToString() : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Customer Name", Trip.CQCustomer != null ? (Trip.CQCustomer.CQCustomerName ?? string.Empty) : string.Empty));
                        htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Customer Contact", Trip.CQCustomer != null ? (Trip.CQCustomer.BillingPhoneNum ?? string.Empty) : string.Empty));

                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</td>");
                        htmlEmailbody.Append("</tr>");
                        
                        if (Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                        {
                            List<PreflightLeg> prefleg = new List<PreflightLeg>();
                            prefleg = Trip.PreflightLegs.OrderBy(x => x.LegNUM).ToList();
                            if (prefleg.Count > 0)
                            {
                                List<spFlightPak_GetPreflightLegForTripReport_Result> rptLegs = new List<spFlightPak_GetPreflightLegForTripReport_Result>();
                                using (cs = new PreflightDataModelContainer())
                                {
                                    cs.ContextOptions.LazyLoadingEnabled = false;
                                    cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                                    cs.ContextOptions.ProxyCreationEnabled = false;
                                    rptLegs = cs.spFlightPak_GetPreflightLegForTripReport(Trip.LastUpdUID, Trip.TripID.ToString(), "").ToList();
                                }
                              
                                foreach (PreflightLeg leg in prefleg)
                                {
                                    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leg))
                                    {
                                        //Handle methods throguh exception manager with return flag
                                        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                                        exManager.Process(() =>
                                        {
                                            spFlightPak_GetPreflightLegForTripReport_Result rptLeg = rptLegs.Where(l => l.LegId.ToString().Trim() == leg.LegID.ToString().Trim()).FirstOrDefault();
                                           
                                            htmlEmailbody.Append("<tr>");
                                            htmlEmailbody.Append("<td bgcolor='#f5dd88' style='border-bottom: #666666 1px solid;' >");
                                            htmlEmailbody.Append("<table border='0' align='center' width='750px'>");
                                            htmlEmailbody.Append("<tr>");
                                            htmlEmailbody.Append("<td align='left'><p style='font-family: arial,  helvetica, sans-serif;font-size: 14px;color: #3366cc;'>Leg " + leg.LegNUM + " Information</p></td>");
                                            htmlEmailbody.Append("</tr>");
                                            htmlEmailbody.Append("</table>");
                                            htmlEmailbody.Append("</td>");
                                            htmlEmailbody.Append("</tr>");

                                            // Line ***
                                            htmlEmailbody.Append("<tr>");
                                            htmlEmailbody.Append("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;' >");
                                            htmlEmailbody.Append("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color:#444;' align='center'>");

                                            if (TripCrew.IsAirport == true)
                                            {
                                                htmlEmailbody.Append("<tr>");
                                                htmlEmailbody.Append("<td><strong>From:</strong></td><td></td>");
                                                if (leg.Airport1 != null)
                                                    htmlEmailbody.Append("<td colspan='4'>" + ((TripCrew.IcaoID == true && tripStatus == "U") ||  tripStatus == "I" ? (leg.Airport1.IcaoID != null ? leg.Airport1.IcaoID.ToString() : string.Empty) + " - " : string.Empty) + (leg.Airport1.AirportName != null ? leg.Airport1.AirportName.ToString() : string.Empty) + ", " + (leg.Airport1.CityName != null ? leg.Airport1.CityName : string.Empty) + ", " + (leg.Airport1.StateName != null ? leg.Airport1.StateName.ToString() : string.Empty) + ", " + (leg.Airport1.CountryName != null ? leg.Airport1.CountryName.ToString() : string.Empty) + " </td>");
                                                else
                                                    htmlEmailbody.Append("<td colspan='4'></td>");
                                                htmlEmailbody.Append("</tr>");

                                                // Line ***
                                                htmlEmailbody.Append("<tr>");
                                                htmlEmailbody.Append("<td><strong>To:</strong></td><td></td>");
                                                if (leg.Airport != null)
                                                    htmlEmailbody.Append("<td colspan='4'>" + ((TripCrew.IcaoID == true  && tripStatus == "U")  || tripStatus == "I" ? (leg.Airport.IcaoID != null ? leg.Airport.IcaoID.ToString() : string.Empty) + " - " : string.Empty) + (leg.Airport.AirportName != null ? leg.Airport.AirportName.ToString() : string.Empty) + ", " + (leg.Airport.CityName.ToString() != null ? leg.Airport.CityName : string.Empty) + ", " + (leg.Airport.StateName != null ? leg.Airport.StateName.ToString() : string.Empty) + ", " + (leg.Airport.CountryName != null ? leg.Airport.CountryName.ToString() : string.Empty) + " </td>");
                                                else
                                                    htmlEmailbody.Append("<td colspan='4'>&nbsp;</td>");
                                                htmlEmailbody.Append("</tr>");
                                            }

                                            // Line ***
                                            htmlEmailbody.Append("<tr>");
                                            htmlEmailbody.Append("<td colspan='6'>");

                                            //leg.PreflightCheckLists 

                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Local", leg.DepartureDTTMLocal != null ? String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.DepartureDTTMLocal) : string.Empty));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("UTC", leg.DepartureGreenwichDTTM != null ? String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.DepartureGreenwichDTTM) : string.Empty));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Local", leg.ArrivalDTTMLocal != null ? String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.ArrivalDTTMLocal) : string.Empty));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("UTC", leg.ArrivalGreenwichDTTM != null ? String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", leg.ArrivalGreenwichDTTM) : string.Empty));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Leg Purpose", leg.FlightPurpose ?? string.Empty));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Pax", leg.PassengerTotal != null ? leg.PassengerTotal.ToString() : string.Empty));
                                            if (TripCrew.IsRequestPhoneNum == true)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Leg Requestor", leg.Passenger != null ? (leg.Passenger.PassengerName ?? string.Empty) : string.Empty));
                                            }

                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Distance", commPreFlight.ConvertToKilomenterBasedOnCompanyProfile(Convert.ToDecimal(leg.Distance), UserPrincipal.Identity.Settings.IsKilometer).ToString()));

                                            if (TripCrew.IsDepartmentAuthorization == true){
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Department", leg.Department != null ? (leg.Department.DepartmentName ?? string.Empty) : string.Empty));
                                            }

                                            double ETE = 0.0;
                                            string vETE = string.Empty;
                                            if (leg.ElapseTM != null)
                                            {
                                                ETE = commPreFlight.RoundElpTime((double)leg.ElapseTM, UserPrincipal.Identity.Settings.TimeDisplayTenMin, UserPrincipal.Identity.Settings.ElapseTMRounding);
                                                if (UserPrincipal.Identity.Settings.TimeDisplayTenMin != null && UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    vETE = commPreFlight.Preflight_ConvertTenthsToMins(ETE.ToString(), UserPrincipal.Identity.Settings.TimeDisplayTenMin);
                                                else
                                                {
                                                    vETE = Math.Round(ETE, 1).ToString();
                                                    if (vETE.IndexOf(".") < 0)
                                                        vETE = vETE + ".0";
                                                }
                                                vETE = FlightPak.Common.Utility.DefaultEteResult(vETE);
                                            }
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("ETE", vETE));

                                            if (TripCrew.IsDepartmentAuthorization == true)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Authorization", leg.DepartmentAuthorization != null ? (leg.DepartmentAuthorization.DeptAuthDescription ?? string.Empty) : string.Empty));
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Auth Phone", leg.DepartmentAuthorization != null ? (leg.DepartmentAuthorization.AuthorizerPhoneNum ?? string.Empty) : string.Empty));
                                            }
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Flight Category", leg.FlightCatagory != null ? (leg.FlightCatagory.FlightCatagoryCD ?? string.Empty) + " -" + (leg.FlightCatagory.FlightCatagoryDescription ?? string.Empty) : string.Empty));
                                            if (TripCrew.IsFAR == true)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("FAR", leg.FedAviationRegNUM));
                                            }

                                            if (TripCrew.IsEndDuty == true)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("End Duty", Math.Round(Convert.ToDecimal(leg.DutyHours), 0).ToString()));
                                            }
                                            if (TripCrew.IsOverride == true)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Override", Math.Round(Convert.ToDecimal(leg.OverrideValue), 0).ToString()));
                                            }

                                            if (TripCrew.IsCrewRules == true && leg.CrewDutyRule != null)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Crew Rules", leg.CrewDutyRule.CrewDutyRuleCD));
                                            }
                                            if (TripCrew.IsDuty == true && leg.CrewDutyRule != null)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Duty Hours", Math.Round(Convert.ToDecimal(leg.CrewDutyRule.MaximumDutyHrs), 0).ToString()));
                                            }
                                            if (TripCrew.IsFlightHours == true && leg.CrewDutyRule != null)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Flight Hours", Math.Round(Convert.ToDecimal(leg.CrewDutyRule.MaximumFlightHrs), 0).ToString()));
                                            }
                                            if (TripCrew.IsRestHrs == true && leg.CrewDutyRule != null)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Rest Hours", Math.Round(Convert.ToDecimal(leg.CrewDutyRule.RestMultipleHrs), 0).ToString()));
                                            }

                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Winds", Math.Round(Convert.ToDecimal(leg.WindsBoeingTable), 2).ToString()));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Fuel Burn", Math.Round((decimal)rptLeg.FuelBurn, 0).ToString()));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Minimum Fuel", Math.Round((decimal)rptLeg.MinFuel, 0).ToString()));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Fuel Load", leg.FuelLoad == null ? Math.Round(Convert.ToDecimal(leg.FuelLoad), 0).ToString() : string.Empty));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Power Setting", leg.PowerSetting));
                                            htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("True Air Speed", Math.Round(Convert.ToDecimal(leg.TrueAirSpeed), 0).ToString()));

                                            if (TripCrew.IsAirport == true)
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Dep ATIS", leg.Airport1 != null ? (leg.Airport1.AtisPhoneNum ?? string.Empty) : string.Empty));
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("ATIS Freq.", leg.Airport1 != null ? (leg.Airport1.ATISFreq ?? string.Empty) : string.Empty));
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("Arr ATIS", leg.Airport != null ? (leg.Airport.AtisPhoneNum ?? string.Empty) : string.Empty));
                                                htmlEmailbody.AppendFormat("{0}", TripsheetHtmlCellEmail("ATIS Freq.", leg.Airport != null ? (leg.Airport.ATISFreq ?? string.Empty) : string.Empty));
                                            }
                                            htmlEmailbody.Append("</td>");
                                            htmlEmailbody.Append("</tr>");

                                            htmlEmailbody.Append("</table></td>");
                                            htmlEmailbody.Append("</tr>");

                                            
                                            // Line ***
                                            #region FBO
                                            if ((TripCrew.IsFBO == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                if (leg.PreflightFBOLists != null)
                                                {
                                                    string strFboHtml = string.Empty;
                                                    foreach (PreflightFBOList legFbo in leg.PreflightFBOLists)
                                                    {
                                                        ////To FBO
                                                        if (legFbo.IsArrivalFBO == true && TripCrew.IsArrivalFBO != null && TripCrew.IsArrivalFBO == true)
                                                        {
                                                            strFboHtml = strFboHtml + this.TripsheetFBOForCrewEmail(TripCrew, "Arr", legFbo, tripStatus);
                                                        }

                                                        ////From FBO
                                                        if (legFbo.IsDepartureFBO == true && TripCrew.IsDepartureFBO != null && TripCrew.IsDepartureFBO == true)
                                                        {
                                                            strFboHtml = strFboHtml + this.TripsheetFBOForCrewEmail(TripCrew, "Dep", legFbo, tripStatus);
                                                        }
                                                    }
                                                    htmlEmailbody.Append(strFboHtml);
                                                }
                                            }
                                            #endregion

                                            #region Pax Transport & Hotel Details
                                            if ((TripCrew.IsTransportation == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                if (leg.PreflightTransportLists != null)
                                                {
                                                    string strTranHtml = string.Empty;
                                                    foreach (PreflightTransportList legTran in leg.PreflightTransportLists)
                                                    {
                                                        ////Arr Transport PAX
                                                        if (legTran.IsArrivalTransport == true && legTran.CrewPassengerType == "P" && TripCrew.IsPassDepartTRANS != null && TripCrew.IsPassDepartTRANS == true)
                                                        {
                                                            strTranHtml = strTranHtml + this.TripsheetTransForCrewEmail(TripCrew, "Arr Pax", legTran, tripStatus);
                                                        }

                                                        ////Dep Transport PAX
                                                        if (legTran.IsDepartureTransport == true && legTran.CrewPassengerType == "P" && TripCrew.IsPassArrivalHotel != null && TripCrew.IsPassArrivalHotel == true)
                                                        {
                                                            strTranHtml = strTranHtml + this.TripsheetTransForCrewEmail(TripCrew, "Dep Pax", legTran, tripStatus);
                                                        }
                                                    }
                                                    htmlEmailbody.Append(strTranHtml);
                                                }
                                            }
                                            if ((TripCrew.IsHotel == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                if (leg.PreflightHotelLists != null)
                                                {
                                                    string strHotelHtml = string.Empty;
                                                    foreach (PreflightHotelList legHotel in leg.PreflightHotelLists)
                                                    {
                                                        ////Arr Hotel PAX
                                                        if (legHotel.isArrivalHotel == true && legHotel.crewPassengerType == "P" && TripCrew.IsPassHotel != null && TripCrew.IsPassHotel == true)
                                                        {
                                                            strHotelHtml = strHotelHtml + this.TripsheetHotelForCrewEmail(TripCrew, "Arr Pax", legHotel, tripStatus);
                                                        }

                                                        ////Dep Hotel PAX
                                                        if (legHotel.isDepartureHotel == true && legHotel.crewPassengerType == "P" && TripCrew.IsPassHotel != null && TripCrew.IsPassHotel == true)
                                                        {
                                                            strHotelHtml = strHotelHtml + this.TripsheetHotelForCrewEmail(TripCrew, "Dep Pax", legHotel, tripStatus);
                                                        }
                                                    }
                                                    htmlEmailbody.Append(strHotelHtml);
                                                }
                                            }
                                            #endregion

                                            #region Crew Transport & Hotel Details

                                            if ((TripCrew.IsTransportation == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                if (leg.PreflightTransportLists != null)
                                                {
                                                    string strTranHtml = string.Empty;
                                                    foreach (PreflightTransportList legTran in leg.PreflightTransportLists)
                                                    {
                                                        ////Arr Transport PAX
                                                        if (legTran.IsArrivalTransport == true && legTran.CrewPassengerType == "C" && TripCrew.IsCrewArrivalTRANS != null && TripCrew.IsCrewArrivalTRANS == true)
                                                        {
                                                            strTranHtml = strTranHtml + this.TripsheetTransForCrewEmail(TripCrew, "Arr Crew", legTran, tripStatus);
                                                        }

                                                        ////Dep Transport PAX
                                                        if (legTran.IsDepartureTransport == true && legTran.CrewPassengerType == "C" && TripCrew.IsCrewDepartTRANS != null && TripCrew.IsCrewDepartTRANS == true)
                                                        {
                                                            strTranHtml = strTranHtml + this.TripsheetTransForCrewEmail(TripCrew, "Dep Crew", legTran, tripStatus);
                                                        }
                                                    }
                                                    htmlEmailbody.Append(strTranHtml);
                                                }
                                            }
                                            if ((TripCrew.IsHotel == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                if (leg.PreflightHotelLists != null)
                                                {
                                                    string strHotelHtml = string.Empty;
                                                    foreach (PreflightHotelList legHotel in leg.PreflightHotelLists)
                                                    {
                                                        ////Arr Transport PAX
                                                        if (legHotel.isArrivalHotel == true && legHotel.crewPassengerType == "C" && TripCrew.IsHotel != null && TripCrew.IsHotel == true)
                                                        {
                                                            strHotelHtml = strHotelHtml + this.TripsheetHotelForCrewEmail(TripCrew, "Arr Crew", legHotel, tripStatus);
                                                        }

                                                        ////Dep Transport PAX
                                                        if (legHotel.isDepartureHotel == true && legHotel.crewPassengerType == "C" && TripCrew.IsHotel != null && TripCrew.IsHotel == true)
                                                        {
                                                            strHotelHtml = strHotelHtml + this.TripsheetHotelForCrewEmail(TripCrew, "Dep Crew", legHotel, tripStatus);
                                                        }
                                                    }
                                                    htmlEmailbody.Append(strHotelHtml);
                                                }
                                            }

                                            #endregion

                                            #region Catering Details
                                            if ((TripCrew.IsCatering == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                if (leg.PreflightCateringDetails != null)
                                                {
                                                    string strCateringHtml = string.Empty;
                                                    foreach (PreflightCateringDetail legCaterer in leg.PreflightCateringDetails)
                                                    {
                                                        ////Arr Catering PAX
                                                        if (legCaterer.ArriveDepart == "A" && TripCrew.IsCatering != null && TripCrew.IsCatering == true)
                                                        {
                                                            strCateringHtml = strCateringHtml + this.TripsheetCateringForCrewEmail(TripCrew, "Arr", legCaterer, tripStatus);
                                                        }
                                                        ////Dep Catering PAX
                                                        if (legCaterer.ArriveDepart == "D" && TripCrew.IsCatering != null && TripCrew.IsCatering == true)
                                                        {
                                                            strCateringHtml = strCateringHtml + this.TripsheetCateringForCrewEmail(TripCrew, "Dep", legCaterer, tripStatus);
                                                        }
                                                    }
                                                    htmlEmailbody.Append(strCateringHtml);
                                                }
                                            }
                                            #endregion

                                            #region Leg Notes
                                            htmlEmailbody.AppendLine("<tr>");
                                            htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                                            htmlEmailbody.AppendLine("<table border='0' align='center' width='750px'>");
                                            htmlEmailbody.AppendLine("<tr>");
                                            htmlEmailbody.AppendLine("<td align='center'>");
                                            htmlEmailbody.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>Leg Notes</p>");
                                            htmlEmailbody.AppendLine("</td>");
                                            htmlEmailbody.AppendLine("</tr>");
                                            htmlEmailbody.AppendLine("</table>");
                                            htmlEmailbody.AppendLine("</td>");
                                            htmlEmailbody.AppendLine("</tr>");
                                            if ((TripCrew.IsCrewNotes == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid;'>");
                                                htmlEmailbody.AppendLine("<table border='0' align='center' width='750px' style='font: 12px Arial, Helvetica, sans-serif; color: #444;'>");
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td>");
                                                htmlEmailbody.AppendLine("Leg Notes");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("<td >");
                                                htmlEmailbody.AppendFormat("{0}", leg.CrewNotes ?? string.Empty);
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");
                                                htmlEmailbody.AppendLine("</table>");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");
                                            }
                                            if ((TripCrew.IsPassNotes == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid;'>");
                                                htmlEmailbody.AppendLine("<table border='0' align='center' width='750px' style='font: 12px Arial, Helvetica, sans-serif; color: #444;'>");
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td>");
                                                htmlEmailbody.AppendLine("Leg Notes");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("<td >");
                                                htmlEmailbody.AppendFormat("{0}", leg.Notes ?? string.Empty);
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");
                                                htmlEmailbody.AppendLine("</table>");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");
                                            }
                                            #endregion

                                            #region OutbountInstruction 
                                            if ((TripCrew.IsOutboundINST == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetOutboundForCrewEmail(TripCrew, "Outbound Instructions", leg));
                                            }
                                            #endregion
                                           
                                            #region Passenger Detail List

                                            if ((TripCrew.IsPassenger == true && tripStatus == "U") || tripStatus == "I")
                                            {
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                                                htmlEmailbody.AppendLine("<table border='0' align='center' width='750px'>");
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td align='center'>");
                                                htmlEmailbody.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>Passenger Notes</p>");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");
                                                htmlEmailbody.AppendLine("</table>");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");

                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid;'>");
                                                htmlEmailbody.AppendLine("<table border='0' align='center' width='750px' style='font: 12px Arial, Helvetica, sans-serif; color: #444;'>");
                                                htmlEmailbody.AppendLine("<tr>");
                                                htmlEmailbody.AppendLine("<td align='center'>");
                                                if (leg.Notes != null) htmlEmailbody.AppendLine(leg.Notes.ToString());
                                                else htmlEmailbody.AppendLine("&nbsp;");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");
                                                htmlEmailbody.AppendLine("</table>");
                                                htmlEmailbody.AppendLine("</td>");
                                                htmlEmailbody.AppendLine("</tr>");

                                                if (leg.PreflightPassengerLists != null)
                                                {
                                                    StringBuilder paxHtml = new StringBuilder();

                                                    paxHtml.AppendLine("<tr>");
                                                    paxHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                                                    paxHtml.AppendLine("<table width='650' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");
                                                    paxHtml.AppendLine("<tr>");
                                                    paxHtml.AppendLine("<td style='width: 35px'><strong>No.</strong></td>");
                                                    paxHtml.AppendLine("<td style='width: 85px'><strong>CNG</strong></td>");
                                                    paxHtml.AppendLine("<td style='width: 65px'><strong>CODE</strong></td>");
                                                    paxHtml.AppendLine("<td><strong>Passenger Name</strong></td>");
                                                    paxHtml.AppendLine("<td style='width: 100px'><strong>" + leg.LegNUM + "</strong></td>");
                                                    paxHtml.AppendLine("</tr>");
                                                    int paxCount = 1;

                                                    foreach (PreflightPassengerList legPAX in leg.PreflightPassengerLists)
                                                    {
                                                        PreflightDataModelContainer Container;
                                                        using (Container = new PreflightDataModelContainer())
                                                        {
                                                            IQueryable<Passenger> Passengers = preflightCompiledQuery.getPassenger(Container, (long)legPAX.PassengerID, CustomerID);
                                                            Passenger TripPax = new Passenger();
                                                            if (Passengers != null && Passengers.ToList().Count > 0) TripPax = Passengers.ToList()[0];
                                                            if (TripPax != null)
                                                            {
                                                                paxHtml.AppendLine("<tr>");
                                                                paxHtml.AppendLine("<td style='vertical-align:top'>" + paxCount + "</td>");
                                                                paxHtml.AppendLine("<td style='vertical-align:top'>" + TripPax.IsEmployeeType + " </td>");
                                                                paxHtml.AppendLine("<td style='vertical-align:top'>" + TripPax.PassengerRequestorCD + " </td>");
                                                                paxHtml.AppendLine("<td style='vertical-align:top'>" + TripPax.LastName + " " + TripPax.FirstName + " " + TripPax.MiddleInitial);
                                                                if (TripPax.Department != null)
                                                                {
                                                                    paxHtml.AppendLine("<br />Dept:" + TripPax.Department.DepartmentName);
                                                                }
                                                                if (TripPax.PhoneNum != null && TripCrew.IsPassengerPhoneNum != null && TripCrew.IsPassengerPhoneNum != true)
                                                                {
                                                                    paxHtml.AppendLine("<br />Phone:" + TripPax.PhoneNum);
                                                                }
                                                                if (TripPax.AdditionalPhoneNum != null)
                                                                {
                                                                    paxHtml.AppendLine("<br />Addl Ph:" + TripPax.AdditionalPhoneNum);
                                                                }
                                                                foreach (CrewPassengerPassport pp in TripPax.CrewPassengerPassports)
                                                                {
                                                                    paxHtml.AppendLine("<br />Passport No.:" + Crypting.Decrypt(pp.PassportNum));
                                                                }
                                                                if (TripPax.CountryID != null)
                                                                {
                                                                    paxHtml.AppendLine("<br />" + TripPax.CountryID);
                                                                }
                                                                if (TripPax.DateOfBirth != null)
                                                                {
                                                                    paxHtml.AppendLine("<br />DOB:" + Convert.ToDateTime(TripPax.DateOfBirth).ToString("MM/dd/yyyy"));
                                                                }

                                                                paxHtml.AppendLine("</td>");

                                                                if (TripPax.FlightPurpose != null)
                                                                {
                                                                    paxHtml.AppendLine("<td style='vertical-align:top'>" + TripPax.FlightPurpose.FlightPurposeCD + "</td>");
                                                                }
                                                                else
                                                                {
                                                                    paxHtml.AppendLine("<td style='vertical-align:top'></td>");
                                                                }
                                                                paxHtml.AppendLine("</tr>");
                                                            }
                                                        }
                                                        paxCount += 1;
                                                    }

                                                    paxHtml.AppendLine("</table>");
                                                    paxHtml.AppendLine("</td>");
                                                    paxHtml.AppendLine("</tr>");

                                                    htmlEmailbody.Append(paxHtml.ToString());
                                                }
                                            }
                                            #endregion

                                            #region Checklist
                                            if ((TripCrew.IsCheckList1 == true && tripStatus == "U") || (TripCrew.IsCheckList == true && tripStatus == "I"))
                                            {
                                                htmlEmailbody.AppendFormat("{0}", TripsheetChecklistForCrewEmail(TripCrew, "Checklist", leg));
                                            }
                                            #endregion

                                        }, FlightPak.Common.Constants.Policy.DataLayer);
                                    }
                                }
                            }

                            #region Crew Detail List

                            if ((TripCrew.IsCrew == true && tripStatus == "U") || tripStatus == "I" || !string.IsNullOrEmpty(emailType))
                            {
                                if (prefleg.Count > 0)
                                {
                                    StringBuilder crewHtml = new StringBuilder();
                                    crewHtml.AppendLine("<tr>");
                                    crewHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                                    crewHtml.AppendLine("<table border='0' align='center' width='750px'>");
                                    crewHtml.AppendLine("<tr>");
                                    crewHtml.AppendLine("<td align='center' style='width: 120px'>");
                                    crewHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>Trip Crew</p>");
                                    crewHtml.AppendLine("</td>");
                                    crewHtml.AppendLine("</tr>");
                                    crewHtml.AppendLine("</table>");
                                    crewHtml.AppendLine("</td>");
                                    crewHtml.AppendLine("</tr>");
                                    crewHtml.AppendLine("<tr>");
                                    crewHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                                    crewHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");
                                    crewHtml.AppendLine("<tr>");
                                    crewHtml.AppendLine("<td><strong>CODE</strong></td>");
                                    crewHtml.AppendLine("<td><strong>Crew Name</strong></td>");
                                    crewHtml.AppendLine("<td><strong>Mobile</strong></td>");
                                    crewHtml.AppendLine("<td><strong>Leg</strong></td>");
                                    crewHtml.AppendLine("<td><strong>Duty Type</strong></td>");
                                    crewHtml.AppendLine("</tr>");
                                    foreach (PreflightLeg leg in prefleg)
                                    {
                                        if (leg.PreflightCrewLists != null)
                                        {

                                            int paxCount = 1;
                                            if (!string.IsNullOrEmpty(emailType) && emailType.Contains(FlightPak.Common.Constants.EmailSubject.UnAssignedTrip))
                                            {
                                                PreflightCrewList objCrew = new PreflightCrewList();
                                                int legCount = leg.PreflightCrewLists.Where(x => x.CrewID == TripCrew.CrewID).Count();
                                                if (legCount == 0)
                                                {
                                                    if (LegIDs.Any(p => p.Value == leg.LegID))
                                                    {
                                                        objCrew.CrewID = TripCrew.CrewID;
                                                        leg.PreflightCrewLists.Add(objCrew);
                                                    }
                                                }

                                            }
                                            foreach (PreflightCrewList legCrew in leg.PreflightCrewLists)
                                            {
                                                PreflightDataModelContainer Container;
                                                using (Container = new PreflightDataModelContainer())
                                                {
                                                    IQueryable<Crew> Crews = preflightCompiledQuery.getCrew(Container,(long)legCrew.CrewID,CustomerID);

                                                    Crew htmlTripCrew = new Crew();
                                                    if (Crews != null && Crews.ToList().Count > 0) htmlTripCrew = Crews.ToList()[0];
                                                    if (htmlTripCrew != null)
                                                    {
                                                        //background red for deleted member
                                                        if (emailType.Contains(FlightPak.Common.Constants.EmailSubject.UnAssignedTrip) && TripCrew.CrewID == htmlTripCrew.CrewID && LegIDs.Any(p => p.Value == leg.LegID))
                                                            crewHtml.AppendLine("<tr style='background-color:red;'>");
                                                        else
                                                            crewHtml.AppendLine("<tr>");
                                                            crewHtml.AppendLine("<td style='vertical-align:top'>" + htmlTripCrew.CrewCD + "</td>");
                                                        crewHtml.AppendLine("<td style='vertical-align:top'>" + htmlTripCrew.LastName + " " + htmlTripCrew.FirstName + " " + htmlTripCrew.MiddleInitial);
                                                        foreach (CrewPassengerPassport pp in htmlTripCrew.CrewPassengerPassports)
                                                        {
                                                            crewHtml.AppendLine("<br />Passport No.:" + Crypting.Decrypt(pp.PassportNum));
                                                            crewHtml.AppendLine("<br />Expiry Date:" + Convert.ToDateTime(pp.PassportExpiryDT).ToString("MM/dd/yyyy"));
                                                        }
                                                        if (htmlTripCrew.CountryOfBirth != null)
                                                        {
                                                            crewHtml.AppendLine("<br />" + htmlTripCrew.CountryOfBirth);
                                                        }
                                                        if (htmlTripCrew.BirthDT != null)
                                                        {
                                                            crewHtml.AppendLine("<br />DOB:" + Convert.ToDateTime(htmlTripCrew.BirthDT).ToString("MM/dd/yyyy"));
                                                        }

                                                        crewHtml.AppendLine("</td>");
                                                        crewHtml.AppendLine("<td style='vertical-align:top'>"  + htmlTripCrew.CellPhoneNum + "</td>");
                                                        crewHtml.AppendLine("<td style='vertical-align:top'>" + leg.LegNUM + "</td>");
                                                        if (legCrew.DutyTYPE != null)
                                                        {
                                                            string strDutyType = string.Empty;
                                                            switch (legCrew.DutyTYPE)
                                                            {
                                                                case "P":
                                                                    strDutyType = "PILOT";
                                                                    break;
                                                                case "S":
                                                                    strDutyType = "PILOT";
                                                                    break;
                                                                default:
                                                                    strDutyType = "ADDL CREW";
                                                                    break;
                                                            }
                                                            crewHtml.AppendLine("<td style='vertical-align:top'>" + strDutyType + "</td>");
                                                        }
                                                        else
                                                        {
                                                            crewHtml.AppendLine("<td style='vertical-align:top'></td>");
                                                        }
                                                        crewHtml.AppendLine("</tr>");
                                                    }

                                                }
                                            }
                                            if (!string.IsNullOrEmpty(emailType) && emailType.Contains(FlightPak.Common.Constants.EmailSubject.UnAssignedTrip))
                                            {
                                                PreflightCrewList objCrew = new PreflightCrewList();
                                                int legCount = leg.PreflightCrewLists.Where(x => x.CrewID == TripCrew.CrewID && x.CustomerID == null).Count();
                                                if (legCount == 1 && LegIDs.Any(p => p.Value == leg.LegID))
                                                {
                                                    objCrew = leg.PreflightCrewLists.Where(r => r.CrewID == TripCrew.CrewID).FirstOrDefault();//TripCrew.CrewID;
                                                    if (objCrew != null)
                                                    {
                                                        leg.PreflightCrewLists.Remove(objCrew);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    crewHtml.AppendLine("</table>");
                                    crewHtml.AppendLine("</td>");
                                    crewHtml.AppendLine("</tr>");

                                    htmlEmailbody.Append(crewHtml.ToString());
                                }
                            }

                            #endregion

                            #region Approved No-Fly list

                            htmlEmailbody.AppendLine("<tr>");
                            htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                            htmlEmailbody.AppendLine("<table border='0' align='center' width='750px'>");
                            htmlEmailbody.AppendLine("<tr>");
                            htmlEmailbody.AppendLine("<td align='center'>");
                            htmlEmailbody.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>Approved No-Fly List</p>");
                            htmlEmailbody.AppendLine("</td>");
                            htmlEmailbody.AppendLine("</tr>");
                            htmlEmailbody.AppendLine("</table>");
                            htmlEmailbody.AppendLine("</td>");
                            htmlEmailbody.AppendLine("</tr>");

                            List<spFlightPak_GetPAXNoFlyInformationReport_Result> rptPax;
                    
                            using (cs = new PreflightDataModelContainer())
                            {
                                rptPax = new List<spFlightPak_GetPAXNoFlyInformationReport_Result>();
                                cs.ContextOptions.LazyLoadingEnabled = false;
                                cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                                cs.ContextOptions.ProxyCreationEnabled = false;
                                rptPax = cs.spFlightPak_GetPAXNoFlyInformationReport(Trip.LastUpdUID, Trip.TripID.ToString(), "", "10002175006", "FBOS::0||DISPATCHNO::1||PURPOSE::0").ToList();
                            }
                            if (rptPax != null)
                            {
                                StringBuilder paxHtml = new StringBuilder();

                                paxHtml.AppendLine("<tr>");
                                paxHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                                paxHtml.AppendLine("<table width='650' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");
                                paxHtml.AppendLine("<tr>");
                                paxHtml.AppendLine("<td><strong>Passenger Name</strong></td>");
                                paxHtml.AppendLine("<td style='width: 100px'><strong>CODE</strong></td>");
                                paxHtml.AppendLine("<td style='width: 100px'><strong>Leg</strong></td>");
                                paxHtml.AppendLine("<td style='width: 100px'><strong>Status</strong></td>");
                                paxHtml.AppendLine("</tr>");

                                foreach (spFlightPak_GetPAXNoFlyInformationReport_Result pPAX in rptPax)
                                {
                                    if (pPAX != null && pPAX.PassengerName != string.Empty)
                                    {
                                        paxHtml.AppendLine("<tr>");
                                        paxHtml.AppendLine("<td style='vertical-align:top'>" + pPAX.PassengerName + "</td>");
                                        paxHtml.AppendLine("<td style='vertical-align:top'>" + pPAX.Code + " </td>");
                                        paxHtml.AppendLine("<td style='vertical-align:top'>" + pPAX.LegNum + " </td>");
                                        paxHtml.AppendLine("<td style='vertical-align:top'>" + pPAX.Status + " </td>");
                                        paxHtml.AppendLine("</tr>");
                                    }
                                }

                                paxHtml.AppendLine("</table>");
                                paxHtml.AppendLine("</td>");
                                paxHtml.AppendLine("</tr>");

                                htmlEmailbody.Append(paxHtml.ToString());
                            }

                            #endregion

                            #region Notes and Announcements
                            htmlEmailbody.AppendLine("<tr>");
                            htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                            htmlEmailbody.AppendLine("<table border='0' align='center' width='750px'>");
                            htmlEmailbody.AppendLine("<tr>");
                            htmlEmailbody.AppendLine("<td align='center'>");
                            htmlEmailbody.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>Notes and Announcments</p>");
                            htmlEmailbody.AppendLine("</td>");
                            htmlEmailbody.AppendLine("</tr>");
                            htmlEmailbody.AppendLine("</table>");
                            htmlEmailbody.AppendLine("</td>");
                            htmlEmailbody.AppendLine("</tr>");
                            htmlEmailbody.AppendLine("<tr>");
                            htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid;'>");
                            htmlEmailbody.AppendLine("<table border='0' align='center' width='750px' style='font: 12px Arial, Helvetica, sans-serif; color: #444;'>");
                            htmlEmailbody.AppendLine("<tr>");
                            htmlEmailbody.AppendLine("<td align='center'>");
                            htmlEmailbody.AppendFormat("{0}", Trip.Company != null ? (Trip.Company.ApplicationMessage ?? string.Empty) : string.Empty);
                            htmlEmailbody.AppendLine("</td>");
                            htmlEmailbody.AppendLine("</tr>");
                            htmlEmailbody.AppendLine("</table>");
                            htmlEmailbody.AppendLine("</td>");
                            htmlEmailbody.AppendLine("</tr>");
                            #endregion

                            #region Cancellation Description

                            if (TripCrew.IsCancelDescription == true)
                            {
                                htmlEmailbody.AppendLine("<tr><td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'><table border='0' align='center' width='750px'><tr>");
                                htmlEmailbody.AppendLine("<td align='center'>");
                                htmlEmailbody.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>Cancellation Description</p>");
                                htmlEmailbody.AppendLine("</td></tr></table>");
                                htmlEmailbody.AppendLine("</td></tr>");
                                htmlEmailbody.AppendLine("<tr>");
                                htmlEmailbody.AppendLine("<td style='border-bottom: #666666 1px solid;'>");
                                htmlEmailbody.AppendLine("<table border='0' align='center' width='750px' style='font: 12px Arial, Helvetica, sans-serif; color: #444;'>");
                                htmlEmailbody.AppendLine("<tr>");
                                htmlEmailbody.AppendLine("<td align='center'>");
                                htmlEmailbody.AppendFormat("{0}", Trip.CancelDescription ?? string.Empty);
                                htmlEmailbody.AppendLine("</td></tr></table>");
                                htmlEmailbody.AppendLine("</td></tr>");
                            }

                            #endregion
                        }

                        htmlEmailbody.Append("</table>");
                        htmlEmailbody.Append("</body>");
                        htmlEmailbody.Append("</html>");

                        FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                        emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                        {
                            //Email logic has been modified by Vishwa on 05-02-2013
                            CustomerID = CustomerID,
                            EmailBody = htmlEmailbody.ToString(),
                            EmailReqTime = DateTime.UtcNow,
                            EmailReqUID = UserName,
                            EmailSentTo = emailadd,
                            EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                            EmailSubject = emailsubject,
                            EmailTmplID = null,
                            EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                            IsBodyHTML = true,
                        }, null);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        private string TripsheetFBOForCrewEmail(Crew TripCrew, string fboStatus, PreflightFBOList labFbo, string tripStatus)
        {
            StringBuilder fboHtml = new StringBuilder();
            try
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, fboStatus, labFbo))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        fboHtml.AppendLine("<tr>");
                        fboHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");

                        fboHtml.AppendLine("<table border='0' align='center' width='750px'>");
                        fboHtml.AppendLine("<tr>");
                        fboHtml.AppendLine("<td align='left'>");
                        fboHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>" + fboStatus + " FBO</p>");
                        fboHtml.AppendLine("</td>");
                        fboHtml.AppendLine("</tr>");
                        fboHtml.AppendLine("</table>");

                        fboHtml.AppendLine("</td>");
                        fboHtml.AppendLine("</tr>");

                        fboHtml.AppendLine("<tr>");
                        fboHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        fboHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");
                        fboHtml.AppendLine("<tr>");
                        fboHtml.AppendLine("<td><strong>Name:</strong></td>");
                        fboHtml.AppendLine("<td></td>");
                        fboHtml.AppendLine("<td colspan='4'>" + labFbo.PreflightFBOName + "</td>");
                        fboHtml.AppendLine("</tr>");

                        if (labFbo.FBO != null)
                        {

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td colspan='3'>");
                            fboHtml.AppendLine("<table border='0' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center' width='100%' cellpadding='0' cellspacing='0'>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td width='25px'><strong>Ph:</strong></td>");
                            fboHtml.AppendLine("<td width='150px'>" + labFbo.PhoneNum1 + "</td>");
                            fboHtml.AppendLine("<td width='30px'><strong>Fax:</strong></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FaxNUM + "</td>");
                            fboHtml.AppendLine("</tr>");
                            fboHtml.AppendLine("</table>");
                            fboHtml.AppendLine("</td>");
                            fboHtml.AppendLine("<td><strong>E-Mail:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.EmailAddress + "</td>");
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td><strong>Contact:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td colspan='4'>" + labFbo.FBO.Contact + "</td>");
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td><strong>Address:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            string strAddr = labFbo.FBO.Addr1 != null ? (labFbo.FBO.Addr1.Trim() != "" ? labFbo.FBO.Addr1 : string.Empty) : string.Empty;
                            strAddr += labFbo.FBO.Addr2 != null ? (labFbo.FBO.Addr2.Trim() != "" ? (strAddr != string.Empty ? ", " : string.Empty) + labFbo.FBO.Addr2 : string.Empty) : string.Empty;
                            strAddr += labFbo.FBO.Addr3 != null ? (labFbo.FBO.Addr3.Trim() != "" ? (strAddr != string.Empty ? ", " : string.Empty) + labFbo.FBO.Addr3 : string.Empty) : string.Empty;
                            fboHtml.AppendFormat("<td colspan='4'>{0}</td>", strAddr);
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td><strong>City:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.CityName + "</td>");
                            fboHtml.AppendLine("<td><strong>State:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.StateName + "</td>");
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td><strong>Postal Code:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.PostalZipCD + "</td>");
                            fboHtml.AppendLine("<td><strong></strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td width='17%'><strong>Fuel Brand:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td width='34%'>" + labFbo.FBO.FuelBrand + "</td>");
                            fboHtml.AppendLine("<td width='20%'><strong>Last Price:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td width='29%'>" + Math.Round((decimal)labFbo.FBO.LastFuelPrice, 4) + "</td>");
                            fboHtml.AppendLine("</tr>");
                            fboHtml.AppendLine("<tr>");

                            fboHtml.AppendLine("<td><strong>Last Purchase Date:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + Convert.ToDateTime(labFbo.FBO.LastFuelDT).ToString("MM/DD/YYYY") + "</td>");
                            fboHtml.AppendLine("<td><strong>Neg Price:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + Math.Round((decimal)labFbo.FBO.NegotiatedFuelPrice, 4)  + "</td>");
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td><strong>Pay Type:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.PaymentType + "</td>");
                            fboHtml.AppendLine("<td><strong>Fuel Qty:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + Math.Round((decimal)labFbo.FBO.FuelQty, 2) + "</td>");
                            fboHtml.AppendLine("</tr>");

                            fboHtml.AppendLine("<tr>");
                            fboHtml.AppendLine("<td><strong>UVAir:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.CreditCardUVAir + "</td>");
                            fboHtml.AppendLine("<td><strong>Freq:</strong></td>");
                            fboHtml.AppendLine("<td></td>");
                            fboHtml.AppendLine("<td>" + labFbo.FBO.Frequency + "</td>");
                            fboHtml.AppendLine("</tr>");

                            if ((labFbo.IsDepartureFBO == true && TripCrew.IsFBODepartConfirm == true && tripStatus == "U") || (labFbo.IsArrivalFBO == true && TripCrew.IsFBOArrivalConfirm == true && tripStatus == "U") || tripStatus == "I")
                            {
                                fboHtml.AppendLine("<tr>");
                                fboHtml.AppendLine("<td><strong>Confirm:</strong></td>");
                                fboHtml.AppendLine("<td></td>");
                                fboHtml.AppendLine("<td colspan='4'>" + labFbo.ConfirmationStatus + "</td>");
                                fboHtml.AppendLine("</tr>");
                            }

                            if ((labFbo.IsDepartureFBO == true && TripCrew.IsFBODepartComment == true && tripStatus == "U") || (labFbo.IsArrivalFBO == true && TripCrew.IsFBOArrivalComment == true && tripStatus == "U") || tripStatus == "I")
                            {
                                fboHtml.AppendLine("<tr>");
                                fboHtml.AppendLine("<td><strong>Comments:</strong></td>");
                                fboHtml.AppendLine("<td></td>");
                                fboHtml.AppendLine("<td colspan='4'>" + labFbo.Comments + "</td>");
                                fboHtml.AppendLine("</tr>");
                            }
                        }

                        fboHtml.AppendLine("</table>");
                        fboHtml.AppendLine("</td>");
                        fboHtml.AppendLine("</tr>");
                        
                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
                return fboHtml.ToString();
            }
            catch (Exception ex)
            {
                return fboHtml.ToString();
            }
            
        }

        private string TripsheetTransForCrewEmail(Crew TripCrew, string tranStatus, PreflightTransportList legTran, string tripStatus)
        {
            StringBuilder transHtml = new StringBuilder();
            try
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, tranStatus, legTran))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        transHtml.AppendLine("<tr>");
                        transHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                        transHtml.AppendLine("<table border='0' align='center' width='750px'>");
                        transHtml.AppendLine("<tr>");
                        transHtml.AppendLine("<td align='left'>");
                        transHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>" + tranStatus + " Trans</p>");
                        transHtml.AppendLine("</td>");
                        transHtml.AppendLine("</tr>");
                        transHtml.AppendLine("</table>");
                        transHtml.AppendLine("</td>");
                        transHtml.AppendLine("</tr>");

                        transHtml.AppendLine("<tr>");
                        transHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        transHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");

                        transHtml.AppendLine("<tr>");
                        transHtml.AppendLine("<td><strong>Name:</strong></td>");
                        transHtml.AppendLine("<td></td>");
                        transHtml.AppendLine("<td colspan='4'>" + legTran.PreflightTransportName + "</td>");
                        transHtml.AppendLine("</tr>");


                        transHtml.AppendLine("<tr>");
                        transHtml.AppendLine("<td style='width: 17%'><strong>Ph:</strong></td>");
                        transHtml.AppendLine("<td></td>");
                        transHtml.AppendLine("<td style='width: 34%'>" + legTran.PhoneNum1 + "</td>");
                        transHtml.AppendLine("<td style='width: 20%'><strong>Fax:</strong></td>");
                        transHtml.AppendLine("<td></td>");
                        transHtml.AppendLine("<td style='width: 29%'>" + legTran.FaxNUM + "</td>");
                        transHtml.AppendLine("</tr>");

                        if (legTran.Transport != null)
                        {
                            transHtml.AppendLine("<tr>");
                            transHtml.AppendLine("<td><strong>Rate:</strong></td>");
                            transHtml.AppendLine("<td></td>");
                            transHtml.AppendLine("<td>" + Math.Round((decimal)legTran.Transport.NegotiatedRate, 4) + "</td>");
                            transHtml.AppendLine("<td><strong></strong></td>");
                            transHtml.AppendLine("<td></td>");
                            transHtml.AppendLine("<td></td>");
                            transHtml.AppendLine("</tr>");
                        }

                        if ((legTran.IsDepartureTransport == true && legTran.CrewPassengerType == "C" && TripCrew.IsCrewDepartTRANSConfirm == true && tripStatus == "U") || (legTran.IsArrivalTransport == true && legTran.CrewPassengerType == "C" && TripCrew.IsCrewArrivalConfirm == true && tripStatus == "U") ||
                            (legTran.IsDepartureTransport == true && legTran.CrewPassengerType == "P" && TripCrew.IsPassDepartTRANSConfirm == true && tripStatus == "U") || (legTran.IsArrivalTransport == true && legTran.CrewPassengerType == "P" && TripCrew.IsPassArrivalConfirm == true && tripStatus == "U") || tripStatus == "I")
                        {
                            transHtml.AppendLine("<tr>");
                            transHtml.AppendLine("<td><strong>Confirm:</strong></td>");
                            transHtml.AppendLine("<td></td>");
                            transHtml.AppendLine("<td colspan='4'>" + legTran.ConfirmationStatus + "</td>");
                            transHtml.AppendLine("</tr>");
                        }
                        if ((legTran.IsDepartureTransport == true && legTran.CrewPassengerType == "C" && TripCrew.IsCrewDepartTRANSComments == true && tripStatus == "U") || (legTran.IsArrivalTransport == true && legTran.CrewPassengerType == "C" && TripCrew.IsCrewArrivalComments == true && tripStatus == "U") ||
                            (legTran.IsDepartureTransport == true && legTran.CrewPassengerType == "P" && TripCrew.IsPassDepartTRANSComments == true && tripStatus == "U") || (legTran.IsArrivalTransport == true && legTran.CrewPassengerType == "P" && TripCrew.IsPassArrivalComments == true && tripStatus == "U") || tripStatus == "I")
                        {
                            transHtml.AppendLine("<tr>");
                            transHtml.AppendLine("<td><strong>Comments:</strong></td>");
                            transHtml.AppendLine("<td></td>");
                            transHtml.AppendLine("<td colspan='4'>" + legTran.Comments + "</td>");
                            transHtml.AppendLine("</tr>");
                        }

                        transHtml.AppendLine("</table>");
                        transHtml.AppendLine("</td>");
                        transHtml.AppendLine("</tr>");

                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
                return transHtml.ToString();
            }
             catch (Exception ex)
             {
                 return transHtml.ToString();
             }
        }

        private string TripsheetHotelForCrewEmail(Crew TripCrew, string hotelStatus, PreflightHotelList legHotel, string tripStatus)
        {
            StringBuilder hotelHtml = new StringBuilder();
            try
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, hotelStatus, legHotel))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        
                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                        hotelHtml.AppendLine("<table border='0' align='center' width='750px'>");
                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td align='left'>");
                        hotelHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>" + hotelStatus + " Hotels</p>");
                        hotelHtml.AppendLine("</td>");
                        hotelHtml.AppendLine("</tr>");
                        hotelHtml.AppendLine("</table>");
                        hotelHtml.AppendLine("</td>");
                        hotelHtml.AppendLine("</tr>");

                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        hotelHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");

                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td><strong>Name:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("<td colspan='4'>" + legHotel.PreflightHotelName + "</td>");
                        hotelHtml.AppendLine("</tr>");
                        string strAddress = legHotel.Address1 != null ? (legHotel.Address1.Trim() != "" ? legHotel.Address1 : string.Empty) : string.Empty;
                        strAddress += legHotel.Address2 != null ? (legHotel.Address2.Trim() != "" ? (strAddress != string.Empty ? ", " : string.Empty) + legHotel.Address2 : string.Empty) : string.Empty;
                        strAddress += legHotel.Address3 != null ? (legHotel.Address3.Trim() != "" ? (strAddress != string.Empty ? ", " : string.Empty) + legHotel.Address3 : string.Empty) : string.Empty;
                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td><strong>Address:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendFormat("<td colspan='4'>{0}</td>", strAddress);
                        hotelHtml.AppendLine("</tr>");

                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td style='width: 17%'><strong>City:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("<td style='width: 34%'>" + legHotel.CityName + "</td>");
                        hotelHtml.AppendLine("<td style='width: 20%'><strong>State:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("<td style='width: 29%'>" + legHotel.StateName + "</td>");
                        hotelHtml.AppendLine("</tr>");

                        hotelHtml.AppendLine("<tr>");
                        hotelHtml.AppendLine("<td><strong>Postal Code:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("<td>" + legHotel.PostalZipCD + "</td>");
                        hotelHtml.AppendLine("<td><strong>E-Mail:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        if (legHotel.Hotel != null)
                        {
                            hotelHtml.AppendLine("<td>" + legHotel.Hotel.ContactEmail + "</td>");
                        }
                        else
                        {
                            hotelHtml.AppendLine("<td></td>");
                        }

                        hotelHtml.AppendLine("</tr>");
                        hotelHtml.AppendLine("<tr>");

                        hotelHtml.AppendLine("<td><strong>Rate:</strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("<td>" + (legHotel.Rate.HasValue ? Math.Round((decimal)legHotel.Rate, 4).ToString() : string.Empty) + "</td>");
                        hotelHtml.AppendLine("<td><strong></strong></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("<td></td>");
                        hotelHtml.AppendLine("</tr>");

                        if ((legHotel.crewPassengerType == "P" && TripCrew.IsPassHotelConfirm == true && tripStatus == "U") || (legHotel.crewPassengerType == "C" && TripCrew.IsCrewHotelConfirm == true && tripStatus == "U") || tripStatus == "I")
                        {
                            hotelHtml.AppendLine("<tr>");
                            hotelHtml.AppendLine("<td><strong>Confirm:</strong></td>");
                            hotelHtml.AppendLine("<td></td>");
                            hotelHtml.AppendLine("<td colspan='4'>" + legHotel.ConfirmationStatus + "</td>");
                            hotelHtml.AppendLine("</tr>");
                        }

                        if ((legHotel.crewPassengerType == "P" && TripCrew.IsPassHotelComments == true && tripStatus == "U") || (legHotel.crewPassengerType == "C" && TripCrew.IsCrewHotelComments == true && tripStatus == "U") || tripStatus == "I")
                        {
                            hotelHtml.AppendLine("<tr>");
                            hotelHtml.AppendLine("<td><strong>Comments:</strong></td>");
                            hotelHtml.AppendLine("<td></td>");
                            hotelHtml.AppendLine("<td colspan='4'>" + legHotel.Comments + "</td>");
                            hotelHtml.AppendLine("</tr>");
                        }

                        hotelHtml.AppendLine("</table>");
                        hotelHtml.AppendLine("</td>");
                        hotelHtml.AppendLine("</tr>");
                        
                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
                return hotelHtml.ToString();
            }
             catch (Exception ex)
             {
                 return hotelHtml.ToString();
             }
        }

        private string TripsheetCateringForCrewEmail(Crew TripCrew, string cateringStatus, PreflightCateringDetail legCaterer, string tripStatus)
        {
            StringBuilder cateringHtml = new StringBuilder();
            try
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, cateringStatus, legCaterer))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                       
                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                        cateringHtml.AppendLine("<table border='0' align='center' width='750px'>");
                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td align='left'>");
                        cateringHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>" + cateringStatus + " Catering:</p>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");
                        cateringHtml.AppendLine("</table>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        cateringHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td><strong>Name:</strong></td>");
                        cateringHtml.AppendLine("<td colspan='4'>" + legCaterer.CateringContactName + "</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td style='width: 17%'><strong>Ph:</strong></td>");
                        cateringHtml.AppendLine("<td></td>");
                        cateringHtml.AppendLine("<td style='width: 34%'>" + legCaterer.ContactPhone + "</td>");
                        cateringHtml.AppendLine("<td style='width: 20%'><strong>Fax:</strong></td>");
                        cateringHtml.AppendLine("<td></td>");
                        cateringHtml.AppendLine("<td style='width: 29%'>" + legCaterer.ContactFax + "</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td><strong>Rate:</strong></td>");
                        cateringHtml.AppendLine("<td></td>");
                        if (legCaterer.Catering != null)
                        {
                            cateringHtml.AppendLine("<td>" + Math.Round((decimal)legCaterer.Catering.NegotiatedRate, 4) + "</td>");
                        }
                        else
                        {
                            cateringHtml.AppendLine("<td></td>");
                        }
                        cateringHtml.AppendLine("<td><strong></strong></td>");
                        cateringHtml.AppendLine("<td></td>");
                        cateringHtml.AppendLine("<td></td>");
                        cateringHtml.AppendLine("</tr>");

                        if ((legCaterer.ArriveDepart == "D" && TripCrew.IsDepartCateringConfirm == true && tripStatus == "U") || (legCaterer.ArriveDepart == "A" && TripCrew.IsArrivalCateringConfirm == true && tripStatus == "U") || tripStatus == "I")
                        {
                            cateringHtml.AppendLine("<tr>");
                            cateringHtml.AppendLine("<td><strong>Confirm:</strong></td>");
                            cateringHtml.AppendLine("<td></td>");
                            cateringHtml.AppendLine("<td colspan='4'>" + legCaterer.CateringConfirmation + "</td>");
                            cateringHtml.AppendLine("</tr>");
                        }
                        if ((legCaterer.ArriveDepart == "D" && TripCrew.IsDepartCateringComment == true && tripStatus == "U") || (legCaterer.ArriveDepart == "A" && TripCrew.IsArrivalCateringComment == true && tripStatus == "U") || tripStatus == "I")
                        {
                            cateringHtml.AppendLine("<tr>");
                            cateringHtml.AppendLine("<td><strong>Comments:</strong></td>");
                            cateringHtml.AppendLine("<td></td>");
                            cateringHtml.AppendLine("<td colspan='4'>" + legCaterer.CateringComments + "</td>");
                            cateringHtml.AppendLine("</tr>");
                        }
                        cateringHtml.AppendLine("</table>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");
                        
                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
                return cateringHtml.ToString();
            }
            catch (Exception ex)
            {
                return cateringHtml.ToString();
            }
        }

        private string TripsheetOutboundForCrewEmail(Crew TripCrew, string label, PreflightLeg legOutbound)
        {
            StringBuilder cateringHtml = new StringBuilder();
            try
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, label, legOutbound))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                        cateringHtml.AppendLine("<table border='0' align='center' width='750px'>");
                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td align='left'>");
                        cateringHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>" + label + ":</p>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");
                        cateringHtml.AppendLine("</table>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        cateringHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td><strong></strong></td>");
                        cateringHtml.AppendLine("<td colspan='4'>" + legOutbound.OutbountInstruction  + "</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td><strong></strong></td>");
                        cateringHtml.AppendLine("<td colspan='4'>" + legOutbound.CrewFuelLoad + "</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td><strong></strong></td>");
                        cateringHtml.AppendLine("<td colspan='4'>" + legOutbound.FuelLoad + "</td>");
                        cateringHtml.AppendLine("</tr>");
                        foreach (PreflightTripOutbound outbound in legOutbound.PreflightTripOutbounds)
                        {
                            bool addContent = false;
                            if (TripCrew.Label1 == true && outbound.OutboundInstructionNUM == 1) addContent = true;
                            if (TripCrew.Label2 == true && outbound.OutboundInstructionNUM == 2) addContent = true;
                            if (TripCrew.Label3 == true && outbound.OutboundInstructionNUM == 3) addContent = true;
                            if (TripCrew.Label4 == true && outbound.OutboundInstructionNUM == 4) addContent = true;
                            if (TripCrew.Label5 == true && outbound.OutboundInstructionNUM == 5) addContent = true;
                            if (TripCrew.Label6 == true && outbound.OutboundInstructionNUM == 6) addContent = true;
                            if (TripCrew.Label7 == true && outbound.OutboundInstructionNUM == 7) addContent = true;

                            if (addContent)
                            {
                                cateringHtml.AppendLine("<tr>");
                                cateringHtml.AppendLine("<td style='width: 17%'><strong></strong></td>");
                                cateringHtml.AppendLine("<td></td>");
                                cateringHtml.AppendLine("<td style='width: 34%'>" + outbound.OutboundDescription + "</td>");
                                cateringHtml.AppendLine("<td style='width: 20%'><strong></strong></td>");
                                cateringHtml.AppendLine("<td></td>");
                                cateringHtml.AppendLine("<td style='width: 29%'></td>");
                                cateringHtml.AppendLine("</tr>");
                            }
                        }

                        cateringHtml.AppendLine("</table>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");

                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
                return cateringHtml.ToString();
            }
            catch (Exception ex)
            {
                return cateringHtml.ToString();
            }
        }

        private string TripsheetChecklistForCrewEmail(Crew TripCrew, string label, PreflightLeg legChecklist)
        {
            StringBuilder cateringHtml = new StringBuilder();
            try
            {
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripCrew, label, legChecklist))
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td style='border-bottom: #666666 1px solid; background-color: #d3d3d3'>");
                        cateringHtml.AppendLine("<table border='0' align='center' width='750px'>");
                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td align='left'>");
                        cateringHtml.AppendLine("<p style='font-family: arial,  helvetica, sans-serif; font-size: 14px; color: #3366cc;'>" + label + ":</p>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");
                        cateringHtml.AppendLine("</table>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");

                        cateringHtml.AppendLine("<tr>");
                        cateringHtml.AppendLine("<td bgcolor='#efefef' style='border-bottom: #666666 1px solid;'>");
                        cateringHtml.AppendLine("<table width='750' style='font: 12px Arial, Helvetica, sans-serif; color: #444;' align='center'>");

                        foreach (PreflightCheckList checkList in legChecklist.PreflightCheckLists)
                        {
                            if (checkList.IsCompleted == true)
                            {
                                cateringHtml.AppendLine("<tr>");
                                cateringHtml.AppendLine("<td style='width: 17%'><strong>" + checkList.ComponentDescription + "</strong></td>");
                                cateringHtml.AppendLine("<td></td>");
                                cateringHtml.AppendLine("<td style='width: 34%'>" + checkList.TripManagerCheckList.CheckListDescription + "</td>");
                                cateringHtml.AppendLine("<td style='width: 20%'><strong></strong></td>");
                                cateringHtml.AppendLine("<td></td>");
                                cateringHtml.AppendLine("<td style='width: 29%'></td>");
                                cateringHtml.AppendLine("</tr>");
                            }
                        }

                        cateringHtml.AppendLine("</table>");
                        cateringHtml.AppendLine("</td>");
                        cateringHtml.AppendLine("</tr>");

                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
                return cateringHtml.ToString();
            }
            catch (Exception ex)
            {
                return cateringHtml.ToString();
            }
        }

        private string TripsheetHtmlCellEmail(string vLabel,string vContent)
        {
            StringBuilder htmlCell = new StringBuilder();
            htmlCell.Append("<div style='width: 50%; float: left;'>");
            htmlCell.AppendFormat("<div style='width: 40%;float: left;padding: 1px 0 1px;'><strong>{0}:</strong></div>", vLabel);
            htmlCell.AppendFormat("<div style='width: 60%;float: left;'>{0}</div>", vContent);
            htmlCell.Append("</div>");
            return htmlCell.ToString();
        }

        #endregion

        private void UpdateEntity(ref PreflightMain Trip, ref PreflightDataModelContainer Container,ref bool isTripChange)
        {
            // Load original parent including the child item collection 

            #region "History"
            StringBuilder HistoryDescription = new System.Text.StringBuilder();
            #endregion


            Trip.LastUpdTS = DateTime.UtcNow;
            Trip.LastUpdUID = UserPrincipal.Identity.Name;

            var TripId = Trip.TripID;
            PreflightMain PreflightMainentity = Container.GetTripByTripID(CustomerID, TripId).SingleOrDefault();
            //var TripId = Trip.TripID;
            //PreflightMain PreflightMainentity = Container.PreflightMains
            //    .Where(p => p.TripID == TripId).SingleOrDefault();


            #region Main
            if (Trip.State == TripEntityState.Modified)
            {
                if (PreflightMainentity != null)
                {
                    Container.PreflightMains.Attach(PreflightMainentity);
                    Container.PreflightMains.ApplyCurrentValues(Trip);
                }
                #region "History For Modified Trip"
                var originalValues = Container.ObjectStateManager.GetObjectStateEntry(PreflightMainentity).OriginalValues;
                var CurrentValues = Container.ObjectStateManager.GetObjectStateEntry(PreflightMainentity).CurrentValues;
                PropertyInfo[] properties = typeof(PreflightMain).GetProperties();

                for (int i = 0; i < originalValues.FieldCount; i++)
                {
                    var fieldname = originalValues.GetName(i);

                    object value1 = originalValues.GetValue(i);
                    object value2 = CurrentValues.GetValue(i);

                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                    {
                        #region "Depart Date"
                        if (fieldname.ToLower() == "estdeparturedt")
                        {
                            DateTime oldVal = Convert.ToDateTime(value1).Date;
                            DateTime currVal = Convert.ToDateTime(value2).Date;
                            if (oldVal != currVal)
                            {
                                HistoryDescription.AppendLine(
                                    string.Format(
                                    "{0} Changed From {1} To {2}", "Depart Date",
                                    (!string.IsNullOrEmpty(Convert.ToString(value1)) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value1)) : "<span style='font-weight:bold'>NULL</span>"),
                                    (!string.IsNullOrEmpty(Convert.ToString(value2)) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value2)) : "<span style='font-weight:bold'>NULL</span>")
                                    ));
                            }

                        }

                        #endregion

                        #region "Flight Number"
                        if (fieldname.ToLower() == "flightnum")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Flight No.", value1, value2, false));
                        }

                        #endregion

                        #region "Request Date"
                        if (fieldname.ToLower() == "requestdt")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Request Date", value1, value2, true));
                        }

                        #endregion

                        #region "Fleet"

                        if (fieldname.ToLower() == "fleetid")
                        {
                            Int64 OldFleetID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldFleetID = (Int64)value1;

                            Int64 NewFleetID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewFleetID = (Int64)value2;
                            IQueryable<Fleet> Fleets = preflightCompiledQuery.getFleet(Container, OldFleetID, CustomerID);
                            Fleet OldFleet = new Fleet();

                            if (Fleets != null && Fleets.ToList().Count > 0)
                                OldFleet = Fleets.ToList()[0];
                            Fleets = null;

                            IQueryable<Fleet> NewFleets = preflightCompiledQuery.getFleet(Container, NewFleetID, CustomerID);
                            Fleet NewFleet = new Fleet();

                            if (NewFleets != null && NewFleets.ToList().Count > 0)
                                NewFleet = NewFleets.ToList()[0];
                            else
                                Trip.FleetID = 0;

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Tail No.", OldFleet.TailNum, NewFleet.TailNum, false));
                        }

                        #endregion

                        #region "Trippurpose"
                        if (fieldname.ToLower() == "tripdescription")
                        {
                            if (Trip.RecordType == "T")
                            {
                                HistoryDescription.AppendLine(FormatHistoryValueChange("Trip Purpose", value1, value2, false));
                            }
                            else
                            {
                                HistoryDescription.AppendLine(FormatHistoryValueChange("Comment", value1, value2, false));
                            }
                        }

                        #endregion

                        #region "private"

                        if (fieldname.ToLower() == "isprivate")
                        {
                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Trip",
                                ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Confidential" : "Not Confidential"),
                                ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Confidential" : "Not Confidential")));
                        }
                        #endregion

                        #region "Status"

                        if (fieldname.ToLower() == "tripstatus")
                        {
                            string OldStatus = string.Empty;
                            string NewStatus = string.Empty;
                            switch (value1.ToString())
                            {
                                case "W": OldStatus = "W-Worksheet"; break;
                                case "T": OldStatus = "T-Tripsheet"; break;
                                case "U": OldStatus = "U-Unfulfilled"; break;
                                case "X": OldStatus = "X-Cancelled"; break;
                                case "H": OldStatus = "H-Hold"; break;
                            }
                            switch (value2.ToString())
                            {
                                case "W": NewStatus = "W-Worksheet"; break;
                                case "T": NewStatus = "T-Tripsheet"; break;
                                case "U": NewStatus = "U-Unfulfilled"; break;
                                case "X": NewStatus = "X-Cancelled"; break;
                                case "H": NewStatus = "H-Hold"; break;
                            }

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Trip Status", OldStatus, NewStatus));
                        }
                        #endregion

                        #region "Aircraft"

                        if (fieldname.ToLower() == "aircraftid")
                        {
                            Int64 OldAircraftID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAircraftID = (Int64)value1;

                            Int64 NewAircraftID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAircraftID = (Int64)value2;

                            IQueryable<Aircraft> Aircrafts = preflightCompiledQuery.getAircraft(Container, OldAircraftID, CustomerID);
                            Aircraft OldAircraft = new Aircraft();
                            if (Aircrafts != null && Aircrafts.ToList().Count > 0)
                                OldAircraft = Aircrafts.ToList()[0];

                            Aircrafts = preflightCompiledQuery.getAircraft(Container, NewAircraftID, CustomerID);
                            Aircraft NewAircraft = new Aircraft();
                            if (Aircrafts != null && Aircrafts.ToList().Count > 0)
                                NewAircraft = Aircrafts.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Type", OldAircraft.AircraftCD, NewAircraft.AircraftCD, false));
                        }

                        #endregion

                        #region "Dispatch NUM"

                        if (fieldname.ToLower() == "dispatchnum")
                        {
                            if (Trip.RecordType == "T")
                            {
                                HistoryDescription.AppendLine(FormatHistoryValueChange("Dispatch NUM", value1, value2, false));
                            }
                        }

                        #endregion

                        #region "Dispatcher Username"


                        if (fieldname.ToLower() == "dispatcherusername")
                        {
                            if (Convert.ToString(value1).Trim() != Convert.ToString(value2).Trim())
                            {
                                HistoryDescription.AppendLine(FormatHistoryValueChange("Dispatcher Username", value1, value2, false));
                            }
                        }
                        #endregion

                        #region "Notes"


                        if (fieldname.ToLower() == "notes")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Trip Alert", value1, value2, false));
                        }


                        #endregion

                        #region "TripSheetNotes"


                        if (fieldname.ToLower() == "tripsheetnotes")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Notes", value1, value2, false));
                        }


                        #endregion

                        #region "CancelDescription"


                        if (fieldname.ToLower() == "canceldescription")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Cancel Description", value1, value2, false));
                        }


                        #endregion

                        #region "Passenger"

                        if (fieldname.ToLower() == "passengerrequestorid")
                        {
                            Int64 OldPassengerID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldPassengerID = (Int64)value1;

                            Int64 NewPassengerID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewPassengerID = (Int64)value2;

                            IQueryable<Passenger> Passengers = preflightCompiledQuery.getPassenger(Container, OldPassengerID, CustomerID);
                            Passenger OldPassenger = new Passenger();
                            if (Passengers != null && Passengers.ToList().Count > 0)
                                OldPassenger = Passengers.ToList()[0];

                            Passengers = preflightCompiledQuery.getPassenger(Container, NewPassengerID, CustomerID);
                            Passenger Newpassenger = new Passenger();
                            if (Passengers != null && Passengers.ToList().Count > 0)
                                Newpassenger = Passengers.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Requestor", OldPassenger.PassengerRequestorCD, Newpassenger.PassengerRequestorCD, false));
                        }


                        #endregion

                        #region "Account"


                        if (fieldname.ToLower() == "accountid")
                        {
                            Int64 OldAccountID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAccountID = (Int64)value1;
                            Int64 NewAccountID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAccountID = (Int64)value2;
                            IQueryable<Account> Accounts = preflightCompiledQuery.getAccount(Container, OldAccountID, CustomerID);
                            Account OldAccount = new Account();
                            if (Accounts != null && Accounts.ToList().Count > 0)
                                OldAccount = Accounts.ToList()[0];
                            Accounts = preflightCompiledQuery.getAccount(Container, NewAccountID, CustomerID);
                            Account NewAccount = new Account();
                            if (Accounts != null && Accounts.ToList().Count > 0)
                                NewAccount = Accounts.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Account", OldAccount.AccountNum, NewAccount.AccountNum, false));

                        }


                        #endregion

                        #region "Department"

                        if (fieldname.ToLower() == "departmentid")
                        {
                            Int64 OldDepartmentID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldDepartmentID = (Int64)value1;

                            Int64 NewDepartmentID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewDepartmentID = (Int64)value2;

                            IQueryable<Department> Departments = preflightCompiledQuery.getDepartment(Container, OldDepartmentID, CustomerID);
                            Department OldDepartment = new Department();
                            if (Departments != null && Departments.ToList().Count > 0)
                                OldDepartment = Departments.ToList()[0];

                            Departments = preflightCompiledQuery.getDepartment(Container, NewDepartmentID, CustomerID);
                            Department NewDepartment = new Department();
                            if (Departments != null && Departments.ToList().Count > 0)
                                NewDepartment = Departments.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Department", OldDepartment.DepartmentCD, NewDepartment.DepartmentCD, false));
                        }


                        #endregion

                        #region "Authorization"

                        if (fieldname.ToLower() == "authorizationid")
                        {
                            Int64 OldAuthorizationID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAuthorizationID = (Int64)value1;

                            Int64 NewAuthorizationID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAuthorizationID = (Int64)value2;

                            IQueryable<DepartmentAuthorization> DepartmentAuthorizations = preflightCompiledQuery.getDepartmentAuthorization(Container, OldAuthorizationID, CustomerID);
                            DepartmentAuthorization OldAuthorization = new DepartmentAuthorization();
                            if (DepartmentAuthorizations != null && DepartmentAuthorizations.ToList().Count > 0)
                                OldAuthorization = DepartmentAuthorizations.ToList()[0];

                            DepartmentAuthorizations = preflightCompiledQuery.getDepartmentAuthorization(Container, NewAuthorizationID, CustomerID);
                            DepartmentAuthorization NewAuthorization = new DepartmentAuthorization();
                            if (DepartmentAuthorizations != null && DepartmentAuthorizations.ToList().Count > 0)
                                NewAuthorization = DepartmentAuthorizations.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Authorization", OldAuthorization.AuthorizationCD, NewAuthorization.AuthorizationCD, false));
                        }


                        #endregion

                        #region "CQCustomer"

                        if (fieldname.ToLower() == "cqcustomerid")
                        {
                            Int64 OldCQCustomerID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldCQCustomerID = (Int64)value1;

                            Int64 NewCQCustomerID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewCQCustomerID = (Int64)value2;

                            IQueryable<CQCustomer> CQCustomers = preflightCompiledQuery.getCQCustomer(Container, OldCQCustomerID, CustomerID);
                            CQCustomer OldCQCustomer = new CQCustomer();
                            if (CQCustomers != null && CQCustomers.ToList().Count > 0)
                                OldCQCustomer = CQCustomers.ToList()[0];

                            CQCustomers = preflightCompiledQuery.getCQCustomer(Container, NewCQCustomerID, CustomerID);
                            CQCustomer NewCQCustomer = new CQCustomer();
                            if (CQCustomers != null && CQCustomers.ToList().Count > 0)
                                NewCQCustomer = CQCustomers.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Charter Customer", OldCQCustomer.CQCustomerCD, NewCQCustomer.CQCustomerCD, false));
                        }


                        #endregion

                        #region "Client"

                        if (fieldname.ToLower() == "clientid")
                        {
                            Int64 OldClientID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldClientID = (Int64)value1;

                            Int64 NewClientID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewClientID = (Int64)value2;

                            IQueryable<Client> Clients = preflightCompiledQuery.getClient(Container, OldClientID, CustomerID);
                            Client OldClient = new Client();
                            if (Clients != null && Clients.ToList().Count > 0)
                                OldClient = Clients.ToList()[0];

                            Clients = preflightCompiledQuery.getClient(Container, NewClientID, CustomerID);
                            Client NewClient = new Client();
                            if (Clients != null && Clients.ToList().Count > 0)
                                NewClient = Clients.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Client", OldClient.ClientCD, NewClient.ClientCD, false));
                        }


                        #endregion

                        #region "EmergencyContact"

                        if (fieldname.ToLower() == "emergencycontactid")
                        {
                            Int64 OldEmergencyContactID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldEmergencyContactID = (Int64)value1;

                            Int64 NewEmergencyContactID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewEmergencyContactID = (Int64)value2;

                            IQueryable<EmergencyContact> EmergencyContacts = preflightCompiledQuery.getEmergencyContact(Container, OldEmergencyContactID, CustomerID);
                            EmergencyContact OldEmergencyContact = new EmergencyContact();
                            if (EmergencyContacts != null && EmergencyContacts.ToList().Count > 0)
                                OldEmergencyContact = EmergencyContacts.ToList()[0];

                            EmergencyContacts = preflightCompiledQuery.getEmergencyContact(Container, NewEmergencyContactID, CustomerID);
                            EmergencyContact NewEmergencyContact = new EmergencyContact();
                            if (EmergencyContacts != null && EmergencyContacts.ToList().Count > 0)
                                NewEmergencyContact = EmergencyContacts.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Emergency Contact", OldEmergencyContact.EmergencyContactCD, NewEmergencyContact.EmergencyContactCD, false));
                        }


                        #endregion

                        #region "Releasedby"

                        if (fieldname.ToLower() == "crewid")
                        {
                            Int64 OldReleasedby = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldReleasedby = (Int64)value1;

                            Int64 NewReleasedby = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewReleasedby = (Int64)value2;

                            IQueryable<Crew> Crews = preflightCompiledQuery.getCrew(Container, OldReleasedby, CustomerID);
                            Crew OldCrew = new Crew();
                            if (Crews != null && Crews.ToList().Count > 0)
                                OldCrew = Crews.ToList()[0];

                            Crews = preflightCompiledQuery.getCrew(Container, NewReleasedby, CustomerID);
                            Crew NewCrew = new Crew();
                            if (Crews != null && Crews.ToList().Count > 0)
                                NewCrew = Crews.ToList()[0];

                            HistoryDescription.AppendLine(FormatHistoryValueChange("Released by", OldCrew.CrewCD, NewCrew.CrewCD, false));
                        }


                        #endregion

                        #region All Legs Deleted - Need to check
                        if (fieldname.ToLower() == "preflightLeg")
                        {
                            //List<PreflightLeg> Oldlegs = new List<PreflightLeg>();
                            //Oldlegs = 

                            //HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Trip Purpose", value1, value2));
                        }
                        #endregion

                        #region "HomeBase"

                        if (fieldname.ToLower() == "homebaseid")
                        {
                            Int64 OldHomebaseID = 0;
                            if (value1 != null)
                                OldHomebaseID = (Int64)value1;

                            Int64 NewHomebaseID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewHomebaseID = (Int64)value2;


                            IQueryable<Company> Companys = preflightCompiledQuery.getCompany(Container, OldHomebaseID, CustomerID);
                            Company OldCompany = new Company();
                            if (Companys != null && Companys.ToList().Count > 0)
                                OldCompany = Companys.ToList()[0];

                            Companys = preflightCompiledQuery.getCompany(Container, NewHomebaseID, CustomerID);
                            Company NewCompany = new Company();
                            if (Companys != null && Companys.ToList().Count > 0)
                                NewCompany = Companys.ToList()[0];

                            //Company OldCompany = Container.Companies.Where(x => x.HomebaseID == OldHomebaseID).SingleOrDefault();
                            //Company NewCompany = Container.Companies.Where(x => x.HomebaseID == NewHomebaseID).SingleOrDefault();

                            string OldAirportID = string.Empty;
                            string NewAirportID = string.Empty;
                            if (OldCompany != null && NewCompany != null && NewCompany.HomebaseAirportID != null && OldCompany.HomebaseAirportID != null)
                            {
                                IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, (long)OldCompany.HomebaseAirportID, CustomerID);

                                Airport OldAirport = new Airport();
                                if (Airports != null && Airports.ToList().Count > 0)
                                    OldAirport = Airports.ToList()[0];
                                else
                                {
                                    // Fix for #8222
                                    IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, (long)OldCompany.HomebaseAirportID);
                                    OldAirport = airportList.ToList()[0];
                                }

                                //Airport OldAirport = Container.Airports.Where(x => x.AirportID == OldCompany.HomebaseAirportID).SingleOrDefault();
                                if (OldAirport != null)
                                {
                                    OldAirportID = OldAirport.IcaoID;
                                }


                                IQueryable<Airport> newAirports = preflightCompiledQuery.getAirport(Container, (long)NewCompany.HomebaseAirportID, CustomerID);
                                Airport NewAirport = new Airport();
                                if (Airports != null && newAirports.ToList().Count > 0)
                                    NewAirport = newAirports.ToList()[0];
                                else
                                {
                                    // Fix for #8222
                                    IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, (long)NewCompany.HomebaseAirportID);
                                    NewAirport = airportList.ToList()[0];
                                }

                                //Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewCompany.HomebaseAirportID).SingleOrDefault();
                                if (NewAirport != null)
                                {
                                    NewAirportID = NewAirport.IcaoID;
                                }

                                if (!string.IsNullOrEmpty(OldAirportID) && !string.IsNullOrEmpty(NewAirportID))
                                {
                                    HistoryDescription.AppendLine(string.Format("Homebase Changed From {0} To {1}", OldAirportID, NewAirportID));
                                }
                            }
                        }


                        #endregion

                        #region "FleetCalendarNotes"

                        if (fieldname.ToLower() == "fleetcalendarnotes")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Fleet Calendar Notes", value1, value2, false));
                        }

                        #endregion

                        #region "CrewCalendarNotes"

                        if (fieldname.ToLower() == "crewcalendarnotes")
                        {
                            HistoryDescription.AppendLine(FormatHistoryValueChange("Crew Calendar Notes", value1, value2, false));
                        }

                        #endregion
                    }
                }
                #endregion
            }
            else if (Trip.State == TripEntityState.Added)
            {
                Trip.IsDeleted = false;
                // Trip.RecordType = "T";
                Container.PreflightMains.AddObject(Trip);
                #region "History"

                if (Trip.RecordType == "T")
                {
                    HistoryDescription.AppendLine("");
                    HistoryDescription.AppendLine("New Trip Added");
                    HistoryDescription.AppendLine("");
                    #region "HomeBase"

                    if (!string.IsNullOrEmpty(Trip.HomeBaseAirportICAOID))
                    {
                        HistoryDescription.AppendLine(string.Format("HomeBase : {0}", Trip.HomeBaseAirportICAOID));
                    }
                    #endregion

                    #region "Client"

                    if (Trip.Client != null && Trip.ClientID != 0)
                    {
                        HistoryDescription.AppendLine(string.Format("Client Code : {0}", Trip.Client.ClientCD));
                    }
                    #endregion

                    #region "Depart Date"
                    if (!string.IsNullOrEmpty(Trip.EstDepartureDT.ToString()))
                    {
                        HistoryDescription.AppendLine(string.Format("Depart Date : {0}", Trip.EstDepartureDT));
                    }
                    #endregion

                    #region "Request Date"
                    if (!string.IsNullOrEmpty(Trip.RequestDT.ToString()))
                    {
                        HistoryDescription.AppendLine(string.Format("Request Date : {0}", Trip.RequestDT));
                    }
                    #endregion


                    #region "Tail Num"
                    if (Trip.Fleet != null && !string.IsNullOrEmpty(Trip.Fleet.TailNum))
                    {
                        HistoryDescription.AppendLine(string.Format("Tail Num : {0}", Trip.Fleet.TailNum));
                    }
                    #endregion


                    #region "Tail Num"

                    if (!string.IsNullOrEmpty(Trip.FlightNUM))
                    {
                        HistoryDescription.AppendLine(string.Format("Flight Num : {0}", Trip.FlightNUM));
                    }
                    #endregion


                    #region "Trip Status"

                    if (!string.IsNullOrEmpty(Trip.TripStatus))
                    {
                        string status = string.Empty;
                        switch (Trip.TripStatus)
                        {
                            case "W": status = "W-Worksheet"; break;
                            case "T": status = "T-Tripsheet"; break;
                            case "U": status = "U-Unfulfilled"; break;
                            case "X": status = "X-Cancelled"; break;
                            case "H": status = "H-Hold"; break;
                        }
                        HistoryDescription.AppendLine(string.Format("Trip Status : {0}", status));
                    }

                    #endregion


                    #region "Private"
                    if (Trip.IsPrivate == true)
                    {
                        HistoryDescription.AppendLine("Trip is Confidential");
                    }
                    else
                    {
                        HistoryDescription.AppendLine("Trip is NOT Confidential");
                    }

                    #endregion

                    #region "Emergency Contact"
                    if (Trip.EmergencyContact != null && !string.IsNullOrEmpty(Trip.EmergencyContact.EmergencyContactCD))
                    {
                        HistoryDescription.AppendLine(string.Format("Emergency Contact : {0}", Trip.EmergencyContact.EmergencyContactCD));
                    }
                    #endregion

                    #region "Account"
                    if (Trip.Account != null && !string.IsNullOrEmpty(Trip.Account.AccountNum))
                    {
                        HistoryDescription.AppendLine(string.Format("Account No : {0}", Trip.Account.AccountNum));
                    }
                    #endregion
                    #region "Department"
                    if (Trip.Department != null && !string.IsNullOrEmpty(Trip.Department.DepartmentCD))
                    {
                        HistoryDescription.AppendLine(string.Format("Department : {0}", Trip.Department.DepartmentCD));
                    }
                    #endregion
                    #region "Request Date"
                    HistoryDescription.AppendLine(string.Format("Request Date : {0}", Trip.RequestDT));
                    #endregion
                    #region "Authorization"
                    if (Trip.DepartmentAuthorization != null && !string.IsNullOrEmpty(Trip.DepartmentAuthorization.AuthorizationCD))
                        HistoryDescription.AppendLine(string.Format("Authorization Code : {0}", Trip.DepartmentAuthorization.AuthorizationCD));
                    #endregion
                    #region "Trip Purpose"
                    if (!string.IsNullOrEmpty(Trip.TripDescription))
                        HistoryDescription.AppendLine(string.Format("Trip Purpose : {0}", Trip.TripDescription));
                    #endregion
                    #region "Dispatch Num"
                    if (!string.IsNullOrEmpty(Trip.DispatchNUM))
                        HistoryDescription.AppendLine(string.Format("Dispatch Num : {0}", Trip.DispatchNUM));
                    #endregion
                    #region "Dispatcher"
                    if (!string.IsNullOrEmpty(Trip.DispatcherUserName))
                        HistoryDescription.AppendLine(string.Format("Dispatcher : {0}", Trip.DispatcherUserName));
                    #endregion
                    #region "Trip Alert"
                    if (!string.IsNullOrEmpty(Trip.Notes))
                        HistoryDescription.AppendLine(string.Format("Trip Alert : {0}", Trip.Notes));
                    #endregion
                    #region "Trip Notes"
                    if (!string.IsNullOrEmpty(Trip.TripSheetNotes))
                        HistoryDescription.AppendLine(string.Format("Trip Notes : {0}", Trip.TripSheetNotes));
                    #endregion
                    #region "Cancellation Description"
                    if (!string.IsNullOrEmpty(Trip.CancelDescription))
                        HistoryDescription.AppendLine(string.Format("Cancellation Description : {0}", Trip.CancelDescription));
                    #endregion
                }
                else if (Trip.RecordType == "M")
                {
                    HistoryDescription.AppendLine("New Fleet Calendar Activity Added");
                    if (Trip.FleetID != null)
                    {
                        Int64 FleetID = (long)Trip.FleetID;

                        IQueryable<Fleet> Fleets = preflightCompiledQuery.getFleet(Container, FleetID, CustomerID);
                        Fleet fleet = new Fleet();
                        if (Fleets != null && Fleets.ToList().Count > 0)
                            fleet = Fleets.ToList()[0];
                        //Fleet fleet = Container.Fleets.Where(x => x.FleetID == FleetID).SingleOrDefault();
                        if (fleet != null)
                        {
                            if (Trip.PreflightLegs != null)
                            {
                                PreflightLeg Leg = Trip.PreflightLegs.Where(x => x.LegNUM == 1 && x.IsDeleted == false).SingleOrDefault();
                                if (Leg != null) HistoryDescription.AppendLine(string.Format("Tail No: {0} Duty Type: {1} ", fleet.TailNum, Leg.DutyTYPE));
                            }
                        }
                    }
                }
                else if (Trip.RecordType == "C")
                    HistoryDescription.AppendLine("New Crew Calendar Activity Added");
                #endregion
            }
            else
            {
                if (PreflightMainentity != null)
                {
                    Container.PreflightMains.Attach(PreflightMainentity);
                }
                //Container.PreflightMains.Attach(Trip);
                //Container.PreflightMains.Attach(PreflightMainentity);
                //Container.PreflightMains.ApplyCurrentValues(Trip);
            }
            #endregion

            #region "Legs"
            foreach (var Leg in Trip.PreflightLegs.ToList())
            {
                Leg.LastUpdTS = DateTime.UtcNow;
                Leg.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                PreflightLeg originalLeg = new PreflightLeg();

                if (Leg.State != TripEntityState.Added)
                {
                    originalLeg = Container.GetLegByLegID(CustomerID, Leg.LegID).SingleOrDefault();

                    //originalLeg = Container.PreflightLegs
                    //.Where(c => c.LegID == Leg.LegID)
                    //.SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (Leg.State == TripEntityState.Modified)
                    {
                        Leg.IsDeleted = false;
                        Leg.CustomerID = CustomerID;
                        if (originalLeg != null)
                        {
                            Container.PreflightLegs.Attach(originalLeg);
                            Container.PreflightLegs.ApplyCurrentValues(Leg);
                        }
                        #region "History For Modified Leg"
                        var originalLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).OriginalValues;
                        var CurrentLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).CurrentValues;
                        PropertyInfo[] properties = typeof(PreflightLeg).GetProperties();
                        for (int i = 0; i < originalLegValues.FieldCount; i++)
                        {
                            var fieldname = originalLegValues.GetName(i);

                            object value1 = originalLegValues.GetValue(i);
                            object value2 = CurrentLegValues.GetValue(i);

                            if (value1 != value2 && (value1 == null || !value1.Equals(value2)) && (!(value1.GetStringOrNull().Trim().Equals(value2.GetStringOrNull().Trim()))))
                            {
                                #region "Distance"
                                if (fieldname.ToLower() == "distance")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Miles(N)", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "PowerSetting"

                                if (fieldname.ToLower() == "powersetting")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Power Setting ", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "TrueAirSpeed"

                                if (fieldname.ToLower() == "trueairspeed")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "TrueAirSpeed", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "WindsBoeingTable"
                                if (fieldname.ToLower() == "windsboeingtable")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "WindsBoeingTable ", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "TakeoffBIAS"
                                if (fieldname.ToLower() == "takeoffbias")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "T/O Bias ", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "ElapseTM"
                                if (fieldname.ToLower() == "elapsetm")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "ETE ", Math.Round(Convert.ToDecimal(value1), 3).ToString(), Math.Round(Convert.ToDecimal(value2), 3).ToString()));
                                }
                                #endregion

                                #region "CrewDutyRulesID"
                                if (fieldname.ToLower() == "crewdutyrulesid")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Crew RulesID ", (string.IsNullOrEmpty(Convert.ToString(value1)) ? null : getCrewRule(Convert.ToInt64(value1), CustomerID)), (string.IsNullOrEmpty(Convert.ToString(value2)) ? null : getCrewRule(Convert.ToInt64(value2), CustomerID))));
                                }
                                #endregion

                                #region "LandingBias"
                                if (fieldname.ToLower() == "landingbias")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Land Bias ", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "FlightNUM"
                                if (fieldname.ToLower() == "flightnum")
                                {
                                    if (Convert.ToString(value1).Trim() != Convert.ToString(value2).Trim())
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Flight No. ", value1.ToString(), value2.ToString()));
                                }
                                #endregion

                                #region "WindReliability"
                                if (fieldname.ToLower() == "windreliability")
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(value1)))
                                    {
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "WindReliability ", getWindReliability(Convert.ToInt32(value1)), getWindReliability(Convert.ToInt32(value2))));
                                    }
                                }
                                #endregion

                                #region "Domestic/International"
                                if (fieldname.ToLower() == "dutytype1") {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(value1)))
                                    {
                                        value1 = Convert.ToInt32(value1) == 1 ? "Domestic" : "International";
                                        value2 = Convert.ToInt32(value2) == 1 ? "Domestic" : "International";
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Duty Type1 ",value1,value2));
                                    }
                                }
                                #endregion

                                #region "DepartIcaoID"

                                if (fieldname.ToLower() == "departicaoid")
                                {
                                    Int64 OldDepartICAOID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldDepartICAOID = (Int64)value1;

                                    Int64 NewDepartICAOID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewDepartICAOID = (Int64)value2;

                                    IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, OldDepartICAOID, CustomerID);
                                    Airport OldAirport = new Airport();
                                    if (Airports != null && Airports.ToList().Count > 0)
                                        OldAirport = Airports.ToList()[0];
                                    else
                                    {
                                        // Fix for #8222
                                        IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, OldDepartICAOID);
                                        OldAirport = airportList.ToList()[0];
                                    }

                                    Airports = preflightCompiledQuery.getAirport(Container, NewDepartICAOID, CustomerID);
                                    Airport NewAirport = new Airport();
                                    if (Airports != null && Airports.ToList().Count > 0)
                                        NewAirport = Airports.ToList()[0];
                                    else
                                    {
                                        // Fix for #8222
                                        IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, NewDepartICAOID);
                                        NewAirport = airportList.ToList()[0];
                                    }


                                    if (Trip.RecordType == "T")
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Departs ICAO", OldAirport.IcaoID, NewAirport.IcaoID));
                                    else
                                        HistoryDescription.AppendLine(FormatHistoryValueChange("Departs ICAO", OldAirport.IcaoID, NewAirport.IcaoID,false));

                                }

                                #endregion

                                #region "ArriveIcaoID"

                                if (fieldname.ToLower() == "arriveicaoid")
                                {
                                    Int64 OldArriveICAOID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldArriveICAOID = (Int64)value1;

                                    Int64 NewArriveICAOID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewArriveICAOID = (Int64)value2;

                                    IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, OldArriveICAOID, CustomerID);
                                    Airport OldAirport = new Airport();
                                    if (Airports != null && Airports.ToList().Count > 0)
                                        OldAirport = Airports.ToList()[0];
                                    else
                                    {
                                        // Fix for #8222
                                        IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, OldArriveICAOID);
                                        OldAirport = airportList.ToList()[0];
                                    }

                                    Airports = preflightCompiledQuery.getAirport(Container, NewArriveICAOID, CustomerID);
                                    Airport NewAirport = new Airport();
                                    if (Airports != null && Airports.ToList().Count > 0)
                                        NewAirport = Airports.ToList()[0];
                                    else
                                    {
                                        // Fix for #8222
                                        IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, NewArriveICAOID);
                                        NewAirport = airportList.ToList()[0];
                                    }

                                    if (Trip.RecordType == "T")
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Arrives ICAO", OldAirport.IcaoID, NewAirport.IcaoID));
                                }

                                #endregion

                                #region "Departure Date Local"
                                if (fieldname.ToLower() == "departuredttmlocal")
                                {

                                    if (Trip.RecordType == "T")
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Departure Local Date/Time",value1,value2,true));
                                    else
                                        HistoryDescription.AppendLine(FormatHistoryValueChange("Start Date/Time", value1, value2, true));
                                }


                                #endregion

                                #region "Arrival Date Arrival"
                                if (fieldname.ToLower() == "arrivaldttmlocal")
                                {
                                    if (Trip.RecordType == "T")
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Arrival Local Date/Time",value1,value2,true));
                                    else
                                        HistoryDescription.AppendLine(FormatHistoryValueChange("End Date/Time", value1, value2, true));

                                }

                                #endregion

                                #region "Purpose"
                                if (fieldname.ToLower() == "flightpurpose")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Trip Purpose", value1, value2, false));
                                }
                                #endregion

                                #region "Passenger"

                                if (fieldname.ToLower() == "passengerrequestorid")
                                {
                                    Int64 OldPassengerID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldPassengerID = (Int64)value1;

                                    Int64 NewPassengerID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewPassengerID = (Int64)value2;

                                    IQueryable<Passenger> Passengers = preflightCompiledQuery.getPassenger(Container, OldPassengerID, CustomerID);
                                    Passenger OldPassenger = new Passenger();
                                    if (Passengers != null && Passengers.ToList().Count > 0)
                                        OldPassenger = Passengers.ToList()[0];

                                    Passengers = preflightCompiledQuery.getPassenger(Container, NewPassengerID, CustomerID);
                                    Passenger NewPassenger = new Passenger();
                                    if (Passengers != null && Passengers.ToList().Count > 0)
                                        NewPassenger = Passengers.ToList()[0];

                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Requestor", OldPassenger.PassengerRequestorCD, NewPassenger.PassengerRequestorCD));
                                }


                                #endregion

                                #region "Account"


                                if (fieldname.ToLower() == "accountid")
                                {
                                    Int64 OldAccountID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldAccountID = (Int64)value1;
                                    Int64 NewAccountID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewAccountID = (Int64)value2;
                                    IQueryable<Account> Accounts = preflightCompiledQuery.getAccount(Container, OldAccountID, CustomerID);
                                    Account OldAccount = new Account();
                                    if (Accounts != null && Accounts.ToList().Count > 0)
                                        OldAccount = Accounts.ToList()[0];
                                    Accounts = preflightCompiledQuery.getAccount(Container, NewAccountID, CustomerID);
                                    Account NewAccount = new Account();
                                    if (Accounts != null && Accounts.ToList().Count > 0)
                                        NewAccount = Accounts.ToList()[0];
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Account", OldAccount.AccountNum, NewAccount.AccountNum));
                                }
                                #endregion
                                #region "Department"

                                if (fieldname.ToLower() == "departmentid")
                                {
                                    Int64 OldDepartmentID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldDepartmentID = (Int64)value1;

                                    Int64 NewDepartmentID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewDepartmentID = (Int64)value2;

                                    IQueryable<Department> Departments = preflightCompiledQuery.getDepartment(Container, OldDepartmentID, CustomerID);
                                    Department OldDepartment = new Department();
                                    if (Departments != null && Departments.ToList().Count > 0)
                                        OldDepartment = Departments.ToList()[0];

                                    Departments = preflightCompiledQuery.getDepartment(Container, NewDepartmentID, CustomerID);
                                    Department NewDepartment = new Department();
                                    if (Departments != null && Departments.ToList().Count > 0)
                                        NewDepartment = Departments.ToList()[0];

                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Department", OldDepartment.DepartmentCD, NewDepartment.DepartmentCD));
                                }


                                #endregion

                                #region "Authorization"

                                if (fieldname.ToLower() == "authorizationid")
                                {
                                    Int64 OldAuthorizationID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldAuthorizationID = (Int64)value1;

                                    Int64 NewAuthorizationID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewAuthorizationID = (Int64)value2;

                                    IQueryable<DepartmentAuthorization> DepartmentAuthorizations = preflightCompiledQuery.getDepartmentAuthorization(Container, OldAuthorizationID, CustomerID);
                                    DepartmentAuthorization OldDepartmentAuthorization = new DepartmentAuthorization();
                                    if (DepartmentAuthorizations != null && DepartmentAuthorizations.ToList().Count > 0)
                                        OldDepartmentAuthorization = DepartmentAuthorizations.ToList()[0];

                                    DepartmentAuthorizations = preflightCompiledQuery.getDepartmentAuthorization(Container, NewAuthorizationID, CustomerID);
                                    DepartmentAuthorization NewDepartmentAuthorization = new DepartmentAuthorization();
                                    if (DepartmentAuthorizations != null && DepartmentAuthorizations.ToList().Count > 0)
                                        NewDepartmentAuthorization = DepartmentAuthorizations.ToList()[0];

                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Authorization", OldDepartmentAuthorization.AuthorizationCD, NewDepartmentAuthorization.AuthorizationCD));
                                }


                                #endregion


                                #region "CQCustomer"

                                if (fieldname.ToLower() == "cqcustomerid")
                                {
                                    Int64 OldCQCustomerID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldCQCustomerID = (Int64)value1;

                                    Int64 NewCQCustomerID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewCQCustomerID = (Int64)value2;

                                    IQueryable<CQCustomer> CQCustomers = preflightCompiledQuery.getCQCustomer(Container, OldCQCustomerID, CustomerID);
                                    CQCustomer OldCQCustomer = new CQCustomer();
                                    if (CQCustomers != null && CQCustomers.ToList().Count > 0)
                                        OldCQCustomer = CQCustomers.ToList()[0];

                                    CQCustomers = preflightCompiledQuery.getCQCustomer(Container, NewCQCustomerID, CustomerID);
                                    CQCustomer NewCQCustomer = new CQCustomer();
                                    if (CQCustomers != null && CQCustomers.ToList().Count > 0)
                                        NewCQCustomer = CQCustomers.ToList()[0];

                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Charter Customer", OldCQCustomer.CQCustomerCD, NewCQCustomer.CQCustomerCD));
                                }


                                #endregion

                                #region "Client"

                                if (fieldname.ToLower() == "clientid")
                                {
                                    Int64 OldClientID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldClientID = (Int64)value1;

                                    Int64 NewClientID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewClientID = (Int64)value2;

                                    IQueryable<Client> Clients = preflightCompiledQuery.getClient(Container, OldClientID, CustomerID);
                                    Client OldClient = new Client();
                                    if (Clients != null && Clients.ToList().Count > 0)
                                        OldClient = Clients.ToList()[0];

                                    Clients = preflightCompiledQuery.getClient(Container, NewClientID, CustomerID);
                                    Client NewClient = new Client();
                                    if (Clients != null && Clients.ToList().Count > 0)
                                        NewClient = Clients.ToList()[0];
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Client", OldClient.ClientCD, NewClient.ClientCD));
                                }


                                #endregion

                                #region "Flight Category"

                                if (fieldname.ToLower() == "flightcategoryid")
                                {
                                    Int64 OldFlightCategoryID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldFlightCategoryID = (Int64)value1;

                                    Int64 NewFlightCategoryID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewFlightCategoryID = (Int64)value2;

                                    IQueryable<FlightCatagory> FlightCatagorys = preflightCompiledQuery.getFlightCatagory(Container, OldFlightCategoryID, CustomerID);
                                    FlightCatagory OldFlightCatagory = new FlightCatagory();
                                    if (FlightCatagorys != null && FlightCatagorys.ToList().Count > 0)
                                        OldFlightCatagory = FlightCatagorys.ToList()[0];

                                    FlightCatagorys = preflightCompiledQuery.getFlightCatagory(Container, NewFlightCategoryID, CustomerID);
                                    FlightCatagory NewFlightCatagory = new FlightCatagory();
                                    if (FlightCatagorys != null && FlightCatagorys.ToList().Count > 0)
                                        NewFlightCatagory = FlightCatagorys.ToList()[0];

                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Flight Category", OldFlightCatagory.FlightCatagoryCD, NewFlightCatagory.FlightCatagoryCD));
                                }


                                #endregion

                                #region "private"

                                if (fieldname.ToLower() == "isprivate")
                                {
                                    HistoryDescription.AppendLine(string.Format("Leg {0} Changed From {1} To {2}", originalLeg.LegNUM,
                                        ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Confidential" : "Not Confidential"),
                                        ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Confidential" : "Not Confidential")));
                                }
                                #endregion

                                #region "DutyEnd"
                                if (fieldname.ToLower() == "isdutyend")
                                {
                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Duty End",
                                    ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "End Of Duty" : "On Duty"),
                                    ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "End Of Duty" : "On Duty")));
                                }
                                #endregion

                                #region "ApproxTM"
                                if (fieldname.ToLower() == "isapproxtm")
                                {
                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "IsApproxTM",
                                    ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "ApproxTM" : "Not ApproxTM"),
                                    ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "ApproxTM" : "Not ApproxTM")));
                                }
                                #endregion

                                #region "FuelLoad"
                                if (fieldname.ToLower() == "crewfuelload")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Fuel", value1, value2));
                                }
                                #endregion

                                #region "Crew Notes"
                                if (fieldname.ToLower() == "crewnotes")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Crew Notes", value1, value2));
                                }


                                #endregion

                                #region "PAX Notes"
                                if (fieldname.ToLower() == "notes")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "PAX Notes", value1, value2));
                                }


                                #endregion

                                #region "USA Border Crossing"
                                if (fieldname.ToLower() == "uscrossing")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "USA Border Crossing", value1, value2));
                                }


                                #endregion

                                #region "UWA ID"
                                if (fieldname.ToLower() == "uwaid")
                                {
                                    if (value1.ToString().Trim() != value2.ToString().Trim())
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "APIS UWA ID", value1, value2));
                                }
                                #endregion

                                #region "OutbountInstruction"
                                if (fieldname.ToLower() == "outboundinstruction")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Outbound Instruction Comments", value1, value2));
                                }
                                #endregion

                                #region "ConfirmID"
                                if (fieldname.ToLower() == "confirmid")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "CBP Confirm No.", value1, value2));
                                }
                                #endregion

                                #region "Duty Type"
                                if (fieldname.ToLower() == "dutytype")
                                {
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Duty", value1, value2));
                                }
                                #endregion

                                #region   Estimated U.S. Gallons
                                if (fieldname.ToLower() == "estfuelqty")
                                {
                                    if (Leg.FuelUnits == null)
                                        Leg.FuelUnits = 0;
                                    string oldstring = string.Empty;
                                    decimal decval = 0;
                                    if (decimal.TryParse(value1.ToString(), out decval))
                                        oldstring = Math.Round(decval, 0).ToString();
                                    decval = 0;
                                    string newstring = string.Empty;
                                    if (decimal.TryParse(value2.ToString(), out decval))
                                        newstring = Math.Round(decval, 0).ToString();
                                    HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Estimated " + GetFuelUnitstring((decimal)Leg.FuelUnits), oldstring, newstring));
                                }
                                #endregion

                                #region   Fuel Units
                                if (fieldname.ToLower() == "fuelunits")
                                {
                                    if (value1 == null || value1.ToString() == string.Empty)
                                        value1 = "-1";
                                    if (value2 == null || value2.ToString() == string.Empty)
                                        value2 = "-1";
                                    decimal decval = 0;
                                    string oldstring = string.Empty;
                                    if (decimal.TryParse(value1.ToString(), out decval))
                                        oldstring = GetFuelUnitstring(decval);
                                    decval = 0;
                                    string newstring = string.Empty;
                                    if (decimal.TryParse(value2.ToString(), out decval))
                                        newstring = GetFuelUnitstring(decval);
                                    if (oldstring != newstring)
                                        HistoryDescription.AppendLine(FormateHistoryValueChange(Convert.ToString(originalLeg.LegNUM), "Fuel Units", oldstring, newstring));
                                }
                                #endregion

                            }
                        }
                                #endregion
                    }
                    else if (Leg.State == TripEntityState.Deleted)
                    {
                        if (originalLeg != null)
                        {
                            originalLeg.LastUpdTS = DateTime.UtcNow;
                            originalLeg.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.PreflightLegs.DeleteObject(originalLeg);
                        }
                        #region "History For Deleted Leg"
                        HistoryDescription.AppendLine(string.Format("Leg {0} Deleted", originalLeg.LegNUM));
                        #endregion
                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalLeg, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    Leg.IsDeleted = false;
                    Leg.CustomerID = CustomerID;
                    if (Trip.State != TripEntityState.Added)
                        PreflightMainentity.PreflightLegs.Add(Leg);
                    Container.ObjectStateManager.ChangeObjectState(Leg, System.Data.EntityState.Added);

                    #region "History"

                    if (Trip.RecordType == "T")
                    {
                        HistoryDescription.AppendLine("New Leg Added To Trip With Details:");
                        Airport DepAiport = new Airport();
                        Airport ArrAirport = new Airport();
                        if (Leg.DepartICAOID != null)
                        {

                            IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, (Int64)Leg.DepartICAOID, CustomerID);
                            if (Airports != null && Airports.ToList().Count > 0)
                                DepAiport = Airports.ToList()[0];
                            else
                            {
                                // Fix for #8222
                                IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, (Int64)Leg.DepartICAOID);
                                DepAiport = airportList.ToList()[0];
                            }
                            //DepAirport = Container.Airports.Where(x => x.AirportID == (Int64)Leg.DepartICAOID).SingleOrDefault();
                        }
                        if (Leg.ArriveICAOID != null)
                        {
                            IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, (Int64)Leg.ArriveICAOID, CustomerID);
                            if (Airports != null && Airports.ToList().Count > 0)
                                ArrAirport = Airports.ToList()[0];
                            else
                            {
                                // Fix for #8222
                                IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, (Int64)Leg.ArriveICAOID);
                                ArrAirport = airportList.ToList()[0];
                            }
                            //ArrAirport = Container.Airports.Where(x => x.AirportID == (Int64)Leg.ArriveICAOID).SingleOrDefault();
                        }
                        if (DepAiport != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} Departs ICAO: {1}", Leg.LegNUM, DepAiport.IcaoID));
                        if (ArrAirport != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} Arrives ICAO: {1}", Leg.LegNUM, ArrAirport.IcaoID));



                        if (Leg.DepartureDTTMLocal != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} {1}: {2}", Leg.LegNUM, "Departure Local Date/Time", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", (DateTime)Leg.DepartureDTTMLocal)));

                        if (Leg.ArrivalDTTMLocal != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} {1}: {2}", Leg.LegNUM, "Arrival Local Date/Time", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", (DateTime)Leg.ArrivalDTTMLocal)));

                        if (!string.IsNullOrEmpty(Leg.FlightNUM))
                            HistoryDescription.AppendLine(string.Format("Leg {0} Flight Num: {1}", Leg.LegNUM, Leg.FlightNUM));

                    }
                    #endregion
                }

                #region "Crews"

                #region "All Crews deleted"
                bool blCrewdelete = false;
                if (Trip.RecordType == "T" && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    foreach (PreflightLeg Leglist in Trip.PreflightLegs)
                    {
                        if (Leglist.PreflightCrewLists != null && Leglist.PreflightCrewLists.Count > 0)
                        {
                            List<PreflightCrewList> availcrew = new List<PreflightCrewList>();
                            availcrew = Leglist.PreflightCrewLists.Where(x => x.IsDeleted == false).ToList();

                            if (availcrew == null || availcrew.Count == 0)
                            {
                                blCrewdelete = true;
                                HistoryDescription.AppendLine(string.Format("All Crew Deleted for Leg: {0}", Leg.LegNUM));
                            }
                        }

                    }
                }
                #endregion

                foreach (PreflightCrewList crewList in Leg.PreflightCrewLists.ToList())
                {

                    PreflightCrewList OriginalCrewlist = new PreflightCrewList();
                    crewList.LastUpdTS = DateTime.UtcNow;
                    crewList.LastUpdUID = UserPrincipal.Identity.Name;

                    if (crewList.State != TripEntityState.Added)
                    {
                        OriginalCrewlist = Container.GetPreflightCrewListByPreflightCrewListID(CustomerID, crewList.PreflightCrewListID).SingleOrDefault();

                        // OriginalCrewlist = Container.PreflightCrewLists
                        //.Where(c => c.PreflightCrewListID == crewList.PreflightCrewListID)
                        //.SingleOrDefault();


                        // Is original child item with same ID in DB?   
                        if (crewList.State == TripEntityState.Modified)
                        {
                            if (OriginalCrewlist != null)
                            {
                                crewList.IsDeleted = false;
                                crewList.CustomerID = CustomerID;
                                Container.PreflightCrewLists.Attach(OriginalCrewlist);
                                Container.PreflightCrewLists.ApplyCurrentValues(crewList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Modified Crew"
                                    var originalCrewValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCrewlist).OriginalValues;
                                    var CurrentCrewValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCrewlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightCrewList).GetProperties();
                                    for (int i = 0; i < originalCrewValues.FieldCount; i++)
                                    {
                                        var fieldname = originalCrewValues.GetName(i);

                                        object value1 = originalCrewValues.GetValue(i);
                                        object value2 = CurrentCrewValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "Purpose"
                                            if (fieldname.ToLower() == "dutytype")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalCrewlist.DutyTYPE.ToString()))
                                                        HistoryDescription.AppendLine(string.Format("Dutytype for Crew {0} on Leg {1} changed from {2} to {3}", OriginalCrewlist.CrewLastName + " " + OriginalCrewlist.CrewFirstName + " " + OriginalCrewlist.CrewMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }

                                            }
                                            #endregion

                                            #region "Street"
                                            if (fieldname.ToLower() == "street")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalCrewlist.Street))
                                                        HistoryDescription.AppendLine(string.Format("Destination-Street for Crew {0} on Leg {1}  changed from {2} to {3} ", OriginalCrewlist.CrewLastName + " " + OriginalCrewlist.CrewFirstName + " " + OriginalCrewlist.CrewMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }

                                            }
                                            #endregion

                                            #region "City"
                                            if (fieldname.ToLower() == "cityname")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalCrewlist.CityName))
                                                        HistoryDescription.AppendLine(string.Format("Destination-City for Crew {0} on Leg {1}  changed from {2} to {3}", OriginalCrewlist.CrewLastName + " " + OriginalCrewlist.CrewFirstName + " " + OriginalCrewlist.CrewMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }

                                            }
                                            #endregion

                                            #region "Sate"
                                            if (fieldname.ToLower() == "statename")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalCrewlist.StateName))
                                                        HistoryDescription.AppendLine(string.Format("Destination-State for Crew {0} on Leg {1}  changed from {2} to {3}", OriginalCrewlist.CrewLastName + " " + OriginalCrewlist.CrewFirstName + " " + OriginalCrewlist.CrewMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }

                                            }
                                            #endregion

                                            #region "Pincode"
                                            if (fieldname.ToLower() == "postalzipcd")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalCrewlist.PostalZipCD))
                                                        HistoryDescription.AppendLine(string.Format("Destination-ZIP for Crew {0} on Leg {1}  changed from {2} to {3}", OriginalCrewlist.CrewLastName + " " + OriginalCrewlist.CrewFirstName + " " + OriginalCrewlist.CrewMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }

                                            }
                                            #endregion

                                            #region "PassportID"
                                            if (fieldname.ToLower() == "passportid")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalCrewlist.PassportID.ToString()))
                                                        HistoryDescription.AppendLine(string.Format("Passport Number of Crew {0} on Leg {1}  changed from {2} to {3}", OriginalCrewlist.CrewLastName + OriginalCrewlist.CrewFirstName + OriginalCrewlist.CrewMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }

                                            }
                                            #endregion
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                        else if (crewList.State == TripEntityState.Deleted)
                        {
                            if (OriginalCrewlist != null)
                            {
                                OriginalCrewlist.LastUpdTS = DateTime.UtcNow;
                                OriginalCrewlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightCrewLists.DeleteObject(OriginalCrewlist);

                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Deleted Crew"
                                    if (!blCrewdelete)
                                        HistoryDescription.AppendLine(string.Format("Crew {0}  in Leg {1}", OriginalCrewlist.CrewLastName + " " + OriginalCrewlist.CrewFirstName + " " + OriginalCrewlist.CrewMiddleName, Leg.LegNUM + " Deleted!"));
                                    #endregion
                                }
                            }
                        }

                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        crewList.IsDeleted = false;
                        crewList.CustomerID = CustomerID;
                        if (Leg.State != TripEntityState.Added)
                        {
                            if (originalLeg != null)
                                originalLeg.PreflightCrewLists.Add(crewList);
                        }
                        Container.ObjectStateManager.ChangeObjectState(crewList, System.Data.EntityState.Added);
                        if (Leg.State != TripEntityState.Deleted)
                        {
                            #region "History For Added Crew"
                            if (Trip.RecordType == "T")
                                HistoryDescription.AppendLine(string.Format("New Crew-member(s) Added to Trip Leg: {0}  Name: {1} Duty Type: {2} ", Leg.LegNUM, crewList.CrewLastName + " " + crewList.CrewFirstName + " " + crewList.CrewMiddleName, crewList.DutyTYPE));
                            else if (Trip.RecordType == "C")
                                HistoryDescription.AppendLine(string.Format("New Crew-member(s) Added Name: {0} Duty Type: {1} ", crewList.CrewLastName + " " + crewList.CrewFirstName + " " + crewList.CrewMiddleName, Leg.DutyTYPE));
                            #endregion
                        }
                    }

                    #region "HotelXrefList This needs to be removed "

                    foreach (PreflightCrewHotelList hotelList in crewList.PreflightCrewHotelLists.ToList())
                    {
                        hotelList.LastUpdTS = DateTime.UtcNow;
                        hotelList.LastUpdUID = UserPrincipal.Identity.Name;
                        hotelList.CustomerID = Leg.CustomerID.Value;

                        if (hotelList.State != TripEntityState.Added)
                        {
                            var OriginalHotellist = Container.GetPreflightCrewHotelListByPreflightCrewHotelListID(CustomerID, hotelList.PreflightCrewHotelListID).SingleOrDefault();

                            //var OriginalHotellist = Container.PreflightCrewHotelLists
                            //.Where(c => c.PreflightCrewHotelListID == hotelList.PreflightCrewHotelListID)
                            //.SingleOrDefault();

                            if (hotelList.State == TripEntityState.Modified)
                            {
                                hotelList.IsDeleted = false;
                                hotelList.PreflightCrewListID = crewList.PreflightCrewListID;
                                if (OriginalHotellist != null)
                                {
                                    Container.PreflightCrewHotelLists.Attach(OriginalHotellist);
                                    Container.PreflightCrewHotelLists.ApplyCurrentValues(hotelList);
                                    if (Leg.State != TripEntityState.Deleted)
                                    {
                                        #region "History For Modified Hotel Crew"
                                        var originalHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).OriginalValues;
                                        var CurrentHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).CurrentValues;
                                        PropertyInfo[] properties = typeof(PreflightCrewHotelList).GetProperties();
                                        for (int i = 0; i < originalHotelValues.FieldCount; i++)
                                        {
                                            var fieldname = originalHotelValues.GetName(i);

                                            object Orivalue = originalHotelValues.GetValue(i);
                                            object Currvalue = CurrentHotelValues.GetValue(i);

                                            Orivalue = Orivalue.ToString().Trim();
                                            Currvalue = Currvalue.ToString().Trim();

                                            object value1 = Orivalue;
                                            object value2 = Currvalue;

                                            if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                            {
                                                #region "History For Modified Crew Hotel"
                                                if (fieldname.ToLower() == "preflighthotelname")
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalHotellist.PreflightHotelName.ToString()) && !value1.Equals(value2))
                                                        HistoryDescription.AppendLine(string.Format("Crew Hotel has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                }
                                                #endregion

                                                #region "confirmationstatus"
                                                if (fieldname.ToLower() == "confirmationstatus")
                                                {
                                                    if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.ConfirmationStatus))
                                                            HistoryDescription.AppendLine(string.Format("Crew Hotel Confirm {0} added to leg  {1} ", value1, Leg.LegNUM));
                                                    }
                                                    else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.ConfirmationStatus))
                                                            HistoryDescription.AppendLine(string.Format("Crew Hotel Confirm has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                    }

                                                }
                                                #endregion

                                                #region "comments"
                                                if (fieldname.ToLower() == "comments")
                                                {
                                                    if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.Comments))
                                                            HistoryDescription.AppendLine(string.Format("Crew Hotel Comments {0} added to leg {1}", value1, Leg.LegNUM));
                                                    }
                                                    else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.Comments))
                                                            HistoryDescription.AppendLine(string.Format("Crew Hotel Comments has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                    }
                                                }
                                                #endregion

                                                #region "Status"


                                                if (fieldname.ToLower() == "status")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Status of Crew Hotel in Leg {0} Set {1}", Leg.LegNUM, value2));

                                                }
                                                //if (fieldname.ToLower() == "iscompleted")
                                                //{
                                                //    if (!string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "True")
                                                //    {
                                                //        HistoryDescription.AppendLine(string.Format("Status of Crew Hotel in Leg {0} Set Completed", Leg.LegNUM));
                                                //    }
                                                //    else
                                                //    {
                                                //        HistoryDescription.AppendLine(string.Format("Status of Crew Hotel in Leg {0} Set Not Completed", Leg.LegNUM));
                                                //    }

                                                //}
                                                #endregion

                                            }
                                        }

                                        #endregion
                                    }
                                }
                            }
                            else if (hotelList.State == TripEntityState.Deleted)
                            {
                                if (OriginalHotellist != null)
                                {
                                    OriginalHotellist.LastUpdTS = DateTime.UtcNow;
                                    OriginalHotellist.LastUpdUID = UserPrincipal.Identity.Name;
                                    Container.PreflightCrewHotelLists.DeleteObject(OriginalHotellist);
                                    if (Leg.State != TripEntityState.Deleted)
                                    {
                                        #region "History For Deleted Hotel"
                                        HistoryDescription.AppendLine(string.Format("Crew Hotel {1} for Leg {0} Deleted!", originalLeg.LegNUM, OriginalHotellist.PreflightHotelName));
                                        #endregion
                                    }
                                }
                            }
                        }
                        else
                        {
                            // No -> It's a new child item -> Insert             
                            hotelList.IsDeleted = false;
                            if (crewList.State != TripEntityState.Added)
                                if (OriginalCrewlist != null)
                                    OriginalCrewlist.PreflightCrewHotelLists.Add(hotelList);
                            Container.ObjectStateManager.ChangeObjectState(hotelList, System.Data.EntityState.Added);
                            if (Leg.State != TripEntityState.Deleted)
                            {
                                #region "History For Added Crew Hotel"
                                HistoryDescription.AppendLine(string.Format("New Crew Hotel {0} added to leg {1} ", hotelList.PreflightHotelName, Leg.LegNUM));
                                if (!string.IsNullOrEmpty(hotelList.Comments))
                                {
                                    HistoryDescription.AppendLine(string.Format("Crew Hotel Comments {0} added to leg {1}", hotelList.Comments, Leg.LegNUM));
                                }
                                if (!string.IsNullOrEmpty(hotelList.ConfirmationStatus))
                                {
                                    HistoryDescription.AppendLine(string.Format("Crew Hotel Confirm {0} added to leg {1}", hotelList.ConfirmationStatus, Leg.LegNUM));
                                }
                                if (!string.IsNullOrEmpty(hotelList.Status))
                                {
                                    HistoryDescription.AppendLine(string.Format("Status of Crew Hotel in Leg {0} Set {1}", Leg.LegNUM, hotelList.Status));
                                }

                                #endregion
                            }
                        }

                    }
                    #endregion
                }
                #endregion

                #region PreflightHotel Common for Crew/PAX

                foreach (PreflightHotelList HotelList in Leg.PreflightHotelLists.ToList())
                {

                    PreflightHotelList OriginalHotellist = new PreflightHotelList();
                    HotelList.LastUpdTS = DateTime.UtcNow;
                    HotelList.LastUpdUID = UserPrincipal.Identity.Name;

                    if (HotelList.State != TripEntityState.Added)
                    {
                        //Need to change this.
                        OriginalHotellist = Container.GetPreflightHotelListByPreflightHotelListID(CustomerID, HotelList.PreflightHotelListID).SingleOrDefault();


                        // Is original child item with same ID in DB?   
                        if (HotelList.State == TripEntityState.Modified)
                        {
                            if (OriginalHotellist != null)
                            {
                                HotelList.IsDeleted = false;
                                HotelList.CustomerID = CustomerID;
                                Container.PreflightHotelLists.Attach(OriginalHotellist);
                                Container.PreflightHotelLists.ApplyCurrentValues(HotelList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Modified Hotel"
                                    var originalHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).OriginalValues;
                                    var CurrentHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightCrewHotelList).GetProperties();
                                    for (int i = 0; i < originalHotelValues.FieldCount; i++)
                                    {
                                        var fieldname = originalHotelValues.GetName(i);

                                        object Orivalue = originalHotelValues.GetValue(i);
                                        object Currvalue = CurrentHotelValues.GetValue(i);

                                        Orivalue = Orivalue.ToString().Trim();
                                        Currvalue = Currvalue.ToString().Trim();

                                        object value1 = Orivalue;
                                        object value2 = Currvalue;

                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "History For Modified Crew Hotel"
                                            if (fieldname.ToLower() == "preflighthotelname")
                                            {
                                                if (!string.IsNullOrEmpty(OriginalHotellist.PreflightHotelName.ToString()) && !value1.Equals(value2))
                                                    HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                            }

                                            if (fieldname.ToLower() == "cityname")
                                            {
                                                if (!string.IsNullOrEmpty(OriginalHotellist.PreflightHotelName.ToString()) && !value1.Equals(value2))
                                                    HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel City has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                            }


                                            if (fieldname.ToLower() == "stateprovince")
                                            {
                                                if (!string.IsNullOrEmpty(OriginalHotellist.StateProvince.ToString()) && !value1.Equals(value2))
                                                    HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Address1 has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                            }


                                            if (fieldname.ToLower() == "postalcode")
                                            {
                                                if (!string.IsNullOrEmpty(OriginalHotellist.PostalCode.ToString()) && !value1.Equals(value2))
                                                    HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Address1 has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                            }

                                            if (fieldname.ToLower() == "faxnum")
                                            {
                                                if (!string.IsNullOrEmpty(OriginalHotellist.FaxNUM.ToString()) && !value1.Equals(value2))
                                                    HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Address1 has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                            }
                                            #endregion

                                            #region "confirmationstatus"
                                            if (fieldname.ToLower() == "confirmationstatus")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalHotellist.ConfirmationStatus))
                                                        HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Confirm {0} added to leg  {1} ", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalHotellist.ConfirmationStatus))
                                                        HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Confirm has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                }

                                            }
                                            #endregion

                                            #region "comments"
                                            if (fieldname.ToLower() == "comments")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalHotellist.Comments))
                                                        HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Comments {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalHotellist.Comments))
                                                        HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Comments has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                }
                                            }
                                            #endregion

                                            #region "Status"
                                            if (fieldname.ToLower() == "status")
                                            {
                                                HistoryDescription.AppendLine(string.Format("Status of " + GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel in Leg {0} Set {1}", Leg.LegNUM, value2));
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        else if (HotelList.State == TripEntityState.Deleted)
                        {
                            if (OriginalHotellist != null)
                            {
                                OriginalHotellist.LastUpdTS = DateTime.UtcNow;
                                OriginalHotellist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightHotelLists.DeleteObject(OriginalHotellist);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Deleted Hotel"
                                    HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel {1} for Leg {0} Deleted!", originalLeg.LegNUM, OriginalHotellist.PreflightHotelName));
                                    #endregion
                                }
                            }
                        }
                    }
                    else
                    {
                        HotelList.IsDeleted = false;
                        HotelList.CustomerID = CustomerID;
                        if (Leg.State != TripEntityState.Added)
                        {
                            if (originalLeg != null)
                                originalLeg.PreflightHotelLists.Add(HotelList);
                        }
                        Container.ObjectStateManager.ChangeObjectState(HotelList, System.Data.EntityState.Added);
                        if (Leg.State != TripEntityState.Deleted)
                        {
                            #region "History For Added Crew Hotel"

                            HistoryDescription.AppendLine(string.Format("New " + GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel {0} added to leg {1} ", HotelList.PreflightHotelName, Leg.LegNUM));

                            if (!string.IsNullOrEmpty(HotelList.Comments))
                            {

                                HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Comments {0} added to leg {1}", HotelList.Comments, Leg.LegNUM));

                            }

                            if (!string.IsNullOrEmpty(HotelList.ConfirmationStatus))
                            {

                                HistoryDescription.AppendLine(string.Format(GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel Confirm {0} added to leg {1}", HotelList.ConfirmationStatus, Leg.LegNUM));

                            }

                            if (!string.IsNullOrEmpty(HotelList.Status))
                            {

                                HistoryDescription.AppendLine(string.Format("Status of " + GetCrewOrPAXString(HotelList.crewPassengerType) + GetDepartOrArrivalString((bool)HotelList.isDepartureHotel) + "Hotel in Leg {0} Set {1}", Leg.LegNUM, HotelList.Status));

                            }


                            #endregion
                        }
                    }
                    #region PreflightHotelCrewPassengerList


                    if (HotelList.State == TripEntityState.Modified && OriginalHotellist != null)
                    {
                        foreach (PreflightHotelCrewPassengerList PreflightHotelCrewPassengerList in OriginalHotellist.PreflightHotelCrewPassengerLists.ToList())
                        {
                            Container.ObjectStateManager.ChangeObjectState(PreflightHotelCrewPassengerList, System.Data.EntityState.Deleted);
                        }
                    }



                    //if (HotelList.State != TripEntityState.NoChange)

                    //{


                    // if (HotelList.State != TripEntityState.Added)

                    // {

                    #region CrewID List

                    if (HotelList.CrewIDList != null)
                    {

                        foreach (Int64 CrewID in HotelList.CrewIDList)
                        {

                            PreflightHotelCrewPassengerList CrewList = new PreflightHotelCrewPassengerList();


                            CrewList.PreflightHotelListID = HotelList.PreflightHotelListID;

                            CrewList.CrewID = CrewID;

                            CrewList.CustomerID = HotelList.CustomerID;

                            CrewList.LastUpdUID = UserPrincipal.Identity.Name;

                            CrewList.LastUpdTS = DateTime.UtcNow;

                            CrewList.PassengerRequestorID = null;

                            if (HotelList.State == TripEntityState.Added)

                                HotelList.PreflightHotelCrewPassengerLists.Add(CrewList);

                            else if (HotelList.State == TripEntityState.Modified)

                                OriginalHotellist.PreflightHotelCrewPassengerLists.Add(CrewList);

                            if (HotelList.State == TripEntityState.Added || HotelList.State == TripEntityState.Modified)

                                Container.ObjectStateManager.ChangeObjectState(CrewList, System.Data.EntityState.Added);

                        }

                    }


                    #endregion

                    #region PAXID List

                    if (HotelList.PAXIDList != null)
                    {

                        foreach (Int64 PAXID in HotelList.PAXIDList)
                        {

                            PreflightHotelCrewPassengerList PaxList = new PreflightHotelCrewPassengerList();


                            PaxList.PreflightHotelListID = HotelList.PreflightHotelListID;

                            PaxList.CrewID = null;

                            PaxList.CustomerID = HotelList.CustomerID;

                            PaxList.LastUpdUID = UserPrincipal.Identity.Name;

                            PaxList.LastUpdTS = DateTime.UtcNow;

                            PaxList.PassengerRequestorID = PAXID;

                            if (HotelList.State == TripEntityState.Added)

                                HotelList.PreflightHotelCrewPassengerLists.Add(PaxList);

                            else if (HotelList.State == TripEntityState.Modified)

                                OriginalHotellist.PreflightHotelCrewPassengerLists.Add(PaxList);

                            if (HotelList.State == TripEntityState.Added || HotelList.State == TripEntityState.Modified)

                                Container.ObjectStateManager.ChangeObjectState(PaxList, System.Data.EntityState.Added);

                        }

                    }

                    #endregion

                    #region Commented

                    // }

                    //}

                    //else

                    //{

                    // #region CrewID List

                    // if (HotelList.CrewIDList != null)

                    // {

                    // foreach (Int64 CrewID in HotelList.CrewIDList)

                    // {

                    // PreflightHotelCrewPassengerList CrewList = new PreflightHotelCrewPassengerList();


                    // CrewList.PreflightHotelListID = HotelList.PreflightHotelListID;

                    // CrewList.CrewID = CrewID;

                    // CrewList.CustomerID = HotelList.CustomerID;

                    // CrewList.LastUpdUID = UserPrincipal.Identity.Name;

                    // CrewList.LastUpdTS = DateTime.UtcNow;

                    // CrewList.PassengerRequestorID = null;

                    // HotelList.PreflightHotelCrewPassengerLists.Add(CrewList);

                    // }

                    // }

                    // #endregion

                    // #region PAXID List

                    // if (HotelList.PAXIDList != null)

                    // {

                    // foreach (Int64 PAXID in HotelList.PAXIDList)

                    // {

                    // PreflightHotelCrewPassengerList PaxList = new PreflightHotelCrewPassengerList();


                    // PaxList.PreflightHotelListID = HotelList.PreflightHotelListID;

                    // PaxList.CrewID = null;

                    // PaxList.CustomerID = HotelList.CustomerID;

                    // PaxList.LastUpdUID = UserPrincipal.Identity.Name;

                    // PaxList.LastUpdTS = DateTime.UtcNow;

                    // PaxList.PassengerRequestorID = PAXID;

                    // HotelList.PreflightHotelCrewPassengerLists.Add(PaxList);

                    // }

                    // }

                    // #endregion

                    //}

                    #endregion


                    #endregion
                }
                #endregion

                #region "PAX"

                #region "All PAX deleted"
                bool blPaxdelete = false;
                if (Trip.RecordType == "T" && Trip.PreflightLegs != null && Trip.PreflightLegs.Count > 0)
                {
                    foreach (PreflightLeg Leglist in Trip.PreflightLegs)
                    {
                        if (Leglist.PreflightPassengerLists != null && Leglist.PreflightPassengerLists.Count > 0)
                        {
                            List<PreflightPassengerList> availpax = new List<PreflightPassengerList>();
                            availpax = Leglist.PreflightPassengerLists.Where(x => x.IsDeleted == false).ToList();
                            if (availpax == null || availpax.Count == 0)
                            {
                                blPaxdelete = true;
                                HistoryDescription.AppendLine(string.Format("All Passenger Deleted for Leg: {0}", Leg.LegNUM));
                            }
                        }
                    }
                }
                #endregion

                foreach (PreflightPassengerList paxList in Leg.PreflightPassengerLists.ToList())
                {
                    PreflightPassengerList OriginalPaxlist = new PreflightPassengerList();
                    paxList.LastUpdTS = DateTime.UtcNow;
                    paxList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (paxList.State != TripEntityState.Added)
                    {
                        OriginalPaxlist = Container.GetPreflightPassengerListByPreflightPassengerListID(CustomerID, paxList.PreflightPassengerListID).SingleOrDefault();
                        if (paxList.State == TripEntityState.Modified)
                        {
                            paxList.IsDeleted = false;
                            paxList.CustomerID = CustomerID;
                            if (OriginalPaxlist != null)
                            {
                                Container.PreflightPassengerLists.Attach(OriginalPaxlist);
                                Container.PreflightPassengerLists.ApplyCurrentValues(paxList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Modified Pax"
                                    var originalPaxValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalPaxlist).OriginalValues;
                                    var CurrentPaxValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalPaxlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightPassengerList).GetProperties();
                                    for (int i = 0; i < originalPaxValues.FieldCount; i++)
                                    {
                                        var fieldname = originalPaxValues.GetName(i);
                                        object value1 = originalPaxValues.GetValue(i);
                                        object value2 = CurrentPaxValues.GetValue(i);
                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "Purpose"

                                            if (fieldname.ToLower() == "flightpurposeid")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    IQueryable<FlightPurpose> FlightPurposes = preflightCompiledQuery.getFlightPurpose(Container, (Int64)value1, CustomerID);
                                                    FlightPurpose OldPurpose = new FlightPurpose();
                                                    if (FlightPurposes != null && FlightPurposes.ToList().Count > 0)
                                                        OldPurpose = FlightPurposes.ToList()[0];
                                                    FlightPurposes = preflightCompiledQuery.getFlightPurpose(Container, (Int64)value2, CustomerID);
                                                    FlightPurpose NewPurpose = new FlightPurpose();
                                                    if (FlightPurposes != null && FlightPurposes.ToList().Count > 0)
                                                        NewPurpose = FlightPurposes.ToList()[0];
                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.FlightPurposeID.ToString()))
                                                        HistoryDescription.AppendLine(string.Format("Flight Purpose for Passenger {0} on Leg {1} changed from {2} to {3} ", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM, OldPurpose.FlightPurposeCD, NewPurpose.FlightPurposeCD + "."));
                                                }


                                            }

                                            #endregion
                                            #region "Street"
                                            if (fieldname.ToLower() == "street")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.Street))
                                                        HistoryDescription.AppendLine(string.Format("Destination-Street for Passenger {0} on Leg {1} changed from {2} to {3}", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }
                                            }

                                            #endregion
                                            #region "City"
                                            if (fieldname.ToLower() == "cityname")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.CityName))
                                                        HistoryDescription.AppendLine(string.Format("Destination-City for Passenger {0} on Leg {1} changed from {2} to {3}", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }
                                            }
                                            #endregion
                                            #region "Sate"
                                            if (fieldname.ToLower() == "statename")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.StateName))
                                                        HistoryDescription.AppendLine(string.Format("Destination-State for Passenger {0} on Leg {1} changed from {2} to {3}", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }
                                            }
                                            #endregion
                                            #region "Pincode"
                                            if (fieldname.ToLower() == "postalzipcd")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.PostalZipCD))
                                                        HistoryDescription.AppendLine(string.Format("Destination-ZIP for Passenger {0} on Leg {1} changed from {2} to {3}", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }
                                            }

                                            #endregion
                                            #region "PassportID"
                                            if (fieldname.ToLower() == "passportid")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.PassportID.ToString()))
                                                        HistoryDescription.AppendLine(string.Format("Passport Number of Passenger {0} on Leg {1} changed from {2} to {3}", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM, value1, value2 + "."));
                                                }
                                            }
                                            #endregion
                                        }

                                    }


                                    #endregion
                                }
                            }
                        }
                        else if (paxList.State == TripEntityState.Deleted)
                        {
                            if (OriginalPaxlist != null)
                            {
                                OriginalPaxlist.LastUpdTS = DateTime.UtcNow;
                                OriginalPaxlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightPassengerLists.DeleteObject(OriginalPaxlist);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Deleted Pax"
                                    if (!blPaxdelete)
                                        HistoryDescription.AppendLine(string.Format("Passenger {0} in Leg {1}", OriginalPaxlist.PassengerLastName + " " + OriginalPaxlist.PassengerFirstName + " " + OriginalPaxlist.PassengerMiddleName, Leg.LegNUM + " Deleted!"));
                                    #endregion
                                }
                            }
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert
                        paxList.IsDeleted = false;
                        paxList.CustomerID = CustomerID;
                        if (Leg.State != TripEntityState.Added)
                        {
                            if (originalLeg != null)
                                originalLeg.PreflightPassengerLists.Add(paxList);
                        }
                        Container.ObjectStateManager.ChangeObjectState(paxList, System.Data.EntityState.Added);
                        if (Leg.State != TripEntityState.Deleted)
                        {
                            #region "History For Added Pax"
                            IQueryable<FlightPurpose> FlightPurposes = preflightCompiledQuery.getFlightPurpose(Container, Convert.ToInt64(paxList.FlightPurposeID), CustomerID);
                            FlightPurpose fpPurpose = new FlightPurpose();
                            if (FlightPurposes != null && FlightPurposes.ToList().Count > 0)
                                fpPurpose = FlightPurposes.ToList()[0];
                            HistoryDescription.AppendLine(string.Format("New Passenger(s) Added to Trip Leg: {0} Code: {1} Name: {2} Purpose: {3}", Leg.LegNUM, paxList.Passenger.PassengerRequestorCD, paxList.PassengerLastName + " " + paxList.PassengerFirstName + " " + paxList.PassengerMiddleName, fpPurpose.FlightPurposeCD));
                            #endregion
                        }
                    }
                    #region "HotelXrefList"
                    foreach (PreflightPassengerHotelList hotelList in paxList.PreflightPassengerHotelLists.ToList())
                    {
                        hotelList.CustomerID = CustomerID;
                        hotelList.LastUpdTS = DateTime.UtcNow;
                        hotelList.LastUpdUID = UserPrincipal.Identity.Name;
                        if (hotelList.State != TripEntityState.Added)
                        {
                            var OriginalHotellist = Container.GetPreflightPassengerHotelListByPreflightPassengerHotelListID(CustomerID, hotelList.PreflightPassengerHotelListID).SingleOrDefault();
                            if (hotelList.State == TripEntityState.Modified)
                            {
                                hotelList.IsDeleted = false;
                                hotelList.PreflightPassengerListID = paxList.PreflightPassengerListID;
                                if (OriginalHotellist != null)
                                {
                                    Container.PreflightPassengerHotelLists.Attach(OriginalHotellist);
                                    Container.PreflightPassengerHotelLists.ApplyCurrentValues(hotelList);
                                    if (Leg.State != TripEntityState.Deleted)
                                    {
                                        #region "History For Modified Hotel Pax"
                                        var originalHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).OriginalValues;
                                        var CurrentHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).CurrentValues;
                                        PropertyInfo[] properties = typeof(PreflightPassengerHotelList).GetProperties();
                                        for (int i = 0; i < originalHotelValues.FieldCount; i++)
                                        {
                                            var fieldname = originalHotelValues.GetName(i);
                                            object value1 = originalHotelValues.GetValue(i);
                                            object value2 = CurrentHotelValues.GetValue(i);
                                            if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                            {
                                                #region "History For Modified Passenger Hotel"
                                                if (fieldname.ToLower() == "preflighthotelname")
                                                {
                                                    if (!string.IsNullOrEmpty(OriginalHotellist.PreflightHotelName.ToString()) && !value1.Equals(value2))
                                                        HistoryDescription.AppendLine(string.Format("Passenger Hotel has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                }

                                                #endregion
                                                #region "confirmationstatus"
                                                if (fieldname.ToLower() == "confirmationstatus")
                                                {
                                                    if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.ConfirmationStatus))
                                                            HistoryDescription.AppendLine(string.Format("Passenger Hotel Confirm {0} added to leg {1}", value1, Leg.LegNUM));
                                                    }
                                                    else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.ConfirmationStatus))
                                                            HistoryDescription.AppendLine(string.Format("Passenger Hotel Confirm has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                    }
                                                }

                                                #endregion
                                                #region "comments"
                                                if (fieldname.ToLower() == "comments")
                                                {
                                                    if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.Comments))
                                                            HistoryDescription.AppendLine(string.Format("Passenger Hotel Comments {0} added to leg {1}", value1, Leg.LegNUM));
                                                    }
                                                    else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                    {
                                                        if (!string.IsNullOrEmpty(OriginalHotellist.Comments))
                                                            HistoryDescription.AppendLine(string.Format("Passenger Hotel Comments has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                    }
                                                }

                                                #endregion
                                                #region "Status"
                                                if (fieldname.ToLower() == "status")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Status of Passenger Hotel in Leg {0} Set {1}", Leg.LegNUM, value2));
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                            else if (hotelList.State == TripEntityState.Deleted)
                            {

                                if (OriginalHotellist != null)
                                {

                                    OriginalHotellist.LastUpdTS = DateTime.UtcNow;

                                    OriginalHotellist.LastUpdUID = UserPrincipal.Identity.Name;

                                    Container.PreflightPassengerHotelLists.DeleteObject(OriginalHotellist);

                                    if (Leg.State != TripEntityState.Deleted)
                                    {

                                        #region "History For Deleted Hotel"

                                        HistoryDescription.AppendLine(string.Format("Passenger Hotel {0} for Leg {1} Deleted!", OriginalHotellist.PreflightHotelName, Leg.LegNUM));

                                        #endregion

                                    }

                                }

                            }

                        }

                        else
                        {

                            // No -> It's a new child item -> Insert

                            hotelList.IsDeleted = false;

                            if (paxList.State != TripEntityState.Added)

                                if (OriginalPaxlist != null)

                                    OriginalPaxlist.PreflightPassengerHotelLists.Add(hotelList);

                            //hotelList.PreflightPassengerListID = paxList.PreflightPassengerListID;

                            Container.ObjectStateManager.ChangeObjectState(hotelList, System.Data.EntityState.Added);

                            if (Leg.State != TripEntityState.Deleted)
                            {

                                #region "History For Added Pax Hotel"



                                HistoryDescription.AppendLine(string.Format("New Passenger Hotel {0} added to leg {1} ", hotelList.PreflightHotelName, Leg.LegNUM));


                                if (!string.IsNullOrEmpty(hotelList.Comments))
                                {

                                    HistoryDescription.AppendLine(string.Format("Passenger Hotel Comments {0} added to leg {1}", hotelList.Comments, Leg.LegNUM));

                                }

                                if (!string.IsNullOrEmpty(hotelList.ConfirmationStatus))
                                {

                                    HistoryDescription.AppendLine(string.Format("Passenger Hotel Confirm {0} added to leg {1}", hotelList.ConfirmationStatus, Leg.LegNUM));

                                }

                                if (!string.IsNullOrEmpty(hotelList.Status))
                                {

                                    HistoryDescription.AppendLine(string.Format("Status of Passenger Hotel in Leg {0} Set {1}", Leg.LegNUM, hotelList.Status));

                                }


                                #endregion

                            }

                        }


                    }

                    #endregion

                }

                #endregion

                #region "Hotel"
                foreach (PreflightHotelList hotelList in Leg.PreflightHotelLists)
                {
                    hotelList.CustomerID = Leg.CustomerID.Value;
                    hotelList.LastUpdTS = DateTime.UtcNow;
                    hotelList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (hotelList.State != TripEntityState.Added)
                    {
                        var orignalHotelList =
                        Container.GetPreflightHotelListByPreflightHotelListID(CustomerID,
                        hotelList.PreflightHotelListID).SingleOrDefault();
                        if (hotelList.State == TripEntityState.Modified)
                        {
                            hotelList.IsDeleted = false;
                            hotelList.LegID = Leg.LegID;
                            if (orignalHotelList != null)
                            {
                                Container.PreflightHotelLists.ApplyCurrentValues(hotelList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    var orginalHotelValues = Container.ObjectStateManager.GetObjectStateEntry(orignalHotelList).OriginalValues;
                                    var currentHotelValues = Container.ObjectStateManager.GetObjectStateEntry(orignalHotelList).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightHotelList).GetProperties();
                                    for (int i = 0; i < orginalHotelValues.FieldCount; i++)
                                    {
                                        var fieldname = orginalHotelValues.GetName(i);
                                        object value1 = orginalHotelValues.GetValue(i);
                                        object value2 = currentHotelValues.GetValue(i);
                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            if (fieldname.ToLower() == "preflighthotelname")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Name {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Name has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "address1")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Address1 {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Address1 has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "address2")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Address2 {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Address2 has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "address3")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Address3 {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Address3 has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "city")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel City {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel City has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "countrycode")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Country {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Country has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "faxnum")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Fax number {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Fax number has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "postalcode")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Postal code {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Postal code has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "phonenum1")
                                            {

                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Phone number {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Phone number has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "rate")
                                            {

                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Rate {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Rate has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "datein")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel DateIn {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel DateIn has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "dateout")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel DateOut {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel DateOut has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "noofrooms")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel No of Rooms {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel No Of Rooms has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "noofbeds")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel No of Beds {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel No Of Beds has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "roomtype")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Room type {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Room type has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "roomdescription")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Room description {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Room description has been changed for {1} on leg {0} from {2} to {3}", value1, Leg.LegNUM));
                                                }
                                            }

                                            if (fieldname.ToLower() == "clubcard")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Club Card {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Club Card has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }
                                            }

                                            if (fieldname.ToLower() == "maxcapamount")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Max Cap Amount {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(
                                                    string.Format("The Preflight Hotel Max cap Amount has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "comments")
                                            {

                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Comments {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Comments has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "confirmationstatus")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Confirmations Status {0} added to leg {1}", value1, Leg.LegNUM));
                                                }

                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Hotel Confirmations Status has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                            if (fieldname.ToLower() == "status")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("New Preflight Hotel Status {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("The Preflight Hotel Status has been changed for {1} on leg {0} from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }


                #endregion

                #region "Transport"

                #region "Transport History"
                if (Leg.State != TripEntityState.Deleted)
                {
                    if (originalLeg != null)
                    {
                        #region departure Crew Transport
                        PreflightTransportList olddepTransport = new PreflightTransportList();
                        PreflightTransportList newdepTransport = new PreflightTransportList();
                        olddepTransport = originalLeg.PreflightTransportLists.Where(x => x.IsDepartureTransport == true && x.CrewPassengerType == "C" && x.IsDeleted == false).FirstOrDefault();
                        newdepTransport = Leg.PreflightTransportLists.Where(x => x.IsDepartureTransport == true && x.CrewPassengerType == "C" && x.IsDeleted == false).FirstOrDefault();

                        if (olddepTransport == null && newdepTransport != null)
                        {
                            //transport = Container.Transports.Where(x => x.TransportID == (Int64)newdepTransport.TransportID).SingleOrDefault();
                            HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation {0} added to leg {1}", newdepTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newdepTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation Comments {0} added to leg {1}", newdepTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newdepTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation Confirm {0} added to leg {1}", newdepTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newdepTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Crew Arrival Transportation in Leg {0} Set {1}", Leg.LegNUM, newdepTransport.Status));
                            }
                        }

                        if (olddepTransport != null && newdepTransport != null && ((olddepTransport.TransportID != null && newdepTransport.TransportID != null && (olddepTransport.TransportID != newdepTransport.TransportID))))
                        {
                            //if (Leg.Airport1 != null && Leg.Airport != null)
                            //    HistoryDescription.AppendLine(string.Format("Crew Departure Transportation has been changed for leg {0} from {1}", Leg.LegNUM, Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID));
                            //else
                            HistoryDescription.AppendLine(string.Format("Crew Departure Transportation has been changed for leg {0} from {1} to {2}", Leg.LegNUM, olddepTransport.PreflightTransportName, newdepTransport.PreflightTransportName));
                        }

                        #endregion

                        #region departure PAX Transport
                        PreflightTransportList oldPaxdepTransport = new PreflightTransportList();
                        PreflightTransportList newPaxdepTransport = new PreflightTransportList();
                        oldPaxdepTransport = originalLeg.PreflightTransportLists.Where(x => x.IsDepartureTransport == true && x.CrewPassengerType == "P" && x.IsDeleted == false).FirstOrDefault();
                        newPaxdepTransport = Leg.PreflightTransportLists.Where(x => x.IsDepartureTransport == true && x.CrewPassengerType == "P" && x.IsDeleted == false).FirstOrDefault();


                        if (oldPaxdepTransport == null && newPaxdepTransport != null)
                        {
                            HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation {0} added to leg {1}", newPaxdepTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newPaxdepTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation Comments {0} added to leg {1}", newPaxdepTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxdepTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation Confirm {0} added to leg {1}", newPaxdepTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxdepTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Passenger Departure Transportation in Leg {0} Set {1}", Leg.LegNUM, newPaxdepTransport.Status));
                            }


                        }

                        if (oldPaxdepTransport != null && newPaxdepTransport != null && ((oldPaxdepTransport.PreflightTransportName != null && newPaxdepTransport.PreflightTransportName != null && (oldPaxdepTransport.PreflightTransportName != newPaxdepTransport.PreflightTransportName))))
                        {
                            //if (Leg.Airport1 != null && Leg.Airport != null)
                            //    HistoryDescription.AppendLine(string.Format("Passenger Departure Transportation has been changed for leg {0} from {1}", Leg.LegNUM, Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID));
                            //else
                            HistoryDescription.AppendLine(string.Format("Passenger Departure Transportation has been changed for leg {0} from {1} to {2}", Leg.LegNUM, oldPaxdepTransport.PreflightTransportName, newPaxdepTransport.PreflightTransportName));
                        }
                        #endregion

                        #region arrival Crew Transport
                        PreflightTransportList oldarrTransport = new PreflightTransportList();
                        PreflightTransportList newarrTransport = new PreflightTransportList();
                        oldarrTransport = originalLeg.PreflightTransportLists.Where(x => x.IsArrivalTransport == true && x.CrewPassengerType == "C" && x.IsDeleted == false).FirstOrDefault();
                        newarrTransport = Leg.PreflightTransportLists.Where(x => x.IsArrivalTransport == true && x.CrewPassengerType == "C" && x.IsDeleted == false).FirstOrDefault();

                        if (oldarrTransport == null && newarrTransport != null)
                        {
                            HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation {0} added to leg{1}", newarrTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newarrTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation Comments {0} added to leg {1}", newarrTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newarrTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation Confirm {0} added to leg {1}", newarrTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newarrTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Crew Arrival Transportation in Leg {0} Set {1}", Leg.LegNUM, newarrTransport.Status));
                            }
                        }

                        if (oldarrTransport != null && newarrTransport != null && ((oldarrTransport.PreflightTransportName != null && newarrTransport.PreflightTransportName != null && (oldarrTransport.PreflightTransportName != newarrTransport.PreflightTransportName))))
                        {
                            //if (newarrTransport.TransportID != null)
                            //{
                            //if (Leg.Airport1 != null && Leg.Airport != null)
                            //    HistoryDescription.AppendLine(string.Format("Crew Arrival Transportation has been changed for leg {0} from {1}", Leg.LegNUM, Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID));
                            //else
                            HistoryDescription.AppendLine(string.Format("Crew Arrival Transportation has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, oldarrTransport.PreflightTransportName, newarrTransport.PreflightTransportName));

                            //}
                        }

                        #endregion

                        #region arrival PAX Transport
                        PreflightTransportList oldPaxarrTransport = new PreflightTransportList();
                        PreflightTransportList newPaxarrTransport = new PreflightTransportList();
                        oldPaxarrTransport = originalLeg.PreflightTransportLists.Where(x => x.IsArrivalTransport == true && x.CrewPassengerType == "P" && x.IsDeleted == false).FirstOrDefault();
                        newPaxarrTransport = Leg.PreflightTransportLists.Where(x => x.IsArrivalTransport == true && x.CrewPassengerType == "P" && x.IsDeleted == false).FirstOrDefault();




                        if (oldPaxarrTransport == null && newPaxarrTransport != null)
                        {

                            HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation {0} added to leg {1}", newPaxarrTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newPaxarrTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation Comments {0} added to leg {1}", newPaxarrTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxarrTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation Confirm {0} added to leg {1}", newPaxarrTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxarrTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Passenger Arrival Transportation in Leg {0} Set {1}", Leg.LegNUM, newPaxarrTransport.Status));
                            }
                        }

                        if (oldPaxarrTransport != null && newPaxarrTransport != null && ((oldPaxarrTransport.PreflightTransportName != null && newPaxarrTransport.PreflightTransportName != null && (oldPaxarrTransport.PreflightTransportName != newPaxarrTransport.PreflightTransportName))))
                        {
                            //if (Leg.Airport1 != null && Leg.Airport != null)
                            //    HistoryDescription.AppendLine(string.Format("Passenger Arrival Transportation has been changed for leg {0} from {1}", Leg.LegNUM, Leg.Airport1.IcaoID + "-" + Leg.Airport.IcaoID));
                            //else
                            HistoryDescription.AppendLine(string.Format("Passenger Arrival Transportation has been changed for leg {0} from {1} to {2}", Leg.LegNUM, oldPaxarrTransport.PreflightTransportName, newPaxarrTransport.PreflightTransportName));
                        }

                        #endregion
                    }
                    else
                    {

                        #region departure Crew

                        PreflightTransportList newdepTransport = new PreflightTransportList();
                        newdepTransport = Leg.PreflightTransportLists.Where(x => x.IsDepartureTransport == true && x.CrewPassengerType == "C" && x.IsDeleted == false).FirstOrDefault();

                        if (newdepTransport != null)
                        {

                            HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation {0} added to leg{1}", newdepTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newdepTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation Comments {0} added to leg {1}", newdepTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newdepTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation Confirm {0} added to leg {1}", newdepTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newdepTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Crew Departure Transportation in Leg {0} Set {1}", Leg.LegNUM, newdepTransport.Status));
                            }

                        }

                        #endregion

                        #region departure Pax

                        PreflightTransportList newPaxdepTransport = new PreflightTransportList();
                        newPaxdepTransport = Leg.PreflightTransportLists.Where(x => x.IsDepartureTransport == true && x.CrewPassengerType == "P" && x.IsDeleted == false).FirstOrDefault();

                        if (newPaxdepTransport != null)
                        {
                            HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation {0} added to leg {1}", newPaxdepTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newPaxdepTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation Comments {0} added to leg {1}", newPaxdepTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxdepTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation Confirm {0} added to leg {1}", newPaxdepTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxdepTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Passenger Departure Transportation in Leg {0} Set {1}", Leg.LegNUM, newPaxdepTransport.Status));
                            }


                        }

                        #endregion

                        #region arrival Crew

                        PreflightTransportList newarrTransport = new PreflightTransportList();
                        newarrTransport = Leg.PreflightTransportLists.Where(x => x.IsArrivalTransport == true && x.CrewPassengerType == "C" && x.IsDeleted == false).FirstOrDefault();

                        if (newarrTransport != null)
                        {
                            HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation {0} added to leg {1}", newarrTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newarrTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation Comments {0} added to leg {1}", newarrTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newarrTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation Confirm {0} added to leg {1}", newarrTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newarrTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Crew Arrival Transportation in Leg {0} Set {1}", Leg.LegNUM, newarrTransport.Status));
                            }


                        }

                        #endregion

                        #region arrival Pax

                        PreflightTransportList newPaxarrTransport = new PreflightTransportList();
                        newPaxarrTransport = Leg.PreflightTransportLists.Where(x => x.IsArrivalTransport == true && x.CrewPassengerType == "P" && x.IsDeleted == false).FirstOrDefault();

                        Transport newPaxarrtransport = new Transport();

                        if (newPaxarrTransport != null)
                        {
                            HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation {0} added to leg {1}", newPaxarrTransport.PreflightTransportName, Leg.LegNUM));
                            if (!string.IsNullOrEmpty(newPaxarrTransport.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation Comments {0} added to leg {1}", newPaxarrTransport.Comments, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxarrTransport.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation Confirm {0} added to leg {1}", newPaxarrTransport.ConfirmationStatus, Leg.LegNUM));
                            }
                            if (!string.IsNullOrEmpty(newPaxarrTransport.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Passenger Arrival Transportation in Leg {0} Set {1}", Leg.LegNUM, newPaxarrTransport.Status));
                            }


                        }

                        #endregion
                    }
                }
                #endregion

                foreach (PreflightTransportList transportList in Leg.PreflightTransportLists.ToList())
                {
                    transportList.CustomerID = Leg.CustomerID.Value;
                    transportList.LastUpdTS = DateTime.UtcNow;
                    transportList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (transportList.State != TripEntityState.Added)
                    {
                        var OriginalTransportlist = Container.GetPreflightTransportListByPreflightTransportID(CustomerID, transportList.PreflightTransportID).SingleOrDefault();

                        //var OriginalTransportlist = Container.PreflightTransportLists
                        //.Where(c => c.PreflightTransportID == transportList.PreflightTransportID)
                        //.SingleOrDefault();

                        if (transportList.State == TripEntityState.Modified)
                        {
                            transportList.IsDeleted = false;
                            transportList.LegID = Leg.LegID;
                            if (OriginalTransportlist != null)
                            {
                                Container.PreflightTransportLists.Attach(OriginalTransportlist);
                                Container.PreflightTransportLists.ApplyCurrentValues(transportList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Modified Transport Crew"
                                    var originalTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).OriginalValues;
                                    var CurrentTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightTransportList).GetProperties();
                                    for (int i = 0; i < originalTransportValues.FieldCount; i++)
                                    {
                                        var fieldname = originalTransportValues.GetName(i);

                                        object value1 = originalTransportValues.GetValue(i);
                                        object value2 = CurrentTransportValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "History For Modified Passenger Hotel"
                                            if (fieldname.ToLower() == "preflighttransportname")
                                            {
                                                if (!string.IsNullOrEmpty(OriginalTransportlist.PreflightTransportName.ToString()) && !value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Crew Departure Transportation has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Crew Arrival Transportation has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                }
                                            }
                                            #endregion

                                            #region "confirmationstatus"
                                            if (fieldname.ToLower() == "confirmationstatus")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation Confirm {0} added to leg {1}", value1, Leg.LegNUM));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("New Crew Arrival Transportation Confirm {0} added to leg {1} ", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !value1.Equals(value2))
                                                        HistoryDescription.AppendLine(string.Format("Crew Departure Transportation Confirm has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Crew Arrival Transportation Confirm has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                }

                                            }
                                            #endregion

                                            #region "comments"
                                            if (fieldname.ToLower() == "comments")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("New Crew Departure Transportation Comments {0} added to leg {1} ", value1, Leg.LegNUM));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("New Crew Arrival Transporattion Comments {0} added to leg {1} ", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Crew Departure Transportation Comments has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Crew Arrival Transportation Comments has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                }
                                            }
                                            #endregion

                                            #region "Status"

                                            //if (fieldname.ToLower() == "iscompleted")
                                            //{
                                            //    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "True")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Crew Departure Transportation in Leg {0} Set Completed", originalLeg.LegNUM));
                                            //    }
                                            //    if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "True")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Crew Arrival Transportation in Leg {0} Set Completed", originalLeg.LegNUM));
                                            //    }
                                            //    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "False")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Crew Departure Transportation in Leg {0} Set Not Completed", originalLeg.LegNUM));
                                            //    }
                                            //    if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "False")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Crew Arrival Transportation in Leg {0} Set Not Completed", originalLeg.LegNUM));
                                            //    }

                                            //}

                                            if (fieldname.ToLower() == "status")
                                            {
                                                if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Status of Crew Departure Transportation in Leg {0} Set {1}", originalLeg.LegNUM, value2.ToString()));
                                                }
                                                if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Status of Crew Arrival Transportation in Leg {0} Set Set {1}", originalLeg.LegNUM, value2.ToString()));
                                                }
                                            }
                                            #endregion

                                            #region Name

                                            if (fieldname.ToLower() == "preflighttransportname")
                                            {

                                                if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {

                                                    HistoryDescription.AppendLine(string.Format("Preflight Transport Name of Crew Departure Transportation in Leg {0} Set {1}", originalLeg.LegNUM, value2.ToString()));

                                                }

                                                if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {

                                                    HistoryDescription.AppendLine(string.Format("Preflight Transport Name of Crew Arrival Transportation in Leg {0} Set Set {1}", originalLeg.LegNUM, value2.ToString()));

                                                }

                                            }

                                            #endregion


                                            #region Phone

                                            if (fieldname.ToLower() == "phonenum1")
                                            {

                                                if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {

                                                    HistoryDescription.AppendLine(string.Format("Preflight Transport Phone number of Crew Departure Transportation in Leg {0} Set {1}", originalLeg.LegNUM, value2.ToString()));

                                                }

                                                if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {

                                                    HistoryDescription.AppendLine(string.Format("Preflight Transport Phone number of Crew Arrival Transportation in Leg {0} Set Set {1}", originalLeg.LegNUM, value2.ToString()));

                                                }

                                            }

                                            #endregion


                                            #region FaxNum

                                            if (fieldname.ToLower() == "faxnum")
                                            {

                                                if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {

                                                    HistoryDescription.AppendLine(string.Format("Preflight Transport Fax number of Crew Departure Transportation in Leg {0} Set {1}", originalLeg.LegNUM, value2.ToString()));

                                                }

                                                if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C" && !string.IsNullOrEmpty(value2.ToString()))
                                                {

                                                    HistoryDescription.AppendLine(string.Format("Preflight Transport Fax number of Crew Arrival Transportation in Leg {0} Set Set {1}", originalLeg.LegNUM, value2.ToString()));

                                                }

                                            }

                                            #endregion
                                        }
                                    }

                                    #endregion

                                    #region "History For Modified Transport Pax"
                                    var originalPaxTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).OriginalValues;
                                    var CurrentPaxTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).CurrentValues;
                                    PropertyInfo[] Paxproperties = typeof(PreflightTransportList).GetProperties();
                                    for (int i = 0; i < originalPaxTransportValues.FieldCount; i++)
                                    {
                                        var Paxfieldname = originalPaxTransportValues.GetName(i);

                                        object value1 = originalPaxTransportValues.GetValue(i);
                                        object value2 = CurrentPaxTransportValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "confirmationstatus"
                                            if (Paxfieldname.ToLower() == "confirmationstatus")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P")
                                                        HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation Confirm {0} added to leg {1}", value1, Leg.LegNUM));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation Confirm {0} added to leg {1} ", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P")
                                                        HistoryDescription.AppendLine(string.Format("Passenger Departure Transportation Confirm has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Passneger Arrival Transportation Confirm has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                }

                                            }
                                            #endregion

                                            #region "comments"
                                            if (Paxfieldname.ToLower() == "comments")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) || !string.IsNullOrWhiteSpace(value2.ToString()) && value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P")
                                                        HistoryDescription.AppendLine(string.Format("New Passenger Departure Transportation Comments {0} added to leg {1}", value1, Leg.LegNUM));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("New Passenger Arrival Transportation Comments {0} added to leg {1}", value1, Leg.LegNUM));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()) && !value1.Equals(value2))
                                                {
                                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P")
                                                        HistoryDescription.AppendLine(string.Format("Passenger Departure Transportation Comments has been changed for leg {0} from {1} to {2}", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                                        HistoryDescription.AppendLine(string.Format("Passenger Arrival Transportation Comments has been changed for leg {0} from {1} to {2} ", Leg.LegNUM, value1, value2));
                                                }
                                            }
                                            #endregion

                                            #region "Status"

                                            //if (Paxfieldname.ToLower() == "iscompleted")
                                            //{
                                            //    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "True")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Passenger Departure Transportation in Leg {0} Set Completed", originalLeg.LegNUM));
                                            //    }
                                            //    if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "P" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "True")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Passenger Arrival Transportation in Leg {0} Set Completed", originalLeg.LegNUM));
                                            //    }
                                            //    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "False")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Passenger Departure Transportation in Leg {0} Set Not Completed", originalLeg.LegNUM));
                                            //    }
                                            //    if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "P" && !string.IsNullOrEmpty(value2.ToString()) && value2.ToString() == "False")
                                            //    {
                                            //        HistoryDescription.AppendLine(string.Format("Status of Passenger Arrival Transportation in Leg {0} Set Not Completed", originalLeg.LegNUM));
                                            //    }

                                            //}


                                            if (Paxfieldname.ToLower() == "status")
                                            {
                                                if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P" && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Status of Passenger Departure Transportation in Leg {0} Set {1}", originalLeg.LegNUM, value2.ToString()));
                                                }
                                                if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "P" && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Status of Passenger Arrival Transportation in Leg {0} Set {1}", originalLeg.LegNUM, value2.ToString()));
                                                }

                                            }
                                            #endregion

                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                        else if (transportList.State == TripEntityState.Deleted)
                        {
                            if (OriginalTransportlist != null)
                            {
                                OriginalTransportlist.LastUpdUID = UserPrincipal.Identity.Name;
                                OriginalTransportlist.LastUpdTS = DateTime.UtcNow;
                                Container.PreflightTransportLists.DeleteObject(OriginalTransportlist);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Deleted Pax Transport"
                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "P")
                                    {
                                        HistoryDescription.AppendLine(string.Format("Passenger Departure Transport {0} for Leg {1} Deleted!", OriginalTransportlist.PreflightTransportName, Leg.LegNUM));
                                    }
                                    if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "P")
                                    {
                                        HistoryDescription.AppendLine(string.Format("Passenger Arrival Transport {0} for Leg {1} Deleted!", OriginalTransportlist.PreflightTransportName, Leg.LegNUM));
                                    }
                                    #endregion

                                    #region "History For Deleted Crew Transport"
                                    if ((bool)OriginalTransportlist.IsDepartureTransport && OriginalTransportlist.CrewPassengerType == "C")
                                    {
                                        HistoryDescription.AppendLine(string.Format("Crew Departure Transport for {0} for Leg {1} Deleted!", OriginalTransportlist.PreflightTransportName, Leg.LegNUM));
                                    }
                                    if ((bool)OriginalTransportlist.IsArrivalTransport && OriginalTransportlist.CrewPassengerType == "C")
                                    {
                                        HistoryDescription.AppendLine(string.Format("Crew Arrival Transport for {0} for Leg {1} Deleted!", OriginalTransportlist.PreflightTransportName, Leg.LegNUM));
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        transportList.IsDeleted = false;
                        transportList.LegID = Leg.LegID;
                        if (Leg.State != TripEntityState.Added)
                        {
                            if (originalLeg != null)
                                originalLeg.PreflightTransportLists.Add(transportList);
                        }
                        Container.ObjectStateManager.ChangeObjectState(transportList, System.Data.EntityState.Added);
                    }
                }
                #endregion

                #region "FBO"
                if (Leg.State != TripEntityState.Deleted)
                {
                    #region "History Fbo"
                    if (originalLeg != null)
                    {
                        #region departure
                        PreflightFBOList olddepFbo = new PreflightFBOList();
                        PreflightFBOList newdepFbo = new PreflightFBOList();

                        olddepFbo = originalLeg.PreflightFBOLists.Where(x => x.IsDepartureFBO == true && x.IsDeleted == false).FirstOrDefault();
                        newdepFbo = Leg.PreflightFBOLists.Where(x => x.IsDepartureFBO == true && x.IsDeleted == false).FirstOrDefault();

                        if (olddepFbo == null && newdepFbo != null)
                        {
                            if (newdepFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", newdepFbo.PreflightFBOName, Leg.LegNUM)); // Fix for #2243
                            }

                            if (!string.IsNullOrEmpty(newdepFbo.Status) && !string.IsNullOrWhiteSpace(newdepFbo.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Departure FBO for Leg {0} set ''{1}''", Leg.LegNUM, newdepFbo.Status));
                            }

                            if (!string.IsNullOrEmpty(newdepFbo.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newdepFbo.ConfirmationStatus.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newdepFbo.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newdepFbo.Comments.ToString()));
                            }

                        }

                        //if (olddepFbo != null && newdepFbo == null)
                        //{
                        //    if (olddepFbo.FBOID != null)
                        //    {
                        //        fbo = Container.FBOes.Where(x => x.FBOID == (Int64)olddepFbo.FBOID).SingleOrDefault();
                        //        if (fbo != null)
                        //        {
                        //            HistoryDescription.AppendLine(string.Format("Departure FBO {0} is deleted in Leg {1} ", fbo.FBOCD, Leg.LegNUM));
                        //        }
                        //    }
                        //}

                        if (olddepFbo != null && newdepFbo != null && ((olddepFbo.PreflightFBOName != null && newdepFbo.PreflightFBOName != null && (olddepFbo.PreflightFBOName != newdepFbo.PreflightFBOName))))
                        {
                            if (newdepFbo.PreflightFBOName != null && olddepFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} is changed to {1} for Leg {2} ", olddepFbo.PreflightFBOName, newdepFbo.PreflightFBOName, Leg.LegNUM)); // #2243
                            }
                        }

                        if (olddepFbo != null && newdepFbo != null && ((olddepFbo.FBOID != null && newdepFbo.FBOID == null && (olddepFbo.FBOID != newdepFbo.FBOID))))
                        {
                            if (olddepFbo.PreflightFBOName != null)
                            {

                                HistoryDescription.AppendLine(string.Format("Departure FBO has been changed for Leg {1} from {0} to .", olddepFbo.PreflightFBOName, Leg.LegNUM)); // #2243

                            }
                        }

                        if (olddepFbo != null && newdepFbo != null && ((olddepFbo.FBOID == null && newdepFbo.FBOID != null && (olddepFbo.FBOID != newdepFbo.FBOID))))
                        {
                            if (newdepFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", newdepFbo.PreflightFBOName, Leg.LegNUM)); // #2243
                            }
                        }

                        #endregion

                        #region arrival
                        PreflightFBOList oldarrFbo = new PreflightFBOList();
                        PreflightFBOList newarrFbo = new PreflightFBOList();
                        oldarrFbo = originalLeg.PreflightFBOLists.Where(x => x.IsArrivalFBO == true && x.IsDeleted == false).FirstOrDefault();
                        newarrFbo = Leg.PreflightFBOLists.Where(x => x.IsArrivalFBO == true && x.IsDeleted == false).FirstOrDefault();

                        if (oldarrFbo == null && newarrFbo != null)
                        {
                            if (newarrFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", newarrFbo.PreflightFBOName, Leg.LegNUM));
                            }

                            if (!string.IsNullOrEmpty(newarrFbo.Status) && !string.IsNullOrWhiteSpace(newarrFbo.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Arrival FBO for Leg {0} set ''{1}''", Leg.LegNUM, newarrFbo.Status));
                            }

                            if (!string.IsNullOrEmpty(newarrFbo.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newarrFbo.ConfirmationStatus.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newarrFbo.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newarrFbo.Comments.ToString()));
                            }
                        }

                        if (oldarrFbo != null && newarrFbo != null && ((oldarrFbo.PreflightFBOName != null && newarrFbo.PreflightFBOName != null && (oldarrFbo.PreflightFBOName != newarrFbo.PreflightFBOName))))
                        {
                            if (newarrFbo.PreflightFBOName != null && oldarrFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is changed to {1} for Leg {2} ", oldarrFbo.PreflightFBOName, newarrFbo.PreflightFBOName, Leg.LegNUM));

                            }
                        }

                        if (oldarrFbo != null && newarrFbo != null && ((oldarrFbo.FBOID != null && newarrFbo.FBOID == null && (oldarrFbo.FBOID != newarrFbo.FBOID))))
                        {
                            if (oldarrFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO has been changed for Leg {1} from {0} to .", oldarrFbo.PreflightFBOName, Leg.LegNUM));
                            }
                        }

                        if (oldarrFbo != null && newarrFbo != null && ((oldarrFbo.FBOID == null && newarrFbo.FBOID != null && (oldarrFbo.FBOID != newarrFbo.FBOID))))
                        {
                            if (newarrFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", newarrFbo.PreflightFBOName, Leg.LegNUM));
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        #region departure

                        PreflightFBOList newdepFbo = new PreflightFBOList();
                        newdepFbo = Leg.PreflightFBOLists.Where(x => x.IsDepartureFBO == true && x.IsDeleted == false).FirstOrDefault();


                        if (newdepFbo != null)
                        {
                            if (newdepFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", newdepFbo.PreflightFBOName, Leg.LegNUM)); // #2243
                            }

                            if (!string.IsNullOrEmpty(newdepFbo.Status) && !string.IsNullOrWhiteSpace(newdepFbo.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Departure FBO for Leg {0} set ''{1}''", Leg.LegNUM, newdepFbo.Status));
                            }

                            if (!string.IsNullOrEmpty(newdepFbo.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newdepFbo.ConfirmationStatus.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newdepFbo.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure FBO {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newdepFbo.Comments.ToString()));
                            }
                        }

                        #endregion

                        #region arrival

                        PreflightFBOList newarrFbo = new PreflightFBOList();
                        newdepFbo = Leg.PreflightFBOLists.Where(x => x.IsArrivalFBO == true && x.IsDeleted == false).FirstOrDefault();
                        if (newarrFbo != null)
                        {
                            if (newarrFbo.PreflightFBOName != null)
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", newarrFbo.PreflightFBOName, Leg.LegNUM));
                            }

                            if (!string.IsNullOrEmpty(newarrFbo.Status) && !string.IsNullOrWhiteSpace(newarrFbo.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Arrival FBO for Leg {0} set ''{1}''", Leg.LegNUM, newarrFbo.Status));
                            }

                            if (!string.IsNullOrEmpty(newarrFbo.ConfirmationStatus))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newarrFbo.ConfirmationStatus.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newarrFbo.Comments))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newarrFbo.Comments.ToString()));
                            }
                        }

                        #endregion
                    }

                    #endregion
                }
                foreach (PreflightFBOList fboList in Leg.PreflightFBOLists.ToList())
                {
                    fboList.LastUpdTS = DateTime.UtcNow;
                    fboList.LastUpdUID = UserPrincipal.Identity.Name;
                    fboList.CustomerID = Leg.CustomerID;
                    if (fboList.State != TripEntityState.Added)
                    {
                        var OriginalFBOlist = Container.GetPreflightFBOListByPreflightFBOID(CustomerID, fboList.PreflightFBOID).SingleOrDefault();

                        //var OriginalFBOlist = Container.PreflightFBOLists
                        //.Where(c => c.PreflightFBOID == fboList.PreflightFBOID)
                        //.SingleOrDefault();

                        if (fboList.State == TripEntityState.Modified)
                        {
                            fboList.IsDeleted = false;
                            fboList.LegID = Leg.LegID;
                            if (OriginalFBOlist != null)
                            {
                                Container.PreflightFBOLists.Attach(OriginalFBOlist);
                                Container.PreflightFBOLists.ApplyCurrentValues(fboList);
                                Container.ObjectStateManager.ChangeObjectState(OriginalFBOlist, System.Data.EntityState.Modified);
                                if (Leg.State != TripEntityState.Deleted)
                                {

                                    #region "History For Modified FBO"
                                    var originalFBOValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalFBOlist).OriginalValues;
                                    var CurrentFboValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalFBOlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightFBOList).GetProperties();
                                    for (int i = 0; i < originalFBOValues.FieldCount; i++)
                                    {
                                        var fieldname = originalFBOValues.GetName(i);

                                        object value1 = originalFBOValues.GetValue(i);
                                        object value2 = CurrentFboValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "confirmationstatus"
                                            if (fieldname.ToLower() == "confirmationstatus")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, value2.ToString()));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, value2.ToString()));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Confirmation", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Confirmation", Leg.LegNUM, value1, value2));
                                                }
                                                else if ((string.IsNullOrEmpty(value2.ToString()) || string.IsNullOrWhiteSpace(value2.ToString())) && !string.IsNullOrEmpty(value1.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} deleted for Leg {1}", "Confirmation", Leg.LegNUM));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} deleted for Leg {1}", "Confirmation", Leg.LegNUM));
                                                }

                                            }
                                            #endregion

                                            #region "comments"
                                            if (fieldname.ToLower() == "comments")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, value2.ToString()));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, value2.ToString()));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Comments", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Comments", Leg.LegNUM, value1, value2));
                                                }
                                                else if ((string.IsNullOrEmpty(value2.ToString()) || string.IsNullOrWhiteSpace(value2.ToString())) && !string.IsNullOrEmpty(value1.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} deleted for Leg {1}", "Comments", Leg.LegNUM));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} deleted for Leg {1}", "Comments", Leg.LegNUM));
                                                }

                                            }
                                            #endregion

                                            #region "status"
                                            if (fieldname.ToLower() == "status")
                                            {

                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Status of Departure FBO for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Status of Arrival FBO for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }

                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                                        HistoryDescription.AppendLine(string.Format("Status of Departure FBO changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                                        HistoryDescription.AppendLine(string.Format("Status of Arrival FBO changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }
                                            //if (fieldname.ToLower() == "iscompleted")
                                            //{
                                            //    if ((bool)OriginalFBOlist.IsDepartureFBO)
                                            //    {
                                            //        bool test = false;
                                            //        if (string.IsNullOrEmpty(value1.ToString()))
                                            //        {
                                            //            value1 = test;
                                            //        }

                                            //        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                            //        {
                                            //            HistoryDescription.AppendLine(string.Format("Status of Departure FBO for Leg {0} Changed From {1} To {2}", originalLeg.LegNUM,
                                            //            ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Completed" : "Not Completed"),
                                            //            ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Completed" : "Not Completed")));
                                            //        }
                                            //    }
                                            //    else if ((bool)OriginalFBOlist.IsArrivalFBO)
                                            //    {
                                            //        bool test = false;
                                            //        if (string.IsNullOrEmpty(value1.ToString()))
                                            //        {
                                            //            value1 = test;
                                            //        }

                                            //        if (value1 != value2 && (string.IsNullOrEmpty(value1.ToString()) || !value1.Equals(value2)))
                                            //        {
                                            //            HistoryDescription.AppendLine(string.Format("Status of Arrival FBO for Leg {0} Changed From {1} To {2}", originalLeg.LegNUM,
                                            //            ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Completed" : "Not Completed"),
                                            //            ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Completed" : "Not Completed")));
                                            //        }
                                            //    }

                                            //}
                                            #endregion

                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                        else if (fboList.State == TripEntityState.Deleted)
                        {
                            if (OriginalFBOlist != null)
                            {
                                OriginalFBOlist.LastUpdTS = DateTime.UtcNow;
                                OriginalFBOlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightFBOLists.DeleteObject(OriginalFBOlist);
                            }

                        }
                        else
                        {
                            if (OriginalFBOlist != null)
                            {
                                Container.ObjectStateManager.ChangeObjectState(OriginalFBOlist, System.Data.EntityState.Unchanged);
                            }
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        fboList.IsDeleted = false;
                        fboList.LegID = Leg.LegID;
                        if (Leg.State != TripEntityState.Added)
                            originalLeg.PreflightFBOLists.Add(fboList);
                        Container.ObjectStateManager.ChangeObjectState(fboList, System.Data.EntityState.Added);

                    }
                }

                #endregion

                #region "Catering"
                if (Leg.State != TripEntityState.Deleted)
                {
                    #region "History Catering"
                    if (originalLeg != null)
                    {
                        #region departure
                        PreflightCateringDetail olddepcatering = new PreflightCateringDetail();
                        PreflightCateringDetail newdepcatering = new PreflightCateringDetail();
                        olddepcatering = originalLeg.PreflightCateringDetails.Where(x => x.ArriveDepart == "D" && x.IsDeleted == false).FirstOrDefault();
                        newdepcatering = Leg.PreflightCateringDetails.Where(x => x.ArriveDepart == "D" && x.IsDeleted == false).FirstOrDefault();

                        if (olddepcatering == null && newdepcatering != null)
                        {

                            HistoryDescription.AppendLine(string.Format("Departure Catering {0} is added to Leg {1} ", newdepcatering.ContactName, Leg.LegNUM));

                            if (!string.IsNullOrEmpty(newdepcatering.Status) && !string.IsNullOrWhiteSpace(newdepcatering.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Departure Catering for Leg {0} set ''{1}''", Leg.LegNUM, newdepcatering.Status));
                            }

                            if (!string.IsNullOrEmpty(newdepcatering.CateringConfirmation))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure Catering {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newdepcatering.CateringConfirmation.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newdepcatering.CateringComments))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure Catering {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newdepcatering.CateringComments.ToString()));
                            }
                        }

                        //if (olddepcatering != null && newdepcatering == null)
                        //{
                        //    if (olddepcatering.CateringID != null)
                        //    {
                        //        catering = Container.Caterings.Where(x => x.CateringID == (Int64)olddepcatering.CateringID).SingleOrDefault();
                        //        if (catering != null)
                        //        {
                        //            HistoryDescription.AppendLine(string.Format("Departure Catering {0} is deleted in Leg {1} ", catering.CateringCD, Leg.LegNUM));
                        //        }
                        //    }
                        //}

                        if (olddepcatering != null && newdepcatering != null && ((olddepcatering.CateringID != null && newdepcatering.CateringID != null && (olddepcatering.CateringID != newdepcatering.CateringID))))
                        {
                            HistoryDescription.AppendLine(string.Format("Departure Catering for Leg {2} is changed from {0} to {1}  ", olddepcatering.ContactName, newdepcatering.ContactName, Leg.LegNUM));
                        }

                        if (olddepcatering != null && newdepcatering == null)
                        {
                            HistoryDescription.AppendLine(string.Format("Departure Catering has been changed for Leg {1} from {0} to .", olddepcatering.ContactName, Leg.LegNUM));
                        }

                        if (olddepcatering != null && newdepcatering != null && ((olddepcatering.CateringID == null && newdepcatering.CateringID != null && (olddepcatering.CateringID != newdepcatering.CateringID))))
                        {
                            HistoryDescription.AppendLine(string.Format("Departure Catering {0} is added to Leg {1} ", newdepcatering.ContactName, Leg.LegNUM));
                        }
                        #endregion

                        #region arrival
                        PreflightCateringDetail oldarrCatering = new PreflightCateringDetail();
                        PreflightCateringDetail newarrCaterng = new PreflightCateringDetail();
                        oldarrCatering = originalLeg.PreflightCateringDetails.Where(x => x.ArriveDepart == "A" && x.IsDeleted == false).FirstOrDefault();
                        newarrCaterng = Leg.PreflightCateringDetails.Where(x => x.ArriveDepart == "A" && x.IsDeleted == false).FirstOrDefault();

                        if (oldarrCatering == null && newarrCaterng != null)
                        {

                            HistoryDescription.AppendLine(string.Format("Arrival Catering {0} is added to Leg {1} ", newarrCaterng.ContactName, Leg.LegNUM));

                            if (!string.IsNullOrEmpty(newarrCaterng.Status) && !string.IsNullOrWhiteSpace(newarrCaterng.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Arrival Catering for Leg {0} set ''{1}''", Leg.LegNUM, newarrCaterng.Status));
                            }

                            if (!string.IsNullOrEmpty(newarrCaterng.CateringConfirmation))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival Catering {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newarrCaterng.CateringConfirmation.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newarrCaterng.CateringComments))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival Catering {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newarrCaterng.CateringComments.ToString()));
                            }
                        }



                        if (oldarrCatering != null && newarrCaterng != null && ((oldarrCatering.ContactName != null && newarrCaterng.ContactName != null && (oldarrCatering.ContactName != newarrCaterng.ContactName))))
                        {
                            HistoryDescription.AppendLine(string.Format("Arrival Catering {0} is changed to {1} for Leg {2} ", oldarrCatering.ContactName, newarrCaterng.ContactName, Leg.LegNUM));
                        }

                        if (oldarrCatering != null && newarrCaterng == null)
                        {

                            HistoryDescription.AppendLine(string.Format("Departure Catering has been changed for Leg {1} from {0} to .", oldarrCatering.ContactName, Leg.LegNUM));

                        }

                        if (oldarrCatering != null && newarrCaterng != null && ((oldarrCatering.ContactName == null && newarrCaterng.ContactName != null && (oldarrCatering.ContactName != newarrCaterng.ContactName))))
                        {
                            HistoryDescription.AppendLine(string.Format("Departure Catering {0} is added to Leg {1} ", newarrCaterng.ContactName, Leg.LegNUM));
                        }

                        #endregion
                    }
                    else
                    {
                        #region departure

                        PreflightCateringDetail newdepCatering = new PreflightCateringDetail();
                        newdepCatering = Leg.PreflightCateringDetails.Where(x => x.ArriveDepart == "D" && x.IsDeleted == false).FirstOrDefault();

                        if (newdepCatering != null)
                        {


                            HistoryDescription.AppendLine(string.Format("Departure Catering {0} is added to Leg {1} ", newdepCatering.ContactName, Leg.LegNUM));

                            if (!string.IsNullOrEmpty(newdepCatering.Status) && !string.IsNullOrWhiteSpace(newdepCatering.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Departure Catering for Leg {0} set ''{1}''", Leg.LegNUM, newdepCatering.Status));
                            }

                            if (!string.IsNullOrEmpty(newdepCatering.CateringConfirmation))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure Catering {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newdepCatering.CateringConfirmation.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newdepCatering.CateringComments))
                            {
                                HistoryDescription.AppendLine(string.Format("Departure Catering {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newdepCatering.CateringComments.ToString()));
                            }
                        }

                        #endregion

                        #region arrival

                        PreflightCateringDetail newarrCatering = new PreflightCateringDetail();
                        newarrCatering = Leg.PreflightCateringDetails.Where(x => x.ArriveDepart == "A" && x.IsDeleted == false).FirstOrDefault();


                        if (newarrCatering != null)
                        {
                            HistoryDescription.AppendLine(string.Format("Arrival Catering {0} is added to Leg {1} ", newarrCatering.ContactName, Leg.LegNUM));

                            if (!string.IsNullOrEmpty(newarrCatering.Status) && !string.IsNullOrWhiteSpace(newarrCatering.Status))
                            {
                                HistoryDescription.AppendLine(string.Format("Status of Arrival Catering for Leg {0} set ''{1}''", Leg.LegNUM, newarrCatering.Status));
                            }
                            if (!string.IsNullOrEmpty(newarrCatering.CateringConfirmation))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival Catering {0} ''{2}'' added to Leg {1}", "Confirmation", Leg.LegNUM, newarrCatering.CateringConfirmation.ToString()));
                            }
                            if (!string.IsNullOrEmpty(newarrCatering.CateringComments))
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival Catering {0} ''{2}'' added to Leg {1}", "Comments", Leg.LegNUM, newarrCatering.CateringComments.ToString()));
                            }
                        }

                        #endregion
                    }

                    #endregion
                }
                foreach (PreflightCateringDetail caterList in Leg.PreflightCateringDetails.ToList())
                {
                    caterList.CustomerID = Leg.CustomerID;
                    caterList.LastUpdTS = DateTime.UtcNow;
                    caterList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (caterList.State != TripEntityState.Added)
                    {
                        var OriginalCateringlist = Container.GetPreflightCateringDetailByPreflightCateringID(CustomerID, caterList.PreflightCateringID).SingleOrDefault();
                        if (caterList.State == TripEntityState.Modified)
                        {
                            caterList.IsDeleted = false;
                            caterList.LegID = Leg.LegID;
                            if (OriginalCateringlist != null)
                            {
                                Container.PreflightCateringDetails.Attach(OriginalCateringlist);
                                Container.PreflightCateringDetails.ApplyCurrentValues(caterList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History For Modified Catering"
                                    var originalCateringValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCateringlist).OriginalValues;
                                    var CurrentCateringValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCateringlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightCateringDetail).GetProperties();
                                    for (int i = 0; i < originalCateringValues.FieldCount; i++)
                                    {
                                        var fieldname = originalCateringValues.GetName(i);

                                        object value1 = originalCateringValues.GetValue(i);
                                        object value2 = CurrentCateringValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "confirmationstatus"
                                            if (fieldname.ToLower() == "cateringconfirmation")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Departure Catering {0} ''{2}'' added for Leg {1}", "Confirmation", Leg.LegNUM, value2.ToString()));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Arrival Catering {0} ''{2}'' added for Leg {1}", "Confirmation", Leg.LegNUM, value2.ToString()));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Departure Catering {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Confirmation", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Arrival Catering {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Confirmation", Leg.LegNUM, value1, value2));
                                                }
                                                else if ((string.IsNullOrEmpty(value2.ToString()) || string.IsNullOrWhiteSpace(value2.ToString())) && !string.IsNullOrEmpty(value1.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Departure Catering {0} deleted for Leg {1}", "Confirmation", Leg.LegNUM));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Arrival Catering {0} deleted for Leg {1}", "Confirmation", Leg.LegNUM));
                                                }

                                            }
                                            #endregion

                                            #region "confirmationstatus"
                                            if (fieldname.ToLower() == "cateringcomments")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Departure Catering {0} ''{2}'' added for Leg {1}", "Comments", Leg.LegNUM, value2.ToString()));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Arrival Catering {0} ''{2}'' added for Leg {1}", "Comments", Leg.LegNUM, value2.ToString()));
                                                }
                                                else if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Departure Catering {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Comments", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Arrival Catering {0} changed for Leg {1} from ''{2}'' to ''{3}''", "Comments", Leg.LegNUM, value1, value2));
                                                }
                                                else if ((string.IsNullOrEmpty(value2.ToString()) || string.IsNullOrWhiteSpace(value2.ToString())) && !string.IsNullOrEmpty(value1.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Departure Catering {0} deleted for Leg {1}", "Comments", Leg.LegNUM));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Arrival Catering {0} deleted for Leg {1}", "Comments", Leg.LegNUM));
                                                }

                                            }
                                            #endregion

                                            #region "status"

                                            if (fieldname.ToLower() == "status")
                                            {

                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Status of Departure Catering for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Status of Arrival Catering for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }

                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Status of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Status of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                            #region "ContactName"

                                            if (fieldname.ToLower() == "contactname")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Contact Name of Departure Catering for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Contact Name of Arrival Catering for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }


                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Contact Name of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Contact Name of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                            #region "CateringContactName"


                                            if (fieldname.ToLower() == "cateringcontactname")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Catering Contact Name of Departure Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Catering Contact Name of Arrival Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }

                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Catering Contact Name of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Catering Contact Name of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                            #region "Contact Phone"

                                            if (fieldname.ToLower() == "contactphone")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Contact Phone of Departure Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Contact Phone of Arrival Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }

                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Contact Phone of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Contact Phone of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                            #region "Contact Fax"

                                            if (fieldname.ToLower() == "contactfax")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Contact Fax of Departure Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Contact Fax of Arrival Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }

                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Contact Fax of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Contact Fax of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                            #region "Cost"

                                            if (fieldname.ToLower() == "cost")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Cost of Departure Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Cost of Arrival Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Cost of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Cost of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                            #region "CateringID"

                                            if (fieldname.ToLower() == "cateringid")
                                            {
                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrWhiteSpace(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Catering of Departure Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Catering of Arrival Catering add for Leg {0} set ''{1}''", Leg.LegNUM, value2));
                                                }

                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    if (OriginalCateringlist.ArriveDepart == "D")
                                                        HistoryDescription.AppendLine(string.Format("Catering of Departure Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                    else if (OriginalCateringlist.ArriveDepart == "A")
                                                        HistoryDescription.AppendLine(string.Format("Catering of Arrival Catering changed for Leg {0} from ''{1}'' to ''{2}''", Leg.LegNUM, value1, value2));
                                                }
                                            }

                                            #endregion

                                        }

                                    }


                                    #endregion

                                }
                            }
                        }
                        else if (caterList.State == TripEntityState.Deleted)
                        {
                            if (OriginalCateringlist != null)
                            {
                                OriginalCateringlist.LastUpdTS = DateTime.UtcNow;
                                OriginalCateringlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightCateringDetails.DeleteObject(OriginalCateringlist);
                            }
                        }
                        else
                        {
                            if (OriginalCateringlist != null)
                                Container.ObjectStateManager.ChangeObjectState(OriginalCateringlist, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        caterList.IsDeleted = false;
                        caterList.LegID = Leg.LegID;
                        if (Leg.State != TripEntityState.Added)
                            if (originalLeg != null)
                                originalLeg.PreflightCateringDetails.Add(caterList);
                        Container.ObjectStateManager.ChangeObjectState(caterList, System.Data.EntityState.Added);


                    }
                }

                #endregion

                #region "Outbound Instructions"

                foreach (PreflightTripOutbound outboundInstructions in Leg.PreflightTripOutbounds.ToList())
                {
                    outboundInstructions.LastUpdTS = DateTime.UtcNow;
                    outboundInstructions.LastUpdUID = UserPrincipal.Identity.Name;
                    outboundInstructions.CustomerID = Leg.CustomerID;
                    if (outboundInstructions.State != TripEntityState.Added)
                    {
                        var OriginalOutboundInstructions = Container.GetPreflightTripOutboundByPreflightTripOutboundID(CustomerID, outboundInstructions.PreflightTripOutboundID).SingleOrDefault();

                        if (outboundInstructions.State == TripEntityState.Modified)
                        {
                            outboundInstructions.IsDeleted = false;
                            outboundInstructions.LegID = Leg.LegID;
                            if (OriginalOutboundInstructions != null)
                            {
                                Container.PreflightTripOutbounds.Attach(OriginalOutboundInstructions);
                                Container.PreflightTripOutbounds.ApplyCurrentValues(outboundInstructions);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History for Modified Outbound"

                                    var originalOutboundInstructionValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalOutboundInstructions).OriginalValues;
                                    var CurrentOutboundInstructionValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalOutboundInstructions).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightTripOutbound).GetProperties();
                                    for (int i = 0; i < originalOutboundInstructionValues.FieldCount; i++)
                                    {
                                        var fieldname = originalOutboundInstructionValues.GetName(i);

                                        object value1 = originalOutboundInstructionValues.GetValue(i);
                                        object value2 = CurrentOutboundInstructionValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            if (fieldname.ToLower() == "outbounddescription")
                                            {
                                                switch (outboundInstructions.OutboundInstructionNUM)
                                                {
                                                    case 1:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, value1, value2));
                                                        break;
                                                    case 2:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab2, value1, value2));
                                                        break;
                                                    case 3:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab3, value1, value2));
                                                        break;
                                                    case 4:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab4, value1, value2));
                                                        break;
                                                    case 5:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab5, value1, value2));
                                                        break;
                                                    case 6:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab6, value1, value2));
                                                        break;
                                                    case 7:
                                                        if (!string.IsNullOrEmpty(OriginalOutboundInstructions.OutboundDescription))
                                                            HistoryDescription.AppendLine(string.Format("The Outbound Instructions for {1} on leg {0} changed from {2} to {3}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab7, value1, value2));
                                                        break;
                                                }
                                            }
                                        }
                                    }

                                    #endregion
                                }
                            }

                        }
                        else if (outboundInstructions.State == TripEntityState.Deleted)
                        {
                            if (OriginalOutboundInstructions != null)
                            {
                                OriginalOutboundInstructions.IsDeleted = true;
                                OriginalOutboundInstructions.LastUpdTS = DateTime.UtcNow;
                                OriginalOutboundInstructions.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightTripOutbounds.DeleteObject(OriginalOutboundInstructions);
                            }
                        }
                        else
                        {
                            if (OriginalOutboundInstructions != null)

                                Container.ObjectStateManager.ChangeObjectState(OriginalOutboundInstructions, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        outboundInstructions.IsDeleted = false;
                        if (Leg.State != TripEntityState.Added)
                            if (originalLeg != null)
                                originalLeg.PreflightTripOutbounds.Add(outboundInstructions);
                        Container.ObjectStateManager.ChangeObjectState(outboundInstructions, System.Data.EntityState.Added);
                        if (Leg.State != TripEntityState.Deleted)
                        {
                            #region "History"
                            switch (outboundInstructions.OutboundInstructionNUM)
                            {
                                case 1:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab1, outboundInstructions.OutboundDescription));
                                    break;
                                case 2:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab2, outboundInstructions.OutboundDescription));
                                    break;
                                case 3:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab3, outboundInstructions.OutboundDescription));
                                    break;
                                case 4:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab4, outboundInstructions.OutboundDescription));
                                    break;
                                case 5:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab5, outboundInstructions.OutboundDescription));
                                    break;
                                case 6:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab6, outboundInstructions.OutboundDescription));
                                    break;
                                case 7:
                                    if (!string.IsNullOrEmpty(outboundInstructions.OutboundDescription))
                                        HistoryDescription.AppendLine(string.Format("New Outbound Instructions added for {1}: {2} on leg {0}", Leg.LegNUM, UserPrincipal.Identity.Settings.RptTabOutbdInstLab7, outboundInstructions.OutboundDescription));
                                    break;
                            }
                            #endregion
                        }
                    }
                }

                #endregion

                #region "Checklist"
                if (Leg.State != TripEntityState.Deleted)
                {

                    #region "History Checklist modified"
                    if (originalLeg != null)
                    {
                        if (originalLeg.LegID == Leg.LegID)
                        {
                            if ((originalLeg.PreflightCheckLists == null || originalLeg.PreflightCheckLists.Count == 0)
                                && (Leg.PreflightCheckLists != null && Leg.PreflightCheckLists.Count > 0))
                            {
                                TripManagerCheckListGroup triplst = new TripManagerCheckListGroup();
                                Int64 newCheckGroupID = (long)Leg.PreflightCheckLists.First().CheckGroupID;
                                IQueryable<TripManagerCheckListGroup> TripManagerCheckListGroups = preflightCompiledQuery.getTripManagerCheckListGroup(Container, newCheckGroupID, CustomerID);
                                TripManagerCheckListGroup OldTripManagerCheckListGroup = new TripManagerCheckListGroup();
                                if (TripManagerCheckListGroups != null && TripManagerCheckListGroups.ToList().Count > 0)
                                    triplst = TripManagerCheckListGroups.ToList()[0];
                                HistoryDescription.AppendLine(string.Format("New Checklist Group {0} added for leg {1}", triplst.CheckGroupCD, Leg.LegNUM));
                            }
                            else if ((originalLeg.PreflightCheckLists != null || originalLeg.PreflightCheckLists.Count > 0)
                               && (Leg.PreflightCheckLists != null && Leg.PreflightCheckLists.Count > 0))
                            {
                                TripManagerCheckListGroup newtriplst = new TripManagerCheckListGroup();
                                if (Leg.PreflightCheckLists.First().CheckGroupID != null)
                                {
                                    Int64 newCheckGroupID = (long)Leg.PreflightCheckLists.First().CheckGroupID;

                                    IQueryable<TripManagerCheckListGroup> TripManagerCheckListGroups = preflightCompiledQuery.getTripManagerCheckListGroup(Container, newCheckGroupID, CustomerID);
                                    if (TripManagerCheckListGroups != null && TripManagerCheckListGroups.ToList().Count > 0)
                                        newtriplst = TripManagerCheckListGroups.ToList()[0];

                                    TripManagerCheckListGroup oldtriplst = new TripManagerCheckListGroup();
                                    Int64 oldCheckGroupID = (long)originalLeg.PreflightCheckLists.First().CheckGroupID;


                                    TripManagerCheckListGroups = preflightCompiledQuery.getTripManagerCheckListGroup(Container, oldCheckGroupID, CustomerID);
                                    if (TripManagerCheckListGroups != null && TripManagerCheckListGroups.ToList().Count > 0)
                                        oldtriplst = TripManagerCheckListGroups.ToList()[0];

                                    oldtriplst = Container.TripManagerCheckListGroups.Where(x => x.CheckGroupID == oldCheckGroupID).SingleOrDefault();
                                    if (newCheckGroupID != oldCheckGroupID)
                                        HistoryDescription.AppendLine(string.Format("Checklist Group for leg {0} changed from {1} to {2}", Leg.LegNUM, oldtriplst.CheckGroupDescription, newtriplst.CheckGroupDescription));
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Leg.PreflightCheckLists != null && Leg.PreflightCheckLists.Count > 0)
                        {
                            TripManagerCheckListGroup triplst = new TripManagerCheckListGroup();
                            Int64 newCheckGroupID = (long)Leg.PreflightCheckLists.First().CheckGroupID;

                            IQueryable<TripManagerCheckListGroup> TripManagerCheckListGroups = preflightCompiledQuery.getTripManagerCheckListGroup(Container, newCheckGroupID, CustomerID);
                            if (TripManagerCheckListGroups != null && TripManagerCheckListGroups.ToList().Count > 0)
                                triplst = TripManagerCheckListGroups.ToList()[0];

                            HistoryDescription.AppendLine(string.Format("New Checklist Group {0} added for leg {1}", triplst.CheckGroupCD, Leg.LegNUM));
                        }
                    }

                    #endregion
                }
                foreach (PreflightCheckList checkList in Leg.PreflightCheckLists.ToList())
                {
                    checkList.LastUptTS = DateTime.UtcNow;
                    checkList.LastUpdUID = UserPrincipal.Identity.Name;
                    checkList.CustomerID = Leg.CustomerID;
                    if (checkList.State != TripEntityState.Added)
                    {
                        var OriginalCheckList = Container.GetPreflightCheckListByPreflightCheckListID(CustomerID, checkList.PreflightCheckListID).SingleOrDefault();

                        if (checkList.State == TripEntityState.Modified)
                        {
                            checkList.IsDeleted = false;
                            checkList.LegID = Leg.LegID;
                            if (OriginalCheckList != null)
                            {
                                Container.PreflightCheckLists.Attach(OriginalCheckList);
                                Container.PreflightCheckLists.ApplyCurrentValues(checkList);
                                if (Leg.State != TripEntityState.Deleted)
                                {
                                    #region "History for Checklist in Modified"

                                    var originalCheckListValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCheckList).OriginalValues;
                                    var CurrentCheckListValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCheckList).CurrentValues;
                                    PropertyInfo[] properties = typeof(PreflightLeg).GetProperties();
                                    for (int i = 0; i < originalCheckListValues.FieldCount; i++)
                                    {
                                        var fieldname = originalCheckListValues.GetName(i);

                                        object value1 = originalCheckListValues.GetValue(i);
                                        object value2 = CurrentCheckListValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            TripManagerCheckList newtriplst = new TripManagerCheckList();

                                            IQueryable<TripManagerCheckList> TripManagerCheckLists = preflightCompiledQuery.getTripManagerCheckList(Container, (long)OriginalCheckList.CheckListID, CustomerID);
                                            if (TripManagerCheckLists != null && TripManagerCheckLists.ToList().Count > 0)
                                                newtriplst = TripManagerCheckLists.ToList()[0];

                                            if (fieldname.ToLower() == "iscompleted")
                                            {
                                                if (!(bool)OriginalCheckList.IsCompleted)
                                                    HistoryDescription.AppendLine(string.Format("Status of Checklist code {1} in Leg {0} Changed From Completed to Not Completed", originalLeg.LegNUM, newtriplst.CheckListCD));
                                                else if ((bool)OriginalCheckList.IsCompleted)
                                                    HistoryDescription.AppendLine(string.Format("Status of Checklist code {1} in Leg {0} set Completed", originalLeg.LegNUM, newtriplst.CheckListCD));
                                            }
                                        }
                                    }

                                    #endregion
                                }
                            }
                        }
                        else if (checkList.State == TripEntityState.Deleted)
                        {
                            if (OriginalCheckList != null)
                            {
                                OriginalCheckList.IsDeleted = true;
                                OriginalCheckList.LastUptTS = DateTime.UtcNow;
                                OriginalCheckList.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.PreflightCheckLists.DeleteObject(OriginalCheckList);
                            }
                        }
                        else
                        {
                            if (OriginalCheckList != null)
                                Container.ObjectStateManager.ChangeObjectState(OriginalCheckList, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        checkList.IsDeleted = false;
                        if (Leg.State != TripEntityState.Added)
                            if (originalLeg != null)
                                originalLeg.PreflightCheckLists.Add(checkList);
                        Container.ObjectStateManager.ChangeObjectState(checkList, System.Data.EntityState.Added);
                        if (Leg.State != TripEntityState.Deleted)
                        {
                            #region "History"
                            if ((bool)checkList.IsCompleted)
                            {
                                TripManagerCheckList newtriplst = new TripManagerCheckList();
                                IQueryable<TripManagerCheckList> TripManagerCheckLists = preflightCompiledQuery.getTripManagerCheckList(Container, (long)checkList.CheckListID, CustomerID);
                                if (TripManagerCheckLists != null && TripManagerCheckLists.ToList().Count > 0)
                                    newtriplst = TripManagerCheckLists.ToList()[0];
                                //newtriplst = Container.TripManagerCheckLists.Where(x => x.CheckListID == checkList.CheckListID).SingleOrDefault();

                                HistoryDescription.AppendLine(string.Format("Status of Checklist code {1} in Leg {0} set Completed", Leg.LegNUM, newtriplst.CheckListCD));
                            }

                            #endregion
                        }
                    }
                }

                #endregion

            }
                        #endregion

            #region TripException

            if (Trip.State == TripEntityState.Modified || Trip.State == TripEntityState.NoChange)
            {
                foreach (PreflightTripException PrefException in PreflightMainentity.PreflightTripExceptions.ToList())
                {
                    Container.ObjectStateManager.ChangeObjectState(PrefException, System.Data.EntityState.Deleted);
                }
            }

            foreach (PreflightTripException PrefException in Trip.PreflightTripExceptions.ToList())
            {

                if (Trip.State != TripEntityState.NoChange)
                {

                    if (Trip.State != TripEntityState.Added)
                    {
                        PreflightTripException Tripexp = new PreflightTripException();
                        Tripexp.TripID = PreflightMainentity.TripID;
                        Tripexp.CustomerID = PrefException.CustomerID;
                        Tripexp.ExceptionDescription = PrefException.ExceptionDescription;
                        Tripexp.LastUpdUID = UserPrincipal.Identity.Name;
                        Tripexp.LastUpdTS = DateTime.UtcNow;
                        Tripexp.ExceptionTemplateID = PrefException.ExceptionTemplateID;
                        Tripexp.DisplayOrder = PrefException.DisplayOrder;
                        PreflightMainentity.PreflightTripExceptions.Add(Tripexp);
                        // Container.ObjectStateManager.ChangeObjectState(Tripexp, System.Data.EntityState.Added);
                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(PrefException, System.Data.EntityState.Added);
                    }

                }
                else
                {
                    PreflightTripException Tripexp = new PreflightTripException();
                    Tripexp.TripID = PreflightMainentity.TripID;
                    Tripexp.CustomerID = PrefException.CustomerID;
                    Tripexp.ExceptionDescription = PrefException.ExceptionDescription;
                    Tripexp.LastUpdUID = UserPrincipal.Identity.Name;
                    Tripexp.LastUpdTS = DateTime.UtcNow;
                    Tripexp.ExceptionTemplateID = PrefException.ExceptionTemplateID;
                    Tripexp.DisplayOrder = PrefException.DisplayOrder;
                    PreflightMainentity.PreflightTripExceptions.Add(Tripexp);
                    Container.ObjectStateManager.ChangeObjectState(Tripexp, System.Data.EntityState.Added);
                }
                //Container.PreflightTripExceptions.AddObject(PrefException);
            }
            #endregion

            #region "TripHistory"
            if (HistoryDescription != null && HistoryDescription.Length > 0)
            {
                if (Trip.State != TripEntityState.NoChange)
                {
                    PreflightTripHistory TripHistory = new PreflightTripHistory();
                    if (Trip.State == TripEntityState.Added)
                    {
                        TripHistory.TripID = Trip.TripID;
                    }
                    else
                    {
                        TripHistory.TripID = PreflightMainentity.TripID;
                    }
                    TripHistory.CustomerID = CustomerID;
                    TripHistory.HistoryDescription = HistoryDescription.ToString();
                    TripHistory.LastUpdUID = UserPrincipal.Identity.Name;
                    TripHistory.LastUpdTS = DateTime.UtcNow;
                    TripHistory.RevisionNumber = (int?)Trip.RevisionNUM;

                    if (Trip.State != TripEntityState.Added)
                    {
                        PreflightMainentity.PreflightTripHistories.Add(TripHistory);
                        // Container.ObjectStateManager.ChangeObjectState(Tripexp, System.Data.EntityState.Added);
                    }
                    else
                    {
                        Container.PreflightTripHistories.AddObject(TripHistory);
                        //Container.ObjectStateManager.ChangeObjectState(TripHistory, System.Data.EntityState.Added);
                    }

                    isTripChange = true;
                }
            }



            #endregion
        }

        #region Preflight Trip History

        public ReturnValue<Data.Preflight.PreflightTripHistory> GetHistory(long CustomerId, long TripId)
        {
            ReturnValue<Data.Preflight.PreflightTripHistory> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, TripId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.PreflightTripHistory>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetHistory(CustomerId, TripId).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        #endregion

        private string GetCrewOrPAXString(string CrewPassengerType)
        {
            string CrewORPAX = string.Empty;

            if (CrewPassengerType.ToLower() == "p")
                CrewORPAX = "Passenger";
            else
                CrewORPAX = "Crew";

            return CrewORPAX;
        }

        private string GetDepartOrArrivalString(bool isDepart)
        {
            string strArrOrDep = string.Empty;

            if (isDepart)
                strArrOrDep = " Departure ";
            else
                strArrOrDep = " Arrival ";

            return strArrOrDep;
        }

        public ReturnValue<PreflightMain> DeleteTrip(Int64 TripID)
        {
            ReturnValue<PreflightMain> ReturnValue = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer Container = new PreflightDataModelContainer();
            PreflightMain trip = new PreflightMain();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        //lock
                        LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                        lockManager.Lock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, TripID);

                        using (Container = new PreflightDataModelContainer())
                        {
                            Container.DeleteTrip(TripID, UserName, DateTime.UtcNow, true);

                            ReturnValue.EntityInfo = null;
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock
                        LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, TripID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        #region "Copy Trip"

        public ReturnValue<PreflightMain> CopyTripDetails(DateTime DepartDate, long tripID, long customerID, bool IsLogisticsToBeCopied, bool IsCrewToBeCopied, bool IsPaxToBeCopied, bool isHistoryToBeCopied)
        {
            ReturnValue<PreflightMain> ReturnValue = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, isHistoryToBeCopied))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopytripID = new long();
                    CopytripID = 0;
                    bool isAutoDispatch = false;
                    bool IsRevision = false;
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        if (UserPrincipal.Identity.Settings.IsAutomaticDispatchNum == true)
                            isAutoDispatch = true;
                        else
                            isAutoDispatch = false;
                        if (UserPrincipal.Identity.Settings.IsAutoRevisionNum == true)
                            IsRevision = true;
                        else
                            IsRevision = false;
                        //CopytripID = cs.GetCopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, true, isAutoDispatch, IsRevision).FirstOrDefault();
                        CopytripID = cs.GetCopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, true, isAutoDispatch, IsRevision, UserName, isHistoryToBeCopied).FirstOrDefault();
                        //return (long)ret;

                        ReturnValue = GetTrip((long)CopytripID);

                        if (ReturnValue.ReturnFlag)
                        {
                            //ReturnValue.EntityList[0].State = TripEntityState.Modified;
                            PreflightMain Trip = ReturnValue.EntityList[0];
                            LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                            lockManager.Lock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, Trip.TripID);

                            Trip.State = TripEntityState.NoChange;
                            ReturnValue = UpdateTrip(Trip);

                            #region "TripHistory"

                            List<PreflightMain> PreflightMains = new List<PreflightMain>();

                            PreflightMains = cs.GetTripByTripID(CustomerID, tripID).ToList();
                            if (PreflightMains != null && PreflightMains.Count > 0)
                            {
                                PreflightMain OldTrip = PreflightMains[0];//cs.PreflightMains.Where(x => (x.TripID == tripID && x.CustomerID == customerID)).SingleOrDefault();
                                if (OldTrip != null)
                                {
                                    if (!isHistoryToBeCopied)
                                    {
                                        PreflightTripHistory TripHistory = new PreflightTripHistory();
                                        TripHistory.TripID = Trip.TripID;
                                        TripHistory.CustomerID = CustomerID;
                                        TripHistory.HistoryDescription = "Trip Details Copied From Trip No.: " + OldTrip.TripNUM.ToString();
                                        TripHistory.LastUpdUID = UserPrincipal.Identity.Name;
                                        TripHistory.LastUpdTS = DateTime.UtcNow;
                                        //Trip.PreflightTripHistories.Add(TripHistory);
                                        cs.PreflightTripHistories.AddObject(TripHistory);
                                        cs.SaveChanges();
                                    }
                                }
                            }
                            // Container.ObjectStateManager.ChangeObjectState(Tripexp, System.Data.EntityState.Added);


                            #endregion

                            if (ReturnValue.ReturnFlag)
                            {

                                var retvalexp = GetTripExceptionList(Trip.TripID);

                                if (retvalexp.ReturnFlag)
                                {
                                    if (retvalexp.EntityList != null && retvalexp.EntityList.Count > 0)
                                    {
                                        Trip.TripException = "!";
                                    }
                                }


                                ReturnValue.EntityInfo = Trip;
                                ReturnValue.ReturnFlag = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        #endregion

        #region "Copy Crew Calender Entries"

        public ReturnValue<PreflightMain> CopyCrewCalenderDetails(DateTime DepartDate, long tripID, long customerID)
        {
            ReturnValue<PreflightMain> ReturnValue = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, tripID, customerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopytripID = new long();
                    CopytripID = 0;
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        CopytripID = cs.CopyCrewCalenderDetails(DepartDate, tripID, customerID, true, UserName).FirstOrDefault();
                        ReturnValue = GetTrip((long)CopytripID);
                        if (ReturnValue.ReturnFlag)
                        {
                            PreflightMain Trip = ReturnValue.EntityList[0];
                            Trip.State = TripEntityState.NoChange;
                            LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                            lockManager.Lock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, Trip.TripID);

                            ReturnValue = UpdateTrip(Trip);
                            if (ReturnValue.ReturnFlag)
                            {
                                var retvalexp = GetTripExceptionList(Trip.TripID);
                                if (retvalexp.ReturnFlag)
                                {
                                    if (retvalexp.EntityList != null && retvalexp.EntityList.Count > 0)
                                    {
                                        Trip.TripException = "!";
                                    }
                                }
                                ReturnValue.EntityInfo = Trip;
                                ReturnValue.ReturnFlag = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        #endregion

        #endregion

        #region "Trip Leg"

        public ReturnValue<PreflightMain> AddPreflightLeg(PreflightLeg preflightLeg)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> UpdatePreflightLeg(PreflightLeg preflightLeg)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> DeletePreflightLeg(string LogNumber, string TripNumber, string LegID, string TripLegID)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Exceptions

        #endregion

        #region "Trip  Crew"

        public ReturnValue<PreflightMain> DeletePreflightCrew(string LogNumber, string TripNumber, string LegID, string TripLegID)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> UpdatePreflightCrew(PreflightCrewList preflightCrewlist)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<PreflightMain> AddPreflightCrew(PreflightCrewList preflightCrewlist)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region PostFlight
        public ReturnValue<Data.Preflight.PostflightMain> GetLogByTripID(long tripID)
        {
            ReturnValue<Data.Preflight.PostflightMain> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.PostflightMain>();
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetLogByTripID(CustomerID, tripID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        #endregion

        #region "Validate Entire Trip"

        public ReturnValue<PreflightTripException> ValidateTripforMandatoryExcep(PreflightMain Trip)
        {

            List<PreflightTripException> ExceptionList;
            PreflightDataModelContainer Container;
            ReturnValue<PreflightTripException> returnValue = new ReturnValue<PreflightTripException>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ExceptionList = new List<PreflightTripException>();
                    using (Container = new PreflightDataModelContainer())
                    {

                        //List<PreflightTripException> preflightExceptions = new List<PreflightTripException>();
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        TripExceptions = Container.GetExceptionTemplate((long)(PreflightExceptionModule.Preflight)).ToList();
                        //.ExceptionTemplates.Where(X => X.ModuleID == (long)(PreflightExceptionModule.Preflight)).ToList();
                        List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        List<ExceptionTemplate> PreflightMain_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.PreflightMain).ToList();
                        List<ExceptionTemplate> PreflightLeg_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.Legs).ToList();
                        List<ExceptionTemplate> PreflightCrew_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.Crew).ToList();

                        #region main
                        if(Trip.TripID != 0 || Trip.IsViewModelEdited)
                        {
                            foreach(ExceptionTemplate exptemp in PreflightMain_TemplateList.ToList())
                            {
                                if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.AircraftTypeReq))
                                {
                                    if(Trip.AircraftID == null)
                                    {
                                        PreflightTripException CurrException = new PreflightTripException();
                                        CurrException.TripID = Trip.TripID;
                                        CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                        CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                        CurrException.ExceptionTemplate = exptemp;
                                        ExceptionList.Add(CurrException);
                                    }
                                }

                                if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.TripRequiresAtleastoneLeg))
                                {
                                    if(Trip.PreflightLegs == null || Trip.PreflightLegs.Count == 0)
                                    {
                                        PreflightTripException CurrException = new PreflightTripException();
                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                        CurrException.TripID = Trip.TripID;
                                        CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                        CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                        CurrException.ExceptionTemplate = exptemp;
                                        ExceptionList.Add(CurrException);
                                    }
                                }

                                if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.HomebaseRequired))
                                {
                                    if(Trip.HomebaseID == null)
                                    {
                                        PreflightTripException CurrException = new PreflightTripException();
                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                        CurrException.TripID = Trip.TripID;
                                        CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                        CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                        CurrException.ExceptionTemplate = exptemp;
                                        ExceptionList.Add(CurrException);
                                    }
                                }

                                //if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.TailNoRequired))
                                //{
                                //    if (Trip.FleetID == null)
                                //    {
                                //        PreflightTripException CurrException = new PreflightTripException();
                                //        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                //        CurrException.TripID = Trip.TripID;
                                //        CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                //        CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                //        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                //        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                //        CurrException.LastUpdTS = DateTime.UtcNow;
                                //        CurrException.ExceptionTemplate = exptemp;
                                //        ExceptionList.Add(CurrException);
                                //    }
                                //}

                                if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.DepartDateRequired))
                                {
                                    if(Trip.EstDepartureDT == null)
                                    {
                                        PreflightTripException CurrException = new PreflightTripException();
                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                        CurrException.TripID = Trip.TripID;
                                        CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                        CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                        CurrException.ExceptionTemplate = exptemp;
                                        ExceptionList.Add(CurrException);
                                    }
                                }


                                if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.ClientCodeRequired))
                                {
                                    if(Trip.ClientID == null)
                                    {
                                        PreflightTripException CurrException = new PreflightTripException();
                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                        CurrException.TripID = Trip.TripID;
                                        CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                        CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                        CurrException.ExceptionTemplate = exptemp;
                                        ExceptionList.Add(CurrException);
                                    }
                                }

                            }
                        }
                        #endregion

                        #region Leg Validation
                        if (Trip.PreflightLegs != null) //&& Trip.PreflightLegs.Count > 0
                        {
                            foreach (PreflightLeg preflightLegitem in Preflegs)
                            {
                                if(Trip.TripID != 0 || preflightLegitem.IsLegEdited)
                                {
                                    foreach(ExceptionTemplate exptemp in PreflightLeg_TemplateList.ToList())
                                    {
                                        if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.DepartureICAOIDReq))
                                        {
                                            if(preflightLegitem.DepartICAOID == null || preflightLegitem.DepartICAOID == 0)
                                            {
                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                                string Desc = string.Empty;
                                                Desc = exptemp.ExceptionTemplateDescription;
                                                Desc = Desc.Replace("<legnum>", preflightLegitem.LegNUM.ToString());
                                                CurrException.ExceptionDescription = Desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                ExceptionList.Add(CurrException);
                                            }
                                        }
                                        if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.ArrivalICAOIDReq))
                                        {
                                            if(preflightLegitem.ArriveICAOID == null || preflightLegitem.ArriveICAOID == 0)
                                            {
                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                                string Desc = string.Empty;
                                                Desc = exptemp.ExceptionTemplateDescription;
                                                Desc = Desc.Replace("<legnum>", preflightLegitem.LegNUM.ToString());
                                                CurrException.ExceptionDescription = Desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                ExceptionList.Add(CurrException);
                                            }
                                        }


                                        if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.LocalDepartDateRequired))
                                        {
                                            if(preflightLegitem.DepartureDTTMLocal == null)
                                            {
                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                                string Desc = string.Empty;
                                                Desc = exptemp.ExceptionTemplateDescription;
                                                Desc = Desc.Replace("<legnum>", preflightLegitem.LegNUM.ToString());
                                                CurrException.ExceptionDescription = Desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                ExceptionList.Add(CurrException);
                                            }
                                        }

                                        if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.LocalArrivalDateRequired))
                                        {
                                            if(preflightLegitem.ArrivalDTTMLocal == null)
                                            {
                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                                string Desc = string.Empty;
                                                Desc = exptemp.ExceptionTemplateDescription;
                                                Desc = Desc.Replace("<legnum>", preflightLegitem.LegNUM.ToString());
                                                CurrException.ExceptionDescription = Desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                ExceptionList.Add(CurrException);
                                            }
                                        }

                                        if(exptemp.ExceptionTemplateID == (long) (PreflightExceptionCheckList.LegClientCodeRequired))
                                        {
                                            if(preflightLegitem.ClientID == null)
                                            {
                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                                string Desc = string.Empty;
                                                Desc = exptemp.ExceptionTemplateDescription;
                                                Desc = Desc.Replace("<legnum>", preflightLegitem.LegNUM.ToString());
                                                CurrException.ExceptionDescription = Desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                ExceptionList.Add(CurrException);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        returnValue.ReturnFlag = true;
                        returnValue.EntityList = ExceptionList;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;
        }
        /// <summary>
        /// Method to Validate Trip information
        /// </summary>
        /// <returns>Validate Business Exceptions</returns>
        private List<PreflightTripException> ValidateTrip(PreflightMain Trip)
        {

            const string Three0str = "000";
            const string Six0str = Three0str + Three0str;
            const string Twelve0str = Six0str + Six0str;
            const string Nine0str = Six0str + Three0str;
            const string str001 = "001";
            const string str002 = "002";
            const string str003 = "003";
            const string str004 = "004";
            const string str005 = "005";
            const string str006 = "006";
            const string str007 = "007";
            const string str008 = "008";
            const string str009 = "009";
            const string str010 = "010";
            const string str011 = "011";
            const string str012 = "012";
            const string str013 = "013";



            double minRunWay = 0, maxRunWay = 0;
            var DepartAirportLists = new List<Airport>();
            var ArrAirportLists = new List<Airport>();
            var FleetLists = new List<Fleet>();
            var preFlightCrewLists = new List<Crew>();
            List<PreflightTripException> preflightExceptions = new List<PreflightTripException>();

            ////#region Main Exception
            PreflightDataModelContainer Container;
            List<ExceptionTemplate> PreflightMain_TemplateList;
            List<ExceptionTemplate> PreflightLeg_TemplateList;
            List<ExceptionTemplate> PreflightCrew_TemplateList;
            List<ExceptionTemplate> PreflightPax_TemplateList;
            List<ExceptionTemplate> PreflightFBO_TemplateList;
            List<PreflightCrewList> ActiveStatusCrewlists = new List<PreflightCrewList>();
            //List<PreflightCrewList> PassengerPassportCrewlists = new List<PreflightCrewList>();
            List<long> PassengerPassportCrewlists = new List<long>();
            List<long> CrewNoPassportCrewlists = new List<long>();
            List<long> TypeRatingCrewlists = new List<long>();
            List<long> TripOverlapLists = new List<long>();
            List<long> DutyRuleCrewNotQualifiedlists = new List<long>();
            List<Int64> InActiveCrewlists = new List<Int64>();
            List<long> PassengerPassportPaxlists = new List<long>();
            List<long> PAXNoPassportPaxlists = new List<long>();
            List<long> PAXNoChoicePassportList = new List<long>();
            List<long> Crewchecklistlists = new List<long>();
            List<long> CrewNoChoicePassportList = new List<long>();
            List<long> AircraftFarRuleLegLists = new List<long>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                preflightExceptions = new List<PreflightTripException>();
                ////#region Main Exception

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Container = new PreflightDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        TripExceptions = Container.GetExceptionTemplate((long)(PreflightExceptionModule.Preflight)).ToList();//.ExceptionTemplates.Where(X => X.ModuleID == (long)(PreflightExceptionModule.Preflight)).ToList();
                        List<PreflightLeg> Preflegs = Trip.PreflightLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        //List<PreflightLeg> PrefCrewlists = Trip.pre.Where(x => x.LegNUM != null && x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        PreflightMain_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.PreflightMain).ToList();
                        PreflightLeg_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.Legs).ToList();
                        PreflightCrew_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.Crew).ToList();
                        PreflightPax_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.Passenger).ToList();
                        PreflightFBO_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)PreflightExceptionSubModule.FBO).ToList();

                        #region Main Validationn
                        foreach (ExceptionTemplate exptemp in PreflightMain_TemplateList.ToList())
                        {
                            //000000000000 + 001
                            #region Aicraft Null check
                            if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.AircraftTypeReq))
                            {
                                if (Trip.AircraftID == null)
                                {
                                    PreflightTripException CurrException = new PreflightTripException();
                                    CurrException.TripID = Trip.TripID;
                                    CurrException.CustomerID = CustomerID;
                                    CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    CurrException.DisplayOrder = string.Format("{0}{1}", Twelve0str, str001);
                                    preflightExceptions.Add(CurrException);
                                }
                            }
                            #endregion

                            //0000000000000 + 002
                            #region "Atleast one Leg Check"
                            if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.TripRequiresAtleastoneLeg))
                            {
                                if (Trip.PreflightLegs == null || Trip.PreflightLegs.Count == 0)
                                {
                                    PreflightTripException CurrException = new PreflightTripException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.TripID = Trip.TripID;
                                    CurrException.CustomerID = CustomerID;
                                    CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    CurrException.DisplayOrder = string.Format("{0}{1}", Twelve0str, str002);
                                    preflightExceptions.Add(CurrException);
                                }
                            }
                            #endregion

                            //0000000000000 + 003
                            #region Inspection Hours

                            if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.LegInspection))
                            {
                                if (UserPrincipal.Identity.Settings.IsTailInsuranceDue != null)
                                {
                                    if (UserPrincipal.Identity.Settings.IsTailInsuranceDue == true)
                                    {
                                        #region Variable Declaration
                                        DateTime date = DateTime.Now;
                                        double lnTotFltHrs = 0;
                                        double DiffInHrs;
                                        string lcDiffInHrs;
                                        double ete;
                                        string lcHrs = string.Empty;
                                        #endregion
                                        if (Trip.FleetID != null)
                                        {
                                            if (Trip.FleetID != 0)
                                            {
                                                IQueryable<Fleet> Fleets = preflightCompiledQuery.getFleet(Container, (long)Trip.FleetID, CustomerID);
                                                if (Fleets != null && Fleets.ToList().Count > 0)
                                                    FleetLists = Fleets.ToList();

                                                //FleetLists = (from fleetLists in Container.Fleets
                                                //              where (fleetLists.FleetID == Trip.FleetID)
                                                //              select fleetLists).ToList();

                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = CustomerID;
                                                StringBuilder desc = new StringBuilder();
                                                if (FleetLists[0].LastInspectionDT != null && FleetLists[0].InspectionHrs != 0 && FleetLists[0].WarningHrs != null)
                                                {
                                                    var TotFltHrs = Container.GetTotETEFlightHours(Trip.FleetID, Trip.TripID, UserPrincipal.Identity.CustomerID).ToList<decimal?>();

                                                    if (TotFltHrs != null)
                                                    {
                                                        lnTotFltHrs = Convert.ToDouble(TotFltHrs[0]);
                                                    }
                                                    // lnTotFltHrs = Convert.ToDouble(TotFltHrs);

                                                    foreach (PreflightLeg preflightLeg in Preflegs)
                                                    {
                                                        date = preflightLeg.HomeDepartureDTTM == null ? DateTime.MinValue : (DateTime)preflightLeg.HomeDepartureDTTM;

                                                        if (UserPrincipal.Identity.Settings.LogFixed != null && UserPrincipal.Identity.Settings.LogFixed == 1)
                                                        {
                                                            date = preflightLeg.HomeDepartureDTTM == null ? DateTime.MinValue : (DateTime)preflightLeg.HomeDepartureDTTM;
                                                        }
                                                        else
                                                        {
                                                            if (UserPrincipal.Identity.Settings.LogFixed != null && UserPrincipal.Identity.Settings.LogFixed == 2)
                                                                date = preflightLeg.DepartureGreenwichDTTM == null ? DateTime.MinValue : (DateTime)preflightLeg.DepartureGreenwichDTTM;
                                                        }

                                                        if (date > (DateTime)FleetLists[0].LastInspectionDT)
                                                        {
                                                            ete = preflightLeg.ElapseTM == null ? 0 : (double)preflightLeg.ElapseTM;

                                                            lnTotFltHrs = lnTotFltHrs + ete;
                                                        }
                                                    }
                                                    bool ShowException = false;

                                                    if (lnTotFltHrs >= (double)FleetLists[0].InspectionHrs)
                                                    {
                                                        ShowException = true;
                                                        desc.Clear();
                                                        desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                        desc = desc.Replace("<inspection due date>", "has passed inspection due date");
                                                        //Aircraft <inspection due date>
                                                    }

                                                    else
                                                    {
                                                        if (lnTotFltHrs >= (double)FleetLists[0].WarningHrs)// decimal
                                                        {
                                                            DiffInHrs = (double)FleetLists[0].InspectionHrs - (double)lnTotFltHrs;
                                                            lcDiffInHrs = DiffInHrs.ToString("0.00");
                                                            lcHrs = lcDiffInHrs;
                                                            // lcHrs = ConvertMinToTenths(lcDiffInHrs, true, "1");

                                                            if ((double)UserPrincipal.Identity.Settings.TimeDisplayTenMin == 1)
                                                                lcHrs = lcDiffInHrs;
                                                            else
                                                            {
                                                                if ((double)UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2) //hh:mm
                                                                {
                                                                    lcHrs = commPreFlight.Preflight_ConvertTenthsToMins(lcDiffInHrs, UserPrincipal.Identity.Settings.TimeDisplayTenMin);
                                                                }
                                                            }
                                                            ShowException = true;
                                                            desc.Clear();
                                                            desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                            desc = desc.Replace("<inspection due date>", "inspection due in ");
                                                            desc = desc.AppendFormat(" {0} hrs", lcHrs);
                                                        }
                                                    }

                                                    CurrException.ExceptionDescription = desc.ToString();
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    CurrException.DisplayOrder = string.Format("{0}{1}", Twelve0str, str003);
                                                    if (ShowException)
                                                        preflightExceptions.Add(CurrException);

                                                }
                                            }
                                        }

                                    }

                                }
                            }

                            #endregion

                            //0000000000000 + 004
                            #region "DepartMent Inactive"
                            if (Trip.DepartmentID != null)
                            {
                                if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.TripDepartmentInactive))
                                {
                                    long DepartmentID = (long)Trip.DepartmentID;

                                    var DepartMentList = new List<Department>();
                                    IQueryable<Department> Departments = preflightCompiledQuery.getDepartment(Container, DepartmentID, CustomerID);
                                    if (Departments != null && Departments.ToList().Count > 0)
                                        DepartMentList = Departments.ToList();

                                    //var DepartMentList = (from arrLists in Container.Departments
                                    //                      where (arrLists.DepartmentID == DepartmentID) //AirportID & ArrivalICAOID are equal
                                    //                      select arrLists).ToList();
                                    if (DepartMentList != null && DepartMentList.Count > 0)
                                    {
                                        if (DepartMentList[0].IsInActive == true)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplateDescription;
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            CurrException.DisplayOrder = string.Format("{0}{1}", Twelve0str, str004);
                                            preflightExceptions.Add(CurrException);
                                        }
                                    }
                                }
                            }
                            #endregion

                            //0000000000000 + 005
                            #region "Authorization Inactive"
                            if (Trip.AuthorizationID != null)
                            {
                                if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.TripAuthorizationInactive))
                                {
                                    long AuthorizationID = (long)Trip.AuthorizationID;
                                    var Authorizationlist = new List<DepartmentAuthorization>();
                                    IQueryable<DepartmentAuthorization> DepartmentAuthorizations = preflightCompiledQuery.getDepartmentAuthorization(Container, AuthorizationID, CustomerID);
                                    if (DepartmentAuthorizations != null && DepartmentAuthorizations.ToList().Count > 0)
                                        Authorizationlist = DepartmentAuthorizations.ToList();

                                    //var Authorizationlist = (from arrLists in Container.DepartmentAuthorizations
                                    //                         where (arrLists.AuthorizationID == AuthorizationID) //AirportID & ArrivalICAOID are equal
                                    //                         select arrLists).ToList();
                                    if (Authorizationlist != null && Authorizationlist.Count > 0)
                                    {
                                        if (Authorizationlist[0].IsInActive == true)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplateDescription;
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            CurrException.DisplayOrder = string.Format("{0}{1}", Twelve0str, str005);
                                            preflightExceptions.Add(CurrException);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        #endregion

                        List<PreflightCrewList> TripCrewList = new List<PreflightCrewList>();
                        if (Preflegs != null && Preflegs.Count > 0)
                        {
                            #region Leg Validation
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                #region "GetAllCrewList"
                                if (Preflegs[i].PreflightCrewLists != null && Preflegs[i].PreflightCrewLists.Count > 0)
                                {
                                    TripCrewList.AddRange(Preflegs[i].PreflightCrewLists);
                                    //TripCrewList.OrderBy(x => x.Crew.CrewCD);
                                }
                                #endregion
                                foreach (ExceptionTemplate exptemp in PreflightLeg_TemplateList.ToList())
                                {
                                    //Legnum ="001...010..025"
                                    //exceptionnum 000000001
                                    //010 + 000000001 + 000

                                    #region DepartICaoID
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.DepartureICAOIDReq))
                                    {
                                        if (Preflegs[i].DepartICAOID == null)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplateDescription;
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());


                                            length = length.Substring(length.Count() - 3, 3);

                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str001, Three0str);
                                            preflightExceptions.Add(CurrException);

                                        }
                                    }
                                    #endregion

                                    //010 + 000000002 + 000
                                    //000000002
                                    #region "Arrival ICaoID"
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.ArrivalICAOIDReq))
                                    {
                                        if (Preflegs[i].ArriveICAOID == null)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplateDescription;
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                            length = length.Substring(length.Count() - 3, 3);

                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str002, Three0str);
                                            preflightExceptions.Add(CurrException);
                                        }

                                    }
                                    #endregion

                                    #region "Depature ICAO Should Be Same as Previous Leg Arrival ICAO"
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.DepatureICAOShouldBeSameAsPreviousLegArrICAO))
                                    {
                                        if (Preflegs[i].LegNUM > 1)
                                        {
                                            var PreviousLeg = Preflegs.Where(F => F.LegNUM == Preflegs[i].LegNUM-1 && F.State != TripEntityState.Deleted).FirstOrDefault();
                                                if (PreviousLeg != null && PreviousLeg.ArriveICAOID != Preflegs[i].DepartICAOID)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<CurrentLeg>", Preflegs[i].LegNUM.ToString());
                                                    desc = desc.Replace("<PreviousLeg>", PreviousLeg.LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str002, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                    break;
                                                }
                                        }
                                    }
                                    #endregion

                                    //000000003
                                    #region "Arrival Airport Inactive"
                                    if (Preflegs[i].ArriveICAOID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.ArrivalAirportInActive))
                                        {
                                            long arrivalAirportId = (long)Preflegs[i].ArriveICAOID;

                                            IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, arrivalAirportId, CustomerID);
                                            if (Airports != null && Airports.ToList().Count > 0)
                                                ArrAirportLists = Airports.ToList();
                                            else
                                            {
                                                // Fix for #8222
                                                IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, arrivalAirportId);
                                                ArrAirportLists = airportList.ToList();
                                            }

                                            //ArrAirportLists = (from arrLists in Container.Airports
                                            //                   where (arrLists.AirportID == arrivalAirportId) //AirportID & ArrivalICAOID are equal
                                            //                   select arrLists).ToList();
                                            if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                            {
                                                if (ArrAirportLists[0].IsInActive == true)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str003, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //000000004
                                    #region "Depart Airport Inactive"
                                    if (Preflegs[i].DepartICAOID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.DepartureAirportInActive))
                                        {
                                            long departAirportId = (long)Preflegs[i].DepartICAOID;

                                            IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, departAirportId, CustomerID);
                                            if (Airports != null && Airports.ToList().Count > 0)
                                                DepartAirportLists = Airports.ToList();
                                            else
                                            {
                                                // Fix for #8222
                                                IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, departAirportId);
                                                DepartAirportLists = airportList.ToList();
                                            }

                                            //DepartAirportLists = (from departLists in Container.Airports
                                            //                      where (departLists.AirportID == departAirportId)
                                            //                      select departLists).ToList();
                                            if (DepartAirportLists != null && DepartAirportLists.Count > 0)
                                            {
                                                if (DepartAirportLists[0].IsInActive == true)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str004, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //000000005
                                    #region Distance Beyond Aircraft Capability
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.LegDistbeyondAircraftCapability))
                                    {
                                        if (Trip.AircraftID != null)
                                        {
                                            double aircrPS1 = 0.0, aircrPS2 = 0.0, aircrPS3 = 0.0;
                                            var airCraftLists = new List<Aircraft>();
                                            IQueryable<Aircraft> Aircrafts = preflightCompiledQuery.getAircraft(Container, (long)Trip.AircraftID, CustomerID);
                                            if (Aircrafts != null && Aircrafts.ToList().Count > 0)
                                                airCraftLists = Aircrafts.ToList();

                                            //var airCraftLists = (from airCrafts in Container.Aircraft
                                            //                     where (airCrafts.AircraftID == (long)Trip.AircraftID)
                                            //                     select airCrafts).ToList();
                                            if (airCraftLists != null) //&& airCraftLists.Count > 0
                                            {
                                                aircrPS1 = ((double)(airCraftLists[0].PowerSettings1TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings1HourRange));
                                                aircrPS2 = ((double)(airCraftLists[0].PowerSettings2TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings2HourRange));
                                                aircrPS3 = ((double)(airCraftLists[0].PowerSettings3TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings3HourRange));
                                                if (Preflegs[i].Distance != null)
                                                {

                                                    if (Preflegs[i].PowerSetting == "1" && aircrPS1 < (double)Preflegs[i].Distance)
                                                    {
                                                        PreflightTripException CurrException = new PreflightTripException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.TripID = Trip.TripID;
                                                        CurrException.CustomerID = CustomerID;
                                                        string desc = string.Empty;
                                                        desc = exptemp.ExceptionTemplateDescription;
                                                        desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                        CurrException.ExceptionDescription = desc;
                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                        CurrException.ExceptionTemplate = exptemp;
                                                        String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                        length = length.Substring(length.Count() - 3, 3);

                                                        CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str005, Three0str);
                                                        preflightExceptions.Add(CurrException);
                                                    }
                                                    if (Preflegs[i].PowerSetting == "2" && aircrPS2 < (double)Preflegs[i].Distance)
                                                    {
                                                        PreflightTripException CurrException = new PreflightTripException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.TripID = Trip.TripID;
                                                        CurrException.CustomerID = CustomerID;
                                                        string desc = string.Empty;
                                                        desc = exptemp.ExceptionTemplateDescription;
                                                        desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                        CurrException.ExceptionDescription = desc;
                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                        CurrException.ExceptionTemplate = exptemp;
                                                        String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                        length = length.Substring(length.Count() - 3, 3);

                                                        CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str005, Three0str);

                                                        preflightExceptions.Add(CurrException);
                                                    }
                                                    if (Preflegs[i].PowerSetting == "3" && aircrPS3 < (double)Preflegs[i].Distance)
                                                    {
                                                        PreflightTripException CurrException = new PreflightTripException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.TripID = Trip.TripID;
                                                        CurrException.CustomerID = CustomerID;
                                                        string desc = string.Empty;
                                                        desc = exptemp.ExceptionTemplateDescription;
                                                        desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                        CurrException.ExceptionDescription = desc;
                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                        CurrException.ExceptionTemplate = exptemp;
                                                        String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                        length = length.Substring(length.Count() - 3, 3);

                                                        CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str005, Three0str);

                                                        preflightExceptions.Add(CurrException);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    //000000006
                                    #region Arrival Airport Max runway below aircaft requirements

                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.ArrivalAirportMaxRunwayBelowAircraftRequirements))
                                    {

                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (Trip.FleetID != null)
                                                {
                                                    if (Trip.FleetID != 0)
                                                    {

                                                        IQueryable<Fleet> Fleets = preflightCompiledQuery.getFleet(Container, (long)Trip.FleetID, CustomerID);
                                                        Fleet OldFleet = new Fleet();
                                                        if (Fleets != null && Fleets.ToList().Count > 0)
                                                            FleetLists = Fleets.ToList();

                                                        //FleetLists = (from fleetLists in Container.Fleets
                                                        //              where (fleetLists.FleetID == Trip.FleetID)
                                                        //              select fleetLists).ToList();

                                                        if (FleetLists != null)
                                                        {
                                                            if (FleetLists.Count > 0)
                                                            {
                                                                if (FleetLists[0].MimimumRunway != null)
                                                                    //if ((double)FleetLists[0].MimimumRunway != 0.0)
                                                                    minRunWay = (double)FleetLists[0].MimimumRunway;
                                                                else
                                                                    minRunWay = 0.0;
                                                            }
                                                        }


                                                        long arrivalAirportId = (long)Preflegs[i].ArriveICAOID;

                                                        IQueryable<Airport> Airports = preflightCompiledQuery.getAirport(Container, arrivalAirportId, CustomerID);
                                                        Airport OldAirport = new Airport();
                                                        if (Airports != null && Airports.ToList().Count > 0)
                                                        {
                                                            ArrAirportLists = Airports.ToList();
                                                        }
                                                        else
                                                        {
                                                            // Fix for #8222
                                                            IQueryable<Airport> airportList = preflightCompiledQuery.getAirportByAirportID(Container, arrivalAirportId);
                                                            ArrAirportLists = airportList.ToList();
                                                        }

                                                        //ArrAirportLists = (from arrLists in Container.Airports
                                                        //                   where (arrLists.AirportID == arrivalAirportId) //AirportID & ArrivalICAOID are equal
                                                        //                   select arrLists).ToList();

                                                        if (ArrAirportLists != null)
                                                        {
                                                            if (ArrAirportLists.Count > 0)
                                                            {
                                                                if (ArrAirportLists[0].LongestRunway != null)
                                                                    maxRunWay = (double)ArrAirportLists[0].LongestRunway;
                                                                else
                                                                    maxRunWay = 0.0;
                                                            }
                                                        }
                                                        if (minRunWay > maxRunWay)
                                                        {
                                                            PreflightTripException CurrException = new PreflightTripException();
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplateDescription;
                                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                            CurrException.ExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                            length = length.Substring(length.Count() - 3, 3);

                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str006, Three0str);
                                                            preflightExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    //000000007
                                    #region Aircraft FARRule violation exists Aircraft not assigned to FAR
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.AircraftFARRuleviolationexistsAircraftnotassignedtoFAR))
                                    {
                                        if (Trip.FleetID != null)
                                        {
                                            if (Trip.FleetID != 0)
                                            {
                                                IQueryable<Fleet> Fleets = preflightCompiledQuery.getFleet(Container, (long)Trip.FleetID, CustomerID);
                                                if (Fleets != null && Fleets.ToList().Count > 0)
                                                    FleetLists = Fleets.ToList();

                                                //FleetLists = (from fleetLists in Container.Fleets
                                                //              where (fleetLists.FleetID == Trip.FleetID)
                                                //              select fleetLists).ToList();

                                                if (FleetLists != null)
                                                {
                                                    if (FleetLists.Count > 0)
                                                    {
                                                        //bool alreadyExists = false;


                                                        //if (AircraftFarRuleLegLists.Contains((long)Preflegs[i].LegID))//&& (long)PrefCrewlist.PassportID
                                                        //    {
                                                        //        alreadyExists = true;
                                                        //    }
                                                        //    if (alreadyExists == false)
                                                        //    {
                                                        if (Preflegs[i].FedAviationRegNUM != null)
                                                        {
                                                            if (Preflegs[i].FedAviationRegNUM == "91" && (FleetLists[0].IsFAR91 == null ? false : FleetLists[0].IsFAR91) == false || Preflegs[i].FedAviationRegNUM == "135" && (FleetLists[0].IsFAR135 == null ? false : FleetLists[0].IsFAR135) == false)
                                                            {
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                CurrException.TripID = Trip.TripID;
                                                                CurrException.CustomerID = CustomerID;
                                                                StringBuilder desc = new StringBuilder();
                                                                desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                                if (!string.IsNullOrEmpty(Preflegs[i].FedAviationRegNUM))
                                                                    desc = desc.AppendFormat(" {0}", Preflegs[i].FedAviationRegNUM);
                                                                desc = desc.AppendFormat(" in Leg {0}", Preflegs[i].LegNUM.Value.ToString());
                                                                CurrException.ExceptionDescription = desc.ToString();
                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                CurrException.ExceptionTemplate = exptemp;
                                                                String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                                length = length.Substring(length.Count() - 3, 3);

                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str007, Three0str);
                                                                AircraftFarRuleLegLists.Add((long)Preflegs[i].LegID);
                                                                preflightExceptions.Add(CurrException);

                                                            }
                                                            //}
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //000000008
                                    #region LegTime Aloft AircraftCapability
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.LegTimeAloftAircraftCapability))
                                    {

                                        if (Trip.AircraftID != null)
                                        {
                                            List<Aircraft> airCraftLists = new List<Aircraft>();

                                            IQueryable<Aircraft> Aircrafts = preflightCompiledQuery.getAircraft(Container, (long)Trip.AircraftID, CustomerID);

                                            if (Aircrafts != null && Aircrafts.ToList().Count > 0)
                                                airCraftLists = Aircrafts.ToList();

                                            //var airCraftLists = (from airCrafts in Container.Aircraft
                                            //                     where (airCrafts.AircraftID == (long)Trip.AircraftID)
                                            //                     select airCrafts).ToList();
                                            if (Preflegs[i].ElapseTM != null)
                                            {

                                                if (Preflegs[i].PowerSetting == "1" && (double)airCraftLists[0].PowerSettings1HourRange < (double)Preflegs[i].ElapseTM)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());


                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str008, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                                if (Preflegs[i].PowerSetting == "2" && (double)airCraftLists[0].PowerSettings1HourRange < (double)Preflegs[i].ElapseTM)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());


                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str008, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                                if (Preflegs[i].PowerSetting == "3" && (double)airCraftLists[0].PowerSettings1HourRange < (double)Preflegs[i].ElapseTM)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str008, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                            }
                                        }


                                    }
                                    #endregion

                                    //000000009
                                    //010 + 000000009+ 001  002  010
                                    //010000000009001
                                    //010000000009002
                                    //010000000009010
                                    #region AircraftConflictExistsForLeg
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.AircraftConflictExistsForLeg))
                                    {
                                        if (Trip.FleetID != null)
                                        {
                                            if (Trip.FleetID != 0)
                                            {
                                                if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                                {
                                                    if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                                    {
                                                        List<LegAircraftConflict> LegconflictList = new List<LegAircraftConflict>();
                                                        LegconflictList = Container.GetLegAircraftConflict((long)Trip.TripID, (long)Trip.FleetID, CustomerID, Preflegs[i].DepartureGreenwichDTTM, Preflegs[i].ArrivalGreenwichDTTM).ToList();

                                                        if (LegconflictList != null && LegconflictList.Count > 0)
                                                        {

                                                            string increstr = string.Empty;
                                                            int incrementCount = 0;
                                                            foreach (LegAircraftConflict AircraftConflit in LegconflictList)
                                                            {
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                ExceptionTempl = exptemp;
                                                                CurrException.TripID = Trip.TripID;
                                                                CurrException.CustomerID = CustomerID;
                                                                StringBuilder Desc = new StringBuilder();

                                                                Desc = Desc.AppendFormat("Leg {0}:", Preflegs[i].LegNUM);
                                                                Desc = Desc.AppendFormat(" Tail : {0}", AircraftConflit.TailNum);


                                                                if (AircraftConflit.RecordType == "T")
                                                                {
                                                                    Desc = Desc.AppendFormat(" conflicts Trip No.: {0}", AircraftConflit.TripNUM);
                                                                    Desc = Desc.AppendFormat(" Departure: {0}", AircraftConflit.DepartICaoID);
                                                                    Desc = Desc.AppendFormat(" Arrival: {0}", AircraftConflit.ArrivalICaoID);

                                                                }
                                                                else
                                                                {
                                                                    Desc = Desc.AppendFormat(" conflicts Fleet Calendar Entry");
                                                                }

                                                                Desc = Desc.AppendFormat(CultureInfo.InvariantCulture, " Date/Time:  {0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", AircraftConflit.DepartureGreenwichDTTM);
                                                                Desc = Desc.AppendFormat(CultureInfo.InvariantCulture, " To {0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", AircraftConflit.ArrivalGreenwichDTTM);

                                                                if (AircraftConflit.RecordType == "M")
                                                                    Desc = Desc.AppendFormat(" Duty: {0}", AircraftConflit.DutyTYPE);



                                                                CurrException.ExceptionDescription = Desc.ToString();
                                                                CurrException.ExceptionTemplateID = ExceptionTempl.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                //CurrException.ExceptionTemplate = ExceptionTempl;
                                                                String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                                length = length.Substring(length.Count() - 3, 3);

                                                                increstr = incrementCount.ToString();
                                                                increstr = string.Format("{0}{1}", Three0str, increstr);
                                                                increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str009, increstr);
                                                                preflightExceptions.Add(CurrException);
                                                                incrementCount = incrementCount + 1;
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                    #endregion




                                    //010000000010 + 001
                                    #region "DepartMent Inactive"
                                    if (Preflegs[i].DepartmentID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.LegDepartmentInactive))
                                        {
                                            long DepartmentID = (long)Preflegs[i].DepartmentID;
                                            List<Department> DepartMentList = new List<Department>();

                                            IQueryable<Department> Departments = preflightCompiledQuery.getDepartment(Container, DepartmentID, CustomerID);
                                            if (Departments != null && Departments.ToList().Count > 0)
                                                DepartMentList = Departments.ToList();

                                            //var DepartMentList = (from arrLists in Container.Departments
                                            //                      where (arrLists.DepartmentID == DepartmentID) //AirportID & ArrivalICAOID are equal
                                            //                      select arrLists).ToList();
                                            if (DepartMentList != null && DepartMentList.Count > 0)
                                            {
                                                if (DepartMentList[0].IsInActive == true)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str010, str001);


                                                    preflightExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //0100000000011 + 002
                                    #region "Authorization Inactive"
                                    if (Preflegs[i].AuthorizationID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.LegAuthorizationInactive))
                                        {
                                            long AuthorizationID = (long)Preflegs[i].AuthorizationID;

                                            IQueryable<DepartmentAuthorization> DepartmentAuthorizations = preflightCompiledQuery.getDepartmentAuthorization(Container, AuthorizationID, CustomerID);
                                            List<DepartmentAuthorization> Authorizationlist = new List<DepartmentAuthorization>();
                                            if (DepartmentAuthorizations != null && DepartmentAuthorizations.ToList().Count > 0)
                                                Authorizationlist = DepartmentAuthorizations.ToList();

                                            //var Authorizationlist = (from arrLists in Container.DepartmentAuthorizations
                                            //                         where (arrLists.AuthorizationID == AuthorizationID) //AirportID & ArrivalICAOID are equal
                                            //                         select arrLists).ToList();

                                            if (Authorizationlist != null && Authorizationlist.Count > 0)
                                            {
                                                if (Authorizationlist[0].IsInActive == true)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str010, str002);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion


                                    #region Number Of Passenge rExceed Aircraft Seat Capacity
                                    if (((Preflegs[i].SeatTotal == null ? 0 : Preflegs[i].SeatTotal) - (Preflegs[i].PassengerTotal == null ? 0 : Preflegs[i].PassengerTotal)) < 0)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.NumberOfPassengerExceedAirCraftCapacity))
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplateDescription;
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            desc = desc.Replace("<totalseat>", (Preflegs[i].SeatTotal == null ? 0 : Preflegs[i].SeatTotal).ToString());
                                            desc = desc.Replace("<totalpax>", (Preflegs[i].PassengerTotal == null ? 0 : Preflegs[i].PassengerTotal).ToString());
                                            desc = desc.Replace("<availableseat>", ((Preflegs[i].SeatTotal == null ? 0 : Preflegs[i].SeatTotal) - (Preflegs[i].PassengerTotal == null ? 0 : Preflegs[i].PassengerTotal)).ToString());

                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;

                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", Six0str, str013, str002, Preflegs[i].LegNUM.ToString());
                                            preflightExceptions.Add(CurrException);
                                        }
                                    }
                                    #endregion


                                    #region Flight Category Inactive                                   
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.FlightCategoryInactive))
                                        {
                                            if (Preflegs[i].FlightCategoryID != null)
                                            {
                                                IQueryable<FlightCatagory> FlightCatagorys = preflightCompiledQuery.getFlightCatagory(Container, Convert.ToInt64(Preflegs[i].FlightCategoryID), CustomerID);
                                                if (FlightCatagorys != null && FlightCatagorys.Count() > 0)
                                                {                    
                                                    foreach (FlightCatagory flightCategory in FlightCatagorys)
                                                    {
                                                        if (flightCategory.IsInActive == true)
                                                        {
                                                            PreflightTripException CurrException = new PreflightTripException();
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplateDescription;
                                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());

                                                            CurrException.ExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;

                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", Six0str, str013, str002, Preflegs[i].LegNUM.ToString());
                                                            preflightExceptions.Add(CurrException);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    
                                    #endregion


                                    //Leg prev and next ovelap
                                    if (i + 1 < Preflegs.Count)
                                    {
                                        #region CurrentLeg and Prev Leg Exceeds Default System Range
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CurrentandPreviousLegExceedsDefaultsSystemRange))
                                        {
                                            if (Preflegs[i].ArrivalDTTMLocal != null && Preflegs[i + 1].DepartureDTTMLocal != null)
                                            {
                                                if (Preflegs[i].ArrivalDTTMLocal < Preflegs[i + 1].DepartureDTTMLocal)
                                                {
                                                    if (UserPrincipal.Identity.Settings.TripsheetDTWarning != null)
                                                    {
                                                        if ((double)UserPrincipal.Identity.Settings.TripsheetDTWarning > 0)
                                                        {
                                                            TimeSpan span = ((DateTime)Preflegs[i + 1].DepartureDTTMLocal).Subtract((DateTime)Preflegs[i].ArrivalDTTMLocal);
                                                            if (((span.Days * 24 * 60) + (span.Hours * 60) + (span.Minutes)) > (double)UserPrincipal.Identity.Settings.TripsheetDTWarning * 24 * 60)
                                                            {
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                CurrException.TripID = Trip.TripID;
                                                                CurrException.CustomerID = CustomerID;
                                                                string desc = string.Empty;
                                                                desc = exptemp.ExceptionTemplateDescription;
                                                                desc = desc.Replace("<legnum1>", Preflegs[i + 1].LegNUM.ToString());
                                                                desc = desc.Replace("<legnum2>", Preflegs[i].LegNUM.ToString());
                                                                CurrException.ExceptionDescription = desc;
                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                CurrException.ExceptionTemplate = exptemp;
                                                                String length = string.Format("{0}{1}", Three0str, Preflegs[i + 1].LegNUM.ToString());

                                                                length = length.Substring(length.Count() - 3, 3);

                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str010, Three0str);
                                                                preflightExceptions.Add(CurrException);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Leg Arrival Date over laps with Next Departure date
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CurrentLegArrivalDateTimeOverlapsNextLegDepartureDateTime))
                                        {
                                            if (Preflegs[i + 1].DepartureGreenwichDTTM != null && Preflegs[i].ArrivalGreenwichDTTM != null)
                                            {
                                                if (Preflegs[i].ArrivalDTTMLocal > Preflegs[i + 1].DepartureDTTMLocal)
                                                {
                                                    PreflightTripException CurrException = new PreflightTripException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.TripID = Trip.TripID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplateDescription;
                                                    desc = desc.Replace("<legnum1>", String.Format(CultureInfo.InvariantCulture, "{1} : {0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i].ArrivalDTTMLocal, Preflegs[i].LegNUM.ToString()));
                                                    desc = desc.Replace("<legnum2>", String.Format(CultureInfo.InvariantCulture, "{1} : {0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i + 1].DepartureDTTMLocal, Preflegs[i + 1].LegNUM.ToString()));
                                                    CurrException.ExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str011, Three0str);
                                                    preflightExceptions.Add(CurrException);
                                                }
                                            }

                                        }
                                        #endregion
                                        /////////////////////////////////////////////////problem is here /////////////////////////////////////////////////////////////////////
                                        #region Leg Dates Over Lap
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CurrentLegLocalDepartDateTimeOverlapsPrevLegLocalArrivalDateTime))
                                        {
                                            if (Preflegs[i].ArrivalGreenwichDTTM > Preflegs[i + 1].DepartureGreenwichDTTM)
                                            //if (CurrentLegGmtDepart <= NextLegGmtArr)
                                            {
                                                PreflightTripException CurrException = new PreflightTripException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.TripID = Trip.TripID;
                                                CurrException.CustomerID = CustomerID;
                                                string desc = string.Empty;
                                                desc = exptemp.ExceptionTemplateDescription;
                                                desc = desc.Replace("<legnum1>", String.Format(CultureInfo.InvariantCulture, "{1} : {0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i + 1].DepartureDTTMLocal, Preflegs[i + 1].LegNUM.ToString()));
                                                desc = desc.Replace("<legnum2>", String.Format(CultureInfo.InvariantCulture, "{1} : {0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i].ArrivalDTTMLocal, Preflegs[i].LegNUM.ToString()));
                                                CurrException.ExceptionDescription = desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                String length = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                length = length.Substring(length.Count() - 3, 3);

                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", length, Six0str, str012, Three0str);
                                                preflightExceptions.Add(CurrException);
                                            }
                                        }
                                        #endregion


                                    }

                                }
                            }
                            #endregion

                        }

                        if (Preflegs != null && Preflegs.Count > 0)
                        {
                            //leg + crewcode + runn
                            //010 + 000MUR + 009 + 000
                            bool show24Overlap = false;

                            List<GetTripOverlapforCrewParameter> Crew24List = new List<GetTripOverlapforCrewParameter>();
                            ExceptionTemplate CrewExtemp = new ExceptionTemplate();

                            #region Crew Validation
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                foreach (ExceptionTemplate exptemp in PreflightCrew_TemplateList.ToList())
                                {
                                    #region "Atleast one Crew Check"
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewNotAssignedToLeg))
                                    {
                                        List<PreflightCrewList> PrefCrewList = new List<PreflightCrewList>();

                                        bool crewnotexist = false;

                                        if (Preflegs[i].PreflightCrewLists != null)
                                        {
                                            if (Preflegs[i].PreflightCrewLists.Count > 0)
                                            {
                                                PrefCrewList = Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList();// && x.IsDiscount==false

                                                if (PrefCrewList != null && PrefCrewList.Count > 0)
                                                    //if (PrefCrewList != null && PrefCrewList.Count == 0)
                                                    crewnotexist = false;
                                                else
                                                    crewnotexist = true;
                                            }
                                            else
                                                crewnotexist = true;
                                        }
                                        else
                                            crewnotexist = true;

                                        if (crewnotexist) // (PrefCrewList == null && PrefCrewList.Count == 0))//
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = string.Format("{0}{1}", desc, exptemp.ExceptionTemplateDescription);
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, Six0str, Three0str, Three0str);
                                            preflightExceptions.Add(CurrException);
                                        }

                                    }
                                    #endregion

                                    #region DutyType Not available forCrew
                                    //if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.DutyTypeNotAvailableForCrew))
                                    //{
                                    //    if (Preflegs[i].PreflightCrewLists != null && Preflegs[i].PreflightCrewLists.Count > 0)
                                    //    {
                                    //        foreach (PreflightCrewList PrefCrew in Preflegs[i].PreflightCrewLists)
                                    //        {
                                    //            if (PrefCrew.DutyTYPE == null && PrefCrew.IsDeleted == false)
                                    //            {
                                    //                PreflightTripException CurrException = new PreflightTripException();
                                    //                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    //                CurrException.TripID = Trip.TripID;
                                    //                CurrException.CustomerID = CustomerID;
                                    //                string desc = string.Empty;
                                    //                desc = exptemp.ExceptionTemplateDescription;
                                    //                desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                    //                CurrException.ExceptionDescription = desc;
                                    //                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    //                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    //                CurrException.LastUpdTS = DateTime.UtcNow;
                                    //                CurrException.ExceptionTemplate = exptemp;

                                    //                ////CurrException.DisplayOrder = Preflegs[i].LegNUM + PrefCrew. + Three0str;
                                    //                preflightExceptions.Add(CurrException);
                                    //            }

                                    //        }
                                    //    }
                                    //}
                                    #endregion

                                    #region Crew Passport Does Not Exists for International Leg

                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewNoPassportIntLeg))
                                    {
                                        if (Preflegs[i].PreflightCrewLists != null && Preflegs[i].CheckGroup != null && Preflegs[i].CheckGroup.ToLower() != "dom")
                                        {

                                            foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                bool alreadyExists = false;

                                                if (CrewNoPassportCrewlists.Contains((long)PrefCrewlist.CrewID))//&& (long)PrefCrewlist.PassportID
                                                {
                                                    alreadyExists = true;
                                                }

                                                if (alreadyExists == false)
                                                {
                                                    if (PrefCrewlist.PassportID == null || (PrefCrewlist.PassportID != null && (long)PrefCrewlist.PassportID == 0))
                                                    {
                                                        PreflightTripException CurrException = new PreflightTripException();

                                                        Crew currentcrew = new Crew();

                                                        IQueryable<Crew> Crews = preflightCompiledQuery.getCrew(Container, (long)PrefCrewlist.CrewID, CustomerID);
                                                        Crew OldCrew = new Crew();
                                                        if (Crews != null && Crews.ToList().Count > 0)
                                                            currentcrew = Crews.ToList()[0];

                                                        //currentcrew = (from crewisinactive in Container.Crews
                                                        //               where crewisinactive.IsDeleted == false && crewisinactive.CrewID == PrefCrewlist.CrewID
                                                        //               select crewisinactive).SingleOrDefault();

                                                        var CrewPassportList = Container.GetAllCrewPassport(PrefCrewlist.CrewID, null, CustomerID).ToList();

                                                        if (currentcrew != null && (CrewPassportList == null || CrewPassportList != null && CrewPassportList.Count == 0))
                                                        {

                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplateDescription;
                                                            desc = desc.Replace("<crewcode>", currentcrew.CrewCD);
                                                            CurrException.ExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            String crewCode = string.Format("{0}{1}", currentcrew.CrewCD, Six0str);
                                                            crewCode = crewCode.Substring(0, 6);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str008, Three0str);
                                                            CrewNoPassportCrewlists.Add((long)PrefCrewlist.CrewID);
                                                            preflightExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Crew Passport Expiry Date

                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewPassportExpire))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsAutoCrew != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsAutoCrew == true)
                                            {
                                                if (Preflegs[i].PreflightCrewLists != null)
                                                {
                                                    foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        bool alreadyExists = false;
                                                        if (PrefCrewlist.PassportID != null && PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")// && PrefCrewlist.IsDiscount==false
                                                        {
                                                            if (PassengerPassportCrewlists.Contains((long)PrefCrewlist.CrewID))//&& (long)PrefCrewlist.PassportID
                                                            {
                                                                alreadyExists = true;
                                                            }

                                                            if (alreadyExists == false)
                                                            {
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                var crewPassengerPassportLists = Container.GetPassportbyPassportId((long)PrefCrewlist.PassportID, true).ToList();
                                                                int lastleg = Preflegs.Count;
                                                                //DateTime lastLegDate = (DateTime)Preflegs[lastleg - 1].ArrivalGreenwichDTTM;
                                                                //DateTime ExpiryDate = lastLegDate.AddMonths(6);
                                                                DateTime MaxDate = Convert.ToDateTime((from maxDate in Preflegs select maxDate.ArrivalGreenwichDTTM).Max());
                                                                DateTime ExpiryDate = MaxDate.AddMonths(7);

                                                                if (crewPassengerPassportLists.Count > 0)
                                                                {
                                                                    if (crewPassengerPassportLists[0].PassportExpiryDT <= ExpiryDate || crewPassengerPassportLists[0].PassportExpiryDT <= MaxDate)
                                                                    {
                                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                        CurrException.TripID = Trip.TripID;
                                                                        CurrException.CustomerID = CustomerID;
                                                                        StringBuilder desc = new StringBuilder();
                                                                        desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                                        desc = desc.Replace("<Name>", crewPassengerPassportLists[0].PassportHolderCD);
                                                                        if (crewPassengerPassportLists[0].PassportExpiryDT > DateTime.Now)
                                                                            desc = desc.Replace("<Expire>", "is Expiring");
                                                                        else
                                                                            desc = desc.Replace("<Expire>", "Expired");
                                                                        string passportDate = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", crewPassengerPassportLists[0].PassportExpiryDT);
                                                                        desc = desc.AppendFormat(" on {0}", passportDate);
                                                                        CurrException.ExceptionDescription = desc.ToString();
                                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                                        CurrException.ExceptionTemplate = exptemp;
                                                                        String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                                        lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                                        String crewCode = string.Format("{0}{1}", crewPassengerPassportLists[0].PassportHolderCD, Six0str);
                                                                        crewCode = crewCode.Substring(0, 6);
                                                                        CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str001, Three0str);
                                                                        PassengerPassportCrewlists.Add((long)PrefCrewlist.CrewID);
                                                                        preflightExceptions.Add(CurrException);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Crew travelling on an international leg has not selected a Choice passport
                                    // Fix for #1325
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewNoChoicePassportIntLeg))
                                    {

                                        if (Preflegs[i].PreflightCrewLists != null && Preflegs[i].CheckGroup != null && Preflegs[i].CheckGroup.ToLower() != "dom")
                                        {
                                            foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                bool alreadyExists = false;

                                                if (CrewNoChoicePassportList.Contains((long)PrefCrewlist.CrewID))
                                                    alreadyExists = true;

                                                if (alreadyExists == false)
                                                {
                                                    if (PrefCrewlist.PassportID == null || (PrefCrewlist.PassportID != null && (long)PrefCrewlist.PassportID == 0))
                                                    {
                                                        PreflightTripException CurrException = new PreflightTripException();

                                                        IQueryable<Crew> Crews = preflightCompiledQuery.getCrew(Container, (long)PrefCrewlist.CrewID, CustomerID);
                                                        Crew currentcrew = new Crew();
                                                        if (Crews != null && Crews.ToList().Count > 0)
                                                            currentcrew = Crews.ToList()[0];

                                                        var CrewPassportList = Container.GetAllCrewPassport(PrefCrewlist.CrewID, null, CustomerID).ToList();
                                                        var CrewPassportNoChoiceList = Container.GetAllCrewPassport(PrefCrewlist.CrewID, null, CustomerID).Where(x => x.Choice == true).ToList();
                                                        int incrementCount = 0;
                                                        string increstr = string.Empty;
                                                        if (CrewPassportList != null && CrewPassportList.Count > 0 && (CrewPassportNoChoiceList == null || (CrewPassportNoChoiceList != null && CrewPassportNoChoiceList.Count == 0)))
                                                        {
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplateDescription;
                                                            desc = desc.Replace("<crewcode>", currentcrew.CrewCD);
                                                            CurrException.ExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;



                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);

                                                            String crewCode = string.Format("{0}{1}", currentcrew.CrewCD, Six0str);
                                                            crewCode = crewCode.Substring(0, 6);

                                                            increstr = incrementCount.ToString();
                                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str007, increstr);


                                                            CrewNoChoicePassportList.Add((long)PrefCrewlist.CrewID);
                                                            preflightExceptions.Add(CurrException);
                                                            incrementCount = incrementCount + 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Crew – Active Status to Leg
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewisNotActiveAndIsAssignedonLeg))
                                    {
                                        if (TripCrewList != null && TripCrewList.Count > 0)
                                        {
                                            //var query = //from crew in Container.Crews
                                            //            from prefcrewlist in TripCrewList
                                            //            join prefleg in Preflegs on prefcrewlist.EntityKey equals prefleg.EntityKey
                                            ////            where crew.IsStatus == false
                                            //            where (prefcrewlist.IsDeleted == false )
                                            //            orderby prefcrewlist.CrewID, prefleg.LegNUM
                                            //            select new { CrewID = prefcrewlist.CrewID, Legnum = prefleg.LegNUM };

                                            var query = //from crew in Container.Crews

                                                        from prefleg in Preflegs
                                                        from crewlist in prefleg.PreflightCrewLists
                                                        //            where crew.IsStatus == false
                                                        where (crewlist.IsDeleted == false && crewlist.DutyTYPE != "D")
                                                        orderby crewlist.CrewID, prefleg.LegNUM
                                                        select new { CrewID = crewlist.CrewID, Legnum = prefleg.LegNUM };



                                            int crewindex = 0;
                                            Int64 currcrew = 0;
                                            string Legspresent = string.Empty;
                                            int incrementCount = 0;
                                            string increstr = string.Empty;
                                            foreach (var crewinactive in query)
                                            {

                                                if (crewindex == 0)
                                                {
                                                    currcrew = (long)crewinactive.CrewID;
                                                    Legspresent = string.Format(" {0},", crewinactive.Legnum.ToString());

                                                }
                                                else
                                                {
                                                    if (currcrew == crewinactive.CrewID)
                                                    {
                                                        Legspresent = string.Format("{0} {1}, ", Legspresent, crewinactive.Legnum.ToString());
                                                    }
                                                    else
                                                    {
                                                        if (!InActiveCrewlists.Contains(currcrew))//&& (long)PrefCrewlist.PassportID
                                                        {
                                                            List<Crew> currentcrew = new List<Crew>();

                                                            IQueryable<Crew> Crews = preflightCompiledQuery.getInActvieCrew(Container, currcrew, CustomerID);
                                                            if (Crews != null && Crews.ToList().Count > 0)
                                                                currentcrew = Crews.ToList();

                                                            //currentcrew = (from crewisinactive in Container.Crews
                                                            //               where crewisinactive.IsStatus == false && crewisinactive.CrewID == currcrew
                                                            //               select crewisinactive).ToList();

                                                            if (currentcrew != null && currentcrew.Count > 0)
                                                            {
                                                                StringBuilder desc = new StringBuilder();
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                CurrException.TripID = Trip.TripID;
                                                                CurrException.CustomerID = CustomerID;
                                                                desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                                desc = desc.Replace("<crewcode>", currentcrew[0].CrewCD);
                                                                Legspresent = Legspresent.Substring(0, Legspresent.Length - 1);
                                                                desc = desc.AppendFormat(" {0}", Legspresent);
                                                                CurrException.ExceptionDescription = desc.ToString();
                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                CurrException.ExceptionTemplate = exptemp;
                                                                String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                                lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);

                                                                String crewCode = string.Format("{0}{1}", currentcrew[0].CrewCD, Six0str);
                                                                crewCode = crewCode.Substring(0, 6);

                                                                increstr = incrementCount.ToString();
                                                                increstr = string.Format("{0}{1}", Three0str, increstr);
                                                                increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str002, increstr);

                                                                preflightExceptions.Add(CurrException);
                                                                incrementCount = incrementCount + 1;
                                                                InActiveCrewlists.Add(currcrew);

                                                            }
                                                            currcrew = (long)crewinactive.CrewID;
                                                            Legspresent = string.Format(" {0},", crewinactive.Legnum.ToString());
                                                        }
                                                    }
                                                }
                                                crewindex++;

                                                if (crewindex == query.Count())
                                                {
                                                    if (!InActiveCrewlists.Contains(currcrew))//&& (long)PrefCrewlist.PassportID
                                                    {
                                                        List<Crew> currentcrew = new List<Crew>();
                                                        IQueryable<Crew> Crews = preflightCompiledQuery.getInActvieCrew(Container, currcrew, CustomerID);
                                                        if (Crews != null && Crews.ToList().Count > 0)
                                                            currentcrew = Crews.ToList();

                                                        //currentcrew = (from crewisinactive in Container.Crews
                                                        //               where crewisinactive.IsStatus == false && crewisinactive.CrewID == currcrew
                                                        //               select crewisinactive).ToList();
                                                        if (currentcrew != null && currentcrew.Count > 0)
                                                        {
                                                            StringBuilder desc = new StringBuilder();
                                                            PreflightTripException CurrException = new PreflightTripException();
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                            desc = desc.Replace("<crewcode>", currentcrew[0].CrewCD);
                                                            Legspresent = Legspresent.Substring(0, Legspresent.Length - 1);
                                                            desc = desc.AppendFormat(" {0}", Legspresent);
                                                            CurrException.ExceptionDescription = desc.ToString();
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            String crewCode = string.Format("{0}{1}", currentcrew[0].CrewCD, Six0str);
                                                            crewCode = crewCode.Substring(0, 6);
                                                            increstr = incrementCount.ToString();
                                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str002, increstr);

                                                            preflightExceptions.Add(CurrException);
                                                            incrementCount = incrementCount + 1;
                                                            InActiveCrewlists.Add(currcrew);

                                                        }
                                                        currcrew = (long)crewinactive.CrewID;
                                                        Legspresent = string.Format(" {0},", crewinactive.Legnum.ToString());
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    #region Crew - Code Not Qualified CrewDutyRule
                                    // Fix for PROD-122
                                    if (UserPrincipal.Identity.Settings.IsCrewQaulifiedFAR != null && (bool)UserPrincipal.Identity.Settings.IsCrewQaulifiedFAR)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewCodeflyinglegnumnotqualifiedCrewDutyRule))
                                        {
                                            if (Trip.FleetID != null && Trip.FleetID > 0)
                                            {

                                                foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                {
                                                    bool alreadyExists = false;
                                                    Crew crew = Container.Crews.Where(C => C.CrewID == PrefCrewlist.CrewID).First();
                                                    if (PrefCrewlist.DutyTYPE != null && PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")// && PrefCrewlist.DutyTYPE!="D"
                                                    {
                                                        if (DutyRuleCrewNotQualifiedlists.Contains((long)PrefCrewlist.CrewID))//&& (long)PrefCrewlist.PassportID
                                                        {
                                                            alreadyExists = true;
                                                        }

                                                        if (alreadyExists == false)
                                                        {
                                                            List<GetNotQualifiedCrewDutyRule> crewDutyRuleNotQualifiedLists = Container.GetNotQualifiedCrewDutyRule(PrefCrewlist.DutyTYPE, Trip.FleetID, (long)PrefCrewlist.CrewID, Preflegs[i].FedAviationRegNUM, CustomerID).ToList();
                                                            //tNotQualifiedCrewDutyRule(Trip.FleetID,Trip.CrewID).ToList();
                                                            if (crewDutyRuleNotQualifiedLists == null || crewDutyRuleNotQualifiedLists.Count == 0)
                                                            {
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                CurrException.TripID = Trip.TripID;
                                                                CurrException.CustomerID = CustomerID;
                                                                StringBuilder desc = new StringBuilder();
                                                                desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                                desc = desc.Replace("<crewcode>", crew.CrewCD);
                                                                desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                                if (!string.IsNullOrEmpty(Preflegs[i].FedAviationRegNUM) && (!string.IsNullOrWhiteSpace(Preflegs[i].FedAviationRegNUM)))
                                                                    desc = desc.AppendFormat(" {0}", Preflegs[i].FedAviationRegNUM);
                                                                CurrException.ExceptionDescription = desc.ToString(); ;
                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                CurrException.ExceptionTemplate = exptemp;
                                                                String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                                lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);

                                                                String crewCode = string.Format("{0}{1}", crew.CrewCD, Six0str);
                                                                crewCode = crewCode.Substring(0, 6);
                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str003, Three0str);

                                                                TypeRatingCrewlists.Add((long)PrefCrewlist.CrewID);
                                                                preflightExceptions.Add(CurrException);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    #region CrewTypeRating
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewTypeRating))
                                    {
                                        if (Trip.FleetID != null)
                                        {
                                            if (Trip.FleetID != 0)
                                            {
                                                foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                {
                                                    bool alreadyExists = false;
                                                    IQueryable<Crew> Crews = preflightCompiledQuery.getCrew(Container, (long)PrefCrewlist.CrewID, CustomerID);
                                                    Crew crew = new Crew();
                                                    if (Crews != null && Crews.ToList().Count > 0)
                                                        crew = Crews.ToList()[0];

                                                    //Crew crew = Container.Crews.Where(C => C.CrewID == PrefCrewlist.CrewID).First();

                                                    IQueryable<Aircraft> Aircrafts = preflightCompiledQuery.getAircraft(Container, (long)Trip.AircraftID, CustomerID);
                                                    Aircraft aircraft = new Aircraft();
                                                    if (Aircrafts != null && Aircrafts.ToList().Count > 0)
                                                        aircraft = Aircrafts.ToList()[0];

                                                    //Aircraft aircraft = Container.Aircraft.Where(C => C.AircraftID == Trip.AircraftID).First();
                                                    if (PrefCrewlist.DutyTYPE != null && PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")
                                                    {
                                                        if (TypeRatingCrewlists.Contains((long)PrefCrewlist.CrewID))//&& (long)PrefCrewlist.PassportID
                                                        {
                                                            alreadyExists = true;
                                                        }

                                                        if (alreadyExists == false)
                                                        {
                                                            List<GetCrewTypeRating> CrewTypeRatingLists = Container.GetCrewTypeRating(Trip.FleetID, (long)PrefCrewlist.CrewID, CustomerID).ToList();
                                                            if (CrewTypeRatingLists == null || CrewTypeRatingLists.Count == 0)
                                                            {
                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                CurrException.TripID = Trip.TripID;
                                                                CurrException.CustomerID = CustomerID;
                                                                StringBuilder desc = new StringBuilder();
                                                                desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                                desc = desc.Replace("<crewcode>", crew.CrewCD);
                                                                desc = desc.AppendFormat(" {0}", aircraft.AircraftCD);
                                                                CurrException.ExceptionDescription = desc.ToString();
                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                CurrException.ExceptionTemplate = exptemp;
                                                                String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                                lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);

                                                                String crewCode = string.Format("{0}{1}", crew.CrewCD, Six0str);
                                                                crewCode = crewCode.Substring(0, 6);
                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str004, Three0str);

                                                                TypeRatingCrewlists.Add((long)PrefCrewlist.CrewID);
                                                                preflightExceptions.Add(CurrException);
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    #region Crew Checklist Conflicts
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewChecklistConflicts))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (Preflegs[i].PreflightCrewLists != null)
                                                {


                                                    foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        bool alreadyExists = false;
                                                        if (Crewchecklistlists.Contains((long)PrefCrewlist.CrewID))//&& (long)PrefCrewlist.PassportID
                                                        {
                                                            alreadyExists = true;
                                                        }

                                                        if (!alreadyExists)
                                                        {
                                                            int LastCount = 0;
                                                            LastCount = Preflegs.Count;
                                                            int incrementCount = 0;
                                                            string increstr = string.Empty;
                                                            if (Preflegs[i].DepartureGreenwichDTTM != null && Preflegs[LastCount - 1].ArrivalGreenwichDTTM != null && PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")// 
                                                            {
                                                                DateTime FirstLegDepartUTCDate = (DateTime)Preflegs[i].DepartureGreenwichDTTM;
                                                                DateTime LastLegArrivalUTCDate = (DateTime)Preflegs[LastCount - 1].ArrivalGreenwichDTTM;
                                                                List<GetCrewChecklistConflicts> crewConflictsLists = Container.GetCrewChecklistConflicts(CustomerID, (DateTime)FirstLegDepartUTCDate, (DateTime)LastLegArrivalUTCDate, PrefCrewlist.CrewID).ToList();
                                                                if (crewConflictsLists.Count > 0)
                                                                {
                                                                    foreach (GetCrewChecklistConflicts checklist in crewConflictsLists)
                                                                    {
                                                                        PreflightTripException CurrException = new PreflightTripException();
                                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                        CurrException.TripID = Trip.TripID;
                                                                        CurrException.CustomerID = CustomerID;
                                                                        StringBuilder desc = new StringBuilder();
                                                                        desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                                        desc = desc.Replace("<crewcode>", checklist.CrewCD);

                                                                        desc = desc.AppendFormat(" Out-of-date checklist {0}", checklist.CrewChecklistDescription);
                                                                        CurrException.ExceptionDescription = desc.ToString();
                                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                                        CurrException.ExceptionTemplate = exptemp;
                                                                        String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());

                                                                        lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);

                                                                        String crewCode = string.Format("{0}{1}", checklist.CrewCD, Six0str);
                                                                        crewCode = crewCode.Substring(0, 6);
                                                                        increstr = incrementCount.ToString();
                                                                        increstr = string.Format("{0}{1}", Three0str, increstr);
                                                                        increstr = increstr.Substring(increstr.Count() - 3, 3);

                                                                        CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str005, increstr);
                                                                        preflightExceptions.Add(CurrException);
                                                                        incrementCount = incrementCount + 1;
                                                                    }
                                                                }
                                                            }
                                                            Crewchecklistlists.Add((long)PrefCrewlist.CrewID);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    #endregion

                                    #region Leg CrewConflicts
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewConflicts))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (Preflegs[i].PreflightCrewLists != null && Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList().Count() > 0)
                                                {
                                                    string crewIdsString = string.Empty;
                                                    string crewIds = string.Empty;
                                                    int ListsCount = 0;
                                                    int crewConflicts = 0;
                                                    foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        if (PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")
                                                        {
                                                            crewIds = PrefCrewlist.CrewID.ToString();
                                                            crewIdsString += string.Format("{0} ,", crewIds);//cre1,cre2,cre3,
                                                        }
                                                    }

                                                    if (!string.IsNullOrEmpty(crewIdsString))
                                                    {
                                                        crewIdsString = crewIdsString.Remove(crewIdsString.Length - 1);
                                                        if (Preflegs[i].DepartureGreenwichDTTM != null && Preflegs[i].ArrivalGreenwichDTTM != null) // && PrefCrewlist.IsDeleted == false
                                                        {
                                                            DateTime LegDepartUTCDate = (DateTime)Preflegs[i].DepartureGreenwichDTTM;
                                                            DateTime LegArrivalUTCDate = (DateTime)Preflegs[i].ArrivalGreenwichDTTM;
                                                            List<GetCrewConflicts> crewConflictsLists = Container.GetCrewConflicts(CustomerID, (DateTime)LegDepartUTCDate, (DateTime)LegArrivalUTCDate, crewIdsString, (Int64)Trip.TripID).ToList();
                                                            crewConflicts = crewConflictsLists.Count;
                                                            int incrementCount = 0;
                                                            string increstr = string.Empty;
                                                            if (crewConflictsLists.Count > 0)
                                                            {
                                                                for (ListsCount = 0; ListsCount < crewConflicts; ListsCount++)
                                                                {
                                                                    PreflightTripException CurrException = new PreflightTripException();
                                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                    CurrException.TripID = Trip.TripID;
                                                                    CurrException.CustomerID = CustomerID;
                                                                    StringBuilder desc = new StringBuilder();
                                                                    desc = desc.AppendFormat("{0}", exptemp.ExceptionTemplateDescription);
                                                                    desc = desc.AppendFormat(" for Trip {0}", crewConflictsLists[ListsCount].TripNUM);
                                                                    desc = desc.Replace("<crewcode>", crewConflictsLists[ListsCount].CrewCD);

                                                                    CurrException.ExceptionDescription = desc.ToString();
                                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                                    CurrException.ExceptionTemplate = exptemp;
                                                                    String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                                    lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);

                                                                    String crewCode = string.Format("{0}{1}", crewConflictsLists[ListsCount].CrewCD, Six0str);
                                                                    crewCode = crewCode.Substring(0, 6);

                                                                    increstr = incrementCount.ToString();
                                                                    increstr = string.Format("{0}{1}", Three0str, increstr);
                                                                    increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str006, increstr);
                                                                    preflightExceptions.Add(CurrException);
                                                                    incrementCount = incrementCount + 1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region crew calendar entry conflict
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewConflicts))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (Preflegs[i].PreflightCrewLists != null && Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList().Count() > 0)
                                                {
                                                    string crewIdsString = string.Empty;
                                                    string crewIds = string.Empty;
                                                    string increstr = string.Empty;
                                                    int incrementCount = 0;
                                                    foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        if (PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")
                                                        {
                                                            crewIds = PrefCrewlist.CrewID.ToString();
                                                            if (PrefCrewlist == Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).LastOrDefault())
                                                                crewIdsString += string.Format("{0}", crewIds);//cre1,cre2,cre3,
                                                            else
                                                                crewIdsString += string.Format("{0} ,", crewIds);//cre1,cre2,cre3,
                                                        }
                                                    }
                                                    Collection<string> selectedNodes = new Collection<string>();
                                                    string[] chkcrewConflicts;
                                                    chkcrewConflicts = Convert.ToString(crewIdsString).Split(',');
                                                    for (int j = 0; j <= chkcrewConflicts.Length - 1; j++)
                                                    {
                                                        selectedNodes.Add(Convert.ToString(chkcrewConflicts[j]));
                                                    }
                                                    var inputFromCrewTree = selectedNodes;
                                                    FlightPak.Business.Preflight.ScheduleCalendarManager objSchedule = new FlightPak.Business.Preflight.ScheduleCalendarManager();
                                                    FlightPak.Business.Preflight.ScheduleCalendarEntities.FilterEntity serviceFilterCriteria = new FlightPak.Business.Preflight.ScheduleCalendarEntities.FilterEntity();
                                                    var objvalue = objSchedule.GetCrewCalendarData(Preflegs[i].DepartureDTTMLocal.Value, Preflegs[i].ArrivalDTTMLocal.Value.AddDays(1), serviceFilterCriteria, inputFromCrewTree, false);
                                                    List<CrewCalendarDataResult> calendarResult = new List<CrewCalendarDataResult>();
                                                    if (objvalue.ReturnFlag)
                                                    {
                                                        calendarResult = objvalue.EntityList.Where(x => x.RecordType == "C" && chkcrewConflicts.Contains(x.CrewID.Value.ToString())).ToList();
                                                        var conflictitems = calendarResult.ToList();
                                                        string strWarning = String.Empty;
                                                        int count = 0;
                                                        PreflightTripException CurrException = new PreflightTripException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.TripID = Trip.TripID;
                                                        CurrException.CustomerID = CustomerID;
                                                        foreach (var items in conflictitems)
                                                        {
                                                            count++;
                                                            if (count == 1)
                                                                strWarning = strWarning + "<br/>";
                                                            else
                                                                strWarning = String.Format("{0}<br/>", strWarning);
                                                            if (items.RecordType == "C")
                                                                strWarning = String.Format("{0}<br/> Crew Code:{1}; Crew Calendar Entry:{2};<br/> Date/Time:{3} to {4} <br/>Description: {5}<br/>Conflicts with Crew Calendar Entry #{6} Scheduled on {7}<br/>", strWarning, Convert.ToString(items.CrewCD), i, Preflegs[i].DepartureDTTMLocal.Value, Preflegs[i].ArrivalDTTMLocal.Value.AddDays(1), items.CrewDutyTypeDescription, items.TripNUM, items.HomeDepartureDTTM.ToString());
                                                            else if (items.RecordType == "T")
                                                                strWarning = String.Format("{0}<br/> Crew Code:{1}; Crew Calendar Entry:{2};<br/> Date/Time:{3} to {4} <br/>Description: {5}<br/>Conflicts with Trip #{6} Scheduled on {7}<br/>", strWarning, Convert.ToString(items.CrewCD), i, Preflegs[i].DepartureDTTMLocal.Value, Preflegs[i].ArrivalDTTMLocal.Value.AddDays(1), items.CrewDutyTypeDescription, items.TripNUM, items.HomeDepartureDTTM.ToString());
                                                            CurrException.ExceptionDescription = strWarning;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            String crewCode = string.Format("{0}{1}", items.CrewCD, Six0str);
                                                            crewCode = crewCode.Substring(0, 6);
                                                            increstr = incrementCount.ToString();
                                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str006, increstr);
                                                            preflightExceptions.Add(CurrException);
                                                            incrementCount = incrementCount + 1;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                            #endregion

                                    #region Checking 24-hrs Trip Overlap for Crew
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.CrewisAlsoFlyingOnTripFallsWithin24HourPeriod))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsCrewOlap != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsCrewOlap == true)
                                            {
                                                if (Preflegs[i].DepartureGreenwichDTTM != null && Preflegs[i].ArrivalGreenwichDTTM != null) // && PrefCrewlist.IsDeleted == false
                                                {
                                                    foreach (PreflightCrewList PrefCrewlist in Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        if (PrefCrewlist.IsDeleted == false && PrefCrewlist.DutyTYPE != "D")
                                                        {
                                                            show24Overlap = true;
                                                            DateTime LegDepartUTCDate = (DateTime)Preflegs[i].DepartureGreenwichDTTM;
                                                            DateTime LegArrivalUTCDate = (DateTime)Preflegs[i].ArrivalGreenwichDTTM;
                                                            List<GetTripOverlapforCrewParameter> CrewOverlapLists = Container.GetTripOverlapforCrewParameter((long)PrefCrewlist.CrewID, (DateTime)LegDepartUTCDate, (DateTime)LegArrivalUTCDate, (Int64)Trip.TripID, CustomerID).ToList();
                                                            if (CrewOverlapLists != null)
                                                                Crew24List.AddRange(CrewOverlapLists);
                                                            CrewExtemp = exptemp;
                                                        }
                                                        //}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion


                                    #region Crew  PIC Not assignmed to Crew in Leg
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.PICNotAssignedToCrewInLeg))
                                    {                                        
                                        List<PreflightCrewList> PrefCrewListWithPIC = new List<PreflightCrewList>();
                                        bool havePICdutytype = false;
                                        if (Preflegs[i].PreflightCrewLists != null)
                                        {
                                            if (Preflegs[i].PreflightCrewLists.Count > 0)
                                            {
                                                PrefCrewListWithPIC = Preflegs[i].PreflightCrewLists.Where(x => x.IsDeleted == false && (x.DutyTYPE??"").Trim() == "P").ToList();// && x.IsDiscount==false

                                                if (PrefCrewListWithPIC != null && PrefCrewListWithPIC.Count > 0)                                                    
                                                    havePICdutytype = true;                                                
                                            }
                                        }

                                        if (havePICdutytype == false)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = string.Format("{0}{1}", desc, exptemp.ExceptionTemplateDescription);
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, Six0str, Three0str, Three0str);
                                            preflightExceptions.Add(CurrException);
                                        }
                                    }
                                    #endregion

                                }

                            }

                            if (show24Overlap)
                            {
                                if (Crew24List != null && Crew24List.Count > 0)
                                {
                                    List<GetTripOverlapforCrewParameter> OrderedList = new List<GetTripOverlapforCrewParameter>();

                                    OrderedList = (from overlap in Crew24List
                                                   orderby overlap.CrewCD, overlap.PreflightorPostflight, overlap.DorA, overlap.TripnumorLognum
                                                   select overlap).ToList();

                                    var CrewIDs = (from overlap in OrderedList
                                                   select overlap.CrewCD).Distinct().OrderBy(name => name).ToList();

                                    int incrementCount = 0;
                                    string increstr = string.Empty;
                                    foreach (string crewid in CrewIDs)
                                    {
                                        #region Preflight Depart
                                        var PreflightDeptTripnums = (from overlap in OrderedList
                                                                     where (overlap.PreflightorPostflight == "Preflight" && overlap.DorA == "D")
                                                                     select overlap.TripnumorLognum).Distinct().OrderBy(name => name).ToList();

                                        string DeptTripnumstr = string.Empty;
                                        if (PreflightDeptTripnums != null && PreflightDeptTripnums.Count > 0)
                                        {
                                            foreach (Int64 Tripnum in PreflightDeptTripnums)
                                                DeptTripnumstr = string.Format("{0} # {1}", DeptTripnumstr, Tripnum);


                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            StringBuilder desc = new StringBuilder();
                                            desc = desc.Append(CrewExtemp.ExceptionTemplateDescription);
                                            desc = desc.Replace("<crewcode>", crewid);
                                            desc = desc.Replace("<triporlog>", "Trip");
                                            desc = desc.Replace("<tripnum>", DeptTripnumstr);

                                            desc = desc.Replace("<trip>", " before this Trip ");
                                            //else
                                            //    desc = desc.Replace("<trip>", " after this trip ");
                                            CurrException.ExceptionDescription = desc.ToString();
                                            CurrException.ExceptionTemplateID = CrewExtemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = CrewExtemp;
                                            String lengthstr = Three0str;
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            String crewCode = string.Format("{0}{1}", crewid, Six0str);
                                            crewCode = crewCode.Substring(0, 6);
                                            increstr = incrementCount.ToString();
                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str007, increstr);
                                            preflightExceptions.Add(CurrException);

                                        }
                                        #endregion

                                        #region "Preflight Arrival"
                                        var PreflightArrTripnums = (from overlap in OrderedList
                                                                    where (overlap.PreflightorPostflight == "Preflight" && overlap.DorA == "A")
                                                                    select overlap.TripnumorLognum).Distinct().OrderBy(name => name).ToList();

                                        string ArrTripnumstr = string.Empty;
                                        if (PreflightArrTripnums != null && PreflightArrTripnums.Count > 0)
                                        {
                                            foreach (Int64 Tripnum in PreflightArrTripnums)
                                                ArrTripnumstr = string.Format("{0} # {1}", ArrTripnumstr, Tripnum);


                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            StringBuilder desc = new StringBuilder();
                                            desc = desc.Append(CrewExtemp.ExceptionTemplateDescription);
                                            desc = desc.Replace("<crewcode>", crewid);
                                            desc = desc.Replace("<triporlog>", "Trip");
                                            desc = desc.Replace("<tripnum>", ArrTripnumstr);
                                            desc = desc.Replace("<trip>", " after this Trip ");
                                            CurrException.ExceptionDescription = desc.ToString();
                                            CurrException.ExceptionTemplateID = CrewExtemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = CrewExtemp;
                                            String lengthstr = Three0str;
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            String crewCode = string.Format("{0}{1}", crewid, Six0str);
                                            crewCode = crewCode.Substring(0, 6);
                                            increstr = incrementCount.ToString();
                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str007, increstr);
                                            preflightExceptions.Add(CurrException);

                                        }
                                        #endregion "Preflight Arrival"


                                        #region Postflight Depart
                                        var PostflightDeptTripnums = (from overlap in OrderedList
                                                                      where (overlap.PreflightorPostflight == "PostFlight" && overlap.DorA == "D")
                                                                      select overlap.TripnumorLognum).Distinct().OrderBy(name => name).ToList();

                                        string DeptLognumstr = string.Empty;
                                        if (PostflightDeptTripnums != null && PostflightDeptTripnums.Count > 0)
                                        {
                                            foreach (Int64 Tripnum in PostflightDeptTripnums)
                                                DeptLognumstr = string.Format("{0} # {1}", DeptLognumstr, Tripnum);


                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            StringBuilder desc = new StringBuilder();
                                            desc = desc.Append(CrewExtemp.ExceptionTemplateDescription);
                                            desc = desc.Replace("<crewcode>", crewid);
                                            desc = desc.Replace("<triporlog>", "Log");
                                            desc = desc.Replace("<tripnum>", DeptLognumstr);

                                            desc = desc.Replace("<trip>", " before this Log ");
                                            //else
                                            //    desc = desc.Replace("<trip>", " after this trip ");
                                            CurrException.ExceptionDescription = desc.ToString();
                                            CurrException.ExceptionTemplateID = CrewExtemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = CrewExtemp;
                                            String lengthstr = Three0str;
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            String crewCode = string.Format("{0}{1}", crewid, Six0str);
                                            crewCode = crewCode.Substring(0, 6);
                                            increstr = incrementCount.ToString();
                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str007, increstr);
                                            preflightExceptions.Add(CurrException);

                                        }
                                        #endregion

                                        #region "Preflight Arr"
                                        var PostflightArrTripnums = (from overlap in OrderedList
                                                                     where (overlap.PreflightorPostflight == "PostFlight" && overlap.DorA == "A")
                                                                     select overlap.TripnumorLognum).Distinct().OrderBy(name => name).ToList();

                                        string ArrLognumstr = string.Empty;
                                        if (PostflightArrTripnums != null && PostflightArrTripnums.Count > 0)
                                        {
                                            foreach (Int64 Tripnum in PostflightArrTripnums)
                                                ArrLognumstr = string.Format("{0} # {1}", ArrLognumstr, Tripnum);


                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            StringBuilder desc = new StringBuilder();
                                            desc = desc.Append(CrewExtemp.ExceptionTemplateDescription);
                                            desc = desc.Replace("<crewcode>", crewid);
                                            desc = desc.Replace("<triporlog>", "Log");
                                            desc = desc.Replace("<tripnum>", ArrLognumstr);
                                            desc = desc.Replace("<trip>", " after this Log ");
                                            CurrException.ExceptionDescription = desc.ToString();
                                            CurrException.ExceptionTemplateID = CrewExtemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = CrewExtemp;
                                            String lengthstr = Three0str;
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            String crewCode = string.Format("{0}{1}", crewid, Six0str);
                                            crewCode = crewCode.Substring(0, 6);
                                            increstr = incrementCount.ToString();
                                            increstr = string.Format("{0}{1}", Three0str, increstr);
                                            increstr = increstr.Substring(increstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, crewCode, str007, increstr);
                                            preflightExceptions.Add(CurrException);

                                        }
                                        #endregion "Preflight Arr"

                                    }

                                }
                            }


                            #endregion
                        }

                        if (Preflegs != null && Preflegs.Count > 0)
                        {
                            //leg + paxcode + runn
                            //010 + 00MURI + 009 + 000
                            #region PAX Validation
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                foreach (ExceptionTemplate exptemp in PreflightPax_TemplateList.ToList())
                                {
                                    #region TripPax - Number of Passengers Exceeds Aircraft Seat Capacity
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.NumberofPassengersExceedsAircraftSeatCapacity))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (Preflegs[i].IsDeleted == false)
                                                {

                                                    if (Preflegs[i].ReservationAvailable == null || Preflegs[i].ReservationAvailable < 0)
                                                    {
                                                        if (Preflegs[i].PassengerTotal != null)
                                                        {
                                                            PreflightTripException CurrException = new PreflightTripException();
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            StringBuilder desc = new StringBuilder();
                                                            desc = desc.Append(exptemp.ExceptionTemplateDescription);
                                                            desc = desc.Replace("<PassengerCount>", Preflegs[i].PassengerTotal.ToString());
                                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                            if (Preflegs[i].SeatTotal > 0)
                                                                desc = desc.AppendFormat(" : {0}", Preflegs[i].SeatTotal.ToString());
                                                            else
                                                                desc = desc.Append(" : 0");
                                                            CurrException.ExceptionDescription = desc.ToString();
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}", lengthstr, Nine0str, Three0str);
                                                            preflightExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Pax Passport Does Not Exists for International Leg

                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.PAXNoPassportIntLeg))
                                    {
                                        if (Preflegs[i].PreflightPassengerLists != null && Preflegs[i].CheckGroup != null && Preflegs[i].CheckGroup.ToLower() != "dom")
                                        {

                                            foreach (PreflightPassengerList PrefPAXlist in Preflegs[i].PreflightPassengerLists.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                bool alreadyExists = false;

                                                if (PAXNoPassportPaxlists.Contains((long)PrefPAXlist.PassengerID))//&& (long)PrefCrewlist.PassportID
                                                {
                                                    alreadyExists = true;
                                                }

                                                if (alreadyExists == false)
                                                {


                                                    if (PrefPAXlist.PassportID == null || (PrefPAXlist.PassportID != null && (long)PrefPAXlist.PassportID == 0))
                                                    {
                                                        var PaxPassportList = Container.GetAllCrewPassport(null, PrefPAXlist.PassengerID, CustomerID).ToList();


                                                        PreflightTripException CurrException = new PreflightTripException();

                                                        IQueryable<Passenger> Passengers = preflightCompiledQuery.getPassenger(Container, (long)PrefPAXlist.PassengerID, CustomerID);
                                                        Passenger currentpax = new Passenger();
                                                        if (Passengers != null && Passengers.ToList().Count > 0)
                                                            currentpax = Passengers.ToList()[0];

                                                        //currentpax = (from Paxnopassport in Container.Passengers
                                                        //              where Paxnopassport.IsDeleted == false && Paxnopassport.PassengerRequestorID == PrefPAXlist.PassengerID
                                                        //select Paxnopassport).SingleOrDefault();
                                                        if (currentpax != null && (PaxPassportList == null || PaxPassportList != null && PaxPassportList.Count == 0))
                                                        {

                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplateDescription;
                                                            desc = desc.Replace("<paxcode>", currentpax.PassengerRequestorCD);
                                                            CurrException.ExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            String paxCode = string.Format("{0}{1}", currentpax.PassengerRequestorCD, Six0str);
                                                            paxCode = paxCode.Substring(0, 6);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, paxCode, str003, Three0str);
                                                            PAXNoPassportPaxlists.Add((long)PrefPAXlist.PassengerID);
                                                            preflightExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Passenger travelling on an international leg has not selected a Choice passport
                                    // Fix for #1325
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.PAXNoChoicePassportIntLeg))
                                    {
                                        if (Preflegs[i].PreflightPassengerLists != null && Preflegs[i].CheckGroup != null && Preflegs[i].CheckGroup.ToLower() != "dom")
                                        {
                                            foreach (PreflightPassengerList PrefPAXlist in Preflegs[i].PreflightPassengerLists.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                bool alreadyExists = false;

                                                if (PAXNoChoicePassportList.Contains((long)PrefPAXlist.PassengerID))
                                                    alreadyExists = true;

                                                if (alreadyExists == false)
                                                {
                                                    if (PrefPAXlist.PassportID == null || (PrefPAXlist.PassportID != null && (long)PrefPAXlist.PassportID == 0))
                                                    {
                                                        PreflightTripException CurrException = new PreflightTripException();

                                                        IQueryable<Passenger> Passengers = preflightCompiledQuery.getPassenger(Container, (long)PrefPAXlist.PassengerID, CustomerID);
                                                        Passenger currentpax = new Passenger();
                                                        if (Passengers != null && Passengers.ToList().Count > 0)
                                                            currentpax = Passengers.ToList()[0];

                                                        var PaxPassportList = Container.GetAllCrewPassport(null, PrefPAXlist.PassengerID, CustomerID).ToList();
                                                        var PaxPassportNoChoiceList = Container.GetAllCrewPassport(null, PrefPAXlist.PassengerID, CustomerID).Where(x => x.Choice == true).ToList();





                                                        if (PaxPassportList != null && PaxPassportList.Count > 0 && (PaxPassportNoChoiceList == null || (PaxPassportNoChoiceList != null && PaxPassportNoChoiceList.Count == 0)))
                                                        {
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.TripID = Trip.TripID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplateDescription;
                                                            desc = desc.Replace("<paxcode>", currentpax.PassengerRequestorCD);
                                                            CurrException.ExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            String paxCode = string.Format("{0}{1}", currentpax.PassengerRequestorCD, Six0str);
                                                            paxCode = paxCode.Substring(0, 6);
                                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, paxCode, str004, Three0str);

                                                            PAXNoChoicePassportList.Add((long)PrefPAXlist.PassengerID);
                                                            preflightExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Passenger Passport Expiry Date
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.PassengerPassportExpire))
                                    {
                                        if (UserPrincipal.Identity.Settings != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsAutoPassenger == true)
                                            {
                                                if (Preflegs[i].PreflightPassengerLists != null)
                                                {
                                                    bool alreadyExists = false;

                                                    //List<CrewPassengerPassport> crewPassengerPassportLists = new List<CrewPassengerPassport>();
                                                    foreach (PreflightPassengerList PrefPasslist in Preflegs[i].PreflightPassengerLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        if (PrefPasslist.IsDeleted == false)
                                                        {
                                                            if (PassengerPassportPaxlists.Contains((long)PrefPasslist.PassengerID))//&& (long)PrefCrewlist.PassportID
                                                            {
                                                                alreadyExists = true;
                                                            }

                                                            if (alreadyExists == false)
                                                            {                                                              
                                                                List<GetAllCrewPassport> PaxPassportList = Container.GetAllCrewPassport(null, PrefPasslist.PassengerID, CustomerID).ToList();

                                                                if (PaxPassportList != null && PaxPassportList.Count > 0)
                                                                {
                                                                    foreach (GetAllCrewPassport paxPassport in PaxPassportList)
                                                                    {
                                                                        var crewPassengerPassportLists = Container.GetPassportbyPassportId((long)paxPassport.PassportID, false).ToList();

                                                                        DateTime MaxDate = Convert.ToDateTime((from maxDate in Preflegs select maxDate.ArrivalGreenwichDTTM).Max());
                                                                        DateTime ExpiryDate = MaxDate.AddMonths(7);

                                                                        if (crewPassengerPassportLists.Count > 0)
                                                                        {
                                                                            if (crewPassengerPassportLists[0].PassportExpiryDT <= ExpiryDate || crewPassengerPassportLists[0].PassportExpiryDT <= MaxDate)
                                                                            {
                                                                                PreflightTripException CurrException = new PreflightTripException();
                                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                                CurrException.TripID = Trip.TripID;
                                                                                CurrException.CustomerID = CustomerID;
                                                                                StringBuilder desc = new StringBuilder();
                                                                                desc = desc.Append(exptemp.ExceptionTemplateDescription);

                                                                                desc = desc.Replace("<Name>", crewPassengerPassportLists[0].PassportHolderCD);
                                                                                if (crewPassengerPassportLists[0].PassportExpiryDT > DateTime.Now)
                                                                                    desc = desc.Replace("<Expire>", "is Expiring");
                                                                                else
                                                                                    desc = desc.Replace("<Expire>", "Expired");
                                                                                string passportDate = String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", crewPassengerPassportLists[0].PassportExpiryDT);
                                                                                desc = desc.AppendFormat(" on {0}", passportDate);
                                                                                CurrException.ExceptionDescription = desc.ToString();
                                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                                CurrException.ExceptionTemplate = exptemp;
                                                                                PassengerPassportPaxlists.Add((long)PrefPasslist.PassengerID);
                                                                                String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                                                lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                                                String Paxcode = string.Format("{0}{1}", crewPassengerPassportLists[0].PassportHolderCD, Six0str);
                                                                                Paxcode = Paxcode.Substring(0, 6);
                                                                                CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, Paxcode, str001, Three0str);
                                                                                preflightExceptions.Add(CurrException);
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Leg PAXConflicts
                                    if (exptemp.ExceptionTemplateID == (long)(PreflightExceptionCheckList.PaxConflicts))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsPassengerOlap != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsPassengerOlap == true)
                                            {
                                                if (Preflegs[i].PreflightPassengerLists != null && Preflegs[i].PreflightPassengerLists.Where(x => x.IsDeleted == false).ToList().Count() > 0)
                                                {
                                                    string PaxIdsString = string.Empty;
                                                    StringBuilder desc = new StringBuilder();
                                                    int paxListsCount = 0;
                                                    string PaxIds = string.Empty;
                                                    foreach (PreflightPassengerList PrefPaxlist in Preflegs[i].PreflightPassengerLists.Where(x => x.IsDeleted == false).ToList())
                                                    {
                                                        if (PrefPaxlist.IsDeleted == false)
                                                        {
                                                            PaxIds = PrefPaxlist.PassengerID.ToString();
                                                            PaxIdsString += string.Format("{0},", PaxIds);//pax1,pax2,pax3,
                                                        }
                                                    }

                                                    if (!string.IsNullOrEmpty(PaxIdsString))
                                                    {
                                                        PaxIdsString = PaxIdsString.Remove(PaxIdsString.Length - 1);
                                                        if (Preflegs[i].DepartureGreenwichDTTM != null && Preflegs[i].ArrivalGreenwichDTTM != null) // && PrefCrewlist.IsDeleted == false
                                                        {
                                                            DateTime LegDepartUTCDate = (DateTime)Preflegs[i].DepartureGreenwichDTTM;
                                                            DateTime LegArrivalUTCDate = (DateTime)Preflegs[i].ArrivalGreenwichDTTM;
                                                            var paxConflictsLists = Container.GetPaxConflicts(CustomerID, (DateTime)LegDepartUTCDate, (DateTime)LegArrivalUTCDate, PaxIdsString, (Int64)Trip.TripID).ToList();
                                                            if (paxConflictsLists.Count > 0)
                                                            {
                                                                paxListsCount = paxConflictsLists.Count;
                                                                int incrementCount = 0;
                                                                string increstr = string.Empty;
                                                                for (int listCount = 0; listCount < paxListsCount; listCount++)
                                                                {

                                                                    PreflightTripException CurrException = new PreflightTripException();
                                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                    CurrException.TripID = Trip.TripID;
                                                                    CurrException.CustomerID = CustomerID;
                                                                    CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                                                    desc = desc.Append(CurrException.ExceptionDescription);
                                                                    desc = desc.Replace("<paxcode>", paxConflictsLists[listCount].PassengerRequestorCD);
                                                                    desc = desc.AppendFormat(" for Trip {0}", paxConflictsLists[listCount].TripNUM);
                                                                    desc = desc.AppendFormat(" for Leg {0}", Preflegs[i].LegNUM);
                                                                    CurrException.ExceptionDescription = desc.ToString();
                                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                                    CurrException.ExceptionTemplate = exptemp;
                                                                    String lengthstr = string.Format("{0}{1}", Three0str, Preflegs[i].LegNUM.ToString());
                                                                    lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                                    String PaxCode = string.Format("{0}{1}", paxConflictsLists[listCount].PassengerRequestorCD, Six0str);
                                                                    PaxCode = PaxCode.Substring(0, 6);
                                                                    increstr = incrementCount.ToString();
                                                                    increstr = string.Format("{0}{1}", Three0str, increstr);
                                                                    increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                                    CurrException.DisplayOrder = string.Format("{0}{1}{2}{3}", lengthstr, PaxCode, str002, increstr);
                                                                    //PassengerPassportPaxlists.Add((long)PrefPaxlist.PassengerID);
                                                                    preflightExceptions.Add(CurrException);
                                                                    incrementCount = incrementCount + 1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                
                                foreach (ExceptionTemplate exptemp in PreflightFBO_TemplateList.ToList())
                                {
                                    foreach (PreflightFBOList item in Preflegs[i].PreflightFBOLists)
                                    {
                                        #region "Select Arrival FBO"

                                        if (exptemp.ExceptionTemplateID ==
                                            (long) (PreflightExceptionCheckList.LogisticArrivalFBOICAOId) &&
                                            item.IsArrivalFBO == true && item.IsDepartureFBO == false &&
                                            item.FBOID == null)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                            string desc = CurrException.ExceptionDescription;
                                            desc = desc.Replace("<legnum>", Convert.ToString(Preflegs[i].LegNUM));
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String lengthstr = string.Format("{0}{1}", Three0str,
                                                Preflegs[i].LegNUM.ToString());
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}", lengthstr, str002, i);
                                            preflightExceptions.Add(CurrException);
                                        }

                                        #endregion

                                        #region "Select Depature FBO"

                                        if (exptemp.ExceptionTemplateID ==
                                            (long)(PreflightExceptionCheckList.LogisticDepatureFBOICAOId) && item.IsDepartureFBO == true && item.IsArrivalFBO == false &&
                                            item.FBOID == null)
                                        {
                                            PreflightTripException CurrException = new PreflightTripException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.TripID = Trip.TripID;
                                            CurrException.CustomerID = CustomerID;
                                            CurrException.ExceptionDescription = exptemp.ExceptionTemplateDescription;
                                            string desc = CurrException.ExceptionDescription;
                                            desc = desc.Replace("<legnum>", Convert.ToString(Preflegs[i].LegNUM));
                                            CurrException.ExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String lengthstr = string.Format("{0}{1}", Three0str,
                                                Preflegs[i].LegNUM.ToString());
                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                            CurrException.DisplayOrder = string.Format("{0}{1}{2}", lengthstr,str002, i);
                                            preflightExceptions.Add(CurrException);
                                        }
                                        #endregion
                                    }

                                }
                            }
                            #endregion
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return preflightExceptions;

        }

        #region MinToTenths
        /// <summary>
        /// Method to Get Tenths Conversion Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <param name="isTenths">Pass Boolean Value</param>
        /// <param name="conversionType">Pass Conversion Type</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        private string ConvertMinToTenths(String time, Boolean isTenths, String conversionType)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time, isTenths, conversionType))
            {
                string result = "00.0";

                if (time.IndexOf(":") != -1)
                {
                    string[] timeArray = time.Split(':');
                    decimal hour = Convert.ToDecimal(timeArray[0]);
                    decimal minute = Convert.ToDecimal(timeArray[1]);

                    if (isTenths && (conversionType == "1" || conversionType == "2")) // Standard and Flightpak Conversion
                    {

                        List<StandardFlightpakConversion> StandardFlightpakConversionList = new List<StandardFlightpakConversion>();
                        StandardFlightpakConversionList = LoadStandardFPConversion();
                        //Session["StandardFlightpakConversion"];
                        var standardList = StandardFlightpakConversionList.ToList().Where(x => minute >= x.StartMinutes && minute <= x.EndMinutes && x.ConversionType.ToString() == conversionType).ToList();

                        if (standardList.Count > 0)
                        {
                            result = Convert.ToString(hour + standardList[0].Tenths);
                        }

                    }

                    else
                    {
                        decimal decimalOfMin = 0;
                        if (minute > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = minute / 60;

                        result = Convert.ToString(hour + decimalOfMin);
                    }
                }
                return result;
            }
        }
        #endregion

        #region TenthsToMins
        /// <summary>
        /// Method to Get Minutes from decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Tenths Converted Value</returns>
        
        #region StandardFlightpakConversion
        public List<StandardFlightpakConversion> LoadStandardFPConversion()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                List<StandardFlightpakConversion> conversionList = new List<StandardFlightpakConversion>();
                // Set Standard Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 5, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 6, EndMinutes = 11, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 12, EndMinutes = 17, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 18, EndMinutes = 23, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 24, EndMinutes = 29, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 30, EndMinutes = 35, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 36, EndMinutes = 41, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 42, EndMinutes = 47, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 48, EndMinutes = 53, ConversionType = 1 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 54, EndMinutes = 59, ConversionType = 1 });

                // Set Flightpak Conversions
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.0M, StartMinutes = 0, EndMinutes = 2, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.1M, StartMinutes = 3, EndMinutes = 8, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.2M, StartMinutes = 9, EndMinutes = 14, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.3M, StartMinutes = 15, EndMinutes = 21, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.4M, StartMinutes = 22, EndMinutes = 27, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.5M, StartMinutes = 28, EndMinutes = 32, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.6M, StartMinutes = 33, EndMinutes = 39, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.7M, StartMinutes = 40, EndMinutes = 44, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.8M, StartMinutes = 45, EndMinutes = 50, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 0.9M, StartMinutes = 51, EndMinutes = 57, ConversionType = 2 });
                conversionList.Add(new StandardFlightpakConversion() { Tenths = 1.0M, StartMinutes = 58, EndMinutes = 59, ConversionType = 2 });

                return conversionList;
            }
        }
        #endregion
        #endregion
        #endregion

        public ReturnValue<ExceptionTemplate> GetExceptionTemplateList()
        {
            ReturnValue<ExceptionTemplate> ret = new ReturnValue<ExceptionTemplate>();
            PreflightDataModelContainer cs;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new PreflightDataModelContainer();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    List<ExceptionTemplate> BusinessErrors = cs.GetExceptionTemplate((long)PreflightExceptionModule.Preflight).ToList();
                    //List<ExceptionTemplate> BusinessErrors = (from Exc in cs.ExceptionTemplates
                    //                                          where (Exc.ModuleID == (long)PreflightExceptionModule.Preflight)
                    //                                          select Exc).ToList();


                    ret.EntityList = BusinessErrors;

                    if (ret.EntityList != null && ret.EntityList.Count > 0)

                        ret.ReturnFlag = true;
                    else
                        ret.ReturnFlag = false;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public void CurrentException(ref PreflightTripException preflightTripExcep, ExceptionTemplate exceptionTemplate, PreflightMain Trip)
        {
            preflightTripExcep.TripID = Trip.TripID;
            preflightTripExcep.CustomerID = CustomerID;
            preflightTripExcep.ExceptionDescription = exceptionTemplate.ExceptionTemplateDescription;
            preflightTripExcep.ExceptionTemplateID = exceptionTemplate.ExceptionTemplateID;
            preflightTripExcep.LastUpdUID = UserPrincipal.Identity.Name;
            preflightTripExcep.LastUpdTS = DateTime.UtcNow;
            preflightTripExcep.ExceptionTemplate = exceptionTemplate;

        }

        #region "Dispatcher"
        public ReturnValue<GetDispatcherList> GetDispatcherList()
        {
            ReturnValue<GetDispatcherList> ret = new ReturnValue<GetDispatcherList>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetDispatcherList(CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }
        #endregion

        #region "Pax"
        public ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreFlightAvailablePaxList()
        {
            ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllPreFlightAvailablePaxList(CustomerID, ClientId).ToList();
                        foreach (var item in ret.EntityList)
                        {
                            item.Passport = Crypting.Decrypt(item.Passport);
                        }
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetPreFlightAvailablePaxListByIDOrCD(long PassengerRequestorID, string PassengerRequestorCD)
        {
            ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetPreFlightAvailablePaxListByIDOrCD(CustomerID, ClientId, PassengerRequestorID, PassengerRequestorCD).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> GetAllPreflightAvailablePaxListForPaxTab()
        {
            ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllPreFlightAvailablePaxList>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllPreFlightAvailablePaxList(CustomerID, ClientId).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary> GetAllPreFlightPaxSummary(long tripID)
        {
            ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAllPreFlightPaxSummary>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllPreFlightPaxSummary(CustomerID, tripID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        #endregion

        #region "UWA Interface"

        #region ATS Submit

        public ReturnValue<PreflightMain> GetUWAL(Int64 TripID, bool isUWASubmit)
        {
            ReturnValue<PreflightMain> ret = null;
            PreflightDataModelContainer cs = null;
            PreflightMain prefMain = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID, isUWASubmit))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<PreflightMain>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        prefMain = new PreflightMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;


                        ret.EntityList = cs.GetTripByTripID(CustomerID, TripID).ToList();

                        //ret.EntityList = (from trips in cs.PreflightMains
                        //                  where (trips.TripID == TripID)
                        //                  select trips).ToList();
                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {
                            prefMain = ret.EntityList[0];
                            if (isUWASubmit)
                                LoadTripProperties(ref prefMain, ref cs);
                            LoadUWALProperties(ref prefMain, ref cs, isUWASubmit);
                            ret.EntityList[0] = prefMain;
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            prefMain = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }


            return ret;
        }

        public ReturnValue<PreflightMain> UpdateUWAL(PreflightMain prefMain)
        {
            ReturnValue<PreflightMain> ReturnValue = null;
            PreflightDataModelContainer Container = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(prefMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new PreflightDataModelContainer())
                        {
                            ReturnValue = new ReturnValue<PreflightMain>();

                            foreach (var leg in prefMain.PreflightLegs.ToList())
                            {
                                PreflightLeg PreflightLegentity = Container.PreflightLegs.Where(p => p.LegID == leg.LegID).SingleOrDefault();

                                //Insert to UWAL Table
                                foreach (var uwalEnt in leg.UWALs.ToList())
                                {
                                    UWAL uwalOrg = Container.UWALs.Where(x => x.LegID == uwalEnt.LegID).SingleOrDefault();
                                    if (uwalEnt.UWALID != 0)
                                    {
                                        Container.UWALs.Attach(uwalOrg);
                                        Container.UWALs.ApplyCurrentValues(uwalEnt);
                                    }
                                    else
                                    {
                                        PreflightLegentity.UWALs.Add(uwalEnt);
                                        Container.ObjectStateManager.ChangeObjectState(uwalEnt, EntityState.Added);
                                    }
                                }
                                //Delete the existing records
                                DeleteUWASpServ(leg.LegID);
                                //Insert to UWASpServ Table
                                foreach (var uwalSpEnt in leg.UWASpServs.ToList())
                                {

                                    PreflightLegentity.UWASpServs.Add(uwalSpEnt);
                                    Container.ObjectStateManager.ChangeObjectState(uwalSpEnt, EntityState.Added);
                                }

                                //Insert to UWAPermit Table
                                foreach (var uwaPermitEnt in leg.UWAPermits.ToList())
                                {
                                    if (uwaPermitEnt.State == TripEntityState.Added)
                                    {
                                        PreflightLegentity.UWAPermits.Add(uwaPermitEnt);
                                        Container.ObjectStateManager.ChangeObjectState(uwaPermitEnt, EntityState.Added);
                                    }
                                    else if (uwaPermitEnt.State == TripEntityState.Modified)
                                    {
                                        UWAPermit uwaPermitOrg = Container.UWAPermits.Where(x => x.UWAPermit1 == uwaPermitEnt.UWAPermit1).SingleOrDefault();
                                        Container.UWAPermits.Attach(uwaPermitOrg);
                                        Container.UWAPermits.ApplyCurrentValues(uwaPermitEnt);
                                    }
                                    else if (uwaPermitEnt.State == TripEntityState.Deleted)
                                    {
                                        UWAPermit uwaPermitOrg = Container.UWAPermits.Where(x => x.UWAPermit1 == uwaPermitEnt.UWAPermit1).SingleOrDefault();
                                        //PreflightLegentity.UWAPermits.Remove(uwaPermitOrg);
                                        Container.UWAPermits.DeleteObject(uwaPermitOrg);
                                        //Container.ObjectStateManager.ChangeObjectState(uwaPermitEnt, EntityState.Deleted);
                                    }

                                }
                            }
                            Container.SaveChanges();
                            LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                            lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, prefMain.TripID);
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock

                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;

        }

        public ReturnValue<UWASpServ> DeleteUWASpServ(Int64 LegID)
        {
            ReturnValue<UWASpServ> ReturnValue = null;
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<UWASpServ>();
                    using (Container = new Data.Preflight.PreflightDataModelContainer())
                    {
                        Container.DeleteUWASpServ(LegID);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;

        }

        public void LoadUWALProperties(ref PreflightMain prefMain, ref PreflightDataModelContainer cs, bool isUWASubmit)
        {
            //cs.LoadProperty(prefMain, "UWATSSMains");
            cs.LoadProperty(prefMain, c => c.UWATSSMains);
            if (!isUWASubmit)
                cs.LoadProperty(prefMain, c => c.PreflightLegs);
            //cs.LoadProperty(prefMain, "PreflightLegs");
            foreach (PreflightLeg prefLeg in prefMain.PreflightLegs.ToList())
            {
                if (prefLeg.IsDeleted)
                    prefMain.PreflightLegs.Remove(prefLeg);
                cs.LoadProperty(prefLeg, c => c.UWALs);
                cs.LoadProperty(prefLeg, c => c.UWASpServs);
                cs.LoadProperty(prefLeg, c => c.UWAPermits);

                //cs.LoadProperty(prefLeg, "UWALs");
                //cs.LoadProperty(prefLeg, "UWASpServs");
                //cs.LoadProperty(prefLeg, "UWAPermits");
            }
        }

        public ReturnValue<Data.Preflight.GetUWACrewPassportVisa_Result> GetUWACrewPassportVisa(Int64 LegID, Int64 CustomerID)
        {
            ReturnValue<Data.Preflight.GetUWACrewPassportVisa_Result> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegID, CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        ret = new ReturnValue<GetUWACrewPassportVisa_Result>();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetUWACrewPassportVisa(LegID, CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;

        }

        public ReturnValue<Data.Preflight.GetUWAPassengerPassportVisa_Result> GetUWAPassengerPassportVisa(Int64 LegID, Int64 CustomerID)
        {
            ReturnValue<Data.Preflight.GetUWAPassengerPassportVisa_Result> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LegID, CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        ret = new ReturnValue<GetUWAPassengerPassportVisa_Result>();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetUWAPassengerPassportVisa(LegID, CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<List<string>> TSSSubmit(Int64 TripID, string UWATripID, bool isManualSubmit, string RequesterAppname)
        {
            ReturnValue<List<string>> objMain = null;
            PreflightMain Trip = null;
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID, UWATripID, isManualSubmit))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objMain = new ReturnValue<List<string>>();
                    Trip = new PreflightMain();
                    using (Container = new PreflightDataModelContainer())
                    {
                        var vUWALTrip = GetUWAL(TripID, true);
                        if (vUWALTrip != null && vUWALTrip.ReturnFlag == true)
                        {
                            Trip = vUWALTrip.EntityList[0];
                            if (!isManualSubmit)
                            {
                                UWAManager objUWAManager = new UWAManager();
                                objMain = objUWAManager.TSSSubmit(Trip, RequesterAppname, UserName);
                            }
                            else
                            {
                                objMain.ReturnFlag = true;
                                List<string> lst = new List<string>();
                                lst.Add(UWATripID);
                                objMain.EntityInfo = lst;
                            }
                            if (objMain.ReturnFlag == true && objMain.EntityInfo != null)
                            {
                                //Trip.UWATSSMains = new List<UWATSSMain>();                    
                                string uwaTripID = objMain.EntityInfo[0].ToString();
                                UWATSSMain UWATSSMainentity = Container.UWATSSMains.Where(p => p.TripID == Trip.TripID).SingleOrDefault();
                                UWATSSMain uwaTSSMain = new UWATSSMain();
                                uwaTSSMain.UWATripID = uwaTripID;
                                uwaTSSMain.CustomerID = Trip.CustomerID;
                                uwaTSSMain.FleetID = Trip.FleetID;
                                uwaTSSMain.EstDepartureDT = Trip.EstDepartureDT;
                                uwaTSSMain.TripDestination = Trip.HomeBaseAirportICAOID;
                                uwaTSSMain.RequestDT = DateTime.Now.Date;
                                uwaTSSMain.TripID = Trip.TripID;
                                uwaTSSMain.TripRequestID = UserPrincipal.Identity.FirstName + " " + UserPrincipal.Identity.MiddleName + " " + UserPrincipal.Identity.LastName;
                                uwaTSSMain.LastUpdUID = UserPrincipal.Identity.Name;
                                uwaTSSMain.LastUptTS = DateTime.UtcNow;
                                if (!isManualSubmit)
                                    uwaTSSMain.TripStatusLastRetrieveDT = Convert.ToDateTime(objMain.EntityInfo[1].ToString()); //TripRetriveTS                            

                                if (UWATSSMainentity != null)
                                {
                                    uwaTSSMain.TripReqLastSubmitDT = UWATSSMainentity.RequestDT;
                                    UWATSSMain UWATSSMainOrg = Container.UWATSSMains.Where(p => p.TripID == Trip.TripID).SingleOrDefault();
                                    Container.UWATSSMains.Attach(UWATSSMainOrg);
                                    Container.UWATSSMains.ApplyCurrentValues(UWATSSMainentity);
                                }
                                else
                                {
                                    Container.UWATSSMains.AddObject(uwaTSSMain);
                                    Container.ObjectStateManager.ChangeObjectState(uwaTSSMain, EntityState.Added);
                                }
                                Container.SaveChanges();
                                objMain.ReturnFlag = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objMain;
        }

        public ReturnValue<List<string>> TSSCheckStatus(String UWATripID, string RequesterAppname)
        {
            ReturnValue<List<string>> objMain = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UWATripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objMain = new ReturnValue<List<string>>();

                    UWAManager objUWAManager = new UWAManager();
                    objMain = objUWAManager.TSSCheckStatus(UWATripID, RequesterAppname);

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objMain;
        }

        #endregion

        #region EAPIS

        public ReturnValue<PreflightMain> GetAPIS(Int64 TripID)
        {
            ReturnValue<PreflightMain> ret = null;
            PreflightDataModelContainer cs = null;
            PreflightMain prefMain = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<PreflightMain>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        prefMain = new PreflightMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetTripByTripID(CustomerID, TripID).ToList();
                        //(from trips in cs.PreflightMains
                        //              where (trips.TripID == TripID)
                        //              select trips).ToList();
                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {
                            prefMain = ret.EntityList[0];

                            LoadTripProperties(ref prefMain, ref cs);
                            LoadUWAAPISProperties(ref prefMain, ref cs);


                            ret.EntityList[0] = prefMain;
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            prefMain = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }


            return ret;
        }

        public void LoadUWAAPISProperties(ref PreflightMain prefMain, ref PreflightDataModelContainer cs)
        {

            if (prefMain.Fleet.EmergencyContactID != null) cs.LoadProperty(prefMain.Fleet, c => c.EmergencyContact);

            foreach (PreflightLeg prefLeg in prefMain.PreflightLegs)
            {
                foreach (PreflightCrewList prefCrew in prefLeg.PreflightCrewLists)
                {
                    cs.LoadProperty(prefCrew, c => c.Crew);
                }


                foreach (PreflightPassengerList prefPass in prefLeg.PreflightPassengerLists)
                {
                    cs.LoadProperty(prefPass, c => c.Passenger);
                }
            }
            // if (prefMain.CrewID != null) cs.LoadProperty(prefMain.Crew, c=> c.CrewPassengerPassport);
        }

        public ReturnValue<List<string>> EAPISSubmit(Int64 TripID, List<string> lstLegs, string Domainname)
        {
            ReturnValue<List<string>> objMain = null;
            PreflightMain Trip = null;
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID, lstLegs))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objMain = new ReturnValue<List<string>>();
                    Trip = new PreflightMain();
                    using (Container = new PreflightDataModelContainer())
                    {
                        var vEAPISTrip = GetAPIS(TripID);
                        if (vEAPISTrip != null && vEAPISTrip.ReturnFlag == true)
                        {
                            Trip = vEAPISTrip.EntityList[0];
                            UWAManager objUWAManager = new UWAManager();
                            StringBuilder sbAPISExceptions = new System.Text.StringBuilder();
                            //int ActualLegcount = Convert.ToInt32(Trip.PreflightLegs.Count);
                            //int Validlegs = 0;
                            foreach (PreflightLeg prefLeg in Trip.PreflightLegs)
                            {
                                foreach (string sPrefLeg in lstLegs)
                                {
                                    if (sPrefLeg == prefLeg.LegID.ToString())
                                    {
                                        String sBound = string.Empty;
                                        long CountryIDX = -1;
                                        FlightPak.Data.MasterCatalog.Country CountryService = new Data.MasterCatalog.Country();
                                        FlightPak.Data.MasterCatalog.MasterDataContainer FPMasterContainer = new Data.MasterCatalog.MasterDataContainer();
                                        var objCountryVal = FPMasterContainer.GetAllCountry().Where((x => x.CountryCD.TrimEnd() == "US")).ToList();
                                        if (objCountryVal.Count > 0)
                                        {
                                            CountryIDX = ((FlightPak.Data.MasterCatalog.Country)objCountryVal[0]).CountryID;
                                        }
                                        if (prefLeg.Airport.CountryID == CountryIDX)
                                            sBound = "Arrival";
                                        else
                                            sBound = "Departure";

                                        objMain = objUWAManager.EAPISSubmit(Trip, prefLeg, sBound, Domainname, UserName);

                                        if (objMain.ReturnFlag == true && objMain.EntityInfo != null)
                                        {
                                            //Validlegs = Validlegs + 1;
                                            string EAPISID = objMain.EntityInfo[0].ToString();
                                            PreflightLeg PrefLegEntity = Container.PreflightLegs.Where(p => p.LegID == prefLeg.LegID && p.TripID == prefLeg.TripID && p.CustomerID == prefLeg.CustomerID).SingleOrDefault();
                                            if (prefLeg.UWAID != null && prefLeg.UWAID != string.Empty)
                                            {
                                                PrefLegEntity.UWAID = EAPISID;
                                                PrefLegEntity.CustomerID = prefLeg.CustomerID;

                                                if (PrefLegEntity != null)
                                                {
                                                    //Container.PreflightLegs.Attach(PrefLegEntity);
                                                    Container.ObjectStateManager.ChangeObjectState(PrefLegEntity, System.Data.EntityState.Modified);

                                                    // Container.PreflightLegs.ApplyCurrentValues(eapisPrefLeg);
                                                }
                                                Container.SaveChanges();

                                                /*When ever the Leg is submitted, 
                                                 * we need to set the APIS Submit Date
                                                and If all the legs are valid and got UWAID,
                                                 * we need to set APIS Exception to empty 
                                                 * and set the Status to "Submitted"
                                                 * and IaAPisValid to True In the Preflightmain table.
                                                  */
                                                PreflightMain PrefMainEntity = Container.PreflightMains.Where(p => p.TripID == prefLeg.TripID && p.TripID == prefLeg.TripID && p.CustomerID == prefLeg.CustomerID).SingleOrDefault();
                                                //if (ActualLegcount == Validlegs)
                                                //{
                                                PrefMainEntity.APISException = string.Empty;
                                                PrefMainEntity.APISStatus = "Submitted";
                                                PrefMainEntity.IsAPISValid = true;
                                                //}
                                                PrefMainEntity.CustomerID = Trip.CustomerID;
                                                PrefMainEntity.TripID = Trip.TripID;
                                                PrefMainEntity.APISSubmit = DateTime.Now;
                                                if (PrefMainEntity != null)
                                                {
                                                    Container.ObjectStateManager.ChangeObjectState(PrefMainEntity, System.Data.EntityState.Modified);
                                                }
                                                Container.SaveChanges();
                                            }
                                            objMain.ReturnFlag = true;
                                        }
                                        else
                                        {
                                            PreflightMain PrefMainEntity = Container.PreflightMains.Where(p => p.TripID == prefLeg.TripID && p.TripID == prefLeg.TripID && p.CustomerID == prefLeg.CustomerID).SingleOrDefault();
                                            if (objMain.EntityInfo.Count > 0)
                                            {
                                                foreach (string str in objMain.EntityInfo)
                                                {
                                                    sbAPISExceptions.Append(Environment.NewLine);
                                                    sbAPISExceptions.Append(str);
                                                }
                                            }
                                            PrefMainEntity.APISException = sbAPISExceptions.ToString();
                                            PrefMainEntity.IsAPISValid = false;
                                            PrefMainEntity.CustomerID = Trip.CustomerID;
                                            PrefMainEntity.TripID = Trip.TripID;
                                            PrefMainEntity.APISSubmit = DateTime.Now;

                                            if (PrefMainEntity != null)
                                            {
                                                Container.ObjectStateManager.ChangeObjectState(PrefMainEntity, System.Data.EntityState.Modified);
                                            }
                                            Container.SaveChanges();
                                            objMain.ReturnFlag = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objMain;
        }

        public void UpdateAPISException(Int64 TripID, string APISException)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    var resultSet = Container.UpdateAPISException(CustomerID, UserName, TripID, APISException, DateTime.Now);

                }, FlightPak.Common.Constants.Policy.BusinessLayer);
            }
        }

        #endregion

        #region FUEL

        public ReturnValue<PreflightFuel> GetFuelPrices(List<string> ICAOID, long AirportID, string userName, string password)
        {
            ReturnValue<PreflightFuel> objMain = null;
            List<PreflightFuel> uwaPFFuelLst = null;
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ICAOID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    objMain = new ReturnValue<PreflightFuel>();
                    uwaPFFuelLst = new List<PreflightFuel>().ToList();
                    UWAManager objUWAManager = new UWAManager();
                    Container = new PreflightDataModelContainer();

                    IQueryable<UserMaster> Usermasters = preflightCompiledQuery.getUser(Container, UserName, CustomerID);
                    UserMaster LoggedInUser = new UserMaster();
                    if (Usermasters != null && Usermasters.ToList().Count > 0)
                        LoggedInUser = Usermasters.ToList()[0];
                    if (LoggedInUser != null && !string.IsNullOrWhiteSpace(LoggedInUser.UVTripLinkID))
                        objMain = objUWAManager.GetFuelPrice(ICAOID, LoggedInUser.UVTripLinkID, LoggedInUser.UVTripLinkPassword);
                    else
                        objMain = objUWAManager.GetFuelPrice(ICAOID, userName, password);


                    if (objMain.ReturnFlag)
                    {
                        //Delete existing data before insert.
                        //IQueryable<PreflightFuel> PreflightFuels = preflightCompiledQuery.getPreflightFuel(Container, AirportID, UserPrincipal.Identity.SessionID);
                        //if (PreflightFuels != null && PreflightFuels.ToList().Count > 0)
                        //    uwaPFFuelLst = PreflightFuels.ToList();

                        uwaPFFuelLst = Container.PreflightFuels.Where(p => p.UWAFuelInterfaceRequestID != null && p.UWAFuelInterfaceRequestID == UserPrincipal.Identity.SessionID && p.AirportID == AirportID).ToList();
                        if (uwaPFFuelLst.Count > 0)
                        {
                            foreach (PreflightFuel fp in uwaPFFuelLst)
                            {
                                Container.ObjectStateManager.ChangeObjectState(fp, System.Data.EntityState.Deleted);                                
                            }
                            // Apply Delete operation ondatabase side
                            try
                            {
                                Container.SaveChanges();                                
                            }
                            catch (OptimisticConcurrencyException ec)
                            {
                                Container.Refresh(RefreshMode.StoreWins, uwaPFFuelLst);
                                Container.SaveChanges();
                            }
                        }
                       
                        //uwaPFFuelLst = objMain.EntityList;
                        foreach (PreflightFuel uwaFPFuel in objMain.EntityList)
                        {
                            PreflightFuel preffuel = new PreflightFuel();

                            preffuel.CustomerID = UserPrincipal.Identity.CustomerID;
                            preffuel.AirportID = Convert.ToInt64(AirportID);
                            preffuel.LastUpdTS = DateTime.UtcNow;
                            preffuel.LastUpdUID = UserPrincipal.Identity.Name;
                            preffuel.UpdateDTTM = DateTime.UtcNow;
                            preffuel.UWAFuelInterfaceRequestID = UserPrincipal.Identity.SessionID;
                            preffuel.IsDeleted = false;
                            preffuel.PreflightFuelID = Guid.NewGuid();
                            preffuel.FBOName = uwaFPFuel.FBOName;
                            preffuel.GallonFrom = uwaFPFuel.GallonFrom;
                            preffuel.GallonTo = uwaFPFuel.GallonTo;
                            preffuel.EffectiveDT = uwaFPFuel.EffectiveDT;
                            preffuel.UnitPrice = uwaFPFuel.UnitPrice;

                            //uwaFPFuel.CustomerID = UserPrincipal.Identity.CustomerID;
                            //uwaFPFuel.AirportID = Convert.ToInt64(AirportID);
                            //uwaFPFuel.LastUpdTS = DateTime.UtcNow;
                            //uwaFPFuel.LastUpdUID = UserPrincipal.Identity.Name;
                            //uwaFPFuel.UpdateDTTM = DateTime.UtcNow;
                            //uwaFPFuel.UWAFuelInterfaceRequestID = UserPrincipal.Identity.SessionID;
                            //uwaFPFuel.IsDeleted = false;

                            //Container.PreflightFuels.AddObject(preffuel);
                            // uwaPFFuelLst.Add(uwaFPFuel);
                            Container.AddToPreflightFuels(preffuel);
                            Container.ObjectStateManager.ChangeObjectState(preffuel, System.Data.EntityState.Added);
                            

                        }
                        try
                        {
                            Container.SaveChanges();
                            
                        }
                        catch (OptimisticConcurrencyException ec)
                        {
                            Container.Refresh(RefreshMode.ClientWins, Container.PreflightFuels);
                            Container.Refresh(RefreshMode.ClientWins, uwaPFFuelLst);
                            Container.SaveChanges();
                        }
                        objMain.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objMain;
        }

        #endregion

        #endregion

        #region Fuel
        public ReturnValue<FlightPakFuelData> GetFlightPakFuelData(Int64 TripID, Int64 DepatureICAOID)
        {
            ReturnValue<Data.Preflight.FlightPakFuelData> ret = new ReturnValue<Data.Preflight.FlightPakFuelData>();
            PreflightDataModelContainer cs = new PreflightDataModelContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID, DepatureICAOID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.FlightPakFuelData>();
                    ret.ReturnFlag = false;
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetFlightPakFuelData(TripID, DepatureICAOID, CustomerID, UserPrincipal.Identity.SessionID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }


        public ReturnValue<FBOBestPrice_Result> GetFBOBestPrice(Int64 TripID, Int64 DepatureICAOID, decimal Gallon)
        {
            ReturnValue<Data.Preflight.FBOBestPrice_Result> ret = null;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID, DepatureICAOID, Gallon))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.FBOBestPrice_Result>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetFBOBestPrice(TripID, DepatureICAOID, Gallon, UserPrincipal.Identity.SessionID, CustomerID).ToList();

                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }


        public bool UpdatePreflightFuelDetails(Int64 TripID)
        {

            bool DataUpdated = false;
            PreflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.UpdatePreflightFuelDetails((long)TripID, CustomerID, UserPrincipal.Identity.SessionID);
                        DataUpdated = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

                return DataUpdated;
            }

        }

        #endregion

        #region "SIFL"

        public ReturnValue<Data.Preflight.GetSIFLPassengerInfo> GetSIFLPassengerInfo(long tripID)
        {

            ReturnValue<Data.Preflight.GetSIFLPassengerInfo> ret = null;
            PreflightDataModelContainer cs = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, tripID))
            {
                ret = new ReturnValue<Data.Preflight.GetSIFLPassengerInfo>();
                using (cs = new PreflightDataModelContainer())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    exManager.Process(() =>
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetSIFLPassengerInfo(CustomerID, tripID).ToList();

                        ret.ReturnFlag = true;
                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
            }

            return ret;
        }

        public ReturnValue<Data.Preflight.GetAllPreflightTripSIFL> GetAllPreflightTripSIFL(long TripID)
        {
            ReturnValue<Data.Preflight.GetAllPreflightTripSIFL> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, TripID))
            {
                ret = new ReturnValue<Data.Preflight.GetAllPreflightTripSIFL>();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllPreflightTripSIFL(TripID, CustomerID).ToList();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Preflight.GetSIFLPassengerEmployeeType> GetSIFLPassengerEmployeeType(long passengerRequestorID)
        {
            ReturnValue<Data.Preflight.GetSIFLPassengerEmployeeType> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, passengerRequestorID))
            {
                ret = new ReturnValue<Data.Preflight.GetSIFLPassengerEmployeeType>();

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetSIFLPassengerEmployeeType(CustomerID, passengerRequestorID).ToList();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Preflight.CheckPreflightTripSIFL> CheckPreflightTripSIFL(long passengerRequestorID, long departIcaoID, long arrivalIcaoID)
        {
            ReturnValue<Data.Preflight.CheckPreflightTripSIFL> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID, passengerRequestorID))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.CheckPreflightTripSIFL>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.CheckPreflightTripSIFL(CustomerID, passengerRequestorID, departIcaoID, arrivalIcaoID).ToList();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.Preflight.GetPreflightTripSIFLID> GetPreflightTripSIFLID()
        {
            ReturnValue<Data.Preflight.GetPreflightTripSIFLID> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetPreflightTripSIFLID>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetPreflightTripSIFLID(CustomerID).ToList();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.Preflight.GetAssociatePassengerID> GetAssociatePassengerID(long PassengerRequestorID)
        {
            ReturnValue<Data.Preflight.GetAssociatePassengerID> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetAssociatePassengerID>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAssociatePassengerID(PassengerRequestorID, CustomerID).ToList();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.Preflight.GetPassenger> GetPassengerbyIDOrCD(long PassengerRequestorID, string PassengerRequestorCD)
        {
            ReturnValue<Data.Preflight.GetPassenger> ret = null;
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Preflight.GetPassenger>();
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;
                        ret.EntityList = cs.GetPassenger(CustomerID, clientID, PassengerRequestorID, PassengerRequestorCD).ToList();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<PreflightTripSIFL> GetSIFL(Int64 SIFLID)
        {
            ReturnValue<PreflightTripSIFL> ret = new ReturnValue<PreflightTripSIFL>();
            PreflightDataModelContainer cs = null;
            PreflightTripSIFL prefSifl = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SIFLID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        prefSifl = new PreflightTripSIFL();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        //long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;

                        //if (clientID > 0)
                        //{

                        IQueryable<PreflightTripSIFL> PreflightTripSIFLs = preflightCompiledQuery.getPreflightTripSIFL(cs, SIFLID, CustomerID);
                        PreflightTripSIFL OldPreflightTripSIFL = new PreflightTripSIFL();
                        if (PreflightTripSIFLs != null && PreflightTripSIFLs.ToList().Count > 0)
                            ret.EntityList = PreflightTripSIFLs.ToList();

                        //ret.EntityList = (from sifls in cs.PreflightTripSIFLs
                        //                  where ((sifls.PreflightTripSIFLID == SIFLID) && (sifls.CustomerID == CustomerID))
                        //                  select sifls).ToList();
                        //}
                        //else
                        //{
                        //    ret.EntityList = (from sifls in cs.PreflightTripSIFLs
                        //                      where ((sifls.PreflightTripSIFLID == SIFLID) && (sifls.CustomerID == CustomerID))
                        //                      select sifls).ToList();
                        //}

                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {
                            prefSifl = ret.EntityList[0];
                            //LoadTripProperties(ref prefMain, ref cs);
                            ret.EntityList[0] = prefSifl;
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            prefSifl = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<PreflightTripSIFL> UpdatePreflightTripSIFL(PreflightTripSIFL sifl)
        {
            ReturnValue<PreflightTripSIFL> ReturnValue = new ReturnValue<PreflightTripSIFL>();
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sifl))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new PreflightDataModelContainer())
                        {

                            sifl.CustomerID = CustomerID;

                            sifl.LastUpdTS = DateTime.UtcNow;
                            sifl.LastUpdUID = UserPrincipal.Identity.Name;

                            var SiflId = sifl.PreflightTripSIFLID;

                            if (sifl.State == TripEntityState.Modified)
                            {
                                IQueryable<PreflightTripSIFL> PreflightTripSIFLs = preflightCompiledQuery.getPreflightTripSIFL(Container, SiflId, CustomerID);
                                PreflightTripSIFL PreflightTripSiflEntity = new PreflightTripSIFL();
                                if (PreflightTripSIFLs != null && PreflightTripSIFLs.ToList().Count > 0)
                                    PreflightTripSiflEntity = PreflightTripSIFLs.ToList()[0];

                                //PreflightTripSIFL PreflightTripSiflEntity = Container.PreflightTripSIFLs.Where(s => s.PreflightTripSIFLID == SiflId).FirstOrDefault();
                                if (PreflightTripSiflEntity != null)
                                {
                                    Container.PreflightTripSIFLs.Attach(PreflightTripSiflEntity);
                                }
                                Container.PreflightTripSIFLs.ApplyCurrentValues(sifl);
                            }
                            else if (sifl.State == TripEntityState.Added)
                            {
                                //sifl.IsDeleted = false;                            
                                Container.PreflightTripSIFLs.AddObject(sifl);
                            }
                            else if (sifl.State == TripEntityState.Deleted)
                            {
                                IQueryable<PreflightTripSIFL> PreflightTripSIFLs = preflightCompiledQuery.getPreflightTripSIFL(Container, SiflId, CustomerID);
                                PreflightTripSIFL PreflightTripSiflEntity = new PreflightTripSIFL();
                                if (PreflightTripSIFLs != null && PreflightTripSIFLs.ToList().Count > 0)
                                    PreflightTripSiflEntity = PreflightTripSIFLs.ToList()[0];
                                //PreflightTripSIFL PreflightTripSiflEntity = Container.PreflightTripSIFLs.Where(s => s.PreflightTripSIFLID == SiflId).FirstOrDefault();
                                if (PreflightTripSiflEntity != null)
                                {
                                    PreflightTripSiflEntity.LastUpdTS = DateTime.UtcNow;
                                    PreflightTripSiflEntity.LastUpdUID = UserPrincipal.Identity.Name;
                                }
                                //PreflightTripSiflEntity.IsDeleted = true;
                                //Container.PreflightTripSIFLs.Attach(PreflightTripSiflEntity);
                                Container.DeleteObject(sifl);
                            }
                            Container.SaveChanges();

                            ReturnValue = GetSIFL(sifl.PreflightTripSIFLID);

                            ReturnValue.EntityInfo = ReturnValue.EntityList[0];

                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock
                        LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, sifl.PreflightTripSIFLID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<PreflightTripHistory> UpdatePreflightTripHistory(PreflightTripHistory history)
        {
            ReturnValue<PreflightTripHistory> ReturnValue = new ReturnValue<PreflightTripHistory>();
            PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(history))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new PreflightDataModelContainer())
                        {

                            history.CustomerID = CustomerID;

                            history.LastUpdTS = DateTime.UtcNow;
                            history.LastUpdUID = UserPrincipal.Identity.Name;

                            var HistoryId = history.TripHistoryID;

                            IQueryable<PreflightTripHistory> PreflightTripHistorys = preflightCompiledQuery.getPreflightTripHistory(Container, HistoryId, CustomerID);
                            PreflightTripHistory PreflightTripHistoryEntity = new PreflightTripHistory();
                            if (PreflightTripHistorys != null && PreflightTripHistorys.ToList().Count > 0)
                                PreflightTripHistoryEntity = PreflightTripHistorys.ToList()[0];

                            //PreflightTripHistory PreflightTripHistoryEntity = Container.PreflightTripHistories.Where(s => s.TripHistoryID == HistoryId).FirstOrDefault();
                            if (PreflightTripHistoryEntity != null)
                            {
                                Container.PreflightTripHistories.Attach(PreflightTripHistoryEntity);
                            }
                            Container.PreflightTripHistories.ApplyCurrentValues(history);


                            Container.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock
                        LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, history.TripHistoryID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<PreflightTripSIFL> DeletePreflightTripSIFL(Int64 SiflID)
        {
            ReturnValue<PreflightTripSIFL> ReturnValue = new ReturnValue<PreflightTripSIFL>();
            PreflightDataModelContainer Container = new PreflightDataModelContainer();
            PreflightTripSIFL sifl = new PreflightTripSIFL();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SiflID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    using (Container = new PreflightDataModelContainer())
                    {
                        sifl = new PreflightTripSIFL();
                        IQueryable<PreflightTripSIFL> PreflightTripSIFLs = preflightCompiledQuery.getPreflightTripSIFL(Container, SiflID, CustomerID);
                        if (PreflightTripSIFLs != null && PreflightTripSIFLs.ToList().Count > 0)
                            sifl = PreflightTripSIFLs.ToList()[0];
                        //sifl = Container.PreflightTripSIFLs.Where(X => X.PreflightTripSIFLID == SiflID).FirstOrDefault();
                        if (sifl != null)
                        {
                            //sifl.IsDeleted = true;
                            sifl.State = TripEntityState.Deleted;
                            //sifl.LastUpdTS = DateTime.Now;
                            //UpdateObjectState(ref trip, ref Container);
                            Container.DeleteObject(sifl);
                            Container.SaveChanges();
                        }
                        ReturnValue.EntityInfo = null;
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<PreflightTripSIFL> DeleteAllPreflightTripSIFL(Int64 TripID)
        {
            ReturnValue<PreflightTripSIFL> ReturnValue = new ReturnValue<PreflightTripSIFL>();
            PreflightDataModelContainer Container = new PreflightDataModelContainer();
            PreflightTripSIFL sifl = new PreflightTripSIFL();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Container = new PreflightDataModelContainer())
                    {
                        Container.DeleteAllPreflightTripSIFL(TripID, CustomerID);
                        ReturnValue.EntityInfo = null;
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        #endregion

        #region MoveTrip
        public ReturnValue<PreflightLeg> UpdatePreflightLegForMove(List<PreflightLeg> NewLegList, List<PreflightLeg> OldLegList, Int64 OldTripID, Int64 NewTripID)
        {
            ReturnValue<PreflightLeg> ReturnValue = null;
            ReturnValue<PreflightMain> MainReturnValue;

            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewLegList, OldLegList, OldTripID, NewTripID))
            {
                bool isAutoDispatch = false;
                bool IsRevision = false;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<PreflightLeg>();
                    MainReturnValue = new ReturnValue<PreflightMain>();

                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        if (UserPrincipal.Identity.Settings.IsAutomaticDispatchNum == true)
                            isAutoDispatch = true;
                        else
                            isAutoDispatch = false;
                        if (UserPrincipal.Identity.Settings.IsAutoRevisionNum == true)
                            IsRevision = true;
                        else
                            IsRevision = false;
                        if (NewTripID == 0)
                        {
                            NewTripID = (long)cs.GetCopyTripDetails(DateTime.Now, OldTripID, CustomerID, false, false, false, false, isAutoDispatch, IsRevision, UserName, false).FirstOrDefault();
                        }

                        foreach (PreflightLeg Leg in NewLegList)
                        {
                            cs.UpdateTripIdForPreflightLeg(Leg.LegID, NewTripID, Leg.LegNUM);
                        }

                        foreach (PreflightLeg Leg in OldLegList)
                        {
                            cs.UpdateTripIdForPreflightLeg(Leg.LegID, Leg.TripID, Leg.LegNUM);
                        }
                        if (OldLegList.Count == 0)
                            cs.DeletePreFlightMain(OldTripID, UserPrincipal.Identity.Name, DateTime.Now, true);

                        MainReturnValue = new ReturnValue<PreflightMain>();
                        MainReturnValue = GetTrip((long)OldTripID);
                        if (MainReturnValue.ReturnFlag)
                        {
                            // MainReturnValue.EntityList[0].State = TripEntityState.Modified;

                            PreflightMain Trip = MainReturnValue.EntityList[0];
                            Trip.State = TripEntityState.NoChange;
                            LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                            lockManager.Lock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, Trip.TripID);
                            MainReturnValue = UpdateTrip(Trip);
                        }

                        MainReturnValue = new ReturnValue<PreflightMain>();
                        MainReturnValue = GetTrip((long)NewTripID);
                        if (MainReturnValue.ReturnFlag)
                        {
                            PreflightMain Trip = MainReturnValue.EntityList[0];
                            Trip.State = TripEntityState.NoChange;
                            // MainReturnValue.EntityList[0].State = TripEntityState.Modified;
                            LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                            lockManager.Lock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, Trip.TripID);
                            MainReturnValue = UpdateTrip(Trip);
                        }

                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        #endregion

        #region Update CorporateRequest from Preflight
        public ReturnValue<PreflightMain> UpdateCRFromPreflight(PreflightMain Trip)
        {
            ReturnValue<PreflightMain> ReturnValue = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer Container = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new PreflightDataModelContainer())
                        {

                            //Handle methods through exception manager with return flag
                            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                            exManager.Process(() =>
                            {
                                Container.UpdateCorporateRequestFromPreflight(Trip.TripID, Trip.CustomerID, UserName);
                                ReturnValue = GetTrip(Trip.TripID);
                            }, FlightPak.Common.Constants.Policy.BusinessLayer);

                        }
                    }
                    catch (Exception ex)
                    {
                        //manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock
                        LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, Trip.TripID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;

        }
        #endregion

        public ReturnValue<PreflightMain> GetCQQuotetoPreflight(Int64 CQFileID, Int64 CQMainID, Boolean IsAutoDispatch, string TripStatus, Boolean IsRevision)
        {
            ReturnValue<PreflightMain> ReturnValue = new ReturnValue<PreflightMain>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, CQMainID, IsAutoDispatch, TripStatus, IsRevision))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopyQuoteID = new long();
                    CopyQuoteID = 0;
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        //CopytripID = cs.GetCopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, true, isAutoDispatch, IsRevision).FirstOrDefault();
                        CopyQuoteID = cs.GetCopyCQtoPreflight(CQFileID, CQMainID, UserName, CustomerID, IsAutoDispatch, TripStatus, IsRevision).FirstOrDefault();
                        //return (long)ret;

                        ReturnValue = GetTrip((long)CopyQuoteID);

                        if (ReturnValue.ReturnFlag)
                        {
                            //ReturnValue.EntityList[0].State = TripEntityState.Modified;
                            PreflightMain Trip = ReturnValue.EntityList[0];

                            LockManager<Data.Preflight.PreflightMain> lockManager = new LockManager<PreflightMain>();
                            lockManager.Lock(FlightPak.Common.Constants.EntitySet.Preflight.PreflightMain, Trip.TripID);

                            Trip.State = TripEntityState.NoChange;
                            ReturnValue = UpdateTrip(Trip);

                            #region "TripHistory"

                            IQueryable<CQFile> CQFiles = preflightCompiledQuery.getCQFile(cs, CQFileID, CustomerID);
                            CQFile cqFile = new CQFile();
                            if (CQFiles != null && CQFiles.ToList().Count > 0)
                                cqFile = CQFiles.ToList()[0];


                            IQueryable<CQMain> CQMains = preflightCompiledQuery.getCQMain(cs, CQMainID, CustomerID);
                            CQMain cqMain = new CQMain();
                            if (CQMains != null && CQMains.ToList().Count > 0)
                                cqMain = CQMains.ToList()[0];
                            // PreflightMain OldTrip = cs.GetTripByTripID(CustomerID, Trip.TripID).SingleOrDefault(); //cs.PreflightMains.Where(x => (x.TripID == CQFileID && x.CustomerID == CustomerID)).SingleOrDefault();
                            if (cqFile != null && cqMain != null)
                            {
                                //sujitha if (!isHistoryToBeCopied)
                                //{
                                PreflightTripHistory TripHistory = new PreflightTripHistory();
                                TripHistory.TripID = Trip.TripID;
                                TripHistory.CustomerID = CustomerID;
                                TripHistory.HistoryDescription = "Trip Details Copied From File No.: " + cqFile.FileNUM.ToString() + "Quote No.:" + cqMain.QuoteNUM.ToString();
                                TripHistory.LastUpdUID = UserPrincipal.Identity.Name;
                                TripHistory.LastUpdTS = DateTime.UtcNow;
                                //Trip.PreflightTripHistories.Add(TripHistory);
                                cs.PreflightTripHistories.AddObject(TripHistory);
                                cs.SaveChanges();
                                //}
                            }
                            // Container.ObjectStateManager.ChangeObjectState(Tripexp, System.Data.EntityState.Added);


                            #endregion

                            if (ReturnValue.ReturnFlag)
                            {

                                var retvalexp = GetTripExceptionList(Trip.TripID);

                                if (retvalexp.ReturnFlag)
                                {
                                    if (retvalexp.EntityList != null && retvalexp.EntityList.Count > 0)
                                    {
                                        Trip.TripException = "!";
                                    }
                                }


                                ReturnValue.EntityInfo = Trip;
                                ReturnValue.ReturnFlag = true;

                            }
                        }
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        #region Optimization

        public String getWindReliability(int val) {
            String per = null;
            switch (val)
            {
                case 1:
                    per = "50%";
                    break;
                case 2:
                    per = "75%";
                    break;
                case 3:
                    per = "85%";
                    break;
            }
            return per;
        }

        public static String getCrewRule(Int64 CrewRuleId,Int64 CusttomerID)
        {
            FlightPak.Data.MasterCatalog.CrewDutyRules objCrewVal = null;
            string CrewDutyRuleCD = null;
            FlightPak.Data.MasterCatalog.CrewDutyRules CrewDuty = new Data.MasterCatalog.CrewDutyRules();
            FlightPak.Data.MasterCatalog.MasterDataContainer FPMasterContainer = new Data.MasterCatalog.MasterDataContainer();
            objCrewVal = FPMasterContainer.GetAllCrewDutyRules(CusttomerID).Where(c => c.CrewDutyRulesID == CrewRuleId).SingleOrDefault<FlightPak.Data.MasterCatalog.CrewDutyRules>();
            if (objCrewVal != null)
            {
                CrewDutyRuleCD = Convert.ToString(objCrewVal.CrewDutyRuleCD);
            }
            return CrewDutyRuleCD;
        }

        public String FormatHistoryValueChange(object value1, object value2, String historyFieldTag)
        {
            string fieldOldValue = "";
            string fieldNewValue = "";

            if (value1 != null && !string.IsNullOrEmpty(value1.ToString().Trim()))
                fieldOldValue = value1.ToString();
            else
                fieldOldValue = "<span style='font-weight:bold'>NULL</span>";

            if (value2 != null && !string.IsNullOrEmpty(value2.ToString().Trim()))
                fieldNewValue = value2.ToString();
            else
                fieldNewValue = "<span style='font-weight:bold'>NULL</span>";

            string strHistoryLine = string.Format("{0} Changed From {1} To {2}", historyFieldTag, fieldOldValue, fieldNewValue);
            return strHistoryLine;
        }

        public string FormatHistoryValueChange(string fieldText, object value1, object value2, bool isDate = false)
        {
            string historyRecord = string.Empty;

            if (isDate)
            {
                if (string.IsNullOrEmpty(Convert.ToString(value1).Trim()) && !string.IsNullOrEmpty(Convert.ToString(value2).Trim()))
                {
                    historyRecord = string.Format("{0} Added: {1}.", fieldText, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2));
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(value1).Trim()) && string.IsNullOrEmpty(Convert.ToString(value2).Trim()))
                {
                    historyRecord = string.Format("{0} Removed From Trip With Details: {1}.", fieldText, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1));
                }
                else
                {
                    historyRecord = string.Format("{0} Changed From {1} To {2}.", fieldText, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1), String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2));
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Convert.ToString(value1).Trim()) && !string.IsNullOrEmpty(Convert.ToString(value2).Trim()))
                {
                    historyRecord = string.Format("{0} Added: {1}.", fieldText, value2.ToString().Trim());
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(value1).Trim()) && string.IsNullOrEmpty(Convert.ToString(value2).Trim()))
                {
                    historyRecord = string.Format("{0} Removed From Trip With Details: {1}.", fieldText, value1.ToString().Trim());
                }
                else
                {
                    historyRecord = string.Format("{0} Changed From {1} To {2}.", fieldText, value1.ToString().Trim(), value2.ToString().Trim());
                }
            }
            return historyRecord;
        
        }

        public String FormateHistoryValueChange(String historyFieldTag, String historyFieldTagLabel, object OldValue, object NewValue, bool isDate = false)
        {
            string strHistoryLine = string.Empty;

            if (isDate)
            {
                if (string.IsNullOrEmpty(Convert.ToString(OldValue)) && !string.IsNullOrEmpty(Convert.ToString(NewValue)))
                {
                    strHistoryLine = string.Format("Leg {0} {1} Added To Leg With Details : {2}", historyFieldTag, historyFieldTagLabel, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", NewValue));
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(OldValue)) && string.IsNullOrEmpty(Convert.ToString(NewValue)))
                {
                    strHistoryLine = string.Format("Leg {0} {1} Removed From Leg With Details : {2}", historyFieldTag, historyFieldTagLabel, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", OldValue));
                }
                else if (Convert.ToString(OldValue).Trim() != Convert.ToString(NewValue).Trim())
                {
                    strHistoryLine = string.Format("Leg {0} {1} Changed From {2} To {3}", historyFieldTag, historyFieldTagLabel, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", OldValue), String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", NewValue));
                }
            }
            else 
            {
                if (string.IsNullOrEmpty(Convert.ToString(OldValue)) && !string.IsNullOrEmpty(Convert.ToString(NewValue)))
                {
                    strHistoryLine = string.Format("Leg {0} {1} Added To Leg With Details: {2}.", historyFieldTag, historyFieldTagLabel, NewValue.ToString().Trim());
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(OldValue)) && string.IsNullOrEmpty(Convert.ToString(NewValue)))
                {
                    strHistoryLine = string.Format("Leg {0} {1} Removed From Leg With Details: {2}.", historyFieldTag, historyFieldTagLabel, OldValue.ToString().Trim());
                }
                else if (Convert.ToString(OldValue) != Convert.ToString(NewValue))
                {
                    strHistoryLine = string.Format("Leg {0} {1} Changed From {2} To {3}.", historyFieldTag, historyFieldTagLabel, Convert.ToString(OldValue).Trim(), Convert.ToString(NewValue).Trim());
                }
            }

            return strHistoryLine;
        }

        public String FormateHistoryValueChange(String historyFieldTag, object value1, object value2)
        {
            string strHistoryLine = string.Format("{0} Changed From {1} To {2}", historyFieldTag, (value1 != null ? value1 : "<span style='font-weight:bold'>NULL</span>"), (value2 != null ? value2 : "<span style='font-weight:bold'>NULL</span>"));
            return strHistoryLine;
        }


        public ReturnValue<IndividualDispatcher> GetDispatcherByUsername(string Username)
        {
            ReturnValue<IndividualDispatcher> ret = new ReturnValue<IndividualDispatcher>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Username))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetDispatcherByUsername(CustomerID, Username).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualPassenger> GetIndividualPassengerByIDOrCD(string passengerRequestorCD, long passengerRequestorID)
        {
            ReturnValue<IndividualPassenger> ret = new ReturnValue<IndividualPassenger>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerRequestorCD, passengerRequestorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetIndividualPassengerByIDOrCD(CustomerID, passengerRequestorCD, passengerRequestorID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualCrew> GetCrewBYIDOrCD(string CrewCD, long CrewID)
        {
            ReturnValue<IndividualCrew> ret = new ReturnValue<IndividualCrew>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewCD, CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetCrewBYIDOrCD(CustomerID, CrewCD, CrewID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualEmergencyContact> GetEmergencyContactByIDOrCD(string EmergencyContactCD, long EmergencyContactID)
        {
            ReturnValue<IndividualEmergencyContact> ret = new ReturnValue<IndividualEmergencyContact>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EmergencyContactCD, EmergencyContactCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetEmergencyContactByIDOrCD(CustomerID, EmergencyContactCD, EmergencyContactID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }

        public ReturnValue<IndividualFleet> GetFleetByIDOrTailnum(string Tailnum, long FleetID)
        {
            ReturnValue<IndividualFleet> ret = new ReturnValue<IndividualFleet>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Tailnum, FleetID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetFleetByIDOrTailnum(CustomerID, Tailnum, FleetID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualClient> GetClientByIDOrCD(string ClientCD, long ClientID)
        {
            ReturnValue<IndividualClient> ret = new ReturnValue<IndividualClient>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientCD, ClientID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetClientByIDOrCD(CustomerID, ClientCD, ClientID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }

        public ReturnValue<IndividualCompany> GetCompanyMasterbyHomebaseCDorID(string HomebaseCD, long HomebaseID)
        {
            ReturnValue<IndividualCompany> ret = new ReturnValue<IndividualCompany>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomebaseCD, HomebaseID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetCompanyMasterbyHomebaseCDorID(CustomerID, HomebaseCD, HomebaseID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualDepartmentAuthorization> GetDepartmentAuthorizationByCDOrID(string DepartmentAuthorizationCD, long DepartmentAuthorizationID)
        {
            ReturnValue<IndividualDepartmentAuthorization> ret = new ReturnValue<IndividualDepartmentAuthorization>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorizationCD, DepartmentAuthorizationID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetDepartmentAuthorizationByCDOrID(CustomerID, DepartmentAuthorizationCD, DepartmentAuthorizationID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualDepartment> GetDepartmentByCDOrID(string DepartmentCD, long DepartmentID)
        {
            ReturnValue<IndividualDepartment> ret = new ReturnValue<IndividualDepartment>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentCD, DepartmentID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetDepartmentByCDOrID(CustomerID, DepartmentCD, DepartmentID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<IndividualAccount> GetAccountByCDorID(string AccountCD, long AccountID)
        {
            ReturnValue<IndividualAccount> ret = new ReturnValue<IndividualAccount>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AccountCD, AccountID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAccountByCDorID(CustomerID, AccountCD, AccountID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }

        public ReturnValue<IndividualAircraft> GetAircraftByIDOrCD(string AircraftCD, long AircraftID)
        {
            ReturnValue<IndividualAircraft> ret = new ReturnValue<IndividualAircraft>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftCD, AircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAircraftByIDOrCD(CustomerID, AircraftCD, AircraftID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }


        public ReturnValue<GetAllFleetForPopup> GetAllFleetForPopup(bool FetchActiveOnly, string IsFixedRotary, string IcaoID, long VendorID)
        {
            ReturnValue<GetAllFleetForPopup> ret = new ReturnValue<GetAllFleetForPopup>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FetchActiveOnly, IsFixedRotary, IcaoID, VendorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllFleetForPopup(CustomerID, ClientId, FetchActiveOnly, IsFixedRotary, IcaoID, VendorID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }

        public ReturnValue<GetAllPassengerForPopup> GetAllPassengerForPopup(bool FetchActiveOnly, bool IsRequestor, string IcaoID, long CQCustomerID)
        {
            ReturnValue<GetAllPassengerForPopup> ret = new ReturnValue<GetAllPassengerForPopup>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FetchActiveOnly, IsRequestor, IcaoID, CQCustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        ret.EntityList = cs.GetAllPassengerForPopup(CustomerID, ClientId, FetchActiveOnly, IsRequestor, IcaoID, CQCustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }

        public ReturnValue<GetAllCrewForPopup> GetAllCrewForPopup(long ClientID, bool FetchActiveOnly, bool IsRotatory, bool IsFixed, string IcaoID, long CrewGroupID)
        {
            ReturnValue<GetAllCrewForPopup> ret = new ReturnValue<GetAllCrewForPopup>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID, FetchActiveOnly, IsRotatory, IsFixed, IcaoID, CrewGroupID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        ret.EntityList = cs.GetAllCrewForPopup(CustomerID, ClientID, FetchActiveOnly, IsRotatory, IsFixed, IcaoID, CrewGroupID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return ret;
        }
        #endregion

        // Fix for #1325 - invalid no passport exception for a pax who has a current passport on file
        public ReturnValue<GetAllCrewPassport> GetAllCrewPAXPassport(Int64? CrewId, Int64? PAXId)
        {
            ReturnValue<GetAllCrewPassport> ret = new ReturnValue<GetAllCrewPassport>();
            PreflightDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewId, PAXId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new PreflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCrewPassport(CrewId, PAXId, CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="Trip"></param>
        /// <param name="EmailList"></param>
        /// <param name="FromEmail"></param>
        /// <param name="HtmlBody"></param>
        public void SendTripDetails(PreflightMain Trip, List<string> EmailList, string FromEmail, string HtmlBody, List<String> attachments)
        {
            if (EmailList != null && EmailList.Count > 0)
            {
                string Emailsubject = string.Format("Flight Scheduling Software - Trip No.: {0}", Trip.TripNUM);

                StringBuilder htmlEmailbody = new StringBuilder();
                htmlEmailbody.Append(HtmlBody);
                SendEmailByList(EmailList, Emailsubject, htmlEmailbody, FromEmail, attachments);
            }
        }

        /// <summary>
        /// 
        /// </summary>        
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="EmailList"></param>
        /// <param name="Emailsubject"></param>
        /// <param name="htmlEmailbody"></param>
        /// <param name="FromEmail"></param>
        /// <param name="attachments"></param>
        private void SendEmailByList(List<string> EmailList, string Emailsubject, StringBuilder htmlEmailbody, string FromEmail, List<String> attachments)
        {
            foreach (string emailadd in EmailList)
            {
                FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                {                    
                    CustomerID = CustomerID,
                    EmailBody = htmlEmailbody.ToString(),
                    EmailReqTime = DateTime.UtcNow,
                    EmailReqUID = UserName,
                    EmailSentTo = emailadd,
                    EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                    EmailSubject = Emailsubject,
                    EmailTmplID = null,
                    EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                    IsBodyHTML = true,
                },
                null,                   
                FromEmail,
                attachments
                );                
            }
        }
        /// <summary>
        /// Generate Preflight Adhoc Report
        /// </summary>
        /// <param name="HomeBaseID"></param>
        /// <param name="ClientID"></param>
        /// <param name="FleetID"></param>
        /// <param name="IsPersonal"></param>
        /// <param name="IsCompleted"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="FromTripNUM"></param>
        /// <param name="ToTripNUM"></param>
        /// <returns>User Search result for Preflight Adhoc report</returns>
        public ReturnValue<Data.Preflight.GetPreReportSearch> GetPreReportList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate, DateTime EndDate, Int64 FromTripNUM, Int64 ToTripNUM)
        {
            ReturnValue<Data.Preflight.GetPreReportSearch> Results = null;
            Data.Preflight.PreflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID, ClientID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromTripNUM, ToTripNUM))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Preflight.GetPreReportSearch>();
                    using (Container = new PreflightDataModelContainer())
                    {
                        if (ClientID == 0)
                            ClientID = Convert.ToInt64(UserPrincipal.Identity.ClientId);

                        Container.ContextOptions.ProxyCreationEnabled = false;
                        Results.EntityList = Container.GetPreReportSearch(CustomerID, ClientID, HomeBaseID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromTripNUM, ToTripNUM).ToList();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
       
    }

    /// <summary>
    /// Class Added for Default Standard and Flightpak Conversion Setting
    /// </summary>

    public class StandardFlightpakConversion
    {
        public decimal Tenths { get; set; }
        public int StartMinutes { get; set; }
        public int EndMinutes { get; set; }
        public int ConversionType { get; set; }
    }

    public static class preflightCompiledQuery
    {
        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, string, Int64, IQueryable<UserMaster>> _getUser;
        public static IQueryable<UserMaster> getUser(FlightPak.Data.Preflight.PreflightDataModelContainer db, string Username, Int64 CustomerID)
        {

            _getUser = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, string, Int64, IQueryable<UserMaster>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, string User, Int64 customerID) => from user in cont.UserMasters
                                                                                                    where user.UserName == Username && CustomerID == user.CustomerID
                                                                                                    select user);
            return _getUser.Invoke(db, Username, CustomerID);

        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Fleet>> _getFleet;
        public static IQueryable<Fleet> getFleet(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 FleetID, Int64 CustomerID)
        {

            _getFleet = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Fleet>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 fleetid, Int64 customerID) => from fleet in cont.Fleets
                                                                                                      where fleet.FleetID == FleetID && CustomerID == fleet.CustomerID
                                                                                                      select fleet);
            return _getFleet.Invoke(db, FleetID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<FlightCatagory>> _getFlightCatagory;
        public static IQueryable<FlightCatagory> getFlightCatagory(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 FlightCatagoryID, Int64 CustomerID)
        {

            _getFlightCatagory = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<FlightCatagory>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 flightcatagoryid, Int64 customerID) => from flightcatagory in cont.FlightCatagories
                                                                                                               where flightcatagory.FlightCategoryID == FlightCatagoryID && CustomerID == flightcatagory.CustomerID
                                                                                                               select flightcatagory);

            return _getFlightCatagory.Invoke(db, FlightCatagoryID, CustomerID);
        }


        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Airport>> _getAirport;
        public static IQueryable<Airport> getAirport(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 AirportID, Int64 CustomerID)
        {

            _getAirport = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Airport>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 airportid, Int64 customerID) => from airport in cont.Airports
                                                                                                        where airport.AirportID == AirportID && CustomerID == airport.CustomerID
                                                                                                        select airport);

            return _getAirport.Invoke(db, AirportID, CustomerID);
        }

        // Added for Fix #8222
        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, IQueryable<Airport>> _getAirportByAirportID;
        public static IQueryable<Airport> getAirportByAirportID(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 AirportID)
        {

            _getAirportByAirportID = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, IQueryable<Airport>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 airportid) => from airport in cont.Airports
                                                                                      where airport.AirportID == AirportID
                                                                                      select airport);

            return _getAirportByAirportID.Invoke(db, AirportID);
        }


        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Crew>> _getInActvieCrew;
        public static IQueryable<Crew> getInActvieCrew(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CrewID, Int64 CustomerID)
        {

            _getInActvieCrew = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Crew>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 crewid, Int64 customerID) => from crew in cont.Crews
                                                                                                     where crew.CrewID == CrewID && CustomerID == crew.CustomerID && crew.IsStatus == false
                                                                                                     select crew);

            return _getInActvieCrew.Invoke(db, CrewID, CustomerID);

        }
        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Crew>> _getCrew;
        public static IQueryable<Crew> getCrew(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CrewID, Int64 CustomerID)
        {

            _getCrew = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Crew>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 crewid, Int64 customerID) => from crew in cont.Crews
                                                                                                     where crew.CrewID == CrewID && CustomerID == crew.CustomerID
                                                                                                     select crew);

            return _getCrew.Invoke(db, CrewID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Aircraft>> _getAircraft;
        public static IQueryable<Aircraft> getAircraft(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 AircraftID, Int64 CustomerID)
        {

            _getAircraft = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Aircraft>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 aircraftid, Int64 customerID) => from aircraft in cont.Aircraft
                                                                                                         where aircraft.AircraftID == AircraftID && CustomerID == aircraft.CustomerID
                                                                                                         select aircraft);

            return _getAircraft.Invoke(db, AircraftID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Passenger>> _getPassenger;
        public static IQueryable<Passenger> getPassenger(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 PassengerID, Int64 CustomerID)
        {

            _getPassenger = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Passenger>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 passengerid, Int64 customerID) => from passenger in cont.Passengers
                                                                                                          where passenger.PassengerRequestorID == PassengerID && CustomerID == passenger.CustomerID
                                                                                                          select passenger);

            return _getPassenger.Invoke(db, PassengerID, CustomerID);
        }


        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Department>> _getDepartment;
        public static IQueryable<Department> getDepartment(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 DepartmentID, Int64 CustomerID)
        {

            _getDepartment = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Department>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 departmentid, Int64 customerID) => from department in cont.Departments
                                                                                                           where department.DepartmentID == DepartmentID && CustomerID == department.CustomerID
                                                                                                           select department);

            return _getDepartment.Invoke(db, DepartmentID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<DepartmentAuthorization>> _getDepartmentAuthorization;
        public static IQueryable<DepartmentAuthorization> getDepartmentAuthorization(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 DepartmentAuthorizationID, Int64 CustomerID)
        {

            _getDepartmentAuthorization = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<DepartmentAuthorization>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 departmentAuthorizationid, Int64 customerID) => from departmentAuthorization in cont.DepartmentAuthorizations
                                                                                                                        where departmentAuthorization.AuthorizationID == DepartmentAuthorizationID && CustomerID == departmentAuthorization.CustomerID
                                                                                                                        select departmentAuthorization);

            return _getDepartmentAuthorization.Invoke(db, DepartmentAuthorizationID, CustomerID);
        }


        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<CQCustomer>> _getCQCustomer;
        public static IQueryable<CQCustomer> getCQCustomer(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CQCustomerID, Int64 CustomerID)
        {

            _getCQCustomer = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<CQCustomer>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 cqCustomerid, Int64 customerID) => from cqCustomer in cont.CQCustomers
                                                                                                           where cqCustomer.CQCustomerID == CQCustomerID && CustomerID == cqCustomer.CustomerID
                                                                                                           select cqCustomer);

            return _getCQCustomer.Invoke(db, CQCustomerID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Client>> _getClient;
        public static IQueryable<Client> getClient(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 ClientID, Int64 CustomerID)
        {

            _getClient = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Client>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 clientid, Int64 customerID) => from client in cont.Clients
                                                                                                       where client.ClientID == ClientID && CustomerID == client.CustomerID
                                                                                                       select client);

            return _getClient.Invoke(db, ClientID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<EmergencyContact>> _getEmergencyContact;
        public static IQueryable<EmergencyContact> getEmergencyContact(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 EmergencyContactID, Int64 CustomerID)
        {

            _getEmergencyContact = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<EmergencyContact>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 emergencyContactid, Int64 customerID) => from emergencyContact in cont.EmergencyContacts
                                                                                                                 where emergencyContact.EmergencyContactID == EmergencyContactID && CustomerID == emergencyContact.CustomerID
                                                                                                                 select emergencyContact);

            return _getEmergencyContact.Invoke(db, EmergencyContactID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Company>> _getCompany;
        public static IQueryable<Company> getCompany(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CompanyID, Int64 CustomerID)
        {

            _getCompany = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Company>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 companyid, Int64 customerID) => from company in cont.Companies
                                                                                                        where company.HomebaseID == CompanyID && CustomerID == company.CustomerID
                                                                                                        select company);

            return _getCompany.Invoke(db, CompanyID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Transport>> _getTransport;
        public static IQueryable<Transport> getTransport(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 TransportID, Int64 CustomerID)
        {

            _getTransport = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Transport>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 transportid, Int64 customerID) => from transport in cont.Transports
                                                                                                          where transport.TransportID == TransportID && CustomerID == transport.CustomerID
                                                                                                          select transport);

            return _getTransport.Invoke(db, TransportID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<FBO>> _getFBO;
        public static IQueryable<FBO> getFbo(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 FBOID, Int64 CustomerID)
        {

            _getFBO = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<FBO>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 fboid, Int64 customerID) => from fbo in cont.FBOes
                                                                                                    where fbo.FBOID == FBOID && CustomerID == fbo.CustomerID
                                                                                                    select fbo);

            return _getFBO.Invoke(db, FBOID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Catering>> _getCatering;
        public static IQueryable<Catering> getCatering(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CateringID, Int64 CustomerID)
        {

            _getCatering = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Catering>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 fboid, Int64 customerID) => from fbo in cont.Caterings
                                                                                                    where fbo.CateringID == CateringID && CustomerID == fbo.CustomerID
                                                                                                    select fbo);

            return _getCatering.Invoke(db, CateringID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<TripManagerCheckList>> _getTripManagerCheckList;
        public static IQueryable<TripManagerCheckList> getTripManagerCheckList(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 TripManagerCheckListID, Int64 CustomerID)
        {

            _getTripManagerCheckList = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<TripManagerCheckList>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 tripManagerCheckListid, Int64 customerID) => from tripManagerCheckList in cont.TripManagerCheckLists
                                                                                                                     where tripManagerCheckList.CheckListID == TripManagerCheckListID && CustomerID == tripManagerCheckList.CustomerID
                                                                                                                     select tripManagerCheckList);

            return _getTripManagerCheckList.Invoke(db, TripManagerCheckListID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<TripManagerCheckListGroup>> _getTripManagerCheckListGroup;
        public static IQueryable<TripManagerCheckListGroup> getTripManagerCheckListGroup(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 TripManagerCheckListGroupID, Int64 CustomerID)
        {

            _getTripManagerCheckListGroup = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<TripManagerCheckListGroup>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 tripManagerCheckListGroupid, Int64 customerID) => from tripManagerCheckListGroup in cont.TripManagerCheckListGroups
                                                                                                                          where tripManagerCheckListGroup.CheckGroupID == TripManagerCheckListGroupID && CustomerID == tripManagerCheckListGroup.CustomerID
                                                                                                                          select tripManagerCheckListGroup);

            return _getTripManagerCheckListGroup.Invoke(db, TripManagerCheckListGroupID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<FlightPurpose>> _getFlightPurpose;
        public static IQueryable<FlightPurpose> getFlightPurpose(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 FlightPurposeID, Int64 CustomerID)
        {

            _getFlightPurpose = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<FlightPurpose>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 flightPurposeid, Int64 customerID) => from flightPurpose in cont.FlightPurposes
                                                                                                              where flightPurpose.FlightPurposeID == FlightPurposeID && CustomerID == flightPurpose.CustomerID
                                                                                                              select flightPurpose);

            return _getFlightPurpose.Invoke(db, FlightPurposeID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<PreflightTripSIFL>> _getPreflightTripSIFL;
        public static IQueryable<PreflightTripSIFL> getPreflightTripSIFL(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 PreflightTripSIFLID, Int64 CustomerID)
        {

            _getPreflightTripSIFL = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<PreflightTripSIFL>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 preflightTripSIFLid, Int64 customerID) => from preflightTripSIFL in cont.PreflightTripSIFLs
                                                                                                                  where preflightTripSIFL.PreflightTripSIFLID == PreflightTripSIFLID && CustomerID == preflightTripSIFL.CustomerID
                                                                                                                  select preflightTripSIFL);

            return _getPreflightTripSIFL.Invoke(db, PreflightTripSIFLID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<PreflightTripHistory>> _getPreflightTripHistory;
        public static IQueryable<PreflightTripHistory> getPreflightTripHistory(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 PreflightTripHistoryID, Int64 CustomerID)
        {

            _getPreflightTripHistory = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<PreflightTripHistory>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 preflightTripHistoryid, Int64 customerID) => from preflightTripHistory in cont.PreflightTripHistories
                                                                                                                     where preflightTripHistory.TripHistoryID == PreflightTripHistoryID && CustomerID == preflightTripHistory.CustomerID
                                                                                                                     select preflightTripHistory);
            return _getPreflightTripHistory.Invoke(db, PreflightTripHistoryID, CustomerID);
        }


        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<CQFile>> _getCQFile;
        public static IQueryable<CQFile> getCQFile(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CQFileID, Int64 CustomerID)
        {
            _getCQFile = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<CQFile>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 cqFileid, Int64 customerID) => from cqFile in cont.CQFiles
                                                                                                       where cqFile.CQFileID == CQFileID && CustomerID == cqFile.CustomerID
                                                                                                       select cqFile);
            return _getCQFile.Invoke(db, CQFileID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<CQMain>> _getCQMain;
        public static IQueryable<CQMain> getCQMain(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 CQMainID, Int64 CustomerID)
        {

            _getCQMain = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<CQMain>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 cqMainid, Int64 customerID) => from cqMain in cont.CQMains
                                                                                                       where cqMain.CQMainID == CQMainID && CustomerID == cqMain.CustomerID
                                                                                                       select cqMain);
            return _getCQMain.Invoke(db, CQMainID, CustomerID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Guid, IQueryable<PreflightFuel>> _getPreflightFuel;
        public static IQueryable<PreflightFuel> getPreflightFuel(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 AirportID, Guid UWAFuelInterfaceRequestID)
        {

            _getPreflightFuel = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Guid, IQueryable<PreflightFuel>>
     ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 airportID, Guid uwaFuelInterfaceRequestID) => from preflightFuel in cont.PreflightFuels
                                                                                                                      where preflightFuel.AirportID == AirportID && UWAFuelInterfaceRequestID == preflightFuel.UWAFuelInterfaceRequestID
                                                                                                                      select preflightFuel);
            return _getPreflightFuel.Invoke(db, AirportID, UWAFuelInterfaceRequestID);
        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Account>> _getAccount;
        public static IQueryable<Account> getAccount(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 AccountID, Int64 CustomerID)
        {
            _getAccount = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<Account>>
            ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 accountid, Int64 customerID) => from account in cont.Accounts
                                                                                                               where account.AccountID == AccountID && CustomerID == account.CustomerID
                                                                                                               select account);
            return _getAccount.Invoke(db, AccountID, CustomerID);

        }

        public static Func<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<PreflightMain>> _getPreflightMain;

        public static IQueryable<PreflightMain> getPreflightMain(FlightPak.Data.Preflight.PreflightDataModelContainer db, Int64 TripID, Int64 CustomerID)
        {


            _getPreflightMain = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Preflight.PreflightDataModelContainer, Int64, Int64, IQueryable<PreflightMain>>

            ((FlightPak.Data.Preflight.PreflightDataModelContainer cont, Int64 tripID, Int64 customerID) => from preflightMain in cont.PreflightMains

                                                                                                            where preflightMain.TripID == TripID && CustomerID == preflightMain.CustomerID

                                                                                                            select preflightMain);


            return _getPreflightMain.Invoke(db, TripID, CustomerID);

        }

    }

}


