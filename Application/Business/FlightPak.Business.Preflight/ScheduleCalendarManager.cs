﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Data.MasterCatalog;
using FlightPak.Business.Common;
using FlightPak.Business.Calculations;
using FlightPak.Business.Preflight.ScheduleCalendarEntities;
using FlightPak.Data.Preflight;
using FlightPak.Common;
using FlightPak.Common.Constants;
using System.Collections.ObjectModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;


namespace FlightPak.Business.Preflight
{
    public class ScheduleCalendarManager : BaseManager,  IPreflight.ICacheable
    {
        protected List<PreflightCrewList> PreflightCrewList = null;
        protected List<PreflightPassengerList> PreflightPassengerList = null;
        private ExceptionManager exManager;


        #region ScheduleCalender Advanced Filter

        /// <summary>
        /// Gets the user specific default advanced filter settings.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>user specific default advanced filter settings. returns string.empty if there is no such user id 
        /// exist</returns>
        public string RetrieveFilterSettings()
        {
            string settings = string.Empty;
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    var resultSet = Container.GetAdvancedFilters(CustomerID, UserName).ToList();
                    if (resultSet.Count() > 0)
                    {
                        var entityAdvancedFilter = resultSet.First();
                        settings = entityAdvancedFilter.Settings;
                    }
                   
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return settings;
        }

        /// <summary>
        /// Gets the system default advanced filter settings.
        /// </summary>
        /// <returns>System default advanced filter settings.</returns>
        public string RetrieveSystemDefaultSettings()
        {
            string settings = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    var queryableAdvancedFilter = Container.PreflightAdvancedFilters.Where(x => x.UserName == null && x.CustomerID == null);

                    if (queryableAdvancedFilter.Count() > 0)
                    {
                        var entityAdvancedFilter = queryableAdvancedFilter.First();
                        settings = entityAdvancedFilter.Settings;
                        
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return settings;
	
        }

        /// <summary>
        /// Saves the user specific settings. If there is already settings available for the user, then the previous settings
        /// will be overriden with this new settings else new settings will be created.
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="xmlSettings">Advanced filter settings</param>
        /// <returns>True if the save operation is successful.</returns>
        public bool SaveUserDefaultSettings(string xmlSettings)
        {
            bool successFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( xmlSettings))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.AddAdvancedFilters(CustomerID, UserName, xmlSettings);

                    Container.SaveChanges();
                    successFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return successFlag;
        }

        /// <summary>
        /// Checks if the advanced filter options available for the user
        /// </summary>
        /// <param name="userID">user id</param>
        /// <returns>True if user settings exist else false. </returns>
        public bool IsUserSettingsAvailable()
        {
            bool isAvailable = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    var resultSet = Container.GetAdvancedFilters(CustomerID, UserName).ToList();
                    if (resultSet.Count() == 0)
                        isAvailable= false;
                    else
                    {
                        var entityAdvancedFilter = resultSet.First();
                        if (string.IsNullOrWhiteSpace(entityAdvancedFilter.Settings))
                            isAvailable = false;
                        else
                            isAvailable = true;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return isAvailable;
            }
	
        }

        #endregion

        #region ScheduleCalender Preferences

        /// <summary>
        /// Gets the user specific default Schedule Calender Preferences.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>user specific default advanced filter settings. returns string.empty if there is no such user id 
        /// exist</returns>
        public string RetrieveScheduleCalenderPreferences(string CalenderType)
        {
            string fleetids = string.Empty;
            string crewids = string.Empty;
            string crewgroupids = string.Empty;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    var resultSet = Container.GetScheduleCalenderPreferences(CustomerID, UserName, CalenderType).ToList();
                    if (resultSet.Count() > 0)
                    {
                        var entityScheduleCalenderPreferences = resultSet.First();
                        fleetids = entityScheduleCalenderPreferences.FleetIDS;
                        crewids = entityScheduleCalenderPreferences.CrewIDS;
                        crewgroupids = entityScheduleCalenderPreferences.CrewGroupIDS;  
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return fleetids + "|" + crewids + "|" + crewgroupids;
        }

        /// <summary>
        /// Gets the system default advanced filter settings.
        /// </summary>
        /// <returns>System default advanced filter settings.</returns>
       

        /// <summary>
        /// Saves the user specific settings. If there is already settings available for the user, then the previous settings
        /// will be overriden with this new settings else new settings will be created.
        /// </summary>
        /// <param name="userID">User Id</param>
        /// <param name="xmlSettings">Advanced filter settings</param>
        /// <returns>True if the save operation is successful.</returns>
        public bool SaveScheduleCalanderPreferences(string CalenderType, string fleetIDS, string crewIDS, string crewGroupIDS, bool isCrewGroup)
        {
            bool successFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetIDS, crewIDS, crewGroupIDS))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.AddScheduleCalenderPreferences(CustomerID, UserName, fleetIDS, crewIDS, CalenderType, crewGroupIDS, isCrewGroup);

                    Container.SaveChanges();
                    successFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return successFlag;
        }

        /// <summary>
        /// Checks if the advanced filter options available for the user
        /// </summary>
        /// <param name="userID">user id</param>
        /// <returns>True if user settings exist else false. </returns>
        public bool IsScheduleCalenderPreferencesAvailable(string CalenderType)
        {
            bool isAvailable = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    var resultSet = Container.GetScheduleCalenderPreferences(CustomerID, UserName, CalenderType).ToList();
                    isAvailable = (resultSet.Count() == 0) ? false : true;
                                       
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return isAvailable;
            }

        }

        #endregion

        # region ScheduleCalendar Preferences Monthly

        //Monthly Tab SatSunWeek & NoPastWeek

        public string RetrieveScheduleCalenderPreferencesMonthly(string CalenderType)
        {
            bool satSunWeek = false;
            bool noPastWeek = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    var resultSet = Container.GetScheduleCalenderPreferencesMonthly(CustomerID, UserName, CalenderType).ToList();
                    if (resultSet.Count() > 0)
                    {
                        var entityScheduleCalenderPreferencesMonthly = resultSet.First();
                        satSunWeek = entityScheduleCalenderPreferencesMonthly.SaturdaySundayWeek;
                        noPastWeek = entityScheduleCalenderPreferencesMonthly.NoPastWeek;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return satSunWeek + "|" + noPastWeek;
        }

        public bool SaveScheduleCalanderPreferencesMonthly(string CalenderType, bool satSunWeek, bool noPastWeek)
        {
            bool successFlag = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CalenderType, satSunWeek, noPastWeek))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.AddScheduleCalenderPreferencesMonthly(CustomerID, UserName, satSunWeek, noPastWeek, CalenderType);

                    Container.SaveChanges();
                    successFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return successFlag;
        }

        #endregion


        # region Schedule Calendar

        public ReturnValue<Data.Preflight.FleetCalendarDataResult> GetFleetCalendarData(DateTime startDate, DateTime endDate, FilterEntity filterEntity, Collection<string> fleetTreeInput, bool retrievePaxDetails, bool isStandByCrew, bool showRecordStartsInDateRange)
        {

            ReturnValue<Data.Preflight.FleetCalendarDataResult> finalList = new ReturnValue<Data.Preflight.FleetCalendarDataResult>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, endDate, filterEntity, fleetTreeInput, retrievePaxDetails, isStandByCrew, showRecordStartsInDateRange))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    finalList.EntityList = new List<FleetCalendarDataResult>();

                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    if (filterEntity.TimeBase == TimeBase.None)
                    {
                        filterEntity.TimeBase = TimeBase.Local;
                    }

                    long? clientId = null;
                    if (!String.IsNullOrEmpty(filterEntity.ClientID))
                    {
                        clientId = Convert.ToInt64(filterEntity.ClientID);
                    }

                    long? requestorId = null;
                    if (!String.IsNullOrEmpty(filterEntity.RequestorID))
                    {
                        requestorId = Convert.ToInt64(filterEntity.RequestorID);
                    }

                    long? departmentId = null;
                    if (!String.IsNullOrEmpty(filterEntity.DepartmentID))
                    {
                        departmentId = Convert.ToInt64(filterEntity.DepartmentID);
                    }

                    long? flightCategoryId = null;
                    if (!String.IsNullOrEmpty(filterEntity.FlightCategoryID))
                    {
                        flightCategoryId = Convert.ToInt64(filterEntity.FlightCategoryID);
                    }

                    long? crewDutyId = null;
                    if (!String.IsNullOrEmpty(filterEntity.CrewDutyID))
                    {
                        crewDutyId = Convert.ToInt64(filterEntity.CrewDutyID);
                    }

                    long? homeBaseOnlyId = null;
                    if (filterEntity.HomeBaseOnly)
                    {
                        // get user's home base id and filter
                        if (HomeBaseID != null && HomeBaseID != 0)
                        {
                            homeBaseOnlyId = HomeBaseID;
                        }
                    }
                    var resultSet = Container.GetFleetCalendarData(CustomerID, filterEntity.TimeBase.ToString(), startDate, endDate, clientId, requestorId,
                                                                    departmentId, flightCategoryId, crewDutyId, homeBaseOnlyId).ToList();


                    var RONLegsList = new List<FleetCalendarDataResult>();
                    // get RON records having enddate != start date (i.e ron greater than 1 day)
                    var dummyLegsHavingEndDateGreaterThanStartDate = resultSet.Where(x => x.LegNUM == 0 && x.ArrivalDisplayTime.Value.Date > x.DepartureDisplayTime.Value.Date).ToList();
                    // get the dates in between start date and endDate of RON record and split them
                    for (int i = 0; i < dummyLegsHavingEndDateGreaterThanStartDate.Count(); i++)
                    {
                        var dummyLeg = dummyLegsHavingEndDateGreaterThanStartDate[i];
                        var datesBetweenDictionary = GetDates(dummyLeg.DepartureDisplayTime.Value, dummyLeg.ArrivalDisplayTime.Value);
                        foreach (var date in datesBetweenDictionary)
                        {
                            var RON = new FleetCalendarDataResult();
                            RON.LegID = dummyLeg.LegID;
                            RON.LegNUM = dummyLeg.LegNUM;
                            RON.TripID = dummyLeg.TripID;
                            RON.TripNUM = dummyLeg.TripNUM;
                            RON.TailNum = dummyLeg.TailNum;
                            RON.TripStatus = dummyLeg.TripStatus;
                            RON.StartDate = dummyLeg.StartDate;
                            RON.EndDate = dummyLeg.EndDate;
                            RON.IsAircraftStandby = dummyLeg.IsAircraftStandby;
                            RON.IsCrewStandBy = dummyLeg.IsCrewStandBy;
                            RON.IsInActive = dummyLeg.IsInActive;
                            RON.IsLog = dummyLeg.IsLog;
                            RON.IsPrivateLeg = dummyLeg.IsPrivateLeg;
                            RON.IsPrivateTrip = dummyLeg.IsPrivateTrip;
                            RON.ClientCD = dummyLeg.ClientCD;
                            RON.ClientID = dummyLeg.ClientID;
                            RON.HomebaseID = dummyLeg.HomebaseID;
                            RON.HomebaseCD = dummyLeg.HomebaseCD;
                            RON.Notes = dummyLeg.Notes;
                            RON.Description = dummyLeg.Description;

                            RON.DepartureDisplayTime = date.Key;
                            RON.ArrivalDisplayTime = date.Value;

                            RON.FlightCatagoryCD = dummyLeg.FlightCatagoryCD;
                            RON.FlightCatagoryDescription = dummyLeg.FlightCatagoryDescription;
                            RON.FlightCategoryBackColor = dummyLeg.FlightCategoryBackColor;
                            RON.FlightCategoryForeColor = dummyLeg.FlightCategoryForeColor;
                            RON.FlightCategoryID = dummyLeg.FlightCategoryID;

                            RON.FlightNUM = dummyLeg.FlightNUM;

                            RON.ArrivalICAOID = dummyLeg.ArrivalICAOID;
                            RON.ArrivalCity = dummyLeg.ArrivalCity;
                            RON.ArrivalAirportName = dummyLeg.ArrivalAirportName;

                            RON.AircaftDutyBackColor = dummyLeg.AircaftDutyBackColor;
                            RON.AircraftBackColor = dummyLeg.AircraftBackColor;
                            RON.AircraftDutyCD = dummyLeg.AircraftDutyCD;
                            RON.AircraftDutyDescription = dummyLeg.AircraftDutyDescription;
                            RON.AircraftDutyForeColor = dummyLeg.AircraftDutyForeColor;
                            RON.AircraftDutyID = dummyLeg.AircraftDutyID;
                            RON.AircraftForeColor = dummyLeg.AircraftForeColor;

                            RON.CrewCodes = dummyLeg.CrewCodes;
                            RON.CrewFullNames = dummyLeg.CrewFullNames;
                            RON.CumulativeETE = dummyLeg.CumulativeETE;
                            RON.TotalETE = dummyLeg.TotalETE;
                            RON.ElapseTM = dummyLeg.ElapseTM;
                            RON.FleetID = dummyLeg.FleetID;

                            RON.CrewDutyID = dummyLeg.CrewDutyID;
                            RON.CrewDutyTypeBackColor = dummyLeg.CrewDutyTypeBackColor;
                            RON.CrewDutyTypeCD = dummyLeg.CrewDutyTypeCD;
                            RON.CrewDutyTypeDescription = dummyLeg.CrewDutyTypeDescription;
                            RON.CrewDutyTypeForeColor = dummyLeg.CrewDutyTypeForeColor;

                            RON.LastUpdTS = dummyLeg.LastUpdTS;
                            RON.PassengerRequestorCD = dummyLeg.PassengerRequestorCD;

                            RON.DepartureICAOID = dummyLeg.DepartureICAOID;

                            RON.DepartureAirportName = dummyLeg.DepartureAirportName;
                            RON.DepartureCity = dummyLeg.DepartureCity;
                            RON.PassengerTotal = dummyLeg.PassengerTotal;
                            RON.PassengerCodes = dummyLeg.PassengerCodes;
                            RON.PassengerNames = dummyLeg.PassengerNames;
                            RON.FlightPurpose = dummyLeg.FlightPurpose;
                            RON.DepartmentCD = dummyLeg.DepartmentCD;
                            RON.AuthorizationCD = dummyLeg.AuthorizationCD;
                            RON.FuelLoad = dummyLeg.FuelLoad;
                            RON.OutbountInstruction = dummyLeg.OutbountInstruction;
                            RON.ReservationAvailable = dummyLeg.ReservationAvailable;
                            RON.RecordType = dummyLeg.RecordType;

                            RONLegsList.Add(RON);
                        }
                        resultSet.Remove(dummyLeg); // remove dummyLeg retrieve from db..
                        resultSet.AddRange(RONLegsList);
                        RONLegsList = new List<FleetCalendarDataResult>();
                    }


                    //Except dayview and FleetCalendarEntries- display records whose start date falls within input date range....
                    if (showRecordStartsInDateRange) // start date fall within date range = true
                    {
                        if (startDate.Date == endDate.Date) // day view
                        {
                            resultSet = resultSet.Where(x => x.DepartureDisplayTime.Value.Date == startDate.Date || x.DepartureDisplayTime.Value.Date == endDate.Date || x.ArrivalDisplayTime.Value.Date == startDate.Date || x.ArrivalDisplayTime.Value.Date == endDate.Date).ToList();
                        }
                       
                    }


                    List<FleetCalendarDataResult> calendarResult = new List<FleetCalendarDataResult>();
                    calendarResult.AddRange(resultSet.Where(x => x.FleetID.HasValue && fleetTreeInput.Contains(x.FleetID.Value.ToString())).ToList());

                    
                    if (!string.IsNullOrEmpty(filterEntity.HomeBaseID))
                    {
                        List<String> homeBaseIds = new List<String>(filterEntity.HomeBaseID.Split(','));
                        calendarResult = calendarResult.Where(x => x.HomebaseID.HasValue).ToList();
                        calendarResult = calendarResult.Where(x => homeBaseIds.Contains(x.HomebaseID.Value.ToString())).ToList();
                    }


                    List<FleetCalendarDataResult> tripData = calendarResult.Where(x => x.RecordType == "T").ToList();

                    //vendor filters
                    switch (filterEntity.VendorsFilter)
                    {
                        //include active vendors == both inactive and active vendors
                        case VendorOption.IncludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                        // only show active vendors  == active vendors
                        case VendorOption.OnlyShowActiveVendors: tripData = tripData.Where(x => x.IsInActive.HasValue && !x.IsInActive.Value).ToList();
                            break;
                        //exclude active vendors == Inactive vendors
                        case VendorOption.ExcludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || (x.IsInActive.HasValue && x.IsInActive.Value)).ToList();
                            break;

                        default: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                    }


                    //GET CREW CODES
                    foreach (var result in tripData)
                    {
                            var crewDictionary = GetCrewCodes(result.LegID, filterEntity.FixedWingCrewOnly, filterEntity.RotaryWingCrewCrewOnly, filterEntity.AllCrew);

                            result.CrewCodes = crewDictionary["CrewCodes"];
                            result.CrewFullNames = crewDictionary["CrewFullNames"];
                            
                            // set  passenger codes
                            if (retrievePaxDetails)
                            {
                                var passengerDictionary = GetPassengers(result.LegID);
                                result.PassengerCodes = passengerDictionary["PassengerCodes"];
                                result.PassengerNames = passengerDictionary["PassengerNames"];
                            }
                      
                    }

                    //Apply other filters

                  
                    
                    List<FleetCalendarDataResult> filteredTripResult = new List<FleetCalendarDataResult>();
                    // get data having only the selected status values and assign it to filteredResult
                    bool isStatusFilter = false;
                    if (filterEntity.Trip)
                    {
                        var dataWithStatusTrip = tripData.Where(x => x.TripStatus == "T").ToList();
                        filteredTripResult.AddRange(dataWithStatusTrip);
                        isStatusFilter = true;
                    }
                    if (filterEntity.WorkSheet)
                    {
                        var dataWithWorkSheetStatus = tripData.Where(x => x.TripStatus == "W").ToList();
                        filteredTripResult.AddRange(dataWithWorkSheetStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Canceled)
                    {
                        var dataWithCanceledStatus = tripData.Where(x => x.TripStatus == "X").ToList();
                        filteredTripResult.AddRange(dataWithCanceledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.UnFulfilled)
                    {
                        var dataWithUnFulfilledStatus = tripData.Where(x => x.TripStatus == "U").ToList();
                        filteredTripResult.AddRange(dataWithUnFulfilledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Hold)
                    {
                        var dataWithHoldStatus = tripData.Where(x => x.TripStatus == "H").ToList();
                        filteredTripResult.AddRange(dataWithHoldStatus);
                        isStatusFilter = true;
                    }

                    var calendarEntriesResult = calendarResult.Where(x => x.RecordType == "M" || x.RecordType == "C").ToList();
                    if (isStandByCrew) // if CrewStandBy checked 
                    {
                        var crewEntries = calendarEntriesResult.Where(x => x.RecordType == "C").ToList();
                        foreach (var result in crewEntries)
                        {
                            var crewDictionary = GetCrewCodes(result.LegID, false, false, false);

                            result.CrewCodes = crewDictionary["CrewCodes"];
                            result.CrewFullNames = crewDictionary["CrewFullNames"];
                        }
                    }

                    if (isStatusFilter)
                    {
                        finalList.EntityList.AddRange(filteredTripResult);
                    }
                    else
                    {
                        finalList.EntityList.AddRange(tripData);
                    }
                    finalList.EntityList.AddRange(calendarEntriesResult);
                    finalList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

                return finalList;
        }

        private static Dictionary<DateTime, DateTime> GetDates(DateTime startDate, DateTime endDate)
        {
            Dictionary<DateTime, DateTime> dates = new Dictionary<DateTime, DateTime>();
         
            DateTime toDate1 = startDate.AddDays(1).AddMinutes(-1); // include 14
            dates.Add(startDate, toDate1);

            while ((startDate = startDate.AddDays(1)) < endDate)
            {
                DateTime toDate = startDate.AddDays(1).AddMinutes(-1);
                dates.Add(startDate,toDate);
            }

            return dates;
        }
        //To get filtered result to Weekly Crew view...
        public ReturnValue<Data.Preflight.CrewCalendarDataResult> GetCrewCalendarData(DateTime startDate, DateTime endDate, FilterEntity filterEntity, Collection<string> crewTreeInput, bool showRecordStartsInDateRange)
        {

            ReturnValue<Data.Preflight.CrewCalendarDataResult> finalList = new ReturnValue<Data.Preflight.CrewCalendarDataResult>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, endDate, filterEntity, crewTreeInput, showRecordStartsInDateRange))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    finalList.EntityList = new List<CrewCalendarDataResult>();

                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    if (filterEntity.TimeBase == TimeBase.None)
                    {
                        filterEntity.TimeBase = TimeBase.Local;
                    }

                    long? clientId = null;
                    if (!String.IsNullOrEmpty(filterEntity.ClientID))
                    {
                        clientId = Convert.ToInt64(filterEntity.ClientID);
                    }

                    long? requestorId = null;
                    if (!String.IsNullOrEmpty(filterEntity.RequestorID))
                    {
                        requestorId = Convert.ToInt64(filterEntity.RequestorID);
                    }

                    long? departmentId = null;
                    if (!String.IsNullOrEmpty(filterEntity.DepartmentID))
                    {
                        departmentId = Convert.ToInt64(filterEntity.DepartmentID);
                    }

                    long? flightCategoryId = null;
                    if (!String.IsNullOrEmpty(filterEntity.FlightCategoryID))
                    {
                        flightCategoryId = Convert.ToInt64(filterEntity.FlightCategoryID);
                    }

                    long? crewDutyId = null;
                    if (!String.IsNullOrEmpty(filterEntity.CrewDutyID))
                    {
                        crewDutyId = Convert.ToInt64(filterEntity.CrewDutyID);
                    }

                    long? homeBaseOnlyId = null;
                    if (filterEntity.HomeBaseOnly)
                    {
                        // get user's home base id and filter
                        if (HomeBaseID != null && HomeBaseID != 0)
                        {
                            homeBaseOnlyId = HomeBaseID;
                        }
                    }

                    var resultSet = Container.GetCrewCalendarData(CustomerID, filterEntity.TimeBase.ToString(), startDate, endDate, clientId, requestorId,
                                                                    departmentId, flightCategoryId, crewDutyId, homeBaseOnlyId).ToList();

                    var RONLegsList = new List<CrewCalendarDataResult>();
                    // get RON records having enddate != start date (i.e ron greater than 1 day)
                    var dummyLegsHavingEndDateGreaterThanStartDate = resultSet.Where(x => x.LegNUM == 0 && x.ArrivalDisplayTime.Value.Date > x.DepartureDisplayTime.Value.Date).ToList();
                    // get the dates in between start date and endDate of RON record and split them
                    for (int i = 0; i < dummyLegsHavingEndDateGreaterThanStartDate.Count(); i++)
                    {
                        var dummyLeg = dummyLegsHavingEndDateGreaterThanStartDate[i];
                        var datesBetweenDictionary = GetDates(dummyLeg.DepartureDisplayTime.Value, dummyLeg.ArrivalDisplayTime.Value);
                        foreach (var date in datesBetweenDictionary)
                        {
                            var RON = new CrewCalendarDataResult();
                            RON.LegID = dummyLeg.LegID;
                            RON.LegNUM = dummyLeg.LegNUM;
                            RON.TripID = dummyLeg.TripID;
                            RON.TripNUM = dummyLeg.TripNUM;
                            RON.TailNum = dummyLeg.TailNum;
                            RON.TripStatus = dummyLeg.TripStatus;
                            RON.StartDate = dummyLeg.StartDate;
                            RON.EndDate = dummyLeg.EndDate;
                            RON.IsInActive = dummyLeg.IsInActive;
                            RON.IsLog = dummyLeg.IsLog;
                            RON.IsPrivateLeg = dummyLeg.IsPrivateLeg;
                            RON.IsPrivateTrip = dummyLeg.IsPrivateTrip;
                            RON.ClientCD = dummyLeg.ClientCD;
                            RON.ClientID = dummyLeg.ClientID;
                            RON.CrewID = dummyLeg.CrewID;
                            RON.CrewCD = dummyLeg.CrewCD;
                            RON.CrewFirstName = dummyLeg.CrewFirstName;
                            RON.CrewMiddleName = dummyLeg.CrewMiddleName;
                            RON.CrewLastName = dummyLeg.CrewLastName;
                            RON.CrewNotes = dummyLeg.CrewNotes;
                            RON.CrewStatus = dummyLeg.CrewStatus;
                            RON.IsFixedWing = dummyLeg.IsFixedWing;
                            RON.IsRotaryWing = dummyLeg.IsRotaryWing;
                            RON.HomebaseID = dummyLeg.HomebaseID;
                            RON.HomebaseCD = dummyLeg.HomebaseCD;
                            RON.Notes = dummyLeg.Notes;
                            RON.Description = dummyLeg.Description;

                            RON.DepartureDisplayTime = date.Key;
                            RON.ArrivalDisplayTime = date.Value;

                            RON.FlightCatagoryCD = dummyLeg.FlightCatagoryCD;
                            RON.FlightCatagoryDescription = dummyLeg.FlightCatagoryDescription;
                            RON.FlightCategoryBackColor = dummyLeg.FlightCategoryBackColor;
                            RON.FlightCategoryForeColor = dummyLeg.FlightCategoryForeColor;
                            RON.FlightCategoryID = dummyLeg.FlightCategoryID;

                            RON.FlightNUM = dummyLeg.FlightNUM;

                            RON.ArrivalICAOID = dummyLeg.ArrivalICAOID;
                            RON.ArrivalCity = dummyLeg.ArrivalCity;
                            RON.ArrivalAirportName = dummyLeg.ArrivalAirportName;

                            RON.AircaftDutyBackColor = dummyLeg.AircaftDutyBackColor;
                            RON.AircraftBackColor = dummyLeg.AircraftBackColor;
                            RON.AircraftDutyCD = dummyLeg.AircraftDutyCD;
                            RON.AircraftDutyDescription = dummyLeg.AircraftDutyDescription;
                            RON.AircraftDutyForeColor = dummyLeg.AircraftDutyForeColor;
                            RON.AircraftDutyID = dummyLeg.AircraftDutyID;
                            RON.AircraftForeColor = dummyLeg.AircraftForeColor;

                            RON.CrewCodes = dummyLeg.CrewCodes;
                            RON.CrewFullNames = dummyLeg.CrewFullNames;
                            RON.CumulativeETE = dummyLeg.CumulativeETE;
                            RON.TotalETE = dummyLeg.TotalETE;
                            
                            RON.ElapseTM = dummyLeg.ElapseTM;
                            RON.FleetID = dummyLeg.FleetID;

                            RON.CrewDutyID = dummyLeg.CrewDutyID;
                            RON.CrewDutyTypeBackColor = dummyLeg.CrewDutyTypeBackColor;
                            RON.CrewDutyTypeCD = dummyLeg.CrewDutyTypeCD;
                            RON.CrewDutyTypeDescription = dummyLeg.CrewDutyTypeDescription;
                            RON.CrewDutyTypeForeColor = dummyLeg.CrewDutyTypeForeColor;

                            RON.LastUpdTS = dummyLeg.LastUpdTS;
                            RON.PassengerRequestorCD = dummyLeg.PassengerRequestorCD;

                            RON.DepartureICAOID = dummyLeg.DepartureICAOID;

                            RON.DepartureAirportName = dummyLeg.DepartureAirportName;
                            RON.DepartureCity = dummyLeg.DepartureCity;
                            RON.PassengerTotal = dummyLeg.PassengerTotal;
                            RON.PassengerCodes = dummyLeg.PassengerCodes;
                            RON.PassengerNames = dummyLeg.PassengerNames;
                            RON.FlightPurpose = dummyLeg.FlightPurpose;
                            RON.DepartmentCD = dummyLeg.DepartmentCD;
                            RON.AuthorizationCD = dummyLeg.AuthorizationCD;
                            RON.ReservationAvailable = dummyLeg.ReservationAvailable;
                            RON.RecordType = dummyLeg.RecordType;

                           
                            RONLegsList.Add(RON);
                        }
                        resultSet.Remove(dummyLeg); // remove dummyLeg retrieve from db..
                        resultSet.AddRange(RONLegsList);
                        RONLegsList = new List<CrewCalendarDataResult>();
                    }


                    //Except dayview and FleetCalendarEntries- display records whose start date falls within input date range....
                    if (showRecordStartsInDateRange) // start date fall within date range = true
                    {
                        if (startDate.Date == endDate.Date) // day view
                        {
                            resultSet = resultSet.Where(x => x.DepartureDisplayTime.Value.Date == startDate.Date || x.DepartureDisplayTime.Value.Date == endDate.Date || x.ArrivalDisplayTime.Value.Date == startDate.Date || x.ArrivalDisplayTime.Value.Date == endDate.Date).ToList();
                        }

                    }


                    List<CrewCalendarDataResult> calendarResult = new List<CrewCalendarDataResult>();
                    calendarResult.AddRange(resultSet.Where(x => x.CrewID.HasValue && crewTreeInput.Contains(x.CrewID.Value.ToString())).ToList());

                    if (!string.IsNullOrEmpty(filterEntity.HomeBaseID))
                    {
                        List<String> homeBaseIds = new List<String>(filterEntity.HomeBaseID.Split(','));
                        calendarResult = calendarResult.Where(x => x.HomebaseID.HasValue).ToList();
                        calendarResult = calendarResult.Where(x => homeBaseIds.Contains(x.HomebaseID.Value.ToString())).ToList();
                    }
                    

                    //GET CREW CODES
                    foreach (var result in calendarResult)
                    {
                       
                            var crewDictionary = GetCrewCodes(result.LegID, filterEntity.FixedWingCrewOnly, filterEntity.RotaryWingCrewCrewOnly, filterEntity.AllCrew);

                            result.CrewCodes = crewDictionary["CrewCodes"];
                            result.CrewFullNames = crewDictionary["CrewFullNames"];
                            // set  passenger codes

                            var passengerDictionary = GetPassengers(result.LegID);
                            result.PassengerCodes = passengerDictionary["PassengerCodes"];
                            result.PassengerNames = passengerDictionary["PassengerNames"];
                        
                    }

                    List<CrewCalendarDataResult> tripData = calendarResult.Where(x => x.RecordType == "T").ToList();
                    //vendor filters
                    switch (filterEntity.VendorsFilter)
                    {
                        //include active vendors == both inactive and active vendors
                        case VendorOption.IncludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                        // only show active vendors  == active vendors
                        case VendorOption.OnlyShowActiveVendors: tripData = tripData.Where(x => x.IsInActive.HasValue && !x.IsInActive.Value).ToList();
                            break;
                        //exclude active vendors == Inactive vendors
                        case VendorOption.ExcludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || (x.IsInActive.HasValue && x.IsInActive.Value)).ToList();
                            break;

                        default: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                    }

                    tripData = ApplyFilters(filterEntity, Container, tripData);

                    if (filterEntity.FixedWingCrewOnly)
                    {
                        tripData = tripData.Where(x => x.IsFixedWing.HasValue && x.IsFixedWing.Value).ToList();
                    }
                    if (filterEntity.RotaryWingCrewCrewOnly)
                    {
                        tripData = tripData.Where(x => x.IsRotaryWing.HasValue && x.IsRotaryWing.Value).ToList();
                    }


                    List<CrewCalendarDataResult> filteredTripResult = new List<CrewCalendarDataResult>();
                    // get data having only the selected status values and assign it to filteredResult
                    bool isStatusFilter = false;
                    if (filterEntity.Trip)
                    {
                        var dataWithStatusTrip = tripData.Where(x => x.TripStatus == "T").ToList();
                        filteredTripResult.AddRange(dataWithStatusTrip);
                        isStatusFilter = true;
                    }
                    if (filterEntity.WorkSheet)
                    {
                        var dataWithWorkSheetStatus = tripData.Where(x => x.TripStatus == "W").ToList();
                        filteredTripResult.AddRange(dataWithWorkSheetStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Canceled)
                    {
                        var dataWithCanceledStatus = tripData.Where(x => x.TripStatus == "X").ToList();
                        filteredTripResult.AddRange(dataWithCanceledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.UnFulfilled)
                    {
                        var dataWithUnFulfilledStatus = tripData.Where(x => x.TripStatus == "U").ToList();
                        filteredTripResult.AddRange(dataWithUnFulfilledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Hold)
                    {
                        var dataWithHoldStatus = tripData.Where(x => x.TripStatus == "H").ToList();
                        filteredTripResult.AddRange(dataWithHoldStatus);
                        isStatusFilter = true;
                    }

                    var calendarEntriesResult = calendarResult.Where(x => x.RecordType == "C").ToList();
                    if (isStatusFilter)
                    {
                        finalList.EntityList.AddRange(filteredTripResult);
                    }
                    else
                    {
                        finalList.EntityList.AddRange(tripData);
                    }
                    finalList.EntityList.AddRange(calendarEntriesResult);


                    finalList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return finalList;
        }

        private List<CrewCalendarDataResult> ApplyFilters(FilterEntity filterEntity, Data.Preflight.PreflightDataModelContainer Container, List<CrewCalendarDataResult> calendarResult)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( filterEntity, Container, calendarResult))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //Apply other filters
                    if (!filterEntity.AllCrew)
                    {
                        calendarResult = calendarResult.Where(x => x.CrewStatus.HasValue && x.CrewStatus.Value).ToList();
                    }
                   
                   
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
           
            return calendarResult;
        }

        private Dictionary<string, string> GetCrewCodes(long legId, bool fixedWingOnly, bool rotorWingOnly, bool isAllCrew)
        {
            var dictionary = new Dictionary<string, string>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(legId, fixedWingOnly, rotorWingOnly, isAllCrew ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    List<string> codes = new List<string>();
                    List<string> fullNames = new List<string>();

                    var crews = Container.GetPreflightCrewsByLeg(CustomerID, legId, isAllCrew, fixedWingOnly, rotorWingOnly).ToList();
                    if (crews.Count > 0)
                    {
                        codes = crews.Select(x => x.CrewCD.Trim()).ToList(); // Removed spaces in crew codes.   DefectID : 2916                     
                        //codes = crews.Select(x => x.CrewCD).ToList(); 
                        fullNames = crews.Select(x => x.FullName).ToList();
                       
                    }
                    dictionary.Add("CrewCodes", string.Join(",", codes.ToArray()));                    
                    dictionary.Add("CrewFullNames", string.Join(";", fullNames.ToArray()));
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	

            return dictionary;
        }

        private Dictionary<string, string> GetPassengers(long legId)
        {
            var dictionary = new Dictionary<string, string>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(legId ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    Collection<string> codes = new Collection<string>();
                    Collection<string> fullNames = new Collection<string>();

                    List<string> passengerCodes = new List<string>();
                    List<string> passengerNames = new List<string>();
                    var passengers = Container.GetPreflightPassengersByLeg(CustomerID, legId).ToList();
                    if (passengers.Count > 0)
                    {
                        passengerCodes = passengers.Select(x => x.PassengerRequestorCD).ToList();
                        passengerNames = passengers.Select(x => x.FullName).ToList();
                       
                    }
                    dictionary.Add("PassengerCodes", string.Join(",", passengerCodes.ToArray()));
                    dictionary.Add("PassengerNames", string.Join(";", passengerNames.ToArray()));
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	

            return dictionary;
        }

        public ReturnValue<Data.Preflight.FleetResourceTypeResult> GetFleetResourceType(string vendorFilterInput, Collection<string> fleetIds, bool homebaseOnly, string homebaseIds)
        {
              ReturnValue<Data.Preflight.FleetResourceTypeResult> fleetResources = new ReturnValue<Data.Preflight.FleetResourceTypeResult>();
              using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorFilterInput, fleetIds, homebaseOnly, homebaseIds))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    if (fleetIds.Count > 0 && fleetIds[0] == "Fleet")
                    {
                        fleetIds.RemoveAt(0);
                    }

                   
                    // if hombase is selected in filter...
                    if (string.IsNullOrEmpty(homebaseIds))
                    {
                        homebaseIds = "0";
                    }

                    //if homebaseOnly is true, filter based on HomeBase ID
                    long userHomebaseId = HomeBaseID.HasValue ? HomeBaseID.Value : 0;
                    if (!homebaseOnly)
                    {
                        userHomebaseId = 0;
                    }

                    string fleetIdsString = string.Join(",", new Collection<string>(fleetIds));
                    fleetResources.EntityList = Container.GetFleetResourceType(CustomerID, ClientId, vendorFilterInput, fleetIdsString, homebaseIds, userHomebaseId).ToList();

                    if (fleetResources.EntityList != null)
                    {
                        fleetResources.EntityList = fleetResources.EntityList.Where(x => x.TailNum != null).ToList();
                    }
                  
                    ////if homebaseOnly is true, filter based on HomeBase ID
                    //if (homebaseOnly)
                    //{
                    //    fleetResources.EntityList = fleetResources.EntityList.Where(x => HomeBaseID != null && x.HomebaseID == HomeBaseID).ToList();
                    //}

                   
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return fleetResources;
        }

        /// <summary>
        /// To get Crew Resource
        /// </summary>
        /// <param name="crewIds">crew ids from tree view based</param>
        /// <returns></returns>
        public ReturnValue<Data.Preflight.CrewResourceTypeResult> GetCrewResourceType(Collection<string> crewIds, bool homebaseOnly, string homebaseIds, bool IsFixedCrewOnly, bool IsRotaryWingCrewOnly, string CalenderType)
        {
            ReturnValue<Data.Preflight.CrewResourceTypeResult> crewResources = new ReturnValue<Data.Preflight.CrewResourceTypeResult>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewIds, homebaseOnly, homebaseIds))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    if (crewIds.Count > 0 && crewIds[0] == "Crew")
                    {
                        crewIds.RemoveAt(0);
                    }

                    // if hombase is selected in filter...
                    if (string.IsNullOrEmpty(homebaseIds))
                    {
                        homebaseIds = "0";
                    }
                    
                    //if homebaseOnly is true, filter based on HomeBase ID
                    long userHomebaseId = HomeBaseID.HasValue ? HomeBaseID.Value : 0;
                    if (!homebaseOnly)
                    {
                        userHomebaseId = 0;
                    }
                    string crewIdsString = string.Join(",", new Collection<string>(crewIds));
                    crewResources.EntityList = Container.GetCrewResourceType(CustomerID, ClientId, crewIdsString, homebaseIds, userHomebaseId, IsFixedCrewOnly, IsRotaryWingCrewOnly, UserName, CalenderType).ToList();

                    if (crewResources.EntityList != null)
                    {
                        crewResources.EntityList = crewResources.EntityList.Where(x => x.CrewCD != null).ToList();
                    }

                    //if homebaseOnly is true, filter based on HomeBase ID
                    //if (homebaseOnly)
                    //{
                    //    crewResources.EntityList = crewResources.EntityList.Where(x => HomeBaseID != null && x.HomebaseID == HomeBaseID).ToList();
                    //}
                    

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return crewResources;
        }

        public ReturnValue<Data.Preflight.LegendsResult> GetAllLegends()
        {
            ReturnValue<Data.Preflight.LegendsResult> legends = new ReturnValue<Data.Preflight.LegendsResult>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    legends.EntityList = Container.GetAllLegends(CustomerID).ToList();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return legends;
        }

        public ReturnValue<Data.Preflight.FleetPlannerCalendarDataResult> GetFleetPlannerCalendarData(DateTime startDate, short timeinterval, FilterEntity filterEntity, Collection<string> fleetTreeInput, bool IsTrip, bool IsHold, bool IsWorkSheet, bool IsCanceled, bool IsUnFulfilled)
        {

            ReturnValue<Data.Preflight.FleetPlannerCalendarDataResult> finalList = new ReturnValue<Data.Preflight.FleetPlannerCalendarDataResult>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, timeinterval, filterEntity, fleetTreeInput))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    finalList.EntityList = new List<FleetPlannerCalendarDataResult>();

                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    if (filterEntity.TimeBase == TimeBase.None)
                    {
                        filterEntity.TimeBase = TimeBase.Local;
                    }


                    var resultSet = Container.GetFleetPlannerCalendarData(CustomerID, startDate, timeinterval, filterEntity.TimeBase.ToString(), IsTrip, IsHold, IsWorkSheet, IsCanceled, IsUnFulfilled).ToList();

                    List<FleetPlannerCalendarDataResult> calendarResult = new List<FleetPlannerCalendarDataResult>();
                    calendarResult.AddRange(resultSet.Where(x => x.FleetID.HasValue && fleetTreeInput.Contains(x.FleetID.Value.ToString())).ToList());
                   
                    //Changed as per the Global Filters
                    if (!string.IsNullOrEmpty(filterEntity.ClientID))
                    {
                        calendarResult = calendarResult.Where(x => x.ClientID.HasValue && x.ClientID == Utility.ConvertToInt64(filterEntity.ClientID)).ToList();
                    }                   
                    if (!string.IsNullOrEmpty(filterEntity.HomeBaseID))
                    {
                        List<String> homeBaseIds = new List<String>(filterEntity.HomeBaseID.Split(','));
                        calendarResult = calendarResult.Where(x => x.HomebaseID.HasValue).ToList();
                        calendarResult = calendarResult.Where(x => homeBaseIds.Contains(x.HomebaseID.Value.ToString())).ToList();
                    }
                    if (filterEntity.HomeBaseOnly)
                    {                       
                        calendarResult = calendarResult.Where(x => x.HomebaseID.HasValue && HomeBaseID.HasValue && x.HomebaseID.Value == HomeBaseID.Value).ToList();
                    }

                    List<FleetPlannerCalendarDataResult> tripData = calendarResult.Where(x => x.RecordType == "T").ToList();
                    //vendor filters
                    switch (filterEntity.VendorsFilter)
                    {
                        //include active vendors == both inactive and active vendors
                        case VendorOption.IncludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                        // only show active vendors  == active vendors
                        case VendorOption.OnlyShowActiveVendors: tripData = tripData.Where(x => x.IsInActive.HasValue && !x.IsInActive.Value).ToList();
                            break;
                        //exclude active vendors == Inactive vendors
                        case VendorOption.ExcludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || (x.IsInActive.HasValue && x.IsInActive.Value)).ToList();
                            break;

                        default: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                    }

                    //GET CREW CODES
                    foreach (var result in tripData)
                    {
                        var crewDictionary = GetCrewCodes(result.LegID, filterEntity.FixedWingCrewOnly, filterEntity.RotaryWingCrewCrewOnly, filterEntity.AllCrew);

                        result.CrewCodes = crewDictionary["CrewCodes"];
                        result.CrewFullNames = crewDictionary["CrewFullNames"];                        
                    }

                    //Apply other filters
                   
                    if (!string.IsNullOrEmpty(filterEntity.RequestorID))
                    {
                        tripData = tripData.Where(x => x.PassengerRequestorID.HasValue && x.PassengerRequestorID == Utility.ConvertToInt64(filterEntity.RequestorID)).ToList();
                    }
                    if (!string.IsNullOrEmpty(filterEntity.DepartmentID))
                    {
                        tripData = tripData.Where(x => x.DepartmentID.HasValue && x.DepartmentID == Utility.ConvertToInt64(filterEntity.DepartmentID)).ToList();
                    }

                    if (!string.IsNullOrEmpty(filterEntity.FlightCategoryID))
                    {
                        tripData = tripData.Where(x => x.FlightCategoryID.HasValue && x.FlightCategoryID == Utility.ConvertToInt64(filterEntity.FlightCategoryID)).ToList();
                    } 

                    List<FleetPlannerCalendarDataResult> filteredTripResult = new List<FleetPlannerCalendarDataResult>();
                    // get data having only the selected status values and assign it to filteredResult
                    bool isStatusFilter = false;
                    if (filterEntity.Trip)
                    {
                        var dataWithStatusTrip = tripData.Where(x => x.TripStatus == "T").ToList();
                        filteredTripResult.AddRange(dataWithStatusTrip);
                        isStatusFilter = true;
                    }
                    if (filterEntity.WorkSheet)
                    {
                        var dataWithWorkSheetStatus = tripData.Where(x => x.TripStatus == "W").ToList();
                        filteredTripResult.AddRange(dataWithWorkSheetStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Canceled)
                    {
                        var dataWithCanceledStatus = tripData.Where(x => x.TripStatus == "X").ToList();
                        filteredTripResult.AddRange(dataWithCanceledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.UnFulfilled)
                    {
                        var dataWithUnFulfilledStatus = tripData.Where(x => x.TripStatus == "U").ToList();
                        filteredTripResult.AddRange(dataWithUnFulfilledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Hold)
                    {
                        var dataWithHoldStatus = tripData.Where(x => x.TripStatus == "H").ToList();
                        filteredTripResult.AddRange(dataWithHoldStatus);
                        isStatusFilter = true;
                    }

                    var calendarEntriesResult = calendarResult.Where(x => x.RecordType == "M" || x.RecordType == "C").ToList();
                    if (isStatusFilter)
                    {
                        finalList.EntityList.AddRange(filteredTripResult);
                    }
                    else
                    {
                        finalList.EntityList.AddRange(tripData);
                    }
                    finalList.EntityList.AddRange(calendarEntriesResult);

                    finalList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return finalList;
        }

        public ReturnValue<Data.Preflight.CrewPlannerCalendarDataResult> GetCrewPlannerCalendarData(DateTime startDate, short timeinterval, FilterEntity filterEntity, Collection<string> crewTreeInput, bool isTrip, bool isHold,  bool isWorkSheet, bool isCanceled, bool isUnFulfilled)
        {

            ReturnValue<Data.Preflight.CrewPlannerCalendarDataResult> finalList = new ReturnValue<Data.Preflight.CrewPlannerCalendarDataResult>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(startDate, timeinterval, filterEntity, crewTreeInput, isTrip, isHold, isWorkSheet, isCanceled, isUnFulfilled))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    finalList.EntityList = new List<CrewPlannerCalendarDataResult>();

                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    if (filterEntity.TimeBase == TimeBase.None)
                    {
                        filterEntity.TimeBase = TimeBase.Local;
                    }

                    var resultSet = Container.GetCrewPlannerCalendarData(CustomerID, startDate, timeinterval, filterEntity.TimeBase.ToString(), isTrip, isHold, isWorkSheet, isCanceled, isUnFulfilled).ToList();

                    List<CrewPlannerCalendarDataResult> calendarResult = new List<CrewPlannerCalendarDataResult>();
                    calendarResult.AddRange(resultSet.Where(x => x.CrewID.HasValue && crewTreeInput.Contains(x.CrewID.Value.ToString())).ToList());
                    //Changed as per the Global Filter
                    if (!string.IsNullOrEmpty(filterEntity.ClientID))
                    {
                        calendarResult = calendarResult.Where(x => x.ClientID.HasValue && x.ClientID == Utility.ConvertToInt64(filterEntity.ClientID)).ToList();
                    }

                    if (!string.IsNullOrEmpty(filterEntity.CrewDutyID))
                    {
                        calendarResult = calendarResult.Where(x => x.CrewDutyID.HasValue && x.CrewDutyID == Utility.ConvertToInt64(filterEntity.CrewDutyID)).ToList();
                    }
                    if (!string.IsNullOrEmpty(filterEntity.HomeBaseID))
                    {
                        List<String> homeBaseIds = new List<String>(filterEntity.HomeBaseID.Split(','));
                        calendarResult = calendarResult.Where(x => x.HomebaseID.HasValue).ToList();
                        calendarResult = calendarResult.Where(x => homeBaseIds.Contains(x.HomebaseID.Value.ToString())).ToList();
                    }
                    if (!string.IsNullOrEmpty(filterEntity.RequestorID))
                    {
                        calendarResult = calendarResult.Where(x => x.PassengerRequestorID.HasValue && x.PassengerRequestorID == Utility.ConvertToInt64(filterEntity.RequestorID)).ToList();
                    }
                    if (!string.IsNullOrEmpty(filterEntity.DepartmentID))
                    {
                        calendarResult = calendarResult.Where(x => x.DepartmentID.HasValue && x.DepartmentID == Utility.ConvertToInt64(filterEntity.DepartmentID)).ToList();
                    }

                    if (!string.IsNullOrEmpty(filterEntity.FlightCategoryID))
                    {
                        calendarResult = calendarResult.Where(x => x.FlightCategoryID.HasValue && x.FlightCategoryID == Utility.ConvertToInt64(filterEntity.FlightCategoryID)).ToList();
                    }

                    if (filterEntity.HomeBaseOnly)
                    {                       
                        calendarResult = calendarResult.Where(x => x.HomebaseID.HasValue && HomeBaseID.HasValue && x.HomebaseID.Value == HomeBaseID.Value).ToList();
                    }
                    
                    List<CrewPlannerCalendarDataResult> tripData = calendarResult.Where(x => x.RecordType == "T").ToList();
                    //vendor filters
                    switch (filterEntity.VendorsFilter)
                    {
                        //include active vendors == both inactive and active vendors
                        case VendorOption.IncludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                        // only show active vendors  == active vendors
                        case VendorOption.OnlyShowActiveVendors: tripData = tripData.Where(x => x.IsInActive.HasValue && !x.IsInActive.Value).ToList();
                            break;
                        //exclude active vendors == Inactive vendors
                        case VendorOption.ExcludeActiveVendors: tripData = tripData.Where(x => !x.IsInActive.HasValue || (x.IsInActive.HasValue && x.IsInActive.Value)).ToList();
                            break;

                        default: tripData = tripData.Where(x => !x.IsInActive.HasValue || x.IsInActive.HasValue).ToList();
                            break;
                    }

                    if (!filterEntity.AllCrew)
                    {
                        tripData = tripData.Where(x => x.CrewStatus.HasValue && x.CrewStatus.Value).ToList();
                    }
                   

                    if (filterEntity.FixedWingCrewOnly)
                    {
                        tripData = tripData.Where(x => x.IsFixedWing.HasValue && x.IsFixedWing.Value).ToList();
                    }
                    if (filterEntity.RotaryWingCrewCrewOnly)
                    {
                        tripData = tripData.Where(x => x.IsRotaryWing.HasValue && x.IsRotaryWing.Value).ToList();
                    }


                    List<CrewPlannerCalendarDataResult> filteredTripResult = new List<CrewPlannerCalendarDataResult>();
                    // get data having only the selected status values and assign it to filteredResult
                    bool isStatusFilter = false;
                    if (filterEntity.Trip)
                    {
                        var dataWithStatusTrip = tripData.Where(x => x.TripStatus == "T").ToList();
                        filteredTripResult.AddRange(dataWithStatusTrip);
                        isStatusFilter = true;
                    }
                    if (filterEntity.WorkSheet)
                    {
                        var dataWithWorkSheetStatus = tripData.Where(x => x.TripStatus == "W").ToList();
                        filteredTripResult.AddRange(dataWithWorkSheetStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Canceled)
                    {
                        var dataWithCanceledStatus = tripData.Where(x => x.TripStatus == "X").ToList();
                        filteredTripResult.AddRange(dataWithCanceledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.UnFulfilled)
                    {
                        var dataWithUnFulfilledStatus = tripData.Where(x => x.TripStatus == "U").ToList();
                        filteredTripResult.AddRange(dataWithUnFulfilledStatus);
                        isStatusFilter = true;
                    }
                    if (filterEntity.Hold)
                    {
                        var dataWithHoldStatus = tripData.Where(x => x.TripStatus == "H").ToList();
                        filteredTripResult.AddRange(dataWithHoldStatus);
                        isStatusFilter = true;
                    }

                    var calendarEntriesResult = calendarResult.Where(x => x.RecordType == "C").ToList();
                    if (isStatusFilter)
                    {
                        finalList.EntityList.AddRange(filteredTripResult);
                    }
                    else
                    {
                        finalList.EntityList.AddRange(tripData);
                    }
                    finalList.EntityList.AddRange(calendarEntriesResult);


                    finalList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return finalList;
        }

        #endregion

       
      

        #region "Schedule Calendar Fleet Entries From Context menu"
        /// <summary>
        /// Get the Fleet Calendar Entries
        /// </summary>
        /// <param name="timebase"></param>
        /// <returns></returns>
        public ReturnValue<Data.Preflight.FleetCalendarEntriesResult> GetFleetCalendarEntries(string timebase, DateTime startDate, DateTime endDate)
        { //FilterEntity filterEntity
            ReturnValue<Data.Preflight.FleetCalendarEntriesResult> finalList = new ReturnValue<Data.Preflight.FleetCalendarEntriesResult>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timebase, startDate, endDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    //// Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //if (filterEntity.TimeBase == TimeBase.None)
                    //{
                    //    filterEntity.TimeBase = TimeBase.Local;
                    //}
                    // FlightPak.Business.Preflight.ScheduleCalendarEntities.TimeBase tb = new FlightPak.Business.Preflight.ScheduleCalendarEntities.TimeBase();

                    var resultSet = Container.GetFleetCalendarEntries(CustomerID, timebase.ToString(), startDate, endDate).ToList();
                    List<FleetCalendarEntriesResult> FleetcalendarResult = new List<FleetCalendarEntriesResult>();
                    FleetcalendarResult.AddRange(resultSet);
                    finalList.EntityList = FleetcalendarResult;
                    finalList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return finalList;
        }

        /// <summary>
        /// Insert the Vales for Fleet Calendar Entries
        /// </summary>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>

        public void AddFleetCalendarEntry(string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, Int64 clientid, Int64 fleetid, string notesec)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tailnumber, homebaseid, timebase, startdate, enddate, icaoid, dutytype, notes, clientid, fleetid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.AddFleetCalendarEntries(CustomerID, tailnumber, homebaseid, UserName, startdate, enddate, startdate, enddate, startdate, enddate, icaoid, dutytype, notes, clientid, fleetid, notesec);
                    Container.SaveChanges();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
        }
        /// <summary>
        /// To update values into DB from Fleet Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        public void UpdateFleetCalendarEntry(Int64 fleetid, string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, Int64 legid, Int64 tripid, Int64 clientid, string notesec)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetid, tailnumber, homebaseid, timebase, startdate, enddate, icaoid, dutytype, notes, legid, tripid, clientid ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.UpdateFleetCalendarEntries(fleetid, CustomerID, tailnumber, homebaseid, UserName, startdate, enddate, startdate, enddate, startdate, enddate, icaoid, dutytype, notes, legid, tripid, clientid, notesec);
                    Container.SaveChanges();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
        }
        /// <summary>
        /// Delete Fleet Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        public void DeleteFleetCalendarEntry(Int64 fleetid, Int64 legid, Int64 tripid)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetid, legid, tripid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.DeleteFleetCalendarEntries(fleetid, CustomerID, UserName, legid, tripid);
                    Container.SaveChanges();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
        }

        #endregion

        #region "Schedule Calendar Crew Entries From Context menu"
        /// <summary>
        /// Get the Crew Calendar Entries
        /// </summary>
        /// <param name="timebase"></param>
        /// <returns></returns>
        public ReturnValue<Data.Preflight.CrewCalendarEntriesResult> GetCrewCalendarEntries(string timebase, DateTime startDate, DateTime endDate)
        {
            ReturnValue<Data.Preflight.CrewCalendarEntriesResult> finalList = new ReturnValue<Data.Preflight.CrewCalendarEntriesResult>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(timebase, startDate, endDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    //// Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    var resultSet = Container.GetCrewCalendarEntries(CustomerID, timebase.ToString(), startDate, endDate).ToList();
                    List<CrewCalendarEntriesResult> CrewcalendarResult = new List<CrewCalendarEntriesResult>();
                    CrewcalendarResult.AddRange(resultSet);
                    finalList.EntityList = CrewcalendarResult;
                    finalList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return finalList;
        }

        /// <summary>
        ///  Insert the Vales for Crew Calendar Entries
        /// </summary>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        /// <param name="clientid"></param>
        /// <param name="CrewIDs"></param>
        public void AddCrewCalendarEntry(string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, Int64 clientid, string CrewIDs, Int64 fleetid, string notesec)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tailnumber, homebaseid, timebase, startdate, enddate, icaoid, dutytype, notes, clientid, CrewIDs, fleetid))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    DateTime DepartureDTTMLocal = CalculationManager.GetGMT(Convert.ToInt64(icaoid), startdate, false, false);
                    DateTime ArrivalDTTMLocal = CalculationManager.GetGMT(Convert.ToInt64(icaoid), enddate, false, false);
                    DateTime DepartureGreenwichDTTM = CalculationManager.GetGMT(Convert.ToInt64(icaoid), startdate, true, true);
                    DateTime ArrivalGreenwichDTTM = CalculationManager.GetGMT(Convert.ToInt64(icaoid), enddate, true, true);
                    DateTime HomeDepartureDTTM = CalculationManager.GetGMT(Convert.ToInt64(homebaseid), startdate, false, false);
                    DateTime HomeArrivalDTTM = CalculationManager.GetGMT(Convert.ToInt64(homebaseid), enddate, false, false);

                    Container.AddCrewCalendarEntries(CustomerID, tailnumber, homebaseid, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM, ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, icaoid, dutytype, notes, clientid, CrewIDs, fleetid, UserName, notesec);
                    Container.SaveChanges();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
        }
        /// <summary>
        /// update Crew Calendar Entries From Context Menu
        /// </summary>
        /// <param name="fleetid"></param>
        /// <param name="tailnumber"></param>
        /// <param name="homebaseid"></param>
        /// <param name="timebase"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="icaoid"></param>
        /// <param name="dutytype"></param>
        /// <param name="notes"></param>
        /// <param name="legid"></param>
        /// <param name="tripid"></param>
        /// <param name="clientid"></param>
        /// <param name="PreviousNUM"></param>
        /// <param name="CrewID"></param>
        public void UpdateCrewCalendarEntry(Int64 fleetid, string tailnumber, Int64 homebaseid, string timebase, DateTime startdate, DateTime enddate, Int64 icaoid, string dutytype, string notes, string legid, string tripid, Int64 clientid, Int64 PreviousNUM, string CrewID, string notesec)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetid, tailnumber, homebaseid, timebase, startdate, enddate, icaoid, dutytype, notes, legid, tripid, clientid, PreviousNUM, CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;


                    DateTime DepartureDTTMLocal = CalculationManager.GetGMT(Convert.ToInt64(icaoid), startdate, false, false);
                    DateTime ArrivalDTTMLocal = CalculationManager.GetGMT(Convert.ToInt64(icaoid), enddate, false, false);
                    DateTime DepartureGreenwichDTTM = CalculationManager.GetGMT(Convert.ToInt64(icaoid), startdate, true, true);
                    DateTime ArrivalGreenwichDTTM = CalculationManager.GetGMT(Convert.ToInt64(icaoid), enddate, true, true);
                    DateTime HomeDepartureDTTM = CalculationManager.GetGMT(Convert.ToInt64(homebaseid), startdate, false, false);
                    DateTime HomeArrivalDTTM = CalculationManager.GetGMT(Convert.ToInt64(homebaseid), enddate, false, false);

                    Container.UpdateCrewCalendarEntries(fleetid, CustomerID, tailnumber, homebaseid, UserName, DepartureDTTMLocal, ArrivalDTTMLocal, DepartureGreenwichDTTM, ArrivalGreenwichDTTM, HomeDepartureDTTM, HomeArrivalDTTM, icaoid, dutytype, notes, clientid, PreviousNUM, legid, tripid, CrewID, notesec);
                    Container.SaveChanges();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
        }
        /// <summary>
        /// Delete Crew Calendar Entries
        /// </summary>
        /// <param name="fleetid"></param>
        public void DeleteCrewCalendarEntry(string legid, string tripid, string crewids)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(legid, tripid, crewids))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.DeleteCrewCalendarEntries(CustomerID, UserName, legid, tripid, crewids);
                    Container.SaveChanges();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
        }

        #endregion

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        #region Tree view

        public ReturnValue<Data.Preflight.FleetTreeResult> GetFleetInfoForFleetTree(bool homebaseOnly)
        {
            ReturnValue<Data.Preflight.FleetTreeResult> fleetInfoForFleetTreeResult = new ReturnValue<Data.Preflight.FleetTreeResult>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    //if homebaseOnly is true, filter based on HomeBase ID
                    long userHomebaseId = HomeBaseID.HasValue ? HomeBaseID.Value : 0;
                    if (!homebaseOnly)
                    {
                        userHomebaseId = 0;
                    }
                     fleetInfoForFleetTreeResult.EntityList = Container.GetFleetInfoForFleetTree(CustomerID, ClientId, userHomebaseId).ToList(); 
                    //if homebaseOnly is true, filter based on HomeBase ID
                    // if (homebaseOnly)
                    //{
                    //    fleetInfoForFleetTreeResult.EntityList = fleetInfoForFleetTreeResult.EntityList.Where(x => HomeBaseID != null && x.HomebaseID == HomeBaseID).ToList();
                    //}
                    
                   
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return fleetInfoForFleetTreeResult;
        }

        public ReturnValue<Data.Preflight.CrewTreeResult> GetCrewInfoForCrewTree(bool homebaseOnly)
        {
            ReturnValue<Data.Preflight.CrewTreeResult> crewInfoForCrewTreeResult = new ReturnValue<Data.Preflight.CrewTreeResult>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseOnly))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Preflight.PreflightDataModelContainer Container = new Data.Preflight.PreflightDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    //if homebaseOnly is true, filter based on HomeBase ID
                    long userHomebaseId = HomeBaseID.HasValue ? HomeBaseID.Value : 0;
                    if (!homebaseOnly)
                    {
                        userHomebaseId = 0;
                    }
                    crewInfoForCrewTreeResult.EntityList  = Container.GetCrewInfoForCrewTree(CustomerID, ClientId, userHomebaseId).ToList(); 
                    //if homebaseOnly is true, filter based on HomeBase ID
                    //if (homebaseOnly)
                    //{
                    //    crewInfoForCrewTreeResult.EntityList = crewInfoForCrewTreeResult.EntityList .Where(x => HomeBaseID != null && x.HomebaseID == HomeBaseID).ToList();
                    //}
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return crewInfoForCrewTreeResult;
        }
          #endregion
    }

   
}
