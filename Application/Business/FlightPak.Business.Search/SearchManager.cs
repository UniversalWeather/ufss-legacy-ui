﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Objects;

//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;
using FlightPak.Business.Common;

namespace FlightPak.Business.Search
{
    public class SearchManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Search.SearchResult> SimpleSearch(string SearchStr, string Module, int PageNumber, int PageSize)
        {
            ReturnValue<Data.Search.SearchResult> ReturnValue = null;
            Data.Search.SearchDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Search.SearchResult>();
                    objContainer = new Data.Search.SearchDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    
                    
                    //ReturnValue.EntityList = objContainer.FlightPakSimpleSearch(SearchStr, base.UserPrincipal.Identity.SessionID, base.CustomerID, ModuleName, false, PageNumber, PageSize).ToList();
                    ReturnValue.EntityList = objContainer.FlightPakSimpleSearch(SearchStr, base.UserPrincipal.Identity.SessionID, Module, 1, 1).ToList();

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public ReturnValue<Data.Search.FlightPakHelpContentSearch> HelpContentSearch(string SearchStr)
        {
            ReturnValue<Data.Search.FlightPakHelpContentSearch> ReturnValue = null;
            Data.Search.SearchDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Search.FlightPakHelpContentSearch>();
                    objContainer = new Data.Search.SearchDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    
                    ReturnValue.EntityList = objContainer.FlightPakHelpContentSearch(SearchStr).ToList();

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
    }
}
