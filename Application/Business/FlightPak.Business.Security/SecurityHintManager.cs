﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Business.Common;
using FlightPak.Framework.Encryption;

namespace FlightPak.Business.Security
{

    public class SecurityHintManager
    {
        /// <summary>
        /// 
        /// </summary>
        private ExceptionManager exManager;

        /// <summary>
        /// Add or Update User Securtiy Hint
        /// </summary>
        /// <param name="userSecurtiyHint"></param>
        /// <returns></returns>
        public ReturnValue<FlightPak.Framework.Data.UserSecurityHint> AddOrUpdate(FlightPak.Framework.Data.UserSecurityHint userSecurtiyHint)
        {
            ReturnValue<FlightPak.Framework.Data.UserSecurityHint> objUserSecurityHint = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                objUserSecurityHint = new ReturnValue<FlightPak.Framework.Data.UserSecurityHint>();
                Crypto crypto = new Crypto();
                userSecurtiyHint.SecurityAnswer = crypto.Encrypt(userSecurtiyHint.SecurityAnswer);
                userSecurtiyHint.SecurityQuestion = crypto.Encrypt(userSecurtiyHint.SecurityQuestion);
                using (FlightPak.Framework.Data.FrameworkModelContainer objContainer = new FlightPak.Framework.Data.FrameworkModelContainer())
                {
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    bool UpdateFlag = false;
                    UpdateFlag = objContainer.GetUserSecurityHintByUserName(userSecurtiyHint.UserName).Count() > 0;
                    if (UpdateFlag)
                    {
                        objContainer.UserSecurityHints.Attach(userSecurtiyHint);
                        objContainer.ObjectStateManager.ChangeObjectState(userSecurtiyHint, System.Data.EntityState.Modified);
                        objContainer.SaveChanges();
                    }
                    else
                    {
                        objContainer.AddToUserSecurityHints(userSecurtiyHint);
                        objContainer.SaveChanges();
                    }
                }
                objUserSecurityHint.ReturnFlag = true;
            }, FlightPak.Common.Constants.Policy.DataLayer);
            return objUserSecurityHint;
        }

        /// <summary>
        /// Get User Security Hint By User Name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public ReturnValue<FlightPak.Framework.Data.UserSecurityHint> GetUserSecurityHintByUserName(string userName)
        {
            ReturnValue<FlightPak.Framework.Data.UserSecurityHint> objUserSecurityHint = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                objUserSecurityHint = new ReturnValue<FlightPak.Framework.Data.UserSecurityHint>();
                using (FlightPak.Framework.Data.FrameworkModelContainer objContainer = new FlightPak.Framework.Data.FrameworkModelContainer())
                {
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objUserSecurityHint.EntityInfo = objContainer.GetUserSecurityHintByUserName(userName).FirstOrDefault();
                    objUserSecurityHint.ReturnFlag = true;
                }
            }, FlightPak.Common.Constants.Policy.DataLayer);
            return objUserSecurityHint;
        }

        /// <summary>
        /// Get User Security Hint By EmailId
        /// </summary>
        /// <param name="EMailId"></param>
        /// <returns></returns>
        public ReturnValue<FlightPak.Framework.Data.UserSecurityHint> GetUserSecurityHintByEmailId(string EMailId)
        {
            ReturnValue<FlightPak.Framework.Data.UserSecurityHint> objUserSecurityHint = null;
            //Handle methods throguh exception manager with return flag
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                objUserSecurityHint = new ReturnValue<FlightPak.Framework.Data.UserSecurityHint>();
                using (FlightPak.Framework.Data.FrameworkModelContainer objContainer = new FlightPak.Framework.Data.FrameworkModelContainer())
                {
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objUserSecurityHint.EntityInfo = objContainer.GetUserSecurityHintByEmailId(EMailId).FirstOrDefault();
                    Crypto crypto = new Crypto();
                    if (objUserSecurityHint.EntityInfo != null && !string.IsNullOrEmpty(objUserSecurityHint.EntityInfo.SecurityQuestion))
                    {
                        objUserSecurityHint.EntityInfo.SecurityQuestion = crypto.Decrypt(objUserSecurityHint.EntityInfo.SecurityQuestion);
                    }
                    if (objUserSecurityHint.EntityInfo != null && !string.IsNullOrEmpty(objUserSecurityHint.EntityInfo.SecurityAnswer))
                    {
                        objUserSecurityHint.EntityInfo.SecurityAnswer = crypto.Decrypt(objUserSecurityHint.EntityInfo.SecurityAnswer);
                    }
                    objUserSecurityHint.ReturnFlag = true;
                }
            }, FlightPak.Common.Constants.Policy.DataLayer);
            return objUserSecurityHint;
        }
    }
}
