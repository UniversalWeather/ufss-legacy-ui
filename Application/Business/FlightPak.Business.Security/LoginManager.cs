﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;

namespace FlightPak.Business.Security
{
    public class LoginManager
    {
        private ExceptionManager exManager;
        public string GetSaltValue(string UserName)
        {
            //return "ABC";
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<string>(() =>
                {
                    //return string.Empty;
                    using (Framework.Data.FrameworkModelContainer container = new Framework.Data.FrameworkModelContainer())
                    {
                        //Salt length & Password length should be retrived from constants
                        return container.GetSaltValue(UserName, 5, 44).FirstOrDefault();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }

        /// <summary>
        /// This method is used to construct the principal based on login session id
        /// </summary>
        /// <param name="SessionID"></param>
        /// <returns></returns>
        public FlightPak.Framework.Security.FPPrincipal GetUserPrincipal(string SessionID)
        {
            Guid _sessionID;
            if (!Guid.TryParse(SessionID, out _sessionID))
            {
                throw new ArgumentException("Invalid Session");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SessionID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<Framework.Security.FPPrincipal>(() =>
                {
                    return new Framework.Security.FPPrincipal(_sessionID, false);
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        /// <summary>
        /// This method is used to Authenticate an user
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <param name="SessionId"></param>
        /// <returns></returns>
        public bool Login(string UserName, string Password, Guid SessionId, Int32 InvalidLoginCount)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName, Password, SessionId, InvalidLoginCount))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Framework.Data.FrameworkModelContainer container = new Framework.Data.FrameworkModelContainer())
                    {
                        var returnValue = container.Login(SessionId, UserName, Password.ToString(), InvalidLoginCount).FirstOrDefault();
                        return returnValue.HasValue ? returnValue.Value > 0 : false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        /// <summary>
        /// This method is used to clear the login records when the user clicks Log out button
        /// </summary>
        /// <param name="SessionId"></param>
        public void LogOut(string SessionId)
        {
            Guid _sessionID;
            if (!Guid.TryParse(SessionId, out _sessionID))
            {
                throw new ArgumentException("Invalid GUID");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SessionId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Framework.Data.FrameworkModelContainer container = new Framework.Data.FrameworkModelContainer())
                    {
                        // Clear the value from FPLoginSession
                        container.Logout(_sessionID);

                        // Clear the value from Cache
                        var cacheManager = new CacheManager();
                        cacheManager.Remove(_sessionID.ToString());
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }
        public void LogOutByEmailId(string EmailId, string SessionId)
        {
            Guid _sessionID;
            if (!Guid.TryParse(SessionId, out _sessionID))
            {
                throw new ArgumentException("Invalid GUID");
            }

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(EmailId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Framework.Data.FrameworkModelContainer container = new Framework.Data.FrameworkModelContainer())
                    {
                        // Clear the value from FPLoginSession
                        container.LogOutForAllSessions(EmailId, _sessionID);

                        //TODO: Need to decide whether clear cache here.
                        //// Clear the value from Cache
                        //var cacheManager = new CacheManager();
                        //cacheManager.Remove(_sessionID.ToString());
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }

        /// <summary>
        /// This method is used to check the User has active sessions in login table.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public bool CheckAuthenticationByEmailId(string emailId, string SessionId)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emailId, SessionId))
            {
                Guid _sessionID;
                if (!Guid.TryParse(SessionId, out _sessionID))
                {
                    throw new ArgumentException("Invalid GUID");
                }
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Framework.Data.FrameworkModelContainer container = new FlightPak.Framework.Data.FrameworkModelContainer())
                    {
                        var returnValue = container.CheckLoginForAllSessions(emailId, _sessionID).FirstOrDefault();
                        return returnValue.HasValue ? returnValue.Value > 0 : false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }
    }
}
