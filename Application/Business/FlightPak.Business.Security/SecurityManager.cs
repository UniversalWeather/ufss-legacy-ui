﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Business.Common;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Security
{
    public class SecurityManager
    {
        private ExceptionManager exManager;
        public string GetUserNameByEmail(string email, string securityQuestion, string securityAnswer)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(email))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<string>(() =>
                {
                    using (Framework.Data.FrameworkModelContainer container = new Framework.Data.FrameworkModelContainer())
                    {
                        var returnValue = container.GetUserNameByEmail(email, securityQuestion, securityAnswer).FirstOrDefault();
                        if (returnValue != null && returnValue.UserName.Length > 0)
                        {
                            //EmailManager.SendMail(returnValue.EmailID, "Recover User Name", "User Name is " + returnValue.UserName, "User Name");
                        }

                    }
                    // Vishwa
                    // Return same message for both success and failiure cases.
                    // It will prevent this operation from Dictionary attack
                    return "Your user name has been sent to your E-mail.";
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        /// <summary>
        /// This method is used to check the session id has valid login details in login table.
        /// </summary>
        /// <param name="sessionId"></param>
        /// <returns></returns>
        public bool CheckAuthentication(string sessionId)
        {
            Guid _sessionID;
            if (!Guid.TryParse(sessionId, out _sessionID))
            {
                throw new ArgumentException("Invalid GUID");
            }


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(sessionId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Framework.Data.FrameworkModelContainer container = new FlightPak.Framework.Data.FrameworkModelContainer())
                    {
                        var returnValue = container.CheckLogin(_sessionID).FirstOrDefault();
                        return returnValue.HasValue ? returnValue.Value > 0 : false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        
    }
}
