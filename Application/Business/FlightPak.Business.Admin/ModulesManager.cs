﻿using System.Linq;
using FlightPak.Business.Common;

//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Admin
{
    public class ModulesManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.GetAllModules> GetList()
        {
            ReturnValue<Data.Admin.GetAllModules> ret = null;
            Data.Admin.AdminDataContainer cs = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.Admin.AdminDataContainer();
                    ret = new ReturnValue<Data.Admin.GetAllModules>();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllModules(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
    }
}
