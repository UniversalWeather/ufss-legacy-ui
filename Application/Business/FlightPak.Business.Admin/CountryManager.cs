﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.Admin
{

    public class CountryManager : BaseManager
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public ReturnValue<Data.Admin.Country> GetList()
        {
            ReturnValue<Data.Admin.Country> Country = new ReturnValue<Data.Admin.Country>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    Country.EntityList = objContainer.GetAllCountry().ToList();
                    Country.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return Country;
        }

        /// <summary>
        /// To add values into the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.Admin.Country> Add(Data.Admin.Country CountryType)
        {
            ReturnValue<Data.Admin.Country> Country = new ReturnValue<Data.Admin.Country>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CountryType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {


                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.AddToCountries(CountryType);
                    objContainer.SaveChanges();
                    Country.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return Country;
        }

        /// <summary>
        /// To Update values to the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.Admin.Country> Update(Data.Admin.Country CountryType)
        {
            ReturnValue<Data.Admin.Country> Country = new ReturnValue<Data.Admin.Country>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CountryType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Admin.AdminDataContainer Container = new Data.Admin.AdminDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.Countries.Attach(CountryType);
                    Common.EMCommon.SetAllModified<Data.Admin.Country>(CountryType, Container);
                    Container.SaveChanges();
                    Country.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return Country;
        }

        /// <summary>
        /// To delete from the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.Admin.Country> Delete(Data.Admin.Country CountryType)
        {
            ReturnValue<Data.Admin.Country> Country = new ReturnValue<Data.Admin.Country>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CountryType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Admin.AdminDataContainer Container = new Data.Admin.AdminDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.Countries.Attach(CountryType);
                    Container.Countries.DeleteObject(CountryType);
                    Container.SaveChanges();
                    Country.ReturnFlag = true;
                    return Country;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Country;

        }
    }
}
