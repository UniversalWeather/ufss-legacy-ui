﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Business.Common;
using System.Data;

//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Security;
using System.Data.Objects.DataClasses;
using FlightPak.Data.Admin;
using System.Data.Objects;

namespace FlightPak.Business.Admin
{
    public class CustomerManager : BaseManager
    {
        FlightPak.Framework.Encryption.Crypto Crypting = new Framework.Encryption.Crypto();
        private ExceptionManager exManager;

        public ReturnValue<Data.Admin.GetAllCustomersLicence> GetList()
        {
            ReturnValue<Data.Admin.GetAllCustomersLicence> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.GetAllCustomersLicence>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllCustomers().ToList();
                    foreach (var Item in ret.EntityList)
                    {
                        Item.ApisPassword = Crypting.Decrypt(Item.ApisPassword);
                    }
                    ret.ReturnFlag = true;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;

        }

        public ReturnValue<Data.Admin.GetAllAirport> GetAirportList(Int64 customerID)
        {
            ReturnValue<Data.Admin.GetAllAirport> ret = new ReturnValue<Data.Admin.GetAllAirport>();
            Data.Admin.AdminDataContainer cs = new Data.Admin.AdminDataContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.GetAllAirport>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllAirport(customerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }


        public ReturnValue<Data.Admin.Customer> Add(Data.Admin.Customer customer)
        {
            string password = Membership.GeneratePassword(12, 1);
            //string password = "fpk_123";
            ReturnValue<Data.Admin.Customer> ret = null;
            IEnumerable<KeyValuePair<string, object>> entityKeyValues = null;
            Data.Admin.UserMaster userMaster = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customer))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.Customer>();
                    entityKeyValues =
                      new KeyValuePair<string, object>[] {
            new KeyValuePair<string, object>("CustomerID", customer.CustomerID)
            };
                    customer.EntityKey = new EntityKey("AdminDataContainer.Customer", entityKeyValues);
                    customer.IsDeleted = false;
                    userMaster = customer.UserMaster.FirstOrDefault();
                    using (Data.Admin.AdminDataContainer cs = new Data.Admin.AdminDataContainer())
                    {
                        customer.UserMaster.Remove(userMaster);
                        customer.ApisPassword = Crypting.Encrypt(customer.ApisPassword);
                        cs.Customer.AddObject(customer);


                        cs.SaveChanges(System.Data.Objects.SaveOptions.None);

                    }
                    using (Data.Admin.AdminDataContainer container = new Data.Admin.AdminDataContainer())
                    {
                        userMaster.HomebaseID = customer.Company.FirstOrDefault().HomebaseID;
                        userMaster.CustomerID = customer.CustomerID;
                        container.UserMaster.AddObject(userMaster);
                        container.SaveChanges();
                    }
                    using (Data.Admin.AdminDataContainer container1 = new Data.Admin.AdminDataContainer())
                    {
                        FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                        emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                        {
                            //Email logic has been modified by Vishwa on 05-02-2013
                            CustomerID = customer.CustomerID,
                            EmailBody = string.Empty,
                            EmailReqTime = DateTime.UtcNow,
                            EmailReqUID = userMaster.UserName,
                            EmailSentTo = customer.PrimaryEmailID,
                            EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                            EmailSubject = string.Empty,
                            EmailTmplID = FlightPak.Common.Constants.EmailTemplates.NewUserCreationTemplate,
                            EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                            IsBodyHTML = true,
                            LastUPDUID = userMaster.UserName
                        }, 
                        new Dictionary<string, string> { 
                        { "[[Client Name]]", userMaster.FirstName + " " + userMaster.LastName }, 
                        { "[[User Name]]", customer.PrimaryEmailID },
                        { "[[Password]]", password },
                        { "[[Name]]", "Support Team" }, // TODO: Vishwa
                        });
                        container1.ResetPassword(customer.CustomerID, userMaster.UserName, GetHashedPassword(password), userMaster.UserName);
                        container1.SaveChanges();
                    }


                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }





        public ReturnValue<Data.Admin.Customer> Update(Data.Admin.Customer customer, int customerModuleCount)
        {
            ReturnValue<Data.Admin.Customer> ret = null;
            IEnumerable<KeyValuePair<string, object>> entityKeyValues = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customer, customerModuleCount))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    entityKeyValues =
                    new KeyValuePair<string, object>[] {
            new KeyValuePair<string, object>("CustomerID", customer.CustomerID)
            };

                    ret = new ReturnValue<Data.Admin.Customer>();
                    customer.EntityKey = new EntityKey("AdminDataContainer.Customer", entityKeyValues);
                    customer.IsDeleted = false;
                    EntityCollection<CustomerModuleAccess> moduleAccess = new EntityCollection<CustomerModuleAccess>();
                    var CustomerLicense = customer.CustomerLicense.FirstOrDefault();
                    using (Data.Admin.AdminDataContainer Context = new Data.Admin.AdminDataContainer())
                    {
                        customer.ApisPassword = Crypting.Encrypt(customer.ApisPassword);

                        var array = new CustomerModuleAccess[customer.CustomerModuleAccess.Count()];
                        customer.CustomerModuleAccess.CopyTo(array, 0);
                        int i = -1;
                        foreach (var entity in array)
                        {
                            if (entity.CustomerModuleAccessID == 0 && entity.LastUpdTS == null)
                            {
                                entity.CustomerModuleAccessID = i;
                                i = i - 1;
                            }
                            moduleAccess.Add(entity);
                        }
                        customer.CustomerModuleAccess.Clear();
                        Context.Customer.Attach(customer);
                        //Set object state as modified for modifying a prent object
                        Context.ObjectStateManager.ChangeObjectState(customer, System.Data.EntityState.Modified);



                        if (CustomerLicense != null)
                        {
                            if (CustomerLicense.CustomerLicenseID == 1)
                            {
                                Context.ObjectStateManager.ChangeObjectState(CustomerLicense, System.Data.EntityState.Added);
                            }
                            else
                            {
                                Context.CustomerLicense.Attach(CustomerLicense);
                                Context.ObjectStateManager.ChangeObjectState(CustomerLicense, System.Data.EntityState.Modified);
                            }
                        }

                        foreach (Data.Admin.CustomerModuleAccess customerModuleList in moduleAccess)
                        {
                            if (customerModuleList.LastUpdTS == null)
                            {
                                customerModuleList.LastUpdTS = DateTime.UtcNow;
                                Context.CustomerModuleAccess.AddObject(customerModuleList);
                                Context.ObjectStateManager.ChangeObjectState(customerModuleList, System.Data.EntityState.Added);

                            }
                            else
                            {
                                Context.CustomerModuleAccess.Attach(customerModuleList);
                                Context.ObjectStateManager.ChangeObjectState(customerModuleList, System.Data.EntityState.Modified);
                            }
                        }

                        Context.SaveChanges();
                    }
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Admin.Customer> Delete(Data.Admin.Customer customer)
        {
            ReturnValue<Data.Admin.Customer> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customer))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.Customer>();
                    cs = new Data.Admin.AdminDataContainer();
                    customer.IsDeleted = true;
                    cs.AttachTo(typeof(Data.Admin.Customer).Name, customer);
                    cs.Customer.DeleteObject(customer);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }


        public ReturnValue<Data.Admin.UserMaster> ValidateUser(string userName)
        {
            ReturnValue<Data.Admin.UserMaster> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.UserMaster>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.ValidateUser(userName).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        public ReturnValue<Data.Admin.Customer> GetLock(Data.Admin.Customer account)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }


        public string GetHashedPassword(string plainText)
        {
            string SaltValue = Crypting.GenerateSaltValue();
            return Crypting.GenerateHash(plainText, SaltValue);
        }

        public Guid? customerID { get; set; }

        public Int64 GetUWACustomerId()
        {
            Data.Admin.AdminDataContainer cs = null;
            ObjectParameter param = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    param = new ObjectParameter("ReturnValue", typeof(Int64));
                    cs.GetUWACustomerId(param);

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return (Int64)param.Value;

        }

        public Int64? GetActualUserCount(Int64 customerId)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Data.Admin.AdminDataContainer cs = null;
                Int64? actualUserCount = null;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    actualUserCount = cs.GetActualUserCount(customerId).FirstOrDefault();
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return actualUserCount;
            }
        }

        public Int64? GetAircraftUsedCount(Int64 customerId)
        {
            Int64? AircraftUsedCount = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    AircraftUsedCount = objContainer.GetAircraftUsedCount(customerId).FirstOrDefault();
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return AircraftUsedCount;
            }
        }
    }
}
