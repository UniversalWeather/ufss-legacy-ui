﻿using System;
using FlightPak.Business.Common;

//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace FlightPak.Business.Admin
{
    public class CustomerLicensesManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.CustomerLicense> GetList()
        {
            ReturnValue<Data.Admin.CustomerLicense> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.CustomerLicense>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;

        }

        public ReturnValue<Data.Admin.CustomerLicense> Add(Data.Admin.CustomerLicense CustomerLicense)
        {
            ReturnValue<Data.Admin.CustomerLicense> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerLicense))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CustomerLicense.IsDeleted = false;
                    ret = new ReturnValue<Data.Admin.CustomerLicense>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.CustomerLicense.AddObject(CustomerLicense);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Admin.CustomerLicense> Update(Data.Admin.CustomerLicense CustomerLicense)
        {
            ReturnValue<Data.Admin.CustomerLicense> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerLicense))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.CustomerLicense>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.CustomerLicense.Attach(CustomerLicense);
                    Common.EMCommon.SetAllModified<Data.Admin.CustomerLicense>(CustomerLicense, cs);
                    cs.SaveChanges();

                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return ret;


        }
        public ReturnValue<Data.Admin.CustomerLicense> Delete(Data.Admin.CustomerLicense CustomerLicense)
        {
            ReturnValue<Data.Admin.CustomerLicense> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerLicense))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.CustomerLicense>();
                    cs = new Data.Admin.AdminDataContainer();
                    CustomerLicense.IsDeleted = true;
                    cs.AttachTo(typeof(Data.Admin.CustomerLicense).Name, CustomerLicense);
                    cs.CustomerLicense.DeleteObject(CustomerLicense);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return ret;
        }

        public ReturnValue<Data.Admin.CustomerLicense> GetLock(Data.Admin.CustomerLicense account)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}
