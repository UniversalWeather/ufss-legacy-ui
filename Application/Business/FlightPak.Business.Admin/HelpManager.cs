﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Business.Common;
using System.Data;
using System.Data.Objects;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;

namespace FlightPak.Business.Admin
{
    public class HelpManager : BaseManager
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Admin.HelpContent> Add(Data.Admin.HelpContent oHelpContent)
        {
            ReturnValue<Data.Admin.HelpContent> ReturnValue = new ReturnValue<Data.Admin.HelpContent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpContent))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        objContainer.HelpContents.AddObject(oHelpContent);                        
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oHelpContent;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Admin.HelpContent> Update(Data.Admin.HelpContent oHelpContent)
        {
            ReturnValue<Data.Admin.HelpContent> ReturnValue = new ReturnValue<Data.Admin.HelpContent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpContent))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        objContainer.HelpContents.Attach(oHelpContent);
                        objContainer.ObjectStateManager.ChangeObjectState(oHelpContent, System.Data.EntityState.Modified);                        
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Admin.HelpContent> Delete(Data.Admin.HelpContent oHelpContent)
        {
            ReturnValue<Data.Admin.HelpContent> ReturnValue = new ReturnValue<Data.Admin.HelpContent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpContent))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        //objContainer.HelpContents.Attach(oHelpContent);
                        //objContainer.HelpContents.DeleteObject(oHelpContent);
                        //objContainer.SaveChanges();
                        //ReturnValue.ReturnFlag = true;
                        throw new NotImplementedException();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Admin.HelpContent> GetList(Data.Admin.HelpContent oHelpContent)
        {
            ReturnValue<Data.Admin.HelpContent> ReturnValue = new ReturnValue<Data.Admin.HelpContent>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpContent))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Admin.GetHelpContent> GetHelpContent(Data.Admin.HelpContent oHelpContent)
        {
            ReturnValue<Data.Admin.GetHelpContent> ReturnValue = new ReturnValue<Data.Admin.GetHelpContent>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpContent))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        ReturnValue.EntityList = objContainer.GetHelpContent(oHelpContent.Identifier, oHelpContent.Category, oHelpContent.Topic, oHelpContent.IsDeleted, oHelpContent.IsInActive).ToList();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        
        //For HelpItems

        public ReturnValue<Data.Admin.HelpItem> Add(Data.Admin.HelpItem oHelpItem)
        {
            ReturnValue<Data.Admin.HelpItem> ReturnValue = new ReturnValue<Data.Admin.HelpItem>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpItem))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        objContainer.HelpItems.AddObject(oHelpItem);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oHelpItem;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Admin.HelpItem> Delete(Data.Admin.HelpItem oHelpItem)
        {
            ReturnValue<Data.Admin.HelpItem> ReturnValue = new ReturnValue<Data.Admin.HelpItem>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpItem))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.HelpItems.Attach(oHelpItem);
                        objContainer.HelpItems.DeleteObject(oHelpItem);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                        return ReturnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Admin.HelpItem> GetList(Data.Admin.HelpItem oHelpItem)
        {
            ReturnValue<Data.Admin.HelpItem> ReturnValue = new ReturnValue<Data.Admin.HelpItem>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oHelpItem))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetHelpItems().ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public bool UpdateHelpItems(string strColumn, string strValue, string ItemID)
        {
            bool returnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(strColumn, strValue, ItemID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                    {
                        objContainer.UpdateHelpItems(strColumn, strValue, ItemID);
                        returnValue = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return returnValue;
        }

    }
}

