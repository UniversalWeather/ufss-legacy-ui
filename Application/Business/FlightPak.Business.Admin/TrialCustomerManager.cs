﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//For Tracing and Exception Handling
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;
using FlightPak.Business.Common;
using System.Xml;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data.EntityClient;
using FlightPak.Data.Admin.TrialCustomerDataSetTableAdapters;

namespace FlightPak.Business.Admin
{
    public class TrialCustomerManager : BaseManager
    {
        private ExceptionManager exManager;
        //public string ProcessTrialCustomer(long CustomerId, string strdocument)
        //{
        //    string returnValue = string.Empty;
        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        //Handle methods throguh exception manager with return flag
        //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //        exManager.Process(() =>
        //        {
        //            string connectionString = string.Empty;
        //            XmlDocument document = new XmlDocument();
        //            document.LoadXml(strdocument);
        //            Data.Admin.TrialCustomerDataSet dsTrialCustomer = new Data.Admin.TrialCustomerDataSet();
        //            dsTrialCustomer.ReadXml(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(document.OuterXml)));

        //            // Seed data will occupy first 4000 records. So starting from 4000
        //            // If there is any addition in seed data, dev should re-consider this counter.
        //            int trialCustomerCounter = 4001;
        //            Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
        //            EntityConnection entityConnection = objContainer.Connection as EntityConnection;
        //            if (null != entityConnection)
        //            {
        //                connectionString = entityConnection.StoreConnection.ConnectionString;
        //            }

        //            using (SqlConnection destinationConnection =
        //                  new SqlConnection(connectionString))
        //            {
        //                // open the connection
        //                destinationConnection.Open();
        //                SqlTransaction trialCustomerTransaction = destinationConnection.BeginTransaction();
        //                try
        //                {
        //                    //Handle methods throguh exception manager with return flag
        //                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //                    exManager.Process(() =>
        //                    {
        //                        foreach (FlightPak.Data.Admin.TrialCustomerDataSet.TripManagerCheckListGroupRow row in dsTrialCustomer.TripManagerCheckListGroup)
        //                        {
        //                            row.CustomerID = CustomerID;
        //                            row.LastUpdTS = DateTime.UtcNow;
        //                            row.LastUpdUID = "UWA Admin";
        //                            row.CheckGroupID = Convert.ToInt64(CustomerId.ToString() + trialCustomerCounter.ToString());
        //                            trialCustomerCounter++;
        //                        }
        //                        foreach (FlightPak.Data.Admin.TrialCustomerDataSet.TripManagerCheckListRow row in dsTrialCustomer.TripManagerCheckList)
        //                        {
        //                            row.CustomerID = CustomerID;
        //                            row.LastUpdTS = DateTime.UtcNow;
        //                            row.LastUpdUID = "UWA Admin";
        //                            row.CheckListID = Convert.ToInt64(CustomerId.ToString() + trialCustomerCounter.ToString());
        //                            trialCustomerCounter++;
        //                        }
        //                        TripManagerCheckListGroupTableAdapter tripManagerCheckListGroupTableAdapter = new TripManagerCheckListGroupTableAdapter();

        //                        tripManagerCheckListGroupTableAdapter.Insert(
        //                        TripManagerCheckListTableAdapter tripManagerCheckListTableAdapter = new TripManagerCheckListTableAdapter();
        //                        tripManagerCheckListTableAdapter.Update(dsTrialCustomer);


        //                    }, FlightPak.Common.Constants.Policy.DataLayer);
        //                }
        //                catch (Exception ex)
        //                {
        //                    trialCustomerTransaction.Rollback();
        //                    //exManager.HandleException(ex, FlightPak.Common.Constants.Policy.DataLayer);
        //                    returnValue = "An error has been occured in trial customer creation. Please contact administrator." + "<br/><b>Exception Message: </b>" + ex.Message;

        //                }
        //                finally
        //                {
        //                    destinationConnection.Close();
        //                }

        //            }

        //        }, FlightPak.Common.Constants.Policy.DataLayer);

        //    }
        //    return returnValue;
        //}

        public string ProcessTrialCustomer(long CustomerId, string strdocument)
        {
            string returnValue = string.Empty;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    string connectionString = string.Empty;
                    XmlDocument document = new XmlDocument();
                    document.XmlResolver = null;
                    document.LoadXml(strdocument);
                    Data.Admin.TrialCustomerDataSet dsTrialCustomer = new Data.Admin.TrialCustomerDataSet();
                    DataSet ds = new DataSet();
                    ds.ReadXml(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(document.OuterXml)));
                    foreach (DataTable dt in ds.Tables)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            dsTrialCustomer.Tables[dt.TableName].ImportRow(dr);

                        }

                    }
                    //DataSet dsTrialCustomer = new DataSet();
                    //try
                    //{
                    //    //dsTrialCustomer.ReadXml(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(document.OuterXml)), XmlReadMode.IgnoreSchema);
                    //    //dsTrialCustomer.ReadXmlSchema(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(document.OuterXml)));
                    //    //dsTrialCustomer.ReadXml(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(document.OuterXml)));

                    //    dsTrialCustomer.ReadXml(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(document.OuterXml)));
                    //}
                    //catch (Exception)
                    //{

                    //    throw;
                    //}

                    // Seed data will occupy first 4000 records. So starting from 4000
                    // If there is any addition in seed data, dev should re-consider this counter.
                    int trialCustomerCounter = 6001;
                    //int foreignKeyCounter = 4000;
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    EntityConnection entityConnection = objContainer.Connection as EntityConnection;
                    //----------------Commendted by kulwant----------------------------------------
                    //if (null != entityConnection)
                    //{
                    //    connectionString = entityConnection.StoreConnection.ConnectionString;
                    //}
                    //----------------END Commendted by kulwant----------------------------------------

                    //string connectionString = @"Server=10.0.2.216;Database=FP_UAT_DM2;Trusted_Connection=true";

                    if (entityConnection!=null)
                    { 
                    using (SqlConnection destinationConnection =
                           new SqlConnection(entityConnection.StoreConnection.ConnectionString))
                    {
                        // open the connection
                        destinationConnection.Open();
                        SqlTransaction trialCustomerTransaction = destinationConnection.BeginTransaction();
                        SqlBulkCopy bulkCopy;
                        try
                        {
                            //Handle methods throguh exception manager with return flag
                            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                            exManager.Process(() =>
                            {
                                #region Update Relation Ships
                                foreach (DataTable tables in dsTrialCustomer.Tables)
                                {
                                    //bulkCopy = new SqlBulkCopy(destinationConnection, SqlBulkCopyOptions.CheckConstraints, trialCustomerTransaction);
                                    //foreach (DataColumn columns in tables.Columns)
                                    //{
                                    //    bulkCopy.ColumnMappings.Add(columns.ColumnName, columns.ColumnName);
                                    //}

                                    foreach (DataRow row in tables.Rows)
                                    {                                        
                                        if (tables.TableName == "UserMaster")
                                        {
                                            //row["UserName"] = row["UserName"].ToString().Replace("$CUSTOMERID$", CustomerId.ToString());
                                            //row["EmailID"] = row["EmailID"].ToString().Replace("$CUSTOMERID$", CustomerId.ToString());

                                            row["UserName"] = "SUPERVISOR_" + CustomerId.ToString();
                                            row["EmailID"] = "SUPERVISOR_" + CustomerId.ToString() + "@Somewhere.com";
                                            row["UserMasterID"] = CustomerId.ToString() + trialCustomerCounter.ToString();
                                        }
                                        else if (tables.TableName == "UserPassword")
                                        {
                                            //row["UserName"] = row["UserName"].ToString().Replace("$CUSTOMERID$", CustomerId.ToString());
                                            row["UserName"] = "SUPERVISOR_" + CustomerId.ToString();
                                            //row["FPPWD"] = "9YcnTNnVY4oYg77wg9pqDDh+pqeyLFPcUzFHnCPG9YU=䢍⊏�";
                                            row["FPPWD"] = "43v43o2tzD+DWKrvEoyDpqtjvuuJKOa/Lo4h7dVO4tg=��";

                                            row[0] = CustomerId.ToString() + trialCustomerCounter.ToString();
                                        }
                                        //Added by Ramesh since DBGeneralInfo did not have LastUpdUID & LastUpdTS columns
                                        else
                                        {
                                            if (tables.TableName == "CrewRating")
                                            {
                                                row["CrewRatingID"] = CustomerId.ToString() + trialCustomerCounter.ToString();
                                            }
                                            else
                                            {
                                                row[0] = CustomerId.ToString() + trialCustomerCounter.ToString();
                                            }
                                            //Ramesh: _ to be removed
                                            if (tables.TableName != "DBGeneralInfo" && tables.TableName != "UWALAvail" && tables.TableName != "FlightCatagory" && tables.TableName != "TripManagerCheckListGroup")
                                            {
                                                row["LastUpdUID"] = "SUPERVISOR_" + CustomerId.ToString();
                                                if (tables.TableName == "CrewDutyRules")
                                                {
                                                    row["LastUptTM"] = DateTime.Now;
                                                }
                                                else if (tables.TableName == "PassengerAdditionalInfo")
                                                {
                                                    row["LastUptTS"] = DateTime.Now;
                                                }
                                                else
                                                {
                                                    row["LastUpdTS"] = DateTime.Now;
                                                }
                                            }
                                        }
                                        
                                        if (tables.TableName != "UserPassword" && tables.TableName != "UWALAvail")
                                            row["CustomerId"] = CustomerId;

                                        trialCustomerCounter++;
                                    }
                                    //bulkCopy.DestinationTableName = tables.TableName;
                                    //bulkCopy.WriteToServer(tables);

                                }
                                #endregion
                                #region Bulk Insert
                                foreach (DataTable tables in dsTrialCustomer.Tables)
                                {
                                    if (tables.Rows.Count > 0)
                                    {
                                        bulkCopy = new SqlBulkCopy(destinationConnection, SqlBulkCopyOptions.CheckConstraints, trialCustomerTransaction);
                                        
                                        foreach (DataColumn columns in tables.Columns)
                                        {
                                            bulkCopy.ColumnMappings.Add(columns.ColumnName, columns.ColumnName);
                                        }
                                        bulkCopy.DestinationTableName = tables.TableName;
                                        if (tables.TableName == "WorldWind")
                                        {
                                            bulkCopy.BulkCopyTimeout = 600;
                                        }
                                        bulkCopy.WriteToServer(tables);
                                    }
                                }
                                #endregion
                                #region Update FlightPakSequence
                                //Update FlightPakSequence
                                using (FlightPak.Data.Admin.AdminDataContainer container = new Data.Admin.AdminDataContainer())
                                {
                                    container.UpdateSequenceForTrialCustomer(CustomerId, 45000);
                                }
                                #region Removed Code - Veracode Critical Issue (SQL Injection)
                                //Sql bulk copy doesn't support update. So first get the existing record, delete the record, and re-insert the updated record.
                                //SqlCommand command = new SqlCommand("SELECT * FROM FlightpakSequence WHERE CUSTOMERID = " + CustomerId, destinationConnection, trialCustomerTransaction);
                                //command.CommandType = CommandType.Text;
                                //DataTable sequenceTable = new DataTable();
                                //sequenceTable.Load(command.ExecuteReader());
                                ////command = new SqlCommand("Select isnull(MAX(FlightpakSequenceID + 1),1) from FlightpakSequence ", destinationConnection, trialCustomerTransaction);
                                ////int PrimaryKeyId = command
                                //command = new SqlCommand("DELETE FROM FlightpakSequence WHERE CUSTOMERID = " + CustomerId, destinationConnection, trialCustomerTransaction);
                                //command.ExecuteNonQuery();
                                ////Intentially left the primary checking, if there is no row at level 0 it should roll back the whole transaction.
                                //sequenceTable.Rows[0]["MasterModuleCurrentNo"] = 5000;
                                //bulkCopy = new SqlBulkCopy(destinationConnection, SqlBulkCopyOptions.Default, trialCustomerTransaction);
                                //foreach (DataColumn columns in sequenceTable.Columns)
                                //{
                                //    bulkCopy.ColumnMappings.Add(columns.ColumnName, columns.ColumnName);
                                //}
                                //bulkCopy.DestinationTableName = "FlightpakSequence";
                                //bulkCopy.WriteToServer(sequenceTable);
                                #endregion
                                #endregion
                                trialCustomerTransaction.Commit();

                              // trialCustomerTransaction.Rollback();

                                returnValue = "Trial customer created successfully";
                            }, FlightPak.Common.Constants.Policy.DataLayer);
                        }//
                        catch (Exception ex)
                        {
                            trialCustomerTransaction.Rollback();
                            //exManager.HandleException(ex, FlightPak.Common.Constants.Policy.DataLayer);
                            returnValue = "An error has been occured in trial customer creation. Please contact administrator." + "<br/><b>Exception Message: </b>" + ex.Message;

                        }
                        finally
                        {
                            destinationConnection.Close();
                        }
                    }
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return returnValue;
        }
    }
}
