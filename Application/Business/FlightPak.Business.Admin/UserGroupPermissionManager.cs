﻿using System;
using System.Linq;
using FlightPak.Business.Common;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Admin
{
    public class UserGroupPermissionManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.GetUserGroupPermission> GetList(Int64 UserGroupID, Int64 CustomerID)
        {
            ReturnValue<Data.Admin.GetUserGroupPermission> ret = null;
            Data.Admin.AdminDataContainer cs = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupID, CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.GetUserGroupPermission>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetUserGroupPermission(UserGroupID, CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return ret;
        }
    }
}
