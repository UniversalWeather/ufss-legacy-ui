﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Business.Common;
using System.Data;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FlightPak.Business.Admin
{
    public class UserGroupManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.UserGroup> GetList(Int64 CustomerID)
        {
            ReturnValue<Data.Admin.UserGroup> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.UserGroup>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllUserGroup(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Admin.UserGroup> GetUserGroupByGroupCode(string userGroupCode)
        {
            ReturnValue<Data.Admin.UserGroup> returnValue = null;
            Data.Admin.AdminDataContainer Context = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroupCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    returnValue = new ReturnValue<Data.Admin.UserGroup>();
                    Context = new Data.Admin.AdminDataContainer();
                    Context.ContextOptions.LazyLoadingEnabled = false;
                    Context.ContextOptions.ProxyCreationEnabled = false;
                    returnValue.EntityInfo = Context.GetUserGroupByGroupCode(userGroupCode, base.CustomerID).FirstOrDefault();
                    returnValue.ReturnFlag = true;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;

        }

        

        public ReturnValue<Data.Admin.UserGroup> Update(Data.Admin.UserGroup userGroup, List<Data.Admin.UserGroupPermission> dtUpdatePermission, int maintainGroupModuleCount)
        {
            ReturnValue<Data.Admin.UserGroup> ret = new ReturnValue<Data.Admin.UserGroup>(); ;
            IEnumerable<KeyValuePair<string, object>> entityKeyValues = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroup, dtUpdatePermission, maintainGroupModuleCount))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(userGroup, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        //ret = new ReturnValue<Data.Admin.UserGroup>();
                        entityKeyValues =
                        new KeyValuePair<string, object>[] {
            new KeyValuePair<string, object>("UserGroupID", userGroup.UserGroupID)           
            };
                        userGroup.EntityKey = new EntityKey("AdminDataContainer.UserGroup", entityKeyValues);
                        userGroup.IsDeleted = false;
                        using (Data.Admin.AdminDataContainer Context = new Data.Admin.AdminDataContainer())
                        {
                            userGroup.LastUpdUID = base.UserName;
                            Context.UserGroup.Attach(userGroup);
                            //Set object state as modified for modifying a prent object
                            Context.ObjectStateManager.ChangeObjectState(userGroup, System.Data.EntityState.Modified);
                            Context.SaveChanges();
                            try
                            {
                                var datatable = dtUpdatePermission.ToDataTable();
                                datatable.TableName = "UpdateTable";
                                SqlParameter param = new SqlParameter("UpdateTable", datatable);
                                param.TypeName = "[dbo].[UserGroupPermissionType]";
                                param.SqlDbType = SqlDbType.Structured;
                                Context.ExecuteStoreCommand("EXEC spFlightPak_CustomUpdateUserGroupPermission @UpdateTable", param);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }

                            //int maintainGroupCountvalue = 0;
                            //foreach (Data.Admin.UserGroupPermission userGroupPermission in userGroup.UserGroupPermission)
                            //{
                            //    if (maintainGroupCountvalue == maintainGroupModuleCount)
                            //    {

                            //        break;
                            //    }
                            //    else
                            //    {
                            //        if (userGroupPermission.LastUpdTS == null)
                            //        {
                            //            userGroupPermission.LastUpdTS = DateTime.UtcNow;

                            //            Context.ObjectStateManager.ChangeObjectState(userGroupPermission, System.Data.EntityState.Added);
                            //            Context.UserGroupPermission.AddObject(userGroupPermission);
                            //        }
                            //        else
                            //        {

                            //            Context.UserGroupPermission.Attach(userGroupPermission);
                            //            Context.ObjectStateManager.ChangeObjectState(userGroupPermission, System.Data.EntityState.Modified);

                            //        }
                            //    }
                            //    maintainGroupCountvalue++;
                            //}



                            // Once the user is updated, we should clear all the objects in cache.
                            var cacheManager = new CacheManager();
                            cacheManager.RemoveAll();
                        }

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;

        }
        public ReturnValue<Data.Admin.UserGroup> Delete(Data.Admin.UserGroup userGroup)
        {
            ReturnValue<Data.Admin.UserGroup> ret = null;
            Data.Admin.AdminDataContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    userGroup.IsDeleted = true;
                    userGroup.LastUpdUID = base.UserName;
                    ret = new ReturnValue<Data.Admin.UserGroup>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.AttachTo(typeof(Data.Admin.UserGroup).Name, userGroup);
                    cs.UserGroup.DeleteObject(userGroup);
                    cs.SaveChanges();

                    // Once the user is updated, we should clear all the objects in cache.
                    var cacheManager = new CacheManager();
                    cacheManager.RemoveAll();

                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.Admin.UserGroup> GetLock(Data.Admin.UserGroup userGroup)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }


        public ReturnValue<Data.Admin.UserGroup> Add(Data.Admin.UserGroup userGroup)
        {
            ReturnValue<Data.Admin.UserGroup> ret = new ReturnValue<Data.Admin.UserGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(userGroup, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        IEnumerable<KeyValuePair<string, object>> entityKeyValues =
                                   new KeyValuePair<string, object>[] {
                            new KeyValuePair<string, object>("UserGroupID", userGroup.UserGroupID)           
                                };
                        userGroup.EntityKey = new EntityKey("AdminDataContainer.UserGroup", entityKeyValues);
                        userGroup.IsDeleted = false;
                        using (Data.Admin.AdminDataContainer cs = new Data.Admin.AdminDataContainer())
                        {
                            userGroup.LastUpdUID = base.UserName;
                            cs.UserGroup.AddObject(userGroup);
                            cs.SaveChanges();
                        }
                        ret.ReturnFlag = true;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }


        public Guid? customerID { get; set; }
    }
}
