﻿using System;
using System.Linq;
using FlightPak.Business.Common;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Admin
{
    public class CustomerModuleAccessesManager:BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.GetCustomerModuleAccesses> GetList(Int64 CustomerId)
        {
            ReturnValue<Data.Admin.GetCustomerModuleAccesses> ret = null;
            Data.Admin.AdminDataContainer cs = null;


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Admin.GetCustomerModuleAccesses>();
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetCustomerModuleAccess(CustomerId).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return ret;
        }

        public ReturnValue<Data.Admin.Customer> Add(Data.Admin.Customer customer)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.Admin.Customer> Update(Data.Admin.Customer customer)
        {
            throw new NotImplementedException();
        }

    }
}
