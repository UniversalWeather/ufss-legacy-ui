﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Business.Common;
using System.Data;
using System.Data.Objects;

//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;

namespace FlightPak.Business.Admin
{
    public class UserManager : BaseManager
    {
        FlightPak.Framework.Encryption.Crypto Crypting = new Framework.Encryption.Crypto();
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.GetAllUserMasterResult> GetList()
        {
            ReturnValue<Data.Admin.GetAllUserMasterResult> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.GetAllUserMasterResult>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetAllUserMaster(CustomerID).ToList();
                    foreach (var Item in ReturnValue.EntityList)
                    {
                        Item.UVTripLinkPassword = Crypting.Decrypt(Item.UVTripLinkPassword);
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserMaster> Add(Data.Admin.UserMaster userMaster, string password, bool isClientCode = false)
        {
            ReturnValue<Data.Admin.UserMaster> ReturnValue = new ReturnValue<Data.Admin.UserMaster>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userMaster, password, isClientCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(userMaster, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        //ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                        IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
                                                                        new KeyValuePair<string, object>("UserName", userMaster.UserName)
                                                                        };
                        userMaster.EntityKey = new EntityKey("AdminDataContainer.UserMaster", entityKeyValues);
                        userMaster.IsDeleted = false;

                        using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                        {
                            userMaster.UVTripLinkPassword = Crypting.Encrypt(userMaster.UVTripLinkPassword);
                            userMaster.LastUpdUID = base.UserName;
                            objContainer.UserMaster.AddObject(userMaster);

                            if (isClientCode && userMaster.ClientCodeMapping.FirstOrDefault() != null)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(userMaster.ClientCodeMapping.FirstOrDefault(), EntityState.Added);
                            }

                            ////Send Email
                            //objContainer.AddToFPEmailTranLog(new Data.Admin.FPEmailTranLog
                            //{
                            //    CustomerID = CustomerID,
                            //    EmailPrio = true,
                            //    EmailReqUID = UserName,
                            //    EmailSentTo = userMaster.EmailID,
                            //    EmailStatus = "Not Done",
                            //    LastUPDUID = UserName,
                            //    EmailBody = "Welcome to Flight Pak Application",
                            //    EmailSubject = "User Name:" + userMaster.UserName + Environment.NewLine + "Password:" + password
                            //});


                            ////     foreach (Data.Admin.UserGroupMapping userGroupMapping in userMaster.UserGroupMapping)
                            ////{
                            ////cs.ObjectStateManager.ChangeObjectState(obj, System.Data.EntityState.Added);
                            ////    objContainer.UserGroupMapping.AddObject(userGroupMapping);
                            ////}
                            objContainer.SaveChanges();

                            FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                            emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                            {
                                //Email logic has been modified by Vishwa on 05-02-2013
                                CustomerID = CustomerID,
                                EmailBody = string.Empty,
                                EmailReqTime = DateTime.UtcNow,
                                EmailReqUID = base.UserName,
                                EmailSentTo = userMaster.EmailID,
                                EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                                EmailSubject = string.Empty,
                                EmailTmplID = FlightPak.Common.Constants.EmailTemplates.NewUserCreationTemplate,
                                EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                                IsBodyHTML = true,
                            },
                            new Dictionary<string, string> { 
                                { "[[Client Name]]", userMaster.FirstName + " " + userMaster.LastName }, 
                                { "[[User Name]]", userMaster.EmailID },
                                { "[[Password]]", password },
                                { "[[Name]]", "Support Team" }, // TODO: Vishwa
                                });

                            //Insert Password
                            objContainer.ResetPassword(base.CustomerID, userMaster.UserName, GetHashedPassword(password), base.UserName);
                            objContainer.SaveChanges();
                            //EmailManager.SendMail(userMaster.EmailID, "Welcome to Flight Pak Application", "User Name:" + userMaster.EmailID + Environment.NewLine + "Password:" + password, "Registration");
                        }
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
            //ReturnValue<Data.Admin.UserMaster> ret = new ReturnValue<Data.Admin.UserMaster>();
            //userMaster.IsDeleted = false;
            //Data.Admin.AdminDataContainer cs = new Data.Admin.AdminDataContainer();
            //cs.AddToUserMasters(userMaster);
            //cs.SaveChanges();
            //ret.ReturnFlag = true;
            //return ret;
        }
        public ReturnValue<Data.Admin.UserMaster> Update(Data.Admin.UserMaster userMaster, int userGroupCount, bool isClientCode = false)
        {
            ReturnValue<Data.Admin.UserMaster> ReturnValue = new ReturnValue<Data.Admin.UserMaster>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userMaster, userGroupCount, isClientCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(userMaster, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        //ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                        IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
                                                                        new KeyValuePair<string, object>("UserName", userMaster.UserName)
                                                                        };
                        userMaster.EntityKey = new EntityKey("AdminDataContainer.UserMaster", entityKeyValues);
                        using (Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer())
                        {
                            userMaster.UVTripLinkPassword = Crypting.Encrypt(userMaster.UVTripLinkPassword);
                            userMaster.LastUpdUID = base.UserName;
                            objContainer.UserMaster.Attach(userMaster);
                            objContainer.DeleteClientCodeMappingByUserName(userMaster.UserName);
                            //Set object state as modified for modifying a prent object
                            objContainer.ObjectStateManager.ChangeObjectState(userMaster, System.Data.EntityState.Modified);
                            if (isClientCode && userMaster.ClientCodeMapping.FirstOrDefault() != null)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(userMaster.ClientCodeMapping.FirstOrDefault(), EntityState.Added);
                            }
                            int userGroupCountValue = 0;
                            foreach (Data.Admin.UserGroupMapping UserGroupList in userMaster.UserGroupMapping)
                            {
                                if (userGroupCount == userGroupCountValue)
                                {
                                    break;
                                }
                                else
                                {
                                    if (UserGroupList.LastUpdTS == null)
                                    {
                                        UserGroupList.LastUpdTS = DateTime.UtcNow;
                                        objContainer.ObjectStateManager.ChangeObjectState(UserGroupList, System.Data.EntityState.Added);
                                        objContainer.UserGroupMapping.AddObject(UserGroupList);
                                    }
                                    else
                                    {
                                        objContainer.UserGroupMapping.Attach(UserGroupList);
                                        Common.EMCommon.SetAllModified<Data.Admin.UserGroupMapping>(UserGroupList, objContainer);

                                        //  Context.UserGroupMappings.Attach(obj);
                                        //Context.ObjectStateManager.ChangeObjectState(obj, System.Data.EntityState.Modified);
                                    }
                                }
                                userGroupCountValue++;
                            }
                            objContainer.SaveChanges();

                            // Once the user is updated, we should clear all the objects in cache.
                            var cacheManager = new CacheManager();
                            cacheManager.RemoveAll();

                            ReturnValue.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserMaster> Delete(Data.Admin.UserMaster userMaster)
        {
            //ReturnValue<Data.Admin.UserMaster> ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
            //userMaster.IsDeleted = true;
            //Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
            //objContainer.AttachTo("UserMaster", userMaster);
            //objContainer.UserMaster.DeleteObject(userMaster);
            //objContainer.SaveChanges();
            //ReturnValue.ReturnFlag = true;
            //return ReturnValue;

            ReturnValue<Data.Admin.UserMaster> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userMaster))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    userMaster.LastUpdUID = base.UserName;
                    objContainer.AttachTo(typeof(Data.Admin.UserMaster).Name, userMaster);
                    objContainer.UserMaster.DeleteObject(userMaster);
                    objContainer.SaveChanges();

                    // Once the user is updated, we should clear all the objects in cache.
                    var cacheManager = new CacheManager();
                    cacheManager.RemoveAll();

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserMaster> GetLock(Data.Admin.UserMaster account)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public Guid? userMasterID { get; set; }

        public ReturnValue<Data.Admin.UserMaster> ResetPassword(string userName, string answer, string hashPassword)
        {
            ReturnValue<Data.Admin.UserMaster> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, answer, hashPassword))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                    objContainer = new Data.Admin.AdminDataContainer();

                    //Reset Password
                    objContainer.ResetPassword(base.CustomerID, userName, GetHashedPassword(hashPassword), base.UserName);
                    //Insert Email

                    //TODO:Implement a sp to get email of a user
                    var email = GetList().EntityList.Where(x => x.UserName.Trim() == userName.Trim()).FirstOrDefault();
                    //objContainer.AddToFPEmailTranLog(new Data.Admin.FPEmailTranLog
                    //{
                    //    CustomerID = CustomerID,
                    //    EmailPrio = true,
                    //    EmailReqUID = UserName,
                    //    EmailSentTo = email.EmailID,
                    //    EmailStatus = "Not Done",
                    //    LastUPDUID = UserName,
                    //    EmailSubject = "Reset Password",
                    //    EmailBody = "Password is " + hashPassword
                    //});
                    //objContainer.SaveChanges();
                    //EmailManager.SendMail(email.EmailID, "Reset Password", "Password is " + hashPassword, "Reset Password");

                    FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                    emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                    {
                        //Email logic has been modified by Vishwa on 05-02-2013
                        CustomerID = CustomerID,
                        EmailBody = string.Empty,
                        EmailReqTime = DateTime.UtcNow,
                        EmailReqUID = UserName,
                        EmailSentTo = email.EmailID,
                        EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                        EmailSubject = string.Empty,
                        EmailTmplID = FlightPak.Common.Constants.EmailTemplates.ResetPasswordTemplate,
                        EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                        IsBodyHTML = true,
                        LastUPDUID = UserName
                    },
                    new Dictionary<string, string> { 
                                { "[[Client Name]]", email.FirstName + " " + email.LastName }, 
                                { "[[User Name]]", email.EmailID },
                                { "[[Password]]", hashPassword },
                                { "[[Name]]", "Support Team" }, // TODO: Vishwa
                                });

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public bool SendLockedEmailToUser(string userName)
        {
            bool ReturnValue = false;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                  
                    objContainer = new Data.Admin.AdminDataContainer();
                    //TODO:Implement a sp to get email of a user
                    var email = GetList().EntityList.Where(x => x.UserName.Trim() == userName.Trim()).FirstOrDefault();

                    FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                    emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                    {
                        CustomerID = CustomerID,
                        EmailBody = string.Empty,
                        EmailReqTime = DateTime.UtcNow,
                        EmailReqUID = UserName,
                        EmailSentTo = email.EmailID,
                        EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                        EmailSubject = string.Empty,
                        EmailTmplID = FlightPak.Common.Constants.EmailTemplates.LockedUserTemplate,
                        EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                        IsBodyHTML = true,
                        LastUPDUID = UserName
                    },
                    new Dictionary<string, string> { 
                                { "[[Client Name]]", email.FirstName + " " + email.LastName }, 
                                { "[[User Name]]", email.EmailID },
                                { "[[Name]]", "Support Team" }, // TODO: Vishwa
                                });

                    ReturnValue= true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }


        public string GetHashedPassword(string plainText)
        {
            string SaltValue = Crypting.GenerateSaltValue();
            return Crypting.GenerateHash(plainText, SaltValue);
        }

        public bool ChangePassword(string oldPassword, string newPassword)
        {
            bool returnValue = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldPassword, newPassword))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ObjectParameter param = new ObjectParameter("ReturnValue", typeof(int));
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ChangePassword(base.UserName, oldPassword, newPassword, base.CustomerID, param);
                    //TODO:Implement a sp to get email of a user
                    returnValue = (int)param.Value > 0;
                    if (returnValue)
                    {
                        //objContainer.AddToFPEmailTranLog(new Data.Admin.FPEmailTranLog
                        //{
                        //    CustomerID = CustomerID,
                        //    EmailPrio = true,
                        //    EmailReqUID = UserName,
                        //    EmailSentTo = base.UserPrincipal.Identity.Email,
                        //    EmailStatus = "Not Done",
                        //    LastUPDUID = UserName,
                        //    EmailSubject = "Change Password",
                        //    EmailBody = "Your passwor has been changed"
                        //});
                        FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                        emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                        {
                            //Email logic has been modified by Vishwa on 05-02-2013
                            CustomerID = CustomerID,
                            EmailBody = string.Empty,
                            EmailReqTime = DateTime.UtcNow,
                            EmailReqUID = UserName,
                            EmailSentTo = base.UserPrincipal.Identity.Email,
                            EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                            EmailSubject = string.Empty,
                            EmailTmplID = FlightPak.Common.Constants.EmailTemplates.ChangePasswordTemplate,
                            EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                            IsBodyHTML = true,
                            LastUPDUID = UserName
                        },
                        new Dictionary<string, string> { 
                                { "[[Client Name]]", base.UserPrincipal.Identity.FirstName + " " + base.UserPrincipal.Identity.LastName }, 
                                { "[[Name]]", "Support Team" }, // TODO: Vishwa
                                });
                    }
                    objContainer.SaveChanges();
                    //if (returnValue)
                    //    EmailManager.SendMail(base.UserPrincipal.Identity.Email, "Change Password", "Your password has been changed successfully.", "Change Password");
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;
        }
        public bool ChangePasswordWithPeriod(string oldPassword, string newPassword, int resetPeriod)
        {
            bool returnValue = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldPassword, newPassword, resetPeriod))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ObjectParameter param = new ObjectParameter("ReturnValue", typeof(int));
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ChangePasswordWithPeriod(base.UserName, oldPassword, newPassword, base.CustomerID,resetPeriod, param);
                    //TODO:Implement a sp to get email of a user
                    returnValue = (int)param.Value > 0;
                    if (returnValue)
                    {
                        //objContainer.AddToFPEmailTranLog(new Data.Admin.FPEmailTranLog
                        //{
                        //    CustomerID = CustomerID,
                        //    EmailPrio = true,
                        //    EmailReqUID = UserName,
                        //    EmailSentTo = base.UserPrincipal.Identity.Email,
                        //    EmailStatus = "Not Done",
                        //    LastUPDUID = UserName,
                        //    EmailSubject = "Change Password",
                        //    EmailBody = "Your passwor has been changed"
                        //});
                        FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                        emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                        {
                            //Email logic has been modified by Vishwa on 05-02-2013
                            CustomerID = CustomerID,
                            EmailBody = string.Empty,
                            EmailReqTime = DateTime.UtcNow,
                            EmailReqUID = UserName,
                            EmailSentTo = base.UserPrincipal.Identity.Email,
                            EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                            EmailSubject = string.Empty,
                            EmailTmplID = FlightPak.Common.Constants.EmailTemplates.ChangePasswordTemplate,
                            EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                            IsBodyHTML = true,
                            LastUPDUID = UserName
                        },
                        new Dictionary<string, string> { 
                                { "[[Client Name]]", base.UserPrincipal.Identity.FirstName + " " + base.UserPrincipal.Identity.LastName }, 
                                { "[[Name]]", "Support Team" }, // TODO: Vishwa
                                });
                    }
                    objContainer.SaveChanges();
                    //if (returnValue)
                    //    EmailManager.SendMail(base.UserPrincipal.Identity.Email, "Change Password", "Your password has been changed successfully.", "Change Password");
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;
        }

        public bool CheckResetpasswordPeriod(string emailId)
        {
            bool returnValue = false;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emailId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ObjectParameter param = new ObjectParameter("ReturnValue", typeof(int));
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.CheckResetpasswordPeriod(emailId, param);
                    //TODO:Implement a sp to get email of a user
                    returnValue = (int)param.Value > 0;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;
        }

        public ReturnValue<Data.Admin.GetUserMasterClient> GetUserMasterClient()
        {
            ReturnValue<Data.Admin.GetUserMasterClient> ReturnValue = null;
            Data.Admin.AdminDataContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.GetUserMasterClient>();
                    Container = new Data.Admin.AdminDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetUserMasterClient(CustomerID, ClientId).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public ReturnValue<Data.Admin.GetPasswordHistory> GetPasswordHistory(string UserName)
        {
            ReturnValue<Data.Admin.GetPasswordHistory> ReturnValue = null;
            Data.Admin.AdminDataContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.GetPasswordHistory>();
                    Container = new Data.Admin.AdminDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetPasswordHistory(UserName).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public ReturnValue<Data.Admin.UserMaster> GetUserByUserName(string userName)
        {
            ReturnValue<Data.Admin.UserMaster> ReturnValue = null;
            Data.Admin.AdminDataContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                    Container = new Data.Admin.AdminDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityInfo = Container.GetUserByUserName(userName).FirstOrDefault();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public ReturnValue<Data.Admin.UserMaster> ResetPasswordWithoutAuth(string userName, string answer, string hashPassword, string emailId)
        {
            ReturnValue<Data.Admin.UserMaster> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, answer, hashPassword, emailId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    
                    //Reset Password
                    if (objContainer.ResetPasswordWithoutAuth(userName, GetHashedPassword(hashPassword), userName, Crypting.Encrypt(answer)) > 0)
                    {
                        //Insert Email

                        //TODO:Implement a sp to get email of a user
                        var email = GetUserByEMailId(userName).EntityList.FirstOrDefault();
                        //objContainer.AddToFPEmailTranLog(new Data.Admin.FPEmailTranLog
                        //{
                        //    CustomerID = email.CustomerID.Value,
                        //    EmailPrio = true,
                        //    EmailReqUID = userName,
                        //    EmailSentTo = email.EmailID,
                        //    EmailStatus = "Not Done",
                        //    LastUPDUID = userName,
                        //    EmailSubject = "Reset Password",
                        //    EmailBody = "Password is " + hashPassword
                        //});
                        //objContainer.SaveChanges();
                        //EmailManager.SendMail(email.EmailID, "Reset Password", "Password is " + hashPassword, "Reset Password");
                        FlightPak.Business.Common.EmailManager emailManager = new EmailManager();
                        emailManager.SendEmail(new FlightPak.Data.Common.FPEmailTranLog
                        {
                            //Email logic has been modified by Vishwa on 05-02-2013
                            CustomerID = email.CustomerID.Value,
                            EmailBody = string.Empty,
                            EmailReqTime = DateTime.UtcNow,
                            EmailReqUID = email.UserName,
                            EmailSentTo = email.EmailID,
                            EmailStatus = FlightPak.Common.Constants.Email.Status.NotDone,
                            EmailSubject = string.Empty,
                            EmailTmplID = FlightPak.Common.Constants.EmailTemplates.ResetPasswordTemplate,
                            EmailType = FlightPak.Common.Constants.Email.EmailConstant,
                            IsBodyHTML = true,
                            LastUPDUID = email.UserName
                        },
                        new Dictionary<string, string> { 
                                { "[[Client Name]]", email.FirstName + " " + email.LastName }, 
                                { "[[User Name]]", email.EmailID },
                                { "[[Password]]", hashPassword },
                                { "[[Name]]", "Support Team" }, // TODO: Vishwa
                                });
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public ReturnValue<Data.Admin.GetUserByEmailId> GetUserByEMailId(string emailId)
        {
            ReturnValue<Data.Admin.GetUserByEmailId> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emailId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.GetUserByEmailId>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetUserByEmailId(emailId).ToList();

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }


        public Int64? GetUsedUserCount(Int64 customerId)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                Data.Admin.AdminDataContainer cs = null;
                Int64? actualUserCount = null;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.Admin.AdminDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    actualUserCount = cs.GetUsedUserCount(customerId).FirstOrDefault();
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return actualUserCount;
            }
        }
        public ReturnValue<Data.Admin.UserMaster> LockOrUnlockUser(string userName, bool isUserLock)
        {
            ReturnValue<Data.Admin.UserMaster> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, isUserLock))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserMaster>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    //Reset Password
                    if (objContainer.LockUnLockUser(userName, isUserLock) > 0)
                    {
                        ReturnValue.ReturnFlag = true;
                    }
                    
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }


    }
}

