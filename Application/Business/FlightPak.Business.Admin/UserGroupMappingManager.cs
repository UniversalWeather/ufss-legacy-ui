﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Business.Common;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Admin
{
    public class UserGroupMappingManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.UserGroup> GetUserGroupAvailableList(string userName, Int64 CustomerID)
        {
            ReturnValue<Data.Admin.UserGroup> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroup>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetUserGroupAvailableList(userName, CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroup> GetUserGroupSelectedList(string userName, Int64 CustomerID)
        {
            ReturnValue<Data.Admin.UserGroup> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(userName, CustomerID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroup>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetUserGroupSelectedList(userName, CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroupMapping> GetList()
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetUserGroupMapping().ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroupMapping> Add(Data.Admin.UserGroupMapping UserGroupMapping)
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupMapping))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.AddToUserGroupMapping(UserGroupMapping);
                    objContainer.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroupMapping> AddAll(Data.Admin.UserGroupMapping UserGroupMapping)
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupMapping))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
                                                                            new KeyValuePair<string, object>("CustomerID", UserGroupMapping.CustomerID ),
                                                                            new KeyValuePair<string, object>("UserGroupMappingID", UserGroupMapping.UserGroupMappingID)
                                                                            };
                    UserGroupMapping.EntityKey = new System.Data.EntityKey("AdminDataContainer.UserGroupMapping", entityKeyValues);
                    using (Data.Admin.AdminDataContainer Container = new Data.Admin.AdminDataContainer())
                    {
                        Container.UserGroupMapping.AddObject(UserGroupMapping);
                        Container.SaveChanges();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;

        }
        public ReturnValue<Data.Admin.UserGroupMapping> UpdateAll(Data.Admin.UserGroupMapping UserGroupMapping)
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupMapping))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
                                                                            new KeyValuePair<string, object>("CustomerID", UserGroupMapping.CustomerID ),
                                                                            new KeyValuePair<string, object>("UserGroupMappingID", UserGroupMapping.UserGroupMappingID)
                                                                            };
                    UserGroupMapping.EntityKey = new System.Data.EntityKey("AdminDataContainer.UserGroupMapping", entityKeyValues);
                    using (Data.Admin.AdminDataContainer Container = new Data.Admin.AdminDataContainer())
                    {
                        Container.UserGroupMapping.Attach(UserGroupMapping);
                        Container.ObjectStateManager.ChangeObjectState(UserGroupMapping, System.Data.EntityState.Modified);
                        Container.SaveChanges();
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;

        }
        public ReturnValue<Data.Admin.UserGroupMapping> Update(Data.Admin.UserGroupMapping UserGroupMapping)
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupMapping))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.UserGroupMapping.Attach(UserGroupMapping);
                    Common.EMCommon.SetAllModified<Data.Admin.UserGroupMapping>(UserGroupMapping, objContainer);
                    objContainer.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroupMapping> Delete(Data.Admin.UserGroupMapping UserGroupMapping)
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserGroupMapping))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.AttachTo(typeof(Data.Admin.UserGroupMapping).Name, UserGroupMapping);
                    objContainer.UserGroupMapping.DeleteObject(UserGroupMapping);
                    objContainer.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroupMapping> GetListInfo()
        {
            ReturnValue<Data.Admin.UserGroupMapping> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.UserGroupMapping>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    ReturnValue.EntityList = objContainer.UserGroupMapping.ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        public ReturnValue<Data.Admin.UserGroupMapping> GetLock(Data.Admin.UserGroupMapping clientCode)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
