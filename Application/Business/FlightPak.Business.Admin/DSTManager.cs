﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Admin
{
    public class DSTManager : BaseManager
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for Dst page from Dst table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.Admin.DSTRegion> GetList()
        {
            ReturnValue<Data.Admin.DSTRegion> objDSTRegionType = new ReturnValue<Data.Admin.DSTRegion>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objDSTRegionType.EntityList = objContainer.GetAllDSTRegion().ToList();
                    objDSTRegionType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objDSTRegionType;
        }
        /// <summary>
        ///  To insert values from Dst page to Dst table
        /// </summary>
        public ReturnValue<Data.Admin.DSTRegion> Add(Data.Admin.DSTRegion DSTRegionType)
        {
            ReturnValue<Data.Admin.DSTRegion> objDSTRegionType = new ReturnValue<Data.Admin.DSTRegion>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DSTRegionType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.AddToDSTRegions(DSTRegionType);
                    objContainer.SaveChanges();
                    objDSTRegionType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objDSTRegionType;
        }
        /// <summary>
        /// To insert values from Dst page to Dst table
        /// </summary>
        /// <param name="DSTRegionType"></param>
        /// <returns></returns>
        public ReturnValue<Data.Admin.DSTRegion> Update(Data.Admin.DSTRegion DSTRegionType)
        {
            ReturnValue<Data.Admin.DSTRegion> objDSTRegionType = new ReturnValue<Data.Admin.DSTRegion>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DSTRegionType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.DSTRegions.Attach(DSTRegionType);
                    Common.EMCommon.SetAllModified<Data.Admin.DSTRegion>(DSTRegionType, objContainer);
                    objContainer.SaveChanges();
                    objDSTRegionType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objDSTRegionType;
        }
        /// <summary>
        /// To update the selected record as deleted, from Dst page in Dst table
        /// </summary>
        /// <param name="DSTRegionType"></param>
        /// <returns></returns>
        public ReturnValue<Data.Admin.DSTRegion> Delete(Data.Admin.DSTRegion DSTRegionType)
        {
            ReturnValue<Data.Admin.DSTRegion> objDSTRegionType = new ReturnValue<Data.Admin.DSTRegion>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DSTRegionType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Admin.AdminDataContainer objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.DSTRegions.Attach(DSTRegionType);
                    objContainer.DSTRegions.DeleteObject(DSTRegionType);
                    objContainer.SaveChanges();
                    objDSTRegionType.ReturnFlag = true;
                    return objDSTRegionType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objDSTRegionType;



        }
    }
}
