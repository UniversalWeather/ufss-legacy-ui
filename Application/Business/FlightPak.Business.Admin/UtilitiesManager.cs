﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.Admin
{
    public class UtilitiesManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Admin.GetAllLoggedUsersByCustomerId> GetAllLoggedInUsers(bool includeCurrentUser)
        {

            ReturnValue<Data.Admin.GetAllLoggedUsersByCustomerId> ReturnValue = null;
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.Admin.GetAllLoggedUsersByCustomerId>();
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetAllLoggedUsersByCustomerId(CustomerID, UserName, includeCurrentUser).ToList();

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public void LogOutAllUsers(string userNameList)
        {
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    string[] arrUserName = userNameList.Split(',');
                    foreach (var item in arrUserName)
                        objContainer.LogOutSelectedUsersByCustomerId(CustomerID, item);


                }, FlightPak.Common.Constants.Policy.DataLayer);

            }


        }

        public void LockCustomer()
        {
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.LockCustomer(CustomerID);


                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }

        public void UnlockCustomer()
        {
            Data.Admin.AdminDataContainer objContainer = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objContainer = new Data.Admin.AdminDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.UnlockCustomer(CustomerID);


                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }
    }
}
