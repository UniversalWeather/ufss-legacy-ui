﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Data.CorporateRequest;
using FlightPak.Business.Common;
using FlightPak.Business.ICorporateRequest;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.ServiceAgents;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Net.Mail;
using System.Reflection;



namespace FlightPak.Business.CorporateRequest
{
    public partial class CorporateRequestManager : BaseManager, ICorporateRequestManager
    {
        private ExceptionManager exManager;
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        #region ExceptionHandling
        List<ExceptionTemplate> TripExceptions = new List<ExceptionTemplate>();
        List<CRException> CorpExceptions = new List<CRException>();
        #endregion


        #region "Request"

        public ReturnValue<CRMain> UpdateRequest(CRMain request)
        {
            ReturnValue<CRMain> ReturnValue = new ReturnValue<CRMain>();
            CorporateRequestDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(request))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new CorporateRequestDataModelContainer())
                        {

                            request.CustomerID = CustomerID;
                            List<CRException> ExceptionList = new List<CRException>();
                            List<ExceptionTemplate> ExceptionTemplateList = new List<ExceptionTemplate>();
                            List<CRException> eSeverityException = new List<CRException>();

                            bool SaveTrip = true;

                            if (request.RecordType == "W")
                            {
                                base.CheckLock = true;
                                ReturnValue.ReturnFlag = base.Validate(request, ref ReturnValue.ErrorMessage);

                                if (ReturnValue.ReturnFlag)
                                {

                                    if (request.CRLegs != null && request.CRLegs.Count > 0)
                                    {
                                        foreach (CRLeg Leg in request.CRLegs.Where(x => x.IsDeleted == false).ToList())
                                        {
                                            ReturnValue<CRLeg> ReturnLegValue = new ReturnValue<CRLeg>();
                                            base.CheckLock = false;
                                            ReturnLegValue.ReturnFlag = base.Validate(Leg, ref ReturnLegValue.ErrorMessage);

                                            if (!ReturnLegValue.ReturnFlag)
                                            {
                                                SaveTrip = false;
                                                ReturnLegValue.ErrorMessage = "Leg " + Leg.LegNUM + ": \\n" + ReturnLegValue.ErrorMessage;
                                                ReturnValue.ErrorMessage += ReturnLegValue.ErrorMessage;
                                                break;
                                            }
                                        }
                                    }


                                }
                                else
                                    SaveTrip = false;
                            }

                            if (SaveTrip)
                            {

                                if (request.RecordType == "W")
                                {
                                    ExceptionList = ValidateRequest(request);
                                    ExceptionTemplateList = Container.ExceptionTemplates.Where(x => x.ModuleID == (long)CorporateRequestExceptionModule.CRMain).ToList();

                                    eSeverityException = (from Exp in ExceptionList
                                                          join ExpTempl in ExceptionTemplateList on Exp.ExceptionTemplateID equals ExpTempl.ExceptionTemplateID
                                                          where ExpTempl.Severity == 1
                                                          select Exp).ToList();
                                }
                                if (eSeverityException != null && eSeverityException.Count > 1)
                                {
                                    ReturnValue.ReturnFlag = false;
                                    ReturnValue.ErrorMessage = "Request has mandatory Exception, Request not saved!";
                                }
                                else
                                {
                                    request.CRExceptions.Clear();

                                    foreach (CRException corpException in ExceptionList)
                                    {
                                        CRException NewExeption = new CRException();
                                        NewExeption.CRMainID = request.CRMainID;
                                        NewExeption.CustomerID = corpException.CustomerID;
                                        NewExeption.CRExceptionDescription = corpException.CRExceptionDescription;
                                        NewExeption.LastUpdUID = UserPrincipal.Identity.Name;
                                        NewExeption.LastUpdTS = DateTime.UtcNow;
                                        NewExeption.ExceptionTemplateID = corpException.ExceptionTemplateID;
                                        NewExeption.DisplayOrder = corpException.DisplayOrder;
                                        request.CRExceptions.Add(NewExeption);
                                    }
                                    UpdateEntity(ref request, ref Container);
                                    Container.SaveChanges();
                                    //Unlock
                                    LockManager<Data.CorporateRequest.CRMain> lockManager = new LockManager<CRMain>();
                                    lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CorporateRequest.CRMain, request.CRMainID);
                                    ReturnValue = GetRequest(request.CRMainID);

                                    ReturnValue.EntityInfo = ReturnValue.EntityList[0];

                                    ReturnValue.ReturnFlag = true;
                                }
                            }
                            else
                            {
                                ReturnValue.ReturnFlag = false;
                                ReturnValue.EntityInfo = request;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        //manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                        ReturnValue.EntityInfo = request;
                    }
                    finally
                    {

                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        /// <summary>
        /// Method to Validate Trip information
        /// </summary>
        /// <returns>Validate Business Exceptions</returns>
        private List<CRException> ValidateRequest(CRMain CorpRequest)
        {
            double minRunWay = 0, maxRunWay = 0;
            var DepartAirportLists = new List<Airport>();
            var ArrAirportLists = new List<Airport>();
            var FleetLists = new List<Fleet>();
            List<CRException> corpExceptions = new List<CRException>();

            ////#region Main Exception
            CorporateRequestDataModelContainer Container;
            List<ExceptionTemplate> CorpMain_TemplateList;
            List<ExceptionTemplate> CorpLeg_TemplateList;
            List<ExceptionTemplate> CorpPax_TemplateList;
            List<long> PassengerPassportCrewlists = new List<long>();
            List<long> TypeRatingCrewlists = new List<long>();
            List<long> TripOverlapLists = new List<long>();
            List<long> DutyRuleCrewNotQualifiedlists = new List<long>();
            List<Int64> InActiveCrewlists = new List<Int64>();
            List<long> PassengerPassportPaxlists = new List<long>();
            List<long> AircraftFarRuleLegLists = new List<long>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CorpRequest))
            {
                corpExceptions = new List<CRException>();
                ////#region Main Exception

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Container = new CorporateRequestDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        TripExceptions = Container.ExceptionTemplates.Where(X => X.ModuleID == (long)(CorporateRequestExceptionModule.CRMain)).ToList();
                        List<CRLeg> Preflegs = CorpRequest.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                        CorpMain_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)CorporateRequestExceptionSubModule.CRmain).ToList();
                        CorpLeg_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)CorporateRequestExceptionSubModule.Legs).ToList();
                        CorpPax_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)CorporateRequestExceptionSubModule.Passenger).ToList();

                        #region Main Validationn
                        foreach (ExceptionTemplate exptemp in CorpMain_TemplateList.ToList())
                        {
                            #region Aicraft Null check
                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.AircraftTypeReq))
                            {
                                if (CorpRequest.AircraftID == null)
                                {
                                    CRException CurrException = new CRException();
                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                    CurrException.CustomerID = CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    CurrException.DisplayOrder = "000000000000" + "001";
                                    corpExceptions.Add(CurrException);
                                }
                            }
                            #endregion

                            //0000000000000 + 002
                            #region "Atleast one Leg Check"
                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.RequestRequiresAtleastoneLeg))
                            {
                                if (CorpRequest.CRLegs == null || CorpRequest.CRLegs.Count == 0)
                                {
                                    CRException CurrException = new CRException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                    CurrException.CustomerID = CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    CurrException.DisplayOrder = "000000000000" + "002";
                                    corpExceptions.Add(CurrException);
                                }
                            }
                            #endregion

                            //0000000000000 + 004
                            #region "DepartMent Inactive"
                            if (CorpRequest.DepartmentID != null)
                            {
                                if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.RequestDepartmentInactive))
                                {
                                    long DepartmentID = (long)CorpRequest.DepartmentID;
                                    var DepartMentList = (from arrLists in Container.Departments
                                                          where (arrLists.DepartmentID == DepartmentID)
                                                          select arrLists).ToList();
                                    if (DepartMentList != null && DepartMentList.Count > 0)
                                    {
                                        if (DepartMentList[0].IsInActive == true)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = CorpRequest.CRMainID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplate1;
                                            CurrException.CRExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            CurrException.DisplayOrder = "000000000000" + "004";
                                            corpExceptions.Add(CurrException);
                                        }
                                    }
                                }
                            }
                            #endregion

                            //0000000000000 + 005
                            #region "Authorization Inactive"
                            if (CorpRequest.AuthorizationID != null)
                            {
                                if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.TripAuthorizationInactive))
                                {
                                    long AuthorizationID = (long)CorpRequest.AuthorizationID;
                                    var Authorizationlist = (from arrLists in Container.DepartmentAuthorizations
                                                             where (arrLists.AuthorizationID == AuthorizationID) //AirportID & ArrivalICAOID are equal
                                                             select arrLists).ToList();
                                    if (Authorizationlist != null && Authorizationlist.Count > 0)
                                    {
                                        if (Authorizationlist[0].IsInActive == true)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = CorpRequest.CRMainID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplate1;
                                            CurrException.CRExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            CurrException.DisplayOrder = "000000000000" + "005";
                                            corpExceptions.Add(CurrException);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        #endregion


                        if (Preflegs != null && Preflegs.Count > 0)
                        {
                            #region Leg Validation
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                foreach (ExceptionTemplate exptemp in CorpLeg_TemplateList.ToList())
                                {

                                    #region DepartICaoID
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.DepartureICAOIDReq))
                                    {
                                        if (Preflegs[i].DAirportID == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = CorpRequest.CRMainID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplate1;
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            CurrException.CRExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String length = ("000" + Preflegs[i].LegNUM.ToString());


                                            length = length.Substring(length.Count() - 3, 3);

                                            CurrException.DisplayOrder = length + "000000001" + "000";
                                            corpExceptions.Add(CurrException);

                                        }
                                    }
                                    #endregion

                                    //010 + 000000002 + 000
                                    //000000002
                                    #region "Arrival ICaoID"
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.ArrivalICAOIDReq))
                                    {
                                        if (Preflegs[i].AAirportID == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = CorpRequest.CRMainID;
                                            CurrException.CustomerID = CustomerID;
                                            string desc = string.Empty;
                                            desc = exptemp.ExceptionTemplate1;
                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                            CurrException.CRExceptionDescription = desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            String length = ("000" + Preflegs[i].LegNUM.ToString());

                                            length = length.Substring(length.Count() - 3, 3);

                                            CurrException.DisplayOrder = length + "000000002" + "000";
                                            corpExceptions.Add(CurrException);
                                        }

                                    }
                                    #endregion

                                    //000000003
                                    #region "Arrival Airport Inactive"
                                    if (Preflegs[i].AAirportID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.ArrivalAirportInActive))
                                        {
                                            long arrivalAirportId = (long)Preflegs[i].AAirportID;
                                            ArrAirportLists = (from arrLists in Container.Airports
                                                               where (arrLists.AirportID == arrivalAirportId) //AirportID & ArrivalICAOID are equal
                                                               select arrLists).ToList();
                                            if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                            {
                                                if (ArrAirportLists[0].IsInActive == true)
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000003" + "000";
                                                    corpExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //000000004
                                    #region "Depart Airport Inactive"
                                    if (Preflegs[i].DAirportID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.DepartureAirportInActive))
                                        {
                                            long departAirportId = (long)Preflegs[i].DAirportID;
                                            DepartAirportLists = (from departLists in Container.Airports
                                                                  where (departLists.AirportID == departAirportId)
                                                                  select departLists).ToList();
                                            if (DepartAirportLists != null && DepartAirportLists.Count > 0)
                                            {
                                                if (DepartAirportLists[0].IsInActive == true)
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000004" + "000";
                                                    corpExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //000000005
                                    #region Distance Beyond Aircraft Capability
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LegDistbeyondAircraftCapability))
                                    {
                                        if (CorpRequest.AircraftID != null)
                                        {
                                            double aircrPS1 = 0.0, aircrPS2 = 0.0, aircrPS3 = 0.0;
                                            var airCraftLists = (from airCrafts in Container.Aircraft
                                                                 where (airCrafts.AircraftID == (long)CorpRequest.AircraftID)
                                                                 select airCrafts).ToList();
                                            if (airCraftLists != null) //&& airCraftLists.Count > 0
                                            {
                                                if (airCraftLists[0].PowerSettings1TrueAirSpeed != null && airCraftLists[0].PowerSettings1HourRange != null)
                                                    aircrPS1 = ((double)(airCraftLists[0].PowerSettings1TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings1HourRange));
                                                else
                                                    aircrPS1 = 0;

                                                if (airCraftLists[0].PowerSettings2TrueAirSpeed != null && airCraftLists[0].PowerSettings2HourRange != null)
                                                    aircrPS2 = ((double)(airCraftLists[0].PowerSettings2TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings2HourRange));
                                                else
                                                    aircrPS2 = 0;

                                                if (airCraftLists[0].PowerSettings3TrueAirSpeed != null && airCraftLists[0].PowerSettings3HourRange != null)
                                                    aircrPS3 = ((double)(airCraftLists[0].PowerSettings3TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings3HourRange));
                                                else
                                                    aircrPS3 = 0;

                                                if (Preflegs[i].Distance != null)
                                                {

                                                    if (Preflegs[i].PowerSetting == "1" && aircrPS1 < (double)Preflegs[i].Distance)
                                                    {
                                                        CRException CurrException = new CRException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.CRMainID = CorpRequest.CRMainID;
                                                        CurrException.CustomerID = CustomerID;
                                                        string desc = string.Empty;
                                                        desc = exptemp.ExceptionTemplate1;
                                                        desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                        CurrException.CRExceptionDescription = desc;
                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                        CurrException.ExceptionTemplate = exptemp;
                                                        String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                        length = length.Substring(length.Count() - 3, 3);

                                                        CurrException.DisplayOrder = length + "000000005" + "000";
                                                        corpExceptions.Add(CurrException);
                                                    }
                                                    if (Preflegs[i].PowerSetting == "2" && aircrPS2 < (double)Preflegs[i].Distance)
                                                    {
                                                        CRException CurrException = new CRException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.CRMainID = CorpRequest.CRMainID;
                                                        CurrException.CustomerID = CustomerID;
                                                        string desc = string.Empty;
                                                        desc = exptemp.ExceptionTemplate1;
                                                        desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                        CurrException.CRExceptionDescription = desc;
                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                        CurrException.ExceptionTemplate = exptemp;
                                                        String length = ("000" + Preflegs[i].LegNUM.ToString());
                                                        length = length.Substring(length.Count() - 3, 3);

                                                        CurrException.DisplayOrder = length + "000000005" + "000";
                                                        corpExceptions.Add(CurrException);
                                                    }
                                                    if (Preflegs[i].PowerSetting == "3" && aircrPS3 < (double)Preflegs[i].Distance)
                                                    {
                                                        CRException CurrException = new CRException();
                                                        ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                        CurrException.CRMainID = CorpRequest.CRMainID;
                                                        CurrException.CustomerID = CustomerID;
                                                        string desc = string.Empty;
                                                        desc = exptemp.ExceptionTemplate1;
                                                        desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                        CurrException.CRExceptionDescription = desc;
                                                        CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                        CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                        CurrException.LastUpdTS = DateTime.UtcNow;
                                                        CurrException.ExceptionTemplate = exptemp;
                                                        String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                        length = length.Substring(length.Count() - 3, 3);

                                                        CurrException.DisplayOrder = length + "000000005" + "000";
                                                        corpExceptions.Add(CurrException);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    //000000006
                                    #region Arrival Airport Max runway below aircaft requirements

                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.ArrivalAirportMaxRunwayBelowAircraftRequirements))
                                    {

                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (CorpRequest.FleetID != null)
                                                {
                                                    if (CorpRequest.FleetID != 0)
                                                    {
                                                        FleetLists = (from fleetLists in Container.Fleets
                                                                      where (fleetLists.FleetID == CorpRequest.FleetID)
                                                                      select fleetLists).ToList();

                                                        if (FleetLists != null)
                                                        {
                                                            if (FleetLists.Count > 0)
                                                            {
                                                                if (FleetLists[0].MimimumRunway != null)
                                                                    //if ((double)FleetLists[0].MimimumRunway != 0.0)
                                                                    minRunWay = (double)FleetLists[0].MimimumRunway;
                                                                else
                                                                    minRunWay = 0.0;
                                                            }
                                                        }


                                                        long arrivalAirportId = (long)Preflegs[i].AAirportID;
                                                        ArrAirportLists = (from arrLists in Container.Airports
                                                                           where (arrLists.AirportID == arrivalAirportId) //AirportID & ArrivalICAOID are equal
                                                                           select arrLists).ToList();

                                                        if (ArrAirportLists != null)
                                                        {
                                                            if (ArrAirportLists.Count > 0)
                                                            {
                                                                if (ArrAirportLists[0].LongestRunway != null)
                                                                    //if ((double)ArrAirportLists[0].WidthLengthRunway != 0.0)
                                                                    maxRunWay = (double)ArrAirportLists[0].LongestRunway; // SAid by Sujitha
                                                                else
                                                                    maxRunWay = 0.0;
                                                            }
                                                        }
                                                        if (minRunWay > maxRunWay)
                                                        {
                                                            CRException CurrException = new CRException();
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.CRMainID = CorpRequest.CRMainID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplate1;
                                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                            CurrException.CRExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                            length = length.Substring(length.Count() - 3, 3);

                                                            CurrException.DisplayOrder = length + "000000006" + "000";
                                                            corpExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    ////000000007
                                    //#region Aircraft FARRule violation exists Aircraft not assigned to FAR
                                    //if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.AircraftFARRuleviolationexistsAircraftnotassignedtoFAR))
                                    //{
                                    //    if (CorpRequest.FleetID != null)
                                    //    {
                                    //        if (CorpRequest.FleetID != 0)
                                    //        {
                                    //            FleetLists = (from fleetLists in Container.Fleets
                                    //                          where (fleetLists.FleetID == CorpRequest.FleetID)
                                    //                          select fleetLists).ToList();

                                    //            if (FleetLists != null)
                                    //            {
                                    //                if (FleetLists.Count > 0)
                                    //                {

                                    //                    if (Preflegs[i].FedAviationRegNUM != null)
                                    //                    {
                                    //                        if (Preflegs[i].FedAviationRegNUM == "91" && (FleetLists[0].IsFAR91 == null ? false : FleetLists[0].IsFAR91) == false || Preflegs[i].FedAviationRegNUM == "135" && (FleetLists[0].IsFAR135 == null ? false : FleetLists[0].IsFAR135) == false)
                                    //                        {
                                    //                            CRException CurrException = new CRException();
                                    //                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    //                            CurrException.CRMainID = CorpRequest.CRMainID;
                                    //                            CurrException.CustomerID = CustomerID;
                                    //                            string desc = string.Empty;
                                    //                            desc = exptemp.ExceptionTemplate1;
                                    //                            if (!string.IsNullOrEmpty(Preflegs[i].FedAviationRegNUM))
                                    //                                desc = desc + Preflegs[i].FedAviationRegNUM;
                                    //                            desc = desc + " " + "in " + " " + "leg " + Preflegs[i].LegNUM;
                                    //                            CurrException.CRExceptionDescription = desc;
                                    //                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    //                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    //                            CurrException.LastUpdTS = DateTime.UtcNow;
                                    //                            CurrException.ExceptionTemplate = exptemp;
                                    //                            String length = ("000" + Preflegs[i].LegNUM.ToString());

                                    //                            length = length.Substring(length.Count() - 3, 3);

                                    //                            CurrException.DisplayOrder = length + "000000007" + "000";
                                    //                            AircraftFarRuleLegLists.Add((long)Preflegs[i].LegID);
                                    //                            corpExceptions.Add(CurrException);

                                    //                        }
                                    //                        //}
                                    //                    }
                                    //                }
                                    //            }
                                    //        }
                                    //    }
                                    //}
                                    //#endregion

                                    //000000008
                                    #region LegTime Aloft AircraftCapability
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LegTimeAloftAircraftCapability))
                                    {

                                        if (CorpRequest.AircraftID != null)
                                        {
                                            var airCraftLists = (from airCrafts in Container.Aircraft
                                                                 where (airCrafts.AircraftID == (long)CorpRequest.AircraftID)
                                                                 select airCrafts).ToList();

                                            if (Preflegs[i].ElapseTM != null)
                                            {
                                                if (Preflegs[i].PowerSetting == "1" && (airCraftLists[0].PowerSettings1HourRange != null && Preflegs[i].ElapseTM != null && (double)airCraftLists[0].PowerSettings1HourRange < (double)Preflegs[i].ElapseTM))
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());


                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000008" + "000";
                                                    corpExceptions.Add(CurrException);
                                                }
                                                if (Preflegs[i].PowerSetting == "2" && (airCraftLists[0].PowerSettings2HourRange != null && Preflegs[i].ElapseTM != null && (double)airCraftLists[0].PowerSettings2HourRange < (double)Preflegs[i].ElapseTM))
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());


                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000008" + "000";
                                                    corpExceptions.Add(CurrException);
                                                }
                                                if (Preflegs[i].PowerSetting == "3" && (airCraftLists[0].PowerSettings3HourRange != null && Preflegs[i].ElapseTM != null && (double)airCraftLists[0].PowerSettings3HourRange < (double)Preflegs[i].ElapseTM))
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());
                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000008" + "000";
                                                    corpExceptions.Add(CurrException);
                                                }
                                            }
                                        }


                                    }
                                    #endregion

                                    //000000009
                                    //010 + 000000009+ 001  002  010
                                    //010000000009001
                                    //010000000009002
                                    //010000000009010
                                    #region AircraftConflictExistsForLeg
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.AircraftConflictExistsForLeg))
                                    {
                                        if (CorpRequest.FleetID != null)
                                        {
                                            if (CorpRequest.FleetID != 0)
                                            {
                                                if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                                {
                                                    if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                                    {
                                                        List<GetAircraftConflictforCorporate> LegconflictList = new List<GetAircraftConflictforCorporate>();
                                                        LegconflictList = Container.GetAircraftConflictforCorporate((long)CorpRequest.CRMainID, (long)CorpRequest.FleetID, CustomerID, Preflegs[i].DepartureGreenwichDTTM, Preflegs[i].ArrivalGreenwichDTTM).ToList();

                                                        if (LegconflictList != null && LegconflictList.Count > 0)
                                                        {

                                                            string increstr = string.Empty;
                                                            int incrementCount = 0;
                                                            foreach (GetAircraftConflictforCorporate AircraftConflit in LegconflictList)
                                                            {
                                                                CRException CurrException = new CRException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                ExceptionTempl = exptemp;
                                                                CurrException.CRMainID = CorpRequest.CRMainID;
                                                                CurrException.CustomerID = CustomerID;
                                                                string Desc = string.Empty;

                                                                Desc = "Leg " + Preflegs[i].LegNUM + ":";
                                                                Desc = Desc + " Tail :" + AircraftConflit.TailNum;


                                                                if (AircraftConflit.RecordType == "T")
                                                                {
                                                                    Desc = Desc + " Conflicts Trip Number: " + AircraftConflit.CRTripNUM;
                                                                    Desc = Desc + " Depart: " + AircraftConflit.DepartICaoID;
                                                                    Desc = Desc + " Arrival: " + AircraftConflit.ArrivalICaoID;

                                                                }
                                                                else
                                                                {
                                                                    Desc = Desc + " Conflicts Fleet Calendar Entry";// +AircraftConflit.TripNUM;
                                                                }

                                                                Desc = Desc + " Date/Time: " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", AircraftConflit.DepartureGreenwichDTTM);
                                                                Desc = Desc + " To " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", AircraftConflit.ArrivalGreenwichDTTM);

                                                                if (AircraftConflit.RecordType == "M")
                                                                    Desc = Desc + " Duty: " + AircraftConflit.DutyTYPE;

                                                                CurrException.CRExceptionDescription = Desc;
                                                                CurrException.ExceptionTemplateID = ExceptionTempl.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                String length = ("000" + Preflegs[i].LegNUM.ToString());
                                                                length = length.Substring(length.Count() - 3, 3);

                                                                increstr = incrementCount.ToString();
                                                                increstr = "000" + increstr;
                                                                increstr = increstr.Substring(increstr.Count() - 3, 3);
                                                                CurrException.DisplayOrder = length + "000000009" + increstr;
                                                                corpExceptions.Add(CurrException);
                                                                incrementCount = incrementCount + 1;
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                    }
                                    #endregion




                                    //010000000010 + 001
                                    #region "DepartMent Inactive"
                                    if (Preflegs[i].DepartmentID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LegDepartmentInactive))
                                        {
                                            long DepartmentID = (long)Preflegs[i].DepartmentID;
                                            var DepartMentList = (from arrLists in Container.Departments
                                                                  where (arrLists.DepartmentID == DepartmentID) //AirportID & ArrivalICAOID are equal
                                                                  select arrLists).ToList();
                                            if (DepartMentList != null && DepartMentList.Count > 0)
                                            {
                                                if (DepartMentList[0].IsInActive == true)
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());
                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000010" + "001";


                                                    corpExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    //0100000000011 + 002
                                    #region "Authorization Inactive"
                                    if (Preflegs[i].AuthorizationID != null)
                                    {
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LegAuthorizationInactive))
                                        {
                                            long AuthorizationID = (long)Preflegs[i].AuthorizationID;
                                            var Authorizationlist = (from arrLists in Container.DepartmentAuthorizations
                                                                     where (arrLists.AuthorizationID == AuthorizationID) //AirportID & ArrivalICAOID are equal
                                                                     select arrLists).ToList();
                                            if (Authorizationlist != null && Authorizationlist.Count > 0)
                                            {
                                                if (Authorizationlist[0].IsInActive == true)
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());
                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000010" + "002";
                                                    corpExceptions.Add(CurrException);
                                                }
                                            }
                                        }
                                    }
                                    #endregion



                                    //Leg prev and next ovelap
                                    if (i + 1 < Preflegs.Count)
                                    {
                                        #region CurrentLeg and Prev Leg Exceeds Default System Range
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.CurrentandPreviousLegExceedsDefaultsSystemRange))
                                        {
                                            if (Preflegs[i].ArrivalDTTMLocal != null && Preflegs[i + 1].DepartureDTTMLocal != null)
                                            {
                                                if (Preflegs[i].ArrivalDTTMLocal < Preflegs[i + 1].DepartureDTTMLocal)
                                                {
                                                    if (UserPrincipal.Identity.Settings.TripsheetDTWarning != null)
                                                    {
                                                        if ((double)UserPrincipal.Identity.Settings.TripsheetDTWarning > 0)
                                                        {
                                                            TimeSpan span = ((DateTime)Preflegs[i + 1].DepartureDTTMLocal).Subtract((DateTime)Preflegs[i].ArrivalDTTMLocal);
                                                            if (((span.Days * 24 * 60) + (span.Hours * 60) + (span.Minutes)) > (double)UserPrincipal.Identity.Settings.TripsheetDTWarning * 24 * 60)
                                                            {
                                                                CRException CurrException = new CRException();
                                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                                CurrException.CRMainID = CorpRequest.CRMainID;
                                                                CurrException.CustomerID = CustomerID;
                                                                string desc = string.Empty;
                                                                desc = exptemp.ExceptionTemplate1;
                                                                desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                                CurrException.CRExceptionDescription = desc;
                                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                                CurrException.ExceptionTemplate = exptemp;
                                                                String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                                length = length.Substring(length.Count() - 3, 3);

                                                                CurrException.DisplayOrder = length + "000000010" + "000";
                                                                corpExceptions.Add(CurrException);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Leg Arrival Date over laps with Next Departure date
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.CurrentLegArrivalDateTimeOverlapsNextLegDepartureDateTime))
                                        {
                                            if (Preflegs[i + 1].DepartureGreenwichDTTM != null && Preflegs[i].ArrivalGreenwichDTTM != null)
                                            {
                                                if (Preflegs[i].ArrivalDTTMLocal > Preflegs[i + 1].DepartureDTTMLocal)
                                                {
                                                    CRException CurrException = new CRException();
                                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                    CurrException.CRMainID = CorpRequest.CRMainID;
                                                    CurrException.CustomerID = CustomerID;
                                                    string desc = string.Empty;
                                                    desc = exptemp.ExceptionTemplate1;
                                                    desc = desc.Replace("<legnum1>", Preflegs[i].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i].ArrivalDTTMLocal));
                                                    desc = desc.Replace("<legnum2>", Preflegs[i + 1].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i + 1].DepartureDTTMLocal));
                                                    CurrException.CRExceptionDescription = desc;
                                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                                    CurrException.ExceptionTemplate = exptemp;
                                                    String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                    length = length.Substring(length.Count() - 3, 3);

                                                    CurrException.DisplayOrder = length + "000000011" + "000";
                                                    corpExceptions.Add(CurrException);
                                                }
                                            }

                                        }
                                        #endregion
                                        /////////////////////////////////////////////////problem is here /////////////////////////////////////////////////////////////////////
                                        #region Leg Dates Over Lap
                                        if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.CurrentLegLocalDepartDateTimeOverlapsPrevLegLocalArrivalDateTime))
                                        {
                                            if (Preflegs[i].ArrivalGreenwichDTTM > Preflegs[i + 1].DepartureGreenwichDTTM)
                                            {
                                                CRException CurrException = new CRException();
                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                CurrException.CRMainID = CorpRequest.CRMainID;
                                                CurrException.CustomerID = CustomerID;
                                                string desc = string.Empty;
                                                desc = exptemp.ExceptionTemplate1;
                                                desc = desc.Replace("<legnum1>", Preflegs[i + 1].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i + 1].DepartureDTTMLocal));
                                                desc = desc.Replace("<legnum2>", Preflegs[i].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", Preflegs[i].ArrivalDTTMLocal));
                                                CurrException.CRExceptionDescription = desc;
                                                CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                CurrException.LastUpdTS = DateTime.UtcNow;
                                                CurrException.ExceptionTemplate = exptemp;
                                                String length = ("000" + Preflegs[i].LegNUM.ToString());

                                                length = length.Substring(length.Count() - 3, 3);

                                                CurrException.DisplayOrder = length + "000000012" + "000";
                                                corpExceptions.Add(CurrException);
                                            }
                                        }
                                        #endregion


                                    }

                                }
                            }
                            #endregion

                        }
                        if (Preflegs != null && Preflegs.Count > 0)
                        {
                            //leg + paxcode + runn
                            //010 + 00MURI + 009 + 000
                            #region PAX Validation
                            for (int i = 0; i < Preflegs.Count; i++)
                            {
                                foreach (ExceptionTemplate exptemp in CorpPax_TemplateList.ToList())
                                {
                                    #region TripPax - Number of Passengers Exceeds Aircraft Seat Capacity
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.NumberofPassengersExceedsAircraftSeatCapacity))
                                    {
                                        if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                                        {
                                            if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                            {
                                                if (Preflegs[i].IsDeleted == false)
                                                {

                                                    if (Preflegs[i].ReservationAvailable != null && Preflegs[i].ReservationAvailable < 0)
                                                    {
                                                        if (Preflegs[i].PassengerTotal != null)
                                                        {
                                                            CRException CurrException = new CRException();
                                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                            CurrException.CRMainID = CorpRequest.CRMainID;
                                                            CurrException.CustomerID = CustomerID;
                                                            string desc = string.Empty;
                                                            desc = exptemp.ExceptionTemplate1;
                                                            desc = desc.Replace("<PassengerCount>", Preflegs[i].PassengerTotal.ToString());
                                                            desc = desc.Replace("<legnum>", Preflegs[i].LegNUM.ToString());
                                                            if (Preflegs[i].SeatTotal > 0)
                                                                desc = desc + " : " + Preflegs[i].SeatTotal.ToString();//+ ":" + Preflegs[i].SeatTotal.ToString();
                                                            else
                                                                desc = desc + " : " + "0";
                                                            CurrException.CRExceptionDescription = desc;
                                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                                            CurrException.ExceptionTemplate = exptemp;
                                                            String lengthstr = ("000" + Preflegs[i].LegNUM.ToString());
                                                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                            CurrException.DisplayOrder = lengthstr + "000000000" + "000";
                                                            corpExceptions.Add(CurrException);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }

                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return corpExceptions;

        }

        private string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                int val;
                if (!string.IsNullOrEmpty(time))
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    if (time.IndexOf(".") != -1)
                    {
                        string[] timeArray = time.Split('.');
                        decimal hour = Convert.ToDecimal(timeArray[0]);
                        decimal minute = Convert.ToDecimal(timeArray[1]);
                        decimal decimal_min = Convert.ToDecimal(String.Concat("0.", minute.ToString()));
                        decimal decimalOfMin = 0;
                        if (decimal_min > 0) // Added conditon to avoid divide by zero error.
                            decimalOfMin = (decimal_min * 60 / 100);
                        decimal finalval = Math.Round(hour + decimalOfMin, 2);
                        result = Convert.ToString(finalval).Replace(".", ":");
                        if (result.IndexOf(":") < 0)
                            result = result + ":00";

                        timeArray = result.Split(':');
                        if (timeArray[1].Length == 1)
                            result = result + "0";
                    }
                    else if (int.TryParse(time, out val))
                    {
                        result = time;
                    }
                }

                return result;
            }
        }

        private void UpdateEntity(ref CRMain CorpRequest, ref CorporateRequestDataModelContainer Container)
        {
            int display = 0;
            // Load original parent including the child item collection 
            #region "History"
            StringBuilder HistoryDescription = new System.Text.StringBuilder();
            #endregion


            CorpRequest.LastUpdTS = DateTime.UtcNow;
            CorpRequest.LastUpdUID = UserPrincipal.Identity.Name;

            var MainId = CorpRequest.CRMainID;
            CRMain crMainentity = Container.CRMains
                .Where(p => p.CRMainID == MainId).SingleOrDefault();

            #region Main
            if (CorpRequest.State == CorporateRequestTripEntityState.Modified)
            {
                Container.CRMains.Attach(crMainentity);
                Container.CRMains.ApplyCurrentValues(CorpRequest);

                #region "History For Modified Request"
                var originalValues = Container.ObjectStateManager.GetObjectStateEntry(crMainentity).OriginalValues;
                var CurrentValues = Container.ObjectStateManager.GetObjectStateEntry(crMainentity).CurrentValues;
                PropertyInfo[] properties = typeof(CRMain).GetProperties();

                for (int i = 0; i < originalValues.FieldCount; i++)
                {
                    var fieldname = originalValues.GetName(i);

                    object value1 = originalValues.GetValue(i);
                    object value2 = CurrentValues.GetValue(i);

                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                    {
                        #region "Depart Date"
                        if (fieldname.ToLower() == "estdeparturedt")
                        {
                            HistoryDescription.AppendLine(
                                string.Format(
                                "{0}  Changed From {1} To {2}", "Estimated Departure Date",
                                (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value1)) : "Empty"),
                                (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value2)) : "Empty")
                                ));

                        }

                        #endregion

                        #region "Request Date"
                        if (fieldname.ToLower() == "requestdt")
                        {
                            HistoryDescription.AppendLine(
                                string.Format(
                                "{0}  Changed From {1} To {2}", "Request Date",
                                (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value1)) : "Empty"),
                                (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value2)) : "Empty")
                                ));

                        }

                        #endregion

                        #region "Fleet"

                        if (fieldname.ToLower() == "fleetid")
                        {
                            Int64 OldFleetID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldFleetID = (Int64)value1;

                            Int64 NewFleetID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewFleetID = (Int64)value2;
                            Fleet OldFleet = Container.Fleets.Where(x => x.FleetID == OldFleetID).SingleOrDefault();
                            Fleet NewFleet = Container.Fleets.Where(x => x.FleetID == NewFleetID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Tail No.",
                                (OldFleet != null ? OldFleet.TailNum : "Empty"),
                                (NewFleet != null ? NewFleet.TailNum : "Empty"))
                                );
                        }

                        #endregion

                        #region "Aircraft"

                        if (fieldname.ToLower() == "aircraftid")
                        {
                            Int64 OldAircraftID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAircraftID = (Int64)value1;

                            Int64 NewAircraftID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAircraftID = (Int64)value2;
                            Aircraft OldAircraft = Container.Aircraft.Where(x => x.AircraftID == OldAircraftID).SingleOrDefault();
                            Aircraft NewAircraft = Container.Aircraft.Where(x => x.AircraftID == NewAircraftID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Type",
                                (OldAircraft != null ? OldAircraft.AircraftCD : "Empty"),
                                (NewAircraft != null ? NewAircraft.AircraftCD : "Empty")));
                        }

                        #endregion

                        #region "Passenger"

                        if (fieldname.ToLower() == "passengerrequestorid")
                        {
                            Int64 OldPassengerID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldPassengerID = (Int64)value1;

                            Int64 NewPassengerID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewPassengerID = (Int64)value2;
                            Passenger Oldpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == OldPassengerID).SingleOrDefault();
                            Passenger Newpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == NewPassengerID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Requestor",
                                (Oldpassenger != null ? Oldpassenger.PassengerRequestorCD : "Empty"),
                                (Newpassenger != null ? Newpassenger.PassengerRequestorCD : "Empty")));
                        }


                        #endregion

                        #region "Department"

                        if (fieldname.ToLower() == "departmentid")
                        {
                            Int64 OldDepartmentID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldDepartmentID = (Int64)value1;

                            Int64 NewDepartmentID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewDepartmentID = (Int64)value2;
                            Department OldDepartment = Container.Departments.Where(x => x.DepartmentID == OldDepartmentID).SingleOrDefault();
                            Department NewDepartment = Container.Departments.Where(x => x.DepartmentID == NewDepartmentID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Department",
                                (OldDepartment != null ? OldDepartment.DepartmentCD : "Empty"),
                                (NewDepartment != null ? NewDepartment.DepartmentCD : "Empty")));
                        }


                        #endregion

                        #region "Authorization"

                        if (fieldname.ToLower() == "authorizationid")
                        {
                            Int64 OldAuthorizationID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAuthorizationID = (Int64)value1;

                            Int64 NewAuthorizationID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAuthorizationID = (Int64)value2;
                            DepartmentAuthorization OldAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == OldAuthorizationID).SingleOrDefault();
                            DepartmentAuthorization NewAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == NewAuthorizationID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Authorization",
                                (OldAuthorization != null ? OldAuthorization.AuthorizationCD : "Empty"),
                                (NewAuthorization != null ? NewAuthorization.AuthorizationCD : "Empty")));
                        }


                        #endregion

                        #region "Client"

                        if (fieldname.ToLower() == "clientid")
                        {
                            Int64 OldClientID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldClientID = (Int64)value1;

                            Int64 NewClientID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewClientID = (Int64)value2;
                            Client OldClient = Container.Clients.Where(x => x.ClientID == OldClientID).SingleOrDefault();
                            Client NewClient = Container.Clients.Where(x => x.ClientID == NewClientID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Client",
                                (OldClient != null ? OldClient.ClientCD : "Empty"),
                                (NewClient != null ? NewClient.ClientCD : "Empty")));
                        }


                        #endregion

                        #region "HomeBase"

                        if (fieldname.ToLower() == "homebaseid")
                        {
                            Int64 OldHomebaseID = 0;
                            if (value1 != null)
                                OldHomebaseID = (Int64)value1;

                            Int64 NewHomebaseID = 0;
                            if (value2 != null)
                                NewHomebaseID = (Int64)value2;


                            Company OldCompany = Container.Companies.Where(x => x.HomebaseID == OldHomebaseID).SingleOrDefault();
                            Company NewCompany = Container.Companies.Where(x => x.HomebaseID == NewHomebaseID).SingleOrDefault();

                            string OldAirportID = string.Empty;
                            string NewAirportID = string.Empty;
                            if (OldCompany != null)
                            {
                                Airport OldAirport = Container.Airports.Where(x => x.AirportID == OldCompany.HomebaseAirportID).SingleOrDefault();
                                if (OldAirport != null)
                                {
                                    OldAirportID = OldAirport.IcaoID;
                                }
                            }

                            if (NewCompany != null)
                            {
                                Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewCompany.HomebaseAirportID).SingleOrDefault();
                                if (NewAirportID != null)
                                {
                                    NewAirportID = NewAirport.IcaoID;
                                }
                            }

                            if (!string.IsNullOrEmpty(OldAirportID) && !string.IsNullOrEmpty(NewAirportID))
                            {
                                HistoryDescription.AppendLine(string.Format("Homebase Changed From {0} To {1}", OldAirportID, NewAirportID));
                            }
                        }


                        #endregion

                        #region "TravelCoordinator"

                        if (fieldname.ToLower() == "travelcoordinatorid")
                        {
                            Int64 OldTravelID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldTravelID = (Int64)value1;

                            Int64 NewTravelID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewTravelID = (Int64)value2;
                            TravelCoordinator OldTravel = Container.TravelCoordinators.Where(x => x.TravelCoordinatorID == OldTravelID).SingleOrDefault();
                            TravelCoordinator NewTravel = Container.TravelCoordinators.Where(x => x.TravelCoordinatorID == NewTravelID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "TravelCoordinator",
                                (OldTravel != null ? OldTravel.TravelCoordCD : "Empty"),
                                (NewTravel != null ? NewTravel.TravelCoordCD : "Empty")));
                        }


                        #endregion


                        #region "CRMain Description"

                        if (fieldname.ToLower() == "crmaindescription")
                        {
                            string OldDescription = null;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldDescription = (string)value1;

                            string NewDescription = null;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewDescription = (string)value2;

                            HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Description",
                                (OldDescription != null ? OldDescription.ToString() : "Empty"),
                                (NewDescription != null ? NewDescription.ToString() : "Empty")));
                        }


                        #endregion

                    }
                }

                #endregion

            }
            else if (CorpRequest.State == CorporateRequestTripEntityState.Added)
            {
                CorpRequest.IsDeleted = false;
                Container.CRMains.AddObject(CorpRequest);

                #region "History"
                if (CorpRequest.RecordType == "W")
                    HistoryDescription.AppendLine("New Trip Request Added");
                #endregion

            }
            else
            {
                Container.CRMains.Attach(crMainentity);
            }
            #endregion

            #region Dispatcher
            foreach (var dispatcherNote in CorpRequest.CRDispatchNotes.ToList())
            {
                dispatcherNote.LastUpdTS = DateTime.UtcNow;
                dispatcherNote.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CRDispatchNote originalDispatchNote = new CRDispatchNote();

                if (dispatcherNote.State != CorporateRequestTripEntityState.Added)
                {
                    originalDispatchNote = Container.CRDispatchNotes
                    .Where(c => c.CRDispatchNotesID == dispatcherNote.CRDispatchNotesID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (dispatcherNote.State == CorporateRequestTripEntityState.Modified)
                    {
                        dispatcherNote.IsDeleted = false;
                        dispatcherNote.CustomerID = CustomerID;
                        Container.CRDispatchNotes.Attach(originalDispatchNote);
                        Container.CRDispatchNotes.ApplyCurrentValues(dispatcherNote);
                    }
                    else if (dispatcherNote.State == CorporateRequestTripEntityState.Deleted)
                    {
                        originalDispatchNote.LastUpdTS = DateTime.UtcNow;
                        originalDispatchNote.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CRDispatchNotes.DeleteObject(originalDispatchNote);
                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalDispatchNote, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    dispatcherNote.IsDeleted = false;
                    dispatcherNote.CustomerID = CustomerID;
                    if (CorpRequest.State != CorporateRequestTripEntityState.Added)
                        crMainentity.CRDispatchNotes.Add(dispatcherNote);
                    Container.ObjectStateManager.ChangeObjectState(dispatcherNote, System.Data.EntityState.Added);

                    #region "History"
                    if (!string.IsNullOrEmpty(dispatcherNote.CRDispatchNotes))
                        HistoryDescription.AppendLine("Requestor Notes Added for Trip Request");
                    #endregion

                }
            }

            #endregion

            #region "Legs"
            foreach (var Leg in CorpRequest.CRLegs.ToList())
            {
                Leg.LastUpdTS = DateTime.UtcNow;
                Leg.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CRLeg originalLeg = new CRLeg();

                if (Leg.State != CorporateRequestTripEntityState.Added)
                {
                    originalLeg = Container.CRLegs
                    .Where(c => c.CRLegID == Leg.CRLegID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (Leg.State == CorporateRequestTripEntityState.Modified)
                    {
                        Leg.IsDeleted = false;
                        Leg.CustomerID = CustomerID;
                        Container.CRLegs.Attach(originalLeg);
                        Container.CRLegs.ApplyCurrentValues(Leg);

                        #region "History For Modified Leg"
                        var originalLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).OriginalValues;
                        var CurrentLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).CurrentValues;
                        PropertyInfo[] properties = typeof(CRLeg).GetProperties();
                        for (int i = 0; i < originalLegValues.FieldCount; i++)
                        {
                            var fieldname = originalLegValues.GetName(i);

                            object value1 = originalLegValues.GetValue(i);
                            object value2 = CurrentLegValues.GetValue(i);

                            if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                            {

                                #region "DepartIcaoID"

                                if (fieldname.ToLower() == "dairportid")
                                {
                                    Int64 OldDepartICAOID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldDepartICAOID = (Int64)value1;

                                    Int64 NewDepartICAOID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewDepartICAOID = (Int64)value2;
                                    Airport OldAiport = Container.Airports.Where(x => x.AirportID == OldDepartICAOID).SingleOrDefault();
                                    Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewDepartICAOID).SingleOrDefault();

                                    if (CorpRequest.RecordType == "W")
                                        HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Departs City/ICAO",
                                            (OldAiport != null ? OldAiport.CityName + "/" + OldAiport.IcaoID : string.Empty),
                                            (NewAirport != null ? NewAirport.CityName + "/" + NewAirport.IcaoID : string.Empty)));
                                    else //Fleet or Crew Calendar Entry
                                        HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "City/ICAO",
                                            (OldAiport != null ? OldAiport.CityName + "/" + OldAiport.IcaoID : string.Empty),
                                            (NewAirport != null ? NewAirport.CityName + "/" + NewAirport.IcaoID : string.Empty)));

                                }

                                #endregion

                                #region "ArriveIcaoID"

                                if (fieldname.ToLower() == "aairportid")
                                {
                                    Int64 OldArriveICAOID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldArriveICAOID = (Int64)value1;

                                    Int64 NewArriveICAOID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewArriveICAOID = (Int64)value2;
                                    Airport OldAiport = Container.Airports.Where(x => x.AirportID == OldArriveICAOID).SingleOrDefault();
                                    Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewArriveICAOID).SingleOrDefault();
                                    if (CorpRequest.RecordType == "W")
                                        HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Arrives City/ICAO",
                                            (OldAiport != null ? OldAiport.CityName + "/" + OldAiport.IcaoID : string.Empty),
                                            (NewAirport != null ? NewAirport.CityName + "/" + NewAirport.IcaoID : string.Empty)));
                                }

                                #endregion

                                #region "Departure Date Local"
                                if (fieldname.ToLower() == "departuredttmlocal")
                                {

                                    if (CorpRequest.RecordType == "W")
                                        HistoryDescription.AppendLine(
                                            string.Format(
                                            "Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Departure Local Date/Time",
                                            (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1)) : string.Empty),
                                            (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2)) : string.Empty)
                                            ));
                                    else
                                        HistoryDescription.AppendLine(
                                        string.Format(
                                        "{0} Changed From {1} To {2}", "Start Date/Time",
                                        (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1)) : string.Empty),
                                        (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2)) : string.Empty)
                                        ));
                                }


                                #endregion

                                #region "Arrival Date Arrival"
                                if (fieldname.ToLower() == "arrivaldttmlocal")
                                {
                                    if (CorpRequest.RecordType == "W")
                                        HistoryDescription.AppendLine(
                                            string.Format(
                                            "Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Arrival Local Date/Time",
                                            (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1)) : string.Empty),
                                            (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2)) : string.Empty)
                                            ));
                                    else
                                        HistoryDescription.AppendLine(
                                        string.Format(
                                        "{0} Changed From {1} To {2}", "End Date/Time",
                                        (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1)) : string.Empty),
                                        (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2)) : string.Empty)
                                        ));

                                }

                                #endregion

                                #region "Passenger"

                                if (fieldname.ToLower() == "passengerrequestorid")
                                {
                                    Int64 OldPassengerID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldPassengerID = (Int64)value1;

                                    Int64 NewPassengerID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewPassengerID = (Int64)value2;
                                    Passenger Oldpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == OldPassengerID).SingleOrDefault();
                                    Passenger Newpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == NewPassengerID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Requestor",
                                        (Oldpassenger != null ? Oldpassenger.PassengerRequestorCD : string.Empty),
                                        (Newpassenger != null ? Newpassenger.PassengerRequestorCD : string.Empty)));
                                }


                                #endregion

                                #region "Department"

                                if (fieldname.ToLower() == "departmentid")
                                {
                                    Int64 OldDepartmentID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldDepartmentID = (Int64)value1;

                                    Int64 NewDepartmentID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewDepartmentID = (Int64)value2;
                                    Department OldDepartment = Container.Departments.Where(x => x.DepartmentID == OldDepartmentID).SingleOrDefault();
                                    Department NewDepartment = Container.Departments.Where(x => x.DepartmentID == NewDepartmentID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Department",
                                        (OldDepartment != null ? OldDepartment.DepartmentCD : string.Empty),
                                        (NewDepartment != null ? NewDepartment.DepartmentCD : string.Empty)));
                                }


                                #endregion

                                #region "Authorization"

                                if (fieldname.ToLower() == "authorizationid")
                                {
                                    Int64 OldAuthorizationID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldAuthorizationID = (Int64)value1;

                                    Int64 NewAuthorizationID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewAuthorizationID = (Int64)value2;
                                    DepartmentAuthorization OldAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == OldAuthorizationID).SingleOrDefault();
                                    DepartmentAuthorization NewAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == NewAuthorizationID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Authorization",
                                        (OldAuthorization != null ? OldAuthorization.AuthorizationCD : string.Empty),
                                        (NewAuthorization != null ? NewAuthorization.AuthorizationCD : string.Empty)));
                                }


                                #endregion

                                #region "Flight Category"

                                if (fieldname.ToLower() == "flightcategoryid")
                                {
                                    Int64 OldFlightCategoryID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldFlightCategoryID = (Int64)value1;

                                    Int64 NewFlightCategoryID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewFlightCategoryID = (Int64)value2;
                                    FlightCatagory OldFlightCategory = Container.FlightCatagories.Where(x => x.FlightCategoryID == OldFlightCategoryID).SingleOrDefault();
                                    FlightCatagory NewFlightCategory = Container.FlightCatagories.Where(x => x.FlightCategoryID == NewFlightCategoryID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Flight Category",
                                        (OldFlightCategory != null ? OldFlightCategory.FlightCatagoryCD : string.Empty),
                                        (NewFlightCategory != null ? NewFlightCategory.FlightCatagoryCD : string.Empty)));
                                }


                                #endregion

                                #region "private"

                                if (fieldname.ToLower() == "isprivate")
                                {
                                    HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Private",
                                        ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Private" : "Not Private"),
                                        ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Private" : "Not Private")));
                                }
                                #endregion
                            }
                        }
                        #endregion

                    }
                    else if (Leg.State == CorporateRequestTripEntityState.Deleted)
                    {
                        originalLeg.LastUpdTS = DateTime.UtcNow;
                        originalLeg.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CRLegs.DeleteObject(originalLeg);

                        #region "History For Deleted Leg"
                        HistoryDescription.AppendLine(string.Format("Leg {0} Deleted", originalLeg.LegNUM));
                        #endregion

                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalLeg, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    Leg.IsDeleted = false;
                    Leg.CustomerID = CustomerID;
                    if (CorpRequest.State != CorporateRequestTripEntityState.Added)
                        crMainentity.CRLegs.Add(Leg);
                    Container.ObjectStateManager.ChangeObjectState(Leg, System.Data.EntityState.Added);

                    #region "History"

                    if (CorpRequest.RecordType == "W")
                    {
                        HistoryDescription.AppendLine("New Leg Added To Trip With Details:");
                        Airport DepAiport = new Airport();
                        Airport ArrAirport = new Airport();
                        if (Leg.DAirportID != null)
                            DepAiport = Container.Airports.Where(x => x.AirportID == (Int64)Leg.DAirportID).SingleOrDefault();
                        if (Leg.AAirportID != null)
                            ArrAirport = Container.Airports.Where(x => x.AirportID == (Int64)Leg.AAirportID).SingleOrDefault();

                        if (DepAiport != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} Departs ICAO: {1}", Leg.LegNUM, DepAiport.IcaoID));
                        if (ArrAirport != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} Arrives ICAO: {1}", Leg.LegNUM, ArrAirport.IcaoID));



                        if (Leg.DepartureDTTMLocal != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} {1}: {2}", Leg.LegNUM, "Departure Local Date/Time", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", (DateTime)Leg.DepartureDTTMLocal)));

                        if (Leg.ArrivalDTTMLocal != null)
                            HistoryDescription.AppendLine(string.Format("Leg {0} {1}: {2}", Leg.LegNUM, "Arrival Local Date/Time", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", (DateTime)Leg.ArrivalDTTMLocal)));
                    }
                    #endregion

                }

                #region "PAX"

                #region "All PAX deleted"
                bool blPaxdelete = false;
                if (CorpRequest.RecordType == "W" && CorpRequest.CRLegs != null && CorpRequest.CRLegs.Count > 0)
                {
                    foreach (CRLeg Leglist in CorpRequest.CRLegs)
                    {
                        if (Leglist.CRPassengers != null && Leglist.CRPassengers.Count > 0)
                        {
                            List<CRPassenger> availpax = new List<CRPassenger>();
                            availpax = Leglist.CRPassengers.Where(x => x.IsDeleted == false).ToList();

                            if (availpax == null || availpax.Count == 0)
                            {
                                blPaxdelete = true;
                                HistoryDescription.AppendLine(string.Format("All Passenger Deleted for Leg: {0}", Leg.LegNUM));
                            }
                        }

                    }
                }
                #endregion

                int adddisplay = 0;
                foreach (CRPassenger paxList in Leg.CRPassengers.ToList())
                {
                    CRPassenger OriginalPaxlist = new CRPassenger();
                    paxList.LastUpdTS = DateTime.UtcNow;
                    paxList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (paxList.State != CorporateRequestTripEntityState.Added)
                    {
                        OriginalPaxlist = Container.CRPassengers
                       .Where(c => c.CRPassengerID == paxList.CRPassengerID)
                       .SingleOrDefault();


                        // Is original child item with same ID in DB?   
                        if (paxList.State == CorporateRequestTripEntityState.Modified)
                        {
                            paxList.IsDeleted = false;
                            paxList.CustomerID = CustomerID;
                            Container.CRPassengers.Attach(OriginalPaxlist);
                            Container.CRPassengers.ApplyCurrentValues(paxList);

                            #region "History For Modified Pax"
                            var originalPaxValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalPaxlist).OriginalValues;
                            var CurrentPaxValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalPaxlist).CurrentValues;
                            PropertyInfo[] properties = typeof(CRPassenger).GetProperties();
                            for (int i = 0; i < originalPaxValues.FieldCount; i++)
                            {
                                var fieldname = originalPaxValues.GetName(i);

                                object value1 = originalPaxValues.GetValue(i);
                                object value2 = CurrentPaxValues.GetValue(i);

                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                {
                                    #region "Purpose"
                                    if (fieldname.ToLower() == "flightpurposeid")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            FlightPurpose OldPurpose = new FlightPurpose();
                                            OldPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)value1).SingleOrDefault();
                                            FlightPurpose NewPurpose = new FlightPurpose();
                                            NewPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)value2).SingleOrDefault();

                                            if (!string.IsNullOrEmpty(OriginalPaxlist.FlightPurposeID.ToString()))
                                                HistoryDescription.AppendLine(string.Format("Passenger Changes: For Leg  #: {0}  Pax Code: {1} Pax Name: {2} Purpose Changed From {3} To {4}", Leg.LegNUM, OriginalPaxlist.PassengerRequestorID, OriginalPaxlist.PassengerName, OldPurpose.FlightPurposeCD, NewPurpose.FlightPurposeCD + "."));
                                        }

                                    }
                                    #endregion

                                    #region "Billing"
                                    if (fieldname.ToLower() == "billing")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (!string.IsNullOrEmpty(OriginalPaxlist.Billing))
                                                HistoryDescription.AppendLine(string.Format("Passenger Changes: For Leg  #: {0}  Pax Code: {1} Pax Name: {2} Billing Code Changed From {3} To {4}", Leg.LegNUM, OriginalPaxlist.PassengerRequestorID, OriginalPaxlist.PassengerName, value1, value2 + "."));
                                        }

                                    }
                                    #endregion

                                    #region "PassportID"
                                    if (fieldname.ToLower() == "passportid")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (!string.IsNullOrEmpty(OriginalPaxlist.PassportID.ToString()))
                                                HistoryDescription.AppendLine(string.Format("Passenger Changes: For Leg  #: {0}  Pax Code: {1} Pax Name: {2} Passport Number Changed From {3} To {4}", Leg.LegNUM, OriginalPaxlist.PassengerRequestorID, OriginalPaxlist.PassengerName, value1, value2 + "."));
                                        }

                                    }
                                    #endregion
                                }
                            }

                            #endregion

                        }
                        else if (paxList.State == CorporateRequestTripEntityState.Deleted)
                        {
                            OriginalPaxlist.LastUpdTS = DateTime.UtcNow;
                            OriginalPaxlist.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.CRPassengers.DeleteObject(OriginalPaxlist);

                            #region "History For Deleted Pax"
                            if (!blPaxdelete)
                            {
                                display = display + 1;
                                FlightPurpose fpPurpose = new FlightPurpose();
                                fpPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)paxList.FlightPurposeID).SingleOrDefault();
                                if (display == 1)
                                    HistoryDescription.AppendLine(string.Format("Passenger Deletions:"));
                                if (display >= 1)
                                    HistoryDescription.AppendLine(string.Format("For Leg: {0} Pax Code: {1} Name: {2}", Leg.LegNUM, paxList.PassengerRequestorID, paxList.PassengerName));
                            }
                            #endregion

                        }

                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        paxList.IsDeleted = false;
                        paxList.CustomerID = CustomerID;
                        if (Leg.State != CorporateRequestTripEntityState.Added)
                            originalLeg.CRPassengers.Add(paxList);
                        Container.ObjectStateManager.ChangeObjectState(paxList, System.Data.EntityState.Added);

                        #region "History For Added Pax"
                        adddisplay = adddisplay + 1;
                        FlightPurpose fpPurpose = new FlightPurpose();
                        fpPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)paxList.FlightPurposeID).SingleOrDefault();
                        if (adddisplay == 1)
                            HistoryDescription.AppendLine(string.Format("Passenger Additions:"));
                        if (adddisplay >= 1)
                            HistoryDescription.AppendLine(string.Format("For Leg: {0} Pax Code: {1} Name: {2} Purpose: {3}", Leg.LegNUM, paxList.PassengerRequestorID, paxList.PassengerName, fpPurpose.FlightPurposeCD));
                        #endregion

                    }
                }
                #endregion

                #region "Transport"
                foreach (CRTransportList transportList in Leg.CRTransportLists.ToList())
                {
                    transportList.CustomerID = Leg.CustomerID.Value;
                    transportList.LastUpdTS = DateTime.UtcNow;
                    transportList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (transportList.State != CorporateRequestTripEntityState.Added)
                    {
                        var OriginalTransportlist = Container.CRTransportLists
                        .Where(c => c.CRTransportListID == transportList.CRTransportListID)
                        .SingleOrDefault();

                        if (transportList.State == CorporateRequestTripEntityState.Modified)
                        {
                            transportList.IsDeleted = false;
                            transportList.LegID = Leg.LegID;
                            Container.CRTransportLists.Attach(OriginalTransportlist);
                            Container.CRTransportLists.ApplyCurrentValues(transportList);

                            #region "History For Modified Transport"
                            var originalTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).OriginalValues;
                            var CurrentTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).CurrentValues;
                            PropertyInfo[] properties = typeof(CRTransportList).GetProperties();
                            for (int i = 0; i < originalTransportValues.FieldCount; i++)
                            {
                                var fieldname = originalTransportValues.GetName(i);

                                object value1 = originalTransportValues.GetValue(i);
                                object value2 = CurrentTransportValues.GetValue(i);

                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                {
                                    if (fieldname.ToLower() == "transportid")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            Transport OldTransport = new Transport();
                                            OldTransport = Container.Transports.Where(x => x.TransportID == (Int64)value1).SingleOrDefault();
                                            Transport NewTransport = new Transport();
                                            NewTransport = Container.Transports.Where(x => x.TransportID == (Int64)value2).SingleOrDefault();

                                            if (!string.IsNullOrEmpty(OriginalTransportlist.TransportID.ToString()) && OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Changed From {0} To {1}", OldTransport.TransportCD, NewTransport.TransportCD + "."));
                                            else if (!string.IsNullOrEmpty(OriginalTransportlist.TransportID.ToString()) && OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Changed From {0} To {1}", OldTransport.TransportCD, NewTransport.TransportCD + "."));
                                        }

                                    }


                                    if (fieldname.ToLower() == "crtransportlistdescription")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Name Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Name Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                    }

                                    if (fieldname.ToLower() == "phonenum")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Phone Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Phone Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Phone Changed From Empty" + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Phone Changed From Empty" + " " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Phone Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Phone Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                    if (fieldname.ToLower() == "faxnum")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Fax Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Fax Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Fax Changed From Empty" + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Fax Changed From Empty" + " " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                    if (fieldname.ToLower() == "rate")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Rate Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Rate Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Rate Changed From Empty" + " " + "to" + " " + value2));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Rate Changed From Empty" + " " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalTransportlist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Transportation Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalTransportlist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Transportation Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                }
                            }

                            #endregion

                        }
                        else if (transportList.State == CorporateRequestTripEntityState.Deleted)
                        {
                            OriginalTransportlist.LastUpdUID = UserPrincipal.Identity.Name;
                            OriginalTransportlist.LastUpdTS = DateTime.UtcNow;
                            Container.CRTransportLists.DeleteObject(OriginalTransportlist);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        transportList.IsDeleted = false;
                        transportList.LegID = Leg.LegID;
                        if (Leg.State != CorporateRequestTripEntityState.Added)
                            originalLeg.CRTransportLists.Add(transportList);
                        Container.ObjectStateManager.ChangeObjectState(transportList, System.Data.EntityState.Added);

                        #region "History For Added Transport"
                        Transport transport = new Transport();
                        if (transportList.TransportID != null)
                        {
                            transport = Container.Transports.Where(x => x.TransportID == (Int64)transportList.TransportID).SingleOrDefault();
                            if (transportList.RecordType == "C")
                            {
                                HistoryDescription.AppendLine(string.Format("Crew Transport Additions: For Leg: {0} Transport Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, transport.TransportCD, (transportList.CRTransportListDescription != null && transportList.CRTransportListDescription.Trim() != string.Empty) ? " Name: " + transportList.CRTransportListDescription : "", (transportList.PhoneNUM != null && transportList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + transportList.PhoneNUM : "", (transportList.FaxNum != null && transportList.FaxNum.Trim() != string.Empty) ? " Fax: " + transportList.FaxNum : "", (transportList.Email != null && transportList.Email.Trim() != string.Empty) ? " Email: " + transportList.Email : "", (transportList.Rate != null && transportList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + transportList.Rate : ""));
                            }
                            if (transportList.RecordType == "P")
                            {
                                HistoryDescription.AppendLine(string.Format("Pax Transport Additions: For Leg: {0} Transport Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, transport.TransportCD, (transportList.CRTransportListDescription != null && transportList.CRTransportListDescription.Trim() != string.Empty) ? " Name: " + transportList.CRTransportListDescription : "", (transportList.PhoneNUM != null && transportList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + transportList.PhoneNUM : "", (transportList.FaxNum != null && transportList.FaxNum.Trim() != string.Empty) ? " Fax: " + transportList.FaxNum : "", (transportList.Email != null && transportList.Email.Trim() != string.Empty) ? " Email: " + transportList.Email : "", (transportList.Rate != null && transportList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + transportList.Rate : ""));
                            }
                        }
                        else
                        {
                            if (transportList.RecordType == "C")
                            {
                                HistoryDescription.AppendLine(string.Format("Crew Transport Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (transportList.CRTransportListDescription != null && transportList.CRTransportListDescription.Trim() != string.Empty) ? " Name: " + transportList.CRTransportListDescription : "", (transportList.PhoneNUM != null && transportList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + transportList.PhoneNUM : "", (transportList.FaxNum != null && transportList.FaxNum.Trim() != string.Empty) ? " Fax: " + transportList.FaxNum : "", (transportList.Email != null && transportList.Email.Trim() != string.Empty) ? " Email: " + transportList.Email : "", (transportList.Rate != null && transportList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + transportList.Rate : ""));
                            }
                            if (transportList.RecordType == "P")
                            {
                                HistoryDescription.AppendLine(string.Format("Pax Transport Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (transportList.CRTransportListDescription != null && transportList.CRTransportListDescription.Trim() != string.Empty) ? " Name: " + transportList.CRTransportListDescription : "", (transportList.PhoneNUM != null && transportList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + transportList.PhoneNUM : "", (transportList.FaxNum != null && transportList.FaxNum.Trim() != string.Empty) ? " Fax: " + transportList.FaxNum : "", (transportList.Email != null && transportList.Email.Trim() != string.Empty) ? " Email: " + transportList.Email : "", (transportList.Rate != null && transportList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + transportList.Rate : ""));
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                #region "FBO"
                foreach (CRFBOList fboList in Leg.CRFBOLists.ToList())
                {
                    fboList.LastUpdTS = DateTime.UtcNow;
                    fboList.LastUpdUID = UserPrincipal.Identity.Name;
                    fboList.CustomerID = Leg.CustomerID;
                    if (fboList.State != CorporateRequestTripEntityState.Added)
                    {
                        var OriginalFBOlist = Container.CRFBOLists
                        .Where(c => c.CRFBOListID == fboList.CRFBOListID)
                        .SingleOrDefault();

                        if (fboList.State == CorporateRequestTripEntityState.Modified)
                        {
                            fboList.IsDeleted = false;
                            fboList.LegID = Leg.LegID;
                            Container.CRFBOLists.Attach(OriginalFBOlist);
                            Container.CRFBOLists.ApplyCurrentValues(fboList);
                            Container.ObjectStateManager.ChangeObjectState(OriginalFBOlist, System.Data.EntityState.Modified);

                            #region "History For Modified FBO"
                            var originalFBOValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalFBOlist).OriginalValues;
                            var CurrentFboValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalFBOlist).CurrentValues;
                            PropertyInfo[] properties = typeof(CRFBOList).GetProperties();
                            for (int i = 0; i < originalFBOValues.FieldCount; i++)
                            {
                                var fieldname = originalFBOValues.GetName(i);

                                object value1 = originalFBOValues.GetValue(i);
                                object value2 = CurrentFboValues.GetValue(i);

                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                {
                                    if (fieldname.ToLower() == "fboid")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            FBO OldFbo = new FBO();
                                            OldFbo = Container.FBOes.Where(x => x.FBOID == (Int64)value1).SingleOrDefault();
                                            FBO NewFbo = new FBO();
                                            NewFbo = Container.FBOes.Where(x => x.FBOID == (Int64)value2).SingleOrDefault();

                                            if (!string.IsNullOrEmpty(OriginalFBOlist.FBOID.ToString()) && OriginalFBOlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart FBO Changed From {0} To {1}", OldFbo.FBOVendor, NewFbo.FBOVendor + "."));
                                            else if (!string.IsNullOrEmpty(OriginalFBOlist.FBOID.ToString()) && OriginalFBOlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive FBO Changed From {0} To {1}", OldFbo.FBOVendor, NewFbo.FBOVendor + "."));
                                        }
                                    }

                                    if (fboList.FBOID == null)
                                    {
                                        if (fieldname.ToLower() == "crfbolistdescription")
                                        {
                                            if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                            {
                                                if (OriginalFBOlist.RecordType == "D")
                                                    HistoryDescription.AppendLine(string.Format("Depart FBO Changed From " + value1 + " " + "to" + " " + value2));
                                                else if (OriginalFBOlist.RecordType == "A")
                                                    HistoryDescription.AppendLine(string.Format("Arrive FBO Changed From " + value1 + " " + "to" + " " + value2));
                                            }
                                        }
                                    }
                                }
                            }

                            #endregion

                        }
                        else if (fboList.State == CorporateRequestTripEntityState.Deleted)
                        {
                            OriginalFBOlist.LastUpdTS = DateTime.UtcNow;
                            OriginalFBOlist.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.CRFBOLists.DeleteObject(OriginalFBOlist);

                        }
                        else
                        {
                            Container.ObjectStateManager.ChangeObjectState(OriginalFBOlist, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        fboList.IsDeleted = false;
                        fboList.LegID = Leg.LegID;
                        if (Leg.State != CorporateRequestTripEntityState.Added)
                            originalLeg.CRFBOLists.Add(fboList);
                        Container.ObjectStateManager.ChangeObjectState(fboList, System.Data.EntityState.Added);

                        #region "History For Added Fbo"
                        FBO fbo = new FBO();
                        fbo = Container.FBOes.Where(x => x.FBOID == (Int64)fboList.FBOID).SingleOrDefault();
                        if (fboList.FBOID != null)
                        {
                            if (fboList.RecordType == "D")
                            {
                                HistoryDescription.AppendLine(string.Format("Depart FBO Additions: For Leg: {0} Fbo Code: {1} Name: {2}", Leg.LegNUM, fbo.FBOCD, fbo.FBOVendor));
                            }
                            if (fboList.RecordType == "A")
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO Additions: For Leg: {0} Fbo Code: {1} Name: {2}", Leg.LegNUM, fbo.FBOCD, fbo.FBOVendor));
                            }
                        }
                        else
                        {
                            if (fboList.RecordType == "D")
                            {
                                HistoryDescription.AppendLine(string.Format("Depart FBO Additions: For Leg: {0}  Name: {1}", Leg.LegNUM, fboList.CRFBOListDescription));
                            }
                            if (fboList.RecordType == "A")
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival FBO Additions: For Leg: {0} Name: {1}", Leg.LegNUM, fboList.CRFBOListDescription));
                            }
                        }
                        #endregion

                    }
                }

                #endregion

                #region "Catering"
                foreach (CRCateringList caterList in Leg.CRCateringLists.ToList())
                {
                    caterList.CustomerID = Leg.CustomerID;
                    caterList.LastUpdTS = DateTime.UtcNow;
                    caterList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (caterList.State != CorporateRequestTripEntityState.Added)
                    {
                        var OriginalCateringlist = Container.CRCateringLists
                        .Where(c => c.CRCateringListID == caterList.CRCateringListID)
                        .SingleOrDefault();

                        if (caterList.State == CorporateRequestTripEntityState.Modified)
                        {
                            caterList.IsDeleted = false;
                            caterList.LegID = Leg.LegID;
                            Container.CRCateringLists.Attach(OriginalCateringlist);
                            Container.CRCateringLists.ApplyCurrentValues(caterList);

                            #region "History For Modified Catering"
                            var originalCateringValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCateringlist).OriginalValues;
                            var CurrentCateringValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCateringlist).CurrentValues;
                            PropertyInfo[] properties = typeof(CRCateringList).GetProperties();
                            for (int i = 0; i < originalCateringValues.FieldCount; i++)
                            {
                                var fieldname = originalCateringValues.GetName(i);

                                object value1 = originalCateringValues.GetValue(i);
                                object value2 = CurrentCateringValues.GetValue(i);

                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                {
                                    if (fieldname.ToLower() == "cateringid")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            Catering OldCatering = new Catering();
                                            OldCatering = Container.Caterings.Where(x => x.CateringID == (Int64)value1).SingleOrDefault();
                                            Catering NewCatering = new Catering();
                                            NewCatering = Container.Caterings.Where(x => x.CateringID == (Int64)value2).SingleOrDefault();

                                            if (!string.IsNullOrEmpty(OriginalCateringlist.CateringID.ToString()) && OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Changed From {0} To {1}", OldCatering.CateringCD, NewCatering.CateringCD + "."));
                                            else if (!string.IsNullOrEmpty(OriginalCateringlist.CateringID.ToString()) && OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Changed From {0} To {1}", OldCatering.CateringCD, NewCatering.CateringCD + "."));
                                        }
                                    }

                                    if (fieldname.ToLower() == "crcateringlistdescription")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Name Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Name Changed From " + value1 + " " + "to" + " " + value2));
                                        }

                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Name Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Name Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Name Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Name Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                    if (fieldname.ToLower() == "phonenum")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Phone Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Phone Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Phone Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Phone Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Phone Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Phone Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                    if (fieldname.ToLower() == "faxnum")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Fax Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Fax Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Fax Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Fax Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                    if (fieldname.ToLower() == "rate")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Rate Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Rate Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Rate Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Rate Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalCateringlist.RecordType == "D")
                                                HistoryDescription.AppendLine(string.Format("Depart Catering Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalCateringlist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Arrive Catering Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                }
                            }

                            #endregion

                        }
                        else if (caterList.State == CorporateRequestTripEntityState.Deleted)
                        {
                            OriginalCateringlist.LastUpdTS = DateTime.UtcNow;
                            OriginalCateringlist.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.CRCateringLists.DeleteObject(OriginalCateringlist);
                        }
                        else
                        {
                            Container.ObjectStateManager.ChangeObjectState(OriginalCateringlist, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        caterList.IsDeleted = false;
                        caterList.LegID = Leg.LegID;
                        if (Leg.State != CorporateRequestTripEntityState.Added)
                            originalLeg.CRCateringLists.Add(caterList);
                        Container.ObjectStateManager.ChangeObjectState(caterList, System.Data.EntityState.Added);

                        #region "History For Added Catering"
                        Catering catering = new Catering();
                        if (caterList.CateringID != null)
                        {
                            catering = Container.Caterings.Where(x => x.CateringID == (Int64)caterList.CateringID).SingleOrDefault();
                            if (caterList.RecordType == "D")
                            {
                                HistoryDescription.AppendLine(string.Format("Depart Catering Additions: For Leg: {0} Catering Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, catering.CateringCD, (caterList.CRCateringListDescription != null && caterList.CRCateringListDescription.Trim() != string.Empty) ? " Name: " + caterList.CRCateringListDescription : "", (caterList.PhoneNUM != null && caterList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + caterList.PhoneNUM : "", (caterList.FaxNum != null && caterList.FaxNum.Trim() != string.Empty) ? " Fax: " + caterList.FaxNum : "", (caterList.Email != null && caterList.Email.Trim() != string.Empty) ? " Email: " + caterList.Email : "", (caterList.Rate != null && caterList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + caterList.Rate : ""));
                            }
                            if (caterList.RecordType == "A")
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival Catering Additions: For Leg: {0} Catering Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, catering.CateringCD, (caterList.CRCateringListDescription != null && caterList.CRCateringListDescription.Trim() != string.Empty) ? " Name: " + caterList.CRCateringListDescription : "", (caterList.PhoneNUM != null && caterList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + caterList.PhoneNUM : "", (caterList.FaxNum != null && caterList.FaxNum.Trim() != string.Empty) ? " Fax: " + caterList.FaxNum : "", (caterList.Email != null && caterList.Email.Trim() != string.Empty) ? " Email: " + caterList.Email : "", (caterList.Rate != null && caterList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + caterList.Rate : ""));
                            }
                        }
                        else
                        {
                            if (caterList.RecordType == "D")
                            {
                                HistoryDescription.AppendLine(string.Format("Depart Catering Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (caterList.CRCateringListDescription != null && caterList.CRCateringListDescription.Trim() != string.Empty) ? " Name: " + caterList.CRCateringListDescription : "", (caterList.PhoneNUM != null && caterList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + caterList.PhoneNUM : "", (caterList.FaxNum != null && caterList.FaxNum.Trim() != string.Empty) ? " Fax: " + caterList.FaxNum : "", (caterList.Email != null && caterList.Email.Trim() != string.Empty) ? " Email: " + caterList.Email : "", (caterList.Rate != null && caterList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + caterList.Rate : ""));
                            }
                            if (caterList.RecordType == "A")
                            {
                                HistoryDescription.AppendLine(string.Format("Arrival Catering Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (caterList.CRCateringListDescription != null && caterList.CRCateringListDescription.Trim() != string.Empty) ? " Name: " + caterList.CRCateringListDescription : "", (caterList.PhoneNUM != null && caterList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + caterList.PhoneNUM : "", (caterList.FaxNum != null && caterList.FaxNum.Trim() != string.Empty) ? " Fax: " + caterList.FaxNum : "", (caterList.Email != null && caterList.Email.Trim() != string.Empty) ? " Email: " + caterList.Email : "", (caterList.Rate != null && caterList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + caterList.Rate : ""));
                            }
                        }
                        #endregion
                    }
                }

                #endregion

                #region "Crew Hotel"
                foreach (CRHotelList hotelList in Leg.CRHotelLists.ToList())
                {
                    hotelList.CustomerID = Leg.CustomerID;
                    hotelList.LastUpdTS = DateTime.UtcNow;
                    hotelList.LastUpdUID = UserPrincipal.Identity.Name;
                    if (hotelList.State != CorporateRequestTripEntityState.Added)
                    {
                        var OriginalHotellist = Container.CRHotelLists
                        .Where(c => c.CRHotelListID == hotelList.CRHotelListID)
                        .SingleOrDefault();

                        if (hotelList.State == CorporateRequestTripEntityState.Modified)
                        {
                            hotelList.IsDeleted = false;
                            hotelList.LegID = Leg.LegID;
                            Container.CRHotelLists.Attach(OriginalHotellist);
                            Container.CRHotelLists.ApplyCurrentValues(hotelList);

                            #region "History For Modified Hotel"
                            var originalHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).OriginalValues;
                            var CurrentHotelValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotellist).CurrentValues;
                            PropertyInfo[] properties = typeof(CRHotelList).GetProperties();
                            for (int i = 0; i < originalHotelValues.FieldCount; i++)
                            {
                                var fieldname = originalHotelValues.GetName(i);

                                object value1 = originalHotelValues.GetValue(i);
                                object value2 = CurrentHotelValues.GetValue(i);

                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                {
                                    if (fieldname.ToLower() == "hotelid")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            Hotel OldHotel = new Hotel();
                                            OldHotel = Container.Hotels.Where(x => x.HotelID == (Int64)value1).SingleOrDefault();
                                            Hotel NewHotel = new Hotel();
                                            NewHotel = Container.Hotels.Where(x => x.HotelID == (Int64)value2).SingleOrDefault();

                                            if (!string.IsNullOrEmpty(OriginalHotellist.HotelID.ToString()) && OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Changed From {0} To {1}", OldHotel.HotelCD, NewHotel.HotelCD + "."));
                                            else if (!string.IsNullOrEmpty(OriginalHotellist.HotelID.ToString()) && OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Changed From {0} To {1}", OldHotel.HotelCD, NewHotel.HotelCD + "."));
                                            else if (!string.IsNullOrEmpty(OriginalHotellist.HotelID.ToString()) && OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Crew Hotel Changed From {0} To {1}", OldHotel.HotelCD, NewHotel.HotelCD + "."));
                                        }
                                    }

                                    if (fieldname.ToLower() == "crhotellistdescription")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Name Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Name Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Name Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Name Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Name Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Name Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Name Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Name Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Name Changed From " + value1 + " " + "to" + " " + "Empty"));

                                        }

                                    }

                                    if (fieldname.ToLower() == "phonenum")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Phone Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Phone Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Phone Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Phone Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Phone Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Phone Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Phone Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Phone Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Phone Changed From " + value1 + " " + "to" + " " + "Empty"));

                                        }
                                    }

                                    if (fieldname.ToLower() == "faxnum")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Fax Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Fax Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Fax Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Fax Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Fax Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Fax Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Fax Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }

                                    if (fieldname.ToLower() == "rate")
                                    {
                                        if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Rate Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Rate Changed From " + value1 + " " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Rate Changed From " + value1 + " " + "to" + " " + value2));
                                        }
                                        if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Rate Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Rate Changed From Empty " + "to" + " " + value2));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Rate Changed From Empty " + "to" + " " + value2));
                                        }
                                        if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                        {
                                            if (OriginalHotellist.RecordType == "C")
                                                HistoryDescription.AppendLine(string.Format("Crew Hotel Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "P")
                                                HistoryDescription.AppendLine(string.Format("Pax Hotel Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                            else if (OriginalHotellist.RecordType == "A")
                                                HistoryDescription.AppendLine(string.Format("Maint Hotel Rate Changed From " + value1 + " " + "to" + " " + "Empty"));
                                        }
                                    }
                                }
                            }

                            #endregion

                        }
                        else if (hotelList.State == CorporateRequestTripEntityState.Deleted)
                        {
                            OriginalHotellist.LastUpdTS = DateTime.UtcNow;
                            OriginalHotellist.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.CRHotelLists.DeleteObject(OriginalHotellist);
                        }
                        else
                        {
                            Container.ObjectStateManager.ChangeObjectState(OriginalHotellist, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        hotelList.IsDeleted = false;
                        hotelList.LegID = Leg.LegID;
                        if (Leg.State != CorporateRequestTripEntityState.Added)
                            originalLeg.CRHotelLists.Add(hotelList);
                        Container.ObjectStateManager.ChangeObjectState(hotelList, System.Data.EntityState.Added);

                        #region "History For Added Hotel"
                        Hotel hotel = new Hotel();
                        if (hotelList.HotelID != null)
                        {
                            hotel = Container.Hotels.Where(x => x.HotelID == (Int64)hotelList.HotelID).SingleOrDefault();
                            if (hotelList.RecordType == "C")
                            {
                                HistoryDescription.AppendLine(string.Format("Crew Hotel Additions: For Leg: {0} Hotel Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, hotel.HotelCD, (hotelList.CRHotelListDescription != null && hotelList.CRHotelListDescription.Trim() != string.Empty) ? " Name: " + hotelList.CRHotelListDescription : "", (hotelList.PhoneNUM != null && hotelList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + hotelList.PhoneNUM : "", (hotelList.FaxNum != null && hotelList.FaxNum.Trim() != string.Empty) ? " Fax: " + hotelList.FaxNum : "", (hotelList.Email != null && hotelList.Email.Trim() != string.Empty) ? " Email: " + hotelList.Email : "", (hotelList.Rate != null && hotelList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + hotelList.Rate : ""));
                            }
                            if (hotelList.RecordType == "P")
                            {
                                HistoryDescription.AppendLine(string.Format("Pax Hotel Additions: For Leg: {0} Hotel Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, hotel.HotelCD, (hotelList.CRHotelListDescription != null && hotelList.CRHotelListDescription.Trim() != string.Empty) ? " Name: " + hotelList.CRHotelListDescription : "", (hotelList.PhoneNUM != null && hotelList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + hotelList.PhoneNUM : "", (hotelList.FaxNum != null && hotelList.FaxNum.Trim() != string.Empty) ? " Fax: " + hotelList.FaxNum : "", (hotelList.Email != null && hotelList.Email.Trim() != string.Empty) ? " Email: " + hotelList.Email : "", (hotelList.Rate != null && hotelList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + hotelList.Rate : ""));
                            }
                            if (hotelList.RecordType == "A")
                            {
                                HistoryDescription.AppendLine(string.Format("Maint Hotel Additions: For Leg: {0} Hotel Code: {1}{2}{3}{4}{5}{6}", Leg.LegNUM, hotel.HotelCD, (hotelList.CRHotelListDescription != null && hotelList.CRHotelListDescription.Trim() != string.Empty) ? " Name: " + hotelList.CRHotelListDescription : "", (hotelList.PhoneNUM != null && hotelList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + hotelList.PhoneNUM : "", (hotelList.FaxNum != null && hotelList.FaxNum.Trim() != string.Empty) ? " Fax: " + hotelList.FaxNum : "", (hotelList.Email != null && hotelList.Email.Trim() != string.Empty) ? " Email: " + hotelList.Email : "", (hotelList.Rate != null && hotelList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + hotelList.Rate : ""));
                            }
                        }
                        else
                        {
                            if (hotelList.RecordType == "C")
                            {
                                HistoryDescription.AppendLine(string.Format("Crew Hotel Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (hotelList.CRHotelListDescription != null && hotelList.CRHotelListDescription.Trim() != string.Empty) ? " Name: " + hotelList.CRHotelListDescription : "", (hotelList.PhoneNUM != null && hotelList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + hotelList.PhoneNUM : "", (hotelList.FaxNum != null && hotelList.FaxNum.Trim() != string.Empty) ? " Fax: " + hotelList.FaxNum : "", (hotelList.Email != null && hotelList.Email.Trim() != string.Empty) ? " Email: " + hotelList.Email : "", (hotelList.Rate != null && hotelList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + hotelList.Rate : ""));
                            }
                            if (hotelList.RecordType == "P")
                            {
                                HistoryDescription.AppendLine(string.Format("Pax Hotel Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (hotelList.CRHotelListDescription != null && hotelList.CRHotelListDescription.Trim() != string.Empty) ? " Name: " + hotelList.CRHotelListDescription : "", (hotelList.PhoneNUM != null && hotelList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + hotelList.PhoneNUM : "", (hotelList.FaxNum != null && hotelList.FaxNum.Trim() != string.Empty) ? " Fax: " + hotelList.FaxNum : "", (hotelList.Email != null && hotelList.Email.Trim() != string.Empty) ? " Email: " + hotelList.Email : "", (hotelList.Rate != null && hotelList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + hotelList.Rate : ""));
                            }
                            if (hotelList.RecordType == "A")
                            {
                                HistoryDescription.AppendLine(string.Format("Maint Hotel Additions: For Leg: {0}{1}{2}{3}{4}{5}", Leg.LegNUM, (hotelList.CRHotelListDescription != null && hotelList.CRHotelListDescription.Trim() != string.Empty) ? " Name: " + hotelList.CRHotelListDescription : "", (hotelList.PhoneNUM != null && hotelList.PhoneNUM.Trim() != string.Empty) ? " Phone: " + hotelList.PhoneNUM : "", (hotelList.FaxNum != null && hotelList.FaxNum.Trim() != string.Empty) ? " Fax: " + hotelList.FaxNum : "", (hotelList.Email != null && hotelList.Email.Trim() != string.Empty) ? " Email: " + hotelList.Email : "", (hotelList.Rate != null && hotelList.Rate.ToString().Trim() != string.Empty) ? " Rate: " + hotelList.Rate : ""));
                            }
                        }
                        #endregion
                    }
                }

                #endregion

            }
            #endregion

            #region RequestException

            if (CorpRequest.State == CorporateRequestTripEntityState.Modified || CorpRequest.State == CorporateRequestTripEntityState.NoChange)
            {
                foreach (CRException CorpException in crMainentity.CRExceptions.ToList())
                {
                    Container.ObjectStateManager.ChangeObjectState(CorpException, System.Data.EntityState.Deleted);
                }
            }

            foreach (CRException CorpException in CorpRequest.CRExceptions.ToList())
            {

                if (CorpRequest.State != CorporateRequestTripEntityState.NoChange)
                {

                    if (CorpRequest.State != CorporateRequestTripEntityState.Added)
                    {
                        CRException Tripexp = new CRException();
                        Tripexp.CRMainID = crMainentity.CRMainID;
                        Tripexp.CustomerID = CorpException.CustomerID;
                        Tripexp.CRExceptionDescription = CorpException.CRExceptionDescription;
                        Tripexp.LastUpdUID = UserPrincipal.Identity.Name;
                        Tripexp.LastUpdTS = DateTime.UtcNow;
                        Tripexp.ExceptionTemplateID = CorpException.ExceptionTemplateID;
                        Tripexp.DisplayOrder = CorpException.DisplayOrder;
                        crMainentity.CRExceptions.Add(Tripexp);
                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(CorpException, System.Data.EntityState.Added);
                    }

                }
                else
                {
                    CRException Tripexp = new CRException();
                    Tripexp.CRMainID = crMainentity.CRMainID;
                    Tripexp.CustomerID = CorpException.CustomerID;
                    Tripexp.CRExceptionDescription = CorpException.CRExceptionDescription;
                    Tripexp.LastUpdUID = UserPrincipal.Identity.Name;
                    Tripexp.LastUpdTS = DateTime.UtcNow;
                    Tripexp.ExceptionTemplateID = CorpException.ExceptionTemplateID;
                    Tripexp.DisplayOrder = CorpException.DisplayOrder;
                    crMainentity.CRExceptions.Add(Tripexp);
                    Container.ObjectStateManager.ChangeObjectState(Tripexp, System.Data.EntityState.Added);
                }
            }
            #endregion

            #region "RequestHistory"
            if (HistoryDescription != null && HistoryDescription.Length > 0)
            {
                if (CorpRequest.State != CorporateRequestTripEntityState.NoChange)
                {


                    CRHistory ReqeustHistory = new CRHistory();
                    if (CorpRequest.State == CorporateRequestTripEntityState.Added)
                    {
                        ReqeustHistory.CRMainID = CorpRequest.CRMainID;
                    }
                    else
                    {
                        ReqeustHistory.CRMainID = crMainentity.CRMainID;
                    }
                    ReqeustHistory.CustomerID = CustomerID;
                    ReqeustHistory.LogisticsHistory = HistoryDescription.ToString();
                    ReqeustHistory.LastUpdUID = UserPrincipal.Identity.Name;
                    ReqeustHistory.LastUpdTS = DateTime.UtcNow;
                    ReqeustHistory.IsDeleted = false;

                    if (CorpRequest.State != CorporateRequestTripEntityState.Added)
                    {
                        crMainentity.CRHistories.Add(ReqeustHistory);
                    }
                    else
                    {
                        Container.CRHistories.AddObject(ReqeustHistory);
                    }
                }
            }
            #endregion


        }


        public ReturnValue<CRMain> GetRequest(Int64 RequestID)
        {
            ReturnValue<CRMain> ret = new ReturnValue<CRMain>();
            CorporateRequestDataModelContainer cs = null;
            CRMain crMain = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RequestID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        crMain = new CRMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;

                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;

                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        if (clientID > 0)
                        {
                            ret.EntityList = (from requests in cs.CRMains
                                              where ((requests.CRMainID == RequestID)
                                                        && (requests.CustomerID == CustomerID)
                                                        && (requests.ClientID == clientID)
                                                        && (requests.IsDeleted == false)
                                              )
                                              select requests).ToList();
                        }
                        else
                        {
                            ret.EntityList = (from requests in cs.CRMains
                                              where ((requests.CRMainID == RequestID)
                                                  && (requests.CustomerID == CustomerID)
                                                  && (requests.IsDeleted == false)
                                              )
                                              select requests).ToList();
                        }

                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {

                            if (isprivateUser)
                            {
                                if (ret.EntityList[0].IsPrivate != null)
                                {
                                    if ((bool)ret.EntityList[0].IsPrivate == false)
                                    {
                                        crMain = ret.EntityList[0];
                                        LoadRequestProperties(ref crMain, ref cs);
                                        ret.EntityList[0] = crMain;
                                        ret.ReturnFlag = true;
                                    }
                                    else
                                    {
                                        crMain = null;
                                        ret.ReturnFlag = false;
                                    }
                                }
                                else
                                {
                                    crMain = ret.EntityList[0];
                                    LoadRequestProperties(ref crMain, ref cs);
                                    ret.EntityList[0] = crMain;
                                    ret.ReturnFlag = true;
                                }

                            }
                            else
                            {
                                crMain = ret.EntityList[0];
                                LoadRequestProperties(ref crMain, ref cs);
                                ret.EntityList[0] = crMain;
                                ret.ReturnFlag = true;
                            }

                        }
                        else
                        {
                            crMain = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }


        private void LoadRequestProperties(ref CRMain crMain, ref CorporateRequestDataModelContainer cs)
        {
            if (crMain.AircraftID != null) cs.LoadProperty(crMain, c => c.Aircraft);
            if (crMain.ClientID != null) cs.LoadProperty(crMain, c => c.Client);
            if (crMain.HomebaseID != null) cs.LoadProperty(crMain, c => c.Company);
            if (crMain.DepartmentID != null) cs.LoadProperty(crMain, c => c.Department);
            if (crMain.AuthorizationID != null) cs.LoadProperty(crMain, c => c.DepartmentAuthorization);
            if (crMain.FleetID != null) cs.LoadProperty(crMain, c => c.Fleet);
            if (crMain.PassengerRequestorID != null) cs.LoadProperty(crMain, c => c.Passenger);
            if (crMain.TravelCoordinatorID != null) cs.LoadProperty(crMain, c => c.TravelCoordinator);

            cs.LoadProperty(crMain, c => c.CRDispatchNotes);

            if (crMain.CRDispatchNotes != null)
            {
                foreach (CRDispatchNote DispatchNote in crMain.CRDispatchNotes.ToList())
                {
                    if (DispatchNote.IsDeleted == true)
                        crMain.CRDispatchNotes.Remove(DispatchNote);

                    if (DispatchNote.CustomerID != null) cs.LoadProperty(DispatchNote, c => c.Customer);
                }

            }

            cs.LoadProperty(crMain, c => c.CRLegs);



            if (crMain.CRLegs != null)
            {
                foreach (CRLeg Leg in crMain.CRLegs.ToList())
                {
                    if (Leg.IsDeleted)
                        crMain.CRLegs.Remove(Leg);

                    if (Leg.DepartmentID != null) cs.LoadProperty(Leg, c => c.Department);
                    if (Leg.AAirportID != null) cs.LoadProperty(Leg, c => c.Airport);
                    if (Leg.DAirportID != null) cs.LoadProperty(Leg, c => c.Airport1);
                    if (Leg.FlightCategoryID != null) cs.LoadProperty(Leg, c => c.FlightCatagory);
                    if (Leg.AuthorizationID != null) cs.LoadProperty(Leg, c => c.DepartmentAuthorization);
                    if (Leg.ClientID != null) cs.LoadProperty(Leg, c => c.Client);
                    if (Leg.FBOID != null) cs.LoadProperty(Leg, c => c.FBO);

                    if (Leg.PassengerRequestorID != null) cs.LoadProperty(Leg, c => c.Passenger);


                    cs.LoadProperty(Leg, c => c.CRPassengers);

                    foreach (CRPassenger prefPass in Leg.CRPassengers)
                    {
                        cs.LoadProperty(prefPass, c => c.Passenger);
                    }

                    cs.LoadProperty(Leg, c => c.CRFBOLists);

                    foreach (CRFBOList Fbo in Leg.CRFBOLists)
                    {
                        cs.LoadProperty(Fbo, c => c.FBO);
                    }

                    cs.LoadProperty(Leg, c => c.CRCateringLists);

                    foreach (CRCateringList PrefCatDt in Leg.CRCateringLists)
                    {
                        cs.LoadProperty(PrefCatDt, c => c.Catering);
                    }

                    cs.LoadProperty(Leg, c => c.CRTransportLists);
                    cs.LoadProperty(Leg, c => c.CRHotelLists);
                }
            }
        }

        public ReturnValue<CRMain> DeleteRequest(Int64 RequestID)
        {
            ReturnValue<CRMain> ReturnValue = new ReturnValue<CRMain>();
            CorporateRequestDataModelContainer Container = new CorporateRequestDataModelContainer();
            CRMain request = new CRMain();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(RequestID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        //lock
                        LockManager<Data.CorporateRequest.CRMain> lockManager = new LockManager<CRMain>();
                        lockManager.Lock(FlightPak.Common.Constants.EntitySet.CorporateRequest.CRMain, RequestID);

                        using (Container = new CorporateRequestDataModelContainer())
                        {
                            request = new CRMain();
                            request = Container.CRMains.Where(X => X.CRMainID == RequestID).First();
                            if (request != null)
                            {
                                request.IsDeleted = true;
                                request.State = CorporateRequestTripEntityState.Deleted;
                                request.LastUpdTS = DateTime.UtcNow;
                                Container.SaveChanges();
                            }
                            ReturnValue.EntityInfo = null;
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock
                        LockManager<Data.CorporateRequest.CRMain> lockManager = new LockManager<CRMain>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CorporateRequest.CRMain, RequestID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        #endregion


        #region "Corp Leg"

        public ReturnValue<CRMain> AddCorporateRequestLeg(CRLeg corporaterequestLeg)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CRMain> UpdateCorporateRequestLeg(CRLeg corporaterequestLeg)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CRMain> DeleteCorporateRequestLeg(string TripNumber, string LegID, string TripLegID)
        {
            throw new NotImplementedException();
        }
        #endregion


        #region "Corp Passenger"

        public ReturnValue<CRMain> DeleteCorporateRequestCrew(string TripNumber, string LegID, string TripLegID)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CRMain> UpdateCorporateRequestCrew(CRPassenger corporaterequestPassenger)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CRMain> AddCorporateRequestCrew(CRPassenger corporaterequestPassenger)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region "Copy Request"

        public ReturnValue<CRMain> CopyRequestDetails(DateTime DepartDate, long tripID, long customerID, bool IsLogisticsToBeCopied, bool IsPaxToBeCopied, bool IsLegsToBeCopied, bool IsAllToBeCopied)
        {
            ReturnValue<CRMain> ReturnValue = new ReturnValue<CRMain>();
            CorporateRequestDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsPaxToBeCopied, IsLegsToBeCopied, IsAllToBeCopied))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopytripID = new long();
                    CopytripID = 0;
                    bool isAutoDispatch = false;
                    bool IsRevision = false;
                    using (cs = new CorporateRequestDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        if (UserPrincipal.Identity.Settings.IsAutomaticDispatchNum == true)
                            isAutoDispatch = true;
                        else
                            isAutoDispatch = false;
                        if (UserPrincipal.Identity.Settings.IsAutoRevisionNum == true)
                            IsRevision = true;
                        else
                            IsRevision = false;

                        CopytripID = cs.GetCopyRequestDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsPaxToBeCopied, IsLegsToBeCopied, IsAllToBeCopied, true, isAutoDispatch, IsRevision, UserName).FirstOrDefault();


                        ReturnValue = GetRequest((long)CopytripID);

                        if (ReturnValue.ReturnFlag)
                        {

                            CRMain CorpRequest = ReturnValue.EntityList[0];
                            LockManager<Data.CorporateRequest.CRMain> lockManager = new LockManager<CRMain>();
                            lockManager.Lock(FlightPak.Common.Constants.EntitySet.CorporateRequest.CRMain, CorpRequest.CRMainID);

                            CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                            ReturnValue = UpdateRequest(CorpRequest);

                            #region "TripHistory"

                            //CRMain OldTrip = cs.CRMains.Where(x => (x.CRMainID == tripID && x.CustomerID == customerID)).SingleOrDefault();
                            //if (OldTrip != null)
                            //{
                            //    if (!isHistoryToBeCopied)
                            //    {
                            //        PreflightTripHistory TripHistory = new PreflightTripHistory();
                            //        TripHistory.TripID = Trip.TripID;
                            //        TripHistory.CustomerID = CustomerID;
                            //        TripHistory.HistoryDescription = "Trip Details Copied From Trip No.: " + OldTrip.TripNUM.ToString();
                            //        TripHistory.LastUpdUID = UserPrincipal.Identity.Name;
                            //        TripHistory.LastUpdTS = DateTime.UtcNow;
                            //        cs.PreflightTripHistories.AddObject(TripHistory);
                            //        cs.SaveChanges();
                            //    }
                            //}



                            #endregion

                            if (ReturnValue.ReturnFlag)
                            {
                                var retvalexp = GetRequestExceptionList(CorpRequest.CRMainID);

                                if (retvalexp.ReturnFlag)
                                {
                                    if (retvalexp.EntityList != null && retvalexp.EntityList.Count > 0)
                                    {
                                        //CorpRequest. = "!";
                                    }
                                }


                                ReturnValue.EntityInfo = CorpRequest;
                                ReturnValue.ReturnFlag = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }
        #endregion

        #region "Exception"
        public ReturnValue<CRException> GetRequestExceptionList(Int64 CRMainID)
        {
            ReturnValue<Data.CorporateRequest.CRException> ret = new ReturnValue<Data.CorporateRequest.CRException>();
            CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CRMainID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.CorporateRequest.CRException>();
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.CRExceptions.Where(x => (x.CRMainID == CRMainID && x.CustomerID == CustomerID)).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<ExceptionTemplate> GetExceptionTemplateList()
        {
            ReturnValue<ExceptionTemplate> ret = new ReturnValue<ExceptionTemplate>();
            CorporateRequestDataModelContainer cs;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new CorporateRequestDataModelContainer();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;

                    List<ExceptionTemplate> BusinessErrors = (from Exc in cs.ExceptionTemplates
                                                              where (Exc.ModuleID == (long)CorporateRequestExceptionModule.CRMain)
                                                              select Exc).ToList();


                    ret.EntityList = BusinessErrors;

                    if (ret.EntityList != null && ret.EntityList.Count > 0)

                        ret.ReturnFlag = true;
                    else
                        ret.ReturnFlag = false;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<CRException> ValidateRequestforMandatoryExcep(CRMain Request)
        {

            List<CRException> ExceptionList;
            CorporateRequestDataModelContainer Container;
            ReturnValue<CRException> returnValue = new ReturnValue<CRException>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Request))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ExceptionList = new List<CRException>();
                    using (Container = new CorporateRequestDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        TripExceptions = Container.ExceptionTemplates.Where(X => X.ModuleID == (long)(CorporateRequestExceptionModule.CRMain)).ToList();
                        List<CRLeg> crlegs = Request.CRLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();

                        List<ExceptionTemplate> CRMain_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)CorporateRequestExceptionSubModule.CRmain).ToList();
                        List<ExceptionTemplate> CRLeg_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)CorporateRequestExceptionSubModule.Legs).ToList();
                        List<ExceptionTemplate> CRPassenger_TemplateList = TripExceptions.Where(mainlist => mainlist.SubModuleID == (long)CorporateRequestExceptionSubModule.Passenger).ToList();

                        #region main
                        foreach (ExceptionTemplate exptemp in CRMain_TemplateList.ToList())
                        {
                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.AircraftTypeReq))
                            {
                                if (Request.AircraftID == null)
                                {
                                    CRException CurrException = new CRException();
                                    CurrException.CRMainID = Request.CRMainID;
                                    CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    ExceptionList.Add(CurrException);
                                }
                            }

                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.RequestRequiresAtleastoneLeg))
                            {
                                if (Request.CRLegs == null || Request.CRLegs.Count == 0)
                                {
                                    CRException CurrException = new CRException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.CRMainID = Request.CRMainID;
                                    CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    ExceptionList.Add(CurrException);
                                }
                            }

                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.HomebaseRequired))
                            {
                                if (Request.HomebaseID == null)
                                {
                                    CRException CurrException = new CRException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.CRMainID = Request.CRMainID;
                                    CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    ExceptionList.Add(CurrException);
                                }
                            }

                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.TailNoRequired))
                            {
                                if (Request.FleetID == null)
                                {
                                    CRException CurrException = new CRException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.CRMainID = Request.CRMainID;
                                    CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    ExceptionList.Add(CurrException);
                                }
                            }

                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.DepartDateRequired))
                            {
                                if (Request.EstDepartureDT == null)
                                {
                                    CRException CurrException = new CRException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.CRMainID = Request.CRMainID;
                                    CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    ExceptionList.Add(CurrException);
                                }
                            }


                            if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.ClientCodeRequired))
                            {
                                if (Request.ClientID == null)
                                {
                                    CRException CurrException = new CRException();
                                    ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                    CurrException.CRMainID = Request.CRMainID;
                                    CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                    CurrException.CRExceptionDescription = exptemp.ExceptionTemplate1;
                                    CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                    CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                    CurrException.LastUpdTS = DateTime.UtcNow;
                                    CurrException.ExceptionTemplate = exptemp;
                                    ExceptionList.Add(CurrException);
                                }
                            }

                        }

                        #endregion

                        #region Leg Validation
                        if (Request.CRLegs != null) //&& Trip.PreflightLegs.Count > 0
                        {
                            foreach (CRLeg CorpLegitem in crlegs)
                            {
                                foreach (ExceptionTemplate exptemp in CRLeg_TemplateList.ToList())
                                {
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.DepartureICAOIDReq))
                                    {
                                        if (CorpLegitem.DAirportID == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = Request.CRMainID;
                                            CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                            string Desc = string.Empty;
                                            Desc = exptemp.ExceptionTemplate1;
                                            Desc = Desc.Replace("<legnum>", CorpLegitem.LegNUM.ToString());
                                            CurrException.CRExceptionDescription = Desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            ExceptionList.Add(CurrException);
                                        }
                                    }
                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.ArrivalICAOIDReq))
                                    {
                                        if (CorpLegitem.AAirportID == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = Request.CRMainID;
                                            CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                            string Desc = string.Empty;
                                            Desc = exptemp.ExceptionTemplate1;
                                            Desc = Desc.Replace("<legnum>", CorpLegitem.LegNUM.ToString());
                                            CurrException.CRExceptionDescription = Desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            ExceptionList.Add(CurrException);
                                        }
                                    }


                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LocalDepartDateRequired))
                                    {
                                        if (CorpLegitem.DepartureDTTMLocal == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = Request.CRMainID;
                                            CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                            string Desc = string.Empty;
                                            Desc = exptemp.ExceptionTemplate1;
                                            Desc = Desc.Replace("<legnum>", CorpLegitem.LegNUM.ToString());
                                            CurrException.CRExceptionDescription = Desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            ExceptionList.Add(CurrException);
                                        }
                                    }

                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LocalArrivalDateRequired))
                                    {
                                        if (CorpLegitem.ArrivalDTTMLocal == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = Request.CRMainID;
                                            CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                            string Desc = string.Empty;
                                            Desc = exptemp.ExceptionTemplate1;
                                            Desc = Desc.Replace("<legnum>", CorpLegitem.LegNUM.ToString());
                                            CurrException.CRExceptionDescription = Desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            ExceptionList.Add(CurrException);
                                        }
                                    }

                                    if (exptemp.ExceptionTemplateID == (long)(CorporateRequestExceptionCheckList.LegClientCodeRequired))
                                    {
                                        if (CorpLegitem.ClientID == null)
                                        {
                                            CRException CurrException = new CRException();
                                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                            CurrException.CRMainID = Request.CRMainID;
                                            CurrException.CustomerID = UserPrincipal.Identity.CustomerID;
                                            string Desc = string.Empty;
                                            Desc = exptemp.ExceptionTemplate1;
                                            Desc = Desc.Replace("<legnum>", CorpLegitem.LegNUM.ToString());
                                            CurrException.CRExceptionDescription = Desc;
                                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                                            CurrException.LastUpdTS = DateTime.UtcNow;
                                            CurrException.ExceptionTemplate = exptemp;
                                            ExceptionList.Add(CurrException);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        returnValue.ReturnFlag = true;
                        returnValue.EntityList = ExceptionList;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;
        }
        #endregion

        #region "Search"
        public ReturnValue<CRMain> GetRequestByTripNum(Int64 CRTripNUM)
        {
            ReturnValue<CRMain> ret = null;
            CRMain crMain = null;
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CRTripNUM))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<CRMain>();
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        crMain = new CRMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;


                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        if (clientID > 0)
                        {
                            ret.EntityList = (from trips in cs.CRMains
                                              where ((trips.CRTripNUM == CRTripNUM)
                                                     && (trips.CustomerID == CustomerID)
                                                     && (trips.ClientID == clientID)
                                                     && (trips.IsDeleted == false)
                                                     )
                                              select trips).ToList();
                        }
                        else
                        {
                            ret.EntityList = (from trips in cs.CRMains
                                              where ((trips.CRTripNUM == CRTripNUM)
                                                    && (trips.CustomerID == CustomerID)
                                                    && (trips.IsDeleted == false)
                                                    )
                                              select trips).ToList();
                        }

                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {
                            if (isprivateUser)
                            {
                                if ((bool)ret.EntityList[0].IsPrivate == false)
                                {
                                    crMain = ret.EntityList[0];
                                    LoadRequestProperties(ref crMain, ref cs);
                                    ret.EntityList[0] = crMain;
                                    ret.ReturnFlag = true;
                                }
                                else
                                {
                                    crMain = null;
                                    ret.ReturnFlag = false;
                                }

                            }
                            else
                            {
                                crMain = ret.EntityList[0];
                                LoadRequestProperties(ref crMain, ref cs);
                                ret.EntityList[0] = crMain;
                                ret.ReturnFlag = true;
                            }
                        }
                        else
                        {
                            crMain = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<View_CorporateRequestMainList> GetAllCorporateRequestMainList(Int64 homebaseID, Int64 clientID, string tripSheet, string worksheetstr, string hold, string cancelled, string schedServ, string Unfulfilled, bool isLog, Nullable<DateTime> EstDepartDate, string homebaseIDCSVStr, Int64 travelID)
        {
            ReturnValue<Data.CorporateRequest.View_CorporateRequestMainList> ret = null;
            CorporateRequestDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, homebaseIDCSVStr, travelID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        ret = new ReturnValue<Data.CorporateRequest.View_CorporateRequestMainList>();

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        if (clientID == 0)
                            clientID = UserPrincipal.Identity.ClientId == null ? 0 : (long)UserPrincipal.Identity.ClientId;

                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        ret.EntityList = cs.GetAllCorporateRequestMainList(CustomerID, homebaseID, clientID, tripSheet, worksheetstr, hold, cancelled, schedServ, Unfulfilled, isLog, EstDepartDate, isprivateUser, homebaseIDCSVStr, travelID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<View_CorporateRequestMainList> GetAllDeletedCorporateRequestMainList()
        {
            ReturnValue<Data.CorporateRequest.View_CorporateRequestMainList> ret = null;
            CorporateRequestDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        ret = new ReturnValue<Data.CorporateRequest.View_CorporateRequestMainList>();

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllDeletedCorporateRequestMainList(CustomerID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion

        #region "Retrieve Pax"
        public ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList> GetAllCorporateRequestAvailablePaxList()
        {
            ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList> ret = null;
            CorporateRequestDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.CorporateRequest.GetAllCorporateRequestAvailablePaxList>();
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCorporateRequestAvailablePaxList(CustomerID, ClientId).ToList();
                        foreach (var item in ret.EntityList)
                        {
                            item.Passport = Crypting.Decrypt(item.Passport);
                        }
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.CorporateRequest.CrewPassengerPassport> GetPassengerPassportByPassportID(Int64 PassportId)
        {
            ReturnValue<Data.CorporateRequest.CrewPassengerPassport> CrewPassport = new ReturnValue<Data.CorporateRequest.CrewPassengerPassport>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassportId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    CrewPassport.EntityList = cs.GetPassengerPassportByPassportID(CustomerID, PassportId).ToList();
                    foreach (var item in CrewPassport.EntityList)
                    {
                        item.PassportNum = Crypting.Decrypt(item.PassportNum);
                    }
                    CrewPassport.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassport;
        }
        #endregion

        #region "Change Queue"

        public ReturnValue<GetAllCRMain> GetCRChangeQueue()
        {
            ReturnValue<Data.CorporateRequest.GetAllCRMain> ret = new ReturnValue<Data.CorporateRequest.GetAllCRMain>();
            CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.CorporateRequest.GetAllCRMain>();
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCRMain(CustomerID, Convert.ToInt64(0)).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<CRMain> QueueTransfer(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRMain.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.QueueTransfer(oCRMain.CRMainID, CustomerID, oCRMain.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<CRMain> DenyRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRMain.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.DenyRequest(oCRMain.CRMainID, CustomerID, oCRMain.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<CRMain> AcceptRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRMain.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.AcceptRequest(oCRMain.CRMainID, CustomerID, oCRMain.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<CRLeg> ChangePrivate(CRLeg oCRLeg)
        {
            ReturnValue<Data.CorporateRequest.CRLeg> ret = new ReturnValue<Data.CorporateRequest.CRLeg>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRLeg))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRLeg.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRLeg.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.ChangePrivate(oCRLeg.CRLegID, oCRLeg.IsPrivate, CustomerID, oCRLeg.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }


        public ReturnValue<CRMain> ApproveRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRMain.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.ApproveRequest(oCRMain.CRMainID, CustomerID, oCRMain.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        #endregion

        #region "SubmitRequest"
        public ReturnValue<CRMain> UpdateCRMainCorpStatus(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRMain.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.UpdateCRMainCorpStatus(oCRMain.CRMainID, CustomerID, oCRMain.CorporateRequestStatus, oCRMain.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion

        #region "CancelRequest"
        public ReturnValue<CRMain> CancelRequest(CRMain oCRMain)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRMain))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRMain.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.CancelRequest(oCRMain.CRMainID, CustomerID, oCRMain.CorporateRequestStatus, oCRMain.LastUpdUID);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion

        #region "History"
        public ReturnValue<CRHistory> UpdateCRHistory(CRHistory history)
        {
            ReturnValue<CRHistory> ReturnValue = new ReturnValue<CRHistory>();
            CorporateRequestDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(history))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        using (Container = new CorporateRequestDataModelContainer())
                        {

                            history.CustomerID = CustomerID;

                            history.LastUpdTS = DateTime.UtcNow;
                            history.LastUpdUID = UserPrincipal.Identity.Name;

                            var HistoryId = history.CRHistoryID;


                            CRHistory CRHistoryEntity = Container.CRHistories.Where(s => s.CRHistoryID == HistoryId).FirstOrDefault();
                            if (CRHistoryEntity != null)
                            {
                                Container.CRHistories.Attach(CRHistoryEntity);
                            }
                            Container.CRHistories.ApplyCurrentValues(history);

                            //Unlock
                            LockManager<Data.CorporateRequest.CRMain> lockManager = new LockManager<CRMain>();
                            lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CorporateRequest.CRMain, history.CRHistoryID);

                            Container.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {

                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.CorporateRequest.CRHistory> GetCRHistory(long CustomerId, long CRMainId)
        {
            ReturnValue<Data.CorporateRequest.CRHistory> ret = null;
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, CRMainId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.CorporateRequest.CRHistory>();
                    using (cs = new CorporateRequestDataModelContainer())
                    {
                        CustomerId = UserPrincipal.Identity.CustomerID;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetCRHistory(CustomerId, CRMainId).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<CRHistory> AddCRHistory(CRHistory oCRHistory)
        {
            ReturnValue<Data.CorporateRequest.CRHistory> ret = new ReturnValue<Data.CorporateRequest.CRHistory>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCRHistory))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        oCRHistory.CustomerID = UserPrincipal.Identity.CustomerID;
                        oCRHistory.LastUpdUID = UserPrincipal.Identity.Name;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        cs.AddCRHistory(oCRHistory.CRMainID, oCRHistory.CustomerID, oCRHistory.LogisticsHistory, oCRHistory.LastUpdUID, oCRHistory.LastUpdTS, oCRHistory.IsDeleted);
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion

        #region "AcknowledgeRequest"
        public ReturnValue<CRMain> RequestAcknowledge(long requestID, long tripID, long customerID, string lastUpdUID, DateTime lastUpdTS)
        {
            ReturnValue<Data.CorporateRequest.CRMain> ret = new ReturnValue<Data.CorporateRequest.CRMain>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(requestID, tripID, customerID, lastUpdUID, lastUpdTS))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (CorporateRequestDataModelContainer cs = new CorporateRequestDataModelContainer())
                    {
                        long? AckRequestID = new long();
                        AckRequestID = 0;
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        AckRequestID = cs.RequestAcknowledge(requestID, tripID, customerID, lastUpdUID, lastUpdTS).FirstOrDefault();
                        ret = GetRequest((long)requestID);

                        if (ret.ReturnFlag)
                        {

                            CRMain CorpRequest = ret.EntityList[0];

                            CorpRequest.State = CorporateRequestTripEntityState.NoChange;
                            ret = UpdateRequest(CorpRequest);

                            if (ret.ReturnFlag)
                            {
                                var retvalexp = GetRequestExceptionList(CorpRequest.CRMainID);

                                if (retvalexp.ReturnFlag)
                                {
                                    if (retvalexp.EntityList != null && retvalexp.EntityList.Count > 0)
                                    {
                                        //CorpRequest. = "!";
                                    }
                                }


                                ret.EntityInfo = CorpRequest;
                                ret.ReturnFlag = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion


        #region "Travelcoordinator"
        public ReturnValue<Data.CorporateRequest.TravelCoordinator> GetTravelcoordinatorbyID(Int64 TravelCoordinatorID)
        {
            ReturnValue<Data.CorporateRequest.TravelCoordinator> travel = new ReturnValue<Data.CorporateRequest.TravelCoordinator>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinatorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    travel.EntityList = cs.GetTravelcoordinatorbyID(CustomerID, TravelCoordinatorID).ToList();
                    travel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return travel;
        }
        #endregion


        #region "Fleet"
        public ReturnValue<Data.CorporateRequest.Fleet> GetFleetbyID(Int64 FleetID)
        {
            ReturnValue<Data.CorporateRequest.Fleet> fleet = new ReturnValue<Data.CorporateRequest.Fleet>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    fleet.EntityList = cs.GetFleetbyID(CustomerID, FleetID).ToList();
                    fleet.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return fleet;
        }

        public ReturnValue<Data.CorporateRequest.GetAllFleetForCR> GetAllFleetforCR(Int64 TravelID)
        {
            ReturnValue<Data.CorporateRequest.GetAllFleetForCR> aircraft = new ReturnValue<Data.CorporateRequest.GetAllFleetForCR>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    aircraft.EntityList = cs.GetAllFleetForCR(TravelID).ToList();
                    aircraft.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return aircraft;
        }
        #endregion


        #region "Aircraft"
        public ReturnValue<Data.CorporateRequest.Aircraft> GetAircraftbyID(Int64 AircraftID)
        {
            ReturnValue<Data.CorporateRequest.Aircraft> aircraft = new ReturnValue<Data.CorporateRequest.Aircraft>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    aircraft.EntityList = cs.GetAircraftbyID(CustomerID, AircraftID).ToList();
                    aircraft.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return aircraft;
        }
        #endregion

        #region "Passenger"
        public ReturnValue<Data.CorporateRequest.Passenger> GetPassengerbyID(Int64 PassengerRequestorID)
        {
            ReturnValue<Data.CorporateRequest.Passenger> aircraft = new ReturnValue<Data.CorporateRequest.Passenger>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    aircraft.EntityList = cs.GetPassengerbyID(CustomerID, PassengerRequestorID).ToList();
                    aircraft.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return aircraft;
        }

        public ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR> GetPassengerByPassengerRequestorIDforCR(Int64 TravelID)
        {
            ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR> aircraft = new ReturnValue<Data.CorporateRequest.GetPassengerByPassengerRequestorIDforCR>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    aircraft.EntityList = cs.GetCRPaxbyPaxId(TravelID).ToList();
                    aircraft.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return aircraft;
        }
        #endregion


        #region "Department"
        public ReturnValue<Data.CorporateRequest.Department> GetDepartmentbyID(Int64 DepartmentID)
        {
            ReturnValue<Data.CorporateRequest.Department> dep = new ReturnValue<Data.CorporateRequest.Department>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    dep.EntityList = cs.GetDepartmentbyID(CustomerID, DepartmentID).ToList();
                    dep.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return dep;
        }
        #endregion

        #region "Authorization"
        public ReturnValue<Data.CorporateRequest.DepartmentAuthorization> GetAuthorizationbyID(Int64 AuthorizationID, Int64 DepartmentID)
        {
            ReturnValue<Data.CorporateRequest.DepartmentAuthorization> dep = new ReturnValue<Data.CorporateRequest.DepartmentAuthorization>();
            CorporateRequestDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cs = new Data.CorporateRequest.CorporateRequestDataModelContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    dep.EntityList = cs.GetAuthorizationbyID(CustomerID, AuthorizationID, DepartmentID).ToList();
                    dep.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return dep;
        }
        #endregion
    }
}
