﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Data.CharterQuote;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
//using System.Text;
using System.Globalization;
using System.Reflection;

namespace FlightPak.Business.CharterQuote
{
    public partial class CharterQuoteManager : BaseManager
    {
        private ExceptionManager exManager;
        #region Comments
        //Defect : 2565 - Fix by Prabhu: 01/17/2014
        //Exception is not calculated properly for copied quote.
        #endregion

        #region "CharterQuote File"

        public ReturnValue<CQFile> GetCQRequestByID(Int64 CQFileID, bool isExpressQuote)
        {
            ReturnValue<CQFile> ret = new ReturnValue<CQFile>();
            CharterQuoteDataModelContainer cs = null;
            CQFile cqfile = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, isExpressQuote))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        cqfile = new CQFile();
                        cs.ContextOptions.LazyLoadingEnabled = false;

                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;

                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        if (clientID > 0)
                        {
                            ret.EntityList = (from CQFiles in cs.CQFiles
                                              where ((CQFiles.CQFileID == CQFileID)
                                                        && (CQFiles.CustomerID == CustomerID)
                                                        && (CQFiles.IsDeleted == false)
                                                        && (CQFiles.ExpressQuote == isExpressQuote)
                                                  // && (trips.IsPrivate == isprivateUser? false: (bool)trips.IsPrivate)
                                              )
                                              select CQFiles).ToList();
                        }
                        else
                        {
                            ret.EntityList = (from CQFiles in cs.CQFiles
                                              where ((CQFiles.CQFileID == CQFileID)
                                                  && (CQFiles.CustomerID == CustomerID)
                                                  && (CQFiles.IsDeleted == false)
                                                  && (CQFiles.ExpressQuote == isExpressQuote)
                                                  // && (trips.IsPrivate == isprivateUser ? false : (bool)trips.IsPrivate)
                                              )
                                              select CQFiles).ToList();
                        }

                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {


                            cqfile = ret.EntityList[0];
                            LoadTripProperties(ref cqfile, ref cs);
                            ret.EntityList[0] = cqfile;
                            ret.ReturnFlag = true;


                        }
                        else
                        {
                            cqfile = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }


        public ReturnValue<CQFile> GetCQRequestByFileNum(Int64 FileNum, bool isExpressQuote)
        {
            ReturnValue<CQFile> ret = null;
            CQFile cqFile = null;
            CharterQuoteDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FileNum, isExpressQuote))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<CQFile>();
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        cqFile = new CQFile();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        long clientID = UserPrincipal.Identity.ClientId != null ? (long)UserPrincipal.Identity.ClientId : 0;


                        bool isprivateUser = false;
                        if (!UserPrincipal.Identity.IsSysAdmin)
                        {
                            if (UserPrincipal.Identity.IsTripPrivacy)
                            {
                                isprivateUser = true;
                            }
                        }

                        if (clientID > 0)
                        {
                            ret.EntityList = (from Files in cs.CQFiles
                                              where ((Files.FileNUM == FileNum)
                                                     && (Files.CustomerID == CustomerID)
                                                  //   && (Files.ClientID == clientID)
                                                     && (Files.IsDeleted == false)
                                                     && (Files.ExpressQuote == isExpressQuote)
                                                  // && (trips.IsPrivate == isprivateUser ? false : (bool)trips.IsPrivate)
                                                     )
                                              select Files).ToList();
                        }
                        else
                        {
                            ret.EntityList = (from Files in cs.CQFiles
                                              where ((Files.FileNUM == FileNum)
                                                    && (Files.CustomerID == CustomerID)
                                                    && (Files.IsDeleted == false)
                                                    && (Files.ExpressQuote == isExpressQuote)
                                                  // && (trips.IsPrivate == isprivateUser ? false : (bool)trips.IsPrivate)
                                                    )
                                              select Files).ToList();
                        }

                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {

                            cqFile = ret.EntityList[0];
                            LoadTripProperties(ref cqFile, ref cs);
                            ret.EntityList[0] = cqFile;
                            ret.ReturnFlag = true;
                        }
                        else
                        {
                            cqFile = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }


        /// <summary>
        /// Method to update Trip information
        /// </summary>
        /// <returns>Trip entity</returns>
        public ReturnValue<Data.CharterQuote.CQFile> UpdateFile(CQFile cqFile)
        {
            ReturnValue<Data.CharterQuote.CQFile> ReturnValue = null;
            Data.CharterQuote.CharterQuoteDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqFile))
            {
                ReturnValue = new ReturnValue<CQFile>();
                try
                {
                    using (Container = new CharterQuoteDataModelContainer())
                    {
                        bool SaveFile = true;
                        if ((cqFile.ExpressQuote == null) || (cqFile.ExpressQuote != null && !(bool)cqFile.ExpressQuote))
                        {
                            base.CheckLock = true;
                            ReturnValue.ReturnFlag = base.Validate(cqFile, ref ReturnValue.ErrorMessage);

                            if (ReturnValue.ReturnFlag)
                            {
                                if (cqFile.CQMains != null && cqFile.CQMains.Count > 0)
                                {
                                    foreach (CQMain cqMain in cqFile.CQMains.Where(x => x.IsDeleted == false).ToList())
                                    {
                                        if (cqMain != null && cqMain.CQLegs != null)
                                        {
                                            foreach (CQLeg cqLeg in cqMain.CQLegs.Where(x => x.IsDeleted == false).ToList())
                                            {
                                                ReturnValue<CQLeg> ReturnLegValue = new ReturnValue<CQLeg>();
                                                base.CheckLock = false;
                                                ReturnLegValue.ReturnFlag = base.Validate(cqLeg, ref ReturnLegValue.ErrorMessage);

                                                if (!ReturnLegValue.ReturnFlag)
                                                {
                                                    SaveFile = false;
                                                    ReturnLegValue.ErrorMessage = "Leg " + cqLeg.LegNUM + ": \\n" + ReturnLegValue.ErrorMessage;
                                                    ReturnValue.ErrorMessage += ReturnLegValue.ErrorMessage;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                                SaveFile = false;
                        }

                        if (SaveFile)
                        {
                            cqFile.CustomerID = UserPrincipal.Identity.CustomerID;
                            cqFile.LastUpdTS = DateTime.UtcNow;
                            if (UserPrincipal.Identity.Name != null)
                                cqFile.LastUpdUID = UserPrincipal.Identity.Name.Trim();

                            Container.ContextOptions.LazyLoadingEnabled = false;
                            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                            Container.ContextOptions.ProxyCreationEnabled = false;

                            #region EXCEPTIONS

                            List<CQException> ExceptionList = new List<CQException>();
                            CharterQuoteValidationManager ValidMgr = new CharterQuoteValidationManager();

                            if (cqFile.CQMains != null)
                            {
                                decimal _creditLimit = 0.0M;
                                if (cqFile.Credit != null)
                                    _creditLimit = (decimal)cqFile.Credit;

                                foreach (CQMain cqMain in cqFile.CQMains)
                                {
                                    var validateResult = ValidMgr.ValidateBusinessRules(cqFile, cqMain, false);

                                    if (validateResult != null && validateResult.ReturnFlag == true && validateResult.EntityList.Count > 0)
                                    {
                                        ExceptionList = validateResult.EntityList;

                                        // Bind the Exception List into Trip Object
                                        if (ExceptionList.Count > 0)
                                        {
                                            //cqMain.CQExceptions.Clear();

                                            foreach (CQException Exception in ExceptionList)
                                            {

                                                CQException ExceptionObj = new CQException();
                                                if (cqMain.CQMainID != 0 && cqMain.State != CQRequestEntityState.Deleted)
                                                {
                                                    cqMain.State = CQRequestEntityState.Modified;
                                                    ExceptionObj.CQMainID = cqMain.CQMainID;
                                                }

                                                //ExceptionObj.ExceptionGroup = GetSubmoduleModule(Convert.ToInt64(Exception.ExceptionGroup));
                                                ExceptionObj.CQExceptionDescription = Exception.CQExceptionDescription;
                                                ExceptionObj.ExceptionTemplateID = Exception.ExceptionTemplateID;
                                                ExceptionObj.DisplayOrder = Exception.DisplayOrder;
                                                ExceptionObj.CustomerID = UserPrincipal.Identity.CustomerID;
                                                cqMain.CQExceptions.Add(ExceptionObj);
                                                //Container.ObjectStateManager.ChangeObjectState(ExceptionObj, System.Data.EntityState.Added);
                                            }
                                        }
                                    }
                                }
                            }



                            #endregion

                            //Update operation goes here
                            UpdateEntity(ref cqFile, ref Container);

                            Container.SaveChanges();

                            //Unlock
                            LockManager<Data.CharterQuote.CQFile> lockManager = new LockManager<CQFile>();
                            lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CharterQuote.CQFile, cqFile.CQFileID);

                            //Return File Updtae Status   
                            ReturnValue = GetCQRequestByID((long)cqFile.CQFileID, (bool)cqFile.ExpressQuote);
                            ReturnValue.EntityInfo = ReturnValue.EntityList[0];
                            ReturnValue.ReturnFlag = true;
                        }
                        else
                        {
                            ReturnValue.ReturnFlag = false;
                            ReturnValue.EntityInfo = cqFile;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //manually handled
                    ReturnValue.ErrorMessage = ex.Message;
                    ReturnValue.ReturnFlag = false;
                    ReturnValue.EntityInfo = null;
                }
                finally
                {

                }

                return ReturnValue;
            }
        }

        private void UpdateEntity(ref CQFile cqFile, ref CharterQuoteDataModelContainer Container)
        {
            // Load original parent including the child item collection 

            #region "History"
            StringBuilder HistoryDescription = new System.Text.StringBuilder();
            #endregion


            cqFile.LastUpdTS = DateTime.UtcNow;
            cqFile.LastUpdUID = UserPrincipal.Identity.Name;

            var cqFileID = cqFile.CQFileID;
            CQFile cqFileEntity = Container.CQFiles
                .Where(p => p.CQFileID == cqFileID).SingleOrDefault();


            #region File
            if (cqFile.State == CQRequestEntityState.Modified)
            {
                Container.CQFiles.Attach(cqFileEntity);
                Container.CQFiles.ApplyCurrentValues(cqFile);

                #region "History For Modified File"
                var originalValues = Container.ObjectStateManager.GetObjectStateEntry(cqFileEntity).OriginalValues;
                var CurrentValues = Container.ObjectStateManager.GetObjectStateEntry(cqFileEntity).CurrentValues;
                PropertyInfo[] properties = typeof(CQFile).GetProperties();

                for (int i = 0; i < originalValues.FieldCount; i++)
                {
                    var fieldname = originalValues.GetName(i);

                    object value1 = originalValues.GetValue(i);
                    object value2 = CurrentValues.GetValue(i);

                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                    {

                        #region "Depart Date"
                        //if (fieldname.ToLower() == "estdeparturedt")
                        //{
                        //    HistoryDescription.AppendLine(
                        //        string.Format(
                        //        "{0}  Changed From {1} To {2}", "Depart Date",
                        //        (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value1)) : string.Empty),
                        //        (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value2)) : string.Empty)
                        //        ));

                        //}
                        #endregion

                    }
                }
                #endregion
            }
            else if (cqFile.State == CQRequestEntityState.Added)
            {
                cqFile.IsDeleted = false;
                // Trip.RecordType = "T";
                Container.CQFiles.AddObject(cqFile);

                #region "History"
                //if (cqFile.RecordType == "T")
                //    HistoryDescription.AppendLine("New Trip Added");
                //else if (cqFile.RecordType == "M")
                //    HistoryDescription.AppendLine("New Fleet Calendar Activity Added");
                //else if (cqFile.RecordType == "C")
                //    HistoryDescription.AppendLine("New Crew Calendar Activity Added");
                #endregion
            }
            else
            {
                Container.CQFiles.Attach(cqFileEntity);
                //Container.CQFiles.Attach(Trip);
                //Container.CQFiles.Attach(CQFileentity);
                //Container.CQFiles.ApplyCurrentValues(Trip);
            }
            #endregion

            #region Quote -> 1.Legs -> 1.1 PAX  - >1.2 Logistics - > 1.2.1 Catering- > 1.2.1 FBO- > 1.2.1 Hotel- > 1.2.1 Transport
            foreach (var cqMain in cqFile.CQMains.ToList())
            {
                cqMain.LastUpdTS = DateTime.UtcNow;
                cqMain.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CQMain originalcqMain = new CQMain();

                if (cqMain.State != CQRequestEntityState.Added)
                {
                    originalcqMain = Container.CQMains
                    .Where(c => c.CQMainID == cqMain.CQMainID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (cqMain.State == CQRequestEntityState.Modified)
                    {
                        cqMain.IsDeleted = false;
                        cqMain.CustomerID = CustomerID;
                        Container.CQMains.Attach(originalcqMain);
                        Container.CQMains.ApplyCurrentValues(cqMain);

                        #region "History For Modified Quote"
                        var originalcqMainValues = Container.ObjectStateManager.GetObjectStateEntry(originalcqMain).OriginalValues;
                        var CurrentcqMainValues = Container.ObjectStateManager.GetObjectStateEntry(originalcqMain).CurrentValues;
                        PropertyInfo[] properties = typeof(CQMain).GetProperties();
                        for (int i = 0; i < originalcqMainValues.FieldCount; i++)
                        {
                            var fieldname = originalcqMainValues.GetName(i);

                            object value1 = originalcqMainValues.GetValue(i);
                            object value2 = CurrentcqMainValues.GetValue(i);

                            if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                            {

                                #region "Submitted Quote to customer"
                                if (fieldname.ToLower() == "issubmitted")
                                {
                                    if (cqMain.IsSubmitted != null && cqMain.IsSubmitted == true)
                                        HistoryDescription.AppendLine(string.Format("Marked Quote Submitted On", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", cqMain.LastSubmitDT)));
                                    else
                                        HistoryDescription.AppendLine(string.Format("Unmarked Quote Submitted"));
                                }
                                #endregion

                                #region "Accepted by customer"
                                if (fieldname.ToLower() == "isaccepted")
                                {
                                    if (cqMain.IsAccepted != null && cqMain.IsAccepted == true)
                                        HistoryDescription.AppendLine(string.Format("Marked Quote Accepted On", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", cqMain.LastAcceptDT)));
                                    else
                                        HistoryDescription.AppendLine(string.Format("Unmarked Quote Accepted"));
                                }
                                #endregion

                                #region "Last Accepted changed"
                                if (fieldname.ToLower() == "lastacceptdt")
                                {
                                    if ((originalcqMain.IsAccepted != null && (bool)originalcqMain.IsAccepted) && (cqMain.IsAccepted != null && (bool)cqMain.IsAccepted))
                                        HistoryDescription.AppendLine(string.Format("{0}  {1} To {2}", "Changed Last Accepted Date From ", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value1), String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value2)));
                                }
                                #endregion

                                #region "Rejection Date"
                                if (fieldname.ToLower() == "rejectdt")
                                {
                                    if (cqMain.RejectDT != null)
                                        HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Rejection Date From", String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value1), String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + "}", value2)));
                                }
                                #endregion

                                #region "Source"
                                if (fieldname.ToLower() == "cqsource")
                                {
                                    if (cqMain.CQSource != null)
                                    {
                                        Int64 OldSourceID = 0;
                                        if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                            OldSourceID = Convert.ToInt64(value1.ToString());

                                        Int64 NewSourceID = 0;
                                        if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                            NewSourceID = Convert.ToInt64(value2.ToString());
                                        if (OldSourceID == 0)
                                        {
                                            value1 = "Vendor";
                                        }
                                        if (OldSourceID == 1)
                                        {
                                            value1 = "Inside";
                                        }
                                        if (OldSourceID == 2)
                                        {
                                            value1 = "Type";
                                        }
                                        if (NewSourceID == 0)
                                        {
                                            value2 = "Vendor";
                                        }
                                        if (NewSourceID == 1)
                                        {
                                            value2 = "Inside";
                                        }
                                        if (NewSourceID == 2)
                                        {
                                            value2 = "Type";
                                        }

                                        HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Source Changed From", value1, value2));
                                    }
                                }
                                #endregion

                                #region "fleet"
                                if (fieldname.ToLower() == "fleetid")
                                {
                                    Int64 OldFleetID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldFleetID = (Int64)value1;

                                    Int64 NewFleetID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewFleetID = (Int64)value2;
                                    Fleet OldFleet = Container.Fleets.Where(x => x.FleetID == OldFleetID).SingleOrDefault();
                                    Fleet NewFleet = Container.Fleets.Where(x => x.FleetID == NewFleetID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Tail Number",
                                        (OldFleet != null ? OldFleet.TailNum : string.Empty),
                                        (NewFleet != null ? NewFleet.TailNum : string.Empty))
                                        );
                                }
                                #endregion

                                #region "AircraftTypeCode"
                                if (fieldname.ToLower() == "aircraftid")
                                {
                                    Int64 OldAircraftID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldAircraftID = (Int64)value1;

                                    Int64 NewAircraftID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewAircraftID = (Int64)value2;
                                    Aircraft OldAircraft = Container.Aircraft.Where(x => x.AircraftID == OldAircraftID).SingleOrDefault();
                                    Aircraft NewAircraft = Container.Aircraft.Where(x => x.AircraftID == NewAircraftID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Aircraft Type",
                                        (OldAircraft != null ? OldAircraft.AircraftCD : string.Empty),
                                        (NewAircraft != null ? NewAircraft.AircraftCD : string.Empty)));

                                }
                                #endregion

                                #region "QuoteDetail"
                                if (fieldname.ToLower() == "quotetotal")
                                {
                                    HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Original Quote Total ", value1, value2));
                                }
                                #endregion


                                #region "DailyUsageForbidden"
                                if (fieldname.ToLower() == "isoverrideusageadj")
                                {
                                    if (cqMain.IsOverrideUsageAdj != null && (bool)cqMain.IsOverrideUsageAdj)
                                        HistoryDescription.AppendLine("Daily Usage Adjustment Overridden");
                                }
                                #endregion


                                #region "LandingFeeForbidden"
                                if (fieldname.ToLower() == "isoverridelandingfee")
                                {
                                    if (cqMain.IsOverrideLandingFee != null && (bool)cqMain.IsOverrideLandingFee)
                                        HistoryDescription.AppendLine("Landing Fee Overridden");
                                }
                                #endregion

                                #region "SegmentFeeForbidden"
                                if (fieldname.ToLower() == "isoverridesegmentfee")
                                {
                                    if (cqMain.IsOverrideSegmentFee != null && (bool)cqMain.IsOverrideSegmentFee)
                                        HistoryDescription.AppendLine("Segment Fee Overridden");
                                }
                                #endregion

                                #region "DiscountPercentage"
                                if (fieldname.ToLower() == "isoverridediscountpercentage")
                                {
                                    if (cqMain.IsOverrideDiscountPercentage != null && (bool)cqMain.IsOverrideDiscountPercentage)
                                        HistoryDescription.AppendLine("Discount Percent Overridden");
                                }
                                #endregion

                                #region "DiscountAmount"
                                if (fieldname.ToLower() == "isoverridediscountamt")
                                {
                                    if (cqMain.IsOverrideDiscountAMT != null && (bool)cqMain.IsOverrideDiscountAMT)
                                        HistoryDescription.AppendLine("Discount Amount Overridden");
                                }
                                #endregion

                                #region "Taxes"
                                if (fieldname.ToLower() == "isoverridetaxes")
                                {
                                    if (cqMain.IsOverrideTaxes != null && (bool)cqMain.IsOverrideTaxes)
                                        HistoryDescription.AppendLine("Taxes Overridden");
                                }
                                #endregion

                                #region "Vendor"
                                if (fieldname.ToLower() == "vendorid")
                                {
                                    Int64 OldVendorID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldVendorID = (Int64)value1;

                                    Int64 NewVendorID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewVendorID = (Int64)value2;
                                    Vendor OldVendorCode = Container.Vendors.Where(x => x.VendorID == OldVendorID).SingleOrDefault();
                                    Vendor NewVendorCode = Container.Vendors.Where(x => x.VendorID == NewVendorID).SingleOrDefault();

                                    HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Vendor Code From", OldVendorCode != null ? OldVendorCode.VendorCD : string.Empty, NewVendorCode != null ? NewVendorCode.VendorCD : string.Empty));
                                }
                                #endregion

                                #region "Federal"
                                if (fieldname.ToLower() == "federaltax")
                                {
                                    Account NewFederalCode = Container.Accounts.Where(x => x.AccountID == cqMain.FederalTax).SingleOrDefault();
                                    Account OldFederalCode = Container.Accounts.Where(x => x.AccountID == originalcqMain.FederalTax).SingleOrDefault();
                                    HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Fed./VAT From ", OldFederalCode != null ? OldFederalCode.AccountNum : string.Empty, NewFederalCode != null ? NewFederalCode.AccountNum : string.Empty));
                                }
                                #endregion

                                #region "Rural"
                                if (fieldname.ToLower() == "quoteruraltax")
                                {
                                    Account NewRuralCode = Container.Accounts.Where(x => x.AccountID == cqMain.QuoteRuralTax).SingleOrDefault();
                                    Account OldRuralCode = Container.Accounts.Where(x => x.AccountID == originalcqMain.QuoteRuralTax).SingleOrDefault();
                                    HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Rural Tax From", OldRuralCode != null ? OldRuralCode.AccountNum : string.Empty, NewRuralCode != null ? NewRuralCode.AccountNum : string.Empty));
                                }
                                #endregion

                                #region "TaxableQuote"
                                if (fieldname.ToLower() == "istaxable")
                                {
                                    if (cqMain.IsTaxable != null && (bool)cqMain.IsTaxable)
                                        HistoryDescription.AppendLine("Marked Quote As Taxable");
                                    else
                                        HistoryDescription.AppendLine("Marked Quote As Non Taxable");

                                }
                                #endregion

                                #region "Requestor"
                                if (fieldname.ToLower() == "passengerrequestorid")
                                {

                                    Int64 OldPassengerRequestorID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldPassengerRequestorID = (Int64)value1;

                                    Int64 NewPassengerRequestorID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewPassengerRequestorID = (Int64)value2;
                                    Passenger NewRequestorCode = Container.Passengers.Where(x => x.PassengerRequestorID == NewPassengerRequestorID).SingleOrDefault();
                                    Passenger OldRequestorCode = Container.Passengers.Where(x => x.PassengerRequestorID == OldPassengerRequestorID).SingleOrDefault();
                                    HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Requestor Code From", OldRequestorCode != null ? OldRequestorCode.PassengerRequestorCD : string.Empty, NewRequestorCode != null ? NewRequestorCode.PassengerRequestorCD : string.Empty));
                                }
                                #endregion

                                #region "Comments"
                                if (fieldname.ToLower() == "comments")
                                {
                                    HistoryDescription.AppendLine(string.Format("{0}  From {1} To {2}", "Changed Quote Comments ", value1, value2));
                                }
                                #endregion


                                #region "StandardCrewDOM"
                                if (fieldname.ToLower() == "standardcrewdom")
                                {
                                    HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Domestic Standard Crew From ", value1, value2));
                                }
                                #endregion

                                #region "StandardCrewIntl"
                                if (fieldname.ToLower() == "standardcrewintl")
                                {
                                    HistoryDescription.AppendLine(string.Format("{0}  {1} To {2}", "Changed International Standard Crew From ", value1, value2));
                                }
                                #endregion

                                #region "StandardCostTotal"
                                if (fieldname.ToLower() == "costtotal")
                                {
                                    HistoryDescription.AppendLine(string.Format("{0}  From {1} To {2}", "Changed Total Fixed Cost ", value1, value2));
                                }
                                #endregion

                                #region "StandardPrepayTotal"
                                if (fieldname.ToLower() == "prepaytotal")
                                {
                                    HistoryDescription.AppendLine(string.Format("{0}  From {1} To {2}", "Changed Total Prepaid/Deposit ", value1, value2));
                                }
                                #endregion

                                #region "FeeGroup"
                                if (fieldname.ToLower() == "feegroupid")
                                {

                                    Int64 OldFeeID = 0;
                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                        OldFeeID = (Int64)value1;

                                    Int64 NewFeeID = 0;
                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                        NewFeeID = (Int64)value2;
                                    FeeGroup OldFeeCode = Container.FeeGroups.Where(x => x.FeeGroupID == OldFeeID).SingleOrDefault();
                                    FeeGroup NewFeeCode = Container.FeeGroups.Where(x => x.FeeGroupID == NewFeeID).SingleOrDefault();
                                    HistoryDescription.AppendLine(string.Format("{0} {1} To {2}", "Changed Fee Group From", OldFeeCode != null ? OldFeeCode.FeeGroupCD : string.Empty, NewFeeCode != null ? NewFeeCode.FeeGroupCD : string.Empty));
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                    else if (cqMain.State == CQRequestEntityState.Deleted)
                    {
                        originalcqMain.LastUpdTS = DateTime.UtcNow;
                        originalcqMain.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CQMains.DeleteObject(originalcqMain);

                        #region "History For Deleted Quote"
                        HistoryDescription.AppendLine(string.Format("Quote {0} Deleted", originalcqMain.QuoteNUM));
                        #endregion
                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalcqMain, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    cqMain.IsDeleted = false;
                    cqMain.CustomerID = CustomerID;
                    if (cqFile.State != CQRequestEntityState.Added)
                        cqFileEntity.CQMains.Add(cqMain);
                    Container.ObjectStateManager.ChangeObjectState(cqMain, System.Data.EntityState.Added);

                    #region "History"
                    HistoryDescription.AppendLine("New Quote Added:");
                    if (cqMain.FleetID != null)
                    {
                        Fleet OldFleet = Container.Fleets.Where(x => x.FleetID == cqMain.FleetID).SingleOrDefault();
                        if (OldFleet.TailNum != null)
                            HistoryDescription.AppendLine(string.Format("{0} {1}", "Tail Number: ", OldFleet != null ? OldFleet.TailNum : string.Empty));
                    }
                    if (cqMain.QuoteTotal != null)
                        HistoryDescription.AppendLine(string.Format("{0} {1}", "Quote Total: ", cqMain.QuoteTotal));
                    #endregion
                }

                #region CQFleetChargeDetail

                foreach (var cqFleetChrDet in cqMain.CQFleetChargeDetails.ToList())
                {
                    cqFleetChrDet.LastUpdTS = DateTime.UtcNow;
                    cqFleetChrDet.LastUpdUID = UserPrincipal.Identity.Name;

                    // Is original child item with same ID in DB?    
                    CQFleetChargeDetail originalFltChrgDet = new CQFleetChargeDetail();

                    if (cqFleetChrDet.State != CQRequestEntityState.Added)
                    {
                        originalFltChrgDet = Container.CQFleetChargeDetails
                        .Where(c => c.CQFleetChargeDetailID == cqFleetChrDet.CQFleetChargeDetailID)
                        .SingleOrDefault();

                        // Yes -> Update scalar properties of child item 
                        if (cqFleetChrDet.State == CQRequestEntityState.Modified)
                        {
                            cqFleetChrDet.IsDeleted = false;
                            cqFleetChrDet.CustomerID = CustomerID;
                            Container.CQFleetChargeDetails.Attach(originalFltChrgDet);
                            Container.CQFleetChargeDetails.ApplyCurrentValues(cqFleetChrDet);

                            #region "History For Modified Fleet"
                            var originalFltChrgDetValues = Container.ObjectStateManager.GetObjectStateEntry(originalFltChrgDet).OriginalValues;
                            var CurrentFltChrgDetValues = Container.ObjectStateManager.GetObjectStateEntry(originalFltChrgDet).CurrentValues;
                            PropertyInfo[] properties = typeof(CQLeg).GetProperties();
                            for (int i = 0; i < originalFltChrgDetValues.FieldCount; i++)
                            {
                                var fieldname = originalFltChrgDetValues.GetName(i);

                                object value1 = originalFltChrgDetValues.GetValue(i);
                                object value2 = CurrentFltChrgDetValues.GetValue(i);

                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                {
                                    #region "Fee Description"
                                    if (fieldname.ToLower() == "cqflightchargedescription")
                                    {
                                        HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Fee Description", value1, value2));
                                    }
                                    #endregion

                                    #region "ChargeUnit"
                                    if (fieldname.ToLower() == "chargeunit")
                                    {
                                        HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Charge Unit of Fee - " + cqFleetChrDet.CQFlightChargeDescription, value1, value2));
                                    }
                                    #endregion

                                    #region "buydom"
                                    if (fieldname.ToLower() == "buydom")
                                    {
                                        decimal oldVal = 0;
                                        if (decimal.TryParse(value1.ToString(), out oldVal))
                                            oldVal = Math.Round(oldVal, 2);

                                        decimal newVal = 0;
                                        if (decimal.TryParse(value2.ToString(), out newVal))
                                            newVal = Math.Round(newVal, 2);

                                        if (newVal != oldVal)
                                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Domestic Fixed Costs/Buy of for Fee - " + cqFleetChrDet.CQFlightChargeDescription, oldVal.ToString(), newVal.ToString()));
                                    }
                                    #endregion

                                    #region "SellDOM"
                                    if (fieldname.ToLower() == "selldom")
                                    {
                                        decimal oldVal = 0;
                                        if (decimal.TryParse(value1.ToString(), out oldVal))
                                            oldVal = Math.Round(oldVal, 2);

                                        decimal newVal = 0;
                                        if (decimal.TryParse(value2.ToString(), out newVal))
                                            newVal = Math.Round(newVal, 2);

                                        if (newVal != oldVal)
                                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Domestic Sell of Fee -" + cqFleetChrDet.CQFlightChargeDescription, oldVal.ToString(), newVal.ToString()));
                                    }
                                    #endregion

                                    #region "IsTaxDOM"

                                    if (fieldname.ToLower() == "istaxdom")
                                    {

                                        HistoryDescription.AppendLine(string.Format("Marked Domestic Tax for Fee - {0} As {1} From {2}", cqFleetChrDet.CQFlightChargeDescription,

                                                ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Taxable" : "Non Taxable"),
                                                ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Taxable" : "Non Taxable")
                                                ));

                                    }
                                    #endregion

                                    #region "IsDiscountDOM"

                                    if (fieldname.ToLower() == "isdiscountdom")
                                    {

                                        HistoryDescription.AppendLine(string.Format("Marked Domestic Fee - {0} As {1} From {2}", cqFleetChrDet.CQFlightChargeDescription,

                                                ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Discount" : "Non Discount"),
                                                ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Discount" : "Non Discount")
                                                ));

                                    }
                                    #endregion

                                    #region "BuyIntl"
                                    if (fieldname.ToLower() == "buyintl")
                                    {
                                        decimal oldVal = 0;
                                        if (decimal.TryParse(value1.ToString(), out oldVal))
                                            oldVal = Math.Round(oldVal, 2);

                                        decimal newVal = 0;
                                        if (decimal.TryParse(value2.ToString(), out newVal))
                                            newVal = Math.Round(newVal, 2);

                                        if (newVal != oldVal)
                                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "International Fixed Costs/Buy of for Fee - " + cqFleetChrDet.CQFlightChargeDescription, oldVal.ToString(), newVal.ToString()));
                                    }
                                    #endregion

                                    #region "SellIntl"
                                    if (fieldname.ToLower() == "sellintl")
                                    {
                                        decimal oldVal = 0;
                                        if (decimal.TryParse(value1.ToString(), out oldVal))
                                            oldVal = Math.Round(oldVal, 2);

                                        decimal newVal = 0;
                                        if (decimal.TryParse(value2.ToString(), out newVal))
                                            newVal = Math.Round(newVal, 2);

                                        if (newVal != oldVal)
                                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "International Sell of Fee -" + cqFleetChrDet.CQFlightChargeDescription, oldVal.ToString(), newVal.ToString()));
                                    }
                                    #endregion

                                    #region "IsTaxIntl"

                                    if (fieldname.ToLower() == "istaxintl")
                                    {

                                        HistoryDescription.AppendLine(string.Format("Marked International Tax for Fee - {0} As {1} From {2}", cqFleetChrDet.CQFlightChargeDescription,

                                                ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Taxable" : "Non Taxable"),
                                                ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Taxable" : "Non Taxable")
                                                ));

                                    }
                                    #endregion

                                    #region "IsDiscountIntl"

                                    if (fieldname.ToLower() == "isdiscountintl")
                                    {

                                        HistoryDescription.AppendLine(string.Format("Marked International Fee - {0} As {1} From {2}", cqFleetChrDet.CQFlightChargeDescription,

                                                ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Discount" : "Non Discount"),
                                                ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Discount" : "Non Discount")
                                                ));

                                    }
                                    #endregion

                                    #region "Quantity"
                                    if (fieldname.ToLower() == "quantity")
                                    {
                                        decimal oldVal = 0;
                                        if (decimal.TryParse(value1.ToString(), out oldVal))
                                            oldVal = Math.Round(oldVal, 2);

                                        decimal newVal = 0;
                                        if (decimal.TryParse(value2.ToString(), out newVal))
                                            newVal = Math.Round(newVal, 2);

                                        if (newVal != oldVal)
                                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Quantity of for Fee - " + cqFleetChrDet.CQFlightChargeDescription, oldVal.ToString(), newVal.ToString()));
                                    }
                                    #endregion

                                    #region "FeeAMT"
                                    if (fieldname.ToLower() == "feeamt")
                                    {
                                        decimal oldVal = 0;
                                        if (decimal.TryParse(value1.ToString(), out oldVal))
                                            oldVal = Math.Round(oldVal, 2);

                                        decimal newVal = 0;
                                        if (decimal.TryParse(value2.ToString(), out newVal))
                                            newVal = Math.Round(newVal, 2);

                                        if (newVal != oldVal)
                                            HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Amount of for Fee - " + cqFleetChrDet.CQFlightChargeDescription, oldVal.ToString(), newVal.ToString()));
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                        else if (cqFleetChrDet.State == CQRequestEntityState.Deleted)
                        {
                            originalFltChrgDet.LastUpdTS = DateTime.UtcNow;
                            originalFltChrgDet.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.CQFleetChargeDetails.DeleteObject(originalFltChrgDet);

                            #region "History For Deleted FltChargeDet"
                            #endregion
                        }
                        else
                        {
                            Container.ObjectStateManager.ChangeObjectState(originalFltChrgDet, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        cqFleetChrDet.IsDeleted = false;
                        cqFleetChrDet.CustomerID = CustomerID;
                        if (cqMain.State != CQRequestEntityState.Added)
                            originalcqMain.CQFleetChargeDetails.Add(cqFleetChrDet);
                        Container.ObjectStateManager.ChangeObjectState(cqFleetChrDet, System.Data.EntityState.Added);

                        #region "History"
                        // Fix for #2242
                        if (cqMain.State == CQRequestEntityState.Modified)
                            HistoryDescription.AppendLine(string.Format("New Fee {0} With Charge Unit {1} As  Added", cqFleetChrDet.CQFlightChargeDescription, cqFleetChrDet.ChargeUnit));
                        #endregion
                    }
                }
                #endregion

                #region CQLeg
                foreach (var Leg in cqMain.CQLegs.ToList())
                {
                    Leg.LastUpdTS = DateTime.UtcNow;
                    Leg.LastUpdUID = UserPrincipal.Identity.Name;

                    // Is original child item with same ID in DB?    
                    CQLeg originalLeg = new CQLeg();

                    if (Leg.State != CQRequestEntityState.Added)
                    {
                        originalLeg = Container.CQLegs
                        .Where(c => c.CQLegID == Leg.CQLegID)
                        .SingleOrDefault();

                        // Yes -> Update scalar properties of child item 
                        if (Leg.State == CQRequestEntityState.Modified)
                        {
                            Leg.IsDeleted = false;
                            Leg.CustomerID = CustomerID;
                            Container.CQLegs.Attach(originalLeg);
                            Container.CQLegs.ApplyCurrentValues(Leg);

                            if (cqMain.State != CQRequestEntityState.Deleted)
                            {
                                #region "History For Modified Leg"
                                var originalLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).OriginalValues;
                                var CurrentLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).CurrentValues;
                                PropertyInfo[] properties = typeof(CQLeg).GetProperties();
                                for (int i = 0; i < originalLegValues.FieldCount; i++)
                                {
                                    var fieldname = originalLegValues.GetName(i);

                                    object value1 = originalLegValues.GetValue(i);
                                    object value2 = CurrentLegValues.GetValue(i);

                                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                    {
                                        #region "DepartIcaoID"

                                        if (fieldname.ToLower() == "dairportid")
                                        {
                                            Int64 OldDepartICAOID = 0;
                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                OldDepartICAOID = (Int64)value1;

                                            Int64 NewDepartICAOID = 0;
                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                NewDepartICAOID = (Int64)value2;
                                            Airport OldAiport = Container.Airports.Where(x => x.AirportID == OldDepartICAOID).SingleOrDefault();
                                            Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewDepartICAOID).SingleOrDefault();


                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Departs ICAO",
                                                (OldAiport != null ? OldAiport.IcaoID : string.Empty),
                                                (NewAirport != null ? NewAirport.IcaoID : string.Empty)));


                                        }

                                        #endregion

                                        #region "ArriveIcaoID"

                                        if (fieldname.ToLower() == "aairportid")
                                        {
                                            Int64 OldArriveICAOID = 0;
                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                OldArriveICAOID = (Int64)value1;

                                            Int64 NewArriveICAOID = 0;
                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                NewArriveICAOID = (Int64)value2;
                                            Airport OldAiport = Container.Airports.Where(x => x.AirportID == OldArriveICAOID).SingleOrDefault();
                                            Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewArriveICAOID).SingleOrDefault();

                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Arrives ICAO",
                                                (OldAiport != null ? OldAiport.IcaoID : string.Empty),
                                                (NewAirport != null ? NewAirport.IcaoID : string.Empty)));
                                        }

                                        #endregion

                                        #region "Departure Date Local"
                                        if (fieldname.ToLower() == "departuredttmlocal")
                                        {


                                            HistoryDescription.AppendLine(
                                                string.Format(
                                                "Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Departure Local Date/Time",
                                                (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1)) : string.Empty),
                                                (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2)) : string.Empty)
                                                ));

                                        }


                                        #endregion

                                        #region "Arrival Date Arrival"
                                        if (fieldname.ToLower() == "arrivaldttmlocal")
                                        {

                                            HistoryDescription.AppendLine(
                                                string.Format(
                                                "Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Arrival Local Date/Time",
                                                (!string.IsNullOrEmpty(value1.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value1)) : string.Empty),
                                                (!string.IsNullOrEmpty(value2.ToString()) ? (String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", value2)) : string.Empty)
                                                ));


                                        }

                                        #endregion

                                        #region "ETE"
                                        if (fieldname.ToLower() == "elapsetm")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "ETE", value1, value2));
                                        }
                                        #endregion

                                        #region "distance"
                                        if (fieldname.ToLower() == "distance")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Distance", value1, value2));
                                        }
                                        #endregion

                                        #region "RON"
                                        if (fieldname.ToLower() == "remainovernightcnt")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "RON", value1, value2));
                                        }
                                        #endregion

                                        #region "Day Room"
                                        if (fieldname.ToLower() == "dayroncnt")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Day Room", value1, value2));
                                        }
                                        #endregion

                                        #region "PassengerTotal"
                                        if (fieldname.ToLower() == "passengertotal")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "PAX Total", value1, value2));
                                        }
                                        #endregion

                                        #region "istaxable"

                                        if (fieldname.ToLower() == "istaxable")
                                        {
                                            if (Leg.IsTaxable != null && (bool)Leg.IsTaxable)
                                                HistoryDescription.AppendLine(string.Format("Leg {0} Marked as Taxable", originalLeg.LegNUM));
                                            else
                                                HistoryDescription.AppendLine(string.Format("Leg {0} Marked as Non Taxable", originalLeg.LegNUM));
                                        }
                                        #endregion

                                        #region "Tax Rate"
                                        if (fieldname.ToLower() == "taxrate")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Tax Rate", value1, value2));
                                        }
                                        #endregion

                                        #region "DutyTYPE"

                                        if (fieldname.ToLower() == "dutytype")
                                        {
                                            if (Leg.DutyTYPE != null || Leg.DutyTYPE == 1)
                                                HistoryDescription.AppendLine(string.Format("Leg {0} Marked as Domestic", originalLeg.LegNUM));
                                            else
                                                HistoryDescription.AppendLine(string.Format("Leg {0} Marked as International", originalLeg.LegNUM));
                                        }
                                        #endregion

                                        #region "istaxable"

                                        if (fieldname.ToLower() == "ispositioning")
                                        {
                                            if (Leg.IsPositioning != null && (bool)Leg.IsPositioning)
                                                HistoryDescription.AppendLine(string.Format("Leg {0} Marked as Positioning Leg", originalLeg.LegNUM));
                                            else
                                                HistoryDescription.AppendLine(string.Format("Leg {0} Marked as Non Positioning Leg", originalLeg.LegNUM));
                                        }
                                        #endregion

                                        #region "flightpurposedescription"

                                        if (fieldname.ToLower() == "flightpurposedescription")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Purpose", value1, value2));
                                        }
                                        #endregion

                                        #region "powersetting"

                                        if (fieldname.ToLower() == "powersetting")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Power Set", value1, value2));
                                        }
                                        #endregion

                                        #region "LandingBIAS"

                                        if (fieldname.ToLower() == "landingbias")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Landing BIAS", value1, value2));
                                        }
                                        #endregion

                                        #region "TakeoffBIAS"

                                        if (fieldname.ToLower() == "takeoffbias")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Takeoff BIAS", value1, value2));
                                        }
                                        #endregion

                                        #region "powersetting"

                                        if (fieldname.ToLower() == "trueairspeed")
                                        {
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "True Air Speed", value1, value2));
                                        }
                                        #endregion

                                        #region "windreliability"

                                        if (fieldname.ToLower() == "windreliability")
                                        {

                                            switch (value1.ToString())
                                            {
                                                case "1": value1 = "50%"; break;
                                                case "2": value1 = "75%"; break;
                                                case "3": value1 = "85%"; break;
                                            }

                                            switch (value2.ToString())
                                            {
                                                case "1": value2 = "50%"; break;
                                                case "2": value2 = "75%"; break;
                                                case "3": value2 = "85%"; break;
                                            }
                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "wind reliability", value1, value2));
                                        }
                                        #endregion

                                        #region "winds"

                                        if (fieldname.ToLower() == "windsboeingtable")
                                        {

                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Winds", value1, value2));
                                        }
                                        #endregion

                                        #region "Override"

                                        if (fieldname.ToLower() == "cqoverride")
                                        {

                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Override", value1, value2));

                                        }
                                        #endregion

                                        #region "CrewDutyRule"

                                        if (fieldname.ToLower() == "crewdutyrulesid")
                                        {
                                            Int64 OldCrewDutyRulesID = 0;
                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                OldCrewDutyRulesID = (Int64)value1;

                                            Int64 NewCrewDutyRulesID = 0;
                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                NewCrewDutyRulesID = (Int64)value2;
                                            CrewDutyRule OldCrewDutyRule = Container.CrewDutyRules.Where(x => x.CrewDutyRulesID == OldCrewDutyRulesID).SingleOrDefault();
                                            CrewDutyRule NewCrewDutyRule = Container.CrewDutyRules.Where(x => x.CrewDutyRulesID == NewCrewDutyRulesID).SingleOrDefault();


                                            HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Crew Rule",
                                                (OldCrewDutyRule != null ? OldCrewDutyRule.CrewDutyRuleCD : string.Empty),
                                                (NewCrewDutyRule != null ? NewCrewDutyRule.CrewDutyRuleCD : string.Empty)));


                                        }

                                        #endregion

                                        #region "PAX Notes"
                                        if (fieldname.ToLower() == "notes")
                                        {
                                            string OldpaxNotes = string.Empty;
                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                OldpaxNotes = value1.ToString();

                                            string NewpaxNotes = string.Empty;
                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                NewpaxNotes = value2.ToString();

                                            if (!string.IsNullOrEmpty(OldpaxNotes) && !string.IsNullOrEmpty(NewpaxNotes))
                                                HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "PAX Notes", value1, value2));

                                            if (string.IsNullOrEmpty(OldpaxNotes) && !string.IsNullOrEmpty(NewpaxNotes))
                                                HistoryDescription.AppendLine(string.Format("Leg {0} {1} is Added", originalLeg.LegNUM, "PAX Notes", value2));
                                        }

                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                        else if (Leg.State == CQRequestEntityState.Deleted)
                        {
                            originalLeg.LastUpdTS = DateTime.UtcNow;
                            originalLeg.LastUpdUID = UserPrincipal.Identity.Name;
                            Container.CQLegs.DeleteObject(originalLeg);

                            if (cqMain.State != CQRequestEntityState.Deleted)
                            {
                                #region "History For Deleted Leg"
                                HistoryDescription.AppendLine(string.Format("Leg {0} Deleted", originalLeg.LegNUM));
                                #endregion
                            }
                        }
                        else
                        {
                            Container.ObjectStateManager.ChangeObjectState(originalLeg, System.Data.EntityState.Unchanged);
                        }
                    }
                    else
                    {
                        // No -> It's a new child item -> Insert             
                        Leg.IsDeleted = false;
                        Leg.CustomerID = CustomerID;
                        if (cqMain.State != CQRequestEntityState.Added)
                            originalcqMain.CQLegs.Add(Leg);
                        Container.ObjectStateManager.ChangeObjectState(Leg, System.Data.EntityState.Added);

                        // Fix for #2242
                        if (cqMain.State == CQRequestEntityState.Modified)
                        {
                            #region "History"
                            Airport DepAiport = new Airport();
                            Airport ArrAirport = new Airport();
                            if (Leg.DAirportID != null)
                                DepAiport = Container.Airports.Where(x => x.AirportID == (Int64)Leg.DAirportID).SingleOrDefault();
                            if (Leg.AAirportID != null)
                                ArrAirport = Container.Airports.Where(x => x.AirportID == (Int64)Leg.AAirportID).SingleOrDefault();

                            if (DepAiport != null && ArrAirport != null)
                                HistoryDescription.AppendLine(string.Format("New Leg {0} {1} - {2} Added", Leg.LegNUM, DepAiport.IcaoID, ArrAirport.IcaoID));

                            #endregion
                        }
                    }

                    #region "PAX"

                    bool blPaxdelete = false;
                    if (Leg.State != CQRequestEntityState.Deleted)
                    {
                        #region "History All PAX deleted"

                        if (cqMain.CQLegs != null && cqMain.CQLegs.Count > 0)
                        {
                            foreach (CQLeg cqLeg in cqMain.CQLegs)
                            {
                                if (cqLeg.CQPassengers != null && cqLeg.CQPassengers.Count > 0)
                                {
                                    List<CQPassenger> availpax = new List<CQPassenger>();
                                    availpax = cqLeg.CQPassengers.Where(x => x.IsDeleted == false).ToList();

                                    if (availpax == null || availpax.Count == 0)
                                    {
                                        blPaxdelete = true;
                                        HistoryDescription.AppendLine(string.Format("All Passenger Deleted for Leg: {0}", cqLeg.LegNUM));
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    foreach (CQPassenger paxList in Leg.CQPassengers.ToList())
                    {
                        CQPassenger OriginalPaxlist = new CQPassenger();
                        paxList.LastUpdTS = DateTime.UtcNow;
                        paxList.LastUpdUID = UserPrincipal.Identity.Name;
                        if (paxList.State != CQRequestEntityState.Added)
                        {
                            OriginalPaxlist = Container.CQPassengers
                           .Where(c => c.CQPassengerID == paxList.CQPassengerID)
                           .SingleOrDefault();


                            // Is original child item with same ID in DB?   
                            if (paxList.State == CQRequestEntityState.Modified)
                            {
                                paxList.IsDeleted = false;
                                paxList.CustomerID = CustomerID;
                                Container.CQPassengers.Attach(OriginalPaxlist);
                                Container.CQPassengers.ApplyCurrentValues(paxList);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Modified Pax"
                                    var originalPaxValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalPaxlist).OriginalValues;
                                    var CurrentPaxValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalPaxlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(CQPassenger).GetProperties();
                                    for (int i = 0; i < originalPaxValues.FieldCount; i++)
                                    {
                                        var fieldname = originalPaxValues.GetName(i);

                                        object value1 = originalPaxValues.GetValue(i);
                                        object value2 = CurrentPaxValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region "Purpose"
                                            if (fieldname.ToLower() == "flightpurposeid")
                                            {
                                                if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                {
                                                    FlightPurpose OldPurpose = new FlightPurpose();
                                                    OldPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)value1).SingleOrDefault();
                                                    FlightPurpose NewPurpose = new FlightPurpose();
                                                    NewPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)value2).SingleOrDefault();

                                                    if (!string.IsNullOrEmpty(OriginalPaxlist.FlightPurposeID.ToString()))
                                                        HistoryDescription.AppendLine(string.Format("Flight Purpose for Passenger {0} on Leg {1} changed from {2} to {3} ", OriginalPaxlist.PassengerName, Leg.LegNUM, OldPurpose.FlightPurposeCD, NewPurpose.FlightPurposeCD + "."));
                                                }

                                            }
                                            #endregion

                                        }
                                    }

                                    #endregion
                                }
                            }
                            else if (paxList.State == CQRequestEntityState.Deleted)
                            {
                                OriginalPaxlist.LastUpdTS = DateTime.UtcNow;
                                OriginalPaxlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.CQPassengers.DeleteObject(OriginalPaxlist);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Deleted Pax"
                                    if (!blPaxdelete)
                                        HistoryDescription.AppendLine(string.Format("Passenger {0}  in Leg {1}", OriginalPaxlist.PassengerName, Leg.LegNUM + " Deleted!"));
                                    #endregion
                                }
                            }

                        }
                        else
                        {
                            // No -> It's a new child item -> Insert             
                            paxList.IsDeleted = false;
                            paxList.CustomerID = CustomerID;
                            if (Leg.State != CQRequestEntityState.Added)
                                originalLeg.CQPassengers.Add(paxList);
                            Container.ObjectStateManager.ChangeObjectState(paxList, System.Data.EntityState.Added);

                            // Fix for #2242
                            if (paxList.State == CQRequestEntityState.Added && Leg.State == CQRequestEntityState.Modified)
                            {
                                #region "History For Added Pax"
                                FlightPurpose fpPurpose = new FlightPurpose();
                                fpPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == (Int64)paxList.FlightPurposeID).SingleOrDefault();
                                if (fpPurpose != null)
                                    HistoryDescription.AppendLine(string.Format("New Passenger(s) Added to Trip Leg: {0} Name: {1} Purpose: {2}", Leg.LegNUM, paxList.PassengerName, fpPurpose.FlightPurposeCD));
                                #endregion
                            }
                        }
                    }
                    #endregion

                    #region "LOGISTICS"

                    #region "FBO"
                    foreach (CQFBOList fboList in Leg.CQFBOLists.ToList())
                    {
                        fboList.LastUpdTS = DateTime.UtcNow;
                        fboList.LastUpdUID = UserPrincipal.Identity.Name;
                        fboList.CustomerID = Leg.CustomerID;
                        if (fboList.State != CQRequestEntityState.Added)
                        {
                            var OriginalFBOlist = Container.CQFBOLists
                            .Where(c => c.CQFBOListID == fboList.CQFBOListID)
                            .SingleOrDefault();

                            if (fboList.State == CQRequestEntityState.Modified)
                            {
                                fboList.IsDeleted = false;
                                fboList.CQLegID = Leg.CQLegID;
                                Container.CQFBOLists.Attach(OriginalFBOlist);
                                Container.CQFBOLists.ApplyCurrentValues(fboList);
                                Container.ObjectStateManager.ChangeObjectState(OriginalFBOlist, System.Data.EntityState.Modified);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Modified FBO"

                                    var originalFBOValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalFBOlist).OriginalValues;
                                    var CurrentFboValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalFBOlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(CQFBOList).GetProperties();
                                    for (int i = 0; i < originalFBOValues.FieldCount; i++)
                                    {
                                        var fieldname = originalFBOValues.GetName(i);

                                        object value1 = originalFBOValues.GetValue(i);
                                        object value2 = CurrentFboValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            if (fieldname.ToLower() == "fboid")
                                            {
                                                #region Depart
                                                if (fboList.RecordType == "FD")
                                                {
                                                    Int64 OldDepartFBOID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDepartFBOID = (Int64)value1;

                                                    Int64 NewDepartFBOID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDepartFBOID = (Int64)value2;
                                                    FBO OldFBO = Container.FBOes.Where(x => x.FBOID == OldDepartFBOID).SingleOrDefault();
                                                    FBO NewFBO = Container.FBOes.Where(x => x.FBOID == NewDepartFBOID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Departure FBO {0} is changed to {1} for Leg {2} ", OldFBO.FBOVendor != null ? OldFBO.FBOVendor : string.Empty, NewFBO.FBOVendor != null ? NewFBO.FBOVendor : string.Empty, Leg.LegNUM)); // #2243
                                                }
                                                #endregion

                                                #region Arrive
                                                if (fboList.RecordType == "FA")
                                                {
                                                    Int64 OldArriveFBOID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldArriveFBOID = (Int64)value1;

                                                    Int64 NewArriveFBOID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewArriveFBOID = (Int64)value2;
                                                    FBO OldFBO = Container.FBOes.Where(x => x.FBOID == OldArriveFBOID).SingleOrDefault();
                                                    FBO NewFBO = Container.FBOes.Where(x => x.FBOID == NewArriveFBOID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is changed to {1} for Leg {2} ", OldFBO.FBOVendor != null ? OldFBO.FBOVendor : string.Empty, NewFBO.FBOVendor != null ? NewFBO.FBOVendor : string.Empty, Leg.LegNUM));
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else if (fboList.State == CQRequestEntityState.Deleted)
                            {
                                OriginalFBOlist.LastUpdTS = DateTime.UtcNow;
                                OriginalFBOlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.CQFBOLists.DeleteObject(OriginalFBOlist);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History for Deleted FBO"
                                    if (OriginalFBOlist.RecordType == "FD")
                                        HistoryDescription.AppendLine(string.Format("Departure FBO for Leg{0} Deleted", Leg.LegNUM));
                                    if (OriginalFBOlist.RecordType == "FA")
                                        HistoryDescription.AppendLine(string.Format("Arrival FBO for Leg{0} Deleted", Leg.LegNUM));
                                    #endregion
                                }
                            }
                            else
                            {
                                Container.ObjectStateManager.ChangeObjectState(OriginalFBOlist, System.Data.EntityState.Unchanged);
                            }
                        }
                        else
                        {
                            // No -> It's a new child item -> Insert             
                            fboList.IsDeleted = false;
                            fboList.CQLegID = Leg.CQLegID;
                            if (Leg.State != CQRequestEntityState.Added)
                                originalLeg.CQFBOLists.Add(fboList);
                            Container.ObjectStateManager.ChangeObjectState(fboList, System.Data.EntityState.Added);

                            // Fix for #2242
                            if (fboList.State == CQRequestEntityState.Added && (Leg.State == CQRequestEntityState.Added || Leg.State == CQRequestEntityState.Modified))
                            {
                                #region "History for Added FBO"

                                //FBO fbo = new FBO();
                                //FBO oldfbo = new FBO();
                                //FBO newfbo = new FBO();

                                //if (originalLeg != null)
                                //{
                                if (fboList.RecordType == "FD")
                                {
                                    #region departure
                                    FBO departFBO = new FBO();
                                    departFBO = Container.FBOes.Where(x => x.FBOID == (Int64)fboList.FBOID).SingleOrDefault();
                                    if (departFBO != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", departFBO.FBOVendor != null ? departFBO.FBOVendor : string.Empty, Leg.LegNUM)); // #2243
                                    }

                                    //CQFBOList olddepFbo = new CQFBOList();
                                    //CQFBOList newdepFbo = new CQFBOList();
                                    //olddepFbo = originalLeg.CQFBOLists.Where(x => x.RecordType == "FD" && x.IsDeleted == false).FirstOrDefault();
                                    //newdepFbo = Leg.CQFBOLists.Where(x => x.RecordType == "FD" && x.IsDeleted == false).FirstOrDefault();

                                    //if (olddepFbo == null && newdepFbo != null)
                                    //{
                                    //    if (newdepFbo.FBOID != null)
                                    //    {
                                    //        fbo = Container.FBOes.Where(x => x.FBOID == (Int64)newdepFbo.FBOID).SingleOrDefault();
                                    //        if (fbo != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", fbo.FBOCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}

                                    #endregion
                                }
                                if (fboList.RecordType == "FA")
                                {
                                    #region arrival
                                    FBO arrivalFBO = new FBO();
                                    arrivalFBO = Container.FBOes.Where(x => x.FBOID == (Int64)fboList.FBOID).SingleOrDefault();
                                    if (arrivalFBO != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", arrivalFBO.FBOVendor != null ? arrivalFBO.FBOVendor : string.Empty, Leg.LegNUM));
                                    }

                                    //CQFBOList oldarrFbo = new CQFBOList();
                                    //CQFBOList newarrFbo = new CQFBOList();
                                    //oldarrFbo = originalLeg.CQFBOLists.Where(x => x.RecordType == "FA" && x.IsDeleted == false).FirstOrDefault();
                                    //newarrFbo = Leg.CQFBOLists.Where(x => x.RecordType == "FA" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldarrFbo == null && newarrFbo != null)
                                    //{
                                    //    if (newarrFbo.FBOID != null)
                                    //    {
                                    //        fbo = Container.FBOes.Where(x => x.FBOID == (Int64)newarrFbo.FBOID).SingleOrDefault();
                                    //        if (fbo != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", fbo.FBOCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}

                                    #endregion
                                }
                                //}
                                //else
                                //{
                                //    if (fboList.RecordType == "FD")
                                //    {
                                //        #region departure

                                //        newfbo = Container.FBOes.Where(x => x.FBOID == (Int64)fboList.FBOID).SingleOrDefault();
                                //        if (newfbo != null)
                                //        {
                                //            HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", newfbo.FBOCD, Leg.LegNUM));
                                //        }
                                //        //CQFBOList newdepFbo = new CQFBOList();
                                //        //newdepFbo = Leg.CQFBOLists.Where(x => x.RecordType == "FD" && x.IsDeleted == false).FirstOrDefault();

                                //        //if (newdepFbo != null)
                                //        //{
                                //        //    if (newdepFbo.FBOID != null)
                                //        //    {
                                //        //        newfbo = Container.FBOes.Where(x => x.FBOID == (Int64)newdepFbo.FBOID).SingleOrDefault();
                                //        //        if (newfbo != null)
                                //        //        {
                                //        //            HistoryDescription.AppendLine(string.Format("Departure FBO {0} is added to Leg {1} ", newfbo.FBOCD, Leg.LegNUM));
                                //        //        }
                                //        //    }
                                //        //}

                                //        #endregion
                                //    }
                                //    if (fboList.RecordType == "FA")
                                //    {
                                //        #region arrival
                                //        FBO newarrfbo = new FBO();

                                //        newarrfbo = Container.FBOes.Where(x => x.FBOID == (Int64)fboList.FBOID).SingleOrDefault();
                                //        if (newarrfbo != null)
                                //        {
                                //            HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", newarrfbo.FBOCD, Leg.LegNUM));
                                //        }

                                //        //CQFBOList newarrFbo = new CQFBOList();
                                //        //newarrFbo = Leg.CQFBOLists.Where(x => x.RecordType == "FA" && x.IsDeleted == false).FirstOrDefault();

                                //        //FBO newarrfbo = new FBO();

                                //        //if (newarrFbo != null)
                                //        //{
                                //        //    if (newarrFbo.FBOID != null)
                                //        //    {
                                //        //        newarrfbo = Container.FBOes.Where(x => x.FBOID == (Int64)newarrFbo.FBOID).SingleOrDefault();
                                //        //        if (newarrfbo != null)
                                //        //        {
                                //        //            HistoryDescription.AppendLine(string.Format("Arrival FBO {0} is added to Leg {1} ", newarrfbo.FBOCD, Leg.LegNUM));
                                //        //        }
                                //        //    }
                                //        //}

                                //        #endregion
                                //    }
                                //}
                                #endregion
                            }
                        }
                    }
                    #endregion

                    #region "Transport"
                    foreach (CQTransportList transportList in Leg.CQTransportLists.ToList())
                    {
                        transportList.CustomerID = Leg.CustomerID.Value;
                        transportList.LastUpdTS = DateTime.UtcNow;
                        transportList.LastUpdUID = UserPrincipal.Identity.Name;
                        if (transportList.State != CQRequestEntityState.Added)
                        {
                            var OriginalTransportlist = Container.CQTransportLists
                            .Where(c => c.CQTransportListID == transportList.CQTransportListID)
                            .SingleOrDefault();

                            if (transportList.State == CQRequestEntityState.Modified)
                            {
                                transportList.IsDeleted = false;
                                transportList.CQLegID = Leg.CQLegID;
                                Container.CQTransportLists.Attach(OriginalTransportlist);
                                Container.CQTransportLists.ApplyCurrentValues(transportList);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Modified Transport"
                                    var originalTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).OriginalValues;
                                    var CurrentTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalTransportlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(CQTransportList).GetProperties();
                                    for (int i = 0; i < originalTransportValues.FieldCount; i++)
                                    {
                                        var fieldname = originalTransportValues.GetName(i);

                                        object value1 = originalTransportValues.GetValue(i);
                                        object value2 = CurrentTransportValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region Pax
                                            if (transportList.RecordType == "TP")
                                            {
                                                #region "cqtransportlistdescription"
                                                if (fieldname.ToLower() == "cqtransportlistdescription")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passenger Transport Name {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "PhoneNUM"
                                                if (fieldname.ToLower() == "phonenum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passenger Transport Phone {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "FaxNUM"
                                                if (fieldname.ToLower() == "faxnum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passenger Transport Fax {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "Email"
                                                if (fieldname.ToLower() == "email")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passenger Transport Email {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion


                                                //#region "Rate"
                                                //if (fieldname.ToLower() == "rate")
                                                //{
                                                //    HistoryDescription.AppendLine(string.Format("Passenger Transport Rate {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                //}
                                                //#endregion



                                                #region "TransportID"
                                                if (fieldname.ToLower() == "transportid")
                                                {

                                                    Int64 OldPaxTransportID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldPaxTransportID = (Int64)value1;

                                                    Int64 NewPaxTransportID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewPaxTransportID = (Int64)value2;
                                                    Transport OldTransport = Container.Transports.Where(x => x.TransportID == OldPaxTransportID).SingleOrDefault();
                                                    Transport NewTransport = Container.Transports.Where(x => x.TransportID == NewPaxTransportID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Passenger Transport Code {0} is changed to {1} for Leg {2} ", OldTransport.TransportCD, NewTransport.TransportCD, Leg.LegNUM));
                                                }
                                                #endregion

                                            }
                                            #endregion
                                            #region Crew
                                            if (transportList.RecordType == "TC")
                                            {

                                                #region "cqtransportlistdescription"
                                                if (fieldname.ToLower() == "cqtransportlistdescription")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Crew Transport Name {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "PhoneNUM"
                                                if (fieldname.ToLower() == "phonenum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Crew Transport Phone {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "FaxNUM"
                                                if (fieldname.ToLower() == "faxnum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Crew Transport Fax {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "Email"
                                                if (fieldname.ToLower() == "email")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Crew Transport Email {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion


                                                //#region "Rate"
                                                //if (fieldname.ToLower() == "rate")
                                                //{
                                                //    HistoryDescription.AppendLine(string.Format("Crew Transport Rate {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                //}
                                                //#endregion

                                                #region "TransportID"
                                                if (fieldname.ToLower() == "transportid")
                                                {


                                                    Int64 OldCrewTransportID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldCrewTransportID = (Int64)value1;

                                                    Int64 NewCrewTransportID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewCrewTransportID = (Int64)value2;
                                                    Transport OldTransport = Container.Transports.Where(x => x.TransportID == OldCrewTransportID).SingleOrDefault();
                                                    Transport NewTransport = Container.Transports.Where(x => x.TransportID == NewCrewTransportID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Crew Transport {0} is changed to {1} for Leg {2} ", OldTransport.TransportCD, NewTransport.TransportCD, Leg.LegNUM));
                                                }
                                                #endregion

                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else if (transportList.State == CQRequestEntityState.Deleted)
                            {
                                OriginalTransportlist.LastUpdUID = UserPrincipal.Identity.Name;
                                OriginalTransportlist.LastUpdTS = DateTime.UtcNow;
                                Container.CQTransportLists.DeleteObject(OriginalTransportlist);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Deleted  Transport"
                                    if (OriginalTransportlist.RecordType == "TP")
                                        HistoryDescription.AppendLine(string.Format("Passenger Transport {1} for Leg{0} Deleted", Leg.LegNUM, OriginalTransportlist.CQTransportListDescription));
                                    if (OriginalTransportlist.RecordType == "TC")
                                        HistoryDescription.AppendLine(string.Format("Passenger Transport {1} for Leg{0} Deleted", Leg.LegNUM, OriginalTransportlist.CQTransportListDescription));
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            // No -> It's a new child item -> Insert             
                            transportList.IsDeleted = false;
                            transportList.CQLegID = Leg.CQLegID;
                            if (Leg.State != CQRequestEntityState.Added)
                                originalLeg.CQTransportLists.Add(transportList);
                            Container.ObjectStateManager.ChangeObjectState(transportList, System.Data.EntityState.Added);

                            // Fix for #2242
                            if (transportList.State == CQRequestEntityState.Added && (Leg.State == CQRequestEntityState.Added || Leg.State == CQRequestEntityState.Modified))
                            {
                                #region "History for Added Transport"

                                //Transport transport = new Transport();
                                //Transport oldTrans = new Transport();
                                //Transport newTrans = new Transport();

                                //if (originalLeg != null)
                                //{
                                if (transportList.RecordType == "TP")
                                {
                                    #region pax
                                    Transport paxTransport = new Transport();
                                    paxTransport = Container.Transports.Where(x => x.TransportID == (Int64)transportList.TransportID).SingleOrDefault();
                                    if (paxTransport != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Passenger Transport Code {0} is added to Leg {1} ", paxTransport.TransportCD, Leg.LegNUM));
                                        HistoryDescription.AppendLine(string.Format("Passenger Transport Name {0} is added to Leg {1} ", transportList.CQTransportListDescription, Leg.LegNUM));
                                    }

                                    //CQTransportList oldpaxTrans = new CQTransportList();
                                    //CQTransportList newpaxTrans = new CQTransportList();
                                    //oldpaxTrans = originalLeg.CQTransportLists.Where(x => x.RecordType == "TP" && x.IsDeleted == false).FirstOrDefault();
                                    //newpaxTrans = Leg.CQTransportLists.Where(x => x.RecordType == "TP" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldpaxTrans == null && newpaxTrans != null)
                                    //{
                                    //    if (newpaxTrans.TransportID != null)
                                    //    {
                                    //        transport = Container.Transports.Where(x => x.TransportID == (Int64)newpaxTrans.TransportID).SingleOrDefault();
                                    //        if (transport != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Passenger Transport {0} is added to Leg {1} ", transport.TransportCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}
                                    #endregion
                                }
                                if (transportList.RecordType == "TC")
                                {
                                    #region crew
                                    Transport crewTransport = new Transport();
                                    crewTransport = Container.Transports.Where(x => x.TransportID == (Int64)transportList.TransportID).SingleOrDefault();
                                    if (crewTransport != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Crew Transport Code {0} is added to Leg {1} ", crewTransport.TransportCD, Leg.LegNUM));
                                        HistoryDescription.AppendLine(string.Format("Crew Transport Name {0} is added to Leg {1} ", transportList.CQTransportListDescription, Leg.LegNUM));
                                    }
                                    //CQTransportList oldcrewTrans = new CQTransportList();
                                    //CQTransportList newcrewTrans = new CQTransportList();
                                    //oldcrewTrans = originalLeg.CQTransportLists.Where(x => x.RecordType == "TC" && x.IsDeleted == false).FirstOrDefault();
                                    //newcrewTrans = Leg.CQTransportLists.Where(x => x.RecordType == "TC" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldcrewTrans == null && newcrewTrans != null)
                                    //{
                                    //    if (newcrewTrans.TransportID != null)
                                    //    {
                                    //        transport = Container.Transports.Where(x => x.TransportID == (Int64)newcrewTrans.TransportID).SingleOrDefault();
                                    //        if (transport != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Crew Transport {0} is added to Leg {1} ", transport.TransportCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}
                                    #endregion
                                }
                                //}
                                //else
                                //{
                                //    if (transportList.RecordType == "TP")
                                //    {
                                //        #region pax
                                //        CQTransportList newpaxTrans = new CQTransportList();
                                //        newpaxTrans = Leg.CQTransportLists.Where(x => x.RecordType == "TP" && x.IsDeleted == false).FirstOrDefault();

                                //        Transport newtransport = new Transport();
                                //        if (newpaxTrans != null)
                                //        {
                                //            if (newpaxTrans.TransportID != null)
                                //            {
                                //                newtransport = Container.Transports.Where(x => x.TransportID == (Int64)newpaxTrans.TransportID).SingleOrDefault();
                                //                if (newtransport != null)
                                //                {
                                //                    HistoryDescription.AppendLine(string.Format("Passenger Transport {0} is added to Leg {1} ", newtransport.TransportCD, Leg.LegNUM));
                                //                }
                                //            }
                                //        }
                                //        #endregion
                                //    }
                                //    if (transportList.RecordType == "TC")
                                //    {
                                //        #region crew
                                //        CQTransportList newcrewTrans = new CQTransportList();
                                //        newcrewTrans = Leg.CQTransportLists.Where(x => x.RecordType == "TC" && x.IsDeleted == false).FirstOrDefault();

                                //        Transport newcrewTransport = new Transport();
                                //        if (newcrewTrans != null)
                                //        {
                                //            if (newcrewTrans.TransportID != null)
                                //            {
                                //                newcrewTransport = Container.Transports.Where(x => x.TransportID == (Int64)newcrewTrans.TransportID).SingleOrDefault();
                                //                if (newcrewTransport != null)
                                //                {
                                //                    HistoryDescription.AppendLine(string.Format("Crew Transport {0} is added to Leg {1} ", newcrewTransport.TransportCD, Leg.LegNUM));
                                //                }
                                //            }
                                //        }
                                //        #endregion
                                //    }
                                //}
                                #endregion
                            }
                        }
                    }
                    #endregion

                    #region "Hotel"
                    foreach (CQHotelList cqHotelList in Leg.CQHotelLists.ToList())
                    {
                        cqHotelList.CustomerID = Leg.CustomerID.Value;
                        cqHotelList.LastUpdTS = DateTime.UtcNow;
                        cqHotelList.LastUpdUID = UserPrincipal.Identity.Name;
                        if (cqHotelList.State != CQRequestEntityState.Added)
                        {
                            var OriginalHotelList = Container.CQHotelLists
                            .Where(c => c.CQHotelListID == cqHotelList.CQHotelListID)
                            .SingleOrDefault();

                            if (cqHotelList.State == CQRequestEntityState.Modified)
                            {
                                cqHotelList.IsDeleted = false;
                                cqHotelList.CQLegID = Leg.CQLegID;
                                Container.CQHotelLists.Attach(OriginalHotelList);
                                Container.CQHotelLists.ApplyCurrentValues(cqHotelList);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Modified Hotel"
                                    var originalTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotelList).OriginalValues;
                                    var CurrentTransportValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalHotelList).CurrentValues;
                                    PropertyInfo[] properties = typeof(CQTransportList).GetProperties();
                                    for (int i = 0; i < originalTransportValues.FieldCount; i++)
                                    {
                                        var fieldname = originalTransportValues.GetName(i);

                                        object value1 = originalTransportValues.GetValue(i);
                                        object value2 = CurrentTransportValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            #region Pax
                                            if (cqHotelList.RecordType == "HP")
                                            {

                                                #region "cqhotellistdescription"
                                                if (fieldname.ToLower() == "cqhotellistdescription")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Name {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "PhoneNUM"
                                                if (fieldname.ToLower() == "phonenum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Phone {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "FaxNUM"
                                                if (fieldname.ToLower() == "faxnum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Fax {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "Email"
                                                if (fieldname.ToLower() == "email")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Email {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion


                                                if (fieldname.ToLower() == "hotelid")
                                                {

                                                    Int64 OldPaxHotelID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldPaxHotelID = (Int64)value1;

                                                    Int64 NewPaxHotelID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewPaxHotelID = (Int64)value2;
                                                    Hotel OldHotel = Container.Hotels.Where(x => x.HotelID == OldPaxHotelID).SingleOrDefault();
                                                    Hotel NewHotel = Container.Hotels.Where(x => x.HotelID == NewPaxHotelID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel {0} is changed to {1} for Leg {2} ", OldHotel.HotelCD, NewHotel.HotelCD, Leg.LegNUM));
                                                }
                                            }
                                            #endregion

                                            #region Crew
                                            if (cqHotelList.RecordType == "HC")
                                            {

                                                #region "cqhotellistdescription"
                                                if (fieldname.ToLower() == "cqhotellistdescription")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Name {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "PhoneNUM"
                                                if (fieldname.ToLower() == "phonenum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Phone {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "FaxNUM"
                                                if (fieldname.ToLower() == "faxnum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Fax {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "Email"
                                                if (fieldname.ToLower() == "email")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Email {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                if (fieldname.ToLower() == "hotelid")
                                                {
                                                    Int64 OldCrewHotelID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldCrewHotelID = (Int64)value1;

                                                    Int64 NewCrewHotelID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewCrewHotelID = (Int64)value2;
                                                    Hotel OldHotel = Container.Hotels.Where(x => x.HotelID == OldCrewHotelID).SingleOrDefault();
                                                    Hotel NewHotel = Container.Hotels.Where(x => x.HotelID == NewCrewHotelID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Crew Hotel {0} is changed to {1} for Leg {2} ", OldHotel.HotelCD, NewHotel.HotelCD, Leg.LegNUM));
                                                }
                                            }
                                            #endregion

                                            #region Maintenance

                                            if (cqHotelList.RecordType == "HM")
                                            {

                                                #region "cqhotellistdescription"
                                                if (fieldname.ToLower() == "cqhotellistdescription")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Name {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "PhoneNUM"
                                                if (fieldname.ToLower() == "phonenum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Phone {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "FaxNUM"
                                                if (fieldname.ToLower() == "faxnum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Fax {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "Email"
                                                if (fieldname.ToLower() == "email")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Passeneger Hotel Email {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                if (fieldname.ToLower() == "hotelid")
                                                {
                                                    Int64 OldMaintHotelID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldMaintHotelID = (Int64)value1;

                                                    Int64 NewMaintHotelID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewMaintHotelID = (Int64)value2;
                                                    Hotel OldHotel = Container.Hotels.Where(x => x.HotelID == OldMaintHotelID).SingleOrDefault();
                                                    Hotel NewHotel = Container.Hotels.Where(x => x.HotelID == NewMaintHotelID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Maintenance Hotel {0} is changed to {1} for Leg {2} ", OldHotel.HotelCD, NewHotel.HotelCD, Leg.LegNUM));
                                                }
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else if (cqHotelList.State == CQRequestEntityState.Deleted)
                            {
                                OriginalHotelList.LastUpdUID = UserPrincipal.Identity.Name;
                                OriginalHotelList.LastUpdTS = DateTime.UtcNow;
                                Container.CQHotelLists.DeleteObject(OriginalHotelList);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History for Deleted Hotel"
                                    if (OriginalHotelList.RecordType == "HP")
                                        HistoryDescription.AppendLine(string.Format("Passenger Hotel {1} for Leg{0} Deleted", Leg.LegNUM, OriginalHotelList.CQHotelListDescription));
                                    if (OriginalHotelList.RecordType == "HC")
                                        HistoryDescription.AppendLine(string.Format("Crew Hotel for {1} Leg{0} Deleted", Leg.LegNUM, OriginalHotelList.CQHotelListDescription));
                                    if (OriginalHotelList.RecordType == "HM")
                                        HistoryDescription.AppendLine(string.Format("Maintenance Hotel {1} for Leg{0} Deleted", Leg.LegNUM, OriginalHotelList.CQHotelListDescription));
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            // No -> It's a new child item -> Insert             
                            cqHotelList.IsDeleted = false;
                            cqHotelList.CQLegID = Leg.CQLegID;
                            if (Leg.State != CQRequestEntityState.Added)
                                originalLeg.CQHotelLists.Add(cqHotelList);
                            Container.ObjectStateManager.ChangeObjectState(cqHotelList, System.Data.EntityState.Added);

                            // Fix for #2242
                            if (cqHotelList.State == CQRequestEntityState.Added && (Leg.State == CQRequestEntityState.Added || Leg.State == CQRequestEntityState.Modified))
                            {
                                #region "History for Added Hotel"

                                Hotel hotel = new Hotel();
                                Hotel oldHotel = new Hotel();
                                Hotel newhotel = new Hotel();

                                //if (originalLeg != null)
                                //{
                                #region pax
                                if (cqHotelList.RecordType == "HP")
                                {
                                    Hotel paxHotel = new Hotel();
                                    paxHotel = Container.Hotels.Where(x => x.HotelID == (Int64)cqHotelList.HotelID).SingleOrDefault();
                                    if (paxHotel != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Passenger Hotel Code {0} is added to Leg {1} ", paxHotel.HotelCD, Leg.LegNUM));
                                        HistoryDescription.AppendLine(string.Format("Passenger Hotel Name {0} is added to Leg {1} ", cqHotelList.CQHotelListDescription, Leg.LegNUM));
                                    }

                                    //CQHotelList oldpaxHotel = new CQHotelList();
                                    //CQHotelList newpaxHotel = new CQHotelList();
                                    //oldpaxHotel = originalLeg.CQHotelLists.Where(x => x.RecordType == "HP" && x.IsDeleted == false).FirstOrDefault();
                                    //newpaxHotel = Leg.CQHotelLists.Where(x => x.RecordType == "HP" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldpaxHotel == null && newpaxHotel != null)
                                    //{
                                    //    if (newpaxHotel.HotelID != null)
                                    //    {
                                    //        hotel = Container.Hotels.Where(x => x.HotelID == (Int64)newpaxHotel.HotelID).SingleOrDefault();
                                    //        if (hotel != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Passenger Hotel {0} is added to Leg {1} ", hotel.HotelCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}
                                }
                                #endregion

                                #region crew
                                if (cqHotelList.RecordType == "HC")
                                {
                                    Hotel crewHotel = new Hotel();
                                    crewHotel = Container.Hotels.Where(x => x.HotelID == (Int64)cqHotelList.HotelID).SingleOrDefault();
                                    if (crewHotel != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Crew Hotel {0} is added to Leg {1} ", crewHotel.HotelCD, Leg.LegNUM));
                                        HistoryDescription.AppendLine(string.Format("Crew Hotel Name {0} is added to Leg {1} ", cqHotelList.CQHotelListDescription, Leg.LegNUM));
                                    }

                                    //CQHotelList oldcrewHotel = new CQHotelList();
                                    //CQHotelList newcrewHotel = new CQHotelList();
                                    //oldcrewHotel = originalLeg.CQHotelLists.Where(x => x.RecordType == "HC" && x.IsDeleted == false).FirstOrDefault();
                                    //newcrewHotel = Leg.CQHotelLists.Where(x => x.RecordType == "HC" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldcrewHotel == null && newcrewHotel != null)
                                    //{
                                    //    if (newcrewHotel.HotelID != null)
                                    //    {
                                    //        hotel = Container.Hotels.Where(x => x.HotelID == (Int64)newcrewHotel.HotelID).SingleOrDefault();
                                    //        if (hotel != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Crew Hotel {0} is added to Leg {1} ", hotel.HotelCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}
                                }
                                #endregion

                                #region maintenance
                                if (cqHotelList.RecordType == "HM")
                                {
                                    Hotel crewMaintenance = new Hotel();
                                    crewMaintenance = Container.Hotels.Where(x => x.HotelID == (Int64)cqHotelList.HotelID).SingleOrDefault();
                                    if (crewMaintenance != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Maintenance Hotel {0} is added to Leg {1} ", crewMaintenance.HotelCD, Leg.LegNUM));
                                        HistoryDescription.AppendLine(string.Format("Maintenance Hotel Name {0} is added to Leg {1} ", cqHotelList.CQHotelListDescription, Leg.LegNUM));
                                    }

                                    //CQHotelList oldmaintHotel = new CQHotelList();
                                    //CQHotelList newmaintHotel = new CQHotelList();
                                    //oldmaintHotel = originalLeg.CQHotelLists.Where(x => x.RecordType == "HM" && x.IsDeleted == false).FirstOrDefault();
                                    //newmaintHotel = Leg.CQHotelLists.Where(x => x.RecordType == "HM" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldmaintHotel == null && newmaintHotel != null)
                                    //{
                                    //    if (newmaintHotel.HotelID != null)
                                    //    {
                                    //        hotel = Container.Hotels.Where(x => x.HotelID == (Int64)newmaintHotel.HotelID).SingleOrDefault();
                                    //        if (hotel != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Maintenance Hotel {0} is added to Leg {1} ", hotel.HotelCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}
                                }
                                #endregion
                                //}
                                //else
                                //{
                                //    #region pax
                                //    if (cqHotelList.RecordType == "HP")
                                //    {
                                //        CQHotelList newpaxHotel = new CQHotelList();
                                //        newpaxHotel = Leg.CQHotelLists.Where(x => x.RecordType == "HP" && x.IsDeleted == false).FirstOrDefault();

                                //        if (newpaxHotel != null)
                                //        {
                                //            if (newpaxHotel.HotelID != null)
                                //            {
                                //                newhotel = Container.Hotels.Where(x => x.HotelID == (Int64)newpaxHotel.HotelID).SingleOrDefault();
                                //                if (newhotel != null)
                                //                {
                                //                    HistoryDescription.AppendLine(string.Format("Pax Transport {0} is added to Leg {1} ", newhotel.HotelCD, Leg.LegNUM));
                                //                }
                                //            }
                                //        }
                                //    }
                                //    #endregion

                                //    #region crew
                                //    if (cqHotelList.RecordType == "HC")
                                //    {
                                //        CQHotelList newcrewHotel = new CQHotelList();
                                //        newcrewHotel = Leg.CQHotelLists.Where(x => x.RecordType == "HC" && x.IsDeleted == false).FirstOrDefault();

                                //        if (newcrewHotel != null)
                                //        {
                                //            if (newcrewHotel.HotelID != null)
                                //            {
                                //                newhotel = Container.Hotels.Where(x => x.HotelID == (Int64)newcrewHotel.HotelID).SingleOrDefault();
                                //                if (newhotel != null)
                                //                {
                                //                    HistoryDescription.AppendLine(string.Format("Crew Transport {0} is added to Leg {1} ", newhotel.HotelCD, Leg.LegNUM));
                                //                }
                                //            }
                                //        }
                                //    }
                                //    #endregion

                                //    #region maintenance
                                //    if (cqHotelList.RecordType == "HM")
                                //    {
                                //        CQHotelList newmaintHotel = new CQHotelList();
                                //        newmaintHotel = Leg.CQHotelLists.Where(x => x.RecordType == "HM" && x.IsDeleted == false).FirstOrDefault();

                                //        if (newmaintHotel != null)
                                //        {
                                //            if (newmaintHotel.HotelID != null)
                                //            {
                                //                newhotel = Container.Hotels.Where(x => x.HotelID == (Int64)newmaintHotel.HotelID).SingleOrDefault();
                                //                if (newhotel != null)
                                //                {
                                //                    HistoryDescription.AppendLine(string.Format("Maintenance Transport {0} is added to Leg {1} ", newhotel.HotelCD, Leg.LegNUM));
                                //                }
                                //            }
                                //        }
                                //    }
                                //    #endregion
                                //}
                                #endregion
                            }
                        }
                    }
                    #endregion

                    #region "Catering"
                    foreach (CQCateringList caterList in Leg.CQCateringLists.ToList())
                    {
                        caterList.CustomerID = Leg.CustomerID;
                        caterList.LastUpdTS = DateTime.UtcNow;
                        caterList.LastUpdUID = UserPrincipal.Identity.Name;
                        if (caterList.State != CQRequestEntityState.Added)
                        {
                            var OriginalCateringlist = Container.CQCateringLists
                            .Where(c => c.CQCateringListID == caterList.CQCateringListID)
                            .SingleOrDefault();

                            if (caterList.State == CQRequestEntityState.Modified)
                            {
                                caterList.IsDeleted = false;
                                caterList.CQLegID = Leg.CQLegID;
                                Container.CQCateringLists.Attach(OriginalCateringlist);
                                Container.CQCateringLists.ApplyCurrentValues(caterList);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History For Modified Catering"
                                    var originalCateringValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCateringlist).OriginalValues;
                                    var CurrentCateringValues = Container.ObjectStateManager.GetObjectStateEntry(OriginalCateringlist).CurrentValues;
                                    PropertyInfo[] properties = typeof(CQCateringList).GetProperties();
                                    for (int i = 0; i < originalCateringValues.FieldCount; i++)
                                    {
                                        var fieldname = originalCateringValues.GetName(i);

                                        object value1 = originalCateringValues.GetValue(i);
                                        object value2 = CurrentCateringValues.GetValue(i);

                                        if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                        {
                                            if (caterList.RecordType == "CM")
                                            {

                                                #region Maintenance
                                                if (fieldname.ToLower() == "cateringid")
                                                {
                                                    Int64 OldManintCateringID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldManintCateringID = (Int64)value1;

                                                    Int64 NewManintCateringID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewManintCateringID = (Int64)value2;
                                                    Catering OldCatering = Container.Caterings.Where(x => x.CateringID == OldManintCateringID).SingleOrDefault();
                                                    Catering NewCatering = Container.Caterings.Where(x => x.CateringID == NewManintCateringID).SingleOrDefault();

                                                    HistoryDescription.AppendLine(string.Format("Departure Catering {0} is changed to {1} for Leg {2} ", OldCatering.CateringCD, NewCatering.CateringCD, Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "CQCateringListDescription"
                                                if (fieldname.ToLower() == "cqcateringlistdescription")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Departure Catering Name {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "PhoneNUM"
                                                if (fieldname.ToLower() == "phonenum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Departure Catering Phone {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "FaxNUM"
                                                if (fieldname.ToLower() == "faxnum")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Departure Catering Fax {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion

                                                #region "Email"
                                                if (fieldname.ToLower() == "email")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Departure Catering Email {0} is changed to {1} for Leg {2} ", value1.ToString(), value2.ToString(), Leg.LegNUM));
                                                }
                                                #endregion
                                            }
                                        }
                                    }

                                    #endregion
                                }
                            }
                            else if (caterList.State == CQRequestEntityState.Deleted)
                            {
                                OriginalCateringlist.LastUpdTS = DateTime.UtcNow;
                                OriginalCateringlist.LastUpdUID = UserPrincipal.Identity.Name;
                                Container.CQCateringLists.DeleteObject(OriginalCateringlist);

                                if (Leg.State != CQRequestEntityState.Deleted)
                                {
                                    #region "History for Deleted Catering"
                                    HistoryDescription.AppendLine(string.Format("Departure Catering {1} for Leg{0} Deleted", Leg.LegNUM, OriginalCateringlist.CQCateringListDescription));
                                    #endregion
                                }
                            }
                            else
                            {
                                Container.ObjectStateManager.ChangeObjectState(OriginalCateringlist, System.Data.EntityState.Unchanged);
                            }
                        }
                        else
                        {
                            // No -> It's a new child item -> Insert             
                            caterList.IsDeleted = false;
                            caterList.CQLegID = Leg.CQLegID;
                            if (Leg.State != CQRequestEntityState.Added)
                                originalLeg.CQCateringLists.Add(caterList);
                            Container.ObjectStateManager.ChangeObjectState(caterList, System.Data.EntityState.Added);

                            // Fix for #2242
                            if (caterList.State == CQRequestEntityState.Added && (Leg.State == CQRequestEntityState.Added || Leg.State == CQRequestEntityState.Modified))
                            {
                                #region "History for Added Catering"

                                //Catering catering = new Catering();
                                //Catering oldCatering = new Catering();
                                //Catering newCatering = new Catering();
                                //if (originalLeg != null)
                                //{
                                if (caterList.RecordType == "CM")
                                {
                                    #region Maintenance
                                    Catering caterMaintenance = new Catering();
                                    caterMaintenance = Container.Caterings.Where(x => x.CateringID == (Int64)caterList.CateringID).SingleOrDefault();
                                    if (caterMaintenance != null)
                                    {
                                        HistoryDescription.AppendLine(string.Format("Departure Catering Code {0} is added to Leg {1} ", caterMaintenance.CateringCD, Leg.LegNUM));
                                        HistoryDescription.AppendLine(string.Format("Departure Catering Name {0} is added to Leg {1} ", caterList.CQCateringListDescription, Leg.LegNUM));
                                    }

                                    //CQCateringList oldmaintCater = new CQCateringList();
                                    //CQCateringList newmaintCater = new CQCateringList();
                                    //oldmaintCater = originalLeg.CQCateringLists.Where(x => x.RecordType == "CM" && x.IsDeleted == false).FirstOrDefault();
                                    //newmaintCater = Leg.CQCateringLists.Where(x => x.RecordType == "CM" && x.IsDeleted == false).FirstOrDefault();

                                    //if (oldmaintCater == null && newmaintCater != null)
                                    //{
                                    //    if (newmaintCater.CateringID != null)
                                    //    {
                                    //        catering = Container.Caterings.Where(x => x.CateringID == (Int64)newmaintCater.CateringID).SingleOrDefault();
                                    //        if (catering != null)
                                    //        {
                                    //            HistoryDescription.AppendLine(string.Format("Maintenance Catering {0} is added to Leg {1} ", catering.CateringCD, Leg.LegNUM));
                                    //        }
                                    //    }
                                    //}
                                    #endregion
                                }
                                //}
                                //else
                                //{
                                //    if (caterList.RecordType == "CM")
                                //    {
                                //        #region Maintenance

                                //        newcatering = Container.Caterings.Where(x => x.CateringID == (Int64)newmaintCatering.CateringID).SingleOrDefault();
                                //        if (newcatering != null)
                                //        {
                                //            HistoryDescription.AppendLine(string.Format("Maintenance Catering {0} is added to Leg {1} ", newcatering.CateringCD, Leg.LegNUM));
                                //        }

                                //        CQCateringList newmaintCatering = new CQCateringList();
                                //        newmaintCatering = Leg.CQCateringLists.Where(x => x.RecordType == "CM" && x.IsDeleted == false).FirstOrDefault();

                                //        Catering newcatering = new Catering();

                                //        if (newmaintCatering != null)
                                //        {
                                //            if (newmaintCatering.CateringID != null)
                                //            {
                                //                newcatering = Container.Caterings.Where(x => x.CateringID == (Int64)newmaintCatering.CateringID).SingleOrDefault();
                                //                if (newcatering != null)
                                //                {
                                //                    HistoryDescription.AppendLine(string.Format("Maintenance Catering {0} is added to Leg {1} ", newcatering.CateringCD, Leg.LegNUM));
                                //                }
                                //            }
                                //        }

                                //        #endregion
                                //    }
                                //}
                                #endregion
                            }
                        }
                    }

                    #endregion

                    #endregion
                }
                #endregion

                #region CQException
                if (cqMain.State != CQRequestEntityState.Added && cqMain.State != CQRequestEntityState.Deleted)
                {
                    if (originalcqMain != null)
                    {
                        Container.LoadProperty(originalcqMain, c => c.CQExceptions);

                        if (originalcqMain.CQExceptions != null)
                        {
                            foreach (CQException Exception in originalcqMain.CQExceptions.ToList())
                            {
                                //CQException exp = Container.CQExceptions.Where(x => x.CQExceptionID == Exception.CQExceptionID).SingleOrDefault();
                                //if (exp != null)
                                //{
                                originalcqMain.CQExceptions.Remove(Exception);
                                Container.CQExceptions.DeleteObject(Exception);
                                //}
                            }
                        }

                    }
                }

                foreach (CQException Exception in cqMain.CQExceptions.ToList())
                {
                    if (Exception.CQExceptionID == 0 && cqMain.State != CQRequestEntityState.Deleted)
                    {
                        if (cqMain.CQMainID != 0)
                        {
                            originalcqMain.CQExceptions.Add(Exception);
                            //if (originalcqMain != null && originalcqMain.CQMainID != null)
                            //    Exception.CQMainID = originalcqMain.CQMainID;
                        }

                        Container.ObjectStateManager.ChangeObjectState(Exception, System.Data.EntityState.Added);
                    }

                }



                #endregion

                #region "CQHistory"
                if (HistoryDescription != null && HistoryDescription.Length > 0)
                {
                    //if (cqMain.State != CQRequestEntityState.NoChange)
                    //{
                    CQHistory cqHistory = new CQHistory();
                    cqHistory.CustomerID = CustomerID;
                    cqHistory.LogisticsHistory = HistoryDescription.ToString();
                    cqHistory.LastUpdUID = UserPrincipal.Identity.Name;
                    cqHistory.LastUpdTS = DateTime.UtcNow;

                    if (originalcqMain.CQMainID > 0)
                    {
                        if ((cqMain.State != CQRequestEntityState.Deleted))
                        {
                            cqHistory.CQMainID = originalcqMain.CQMainID;
                            originalcqMain.CQHistories.Add(cqHistory);
                            Container.ObjectStateManager.ChangeObjectState(cqHistory, System.Data.EntityState.Added);
                        }
                    }
                    else
                    {
                        if (cqMain.CQHistories == null)
                            cqMain.CQHistories = new System.Data.Objects.DataClasses.EntityCollection<CQHistory>();
                        cqMain.CQHistories.Add(cqHistory);
                        Container.ObjectStateManager.ChangeObjectState(cqHistory, System.Data.EntityState.Added);
                    }

                    //}

                    #region "Old Code"
                    //if (cqMain.State != CQRequestEntityState.NoChange)
                    //{
                    //    CQHistory cqHistory = new CQHistory();
                    //    cqHistory.CustomerID = CustomerID;
                    //    cqHistory.LogisticsHistory = HistoryDescription.ToString();
                    //    cqHistory.LastUpdUID = UserPrincipal.Identity.Name;
                    //    cqHistory.LastUpdTS = DateTime.UtcNow;

                    //    if (cqMain.State == CQRequestEntityState.Added || cqMain.State == CQRequestEntityState.NoChange)
                    //    {
                    //        if (cqMain.CQHistories == null)
                    //            cqMain.CQHistories = new System.Data.Objects.DataClasses.EntityCollection<CQHistory>();
                    //        cqMain.CQHistories.Add(cqHistory);

                    //        Container.ObjectStateManager.ChangeObjectState(cqHistory, System.Data.EntityState.Added);
                    //    }
                    //    else if (cqMain.State != CQRequestEntityState.Deleted)
                    //    {
                    //        cqHistory.CQMainID = originalcqMain.CQMainID;
                    //        originalcqMain.CQHistories.Add(cqHistory);
                    //        Container.ObjectStateManager.ChangeObjectState(cqHistory, System.Data.EntityState.Added);
                    //    }
                    //}
                    #endregion
                }

                #endregion
            }
            #endregion

        }

        public ReturnValue<CQFile> DeleteFile(Int64 CQFileID)
        {
            ReturnValue<CQFile> ReturnValue = new ReturnValue<CQFile>();
            CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();
            CQFile file = new CQFile();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    try
                    {
                        //lock
                        LockManager<Data.CharterQuote.CQFile> lockManager = new LockManager<CQFile>();
                        lockManager.Lock(FlightPak.Common.Constants.EntitySet.CharterQuote.CQFile, CQFileID);

                        using (Container = new CharterQuoteDataModelContainer())
                        {
                            file = new CQFile();
                            file = Container.CQFiles.Where(X => X.CQFileID == CQFileID).First();
                            if (file != null)
                            {
                                file.IsDeleted = true;
                                file.State = CQRequestEntityState.Deleted;
                                file.LastUpdTS = DateTime.UtcNow;
                                Container.CQFiles.DeleteObject(file);
                                Container.SaveChanges();
                            }
                            ReturnValue.EntityInfo = null;
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        //manually handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                    finally
                    {
                        //Unlock
                        LockManager<Data.CharterQuote.CQFile> lockManager = new LockManager<CQFile>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CharterQuote.CQFile, CQFileID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        private void LoadTripProperties(ref CQFile cqFile, ref CharterQuoteDataModelContainer cs)
        {
            if (cqFile.CQCustomerID != null) cs.LoadProperty(cqFile, c => c.CQCustomer);
            if (cqFile.CQCustomerContactID != null) cs.LoadProperty(cqFile, c => c.CQCustomerContact);
            if (cqFile.LeadSourceID != null) cs.LoadProperty(cqFile, c => c.LeadSource);
            if (cqFile.SalesPersonID != null) cs.LoadProperty(cqFile, c => c.SalesPerson);
            if (cqFile.HomebaseID != null) cs.LoadProperty(cqFile, c => c.Company);
            if (cqFile.ExchangeRateID != null) cs.LoadProperty(cqFile, c => c.ExchangeRate1);
            #region CQMain Properties
            cs.LoadProperty(cqFile, c => c.CQMains);
            if (cqFile.CQMains != null)
            {
                foreach (CQMain cqMain in cqFile.CQMains.ToList())
                {


                    if (cqMain.IsDeleted)
                        cqFile.CQMains.Remove(cqMain);

                    #region CQMain Properties
                    if (cqMain.VendorID != null) cs.LoadProperty(cqMain, c => c.Vendor);
                    if (cqMain.FleetID != null) cs.LoadProperty(cqMain, c => c.Fleet);
                    if (cqMain.AircraftID != null) cs.LoadProperty(cqMain, c => c.Aircraft);
                    if (cqMain.PassengerRequestorID != null) cs.LoadProperty(cqMain, c => c.Passenger);
                    if (cqMain.HomebaseID != null) cs.LoadProperty(cqMain, c => c.Company);
                    if (cqMain.ClientID != null) cs.LoadProperty(cqMain, c => c.Client);
                    if (cqMain.CQLostBusinessID != null) cs.LoadProperty(cqMain, c => c.CQLostBusiness);
                    if (cqMain.FeeGroupID != null) cs.LoadProperty(cqMain, c => c.FeeGroup);
                    if (cqMain.TripID != null) cs.LoadProperty(cqMain, c => c.PreflightMain);
                    #endregion

                    #region CQFleetChargeDetail Properties
                    cs.LoadProperty(cqMain, c => c.CQFleetChargeDetails);

                    foreach (CQFleetChargeDetail cqfltChrgDet in cqMain.CQFleetChargeDetails.ToList())
                    {
                        if (cqfltChrgDet.IsDeleted)
                            cqMain.CQFleetChargeDetails.Remove(cqfltChrgDet);

                        if (cqfltChrgDet.AccountID != null) cs.LoadProperty(cqfltChrgDet, c => c.Account);
                        if (cqfltChrgDet.AircraftCharterRateID != null) cs.LoadProperty(cqfltChrgDet, c => c.AircraftCharterRate);
                        if (cqfltChrgDet.FleetCharterRateID != null) cs.LoadProperty(cqfltChrgDet, c => c.FleetCharterRate);
                        //if (cqfltChrgDet.FeeGroupID != null) cs.LoadProperty(cqfltChrgDet, c => c.FeeGroup);
                    }
                    #endregion


                    cs.LoadProperty(cqMain, c => c.CQLegs);

                    foreach (CQLeg Leg in cqMain.CQLegs.ToList())
                    {

                        #region CQLeg Properties
                        if (Leg.IsDeleted)
                            cqMain.CQLegs.Remove(Leg);

                        if (Leg.DAirportID != null) cs.LoadProperty(Leg, c => c.Airport1);
                        if (Leg.AAirportID != null) cs.LoadProperty(Leg, c => c.Airport);
                        if (Leg.FBOID != null) cs.LoadProperty(Leg, c => c.FBO);
                        if (Leg.CrewDutyRulesID != null) cs.LoadProperty(Leg, c => c.CrewDutyRule);
                        if (Leg.ClientID != null) cs.LoadProperty(Leg, c => c.Client);
                        if (Leg.OIAAirportID != null) cs.LoadProperty(Leg, c => c.Airport2);
                        if (Leg.OIDAirportID != null) cs.LoadProperty(Leg, c => c.Airport3);
                        if (Leg.OQAAirportID != null) cs.LoadProperty(Leg, c => c.Airport4);
                        if (Leg.OQDAirportID != null) cs.LoadProperty(Leg, c => c.Airport5);
                        #endregion


                        #region CQPassenger Properties
                        cs.LoadProperty(Leg, c => c.CQPassengers);



                        foreach (CQPassenger PAX in Leg.CQPassengers.ToList())
                        {
                            if (PAX.IsDeleted)
                                Leg.CQPassengers.Remove(PAX);

                            if (PAX.PassengerRequestorID != null) cs.LoadProperty(PAX, c => c.Passenger);
                            if (PAX.FlightPurposeID != null) cs.LoadProperty(PAX, c => c.FlightPurpose);
                            if (PAX.PassportID != null) cs.LoadProperty(PAX, c => c.CrewPassengerPassport);
                        }
                        #endregion


                        #region CQFBOList Properties
                        cs.LoadProperty(Leg, c => c.CQFBOLists);



                        foreach (CQFBOList fbo in Leg.CQFBOLists.ToList())
                        {
                            if (fbo.IsDeleted)
                                Leg.CQFBOLists.Remove(fbo);

                            if (fbo.FBOID != null) cs.LoadProperty(fbo, c => c.FBO);
                            if (fbo.AirportID != null) cs.LoadProperty(fbo, c => c.Airport);
                        }
                        #endregion

                        #region CQHotelList Properties
                        cs.LoadProperty(Leg, c => c.CQHotelLists);



                        foreach (CQHotelList hotel in Leg.CQHotelLists.ToList())
                        {
                            if (hotel.IsDeleted)
                                Leg.CQHotelLists.Remove(hotel);

                            if (hotel.HotelID != null) cs.LoadProperty(hotel, c => c.Hotel);
                            if (hotel.AirportID != null) cs.LoadProperty(hotel, c => c.Airport);

                        }
                        #endregion

                        #region CQTransportList Properties
                        cs.LoadProperty(Leg, c => c.CQTransportLists);



                        foreach (CQTransportList transport in Leg.CQTransportLists.ToList())
                        {
                            if (transport.IsDeleted)
                                Leg.CQTransportLists.Remove(transport);

                            if (transport.TransportID != null) cs.LoadProperty(transport, c => c.Transport);
                            if (transport.AirportID != null) cs.LoadProperty(transport, c => c.Airport);
                        }
                        #endregion


                        #region CQCateringList Properties
                        cs.LoadProperty(Leg, c => c.CQCateringLists);



                        foreach (CQCateringList Catering in Leg.CQCateringLists.ToList())
                        {
                            if (Catering.IsDeleted)
                                Leg.CQCateringLists.Remove(Catering);

                            if (Catering.CateringID != null) cs.LoadProperty(Catering, c => c.Catering);
                            if (Catering.AirportID != null) cs.LoadProperty(Catering, c => c.Airport);
                        }
                        #endregion

                    }




                }
            }
            #endregion
        }


        #endregion

        #region "CharterQuote CQ Quote Main"

        public ReturnValue<CQQuoteMain> GetCQQuoteMainByCQMainID(Int64 CQMainID)
        {
            ReturnValue<CQQuoteMain> ret = new ReturnValue<CQQuoteMain>();
            CharterQuoteDataModelContainer cs = null;
            CQQuoteMain cqQuoteMain = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        cqQuoteMain = new CQQuoteMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;

                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;



                        ret.EntityList = (from CQQuotemain in cs.CQQuoteMains
                                          where ((CQQuotemain.CQMainID == CQMainID)
                                                        && (CQQuotemain.CustomerID == CustomerID)
                                                        && (CQQuotemain.IsDeleted == false)
                                              // && (trips.IsPrivate == isprivateUser? false: (bool)trips.IsPrivate)
                                              )
                                          select CQQuotemain).ToList();


                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {


                            cqQuoteMain = ret.EntityList[0];
                            LoadQuoteReportProperties(ref cqQuoteMain, ref cs);
                            ret.EntityList[0] = cqQuoteMain;
                            ret.ReturnFlag = true;


                        }
                        else
                        {
                            cqQuoteMain = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.CharterQuote.CQQuoteMain> UpdateCQQouteMain(CQQuoteMain cqQuoteMain)
        {
            ReturnValue<Data.CharterQuote.CQQuoteMain> ReturnValue = null;
            Data.CharterQuote.CharterQuoteDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqQuoteMain))
            {
                ReturnValue = new ReturnValue<CQQuoteMain>();
                try
                {
                    using (Container = new CharterQuoteDataModelContainer())
                    {
                        cqQuoteMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        cqQuoteMain.LastUpdTS = DateTime.UtcNow;
                        if (UserPrincipal.Identity.Name != null)
                            cqQuoteMain.LastUpdUID = UserPrincipal.Identity.Name.Trim();

                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;

                        //Update operation goes here
                        UpdateCQQuoteMainEntity(ref cqQuoteMain, ref Container);

                        Container.SaveChanges();

                        //Unlock
                        LockManager<Data.CharterQuote.CQFile> lockManager = new LockManager<CQFile>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CharterQuote.CQQuoteMain, cqQuoteMain.CQQuoteMainID);

                        //Return Trip updtae status   
                        ReturnValue = GetCQQuoteMainByCQMainID((long)cqQuoteMain.CQMainID);
                        ReturnValue.EntityInfo = ReturnValue.EntityList[0];
                        ReturnValue.ReturnFlag = true;
                    }
                }
                catch (Exception ex)
                {
                    //manually handled
                    ReturnValue.ErrorMessage = ex.Message;
                    ReturnValue.ReturnFlag = false;
                }
                finally
                {

                }

                return ReturnValue;
            }
        }

        private void UpdateCQQuoteMainEntity(ref CQQuoteMain cqQuoteMain, ref CharterQuoteDataModelContainer Container)
        {

            cqQuoteMain.LastUpdTS = DateTime.UtcNow;
            cqQuoteMain.LastUpdUID = UserPrincipal.Identity.Name;

            #region CQQuoteMain -> CQMain

            if (cqQuoteMain.CQMain != null)
            {
                var cqMain = cqQuoteMain.CQMain;

                CQMain originalcqMain = new CQMain();

                if (cqMain.State != CQRequestEntityState.Added)
                {
                    originalcqMain = Container.CQMains
                    .Where(c => c.CQMainID == cqMain.CQMainID)
                    .SingleOrDefault();

                    if (originalcqMain != null)
                    {
                        Container.CQMains.Attach(originalcqMain);
                        Container.CQMains.ApplyCurrentValues(cqMain);
                        Container.ObjectStateManager.ChangeObjectState(originalcqMain, System.Data.EntityState.Modified);
                    }
                }
                cqQuoteMain.CQMain = null;
            }
            #endregion


            var cqQuoteMainID = cqQuoteMain.CQQuoteMainID;
            CQQuoteMain originalcqQuoteMain = Container.CQQuoteMains
                .Where(p => p.CQQuoteMainID == cqQuoteMainID).SingleOrDefault();

            #region CQQuoteMain
            if (cqQuoteMain.State == CQRequestEntityState.Modified)
            {
                Container.CQQuoteMains.Attach(originalcqQuoteMain);
                Container.CQQuoteMains.ApplyCurrentValues(cqQuoteMain);
            }
            else if (cqQuoteMain.State == CQRequestEntityState.Added)
            {
                cqQuoteMain.IsDeleted = false;
                Container.CQQuoteMains.AddObject(cqQuoteMain);
            }
            else
            {
                Container.CQQuoteMains.Attach(originalcqQuoteMain);
            }
            #endregion

            #region CQQuoteDetail

            foreach (var cqQuoteDetail in cqQuoteMain.CQQuoteDetails.ToList())
            {
                cqQuoteDetail.LastUpdTS = DateTime.UtcNow;
                cqQuoteDetail.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CQQuoteDetail originalcqQuoteDetail = new CQQuoteDetail();

                if (cqQuoteDetail.State != CQRequestEntityState.Added)
                {
                    originalcqQuoteDetail = Container.CQQuoteDetails
                    .Where(c => c.CQQuoteDetailID == cqQuoteDetail.CQQuoteDetailID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (cqQuoteDetail.State == CQRequestEntityState.Modified)
                    {
                        cqQuoteDetail.IsDeleted = false;
                        cqQuoteDetail.CustomerID = CustomerID;
                        Container.CQQuoteDetails.Attach(originalcqQuoteDetail);
                        Container.CQQuoteDetails.ApplyCurrentValues(cqQuoteDetail);

                    }
                    else if (cqQuoteDetail.State == CQRequestEntityState.Deleted)
                    {
                        originalcqQuoteDetail.LastUpdTS = DateTime.UtcNow;
                        originalcqQuoteDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CQQuoteDetails.DeleteObject(originalcqQuoteDetail);

                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalcqQuoteDetail, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    cqQuoteDetail.IsDeleted = false;
                    cqQuoteDetail.CustomerID = CustomerID;
                    if (cqQuoteMain.State != CQRequestEntityState.Added)
                        originalcqQuoteMain.CQQuoteDetails.Add(cqQuoteDetail);
                    Container.ObjectStateManager.ChangeObjectState(cqQuoteDetail, System.Data.EntityState.Added);

                }
            }
            #endregion

            #region CQQuoteAdditionalFees

            foreach (var cqQuoteAdditionalFee in cqQuoteMain.CQQuoteAdditionalFees.ToList())
            {
                cqQuoteAdditionalFee.LastUpdTS = DateTime.UtcNow;
                cqQuoteAdditionalFee.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CQQuoteAdditionalFee originalQuoteAdditionalFee = new CQQuoteAdditionalFee();

                if (cqQuoteAdditionalFee.State != CQRequestEntityState.Added)
                {
                    originalQuoteAdditionalFee = Container.CQQuoteAdditionalFees
                    .Where(c => c.CQQuoteAdditionalFeesID == cqQuoteAdditionalFee.CQQuoteAdditionalFeesID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (cqQuoteAdditionalFee.State == CQRequestEntityState.Modified)
                    {
                        cqQuoteAdditionalFee.IsDeleted = false;
                        cqQuoteAdditionalFee.CustomerID = CustomerID;
                        Container.CQQuoteAdditionalFees.Attach(originalQuoteAdditionalFee);
                        Container.CQQuoteAdditionalFees.ApplyCurrentValues(cqQuoteAdditionalFee);

                    }
                    else if (cqQuoteAdditionalFee.State == CQRequestEntityState.Deleted)
                    {
                        originalQuoteAdditionalFee.LastUpdTS = DateTime.UtcNow;
                        originalQuoteAdditionalFee.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CQQuoteAdditionalFees.DeleteObject(originalQuoteAdditionalFee);

                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalQuoteAdditionalFee, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    cqQuoteAdditionalFee.IsDeleted = false;
                    cqQuoteAdditionalFee.CustomerID = CustomerID;
                    if (cqQuoteMain.State != CQRequestEntityState.Added)
                        originalcqQuoteMain.CQQuoteAdditionalFees.Add(cqQuoteAdditionalFee);
                    Container.ObjectStateManager.ChangeObjectState(cqQuoteAdditionalFee, System.Data.EntityState.Added);

                }
            }
            #endregion



        }

        private void LoadQuoteReportProperties(ref CQQuoteMain cqQuoteMain, ref CharterQuoteDataModelContainer cs)
        {
            if (cqQuoteMain.FileWarehouseID != null) cs.LoadProperty(cqQuoteMain, c => c.FileWarehouse);
            if (cqQuoteMain.CQMainID != null) cs.LoadProperty(cqQuoteMain, c => c.CQMain);

            #region CQMain Properties
            cs.LoadProperty(cqQuoteMain, c => c.CQMain);
            #endregion

            #region CQQuoteDetail Properties
            cs.LoadProperty(cqQuoteMain, c => c.CQQuoteDetails);
            if (cqQuoteMain.CQQuoteDetails != null)
            {
                foreach (CQQuoteDetail quoteDet in cqQuoteMain.CQQuoteDetails)
                {
                    if (quoteDet.DAirportID != null)
                        cs.LoadProperty(quoteDet, c => c.Airport1);

                    if (quoteDet.AAirportID != null)
                        cs.LoadProperty(quoteDet, c => c.Airport);
                }
            }
            #endregion
            #region CQQuoteAdditionalFees Properties
            cs.LoadProperty(cqQuoteMain, c => c.CQQuoteAdditionalFees);
            #endregion

        }

        #endregion

        #region "CharterQuote CQ Invoice Main"

        public ReturnValue<CQInvoiceMain> GetCQInvoiceMainByCQMainID(Int64 CQMainID)
        {
            ReturnValue<CQInvoiceMain> ret = new ReturnValue<CQInvoiceMain>();
            CharterQuoteDataModelContainer cs = null;
            CQInvoiceMain cqInvoiceMain = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        cqInvoiceMain = new CQInvoiceMain();
                        cs.ContextOptions.LazyLoadingEnabled = false;

                        cs.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;



                        ret.EntityList = (from CQinvmain in cs.CQInvoiceMains
                                          where ((CQinvmain.CQMainID == CQMainID)
                                                        && (CQinvmain.CustomerID == CustomerID)
                                                        && (CQinvmain.IsDeleted == false)
                                              // && (trips.IsPrivate == isprivateUser? false: (bool)trips.IsPrivate)
                                              )
                                          select CQinvmain).ToList();


                        if (ret.EntityList != null && ret.EntityList.Count > 0)
                        {


                            cqInvoiceMain = ret.EntityList[0];
                            LoadInvoiceReportProperties(ref cqInvoiceMain, ref cs);
                            ret.EntityList[0] = cqInvoiceMain;
                            ret.ReturnFlag = true;


                        }
                        else
                        {
                            cqInvoiceMain = null;
                            ret.ReturnFlag = false;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.CharterQuote.CQInvoiceMain> UpdateCQInvoiceMain(CQInvoiceMain cqInvoiceMain)
        {
            ReturnValue<Data.CharterQuote.CQInvoiceMain> ReturnValue = null;
            Data.CharterQuote.CharterQuoteDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqInvoiceMain))
            {
                ReturnValue = new ReturnValue<CQInvoiceMain>();
                try
                {
                    using (Container = new CharterQuoteDataModelContainer())
                    {
                        cqInvoiceMain.CustomerID = UserPrincipal.Identity.CustomerID;
                        cqInvoiceMain.LastUpdTS = DateTime.UtcNow;
                        if (UserPrincipal.Identity.Name != null)
                            cqInvoiceMain.LastUpdUID = UserPrincipal.Identity.Name.Trim();

                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;

                        //Update operation goes here
                        UpdateCQInvoiceMainEntity(ref cqInvoiceMain, ref Container);

                        Container.SaveChanges();

                        //Unlock
                        LockManager<Data.CharterQuote.CQFile> lockManager = new LockManager<CQFile>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.CharterQuote.CQInvoiceMain, cqInvoiceMain.CQInvoiceMainID);

                        //Return Trip updtae status   
                        ReturnValue = GetCQInvoiceMainByCQMainID((long)cqInvoiceMain.CQMainID);
                        ReturnValue.EntityInfo = ReturnValue.EntityList[0];
                        ReturnValue.ReturnFlag = true;
                    }
                }
                catch (Exception ex)
                {
                    //manually handled
                    ReturnValue.ErrorMessage = ex.Message;
                    ReturnValue.ReturnFlag = false;
                }
                finally
                {

                }

                return ReturnValue;
            }
        }

        private void UpdateCQInvoiceMainEntity(ref CQInvoiceMain cqInvoiceMain, ref CharterQuoteDataModelContainer Container)
        {

            cqInvoiceMain.LastUpdTS = DateTime.UtcNow;
            cqInvoiceMain.LastUpdUID = UserPrincipal.Identity.Name;

            var cqInvoiceMainID = cqInvoiceMain.CQInvoiceMainID;
            CQInvoiceMain originalcqInvoiceMain = Container.CQInvoiceMains
                .Where(p => p.CQInvoiceMainID == cqInvoiceMainID).SingleOrDefault();

            #region CQInvoiceMain
            if (cqInvoiceMain.State == CQRequestEntityState.Modified)
            {
                Container.CQInvoiceMains.Attach(originalcqInvoiceMain);
                Container.CQInvoiceMains.ApplyCurrentValues(cqInvoiceMain);
            }
            else if (cqInvoiceMain.State == CQRequestEntityState.Added)
            {
                cqInvoiceMain.IsDeleted = false;
                Container.CQInvoiceMains.AddObject(cqInvoiceMain);
            }
            else
            {
                Container.CQInvoiceMains.Attach(originalcqInvoiceMain);
            }
            #endregion

            #region CQInvoiceQuoteDetail

            foreach (var cqInvoiceQuoteDetail in cqInvoiceMain.CQInvoiceQuoteDetails.ToList())
            {
                cqInvoiceQuoteDetail.LastUpdTS = DateTime.UtcNow;
                cqInvoiceQuoteDetail.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CQInvoiceQuoteDetail originalcqInvoiceQuoteDetail = new CQInvoiceQuoteDetail();

                if (cqInvoiceQuoteDetail.State != CQRequestEntityState.Added)
                {
                    originalcqInvoiceQuoteDetail = Container.CQInvoiceQuoteDetails
                    .Where(c => c.CQInvoiceQuoteDetailID == cqInvoiceQuoteDetail.CQInvoiceQuoteDetailID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (cqInvoiceQuoteDetail.State == CQRequestEntityState.Modified)
                    {
                        cqInvoiceQuoteDetail.IsDeleted = false;
                        cqInvoiceQuoteDetail.CustomerID = CustomerID;
                        Container.CQInvoiceQuoteDetails.Attach(originalcqInvoiceQuoteDetail);
                        Container.CQInvoiceQuoteDetails.ApplyCurrentValues(cqInvoiceQuoteDetail);

                    }
                    else if (cqInvoiceQuoteDetail.State == CQRequestEntityState.Deleted)
                    {
                        originalcqInvoiceQuoteDetail.LastUpdTS = DateTime.UtcNow;
                        originalcqInvoiceQuoteDetail.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CQInvoiceQuoteDetails.DeleteObject(originalcqInvoiceQuoteDetail);

                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalcqInvoiceQuoteDetail, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    cqInvoiceQuoteDetail.IsDeleted = false;
                    cqInvoiceQuoteDetail.CustomerID = CustomerID;
                    if (cqInvoiceMain.State != CQRequestEntityState.Added)
                        originalcqInvoiceMain.CQInvoiceQuoteDetails.Add(cqInvoiceQuoteDetail);
                    Container.ObjectStateManager.ChangeObjectState(cqInvoiceQuoteDetail, System.Data.EntityState.Added);

                }
            }
            #endregion

            #region CQInvoiceAdditionalFees

            foreach (var cqInvoiceAdditionalFee in cqInvoiceMain.CQInvoiceAdditionalFees.ToList())
            {
                cqInvoiceAdditionalFee.LastUpdTS = DateTime.UtcNow;
                cqInvoiceAdditionalFee.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CQInvoiceAdditionalFee originalcqInvoiceAdditionalFee = new CQInvoiceAdditionalFee();

                if (cqInvoiceAdditionalFee.State != CQRequestEntityState.Added)
                {
                    originalcqInvoiceAdditionalFee = Container.CQInvoiceAdditionalFees
                    .Where(c => c.CQInvoiceAdditionalFeesID == cqInvoiceAdditionalFee.CQInvoiceAdditionalFeesID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (cqInvoiceAdditionalFee.State == CQRequestEntityState.Modified)
                    {
                        cqInvoiceAdditionalFee.IsDeleted = false;
                        cqInvoiceAdditionalFee.CustomerID = CustomerID;
                        Container.CQInvoiceAdditionalFees.Attach(originalcqInvoiceAdditionalFee);
                        Container.CQInvoiceAdditionalFees.ApplyCurrentValues(cqInvoiceAdditionalFee);

                    }
                    else if (cqInvoiceAdditionalFee.State == CQRequestEntityState.Deleted)
                    {
                        originalcqInvoiceAdditionalFee.LastUpdTS = DateTime.UtcNow;
                        originalcqInvoiceAdditionalFee.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CQInvoiceAdditionalFees.DeleteObject(originalcqInvoiceAdditionalFee);

                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalcqInvoiceAdditionalFee, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    cqInvoiceAdditionalFee.IsDeleted = false;
                    cqInvoiceAdditionalFee.CustomerID = CustomerID;
                    if (cqInvoiceMain.State != CQRequestEntityState.Added)
                        originalcqInvoiceMain.CQInvoiceAdditionalFees.Add(cqInvoiceAdditionalFee);
                    Container.ObjectStateManager.ChangeObjectState(cqInvoiceAdditionalFee, System.Data.EntityState.Added);

                }
            }
            #endregion

            #region CQInvoiceSummary

            foreach (var cqInvoiceSummary in cqInvoiceMain.CQInvoiceSummaries.ToList())
            {
                cqInvoiceSummary.LastUpdTS = DateTime.UtcNow;
                cqInvoiceSummary.LastUpdUID = UserPrincipal.Identity.Name;

                // Is original child item with same ID in DB?    
                CQInvoiceSummary originalcqInvoiceSummary = new CQInvoiceSummary();

                if (cqInvoiceSummary.State != CQRequestEntityState.Added)
                {
                    originalcqInvoiceSummary = Container.CQInvoiceSummaries
                    .Where(c => c.CQInvoiceSummaryID == cqInvoiceSummary.CQInvoiceSummaryID)
                    .SingleOrDefault();

                    // Yes -> Update scalar properties of child item 
                    if (cqInvoiceSummary.State == CQRequestEntityState.Modified)
                    {
                        cqInvoiceSummary.IsDeleted = false;
                        cqInvoiceSummary.CustomerID = CustomerID;
                        Container.CQInvoiceSummaries.Attach(originalcqInvoiceSummary);
                        Container.CQInvoiceSummaries.ApplyCurrentValues(cqInvoiceSummary);

                    }
                    else if (cqInvoiceSummary.State == CQRequestEntityState.Deleted)
                    {
                        originalcqInvoiceSummary.LastUpdTS = DateTime.UtcNow;
                        originalcqInvoiceSummary.LastUpdUID = UserPrincipal.Identity.Name;
                        Container.CQInvoiceSummaries.DeleteObject(originalcqInvoiceSummary);

                    }
                    else
                    {
                        Container.ObjectStateManager.ChangeObjectState(originalcqInvoiceSummary, System.Data.EntityState.Unchanged);
                    }
                }
                else
                {
                    // No -> It's a new child item -> Insert             
                    cqInvoiceSummary.IsDeleted = false;
                    cqInvoiceSummary.CustomerID = CustomerID;
                    if (cqInvoiceMain.State != CQRequestEntityState.Added)
                        originalcqInvoiceMain.CQInvoiceSummaries.Add(cqInvoiceSummary);
                    Container.ObjectStateManager.ChangeObjectState(cqInvoiceSummary, System.Data.EntityState.Added);

                }
            }
            #endregion

        }

        private void LoadInvoiceReportProperties(ref CQInvoiceMain cqInvoiceMain, ref CharterQuoteDataModelContainer cs)
        {
            if (cqInvoiceMain.CQMessageID != null) cs.LoadProperty(cqInvoiceMain, c => c.CQMessage);
            if (cqInvoiceMain.CQMainID != null) cs.LoadProperty(cqInvoiceMain, c => c.CQMain);

            #region CQInvoiceQuoteDetail Properties
            cs.LoadProperty(cqInvoiceMain, c => c.CQInvoiceQuoteDetails);
            if (cqInvoiceMain.CQInvoiceQuoteDetails != null)
            {
                foreach (CQInvoiceQuoteDetail quoteDet in cqInvoiceMain.CQInvoiceQuoteDetails)
                {
                    if (quoteDet.DAirportID != null)
                        cs.LoadProperty(quoteDet, c => c.Airport1);

                    if (quoteDet.AAirportID != null)
                        cs.LoadProperty(quoteDet, c => c.Airport);
                }
            }
            #endregion

            #region CQInvoiceAdditionalFees Properties
            cs.LoadProperty(cqInvoiceMain, c => c.CQInvoiceAdditionalFees);
            #endregion

            #region CQInvoiceSummary Properties
            cs.LoadProperty(cqInvoiceMain, c => c.CQInvoiceSummaries);
            #endregion

        }

        #endregion

        #region "CharterQuote Passenger"

        public ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList> GetAllCharterQuoteAvailablePaxList()
        {
            ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList> ret = null;
            CharterQuoteDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.CharterQuote.GetAllCharterQuoteAvailablePaxList>();
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetAllCharterQuoteAvailablePaxList(CustomerID, ClientId).ToList();
                        //foreach (var item in ret.EntityList)
                        //{
                        //    item.Passport = Crypting.Decrypt(item.Passport);
                        //}
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        #endregion

        public ReturnValue<View_CharterQuoteMainList> GetAllCQMainList(Int64 homebaseID, Nullable<DateTime> EstDepartDate, bool expressQuote, Int64 leadSourceID, string customerType, Int64 salesPersonID)
        {
            ReturnValue<Data.CharterQuote.View_CharterQuoteMainList> ret = null;
            CharterQuoteDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(homebaseID, EstDepartDate, expressQuote, leadSourceID, customerType, salesPersonID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        ret = new ReturnValue<Data.CharterQuote.View_CharterQuoteMainList>();

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;

                        ret.EntityList = cs.GetAllCQMainList(CustomerID, homebaseID, EstDepartDate, expressQuote, leadSourceID, customerType, salesPersonID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<ExceptionTemplate> GetExceptionTemplateList()
        {
            ReturnValue<ExceptionTemplate> ret = new ReturnValue<ExceptionTemplate>();
            CharterQuoteDataModelContainer cs = new CharterQuoteDataModelContainer();

            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            exManager.Process(() =>
            {
                ret = new ReturnValue<ExceptionTemplate>();
                using (cs = new CharterQuoteDataModelContainer())
                {
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.ExceptionTemplates.Where(x => x.ModuleID == (long)CQBusinessErrorModule.CharterQuote).ToList();
                    ret.ReturnFlag = true;
                }
            }, FlightPak.Common.Constants.Policy.DataLayer);
            return ret;
        }

        public ReturnValue<CQException> GetQuoteExceptionList(Int64 cqMainID)
        {
            ReturnValue<CQException> ret = new ReturnValue<CQException>();
            CharterQuoteDataModelContainer cs = new CharterQuoteDataModelContainer();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqMainID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<CQException>();
                    using (cs = new CharterQuoteDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.CQExceptions.Where(x => (x.CQMainID == cqMainID && x.CustomerID == CustomerID)).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<CQException> ValidateQuoteforMandatoryExcep(CQMain cqMain)
        { //Handle methods throguh exception manager with return flag
            List<CQException> ExceptionList;
            ReturnValue<CQException> returnValue = new ReturnValue<CQException>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqMain))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ExceptionList = new List<CQException>();

                    CharterQuoteValidationManager ValidMgr = new CharterQuoteValidationManager();
                    var validateResult = ValidMgr.ValidateBusinessRules(null, cqMain, true);

                    if (validateResult != null && validateResult.ReturnFlag == true && validateResult.EntityList.Count > 0)
                    {
                        ExceptionList = validateResult.EntityList;

                    }
                    returnValue.ReturnFlag = true;
                    returnValue.EntityList = ExceptionList;

                }
                , FlightPak.Common.Constants.Policy.DataLayer);

            }

            return returnValue;
        }

        //public ReturnValue<Data.CharterQuote.CQLostBusiness> GetLostBusinessInfo()
        //{
        //    ReturnValue<Data.CharterQuote.CQLostBusiness> objLostBusinessType = new ReturnValue<Data.CharterQuote.CQLostBusiness>();

        //    using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
        //    {
        //        //Handle methods throguh exception manager with return flag
        //        exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //        exManager.Process(() =>
        //        {
        //            Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
        //            // Fix for recursive infinite loop
        //            objContainer.ContextOptions.LazyLoadingEnabled = false;
        //            objContainer.ContextOptions.ProxyCreationEnabled = false;
        //            objLostBusinessType.EntityList = objContainer.CQGETLostBusiness(CustomerID).ToList();
        //            objLostBusinessType.ReturnFlag = true;
        //        }, FlightPak.Common.Constants.Policy.DataLayer);

        //    }

        //    return objLostBusinessType;
        //}

        public ReturnValue<Data.CharterQuote.CQFile> GetCQCopyQuoteDetail(Int64 CQFileID, Int64 CQMainID)
        {
            ReturnValue<CQFile> ReturnValue = new ReturnValue<CQFile>();
            CharterQuoteDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, CQMainID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopyQuoteID = new long();
                    CopyQuoteID = 0;
                    using (cs = new CharterQuoteDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        //CopytripID = cs.GetCopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, true, isAutoDispatch, IsRevision).FirstOrDefault();
                        CopyQuoteID = cs.GetCQCopyQuoteDetails(CQFileID, CQMainID, UserName, CustomerID).FirstOrDefault();
                        //return (long)ret;

                        ReturnValue = GetCQRequestByID((long)CQFileID, false);

                        if (ReturnValue.ReturnFlag)
                        {
                            //ReturnValue.EntityList[0].State = TripEntityState.Modified;
                            CQFile File = ReturnValue.EntityList[0];

                            File.State = CQRequestEntityState.Modified;

                            #region EXCEPTIONS


                            List<CQException> ExceptionList = new List<CQException>();
                            CharterQuoteValidationManager ValidMgr = new CharterQuoteValidationManager();

                            if (File.CQMains != null)
                            {
                                //Defect : 2565
                                CQMain cqMain = new CQMain();
                                cqMain = File.CQMains.Where(x => x.CQMainID == CopyQuoteID).SingleOrDefault();
                                if (cqMain != null)
                                {
                                    decimal _creditLimit = 0.0M;
                                    if (File.Credit != null)
                                        _creditLimit = (decimal)File.Credit;

                                    var validateResult = ValidMgr.ValidateBusinessRules(File, cqMain, false);

                                    if (validateResult != null && validateResult.ReturnFlag == true && validateResult.EntityList.Count > 0)
                                    {
                                        ExceptionList = validateResult.EntityList;

                                        // Bind the Exception List into Trip Object
                                        if (ExceptionList.Count > 0)
                                        {
                                            //Defect : 2565
                                            CQMain originalcqMain = cs.CQMains
                   .Where(c => c.CQMainID == cqMain.CQMainID)
                   .SingleOrDefault();
                                            if (originalcqMain != null)
                                            {
                                                cs.CQMains.Attach(originalcqMain);
                                                cs.LoadProperty(originalcqMain, c => c.CQExceptions);
                                                //cqMain.CQExceptions.Clear();

                                                foreach (CQException Exception in ExceptionList)
                                                {

                                                    CQException ExceptionObj = new CQException();
                                                    if (cqMain.CQMainID != 0)
                                                    {
                                                        cqMain.State = CQRequestEntityState.Modified;
                                                        ExceptionObj.CQMainID = cqMain.CQMainID;
                                                    }

                                                    //ExceptionObj.ExceptionGroup = GetSubmoduleModule(Convert.ToInt64(Exception.ExceptionGroup));
                                                    ExceptionObj.CQExceptionDescription = Exception.CQExceptionDescription;
                                                    ExceptionObj.ExceptionTemplateID = Exception.ExceptionTemplateID;
                                                    ExceptionObj.DisplayOrder = Exception.DisplayOrder;
                                                    ExceptionObj.CustomerID = UserPrincipal.Identity.CustomerID;
                                                    originalcqMain.CQExceptions.Add(ExceptionObj);
                                                    cs.ObjectStateManager.ChangeObjectState(ExceptionObj, System.Data.EntityState.Added);
                                                }
                                            }
                                        }
                                    }
                                }

                            }



                            #endregion

                            cs.SaveChanges();


                            ReturnValue.EntityInfo = File;
                            ReturnValue.ReturnFlag = true;

                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }


        public ReturnValue<Data.CharterQuote.CQFile> GetCQExpressCopyQuoteDetail(Int64 CQFileID)
        {
            ReturnValue<CQFile> ReturnValue = new ReturnValue<CQFile>();
            CharterQuoteDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopyQuoteID = new long();
                    CopyQuoteID = 0;
                    using (cs = new CharterQuoteDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        //CopytripID = cs.GetCopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, true, isAutoDispatch, IsRevision).FirstOrDefault();
                        cs.GetCopyExpressQuoteDetails(CQFileID, UserName, CustomerID);
                        //return (long)ret;

                        //ReturnValue = GetCQRequestByID((long)CopyQuoteID);

                        //if (ReturnValue.ReturnFlag)
                        //{
                        //    //ReturnValue.EntityList[0].State = TripEntityState.Modified;
                        //    CQFile File = ReturnValue.EntityList[0];

                        //    File.State = CQRequestEntityState.Modified;

                        //    #region EXCEPTIONS


                        //    List<CQException> ExceptionList = new List<CQException>();
                        //    CharterQuoteValidationManager ValidMgr = new CharterQuoteValidationManager();

                        //    if (File.CQMains != null)
                        //    {
                        //        foreach (CQMain cqMain in File.CQMains)
                        //        {
                        //            decimal _creditLimit = 0.0M;
                        //            if (File.Credit != null)
                        //                _creditLimit = (decimal)File.Credit;

                        //            var validateResult = ValidMgr.ValidateBusinessRules(cqMain, _creditLimit, false);

                        //            if (validateResult != null && validateResult.ReturnFlag == true && validateResult.EntityList.Count > 0)
                        //            {
                        //                ExceptionList = validateResult.EntityList;

                        //                // Bind the Exception List into Trip Object
                        //                if (ExceptionList.Count > 0)
                        //                {
                        //                    //cqMain.CQExceptions.Clear();

                        //                    foreach (CQException Exception in ExceptionList)
                        //                    {

                        //                        CQException ExceptionObj = new CQException();
                        //                        if (cqMain.CQMainID != 0)
                        //                        {
                        //                            cqMain.State = CQRequestEntityState.Modified;
                        //                            ExceptionObj.CQMainID = cqMain.CQMainID;
                        //                        }

                        //                        //ExceptionObj.ExceptionGroup = GetSubmoduleModule(Convert.ToInt64(Exception.ExceptionGroup));
                        //                        ExceptionObj.CQExceptionDescription = Exception.CQExceptionDescription;
                        //                        ExceptionObj.ExceptionTemplateID = Exception.ExceptionTemplateID;
                        //                        ExceptionObj.DisplayOrder = Exception.DisplayOrder;
                        //                        ExceptionObj.CustomerID = UserPrincipal.Identity.CustomerID;
                        //                        cqMain.CQExceptions.Add(ExceptionObj);
                        //                        //Container.ObjectStateManager.ChangeObjectState(ExceptionObj, System.Data.EntityState.Added);
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }



                        //#endregion

                        cs.SaveChanges();


                        // ReturnValue.EntityInfo = File;
                        ReturnValue.ReturnFlag = true;


                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        #region Preflight Trip History

        public ReturnValue<Data.CharterQuote.CQHistory> GetHistory(long CQFileID)
        {
            ReturnValue<Data.CharterQuote.CQHistory> ret = null;
            CharterQuoteDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.CharterQuote.CQHistory>();
                    using (cs = new CharterQuoteDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetHistory(CustomerID, CQFileID).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }
        #endregion

        public ReturnValue<Data.CharterQuote.CQFile> GetCQQuotetoPreflight(Int64 CQFileID, Int64 CQMainID, Boolean IsAutoDispatch, string TripStatus, Boolean IsRevision)
        {
            ReturnValue<CQFile> ReturnValue = new ReturnValue<CQFile>();
            CharterQuoteDataModelContainer cs = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQFileID, CQMainID, IsAutoDispatch, TripStatus, IsRevision))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    long? CopyQuoteID = new long();
                    CopyQuoteID = 0;
                    using (cs = new CharterQuoteDataModelContainer())
                    {

                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        //CopytripID = cs.GetCopyTripDetails(DepartDate, tripID, customerID, IsLogisticsToBeCopied, IsCrewToBeCopied, IsPaxToBeCopied, true, isAutoDispatch, IsRevision).FirstOrDefault();
                        CopyQuoteID = cs.GetCopyCQtoPreflight(CQFileID, CQMainID, UserName, CustomerID, IsAutoDispatch, TripStatus, IsRevision).FirstOrDefault();
                        //return (long)ret;

                        ReturnValue = GetCQRequestByID((long)CopyQuoteID, false);

                        if (ReturnValue.ReturnFlag)
                        {
                            //ReturnValue.EntityList[0].State = TripEntityState.Modified;
                            CQFile File = ReturnValue.EntityList[0];

                            File.State = CQRequestEntityState.Modified;

                            #region EXCEPTIONS


                            List<CQException> ExceptionList = new List<CQException>();
                            CharterQuoteValidationManager ValidMgr = new CharterQuoteValidationManager();

                            if (File.CQMains != null)
                            {
                                foreach (CQMain cqMain in File.CQMains)
                                {
                                    decimal _creditLimit = 0.0M;
                                    if (File.Credit != null)
                                        _creditLimit = (decimal)File.Credit;

                                    var validateResult = ValidMgr.ValidateBusinessRules(File, cqMain, false);

                                    if (validateResult != null && validateResult.ReturnFlag == true && validateResult.EntityList.Count > 0)
                                    {
                                        ExceptionList = validateResult.EntityList;

                                        // Bind the Exception List into Trip Object
                                        if (ExceptionList.Count > 0)
                                        {
                                            //cqMain.CQExceptions.Clear();

                                            foreach (CQException Exception in ExceptionList)
                                            {

                                                CQException ExceptionObj = new CQException();
                                                if (cqMain.CQMainID != 0)
                                                {
                                                    cqMain.State = CQRequestEntityState.Modified;
                                                    ExceptionObj.CQMainID = cqMain.CQMainID;
                                                }

                                                //ExceptionObj.ExceptionGroup = GetSubmoduleModule(Convert.ToInt64(Exception.ExceptionGroup));
                                                ExceptionObj.CQExceptionDescription = Exception.CQExceptionDescription;
                                                ExceptionObj.ExceptionTemplateID = Exception.ExceptionTemplateID;
                                                ExceptionObj.DisplayOrder = Exception.DisplayOrder;
                                                ExceptionObj.CustomerID = UserPrincipal.Identity.CustomerID;
                                                cqMain.CQExceptions.Add(ExceptionObj);
                                                //Container.ObjectStateManager.ChangeObjectState(ExceptionObj, System.Data.EntityState.Added);
                                            }
                                        }
                                    }
                                }
                            }



                            #endregion

                            cs.SaveChanges();


                            ReturnValue.EntityInfo = File;
                            ReturnValue.ReturnFlag = true;

                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        public void DeleteQuoteImageFiles(Int64 CQMainID)
        {
            CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQMainID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Container = new CharterQuoteDataModelContainer())
                    {
                        Container.DeleteQuoteImageFiles(CQMainID, CustomerID);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }

        public ReturnValue<Data.CharterQuote.GetQuoteByFileID> GetQuoteByFileID(Int64 CQFileID)
        {
            ReturnValue<Data.CharterQuote.GetQuoteByFileID> objCharterQuote = new ReturnValue<Data.CharterQuote.GetQuoteByFileID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCharterQuote.EntityList = objContainer.GetQuoteByFileID(CQFileID, CustomerID).ToList();
                    objCharterQuote.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCharterQuote;
        }

        public ReturnValue<Data.CharterQuote.GetCQLegsByQuoteID> GetCQLegsByQuoteID(Int64 CQMainID)
        {
            ReturnValue<Data.CharterQuote.GetCQLegsByQuoteID> objCharterQuote = new ReturnValue<Data.CharterQuote.GetCQLegsByQuoteID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCharterQuote.EntityList = objContainer.GetCQLegsByQuoteID(CQMainID, CustomerID).ToList();
                    objCharterQuote.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCharterQuote;
        }

        public ReturnValue<Data.CharterQuote.GetCQReportHeader> GetCQReportHeaderList()
        {
            ReturnValue<Data.CharterQuote.GetCQReportHeader> ret = new ReturnValue<Data.CharterQuote.GetCQReportHeader>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetCQReportHeader(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.CharterQuote.GetCQReportDetail> GetCQReportDetail()
        {
            ReturnValue<Data.CharterQuote.GetCQReportDetail> ret = new ReturnValue<Data.CharterQuote.GetCQReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetCQReportDetail(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.CharterQuote.CQReportHeader> Add(Data.CharterQuote.CQReportHeader oCQReportHeader)
        {
            ReturnValue<Data.CharterQuote.CQReportHeader> objCQReportHeader = new ReturnValue<Data.CharterQuote.CQReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer())
                    {
                        objContainer.AddToCQReportHeaders(oCQReportHeader);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                        objCQReportHeader.EntityInfo = oCQReportHeader;
                        objCQReportHeader.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCQReportHeader;
        }

        public ReturnValue<Data.CharterQuote.CQReportHeader> Update(Data.CharterQuote.CQReportHeader oCQReportHeader)
        {
            ReturnValue<Data.CharterQuote.CQReportHeader> objCQReportHeader = new ReturnValue<Data.CharterQuote.CQReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer())
                    {
                        objContainer.CQReportHeaders.Attach(oCQReportHeader);
                        Common.EMCommon.SetAllModified<Data.CharterQuote.CQReportHeader>(oCQReportHeader, objContainer);
                        objContainer.SaveChanges();
                        objCQReportHeader.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCQReportHeader;
        }

        public ReturnValue<Data.CharterQuote.CQReportHeader> Delete(Data.CharterQuote.CQReportHeader oCQReportHeader)
        {
            ReturnValue<Data.CharterQuote.CQReportHeader> objCQReportHeader = new ReturnValue<Data.CharterQuote.CQReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportHeader))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    objContainer.CQReportHeaders.Attach(oCQReportHeader);
                    //objContainer.AttachTo(typeof(Data.CharterQuote.CQReportHeader).Name, oCQReportHeader);
                    objContainer.CQReportHeaders.DeleteObject(oCQReportHeader);
                    objContainer.SaveChanges();
                    objCQReportHeader.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCQReportHeader;
        }

        public ReturnValue<Data.CharterQuote.CQReportDetail> Add(Data.CharterQuote.CQReportDetail oCQReportDetail)
        {
            ReturnValue<Data.CharterQuote.CQReportDetail> objCQReportDetail = new ReturnValue<Data.CharterQuote.CQReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportDetail))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer())
                    {
                        objContainer.AddToCQReportDetails(oCQReportDetail);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                        objCQReportDetail.EntityInfo = oCQReportDetail;
                        objCQReportDetail.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCQReportDetail;
        }

        public ReturnValue<Data.CharterQuote.CQReportDetail> Update(Data.CharterQuote.CQReportDetail oCQReportDetail)
        {
            ReturnValue<Data.CharterQuote.CQReportDetail> objCQReportDetail = new ReturnValue<Data.CharterQuote.CQReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportDetail))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer())
                    {
                        objContainer.CQReportDetails.Attach(oCQReportDetail);
                        Common.EMCommon.SetAllModified<Data.CharterQuote.CQReportDetail>(oCQReportDetail, objContainer);
                        objContainer.SaveChanges();
                        objCQReportDetail.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCQReportDetail;
        }

        public ReturnValue<Data.CharterQuote.CQReportDetail> Delete(Data.CharterQuote.CQReportDetail oCQReportDetail)
        {
            ReturnValue<Data.CharterQuote.CQReportDetail> objCQReportDetail = new ReturnValue<Data.CharterQuote.CQReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCQReportDetail))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    objContainer.CQReportDetails.Attach(oCQReportDetail);
                    //objContainer.AttachTo(typeof(Data.CharterQuote.CQReportDetail).Name, oCQReportDetail);
                    objContainer.CQReportDetails.DeleteObject(oCQReportDetail);
                    objContainer.SaveChanges();
                    objCQReportDetail.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCQReportDetail;
        }

        /// <summary>
        /// Method for Getting FileWarehouse Details by ID
        /// Fix for #2983
        /// </summary>
        /// <param name="WareHouseID">Pass FileWarehouseID</param>
        /// <returns>Returns FileWarehouse Details List</returns>
        public ReturnValue<Data.CharterQuote.GetFileWarehouseByID> GetFileWarehouseByID(Int64 WareHouseID)
        {
            ReturnValue<Data.CharterQuote.GetFileWarehouseByID> retValue = new ReturnValue<Data.CharterQuote.GetFileWarehouseByID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(WareHouseID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.CharterQuote.CharterQuoteDataModelContainer objContainer = new Data.CharterQuote.CharterQuoteDataModelContainer();
                    retValue.EntityList = objContainer.GetFileWarehouseByID(CustomerID, WareHouseID).ToList();
                    retValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return retValue;
        }
    }
}
