﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using FlightPak.Data.CharterQuote;
using FlightPak.Business.Common;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.CharterQuote
{
    /// <summary>
    /// Charterquote Manager class to manage logging CharterQuote activities
    /// </summary>
    public partial class CharterQuoteValidationManager : BaseManager
    {

        #region "Business Errors"
        public ReturnValue<ExceptionTemplate> GetCQBusinessErrorsList(long ModuleID)
        {
            ReturnValue<Data.CharterQuote.ExceptionTemplate> Results = null;
            Data.CharterQuote.CharterQuoteDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                Results = new ReturnValue<Data.CharterQuote.ExceptionTemplate>();
                Container = new CharterQuoteDataModelContainer();
                Container.ContextOptions.ProxyCreationEnabled = false;

                Container.ContextOptions.LazyLoadingEnabled = false;
                Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                Container.ContextOptions.ProxyCreationEnabled = false;

                List<ExceptionTemplate> BusinessErrors = (from Exc in Container.ExceptionTemplates
                                                          where (Exc.ModuleID == ModuleID)
                                                          select Exc).ToList();

                Int32[] CQBusErrsRef = (Int32[])Enum.GetValues(typeof(CQBusinessErrorReference));
                Int32[] CQBusErrsRefMod = (Int32[])Enum.GetValues(typeof(CQBusinessErrorModule));
                Int32[] CQBusErrsRefSubMod = (Int32[])Enum.GetValues(typeof(CQBusinessErrorSubModule));

                BusinessErrors.ToList().ForEach(delegate(ExceptionTemplate Exptempl)
                {
                    Exptempl.CQErrorReference = (CQBusinessErrorReference)CQBusErrsRef.Single(ErrorTempl => ErrorTempl == Exptempl.ExceptionTemplateID);
                    Exptempl.CQErrorReferenceModule = (CQBusinessErrorModule)CQBusErrsRefMod.Single(ErrorTempl => ErrorTempl == Exptempl.ModuleID);
                    Exptempl.CQErrorReferenceSubModule = (CQBusinessErrorSubModule)CQBusErrsRefSubMod.Single(ErrorTempl => ErrorTempl == Exptempl.SubModuleID);
                });
                Results.EntityList = BusinessErrors;

                if (Results.EntityList != null && Results.EntityList.Count > 0)
                    Results.ReturnFlag = true;
                else
                    Results.ReturnFlag = false;
            }
            return Results;
        }
        public CQException BuildBusErrorObject(CQException CQQuoteExcep, ExceptionTemplate exceptionTemplate, CQMain cqMain, long? legNum, bool IsOverrideDesc, bool ShowQuoteNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {



                CQQuoteExcep.CQMainID = cqMain.CQMainID;
                CQQuoteExcep.CustomerID = CustomerID;
                string QuoteNumStr = string.Empty;
                if (IsOverrideDesc)
                {
                    if (ShowQuoteNum && cqMain.QuoteNUM != null)
                        QuoteNumStr = "Quote " + cqMain.QuoteNUM.ToString() + " : ";

                    if (legNum != null)
                        CQQuoteExcep.CQExceptionDescription = QuoteNumStr + "Leg " + legNum.ToString() + " : " + CQQuoteExcep.CQExceptionDescription;
                    else
                        CQQuoteExcep.CQExceptionDescription = QuoteNumStr + CQQuoteExcep.CQExceptionDescription;

                }
                else
                {
                    if (ShowQuoteNum && cqMain.QuoteNUM != null)
                        QuoteNumStr = "Quote " + cqMain.QuoteNUM.ToString() + " : ";
                    if (legNum != null)
                        CQQuoteExcep.CQExceptionDescription = QuoteNumStr + "Leg " + legNum.ToString() + " : " + exceptionTemplate.ExceptionTemplate1;
                    else
                        CQQuoteExcep.CQExceptionDescription = QuoteNumStr + exceptionTemplate.ExceptionTemplate1;

                }
                CQQuoteExcep.ExceptionTemplateID = exceptionTemplate.ExceptionTemplateID;
                CQQuoteExcep.DisplayOrder = exceptionTemplate.DisplayOrder;
                CQQuoteExcep.LastUpdUID = UserPrincipal.Identity.Name;
                //postflightTripExcep.LastUpdTS = DateTime.UtcNow;
                CQQuoteExcep.ExceptionTemplate = exceptionTemplate;

                return CQQuoteExcep;
            }
        }
        #endregion


        #region "Validate CQ Main"

        public ReturnValue<CQException> ValidateBusinessRules(Data.CharterQuote.CQFile cqFile, Data.CharterQuote.CQMain cqMain, bool IsMandatory)
        {
            ReturnValue<CQException> returnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (cqMain != null)
                {
                    //long CQMainID = 0;

                    //CQLeg[] legsCollection = new CQLeg[cqMain.CQLegs.Count];

                    //cqMain.CQLegs.CopyTo(legsCollection, 0);

                    //legsCollection.ToList().ForEach(delegate(CQLeg leg)
                    //{
                    //    leg.PostflightPassengers = null;
                    //    leg.PostflightSIFLs = null;
                    //    leg.PostflightCrews = null;
                    //});

                    List<CQException> ExceptionList = new List<CQException>();
                    CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    returnValue = new ReturnValue<CQException>();

                    var Exceptionlist = GetCQBusinessErrorsList(10001);
                    List<ExceptionTemplate> TemplateList = new List<ExceptionTemplate>();
                    if (Exceptionlist.ReturnFlag)
                    {
                        TemplateList = Exceptionlist.EntityList;
                    }


                    List<ExceptionTemplate> CQ_Main_TemplateList = TemplateList.Where(mainlist => mainlist.CQErrorReferenceSubModule == CQBusinessErrorSubModule.CQMain && mainlist.Severity == (IsMandatory ? 1 : mainlist.Severity)).ToList();
                    List<ExceptionTemplate> CQ_Leg_TemplateList = TemplateList.Where(leglist => leglist.CQErrorReferenceSubModule == CQBusinessErrorSubModule.CQLeg && leglist.Severity == (IsMandatory ? 1 : leglist.Severity)).ToList();

                    //List<ExceptionTemplate> PO_Pax_TemplateList = TemplateList.Where(mainlist => mainlist.CQErrorReferenceSubModule == CQBusinessErrorSubModule.Passenger).ToList();
                    //List<ExceptionTemplate> PO_Crew_TemplateList = TemplateList.Where(mainlist => mainlist.CQErrorReferenceSubModule == CQBusinessErrorSubModule.Crew).ToList();
                    //List<ExceptionTemplate> PO_Expense_TemplateList = TemplateList.Where(mainlist => mainlist.CQErrorReferenceSubModule == CQBusinessErrorSubModule.Expense).ToList();


                    // Check Validations Main
                    ExceptionList.AddRange(ValidateMain(cqFile, cqMain, CQ_Main_TemplateList));

                    // Check Validation in Legs
                    if (cqMain.CQLegs != null && cqMain.CQLegs.Count > 0)
                    {
                        ExceptionList.AddRange(ValidateLegs(cqMain, cqMain.CQLegs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList(), CQ_Leg_TemplateList));
                    }

                    if (returnValue != null)
                    {
                        if (ExceptionList.Count > 0)
                        {
                            returnValue.ReturnFlag = true;
                        }
                        else
                        {
                            returnValue.ReturnFlag = false;
                        }

                        returnValue.EntityList = ExceptionList;
                    }
                    CQ_Main_TemplateList = null;
                    CQ_Leg_TemplateList = null;
                    //PO_Pax_TemplateList = null;
                    //PO_Crew_TemplateList = null;
                    //PO_Expense_TemplateList = null;
                }
                else
                {
                    if (returnValue != null)
                        returnValue.ReturnFlag = false;
                }

                return returnValue;
            }
        }

        public ReturnValue<CQException> ValidateMain(CQFile cqFile, CQMain cqMain)
        {
            ReturnValue<CQException> returnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (cqMain != null)
                {
                    List<CQException> ExceptionList = new List<CQException>();
                    CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();
                    returnValue = new ReturnValue<CQException>();
                    List<ExceptionTemplate> TemplateList = GetCQBusinessErrorsList((long)CQBusinessErrorModule.CharterQuote).EntityList;

                    List<ExceptionTemplate> CQ_Main_TemplateList = TemplateList.Where(mainlist => mainlist.CQErrorReferenceSubModule == CQBusinessErrorSubModule.CQMain).ToList();

                    // Check Validations
                    ExceptionList.AddRange(ValidateMain(cqFile, cqMain, CQ_Main_TemplateList));

                    if (ExceptionList.Count > 0)
                        returnValue.ReturnFlag = true;
                    else
                        returnValue.ReturnFlag = false;

                    returnValue.EntityList = ExceptionList;

                    CQ_Main_TemplateList = null;
                }
                else
                {
                    returnValue.ReturnFlag = false;
                }

                return returnValue;
            }
        }

        public ReturnValue<CQException> ValidateLegs(CQMain cqMain, List<CQLeg> legs)
        {
            ReturnValue<CQException> returnValue = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                if (cqMain != null && legs != null && legs.Count > 0)
                {
                    List<CQException> ExceptionList = new List<CQException>();
                    CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();
                    returnValue = new ReturnValue<CQException>();
                    List<ExceptionTemplate> TemplateList = GetCQBusinessErrorsList(10009).EntityList;

                    List<ExceptionTemplate> CQ_Leg_TemplateList = TemplateList.Where(mainlist => mainlist.CQErrorReferenceSubModule != CQBusinessErrorSubModule.CQLeg).ToList();

                    // Check Validations
                    ExceptionList.AddRange(ValidateLegs(cqMain, legs, CQ_Leg_TemplateList));

                    if (ExceptionList.Count > 0)
                        returnValue.ReturnFlag = true;
                    else
                        returnValue.ReturnFlag = false;

                    returnValue.EntityList = ExceptionList;

                    CQ_Leg_TemplateList = null;
                }
                else
                {
                    returnValue.ReturnFlag = false;
                }
                return returnValue;
            }
        }

        /// <summary>
        /// Method to Validate Main information
        /// </summary>       
        private List<CQException> ValidateMain(CQFile cqFile, CQMain cqMain, List<ExceptionTemplate> CQ_Main_TemplateList)
        {
            CQException ExceptionObj = null;
            List<CQException> ExceptionList = new List<CQException>();

            Data.CharterQuote.CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();
            Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
            Container.ContextOptions.ProxyCreationEnabled = false;

            foreach (ExceptionTemplate exception in CQ_Main_TemplateList)
            {
                if (cqFile != null && cqFile.CQCustomerID != null && cqFile.CQCustomerID > 0) // Fix for UW-1153 (8166)
                {
                    if (exception.ExceptionTemplateID == (long)CQBusinessErrorReference.OverCustomerCreditLimit) //Over Customer Credit Limit. (Severity : 2)
                    {
                        if (cqMain.QuoteTotal != null && (cqMain.QuoteTotal > cqFile.Credit))
                        {
                            ExceptionObj = new CQException();
                            string Quotenumstr = cqMain.QuoteNUM.ToString().PadLeft(5, '0');
                            Quotenumstr = Quotenumstr.Substring(Quotenumstr.Length - 3, 3);
                            ExceptionObj.DisplayOrder = Quotenumstr + "000000000" + "001";
                            ExceptionObj.CQExceptionDescription = exception.ExceptionTemplate1;
                            ExceptionObj.CQExceptionDescription = ExceptionObj.CQExceptionDescription.Replace("<MainTotalQuote>", Math.Round((decimal)cqMain.QuoteTotal, 2).ToString());
                            ExceptionObj.CQExceptionDescription = ExceptionObj.CQExceptionDescription.Replace("<FileTotalQuote>", Math.Round((decimal)cqFile.Credit, 2).ToString());
                            ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, cqMain, null, true, true));
                        }
                    }
                }

                if (exception.ExceptionTemplateID == (long)CQBusinessErrorReference.VendorCodeReq) //Vendor required when source 1 or 2. (Severity : 1)
                {
                    if (cqMain.CQSource != null && (cqMain.CQSource == 1 && (cqMain.VendorID == null || cqMain.VendorID == 0)))
                    {
                        ExceptionObj = new CQException();
                        string Quotenumstr = cqMain.QuoteNUM.ToString().PadLeft(5, '0');
                        Quotenumstr = Quotenumstr.Substring(Quotenumstr.Length - 3, 3);
                        ExceptionObj.DisplayOrder = Quotenumstr + "000000000" + "002";
                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, cqMain, null, false, true));
                    }
                }

                if (exception.ExceptionTemplateID == (long)CQBusinessErrorReference.TailNumberReq) //TailNumber required when source 1 or 2 . (Severity : 1)
                {
                    if (cqMain.CQSource != null && (
                            (cqMain.CQSource == 1 && (cqMain.FleetID == null || cqMain.FleetID == 0)) || (cqMain.CQSource == 2 && (cqMain.FleetID == null || cqMain.FleetID == 0))
                           ))
                    {
                        ExceptionObj = new CQException();
                        string Quotenumstr = cqMain.QuoteNUM.ToString().PadLeft(5, '0');
                        Quotenumstr = Quotenumstr.Substring(Quotenumstr.Length - 3, 3);
                        ExceptionObj.DisplayOrder = Quotenumstr + "000000000" + "003";
                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, cqMain, null, false, true));
                    }
                }

                if (exception.ExceptionTemplateID == (long)CQBusinessErrorReference.TypeCodeReq) //Aicraft Required when source is 3. (Severity : 1)
                {
                    if (cqMain.CQSource != null && (cqMain.CQSource == 3 && (cqMain.AircraftID == null || cqMain.AircraftID == 0)))
                    {
                        ExceptionObj = new CQException();
                        string Quotenumstr = cqMain.QuoteNUM.ToString().PadLeft(5, '0');
                        Quotenumstr = Quotenumstr.Substring(Quotenumstr.Length - 3, 3);
                        ExceptionObj.DisplayOrder = Quotenumstr + "000000000" + "004";
                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, cqMain, null, false, true));
                    }
                }

                if (exception.ExceptionTemplateID == (long)CQBusinessErrorReference.QuoteHasNoLeg) //Quote has no leg (severity : 1)
                {
                    if ((cqMain.CQLegs != null && cqMain.CQLegs.Where(x => x.IsDeleted == false).Count() == 0) || cqMain.CQLegs == null)
                    {
                        ExceptionObj = new CQException();
                        string Quotenumstr = cqMain.QuoteNUM.ToString().PadLeft(5, '0');
                        Quotenumstr = Quotenumstr.Substring(Quotenumstr.Length - 3, 3);
                        ExceptionObj.DisplayOrder = Quotenumstr + "000000000" + "005";
                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, cqMain, null, false, true));
                    }
                }
            }
            return ExceptionList;
        }

        /// <summary>
        /// Method to Validate Legs information
        /// </summary>     
        private List<CQException> ValidateLegs(CQMain cqMain, List<CQLeg> legs, List<ExceptionTemplate> CQ_Leg_TemplateList)
        {
            CQException ExceptionObj = null;
            List<CQException> ExceptionList = new List<CQException>();
            long logID = 0;
            Data.CharterQuote.CharterQuoteDataModelContainer Container = new CharterQuoteDataModelContainer();
            Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
            Container.ContextOptions.ProxyCreationEnabled = false;



            if (legs != null)
            {
                var DepartAirportLists = new List<Airport>();
                var ArrAirportLists = new List<Airport>();
                var FleetLists = new List<Fleet>();
                double minRunWay = 0, maxRunWay = 0;
                List<CQLeg> cqlegs = legs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM).ToList();
                for (int i = 0; i < cqlegs.Count; i++)
                {
                    string length = ("000" + cqlegs[i].LegNUM.ToString());
                    length = length.Substring(length.Count() - 3, 3);

                    foreach (ExceptionTemplate exptemp in CQ_Leg_TemplateList)
                    {
                        // legnum + 000000001 + 001
                        #region "Arrival Airport Inactive"
                        if (cqlegs[i].AAirportID != null)
                        {
                            if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.ArrivalAirportInActive))
                            {
                                long arrivalAirportId = (long)cqlegs[i].AAirportID;
                                ArrAirportLists = (from arrLists in Container.Airports
                                                   where (arrLists.AirportID == arrivalAirportId) //AirportID & ArrivalICAOID are equal
                                                   select arrLists).ToList();
                                if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                {
                                    if (ArrAirportLists[0].IsInActive == true)
                                    {
                                        ExceptionObj = new CQException();

                                        ExceptionObj.DisplayOrder = length + "000000001" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                    }
                                }
                            }
                        }
                        #endregion

                        //// legnum + 000000002 + 001
                        #region "Depart Airport Inactive"
                        if (cqlegs[i].DAirportID != null)
                        {
                            if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.DepartureAirportInActive))
                            {
                                long departAirportId = (long)cqlegs[i].DAirportID;
                                DepartAirportLists = (from departLists in Container.Airports
                                                      where (departLists.AirportID == departAirportId)
                                                      select departLists).ToList();
                                if (DepartAirportLists != null && DepartAirportLists.Count > 0)
                                {
                                    if (DepartAirportLists[0].IsInActive == true)
                                    {
                                        ExceptionObj = new CQException();

                                        ExceptionObj.DisplayOrder = length + "000000002" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                    }
                                }
                            }
                        }
                        #endregion


                        if (i + 1 < cqlegs.Count)
                        {

                            // legnum + 000000003 + 001
                            #region Leg Arrival Date over laps with Next Departure date
                            if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.CurrentLegArrivalDateTimeOverlapsNextLegDepartureDateTime))
                            {
                                if (cqlegs[i + 1].DepartureDTTMLocal != null && cqlegs[i].ArrivalDTTMLocal != null)
                                {
                                    if (cqlegs[i].ArrivalDTTMLocal > cqlegs[i + 1].DepartureDTTMLocal)
                                    {
                                        string desc = string.Empty;
                                        ExceptionObj = new CQException();
                                        ExceptionObj.CQExceptionDescription = exptemp.ExceptionTemplate1;
                                        desc = exptemp.ExceptionTemplate1;
                                        desc = desc.Replace("<legnum1>", cqlegs[i].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", cqlegs[i].ArrivalDTTMLocal));
                                        desc = desc.Replace("<legnum2>", cqlegs[i + 1].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", cqlegs[i + 1].DepartureDTTMLocal));
                                        ExceptionObj.CQExceptionDescription = desc;
                                        ExceptionObj.DisplayOrder = length + "000000003" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, null, true, false));
                                    }
                                }

                            }
                            #endregion

                            // legnum + 000000004 + 001
                            #region Leg Arrival Date over laps with Next Departure date
                            if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.CurrentLegLocalDepartDateTimeOverlapsPrevLegLocalArrivalDateTime))
                            {
                                string Nextleglength = ("000" + cqlegs[i + 1].LegNUM.ToString());
                                Nextleglength = Nextleglength.Substring(length.Count() - 3, 3);

                                if (cqlegs[i + 1].DepartureDTTMLocal != null && cqlegs[i].ArrivalDTTMLocal != null)
                                {
                                    if (cqlegs[i].ArrivalDTTMLocal > cqlegs[i + 1].DepartureDTTMLocal)
                                    {
                                        string desc = string.Empty;
                                        ExceptionObj = new CQException();
                                        ExceptionObj.CQExceptionDescription = exptemp.ExceptionTemplate1;
                                        desc = exptemp.ExceptionTemplate1;
                                        desc = desc.Replace("<legnum1>", cqlegs[i + 1].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", cqlegs[i + 1].DepartureDTTMLocal));
                                        desc = desc.Replace("<legnum2>", cqlegs[i].LegNUM.ToString() + ": " + String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", cqlegs[i].ArrivalDTTMLocal));
                                        ExceptionObj.CQExceptionDescription = desc;
                                        ExceptionObj.DisplayOrder = Nextleglength + "000000004" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, null, true, false));
                                    }
                                }

                            }
                            #endregion

                        }

                        // legnum + 000000005 + 001
                        #region Distance Beyond Aircraft Capability
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.LegDistbeyondAircraftCapability))
                        {
                            if (cqMain.AircraftID != null)
                            {
                                double aircrPS1 = 0.0, aircrPS2 = 0.0, aircrPS3 = 0.0;
                                var airCraftLists = (from airCrafts in Container.Aircraft
                                                     where (airCrafts.AircraftID == (long)cqMain.AircraftID)
                                                     select airCrafts).ToList();
                                if (airCraftLists != null) //&& airCraftLists.Count > 0
                                {
                                    aircrPS1 = ((double)(airCraftLists[0].PowerSettings1TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings1HourRange));
                                    aircrPS2 = ((double)(airCraftLists[0].PowerSettings2TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings2HourRange));
                                    aircrPS3 = ((double)(airCraftLists[0].PowerSettings3TrueAirSpeed)) * ((double)(airCraftLists[0].PowerSettings3HourRange));
                                    if (cqlegs[i].Distance != null)
                                    {

                                        if (cqlegs[i].PowerSetting == "1" && aircrPS1 < (double)cqlegs[i].Distance)
                                        {
                                            ExceptionObj = new CQException();

                                            ExceptionObj.DisplayOrder = length + "000000005" + "001";
                                            ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));

                                        }
                                        if (cqlegs[i].PowerSetting == "2" && aircrPS2 < (double)cqlegs[i].Distance)
                                        {
                                            ExceptionObj = new CQException();

                                            ExceptionObj.DisplayOrder = length + "000000005" + "001";
                                            ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                        }
                                        if (cqlegs[i].PowerSetting == "3" && aircrPS3 < (double)cqlegs[i].Distance)
                                        {
                                            ExceptionObj = new CQException();

                                            ExceptionObj.DisplayOrder = length + "000000005" + "001";
                                            ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        // legnum + 000000006 + 001
                        #region Arrival Airport Max runway below aircaft requirements

                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.ArrivalAirportMaxRunwayBelowAircraftRequirements))
                        {

                            if (UserPrincipal.Identity.Settings.IsConflictCheck != null)
                            {
                                if (UserPrincipal.Identity.Settings.IsConflictCheck == true)
                                {
                                    if (cqMain.FleetID != null)
                                    {
                                        if (cqMain.FleetID != 0)
                                        {
                                            FleetLists = (from fleetLists in Container.Fleets
                                                          where (fleetLists.FleetID == cqMain.FleetID)
                                                          select fleetLists).ToList();

                                            if (FleetLists != null)
                                            {
                                                if (FleetLists.Count > 0)
                                                {
                                                    if (FleetLists[0].MimimumRunway != null)
                                                        //if ((double)FleetLists[0].MimimumRunway != 0.0)
                                                        minRunWay = (double)FleetLists[0].MimimumRunway;
                                                    else
                                                        minRunWay = 0.0;
                                                }
                                            }


                                            long arrivalAirportId = (long)cqlegs[i].AAirportID;
                                            ArrAirportLists = (from arrLists in Container.Airports
                                                               where (arrLists.AirportID == arrivalAirportId) //AirportID & ArrivalICAOID are equal
                                                               select arrLists).ToList();

                                            if (ArrAirportLists != null)
                                            {
                                                if (ArrAirportLists.Count > 0)
                                                {
                                                    if (ArrAirportLists[0].LongestRunway != null)
                                                        //if ((double)ArrAirportLists[0].WidthLengthRunway != 0.0)
                                                        maxRunWay = (double)ArrAirportLists[0].LongestRunway; // SAid by Sujitha
                                                    else
                                                        maxRunWay = 0.0;
                                                }
                                            }
                                            if (minRunWay > maxRunWay)
                                            {
                                                ExceptionObj = new CQException();

                                                ExceptionObj.DisplayOrder = length + "000000006" + "001";
                                                ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        #endregion

                        // legnum + 000000007 + 001
                        #region LegTime Aloft AircraftCapability
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.LegTimeAloftAircraftCapability))
                        {

                            if (cqMain.AircraftID != null)
                            {
                                var airCraftLists = (from airCrafts in Container.Aircraft
                                                     where (airCrafts.AircraftID == (long)cqMain.AircraftID)
                                                     select airCrafts).ToList();
                                if (cqlegs[i].ElapseTM != null)
                                {

                                    if (cqlegs[i].PowerSetting == "1" && (double)airCraftLists[0].PowerSettings1HourRange < (double)cqlegs[i].ElapseTM)
                                    {
                                        ExceptionObj = new CQException();

                                        ExceptionObj.DisplayOrder = length + "000000007" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                    }
                                    if (cqlegs[i].PowerSetting == "2" && (double)airCraftLists[0].PowerSettings1HourRange < (double)cqlegs[i].ElapseTM)
                                    {
                                        ExceptionObj = new CQException();

                                        ExceptionObj.DisplayOrder = length + "000000007" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                    }
                                    if (cqlegs[i].PowerSetting == "3" && (double)airCraftLists[0].PowerSettings1HourRange < (double)cqlegs[i].ElapseTM)
                                    {
                                        ExceptionObj = new CQException();

                                        ExceptionObj.DisplayOrder = length + "000000007" + "001";
                                        ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                                    }
                                }
                            }


                        }
                        #endregion


                        // legnum + 000000008 + 001
                        #region "Arrival Airport Required"
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.ArrivalIcaoReq))
                        {
                            if (cqlegs[i].AAirportID == null || cqlegs[i].AAirportID == 0)
                            {

                                ExceptionObj = new CQException();
                                ExceptionObj.DisplayOrder = length + "000000008" + "001";
                                ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                            }
                        }
                        #endregion

                        //// legnum + 000000009 + 001
                        #region "Departure Airport Required"
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.DepartIcaoReq))
                        {
                            if (cqlegs[i].DAirportID == null || cqlegs[i].DAirportID == 0)
                            {
                                ExceptionObj = new CQException();

                                ExceptionObj.DisplayOrder = length + "000000009" + "001";
                                ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                            }
                        }
                        #endregion

                        // legnum + 000000010 + 001
                        #region "Departure Date Required"
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.DepartureDateReq))
                        {
                            if (cqlegs[i].DepartureDTTMLocal == null)
                            {
                                ExceptionObj = new CQException();

                                ExceptionObj.DisplayOrder = length + "000000010" + "001";
                                ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                            }
                        }
                        #endregion

                        //// legnum + 000000011 + 001
                        #region "Arrival Date Required"
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.ArrivalDateReq))
                        {
                            if (cqlegs[i].ArrivalDTTMLocal == null)
                            {
                                ExceptionObj = new CQException();

                                ExceptionObj.DisplayOrder = length + "000000011" + "001";
                                ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, false, false));
                            }
                        }
                        #endregion

                        //// legnum + 000000012 + 001
                        #region "No. of Passenger exceeds aircraft seating capacity"
                        if (exptemp.ExceptionTemplateID == (long)(CQBusinessErrorReference.PassengerExceedsAircraftCapacity))
                        {
                            // Fix for #2320
                            if (cqMain.FleetID != null && cqMain.FleetID > 0)
                            {
                                //if (cqlegs[i].ReservationAvailable != null && cqlegs[i].ReservationAvailable < 0 && cqlegs[i].PassengerTotal != null && cqlegs[i].PassengerTotal > 0)
                                if (cqlegs[i].ReservationAvailable != null && cqlegs[i].PassengerTotal != null && cqlegs[i].PassengerTotal > cqlegs[i].ReservationAvailable) // Fix for #2320
                                {
                                    ExceptionObj = new CQException();
                                    ExceptionObj.DisplayOrder = length + "000000012" + "001";
                                    ExceptionObj.CQExceptionDescription = exptemp.ExceptionTemplate1;
                                    string desc = string.Empty;
                                    desc = exptemp.ExceptionTemplate1;
                                    desc = desc.Replace("<PassengerCount>", ((decimal)cqlegs[i].PassengerTotal).ToString());
                                    ExceptionObj.CQExceptionDescription = desc;
                                    ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exptemp, cqMain, (long)cqlegs[i].LegNUM, true, false));
                                }
                            }
                        }
                        #endregion

                    }
                }
            }

            return ExceptionList;
        }

        #endregion

    }
}
