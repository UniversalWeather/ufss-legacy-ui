﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.IMasterCatalog
{
   public interface ICustomerData
    {
        bool Add(FlightPak.Data.MasterCatalog.Customer objCustMaster);
        bool Update(FlightPak.Data.MasterCatalog.Customer objCustMaster);
        bool Delete(FlightPak.Data.MasterCatalog.Customer objCustMaster);
        List<FlightPak.Data.MasterCatalog.Customer> GetList(int tenantID);
    }
}
