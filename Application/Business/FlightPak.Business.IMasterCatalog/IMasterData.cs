﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
namespace FlightPak.Business.IMasterCatalog
{
    public interface IMasterData<T>
    {
        ReturnValue<T> GetList();
        ReturnValue<T> Add(T t);
        ReturnValue<T> Update(T t);
        ReturnValue<T> Delete(T t);
        ReturnValue<T> GetLock(T t);
    }
}
