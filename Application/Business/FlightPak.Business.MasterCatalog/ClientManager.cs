﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;

namespace FlightPak.Business.MasterCatalog
{
    public class ClientManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Client>, IMasterCatalog.ICacheable
    {
        public ReturnValue<Data.MasterCatalog.Client> GetList()
        {
            ReturnValue<Data.MasterCatalog.Client> objClient = new ReturnValue<Data.MasterCatalog.Client>();

            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            
            objContainer.ContextOptions.LazyLoadingEnabled = false;
            objContainer.ContextOptions.ProxyCreationEnabled = false;
            objClient.EntityList = objContainer.GetAllClient(CustomerID).ToList();
            //if (objClient.EntityList.Count > 0)
            //{
            //    for (int i = 0; i < objClient.EntityList.Count; i++)
            //    {
            //        objClient.EntityList[i].ClientCD = objClient.EntityList[i].ClientCD != null ? objClient.EntityList[i].ClientCD.Trim() : string.Empty;
            //    }
            //}
            objClient.ReturnFlag = true;
            return objClient;
        }

        public ReturnValue<Data.MasterCatalog.Client> Add(Data.MasterCatalog.Client client)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Client> Update(Data.MasterCatalog.Client client)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Client> Delete(Data.MasterCatalog.Client client)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Client> GetLock(Data.MasterCatalog.Client client)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
