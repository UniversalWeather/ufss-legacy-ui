﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;

namespace FlightPak.Business.MasterCatalog
{
    public class GetAllAirportManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.GetAllAirport>, IMasterCatalog.ICacheable
    {
        /// <summary>
        /// To get the values into DepartmentAuthorization page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> GetList()
        {
            ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirport>();

            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

            // Fix for recursive infinite loop
            Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.ProxyCreationEnabled = false;
            ReturnValue.EntityList = Container.GetAllAirport(CustomerID).ToList();
            ReturnValue.ReturnFlag = true;
            return ReturnValue;

        }
        /// <summary>
        /// To insert values from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> Add(Data.MasterCatalog.GetAllAirport AirportInfo)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To update the selected from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> Update(Data.MasterCatalog.GetAllAirport AirportInfo)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To update the selected record as deleted, from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> Delete(Data.MasterCatalog.GetAllAirport AirportInfo)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> GetLock(Data.MasterCatalog.GetAllAirport AirportInfo)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
