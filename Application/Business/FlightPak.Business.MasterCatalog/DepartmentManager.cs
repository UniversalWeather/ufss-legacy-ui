﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class DepartmentManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Department>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        /// <summary>
        /// To get the values for Department page from Department table
        /// </summary>
        /// <returns></returns>      
        public ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters> GetDepartmentByWithFilters(string DepartmentCD, long DepartmentID, long ClientID, bool FetchInactiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters> department = new ReturnValue<Data.MasterCatalog.GetDepartmentByWithFilters>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods through exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    department.EntityList = Container.GetDepartmentByWithFilters(CustomerID, DepartmentCD, DepartmentID, ClientID, FetchInactiveOnly).ToList();
                    department.ReturnFlag = true;
                    
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }



            return department;
        }

        #endregion
        /// <summary>
        /// To get the values for Department page from Department table
        /// </summary>
        /// <returns></returns>      
        public ReturnValue<Data.MasterCatalog.Department> GetList()
        {
            ReturnValue<Data.MasterCatalog.Department> objDepartment = new ReturnValue<Data.MasterCatalog.Department>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objDepartment.EntityList = objContainer.GetAllDepartment(CustomerID,ClientId).ToList();
                    objDepartment.ReturnFlag = true;
                    return objDepartment;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDepartment;
            }
        }
        /// <summary>
        /// To insert values from Department page to Department table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Department> Add(Data.MasterCatalog.Department department)
        {
            ReturnValue<Data.MasterCatalog.Department> objDepartment = new ReturnValue<Data.MasterCatalog.Department>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(department))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objDepartment.ReturnFlag = base.Validate(department, ref objDepartment.ErrorMessage);
                    //For Data Anotation
                    if (objDepartment.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToDepartment(department);
                        objContainer.SaveChanges();
                        objDepartment.ReturnFlag = true;
                     //   return objDepartment;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDepartment;
            }
        }
        /// <summary>
        ///  To update values from Deprtment page to Department table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Department> Update(Data.MasterCatalog.Department department)
        {
            ReturnValue<Data.MasterCatalog.Department> objDepartment = new ReturnValue<Data.MasterCatalog.Department>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(department))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                     //For Data Anotation
                    objDepartment.ReturnFlag = base.Validate(department, ref objDepartment.ErrorMessage);
                    //For Data Anotation
                    if (objDepartment.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.Department.Attach(department);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Department>(department, objContainer);
                        objContainer.SaveChanges();
                        objDepartment.ReturnFlag = true;
                        //return objDepartment;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDepartment;
            }
        }
        /// <summary>
        ///  To update the selected record as deleted, from DeaprtmentCatalog page in Deaprtment table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Department> Delete(Data.MasterCatalog.Department department)
        {
            ReturnValue<Data.MasterCatalog.Department> objDepartment = new ReturnValue<Data.MasterCatalog.Department>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(department))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Department).Name, department);
                    objContainer.Department.DeleteObject(department);
                    objContainer.SaveChanges();
                    objDepartment.ReturnFlag = true;
                    return objDepartment;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDepartment;
            }
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Department> GetLock(Data.MasterCatalog.Department department)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values into DepartmentAuthorization page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDepartments> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllDepartments> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllDepartments>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllDepartments(CustomerID,ClientId).ToList();
                    //if (ReturnValue.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < ReturnValue.EntityList.Count; i++)
                    //    {
                    //        ReturnValue.EntityList[i].DepartmentCD = ReturnValue.EntityList[i].DepartmentCD != null ? ReturnValue.EntityList[i].DepartmentCD.Trim() : string.Empty;
                    //    }
                    //}
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// Added by Scheduling calendar team to get Department Entity by department Code.
        /// </summary>
        /// <param name="departmentCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult> GetDepartmentByDepartmentCD(string departmentCode)
        {
            ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult> department = new ReturnValue<Data.MasterCatalog.GetDepartmentByDepartmentCDResult>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods through exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    department.EntityList = Container.GetDepartmentByDepartmentCD(CustomerID, departmentCode,  ClientId).ToList();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }



            return department;
        }

        #region "Get Department for Client"
        public ReturnValue<Data.MasterCatalog.GetDepartmentForClient> GetDepartmentForClient()
        {
            ReturnValue<Data.MasterCatalog.GetDepartmentForClient> objDepartment = new ReturnValue<Data.MasterCatalog.GetDepartmentForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objDepartment.EntityList = objContainer.GetDepartmentForClient(CustomerID, ClientId).ToList();
                    objDepartment.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objDepartment;
        }
        #endregion

        /// <summary>
        /// Method to fetch Department record by passing DepartmentCD, CustomerID, ClientId
        /// </summary>
        /// <param name="DepartmentCD"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD> GetAllDeptByDeptCD(string DepartmentCD)
        {

            ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD> objDepartment = new ReturnValue<Data.MasterCatalog.GetAllDeptByDeptCD>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag

                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();

                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;

                    objContainer.ContextOptions.ProxyCreationEnabled = false;

                    objDepartment.EntityList = objContainer.GetAllDeptByDeptCD(DepartmentCD, CustomerID, ClientId).ToList();

                    objDepartment.ReturnFlag = true;

                    return objDepartment;

                }, FlightPak.Common.Constants.Policy.DataLayer);

                return objDepartment;

            }

        }
    }
}