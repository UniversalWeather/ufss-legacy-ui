﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class AircraftDutyTypeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.AircraftDuty>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.AircraftDuty> GetList()
        {
            ReturnValue<Data.MasterCatalog.AircraftDuty> objAirDutyType = new ReturnValue<Data.MasterCatalog.AircraftDuty>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objAirDutyType.EntityList = objContainer.GetAllAircraftDutyType(CustomerID).ToList();
                    //if (objAirDutyType.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objAirDutyType.EntityList.Count; i++)
                    //    {
                    //        objAirDutyType.EntityList[i].AircraftDutyCD = objAirDutyType.EntityList[i].AircraftDutyCD != null ? objAirDutyType.EntityList[i].AircraftDutyCD.Trim() : string.Empty;
                    //    }
                    //}
                    objAirDutyType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAirDutyType;
        }
        public ReturnValue<Data.MasterCatalog.AircraftDuty> Add(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            ReturnValue<Data.MasterCatalog.AircraftDuty> objAirDutyType = new ReturnValue<Data.MasterCatalog.AircraftDuty>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
            {
                
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objAirDutyType.ReturnFlag = base.Validate(aircraftDuty, ref objAirDutyType.ErrorMessage);
                    //For Data Anotation
                    if (objAirDutyType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();                                                
                        objContainer.AddToAircraftDuty(aircraftDuty);                        
                        objContainer.SaveChanges();
                        objAirDutyType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAirDutyType;
        }

        public ReturnValue<Data.MasterCatalog.AircraftDuty> Update(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            ReturnValue<Data.MasterCatalog.AircraftDuty> objAirDutyType = new ReturnValue<Data.MasterCatalog.AircraftDuty>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objAirDutyType.ReturnFlag = base.Validate(aircraftDuty, ref objAirDutyType.ErrorMessage);
                    //For Data Anotation
                    if (objAirDutyType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AircraftDuty.Attach(aircraftDuty);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.AircraftDuty>(aircraftDuty, objContainer);
                        objContainer.SaveChanges();
                        objAirDutyType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAirDutyType;
        }

        public ReturnValue<Data.MasterCatalog.AircraftDuty> Delete(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            ReturnValue<Data.MasterCatalog.AircraftDuty> objAirDutyType = new ReturnValue<Data.MasterCatalog.AircraftDuty>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraftDuty))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.AircraftDuty).Name, aircraftDuty);
                    objContainer.AircraftDuty.DeleteObject(aircraftDuty);
                    objContainer.SaveChanges();
                    objAirDutyType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAirDutyType;
        }
        public ReturnValue<Data.MasterCatalog.AircraftDuty> GetLock(Data.MasterCatalog.AircraftDuty aircraftDuty)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public Int32 GetPreflightLegAircraftDuty(string aircraftDutyTypeCD)
        {
            Int32 returnValue = 0;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    returnValue = Convert.ToInt32(objContainer.spGetPreflightLegAircraftDutyType(CustomerID, aircraftDutyTypeCD).FirstOrDefault());
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return returnValue;
        }
    }
}
