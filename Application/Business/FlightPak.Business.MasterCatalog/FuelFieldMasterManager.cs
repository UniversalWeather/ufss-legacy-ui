﻿using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.MasterCatalog
{
    public class FuelFieldMasterManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelFieldMaster>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.MasterCatalog.FuelFieldMaster> GetList()
        {
            ReturnValue<Data.MasterCatalog.FuelFieldMaster> ret = new ReturnValue<Data.MasterCatalog.FuelFieldMaster>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelFieldMaster.Where(t => t.IsDeleted == false).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFieldMaster> Add(Data.MasterCatalog.FuelFieldMaster objFuelFieldMaster)
        {
            ReturnValue<Data.MasterCatalog.FuelFieldMaster> ret = new ReturnValue<Data.MasterCatalog.FuelFieldMaster>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFieldMaster))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelFieldMaster.IsDeleted = false;                    
                    ret.ReturnFlag = base.Validate(objFuelFieldMaster, ref ret.ErrorMessage);
                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelFieldMaster.AddObject(objFuelFieldMaster);
                        cs.SaveChanges();                       
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFieldMaster> Update(Data.MasterCatalog.FuelFieldMaster objFuelFieldMaster)
        {
            ReturnValue<Data.MasterCatalog.FuelFieldMaster> ret = new ReturnValue<Data.MasterCatalog.FuelFieldMaster>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFieldMaster))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    ret.ReturnFlag = base.Validate(objFuelFieldMaster, ref ret.ErrorMessage);                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelFieldMaster.Attach(objFuelFieldMaster);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelFieldMaster>(objFuelFieldMaster, cs);
                        cs.SaveChanges();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelFieldMaster> Delete(Data.MasterCatalog.FuelFieldMaster objFuelFieldMaster)
        {
            ReturnValue<Data.MasterCatalog.FuelFieldMaster> ret = new ReturnValue<Data.MasterCatalog.FuelFieldMaster>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFieldMaster))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelFieldMaster.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.FuelFieldMaster).Name, objFuelFieldMaster);
                    cs.FuelFieldMaster.DeleteObject(objFuelFieldMaster);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFieldMaster> GetLock(Data.MasterCatalog.FuelFieldMaster objFuelFieldMaster)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
