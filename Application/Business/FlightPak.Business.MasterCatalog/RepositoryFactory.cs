﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Data.MasterCatalog;
using System.Data.Objects;
using FlightPak.Business.Common;
using System.Reflection;
using Tracing = FlightPak.Common.Tracing;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class RepositoryFactory<TEntity> : MasterCatalogBaseManager<TEntity> where TEntity : class
    {
        #region Variable
        private readonly MasterDataContainer _container;
        internal ObjectSet<TEntity> _set;
        private ExceptionManager exManager;
        ReturnValue<TEntity> _entity = new ReturnValue<TEntity>();

        #endregion

        #region Methods
        /// <summary>
        /// Constructor for initializing object for MasterDataContainer and ObjectSet.
        /// </summary>
        /// <param name="objContainer"></param>
        public RepositoryFactory()
        {
            //this._container = objContainer;
            _container = new MasterDataContainer();
            this._set = _container.CreateObjectSet<TEntity>();
        }

        /// <summary>
        /// Method to Get list of entities. 
        /// </summary>
        /// <returns></returns>
        public ReturnValue<TEntity> GetList()
        {
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    _set.Context.ContextOptions.LazyLoadingEnabled = false;
                    _set.Context.ContextOptions.ProxyCreationEnabled = false;

                    string methodName = "GetAll" + _set.EntitySet.Name;
                    _entity.EntityList = ((ObjectResult<TEntity>)_set.Context.GetType().InvokeMember(methodName, BindingFlags.Default | BindingFlags.InvokeMethod, null, _set.Context, new object[] { })).ToList<TEntity>();
                    _entity.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return _entity;
        }

        /// <summary>
        /// Method to Get list of entities with parameter CustomerId. 
        /// </summary>
        /// <returns></returns>
        public ReturnValue<TEntity> GetList(Int64 CustomerId)
        {
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    _set.Context.ContextOptions.LazyLoadingEnabled = false;
                    _set.Context.ContextOptions.ProxyCreationEnabled = false;

                    string methodName = "GetAll" + _set.EntitySet.Name;
                    _entity.EntityList = ((ObjectResult<TEntity>)_set.Context.GetType().InvokeMember(methodName, BindingFlags.Default | BindingFlags.InvokeMethod, null, _set.Context, new object[] { CustomerId })).ToList<TEntity>();
                    _entity.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return _entity;
        }
        /// <summary>
        /// Method to get a entity based on the filter criteria CustomerId and PrimaryKeyId
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="PrimaryKeyId"></param>
        /// <returns></returns>
        public ReturnValue<TEntity> GetById(Int64 CustomerId, Int64 PrimaryKeyId)
        {
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    _set.Context.ContextOptions.LazyLoadingEnabled = false;
                    _set.Context.ContextOptions.ProxyCreationEnabled = false;

                    string methodName = "GetById" + _set.EntitySet.Name;
                    _entity.EntityInfo = ((TEntity)_set.Context.GetType().InvokeMember(methodName, BindingFlags.Default | BindingFlags.InvokeMethod, null, _set.Context, new object[] { CustomerId, PrimaryKeyId }));
                    _entity.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return _entity;
        }
        /// <summary>
        /// Method to add entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ReturnValue<TEntity> Add(TEntity entity)
        {
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (Tracing.Tracer.NewTraceInputs(entity))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Exception Handling using Exception Manager
                exManager.Process(() =>
                {
                    //Validate the account object and set the validated status & error message to return object
                    _entity.ReturnFlag = base.Validate(entity, ref _entity.ErrorMessage);
                    //Any transactional operation should be done if the entity is valid.
                    if (_entity.ReturnFlag)
                    {
                        _set.AddObject(entity);
                        _set.Context.SaveChanges();
                        _entity.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return _entity;
        }
        /// <summary>
        /// Method to update entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ReturnValue<TEntity> Update(TEntity entity)
        {
            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (Tracing.Tracer.NewTraceInputs(entity))
            {
                //Exception Handling using Exception Manager
                exManager.Process(() =>
                {
                    //Validate the account object and set the validated status & error message to return object
                    _entity.ReturnFlag = base.Validate(entity, ref _entity.ErrorMessage);
                    //Any transactional operation should be done if the entity is valid.
                    if (_entity.ReturnFlag)
                    {
                        _set.Attach(entity);
                        _set.Context.ObjectStateManager.ChangeObjectState(entity, EntityState.Modified);
                        _set.Context.SaveChanges();
                        _entity.ReturnFlag = true;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return _entity;
        }
        /// <summary>
        /// Method to delete entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ReturnValue<TEntity> Delete(TEntity entity)
        {
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (Tracing.Tracer.NewTraceInputs(entity))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Exception Handling using Exception Manager
                exManager.Process(() =>
                {
                    _set.Attach(entity);
                    _set.DeleteObject(entity);
                    _set.Context.SaveChanges();
                    _entity.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return _entity;
        }

        /// <summary>
        /// Method to refresh Cache object.
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }


        //public override void Dispose() 
        //{
        //    DisposeOf<TEntity>(ref TEntity);
        //}  

        //private void DisposeOf<T>(ref T objectToDispose) where T : IDisposable 
        //{ 
        //    if (objectToDispose != null) 
        //    { 
        //        objectToDispose.Dispose(); 
        //        objectToDispose = default(T); 
        //    } 
        //} 

        #endregion
    }
}
