﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class CrewPassportManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewPassengerPassport>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetList()//Data.MasterCatalog.CrewPassengerPassport crewPassport)
        {
            //ReturnValue<Data.MasterCatalog.CrewPassengerPassport> CrewPassport = new ReturnValue<Data.MasterCatalog.CrewPassengerPassport>();
            //Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //Container.ContextOptions.LazyLoadingEnabled = false;
            //Container.ContextOptions.ProxyCreationEnabled = false;
            //CrewPassport.EntityList = Container.GetCrewPassport(crewPassport.CrewID, crewPassport.PassengerRequestorID).ToList();
            //CrewPassport.ReturnFlag = true;
            //return CrewPassport;
            throw new NotImplementedException();
        }
        /// <summary>
        /// To insert values from CrewRoster-Passport Grid to CrewPassport table
        /// </summary>
        /// <param name="crewPassport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerPassport> Add(Data.MasterCatalog.CrewPassengerPassport crewPassport)
        {
            ReturnValue<Data.MasterCatalog.CrewPassengerPassport> CrewPassport = new ReturnValue<Data.MasterCatalog.CrewPassengerPassport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPassport))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    crewPassport.PassportNum = Crypting.Encrypt(crewPassport.PassportNum);
                    Container.AddToCrewPassengerPassport(crewPassport);
                    Container.SaveChanges();
                    CrewPassport.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassport;
        }
        /// <summary>
        /// To update values from CrewRoster-Passport Grid to CrewPassport table
        /// </summary>
        /// <param name="crewPassport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerPassport> Update(Data.MasterCatalog.CrewPassengerPassport crewPassport)
        {
            ReturnValue<Data.MasterCatalog.CrewPassengerPassport> CrewPassport = new ReturnValue<Data.MasterCatalog.CrewPassengerPassport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPassport))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    crewPassport.PassportNum = Crypting.Encrypt(crewPassport.PassportNum);
                    Container.Attach(crewPassport);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewPassengerPassport>(crewPassport, Container);
                    Container.SaveChanges();
                    CrewPassport.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassport;
        }
        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-Passport Grid to CrewPassport table
        /// </summary>
        /// <param name="crewPassport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerPassport> Delete(Data.MasterCatalog.CrewPassengerPassport crewPassport)
        {
            //ReturnValue<Data.MasterCatalog.CrewPassengerPassport> CrewPassport = new ReturnValue<Data.MasterCatalog.CrewPassengerPassport>();
            //Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //Container.AttachTo(typeof(Data.MasterCatalog.CrewPassengerPassport).Name, crewPassport);
            //Container.DeleteObject(crewPassport);
            //Container.SaveChanges();
            //CrewPassport.ReturnFlag = true;
            //return CrewPassport;
            ReturnValue<Data.MasterCatalog.CrewPassengerPassport> CrewPassengerPassport = new ReturnValue<Data.MasterCatalog.CrewPassengerPassport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPassport))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.CrewPassengerPassport).Name, crewPassport);
                    Container.CrewPassengerPassport.DeleteObject(crewPassport);
                    Container.SaveChanges();
                    CrewPassengerPassport.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassengerPassport;
        }
        /// <summary>
        /// To fetch the values from the CrewPassport table based on parameters
        /// </summary>
        /// <param name="crewPassport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllCrewPassport> GetListInfo(Data.MasterCatalog.CrewPassengerPassport crewPassport)
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewPassport> CrewPassport = new ReturnValue<Data.MasterCatalog.GetAllCrewPassport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPassport))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewPassport.EntityList = Container.GetAllCrewPassport(crewPassport.CrewID, crewPassport.PassengerRequestorID, crewPassport.CustomerID).ToList();
                    foreach (var item in CrewPassport.EntityList)
                    {
                        item.PassportNum = Crypting.Decrypt(item.PassportNum);
                    }
                    CrewPassport.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassport;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewPassport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetLock(Data.MasterCatalog.CrewPassengerPassport crewPassport)
        {
            throw new NotImplementedException();
        }



        public ReturnValue<Data.MasterCatalog.GetPassportbyPassportId> GetPassportbyPassportId(Int64 PassportID, Boolean IsCrewPassport)
        {
            ReturnValue<Data.MasterCatalog.GetPassportbyPassportId> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetPassportbyPassportId>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetPassportbyPassportId(PassportID, IsCrewPassport).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.CrewPassengerPassport> GetPassengerPassportByPassengerRequestorID(Int64 PassengerRequestorID)
        {
            ReturnValue<Data.MasterCatalog.CrewPassengerPassport> objCrewPassengerPassport = new ReturnValue<Data.MasterCatalog.CrewPassengerPassport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCrewPassengerPassport.EntityList = objContainer.GetPassengerPassportByPassengerRequestorID(CustomerID, PassengerRequestorID).ToList();
                    foreach (var item in objCrewPassengerPassport.EntityList)
                    {
                        item.PassportNum = Crypting.Decrypt(item.PassportNum);
                    }
                    objCrewPassengerPassport.ReturnFlag = true;
                    return objCrewPassengerPassport;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewPassengerPassport;
            }
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
