﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CQCustomerManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CQCustomer>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters> GetCQCustomerWithFilters(long CQCustomerID, string CQCustomerCD, string FetchHomebaseOnly, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters> ret = new ReturnValue<Data.MasterCatalog.GetCQCustomerWithFilters>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag

                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CQCustomerManager objCustom = new FlightPak.Business.MasterCatalog.CQCustomerManager();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetCQCustomerWithFilters(CustomerID, CQCustomerID, CQCustomerCD, FetchHomebaseOnly, FetchActiveOnly).ToList();

                    ret.ReturnFlag = true;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }
        #endregion

        public ReturnValue<Data.MasterCatalog.CQCustomer> GetList()
        {
            ReturnValue<Data.MasterCatalog.CQCustomer> ret = new ReturnValue<Data.MasterCatalog.CQCustomer>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CQCustomerManager objCustom = new FlightPak.Business.MasterCatalog.CQCustomerManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllCQCustomer(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact> ret = new ReturnValue<Data.MasterCatalog.GetAllCQCustomerAndContact>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CQCustomerManager objCustom = new FlightPak.Business.MasterCatalog.CQCustomerManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllCQCustomerAndContact(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #region not implemented

        public ReturnValue<Data.MasterCatalog.CQCustomer> GetLock(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region "Implement for CQCustomer"

        public ReturnValue<Data.MasterCatalog.CQCustomer> Add(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            ReturnValue<Data.MasterCatalog.CQCustomer> ReturnValue = new ReturnValue<Data.MasterCatalog.CQCustomer>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomer))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.CQCustomer.AddObject(CQCustomer);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = CQCustomer;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.CQCustomer> Update(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            ReturnValue<Data.MasterCatalog.CQCustomer> ReturnValue = new ReturnValue<Data.MasterCatalog.CQCustomer>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomer))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.CQCustomer.Attach(CQCustomer);
                        objContainer.ObjectStateManager.ChangeObjectState(CQCustomer, System.Data.EntityState.Modified);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.CQCustomer> Delete(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            ReturnValue<Data.MasterCatalog.CQCustomer> ReturnValue = new ReturnValue<Data.MasterCatalog.CQCustomer>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomer))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.CQCustomer.Attach(CQCustomer);
                        objContainer.CQCustomer.DeleteObject(CQCustomer);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetCQCustomer> GetCQCustomer(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomer> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCQCustomer>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetCQCustomer(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD> GetCQCustomerByCQCustomerCD(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerCD>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetCQCustomerByCQCustomerCD(CustomerID, CQCustomer.CQCustomerCD).ToList();
                    //if (ReturnValue.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < ReturnValue.EntityList.Count; i++)
                    //    {
                    //        ReturnValue.EntityList[i].CQCustomerCD = ReturnValue.EntityList[i].CQCustomerCD != null ? ReturnValue.EntityList[i].CQCustomerCD.Trim() : string.Empty;
                    //    }
                    //}
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID> GetCQCustomerByCQCustomerID(Data.MasterCatalog.CQCustomer CQCustomer)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCQCustomerByCQCustomerID>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetCQCustomerByCQCustomerID(CustomerID, CQCustomer.CQCustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        #endregion

    }
}
