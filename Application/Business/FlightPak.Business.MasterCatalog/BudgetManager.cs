﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class BudgetManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Budget>, IMasterCatalog.ICacheable
    {

        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.Budget> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Get the
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllBudget> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllBudget> objBudgetType = new ReturnValue<Data.MasterCatalog.GetAllBudget>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objBudgetType.EntityList = objContainer.GetAllBudget(CustomerID).ToList();
                    objBudgetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objBudgetType;
        }

        public ReturnValue<Data.MasterCatalog.Budget> Add(Data.MasterCatalog.Budget budgetType)
        {

            ReturnValue<Data.MasterCatalog.Budget> objBudgetType = new ReturnValue<Data.MasterCatalog.Budget>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //Code for Data Anotation
                    objBudgetType.ReturnFlag = base.Validate(budgetType, ref objBudgetType.ErrorMessage);
                    
                    if (objBudgetType.ReturnFlag)
                     {
                         Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                         objContainer.AddToBudget(budgetType);
                         objContainer.SaveChanges();
                         objBudgetType.ReturnFlag = true;
                     }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objBudgetType;
        }

        public ReturnValue<Data.MasterCatalog.Budget> Update(Data.MasterCatalog.Budget budgetType)
        {

            ReturnValue<Data.MasterCatalog.Budget> objBudgetType = new ReturnValue<Data.MasterCatalog.Budget>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //Code for Data Anotation
                    objBudgetType.ReturnFlag = base.Validate(budgetType, ref objBudgetType.ErrorMessage);
                    
                    if (objBudgetType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.Budget.Attach(budgetType);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Budget>(budgetType, objContainer);
                        objContainer.SaveChanges();
                        objBudgetType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objBudgetType;
        }

        public ReturnValue<Data.MasterCatalog.Budget> Delete(Data.MasterCatalog.Budget budgetType)
        {
            ReturnValue<Data.MasterCatalog.Budget> objBudgettype = new ReturnValue<Data.MasterCatalog.Budget>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(budgetType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Budget).Name, budgetType);
                    objContainer.Budget.DeleteObject(budgetType);
                    objContainer.SaveChanges();
                    objBudgettype.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objBudgettype;
        }

        public ReturnValue<Data.MasterCatalog.Budget> GetLock(Data.MasterCatalog.Budget budgetType)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
