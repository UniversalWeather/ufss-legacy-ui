﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class TravelCoordinatorRequestorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TravelCoordinatorRequestor>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetList()
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> objTravelCoordinatorRequestor = new ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objTravelCoordinatorRequestor.EntityList = objContainer.GetAllTravelCoordinatorRequestor(CustomerID).ToList();
                    objTravelCoordinatorRequestor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objTravelCoordinatorRequestor;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetTravelCoordinatorRequestorSelectedList(Int64 travelCoordCD)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> objTravelCoordinatorRequestor = new ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objTravelCoordinatorRequestor.EntityList = objContainer.GetTravelCoordinatorRequestorSelectedList(CustomerID, travelCoordCD).ToList();
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objTravelCoordinatorRequestor;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> Add(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> objTravelCoordinatorRequestor = new ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToTravelCoordinatorRequestor(travelCoordinatorRequestor);
                    objContainer.SaveChanges();
                    objTravelCoordinatorRequestor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objTravelCoordinatorRequestor;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> Update(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> objTravelCoordinatorRequestor = new ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.TravelCoordinatorRequestor.Attach(travelCoordinatorRequestor);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinatorRequestor>(travelCoordinatorRequestor, objContainer);
                    objContainer.SaveChanges();
                    objTravelCoordinatorRequestor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objTravelCoordinatorRequestor;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> Delete(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> objTravelCoordinatorRequestor = new ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorRequestor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TravelCoordinatorRequestor).Name, travelCoordinatorRequestor);
                    objContainer.TravelCoordinatorRequestor.DeleteObject(travelCoordinatorRequestor);
                    objContainer.SaveChanges();
                    objTravelCoordinatorRequestor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objTravelCoordinatorRequestor;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetListInfo()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorRequestor> GetLock(Data.MasterCatalog.TravelCoordinatorRequestor travelCoordinatorRequestor)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
