﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using FlightPak.Framework.Security;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data.Entity;
using System.Data;
using FlightPak.Framework.Caching;
using FlightPak.ServiceAgents;
using System.Configuration;
using FlightPak.Data.MasterCatalog;
using FlightPak.Common;
using FlightPak.Common.Constants;
using System.Data.EntityClient;
namespace FlightPak.Business.MasterCatalog
{
    public class CompanyManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Company>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        public ReturnValue<Data.MasterCatalog.GetCompanyWithFilters> GetCompanyWithFilters(string HomebaseCD,long HomebaseId,bool FetchInactiveOnly,bool IsDeleted)
        {
            ReturnValue<Data.MasterCatalog.GetCompanyWithFilters> ReturnValue = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetCompanyWithFilters>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetCompanyWithFilters(CustomerID, HomebaseCD, HomebaseId, FetchInactiveOnly, IsDeleted).ToList();
                   
                    foreach (var item in ReturnValue.EntityList)
                    {
                        if (item.UserPasswordCBP != null)
                        {
                            if (item.UserPasswordCBP.Trim() != "")
                            {
                                item.UserPasswordCBP = Crypting.Decrypt(item.UserPasswordCBP);
                            }
                            else
                            {
                                item.UserPasswordCBP = string.Empty;
                            }
                        }
                        else
                        {
                            item.UserPasswordCBP = string.Empty;
                        }
                        if (item.UserNameCBP != null)
                        {
                            if (item.UserNameCBP.Trim() != "")
                            {
                                item.UserNameCBP = Crypting.Decrypt(item.UserNameCBP);
                            }
                            else
                            {
                                item.UserNameCBP = string.Empty;
                            }
                        }
                        else
                        {
                            item.UserNameCBP = string.Empty;
                        }
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        #endregion
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        /// <summary>
        /// To get the values for CompanyProfileCatalog page from Company table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Company> GetList()
        {
            
            throw new NotImplementedException();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllCompanyMaster> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllCompanyMaster> ReturnValue = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllCompanyMaster>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllCompanyMaster(CustomerID).ToList();
                    
                    foreach (var item in ReturnValue.EntityList)
                    {
                        if (item.UserPasswordCBP != null)
                        {
                            if (item.UserPasswordCBP.Trim() != "")
                            {
                                item.UserPasswordCBP = Crypting.Decrypt(item.UserPasswordCBP);
                            }
                            else
                            {
                                item.UserPasswordCBP = string.Empty;
                            }
                        }
                        else
                        {
                            item.UserPasswordCBP = string.Empty;
                        }
                        if (item.UserNameCBP != null)
                        {
                            if (item.UserNameCBP.Trim() != "")
                            {
                                item.UserNameCBP = Crypting.Decrypt(item.UserNameCBP);
                            }
                            else
                            {
                                item.UserNameCBP = string.Empty;
                            }
                        }
                        else
                        {
                            item.UserNameCBP = string.Empty;
                        }
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// Based on HomeBaseID
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId> GetListInfoByHomeBaseId(Int64 HomeBaseID)
        {
            ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId> ReturnValue = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllCompanyMasterbyHomeBaseId>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllCompanyMasterbyHomeBaseId(HomeBaseID).ToList();
                    foreach (var item in ReturnValue.EntityList)
                    {
                        item.UserPasswordCBP = Crypting.Decrypt(item.UserPasswordCBP);
                        item.UserNameCBP = Crypting.Decrypt(item.UserNameCBP);
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To insert values from CompanyProfileCatalog page to Company table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Company> Add(Data.MasterCatalog.Company CompanyData)
        {
            ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Company>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    if ((CompanyData.UserNameCBP != null) && (CompanyData.UserNameCBP != ""))
                    {
                        CompanyData.UserNameCBP = Crypting.Encrypt(CompanyData.UserNameCBP);
                    }
                    else
                    {
                        CompanyData.UserNameCBP = string.Empty;
                    }
                    if ((CompanyData.UserPasswordCBP != null) && (CompanyData.UserPasswordCBP != ""))
                    {
                        CompanyData.UserPasswordCBP = Crypting.Encrypt(CompanyData.UserPasswordCBP);
                    }
                    else
                    {
                        CompanyData.UserPasswordCBP = string.Empty;
                    }
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(CompanyData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Container.AddToCompany(CompanyData);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To update values from CompanyProfileCatalog page to Company table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Company> Update(Data.MasterCatalog.Company CompanyData)
        {
            ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Company>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    if (CompanyData.UserPasswordCBP != null)
                    {
                        if (CompanyData.UserPasswordCBP.Trim() != "")
                        {
                            CompanyData.UserNameCBP = Crypting.Encrypt(CompanyData.UserNameCBP);
                        }
                        else
                        {
                            CompanyData.UserPasswordCBP = string.Empty;
                        }
                    }
                    else
                    {
                        CompanyData.UserPasswordCBP = string.Empty;
                    }
                    if (CompanyData.UserNameCBP != null)
                    {
                        if (CompanyData.UserNameCBP.Trim() != "")
                        {
                            CompanyData.UserPasswordCBP = Crypting.Encrypt(CompanyData.UserPasswordCBP);
                        }
                        else
                        {
                            CompanyData.UserNameCBP = string.Empty;
                        }
                    }
                    else
                    {
                        CompanyData.UserNameCBP = string.Empty;
                    }

                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(CompanyData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Container.Company.Attach(CompanyData);

                        if (CompanyData.PasswordValidDays.HasValue)
                        {
                            if (CompanyData.PasswordValidDays.Value == 4 || CompanyData.PasswordValidDays.Value == 7 || CompanyData.PasswordValidDays.Value == 13)
                            {
                                CompanyData.PasswordValidDays = CompanyData.PasswordValidDays.Value - 1;
                                foreach (var user in CompanyData.UserMaster)
                                {
                                    user.ForceChangePassword = true;
                                }
                            }
                        }
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Company>(CompanyData, Container);
                        Container.SaveChanges();

                        // Once the user is updated, we should clear all the objects in cache.
                        var cacheManager = new CacheManager();
                        cacheManager.RemoveAll();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To update the selected record as deleted, from CompanyProfileCatalog page in Company table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Company> Delete(Data.MasterCatalog.Company CompanyData)
        {
            ReturnValue<Data.MasterCatalog.Company> ReturnValue = null;
            //company.IsDeleted = true;
            Data.MasterCatalog.MasterDataContainer Container = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CompanyData))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Company>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.Company).Name, CompanyData);
                    Container.Company.DeleteObject(CompanyData);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Company> GetLock(Data.MasterCatalog.Company CompanyData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Correct CSV layout 
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public string[] GetValidatedTSARecord(string line)
        {

            string[] columns = null;
            string[] validatedcolumns = new string[11];
            columns = line.Split(',');
            if (columns != null)
            {
                if (columns.Length == 11)
                {
                    return columns;
                }
                else
                {
                    int MISCPosition;
                    MISCPosition = line.IndexOf("sex =");
                    string MISC = string.Empty;
                    string TSAExpMisc;
                    if (MISCPosition != -1)
                    {
                        MISC = line.Substring(MISCPosition);
                        TSAExpMisc = line.Substring(0, MISCPosition - 1);
                        string[] TSAFields = TSAExpMisc.Split(',');
                        int TSAFieldLength = TSAFields.Length;
                        StringBuilder POB = new StringBuilder();
                        for (int i = 7; i < TSAFieldLength - 2; i++)
                        {
                            POB.Append(TSAFields[i]).Append(",");
                        }
                        validatedcolumns[0] = columns[0];
                        validatedcolumns[1] = columns[1];
                        validatedcolumns[2] = columns[2];
                        validatedcolumns[3] = columns[3];
                        validatedcolumns[4] = columns[4];
                        validatedcolumns[5] = columns[5];
                        validatedcolumns[6] = columns[6];
                        validatedcolumns[7] = POB.ToString().TrimEnd(',');
                        validatedcolumns[8] = TSAFields[TSAFieldLength - 2];
                        validatedcolumns[9] = TSAFields[TSAFieldLength - 1];
                        validatedcolumns[10] = MISC;
                    }
                }

            }
            return validatedcolumns;
        }
        /// <summary>
        /// Inserting TSA Records
        /// </summary>
        /// <param name="customerTSA"></param>
        /// <returns></returns>

        public ReturnValue<Data.MasterCatalog.Customer> AddTSA(Data.MasterCatalog.Customer customerTSA)
        {
            ReturnValue<Data.MasterCatalog.Customer> ReturnValue = new ReturnValue<Data.MasterCatalog.Customer>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        EntityConnection entityConnection = objContainer.Connection as EntityConnection;

                        foreach (Data.MasterCatalog.TSANoFly oTSANoFly in customerTSA.TSANoFly)
                        {
                            oTSANoFly.CustomerID = UserPrincipal.Identity.CustomerID;
                            oTSANoFly.LastUpdTS = DateTime.Now;
                            oTSANoFly.LastUpdUID = UserPrincipal.Identity.Name;
                        }
                        foreach (Data.MasterCatalog.TSASelect oTSASelect in customerTSA.TSASelect)
                        {
                            oTSASelect.CustomerID = UserPrincipal.Identity.CustomerID;
                            oTSASelect.LastUpdTS = DateTime.Now;
                            oTSASelect.LastUpdUID = UserPrincipal.Identity.Name;
                        }
                        foreach (Data.MasterCatalog.TSAClear oTSAClear in customerTSA.TSAClear)
                        {
                            oTSAClear.CustomerID = UserPrincipal.Identity.CustomerID;
                            oTSAClear.LastUpdTS = DateTime.Now;
                            oTSAClear.LastUpdUID = UserPrincipal.Identity.Name;
                        }

                        if (customerTSA.TSANoFly.Count != 0)
                        {
                            var tsanoflyData = new List<TSANoFly>();
                            tsanoflyData.AddRange(customerTSA.TSANoFly);                            
                            BulkUploadManager.BulkInsert(entityConnection.StoreConnection.ConnectionString, EntitySet.Database.TSANoFly, tsanoflyData);

                        }

                        if (customerTSA.TSASelect.Count != 0)
                        {
                            var tsaselectData = new List<TSASelect>();
                            tsaselectData.AddRange(customerTSA.TSASelect);
                            BulkUploadManager.BulkInsert(entityConnection.StoreConnection.ConnectionString, EntitySet.Database.TSASelect, tsaselectData);
                        }

                        if (customerTSA.TSAClear.Count != 0)
                        {
                            var tsaclearData = new List<TSAClear>();
                            tsaclearData.AddRange(customerTSA.TSAClear);
                            BulkUploadManager.BulkInsert(entityConnection.StoreConnection.ConnectionString, EntitySet.Database.TSAClear, tsaclearData);
                        }

                       
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// Updating Passenger Status and PreFlight Passenger Status
        /// </summary>
        /// <param name="customerTSA"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Customer> SetTSAstatus(Data.MasterCatalog.Customer customerTSA)
        {
            ReturnValue<Data.MasterCatalog.Customer> ReturnValue = new ReturnValue<Data.MasterCatalog.Customer>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        //This should be retrived from Services --> web.config
                        int MaxCommandTimeOut = -1;
                        if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                        {
                            MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                        }
                        objContainer.CommandTimeout = MaxCommandTimeOut;
                        objContainer.SetTSAstatus(customerTSA.CustomerID);
                        objContainer.CommandTimeout = null;
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// Update TSASequenceNumber of  FlightpakSequence tables for the customer
        /// </summary>
        /// <param name="customerID"></param>
        /// <param name="TSASequenceNumber"></param>
        /// <returns></returns>

        public ReturnValue<Data.MasterCatalog.Customer> UpdateSequenceNumber(long customerid, long TSAUploadCurrentNo)
        {
            ReturnValue<Data.MasterCatalog.Customer> ReturnValue = new ReturnValue<Data.MasterCatalog.Customer>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        //This should be retrived from Services --> web.config
                        int MaxCommandTimeOut = -1;
                        if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                        {
                            MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                        }
                        var tsasequncenum = objContainer.FlightpakSequence.Where(x => x.CustomerID == customerid);
                        foreach (var tsadata in tsasequncenum)
                        {
                            tsadata.TSAUploadCurrentNo = TSAUploadCurrentNo;
                        }
                        objContainer.SaveChanges();
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// To update the selected record as deleted, from CompanyProfileCatalog page in Company table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSANoFly> DeleteTSANOFly(Data.MasterCatalog.TSANoFly TSAData)
        {
            ReturnValue<Data.MasterCatalog.TSANoFly> ReturnValue = new ReturnValue<Data.MasterCatalog.TSANoFly>();
            //company.IsDeleted = true;
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            Container.AttachTo(typeof(Data.MasterCatalog.TSANoFly).Name, TSAData);
            Container.TSANoFly.DeleteObject(TSAData);
            try
            {
                Container.SaveChanges();
            }
            catch (Exception ex)
            {
                
            }
            ReturnValue.ReturnFlag = true;
            return ReturnValue;
        }
        /// <summary>
        /// To update the selected record as deleted, from CompanyProfileCatalog page in Company table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSAClear> DeleteTSAClear(Data.MasterCatalog.TSAClear TSAData)
        {
            ReturnValue<Data.MasterCatalog.TSAClear> ReturnValue = new ReturnValue<Data.MasterCatalog.TSAClear>();
            //company.IsDeleted = true;
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            Container.AttachTo(typeof(Data.MasterCatalog.TSAClear).Name, TSAData);
            Container.TSAClear.DeleteObject(TSAData);
            try
            {
                Container.SaveChanges();
            }
            catch (Exception ex)
            {
               
                
            }
            ReturnValue.ReturnFlag = true;
            return ReturnValue;
        }
        /// <summary>
        /// To update the selected record as deleted, from CompanyProfileCatalog page in Company table
        /// </summary>
        /// <param name="CompanyData"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSASelect> DeleteTSASelect(Data.MasterCatalog.TSASelect TSAData)
        {
            ReturnValue<Data.MasterCatalog.TSASelect> ReturnValue = new ReturnValue<Data.MasterCatalog.TSASelect>();
            //company.IsDeleted = true;
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            Container.AttachTo(typeof(Data.MasterCatalog.TSASelect).Name, TSAData);
            Container.TSASelect.DeleteObject(TSAData);
            try
            {
                Container.SaveChanges();
            }
            catch (Exception ex)
            {
                //Manually Handled
                //Container.Refresh(System.Data.Objects.RefreshMode.ClientWins, TSAData);
                //Container.SaveChanges();
            }
            ReturnValue.ReturnFlag = true;
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.GetCompanyID> GetCompanyID()
        {
            ReturnValue<Data.MasterCatalog.GetCompanyID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCompanyID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetCompanyID(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.TripPrivacySettings> GetTripPrivacySettings()
        {
            ReturnValue<Data.MasterCatalog.TripPrivacySettings> ReturnValue = new ReturnValue<Data.MasterCatalog.TripPrivacySettings>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityInfo = Container.GetTripPrivacySettings(CustomerID, HomeBaseID).FirstOrDefault();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// UpdateCBPCredentials
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="DomainName"></param>
        /// <param name="SenderID"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public ReturnValue<List<string>> UpdateCBPCredentials(string UserName, string DomainName, string SenderID, string Password)
        {
            ReturnValue<List<string>> ReturnValue = new ReturnValue<List<string>>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(UserName, SenderID, Password))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    // ReturnValue = new ReturnValue<List<string>>();
                    UWAManager objUWAManager = new UWAManager();
                    ReturnValue = objUWAManager.CompanyEAPISSubmit(CustomerID, DomainName, UserName, SenderID, Password);

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }


        public ReturnValue<Data.MasterCatalog.Company> UpdateAPISInterface(Int64 HomebaseID, string UserNameCBP, string UserPasswordCBP, string Domain, string APISUrl, bool IsAPISSupport, bool IsAPISAlert)
        {
            ReturnValue<Data.MasterCatalog.Company> ReturnValue = new ReturnValue<Data.MasterCatalog.Company>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomebaseID, UserNameCBP, UserPasswordCBP, Domain, APISUrl, IsAPISSupport, IsAPISAlert))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    if (!string.IsNullOrEmpty(UserNameCBP))
                    {

                        UserNameCBP = Crypting.Encrypt(UserNameCBP);
                    }
                    else
                    {
                        UserNameCBP = string.Empty;
                    }



                    if (!string.IsNullOrEmpty(UserPasswordCBP))
                    {
                        UserPasswordCBP = Crypting.Encrypt(UserPasswordCBP);
                    }
                    else
                    {
                        UserPasswordCBP = string.Empty;
                    }
                    objContainer.UpdateCBPCredentials(HomebaseID, UserNameCBP, UserPasswordCBP, Domain, APISUrl, IsAPISSupport, IsAPISAlert);
                    // Once the user is updated, we should clear all the objects in cache.
                    var cacheManager = new CacheManager();
                    cacheManager.RemoveAll();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// To get the values for TripSheetReportHeader  from TripSheetReportHeader table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> GetTripSheetReportHeaderList()
        {
            ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> ret = new ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CompanyManager objAcct = new FlightPak.Business.MasterCatalog.CompanyManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetTripSheetReportHeader(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }


        #region CQCompany
        public ReturnValue<Data.MasterCatalog.CQMessage> GetCQMessage(string MessageTYPE, Int64 HomebaseID)
        {
            ReturnValue<Data.MasterCatalog.CQMessage> ReturnValue = new ReturnValue<Data.MasterCatalog.CQMessage>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MessageTYPE, HomebaseID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllCQMessage(CustomerID, MessageTYPE, HomebaseID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.CQMessage> Add(Data.MasterCatalog.CQMessage cqMessage)
        {

            ReturnValue<Data.MasterCatalog.CQMessage> objCqType = new ReturnValue<Data.MasterCatalog.CQMessage>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(cqMessage))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ////Code for Data Anotation
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToCQMessage(cqMessage);
                    objContainer.SaveChanges();
                    objCqType.ReturnFlag = true;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCqType;
        }

        public void DeleteCQMessage(string MessageTYPE, Int64 HomebaseID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MessageTYPE, HomebaseID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.CQDeleteMessage(CustomerID, MessageTYPE, HomebaseID);

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

        }

        #endregion
    }
}
