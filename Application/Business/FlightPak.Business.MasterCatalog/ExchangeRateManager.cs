﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class ExchangeRateManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.ExchangeRate>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values into ExchangeRates page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ExchangeRate> GetList()
        {
            ReturnValue<Data.MasterCatalog.ExchangeRate> ExchangeRateType = new ReturnValue<Data.MasterCatalog.ExchangeRate>();
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            // Fix for recursive infinite loop
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ExchangeRateType.EntityList = Container.GetAllExchangeRate(CustomerID).ToList();
                    ExchangeRateType.ReturnFlag = true;
                    return ExchangeRateType;
                }, FlightPak.Common.Constants.Policy.UILayer);
                return ExchangeRateType;
            }
        }
        /// <summary>
        /// To insert values into DB from ExchangeRates page 
        /// </summary>
        /// <param name="ExchangeRate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ExchangeRate> Add(Data.MasterCatalog.ExchangeRate ExchangeRate)
        {
            ReturnValue<Data.MasterCatalog.ExchangeRate> ExchangeRateType = new ReturnValue<Data.MasterCatalog.ExchangeRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ExchangeRateType.ReturnFlag = base.Validate(ExchangeRate, ref ExchangeRateType.ErrorMessage);
                    //For Data Anotation
                    if (ExchangeRateType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.AddToExchangeRate(ExchangeRate);
                        Container.SaveChanges();
                        ExchangeRateType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.UILayer);
            }
            return ExchangeRateType;
        }
        /// <summary>
        /// To update values into DB from ExchangeRates page
        /// </summary>
        /// <param name="ExchangeRate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ExchangeRate> Update(Data.MasterCatalog.ExchangeRate ExchangeRate)
        {
            ReturnValue<Data.MasterCatalog.ExchangeRate> ExchangeRateType = new ReturnValue<Data.MasterCatalog.ExchangeRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ExchangeRateType.ReturnFlag = base.Validate(ExchangeRate, ref ExchangeRateType.ErrorMessage);
                    //For Data Anotation
                    if (ExchangeRateType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.ExchangeRate.Attach(ExchangeRate);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.ExchangeRate>(ExchangeRate, Container);
                        Container.SaveChanges();
                        ExchangeRateType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ExchangeRateType;
        }
        /// <summary>
        /// To update the selected record as deleted, from ExchangeRates page
        /// </summary>
        /// <param name="ExchangeRate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ExchangeRate> Delete(Data.MasterCatalog.ExchangeRate ExchangeRate)
        {
            ReturnValue<Data.MasterCatalog.ExchangeRate> ExchangeRateType = new ReturnValue<Data.MasterCatalog.ExchangeRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ExchangeRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.ExchangeRate).Name, ExchangeRate);
                    Container.ExchangeRate.DeleteObject(ExchangeRate);
                    Container.SaveChanges();
                    ExchangeRateType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ExchangeRateType;
        }
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ExchangeRate> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.ExchangeRate> ExchangeRateType = new ReturnValue<Data.MasterCatalog.ExchangeRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    ExchangeRateType.EntityList = Container.ExchangeRate.ToList();
                    ExchangeRateType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ExchangeRateType;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="ExchangeRateType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ExchangeRate> GetLock(Data.MasterCatalog.ExchangeRate ExchangeRate)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}