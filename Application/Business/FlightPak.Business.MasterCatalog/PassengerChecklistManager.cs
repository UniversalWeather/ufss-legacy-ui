﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class PassengerChecklistManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.PassengerCheckListDetail>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> GetList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To insert values from CrewRoster-Checklist Grid to CrewChecklistDate table
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> Add(Data.MasterCatalog.PassengerCheckListDetail crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> CrewCheckList = new ReturnValue<Data.MasterCatalog.PassengerCheckListDetail>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToPassengerCheckListDetail(crewChecklistDate);
                    Container.SaveChanges();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// To update values from CrewRoster-Checklist Grid to CrewChecklistDate table
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> Update(Data.MasterCatalog.PassengerCheckListDetail crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> CrewCheckList = new ReturnValue<Data.MasterCatalog.PassengerCheckListDetail>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.PassengerCheckListDetail.Attach(crewChecklistDate);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerCheckListDetail>(crewChecklistDate, Container);
                    Container.SaveChanges();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-Checklist Grid to CrewChecklistDate table
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> Delete(Data.MasterCatalog.PassengerCheckListDetail crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> CrewCheckList = new ReturnValue<Data.MasterCatalog.PassengerCheckListDetail>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.PassengerCheckListDetail).Name, crewChecklistDate);
                    Container.PassengerCheckListDetail.DeleteObject(crewChecklistDate);
                    Container.SaveChanges();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// To fetch the values from the CrewChecklistDate table based on parameters
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetPaxChecklistDate> GetListInfo(Data.MasterCatalog.GetPaxChecklistDate crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.GetPaxChecklistDate> CrewCheckList = new ReturnValue<Data.MasterCatalog.GetPaxChecklistDate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewCheckList.EntityList = Container.GetPaxChecklistDate(CustomerID, crewChecklistDate.PassengerID).ToList();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerCheckListDetail> GetLock(Data.MasterCatalog.PassengerCheckListDetail crewChecklistDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
