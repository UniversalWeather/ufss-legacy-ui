﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class TripManagerCheckListGroupManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TripManagerCheckListGroup>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetAllTripmanagerChecklistGroupWithFilters(long CheckGroupID, string CheckGroupCD, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> objtripManagerGroup = new ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objtripManagerGroup.EntityList = objContainer.GetAllTripmanagerChecklistGroupWithFilters(CustomerID, CheckGroupID, CheckGroupCD, FetchActiveOnly).ToList();

                    objtripManagerGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objtripManagerGroup;
        }
        #endregion

        public ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetList()
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> objtripManagerGroup = new ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objtripManagerGroup.EntityList = objContainer.GetAllTripManagerCheckListGroup(CustomerID).ToList();
                    objtripManagerGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objtripManagerGroup;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> Add(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> objtripManagerGroup = new ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objtripManagerGroup.ReturnFlag = base.Validate(tripManagerChecklist, ref objtripManagerGroup.ErrorMessage);
                    //For Data Anotation
                    if (objtripManagerGroup.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToTripManagerCheckListGroup(tripManagerChecklist);
                        objContainer.SaveChanges();
                        objtripManagerGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objtripManagerGroup;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> Update(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> objtripManagerGroup = new ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objtripManagerGroup.ReturnFlag = base.Validate(tripManagerChecklist, ref objtripManagerGroup.ErrorMessage);
                    //For Data Anotation
                    if (objtripManagerGroup.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.TripManagerCheckListGroup.Attach(tripManagerChecklist);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.TripManagerCheckListGroup>(tripManagerChecklist, objContainer);
                        objContainer.SaveChanges();
                        objtripManagerGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objtripManagerGroup;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> Delete(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> objtripManagerGroup = new ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TripManagerCheckListGroup).Name, tripManagerChecklist);
                    objContainer.TripManagerCheckListGroup.DeleteObject(tripManagerChecklist);
                    objContainer.SaveChanges();
                    objtripManagerGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objtripManagerGroup;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckListGroup> GetLock(Data.MasterCatalog.TripManagerCheckListGroup tripManagerChecklist)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
