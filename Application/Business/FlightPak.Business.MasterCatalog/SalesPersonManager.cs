﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
   public  class SalesPersonManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.SalesPerson>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.GetSalesPerson> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetSalesPerson> objCustomerAddress = new ReturnValue<Data.MasterCatalog.GetSalesPerson>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCustomerAddress.EntityList = objContainer.GetSalesPerson(CustomerID).ToList();
                    //if (objCustomerAddress.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objCustomerAddress.EntityList.Count; i++)
                    //    {
                    //        objCustomerAddress.EntityList[i].SalesPersonCD = objCustomerAddress.EntityList[i].SalesPersonCD != null ? objCustomerAddress.EntityList[i].SalesPersonCD.Trim() : string.Empty;
                    //    }
                    //}
                    objCustomerAddress.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;

        }

        public ReturnValue<Data.MasterCatalog.SalesPerson> GetList()
        {
            throw new NotImplementedException();
        }
       
        public ReturnValue<Data.MasterCatalog.SalesPerson> Add(Data.MasterCatalog.SalesPerson salesPerson)
        {
            ReturnValue<Data.MasterCatalog.SalesPerson> objSalesPerson = new ReturnValue<Data.MasterCatalog.SalesPerson>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objSalesPerson.ReturnFlag = base.Validate(salesPerson, ref objSalesPerson.ErrorMessage);
                    //For Data Anotation
                    if (objSalesPerson.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToSalesPerson(salesPerson);
                        objContainer.SaveChanges();
                        objSalesPerson.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objSalesPerson;
        }
        
        public ReturnValue<Data.MasterCatalog.SalesPerson> Update(Data.MasterCatalog.SalesPerson salesPerson)
        {
            ReturnValue<Data.MasterCatalog.SalesPerson> objCustomerAddress = new ReturnValue<Data.MasterCatalog.SalesPerson>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCustomerAddress.ReturnFlag = base.Validate(salesPerson, ref objCustomerAddress.ErrorMessage);
                    //For Data Anotation
                    if (objCustomerAddress.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.SalesPerson.Attach(salesPerson);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.SalesPerson>(salesPerson, objContainer);
                        objContainer.SaveChanges();
                        objCustomerAddress.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;
        }

        public ReturnValue<Data.MasterCatalog.SalesPerson> Delete(Data.MasterCatalog.SalesPerson salesPerson)
        {
            ReturnValue<Data.MasterCatalog.SalesPerson> objCustomerAddress = new ReturnValue<Data.MasterCatalog.SalesPerson>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(salesPerson))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.SalesPerson).Name, salesPerson);
                    objContainer.SalesPerson.DeleteObject(salesPerson);
                    objContainer.SaveChanges();
                    objCustomerAddress.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;
        }

        public ReturnValue<Data.MasterCatalog.GetSalesPersonID> GetSalesPersonID()
        {
            ReturnValue<Data.MasterCatalog.GetSalesPersonID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetSalesPersonID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetSalesPersonID(CustomerID, UserName).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        #region not implemented
        public ReturnValue<Data.MasterCatalog.SalesPerson> GetLock(Data.MasterCatalog.SalesPerson t)
        {
            throw new NotImplementedException();
        }
              
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
