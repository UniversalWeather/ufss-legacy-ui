﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class EmergencyManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.EmergencyContact>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization

        public ReturnValue<Data.MasterCatalog.EmergencyContact> GetAllEmergencyContactWithFilters(long EmergencyContactID, string EmergencyContactCD, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.EmergencyContact> objEmergencyType = new ReturnValue<Data.MasterCatalog.EmergencyContact>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objEmergencyType.EntityList = objContainer.GetAllEmergencyContactWithFilters(CustomerID, EmergencyContactID, EmergencyContactCD, FetchActiveOnly).ToList();
                    objEmergencyType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objEmergencyType;
        }

        #endregion
        public ReturnValue<Data.MasterCatalog.GetEmergencyContact> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetEmergencyContact> objEmergencyType = new ReturnValue<Data.MasterCatalog.GetEmergencyContact>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objEmergencyType.EntityList = objContainer.GetEmergencyContact(CustomerID).ToList();
                    objEmergencyType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objEmergencyType;
        }
        public ReturnValue<Data.MasterCatalog.EmergencyContact> GetList()
        {
            ReturnValue<Data.MasterCatalog.EmergencyContact> objEmergencyType = new ReturnValue<Data.MasterCatalog.EmergencyContact>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            objContainer.ContextOptions.LazyLoadingEnabled = false;
            objContainer.ContextOptions.ProxyCreationEnabled = false;
            objEmergencyType.EntityList = objContainer.GetAllEmergencyContact(CustomerID).ToList();
            objEmergencyType.ReturnFlag = true;
            return objEmergencyType;
        }
        public ReturnValue<Data.MasterCatalog.EmergencyContact> Add(Data.MasterCatalog.EmergencyContact emergencytype)
        {
            ReturnValue<Data.MasterCatalog.EmergencyContact> objEmergencyType = new ReturnValue<Data.MasterCatalog.EmergencyContact>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencytype))
            {                
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objEmergencyType.ReturnFlag = base.Validate(emergencytype, ref objEmergencyType.ErrorMessage);
                    //For Data Anotation
                    if (objEmergencyType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToEmergencyContact(emergencytype);
                        objContainer.SaveChanges();
                        objEmergencyType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objEmergencyType;
        }
        public ReturnValue<Data.MasterCatalog.EmergencyContact> Update(Data.MasterCatalog.EmergencyContact emergencytype)
        {
            ReturnValue<Data.MasterCatalog.EmergencyContact> objEmergencyType = new ReturnValue<Data.MasterCatalog.EmergencyContact>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencytype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objEmergencyType.ReturnFlag = base.Validate(emergencytype, ref objEmergencyType.ErrorMessage);
                    //For Data Anotation
                    if (objEmergencyType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.EmergencyContact.Attach(emergencytype);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.EmergencyContact>(emergencytype, objContainer);
                        objContainer.SaveChanges();
                        objEmergencyType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return objEmergencyType;
        }
        public ReturnValue<Data.MasterCatalog.EmergencyContact> Delete(Data.MasterCatalog.EmergencyContact emergencytype)
        {
            ReturnValue<Data.MasterCatalog.EmergencyContact> objEmergencyType = new ReturnValue<Data.MasterCatalog.EmergencyContact>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(emergencytype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.EmergencyContact).Name, emergencytype);
                    objContainer.EmergencyContact.DeleteObject(emergencytype);
                    objContainer.SaveChanges();
                    objEmergencyType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objEmergencyType;
        }
        public ReturnValue<Data.MasterCatalog.EmergencyContact> GetLock(Data.MasterCatalog.EmergencyContact emergencytype)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
