﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class CrewPaxVisaManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewPassengerVisa>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerVisa> GetList()//Data.MasterCatalog.CrewPassengerVisa crewPaxVisa)
        {
            //ReturnValue<Data.MasterCatalog.CrewPassengerVisa> CrewPassengerVisa = new ReturnValue<Data.MasterCatalog.CrewPassengerVisa>();
            //Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //Container.ContextOptions.LazyLoadingEnabled = false;
            //Container.ContextOptions.ProxyCreationEnabled = false;
            //CrewPassengerVisa.EntityList = Container.GetCrewPaxVisa(crewPaxVisa.CrewID, crewPaxVisa.PassengerRequestorID).ToList();
            //CrewPassengerVisa.ReturnFlag = true;
            //return CrewPassengerVisa;
            throw new NotImplementedException();
        }
        /// <summary>
        /// To insert values from CrewRoster-Visa Grid to CrewPassengerVisa table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerVisa> Add(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa)
        {
            ReturnValue<Data.MasterCatalog.CrewPassengerVisa> CrewPassengerVisa = new ReturnValue<Data.MasterCatalog.CrewPassengerVisa>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPaxVisa))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    crewPaxVisa.VisaNum = Crypting.Encrypt(crewPaxVisa.VisaNum);
                    Container.AddToCrewPassengerVisa(crewPaxVisa);
                    Container.SaveChanges();
                    CrewPassengerVisa.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassengerVisa;
        }
        /// <summary>
        /// To update values from CrewRoster-Visa Grid to CrewPassengerVisa table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerVisa> Update(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa)
        {
            ReturnValue<Data.MasterCatalog.CrewPassengerVisa> CrewPassengerVisa = new ReturnValue<Data.MasterCatalog.CrewPassengerVisa>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPaxVisa))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    crewPaxVisa.VisaNum = Crypting.Encrypt(crewPaxVisa.VisaNum);
                    Container.CrewPassengerVisa.Attach(crewPaxVisa);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewPassengerVisa>(crewPaxVisa, Container);
                    Container.SaveChanges();
                    CrewPassengerVisa.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassengerVisa;
        }
        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-Visa Grid to CrewPassengerVisa table
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerVisa> Delete(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa)
        {
            ReturnValue<Data.MasterCatalog.CrewPassengerVisa> CrewPassengerVisa = new ReturnValue<Data.MasterCatalog.CrewPassengerVisa>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPaxVisa))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.CrewPassengerVisa).Name, crewPaxVisa);
                    Container.CrewPassengerVisa.DeleteObject(crewPaxVisa);
                    Container.SaveChanges();
                    CrewPassengerVisa.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassengerVisa;
        }
        /// <summary>
        /// To fetch the values from the CrewPassengerVisa table based on parameters
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa> GetListInfo(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa)
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa> CrewPassengerVisa = new ReturnValue<Data.MasterCatalog.GetAllCrewPaxVisa>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewPaxVisa))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewPassengerVisa.EntityList = Container.GetAllCrewPaxVisa(crewPaxVisa.CrewID, crewPaxVisa.PassengerRequestorID, crewPaxVisa.CustomerID).ToList();
                    foreach (var item in CrewPassengerVisa.EntityList)
                    {
                        item.VisaNum = Crypting.Decrypt(item.VisaNum);
                    }
                    CrewPassengerVisa.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewPassengerVisa;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewPaxVisa"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewPassengerVisa> GetLock(Data.MasterCatalog.CrewPassengerVisa crewPaxVisa)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
