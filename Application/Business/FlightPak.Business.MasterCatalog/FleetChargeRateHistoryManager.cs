﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetChargeRateHistory : BaseManager, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        public ReturnValue<Data.MasterCatalog.FleetChargeRate> GetList(Int64 FleetID)
        {
            ReturnValue<Data.MasterCatalog.FleetChargeRate> ret = new ReturnValue<Data.MasterCatalog.FleetChargeRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.FleetChargeRateHistory objFleetChargeRateHistory = new FlightPak.Business.MasterCatalog.FleetChargeRateHistory();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllFleetChargeRate(FleetID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;

        }

        public ReturnValue<Data.MasterCatalog.FleetChargeRate> Add(Data.MasterCatalog.FleetChargeRate fleetChargerate)
        {
            ReturnValue<Data.MasterCatalog.FleetChargeRate> ret = new ReturnValue<Data.MasterCatalog.FleetChargeRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargerate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    fleetChargerate.CustomerID = CustomerID;
                    fleetChargerate.IsDeleted = false;
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(fleetChargerate, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.AddToFleetChargeRate(fleetChargerate);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FleetChargeRate> Update(Data.MasterCatalog.FleetChargeRate fleetChargerate)
        {
            ReturnValue<Data.MasterCatalog.FleetChargeRate> ret = new ReturnValue<Data.MasterCatalog.FleetChargeRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetChargerate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(fleetChargerate, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FleetChargeRate.Attach(fleetChargerate);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetChargeRate>(fleetChargerate, cs);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }



        public ReturnValue<Data.MasterCatalog.FleetChargeRate> CalculateFleet(Data.MasterCatalog.FleetChargeRate FleetChargeRate)
        {
            ReturnValue<Data.MasterCatalog.FleetChargeRate> objFleetChargeRate = new ReturnValue<Data.MasterCatalog.FleetChargeRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetChargeRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.CalculateFleetChargeRate(FleetChargeRate.FleetChargeRateID, FleetChargeRate.CustomerID, FleetChargeRate.FleetID, FleetChargeRate.AircraftID, FleetChargeRate.AircraftCD, FleetChargeRate.BeginRateDT, FleetChargeRate.EndRateDT, FleetChargeRate.ChargeRate, FleetChargeRate.ChargeUnit, FleetChargeRate.LastUpdUID, FleetChargeRate.LastUpdTS, FleetChargeRate.IsDeleted);
                    objContainer.SaveChanges();
                    objFleetChargeRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objFleetChargeRate;
        }

        public ReturnValue<Data.MasterCatalog.FleetChargeRate> Delete(Data.MasterCatalog.FleetChargeRate fleetChargerate)
        {
            ReturnValue<Data.MasterCatalog.FleetChargeRate> ret = new ReturnValue<Data.MasterCatalog.FleetChargeRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    fleetChargerate.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.FleetChargeRate).Name, fleetChargerate);
                    cs.FleetChargeRate.DeleteObject(fleetChargerate);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists> GetFleetChargeRateExists(DateTime BeginRateDT, DateTime EndRateDT, Int64 FleetID, Int64 FleetChargeRateID)
        {
            ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleetChargeRateExists>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleetChargeRateExists(BeginRateDT, EndRateDT, FleetID, FleetChargeRateID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetChargeRate> GetLock(Data.MasterCatalog.FleetChargeRate fleetChargerate)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
