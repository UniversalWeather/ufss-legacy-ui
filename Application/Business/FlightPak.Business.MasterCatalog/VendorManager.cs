﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class VendorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Vendor>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.Vendor> GetList()
        {
            //ReturnValue<Data.MasterCatalog.Vendor> objVendor = new ReturnValue<Data.MasterCatalog.Vendor>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

            //// Fix for recursive infinite loop
            //objContainer.ContextOptions.LazyLoadingEnabled = false;
            //objContainer.ContextOptions.ProxyCreationEnabled = false;
            //objVendor.EntityList = objContainer.GetAllVendor(CustomerID,"V").ToList();
            //objVendor.ReturnFlag = true;
            //return objVendor;

            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllVendor> GetVendorListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllVendor> objVendor = new ReturnValue<Data.MasterCatalog.GetAllVendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objVendor.EntityList = objContainer.GetAllVendor(CustomerID, "V").ToList();
                    objVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> Add(Data.MasterCatalog.Vendor vendor)
        {
            ReturnValue<Data.MasterCatalog.Vendor> objVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToVendor(vendor);
                    objContainer.SaveChanges();
                    objVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> Update(Data.MasterCatalog.Vendor vendor)
        {
            ReturnValue<Data.MasterCatalog.Vendor> objVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.Vendor.Attach(vendor);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.Vendor>(vendor, objContainer);
                    objContainer.SaveChanges();
                    objVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> Delete(Data.MasterCatalog.Vendor vendor)
        {
            ReturnValue<Data.MasterCatalog.Vendor> objVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Vendor).Name, vendor);
                    objContainer.Vendor.DeleteObject(vendor);
                    objContainer.SaveChanges();
                    objVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendor;
        }


        public ReturnValue<Data.MasterCatalog.Vendor> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.Vendor> objVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objVendor.EntityList = objContainer.Vendor.ToList();
                    objVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> GetLock(Data.MasterCatalog.Vendor vendor)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
