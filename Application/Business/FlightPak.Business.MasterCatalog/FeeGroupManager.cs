﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
 public  class FeeGroupManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FeeGroup>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for Fee Schedule Group catalog page from FeeGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FeeGroup> GetList()
        {
            ReturnValue<Data.MasterCatalog.FeeGroup> objFeeGroup = new ReturnValue<Data.MasterCatalog.FeeGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFeeGroup.EntityList = objContainer.GetAllFeeGroup(CustomerID).ToList();
                    objFeeGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFeeGroup;
        }

        /// <summary>
        /// To insert values from Lead Source page to Lead Source table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FeeGroup> Add(Data.MasterCatalog.FeeGroup FeeGroup)
        {
            ReturnValue<Data.MasterCatalog.FeeGroup> objFeeGroup = new ReturnValue<Data.MasterCatalog.FeeGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FeeGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFeeGroup.ReturnFlag = base.Validate(FeeGroup, ref objFeeGroup.ErrorMessage);
                    //For Data Anotation
                    if (objFeeGroup.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFeeGroup(FeeGroup);
                        objContainer.SaveChanges();
                        objFeeGroup.ReturnFlag = true;
                        //return objDelayType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFeeGroup;
            }
        }

        /// <summary>
        /// To update values from FeeGroup page to FeeGroup table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FeeGroup> Update(Data.MasterCatalog.FeeGroup FeeGroup)
        {
            ReturnValue<Data.MasterCatalog.FeeGroup> objFeeGroup = new ReturnValue<Data.MasterCatalog.FeeGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFeeGroup.ReturnFlag = base.Validate(FeeGroup, ref objFeeGroup.ErrorMessage);
                    //For Data Anotation
                    if (objFeeGroup.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FeeGroup.Attach(FeeGroup);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FeeGroup>(FeeGroup, objContainer);
                        objContainer.SaveChanges();
                        objFeeGroup.ReturnFlag = true;
                        //return objDelayType;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFeeGroup;
            }

        }
        /// <summary>
        /// To update the selected record as deleted, from FeeGroup page in FeeGroup table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FeeGroup> Delete(Data.MasterCatalog.FeeGroup FeeGroup)
        {

            ReturnValue<Data.MasterCatalog.FeeGroup> objFeeGroup = new ReturnValue<Data.MasterCatalog.FeeGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FeeGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FeeGroup).Name, FeeGroup);
                    objContainer.FeeGroup.DeleteObject(FeeGroup);
                    objContainer.SaveChanges();
                    objFeeGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFeeGroup;

        }
        /// <summary>
        /// To get the values for FeeGroup page from FeeGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FeeGroup> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.FeeGroup> objFeeGroup = new ReturnValue<Data.MasterCatalog.FeeGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFeeGroup.EntityList = objContainer.FeeGroup.ToList();
                    objFeeGroup.ReturnFlag = true;
                    return objFeeGroup;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFeeGroup;
            }

        }
        /// <summary>
        /// To get the values for FeeGroup page from FeeGroup table
        /// </summary>
        /// <returns></returns> 
        public ReturnValue<Data.MasterCatalog.FeeGroup> GetLock(Data.MasterCatalog.FeeGroup t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
