﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
 public  class LeadSourceManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.LeadSource>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for Lead Source catalog page from LeadSource table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.LeadSource> GetList()
        {
            ReturnValue<Data.MasterCatalog.LeadSource> objLeadSource = new ReturnValue<Data.MasterCatalog.LeadSource>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objLeadSource.EntityList = objContainer.GetAllLeadSource(CustomerID).ToList();
                    objLeadSource.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objLeadSource;
        }

        /// <summary>
        /// To insert values from Lead Source page to Lead Source table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.LeadSource> Add(Data.MasterCatalog.LeadSource LeadSource)
        {
            ReturnValue<Data.MasterCatalog.LeadSource> objLeadSource = new ReturnValue<Data.MasterCatalog.LeadSource>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LeadSource))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objLeadSource.ReturnFlag = base.Validate(LeadSource, ref objLeadSource.ErrorMessage);
                    //For Data Anotation
                    if (objLeadSource.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToLeadSource(LeadSource);
                        objContainer.SaveChanges();
                        objLeadSource.ReturnFlag = true;
                        //return objDelayType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLeadSource;
            }
        }

        /// <summary>
        /// To update values from LeadSource page to LeadSource table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.LeadSource> Update(Data.MasterCatalog.LeadSource LeadSource)
        {
            ReturnValue<Data.MasterCatalog.LeadSource> objLeadSource = new ReturnValue<Data.MasterCatalog.LeadSource>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objLeadSource.ReturnFlag = base.Validate(LeadSource, ref objLeadSource.ErrorMessage);
                    //For Data Anotation
                    if (objLeadSource.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.LeadSource.Attach(LeadSource);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.LeadSource>(LeadSource, objContainer);
                        objContainer.SaveChanges();
                        objLeadSource.ReturnFlag = true;
                        //return objDelayType;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLeadSource;
            }

        }
        /// <summary>
        /// To update the selected record as deleted, from LeadSource page in LeadSource table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.LeadSource> Delete(Data.MasterCatalog.LeadSource LeadSource)
        {

            ReturnValue<Data.MasterCatalog.LeadSource> objLeadSource = new ReturnValue<Data.MasterCatalog.LeadSource>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LeadSource))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.LeadSource).Name, LeadSource);
                    objContainer.LeadSource.DeleteObject(LeadSource);
                    objContainer.SaveChanges();
                    objLeadSource.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objLeadSource;

        }
        /// <summary>
        /// To get the values for LeadSource page from LeadSource table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.LeadSource> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.LeadSource> objLeadSource = new ReturnValue<Data.MasterCatalog.LeadSource>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objLeadSource.EntityList = objContainer.LeadSource.ToList();
                    objLeadSource.ReturnFlag = true;
                    return objLeadSource;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLeadSource;
            }

        }
        /// <summary>
        /// To get the values for LeadSource page from LeadSource table
        /// </summary>
        /// <returns></returns> 
        public ReturnValue<Data.MasterCatalog.LeadSource> GetLock(Data.MasterCatalog.LeadSource t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
