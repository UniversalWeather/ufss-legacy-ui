﻿using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.MasterCatalog
{
    public class FuelFieldSynonymManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelFieldSynonym>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.MasterCatalog.FuelFieldSynonym> GetList()
        {
            ReturnValue<Data.MasterCatalog.FuelFieldSynonym> ret = new ReturnValue<Data.MasterCatalog.FuelFieldSynonym>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelFieldSynonym.Where(t => t.IsDeleted == false).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFieldSynonym> Add(Data.MasterCatalog.FuelFieldSynonym objFuelFieldSynonym)
        {
            ReturnValue<Data.MasterCatalog.FuelFieldSynonym> ret = new ReturnValue<Data.MasterCatalog.FuelFieldSynonym>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFieldSynonym))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelFieldSynonym.IsDeleted = false;
                    
                    ret.ReturnFlag = base.Validate(objFuelFieldSynonym, ref ret.ErrorMessage);
                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelFieldSynonym.AddObject(objFuelFieldSynonym);
                        cs.SaveChanges();                       
                        ret.ReturnFlag = true;


                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFieldSynonym> Update(Data.MasterCatalog.FuelFieldSynonym objFuelFieldSynonym)
        {
            ReturnValue<Data.MasterCatalog.FuelFieldSynonym> ret = new ReturnValue<Data.MasterCatalog.FuelFieldSynonym>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFieldSynonym))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    ret.ReturnFlag = base.Validate(objFuelFieldSynonym, ref ret.ErrorMessage);
                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelFieldSynonym.Attach(objFuelFieldSynonym);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelFieldSynonym>(objFuelFieldSynonym, cs);
                        cs.SaveChanges();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelFieldSynonym> Delete(Data.MasterCatalog.FuelFieldSynonym objFuelFieldSynonym)
        {
            ReturnValue<Data.MasterCatalog.FuelFieldSynonym> ret = new ReturnValue<Data.MasterCatalog.FuelFieldSynonym>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFieldSynonym))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelFieldSynonym.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.FuelFieldSynonym).Name, objFuelFieldSynonym);
                    cs.FuelFieldSynonym.DeleteObject(objFuelFieldSynonym);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFieldSynonym> GetLock(Data.MasterCatalog.FuelFieldSynonym objFuelFieldSynonym)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
