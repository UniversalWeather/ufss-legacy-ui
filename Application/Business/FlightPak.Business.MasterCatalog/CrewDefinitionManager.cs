﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewDefinitionManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewDefinition>, IMasterCatalog.ICacheable
    {
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        private ExceptionManager exManager;
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDefinition> GetList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To insert values from CrewRoster-AdditionalInformation Grid to CrewDefinition table
        /// </summary>
        /// <param name="crewDefinition"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDefinition> Add(Data.MasterCatalog.CrewDefinition crewDefinition)
        {
            ReturnValue<Data.MasterCatalog.CrewDefinition> CrewDefinition = new ReturnValue<Data.MasterCatalog.CrewDefinition>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDefinition ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    crewDefinition.InformationValue = Crypting.Encrypt(crewDefinition.InformationValue);
                    objContainer.AddToCrewDefinition(crewDefinition);
                    objContainer.SaveChanges();
                    CrewDefinition.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewDefinition;
        }

        /// <summary>
        /// To update values from CrewRoster-AdditionalInformation Grid to CrewDefinition table
        /// </summary>
        /// <param name="crewDefinition"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDefinition> Update(Data.MasterCatalog.CrewDefinition crewDefinition)
        {
            ReturnValue<Data.MasterCatalog.CrewDefinition> CrewDefinition = new ReturnValue<Data.MasterCatalog.CrewDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDefinition ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    if (crewDefinition.InformationValue != null)
                    {
                        if (crewDefinition.InformationValue.Trim() != "")
                        {
                            crewDefinition.InformationValue = Crypting.Encrypt(crewDefinition.InformationValue);
                        }
                        else
                        {
                            crewDefinition.InformationValue = null;
                        }
                        
                    }
                    else
                    {
                        crewDefinition.InformationValue = null;
                    }
                    objContainer.CrewDefinition.Attach(crewDefinition);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewDefinition>(crewDefinition, objContainer);
                    objContainer.SaveChanges();
                    CrewDefinition.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return CrewDefinition;
        }

        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-AdditionalInformation Grid to CrewDefinition table
        /// </summary>
        /// <param name="crewDefinition"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDefinition> Delete(Data.MasterCatalog.CrewDefinition crewDefinition)
        {
            ReturnValue<Data.MasterCatalog.CrewDefinition> CrewDefinition = new ReturnValue<Data.MasterCatalog.CrewDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDefinition ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.CrewDefinition).Name, crewDefinition);
                    objContainer.CrewDefinition.DeleteObject(crewDefinition);
                    objContainer.SaveChanges();
                    CrewDefinition.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
	
            return CrewDefinition;
        }

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewDefinition"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDefinition> GetLock(Data.MasterCatalog.CrewDefinition crewDefinition)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To fetch the values from the CrewDefinition table based on parameters
        /// </summary>
        /// <param name="crewDefinition"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetCrewDefinition> GetListInfo(Data.MasterCatalog.GetCrewDefinition crewDefinition)
        {
            ReturnValue<Data.MasterCatalog.GetCrewDefinition> CrewDefinition = new ReturnValue<Data.MasterCatalog.GetCrewDefinition>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDefinition ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    CrewDefinition.EntityList = objContainer.GetCrewDefinition(CustomerID, crewDefinition.CrewID).ToList();
                    foreach (var item in CrewDefinition.EntityList)
                    {
                        if (item.InformationValue != null)
                        {
                            if (item.InformationValue.Trim() != "")
                            {
                                item.InformationValue = Crypting.Decrypt(item.InformationValue);
                            }
                            else
                            {
                                item.InformationValue = string.Empty;
                            }
                        }
                        else
                        {
                            item.InformationValue = string.Empty;
                        }
                    }
                    CrewDefinition.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewDefinition;
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
