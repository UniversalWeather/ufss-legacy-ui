﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewCheckListDateManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewCheckListDetail>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewCheckListDetail> GetList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To insert values from CrewRoster-Checklist Grid to CrewChecklistDate table
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewCheckListDetail> Add(Data.MasterCatalog.CrewCheckListDetail crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckListDetail> CrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckListDetail>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToCrewCheckListDetail(crewChecklistDate);
                    Container.SaveChanges();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// To update values from CrewRoster-Checklist Grid to CrewChecklistDate table
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewCheckListDetail> Update(Data.MasterCatalog.CrewCheckListDetail crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckListDetail> CrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckListDetail>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.CrewCheckListDetail.Attach(crewChecklistDate);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewCheckListDetail>(crewChecklistDate, Container);
                    Container.SaveChanges();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-Checklist Grid to CrewChecklistDate table
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewCheckListDetail> Delete(Data.MasterCatalog.CrewCheckListDetail crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckListDetail> CrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckListDetail>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.CrewCheckListDetail).Name, crewChecklistDate);
                    Container.CrewCheckListDetail.DeleteObject(crewChecklistDate);
                    Container.SaveChanges();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// To fetch the values from the CrewChecklistDate table based on parameters
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetCrewCheckListDate> GetListInfo(Data.MasterCatalog.GetCrewCheckListDate crewChecklistDate)
        {
            ReturnValue<Data.MasterCatalog.GetCrewCheckListDate> CrewCheckList = new ReturnValue<Data.MasterCatalog.GetCrewCheckListDate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewChecklistDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewCheckList.EntityList = Container.GetCrewCheckListDate(CustomerID, crewChecklistDate.CrewID).ToList();
                    CrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewCheckList;
        }

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewChecklistDate"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewCheckListDetail> GetLock(Data.MasterCatalog.CrewCheckListDetail crewChecklistDate)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
