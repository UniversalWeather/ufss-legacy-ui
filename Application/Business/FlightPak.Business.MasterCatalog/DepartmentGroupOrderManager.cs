﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class DepartmentGroupOrderManager : BaseManager
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the available values for DepartmentGroup page from DepartmentGroupOrder table based on DepartmentCode 
        /// </summary>
        /// <param name="DepartmentCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Department> GetAvailableList(Int64 DepartmentCode)
        {
            ReturnValue<Data.MasterCatalog.Department> DepartmentMaster = new ReturnValue<Data.MasterCatalog.Department>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    DepartmentMaster.EntityList = Container.GetDeptAvailableList(CustomerID, DepartmentCode,ClientId).ToList();
                    DepartmentMaster.ReturnFlag = true;
                    return DepartmentMaster;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return DepartmentMaster;
            }

        }
        /// <summary>
        /// To get the selected values for DepartmentGroup page from DepartmentGroupOrder table based on DepartmentCode 
        /// </summary>
        /// <param name="DepartmentCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Department> GetSelectedList(Int64 DepartmentCode)
        {
            ReturnValue<Data.MasterCatalog.Department> DepartmentMaster = new ReturnValue<Data.MasterCatalog.Department>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    DepartmentMaster.EntityList = Container.GetDeptSelectedList(CustomerID, DepartmentCode).ToList();
                    DepartmentMaster.ReturnFlag = true;
                    return DepartmentMaster;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return DepartmentMaster;
            }

        }
        /// <summary>
        /// To insert values from DepartmentGroup page to DepartmentGroupOrder table
        /// </summary>
        /// <param name="DeptGroupOrder"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> Add(Data.MasterCatalog.DepartmentGroupOrder DeptGroupOrder)
        {
            ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> DeptGroupOrderMaster = new ReturnValue<Data.MasterCatalog.DepartmentGroupOrder>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DeptGroupOrder))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToDepartmentGroupOrder(DeptGroupOrder);
                    Container.SaveChanges();
                    DeptGroupOrderMaster.ReturnFlag = true;
                    return DeptGroupOrderMaster;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return DeptGroupOrderMaster;
            }

        }
        /// <summary>
        /// To update the selected record as deleted, from DepartmentGroup page in DepartmentGroupOrder table
        /// </summary>
        /// <param name="DeptGroupOrder"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> Delete(Data.MasterCatalog.DepartmentGroupOrder DeptGroupOrder)
        {
            ReturnValue<Data.MasterCatalog.DepartmentGroupOrder> DeptGroupOrderMaster = new ReturnValue<Data.MasterCatalog.DepartmentGroupOrder>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DeptGroupOrder))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.DepartmentGroupOrder).Name, DeptGroupOrder);
                    Container.DepartmentGroupOrder.DeleteObject(DeptGroupOrder);
                    Container.SaveChanges();
                    DeptGroupOrderMaster.ReturnFlag = true;
                    return DeptGroupOrderMaster;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return DeptGroupOrderMaster;
            }

        }
    }
}
