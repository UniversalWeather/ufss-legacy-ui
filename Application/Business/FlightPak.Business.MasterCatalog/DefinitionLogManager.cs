﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class DefinitionLogManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TSDefinitionLog>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for CustomFlightLog page from TSDefinitionLog table
        /// </summary>
        /// <returns></returns>       
        public ReturnValue<Data.MasterCatalog.GetAllPilotLogReset> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllPilotLogReset> PilotLogReset = new ReturnValue<Data.MasterCatalog.GetAllPilotLogReset>();
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.ProxyCreationEnabled = false;
            PilotLogReset.EntityList = Container.GetAllPilotLogReset(CustomerID).ToList();
            PilotLogReset.ReturnFlag = true;
            return PilotLogReset;
        }
        /// <summary>
        /// To insert values from CustomFlightLog page to TSDefinitionLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSDefinitionLog> Add(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            ReturnValue<Data.MasterCatalog.TSDefinitionLog> ReturnValue = new ReturnValue<Data.MasterCatalog.TSDefinitionLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DefinitionData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToTSDefinitionLog(DefinitionData);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        ///  To update values from CustomFlightLog page to TSDefinitionLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSDefinitionLog> Update(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            ReturnValue<Data.MasterCatalog.TSDefinitionLog> ReturnValue = new ReturnValue<Data.MasterCatalog.TSDefinitionLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DefinitionData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.TSDefinitionLog.Attach(DefinitionData);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.TSDefinitionLog>(DefinitionData, Container);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        ///  To update the selected record as deleted, from CustomFlightLog page in TSDefinitionLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSDefinitionLog> Delete(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            ReturnValue<Data.MasterCatalog.TSDefinitionLog> ReturnValue = new ReturnValue<Data.MasterCatalog.TSDefinitionLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DefinitionData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.TSDefinitionLog).Name, DefinitionData);
                    Container.TSDefinitionLog.DeleteObject(DefinitionData);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To fetch the values from the TSDefinitionLog table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSDefinitionLog> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSDefinitionLog> GetLock(Data.MasterCatalog.TSDefinitionLog DefinitionData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
