﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace FlightPak.Business.MasterCatalog
{
    public class TripManagerCheckListManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TripManagerCheckList>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization



        public ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters> GetTripManagerCheckListWithFilters(long CheckListID, string CheckListCD, long CheckGroupID, bool IsInactive)
        {
            ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters> objtripManagerCheckList = new ReturnValue<Data.MasterCatalog.GetTripManagerCheckListWithFilters>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objtripManagerCheckList.EntityList = objContainer.GetTripManagerCheckListWithFilters(CustomerID,CheckListID ,CheckListCD,CheckGroupID ,IsInactive).ToList();
                    objtripManagerCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objtripManagerCheckList;
        }

        #endregion
        public ReturnValue<Data.MasterCatalog.TripManagerCheckList> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList> objtripManagerCheckList = new ReturnValue<Data.MasterCatalog.GetAllTripManagerCheckList>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objtripManagerCheckList.EntityList = objContainer.GetAllTripManagerCheckList(CustomerID).ToList();
                    objtripManagerCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return objtripManagerCheckList;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckList> Add(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckList> objtripManagerCheckList = new ReturnValue<Data.MasterCatalog.TripManagerCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objtripManagerCheckList.ReturnFlag = base.Validate(tripManagerChecklist, ref objtripManagerCheckList.ErrorMessage);
                    //For Data Anotation
                    if (objtripManagerCheckList.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToTripManagerCheckList(tripManagerChecklist);
                        objContainer.SaveChanges();
                        objtripManagerCheckList.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objtripManagerCheckList;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckList> Update(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckList> objtripManagerCheckList = new ReturnValue<Data.MasterCatalog.TripManagerCheckList>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objtripManagerCheckList.ReturnFlag = base.Validate(tripManagerChecklist, ref objtripManagerCheckList.ErrorMessage);
                    //For Data Anotation
                    if (objtripManagerCheckList.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.TripManagerCheckList.Attach(tripManagerChecklist);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.TripManagerCheckList>(tripManagerChecklist, objContainer);
                        objContainer.SaveChanges();
                        objtripManagerCheckList.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return objtripManagerCheckList;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckList> Delete(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            ReturnValue<Data.MasterCatalog.TripManagerCheckList> objtripManagerCheckList = new ReturnValue<Data.MasterCatalog.TripManagerCheckList>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripManagerChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TripManagerCheckList).Name, tripManagerChecklist);
                    objContainer.TripManagerCheckList.DeleteObject(tripManagerChecklist);
                    objContainer.SaveChanges();
                    objtripManagerCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objtripManagerCheckList;
        }

        public ReturnValue<Data.MasterCatalog.TripManagerCheckList> GetLock(Data.MasterCatalog.TripManagerCheckList tripManagerChecklist)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
