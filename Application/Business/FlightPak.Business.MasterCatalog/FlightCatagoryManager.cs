﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class FlightCategoryManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FlightCatagory>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters> GetAllFlightCategoryWithFilters(long ClientID, long FlightCategoryID, string FlightCategoryCD, bool FetchInactiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters> objFlightCategory = new ReturnValue<Data.MasterCatalog.GetAllFlightCategoryWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightCategory.EntityList = objContainer.GetAllFlightCategoryWithFilters(CustomerID, ClientID, FlightCategoryID, FlightCategoryCD, FetchInactiveOnly).ToList();
                    objFlightCategory.ReturnFlag = true;
                    return objFlightCategory;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightCategory;
            }
        }
        #endregion

        /// <summary>
        /// To get the values for FlightCategory page from FlightCategory table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCatagory> GetList()
        {
            ReturnValue<Data.MasterCatalog.FlightCatagory> objFlightCategory = new ReturnValue<Data.MasterCatalog.FlightCatagory>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightCategory.EntityList = objContainer.GetAllFlightCategory(CustomerID, ClientId).ToList();
                    //if (objFlightCategory.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objFlightCategory.EntityList.Count; i++)
                    //    {
                    //        objFlightCategory.EntityList[i].FlightCatagoryCD = objFlightCategory.EntityList[i].FlightCatagoryCD != null ? objFlightCategory.EntityList[i].FlightCatagoryCD.Trim() : string.Empty;
                    //    }
                    //}
                    objFlightCategory.ReturnFlag = true;
                    return objFlightCategory;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightCategory;
            }
        }
        /// <summary>
        /// To insert values from FlightCategory page to FlightCategory table
        /// </summary>
        /// <param name="flightCategory"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCatagory> Add(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            ReturnValue<Data.MasterCatalog.FlightCatagory> objFlightCategory = new ReturnValue<Data.MasterCatalog.FlightCatagory>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFlightCategory.ReturnFlag = base.Validate(flightCategory, ref objFlightCategory.ErrorMessage);
                    //For Data Anotation
                    if (objFlightCategory.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFlightCatagory(flightCategory);
                        objContainer.SaveChanges();
                        objFlightCategory.ReturnFlag = true;
                        //return objFlightCategory;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightCategory;
            }
        }
        /// <summary>
        /// To Update values from FlightCategory page to FlightCategory table 
        /// </summary>
        /// <param name="flightCategory"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCatagory> Update(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            ReturnValue<Data.MasterCatalog.FlightCatagory> objFlightCategory = new ReturnValue<Data.MasterCatalog.FlightCatagory>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFlightCategory.ReturnFlag = base.Validate(flightCategory, ref objFlightCategory.ErrorMessage);
                    //For Data Anotation
                    if (objFlightCategory.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FlightCatagory.Attach(flightCategory);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FlightCatagory>(flightCategory, objContainer);
                        objContainer.SaveChanges();
                        objFlightCategory.ReturnFlag = true;
                        //return objFlightCategory;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightCategory;
            }
        }
        /// <summary>
        // To Delete values of FlightCategory page to FlightCategory table
        /// </summary>
        /// <param name="flightCategory"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCatagory> Delete(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            ReturnValue<Data.MasterCatalog.FlightCatagory> objFlightCategory = new ReturnValue<Data.MasterCatalog.FlightCatagory>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightCategory))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FlightCatagory).Name, flightCategory);
                    objContainer.FlightCatagory.DeleteObject(flightCategory);
                    objContainer.SaveChanges();
                    objFlightCategory.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFlightCategory;
        }
        /// <summary>
        /// To get the values for FlightCategory page from FlightCategory table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightCategories> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllFlightCategories> objFlightCategory = new ReturnValue<Data.MasterCatalog.GetAllFlightCategories>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightCategory.EntityList = objContainer.GetAllFlightCategories(CustomerID, ClientId).ToList();
                    objFlightCategory.ReturnFlag = true;
                    return objFlightCategory;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightCategory;
            }
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCatagory> GetLock(Data.MasterCatalog.FlightCatagory flightCategory)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Added by Scheduling calendar team to get FlightCategory Entity by FlightCategory Code.
        /// </summary>
        /// <param name="flightCategoryCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult> GetFlightCategoryByCategoryCD(string flightCategoryCode)
        {
            ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult> flightCategory = new ReturnValue<Data.MasterCatalog.FlightCategoryByCategoryCDResult>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods through exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    flightCategory.EntityList = Container.GetFlightCategoryByCategoryCD(CustomerID, flightCategoryCode, ClientId).ToList();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return flightCategory;
        }

        #region "Get FlightCatagory for Client"
        public ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient> GetFlightCategoryForClient()
        {
            ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient> objFlightCategory = new ReturnValue<Data.MasterCatalog.GetFlightCategoryForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightCategory.EntityList = objContainer.GetFlightCategoryForClient(CustomerID, ClientId).ToList();
                    objFlightCategory.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFlightCategory;
        }
        #endregion
    }
}
