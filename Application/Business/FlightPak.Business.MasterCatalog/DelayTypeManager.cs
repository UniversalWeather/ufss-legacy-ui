﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class DelayTypeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.DelayType>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization


        public ReturnValue<Data.MasterCatalog.DelayType> GetAllDelayTypeWithFilters(long DelayTypeID, string DelayTypeCD, bool FetchActiveOnly)
        {

            ReturnValue<Data.MasterCatalog.DelayType> objDelayType = new ReturnValue<Data.MasterCatalog.DelayType>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objDelayType.EntityList = objContainer.GetAllDelayTypeWithFilters(CustomerID,DelayTypeID, DelayTypeCD, FetchActiveOnly).ToList();
                    objDelayType.ReturnFlag = true;
                    return objDelayType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDelayType;
            }

        }

        #endregion
        /// <summary>
        /// To get the values for Delaytype page from DelayType table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DelayType> GetList()
        {

            ReturnValue<Data.MasterCatalog.DelayType> objDelayType = new ReturnValue<Data.MasterCatalog.DelayType>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objDelayType.EntityList = objContainer.GetAllDelayType(CustomerID).ToList();
                    objDelayType.ReturnFlag = true;
                    return objDelayType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDelayType;
            }

        }
        /// <summary>
        /// To insert values from Delaytype page to Delaytype table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DelayType> Add(Data.MasterCatalog.DelayType delayType)
        {
            ReturnValue<Data.MasterCatalog.DelayType> objDelayType = new ReturnValue<Data.MasterCatalog.DelayType>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                     //For Data Anotation
                    objDelayType.ReturnFlag = base.Validate(delayType, ref objDelayType.ErrorMessage);
                     //For Data Anotation
                    if (objDelayType.ReturnFlag)
                     {
                         Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                         objContainer.AddToDelayType(delayType);
                         objContainer.SaveChanges();
                         objDelayType.ReturnFlag = true;
                         //return objDelayType;
                     }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDelayType;
            }

        }
        /// <summary>
        /// To update values from Delaytype page to Delaytype table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DelayType> Update(Data.MasterCatalog.DelayType delayType)
        {

            ReturnValue<Data.MasterCatalog.DelayType> objDelayType = new ReturnValue<Data.MasterCatalog.DelayType>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objDelayType.ReturnFlag = base.Validate(delayType, ref objDelayType.ErrorMessage);
                    //For Data Anotation
                    if (objDelayType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.DelayType.Attach(delayType);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.DelayType>(delayType, objContainer);
                        objContainer.SaveChanges();
                        objDelayType.ReturnFlag = true;
                        //return objDelayType;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDelayType;
            }

        }
        /// <summary>
        /// To update the selected record as deleted, from Delaytype page in Delaytype table
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DelayType> Delete(Data.MasterCatalog.DelayType delayType)
        {


            ReturnValue<Data.MasterCatalog.DelayType> objDelaytype = new ReturnValue<Data.MasterCatalog.DelayType>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(delayType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.DelayType).Name, delayType);
                    objContainer.DelayType.DeleteObject(delayType);
                    objContainer.SaveChanges();
                    objDelaytype.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objDelaytype;
        }
        /// <summary>
        /// To get the values for DelayType page from DelayType table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DelayType> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.DelayType> objDelaytype = new ReturnValue<Data.MasterCatalog.DelayType>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objDelaytype.EntityList = objContainer.DelayType.ToList();
                    objDelaytype.ReturnFlag = true;
                    return objDelaytype;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDelaytype;
            }

        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DelayType> GetLock(Data.MasterCatalog.DelayType delayType)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}






