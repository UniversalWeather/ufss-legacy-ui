﻿using FlightPak.Business.Common;
using FlightPak.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FlightPak.Business.MasterCatalog
{
    public class FlightpakFuelManager : BaseManager,IMasterCatalog.IMasterData<Data.MasterCatalog.FlightpakFuel>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.MasterCatalog.FlightpakFuel> GetList()
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> ret = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                   
                    ret.EntityList = cs.FlightpakFuel.Where(t => t.IsActive == true).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData> GetFlightpakFuelListByFilter(long customerId,long? vendorId, string fboName, long? airportIcaoId, DateTime? effectiveDate, bool? isActive,int pageSize,int pageIndex)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData> ret = new ReturnValue<Data.MasterCatalog.FlightpakFuelHistoricalData>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    int startRow = ((pageIndex - 1)*pageSize) + 1;
                    int endRow = (startRow + pageSize) - 1;
                    int skipRow = pageSize * (pageIndex - 1);
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = new List<Data.MasterCatalog.FlightpakFuelHistoricalData>(cs.spGetAllFlightpakFuelHistoricalDataByVendor(vendorId, customerId, fboName, airportIcaoId, effectiveDate, startRow, endRow));
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<int> CountNumberOfFlightpakFuelRecordsForVendor(long fuelVendorId)
        {
            ReturnValue<int> ret = new ReturnValue<int>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;

                    ret.EntityInfo = cs.FlightpakFuel.Include("Airport").Count(t => t.FuelVendorID == fuelVendorId && t.DepartureICAOID == t.Airport.AirportID && t.Airport.CustomerID == CustomerID) + cs.HistoricalFuelPrice.Include("Airport").Count(t => t.FuelVendorID == fuelVendorId && t.DepartureICAOID == t.Airport.AirportID && t.Airport.CustomerID == CustomerID);
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public Business.Common.ReturnValue<FlightpakFuel> GetFlightpakFuelListById(long flightpakFuelId, long customerId)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> ret = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;

                    ret.EntityList = cs.FlightpakFuel.Include("Airport").Where(t => t.IsActive == true && t.FlightpakFuelID == flightpakFuelId && t.CustomerID == customerId).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FlightpakFuel> Add(Data.MasterCatalog.FlightpakFuel flightpakFuel)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> objFlightpakFuel = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightpakFuel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFlightpakFuel.ReturnFlag = base.Validate(flightpakFuel, ref objFlightpakFuel.ErrorMessage);
                    //For Data Anotation
                    if (objFlightpakFuel.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFlightpakFuel(flightpakFuel);
                        objContainer.SaveChanges();
                        objFlightpakFuel.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFlightpakFuel;
        }        
        public List<Data.MasterCatalog.FlightpakFuel> PopulateValidFuelListFromFuelList(List<Data.MasterCatalog.FlightpakFuel> objFlightpakFuelList)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> ret = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFlightpakFuelList))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    long lastid = cs.FlightpakFuel.Select(t => t.FlightpakFuelID).Max();
                    lastid++;
                    List<FlightPak.Data.MasterCatalog.FlightpakFuel> validFuelpakList = new List<Data.MasterCatalog.FlightpakFuel>();
                    foreach (var item in objFlightpakFuelList)
                    {
                        item.FlightpakFuelID = lastid;
                        item.IsActive = true;
                        //For Data Anotation
                        ret.ReturnFlag = base.Validate(item, ref ret.ErrorMessage);
                        if (ret.ReturnFlag)
                        {
                            validFuelpakList.Add(item);
                            lastid++;
                        }
                    }
                    objFlightpakFuelList = new List<Data.MasterCatalog.FlightpakFuel>();
                    objFlightpakFuelList = validFuelpakList;
                    
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFlightpakFuelList;
        }
        public ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> AddBulk(FuelFileData objFuelFileData, string identityName, long customerId)
        {
            ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> ret = new ReturnValue<List<Data.MasterCatalog.FlightpakFuel>>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFileData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>                
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        EntityConnection entityConnection = objContainer.Connection as EntityConnection;
                        var objResult = GetListByFilePath(objFuelFileData, identityName, customerId);
                        if (objResult.ReturnFlag == true)
                        {
                            var objFlightpakFuelList = objResult.EntityInfo;                            
                            objFlightpakFuelList = PopulateValidFuelListFromFuelList(objFlightpakFuelList);
                            string testconnectionstring = entityConnection.StoreConnection.ConnectionString;

                            // Check is the User upload fixed error log csv file. If it True then append records Other wise Do it Normal way
                            if (objFuelFileData.IsErrorLogFile == false)
                            {
                                if (objFuelFileData.IsMaintainHistoricalFuelData)
                                {
                                    // Move old Fuel Data to Historical Fule Data Table
                                    objContainer.spMoveFlightpakFuelDataToHistoricalFuelPriceTbl(objFuelFileData.VendorID, customerId, identityName, DateTime.Now);
                                } else
                                {
                                    // Call stored procedure to Delete Previous data                            
                                    objContainer.spDeleteFlightpakFuelDataByFuelVendorID(objFuelFileData.VendorID, identityName, DateTime.Now, false);
                                }
                            }
                            // Call to insert new file data
                            BulkUploadManager.BulkInsert(testconnectionstring, FlightPak.Common.Constants.EntitySet.Database.FlightpakFuel, objFlightpakFuelList);
                            ret.EntityInfo = objFlightpakFuelList; // Return Valid Records List as Entity Info
                            ret.EntityList = new List<List<FlightpakFuel>>(); // Return InValid Records list as Entity List
                            ret.EntityList.Add(objResult.EntityList[0]);
                            ret.ReturnFlag = true;
                            ret.ErrorMessage = objResult.ErrorMessage;
                            // Update FuelVendor Info Related to file name
                            FuelVendorManager fVendorManger= new FuelVendorManager();
                            FuelVendor objFuelVendor= new FuelVendor();
                            //objFuelVendor.FuelVendorID=objFuelFileData.VendorID;
                            objFuelVendor = fVendorManger.GetFuelVendorByFuelVendorFilter(objFuelFileData.VendorID, "").EntityInfo;

                            if (objFuelVendor != null)
                            {
                                objFuelVendor.NoOfFuelRecordsInserted = objFuelFileData.IsErrorLogFile ? (objFuelVendor.NoOfFuelRecordsInserted ?? 0) + Convert.ToInt64(objFlightpakFuelList.Count) : Convert.ToInt64(objFlightpakFuelList.Count);
                                objFuelVendor.LastUpdUID = identityName;
                                objFuelVendor.LastUpdTS = DateTime.Now;
                                objFuelVendor.FileTYPE = "CSV";
                                objFuelVendor.FileLocation = objFuelFileData.FilePath;
                                objFuelVendor.BaseFileName = objFuelFileData.BaseFileName;
                                fVendorManger.Update(objFuelVendor);
                            }

                            // Adding new file format;
                            if (objFuelFileData.IsCustomMapped || objFuelVendor.FuelSavedFileFormat==null|| objFuelVendor.FuelSavedFileFormat.Count == 0)
                            {
                                SaveFileFormat(objFuelFileData,identityName,customerId);
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public void SaveFileFormat(FuelFileData objFuelFileData,string identityName,long customerId)
        {
            //FuelSavedFileFormatManager saveFileFormatManager = new FuelSavedFileFormatManager();
            
            //FuelSavedFileFormat fileFormat= new FuelSavedFileFormat();
            //fileFormat.VendorID= objFuelFileData.VendorID;
            //fileFormat.LastUpdUID=identityName;
            //fileFormat.LastUpdTS=DateTime.Now;
            //fileFormat.IsInActive=true;
            //fileFormat.IsDeleted=false;
            //fileFormat.CustomerID= customerId;
            //fileFormat.FuelCsvFileFields= new System.Data.Objects.DataClasses.EntityCollection<FuelCsvFileFields>();
            
            //// Get Title rows.
            //// get all Required Fields

            //FuelFieldMasterManager fieldManager = new FuelFieldMasterManager();
            //var fieldList = fieldManager.GetList();

            //var rows = Utility.FetchPreviewRecordsInSession(objFuelFileData.UploadFileData, 1).FirstOrDefault();
            //for (int i = 0; i < rows.Count; i++)            
            //{
            //    FuelCsvFileFields csvField = new FuelCsvFileFields();
            //    csvField.IsDeleted = false;
            //    csvField.IsInActive = false; 
            //    csvField.LastUpdTS = DateTime.Now;
            //    csvField.LastUpdUID = identityName;

            //    if (objFuelFileData.FBO == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "FBO").Select(t=>t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.Vendor==i && objFuelFileData.FBO < 0)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "FBO").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.EffectiveDate == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "EFFECTIVEDATE").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.ICAO == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "ICAO").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.IATA == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "IATA").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.Low == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "LOW").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.High == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "HIGH").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.Price == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "PRICE").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }
            //    else if (objFuelFileData.Text == i)
            //    {
            //        csvField.FieldTitle = rows[i];
            //        csvField.FuelFieldID = fieldList.EntityList.Where(f => f.Title.Trim().ToUpper() == "TEXT").Select(t => t.FuelFieldID).FirstOrDefault();
            //    }


            //    if (csvField.FuelFieldID > 0 && csvField.FieldTitle != null && csvField.FieldTitle.Trim().Length > 0)
            //    {
            //        fileFormat.FuelCsvFileFields.Add(csvField);
            //    }
            //}
            //saveFileFormatManager.Add(fileFormat);
        }

        public ReturnValue<Data.MasterCatalog.FlightpakFuel> Update(Data.MasterCatalog.FlightpakFuel flightpakFuel)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> objFlightpakFuel = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightpakFuel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFlightpakFuel.ReturnFlag = base.Validate(flightpakFuel, ref objFlightpakFuel.ErrorMessage);
                    //For Data Anotation
                    if (objFlightpakFuel.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FlightpakFuel.Attach(flightpakFuel);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FlightpakFuel>(flightpakFuel, objContainer);
                        objContainer.SaveChanges();
                        objFlightpakFuel.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFlightpakFuel;
        }

        public ReturnValue<Data.MasterCatalog.FlightpakFuel> Delete(Data.MasterCatalog.FlightpakFuel flightpakFuel)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> objFlightpakFuel = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightpakFuel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FlightpakFuel).Name, flightpakFuel);
                    objContainer.FlightpakFuel.DeleteObject(flightpakFuel);
                    objContainer.SaveChanges();
                    objFlightpakFuel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFlightpakFuel;
        }

        public ReturnValue<Data.MasterCatalog.FlightpakFuel> GetLock(Data.MasterCatalog.FlightpakFuel flightpakFuel)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Dictionary<string, string>> CheckFileFormat(String fileTitlesCSVformat)
        {
            ReturnValue<Dictionary<string, string>> ret = new ReturnValue<Dictionary<string, string>>();
            Dictionary<string, string> fileformats = new Dictionary<string, string>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fileTitlesCSVformat))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //string[] titles = fileTitlesCSVformat.Split(',');
                    
                    //Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    //cs.ContextOptions.LazyLoadingEnabled = false;
                    //cs.ContextOptions.ProxyCreationEnabled = false;
                    //FuelFieldMasterManager fieldManagerManager= new FuelFieldMasterManager();

                    //var savedFileFormats = cs.FuelSavedFileFormat.Include("FuelCsvFileFields").Where(t => t.IsDeleted == false).ToList();
                    //var fieldMasterList = fieldManagerManager.GetList();
                    //for (int i = 0; i < savedFileFormats.Count; i++)
                    //{
                    //    var item = savedFileFormats[i];
                    //    var csvFilefields=item.FuelCsvFileFields.Where(f=>f.IsDeleted==false).ToList();
                        
                    //    for (int k = 0; k < csvFilefields.Count; k++)
                    //    {
                    //        var tempObj=csvFilefields.ElementAt(k);

                    //        for (int a = 0; a < titles.Length; a++)
                    //        {
                    //            if (tempObj.FieldTitle.Trim().ToUpper().Equals(titles[a].Trim().ToUpper()))
                    //            {
                    //                string mainName=fieldMasterList.EntityList.Where(f=>f.FuelFieldID==tempObj.FuelFieldID).Select(t=>t.Title).FirstOrDefault().Trim().ToUpper();
                    //                if(mainName!=null && !fileformats.ContainsKey(mainName))
                    //                {                                        
                    //                    fileformats.Add(mainName,titles[a].Trim());
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }

                    //    if (fileformats.Count == csvFilefields.Count && csvFilefields.Count > 0)
                    //    {
                    //        var data = new List<Dictionary<string, string>>();
                    //        data.Add(fileformats);                           
                    //        ret.ReturnFlag = true;
                    //        ret.EntityList = data;
                    //        break;
                    //    }
                    //    else
                    //    {                           
                    //        ret.ReturnFlag = false;
                    //        ret.EntityInfo = fileformats;
                    //        ret.EntityList = null;
                    //    }

                    //}

                }, FlightPak.Common.Constants.Policy.DataLayer);            
            }
            return ret;
        }

        public ReturnValue<FuelFileData> CheckFileDataByDataformat(FuelFileData objFuelFileData)
        {
            ReturnValue<FuelFileData> ret = new ReturnValue<FuelFileData>();
            Dictionary<string, string> fileformats = new Dictionary<string, string>();
            string[] ColumnSplitter = new string[] { "\",\"" };
            char SplitDelimiters = ',';

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelFileData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    RecognizeByData(ColumnSplitter,SplitDelimiters,objFuelFileData);

                    if (objFuelFileData.haveRequiredColumnsFound())
                    {
                        objFuelFileData.isAllFieldSet = true;
                        ret.ReturnFlag = true;
                        ret.EntityInfo = objFuelFileData;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;

        }

        private void RecognizeByData(string[] ColumnSplitter, char SplitDelimiters, FuelFileData objFuelFileCSV)
        {

            string line;
            string[] columns = null;
            int cnt = 0;
            int firstRowColumns = 0;
            
            using (StringReader sr = new StringReader(objFuelFileCSV.UploadFileData))
            {
                
                while ((line = sr.ReadLine()) != null)
                {
                                       
                    cnt++;
                    
                    if (line.Contains(ColumnSplitter[0]))
                    {
                        columns = line.Split(ColumnSplitter, StringSplitOptions.None);
                    }
                    else
                    {
                        columns = line.Split(SplitDelimiters);
                    }
                    for (int Index = 0; Index < columns.Count(); Index++)
                    {
                        if (columns[Index].Contains("\""))
                        {
                            columns[Index] = columns[Index].Replace("\"", "");
                        }
                    }
                    int columnCount = columns.Count();

                    if (cnt == 1)
                    {

                        FilterByBasicChecking(objFuelFileCSV,columns);
                        firstRowColumns = columnCount;
                        columnCount = 0;
                    }

                    for (int i = 0; i < columnCount; i++)
                    {
                        if (columns[i].Equals(""))
                        {
                            continue;
                        }
                        if (objFuelFileCSV.Price < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                        {
                            decimal temp1;
                            if ((decimal.TryParse(columns[i], out temp1)) && (columns[i].Length <= 10 && columns[i].Length >= 2))
                            {
                                if(temp1 > 0)
                                    objFuelFileCSV.Price = i;
                            }
                        }
                        if (objFuelFileCSV.EffectiveDate < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                        {
                            DateTime tempdate;
                            if ((DateTime.TryParse(columns[i], out tempdate)))
                            {
                                objFuelFileCSV.EffectiveDate = i;
                            }
                        }
                        if (objFuelFileCSV.High < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                        {
                            int highTemp;
                            if ((int.TryParse(columns[i], out highTemp)) && (columns[i].Length <= 8 && columns[i].Length >= 1))
                            {
                                objFuelFileCSV.High = i;

                                for (int j = 0; j < columns.Count(); j++)
                                {
                                    if (objFuelFileCSV.Low < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                                    {
                                        if (i != j && (int.TryParse(columns[j], out highTemp)) && (columns[j].Length <= 8 && columns[j].Length >= 1))
                                        {
                                            objFuelFileCSV.Low = j;
                                            int high, low;
                                            if (int.TryParse(columns[i], out high))
                                            {
                                                if (int.TryParse(columns[j], out low))
                                                {
                                                    if (high < low)
                                                    {
                                                        objFuelFileCSV.Low = i;
                                                        objFuelFileCSV.High = j;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (objFuelFileCSV.Low < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                        {
                            int highTemp;
                            if ((int.TryParse(columns[i], out highTemp)) && (columns[i].Length <= 8 && columns[i].Length >= 1))
                            {
                                objFuelFileCSV.Low = i;

                                for (int j = 0; j < columns.Count(); j++)
                                {
                                    if (objFuelFileCSV.Low < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                                    {
                                        if (i != j && (int.TryParse(columns[j], out highTemp)) && (columns[j].Length <= 8 && columns[j].Length >= 1))
                                        {
                                            objFuelFileCSV.High = j;
                                            int high, low;
                                            if (int.TryParse(columns[i], out high))
                                            {
                                                if (int.TryParse(columns[j], out low))
                                                {
                                                    if (high < low)
                                                    {
                                                        objFuelFileCSV.Low = i;
                                                        objFuelFileCSV.High = j;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (objFuelFileCSV.ICAO < 0 && objFuelFileCSV.IATA < 0)
                        {
                            if ((columns[i].GetType() == typeof(string)) && (columns[i].Length == 4))
                            {
                                objFuelFileCSV.ICAO = i;
                            }
                        }
                        if (objFuelFileCSV.IATA < 0 && objFuelFileCSV.ICAO < 0)
                        {
                            if ((columns[i].GetType() == typeof(string)) && (columns[i].Length <= 4 && columns[i].Length >= 3))
                            {
                                objFuelFileCSV.IATA = i;
                            }
                        }
                        if (objFuelFileCSV.Vendor < 0 && !objFuelFileCSV.isAnyFieldHaveSameValue(i))
                        {
                            if ((columns[i].GetType() == typeof(string)) && (columns[i].Length >= 10))
                            {
                                objFuelFileCSV.Vendor = i;
                            }
                        }
                    }
                }
            }
        }

        public void FilterByBasicChecking(FuelFileData objFuelFileData,string[] titleColumns)
        {
            for (int clmi = 0; clmi < titleColumns.Length; clmi++)
            {
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_IATA || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_IATA))
                {
                    objFuelFileData.IATA = clmi;                    
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_ICAO || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_ICAO))
                {
                    objFuelFileData.ICAO = clmi;                   
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_SUPPLIER || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_SUPPLIER))
                {
                    objFuelFileData.Vendor = clmi;                   
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_FBONAME)
                {
                    objFuelFileData.FBO = clmi;                   
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_DATE || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_DATE))
                {
                    objFuelFileData.EffectiveDate = clmi;                  
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_LOW.Split(',')[0] || titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_LOW.Split(',')[1] || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_LOW.Split(',')[0]) || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_LOW.Split(',')[1]))
                {
                    objFuelFileData.Low = clmi;
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_HIGH.Split(',')[0] || titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_HIGH.Split(',')[1] || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_HIGH.Split(',')[0]) || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_HIGH.Split(',')[1]))
                {
                    objFuelFileData.High = clmi;                    
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_PRICE || titleColumns[clmi].ToString().ToUpper().StartsWith(FlightPak.Common.Constants.FuelCsvFile.text_PRICE))
                {
                    objFuelFileData.Price = clmi;
                }
                if (titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_TEXT.Split(',')[0] || titleColumns[clmi].ToString().ToUpper() == FlightPak.Common.Constants.FuelCsvFile.text_TEXT.Split(',')[1])
                {
                    objFuelFileData.Text = clmi;                   
                }
            }

        }

        public ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> GetListByFilePath(FuelFileData oFuelFileData,string identityName,long customerId)
        {
            ReturnValue<List<Data.MasterCatalog.FlightpakFuel>> ret = new ReturnValue<List<Data.MasterCatalog.FlightpakFuel>>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFuelFileData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    List<Data.MasterCatalog.FlightpakFuel> list = new List<Data.MasterCatalog.FlightpakFuel>();

                    char SplitDelimiters = ',';
                    string[] ColumnSplitter = new string[] { "\",\"" };
                    string line;
                    string[] columns = null;
                    int titleColumnsCount = 0;
                    int cnt = 0;
                    List<string> readFileRow = new List<string>();                    
                    List<int> blankColumns = new List<int>();
                    Dictionary<string, long> airportListAndID = new Dictionary<string, long>();
                    AirportManager apmanager = new AirportManager();
                    var webClient = new WebClient();
                    var allAirportList =new List<spGetAllAirportIDsWith_ICAO_IATA_Result>();
                    allAirportList = apmanager.GetAirportList_ICAO_IATA().EntityList;
                    var NotSavedRecordsList = new List<FlightpakFuel>();

                    // Use regx use for split when CSV file column value contain comma character
                    var regx = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

                    using (StringReader sr = new StringReader(oFuelFileData.UploadFileData))
                    {
                        while ((line = sr.ReadLine()) != null)
                        {
                            columns = line.Contains(ColumnSplitter[0]) ? line.Split(ColumnSplitter, StringSplitOptions.None) : regx.Split(line);

                            for (int Index = 0; Index < columns.Count(); Index++)
                            {
                                if (columns[Index].Contains("\""))
                                {
                                    columns[Index] = columns[Index].Replace("\"", "");
                                }
                            }
                            if (cnt == 0)
                            {
                                titleColumnsCount = columns.Length;
                            }

                            // Counting first row headers. And Making sure that We fetch that columns data for next all rows,Not more.
                            if (cnt == 0)
                            {
                                for (int i = 0; i < columns.Length; i++)
                                {
                                    if (columns[i].Trim().Length > 0)
                                    {
                                        readFileRow.Add(columns[i]);
                                    }
                                    else
                                    {
                                        blankColumns.Add(i); // Listing Blank Header columns.
                                    }
                                }
                                titleColumnsCount = readFileRow.Count;
                            }
                            else if (titleColumnsCount <= columns.Length)   // if next rows are have same column number as Column Title (1st row)
                            {
                                for (int i = 0; i < columns.Length; i++)
                                {
                                    if (!blankColumns.Contains(i))  // Scanning for blank Header
                                        readFileRow.Add(columns[i]);
                                }
                            }
                            if (readFileRow.Count > 0 && titleColumnsCount <= columns.Length)
                            {
                                if (cnt != 0)  //except header row.
                                {
                                    // Collection data rows.

                                    Data.MasterCatalog.FlightpakFuel objFlightpak = new Data.MasterCatalog.FlightpakFuel();
                                    DateTime tempdate;
                                    if ((DateTime.TryParse(columns[oFuelFileData.EffectiveDate], out tempdate)))
                                    {
                                        objFlightpak.EffectiveDT = tempdate;
                                    }
                                    if (oFuelFileData.FBO > -1)
                                    {
                                        objFlightpak.FBOName = columns[oFuelFileData.FBO].Trim().Length > 25 ? columns[oFuelFileData.FBO].Trim().Substring(0, 25) : columns[oFuelFileData.FBO].Trim();
                                    }
                                    else
                                    {
                                        objFlightpak.FBOName = columns[oFuelFileData.Vendor];
                                    }
                                    objFlightpak.FuelVendorText = oFuelFileData.Text > -1 ? columns[oFuelFileData.Text] : "";
                                    
                                    decimal tempDecimalValue;
                                    if ((decimal.TryParse(columns[oFuelFileData.Price], out tempDecimalValue)))
                                    {
                                        objFlightpak.UnitPrice = tempDecimalValue;
                                    }

                                    int tempIntValue;
                                    if ((int.TryParse(columns[oFuelFileData.High], out tempIntValue)))
                                    {
                                        objFlightpak.GallonTO = tempIntValue;
                                    }
                                    if ((int.TryParse(columns[oFuelFileData.Low], out tempIntValue)))
                                    {
                                        objFlightpak.GallonFrom = tempIntValue;
                                    }
                                    if (oFuelFileData.ICAO>-1)
                                    {
                                        string ICAO = columns[oFuelFileData.ICAO];
                                        objFlightpak.AirportIdentifier = ICAO;
                                        var tempAirport=airportListAndID.Where(a=>a.Key==ICAO).FirstOrDefault();
                                        if (tempAirport.Key != null)
                                        {
                                            objFlightpak.DepartureICAOID = tempAirport.Value;
                                        }
                                        else
                                        {
                                            var airport = allAirportList.Where(f => f.IcaoID == ICAO).FirstOrDefault(); ;//apmanager.GetAirportByAirportICaoID(ICAO);
                                            objFlightpak.DepartureICAOID = airport != null ? airport.AirportID : 0;
                                            airportListAndID.Add(ICAO, Convert.ToInt64(objFlightpak.DepartureICAOID));
                                        }
                                    }else if(oFuelFileData.IATA > -1)
                                    {
                                        string IATA = columns[oFuelFileData.IATA];
                                        objFlightpak.AirportIdentifier = IATA;
                                        var tempAirport = airportListAndID.Where(a => a.Key == IATA).FirstOrDefault();
                                        if (tempAirport.Key != null)
                                        {
                                            objFlightpak.DepartureICAOID = tempAirport.Value;
                                        }
                                        else
                                        {
                                            var airport = allAirportList.Where(f => f.Iata == IATA).FirstOrDefault();
                                            objFlightpak.DepartureICAOID = airport != null ? airport.AirportID : 0;
                                            airportListAndID.Add(IATA,Convert.ToInt64(objFlightpak.DepartureICAOID));
                                        }
                                    }
                                    objFlightpak.CustomerID = customerId;
                                    objFlightpak.LastUpdUID = identityName;
                                    objFlightpak.LastUpdTS = DateTime.Now;
                                    objFlightpak.IsActive = true;
                                    objFlightpak.FuelVendorID = oFuelFileData.VendorID;
                                    objFlightpak.UpdateDTTM = DateTime.Now;                                    
                                    if(IsValidRecordToSave(objFlightpak))
                                        list.Add(objFlightpak);
                                    else
                                    {
                                        NotSavedRecordsList.Add(objFlightpak);
                                    }
                                }
                                readFileRow = new List<string>();
                                cnt++;
                            }
                        }
                    }
                    
                    if (list.Count > 0)
                    {                        
                        airportListAndID = null;
                        ret.ReturnFlag = true;
                        ret.EntityInfo = list; // Valid Records List
                        ret.EntityList=new List<List<FlightpakFuel>>();// Invalid Records List
                        ret.EntityList.Add(NotSavedRecordsList);
                        ret.ErrorMessage = NotSavedRecordsList.Count.ToString();
                    }
                    else
                    {
                        airportListAndID = null;
                        ret.ReturnFlag = false;
                        ret.EntityList.Add(NotSavedRecordsList);// Invalid Records List
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ret;
            }
        }

        private bool IsValidRecordToSave(FlightpakFuel objflightpakFuel)
        {
            bool isValidRecord = true;

            if (objflightpakFuel.UnitPrice == null)
            {
                objflightpakFuel.InvalidRecordErrorMessage="Fuel Price is blank";
                isValidRecord= false;
            } else if (objflightpakFuel.EffectiveDT == null)
            {
                objflightpakFuel.InvalidRecordErrorMessage="Effective Date is blank";
                isValidRecord = false;
            }else if (objflightpakFuel.EffectiveDT == DateTime.MinValue)
            {
                objflightpakFuel.InvalidRecordErrorMessage="Invalid Effective Date";
                isValidRecord = false;
            }else if (objflightpakFuel.DepartureICAOID == 0 || objflightpakFuel.DepartureICAOID==null)
            {
                objflightpakFuel.InvalidRecordErrorMessage="Invalid ICAO or IATA";
                isValidRecord = false;
            }else if (string.IsNullOrWhiteSpace(objflightpakFuel.FBOName))
            {
                objflightpakFuel.InvalidRecordErrorMessage="FBO or Vendor name is blank";
                isValidRecord = false;
            }else if (objflightpakFuel.GallonFrom==null)
            {
               objflightpakFuel.InvalidRecordErrorMessage="Gallon From is blank";
                isValidRecord = false;
            }else if (objflightpakFuel.GallonTO == null)
            {
                objflightpakFuel.InvalidRecordErrorMessage="Gallon To is blank";
                isValidRecord = false;
            }else if (objflightpakFuel.GallonTO == 0)
            {
                objflightpakFuel.InvalidRecordErrorMessage="Gallon To is equal to Zero";
                isValidRecord = false;
            }else
            {
                isValidRecord = true;
            }
            return isValidRecord;
        }


        public ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> UpdateToHistoricalFlightpakFuel(Data.MasterCatalog.HistoricalFuelPrice flightpakFuel)
        {
            ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> objFlightpakFuel = new ReturnValue<Data.MasterCatalog.HistoricalFuelPrice>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightpakFuel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFlightpakFuel.ReturnFlag = base.Validate(flightpakFuel, ref objFlightpakFuel.ErrorMessage);
                    //For Data Anotation
                    if (objFlightpakFuel.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.HistoricalFuelPrice.Attach(flightpakFuel);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.HistoricalFuelPrice>(flightpakFuel, objContainer);
                        objContainer.SaveChanges();
                        objFlightpakFuel.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFlightpakFuel;
        }

        public ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> DeleteToHistoricalFlightpakFuel(Data.MasterCatalog.HistoricalFuelPrice flightpakFuel)
        {
            ReturnValue<Data.MasterCatalog.HistoricalFuelPrice> objFlightpakFuel = new ReturnValue<Data.MasterCatalog.HistoricalFuelPrice>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightpakFuel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.HistoricalFuelPrice).Name, flightpakFuel);
                    objContainer.HistoricalFuelPrice.DeleteObject(flightpakFuel);
                    objContainer.SaveChanges();
                    objFlightpakFuel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFlightpakFuel;
        }
    }
}
