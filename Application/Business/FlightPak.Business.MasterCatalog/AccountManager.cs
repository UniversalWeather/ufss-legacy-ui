﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class AccountManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Account>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ///  
        public ReturnValue<Data.MasterCatalog.GetAccountWithFilters> GetAccountWithFilters(string AccountNum ,long AccountID ,string FetchHomebaseOnly ,bool FetchInactiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetAccountWithFilters> ret = new ReturnValue<Data.MasterCatalog.GetAccountWithFilters>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.AccountManager objAcct = new FlightPak.Business.MasterCatalog.AccountManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAccountWithFilters(CustomerID, AccountNum, AccountID, FetchHomebaseOnly, FetchInactiveOnly).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion
        public ReturnValue<Data.MasterCatalog.GetAccounts> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAccounts> ret = new ReturnValue<Data.MasterCatalog.GetAccounts>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.AccountManager objAcct = new FlightPak.Business.MasterCatalog.AccountManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAccounts(CustomerID).ToList();
                    //if (ret.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < ret.EntityList.Count; i++)
                    //    {
                    //        ret.EntityList[i].AccountNum = ret.EntityList[i].AccountNum != null ? ret.EntityList[i].AccountNum.Trim() : string.Empty;
                    //    }
                    //}
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.Account> GetList()
        {
            ReturnValue<Data.MasterCatalog.Account> ret = new ReturnValue<Data.MasterCatalog.Account>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.AccountManager objAcct = new FlightPak.Business.MasterCatalog.AccountManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllAccounts(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.Account> Add(Data.MasterCatalog.Account account)
        {
            ReturnValue<Data.MasterCatalog.Account> ret = new ReturnValue<Data.MasterCatalog.Account>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(account))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    account.CustomerID = CustomerID;
                    account.IsDeleted = false;
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(account, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.AddToAccount(account);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.Account> Update(Data.MasterCatalog.Account account)
        {
            ReturnValue<Data.MasterCatalog.Account> ret = new ReturnValue<Data.MasterCatalog.Account>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(account))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                     ret.ReturnFlag = base.Validate(account, ref ret.ErrorMessage);
                     //For Data Anotation
                     if (ret.ReturnFlag)
                     {
                         Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                         cs.Account.Attach(account);
                         Common.EMCommon.SetAllModified<Data.MasterCatalog.Account>(account, cs);
                         cs.SaveChanges();

                         ret.ReturnFlag = true;
                     }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.Account> Delete(Data.MasterCatalog.Account account)
        {
            ReturnValue<Data.MasterCatalog.Account> ret = new ReturnValue<Data.MasterCatalog.Account>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(account))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    account.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.Account).Name, account);
                    cs.Account.DeleteObject(account);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.Account> GetLock(Data.MasterCatalog.Account account)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}
