﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Crew>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        /// <summary>
        /// To get the values for CrewRoster page from Crew table
        /// </summary>
        /// <returns></returns>
        /// 
        #region For Performance Optimization

        public ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters> GetAllCrewWithFilters(long ClientID, bool FetchActiveOnly, bool IsRotaryWing, bool IsFixedWing, string ICAOID, long CrewGroupId, long CrewId, string CrewCD)
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters> Crew = new ReturnValue<Data.MasterCatalog.GetAllCrewWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetAllCrewWithFilters(CustomerID,ClientID,FetchActiveOnly,IsRotaryWing,IsFixedWing,ICAOID,CrewGroupId,  CrewId,CrewCD).ToList();
                    foreach (var item in Crew.EntityList)
                    {
                        if (item.SSN != null)
                        {
                            if (item.SSN.Trim() != "")
                            {
                                item.SSN = Crypting.Decrypt(item.SSN);
                            }
                            else
                            {
                                item.SSN = string.Empty;
                            }
                        }
                        else
                        {
                            item.SSN = string.Empty;
                        }
                    }
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }
        #endregion

        public ReturnValue<Data.MasterCatalog.GetAllCrew> GetCrewList()
        {
            ReturnValue<Data.MasterCatalog.GetAllCrew> Crew = new ReturnValue<Data.MasterCatalog.GetAllCrew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetAllCrew(CustomerID, ClientId).ToList();
                    //if (Crew.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < Crew.EntityList.Count; i++)
                    //    {
                    //        Crew.EntityList[i].CrewCD = Crew.EntityList[i].CrewCD != null ? Crew.EntityList[i].CrewCD.Trim() : string.Empty;
                    //    }
                    //}
                    foreach (var item in Crew.EntityList)
                    {
                        if (item.SSN != null)
                        {
                            if (item.SSN.Trim() != "")
                            {
                                item.SSN = Crypting.Decrypt(item.SSN);
                            }
                            else
                            {
                                item.SSN = string.Empty;
                            }
                        }
                        else
                        {
                            item.SSN = string.Empty;
                        }
                    }

                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }

        private Data.MasterCatalog.Crew EncryptCrew(Data.MasterCatalog.Crew Crew)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Crew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Crew.SSN = Crypting.Encrypt(Crew.SSN);
                    foreach (Data.MasterCatalog.CrewDefinition oCrewAdditionalInfo in Crew.CrewDefinition)
                    {
                        oCrewAdditionalInfo.CustomerID = Crew.CustomerID;
                        if (!string.IsNullOrEmpty(oCrewAdditionalInfo.InformationValue))
                        {
                            oCrewAdditionalInfo.InformationValue = Crypting.Encrypt(oCrewAdditionalInfo.InformationValue);
                        }
                    }
                    foreach (Data.MasterCatalog.CrewPassengerVisa oCrewPassengerVisa in Crew.CrewPassengerVisa)
                    {
                        oCrewPassengerVisa.CustomerID = Crew.CustomerID;
                        if (!string.IsNullOrEmpty(oCrewPassengerVisa.VisaNum))
                        {
                            oCrewPassengerVisa.VisaNum = Crypting.Encrypt(oCrewPassengerVisa.VisaNum);
                        }
                    }
                    foreach (Data.MasterCatalog.CrewPassengerPassport oCrewPassengerPassport in Crew.CrewPassengerPassport)
                    {
                        oCrewPassengerPassport.CustomerID = Crew.CustomerID;
                        if (!string.IsNullOrEmpty(oCrewPassengerPassport.PassportNum))
                        {
                            oCrewPassengerPassport.PassportNum = Crypting.Encrypt(oCrewPassengerPassport.PassportNum);
                        }
                    }


                    foreach (Data.MasterCatalog.CrewCheckListDetail oCrewCheckListDetail in Crew.CrewCheckListDetail)
                    {
                        oCrewCheckListDetail.CustomerID = Crew.CustomerID;
                    }
                    // Crew Aircraft Assigned
                    foreach (Data.MasterCatalog.CrewAircraftAssigned oCrewAircraftAssigned in Crew.CrewAircraftAssigned)
                    {
                        oCrewAircraftAssigned.CustomerID = Crew.CustomerID;
                    }
                    ////Commented Until DB is ready
                    //// Crew Rating
                    foreach (Data.MasterCatalog.CrewRating oCrewRating in Crew.CrewRating)
                    {
                        oCrewRating.CustomerID = Crew.CustomerID;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return Crew;
            }
        }
        public ReturnValue<Data.MasterCatalog.GetCrewByCrewID> GetCrewByCrewID(long CrewID)
        {
            ReturnValue<Data.MasterCatalog.GetCrewByCrewID> Crew = new ReturnValue<Data.MasterCatalog.GetCrewByCrewID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetCrewByCrewID(CrewID).ToList();

                    foreach (var item in Crew.EntityList)
                    {
                        if (!string.IsNullOrEmpty(item.SSN))
                        {
                            item.SSN = Crypting.Decrypt(item.SSN);
                        }
                    }
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }

        public ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew> GetCrewGroupBasedCrewInfo(Int64 CrewGroupID)
        {
            ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew> Crew = new ReturnValue<Data.MasterCatalog.GetCrewGroupbasedCrew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetCrewGroupbasedCrew(CustomerID, ClientId, CrewGroupID).ToList();
                    foreach (var item in Crew.EntityList)
                    {
                        if (item.SSN != null)
                        {
                            if (item.SSN.Trim() != "")
                            {
                                item.SSN = Crypting.Decrypt(item.SSN);
                            }
                            else
                            {
                                item.SSN = string.Empty;
                            }
                        }
                        else
                        {
                            item.SSN = string.Empty;
                        }
                    }
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }


        public ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog> GetOtherCrewDutylog(long CrewID,long AircraftID)
        {
            ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog> Crew = new ReturnValue<Data.MasterCatalog.GetOtherCrewDutyLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetOtherCrewDutyLog(CustomerID,CrewID, AircraftID).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }

        public ReturnValue<Data.MasterCatalog.GetCrewforDisplay> GetCrewforDisplay(long CrewID)
        {
            ReturnValue<Data.MasterCatalog.GetCrewforDisplay> Crew = new ReturnValue<Data.MasterCatalog.GetCrewforDisplay>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetCrewforDisplay(CustomerID,string.Empty, CrewID).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }


        public ReturnValue<Data.MasterCatalog.Crew> UpdateCrewDisplay(long CrewID,bool IsDisplay)
        {
            ReturnValue<Data.MasterCatalog.Crew> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.Crew>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID, IsDisplay))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.UpdateCrewInfoDisplay(CrewID, CustomerID, IsDisplay);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }



        public ReturnValue<Data.MasterCatalog.GetAllCrewForGrid> GetAllCrewForGrid()
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewForGrid> Crew = new ReturnValue<Data.MasterCatalog.GetAllCrewForGrid>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    Crew.EntityList = Container.GetAllCrewForGrid(CustomerID, ClientId).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }

        /// <summary>
        /// To insert values from CrewRoster page to Crew table
        /// </summary>
        /// <param name="crew"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> Add(Data.MasterCatalog.Crew crew)
        {
            ReturnValue<Data.MasterCatalog.Crew> ReturnValue = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        //For Data Anotation
                        ReturnValue.ReturnFlag = base.Validate(crew, ref ReturnValue.ErrorMessage);
                        //For Data Anotation
                        if (ReturnValue.ReturnFlag)
                        {
                            crew = EncryptCrew(crew);
                            objContainer.Crew.AddObject(crew);

                            objContainer.SaveChanges();
                            ReturnValue.ReturnFlag = true;
                        }
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To update values from CrewRoster page to Crew table
        /// </summary>
        /// <param name="crew"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> Update(Data.MasterCatalog.Crew crew)
        {
            ReturnValue<Data.MasterCatalog.Crew> ReturnValue = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        //For Data Anotation
                        ReturnValue.ReturnFlag = base.Validate(crew, ref ReturnValue.ErrorMessage);
                        //For Data Anotation
                        if (ReturnValue.ReturnFlag)
                        {
                            //if (crew.SSN != null)
                            //{
                            //    if (crew.SSN.Trim() != "")
                            //    {
                            //        crew.SSN = Crypting.Encrypt(crew.SSN);
                            //    }
                            //    else
                            //    {
                            //        crew.SSN = string.Empty;
                            //    }
                            //}
                            crew = EncryptCrew(crew);
                            objContainer.Crew.Attach(crew);
                            objContainer.ObjectStateManager.ChangeObjectState(crew, System.Data.EntityState.Modified);
                            // Crew Definition
                            foreach (Data.MasterCatalog.CrewDefinition oCrewDefinition in crew.CrewDefinition)
                            {
                                oCrewDefinition.CustomerID = crew.CustomerID;
                                //if (oCrewDefinition.InformationValue != null)
                                //{
                                //    if (oCrewDefinition.InformationValue.Trim() != "")
                                //    {
                                //        oCrewDefinition.InformationValue = Crypting.Encrypt(oCrewDefinition.InformationValue);
                                //    }
                                //    else
                                //    {
                                //        oCrewDefinition.InformationValue = string.Empty;
                                //    }
                                //}
                                if (oCrewDefinition.CrewInfoXRefID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewDefinition, System.Data.EntityState.Added);
                                    objContainer.CrewDefinition.AddObject(oCrewDefinition);
                                }
                                else
                                {
                                    // objContainer.CrewDefinition.Attach(oCrewDefinition);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewDefinition>(oCrewDefinition, objContainer);
                                }
                            }
                            // Crew Passenger Visa
                            foreach (Data.MasterCatalog.CrewPassengerVisa oCrewPassengerVisa in crew.CrewPassengerVisa)
                            {
                                oCrewPassengerVisa.CustomerID = crew.CustomerID;
                                //if (oCrewPassengerVisa.VisaNum != null)
                                //{
                                //    if (oCrewPassengerVisa.VisaNum.Trim() != "")
                                //    {
                                //        oCrewPassengerVisa.VisaNum = Crypting.Encrypt(oCrewPassengerVisa.VisaNum);
                                //    }
                                //    else
                                //    {
                                //        oCrewPassengerVisa.VisaNum = null;
                                //    }

                                //}
                                //else
                                //{
                                //    oCrewPassengerVisa.VisaNum = null;
                                //}


                                if (oCrewPassengerVisa.VisaID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewPassengerVisa, System.Data.EntityState.Added);
                                    objContainer.CrewPassengerVisa.AddObject(oCrewPassengerVisa);
                                }
                                else
                                {
                                    //objContainer.CrewPassengerVisa.Attach(oCrewPassengerVisa);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewPassengerVisa>(oCrewPassengerVisa, objContainer);
                                }
                            }
                            // Crew Passport
                            foreach (Data.MasterCatalog.CrewPassengerPassport oCrewPassengerPassport in crew.CrewPassengerPassport)
                            {
                                oCrewPassengerPassport.CustomerID = crew.CustomerID;
                                //if (oCrewPassengerPassport.PassportNum != null)
                                //{
                                //    if (oCrewPassengerPassport.PassportNum.Trim() != "")
                                //    {
                                //        oCrewPassengerPassport.PassportNum = Crypting.Encrypt(oCrewPassengerPassport.PassportNum);
                                //    }
                                //    else
                                //    {
                                //        oCrewPassengerPassport.PassportNum = null;
                                //    }

                                //}
                                //else
                                //{
                                //    oCrewPassengerPassport.PassportNum = null;
                                //}


                                if (oCrewPassengerPassport.PassportID < 1000)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewPassengerPassport, System.Data.EntityState.Added);
                                    objContainer.CrewPassengerPassport.AddObject(oCrewPassengerPassport);
                                }
                                else
                                {
                                    //objContainer.CrewPassengerPassport.Attach(oCrewPassengerPassport);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewPassengerPassport>(oCrewPassengerPassport, objContainer);
                                }
                            }
                            // Crew ChecklistDetail
                            foreach (Data.MasterCatalog.CrewCheckListDetail oCrewCheckListDetail in crew.CrewCheckListDetail)
                            {
                                oCrewCheckListDetail.CustomerID = crew.CustomerID;
                                if (oCrewCheckListDetail.CheckListID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewCheckListDetail, System.Data.EntityState.Added);
                                    objContainer.CrewCheckListDetail.AddObject(oCrewCheckListDetail);
                                }
                                else
                                {
                                    // objContainer.CrewCheckListDetail.Attach(oCrewCheckListDetail);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewCheckListDetail>(oCrewCheckListDetail, objContainer);
                                }
                            }
                            // Crew Aircraft Assigned
                            foreach (Data.MasterCatalog.CrewAircraftAssigned oCrewAircraftAssigned in crew.CrewAircraftAssigned)
                            {
                                oCrewAircraftAssigned.CustomerID = crew.CustomerID;
                                if (oCrewAircraftAssigned.CrewAircraftAssignedID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewAircraftAssigned, System.Data.EntityState.Added);
                                    objContainer.CrewAircraftAssigned.AddObject(oCrewAircraftAssigned);
                                }
                                else
                                {
                                    //objContainer.CrewAircraftAssigned.Attach(oCrewAircraftAssigned);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewAircraftAssigned>(oCrewAircraftAssigned, objContainer);
                                }
                            }
                            ////Commented Until DB is ready
                            //// Crew Rating
                            foreach (Data.MasterCatalog.CrewRating oCrewRating in crew.CrewRating)
                            {
                                oCrewRating.CustomerID = crew.CustomerID;
                                if (oCrewRating.CrewRatingID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewRating, System.Data.EntityState.Added);
                                    objContainer.CrewRating.AddObject(oCrewRating);
                                }
                                else
                                {
                                    // objContainer.CrewRating.Attach(oCrewRating);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewRating>(oCrewRating, objContainer);
                                }
                            }
                            objContainer.SaveChanges();
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To update the selected record as deleted, from CrewRoster page in Crew table
        /// </summary>
        /// <param name="crew"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> Delete(Data.MasterCatalog.Crew crew)
        {
            ReturnValue<Data.MasterCatalog.Crew> Crew = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.Crew).Name, crew);
                    Container.Crew.DeleteObject(crew);
                    Container.SaveChanges();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> GetLock(Data.MasterCatalog.Crew t)
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Crew> GetList()
        {
            ReturnValue<Data.MasterCatalog.Crew> Crew = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Crew.EntityList = Container.GetCrewList(CustomerID, ClientId).ToList();
                    foreach (var item in Crew.EntityList)
                    {
                        item.SSN = Crypting.Decrypt(item.SSN);
                    }
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }
        /// <summary>
        /// To fetch the values from the Crew table based on parameters
        /// </summary>
        /// <param name="crew"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> GetListInfo(Data.MasterCatalog.Crew crew)
        {
            ReturnValue<Data.MasterCatalog.Crew> Crew = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crew))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //Crew.EntityList = objContainer.GetCrewInfo(CustomerID, crew.CrewCD).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }
        public ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours> GetList(Int64 CrewID)
        {
            ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours> Crew = new ReturnValue<Data.MasterCatalog.GetAllCurrentFlightHours>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Crew.EntityList = Container.GetAllCurrentFlightHours(CrewID).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return Crew;
        }
        public ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency> GetCurrency(Int64 CrewID)
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency> Crew = new ReturnValue<Data.MasterCatalog.GetAllCrewRosterCurrency>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Crew.EntityList = Container.GetAllCrewRosterCurrency(CrewID,UserName).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }

        public ReturnValue<Data.MasterCatalog.GetCrewID> GetCrewID(string CrewCD)
        {
            ReturnValue<Data.MasterCatalog.GetCrewID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCrewID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetCrewID(CustomerID, CrewCD).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        #region "Get Crew for Client"
        public ReturnValue<Data.MasterCatalog.GetCrewForClient> GetCrewForClient()
        {
            ReturnValue<Data.MasterCatalog.GetCrewForClient> objCrew = new ReturnValue<Data.MasterCatalog.GetCrewForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    objCrew.EntityList = Container.GetCrewForClient(CustomerID, ClientId).ToList();
                    objCrew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objCrew;
        }
        #endregion
    }
}
