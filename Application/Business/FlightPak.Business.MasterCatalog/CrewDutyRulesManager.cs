﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewDutyRulesManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewDutyRules>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.CrewDutyRules> GetCrewDutyRulesWithFilters(long CrewDutyRuleId, string CrewDutyRuleCD, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = new ReturnValue<Data.MasterCatalog.CrewDutyRules>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDuRulesType = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetCrewDutyRulesWithFilters(CustomerID, CrewDutyRuleId, CrewDutyRuleCD, FetchActiveOnly).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        #endregion
        /// <summary>
        /// To get the values into CrewDutyRulesCatalog page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyRules> GetList()
        {
            ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = new ReturnValue<Data.MasterCatalog.CrewDutyRules>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CrewDutyRulesManager CrewDuRulesType = new FlightPak.Business.MasterCatalog.CrewDutyRulesManager();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllCrewDutyRules(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ReturnValue;
        }

        /// <summary>
        /// To insert values from CrewDutyRulesCatalog Grid
        /// </summary>
        /// <param name="CrewDutyRule"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyRules> Add(Data.MasterCatalog.CrewDutyRules CrewDutyRule)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = new ReturnValue<Data.MasterCatalog.CrewDutyRules>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRule))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CrewDutyRule.CustomerID = CustomerID;
                    CrewDutyRule.IsDeleted = false;                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(CrewDutyRule, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.AddToCrewDutyRules(CrewDutyRule);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        /// <summary>
        /// To update the selected from CrewDutyRulesCatalog Grid
        /// </summary>
        /// <param name="CrewDutyRule"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyRules> Update(Data.MasterCatalog.CrewDutyRules CrewDutyRule)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = new ReturnValue<Data.MasterCatalog.CrewDutyRules>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRule ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(CrewDutyRule, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.CrewDutyRules.Attach(CrewDutyRule);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewDutyRules>(CrewDutyRule, Container);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return ReturnValue;
        }

        /// <summary>
        /// To update the selected record as deleted, from CrewDutyRulesCatalog Grid
        /// </summary>
        /// <param name="CrewDutyRule"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyRules> Delete(Data.MasterCatalog.CrewDutyRules CrewDutyRule)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyRules> ReturnValue = new ReturnValue<Data.MasterCatalog.CrewDutyRules>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewDutyRule ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.CrewDutyRules).Name, CrewDutyRule);
                    Container.CrewDutyRules.DeleteObject(CrewDutyRule);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return ReturnValue;
        }

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="CrewDutyRule"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyRules> GetLock(Data.MasterCatalog.CrewDutyRules CrewDutyRule)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
