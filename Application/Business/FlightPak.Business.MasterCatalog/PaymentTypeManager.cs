﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class PaymentTypeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.PaymentType>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization

        public ReturnValue<Data.MasterCatalog.PaymentType> GetAllPaymentTypeWithFilters(long PaymentTypeID, string PaymenttypeCD, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.PaymentType> objPaymentType = new ReturnValue<Data.MasterCatalog.PaymentType>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPaymentType.EntityList = objContainer.GetAllPaymentTypeWithFilters(CustomerID,PaymentTypeID, PaymenttypeCD, FetchActiveOnly).ToList();
                    //if (objPaymentType.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objPaymentType.EntityList.Count; i++)
                    //    {
                    //        objPaymentType.EntityList[i].PaymentTypeCD = objPaymentType.EntityList[i].PaymentTypeCD != null ? objPaymentType.EntityList[i].PaymentTypeCD.Trim() : string.Empty;
                    //    }
                    //}
                    objPaymentType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objPaymentType;
        }

        #endregion



        public ReturnValue<Data.MasterCatalog.PaymentType> GetList()
        {
            ReturnValue<Data.MasterCatalog.PaymentType> objPaymentType = new ReturnValue<Data.MasterCatalog.PaymentType>();

            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPaymentType.EntityList = objContainer.GetAllPaymentType(CustomerID).ToList();
                    //if (objPaymentType.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objPaymentType.EntityList.Count; i++)
                    //    {
                    //        objPaymentType.EntityList[i].PaymentTypeCD = objPaymentType.EntityList[i].PaymentTypeCD != null ? objPaymentType.EntityList[i].PaymentTypeCD.Trim() : string.Empty;
                    //    }
                    //}
                    objPaymentType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return objPaymentType;
        }


        public ReturnValue<Data.MasterCatalog.PaymentType> Add(Data.MasterCatalog.PaymentType PaymentTypetype)
        {
            ReturnValue<Data.MasterCatalog.PaymentType> objPaymentTypeType = new ReturnValue<Data.MasterCatalog.PaymentType>();

            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaymentTypetype ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objPaymentTypeType.ReturnFlag = base.Validate(PaymentTypetype, ref objPaymentTypeType.ErrorMessage);
                    //For Data Anotation
                    if (objPaymentTypeType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToPaymentType(PaymentTypetype);
                        objContainer.SaveChanges();
                        objPaymentTypeType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return objPaymentTypeType;
        }

        public ReturnValue<Data.MasterCatalog.PaymentType> Update(Data.MasterCatalog.PaymentType PaymentTypetype)
        {
            ReturnValue<Data.MasterCatalog.PaymentType> objPaymentTypeType = new ReturnValue<Data.MasterCatalog.PaymentType>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaymentTypetype ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objPaymentTypeType.ReturnFlag = base.Validate(PaymentTypetype, ref objPaymentTypeType.ErrorMessage);
                    //For Data Anotation
                    if (objPaymentTypeType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.PaymentType.Attach(PaymentTypetype);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.PaymentType>(PaymentTypetype, objContainer);
                        objContainer.SaveChanges();
                        objPaymentTypeType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return objPaymentTypeType;
        }

        public ReturnValue<Data.MasterCatalog.PaymentType> Delete(Data.MasterCatalog.PaymentType PaymentTypetype)
        {
            ReturnValue<Data.MasterCatalog.PaymentType> objPaymentTypeType = new ReturnValue<Data.MasterCatalog.PaymentType>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PaymentTypetype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.PaymentType).Name, PaymentTypetype);
                    objContainer.PaymentType.DeleteObject(PaymentTypetype);
                    objContainer.SaveChanges();
                    objPaymentTypeType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaymentTypeType;
        }


        public ReturnValue<Data.MasterCatalog.PaymentType> GetListInfo()
        {

            ReturnValue<Data.MasterCatalog.PaymentType> objPaymentType = new ReturnValue<Data.MasterCatalog.PaymentType>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs( ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objPaymentType.EntityList = objContainer.PaymentType.ToList();
                    objPaymentType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return objPaymentType;
        }

        public ReturnValue<Data.MasterCatalog.PaymentType> GetLock(Data.MasterCatalog.PaymentType PaymentTypetype)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}

