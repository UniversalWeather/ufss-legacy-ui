﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetNewCharterRate : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FleetNewCharterRate>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate> GetFleetNewCharterRateListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate> objFleetNewCharterRate = new ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetNewCharterRate.EntityList = objContainer.GetAllFleetNewCharterRate(CustomerID).ToList();
                    objFleetNewCharterRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objFleetNewCharterRate;
        }

        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> Add(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetNewCharterRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToFleetNewCharterRate(FleetNewCharterRate);
                    objContainer.SaveChanges();
                    objFleetNewCharterRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetNewCharterRate;
        }

        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> Update(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetNewCharterRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.FleetNewCharterRate.Attach(FleetNewCharterRate);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetNewCharterRate>(FleetNewCharterRate, objContainer);
                    objContainer.SaveChanges();
                    objFleetNewCharterRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetNewCharterRate;
        }

        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> Delete(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetNewCharterRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetNewCharterRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FleetNewCharterRate).Name, FleetNewCharterRate);
                    objContainer.FleetNewCharterRate.DeleteObject(FleetNewCharterRate);
                    objContainer.SaveChanges();
                    objFleetNewCharterRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetNewCharterRate;
        }

        public ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID> GetAllFleetNewCharterByFleetNewCharterID(long FleetNewcharterID)
        {
            ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID> FleetNewCharter = new ReturnValue<Data.MasterCatalog.GetAllFleetNewCharterByFleetNewCharterID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Removed Hardcoded value CustomerID and used from BaseManager Class
                    // Fetch the values from the FareLevel table based passed CustomerID
                    FleetNewCharter.EntityList = Container.GetAllFleetNewCharterByFleetNewCharterID(FleetNewcharterID).ToList();
                    FleetNewCharter.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return FleetNewCharter;
        }

        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> GetLock(Data.MasterCatalog.FleetNewCharterRate FleetNewCharterRate)
        {
            throw new NotImplementedException();
        }


        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> UpdateCharterTax(Int64 FleetID, Int64 AircraftID, bool DailyUsageTax, bool LandingFeeTax, string LastupdateID, DateTime LastUpTS, Decimal MinimumDailyRequirement, Decimal StandardCrewIntl, Decimal StandardCrewDOM, Int64 CQCustomerID,Decimal MarginalPercentage)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetChargeRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetID,AircraftID,DailyUsageTax,LandingFeeTax,LastupdateID,LastUpTS,MinimumDailyRequirement,StandardCrewIntl,StandardCrewDOM,  CQCustomerID, MarginalPercentage))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.UpdateCharterRateTax(FleetID, AircraftID, DailyUsageTax, LandingFeeTax, LastupdateID, LastUpTS, MinimumDailyRequirement, StandardCrewIntl, StandardCrewDOM, CQCustomerID, MarginalPercentage);
                    objContainer.SaveChanges();
                    objFleetChargeRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objFleetChargeRate;
        }


        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAll(Int64 NewAircraftID, Int64 OldAircraftID)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetChargeRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewAircraftID, OldAircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.CopyAllNewFleetCharterRate(CustomerID, OldAircraftID, NewAircraftID);
                    objContainer.SaveChanges();
                    objFleetChargeRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objFleetChargeRate;
        }

        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAllByFleet(Int64 NewAircraftID, Int64 OldAircraftID)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetChargeRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewAircraftID, OldAircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.CopyAllNewFleetCharterRateByFleet(CustomerID, NewAircraftID, OldAircraftID);
                    objContainer.SaveChanges();
                    objFleetChargeRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objFleetChargeRate;
        }

        public ReturnValue<Data.MasterCatalog.FleetNewCharterRate> CopyAllByCQ(Int64 NewAircraftID, Int64 OldAircraftID)
        {
            ReturnValue<Data.MasterCatalog.FleetNewCharterRate> objFleetChargeRate = new ReturnValue<Data.MasterCatalog.FleetNewCharterRate>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(NewAircraftID, OldAircraftID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.CopyAllNewFleetCharterRateByCQ(CustomerID,NewAircraftID, OldAircraftID);
                    objContainer.SaveChanges();
                    objFleetChargeRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objFleetChargeRate;
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
