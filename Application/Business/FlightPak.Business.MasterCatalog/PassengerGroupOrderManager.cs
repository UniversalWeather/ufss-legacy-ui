﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class PassengerGroupOrderManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.GetPaxAvailableList> GetAvailableList(Int64 paxCode)
        {
            ReturnValue<Data.MasterCatalog.GetPaxAvailableList> PaxMaster = new ReturnValue<Data.MasterCatalog.GetPaxAvailableList>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    PaxMaster.EntityList = objContainer.GetPaxAvailableList(CustomerID, paxCode,ClientId).ToList();
                    PaxMaster.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return PaxMaster;
        }
        public ReturnValue<Data.MasterCatalog.GetPaxSelectedList> GetSelectedList(Int64 paxCode)
        {
            ReturnValue<Data.MasterCatalog.GetPaxSelectedList> PaxMaster = new ReturnValue<Data.MasterCatalog.GetPaxSelectedList>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxCode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    PaxMaster.EntityList = objContainer.GetPaxSelectedList(CustomerID, paxCode).ToList();
                    PaxMaster.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return PaxMaster;
        }
        public ReturnValue<Data.MasterCatalog.PassengerGroupOrder> Add(Data.MasterCatalog.PassengerGroupOrder paxOrder)
        {
            ReturnValue<Data.MasterCatalog.PassengerGroupOrder> PaxGroupOrder = new ReturnValue<Data.MasterCatalog.PassengerGroupOrder>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxOrder))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToPassengerGroupOrder(paxOrder);
                    objContainer.SaveChanges();
                    PaxGroupOrder.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return PaxGroupOrder;
        }
        public ReturnValue<Data.MasterCatalog.PassengerGroupOrder> Delete(Data.MasterCatalog.PassengerGroupOrder paxOrder)
        {
            ReturnValue<Data.MasterCatalog.PassengerGroupOrder> PaxGroupOrder = new ReturnValue<Data.MasterCatalog.PassengerGroupOrder>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxOrder))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.PassengerGroupOrder).Name, paxOrder);
                    objContainer.PassengerGroupOrder.DeleteObject(paxOrder);
                    try 
                    { 
                        objContainer.SaveChanges(); 
                    } 
                    catch (Exception ex) 
                    {
                        //Manually Handled
                    }
                    PaxGroupOrder.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return PaxGroupOrder;
        }
        public ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID> GetPassengerGroupbyGroupID(long PassengerGroupID)
        {
            ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID> PaxMaster = new ReturnValue<Data.MasterCatalog.GetPassengerGroupbyGroupID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    PaxMaster.EntityList = objContainer.GetPassengerGroupbyGroupID(CustomerID, PassengerGroupID).ToList();
                    PaxMaster.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return PaxMaster;
        }
    }
}
