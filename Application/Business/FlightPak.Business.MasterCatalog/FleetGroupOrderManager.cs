﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetGroupOrderManager : BaseManager
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.GetLsavailableFleet> GetLsavailableList(Int64 FleetGroupID)
        {
            ReturnValue<Data.MasterCatalog.GetLsavailableFleet> objFleetMasterType = new ReturnValue<Data.MasterCatalog.GetLsavailableFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetMasterType.EntityList = objContainer.GetLsavailableFleet(CustomerID, FleetGroupID, ClientId).ToList();
                    objFleetMasterType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetMasterType;
        }


        public ReturnValue<Data.MasterCatalog.GetLsselectedFleet> GetLsselectedList(Int64 FleetGroupID)
        {
            ReturnValue<Data.MasterCatalog.GetLsselectedFleet> objFleetMasterType = new ReturnValue<Data.MasterCatalog.GetLsselectedFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetMasterType.EntityList = objContainer.GetLsselectedFleet(CustomerID, FleetGroupID).ToList();
                    objFleetMasterType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetMasterType;
        }


        public ReturnValue<Data.MasterCatalog.FleetGroupOrder> Add(Data.MasterCatalog.FleetGroupOrder fleetgroupordertype)
        {
            ReturnValue<Data.MasterCatalog.FleetGroupOrder> objFleetGroupOrderType = new ReturnValue<Data.MasterCatalog.FleetGroupOrder>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToFleetGroupOrder(fleetgroupordertype);
                    objContainer.SaveChanges();
                    objFleetGroupOrderType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetGroupOrderType;
        }


        public ReturnValue<Data.MasterCatalog.FleetGroupOrder> Delete(Data.MasterCatalog.FleetGroupOrder fleetgroupordertype)
        {
            ReturnValue<Data.MasterCatalog.FleetGroupOrder> objFleetType = new ReturnValue<Data.MasterCatalog.FleetGroupOrder>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FleetGroupOrder).Name, fleetgroupordertype);
                    objContainer.FleetGroupOrder.DeleteObject(fleetgroupordertype);
                    objContainer.SaveChanges();
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetType;
        }

    }

}
