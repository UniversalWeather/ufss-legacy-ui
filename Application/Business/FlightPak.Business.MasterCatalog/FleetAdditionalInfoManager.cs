﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class FleetAdditionalInfoManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FleetProfileDefinition>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();


        public ReturnValue<Data.MasterCatalog.FleetProfileDefinition> GetList()
        {
            ReturnValue<Data.MasterCatalog.FleetProfileDefinition> objFleetType = new ReturnValue<Data.MasterCatalog.FleetProfileDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetType.EntityList = objContainer.GetAllFleetProfileDefinition(CustomerID).ToList();
                    //foreach (var item in objFleetType.EntityList)
                    //{
                    //    item.InformationValue = Crypting.Decrypt(item.InformationValue);
                    //}
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition> GetFleetProfileDefinition()
        {
            ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition> objPaxGroup = new ReturnValue<Data.MasterCatalog.GetFleetProfileDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPaxGroup.EntityList = objContainer.GetFleetProfileDefinition(CustomerID).ToList();
                    //foreach (var item in objPaxGroup.EntityList)
                    //{
                    //    item.InformationValue = Crypting.Decrypt(item.InformationValue);
                    //}
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileDefinition> Add(Data.MasterCatalog.FleetProfileDefinition Passenger)
        {
            ReturnValue<Data.MasterCatalog.FleetProfileDefinition> objPaxGroup = new ReturnValue<Data.MasterCatalog.FleetProfileDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToFleetProfileDefinition(Passenger);
                    objContainer.SaveChanges();
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileDefinition> Update(Data.MasterCatalog.FleetProfileDefinition paxGroup)
        {
            ReturnValue<Data.MasterCatalog.FleetProfileDefinition> objPaxGroup = new ReturnValue<Data.MasterCatalog.FleetProfileDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.FleetProfileDefinition.Attach(paxGroup);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetProfileDefinition>(paxGroup, objContainer);
                    objContainer.SaveChanges();
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileDefinition> Delete(Data.MasterCatalog.FleetProfileDefinition paxGroup)
        {
            ReturnValue<Data.MasterCatalog.FleetProfileDefinition> objPaxGroup = new ReturnValue<Data.MasterCatalog.FleetProfileDefinition>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.AttachTo(typeof(Data.MasterCatalog.FleetProfileDefinition).Name, paxGroup);
                    objContainer.FleetProfileDefinition.DeleteObject(paxGroup);
                    objContainer.SaveChanges();
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileDefinition> GetLock(Data.MasterCatalog.FleetProfileDefinition paxGroup)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
