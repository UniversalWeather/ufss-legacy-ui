﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CQCustomerContactManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CQCustomerContact>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        public ReturnValue<Data.MasterCatalog.CQCustomerContact> GetList()
        {
            ReturnValue<Data.MasterCatalog.CQCustomerContact> ret = new ReturnValue<Data.MasterCatalog.CQCustomerContact>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.CQCustomerContactManager objCustomerContact = new FlightPak.Business.MasterCatalog.CQCustomerContactManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllCQCustomerContact(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #region not implemented

        public ReturnValue<Data.MasterCatalog.CQCustomerContact> GetLock(Data.MasterCatalog.CQCustomerContact t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Implemented for CQCustomerContact"

        public ReturnValue<Data.MasterCatalog.CQCustomerContact> Add(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            ReturnValue<Data.MasterCatalog.CQCustomerContact> ReturnValue = new ReturnValue<Data.MasterCatalog.CQCustomerContact>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        //For Data Anotation
                        ReturnValue.ReturnFlag = base.Validate(CQCustomerContact, ref ReturnValue.ErrorMessage);
                        //For Data Anotation
                        if (ReturnValue.ReturnFlag)
                        {
                            CQCustomerContact = Encrypt(CQCustomerContact);
                            objContainer.CQCustomerContact.AddObject(CQCustomerContact);
                            objContainer.ContextOptions.LazyLoadingEnabled = false;
                            objContainer.ContextOptions.ProxyCreationEnabled = false;
                            objContainer.SaveChanges();
                        }
                    }
                    ReturnValue.EntityInfo = CQCustomerContact;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.CQCustomerContact> Update(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            ReturnValue<Data.MasterCatalog.CQCustomerContact> ReturnValue = new ReturnValue<Data.MasterCatalog.CQCustomerContact>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        //For Data Anotation
                        ReturnValue.ReturnFlag = base.Validate(CQCustomerContact, ref ReturnValue.ErrorMessage);
                        //For Data Anotation
                        if (ReturnValue.ReturnFlag)
                        {
                            CQCustomerContact = Encrypt(CQCustomerContact);
                            objContainer.CQCustomerContact.Attach(CQCustomerContact);
                            objContainer.ObjectStateManager.ChangeObjectState(CQCustomerContact, System.Data.EntityState.Modified);
                            objContainer.SaveChanges();
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.CQCustomerContact> Delete(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            ReturnValue<Data.MasterCatalog.CQCustomerContact> ReturnValue = new ReturnValue<Data.MasterCatalog.CQCustomerContact>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.CQCustomerContact.Attach(CQCustomerContact);
                        objContainer.CQCustomerContact.DeleteObject(CQCustomerContact);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetCQCustomerContact> GetCQCustomerContact(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomerContact> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCQCustomerContact>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetCQCustomerContact(CustomerID).ToList();
                    //Decrypting credit card info
                    for (int Index = 0; Index < ReturnValue.EntityList.Count; Index++)
                    {
                        ReturnValue.EntityList[0].CreditName1 = (ReturnValue.EntityList[0].CreditName1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditName1) : ReturnValue.EntityList[0].CreditName1;
                        ReturnValue.EntityList[0].CreditNum1 = (ReturnValue.EntityList[0].CreditNum1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditNum1) : ReturnValue.EntityList[0].CreditNum1;
                        ReturnValue.EntityList[0].CardType1 = (ReturnValue.EntityList[0].CardType1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CardType1) : ReturnValue.EntityList[0].CardType1;
                        ReturnValue.EntityList[0].ExpirationDate1 = (ReturnValue.EntityList[0].ExpirationDate1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].ExpirationDate1) : ReturnValue.EntityList[0].ExpirationDate1;
                        ReturnValue.EntityList[0].SecurityCode1 = (ReturnValue.EntityList[0].SecurityCode1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].SecurityCode1) : ReturnValue.EntityList[0].SecurityCode1;
                        ReturnValue.EntityList[0].CreditName2 = (ReturnValue.EntityList[0].CreditName2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditName2) : ReturnValue.EntityList[0].CreditName2;
                        ReturnValue.EntityList[0].CreditNum2 = (ReturnValue.EntityList[0].CreditNum2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditNum2) : ReturnValue.EntityList[0].CreditNum2;
                        ReturnValue.EntityList[0].CardType2 = (ReturnValue.EntityList[0].CardType2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CardType2) : ReturnValue.EntityList[0].CardType2;
                        ReturnValue.EntityList[0].ExpirationDate2 = (ReturnValue.EntityList[0].ExpirationDate2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].ExpirationDate2) : ReturnValue.EntityList[0].ExpirationDate2;
                        ReturnValue.EntityList[0].SecurityCode2 = (ReturnValue.EntityList[0].SecurityCode2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].SecurityCode2) : ReturnValue.EntityList[0].SecurityCode2;
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID> GetCQCustomerContactByCQCustomerContactID(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerContactID>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetCQCustomerContactByCQCustomerContactID(CustomerID, CQCustomerContact.CQCustomerContactID).ToList();
                    //Decrypting credit card info
                    for (int Index = 0; Index < ReturnValue.EntityList.Count; Index++)
                    {
                        ReturnValue.EntityList[0].CreditName1 = (ReturnValue.EntityList[0].CreditName1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditName1) : ReturnValue.EntityList[0].CreditName1;
                        ReturnValue.EntityList[0].CreditNum1 = (ReturnValue.EntityList[0].CreditNum1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditNum1) : ReturnValue.EntityList[0].CreditNum1;
                        ReturnValue.EntityList[0].CardType1 = (ReturnValue.EntityList[0].CardType1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CardType1) : ReturnValue.EntityList[0].CardType1;
                        ReturnValue.EntityList[0].ExpirationDate1 = (ReturnValue.EntityList[0].ExpirationDate1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].ExpirationDate1) : ReturnValue.EntityList[0].ExpirationDate1;
                        ReturnValue.EntityList[0].SecurityCode1 = (ReturnValue.EntityList[0].SecurityCode1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].SecurityCode1) : ReturnValue.EntityList[0].SecurityCode1;
                        ReturnValue.EntityList[0].CreditName2 = (ReturnValue.EntityList[0].CreditName2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditName2) : ReturnValue.EntityList[0].CreditName2;
                        ReturnValue.EntityList[0].CreditNum2 = (ReturnValue.EntityList[0].CreditNum2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CreditNum2) : ReturnValue.EntityList[0].CreditNum2;
                        ReturnValue.EntityList[0].CardType2 = (ReturnValue.EntityList[0].CardType2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].CardType2) : ReturnValue.EntityList[0].CardType2;
                        ReturnValue.EntityList[0].ExpirationDate2 = (ReturnValue.EntityList[0].ExpirationDate2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].ExpirationDate2) : ReturnValue.EntityList[0].ExpirationDate2;
                        ReturnValue.EntityList[0].SecurityCode2 = (ReturnValue.EntityList[0].SecurityCode2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[0].SecurityCode2) : ReturnValue.EntityList[0].SecurityCode2;
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID> GetCQCustomerContactByCQCustomerID(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCQCustomerContactByCQCustomerID>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetCQCustomerContactByCQCustomerID(CustomerID, CQCustomerContact.CQCustomerID).ToList();
                    //Decrypting credit card info
                    for (int Index = 0; Index < ReturnValue.EntityList.Count; Index++)
                    {
                        ReturnValue.EntityList[Index].CreditName1 = (ReturnValue.EntityList[Index].CreditName1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].CreditName1) : ReturnValue.EntityList[Index].CreditName1;
                        ReturnValue.EntityList[Index].CreditNum1 = (ReturnValue.EntityList[Index].CreditNum1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].CreditNum1) : ReturnValue.EntityList[Index].CreditNum1;
                        ReturnValue.EntityList[Index].CardType1 = (ReturnValue.EntityList[Index].CardType1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].CardType1) : ReturnValue.EntityList[Index].CardType1;
                        ReturnValue.EntityList[Index].ExpirationDate1 = (ReturnValue.EntityList[Index].ExpirationDate1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].ExpirationDate1) : ReturnValue.EntityList[Index].ExpirationDate1;
                        ReturnValue.EntityList[Index].SecurityCode1 = (ReturnValue.EntityList[Index].SecurityCode1 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].SecurityCode1) : ReturnValue.EntityList[Index].SecurityCode1;
                        ReturnValue.EntityList[Index].CreditName2 = (ReturnValue.EntityList[Index].CreditName2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].CreditName2) : ReturnValue.EntityList[Index].CreditName2;
                        ReturnValue.EntityList[Index].CreditNum2 = (ReturnValue.EntityList[Index].CreditNum2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].CreditNum2) : ReturnValue.EntityList[Index].CreditNum2;
                        ReturnValue.EntityList[Index].CardType2 = (ReturnValue.EntityList[Index].CardType2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].CardType2) : ReturnValue.EntityList[Index].CardType2;
                        ReturnValue.EntityList[Index].ExpirationDate2 = (ReturnValue.EntityList[Index].ExpirationDate2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].ExpirationDate2) : ReturnValue.EntityList[Index].ExpirationDate2;
                        ReturnValue.EntityList[Index].SecurityCode2 = (ReturnValue.EntityList[Index].SecurityCode2 != null) ? Crypting.Decrypt(ReturnValue.EntityList[Index].SecurityCode2) : ReturnValue.EntityList[Index].SecurityCode2;
                    }
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        private Data.MasterCatalog.CQCustomerContact Encrypt(Data.MasterCatalog.CQCustomerContact CQCustomerContact)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CQCustomerContact))
            {
                //Encrypting credit card info
                CQCustomerContact.CreditName1 = (CQCustomerContact.CreditName1 != null) ? Crypting.Encrypt(CQCustomerContact.CreditName1) : CQCustomerContact.CreditName1;
                CQCustomerContact.CreditNum1 = (CQCustomerContact.CreditNum1 != null) ? Crypting.Encrypt(CQCustomerContact.CreditNum1) : CQCustomerContact.CreditNum1;
                CQCustomerContact.CardType1 = (CQCustomerContact.CardType1 != null) ? Crypting.Encrypt(CQCustomerContact.CardType1) : CQCustomerContact.CardType1;
                CQCustomerContact.ExpirationDate1 = (CQCustomerContact.ExpirationDate1 != null) ? Crypting.Encrypt(CQCustomerContact.ExpirationDate1) : CQCustomerContact.ExpirationDate1;
                CQCustomerContact.SecurityCode1 = (CQCustomerContact.SecurityCode1 != null) ? Crypting.Encrypt(CQCustomerContact.SecurityCode1) : CQCustomerContact.SecurityCode1;
                CQCustomerContact.CreditName2 = (CQCustomerContact.CreditName2 != null) ? Crypting.Encrypt(CQCustomerContact.CreditName2) : CQCustomerContact.CreditName2;
                CQCustomerContact.CreditNum2 = (CQCustomerContact.CreditNum2 != null) ? Crypting.Encrypt(CQCustomerContact.CreditNum2) : CQCustomerContact.CreditNum2;
                CQCustomerContact.CardType2 = (CQCustomerContact.CardType2 != null) ? Crypting.Encrypt(CQCustomerContact.CardType2) : CQCustomerContact.CardType2;
                CQCustomerContact.ExpirationDate2 = (CQCustomerContact.ExpirationDate2 != null) ? Crypting.Encrypt(CQCustomerContact.ExpirationDate2) : CQCustomerContact.ExpirationDate2;
                CQCustomerContact.SecurityCode2 = (CQCustomerContact.SecurityCode2 != null) ? Crypting.Encrypt(CQCustomerContact.SecurityCode2) : CQCustomerContact.SecurityCode2;

                return CQCustomerContact;
            }
        }

        #endregion

    }
}
