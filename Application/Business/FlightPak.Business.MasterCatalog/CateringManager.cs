﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class CateringManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Catering>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters> GetAllCateringByAirportIDWithFilters(long AirportID, long CateringID, string CateringCD, bool FetchChoiceOnly, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters> objFbo = new ReturnValue<Data.MasterCatalog.GetAllCateringByAirportIDWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFbo.EntityList = objContainer.GetAllCateringByAirportIDWithFilters(CustomerID, AirportID, CateringID, CateringCD, FetchChoiceOnly, FetchActiveOnly).ToList();
                    objFbo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFbo;
        }
        #endregion

        /// <summary>
        /// To get the values for CateringCatalog page from Catering table
        /// </summary>
        /// <returns></returns>      
        public ReturnValue<Data.MasterCatalog.GetAllCatering> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllCatering> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllCatering>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the Catering table based on CustomerID
                    ReturnValue.EntityList = Container.GetAllCatering(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        /// To insert values from CateringCatalog page to Catering table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Catering> Add(Data.MasterCatalog.Catering CateringData)
        {
            ReturnValue<Data.MasterCatalog.Catering> ReturnValue = new ReturnValue<Data.MasterCatalog.Catering>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CateringData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(CateringData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.AddToCatering(CateringData);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        ///  To update values from CateringCatalog page to Catering table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Catering> Update(Data.MasterCatalog.Catering CateringData)
        {
            ReturnValue<Data.MasterCatalog.Catering> ReturnValue = new ReturnValue<Data.MasterCatalog.Catering>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CateringData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(CateringData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.Catering.Attach(CateringData);

                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Catering>(CateringData, Container);
                        Container.SaveChanges();

                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        ///  To update the selected record as deleted, from CateringCatalog page in Catering table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Catering> Delete(Data.MasterCatalog.Catering CateringData)
        {
            ReturnValue<Data.MasterCatalog.Catering> ReturnValue = new ReturnValue<Data.MasterCatalog.Catering>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CateringData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.Catering).Name, CateringData);
                    Container.Catering.DeleteObject(CateringData);
                    Container.SaveChanges();

                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID> GetAllCateringByAirportID(long airportID)
        {
            ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID> objFbo = new ReturnValue<Data.MasterCatalog.GetAllCateringByAirportID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFbo.EntityList = objContainer.GetAllCateringByAirportID(CustomerID, airportID).ToList();
                    objFbo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFbo;
        }

        public ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD> GetCateringByAirportIDANDCateringCD(long airportID, string cateringCD)
        {
            ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD> objFbo = new ReturnValue<Data.MasterCatalog.GetCateringByAirportIDANDCateringCD>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFbo.EntityList = objContainer.GetCateringByAirportIDANDCateringCD(CustomerID, airportID, cateringCD).ToList();
                    objFbo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFbo;
        }

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Catering> GetLock(Data.MasterCatalog.Catering CateringData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Catering> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values for CateringCatalog page from Catering table
        /// </summary>
        /// <returns></returns>      
        public ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID> GetAllCateringByCateringID(long CateringID)
        {
            ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllCateringByCateringID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the Catering table based on CustomerID
                    ReturnValue.EntityList = Container.GetAllCateringByCateringID(CateringID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
    }
}
