﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetForecastManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FleetForeCast>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FleetForeCast> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.FleetForeCast> Add(Data.MasterCatalog.FleetForeCast FleetForecastType)
        {
            ReturnValue<Data.MasterCatalog.FleetForeCast> ReturnValue = new ReturnValue<Data.MasterCatalog.FleetForeCast>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                   
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToFleetForeCast(FleetForecastType);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetForeCast> Update(Data.MasterCatalog.FleetForeCast FleetForecastType)
        {
            ReturnValue<Data.MasterCatalog.FleetForeCast> ReturnValue = new ReturnValue<Data.MasterCatalog.FleetForeCast>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {   
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.FleetForeCast.Attach(FleetForecastType);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetForeCast>(FleetForecastType, Container);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetForeCast> Delete(Data.MasterCatalog.FleetForeCast FleetForecastType)
        {
            ReturnValue<Data.MasterCatalog.FleetForeCast> ReturnValue = new ReturnValue<Data.MasterCatalog.FleetForeCast>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {   
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.FleetForeCast).Name, FleetForecastType);
                    Container.FleetForeCast.DeleteObject(FleetForecastType);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetAllFleetForecast> GetListInfo(Int64 FleetID, int year)
        {
            ReturnValue<Data.MasterCatalog.GetAllFleetForecast> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFleetForecast>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {   
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllFleetForecast(CustomerID, FleetID, year).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetForeCast> GetLock(Data.MasterCatalog.FleetForeCast FleetForecastType)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }


    }
}


