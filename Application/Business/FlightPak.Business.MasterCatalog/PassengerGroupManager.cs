﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class PassengerGroupManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.PassengerGroup>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for PassengerGroup page from PassengerGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerGroup> GetList()
        {
            ReturnValue<Data.MasterCatalog.PassengerGroup> Results = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.MasterCatalog.PassengerGroup>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Results.EntityList = Container.GetAllPassengerGroup(CustomerID).ToList();
                    //if (Results.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < Results.EntityList.Count; i++)
                    //    {
                    //        Results.EntityList[i].PassengerGroupCD = Results.EntityList[i].PassengerGroupCD != null ? Results.EntityList[i].PassengerGroupCD.Trim() : string.Empty;
                    //    }
                    //}
                    Results.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// To get the values for PassengerGroup page from PassengerGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetPassengerGroupList> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetPassengerGroupList> Results = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.MasterCatalog.GetPassengerGroupList>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Results.EntityList = Container.GetPassengerGroupList(CustomerID).ToList();
                    Results.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// To insert values from PassengerGroup page to PassengerGroup table
        /// </summary>
        /// <param name="PassengerGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerGroup> Add(Data.MasterCatalog.PassengerGroup PassengerGroup)
        {
            ReturnValue<Data.MasterCatalog.PassengerGroup> Results = new ReturnValue<Data.MasterCatalog.PassengerGroup>(); ;
            Data.MasterCatalog.MasterDataContainer Container = null;
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    Results.ReturnFlag = base.Validate(PassengerGroup, ref Results.ErrorMessage);
                    //For Data Anotation
                    if (Results.ReturnFlag)
                    {                        
                        Container = new Data.MasterCatalog.MasterDataContainer();
                        //Container.AddToPassengerGroup(PassengerGroup);
                        Container.PassengerGroup.AddObject(PassengerGroup);
                        Container.SaveChanges();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// To update values from PassengerGroup page to PassengerGroup table
        /// </summary>
        /// <param name="PassengerGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerGroup> Update(Data.MasterCatalog.PassengerGroup PassengerGroup)
        {
            ReturnValue<Data.MasterCatalog.PassengerGroup> ReturnValue = new ReturnValue<Data.MasterCatalog.PassengerGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroup))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {                        
                        //For Data Anotation
                        ReturnValue.ReturnFlag = base.Validate(PassengerGroup, ref ReturnValue.ErrorMessage);
                        //For Data Anotation
                        if (ReturnValue.ReturnFlag)
                        {
                            foreach (Data.MasterCatalog.PassengerGroupOrder oPassengerGroupOrder in PassengerGroup.PassengerGroupOrder)
                            {
                                oPassengerGroupOrder.CustomerID = PassengerGroup.CustomerID;
                                oPassengerGroupOrder.LastUpdUID = PassengerGroup.LastUpdUID;
                            }
                            objContainer.PassengerGroup.Attach(PassengerGroup);
                            objContainer.ObjectStateManager.ChangeObjectState(PassengerGroup, System.Data.EntityState.Modified);
                            foreach (Data.MasterCatalog.PassengerGroupOrder oPassengerGroupOrder in PassengerGroup.PassengerGroupOrder)
                            {
                                if (oPassengerGroupOrder.PassengerGroupOrderID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oPassengerGroupOrder, System.Data.EntityState.Added);
                                    objContainer.PassengerGroupOrder.AddObject(oPassengerGroupOrder);
                                }
                                else
                                {
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerGroupOrder>(oPassengerGroupOrder, objContainer);
                                }
                            }
                            objContainer.SaveChanges();
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To update the selected record as deleted, from PassengerGroup page in PassengerGroup table
        /// </summary>
        /// <param name="PassengerGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerGroup> Delete(Data.MasterCatalog.PassengerGroup PassengerGroup)
        {
            ReturnValue<Data.MasterCatalog.PassengerGroup> Results = null;
            Data.MasterCatalog.MasterDataContainer Container = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerGroup))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.MasterCatalog.PassengerGroup>();
                    Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.PassengerGroup).Name, PassengerGroup);
                    Container.PassengerGroup.DeleteObject(PassengerGroup);
                    Container.SaveChanges();
                    Results.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="PassengerGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerGroup> GetLock(Data.MasterCatalog.PassengerGroup PassengerGroup)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
