﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;

namespace FlightPak.Business.MasterCatalog
{
    public class WorldWindManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Account>, IMasterCatalog.ICacheable
    {

        public ReturnValue<WorldWind> GetWorldWindInfo(long DepartZoneAirprt, long ArrivZoneAirprt, string Quartr, string Alt)
        {
            ReturnValue<WorldWind> ret = new ReturnValue<WorldWind>();
            MasterDataContainer cs = new MasterDataContainer();
            cs.ContextOptions.LazyLoadingEnabled = false;
            cs.ContextOptions.ProxyCreationEnabled = false;
            ret.EntityList = cs.GetWorldWindInfo(DepartZoneAirprt, ArrivZoneAirprt, Quartr, Alt).ToList();
            ret.ReturnFlag = true;
            return ret;
        }

        #region not implemented
        public ReturnValue<Data.MasterCatalog.Account> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> Add(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> Update(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> Delete(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> GetLock(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        #endregion 


    }
}
