﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetComponentManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FleetComponent>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FleetComponent> GetList()
        {
            ReturnValue<Data.MasterCatalog.FleetComponent> objFleetComponentType = new ReturnValue<Data.MasterCatalog.FleetComponent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetComponentType.EntityList = objContainer.GetAllFleetComponent(CustomerID).ToList();
                    objFleetComponentType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetComponentType;
        }

        public ReturnValue<Data.MasterCatalog.FleetComponent> Add(Data.MasterCatalog.FleetComponent fleetcomponenttype)
        {
            ReturnValue<Data.MasterCatalog.FleetComponent> objFleetComponentType = new ReturnValue<Data.MasterCatalog.FleetComponent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetcomponenttype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFleetComponentType.ReturnFlag = base.Validate(fleetcomponenttype, ref objFleetComponentType.ErrorMessage);
                    //For Data Anotation
                    if (objFleetComponentType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFleetComponent(fleetcomponenttype);
                        objContainer.SaveChanges();
                        objFleetComponentType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetComponentType;
        }

        public ReturnValue<Data.MasterCatalog.FleetComponent> Update(Data.MasterCatalog.FleetComponent fleetcomponenttype)
        {
            ReturnValue<Data.MasterCatalog.FleetComponent> objFleetComponentType = new ReturnValue<Data.MasterCatalog.FleetComponent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetcomponenttype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFleetComponentType.ReturnFlag = base.Validate(fleetcomponenttype, ref objFleetComponentType.ErrorMessage);
                    //For Data Anotation
                    if (objFleetComponentType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FleetComponent.Attach(fleetcomponenttype);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetComponent>(fleetcomponenttype, objContainer);
                        objContainer.SaveChanges();
                        objFleetComponentType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetComponentType;
        }

        public ReturnValue<Data.MasterCatalog.FleetComponent> Delete(Data.MasterCatalog.FleetComponent fleetcomponenttype)
        {
            ReturnValue<Data.MasterCatalog.FleetComponent> objFleetComponentType = new ReturnValue<Data.MasterCatalog.FleetComponent>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetcomponenttype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FleetComponent).Name, fleetcomponenttype);
                    objContainer.FleetComponent.DeleteObject(fleetcomponenttype);
                    objContainer.SaveChanges();
                    objFleetComponentType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetComponentType;
        }

        public ReturnValue<Data.MasterCatalog.FleetComponent> GetLock(Data.MasterCatalog.FleetComponent fleetcomponenttype)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}
