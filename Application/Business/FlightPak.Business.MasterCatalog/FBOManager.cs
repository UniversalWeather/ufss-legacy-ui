﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class FBOManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FBO>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        /// <summary>
        /// To fetch the values from the FBO table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters> GetFBOByAirportWithFilters(long AirportID, long FBOID, string FBOCD, bool FetchChoiceOnly, bool FetchUVAIROnly, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters> objFbo = new ReturnValue<Data.MasterCatalog.GetFBOByAirportIDWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFbo.EntityList = objContainer.GetFBOByAirportIDWithFilters(CustomerID, AirportID, FBOID, FBOCD, FetchChoiceOnly, FetchUVAIROnly, FetchActiveOnly).ToList();
                    objFbo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFbo;
        }

        #endregion
        /// <summary>
        /// To get the values for FBOCatalog page from FBO table
        /// </summary>
        /// <returns></returns>    
        public ReturnValue<Data.MasterCatalog.FBO> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To insert values from FBOCatalog page to FBO table
        /// </summary>
        /// <param name="FBO"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FBO> Add(Data.MasterCatalog.FBO FBOData)
        {
            ReturnValue<Data.MasterCatalog.FBO> ReturnValue = new ReturnValue<Data.MasterCatalog.FBO>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(FBOData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        FBOData.CustomerID = CustomerID;
                        Container.AddToFBO(FBOData);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        ///  To update values from FBOCatalog page to FBO table
        /// </summary>
        /// <param name="FBO"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FBO> Update(Data.MasterCatalog.FBO FBOData)
        {
            ReturnValue<Data.MasterCatalog.FBO> ReturnValue = new ReturnValue<Data.MasterCatalog.FBO>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(FBOData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        FBOData.CustomerID = CustomerID;
                        Container.FBO.Attach(FBOData);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FBO>(FBOData, Container);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        ///  To update the selected record as deleted, from FBOCatalog page in FBO table
        /// </summary>
        /// <param name="FBO"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FBO> Delete(Data.MasterCatalog.FBO FBOData)
        {
            ReturnValue<Data.MasterCatalog.FBO> ReturnValue = new ReturnValue<Data.MasterCatalog.FBO>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FBOData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.FBO).Name, FBOData);
                    Container.FBO.DeleteObject(FBOData);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        /// <summary>
        /// To fetch the values from the FBO table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFBO> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllFBO> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFBO>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    ReturnValue.EntityList = Container.GetAllFBO(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }


        /// <summary>
        /// To fetch the values from the FBO table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID> GetAllFBOByAirportID(long airportID)
        {
            ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID> objFbo = new ReturnValue<Data.MasterCatalog.GetAllFBOByAirportID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFbo.EntityList = objContainer.GetAllFBOByAirportID(CustomerID, airportID).ToList();
                    objFbo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFbo;
        }
        /// <summary>
        /// To fetch the values from the FBO table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD> GetFBOByAirportIDAndFBOCD(long airportID, string fboCD)
        {
            ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD> objFbo = new ReturnValue<Data.MasterCatalog.GetFBOByAirportIDAndFBOCD>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFbo.EntityList = objContainer.GetFBOByAirportIDAndFBOCD(CustomerID, airportID, fboCD).ToList();
                    objFbo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFbo;
        }


        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="FBO"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FBO> GetLock(Data.MasterCatalog.FBO FBOData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values for CateringCatalog page from Catering table
        /// </summary>
        /// <returns></returns>      
        public ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID> GetAllFBOByFBOID(long FBOID)
        {
            ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFBOByFBOID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the Catering table based on CustomerID
                    ReturnValue.EntityList = Container.GetAllFBOByFBOID(FBOID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
    }
}

