﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewDutyTypeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewDutyType>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for CrewDutyTypeCatalog page from Catering table
        /// </summary>
        /// <returns></returns>      
        public ReturnValue<Data.MasterCatalog.CrewDutyType> GetList()
        {
            ReturnValue<Data.MasterCatalog.CrewDutyType> objCrewDutyType = new ReturnValue<Data.MasterCatalog.CrewDutyType>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCrewDutyType.EntityList = objContainer.GetAllCrewDutyType(CustomerID).ToList();
                    //if (objCrewDutyType.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objCrewDutyType.EntityList.Count; i++)
                    //    {
                    //        objCrewDutyType.EntityList[i].DutyTypeCD = objCrewDutyType.EntityList[i].DutyTypeCD != null ? objCrewDutyType.EntityList[i].DutyTypeCD.Trim() : string.Empty;
                    //    }
                    //}
                    objCrewDutyType.ReturnFlag = true;
                    return objCrewDutyType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewDutyType;
            }

        }
        /// <summary>
        /// To insert values from CrewDutyType page to CrewDutyType table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyType> Add(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyType> objCrewDutyType = new ReturnValue<Data.MasterCatalog.CrewDutyType>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDuty))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewDutyType.ReturnFlag = base.Validate(crewDuty, ref objCrewDutyType.ErrorMessage);
                    //For Data Anotation
                    if (objCrewDutyType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToCrewDutyType(crewDuty);
                        objContainer.SaveChanges();
                        objCrewDutyType.ReturnFlag = true;
                        //return objCrewDutyType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewDutyType;
            }

        }
        /// <summary>
        ///  To update values from CrewDutyType page to CrewDutyType table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyType> Update(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyType> objCrewDutyType = new ReturnValue<Data.MasterCatalog.CrewDutyType>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewDuty))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewDutyType.ReturnFlag = base.Validate(crewDuty, ref objCrewDutyType.ErrorMessage);
                    //For Data Anotation
                    if (objCrewDutyType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.CrewDutyType.Attach(crewDuty);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewDutyType>(crewDuty, objContainer);
                        objContainer.SaveChanges();
                        objCrewDutyType.ReturnFlag = true;
                        //return objCrewDutyType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewDutyType;
            }

        }
        /// <summary>
        ///  To update the selected record as deleted, from CrewDutytype page in crewdutytype table
        /// </summary>
        /// <param name="Catering"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyType> Delete(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            ReturnValue<Data.MasterCatalog.CrewDutyType> objCrewDutyType = new ReturnValue<Data.MasterCatalog.CrewDutyType>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.AttachTo(typeof(Data.MasterCatalog.CrewDutyType).Name, crewDuty);
                    objContainer.CrewDutyType.DeleteObject(crewDuty);
                    objContainer.SaveChanges();
                    objCrewDutyType.ReturnFlag = true;
                    return objCrewDutyType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewDutyType;
            }

        }
       
        /// <summary>
        /// Hook for implementing locking functionality       
        /// </summary>
        /// <param name="crewDuty"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewDutyType> GetLock(Data.MasterCatalog.CrewDutyType crewDuty)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
