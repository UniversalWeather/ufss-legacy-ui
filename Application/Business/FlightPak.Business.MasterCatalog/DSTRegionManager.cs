﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class DSTRegionManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.DSTRegion>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for Dst page from Dst table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DSTRegion> GetList()
        {
            ReturnValue<Data.MasterCatalog.DSTRegion> objDSTRegionType = new ReturnValue<Data.MasterCatalog.DSTRegion>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objDSTRegionType.EntityList = objContainer.GetAllDSTRegion().ToList();
                    objDSTRegionType.ReturnFlag = true;
                    return objDSTRegionType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDSTRegionType;
            }
        }
        /// <summary>
        ///  To insert values from Dst page to Dst table
        /// </summary>
        public ReturnValue<Data.MasterCatalog.DSTRegion> Add(Data.MasterCatalog.DSTRegion DSTRegionType)
        {
            ReturnValue<Data.MasterCatalog.DSTRegion> objDSTRegionType = new ReturnValue<Data.MasterCatalog.DSTRegion>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToDSTRegion(DSTRegionType);
                    objContainer.SaveChanges();
                    objDSTRegionType.ReturnFlag = true;
                    return objDSTRegionType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDSTRegionType;
            }
        }
        /// <summary>
        /// To insert values from Dst page to Dst table
        /// </summary>
        /// <param name="DSTRegionType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DSTRegion> Update(Data.MasterCatalog.DSTRegion DSTRegionType)
        {
            ReturnValue<Data.MasterCatalog.DSTRegion> objDSTRegionType = new ReturnValue<Data.MasterCatalog.DSTRegion>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DSTRegionType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.DSTRegion.Attach(DSTRegionType);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.DSTRegion>(DSTRegionType, objContainer);
                    objContainer.SaveChanges();
                    objDSTRegionType.ReturnFlag = true;
                    return objDSTRegionType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDSTRegionType;
            }
        }
        /// <summary>
        /// To update the selected record as deleted, from Dst page in Dst table
        /// </summary>
        /// <param name="DSTRegionType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DSTRegion> Delete(Data.MasterCatalog.DSTRegion DSTRegionType)
        {
            ReturnValue<Data.MasterCatalog.DSTRegion> objDSTRegionType = new ReturnValue<Data.MasterCatalog.DSTRegion>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DSTRegionType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.DSTRegion).Name, DSTRegionType);
                    objContainer.DSTRegion.DeleteObject(DSTRegionType);
                    objContainer.SaveChanges();
                    objDSTRegionType.ReturnFlag = true;
                    return objDSTRegionType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDSTRegionType;
            }
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DSTRegionType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DSTRegion> GetLock(Data.MasterCatalog.DSTRegion DSTRegionType)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values for Dst page from Dst table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DSTRegion> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.DSTRegion> objDSTRegionType = new ReturnValue<Data.MasterCatalog.DSTRegion>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objDSTRegionType.EntityList = objContainer.DSTRegion.ToList();
                    objDSTRegionType.ReturnFlag = true;
                    return objDSTRegionType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objDSTRegionType;
            }
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
