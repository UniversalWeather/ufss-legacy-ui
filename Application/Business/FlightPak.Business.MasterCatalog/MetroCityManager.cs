﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class MetroCityManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Metro>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Metro> GetMetroWithFilters(long MetroId, string MetroCD)
        {
            ReturnValue<Data.MasterCatalog.Metro> objMetroCityType = new ReturnValue<Data.MasterCatalog.Metro>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //Guid Customer_ID = Guid.Parse("87429CBE-DD96-4500-A4A3-5F0D4C112296");
            // Fix for recursive infinite loop
            objContainer.ContextOptions.LazyLoadingEnabled = false;
            objContainer.ContextOptions.ProxyCreationEnabled = false;
            // objMetroCityType.EntityList = objContainer.GetAllDelayTypeMaster(Customer_ID).ToList();
            objMetroCityType.EntityList = objContainer.GetMetroCityWithFilters(CustomerID, MetroId, MetroCD).ToList();
            objMetroCityType.ReturnFlag = true;
            return objMetroCityType;
        }

        #endregion

        public ReturnValue<Data.MasterCatalog.Metro> GetList()
        {
            ReturnValue<Data.MasterCatalog.Metro> objMetroCityType = new ReturnValue<Data.MasterCatalog.Metro>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //Guid Customer_ID = Guid.Parse("87429CBE-DD96-4500-A4A3-5F0D4C112296");
            // Fix for recursive infinite loop
            objContainer.ContextOptions.LazyLoadingEnabled = false;
            objContainer.ContextOptions.ProxyCreationEnabled = false;
            // objMetroCityType.EntityList = objContainer.GetAllDelayTypeMaster(Customer_ID).ToList();
            objMetroCityType.EntityList = objContainer.GetAllMetroCity(CustomerID).ToList();
            objMetroCityType.ReturnFlag = true;
            return objMetroCityType;
        }

        public ReturnValue<Data.MasterCatalog.Metro> Add(Data.MasterCatalog.Metro MetroCityType)
        {
            ReturnValue<Data.MasterCatalog.Metro> ret = new ReturnValue<Data.MasterCatalog.Metro>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MetroCityType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(MetroCityType, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        ReturnValue<Data.MasterCatalog.Metro> objMetroCityType = new ReturnValue<Data.MasterCatalog.Metro>();
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToMetro(MetroCityType);
                        objContainer.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.Metro> Update(Data.MasterCatalog.Metro MetroCityType)
        {
            ReturnValue<Data.MasterCatalog.Metro> ret = new ReturnValue<Data.MasterCatalog.Metro>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MetroCityType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(MetroCityType, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        ReturnValue<Data.MasterCatalog.Metro> objMetroCityType = new ReturnValue<Data.MasterCatalog.Metro>();
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.Metro.Attach(MetroCityType);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Metro>(MetroCityType, objContainer);
                        objContainer.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.Metro> Delete(Data.MasterCatalog.Metro MetroCityType)
        {
            ReturnValue<Data.MasterCatalog.Metro> objMetroCityType = new ReturnValue<Data.MasterCatalog.Metro>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(MetroCityType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Metro).Name, MetroCityType);
                    objContainer.Metro.DeleteObject(MetroCityType);
                    objContainer.SaveChanges();
                    objMetroCityType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objMetroCityType;
        }

        public ReturnValue<Data.MasterCatalog.Metro> GetListInfo()
        {

            ReturnValue<Data.MasterCatalog.Metro> objMetroCityType = new ReturnValue<Data.MasterCatalog.Metro>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            objMetroCityType.EntityList = objContainer.Metro.ToList();
            objMetroCityType.ReturnFlag = true;
            return objMetroCityType;
        }

        public ReturnValue<Data.MasterCatalog.Metro> GetLock(Data.MasterCatalog.Metro MetroCityType)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
