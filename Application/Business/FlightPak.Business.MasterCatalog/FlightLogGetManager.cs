﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class FlightLogGetManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.GetAllFlightLog>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for CustomFlightLog page from TSFlightLog table
        /// </summary>
        /// <returns></returns>       
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetList()
        {
            ReturnValue<Data.MasterCatalog.GetAllFlightLog> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFlightLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the TSFlightLog table based on CustomerID
                    //ReturnValue.EntityList = Container.GetAllFlightLog(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.GetAllPilotLog> GetPilotList()
        {
            ReturnValue<Data.MasterCatalog.GetAllPilotLog> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllPilotLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the TSFlightLog table based on CustomerID
                    //ReturnValue.EntityList = Container.GetAllPilotLog(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To insert values from CustomFlightLog page to TSFlightLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> Add(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        ///  To update values from CustomFlightLog page to TSFlightLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> Update(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        ///  To update the selected record as deleted, from CustomFlightLog page in TSFlightLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> Delete(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To fetch the values from the TSFlightLog table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetListInfo(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetLock(Data.MasterCatalog.GetAllFlightLog GetAllFlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.GetAllFlightLog> GetAllFlightLog(Data.MasterCatalog.TSFlightLog TSFlightLog)
        {
            ReturnValue<Data.MasterCatalog.GetAllFlightLog> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFlightLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSFlightLog))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the TSFlightLog table based on CustomerID
                    ReturnValue.EntityList = Container.GetAllFlightLog(CustomerID, TSFlightLog.HomebaseID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.GetAllPilotLog> GetPilotList(Data.MasterCatalog.TSFlightLog TSFlightLog)
        {
            ReturnValue<Data.MasterCatalog.GetAllPilotLog> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllPilotLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSFlightLog))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the TSFlightLog table based on CustomerID
                    ReturnValue.EntityList = Container.GetAllPilotLog(CustomerID, TSFlightLog.HomebaseID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
