﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace FlightPak.Business.MasterCatalog
{
    public class TripsheetReportHeaderManager : BaseManager, IMasterCatalog.ICacheable //,IMasterCatalog.IMasterData<Data.MasterCatalog.TripSheetReportHeader>
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.TripSheetReportHeader> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> GetTripSheetReportHeaderList()
        {
            ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader> ret = new ReturnValue<Data.MasterCatalog.GetTripSheetReportHeader>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetTripSheetReportHeader(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        /// <summary>
        /// Method to get Report Sheet Detail
        /// </summary>
        /// <param name="TripSheetReportHeaderID"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetReportSheetDetail> GetReportSheetDetail(Int64 TripSheetReportHeaderID)
        {
            ReturnValue<Data.MasterCatalog.GetReportSheetDetail> ret = new ReturnValue<Data.MasterCatalog.GetReportSheetDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetReportSheetDetail(CustomerID, TripSheetReportHeaderID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }


        /// <summary>
        /// Method to get Report Sheet Detail using UMPermission
        /// </summary>
        /// <param name="TripSheetReportHeaderID"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission> GetReportSheetDetailByUMPermission(Int64 TripSheetReportHeaderID)
        {
            ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission> ret = new ReturnValue<Data.MasterCatalog.GetTripSheetDetailByUMPermission>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetTripSheetDetailByUMPermission(CustomerID, TripSheetReportHeaderID, UserPrincipal.Identity.SessionID, UserPrincipal.Identity.IsSysAdmin).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        /// <summary>
        /// Method to Update Report Sheet Detail
        /// </summary>
        /// <param name="TripSheetHeaderID"></param>
        /// <param name="ReportNumber"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TripSheetReportHeader> UpdateTripSheetDetail(Int64 TripSheetHeaderID, List<int> ReportNumber)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportHeader> objTripSheetReportHeader = new ReturnValue<Data.MasterCatalog.TripSheetReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TripSheetHeaderID, ReportNumber))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.UpdateReportDetailasDeleted(CustomerID, TripSheetHeaderID);
                        foreach (int RNumber in ReportNumber)
                        {
                            objContainer.UpdateReportDetail(CustomerID, TripSheetHeaderID, RNumber);
                        }
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                        objTripSheetReportHeader.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportHeader;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportHeader> Add(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportHeader> objTripSheetReportHeader = new ReturnValue<Data.MasterCatalog.TripSheetReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.AddToTripSheetReportHeader(oTripSheetReportHeader);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                        objTripSheetReportHeader.EntityInfo = oTripSheetReportHeader;
                        objTripSheetReportHeader.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportHeader;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportHeader> Update(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportHeader> objTripSheetReportHeader = new ReturnValue<Data.MasterCatalog.TripSheetReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.TripSheetReportHeader.Attach(oTripSheetReportHeader);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.TripSheetReportHeader>(oTripSheetReportHeader, objContainer);
                        objContainer.SaveChanges();
                        objTripSheetReportHeader.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportHeader;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportHeader> Delete(Data.MasterCatalog.TripSheetReportHeader oTripSheetReportHeader)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportHeader> objTripSheetReportHeader = new ReturnValue<Data.MasterCatalog.TripSheetReportHeader>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportHeader))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TripSheetReportHeader).Name, oTripSheetReportHeader);
                    objContainer.TripSheetReportHeader.DeleteObject(oTripSheetReportHeader);
                    objContainer.SaveChanges();
                    objTripSheetReportHeader.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportHeader;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportHeader> GetLock(Data.MasterCatalog.TripSheetReportHeader tripManagerChecklist)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetUserGroup> GetUserGroup()
        {
            ReturnValue<Data.MasterCatalog.GetUserGroup> ret = new ReturnValue<Data.MasterCatalog.GetUserGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetUserGroup(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail> GetTripSheetReportDetail()
        {
            ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail> ret = new ReturnValue<Data.MasterCatalog.GetTripSheetReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = objContainer.GetTripSheetReportDetail(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportDetail> Add(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportDetail> objTripSheetReportDetail = new ReturnValue<Data.MasterCatalog.TripSheetReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportDetail))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.AddToTripSheetReportDetail(oTripSheetReportDetail);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                        objTripSheetReportDetail.EntityInfo = oTripSheetReportDetail;
                        objTripSheetReportDetail.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportDetail;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportDetail> Update(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportDetail> objTripSheetReportDetail = new ReturnValue<Data.MasterCatalog.TripSheetReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportDetail))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.TripSheetReportDetail.Attach(oTripSheetReportDetail);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.TripSheetReportDetail>(oTripSheetReportDetail, objContainer);
                        objContainer.SaveChanges();
                        objTripSheetReportDetail.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportDetail;
        }

        public ReturnValue<Data.MasterCatalog.TripSheetReportDetail> Delete(Data.MasterCatalog.TripSheetReportDetail oTripSheetReportDetail)
        {
            ReturnValue<Data.MasterCatalog.TripSheetReportDetail> objTripSheetReportDetail = new ReturnValue<Data.MasterCatalog.TripSheetReportDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oTripSheetReportDetail))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TripSheetReportDetail).Name, oTripSheetReportDetail);
                    objContainer.TripSheetReportDetail.DeleteObject(oTripSheetReportDetail);
                    objContainer.SaveChanges();
                    objTripSheetReportDetail.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTripSheetReportDetail;
        }

    }
}
