﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class ConversionTableManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.ConversionTable>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.ConversionTable> GetList()
        {
            ReturnValue<Data.MasterCatalog.ConversionTable> ret = new ReturnValue<Data.MasterCatalog.ConversionTable>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.ConversionTableManager objAcct = new FlightPak.Business.MasterCatalog.ConversionTableManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllConversionTable(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;

        }

        public ReturnValue<Data.MasterCatalog.ConversionTable> Add(Data.MasterCatalog.ConversionTable conversionTable)
        {
            ReturnValue<Data.MasterCatalog.ConversionTable> ret = new ReturnValue<Data.MasterCatalog.ConversionTable>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(conversionTable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    conversionTable.CustomerID = CustomerID;
                    conversionTable.IsDeleted = false;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AddToConversionTable(conversionTable);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.MasterCatalog.ConversionTable> Update(Data.MasterCatalog.ConversionTable conversionTable)
        {
            ReturnValue<Data.MasterCatalog.ConversionTable> ret = new ReturnValue<Data.MasterCatalog.ConversionTable>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(conversionTable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ConversionTable.Attach(conversionTable);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.ConversionTable>(conversionTable, cs);
                    cs.SaveChanges();

                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;


        }
        public ReturnValue<Data.MasterCatalog.ConversionTable> Delete(Data.MasterCatalog.ConversionTable conversionTable)
        {
            ReturnValue<Data.MasterCatalog.ConversionTable> ret = new ReturnValue<Data.MasterCatalog.ConversionTable>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(conversionTable))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    conversionTable.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.ConversionTable).Name, conversionTable);
                    cs.ConversionTable.DeleteObject(conversionTable);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return ret;
        }

        public ReturnValue<Data.MasterCatalog.ConversionTable> GetLock(Data.MasterCatalog.ConversionTable conversionTable)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}
