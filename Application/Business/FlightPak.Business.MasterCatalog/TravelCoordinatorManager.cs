﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class TravelCoordinatorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TravelCoordinator>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.TravelCoordinator> GetList()
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinator> objTravelCoordinatorType = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objTravelCoordinatorType.EntityList = objContainer.GetAllTravelCoordinator(CustomerID).ToList();
                    objTravelCoordinatorType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorType;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinator> Add(Data.MasterCatalog.TravelCoordinator TravelCoordinator)
        {
            //ReturnValue<Data.MasterCatalog.TravelCoordinator> objTravelCoordinatorType = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //objContainer.AddToTravelCoordinator(travelCoordinatorType);
            //objContainer.SaveChanges();
            //objTravelCoordinatorType.ReturnFlag = true;
            //return objTravelCoordinatorType;
            ReturnValue<Data.MasterCatalog.TravelCoordinator> ReturnValue = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinator))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(TravelCoordinator, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.TravelCoordinator.AddObject(TravelCoordinator);
                        foreach (Data.MasterCatalog.TravelCoordinatorRequestor oTravelCoordinatorRequestor in TravelCoordinator.TravelCoordinatorRequestor)
                        {
                            oTravelCoordinatorRequestor.CustomerID = TravelCoordinator.CustomerID;
                            if (oTravelCoordinatorRequestor.TravelCoordinatorRequestorID < 0)
                            {
                                objContainer.TravelCoordinatorRequestor.AddObject(oTravelCoordinatorRequestor);
                                objContainer.ObjectStateManager.ChangeObjectState(oTravelCoordinatorRequestor, System.Data.EntityState.Added);
                            }
                            else
                            {
                                objContainer.TravelCoordinatorRequestor.Attach(oTravelCoordinatorRequestor);
                                Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinatorRequestor>(oTravelCoordinatorRequestor, objContainer);
                            }
                        }
                        foreach (Data.MasterCatalog.TravelCoordinatorFleet oTravelCoordinatorFleet in TravelCoordinator.TravelCoordinatorFleet)
                        {
                            oTravelCoordinatorFleet.CustomerID = TravelCoordinator.CustomerID;
                            if (oTravelCoordinatorFleet.TravelCoordinatorFleetID < 0)
                            {
                                objContainer.TravelCoordinatorFleet.AddObject(oTravelCoordinatorFleet);
                                objContainer.ObjectStateManager.ChangeObjectState(oTravelCoordinatorFleet, System.Data.EntityState.Added);
                            }
                            else
                            {
                                objContainer.TravelCoordinatorFleet.Attach(oTravelCoordinatorFleet);
                                Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinatorFleet>(oTravelCoordinatorFleet, objContainer);
                            }
                        }
                        objContainer.SaveChanges();
                    }
                    ReturnValue.ReturnFlag = true;
                        }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinator> Update(Data.MasterCatalog.TravelCoordinator TravelCoordinator)
        {
            //ReturnValue<Data.MasterCatalog.TravelCoordinator> objTravelCoordinatorType = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //objContainer.TravelCoordinator.Attach(travelCoordinatorType);
            //Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinator>(travelCoordinatorType, objContainer);
            //objContainer.SaveChanges();
            //objTravelCoordinatorType.ReturnFlag = true;
            //return objTravelCoordinatorType;  
            ReturnValue<Data.MasterCatalog.TravelCoordinator> ReturnValue = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinator))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
                                                                         new KeyValuePair<string, object>("TravelCoordinatorID", TravelCoordinator.TravelCoordinatorID)
                                                                         };
                    TravelCoordinator.EntityKey = new EntityKey("MasterDataContainer.TravelCoordinator", entityKeyValues);
                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(TravelCoordinator, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                        {
                            objContainer.TravelCoordinator.Attach(TravelCoordinator);
                            objContainer.ObjectStateManager.ChangeObjectState(TravelCoordinator, System.Data.EntityState.Modified);
                            foreach (Data.MasterCatalog.TravelCoordinatorRequestor oTravelCoordinatorRequestor in TravelCoordinator.TravelCoordinatorRequestor)
                            {
                                oTravelCoordinatorRequestor.CustomerID = TravelCoordinator.CustomerID;
                                if (oTravelCoordinatorRequestor.TravelCoordinatorRequestorID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oTravelCoordinatorRequestor, System.Data.EntityState.Added);
                                    objContainer.TravelCoordinatorRequestor.AddObject(oTravelCoordinatorRequestor);
                                }
                                else
                                {
                                    //objContainer.TravelCoordinatorRequestor.Attach(oTravelCoordinatorRequestor);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinatorRequestor>(oTravelCoordinatorRequestor, objContainer);
                                }
                            }
                            foreach (Data.MasterCatalog.TravelCoordinatorFleet oTravelCoordinatorFleet in TravelCoordinator.TravelCoordinatorFleet)
                            {
                                oTravelCoordinatorFleet.CustomerID = TravelCoordinator.CustomerID;
                                if (oTravelCoordinatorFleet.TravelCoordinatorFleetID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oTravelCoordinatorFleet, System.Data.EntityState.Added);
                                    objContainer.TravelCoordinatorFleet.AddObject(oTravelCoordinatorFleet);
                                }
                                else
                                {
                                    //objContainer.TravelCoordinatorFleet.Attach(oTravelCoordinatorFleet);
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinatorFleet>(oTravelCoordinatorFleet, objContainer);
                                }
                            }
                            objContainer.SaveChanges();
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinator> Delete(Data.MasterCatalog.TravelCoordinator TravelCoordinator)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinator> objTravelCoordinatorType = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TravelCoordinator))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TravelCoordinator).Name, TravelCoordinator);
                    objContainer.TravelCoordinator.DeleteObject(TravelCoordinator);
                    objContainer.SaveChanges();
                    objTravelCoordinatorType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorType;
        }
        public ReturnValue<Data.MasterCatalog.GetTravelCoordinator> GetListInfo()
        {
            //ReturnValue<Data.MasterCatalog.TravelCoordinator> objTravelCoordinatorType = new ReturnValue<Data.MasterCatalog.TravelCoordinator>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //objTravelCoordinatorType.EntityList = objContainer.TravelCoordinator.ToList();
            //objTravelCoordinatorType.ReturnFlag = true;
            //return objTravelCoordinatorType;
            ReturnValue<Data.MasterCatalog.GetTravelCoordinator> objTravelCoordinatorType = new ReturnValue<Data.MasterCatalog.GetTravelCoordinator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objTravelCoordinatorType.EntityList = objContainer.GetTravelCoordinator(CustomerID).ToList();
                    objTravelCoordinatorType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorType;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinator> GetLock(Data.MasterCatalog.TravelCoordinator TravelCoordinator)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
