﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class MasterCatalogBaseManager<TEntity> : BaseManager where TEntity : class
    {
        //public ExceptionManager exManager
        //{
        //    get
        //    {
        //        if (_exManager == null)
        //            exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
        //        return exManager;
        //    }
        //    set { this.exManager = value; }
        //}
        //private ExceptionManager _exManager;

        public RepositoryFactory<TEntity> factory
        {
            get
            {
                if (_factory == null)
                    _factory = new RepositoryFactory<TEntity>();
                return _factory;
            }
        }
        private RepositoryFactory<TEntity> _factory;

        /// <summary>
        /// Exception Handling Sample.
        /// Tracing Sample
        /// </summary>
        /// <returns></returns>
        public virtual ReturnValue<TEntity> GetList()
        {
            return factory.GetList();
        }
        /// <summary>
        /// 1. Example for Validating entities at business level
        /// 2. Example for Exception Handling using Exception Manager
        /// 3. Tracing Sample
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public virtual ReturnValue<TEntity> Add(TEntity entity)
        {
            return factory.Add(entity);
        }
        /// <summary>
        /// 1. Example for Validating entities at business level
        /// 2. Example for Exception Handling using Exception Manager
        /// 3. Tracing Sample
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public virtual ReturnValue<TEntity> Update(TEntity entity)
        {
            return factory.Update(entity);
        }
        /// <summary>
        /// 1. Example for Exception Handling using Exception Manager 
        /// 2. Tracing Sample
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public virtual ReturnValue<TEntity> Delete(TEntity entity)
        {
            return factory.Delete(entity);
        }
        /// <summary>
        /// Method to Get list of entities with parameter CustomerId. 
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        public virtual ReturnValue<TEntity> GetList(Int64 CustomerId)
        {
            return factory.GetList(CustomerId);
        }
        /// <summary>
        ///  Method to get a entity based on the filter criteria CustomerId and PrimaryKeyId
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="PrimaryKeyId"></param>
        /// <returns></returns>
        public virtual ReturnValue<TEntity> GetById(Int64 CustomerId, Int64 PrimaryKeyId)
        {
            return factory.GetById(CustomerId, PrimaryKeyId);
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
