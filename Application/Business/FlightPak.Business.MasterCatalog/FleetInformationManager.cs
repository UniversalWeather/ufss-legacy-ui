﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetInformation : BaseManager, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FleetInformation> Add(Data.MasterCatalog.FleetInformation fleetinformationtype)
        {
            ReturnValue<Data.MasterCatalog.FleetInformation> objfleetInformationType = new ReturnValue<Data.MasterCatalog.FleetInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinformationtype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToFleetInformation(fleetinformationtype);
                    objContainer.SaveChanges();
                    objfleetInformationType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objfleetInformationType;
        }

        public ReturnValue<Data.MasterCatalog.FleetInformation> Update(Data.MasterCatalog.FleetInformation FleetInformations)
        {
            ReturnValue<Data.MasterCatalog.FleetInformation> ObjFleetInformation = new ReturnValue<Data.MasterCatalog.FleetInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FleetInformations))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.FleetInformation.Attach(FleetInformations);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetInformation>(FleetInformations, objContainer);
                    objContainer.SaveChanges();

                    ObjFleetInformation.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ObjFleetInformation;
        }

        public ReturnValue<Data.MasterCatalog.FleetInformation> GetFleetInformation(Int64 tailNum)
        {
            ReturnValue<Data.MasterCatalog.FleetInformation> objFleetInfo = new ReturnValue<Data.MasterCatalog.FleetInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetInfo.EntityList = objContainer.GetFleetInformation(CustomerID, tailNum).ToList();
                    objFleetInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetInfo;
        }

        /// <summary>
        /// To get fleet infor by tailNumber...
        /// </summary>
        /// <param name="tailNumber"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FleetByTailNumberResult> GetFleetByTailNumber(string tailNumber)
        {
            ReturnValue<Data.MasterCatalog.FleetByTailNumberResult> objFleetType = new ReturnValue<Data.MasterCatalog.FleetByTailNumberResult>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetType.EntityInfo = objContainer.GetFleetByTailNumber(CustomerID, tailNumber, ClientId).Where(x => x.CustomerID == CustomerID && x.TailNum == tailNumber).FirstOrDefault();

                  
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.FleetInformation> GetLock(Data.MasterCatalog.FleetInformation fleetinformationtype)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
