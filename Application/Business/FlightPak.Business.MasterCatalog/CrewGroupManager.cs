﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;


namespace FlightPak.Business.MasterCatalog
{

    public class CrewGroupManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewGroup>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        /// <summary>
        /// To get the values for CrewGroup page from CrewtGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters> GetCrewGroupWithFilters(long CrewGroupID, string CrewGroupCD, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters> Results = new ReturnValue<Data.MasterCatalog.GetCrewGroupWithFilters>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Results.EntityList = Container.GetCrewGroupWithFilters(CustomerID, CrewGroupID, CrewGroupCD, FetchActiveOnly).ToList();
                    Results.ReturnFlag = true;
                    return Results;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return Results;
            }

        }
        #endregion
        /// <summary>
        /// To get the values for CrewGroup page from CrewtGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroup> GetList()
        {
            ReturnValue<Data.MasterCatalog.CrewGroup> objCrewType = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    throw new NotImplementedException();
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewType;
            }

        }
        /// <summary>
        /// To get the values for CrewGroup page from CrewtGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllCrewGroup> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewGroup> Results = new ReturnValue<Data.MasterCatalog.GetAllCrewGroup>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Results.EntityList = Container.GetAllCrewGroup(CustomerID).ToList();
                    Results.ReturnFlag = true;
                    return Results;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return Results;
            }

        }
        /// <summary>
        /// To insert values from CrewGroup page to CrewGroup table
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroup> Add(Data.MasterCatalog.CrewGroup crewtype)
        {
            ReturnValue<Data.MasterCatalog.CrewGroup> objCrewType = new ReturnValue<Data.MasterCatalog.CrewGroup>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewtype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewType.ReturnFlag = base.Validate(crewtype, ref objCrewType.ErrorMessage);
                    //For Data Anotation
                    if (objCrewType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToCrewGroup(crewtype);
                        objContainer.SaveChanges();
                        objCrewType.ReturnFlag = true;
                        //return objCrewType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewType;
            }

        }
        /// <summary>
        /// To Update values from CrewGroup page to CrewGroup table
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroup> Update(Data.MasterCatalog.CrewGroup crewtype)
        {
            ReturnValue<Data.MasterCatalog.CrewGroup> objCrewType = new ReturnValue<Data.MasterCatalog.CrewGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewtype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewType.ReturnFlag = base.Validate(crewtype, ref objCrewType.ErrorMessage);
                    //For Data Anotation
                    if (objCrewType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.CrewGroup.Attach(crewtype);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewGroup>(crewtype, objContainer);
                        objContainer.SaveChanges();
                        objCrewType.ReturnFlag = true;
                        //return objFleetType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewType;
            }

        }
        /// <summary>
        /// To Delete values from CrewGroup page to CrewGroup table
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroup> Delete(Data.MasterCatalog.CrewGroup crewtype)
        {

            ReturnValue<Data.MasterCatalog.CrewGroup> objFleetType = new ReturnValue<Data.MasterCatalog.CrewGroup>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewtype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                   

                            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                            objContainer.AttachTo(typeof(Data.MasterCatalog.CrewGroup).Name, crewtype);
                            objContainer.CrewGroup.DeleteObject(crewtype);
                            objContainer.SaveChanges();
                            objFleetType.ReturnFlag = true;
                        }, FlightPak.Common.Constants.Policy.DataLayer);
                    }
                    return objFleetType;               
               

            }

        
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroup> GetLock(Data.MasterCatalog.CrewGroup crewtype)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }


    }
}

