﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class VendorContactManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.VendorContact>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.VendorContact> GetList()
        {
            throw new NotImplementedException();
            //ReturnValue<Data.MasterCatalog.VendorContact> objVendorContact = new ReturnValue<Data.MasterCatalog.VendorContact>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

            //// Fix for recursive infinite loop
            //objContainer.ContextOptions.LazyLoadingEnabled = false;
            //objContainer.ContextOptions.ProxyCreationEnabled = false;
            //objVendorContact.EntityList = objContainer.GetAllVendorContact(CustomerID).ToList();
            //objVendorContact.ReturnFlag = true;
            //return objVendorContact;
        }
        public ReturnValue<Data.MasterCatalog.VendorContact> Add(Data.MasterCatalog.VendorContact vendorcontact)
        {
            ReturnValue<Data.MasterCatalog.VendorContact> objVendorContact = new ReturnValue<Data.MasterCatalog.VendorContact>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorcontact))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objVendorContact.ReturnFlag = base.Validate(vendorcontact, ref objVendorContact.ErrorMessage);
                    //For Data Anotation
                    if (objVendorContact.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToVendorContact(vendorcontact);
                        objContainer.SaveChanges();
                        objVendorContact.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendorContact;
        }

        public ReturnValue<Data.MasterCatalog.VendorContact> Update(Data.MasterCatalog.VendorContact vendorcontact)
        {
            ReturnValue<Data.MasterCatalog.VendorContact> objVendorContact = new ReturnValue<Data.MasterCatalog.VendorContact>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorcontact))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    //For Data Anotation
                    objVendorContact.ReturnFlag = base.Validate(vendorcontact, ref objVendorContact.ErrorMessage);
                    //For Data Anotation
                    if (objVendorContact.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.VendorContact.Attach(vendorcontact);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.VendorContact>(vendorcontact, objContainer);
                        objContainer.SaveChanges();
                        objVendorContact.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendorContact;
        }

        public ReturnValue<Data.MasterCatalog.VendorContact> Delete(Data.MasterCatalog.VendorContact vendorcontact)
        {
            ReturnValue<Data.MasterCatalog.VendorContact> objVendorContact = new ReturnValue<Data.MasterCatalog.VendorContact>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorcontact))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.VendorContact).Name, vendorcontact);
                    objContainer.VendorContact.DeleteObject(vendorcontact);
                    objContainer.SaveChanges();
                    objVendorContact.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendorContact;
        }

        public ReturnValue<Data.MasterCatalog.GetAllVendorContact> GetListInfo(Int64 vendorCode, string vendorType)
        {
            ReturnValue<Data.MasterCatalog.GetAllVendorContact> objVendorContact = new ReturnValue<Data.MasterCatalog.GetAllVendorContact>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(vendorCode, vendorType))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objVendorContact.EntityList = objContainer.GetAllVendorContact(CustomerID, vendorCode, vendorType).ToList();
                    objVendorContact.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objVendorContact;
        }

        public ReturnValue<Data.MasterCatalog.VendorContact> GetLock(Data.MasterCatalog.VendorContact vendorcontact)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
