﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class DepartmentGroupManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.DepartmentGroup>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for DepartmentGroup page from DepartmentGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroup> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values for DepartmentGroup page from DepartmentGroup table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup> Results = new ReturnValue<Data.MasterCatalog.GetAllDepartmentGroup>();
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            // Fix for recursive infinite loop
            Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.ProxyCreationEnabled = false;
            Results.EntityList = Container.GetAllDepartmentGroup(CustomerID).ToList();
            //if (Results.EntityList.Count > 0)
            //{
            //    for (int i = 0; i < Results.EntityList.Count; i++)
            //    {
            //        Results.EntityList[i].DepartmentGroupCD = Results.EntityList[i].DepartmentGroupCD != null ? Results.EntityList[i].DepartmentGroupCD.Trim() : string.Empty;
            //    }
            //}
            Results.ReturnFlag = true;
            return Results;
        }
        /// <summary>
        /// To insert values from DepartmentGroup page to DepartmentGroup table
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroup> Add(Data.MasterCatalog.DepartmentGroup DepartmentGroup)
        {
            ReturnValue<Data.MasterCatalog.DepartmentGroup> Results = new ReturnValue<Data.MasterCatalog.DepartmentGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                     //For Data Anotation
                    Results.ReturnFlag = base.Validate(DepartmentGroup, ref Results.ErrorMessage);
                    //For Data Anotation
                    if (Results.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.AddToDepartmentGroup(DepartmentGroup);
                        Container.SaveChanges();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// To update values from DepartmentGroup page to DepartmentGroup table
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroup> Update(Data.MasterCatalog.DepartmentGroup DepartmentGroup)
        {
            ReturnValue<Data.MasterCatalog.DepartmentGroup> Results = new ReturnValue<Data.MasterCatalog.DepartmentGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    Results.ReturnFlag = base.Validate(DepartmentGroup, ref Results.ErrorMessage);
                    //For Data Anotation
                    if (Results.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.DepartmentGroup.Attach(DepartmentGroup);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.DepartmentGroup>(DepartmentGroup, Container);
                        Container.SaveChanges();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// To update the selected record as deleted, from DepartmentGroup page in DepartmentGroup table
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroup> Delete(Data.MasterCatalog.DepartmentGroup DepartmentGroup)
        {
            ReturnValue<Data.MasterCatalog.DepartmentGroup> Results = new ReturnValue<Data.MasterCatalog.DepartmentGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.DepartmentGroup).Name, DepartmentGroup);
                    Container.DepartmentGroup.DeleteObject(DepartmentGroup);
                    Container.SaveChanges();
                    Results.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentGroup> GetLock(Data.MasterCatalog.DepartmentGroup DepartmentGroup)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
