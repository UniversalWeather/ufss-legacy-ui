﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CustomerAddressManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CustomAddress>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.CustomAddress> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllCustomerAddress> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllCustomerAddress> objCustomerAddress = new ReturnValue<Data.MasterCatalog.GetAllCustomerAddress>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCustomerAddress.EntityList = objContainer.GetAllCustomerAddress(CustomerID, UserName).ToList();

                    objCustomerAddress.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;



        }


        public ReturnValue<Data.MasterCatalog.CustomAddress> Add(Data.MasterCatalog.CustomAddress customerAddress)
        {

            ReturnValue<Data.MasterCatalog.CustomAddress> objCustomerAddress = new ReturnValue<Data.MasterCatalog.CustomAddress>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCustomerAddress.ReturnFlag = base.Validate(customerAddress, ref objCustomerAddress.ErrorMessage);
                    //For Data Anotation
                    if (objCustomerAddress.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToCustomAddress(customerAddress);
                        objContainer.SaveChanges();
                        objCustomerAddress.ReturnFlag = true;
                    }                    
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;
        }

        public ReturnValue<Data.MasterCatalog.CustomAddress> Update(Data.MasterCatalog.CustomAddress customerAddress)
        {

            ReturnValue<Data.MasterCatalog.CustomAddress> objCustomerAddress = new ReturnValue<Data.MasterCatalog.CustomAddress>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCustomerAddress.ReturnFlag = base.Validate(customerAddress, ref objCustomerAddress.ErrorMessage);
                    //For Data Anotation
                    if (objCustomerAddress.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.CustomAddress.Attach(customerAddress);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CustomAddress>(customerAddress, objContainer);
                        objContainer.SaveChanges();
                        objCustomerAddress.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;

        }

        public ReturnValue<Data.MasterCatalog.CustomAddress> Delete(Data.MasterCatalog.CustomAddress customerAddress)
        {
            ReturnValue<Data.MasterCatalog.CustomAddress> objCustomerAddress = new ReturnValue<Data.MasterCatalog.CustomAddress>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(customerAddress))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.CustomAddress).Name, customerAddress);
                    objContainer.CustomAddress.DeleteObject(customerAddress);
                    objContainer.SaveChanges();
                    objCustomerAddress.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCustomerAddress;

        }

        public ReturnValue<Data.MasterCatalog.GetCustomerID> GetCustomerAddressID()
        {
            ReturnValue<Data.MasterCatalog.GetCustomerID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetCustomerID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetCustomerID(CustomerID,UserName).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.CustomAddress> GetLock(Data.MasterCatalog.CustomAddress customerAddress)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
