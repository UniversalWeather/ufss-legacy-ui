﻿using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.MasterCatalog
{
    public class FuelSavedFileFormatManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelSavedFileFormat>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetList()
        {
            ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> ret = new ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelSavedFileFormat.Where(t => t.IsDeleted == false).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> Add(Data.MasterCatalog.FuelSavedFileFormat objFuelSavedFileFormat)
        {
            ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> ret = new ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelSavedFileFormat))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelSavedFileFormat.IsDeleted = false;
                    
                    ret.ReturnFlag = base.Validate(objFuelSavedFileFormat, ref ret.ErrorMessage);
                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelSavedFileFormat.AddObject(objFuelSavedFileFormat);
                        //foreach (var item in objFuelSavedFileFormat.FuelCsvFileFields)
                        //{
                        //    objFuelSavedFileFormat.FuelCsvFileFields.Add(item);
                        //}                       
                        cs.SaveChanges();                        
                        ret.ReturnFlag = true;


                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> Update(Data.MasterCatalog.FuelSavedFileFormat objFuelSavedFileFormat)
        {
            ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> ret = new ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelSavedFileFormat))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    ret.ReturnFlag = base.Validate(objFuelSavedFileFormat, ref ret.ErrorMessage);
                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelSavedFileFormat.Attach(objFuelSavedFileFormat);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelSavedFileFormat>(objFuelSavedFileFormat, cs);
                        cs.SaveChanges();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> Delete(Data.MasterCatalog.FuelSavedFileFormat objFuelSavedFileFormat)
        {
            ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> ret = new ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelSavedFileFormat))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelSavedFileFormat.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.FuelSavedFileFormat).Name, objFuelSavedFileFormat);
                    cs.FuelSavedFileFormat.DeleteObject(objFuelSavedFileFormat);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetLock(Data.MasterCatalog.FuelSavedFileFormat objFuelSavedFileFormat)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.FuelFileXsltParser> IsLastSavedFileFormatSameAsUploadedNewFile(List<string> csvColumnTitles, FuelFileData objFuelFileData)
        {
            ReturnValue<Data.MasterCatalog.FuelFileXsltParser> ret = new ReturnValue<Data.MasterCatalog.FuelFileXsltParser>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (var cs = new Data.MasterCatalog.MasterDataContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        var savedFileFormatList = cs.FuelSavedFileFormat.Where(s => s.FuelVendorID == objFuelFileData.VendorID && s.IsDeleted == false).OrderByDescending(t => t.FuelSavedFileFormatID).ToList();
                        if (savedFileFormatList.Count > 0)
                        {
                            foreach (var fuelSavedFileFormat in savedFileFormatList)
                            {
                                if (fuelSavedFileFormat.FuelFileXsltParserID != null)
                                {
                                    int xsltParserId = fuelSavedFileFormat.FuelFileXsltParserID.Value;
                                    FuelFileXsltParser parser = cs.FuelFileXsltParser.Include("FuelCsvFileFields").FirstOrDefault(x => x.FuelFileXsltParserID == xsltParserId && x.IsInActive==false && x.IsDeleted==false);
                                    if (parser != null)
                                    {
                                        int matchFound = parser.FuelCsvFileFields.Count(f => csvColumnTitles.Contains(f.FieldTitle));
                                        if (matchFound == csvColumnTitles.Count)
                                        {
                                            ret.EntityInfo = parser;
                                            ret.ReturnFlag = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormatebasedOnFuelVendor(long fuelVendorId,long customerId)
        {
            ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> ret = new ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelSavedFileFormat.Include("FuelFileXsltParser").Where(t => t.FuelFileXsltParser.IsDeleted == false && t.FuelVendorID == fuelVendorId && t.CustomerID == customerId).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> GetFuelSavedFileFormatebasedOnFuelVendorCD(string fuelVendorCD, long customerId)
        {
            ReturnValue<Data.MasterCatalog.FuelSavedFileFormat> ret = new ReturnValue<Data.MasterCatalog.FuelSavedFileFormat>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelSavedFileFormat.Include("FuelFileXsltParser").Include("FuelVendor").Where(t => t.FuelFileXsltParser.IsDeleted == false && t.FuelVendor.VendorCD.Trim()==fuelVendorCD.Trim() && t.CustomerID == customerId).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FuelFileXsltParser> MatcheCSVFormatWithErrorLogForamt(List<string> csvColumnTitles, FuelFileData objFuelFileData)
        {
            ReturnValue<Data.MasterCatalog.FuelFileXsltParser> ret = new ReturnValue<Data.MasterCatalog.FuelFileXsltParser>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (var cs = new Data.MasterCatalog.MasterDataContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        FuelFileXsltParser parser = cs.FuelFileXsltParser.Include("FuelCsvFileFields").FirstOrDefault(x => x.XsltFileName.ToLower() == "errorlogupload" && x.IsInActive == false && x.IsDeleted == false);
                        if (parser != null)
                        {
                            int matchFound = parser.FuelCsvFileFields.Count(f => csvColumnTitles.Contains(f.FieldTitle));
                            if (matchFound == csvColumnTitles.Count)
                            {
                                ret.EntityInfo = parser;
                                ret.ReturnFlag = true;
                            }
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        } 
    }
}
