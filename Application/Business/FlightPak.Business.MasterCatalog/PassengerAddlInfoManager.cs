﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class PassengerAddlInfoManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.PassengerAdditionalInfo>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        public ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetList()
        {
            ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPaxGroup.EntityList = objContainer.GetAllPassengerAdditionalInfo(CustomerID, ClientId).ToList();
                    foreach (var item in objPaxGroup.EntityList)
                    {
                        item.AdditionalINFOValue = Crypting.Decrypt(item.AdditionalINFOValue);
                    }
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetListBYRequestorID(Int64 RequestorID)
        {
            ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPaxGroup.EntityList = objContainer.GetAllPassengerAddInfoReqID(CustomerID, ClientId, RequestorID).ToList();
                    foreach (var item in objPaxGroup.EntityList)
                    {
                        item.AdditionalINFOValue = Crypting.Decrypt(item.AdditionalINFOValue);
                    }
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> Add(Data.MasterCatalog.PassengerAdditionalInfo Passenger)
        {
            ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    Passenger.AdditionalINFOValue = Crypting.Encrypt(Passenger.AdditionalINFOValue);
                    objContainer.AddToPassengerAdditionalInfo(Passenger);
                    objContainer.SaveChanges();
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }


        public ReturnValue<Data.MasterCatalog.PassengerInformation> AddforAllPax(Data.MasterCatalog.PassengerInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewRosterAddInfo.ReturnFlag = base.Validate(crewRosterAddInfo, ref objCrewRosterAddInfo.ErrorMessage);
                    //For Data Anotation
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddPaxInformationforYES(crewRosterAddInfo.CustomerID, crewRosterAddInfo.PassengerInfoCD, crewRosterAddInfo.PassengerDescription, crewRosterAddInfo.ClientID, crewRosterAddInfo.LastUpdUID, crewRosterAddInfo.LastUpdTS, crewRosterAddInfo.IsInActive, crewRosterAddInfo.IsDeleted, crewRosterAddInfo.IsCheckList);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }


        public ReturnValue<Data.MasterCatalog.PassengerInformation> UpdateforAllPax(Data.MasterCatalog.PassengerInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewRosterAddInfo.ReturnFlag = base.Validate(crewRosterAddInfo, ref objCrewRosterAddInfo.ErrorMessage);
                    //For Data Anotation
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.UpdatePaxInformationforYES(crewRosterAddInfo.PassengerInformationID, crewRosterAddInfo.CustomerID, crewRosterAddInfo.PassengerInfoCD, crewRosterAddInfo.PassengerDescription, crewRosterAddInfo.ClientID, crewRosterAddInfo.LastUpdUID, crewRosterAddInfo.LastUpdTS, crewRosterAddInfo.IsShowOnTrip, crewRosterAddInfo.IsDeleted, crewRosterAddInfo.IsCheckList);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }


        public ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> Update(Data.MasterCatalog.PassengerAdditionalInfo paxGroup)
        {
            ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    paxGroup.AdditionalINFOValue = Crypting.Encrypt(paxGroup.AdditionalINFOValue);
                    objContainer.PassengerAdditionalInfo.Attach(paxGroup);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerAdditionalInfo>(paxGroup, objContainer);
                    objContainer.SaveChanges();
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> Delete(Data.MasterCatalog.PassengerAdditionalInfo paxGroup)
        {
            ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(paxGroup))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.PassengerAdditionalInfo).Name, paxGroup);
                    objContainer.PassengerAdditionalInfo.DeleteObject(paxGroup);
                    objContainer.SaveChanges();
                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public ReturnValue<Data.MasterCatalog.PassengerAdditionalInfo> GetLock(Data.MasterCatalog.PassengerAdditionalInfo paxGroup)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
