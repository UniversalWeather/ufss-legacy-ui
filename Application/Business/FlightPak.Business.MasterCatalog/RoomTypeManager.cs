﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;



namespace FlightPak.Business.MasterCatalog
{
   public class RoomTypeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.RoomType>, IMasterCatalog.ICacheable
    {

        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       public ReturnValue<Data.MasterCatalog.RoomType> GetList()
        {
            ReturnValue<Data.MasterCatalog.RoomType> RoomType = new ReturnValue<Data.MasterCatalog.RoomType>();

            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //For Looping through child Entities
            Container.ContextOptions.LazyLoadingEnabled = false;
            //For Looping through child Entities
            Container.ContextOptions.ProxyCreationEnabled = false;
            RoomType.EntityList = Container.GetAllRoomType().ToList();
            RoomType.ReturnFlag = true;
            return RoomType;
        }


       public ReturnValue<Data.MasterCatalog.RoomType> Add(Data.MasterCatalog.RoomType roomtype)
       {
           throw new NotImplementedException();
       }

       public ReturnValue<Data.MasterCatalog.RoomType> Update(Data.MasterCatalog.RoomType roomtype)
       {
           throw new NotImplementedException();
       }

       public ReturnValue<Data.MasterCatalog.RoomType> Delete(Data.MasterCatalog.RoomType roomtype)
       {
           throw new NotImplementedException();
       }

       /// <summary>
       /// To implement the lock feature in the country master
       /// </summary>
       /// <param name="CountryType"></param>
       /// <returns></returns>
       public ReturnValue<Data.MasterCatalog.RoomType> GetLock(Data.MasterCatalog.RoomType RoomType)
       {
           throw new NotImplementedException();
       }

       /// <summary>
       /// 
       /// </summary>
       public void RefreshCache()
       {
           throw new NotImplementedException();
       }
    }
}
