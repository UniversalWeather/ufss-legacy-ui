﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewRosterAdditionalInfoManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewInformation>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.CrewInformation> GetList()
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCrewRosterAddInfo.EntityList = objContainer.GetAllCrewInformation(CustomerID).ToList();
                    objCrewRosterAddInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }
        public ReturnValue<Data.MasterCatalog.CrewInformation> Add(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewRosterAddInfo.ReturnFlag = base.Validate(crewRosterAddInfo, ref objCrewRosterAddInfo.ErrorMessage);
                    //For Data Anotation
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToCrewInformation(crewRosterAddInfo);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }


        public ReturnValue<Data.MasterCatalog.CrewInformation> AddforAllCrew(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewRosterAddInfo.ReturnFlag = base.Validate(crewRosterAddInfo, ref objCrewRosterAddInfo.ErrorMessage);
                    //For Data Anotation
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddCrewInforForAllCrew(crewRosterAddInfo.CrewInfoCD, crewRosterAddInfo.CustomerID, crewRosterAddInfo.CrewInformationDescription, crewRosterAddInfo.LastUpdUID, crewRosterAddInfo.LastUpdTS, crewRosterAddInfo.IsDeleted);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }


        public ReturnValue<Data.MasterCatalog.CrewInformation> UpdateforAllCrew(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewRosterAddInfo.ReturnFlag = base.Validate(crewRosterAddInfo, ref objCrewRosterAddInfo.ErrorMessage);
                    //For Data Anotation
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.UpdateforAllCrew(crewRosterAddInfo.CrewInfoID, crewRosterAddInfo.CrewInfoCD, crewRosterAddInfo.CustomerID, crewRosterAddInfo.CrewInformationDescription, crewRosterAddInfo.LastUpdUID, crewRosterAddInfo.LastUpdTS, crewRosterAddInfo.IsDeleted);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }

        public ReturnValue<Data.MasterCatalog.CrewInformation> Update(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objCrewRosterAddInfo.ReturnFlag = base.Validate(crewRosterAddInfo, ref objCrewRosterAddInfo.ErrorMessage);
                    //For Data Anotation
                    if (objCrewRosterAddInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.CrewInformation.Attach(crewRosterAddInfo);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewInformation>(crewRosterAddInfo, objContainer);
                        objContainer.SaveChanges();
                        objCrewRosterAddInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }
        public ReturnValue<Data.MasterCatalog.CrewInformation> Delete(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRosterAddInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.CrewInformation).Name, crewRosterAddInfo);
                    objContainer.CrewInformation.DeleteObject(crewRosterAddInfo);
                    objContainer.SaveChanges();
                    objCrewRosterAddInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }
        public ReturnValue<Data.MasterCatalog.CrewInformation> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.CrewInformation> objCrewRosterAddInfo = new ReturnValue<Data.MasterCatalog.CrewInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objCrewRosterAddInfo.EntityList = objContainer.CrewInformation.ToList();
                    objCrewRosterAddInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewRosterAddInfo;
        }
        public ReturnValue<Data.MasterCatalog.CrewInformation> GetLock(Data.MasterCatalog.CrewInformation crewRosterAddInfo)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
