﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetProfileInformationManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FleetProfileInformation>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FleetProfileInformation> GetList()
        {
            ReturnValue<Data.MasterCatalog.FleetProfileInformation> ret = new ReturnValue<Data.MasterCatalog.FleetProfileInformation>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    FlightPak.Business.MasterCatalog.FleetProfileInformationManager objAcct = new FlightPak.Business.MasterCatalog.FleetProfileInformationManager();

                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.GetAllFleetProfileInformation(CustomerID).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;

        }

        public ReturnValue<Data.MasterCatalog.FleetProfileInformation> Add(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            ReturnValue<Data.MasterCatalog.FleetProfileInformation> ret = new ReturnValue<Data.MasterCatalog.FleetProfileInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfileInformation))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(fleetProfileInformation, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        fleetProfileInformation.CustomerID = CustomerID;
                        fleetProfileInformation.IsDeleted = false;
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.AddToFleetProfileInformation(fleetProfileInformation);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileInformation> Update(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            ReturnValue<Data.MasterCatalog.FleetProfileInformation> ret = new ReturnValue<Data.MasterCatalog.FleetProfileInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfileInformation))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(fleetProfileInformation, ref ret.ErrorMessage);
                    //For Data Anotation
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FleetProfileInformation.Attach(fleetProfileInformation);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetProfileInformation>(fleetProfileInformation, cs);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileInformation> Delete(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            ReturnValue<Data.MasterCatalog.FleetProfileInformation> ret = new ReturnValue<Data.MasterCatalog.FleetProfileInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetProfileInformation))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    fleetProfileInformation.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.FleetProfileInformation).Name, fleetProfileInformation);
                    cs.FleetProfileInformation.DeleteObject(fleetProfileInformation);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ret;
        }

        public ReturnValue<Data.MasterCatalog.FleetProfileInformation> GetLock(Data.MasterCatalog.FleetProfileInformation fleetProfileInformation)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}
