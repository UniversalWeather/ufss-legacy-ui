﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class SiflRateManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FareLevel>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.FareLevel> GetList()
        {

            ReturnValue<Data.MasterCatalog.FareLevel> objSiflRate = new ReturnValue<Data.MasterCatalog.FareLevel>();


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objSiflRate.EntityList = objContainer.GetAllFareLevel(CustomerID).ToList();
                    objSiflRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }


            return objSiflRate;
        }


        public ReturnValue<Data.MasterCatalog.CheckSiflDate> CheckSiflDate(DateTime StartDate, DateTime EndDate)
        {
            ReturnValue<Data.MasterCatalog.CheckSiflDate> Crew = new ReturnValue<Data.MasterCatalog.CheckSiflDate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Crew.EntityList = Container.CheckSiflDate(CustomerID,StartDate,EndDate).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Crew;
        }

        public ReturnValue<Data.MasterCatalog.FareLevel> Add(Data.MasterCatalog.FareLevel siflRate)
        {

            ReturnValue<Data.MasterCatalog.FareLevel> objSiflRate = new ReturnValue<Data.MasterCatalog.FareLevel>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(siflRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objSiflRate.ReturnFlag = base.Validate(siflRate, ref objSiflRate.ErrorMessage);
                    //For Data Anotation
                    if (objSiflRate.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFareLevel(siflRate);
                        objContainer.SaveChanges();
                        objSiflRate.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objSiflRate;
        }

        public ReturnValue<Data.MasterCatalog.FareLevel> Update(Data.MasterCatalog.FareLevel siflRate)
        {

            ReturnValue<Data.MasterCatalog.FareLevel> objSiflRate = new ReturnValue<Data.MasterCatalog.FareLevel>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(siflRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objSiflRate.ReturnFlag = base.Validate(siflRate, ref objSiflRate.ErrorMessage);
                    //For Data Anotation
                    if (objSiflRate.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FareLevel.Attach(siflRate);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FareLevel>(siflRate, objContainer);
                        objContainer.SaveChanges();
                        objSiflRate.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objSiflRate;

        }

        public ReturnValue<Data.MasterCatalog.FareLevel> Delete(Data.MasterCatalog.FareLevel siflRate)
        {
            ReturnValue<Data.MasterCatalog.FareLevel> objSiflRate = new ReturnValue<Data.MasterCatalog.FareLevel>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(siflRate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FareLevel).Name, siflRate);
                    objContainer.FareLevel.DeleteObject(siflRate);
                    objContainer.SaveChanges();
                    objSiflRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objSiflRate;

        }

        public ReturnValue<Data.MasterCatalog.FareLevel> GetListInfo()
        {

            ReturnValue<Data.MasterCatalog.FareLevel> objSiflRate = new ReturnValue<Data.MasterCatalog.FareLevel>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objSiflRate.EntityList = objContainer.FareLevel.ToList();
                    objSiflRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objSiflRate;
        }

        public ReturnValue<Data.MasterCatalog.FareLevel> GetLock(Data.MasterCatalog.FareLevel siflRate)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
