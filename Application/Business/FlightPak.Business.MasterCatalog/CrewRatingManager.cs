﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewRatingManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewRating>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// Hook for implmenting GetList functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewRating> GetList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To insert values from CrewRoster-TypeRating Grid to CrewRating table
        /// </summary>
        /// <param name="crewRating"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewRating> Add(Data.MasterCatalog.CrewRating crewRating)
        {
            ReturnValue<Data.MasterCatalog.CrewRating> CrewRate = new ReturnValue<Data.MasterCatalog.CrewRating>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRating))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToCrewRating(crewRating);
                    Container.SaveChanges();
                    CrewRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewRate;
        }

        /// <summary>
        /// To update values from CrewRoster-TypeRating Grid to CrewRating table
        /// </summary>
        /// <param name="crewRating"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewRating> Update(Data.MasterCatalog.CrewRating crewRating)
        {
            ReturnValue<Data.MasterCatalog.CrewRating> CrewRate = new ReturnValue<Data.MasterCatalog.CrewRating>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRating))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.CrewRating.Attach(crewRating);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewRating>(crewRating, Container);
                    Container.SaveChanges();
                    CrewRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewRate;
        }

        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-TypeRating Grid to CrewRating table
        /// </summary>
        /// <param name="crewRating"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewRating> Delete(Data.MasterCatalog.CrewRating crewRating)
        {
            ReturnValue<Data.MasterCatalog.CrewRating> CrewRate = new ReturnValue<Data.MasterCatalog.CrewRating>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRating))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.CrewRating).Name, crewRating);
                    Container.CrewRating.DeleteObject(crewRating);
                    Container.SaveChanges();
                    CrewRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewRate;
        }

        /// <summary>
        /// To fetch the values from the CrewRating table based on parameters
        /// </summary>
        /// <param name="crewRating"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetCrewRating> GetListInfo(Data.MasterCatalog.GetCrewRating crewRating)
        {
            ReturnValue<Data.MasterCatalog.GetCrewRating> CrewRate = new ReturnValue<Data.MasterCatalog.GetCrewRating>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewRating))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewRate.EntityList = Container.GetCrewRating(CustomerID, crewRating.CrewID, ClientId).ToList();
                    CrewRate.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return CrewRate;
        }

        public ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember> GetTypeRating(Int64 AircraftCD, Int64 CrewID)
        {
            ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember> Crew = new ReturnValue<Data.MasterCatalog.GetFlightLogDataForCrewMember>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AircraftCD, CrewID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Crew.EntityList = Container.GetFlightLogDataForCrewMember(Convert.ToString(AircraftCD), CrewID).ToList();
                    Crew.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return Crew;
        }

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewRating"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewRating> GetLock(Data.MasterCatalog.CrewRating crewRating)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
