﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class FuelVendorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelVendor>, IMasterCatalog.ICacheable
    {
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.FuelVendor> GetList()
        {
            ReturnValue<Data.MasterCatalog.FuelVendor> objFuelVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFuelVendor.EntityList = objContainer.CustomerFuelVendorAccess.Include("FuelVendor").Where(a => a.CustomerID == CustomerID && a.CanAccess==true && a.IsDeleted==false).Select(a=>a.FuelVendor).ToList();
                    objFuelVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
        public ReturnValue<Data.MasterCatalog.FuelVendor> Add(Data.MasterCatalog.FuelVendor FuelVendor)
        {
            ReturnValue<Data.MasterCatalog.FuelVendor> objFuelVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFuelVendor.ReturnFlag = base.Validate(FuelVendor, ref objFuelVendor.ErrorMessage);
                    //For Data Anotation
                    if (objFuelVendor.ReturnFlag)
                    {
                        //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        //objContainer.AddToFuelVendor(FuelVendor);
                        //objContainer.SaveChanges();
                        //objFuelVendor.ReturnFlag = true;
                        using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                        {
                            foreach (Data.MasterCatalog.FlightpakFuel oFlightpakFuel in FuelVendor.FlightpakFuel)
                            {
                                oFlightpakFuel.CustomerID = FuelVendor.CustomerID;
                                oFlightpakFuel.LastUpdUID = FuelVendor.LastUpdUID;
                            }
                            objContainer.FuelVendor.AddObject(FuelVendor);
                            int MaxCommandTimeOut = -1;
                            if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                            {
                                MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                            }
                            objContainer.CommandTimeout = MaxCommandTimeOut;
                            objContainer.SaveChanges();
                            objContainer.CommandTimeout = null;
                            objFuelVendor.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
        public ReturnValue<Data.MasterCatalog.FuelVendor> Update(Data.MasterCatalog.FuelVendor FuelVendor)
        {
            ReturnValue<Data.MasterCatalog.FuelVendor> objFuelVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFuelVendor.ReturnFlag = base.Validate(FuelVendor, ref objFuelVendor.ErrorMessage);
                    //For Data Anotation
                    if (objFuelVendor.ReturnFlag)
                    {
                        using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                        {
                            objContainer.FuelVendor.Attach(FuelVendor);
                            Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelVendor>(FuelVendor, objContainer);                           
                            int MaxCommandTimeOut = -1;
                            if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                            {
                                MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                            }
                            objContainer.CommandTimeout = MaxCommandTimeOut;
                            objContainer.SaveChanges();
                            objContainer.CommandTimeout = null;
                            objFuelVendor.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
        public ReturnValue<Data.MasterCatalog.FuelVendor> Delete(Data.MasterCatalog.FuelVendor FuelVendor)
        {
            ReturnValue<Data.MasterCatalog.FuelVendor> objFuelVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.FuelVendor.Attach(FuelVendor);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelVendor>(FuelVendor, objContainer);
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    objContainer.CommandTimeout = MaxCommandTimeOut;
                    objContainer.SaveChanges();
                    objContainer.CommandTimeout = null;
                    objFuelVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
        public ReturnValue<Data.MasterCatalog.FuelVendor> GetFuelVendorByFuelVendorFilter(long? fuelVendorID, string fuelVendorCode)
        {
            ReturnValue<Data.MasterCatalog.FuelVendor> retVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {                        
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        if (fuelVendorID != null && fuelVendorID != 0 && string.IsNullOrEmpty(fuelVendorCode) == false)
                        {
                            retVendor.EntityInfo = objContainer.FuelVendor.Include("FuelSavedFileFormat").FirstOrDefault(f => f.IsDeleted == false && f.FuelVendorID == fuelVendorID && f.VendorCD.StartsWith(fuelVendorCode));
                        }
                        else if (fuelVendorID != null && fuelVendorID != 0)
                        {
                            retVendor.EntityInfo = objContainer.FuelVendor.Include("FuelSavedFileFormat").FirstOrDefault(f => f.IsDeleted == false && f.FuelVendorID == fuelVendorID);
                        }
                        else if (string.IsNullOrEmpty(fuelVendorCode)==false)
                        {
                            retVendor.EntityInfo = objContainer.FuelVendor.Include("FuelSavedFileFormat").FirstOrDefault(f => f.IsDeleted == false && f.VendorCD.StartsWith(fuelVendorCode));
                        }
                        retVendor.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return retVendor;
        }
        public void DeleteFlightpakFuelData(Data.MasterCatalog.FuelVendor FuelVendor)
        {
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Delete All Related  Fuel Data
                    var flightpakList = objContainer.FlightpakFuel.Where(t => t.FuelVendorID == FuelVendor.FuelVendorID);
                    
                    foreach (var item in flightpakList)
                    {
                        objContainer.FlightpakFuel.DeleteObject(item);
                    }

                    objContainer.SaveChanges();
                    
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }
        public ReturnValue<Data.MasterCatalog.FuelVendor> GetLock(Data.MasterCatalog.FuelVendor paxGroup)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public long AddBulkFuel(Data.MasterCatalog.FuelVendor FuelVendor)
        {
            //ReturnValue<Data.MasterCatalog.FuelVendor> objFuelVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            //objContainer.AddToFuelVendor(FuelVendor);
            //objContainer.SaveChanges();
            //objFuelVendor.ReturnFlag = true;
            //return objFuelVendor;
            //Commented by vishwa
            //foreach (var item in child)
            //{
            //    FuelVendor.FlightpakFuel.Add(item);
            //}
            ////////////////////////////////////////////////////
            ReturnValue<Data.MasterCatalog.FuelVendor> ReturnValue = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FuelVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
                    //                                                            new KeyValuePair<string, object>("FuelVendorID", FuelVendor.FuelVendorID)
                    //                                                            };
                    //FuelVendor.EntityKey = new System.Data.EntityKey("MasterDataContainer.FuelVendor", entityKeyValues);
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.FuelVendor.AddObject(FuelVendor);
                        objContainer.SaveChanges();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return FuelVendor.FuelVendorID;
        }
        public ReturnValue<Data.MasterCatalog.FlightpakFuel> DeleteFlightpakFuel(Data.MasterCatalog.FlightpakFuel FlightpakFuel)
        {
            ReturnValue<Data.MasterCatalog.FlightpakFuel> objFlightpakFuel = new ReturnValue<Data.MasterCatalog.FlightpakFuel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FlightpakFuel))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FlightpakFuel).Name, FlightpakFuel);
                    objContainer.FlightpakFuel.DeleteObject(FlightpakFuel);
                    objContainer.SaveChanges();
                    objFlightpakFuel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFlightpakFuel;
        }
        public ReturnValue<Data.MasterCatalog.GetFuelVendorText> GetFuelVendorText(Int64 FuelVendorID,decimal GallonFrom,decimal GallonTo,Int64 DepartureICAO,string FBOName)
        {
            ReturnValue<Data.MasterCatalog.GetFuelVendorText> objFuelVendor = new ReturnValue<Data.MasterCatalog.GetFuelVendorText>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFuelVendor.EntityList = objContainer.GetFuelVendorText(FuelVendorID, UserPrincipal.Identity.CustomerID, GallonFrom, GallonTo,DepartureICAO,FBOName).ToList();
                    objFuelVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
        public ReturnValue<Data.MasterCatalog.FuelVendor> GetAllFuelVendorList()
        {
            ReturnValue<Data.MasterCatalog.FuelVendor> objFuelVendor = new ReturnValue<Data.MasterCatalog.FuelVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFuelVendor.EntityList = objContainer.FuelVendor.ToList();
                    objFuelVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
    }
}
