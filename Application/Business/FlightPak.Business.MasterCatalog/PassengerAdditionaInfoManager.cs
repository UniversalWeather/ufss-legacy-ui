﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class PassengerAdditionalInfoManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.PassengerInformation>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.PassengerInformation> GetList()
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objPassengerAdditionalInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPassengerAdditionalInfo.EntityList = objContainer.GetAllPassengerInformation(CustomerID,ClientId).ToList();
                    objPassengerAdditionalInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPassengerAdditionalInfo;
        }
        public ReturnValue<Data.MasterCatalog.PassengerInformation> Add(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objPassengerAdditionalInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objPassengerAdditionalInfo.ReturnFlag = base.Validate(passengerAdditionalInfo, ref objPassengerAdditionalInfo.ErrorMessage);
                    //For Data Anotation
                    if (objPassengerAdditionalInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToPassengerInformation(passengerAdditionalInfo);
                        objContainer.SaveChanges();
                        objPassengerAdditionalInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPassengerAdditionalInfo;
        }
        public ReturnValue<Data.MasterCatalog.PassengerInformation> Update(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objPassengerAdditionalInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objPassengerAdditionalInfo.ReturnFlag = base.Validate(passengerAdditionalInfo, ref objPassengerAdditionalInfo.ErrorMessage);
                    //For Data Anotation
                    if (objPassengerAdditionalInfo.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.PassengerInformation.Attach(passengerAdditionalInfo);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerInformation>(passengerAdditionalInfo, objContainer);
                        objContainer.SaveChanges();
                        objPassengerAdditionalInfo.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPassengerAdditionalInfo;
        }
        public ReturnValue<Data.MasterCatalog.PassengerInformation> Delete(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objPassengerAdditionalInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(passengerAdditionalInfo))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.PassengerInformation).Name, passengerAdditionalInfo);
                    objContainer.PassengerInformation.DeleteObject(passengerAdditionalInfo);
                    objContainer.SaveChanges();
                    objPassengerAdditionalInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPassengerAdditionalInfo;
        }
        public ReturnValue<Data.MasterCatalog.PassengerInformation> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.PassengerInformation> objPassengerAdditionalInfo = new ReturnValue<Data.MasterCatalog.PassengerInformation>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objPassengerAdditionalInfo.EntityList = objContainer.PassengerInformation.ToList();
                    objPassengerAdditionalInfo.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPassengerAdditionalInfo;
        }
        public ReturnValue<Data.MasterCatalog.PassengerInformation> GetLock(Data.MasterCatalog.PassengerInformation passengerAdditionalInfo)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
