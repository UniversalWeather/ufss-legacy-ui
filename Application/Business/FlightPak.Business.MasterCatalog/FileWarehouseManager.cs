﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
 
namespace FlightPak.Business.MasterCatalog
{
    public class FileWarehouseManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FileWarehouse>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FileWarehouse> Add(Data.MasterCatalog.FileWarehouse FWHType)
        {
            ReturnValue<Data.MasterCatalog.FileWarehouse> objFWHType = new ReturnValue<Data.MasterCatalog.FileWarehouse>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    FWHType.CustomerID = CustomerID;
                    objContainer.AddToFileWarehouse(FWHType);
                    objContainer.SaveChanges();
                    objFWHType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFWHType;
        }

        public ReturnValue<Data.MasterCatalog.FileWarehouse> Update(Data.MasterCatalog.FileWarehouse FWHType)
        {
            ReturnValue<Data.MasterCatalog.FileWarehouse> objFWHType = new ReturnValue<Data.MasterCatalog.FileWarehouse>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.FileWarehouse.Attach(FWHType);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FileWarehouse>(FWHType, objContainer);
                    objContainer.SaveChanges();

                    objFWHType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFWHType;
        }

        public ReturnValue<Data.MasterCatalog.FileWarehouse> Delete(Data.MasterCatalog.FileWarehouse FWHType)
        {
            ReturnValue<Data.MasterCatalog.FileWarehouse> objFWHType = new ReturnValue<Data.MasterCatalog.FileWarehouse>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FileWarehouse).Name, FWHType);
                    objContainer.FileWarehouse.DeleteObject(FWHType);
                    objContainer.SaveChanges();
                    objFWHType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFWHType;
        }


        public ReturnValue<Data.MasterCatalog.FileWarehouse> GetFWHList(string RecordType, Int64 RecordID)
        {
            ReturnValue<Data.MasterCatalog.FileWarehouse> objFleetType = new ReturnValue<Data.MasterCatalog.FileWarehouse>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetType.EntityList = objContainer.GetAllFileWarehouse(CustomerID, RecordType, RecordID).ToList();
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetType;
        }
        public ReturnValue<Data.MasterCatalog.FileWarehouse> GetList()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.FileWarehouse> GetLock(Data.MasterCatalog.FileWarehouse FWHType)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}

