﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class DepartmentAuthorizationManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.DepartmentAuthorization>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetDepartmentAuthorizationWithFilters(long ClientId, long DepartmentId, long LongAuthorizationId, string AuthorizationCD, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = new ReturnValue<Data.MasterCatalog.DepartmentAuthorization>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetDepartmentAuthorizationWithFilters(CustomerID, ClientId, DepartmentId, LongAuthorizationId, AuthorizationCD, FetchActiveOnly).ToList();
                    //if (ReturnValue.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < ReturnValue.EntityList.Count; i++)
                    //    {
                    //        ReturnValue.EntityList[i].AuthorizationCD = ReturnValue.EntityList[i].AuthorizationCD != null ? ReturnValue.EntityList[i].AuthorizationCD.Trim() : string.Empty;
                    //    }
                    //}
                    ReturnValue.ReturnFlag = true;
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
        #endregion
        /// <summary>
        /// To get the values into DepartmentAuthorization page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetList()
        {
            ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = new ReturnValue<Data.MasterCatalog.DepartmentAuthorization>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllDepartmentAuthorization(CustomerID,ClientId).ToList();
                    //if (ReturnValue.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < ReturnValue.EntityList.Count; i++)
                    //    {
                    //        ReturnValue.EntityList[i].AuthorizationCD = ReturnValue.EntityList[i].AuthorizationCD != null ? ReturnValue.EntityList[i].AuthorizationCD.Trim() : string.Empty;
                    //    }
                    //}
                    ReturnValue.ReturnFlag = true;
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To insert values from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentAuthorization> Add(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorization)
        {
            ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = new ReturnValue<Data.MasterCatalog.DepartmentAuthorization>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(DepartmentAuthorization, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.AddToDepartmentAuthorization(DepartmentAuthorization);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                       // return ReturnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To update the selected from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentAuthorization> Update(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorization)
        {
            ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = new ReturnValue<Data.MasterCatalog.DepartmentAuthorization>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorization))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(DepartmentAuthorization, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.DepartmentAuthorization.Attach(DepartmentAuthorization);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.DepartmentAuthorization>(DepartmentAuthorization, Container);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                        //return ReturnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// To update the selected record as deleted, from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentAuthorization> Delete(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorization)
        {
            ReturnValue<Data.MasterCatalog.DepartmentAuthorization> ReturnValue = new ReturnValue<Data.MasterCatalog.DepartmentAuthorization>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DepartmentAuthorization))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.DepartmentAuthorization).Name, DepartmentAuthorization);
                    Container.DepartmentAuthorization.DeleteObject(DepartmentAuthorization);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.DepartmentAuthorization> GetLock(Data.MasterCatalog.DepartmentAuthorization DepartmentAuthorization)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values into DepartmentAuthorization page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllDeptAuth> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllDeptAuth>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllDeptAuth(CustomerID,ClientId).ToList();
                    ReturnValue.ReturnFlag = true;
                    return ReturnValue;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return ReturnValue;
            }
        }
    }
}
