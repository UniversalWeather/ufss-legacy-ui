﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;

namespace FlightPak.Business.MasterCatalog
{
    public class CountryManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Country>, IMasterCatalog.ICacheable
    {
        #region For Performance Optimization
        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public ReturnValue<Data.MasterCatalog.Country> GetCountryWithFilters(string CountryCD, long CountryId)
        {
            ReturnValue<Data.MasterCatalog.Country> Country = new ReturnValue<Data.MasterCatalog.Country>();

            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //For Looping through child Entities
            Container.ContextOptions.LazyLoadingEnabled = false;
            //For Looping through child Entities
            Container.ContextOptions.ProxyCreationEnabled = false;
            Country.EntityList = Container.GetCountryWithFilters(CountryCD, CountryId).ToList();
            Country.ReturnFlag = true;
            return Country;
        }

        #endregion
        /// <summary>
        /// To Get the values from the country Master
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public ReturnValue<Data.MasterCatalog.Country> GetList()
        {
            ReturnValue<Data.MasterCatalog.Country> Country = new ReturnValue<Data.MasterCatalog.Country>();

            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //For Looping through child Entities
            Container.ContextOptions.LazyLoadingEnabled = false;
            //For Looping through child Entities
            Container.ContextOptions.ProxyCreationEnabled = false;
            Country.EntityList = Container.GetAllCountry().ToList();
            Country.ReturnFlag = true;
            return Country;
        }

        /// <summary>
        /// To add values into the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Country> Add(Data.MasterCatalog.Country CountryType)
        {
            ReturnValue<Data.MasterCatalog.Country> Country = new ReturnValue<Data.MasterCatalog.Country>();

            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            objContainer.AddToCountry(CountryType);
            objContainer.SaveChanges();
            Country.ReturnFlag = true;
            return Country;
        }

        /// <summary>
        /// To Update values to the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Country> Update(Data.MasterCatalog.Country CountryType)
        {
            ReturnValue<Data.MasterCatalog.Country> Country = new ReturnValue<Data.MasterCatalog.Country>();
            
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            Container.Country.Attach(CountryType);
            Common.EMCommon.SetAllModified<Data.MasterCatalog.Country>(CountryType, Container);
            Container.SaveChanges();
            Country.ReturnFlag = true;
            return Country;
        }

        /// <summary>
        /// To delete from the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Country> Delete(Data.MasterCatalog.Country CountryType)
        {
            ReturnValue<Data.MasterCatalog.Country> Country = new ReturnValue<Data.MasterCatalog.Country>();
            
            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            Container.AttachTo(typeof(Data.MasterCatalog.Country).Name, CountryType);
            Container.Country.DeleteObject(CountryType);
            Container.SaveChanges();
            Country.ReturnFlag = true;
            return Country;
        }

        /// <summary>
        /// To implement the lock feature in the country master
        /// </summary>
        /// <param name="CountryType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Country> GetLock(Data.MasterCatalog.Country CountryType)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
