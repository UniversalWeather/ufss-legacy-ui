﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;


namespace FlightPak.Business.MasterCatalog
{
    public class ClientCodeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Client>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.Client> GetClientWithFilters(Int64 ClientID, string ClientCD, bool IsActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.Client> objClientCode = new ReturnValue<Data.MasterCatalog.Client>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objClientCode.EntityList = objContainer.GetClientWithFilters(CustomerID, ClientID, ClientCD, IsActiveOnly).ToList();
                    //if (objClientCode.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objClientCode.EntityList.Count; i++)
                    //    {
                    //        objClientCode.EntityList[i].ClientCD = objClientCode.EntityList[i].ClientCD != null ? objClientCode.EntityList[i].ClientCD.Trim() : string.Empty;
                    //    }
                    //}
                    objClientCode.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objClientCode;
        }
        #endregion
        public ReturnValue<Data.MasterCatalog.Client> GetList()
        {
            ReturnValue<Data.MasterCatalog.Client> objClientCode = new ReturnValue<Data.MasterCatalog.Client>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objClientCode.EntityList = objContainer.GetAllClient(CustomerID).ToList();
                    //if (objClientCode.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objClientCode.EntityList.Count; i++)
                    //    {
                    //        objClientCode.EntityList[i].ClientCD = objClientCode.EntityList[i].ClientCD != null ? objClientCode.EntityList[i].ClientCD.Trim() : string.Empty;
                    //    }
                    //}
                    objClientCode.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objClientCode;
        }
        public ReturnValue<Data.MasterCatalog.Client> Add(Data.MasterCatalog.Client Client)
        {

            ReturnValue<Data.MasterCatalog.Client> ret = new ReturnValue<Data.MasterCatalog.Client>();
            //IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
            //                                                            new KeyValuePair<string, object>("ClientID", Client.ClientID)
            //                                                            };
            //Client.EntityKey = new System.Data.EntityKey("MasterDataContainer.Client", entityKeyValues);
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Client))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {                    
                     //For Data Anotation
                     ret.ReturnFlag = base.Validate(Client, ref ret.ErrorMessage);
                     //For Data Anotation
                     if (ret.ReturnFlag)
                     {
                         using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                         {
                             objContainer.Client.AddObject(Client);
                             objContainer.ContextOptions.LazyLoadingEnabled = false;
                             objContainer.ContextOptions.ProxyCreationEnabled = false;
                             objContainer.SaveChanges();
                             ret.EntityInfo = Client;
                             ret.ReturnFlag = true;
                         }
                     }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;

        }
        public ReturnValue<Data.MasterCatalog.Client> Update(Data.MasterCatalog.Client Client)
        {

            ReturnValue<Data.MasterCatalog.Client> ret = new ReturnValue<Data.MasterCatalog.Client>();
            //IEnumerable<KeyValuePair<string, object>> entityKeyValues = new KeyValuePair<string, object>[] {
            //                                                            new KeyValuePair<string, object>("ClientID", Client.ClientID)
            //                                                            };
            //Client.EntityKey = new System.Data.EntityKey("MasterDataContainer.Client", entityKeyValues);
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Client))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    //For Data Anotation
                     ret.ReturnFlag = base.Validate(Client, ref ret.ErrorMessage);
                     //For Data Anotation
                     if (ret.ReturnFlag)
                     {
                         using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                         {
                             objContainer.Client.Attach(Client);
                             objContainer.ObjectStateManager.ChangeObjectState(Client, System.Data.EntityState.Modified);
                             //foreach (Data.MasterCatalog.UserMaster UserMasterList in Client.UserMaster)
                             //{
                             //    //if (UserMasterList.LastUpdTS == null)
                             //    //{
                             //    //    objContainer.ObjectStateManager.ChangeObjectState(UserMasterList, System.Data.EntityState.Added);
                             //    //    objContainer.UserMaster.AddObject(UserMasterList);
                             //    //}
                             //    //else
                             //    //{
                             //        objContainer.UserMaster.Attach(UserMasterList);
                             //        Common.EMCommon.SetAllModified<Data.MasterCatalog.UserMaster>(UserMasterList, objContainer);
                             //    //}
                             //}
                             //foreach (Data.MasterCatalog.Fleet FleetList in Client.Fleet)
                             //{
                             //    //if (FleetList.LastUpdTS == null)
                             //    //{
                             //    //    objContainer.ObjectStateManager.ChangeObjectState(FleetList, System.Data.EntityState.Added);
                             //    //    objContainer.Fleet.AddObject(FleetList);
                             //    //}
                             //    //else
                             //    //{
                             //        objContainer.Fleet.Attach(FleetList);
                             //        Common.EMCommon.SetAllModified<Data.MasterCatalog.Fleet>(FleetList, objContainer);
                             //    //}
                             //}
                             //foreach (Data.MasterCatalog.CrewCheckList CrewCheckList in Client.CrewCheckList)
                             //{
                             //    //if (CrewCheckList.LastUpdTS == null)
                             //    //{
                             //    //    objContainer.ObjectStateManager.ChangeObjectState(CrewCheckList, System.Data.EntityState.Added);
                             //    //    objContainer.CrewCheckList.AddObject(CrewCheckList);
                             //    //}
                             //    //else
                             //    //{
                             //        objContainer.CrewCheckList.Attach(CrewCheckList);
                             //        Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewCheckList>(CrewCheckList, objContainer);
                             //    //}
                             //}
                             //foreach (Data.MasterCatalog.Passenger PassengerList in Client.Passenger)
                             //{
                             //    //if (PassengerList.LastUpdTS == null)
                             //    //{
                             //    //    objContainer.ObjectStateManager.ChangeObjectState(PassengerList, System.Data.EntityState.Added);
                             //    //    objContainer.Passenger.AddObject(PassengerList);
                             //    //}
                             //    //else
                             //    //{
                             //        objContainer.Passenger.Attach(PassengerList);
                             //        Common.EMCommon.SetAllModified<Data.MasterCatalog.Passenger>(PassengerList, objContainer);
                             //    //}
                             //}
                             //foreach (Data.MasterCatalog.Department DepartmentList in Client.Department)
                             //{
                             //    //if (DepartmentList.LastUpdTS == null)
                             //    //{
                             //    //    objContainer.ObjectStateManager.ChangeObjectState(DepartmentList, System.Data.EntityState.Added);
                             //    //    objContainer.Department.AddObject(DepartmentList);
                             //    //}
                             //    //else
                             //    //{
                             //        objContainer.Department.Attach(DepartmentList);
                             //        Common.EMCommon.SetAllModified<Data.MasterCatalog.Department>(DepartmentList, objContainer);
                             //    //}
                             //}
                             objContainer.SaveChanges();
                             // Once the user is updated, we should clear all the objects in cache.
                             var cacheManager = new CacheManager();
                             cacheManager.RemoveAll();
                             ret.ReturnFlag = true;
                         }
                     }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;

        }

        public ReturnValue<Data.MasterCatalog.Client> UpdateAll(Int64 ClientID, string UserMaster, string Fleet, string Crew, string Passenger, string Department, string FlightCategory, string FlightPurpose, string FuelLocator)
        {
            ReturnValue<Data.MasterCatalog.Client> ReturnValue = new ReturnValue<Data.MasterCatalog.Client>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ClientID, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.UpdateClientID(ClientID, UserMaster, Fleet, Crew, Passenger, Department, FlightCategory, FlightPurpose, FuelLocator);
                    // Once the user is updated, we should clear all the objects in cache.
                    var cacheManager = new CacheManager();
                    cacheManager.RemoveAll();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.Client> Delete(Data.MasterCatalog.Client clientCode)
        {
            ReturnValue<Data.MasterCatalog.Client> objClientCode = new ReturnValue<Data.MasterCatalog.Client>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(clientCode))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Client).Name, clientCode);
                    objContainer.Client.DeleteObject(clientCode);
                    objContainer.SaveChanges();
                    // Once the user is updated, we should clear all the objects in cache.
                    var cacheManager = new CacheManager();
                    cacheManager.RemoveAll();
                    objClientCode.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objClientCode;
        }
        public ReturnValue<Data.MasterCatalog.Client> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.Client> objClientCode = new ReturnValue<Data.MasterCatalog.Client>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objClientCode.EntityList = objContainer.Client.ToList();
                    objClientCode.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objClientCode;
        }
        public ReturnValue<Data.MasterCatalog.Client> GetLock(Data.MasterCatalog.Client clientCode)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Added by Scheduling calendar team to get Client Entity by client Code.
        /// </summary>
        /// <param name="clientCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.ClientByClientCDResult> GetClientByClientCD(string clientCode)
        {
            ReturnValue<Data.MasterCatalog.ClientByClientCDResult> client = new ReturnValue<Data.MasterCatalog.ClientByClientCDResult>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods through exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    client.EntityList = Container.GetClientByClientCD(CustomerID, clientCode).ToList();
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return client;
        }
    }
}
