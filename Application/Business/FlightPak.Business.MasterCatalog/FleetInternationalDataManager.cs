﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetInternationalDataManager : BaseManager, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FleetPair> Add(Data.MasterCatalog.FleetPair fleetinternationaltype)
        {
            ReturnValue<Data.MasterCatalog.FleetPair> objfleetInternationalType = new ReturnValue<Data.MasterCatalog.FleetPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinternationaltype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {


                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToFleetPair(fleetinternationaltype);
                    objContainer.SaveChanges();
                    objfleetInternationalType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objfleetInternationalType;
        }

        public ReturnValue<Data.MasterCatalog.FleetPair> Update(Data.MasterCatalog.FleetPair fleetinternationaltype)
        {
            ReturnValue<Data.MasterCatalog.FleetPair> objfleetInternationalType = new ReturnValue<Data.MasterCatalog.FleetPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinternationaltype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.FleetPair.Attach(fleetinternationaltype);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetPair>(fleetinternationaltype, objContainer);
                    objContainer.SaveChanges();

                    objfleetInternationalType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objfleetInternationalType;
        }

        public ReturnValue<Data.MasterCatalog.FleetPair> Delete(Data.MasterCatalog.FleetPair fleetinternationaltype)
        {
            ReturnValue<Data.MasterCatalog.FleetPair> objFleetType = new ReturnValue<Data.MasterCatalog.FleetPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleetinternationaltype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FleetPair).Name, fleetinternationaltype);
                    objContainer.FleetPair.DeleteObject(fleetinternationaltype);
                    objContainer.SaveChanges();
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetType;
        }


        public ReturnValue<Data.MasterCatalog.GetFleetPair> GetFleetPair(Int64 FleetID)
        {
            ReturnValue<Data.MasterCatalog.GetFleetPair> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleetPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleetPair(CustomerID, FleetID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetPair> GetFleetPairList(Int64 FleetID)
        {
            throw new NotImplementedException();
        }


        public ReturnValue<Data.MasterCatalog.FleetPair> GetLock(Data.MasterCatalog.FleetPair fleetinternationaltype)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.FleetPair> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
