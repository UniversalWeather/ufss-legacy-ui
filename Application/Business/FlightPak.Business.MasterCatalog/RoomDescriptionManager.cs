﻿using FlightPak.Business.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlightPak.Business.MasterCatalog
{
    public class RoomDescriptionManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.RoomDescription>, IMasterCatalog.ICacheable
    {
        public ReturnValue<Data.MasterCatalog.RoomDescription> GetList()
        {
            ReturnValue<Data.MasterCatalog.RoomDescription> RoomDescription = new ReturnValue<Data.MasterCatalog.RoomDescription>();

            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
            //For Looping through child Entities
            Container.ContextOptions.LazyLoadingEnabled = false;
            //For Looping through child Entities
            Container.ContextOptions.ProxyCreationEnabled = false;
            RoomDescription.EntityList = Container.GetAllRoomDescription().ToList();
            RoomDescription.ReturnFlag = true;
            return RoomDescription;
        }

        public ReturnValue<Data.MasterCatalog.RoomDescription> Add(Data.MasterCatalog.RoomDescription t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.RoomDescription> Update(Data.MasterCatalog.RoomDescription t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.RoomDescription> Delete(Data.MasterCatalog.RoomDescription t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.RoomDescription> GetLock(Data.MasterCatalog.RoomDescription t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
