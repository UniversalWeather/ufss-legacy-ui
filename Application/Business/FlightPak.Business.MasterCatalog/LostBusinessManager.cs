﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class LostBusinessManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CQLostBusiness>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for Delaytype page from DelayType table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CQLostBusiness> GetList()
        {

            ReturnValue<Data.MasterCatalog.CQLostBusiness> objLostBusiness = new ReturnValue<Data.MasterCatalog.CQLostBusiness>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objLostBusiness.EntityList = objContainer.GetAllLostBusiness(CustomerID).ToList();
                    objLostBusiness.ReturnFlag = true;
                    return objLostBusiness;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLostBusiness;
            }

        }
        /// <summary>
        /// To insert values from Delaytype page to Delaytype table
        /// </summary>
        /// <param name="lostBusiness"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CQLostBusiness> Add(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {
            ReturnValue<Data.MasterCatalog.CQLostBusiness> objLostBusiness = new ReturnValue<Data.MasterCatalog.CQLostBusiness>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lostBusiness))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                     //For Data Anotation
                    objLostBusiness.ReturnFlag = base.Validate(lostBusiness, ref objLostBusiness.ErrorMessage);
                     //For Data Anotation
                    if (objLostBusiness.ReturnFlag)
                     {
                         Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                         objContainer.AddToCQLostBusiness(lostBusiness);
                         objContainer.SaveChanges();
                         objLostBusiness.ReturnFlag = true;
                         //return objDelayType;
                     }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLostBusiness;
            }

        }
        /// <summary>
        /// To update values from Delaytype page to Delaytype table
        /// </summary>
        /// <param name="lostBusiness"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CQLostBusiness> Update(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {

            ReturnValue<Data.MasterCatalog.CQLostBusiness> objLostBusiness = new ReturnValue<Data.MasterCatalog.CQLostBusiness>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objLostBusiness.ReturnFlag = base.Validate(lostBusiness, ref objLostBusiness.ErrorMessage);
                    //For Data Anotation
                    if (objLostBusiness.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.CQLostBusiness.Attach(lostBusiness);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CQLostBusiness>(lostBusiness, objContainer);
                        objContainer.SaveChanges();
                        objLostBusiness.ReturnFlag = true;
                        //return objDelayType;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLostBusiness;
            }

        }
        /// <summary>
        /// To update the selected record as deleted, from Delaytype page in Delaytype table
        /// </summary>
        /// <param name="lostBusiness"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CQLostBusiness> Delete(Data.MasterCatalog.CQLostBusiness lostBusiness)
        {


            ReturnValue<Data.MasterCatalog.CQLostBusiness> objLostBusiness = new ReturnValue<Data.MasterCatalog.CQLostBusiness>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(lostBusiness))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.CQLostBusiness).Name, lostBusiness);
                    objContainer.CQLostBusiness.DeleteObject(lostBusiness);
                    objContainer.SaveChanges();
                    objLostBusiness.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objLostBusiness;
        }
        /// <summary>
        /// To get the values for DelayType page from DelayType table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CQLostBusiness> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.CQLostBusiness> objLostBusiness = new ReturnValue<Data.MasterCatalog.CQLostBusiness>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objLostBusiness.EntityList = objContainer.CQLostBusiness.ToList();
                    objLostBusiness.ReturnFlag = true;
                    return objLostBusiness;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objLostBusiness;
            }

        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="delayType"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CQLostBusiness> GetLock(Data.MasterCatalog.CQLostBusiness delayType)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}






