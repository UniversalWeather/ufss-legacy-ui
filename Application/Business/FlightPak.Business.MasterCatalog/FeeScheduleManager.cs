﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FeeScheduleManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FeeSchedule>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.MasterCatalog.FeeSchedule> Add(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            ReturnValue<Data.MasterCatalog.FeeSchedule> ReturnValue = new ReturnValue<Data.MasterCatalog.FeeSchedule>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.FeeSchedule.AddObject(oFeeSchedule);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oFeeSchedule;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FeeSchedule> Update(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            ReturnValue<Data.MasterCatalog.FeeSchedule> ReturnValue = new ReturnValue<Data.MasterCatalog.FeeSchedule>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.FeeSchedule.Attach(oFeeSchedule);
                        objContainer.ObjectStateManager.ChangeObjectState(oFeeSchedule, System.Data.EntityState.Modified);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FeeSchedule> Delete(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            ReturnValue<Data.MasterCatalog.FeeSchedule> ReturnValue = new ReturnValue<Data.MasterCatalog.FeeSchedule>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oFeeSchedule))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.FeeSchedule.Attach(oFeeSchedule);
                        objContainer.FeeSchedule.DeleteObject(oFeeSchedule);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FeeSchedule> GetList()
        {
            ReturnValue<Data.MasterCatalog.FeeSchedule> ReturnValue = new ReturnValue<Data.MasterCatalog.FeeSchedule>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FeeSchedule> GetLock(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            ReturnValue<Data.MasterCatalog.FeeSchedule> ReturnValue = new ReturnValue<Data.MasterCatalog.FeeSchedule>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetFeeSchedule> GetFeeSchedule(Data.MasterCatalog.FeeSchedule oFeeSchedule)
        {
            ReturnValue<Data.MasterCatalog.GetFeeSchedule> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFeeSchedule>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        ReturnValue.EntityList = objContainer.GetFeeSchedule(oFeeSchedule.FeeScheduleID, CustomerID, oFeeSchedule.IsInActive, oFeeSchedule.IsDeleted).ToList();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// Method to Get List of Fee Schedule by Group ID
        /// </summary>
        /// <param name="feeGroupID">Pass Fee Group ID</param>
        /// <returns>Returns Fee Schedule List</returns>
        public ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID> GetFeeScheduleByGroupID(Int64 feeGroupID)
        {
            ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFeeScheduleByGroupID>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(feeGroupID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        ReturnValue.EntityList = objContainer.GetAllFeeScheduleByGroupID(CustomerID, feeGroupID).ToList();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

    }
}
