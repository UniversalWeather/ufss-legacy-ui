﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Fleet>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters> GetAllFleetWithFilters(long ClientID, bool FetchActiveOnly, string IsFixedRotary, string ICAOID, long VendorID, string TailNum, long FleetID)
        {
            ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFleetWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllFleetWithFilters(CustomerID, ClientID, FetchActiveOnly, IsFixedRotary, ICAOID, VendorID, TailNum, FleetID).ToList();
                    ReturnValue.ReturnFlag = true;

                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        #endregion

        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        public ReturnValue<Data.MasterCatalog.Fleet> GetList()
        {
            ReturnValue<Data.MasterCatalog.Fleet> objFleetType = new ReturnValue<Data.MasterCatalog.Fleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                   
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetType.EntityList = objContainer.GetAllFleet(CustomerID,ClientId).ToList();
                    //Commented by Ramesh incorporated this check in SpGetAllFleet for Veracode fix
                    //if (objFleetType.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objFleetType.EntityList.Count; i++)
                    //    {
                    //        objFleetType.EntityList[i].TailNum = objFleetType.EntityList[i].TailNum != null ? objFleetType.EntityList[i].TailNum.Trim() : string.Empty;
                    //    }
                    //}
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.GetAllFleetForPopup> GetAllFleetForPopupList()
        {
            ReturnValue<Data.MasterCatalog.GetAllFleetForPopup> objFleetType = new ReturnValue<Data.MasterCatalog.GetAllFleetForPopup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                   
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleetType.EntityList = objContainer.GetAllFleetForPopup(CustomerID, ClientId).ToList();
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.Fleet> Add(Data.MasterCatalog.Fleet fleettype)
        {
            ReturnValue<Data.MasterCatalog.Fleet> objFleetType = new ReturnValue<Data.MasterCatalog.Fleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleettype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objFleetType.ReturnFlag = base.Validate(fleettype, ref objFleetType.ErrorMessage);
                    //For Data Anotation
                    if (objFleetType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        foreach (Data.MasterCatalog.FleetProfileDefinition ofleetDefinition in fleettype.FleetProfileDefinition)
                        {
                            ofleetDefinition.CustomerID = fleettype.CustomerID;
                            ofleetDefinition.ClientID = fleettype.ClientID;
                            ofleetDefinition.LastUpdUID = fleettype.LastUpdUID;
                            //ofleetDefinition.InformationValue = Crypting.Encrypt(ofleetDefinition.InformationValue);
                        }

                        objContainer.Fleet.AddObject(fleettype);
                        objContainer.SaveChanges();
                        objFleetType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.Fleet> Update(Data.MasterCatalog.Fleet fleettype)
        {
            ReturnValue<Data.MasterCatalog.Fleet> objFleetType = new ReturnValue<Data.MasterCatalog.Fleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleettype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                   
                   //For Data Anotation
                    objFleetType.ReturnFlag = base.Validate(fleettype, ref objFleetType.ErrorMessage);
                    //For Data Anotation
                    if (objFleetType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        foreach (Data.MasterCatalog.FleetProfileDefinition oFleetDefinition in fleettype.FleetProfileDefinition)
                        {
                            oFleetDefinition.CustomerID = fleettype.CustomerID;
                            oFleetDefinition.ClientID = fleettype.ClientID;
                            oFleetDefinition.LastUpdUID = fleettype.LastUpdUID;
                            //oFleetDefinition.InformationValue = Crypting.Encrypt(oFleetDefinition.InformationValue);
                        }
                        objContainer.Fleet.Attach(fleettype);
                        objContainer.ObjectStateManager.ChangeObjectState(fleettype, System.Data.EntityState.Modified);
                        foreach (Data.MasterCatalog.FleetProfileDefinition oFleetDefinition in fleettype.FleetProfileDefinition)
                        {
                            //oFleetDefinition.CustomerID = fleettype.CustomerID;
                            //oFleetDefinition.ClientID = fleettype.ClientID;
                            //oFleetDefinition.LastUpdUID = fleettype.LastUpdUID;
                            //oFleetDefinition.InformationValue = Crypting.Encrypt(oFleetDefinition.InformationValue);
                            if (oFleetDefinition.FleetProfileInfoXRefID < 0)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(oFleetDefinition, System.Data.EntityState.Added);
                                objContainer.FleetProfileDefinition.AddObject(oFleetDefinition);
                            }
                            else
                            {
                                Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetProfileDefinition>(oFleetDefinition, objContainer);
                            }
                        }
                        objContainer.SaveChanges();

                        objFleetType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.Fleet> Delete(Data.MasterCatalog.Fleet fleettype)
        {

            ReturnValue<Data.MasterCatalog.Fleet> objFleetType = new ReturnValue<Data.MasterCatalog.Fleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fleettype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Fleet).Name, fleettype);
                    objContainer.Fleet.DeleteObject(fleettype);
                    objContainer.SaveChanges();
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.Fleet> GetLock(Data.MasterCatalog.Fleet fleettype)
        {
            throw new NotImplementedException();
        }

       

        public ReturnValue<Data.MasterCatalog.GetFleet> GetFleet()
        {
            ReturnValue<Data.MasterCatalog.GetFleet> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleet(CustomerID,ClientId).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetFleetID> GetFleetID(string TailNum, string SerialNum)
        {
            ReturnValue<Data.MasterCatalog.GetFleetID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleetID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleetID(CustomerID, TailNum, SerialNum).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetFleetIDInfo> GetFleetIDInfo(Int64 FleetID)
        {
            ReturnValue<Data.MasterCatalog.GetFleetIDInfo> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleetIDInfo>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleetIDInfo(FleetID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.GetFleetSearch> GetFleetSearch(bool IsInActive, string IsFixedRotary, Int64 HomeBaseID)
        {
            ReturnValue<Data.MasterCatalog.GetFleetSearch> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleetSearch>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleetSearch(CustomerID, IsInActive, IsFixedRotary, ClientId, HomeBaseID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        #region "Get Fleet for Client"
        public ReturnValue<Data.MasterCatalog.GetFleetForClient> GetFleetForClient()
        {
            ReturnValue<Data.MasterCatalog.GetFleetForClient> objFleet = new ReturnValue<Data.MasterCatalog.GetFleetForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFleet.EntityList = objContainer.GetFleetForClient(CustomerID, ClientId).ToList();
                    objFleet.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFleet;
        }
        #endregion

        public Int64? GetAircraftUsedCount()
        {
            Int64? AircraftUsedCount = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    AircraftUsedCount = objContainer.GetAircraftUsedCount(CustomerID).FirstOrDefault();
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return AircraftUsedCount;
            }
        }
        public Int64? GetAircraftActualCount()
        {
            Int64? AircraftActualCount = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    AircraftActualCount = objContainer.GetAircraftActualCount(CustomerID).FirstOrDefault();
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return AircraftActualCount;
            }
        }
    }
}

