﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class FuelLocatorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelLocator>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// To get the values for FuelLocator page from FuelLocator table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters> GetFuelLocatorWithFilters(long FuelLocatorId, string FuelLocatorCD, long ClientId, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters> objFuelLocator = new ReturnValue<Data.MasterCatalog.GetFuelLocatorWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFuelLocator.EntityList = objContainer.GetFuelLocatorWithFilters(CustomerID, FuelLocatorId, FuelLocatorCD, ClientId, FetchActiveOnly).ToList();
                    objFuelLocator.ReturnFlag = true;
                    return objFuelLocator;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFuelLocator;
            }
        }

        #endregion
        /// <summary>
        /// To get the values for FuelLocator page from FuelLocator table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FuelLocator> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To insert values from FuelLocator page to FuelLocator table
        /// </summary>
        /// <param name="flightPurpose"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FuelLocator> Add(Data.MasterCatalog.FuelLocator fuelLocatortype)
        {
            ReturnValue<Data.MasterCatalog.FuelLocator> objFuelLocatorType = new ReturnValue<Data.MasterCatalog.FuelLocator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocatortype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFuelLocatorType.ReturnFlag = base.Validate(fuelLocatortype, ref objFuelLocatorType.ErrorMessage);
                    //For Data Anotation
                    if (objFuelLocatorType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFuelLocator(fuelLocatortype);
                        objContainer.SaveChanges();
                        objFuelLocatorType.ReturnFlag = true;
                       // return objFuelLocatorType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFuelLocatorType;
            }
        }
        /// <summary>
        /// To Update values from FuelLocator Page to FuelLocator table 
        /// </summary>
        /// <param name="flightPurpose"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FuelLocator> Update(Data.MasterCatalog.FuelLocator fuelLocatortype)
        {
            ReturnValue<Data.MasterCatalog.FuelLocator> objFuelLocatorType = new ReturnValue<Data.MasterCatalog.FuelLocator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocatortype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFuelLocatorType.ReturnFlag = base.Validate(fuelLocatortype, ref objFuelLocatorType.ErrorMessage);
                    //For Data Anotation
                    if (objFuelLocatorType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FuelLocator.Attach(fuelLocatortype);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelLocator>(fuelLocatortype, objContainer);
                        objContainer.SaveChanges();
                        objFuelLocatorType.ReturnFlag = true;
                      //  return objFuelLocatorType;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFuelLocatorType;
            }
        }
        /// <summary>
        ///  To Delete values of FuelLocator page to FuelLocator table
        /// </summary>
        /// <param name="flightPurpose"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FuelLocator> Delete(Data.MasterCatalog.FuelLocator fuelLocatortype)
        {
            ReturnValue<Data.MasterCatalog.FuelLocator> objFuelLocatorType = new ReturnValue<Data.MasterCatalog.FuelLocator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(fuelLocatortype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FuelLocator).Name, fuelLocatortype);
                    objContainer.FuelLocator.DeleteObject(fuelLocatortype);
                    objContainer.SaveChanges();
                    objFuelLocatorType.ReturnFlag = true;
                    return objFuelLocatorType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFuelLocatorType;
            }
        }
        /// <summary>
        /// To get the values for FuelLocator page from FuelLocator table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFuelLocator> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllFuelLocator> objFuelLocator = new ReturnValue<Data.MasterCatalog.GetAllFuelLocator>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objFuelLocator.EntityList = objContainer.GetAllFuelLocator(CustomerID,ClientId).ToList();
                    objFuelLocator.ReturnFlag = true;
                    return objFuelLocator;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFuelLocator;
            }
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FuelLocator> GetLock(Data.MasterCatalog.FuelLocator fuelLocatortype)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        #region "Get FuelLocator for Client"
        public ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient> GetFuelLocatorForClient()
        {
            ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient> objGetFuelLocator = new ReturnValue<Data.MasterCatalog.GetFuelLocatorForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objGetFuelLocator.EntityList = objContainer.GetFuelLocatorForClient(CustomerID, ClientId).ToList();
                    objGetFuelLocator.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objGetFuelLocator;
        }
        #endregion
        
    }
}
