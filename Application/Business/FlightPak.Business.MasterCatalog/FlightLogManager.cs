﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class FlightLogManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TSFlightLog>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for CustomFlightLog page from TSFlightLog table
        /// </summary>
        /// <returns></returns>       
        public ReturnValue<Data.MasterCatalog.TSFlightLog> GetList()
        {
            ReturnValue<Data.MasterCatalog.TSFlightLog> ReturnValue = new ReturnValue<Data.MasterCatalog.TSFlightLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    // Fetch the values from the TSFlightLog table based on CustomerID
                    ReturnValue.EntityList = Container.GetAllFlightLogReset(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To insert values from CustomFlightLog page to TSDefinitionLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSFlightLog> Add(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        ///  To update values from CustomFlightLog page to TSDefinitionLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSFlightLog> Update(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        ///  To update the selected record as deleted, from CustomFlightLog page in TSDefinitionLog table
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSFlightLog> Delete(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To fetch the values from the TSDefinitionLog table based on parameters
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSFlightLog> GetListInfo(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="TSDefinitionLog"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSFlightLog> GetLock(Data.MasterCatalog.TSFlightLog FlightLogData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reset Flight Log - Delete corresponding records from flight log  and fetch record based reset condition
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.TSFlightLog> ResetFlightLog(Data.MasterCatalog.TSFlightLog TSFlightLog)
        {
            ReturnValue<Data.MasterCatalog.TSFlightLog> ReturnValue = new ReturnValue<Data.MasterCatalog.TSFlightLog>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TSFlightLog))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.ResetFlightLog(TSFlightLog.TSFlightLogID, TSFlightLog.CustomerID, TSFlightLog.IsPrint, TSFlightLog.HomebaseID, TSFlightLog.Category, TSFlightLog.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
