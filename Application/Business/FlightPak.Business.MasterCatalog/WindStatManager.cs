﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;

namespace FlightPak.Business.MasterCatalog
{
    public class WindStatManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Account>, IMasterCatalog.ICacheable
    {
        public ReturnValue<WindStat> GetWindStatInfo(string Hemisphere, string Quadrilateral, string Altitude, string Quarter)
        {
            ReturnValue<WindStat> ret = new ReturnValue<WindStat>();
            MasterDataContainer cs = new MasterDataContainer();
            cs.ContextOptions.LazyLoadingEnabled = false;
            cs.ContextOptions.ProxyCreationEnabled = false;
            ret.EntityList = cs.GetWindStatInfo(Hemisphere, Quadrilateral, Altitude, Quarter).ToList();
            ret.ReturnFlag = true;
            return ret;
        }

        #region not implemented
        public ReturnValue<Data.MasterCatalog.Account> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> Add(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> Update(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> Delete(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Account> GetLock(Data.MasterCatalog.Account t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        #endregion 
    }
}
