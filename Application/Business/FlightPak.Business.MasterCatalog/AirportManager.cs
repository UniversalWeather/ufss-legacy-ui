﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class AirportManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Airport>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters> GetAirportForGridWithFilters(long AirportID, string ICAOID, long RunwayOnly, bool IsDisplayInctive)
        {
            ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAirportForGridWithFilters>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //This should be retrived from Services --> web.config
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    Container.CommandTimeout = MaxCommandTimeOut;
                    ReturnValue.EntityList = Container.GetAirportForGridWithFilters(CustomerID, AirportID, ICAOID, RunwayOnly, IsDisplayInctive).ToList();
                    ReturnValue.ReturnFlag = true;
                    Container.CommandTimeout = null;

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }        
        public ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter> GetAirportForGridPagingWithFilter(long AirportID, string ICAOID, long RunwayOnly, bool IsDisplayInctive, int StartRowIndex, int PageSize, string filterValue,string SortExpression,string SortOrder,string SelectedICAOID)
        {
           
            
           
            ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAirportForGridPagingWithFilter>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //This should be retrived from Services --> web.config
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    Container.CommandTimeout = MaxCommandTimeOut;
                    ReturnValue.EntityList = Container.GetAirportForGridPagingWithFilter(CustomerID, AirportID, ICAOID, RunwayOnly, IsDisplayInctive, StartRowIndex, PageSize, filterValue, SortExpression, SortOrder, SelectedICAOID).ToList();
                    ReturnValue.ReturnFlag = true;
                    Container.CommandTimeout = null;

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        #endregion

        /// <summary>
        /// To get the values for AirportCatalog page from Airport table
        /// </summary>
        /// <returns></returns>       
        public ReturnValue<Data.MasterCatalog.Airport> GetList()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To insert values from AirportCatalog page to Airport table
        /// </summary>
        /// <param name="Airport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Airport> Add(Data.MasterCatalog.Airport AirportData)
        {
            ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                   
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Airport>();
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(AirportData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Decimal LatitudeDeg = (decimal)AirportData.LatitudeDegree;
                        Decimal LongitudeDeg = (decimal)AirportData.LongitudeDegrees;
                        string LatitudeNS = AirportData.LatitudeNorthSouth;
                        string longitudeEW = AirportData.LongitudeEastWest;                        
                        AirportData.WindZone = DisplayWindZone(LatitudeDeg, LatitudeNS, LongitudeDeg, longitudeEW);
                        Container.AddToAirport(AirportData);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To update values from AirportCatalog page to Airport table
        /// </summary>
        /// <param name="Airport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Airport> Update(Data.MasterCatalog.Airport AirportData)
        {
            ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Airport>();
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(AirportData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Decimal LatitudeDeg = (decimal)AirportData.LatitudeDegree;
                        Decimal LongitudeDeg = (decimal)AirportData.LongitudeDegrees;
                        string LatitudeNS = AirportData.LatitudeNorthSouth;
                        string longitudeEW = AirportData.LongitudeEastWest;                       
                        AirportData.WindZone = DisplayWindZone(LatitudeDeg, LatitudeNS, LongitudeDeg, longitudeEW);
                        Container.Airport.Attach(AirportData);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Airport>(AirportData, Container);
                        Container.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// To update the selected record as deleted, from AirportCatalog page in Airport table
        /// </summary>
        /// <param name="Airport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Airport> Delete(Data.MasterCatalog.Airport AirportData)
        {
            ReturnValue<Data.MasterCatalog.Airport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportData))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Airport>();
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.Airport).Name, AirportData);
                    Container.Airport.DeleteObject(AirportData);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="Airport"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Airport> GetLock(Data.MasterCatalog.Airport AirportData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values into DepartmentAuthorization page
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirport>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllAirport(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }



        public ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid> GetListInfoForGrid(bool IsDisplayInctive)
        {
            ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid > ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirportListForGrid>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //This should be retrived from Services --> web.config
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    Container.CommandTimeout = MaxCommandTimeOut;
                    ReturnValue.EntityList = Container.GetAllAirportListForGrid(CustomerID, IsDisplayInctive).ToList();
                    ReturnValue.ReturnFlag = true;
                    Container.CommandTimeout = null;
                   
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount> GetAirportPositionAndCount(bool IsDisplayInctive, long AirportId, bool selectLastModified)
        {
            ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAirportPositionAndCount>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //This should be retrived from Services --> web.config
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    Container.CommandTimeout = MaxCommandTimeOut;
                    ReturnValue.EntityList = Container.GetAirportPositionAndCount(CustomerID, IsDisplayInctive, AirportId, selectLastModified).ToList();
                    ReturnValue.ReturnFlag = true;
                    Container.CommandTimeout = null;

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging> GetListInfoForGridCustomPaging(bool IsDisplayInctive, decimal MinimumRunWay, int Start, int End, string Sort, string IcaoID, string Iata, string CityName, string StateName, string CountryCD, string CountryName, string AirportName, string OffsetToGMT, string Filter)
        {
            ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirportForGridCustomPaging>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    //This should be retrived from Services --> web.config
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    Container.CommandTimeout = MaxCommandTimeOut;
                    ReturnValue.EntityList =
                        Container.GetAllAirportForGridCustomPaging(CustomerID, IsDisplayInctive, MinimumRunWay, Start,
                            End, Sort, IcaoID, Iata, CityName, StateName, CountryCD, CountryName, AirportName,
                            OffsetToGMT, Filter).ToList();
                    ReturnValue.ReturnFlag = true;
                    Container.CommandTimeout = null;

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        
        public ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit> GetListInfoForGridLimit(int Limit, bool IsDisplayInctive)
        {
            ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirportListForGridLimit>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllAirportListForGridLimit(CustomerID, Limit, IsDisplayInctive).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount> GetListInfoForGridCount(bool IsDisplayInctive)
        {
            ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirportListForGridCount>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllAirportListForGridCount(CustomerID, IsDisplayInctive).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO> GetAirportByAirportIDFBO(Int64 AirportID)
        {
            ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAirportByAirportIDFBO>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAirportByAirportIDFBO(AirportID, CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }






        /// <summary>
        /// To get a single Airport alone
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> GetAirportByAirportID(Int64 AirportID)
        {
            ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirport>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAirportByAirportID(CustomerID,AirportID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }



        /// <summary>
        /// To get a single Airport alone
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllAirport> GetAirportByAirportICaoID(string ICaoID)
        {
            ReturnValue<Data.MasterCatalog.GetAllAirport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllAirport>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAirportByICAOID(CustomerID, ICaoID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }


        /// <summary>
        /// To get a single Airport alone
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAirportByCityName> GetAirportByCityName(string CityName)
        {
            ReturnValue<Data.MasterCatalog.GetAirportByCityName> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAirportByCityName>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAirportByCityName(CustomerID, CityName).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        private int DisplayWindZone(Decimal LatitudeDeg, string LatitudeNS, Decimal LongitudeDeg, string longitudeEW)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(LatitudeDeg, LatitudeNS, LongitudeDeg, longitudeEW))
            {
                int lnZone = 0;
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    int LatDeg = Convert.ToInt32(LatitudeDeg);
                    String LatDirection = LatitudeNS;
                    int LonDeg = Convert.ToInt32(LongitudeDeg);
                    String LongDirection = longitudeEW;
                    int lnOffSet = 0;
                    lnZone = Convert.ToInt32(LatDeg / 5);
                    lnOffSet = Convert.ToInt32(LonDeg / 5);
                    if (LatDirection.ToString().ToUpper().Trim() == "N")
                    {
                        if (lnZone >= 0 && lnZone <= 17)
                        {
                            lnZone = 72 * (17 - lnZone) + 1;
                        }
                        else
                        {
                            lnZone = 0;
                        }
                    }
                    else
                    {
                        if (lnZone >= 0 && lnZone <= 17)
                        {
                            lnZone = 1297 + (72 * lnZone);
                        }
                        else
                        {
                            lnZone = 0;
                        }
                    }
                    if (LongDirection.ToString().ToUpper().Trim() == "E")
                    {
                        if (lnOffSet >= 0 && lnOffSet <= 35)
                        {
                            lnOffSet = 71 - lnOffSet;
                        }
                        else
                        {
                            lnOffSet = 0;
                        }
                    }
                    lnZone = lnZone + lnOffSet;
                    return lnZone;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return lnZone;
            }
        }

        /// <summary>
        /// To get Airport information for Fuel Vendor
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor> GetAirportForFuelVendor()
        {
            ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.GetAirportForFuelVendor>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAirportForFuelVendor(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// To get the values for Airports List for Fuel Data Upload time. AirportId, ICATA and ICAO fields only
        /// </summary>
        /// <returns></returns>       
        public ReturnValue<Data.MasterCatalog.spGetAllAirportIDsWith_ICAO_IATA_Result> GetAirportList_ICAO_IATA()
        {
            ReturnValue<Data.MasterCatalog.spGetAllAirportIDsWith_ICAO_IATA_Result> ReturnValue = new ReturnValue<Data.MasterCatalog.spGetAllAirportIDsWith_ICAO_IATA_Result>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<Data.MasterCatalog.spGetAllAirportIDsWith_ICAO_IATA_Result>();
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Container.CommandTimeout = 120; // 2 minutes
                    ReturnValue.EntityList = Container.spGetAllAirportIDsWith_ICAO_IATA(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
