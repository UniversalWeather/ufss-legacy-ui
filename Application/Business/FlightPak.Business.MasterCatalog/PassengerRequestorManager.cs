﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class PassengerRequestorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Passenger>, IMasterCatalog.ICacheable
    {
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();
        private ExceptionManager exManager;
        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters> GetAllPassengerWithFilters(long ClientId, long CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId)
        {
            ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters> objPaxGroup = new ReturnValue<Data.MasterCatalog.GetAllPassengerWithFilters>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetAllPassengerWithFilters(CustomerID, ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId).ToList();

                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result> GetAllPassengerTelerikPagedGrid(long ClientId, long CQCustomerID, long PassengerId, string PassengerCD, bool ActiveOnly, bool RequestorOnly, string ICAOID, long PaxGroupId, int start, int end, string sort, string PassengerName, string DepartmentCD, string DepartmentName, string PhoneNum)
        {
            ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result> objPaxGroup = new ReturnValue<Data.MasterCatalog.spGetAllPassenger_TelerikPagedGrid_Result>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPaxGroup.EntityList = objContainer.spGetAllPassenger_TelerikPagedGrid(CustomerID, ClientId, CQCustomerID, PassengerId, PassengerCD, ActiveOnly, RequestorOnly, ICAOID, PaxGroupId, start, end, sort, PassengerName, DepartmentCD, DepartmentName, PhoneNum).ToList();

                    objPaxGroup.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        #endregion
        public ReturnValue<Data.MasterCatalog.GetAllPassenger> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllPassenger> objPaxGroup = new ReturnValue<Data.MasterCatalog.GetAllPassenger>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetAllPassenger(CustomerID, ClientId).ToList();
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        public ReturnValue<Data.MasterCatalog.GetAllPassenger> GetPassengerforCorporateRequest()
        {
            ReturnValue<Data.MasterCatalog.GetAllPassenger> objPaxGroup = new ReturnValue<Data.MasterCatalog.GetAllPassenger>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {

                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetAllPassenger(CustomerID, ClientId).ToList();
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }

        private Data.MasterCatalog.Passenger EncryptPassenger(Data.MasterCatalog.Passenger Passenger)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Passenger))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Passenger.PersonalIDNum = Crypting.Encrypt(Passenger.PersonalIDNum);
                    foreach (Data.MasterCatalog.PassengerAdditionalInfo oPassengerAdditionalInfo in Passenger.PassengerAdditionalInfo)
                    {
                        oPassengerAdditionalInfo.CustomerID = Passenger.CustomerID;
                        if (!string.IsNullOrEmpty(oPassengerAdditionalInfo.AdditionalINFOValue))
                        {
                            oPassengerAdditionalInfo.AdditionalINFOValue = Crypting.Encrypt(oPassengerAdditionalInfo.AdditionalINFOValue);
                        }
                    }
                    foreach (Data.MasterCatalog.CrewPassengerVisa oCrewPassengerVisa in Passenger.CrewPassengerVisa)
                    {
                        oCrewPassengerVisa.CustomerID = Passenger.CustomerID;
                        if (!string.IsNullOrEmpty(oCrewPassengerVisa.VisaNum))
                        {
                            oCrewPassengerVisa.VisaNum = Crypting.Encrypt(oCrewPassengerVisa.VisaNum);
                        }
                    }
                    foreach (Data.MasterCatalog.CrewPassengerPassport oCrewPassengerPassport in Passenger.CrewPassengerPassport)
                    {
                        oCrewPassengerPassport.CustomerID = Passenger.CustomerID;
                        if (!string.IsNullOrEmpty(oCrewPassengerPassport.PassportNum))
                        {
                            oCrewPassengerPassport.PassportNum = Crypting.Encrypt(oCrewPassengerPassport.PassportNum);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return Passenger;
            }
        }
        public ReturnValue<Data.MasterCatalog.Passenger> Add(Data.MasterCatalog.Passenger Passenger)
        {
            ReturnValue<Data.MasterCatalog.Passenger> ReturnValue = new ReturnValue<Data.MasterCatalog.Passenger>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(Passenger, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                        {
                            Passenger = EncryptPassenger(Passenger);
                            objContainer.Passenger.AddObject(Passenger);
                            objContainer.ContextOptions.LazyLoadingEnabled = false;
                            objContainer.ContextOptions.ProxyCreationEnabled = false;
                            objContainer.SaveChanges();
                        }
                        ReturnValue.EntityInfo = Passenger;
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.Passenger> Update(Data.MasterCatalog.Passenger Passenger)
        {
            ReturnValue<Data.MasterCatalog.Passenger> ReturnValue = new ReturnValue<Data.MasterCatalog.Passenger>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(Passenger, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                        {
                            Passenger = EncryptPassenger(Passenger);
                            objContainer.Passenger.Attach(Passenger);
                            objContainer.ObjectStateManager.ChangeObjectState(Passenger, System.Data.EntityState.Modified);
                            foreach (Data.MasterCatalog.PassengerAdditionalInfo oPassengerAdditionalInfo in Passenger.PassengerAdditionalInfo)
                            {
                                oPassengerAdditionalInfo.LastUpdUID = Passenger.LastUpdUID;
                                if (oPassengerAdditionalInfo.PassengerAdditionalInfoID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oPassengerAdditionalInfo, System.Data.EntityState.Added);
                                    objContainer.PassengerAdditionalInfo.AddObject(oPassengerAdditionalInfo);
                                }
                                else
                                {
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerAdditionalInfo>(oPassengerAdditionalInfo, objContainer);
                                }
                            }
                            foreach (Data.MasterCatalog.PassengerCheckListDetail oCrewPassengerPassports in Passenger.PassengerCheckListDetail)
                            {
                                oCrewPassengerPassports.LastUpdUID = Passenger.LastUpdUID;
                                if (oCrewPassengerPassports.PassengerCheckListDetailID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewPassengerPassports, System.Data.EntityState.Added);
                                    objContainer.PassengerCheckListDetail.AddObject(oCrewPassengerPassports);
                                }
                                else
                                {
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerCheckListDetail>(oCrewPassengerPassports, objContainer);
                                }
                            }
                            foreach (Data.MasterCatalog.CrewPassengerVisa oCrewPassengerVisa in Passenger.CrewPassengerVisa)
                            {
                                oCrewPassengerVisa.LastUpdUID = Passenger.LastUpdUID;
                                if (oCrewPassengerVisa.VisaID < 0)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewPassengerVisa, System.Data.EntityState.Added);
                                    objContainer.CrewPassengerVisa.AddObject(oCrewPassengerVisa);
                                }
                                else
                                {
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewPassengerVisa>(oCrewPassengerVisa, objContainer);
                                }
                            }
                            foreach (Data.MasterCatalog.CrewPassengerPassport oCrewPassengerPassport in Passenger.CrewPassengerPassport)
                            {
                                oCrewPassengerPassport.LastUpdUID = Passenger.LastUpdUID;
                                if (oCrewPassengerPassport.PassportID < 1000)
                                {
                                    objContainer.ObjectStateManager.ChangeObjectState(oCrewPassengerPassport, System.Data.EntityState.Added);
                                    objContainer.CrewPassengerPassport.AddObject(oCrewPassengerPassport);
                                }
                                else
                                {
                                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewPassengerPassport>(oCrewPassengerPassport, objContainer);
                                }
                            }
                            objContainer.SaveChanges();
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.Passenger> Delete(Data.MasterCatalog.Passenger paxGroup)
        {
            ReturnValue<Data.MasterCatalog.Passenger> objPaxGroup = new ReturnValue<Data.MasterCatalog.Passenger>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.AttachTo(typeof(Data.MasterCatalog.Passenger).Name, paxGroup);
                        objContainer.Passenger.DeleteObject(paxGroup);
                        objContainer.SaveChanges();
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public ReturnValue<Data.MasterCatalog.Passenger> GetLock(Data.MasterCatalog.Passenger paxGroup)
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Passenger> GetList()
        {
            ReturnValue<Data.MasterCatalog.Passenger> objPaxGroup = new ReturnValue<Data.MasterCatalog.Passenger>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetPassenger(CustomerID, ClientId).ToList();
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public ReturnValue<Data.MasterCatalog.Passenger> GetAllPassengerList()
        {
            ReturnValue<Data.MasterCatalog.Passenger> objPaxGroup = new ReturnValue<Data.MasterCatalog.Passenger>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetPassenger(CustomerID, null).ToList();
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID> GetPassengerbyPassengerRequestorID(long PassengerRequestorID)
        {
            ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID> objPaxGroup = new ReturnValue<Data.MasterCatalog.GetPassengerByPassengerRequestorID>();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetPassengerByPassengerRequestorID(PassengerRequestorID).ToList();
                        foreach (var item in objPaxGroup.EntityList)
                        {
                            if (!string.IsNullOrEmpty(item.PersonalIDNum))
                            {
                                item.PersonalIDNum = Crypting.Decrypt(item.PersonalIDNum);
                            }
                        }
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Added by Scheduling calendar team to get PassengerRequestor Entity by PassengerRequestor Code.
        /// </summary>
        /// <param name="requestorCode"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.PassengerRequestorResult> GetPassengerRequestorByPassengerRequestorCD(string requestorCode)
        {
            ReturnValue<Data.MasterCatalog.PassengerRequestorResult> passenger = new ReturnValue<Data.MasterCatalog.PassengerRequestorResult>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        passenger.EntityList = Container.GetPassengerRequestorByPassengerRequestorCD(CustomerID, requestorCode, ClientId).ToList();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return passenger;
        }
        public ReturnValue<Data.MasterCatalog.Passenger> UpdatePassportForFutureTrip(Int64 PassportID, Int64 PassengerID, Int64 CrewID)
        {
            ReturnValue<Data.MasterCatalog.Passenger> ReturnValue = new ReturnValue<Data.MasterCatalog.Passenger>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassportID, PassengerID, CrewID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.UpdatePassportForFutureTrip(PassportID, PassengerID, CrewID);
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        #region "Get Passenger for Client"
        public ReturnValue<Data.MasterCatalog.GetPassengerForClient> GetPassengerForClient()
        {
            ReturnValue<Data.MasterCatalog.GetPassengerForClient> objPassenger = new ReturnValue<Data.MasterCatalog.GetPassengerForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPassenger.EntityList = objContainer.GetPassengerForClient(CustomerID, ClientId).ToList();
                        objPassenger.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPassenger;
        }
        #endregion
        public ReturnValue<Data.MasterCatalog.GetPassengerAssociate> GetPassengerAssociate()
        {
            ReturnValue<Data.MasterCatalog.GetPassengerAssociate> objPaxGroup = new ReturnValue<Data.MasterCatalog.GetPassengerAssociate>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        // Fix for recursive infinite loop
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objPaxGroup.EntityList = objContainer.GetPassengerAssociate(CustomerID, ClientId).ToList();
                        objPaxGroup.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPaxGroup;
        }
    }
}
