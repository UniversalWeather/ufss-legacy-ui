﻿using System.Data.EntityClient;
using FlightPak.Business.Common;
using FlightPak.Common;
using FlightPak.Common.Constants;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.MasterCatalog
{
    public class FuelCsvFileFieldsManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelCsvFileFields>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> GetList()
        {
            ReturnValue<Data.MasterCatalog.FuelCsvFileFields> ret = new ReturnValue<Data.MasterCatalog.FuelCsvFileFields>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelCsvFileFields.Where(t => t.IsDeleted == false).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> Add(Data.MasterCatalog.FuelCsvFileFields objFuelCsvFileFields)
        {
            ReturnValue<Data.MasterCatalog.FuelCsvFileFields> ret = new ReturnValue<Data.MasterCatalog.FuelCsvFileFields>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelCsvFileFields))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelCsvFileFields.IsDeleted = false;                    
                    ret.ReturnFlag = base.Validate(objFuelCsvFileFields, ref ret.ErrorMessage);                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelCsvFileFields.AddObject(objFuelCsvFileFields);
                        cs.SaveChanges();                       
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> Update(Data.MasterCatalog.FuelCsvFileFields objFuelCsvFileFields)
        {
            ReturnValue<Data.MasterCatalog.FuelCsvFileFields> ret = new ReturnValue<Data.MasterCatalog.FuelCsvFileFields>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelCsvFileFields))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    ret.ReturnFlag = base.Validate(objFuelCsvFileFields, ref ret.ErrorMessage);                    
                    if (ret.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.FuelCsvFileFields.Attach(objFuelCsvFileFields);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FuelCsvFileFields>(objFuelCsvFileFields, cs);
                        cs.SaveChanges();

                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> Delete(Data.MasterCatalog.FuelCsvFileFields objFuelCsvFileFields)
        {
            ReturnValue<Data.MasterCatalog.FuelCsvFileFields> ret = new ReturnValue<Data.MasterCatalog.FuelCsvFileFields>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(objFuelCsvFileFields))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    objFuelCsvFileFields.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                    cs.AttachTo(typeof(Data.MasterCatalog.FuelCsvFileFields).Name, objFuelCsvFileFields);
                    cs.FuelCsvFileFields.DeleteObject(objFuelCsvFileFields);
                    cs.SaveChanges();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> GetLock(Data.MasterCatalog.FuelCsvFileFields objFuelCsvFileFields)
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> AddBulkCSVFields(List<Data.MasterCatalog.FuelCsvFileFields> fieldsList)
        {
            ReturnValue<Data.MasterCatalog.FuelCsvFileFields> ret = new ReturnValue<Data.MasterCatalog.FuelCsvFileFields>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        EntityConnection entityConnection = objContainer.Connection as EntityConnection;
                        BulkUploadManager.BulkInsert(entityConnection.StoreConnection.ConnectionString, "FuelCsvFileFields",fieldsList);
                        ret.ReturnFlag = true;
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        public ReturnValue<Data.MasterCatalog.FuelCsvFileFields> DeleteOldCsvFieldsBasedOnXsltParserId(int xsltParserId)
        {
            ReturnValue<Data.MasterCatalog.FuelCsvFileFields> ret = new ReturnValue<Data.MasterCatalog.FuelCsvFileFields>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (MasterDataContainer objContainer = new MasterDataContainer())
                    {
                        IQueryable<FuelCsvFileFields> csvFields = objContainer.FuelCsvFileFields.Where(f => f.FuelFileXsltParserID == xsltParserId);
                        foreach (var field in csvFields)
                        {
                            objContainer.FuelCsvFileFields.DeleteObject(field);
                        }
                        objContainer.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, Policy.DataLayer);
            }
            return ret;
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
