﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{

    public class PayableVendorManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Vendor>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization

        public ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters> GetAllVendorWithFilters(string VendorType, long VendorID, string VendorCD, string FetchHomebaseOnly, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters> objPayableVendor = new ReturnValue<Data.MasterCatalog.GetAllVendorWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPayableVendor.EntityList = objContainer.GetAllVendorWithFilters(CustomerID, "P", VendorID,  VendorCD,  FetchHomebaseOnly,  FetchActiveOnly).ToList();
                    //if (objPayableVendor.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objPayableVendor.EntityList.Count; i++)
                    //    {
                    //        objPayableVendor.EntityList[i].VendorCD = objPayableVendor.EntityList[i].VendorCD != null ? objPayableVendor.EntityList[i].VendorCD.Trim() : string.Empty;
                    //    }
                    //}
                    objPayableVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPayableVendor;
        }

        #endregion

        public ReturnValue<Data.MasterCatalog.Vendor> GetList()
        {
            //ReturnValue<Data.MasterCatalog.Vendor> objPayableVendor = new ReturnValue<Data.MasterCatalog.Vendor>();
            //Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

            //// Fix for recursive infinite loop
            //objContainer.ContextOptions.LazyLoadingEnabled = false;
            //objContainer.ContextOptions.ProxyCreationEnabled = false;
            //objPayableVendor.EntityList = objContainer.GetAllVendor(CustomerID,"P").ToList();
            //objPayableVendor.ReturnFlag = true;
            //return objPayableVendor;

            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllVendor> GetPayableVendorList()
        {
            ReturnValue<Data.MasterCatalog.GetAllVendor> objPayableVendor = new ReturnValue<Data.MasterCatalog.GetAllVendor>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objPayableVendor.EntityList = objContainer.GetAllVendor(CustomerID, "P").ToList();
                    //if (objPayableVendor.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objPayableVendor.EntityList.Count; i++)
                    //    {
                    //        objPayableVendor.EntityList[i].VendorCD = objPayableVendor.EntityList[i].VendorCD != null ? objPayableVendor.EntityList[i].VendorCD.Trim() : string.Empty;
                    //    }
                    //}
                    objPayableVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objPayableVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> Add(Data.MasterCatalog.Vendor payableVendor)
        {
            ReturnValue<Data.MasterCatalog.Vendor> objPayableVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                     //For Data Anotation
                    objPayableVendor.ReturnFlag = base.Validate(payableVendor, ref objPayableVendor.ErrorMessage);
                    //For Data Anotation
                    if (objPayableVendor.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.Vendor.AddObject(payableVendor);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                        objPayableVendor.EntityInfo = payableVendor;
                        objPayableVendor.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objPayableVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> Update(Data.MasterCatalog.Vendor payableVendor)
        {
            ReturnValue<Data.MasterCatalog.Vendor> objPayableVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                 
                    //For Data Anotation
                    objPayableVendor.ReturnFlag = base.Validate(payableVendor, ref objPayableVendor.ErrorMessage);
                    //For Data Anotation
                    if (objPayableVendor.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        
                        objContainer.Vendor.Attach(payableVendor);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Vendor>(payableVendor, objContainer);
                        objContainer.SaveChanges();
                        objPayableVendor.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objPayableVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> Delete(Data.MasterCatalog.Vendor payableVendor)
        {
            ReturnValue<Data.MasterCatalog.Vendor> objPayableVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(payableVendor))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Vendor).Name, payableVendor);
                    objContainer.Vendor.DeleteObject(payableVendor);
                    objContainer.SaveChanges();
                    objPayableVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objPayableVendor;
        }


        public ReturnValue<Data.MasterCatalog.Vendor> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.Vendor> objPayableVendor = new ReturnValue<Data.MasterCatalog.Vendor>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objPayableVendor.EntityList = objContainer.Vendor.ToList();
                    objPayableVendor.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objPayableVendor;
        }

        public ReturnValue<Data.MasterCatalog.Vendor> GetLock(Data.MasterCatalog.Vendor payableVendor)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
