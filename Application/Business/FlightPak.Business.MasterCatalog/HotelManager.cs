﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace FlightPak.Business.MasterCatalog
{
    public class HotelManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Hotel>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters> GetHotelsByAirportIDWithFilters(long AirportID, long HotelID, string HotelCD, bool FetchactiveOnly, bool FetchIsCrewOnly, bool FetchIsPaxOnly)
        {
            ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters> objHotel = new ReturnValue<Data.MasterCatalog.GetHotelsByAirportIDWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objHotel.EntityList = objContainer.GetHotelsByAirportIDWithFilters(CustomerID, AirportID, HotelID, HotelCD, FetchactiveOnly, FetchIsCrewOnly, FetchIsPaxOnly).ToList();
                    objHotel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }
        #endregion

        public ReturnValue<Data.MasterCatalog.Hotel> GetList()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Hotel> Add(Data.MasterCatalog.Hotel hotel)
        {
            ReturnValue<Data.MasterCatalog.Hotel> objHotel = new ReturnValue<Data.MasterCatalog.Hotel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(hotel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objHotel.ReturnFlag = base.Validate(hotel, ref objHotel.ErrorMessage);
                    //For Data Anotation
                    if (objHotel.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToHotel(hotel);
                        objContainer.SaveChanges();
                        objHotel.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }
        public ReturnValue<Data.MasterCatalog.Hotel> Update(Data.MasterCatalog.Hotel hotel)
        {
            ReturnValue<Data.MasterCatalog.Hotel> objHotel = new ReturnValue<Data.MasterCatalog.Hotel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(hotel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objHotel.ReturnFlag = base.Validate(hotel, ref objHotel.ErrorMessage);
                    //For Data Anotation
                    if (objHotel.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.Hotel.Attach(hotel);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Hotel>(hotel, objContainer);
                        objContainer.SaveChanges();
                        objHotel.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }
        public ReturnValue<Data.MasterCatalog.Hotel> Delete(Data.MasterCatalog.Hotel hotel)
        {
            ReturnValue<Data.MasterCatalog.Hotel> objHotel = new ReturnValue<Data.MasterCatalog.Hotel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(hotel))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Hotel).Name, hotel);
                    objContainer.Hotel.DeleteObject(hotel);
                    objContainer.SaveChanges();
                    objHotel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }
        public ReturnValue<Data.MasterCatalog.GetAllHotel> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllHotel> objHotel = new ReturnValue<Data.MasterCatalog.GetAllHotel>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objHotel.EntityList = objContainer.GetAllHotel(CustomerID).ToList();
                    objHotel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }

        public ReturnValue<Data.MasterCatalog.GetHotelsByAirportID> GetHotelsByAirportID(long airportID)
        {
            ReturnValue<Data.MasterCatalog.GetHotelsByAirportID> objHotel = new ReturnValue<Data.MasterCatalog.GetHotelsByAirportID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objHotel.EntityList = objContainer.GetHotelsByAirportID(CustomerID, airportID).ToList();
                    objHotel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }

        public ReturnValue<Data.MasterCatalog.GetHotelByHotelID> GetHotelByHotelID(long hotelID)
        {
            ReturnValue<Data.MasterCatalog.GetHotelByHotelID> objHotel = new ReturnValue<Data.MasterCatalog.GetHotelByHotelID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objHotel.EntityList = objContainer.GetHotelByHotelID(hotelID).ToList();
                    objHotel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }
        public ReturnValue<Data.MasterCatalog.GetHotelByHotelCode> GetHotelByHotelCode(long airportID,string hotelCd)
        {
            ReturnValue<Data.MasterCatalog.GetHotelByHotelCode> objHotel = new ReturnValue<Data.MasterCatalog.GetHotelByHotelCode>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objHotel.EntityList = objContainer.GetHotelByHotelCode(CustomerID, airportID, hotelCd).ToList();
                    objHotel.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objHotel;
        }
        public ReturnValue<Data.MasterCatalog.Hotel> GetLock(Data.MasterCatalog.Hotel hotel)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
