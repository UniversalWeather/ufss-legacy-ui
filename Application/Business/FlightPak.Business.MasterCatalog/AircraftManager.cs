﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class AircraftManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Aircraft>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        public ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftWithFilters(string AircraftCD, long AircraftID, bool FetchInactiveOnly)
        {
            ReturnValue<Data.MasterCatalog.Aircraft> objAircraftType = new ReturnValue<Data.MasterCatalog.Aircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objAircraftType.EntityList = objContainer.GetAircraftWithFilters(CustomerID, AircraftCD, AircraftID, FetchInactiveOnly).ToList();
                    objAircraftType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircraftType;
        }
        #endregion

        public ReturnValue<Data.MasterCatalog.GetAllAircraft> GetAircraftInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllAircraft> objAircraftType = new ReturnValue<Data.MasterCatalog.GetAllAircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objAircraftType.EntityList = objContainer.GetAllAircraft(CustomerID).ToList();
                    //if (objAircraftType.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objAircraftType.EntityList.Count; i++)
                    //    {
                    //        objAircraftType.EntityList[i].AircraftCD = objAircraftType.EntityList[i].AircraftCD != null ? objAircraftType.EntityList[i].AircraftCD.Trim() : string.Empty;
                    //    }
                    //}
                    objAircraftType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircraftType;
        }
        public ReturnValue<Data.MasterCatalog.Aircraft> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.Aircraft> GetAircraftByAircraftID(Int64 AircraftID)
        {
            ReturnValue<Data.MasterCatalog.Aircraft> objAircraftType = new ReturnValue<Data.MasterCatalog.Aircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objAircraftType.EntityList = objContainer.GetAircraftByAircraftID(CustomerID, AircraftID).ToList();
                    objAircraftType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircraftType;
        }


        public ReturnValue<Data.MasterCatalog.Aircraft> Add(Data.MasterCatalog.Aircraft aircraft)
        {
            ReturnValue<Data.MasterCatalog.Aircraft> objAircraftType = new ReturnValue<Data.MasterCatalog.Aircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objAircraftType.ReturnFlag = base.Validate(aircraft, ref objAircraftType.ErrorMessage);
                    //For Data Anotation
                    if (objAircraftType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToAircraft(aircraft);
                        objContainer.SaveChanges();
                        objAircraftType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircraftType;
        }
        public ReturnValue<Data.MasterCatalog.Aircraft> Update(Data.MasterCatalog.Aircraft aircraft)
        {
            ReturnValue<Data.MasterCatalog.Aircraft> objAircraftType = new ReturnValue<Data.MasterCatalog.Aircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objAircraftType.ReturnFlag = base.Validate(aircraft, ref objAircraftType.ErrorMessage);
                    //For Data Anotation
                    if (objAircraftType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.Aircraft.Attach(aircraft);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Aircraft>(aircraft, objContainer);
                        objContainer.SaveChanges();
                        objAircraftType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircraftType;
        }
        public ReturnValue<Data.MasterCatalog.Aircraft> Delete(Data.MasterCatalog.Aircraft aircraft)
        {
            ReturnValue<Data.MasterCatalog.Aircraft> objAircraftType = new ReturnValue<Data.MasterCatalog.Aircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(aircraft))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.Aircraft).Name, aircraft);
                    objContainer.Aircraft.DeleteObject(aircraft);
                    objContainer.SaveChanges();
                    objAircraftType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircraftType;
        }
        public ReturnValue<Data.MasterCatalog.Aircraft> GetLock(Data.MasterCatalog.Aircraft t)
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Aircraft> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.Aircraft> objAircrafttype = new ReturnValue<Data.MasterCatalog.Aircraft>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objAircrafttype.EntityList = objContainer.Aircraft.ToList();
                    objAircrafttype.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objAircrafttype;
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
