﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using System.Data.Objects.DataClasses;
//using FlightPak.Business.Common;

//namespace FlightPak.Business.MasterCatalog
//{
//    public class PassengerPassportInfoManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.PassengerPassportInfo>, IMasterCatalog.ICacheable
//    {
//        public ReturnValue<Data.MasterCatalog.PassengerPassportInfo> GetList()
//        {
//            ReturnValue<Data.MasterCatalog.PassengerPassportInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerPassportInfo>();

//            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            
//            // Fix for recursive infinite loop
//            objContainer.ContextOptions.LazyLoadingEnabled = false;
//            objContainer.ContextOptions.ProxyCreationEnabled = false;
//            objPaxGroup.EntityList = objContainer.GetPassengerPassportInfo().ToList();
//            objPaxGroup.ReturnFlag = true;
//            return objPaxGroup;
//        }

//        public ReturnValue<Data.MasterCatalog.PassengerPassportInfo> Add(Data.MasterCatalog.PassengerPassportInfo Passenger)
//        {
//            ReturnValue<Data.MasterCatalog.PassengerPassportInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerPassportInfo>();

//            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
//            objContainer.AddToPassengerPassportInfo(Passenger);
//            objContainer.SaveChanges();
//            objPaxGroup.ReturnFlag = true;
//            return objPaxGroup;
//        }

//        public ReturnValue<Data.MasterCatalog.PassengerPassportInfo> Update(Data.MasterCatalog.PassengerPassportInfo paxGroup)
//        {
//            ReturnValue<Data.MasterCatalog.PassengerPassportInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerPassportInfo>();

//            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
//            objContainer.PassengerPassportInfo.Attach(paxGroup);
//            Common.EMCommon.SetAllModified<Data.MasterCatalog.PassengerPassportInfo>(paxGroup, objContainer);
//            objContainer.SaveChanges();           
//            objPaxGroup.ReturnFlag = true;
//            return objPaxGroup;
//        }

//        public ReturnValue<Data.MasterCatalog.PassengerPassportInfo> Delete(Data.MasterCatalog.PassengerPassportInfo paxGroup)
//        {
//            ReturnValue<Data.MasterCatalog.PassengerPassportInfo> objPaxGroup = new ReturnValue<Data.MasterCatalog.PassengerPassportInfo>();

//            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

//            objContainer.AttachTo(typeof(Data.MasterCatalog.Passenger).Name, paxGroup);
//            objContainer.PassengerPassportInfo.DeleteObject(paxGroup);
//            objContainer.SaveChanges();           
//            objPaxGroup.ReturnFlag = true;
//            return objPaxGroup;
//        }

//        public ReturnValue<Data.MasterCatalog.PassengerPassportInfo> GetLock(Data.MasterCatalog.PassengerPassportInfo paxGroup)
//        {
//            throw new NotImplementedException();
//        }

//        public void RefreshCache()
//        {
//            throw new NotImplementedException();
//        }
//    }
//}
