﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class RunwayManager : BaseManager , IMasterCatalog.IMasterData<Data.MasterCatalog.Runway>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.Runway> GetList()
        {
            ReturnValue<Data.MasterCatalog.Runway> objRunway = new ReturnValue<Data.MasterCatalog.Runway>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            // Fix for recursive infinite loop
            objContainer.ContextOptions.LazyLoadingEnabled = false;
            objContainer.ContextOptions.ProxyCreationEnabled = false;
            objRunway.EntityList = objContainer.GetRunwayList().ToList();
            objRunway.ReturnFlag = true;
            return objRunway;
        }
        /// <summary>
        /// To get a single Airport alone
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Runway> GetRunwayByAirportID(Int64 AirportID)
        {
            ReturnValue<Data.MasterCatalog.Runway> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.MasterCatalog.Runway>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetRunwayByAirportID(AirportID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.MasterCatalog.Runway> Add(Data.MasterCatalog.Runway runwayType)
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Runway> Update(Data.MasterCatalog.Runway runwayType)
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Runway> Delete(Data.MasterCatalog.Runway runwayType)
        {
            throw new NotImplementedException();
        }
       
        public ReturnValue<Data.MasterCatalog.Runway> GetLock(Data.MasterCatalog.Runway runwaytype)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
