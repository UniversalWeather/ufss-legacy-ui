﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CustomerFuelVendorAccessManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CustomerFuelVendorAccess>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<CustomerFuelVendorAccess> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CustomerFuelVendorAccess> Add(CustomerFuelVendorAccess oCustomerFuelVendorAccess)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CustomerFuelVendorAccess> AddCustomerFuelVendorAccess(List<CustomerFuelVendorAccess> oCustomerFuelVendorAccess,long customerId)
        {
            var retCustomerFuelVendorAccess = new ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccess>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCustomerFuelVendorAccess))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (var objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        foreach (var item in oCustomerFuelVendorAccess)
                        {
                            item.CustomerID = customerId;
                            objContainer.CustomerFuelVendorAccess.AddObject(item);
                        }
                        int maxCommandTimeOut = -1;
                        if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                        {
                            maxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                        }
                        objContainer.CommandTimeout = maxCommandTimeOut;
                        objContainer.SaveChanges(System.Data.Objects.SaveOptions.None);
                        objContainer.CommandTimeout = null;
                        retCustomerFuelVendorAccess.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return retCustomerFuelVendorAccess;
        }
        public ReturnValue<CustomerFuelVendorAccess> UpdateCustomerFuelVendorAccess(List<CustomerFuelVendorAccess> oCustomerFuelVendorAccess, long customerId)
        {
            var retCustomerFuelVendorAccess = new ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccess>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oCustomerFuelVendorAccess))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (var objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        foreach (var item in oCustomerFuelVendorAccess)
                        {
                            item.CustomerID = customerId;
                            if (item.CustomerFuelVendorAccessID > 0)
                            {
                                objContainer.CustomerFuelVendorAccess.Attach(item);
                                Common.EMCommon.SetAllModified<Data.MasterCatalog.CustomerFuelVendorAccess>(item, objContainer);       
                            } else
                            {
                                objContainer.CustomerFuelVendorAccess.AddObject(item);
                            }
                        }
                        int maxCommandTimeOut = -1;
                        if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                        {
                            maxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                        }
                        objContainer.CommandTimeout = maxCommandTimeOut;
                        objContainer.SaveChanges(System.Data.Objects.SaveOptions.None);
                        objContainer.CommandTimeout = null;
                        retCustomerFuelVendorAccess.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return retCustomerFuelVendorAccess;
        }

        public ReturnValue<CustomerFuelVendorAccess> Update(CustomerFuelVendorAccess t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CustomerFuelVendorAccess> Delete(CustomerFuelVendorAccess t)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<CustomerFuelVendorAccess> GetLock(CustomerFuelVendorAccess t)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccessList> GetCustomerFuelVendorAccessList(long? customerId)
        {
            var objFuelVendor = new ReturnValue<Data.MasterCatalog.CustomerFuelVendorAccessList>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (var objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objFuelVendor.EntityList = new List<CustomerFuelVendorAccessList>(objContainer.spGetCustomerFuelVendorAccessList(customerId));
                        objFuelVendor.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFuelVendor;
        }
    }
}
