﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class CrewGroupOrderManager : BaseManager
    {
        private ExceptionManager exManager;
        /// <summary>
        /// To get the values for CrewGroup page from Crew table based on CrewCode
        /// </summary>
        /// <param name="GroupCD"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> GetCrewavailableList(Int64 GroupCD)
        {
            ReturnValue<Data.MasterCatalog.Crew> objCrewMasterType = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(GroupCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCrewMasterType.EntityList = objContainer.GetCrewavailable(CustomerID, GroupCD, ClientId).ToList();
                    objCrewMasterType.ReturnFlag = true;
                    return objCrewMasterType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewMasterType;
            }
        }
        /// <summary>
        /// To get the values for CrewGroup page from Crewgrouporder table based on CrewCode
        /// </summary>
        /// <param name="GroupCD"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.Crew> GetCrewselectedList(Int64 GroupCD)
        {
            ReturnValue<Data.MasterCatalog.Crew> objCrewMasterType = new ReturnValue<Data.MasterCatalog.Crew>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(GroupCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCrewMasterType.EntityList = objContainer.GetCrewselected(CustomerID, GroupCD).ToList();
                    objCrewMasterType.ReturnFlag = true;
                    return objCrewMasterType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewMasterType;
            }
        }
        /// <summary>
        /// To insert values from Crewgroup page to CrewGroupOrder table
        /// </summary>
        /// <param name="crewgroupordertype"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroupOrder> Add(Data.MasterCatalog.CrewGroupOrder crewgroupordertype)
        {
            ReturnValue<Data.MasterCatalog.CrewGroupOrder> objCrewGroupOrderType = new ReturnValue<Data.MasterCatalog.CrewGroupOrder>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewgroupordertype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToCrewGroupOrder(crewgroupordertype);
                    objContainer.SaveChanges();
                    objCrewGroupOrderType.ReturnFlag = true;
                    return objCrewGroupOrderType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewGroupOrderType;
            }
        }
        /// <summary>
        // To Delete values of Crewgroup page to CrewGroupOrder table
        /// </summary>
        /// <param name="crewgroupordertype"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewGroupOrder> Delete(Data.MasterCatalog.CrewGroupOrder crewgroupordertype)
        {
            ReturnValue<Data.MasterCatalog.CrewGroupOrder> objCrewGroupOrderType = new ReturnValue<Data.MasterCatalog.CrewGroupOrder>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewgroupordertype))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.CrewGroupOrder).Name, crewgroupordertype);
                    objContainer.CrewGroupOrder.DeleteObject(crewgroupordertype);
                    objContainer.SaveChanges();
                    objCrewGroupOrderType.ReturnFlag = true;
                    return objCrewGroupOrderType;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objCrewGroupOrderType;
            }
        }
    }
}
