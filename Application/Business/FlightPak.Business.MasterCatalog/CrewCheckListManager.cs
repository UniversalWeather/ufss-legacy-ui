﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewChecklistManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewCheckList>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.CrewCheckList> GetList()
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objCrewCheckList.EntityList = objContainer.GetAllCrewCheckList(CustomerID).ToList();
                    //if (objCrewCheckList.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objCrewCheckList.EntityList.Count; i++)
                    //    {
                    //        objCrewCheckList.EntityList[i].CrewCheckCD = objCrewCheckList.EntityList[i].CrewCheckCD != null ? objCrewCheckList.EntityList[i].CrewCheckCD.Trim() : string.Empty;
                    //    }
                    //}
                    objCrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewCheckList;
        }
        public ReturnValue<Data.MasterCatalog.CrewCheckList> Add(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewCheckList.ReturnFlag = base.Validate(CrewChecklist, ref objCrewCheckList.ErrorMessage);
                    //For Data Anotation
                    if (objCrewCheckList.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToCrewCheckList(CrewChecklist);
                        objContainer.SaveChanges();
                        objCrewCheckList.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewCheckList;
        }

        public ReturnValue<Data.MasterCatalog.CrewCheckList> AddChecklistforAllCrew(Data.MasterCatalog.CrewCheckList crewchecklist)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewChecklist = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewchecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewChecklist.ReturnFlag = base.Validate(crewchecklist, ref objCrewChecklist.ErrorMessage);
                    //For Data Anotation
                    if (objCrewChecklist.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddCrewChecklistforAllCrew(crewchecklist.CustomerID, crewchecklist.CrewCheckCD, crewchecklist.CrewChecklistDescription, crewchecklist.ClientID, crewchecklist.LastUpdUID, crewchecklist.LastUpdTS, crewchecklist.IsScheduleCheck, crewchecklist.IsCrewCurrencyPlanner, crewchecklist.IsDeleted, crewchecklist.IsInActive);
                        objContainer.SaveChanges();
                        objCrewChecklist.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewChecklist;
        }

        public ReturnValue<Data.MasterCatalog.CrewCheckList> UpdateChecklistforAllCrew(Data.MasterCatalog.CrewCheckList crewchecklist)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewChecklist = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewchecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewChecklist.ReturnFlag = base.Validate(crewchecklist, ref objCrewChecklist.ErrorMessage);
                    //For Data Anotation
                    if (objCrewChecklist.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.UpdateCrewChecklistforAllCrew(crewchecklist.CrewCheckID, crewchecklist.CustomerID, crewchecklist.CrewCheckCD, crewchecklist.CrewChecklistDescription, crewchecklist.ClientID, crewchecklist.LastUpdUID, crewchecklist.LastUpdTS, crewchecklist.IsScheduleCheck, crewchecklist.IsCrewCurrencyPlanner, crewchecklist.IsDeleted, crewchecklist.IsInActive);
                        objContainer.SaveChanges();
                        objCrewChecklist.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewChecklist;
        }

        public ReturnValue<Data.MasterCatalog.CrewCheckList> Update(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    //For Data Anotation
                    objCrewCheckList.ReturnFlag = base.Validate(CrewChecklist, ref objCrewCheckList.ErrorMessage);
                    //For Data Anotation
                    if (objCrewCheckList.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.CrewCheckList.Attach(CrewChecklist);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewCheckList>(CrewChecklist, objContainer);
                        objContainer.SaveChanges();
                        objCrewCheckList.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewCheckList;
        }
        public ReturnValue<Data.MasterCatalog.CrewCheckList> Delete(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CrewChecklist))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.CrewCheckList).Name, CrewChecklist);
                    objContainer.CrewCheckList.DeleteObject(CrewChecklist);
                    objContainer.SaveChanges();
                    objCrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewCheckList;
        }
        public ReturnValue<Data.MasterCatalog.CrewCheckList> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.CrewCheckList> objCrewCheckList = new ReturnValue<Data.MasterCatalog.CrewCheckList>();

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objCrewCheckList.EntityList = objContainer.CrewCheckList.ToList();
                    objCrewCheckList.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            return objCrewCheckList;
        }
        public ReturnValue<Data.MasterCatalog.CrewCheckList> GetLock(Data.MasterCatalog.CrewCheckList CrewChecklist)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
