﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;

namespace FlightPak.Business.MasterCatalog
{
    public class BudgetGetListManager:BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.GetAllBudget>, IMasterCatalog.ICacheable
    {
        public ReturnValue<Data.MasterCatalog.GetAllBudget> GetList()
        {
            ReturnValue<Data.MasterCatalog.GetAllBudget> objBudgetType = new ReturnValue<Data.MasterCatalog.GetAllBudget>();
            Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
            // Fix for recursive infinite loop
            objContainer.ContextOptions.LazyLoadingEnabled = false;
            objContainer.ContextOptions.ProxyCreationEnabled = false;
            objBudgetType.EntityList = objContainer.GetAllBudget(CustomerID).ToList();
            objBudgetType.ReturnFlag = true;
            return objBudgetType;
        }


        public ReturnValue<Data.MasterCatalog.GetAllBudget> GetLock(Data.MasterCatalog.GetAllBudget budgetType)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }


        public ReturnValue<Data.MasterCatalog.GetAllBudget> Add(Data.MasterCatalog.GetAllBudget budgetType)
        {

            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllBudget> Update(Data.MasterCatalog.GetAllBudget budgetType)
        {

            throw new NotImplementedException();

        }

        public ReturnValue<Data.MasterCatalog.GetAllBudget> Delete(Data.MasterCatalog.GetAllBudget budgetType)
        {
            throw new NotImplementedException();

        }

    }
}
