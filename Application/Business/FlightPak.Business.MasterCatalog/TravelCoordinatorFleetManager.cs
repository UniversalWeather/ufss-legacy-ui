﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class TravelCoordinatorFleetManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.TravelCoordinatorFleet>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetList()
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> objTravelCoordinatorFleet = new ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objTravelCoordinatorFleet.EntityList = objContainer.GetAllTravelCoordinatorFleet(CustomerID).ToList();
                    objTravelCoordinatorFleet.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorFleet;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> Add(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> objTravelCoordinatorFleet = new ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AddToTravelCoordinatorFleet(travelCoordinatorFleet);
                    objContainer.SaveChanges();
                    objTravelCoordinatorFleet.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorFleet;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetTravelCoordinatorFleetSelectedList(Int64 travelCoordCD)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> objTravelCoordinatorFleet = new ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objTravelCoordinatorFleet.EntityList = objContainer.GetTravelCoordinatorFleetSelectedList(CustomerID, travelCoordCD).ToList();
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorFleet;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> Update(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> objTravelCoordinatorFleet = new ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.TravelCoordinatorFleet.Attach(travelCoordinatorFleet);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.TravelCoordinatorFleet>(travelCoordinatorFleet, objContainer);
                    objContainer.SaveChanges();
                    objTravelCoordinatorFleet.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorFleet;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> Delete(Data.MasterCatalog.TravelCoordinatorFleet travelCoordinatorFleet)
        {
            ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> objTravelCoordinatorFleet = new ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(travelCoordinatorFleet))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.TravelCoordinatorFleet).Name, travelCoordinatorFleet);
                    objContainer.TravelCoordinatorFleet.DeleteObject(travelCoordinatorFleet);
                    objContainer.SaveChanges();
                    objTravelCoordinatorFleet.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objTravelCoordinatorFleet;
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetListInfo()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.TravelCoordinatorFleet> GetLock(Data.MasterCatalog.TravelCoordinatorFleet TravelCoordinatorFleet)
        {
            throw new NotImplementedException();
        }
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
