﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
namespace FlightPak.Business.MasterCatalog
{
    public class FlightPurposeManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FlightPurpose>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        #region For Performance Optimization

        public ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters> GetAllFlightPurposesWithFilters(long ClientID, long FlightPurposeID, string FlightPurposeCD, bool FetchDefaultPurposeOnly, bool FetchPersonalTravelOnly, bool FetchActiveOnly)
        {
            ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters> objFlightPurpose = new ReturnValue<Data.MasterCatalog.GetAllFlightPurposesWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightPurpose.EntityList = objContainer.GetAllFlightPurposesWithFilters(CustomerID, ClientID, FlightPurposeID, FlightPurposeCD, FetchDefaultPurposeOnly, FetchPersonalTravelOnly, FetchActiveOnly).ToList();
                    //if (objFlightPurpose.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objFlightPurpose.EntityList.Count; i++)
                    //    {
                    //        objFlightPurpose.EntityList[i].FlightPurposeCD = objFlightPurpose.EntityList[i].FlightPurposeCD != null ? objFlightPurpose.EntityList[i].FlightPurposeCD.Trim() : string.Empty;
                    //    }
                    //}
                    objFlightPurpose.ReturnFlag = true;
                    return objFlightPurpose;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightPurpose;
            }
        }

        #endregion

        /// <summary>
        /// To get the values for FlightPurpose page from FlightPurpose table
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightPurpose> GetList()
        {
            ReturnValue<Data.MasterCatalog.FlightPurpose> objFlightPurpose = new ReturnValue<Data.MasterCatalog.FlightPurpose>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightPurpose.EntityList = objContainer.GetAllFlightPurpose(CustomerID,ClientId).ToList();
                    //if (objFlightPurpose.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < objFlightPurpose.EntityList.Count; i++)
                    //    {
                    //        objFlightPurpose.EntityList[i].FlightPurposeCD = objFlightPurpose.EntityList[i].FlightPurposeCD != null ? objFlightPurpose.EntityList[i].FlightPurposeCD.Trim() : string.Empty;
                    //    }
                    //}
                    objFlightPurpose.ReturnFlag = true;
                    return objFlightPurpose;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightPurpose;
            }
        }
        /// <summary>
        /// To insert values from FlightPurpose page to FlightPurpose table
        /// </summary>
        /// <param name="flightPurpose"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightPurpose> Add(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            ReturnValue<Data.MasterCatalog.FlightPurpose> objFlightPurpose = new ReturnValue<Data.MasterCatalog.FlightPurpose>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFlightPurpose.ReturnFlag = base.Validate(flightPurpose, ref objFlightPurpose.ErrorMessage);
                    //For Data Anotation
                    if (objFlightPurpose.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.AddToFlightPurpose(flightPurpose);
                        objContainer.SaveChanges();
                        objFlightPurpose.ReturnFlag = true;
                     //   return objFlightPurpose;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightPurpose;
            }
        }
        /// <summary>
        /// To Update values from FlightPurposepage to FlightPurpose table 
        /// </summary>
        /// <param name="flightPurpose"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightPurpose> Update(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            ReturnValue<Data.MasterCatalog.FlightPurpose> objFlightPurpose = new ReturnValue<Data.MasterCatalog.FlightPurpose>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFlightPurpose.ReturnFlag = base.Validate(flightPurpose, ref objFlightPurpose.ErrorMessage);
                    //For Data Anotation
                    if (objFlightPurpose.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FlightPurpose.Attach(flightPurpose);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FlightPurpose>(flightPurpose, objContainer);
                        objContainer.SaveChanges();
                        objFlightPurpose.ReturnFlag = true;
                     //   return objFlightPurpose;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightPurpose;
            }
        }
        /// <summary>
        ///  To Delete values of FlightPurpose page to FlightPurpose table
        /// </summary>
        /// <param name="flightPurpose"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightPurpose> Delete(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            ReturnValue<Data.MasterCatalog.FlightPurpose> objFlightPurpose = new ReturnValue<Data.MasterCatalog.FlightPurpose>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(flightPurpose))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FlightPurpose).Name, flightPurpose);
                    objContainer.FlightPurpose.DeleteObject(flightPurpose);
                    objContainer.SaveChanges();
                    objFlightPurpose.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFlightPurpose;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentGroup"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.FlightPurpose> GetLock(Data.MasterCatalog.FlightPurpose flightPurpose)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To get the values for FlightPurpose page from FlightPurposetable
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllFlightPurposes> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllFlightPurposes> objFlightPurpose = new ReturnValue<Data.MasterCatalog.GetAllFlightPurposes>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightPurpose.EntityList = objContainer.GetAllFlightPurposes(CustomerID, ClientId).ToList();
                    objFlightPurpose.ReturnFlag = true;
                    return objFlightPurpose;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightPurpose;
            }
        }

        public ReturnValue<Data.MasterCatalog.FlightPurpose> GetFlightPurposebyFlightPurposeID(Int64 FlightPurposeID)
        {
            ReturnValue<Data.MasterCatalog.FlightPurpose> objFlightPurpose = new ReturnValue<Data.MasterCatalog.FlightPurpose>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightPurpose.EntityList = objContainer.GetFlightPurposebyFlightPurposeID(CustomerID, FlightPurposeID).ToList();
                    objFlightPurpose.ReturnFlag = true;
                    return objFlightPurpose;
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return objFlightPurpose;
            }
        }

        #region "Get FlightPurpose for Client"
        public ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient> GetFlightPurposeForClient()
        {
            ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient> objFlightPurpose = new ReturnValue<Data.MasterCatalog.GetFlightPurposeForClient>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objFlightPurpose.EntityList = objContainer.GetFlightPurposeForClient(CustomerID, ClientId).ToList();
                    objFlightPurpose.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return objFlightPurpose;
        }
        #endregion
    }
}
