﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.MasterCatalog
{
    public class TransportManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.Transport>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;

        #region For Performance Optimization
        /// <summary>
        /// To get the values for TransportCatalog page from Transport table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters> GetTransportByAirportIDWithFilters(long AirportID, long TransportID, string TransportCD, bool IsChoice, bool IsInActive)
        {
            ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters> ReturnValue = new ReturnValue<Data.MasterCatalog.GetTransportByAirportIDWithFilters>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    // Fetch the values from the Transport table based passed CustomerID
                    ReturnValue.EntityList = Container.GetTransportByAirportIDWithFilters(CustomerID, AirportID, TransportID, TransportCD, IsChoice, IsInActive).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        #endregion
        /// <summary>
        /// To get the values for TransportCatalog page from Transport table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.GetAllTransport> GetListInfo()
        {
            ReturnValue<Data.MasterCatalog.GetAllTransport> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllTransport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    // Fetch the values from the Transport table based passed CustomerID
                    ReturnValue.EntityList = Container.GetAllTransport(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        /// To insert values from TransportCatalog page to Transport table
        /// </summary>
        /// <param name="transport"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.Transport> Add(Data.MasterCatalog.Transport TransportData)
        {
            ReturnValue<Data.MasterCatalog.Transport> ReturnValue = new ReturnValue<Data.MasterCatalog.Transport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TransportData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(TransportData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.AddToTransport(TransportData);
                        Container.SaveChanges();

                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        /// To update values from TransportCatalog page to Transport table
        /// </summary>
        /// <param name="transport"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.Transport> Update(Data.MasterCatalog.Transport TransportData)
        {
            ReturnValue<Data.MasterCatalog.Transport> ReturnValue = new ReturnValue<Data.MasterCatalog.Transport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TransportData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    ReturnValue.ReturnFlag = base.Validate(TransportData, ref ReturnValue.ErrorMessage);
                    //For Data Anotation
                    if (ReturnValue.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                        Container.Transport.Attach(TransportData);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.Transport>(TransportData, Container);
                        Container.SaveChanges();

                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        /// To update the selected record as deleted, from TransportCatalog page in transport table
        /// </summary>
        /// <param name="transport"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.Transport> Delete(Data.MasterCatalog.Transport TransportData)
        {
            ReturnValue<Data.MasterCatalog.Transport> ReturnValue = new ReturnValue<Data.MasterCatalog.Transport>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(TransportData))
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    TransportData.IsDeleted = true;
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.Transport).Name, TransportData);
                    Container.Transport.DeleteObject(TransportData);
                    Container.SaveChanges();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        /// <summary>
        /// To get the values for TransportCatalog page from Transport table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.GetTransportByAirportID> GetTransportByAirportID(long AirportID)
        {
            ReturnValue<Data.MasterCatalog.GetTransportByAirportID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetTransportByAirportID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    // Fetch the values from the Transport table based passed CustomerID
                    ReturnValue.EntityList = Container.GetTransportByAirportID(CustomerID, AirportID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        /// <summary>
        /// To get the values for TransportCatalog page from Transport table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.GetTransportByTransportID> GetTransportByTransportID(long TransportID)
        {
            ReturnValue<Data.MasterCatalog.GetTransportByTransportID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetTransportByTransportID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    // Fetch the values from the Transport table based passed CustomerID
                    ReturnValue.EntityList = Container.GetTransportByTransportID(TransportID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }


        /// <summary>
        /// To get the values for TransportCatalog page from Transport table
        /// </summary>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.GetTransportByTransportCode> GetTransportByTransportCode(long AirportID,string TransportCd)
        {
            ReturnValue<Data.MasterCatalog.GetTransportByTransportCode> ReturnValue = new ReturnValue<Data.MasterCatalog.GetTransportByTransportCode>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {

                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    // Fix for recursive infinite loop

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    // Fetch the values from the Transport table based passed CustomerID
                    ReturnValue.EntityList = Container.GetTransportByTransportCode(CustomerID, AirportID, TransportCd).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="transport"></param>
        /// <returns>ReturnValue<Data.MasterCatalog.Hotel></returns>
        public ReturnValue<Data.MasterCatalog.Transport> GetLock(Data.MasterCatalog.Transport TransportData)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
        public ReturnValue<Data.MasterCatalog.Transport> GetList()
        {
            throw new NotImplementedException();
        }
    }
}
