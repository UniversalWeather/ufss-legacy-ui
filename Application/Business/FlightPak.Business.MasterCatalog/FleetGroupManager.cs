﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using FlightPak.Business.IMasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class FleetGroupManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FleetGroup>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        protected FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        public ReturnValue<Data.MasterCatalog.FleetGroup> GetList()
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.GetAllFleetGroup> GetFleetGroup()
        {
            ReturnValue<Data.MasterCatalog.GetAllFleetGroup> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllFleetGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {


                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllFleetGroup(CustomerID).ToList();
                    //if (ReturnValue.EntityList.Count > 0)
                    //{
                    //    for (int i = 0; i < ReturnValue.EntityList.Count; i++)
                    //    {
                    //        ReturnValue.EntityList[i].FleetGroupCD = ReturnValue.EntityList[i].FleetGroupCD != null ? ReturnValue.EntityList[i].FleetGroupCD.Trim() : string.Empty;
                    //    }
                    //}
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetGroup> Add(Data.MasterCatalog.FleetGroup fleettype)
        {

            ReturnValue<Data.MasterCatalog.FleetGroup> objFleetType = new ReturnValue<Data.MasterCatalog.FleetGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFleetType.ReturnFlag = base.Validate(fleettype, ref objFleetType.ErrorMessage);
                    //For Data Anotation
                    if (objFleetType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();

                        //cs.Fleet_Group.AddObject(fltgroup);                       
                        objContainer.AddToFleetGroup(fleettype);
                        objContainer.SaveChanges();

                        objFleetType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.FleetGroup> Update(Data.MasterCatalog.FleetGroup fleettype)
        {
            ReturnValue<Data.MasterCatalog.FleetGroup> objFleetType = new ReturnValue<Data.MasterCatalog.FleetGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {                    
                    //For Data Anotation
                    objFleetType.ReturnFlag = base.Validate(fleettype, ref objFleetType.ErrorMessage);
                    //For Data Anotation
                    if (objFleetType.ReturnFlag)
                    {
                        Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                        objContainer.FleetGroup.Attach(fleettype);
                        Common.EMCommon.SetAllModified<Data.MasterCatalog.FleetGroup>(fleettype, objContainer);
                        objContainer.SaveChanges();

                        objFleetType.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.FleetGroup> Delete(Data.MasterCatalog.FleetGroup fleettype)
        {

            ReturnValue<Data.MasterCatalog.FleetGroup> objFleetType = new ReturnValue<Data.MasterCatalog.FleetGroup>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.AttachTo(typeof(Data.MasterCatalog.FleetGroup).Name, fleettype);
                    objContainer.FleetGroup.DeleteObject(fleettype);

                    objContainer.SaveChanges();
                    objFleetType.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return objFleetType;
        }

        public ReturnValue<Data.MasterCatalog.GetFleetGroupID> GetFleetGroupID(string FleetGroupCD)
        {
            ReturnValue<Data.MasterCatalog.GetFleetGroupID> ReturnValue = new ReturnValue<Data.MasterCatalog.GetFleetGroupID>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetFleetGroupID(CustomerID, FleetGroupCD).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
            return ReturnValue;
        }

        public ReturnValue<Data.MasterCatalog.FleetGroup> GetLock(Data.MasterCatalog.FleetGroup fleettype)
        {
            throw new NotImplementedException();
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }

    }
}

