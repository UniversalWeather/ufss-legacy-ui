﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
    public class CrewAircraftAssignedManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.CrewAircraftAssigned>, IMasterCatalog.ICacheable
    {
        private ExceptionManager exManager;
        /// <summary>
        /// Hook for implmenting getlist functionality
        /// </summary>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> GetList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// To insert selected values from CrewRoster-AircraftAssigned Grid to CrewAircraftAssigned table
        /// </summary>
        /// <param name="crewAircraftAssigned"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> Add(Data.MasterCatalog.CrewAircraftAssigned crewAircraftAssigned)
        {
            ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> CrewAssign = new ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewAircraftAssigned ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AddToCrewAircraftAssigned(crewAircraftAssigned);
                    Container.SaveChanges();
                    CrewAssign.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewAssign;
        }

        /// <summary>
        /// To update selected values from CrewRoster-AircraftAssigned Grid to CrewAircraftAssigned table
        /// </summary>
        /// <param name="crewAircraftAssigned"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> Update(Data.MasterCatalog.CrewAircraftAssigned crewAircraftAssigned)
        {
            ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> CrewAssign = new ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewAircraftAssigned ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.CrewAircraftAssigned.Attach(crewAircraftAssigned);
                    Common.EMCommon.SetAllModified<Data.MasterCatalog.CrewAircraftAssigned>(crewAircraftAssigned, Container);
                    Container.SaveChanges();
                    CrewAssign.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewAssign;
        }

        /// <summary>
        /// To update the selected record as deleted, from CrewRoster-AircraftAssigned Grid to CrewAircraftAssigned table
        /// </summary>
        /// <param name="crewAircraftAssigned"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> Delete(Data.MasterCatalog.CrewAircraftAssigned crewAircraftAssigned)
        {
            ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> CrewAssign = new ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewAircraftAssigned ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.AttachTo(typeof(Data.MasterCatalog.CrewAircraftAssigned).Name, crewAircraftAssigned);
                    Container.CrewAircraftAssigned.DeleteObject(crewAircraftAssigned);
                    Container.SaveChanges();
                    CrewAssign.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewAssign;
        }

        /// <summary>
        /// To fetch the values from the CrewAircraftAssigned table based on parameters
        /// </summary>
        /// <param name="crewAircraftAssigned"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> GetListInfo(Data.MasterCatalog.CrewAircraftAssigned crewAircraftAssigned)
        {
            ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> CrewAssign = new ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewAircraftAssigned ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewAssign.EntityList = Container.GetCrewAircraftAssigned(CustomerID, crewAircraftAssigned.CrewID).ToList();
                    CrewAssign.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewAssign;
        }

        public ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> GetListInfo(Data.MasterCatalog.GetAllCrewAircraftAssign crewAircraftAssigned)
        {
            ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> CrewAssigns = new ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign>();
            
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(crewAircraftAssigned ))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    CrewAssigns.EntityList = Container.GetAllCrewAircraftAssign(CustomerID, crewAircraftAssigned.CrewID).ToList();
                    CrewAssigns.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
	
            return CrewAssigns;
        }

        //public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> GetListInfo(Data.MasterCatalog.CrewAircraftAssigned crewAircraftAssigned)
        //{
        //    ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> CrewAssign = new ReturnValue<Data.MasterCatalog.CrewAircraftAssigned>();
        //    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
        //    Container.ContextOptions.LazyLoadingEnabled = false;
        //    Container.ContextOptions.ProxyCreationEnabled = false;
        //    CrewAssign.EntityList = Container.GetCrewAircraftAssigned(CustomerID, crewAircraftAssigned.CrewID).ToList();
        //    CrewAssign.ReturnFlag = true;
        //    return CrewAssign;
        //}

        //public ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> GetListInfo(Data.MasterCatalog.GetAllCrewAircraftAssign crewAircraftAssigned)
        //{
        //    ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign> CrewAssigns = new ReturnValue<Data.MasterCatalog.GetAllCrewAircraftAssign>();
        //    Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();
        //    Container.ContextOptions.LazyLoadingEnabled = false;
        //    Container.ContextOptions.ProxyCreationEnabled = false;
        //    CrewAssigns.EntityList = Container.GetAllCrewAircraftAssign(CustomerID, crewAircraftAssigned.CrewID).ToList();
        //    CrewAssigns.ReturnFlag = true;
        //    return CrewAssigns;
        //}

        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="crewAircraftAssigned"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.CrewAircraftAssigned> GetLock(Data.MasterCatalog.CrewAircraftAssigned crewAircraftAssigned)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
