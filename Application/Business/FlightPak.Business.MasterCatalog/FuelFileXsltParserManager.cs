﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlightPak.Business.Common;
using FlightPak.Data.MasterCatalog;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.MasterCatalog
{
   public class FuelFileXsltParserManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.FuelFileXsltParser>, IMasterCatalog.ICacheable
    {
        public ExceptionManager exManager;
        public ReturnValue<FuelFileXsltParser> GetList()
        {
            ReturnValue<FuelFileXsltParser> ret = new ReturnValue<FuelFileXsltParser>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    MasterDataContainer cs=new MasterDataContainer();
                    cs.ContextOptions.LazyLoadingEnabled = false;
                    cs.ContextOptions.ProxyCreationEnabled = false;
                    ret.EntityList = cs.FuelFileXsltParser.Where(t => t.IsDeleted == false && t.IsInActive==false).ToList();
                    ret.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<FuelFileXsltParser> Add(FuelFileXsltParser obj)
        {
            ReturnValue<FuelFileXsltParser> ret = new ReturnValue<FuelFileXsltParser>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    obj.IsDeleted = false;
                    ret.ReturnFlag = base.Validate(obj, ref ret.ErrorMessage);
                    if (ret.ReturnFlag)
                    {
                        MasterDataContainer cs=new MasterDataContainer();
                        cs.FuelFileXsltParser.AddObject(obj);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<FuelFileXsltParser> Update(FuelFileXsltParser obj)
        {
            ReturnValue<FuelFileXsltParser> ret = new ReturnValue<FuelFileXsltParser>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret.ReturnFlag = base.Validate(obj, ref ret.ErrorMessage);
                    if (ret.ReturnFlag)
                    {
                        MasterDataContainer cs = new MasterDataContainer();
                        cs.FuelFileXsltParser.Attach(obj);
                        Common.EMCommon.SetAllModified<FuelFileXsltParser>(obj,cs);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<FuelFileXsltParser> Delete(FuelFileXsltParser obj)
        {
            ReturnValue<FuelFileXsltParser> ret = new ReturnValue<FuelFileXsltParser>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret.ReturnFlag = base.Validate(obj, ref ret.ErrorMessage);
                    if (ret.ReturnFlag)
                    {
                        obj.IsDeleted = true;
                        MasterDataContainer cs=new MasterDataContainer();
                        cs.FuelFileXsltParser.Attach(obj);
                        Common.EMCommon.SetAllModified<FuelFileXsltParser>(obj, cs);
                        cs.SaveChanges();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public ReturnValue<FuelFileXsltParser> GetLock(FuelFileXsltParser obj)
        {
            throw new NotImplementedException();
        }

        public ReturnValue<Data.MasterCatalog.FuelFileXsltParser> GetXsltParserByID(int xsltParserId)
        {
            ReturnValue<Data.MasterCatalog.FuelFileXsltParser> ret = new ReturnValue<Data.MasterCatalog.FuelFileXsltParser>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer())
                    {
                        Data.MasterCatalog.MasterDataContainer cs = new Data.MasterCatalog.MasterDataContainer();
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.FuelFileXsltParser.Include("FuelSavedFileFormat.FuelVendor").Where(t => t.FuelFileXsltParserID == xsltParserId && t.IsDeleted == false).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }

        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
