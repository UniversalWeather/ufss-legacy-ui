﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;

namespace FlightPak.Business.MasterCatalog
{
   public  class GetAllDeptAuthManager : BaseManager, IMasterCatalog.IMasterData<Data.MasterCatalog.GetAllDeptAuth>, IMasterCatalog.ICacheable
    {
        /// <summary>
        /// To get the values into DepartmentAuthorization page
        /// </summary>
        /// <returns></returns>
       public ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetList()
        {
            ReturnValue<Data.MasterCatalog.GetAllDeptAuth> ReturnValue = new ReturnValue<Data.MasterCatalog.GetAllDeptAuth>();

            Data.MasterCatalog.MasterDataContainer Container = new Data.MasterCatalog.MasterDataContainer();

            // Fix for recursive infinite loop
            Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.ProxyCreationEnabled = false;
            ReturnValue.EntityList = Container.GetAllDeptAuth(CustomerID,ClientId).ToList();
            ReturnValue.ReturnFlag = true;
            return ReturnValue;
            
        }
        /// <summary>
        /// To insert values from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDeptAuth> Add(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorization)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To update the selected from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDeptAuth> Update(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorization)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// To update the selected record as deleted, from DepartmentAuthorization Grid
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDeptAuth> Delete(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorization)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implementing locking functionality
        /// </summary>
        /// <param name="DepartmentAuthorization"></param>
        /// <returns></returns>
        public ReturnValue<Data.MasterCatalog.GetAllDeptAuth> GetLock(Data.MasterCatalog.GetAllDeptAuth DepartmentAuthorization)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Hook for implmenting cache refresh functionality
        /// </summary>
        public void RefreshCache()
        {
            throw new NotImplementedException();
        }
    }
}
