﻿using System;
using System.Collections.Generic;
using System.Linq;
using FlightPak.Data.Postflight;
using FlightPak.Business.Common;
using System.Globalization;

namespace FlightPak.Business.Postflight
{
    /// <summary>
    /// Postflight Manager class to manage logging Post Trip activities and expenses
    /// </summary>
    public partial class PostflightValidationsManager : BaseManager
    {

        List<long> PaxPassportList = new List<long>();

        #region "Business Errors"
        public ReturnValue<ExceptionTemplate> GetPOBusinessErrorsList(long ModuleID)
        {
            ReturnValue<Data.Postflight.ExceptionTemplate> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ModuleID))
            {

                Results = new ReturnValue<Data.Postflight.ExceptionTemplate>();
                Container = new PostflightDataModelContainer();
                Container.ContextOptions.ProxyCreationEnabled = false;

                Container.ContextOptions.LazyLoadingEnabled = false;
                Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                Container.ContextOptions.ProxyCreationEnabled = false;

                List<ExceptionTemplate> BusinessErrors = (from Exc in Container.ExceptionTemplates
                                                          where (Exc.ModuleID == ModuleID)
                                                          select Exc).ToList();

                Int32[] POBusErrsRef = (Int32[])Enum.GetValues(typeof(POBusinessErrorReference));
                Int32[] POBusErrsRefMod = (Int32[])Enum.GetValues(typeof(POBusinessErrorModule));
                Int32[] POBusErrsRefSubMod = (Int32[])Enum.GetValues(typeof(POBusinessErrorSubModule));

                BusinessErrors.ToList().ForEach(delegate(ExceptionTemplate Exptempl)
                {
                    Exptempl.POErrorReference = (POBusinessErrorReference)POBusErrsRef.Single(ErrorTempl => ErrorTempl == Exptempl.ExceptionTemplateID);
                    Exptempl.POErrorReferenceModule = (POBusinessErrorModule)POBusErrsRefMod.Single(ErrorTempl => ErrorTempl == Exptempl.ModuleID);
                    Exptempl.POErrorReferenceSubModule = (POBusinessErrorSubModule)POBusErrsRefSubMod.Single(ErrorTempl => ErrorTempl == Exptempl.SubModuleID);
                });
                Results.EntityList = BusinessErrors;

                if (Results.EntityList != null && Results.EntityList.Count > 0)
                    Results.ReturnFlag = true;
                else
                    Results.ReturnFlag = false;
            }
            return Results;
        }

        public PostflightTripException BuildBusErrorObject(PostflightTripException postflightTripExcep, ExceptionTemplate exceptionTemplate, PostflightMain Trip)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(postflightTripExcep, exceptionTemplate, Trip))
            {
                postflightTripExcep.POLogID = Trip.POLogID;
                postflightTripExcep.CustomerID = CustomerID;
                postflightTripExcep.ExceptionGroup = exceptionTemplate.SubModuleID.ToString();
                postflightTripExcep.ExceptionDescription = exceptionTemplate.ExceptionTemplate1;
                postflightTripExcep.ExceptionTemplateID = exceptionTemplate.ExceptionTemplateID;
                postflightTripExcep.DisplayOrder = exceptionTemplate.DisplayOrder;
                postflightTripExcep.LastUpdUID = UserPrincipal.Identity.Name;
                //postflightTripExcep.LastUpdTS = DateTime.UtcNow;
                postflightTripExcep.ExceptionTemplate = exceptionTemplate;
                return postflightTripExcep;
            }
        }

        public PostflightTripException BuildBusErrorLegObject(PostflightTripException postflightTripExcep, ExceptionTemplate exceptionTemplate, long POLogID, long legNumber)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(postflightTripExcep, exceptionTemplate, POLogID, legNumber))
            {
                postflightTripExcep.POLogID = POLogID;
                postflightTripExcep.CustomerID = CustomerID;
                postflightTripExcep.ExceptionGroup = exceptionTemplate.SubModuleID.ToString();
                postflightTripExcep.ExceptionDescription = "Leg " + legNumber.ToString() + " : " + exceptionTemplate.ExceptionTemplate1.ToString();
                postflightTripExcep.ExceptionTemplateID = exceptionTemplate.ExceptionTemplateID;
                postflightTripExcep.LastUpdUID = UserPrincipal.Identity.Name;
                postflightTripExcep.LastUpdTS = DateTime.UtcNow;
                postflightTripExcep.ExceptionTemplate = exceptionTemplate;
                postflightTripExcep.DisplayOrder = postflightTripExcep.DisplayOrder;
                return postflightTripExcep;
            }
        }
        #endregion

        #region "Validate Entire Trip"

        /// <summary>
        /// Method to Validate Trip information
        /// </summary>
        /// <returns>Delete Trip status</returns>
        public ReturnValue<PostflightTripException> ValidateBusinessRules(Data.Postflight.PostflightMain trip)
        {
            ReturnValue<PostflightTripException> returnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
            {
                if (trip != null)
                {
                    long POLogID = 0;
                    PostflightLeg[] legsCollection = new PostflightLeg[trip.PostflightLegs.Count];

                    trip.PostflightLegs.CopyTo(legsCollection, 0);

                    legsCollection.ToList().ForEach(delegate(PostflightLeg leg)
                    {
                        leg.PostflightPassengers = null;
                        leg.PostflightSIFLs = null;
                        leg.PostflightCrews = null;
                    });

                    List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                    PostflightDataModelContainer Container = new PostflightDataModelContainer();

                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    returnValue = new ReturnValue<PostflightTripException>();
                    List<ExceptionTemplate> TemplateList = GetPOBusinessErrorsList(10009).EntityList;

                    List<ExceptionTemplate> PO_Main_TemplateList = TemplateList.Where(mainlist => mainlist.POErrorReferenceSubModule == POBusinessErrorSubModule.PostflightMain).ToList();
                    List<ExceptionTemplate> PO_Leg_TemplateList = TemplateList.Where(mainlist => mainlist.POErrorReferenceSubModule != POBusinessErrorSubModule.PostflightMain).ToList();
                    List<ExceptionTemplate> PO_Pax_TemplateList = TemplateList.Where(mainlist => mainlist.POErrorReferenceSubModule == POBusinessErrorSubModule.Passenger).ToList();
                    List<ExceptionTemplate> PO_Crew_TemplateList = TemplateList.Where(mainlist => mainlist.POErrorReferenceSubModule == POBusinessErrorSubModule.Crew).ToList();
                    List<ExceptionTemplate> PO_Expense_TemplateList = TemplateList.Where(mainlist => mainlist.POErrorReferenceSubModule == POBusinessErrorSubModule.Expense).ToList();

                    // Check Validations
                    ExceptionList.AddRange(ValidateMain(trip, PO_Main_TemplateList));

                    // Check Validation in Legs
                    if (trip.PostflightLegs != null && trip.PostflightLegs.Count > 0)
                    {
                        ExceptionList.AddRange(ValidateLegs(trip, trip.PostflightLegs.ToList(), PO_Leg_TemplateList));

                        foreach (PostflightLeg leg in legsCollection)
                        {
                            if (leg.POLogID != null)
                                POLogID = (long)leg.POLogID;

                            //if (leg.PostflightCrews != null && leg.PostflightCrews.ToList().Count > 0)
                            //    ExceptionList.AddRange(ValidateCrews(leg, leg.PostflightCrews.ToList(), PO_Crew_TemplateList));

                            //if (leg.PostflightPassengers != null && leg.PostflightPassengers.ToList().Count > 0)
                            //{
                            //    try
                            //    {
                            //        ExceptionList.AddRange(ValidatePax(leg, leg.PostflightPassengers.ToList(), PO_Pax_TemplateList));
                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        //Manually Handled
                            //    }
                            //}

                            if (leg.PostflightExpenses != null && leg.PostflightExpenses.ToList().Count > 0)
                            {
                                try
                                {
                                    ExceptionList.AddRange(ValidateExpense(POLogID, (long)leg.LegNUM, leg.PostflightExpenses.ToList(), PO_Expense_TemplateList));
                                }
                                catch (Exception ex)
                                {
                                    //Manually Handled
                                }
                            }

                        }
                    }

                    if (returnValue != null)
                    {
                        if (ExceptionList.Count > 0)
                            returnValue.ReturnFlag = true;
                        else
                            returnValue.ReturnFlag = false;

                        returnValue.EntityList = ExceptionList;
                    }
                    PO_Main_TemplateList = null;
                    PO_Leg_TemplateList = null;
                    PO_Pax_TemplateList = null;
                    PO_Crew_TemplateList = null;
                    PO_Expense_TemplateList = null;
                }
                else
                {
                    if (returnValue != null)
                        returnValue.ReturnFlag = false;
                }

                return returnValue;
            }
        }

        /// <summary>
        /// Method to Validate Main information
        /// </summary>       
        private List<PostflightTripException> ValidateMain(PostflightMain trip, List<ExceptionTemplate> PO_Main_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip, PO_Main_TemplateList))
            {
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();

                Data.Postflight.PostflightDataModelContainer Container = new PostflightDataModelContainer();
                Container.ContextOptions.LazyLoadingEnabled = false;
                Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                Container.ContextOptions.ProxyCreationEnabled = false;

                foreach (ExceptionTemplate exception in PO_Main_TemplateList)
                {
                    if (exception.ExceptionTemplateID == (long)POBusinessErrorReference.AircraftTailNoReq) //Aircraft Tail Number is Required. (Severity : 1)
                    {
                        if (trip.FleetID == null || (trip.FleetID != null && trip.FleetID == 0))
                        {
                            ExceptionObj = new PostflightTripException();
                            ExceptionObj.DisplayOrder = "000000000000" + "001";
                            ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, trip));
                        }
                    }

                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.TripRequiresAtleastoneLeg)) //Trip must have at least 1 leg. (Severity : 1)
                    {
                        if (trip.PostflightLegs == null || (trip.PostflightLegs != null && trip.PostflightLegs.Count == 0))
                        {
                            ExceptionObj = new PostflightTripException();
                            ExceptionObj.DisplayOrder = "000000000000" + "002";
                            ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, trip));
                        }
                    }

                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.DupLogNoExist)) //Duplicate Log Number Exists. (Severity : 1)
                    {

                    }

                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.LogDepartmentInactive)) //Trip Department is inactive. (Severity : 2)
                    {
                        if (trip.DepartmentID != null)
                        {
                            long DepartmentID = (long)trip.DepartmentID;
                            var DepartMentList = (from arrLists in Container.Departments
                                                  where (arrLists.DepartmentID == DepartmentID && arrLists.CustomerID == CustomerID)
                                                  select arrLists).ToList();
                            if (DepartMentList != null && DepartMentList.Count > 0)
                            {
                                if (DepartMentList[0].IsInActive == true)
                                {
                                    ExceptionObj = new PostflightTripException();
                                    ExceptionObj.DisplayOrder = "000000000000" + "003";
                                    ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, trip));
                                }
                            }

                        }
                    }

                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.LogAuthorizationInactive)) //Trip Authorization is inactive. (Severity : 2)
                    {
                        if (trip.AuthorizationID != null)
                        {
                            long AuthorizationID = (long)trip.AuthorizationID;
                            var Authorizationlist = (from arrLists in Container.DepartmentAuthorizations
                                                     where (arrLists.AuthorizationID == AuthorizationID && arrLists.CustomerID == CustomerID)
                                                     select arrLists).ToList();
                            if (Authorizationlist != null && Authorizationlist.Count > 0)
                            {
                                if (Authorizationlist[0].IsInActive == true)
                                {
                                    ExceptionObj = new PostflightTripException();
                                    ExceptionObj.DisplayOrder = "000000000000" + "004";
                                    ExceptionList.Add(BuildBusErrorObject(ExceptionObj, exception, trip));
                                }
                            }

                        }
                    }

                }
                return ExceptionList;
            }
        }

        /// <summary>
        /// Method to Validate Legs information
        /// </summary>     
        private List<PostflightTripException> ValidateLegs(PostflightMain trip, List<PostflightLeg> legs, List<ExceptionTemplate> PO_Leg_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip, legs, PO_Leg_TemplateList))
            {
                var DepartAirportLists = new List<Airport>();
                var ArrAirportLists = new List<Airport>();
                var FleetLists = new List<Fleet>();
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                long logID = 0;
                Data.Postflight.PostflightDataModelContainer Container = new PostflightDataModelContainer();
                Container.ContextOptions.LazyLoadingEnabled = false;
                Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                Container.ContextOptions.ProxyCreationEnabled = false;

                if (legs != null)
                {
                    foreach (PostflightLeg leg in legs.Where(x => x.IsDeleted == false).OrderBy(x => x.LegNUM))
                    {
                        if (leg != null)
                        {
                            if (leg.POLogID != null)
                                logID = (long)leg.POLogID;

                            DateTime? scheduleDateTM = leg.ScheduledTM;
                            DateTime? outDateTM = leg.OutboundDTTM;
                            DateTime? inDateTM = leg.InboundDTTM;

                            string offTime = string.Empty; // "00:00";
                            string onTime = string.Empty; // "00:00";

                            DateTime? onDateTM = null;
                            DateTime? offDateTM = null;

                            if (!string.IsNullOrEmpty(leg.TimeOff))
                                offTime = leg.TimeOff.Trim();
                            if (!string.IsNullOrEmpty(leg.TimeOn))
                                onTime = leg.TimeOn.Trim();

                            String length = ("000" + leg.LegNUM.ToString());
                            length = length.Substring(length.Count() - 3, 3);

                            foreach (ExceptionTemplate exception in PO_Leg_TemplateList)
                            {

                                #region Departure Icao Id is Required
                                if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.DepartureICAOIDReq)) //Departure Icao Id is Required. (Severity : 1)
                                {
                                    if (leg.DepartICAOID == null || (leg.DepartICAOID != null && leg.DepartICAOID == 0))
                                    {
                                        ExceptionObj = new PostflightTripException();
                                        ExceptionObj.DisplayOrder = length + "000000001" + "000";
                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                    }
                                }
                                #endregion

                                #region Arrival Icao Id is Required
                                if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.ArrivalICAOIDReq)) //Arrival Icao Id is Required. (Severity : 1)
                                {
                                    if (leg.ArriveICAOID == null || (leg.ArriveICAOID != null && leg.ArriveICAOID == 0))
                                    {
                                        ExceptionObj = new PostflightTripException();
                                        ExceptionObj.DisplayOrder = length + "000000002" + "000";
                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                    }
                                }
                                #endregion

                                #region Fuel Used cannot be a negative value
                                if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.FuelUsedNotNegativeValue)) //Fuel Used cannot be a negative value. (Severity : 1)
                                {
                                    if (leg.FuelUsed != null && leg.FuelUsed < 0)
                                    {
                                        ExceptionObj = new PostflightTripException();
                                        ExceptionObj.DisplayOrder = length + "000000002" + "001";
                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                    }
                                }
                                #endregion

                                // Not Required Exceptions, condition to check based on company profile whether to show warning messages or not. (Seveirty : 2)
                                if (UserPrincipal.Identity.Settings.IsLogsheetWarning == true)
                                {

                                    #region Block out time cannot be earlier than scheduled time
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.BlockTimeEarlierThanSchTime)) //Block out time cannot be earlier than scheduled time. (Severity : 2)
                                    {
                                        if (leg.ScheduledTM != null && leg.OutboundDTTM != null && Convert.ToInt32(UserPrincipal.Identity.Settings.DutyBasis)==2)  // if Duty Basis is Schedule Datetime then show exception
                                        {
                                            scheduleDateTM = leg.ScheduledTM;
                                            outDateTM = leg.OutboundDTTM;

                                            if (outDateTM < scheduleDateTM)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000003" + "000";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }
                                        }

                                    }
                                    #endregion

                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.SchAndBlockTimeDiffExceedsLimit)) //Difference between Scheduled & Block time exceeds system parameters. (Seveirty : 2)
                                    {
                                        //ExceptionObj.DisplayOrder = length + "000000004" + "000";
                                    }
                                  
                                    #region Leg Time Aloft Beyond Aircraft Capability
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.LegTimeAloftBeyondAircraftCapability)) //Leg Time Aloft Beyond Aircraft Capability (Seveirty : 2)
                                    {

                                        if (trip.AircraftID != null)
                                        {
                                            var airCraftLists = (from airCrafts in Container.Aircraft
                                                                 where (airCrafts.AircraftID == (long)trip.AircraftID && airCrafts.CustomerID == CustomerID)
                                                                 select airCrafts).ToList();
                                            if (leg.ScheduledTM != null && airCraftLists != null && airCraftLists.Count > 0)
                                            {
                                                if (leg.FlightHours != null)
                                                {
                                                    if (airCraftLists[0].PowerSetting == "1" && (double)airCraftLists[0].PowerSettings1HourRange < (double)leg.FlightHours)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000006" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                    else if (airCraftLists[0].PowerSetting == "2" && (double)airCraftLists[0].PowerSettings1HourRange < (double)leg.FlightHours)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000006" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                    else if (airCraftLists[0].PowerSetting == "3" && (double)airCraftLists[0].PowerSettings1HourRange < (double)leg.FlightHours)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000006" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    #endregion

                                    #region Departure Airport Inactive
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.DepartureAirportInActive)) //Departure Airport Inactive. (Seveirty : 2)
                                    {

                                        if (leg.DepartICAOID != null)
                                        {
                                            long departAirportId = (long)leg.DepartICAOID;
                                            DepartAirportLists = (from departLists in Container.Airports
                                                                  where (departLists.AirportID == departAirportId)
                                                                  select departLists).ToList();
                                            if (DepartAirportLists != null && DepartAirportLists.Count > 0)
                                            {
                                                if (DepartAirportLists[0].IsInActive == true)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000007" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }

                                    }
                                    #endregion

                                    #region Arrival Airport Inactive
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.ArrivalAirportInActive)) //Arrival Airport Inactive. (Severity : 2)
                                    {

                                        if (leg.ArriveICAOID != null)
                                        {
                                            long arrivalAirportId = (long)leg.ArriveICAOID;
                                            ArrAirportLists = (from arrLists in Container.Airports
                                                               where (arrLists.AirportID == arrivalAirportId)
                                                               select arrLists).ToList();
                                            if (ArrAirportLists != null && ArrAirportLists.Count > 0)
                                            {
                                                if (ArrAirportLists[0].IsInActive == true)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000008" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }

                                    }
                                    #endregion

                                    #region Difference Between On/In Greater Than 30 Minutes
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.DiffBetOnInTimesGreaterThan30Mins)) //Difference Between On/In Greater Than 30 Minutes. (Severity : 2)
                                    {
                                        inDateTM = leg.InboundDTTM;
                                        if (inDateTM != null)
                                        {
                                            DateTime dt = new DateTime(((DateTime)inDateTM).Year, ((DateTime)inDateTM).Month, ((DateTime)inDateTM).Day);
                                            int StartHrs = 0;
                                            int StartMts = 0;

                                            if (!string.IsNullOrEmpty(onTime))
                                            {
                                                StartHrs = Convert.ToInt16(onTime.Substring(0, 2));
                                                StartMts = Convert.ToInt16(onTime.Substring(3, 2));
                                            }
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);

                                            // onDateTM = Convert.ToDateTime(inDateTM.Value.ToShortDateString() + " " + onTime, CultureInfo.InvariantCulture);
                                            onDateTM = dt;

                                            TimeSpan diffTime = inDateTM.Value.Subtract(onDateTM.Value); // DateTime.Compare(inDateTM.Value, onDateTM.Value);

                                            if (diffTime.TotalMinutes > 30)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000009" + "000";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Difference Between Out/Off Greater Than 30 Minutes
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.DiffBetOutOffTimesGreaterThan30Mins)) //Difference Between Out/Off Greater Than 30 Minutes. (Severity : 2)
                                    {
                                        outDateTM = leg.OutboundDTTM;
                                        if (outDateTM != null)
                                        {
                                            DateTime dt = new DateTime(((DateTime)outDateTM).Year, ((DateTime)outDateTM).Month, ((DateTime)outDateTM).Day);
                                            int StartHrs = 0;
                                            int StartMts = 0;
                                            if (!string.IsNullOrEmpty(offTime))
                                            {
                                                StartHrs = Convert.ToInt16(offTime.Substring(0, 2));
                                                StartMts = Convert.ToInt16(offTime.Substring(3, 2));
                                            }
                                            dt = dt.AddHours(StartHrs);
                                            dt = dt.AddMinutes(StartMts);

                                            offDateTM = dt;

                                            //offDateTM = Convert.ToDateTime(outDateTM.Value.ToShortDateString() + " " + offTime, CultureInfo.InvariantCulture);

                                            TimeSpan outDiffTime = offDateTM.Value.Subtract(outDateTM.Value); // DateTime.Compare(offDateTM.Value, outDateTM.Value);

                                            if (outDiffTime.TotalMinutes > 30)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000010" + "000";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }

                                        }
                                    }
                                    #endregion                                   

                                    #region Flight Category Is Missing
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.FlightCatagoryMissing)) //Flight Category Is Missing. (Severity : 2)
                                    {

                                        if (leg.FlightCategoryID == null || (leg.FlightCategoryID != null && leg.FlightCategoryID == 0))
                                        {
                                            ExceptionObj = new PostflightTripException();
                                            ExceptionObj.DisplayOrder = length + "000000014" + "000";
                                            ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                        }

                                    }
                                    #endregion

                                    #region Fuel Out Greater Than Aircraft Maximum Fuel Amount
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.FuelOutGreaterThanAircraftMaxLimit)) //Fuel Out Greater Than Aircraft Maximum Fuel Amount. (Severity : 2)
                                    {
                                        if (trip.FleetID != null)
                                        {
                                            var fleetLists = (from fleets in Container.Fleets
                                                              where (fleets.FleetID == (long)trip.FleetID && fleets.IsDeleted == false && fleets.CustomerID == CustomerID)
                                                              select fleets).ToList();

                                            if (leg.FuelOut != null && fleetLists != null && fleetLists.Count > 0)
                                            {
                                                if (fleetLists[0].MaximumFuel != null && (leg.FuelOut != 0 && leg.FuelOut > fleetLists[0].MaximumFuel)) // Fix for #7531
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000018" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Fuel In Less Than Aircraft Minimum Fuel Amount
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.FuelInLessThanAircraftMinLimit)) //Fuel In Less Than Aircraft Minimum Fuel Amount. (Severity : 2)
                                    {
                                        if (trip.FleetID != null)
                                        {
                                            var fleetLists = (from fleets in Container.Fleets
                                                              where (fleets.FleetID == (long)trip.FleetID && fleets.IsDeleted == false && fleets.CustomerID == CustomerID)
                                                              select fleets).ToList();

                                            if (leg.FuelIn != null && (fleetLists != null && fleetLists.Count > 0))
                                            {
                                                if (fleetLists[0].MinimumFuel != null && (leg.FuelIn != 0 && leg.FuelIn < fleetLists[0].MinimumFuel)) // Fix for #7531
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000019" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Invalid Scheduled Date
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.InvalidScheduledDate)) //Invalid Scheduled Date, if schedule date greater than 60 days (Severity : 2)
                                    {
                                        if (UserPrincipal.Identity.Settings.IsScheduleDTTM == true)
                                        {
                                            DateTime currentDTTM = DateTime.Now;
                                            if (leg.ScheduledTM == null)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000020" + "000";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }
                                            else if (leg.ScheduledTM != null && leg.ScheduledTM != DateTime.MinValue)
                                            {
                                                if ((Convert.ToDateTime(leg.ScheduledTM).Date - currentDTTM.Date).Days > 60 || (Convert.ToDateTime(leg.ScheduledTM).Date - currentDTTM.Date).Days < -60)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000020" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Invalid Out/Off Times
                                    // Fix for #2920
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.InvalidOutOffTimes)) //Invalid Out/Off Times. (Severity : 2)
                                    {
                                        Int32 offHour = 0;
                                        Int32 offMinute = 0;
                                        bool isInvalid = false;

                                        if (leg.OutboundDTTM != null && leg.OutboundDTTM.HasValue && !string.IsNullOrEmpty(offTime))
                                        {
                                            offHour = Convert.ToInt32(offTime.Substring(0, 2));
                                            offMinute = Convert.ToInt32(offTime.Substring(3, 2));

                                            TimeSpan offTimeVal = new TimeSpan(offHour, offMinute, 0);
                                            TimeSpan outTimeVal = leg.OutboundDTTM.Value.TimeOfDay;

                                            if (offTimeVal < outTimeVal)
                                                isInvalid = true;

                                        }

                                        if (isInvalid) // outResult == true || offResult == true)
                                        {
                                            ExceptionObj = new PostflightTripException();
                                            ExceptionObj.DisplayOrder = length + "000000021" + "000";
                                            ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                        }
                                    }
                                    #endregion

                                    #region Invalid On/In Times
                                    // Fix for #2920
                                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.InvalidOnInTimes)) //Invalid On/In Times. (Severity : 2)
                                    {
                                        //bool onResult = false;
                                        //bool inResult = false;
                                        //double inHour = 0;
                                        //double inMinute = 0;
                                        Int32 onHour = 0;
                                        Int32 onMinute = 0;

                                        bool isInvalid = false;

                                        if (leg.InboundDTTM != null && leg.InboundDTTM.HasValue && !string.IsNullOrEmpty(onTime))
                                        {
                                            onHour = Convert.ToInt32(onTime.Substring(0, 2));
                                            onMinute = Convert.ToInt32(onTime.Substring(3, 2));

                                            TimeSpan onTimeVal = new TimeSpan(onHour, onMinute, 0);  // DateTime.ParseExact(onTime, "HH.mm", CultureInfo.InvariantCulture).TimeOfDay;
                                            TimeSpan inTimeVal = leg.InboundDTTM.Value.TimeOfDay;

                                            if (inTimeVal < onTimeVal)
                                                isInvalid = true;
                                        }

                                        
                                        if (isInvalid) // onResult == true || inResult == true)
                                        {
                                            ExceptionObj = new PostflightTripException();
                                            ExceptionObj.DisplayOrder = length + "000000022" + "000";
                                            ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                        }
                                    }
                                    #endregion

                                    #region Leg prev and next ovelap
                                    PostflightLeg NextLeg = legs.Where(x => x.IsDeleted == false && x.LegNUM == leg.LegNUM + 1).SingleOrDefault();

                                    if (NextLeg != null)
                                    {
                                        #region Scheduled Date/Time Conflicts With Next Leg Departure Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.SchDtTmConflictWithNextLegDepTm)) //Scheduled Date/Time Conflicts With Next Leg Departure Time. (Severity : 2)
                                        {
                                            if (NextLeg.OutboundDTTM != null && leg.ScheduledTM != null)
                                            {
                                                if (leg.ScheduledTM >= NextLeg.OutboundDTTM)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000023" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Out Date/Time Conflicts With Next Leg Departure Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.OutDtTmConflictWithNextLegDepTm)) //Out Date/Time Conflicts With Next Leg Departure Time. (Severity : 2)
                                        {
                                            if (NextLeg.OutboundDTTM != null && leg.OutboundDTTM != null)
                                            {
                                                if (leg.OutboundDTTM >= NextLeg.OutboundDTTM)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000024" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Off Date/Time Conflicts With Next Leg Departure Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.OffDtTmConflictWithNextLegDepTm)) //Off Date/Time Conflicts With Next Leg Departure Time. (Severity : 2)
                                        {
                                            if (leg.OutboundDTTM != null)
                                            {
                                                DateTime dt = new DateTime(((DateTime)leg.OutboundDTTM).Year, ((DateTime)leg.OutboundDTTM).Month, ((DateTime)leg.OutboundDTTM).Day);
                                                int StartHrs = 0;
                                                int StartMts = 0;
                                                if (!string.IsNullOrEmpty(offTime))
                                                {
                                                    StartHrs = Convert.ToInt16(offTime.Substring(0, 2));
                                                    StartMts = Convert.ToInt16(offTime.Substring(3, 2));
                                                }
                                                dt = dt.AddHours(StartHrs);
                                                dt = dt.AddMinutes(StartMts);

                                                offDateTM = dt;

                                                //offDateTM = Convert.ToDateTime(leg.OutboundDTTM.Value.ToShortDateString() + " " + offTime, CultureInfo.InvariantCulture);

                                                if (offDateTM != null && NextLeg.OutboundDTTM != null)
                                                {
                                                    if (offDateTM >= NextLeg.OutboundDTTM)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000025" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region On Date/Time Conflicts With Next Leg Departure Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.OnDtTmConflictWithNextLegDepTm)) //On Date/Time Conflicts With Next Leg Departure Time. (Severity : 2)
                                        {
                                            if (leg.InboundDTTM != null)
                                            {
                                                DateTime dt = new DateTime(((DateTime)leg.InboundDTTM).Year, ((DateTime)leg.InboundDTTM).Month, ((DateTime)leg.InboundDTTM).Day);
                                                int StartHrs = 0;
                                                int StartMts = 0;
                                                if (!string.IsNullOrEmpty(onTime))
                                                {
                                                    StartHrs = Convert.ToInt16(onTime.Substring(0, 2));
                                                    StartMts = Convert.ToInt16(onTime.Substring(3, 2));
                                                }
                                                dt = dt.AddHours(StartHrs);
                                                dt = dt.AddMinutes(StartMts);
                                                onDateTM = dt;

                                                //onDateTM = Convert.ToDateTime(leg.InboundDTTM.Value.ToShortDateString() + " " + onTime, CultureInfo.InvariantCulture);

                                                if (onDateTM != null && NextLeg.OutboundDTTM != null)
                                                {
                                                    if (onDateTM >= NextLeg.OutboundDTTM)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000026" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region In Date/Time Conflicts With Next Leg Departure Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.InDtTmConflictWithNextLegDepTm)) //In Date/Time Conflicts With Next Leg Departure Time. (Severity : 2)
                                        {
                                            if (leg.InboundDTTM != null && NextLeg.OutboundDTTM != null)
                                            {
                                                if (leg.InboundDTTM >= NextLeg.OutboundDTTM)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000027" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                        #endregion
                                    }

                                    #endregion

                                    #region Prev and curr leg overlap

                                    PostflightLeg PrevLeg = legs.Where(x => x.IsDeleted == false && x.LegNUM == leg.LegNUM - 1).SingleOrDefault();

                                    if (PrevLeg != null)
                                    {

                                        #region Departure ICAO Different From Last Arrival ICAO
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.DepICAODiffFromLastArrivalICAO)) //Departure ICAO Different From Last Arrival ICAO. (Severity : 2)
                                        {
                                            if (leg.DepartICAOID != null && PrevLeg.ArriveICAOID != null && leg.DepartICAOID != PrevLeg.ArriveICAOID)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000028" + "000";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }
                                        }
                                        #endregion

                                        #region Scheduled Date/Time Conflicts With Previous Leg Arrive Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.SchDtTmConflictWithPrevLegArrTm)) //Scheduled Date/Time Conflicts With Previous Leg Arrive Time. (Severity : 2)
                                        {
                                            if (PrevLeg.InboundDTTM != null && leg.ScheduledTM != null)
                                            {
                                                if (PrevLeg.InboundDTTM >= leg.ScheduledTM)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000029" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Out Date/Time Conflicts With Previous Leg Arrive Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.OutDtTmConflictWithPrevLegArrTm)) //Out Date/Time Conflicts With Previous Leg Arrive Time. (Severity : 2)
                                        {
                                            if (PrevLeg.InboundDTTM != null && leg.OutboundDTTM != null)
                                            {
                                                if (PrevLeg.InboundDTTM >= leg.OutboundDTTM)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000030" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Off Date/Time Conflicts With Previous Leg Arrive Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.OffDtTmConflictWithPrevLegArrTm)) //Off Date/Time Conflicts With Previous Leg Arrive Time. (Severity : 2)
                                        {
                                            if (leg.OutboundDTTM != null)
                                            {
                                                DateTime dt = new DateTime(((DateTime)leg.OutboundDTTM).Year, ((DateTime)leg.OutboundDTTM).Month, ((DateTime)leg.OutboundDTTM).Day);
                                                int StartHrs = 0;
                                                int StartMts = 0;
                                                if (!string.IsNullOrEmpty(offTime))
                                                {
                                                    StartHrs = Convert.ToInt16(offTime.Substring(0, 2));
                                                    StartMts = Convert.ToInt16(offTime.Substring(3, 2));
                                                }
                                                dt = dt.AddHours(StartHrs);
                                                dt = dt.AddMinutes(StartMts);
                                                offDateTM = dt;

                                                //offDateTM = Convert.ToDateTime(leg.OutboundDTTM.Value.ToShortDateString() + " " + offTime, CultureInfo.InvariantCulture);

                                                if (PrevLeg.InboundDTTM != null && offDateTM != null)
                                                {
                                                    if (PrevLeg.InboundDTTM >= offDateTM)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000031" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region On Date/Time Conflicts With Previous Leg Arrive Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.OnDtTmConflictWithPrevLegArrTm)) //On Date/Time Conflicts With Previous Leg Arrive Time. (Severity : 2)
                                        {
                                            if (leg.InboundDTTM != null)
                                            {
                                                DateTime dt = new DateTime(((DateTime)leg.InboundDTTM).Year, ((DateTime)leg.InboundDTTM).Month, ((DateTime)leg.InboundDTTM).Day);
                                                int StartHrs = 0;
                                                int StartMts = 0;
                                                if (!string.IsNullOrEmpty(onTime))
                                                {
                                                    StartHrs = Convert.ToInt16(onTime.Substring(0, 2));
                                                    StartMts = Convert.ToInt16(onTime.Substring(3, 2));
                                                }
                                                dt = dt.AddHours(StartHrs);
                                                dt = dt.AddMinutes(StartMts);
                                                onDateTM = dt;

                                                //onDateTM = Convert.ToDateTime(leg.InboundDTTM.Value.ToShortDateString() + " " + onTime, CultureInfo.InvariantCulture);

                                                if (PrevLeg.InboundDTTM != null && onDateTM != null)
                                                {
                                                    if (PrevLeg.InboundDTTM >= onDateTM)
                                                    {
                                                        ExceptionObj = new PostflightTripException();
                                                        ExceptionObj.DisplayOrder = length + "000000032" + "000";
                                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region In Date/Time Conflicts With Previous Leg Arrive Time
                                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.InDtTmConflictWithPrevLegArrTm)) //In Date/Time Conflicts With Previous Leg Arrive Time. (Severity : 2)
                                        {
                                            if (PrevLeg.InboundDTTM != null && leg.InboundDTTM != null)
                                            {
                                                if (PrevLeg.InboundDTTM >= leg.InboundDTTM)
                                                {
                                                    ExceptionObj = new PostflightTripException();
                                                    ExceptionObj.DisplayOrder = length + "000000033" + "000";
                                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                    #endregion

                                }

                                #region Leg Department is inactive. (Severity : 2)
                                if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.LegDepartmentInactive)) //Leg Department is inactive. (Severity : 2)
                                {
                                    if (leg.DepartmentID != null)
                                    {
                                        long DepartmentID = (long)leg.DepartmentID;
                                        var DepartMentList = (from arrLists in Container.Departments
                                                              where (arrLists.DepartmentID == DepartmentID && arrLists.CustomerID == CustomerID)
                                                              select arrLists).ToList();
                                        if (DepartMentList != null && DepartMentList.Count > 0)
                                        {
                                            if (DepartMentList[0].IsInActive == true)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000034" + "001";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region  Leg Authorization is inactive. (Severity : 2)
                                if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.LegAuthorizationInactive)) //Leg Authorization is inactive. (Severity : 2)
                                {
                                    if (leg.AuthorizationID != null)
                                    {
                                        long AuthorizationID = (long)leg.AuthorizationID;
                                        var Authorizationlist = (from arrLists in Container.DepartmentAuthorizations
                                                                 where (arrLists.AuthorizationID == AuthorizationID && arrLists.CustomerID == CustomerID)
                                                                 select arrLists).ToList();
                                        if (Authorizationlist != null && Authorizationlist.Count > 0)
                                        {
                                            if (Authorizationlist[0].IsInActive == true)
                                            {
                                                ExceptionObj = new PostflightTripException();
                                                ExceptionObj.DisplayOrder = length + "000000035" + "001";
                                                ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, logID, (long)leg.LegNUM));
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }
                return ExceptionList;
            }
        }

        /// <summary>
        /// Method to Validate Crews information
        /// </summary>
        /// <returns>Validate Trip Crew</returns>
        private List<PostflightTripException> ValidateCrews(PostflightLeg leg, List<PostflightCrew> crewList, List<ExceptionTemplate> PO_Crew_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leg, crewList, PO_Crew_TemplateList))
            {
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                List<long> CrewNoPassportCrewlists = new List<long>();

                //foreach (ExceptionTemplate exception in PO_Crew_TemplateList)
                //{
                //    #region "Crew travelling on international leg does not have a passport"

                //    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.CrewTravellingInternationalLegDoesNotHavePassport))
                //    {
                //        if (leg.PostflightCrews != null && leg.DutyTYPE != null && leg.DutyTYPE == 2)
                //        {
                //            foreach (PostflightCrew crew in crewList)
                //            {
                //                bool alreadyExists = false;

                //                if (CrewNoPassportCrewlists.Contains((long)crew.CrewID))
                //                {
                //                    alreadyExists = true;
                //                }

                //                if (alreadyExists == false)
                //                {
                //                    if (crew.PassportID == null || (PrefCrewlist.PassportID != null && (long)PrefCrewlist.PassportID == 0))
                //                    {
                //                        ExceptionObj = new PostflightTripException();

                //                        Crew currentcrew = new Crew();
                //                        currentcrew = (from crewisinactive in Container.Crews
                //                                       where crewisinactive.IsDeleted == false && crewisinactive.CrewID == PrefCrewlist.CrewID
                //                                       select crewisinactive).SingleOrDefault();

                //                        if (currentcrew != null)
                //                        {
                //                            ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                //                            CurrException.TripID = Trip.TripID;
                //                            CurrException.CustomerID = CustomerID;
                //                            string desc = string.Empty;
                //                            desc = exptemp.ExceptionTemplateDescription;
                //                            desc = desc.Replace("<crewcode>", currentcrew.CrewCD);
                //                            CurrException.ExceptionDescription = desc;
                //                            CurrException.ExceptionTemplateID = exptemp.ExceptionTemplateID;
                //                            CurrException.LastUpdUID = UserPrincipal.Identity.Name;
                //                            CurrException.LastUpdTS = DateTime.UtcNow;
                //                            CurrException.ExceptionTemplate = exptemp;
                //                            String lengthstr = ("000" + Preflegs[i].LegNUM.ToString());
                //                            lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                //                            String crewCode = "000000" + currentcrew.CrewCD;
                //                            crewCode = crewCode.Substring(crewCode.Count() - 6, 6);
                //                            CurrException.DisplayOrder = lengthstr + crewCode + "008" + "000";
                //                            //CurrException.DisplayOrder = Preflegs[i].LegNUM + "00" + crewPassengerPassportLists[0].PassportHolderCD + "001" + "000";
                //                            CrewNoPassportCrewlists.Add((long)PrefCrewlist.CrewID);

                //                            ExceptionList.Add(CurrException);
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //    }
                //    #endregion
                //}

                return ExceptionList;
            }
        }

        /// <summary>
        /// Method to Validate Pax information
        /// </summary>
        /// <returns>Validate Trip Pax</returns>
        private List<PostflightTripException> ValidatePax(PostflightLeg leg, List<PostflightPassenger> paxList, List<ExceptionTemplate> PO_Pax_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(leg, paxList, PO_Pax_TemplateList))
            {
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                PostflightDataModelContainer Container = null;

                foreach (ExceptionTemplate exception in PO_Pax_TemplateList)
                {
                    #region "Passenger travelling on international leg does not have a passport"

                    if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.PassengerTravellingInternationalLegDoesNotHavePassport))
                    {
                        if (leg.PostflightPassengers != null && leg.DutyTYPE != null && leg.DutyTYPE == 2)
                        {
                            foreach (PostflightPassenger pax in paxList)
                            {
                                bool alreadyExists = false;

                                if (PaxPassportList != null && PaxPassportList.Count > 0 && pax.PassengerID != null && PaxPassportList.Contains((long)pax.PassengerID))
                                {
                                    alreadyExists = true;
                                }

                                if (alreadyExists == false)
                                {
                                    if (string.IsNullOrEmpty(pax.PassportNUM) || (pax.PassportNUM != null && pax.PassportNUM == "0"))
                                    {
                                        ExceptionObj = new PostflightTripException();
                                        using (Container = new PostflightDataModelContainer())
                                        {
                                            Passenger currentPax = new Passenger();
                                            currentPax = (from paxisinactive in Container.Passengers
                                                          where paxisinactive.IsDeleted == false && paxisinactive.PassengerRequestorID == pax.PassengerID && paxisinactive.CustomerID == CustomerID
                                                          select paxisinactive).SingleOrDefault();

                                            if (currentPax != null && !string.IsNullOrEmpty(currentPax.PassengerRequestorCD))
                                            {

                                                ExceptionTemplate ExceptionTempl = new ExceptionTemplate();
                                                ExceptionObj.POLogID = leg.POLogID;
                                                ExceptionObj.CustomerID = CustomerID;
                                                string desc = string.Empty;
                                                desc = exception.ExceptionTemplate1;
                                                desc = desc.Replace("<paxcode>", currentPax.PassengerRequestorCD);
                                                ExceptionObj.ExceptionDescription = desc;
                                                ExceptionObj.ExceptionGroup = exception.SubModuleID.ToString();
                                                ExceptionObj.ExceptionTemplateID = exception.ExceptionTemplateID;
                                                ExceptionObj.LastUpdUID = UserPrincipal.Identity.Name;
                                                ExceptionObj.LastUpdTS = DateTime.UtcNow;
                                                ExceptionObj.ExceptionTemplate = exception;

                                                String lengthstr = ("000" + leg.LegNUM.ToString());
                                                lengthstr = lengthstr.Substring(lengthstr.Count() - 3, 3);
                                                String paxCode = "000000" + currentPax.PassengerRequestorCD;
                                                paxCode = paxCode.Substring(paxCode.Count() - 6, 6);
                                                ExceptionObj.DisplayOrder = lengthstr + paxCode + "001" + "000";

                                                PaxPassportList.Add((long)pax.PassengerID);

                                                ExceptionList.Add(ExceptionObj);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                return ExceptionList;
            }
        }

        /// <summary>
        /// Method to Validate Expense information
        /// </summary>
        /// <returns>Validate Trip Pax</returns>
        private List<PostflightTripException> ValidateExpense(long POLogID, long LegNum, List<PostflightExpense> expenses, List<ExceptionTemplate> PO_Expense_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(POLogID, LegNum, expenses, PO_Expense_TemplateList))
            {
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();

                if (expenses != null && expenses.Count > 0)
                {
                    foreach (PostflightExpense expense in expenses)
                    {
                        if (expense != null)
                        {
                            String length = ("000" + LegNum.ToString());
                            length = length.Substring(length.Count() - 3, 3);

                            Int32 counter = 0;
                            foreach (ExceptionTemplate exception in PO_Expense_TemplateList)
                            {
                                // Check for Expenses
                                if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.AccountNumberRequired))
                                {
                                    if (expense.AccountID == null || (expense.AccountID != null && expense.AccountID == 0))
                                    {
                                        ExceptionObj = new PostflightTripException();
                                        if (counter.ToString().Length == 1)
                                            ExceptionObj.DisplayOrder = length + "00000000" + counter.ToString() + "000";
                                        else if (counter.ToString().Length == 2)
                                            ExceptionObj.DisplayOrder = length + "0000000" + counter.ToString() + "000";
                                        else if (counter.ToString().Length == 3)
                                            ExceptionObj.DisplayOrder = length + "000000" + counter.ToString() + "000";

                                        ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, POLogID, LegNum));
                                    }
                                }
                            }
                        }
                    }
                }
                return ExceptionList;
            }
        }

        /// <summary>
        /// Method to Validate Expense information
        /// </summary>
        /// <returns>Validate Trip Pax</returns>
        private List<PostflightTripException> ValidateExpenseCatalog(PostflightExpense expense, List<ExceptionTemplate> PO_Expense_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(expense, PO_Expense_TemplateList))
            {
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();

                if (expense != null)
                {
                    foreach (ExceptionTemplate exception in PO_Expense_TemplateList)
                    {
                        // Check for Expenses
                        if (exception.ExceptionTemplateID == (long)(POBusinessErrorReference.AccountNumberRequired))
                        {
                            if (expense.AccountID == null)
                            {
                                ExceptionObj = new PostflightTripException();
                                if (expense.POLogID != null)
                                    ExceptionList.Add(BuildBusErrorLegObject(ExceptionObj, exception, (long)expense.POLogID, (long)expense.LegNUM));
                            }
                        }
                    }

                }
                return ExceptionList;
            }
        }

        /// <summary>
        /// Method to Validate Pax information
        /// </summary>
        /// <returns>Validate Trip Pax</returns>
        private List<PostflightTripException> ValidateOtherCrewDutyRules(List<PostflightSimulatorLog> otherCrewDutyRules, List<ExceptionTemplate> PO_OCDR_TemplateList)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(otherCrewDutyRules, PO_OCDR_TemplateList))
            {
                PostflightTripException ExceptionObj = null;
                List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                return ExceptionList;
            }
        }
        #endregion
    }
}