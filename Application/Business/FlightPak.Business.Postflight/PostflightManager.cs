﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Globalization;
using FlightPak.Data.Postflight;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace FlightPak.Business.Postflight
{
    /// <summary>
    /// Postflight Manager class to manage logging Post Trip activities and expenses
    /// </summary>
    public partial class PostflightManager : BaseManager
    {
        private ExceptionManager exManager;
        StringBuilder HistoryDescription = new System.Text.StringBuilder();
        FlightPak.Framework.Encryption.Crypto Crypting = new FlightPak.Framework.Encryption.Crypto();

        #region "Trip Main"

        /// <summary>
        /// Method to return Trip informations based on Parameters
        /// </summary>
        /// <returns>Postflight Main Trip List</returns>
        public ReturnValue<Data.Postflight.GetPostflightList> GetList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate)
        {
            ReturnValue<Data.Postflight.GetPostflightList> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID, ClientID, FleetID, IsPersonal, IsCompleted, StartDate))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Postflight.GetPostflightList>();
                    using (Container = new PostflightDataModelContainer())
                    {
                        if (ClientID == 0)
                            ClientID = Convert.ToInt64(UserPrincipal.Identity.ClientId);

                        Container.ContextOptions.ProxyCreationEnabled = false;
                        Results.EntityList = Container.GetPostflightList(CustomerID, ClientID, HomeBaseID, FleetID, IsPersonal, IsCompleted, StartDate).ToList();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }

        /// <summary>
        /// Method to get selected Trip informations
        /// </summary>
        /// <param name="TripNumber">Pass Trip Number</param>
        /// <returns>PostflightMain List</returns>
        public ReturnValue<Data.Postflight.PostflightMain> GetTrip(PostflightMain trip)
        {
            ReturnValue<Data.Postflight.PostflightMain> Results = null;
            Data.Postflight.PostflightMain tripMain = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Postflight.PostflightMain>();
                    tripMain = new PostflightMain();
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        if (trip.POLogID > 0)
                        {
                            tripMain = (from trips in Container.PostflightMains
                                        where (trips.POLogID == trip.POLogID && trips.CustomerID == CustomerID && trips.IsDeleted == false)
                                        select trips).FirstOrDefault();
                        }
                        else if (trip.LogNum > 0)
                        {
                            tripMain = (from trips in Container.PostflightMains
                                        where (trips.LogNum == trip.LogNum && trips.CustomerID == CustomerID && trips.IsDeleted == false)
                                        select trips).FirstOrDefault();
                        }
                        else if (trip.TripID > 0)
                        {
                            tripMain = (from trips in Container.PostflightMains
                                        where (trips.TripID == trip.TripID && trips.CustomerID == CustomerID && trips.IsDeleted == false)
                                        select trips).FirstOrDefault();
                        }

                        if (tripMain != null)
                            SetLazyLoadProps_ForChildren(ref tripMain, ref Container);

                        GetLastUpdatedDateTime(ref tripMain);

                        Results.EntityInfo = tripMain;
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return Results;
        }

        /// <summary>
        /// Method to retrieve last updated date time on a Postflight Trip
        /// </summary>
        /// <param name="Trip">Trip Object to search all last updated date</param>
        /// <returns>Last Updated Datetime</returns>
        private void GetLastUpdatedDateTime(ref PostflightMain Trip)
        {
            DateTime lastUpdatedDTTM = DateTime.MinValue;
            DateTime lastUpdDTTMComparer = DateTime.MinValue;
            string lastUpdatedUser = string.Empty;
            string lastUpdatedUserComparer = string.Empty;
            bool IsRecordModified = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Trip))
            {
                if (Trip != null && Trip.LastUpdTS != null)
                {
                    lastUpdatedDTTM = (DateTime)Trip.LastUpdTS;
                    lastUpdatedUser = Trip.LastUpdUID;
                }
                if (Trip != null && Trip.PostflightLegs != null)
                {
                    if (Trip.PostflightLegs.Count > 0)
                    {
                        Trip.PostflightLegs.ToList().ForEach(delegate(PostflightLeg Leg)
                        {
                            if (Leg != null && Leg.LastUpdTS != null)
                            {
                                IsRecordModified = (DateTime)Leg.LastUpdTS > lastUpdatedDTTM ? true : false;

                                if (IsRecordModified)
                                {
                                    lastUpdatedDTTM = (DateTime)Leg.LastUpdTS;
                                    lastUpdatedUser = Leg.LastUpdUID;
                                }
                            }
                            if (Leg != null && Leg.PostflightPassengers != null)
                            {
                                if (Leg.PostflightPassengers.Count > 0)
                                {
                                    var paxObj = Leg.PostflightPassengers.Where(x => x.LastUpdTS != null);
                                    if (paxObj != null && paxObj.Count() > 0)
                                    {
                                        var LastUpdatedObject = paxObj.OrderByDescending(lastUPDT => lastUPDT.LastUpdTS).First();
                                        if (LastUpdatedObject != null)
                                        {
                                            lastUpdDTTMComparer = (DateTime)LastUpdatedObject.LastUpdTS;
                                            IsRecordModified = (DateTime)lastUpdDTTMComparer > lastUpdatedDTTM ? true : false;

                                            if (IsRecordModified)
                                            {
                                                lastUpdatedDTTM = (DateTime)LastUpdatedObject.LastUpdTS;
                                                lastUpdatedUser = LastUpdatedObject.LastUpdUID;
                                            }
                                        }
                                    }
                                }
                            }
                            if (Leg != null && Leg.PostflightSIFLs != null)
                            {
                                if (Leg.PostflightSIFLs.Count > 0)
                                {
                                    var siflObj = Leg.PostflightSIFLs.Where(x => x.LastUpdTS != null);
                                    if (siflObj != null && siflObj.Count() > 0)
                                    {
                                        var LastUpdatedObject = siflObj.OrderByDescending(lastUPDT => lastUPDT.LastUpdTS).First();
                                        if (LastUpdatedObject != null)
                                        {
                                            lastUpdDTTMComparer = (DateTime)LastUpdatedObject.LastUpdTS;
                                            IsRecordModified = (DateTime)lastUpdDTTMComparer > lastUpdatedDTTM ? true : false;

                                            if (IsRecordModified)
                                            {
                                                lastUpdatedDTTM = (DateTime)LastUpdatedObject.LastUpdTS;
                                                lastUpdatedUser = LastUpdatedObject.LastUpdUID;
                                            }
                                        }
                                    }
                                }
                            }
                            if (Leg != null && Leg.PostflightCrews != null)
                            {
                                if (Leg.PostflightCrews.Count > 0)
                                {
                                    var crewObj = Leg.PostflightCrews.Where(x => x.LastUpdTS != null);
                                    if (crewObj != null && crewObj.Count() > 0)
                                    {
                                        var LastUpdatedObject = crewObj.OrderByDescending(lastUPDT => lastUPDT.LastUpdTS).First();
                                        if (LastUpdatedObject != null)
                                        {
                                            lastUpdDTTMComparer = (DateTime)LastUpdatedObject.LastUpdTS;
                                            IsRecordModified = (DateTime)lastUpdDTTMComparer > lastUpdatedDTTM ? true : false;

                                            if (IsRecordModified)
                                            {
                                                lastUpdatedDTTM = (DateTime)LastUpdatedObject.LastUpdTS;
                                                lastUpdatedUser = LastUpdatedObject.LastUpdUID;
                                            }
                                        }
                                    }
                                }
                            }
                            if (Leg != null && Leg.PostflightExpenses != null)
                            {
                                if (Leg.PostflightExpenses.Count > 0)
                                {
                                    var expObj = Leg.PostflightExpenses.Where(x => x.LastUpdTS != null);
                                    if (expObj != null && expObj.Count() > 0)
                                    {
                                        var LastUpdatedObject = expObj.OrderByDescending(lastUPDT => lastUPDT.LastUpdTS).First();
                                        if (LastUpdatedObject != null)
                                        {
                                            lastUpdDTTMComparer = (DateTime)LastUpdatedObject.LastUpdTS;
                                            IsRecordModified = (DateTime)lastUpdDTTMComparer > lastUpdatedDTTM ? true : false;

                                            if (IsRecordModified)
                                            {
                                                lastUpdatedDTTM = (DateTime)LastUpdatedObject.LastUpdTS;
                                                lastUpdatedUser = LastUpdatedObject.LastUpdUID;
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }

                    Trip.TripLastUpdatedOn = lastUpdatedDTTM;
                    Trip.TripLastUpdatedUser = lastUpdatedUser;
                }

            }
        }

        /// <summary>
        /// Method to set load property for referenced objects
        /// </summary>
        /// <param name="trip"></param>
        /// <returns></returns>
        private void SetLazyLoadProps_ForChildren(ref PostflightMain trip, ref PostflightDataModelContainer container)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip, container))
            {
                if (trip.AccountID != null && trip.AccountID > 0) container.LoadProperty(trip, c => c.Account);
                //if (trip.Aircraft != null) container.LoadProperty(trip, c => c.Aircraft);
                if (trip.FleetID != null && trip.FleetID > 0) container.LoadProperty(trip, c => c.Fleet);
                if (trip.HomebaseID != null && trip.HomebaseID > 0) container.LoadProperty(trip, c => c.Company);
                if (trip.Company != null)
                {
                    if (trip.Company.HomebaseAirportID != null && trip.Company.HomebaseAirportID > 0) container.LoadProperty(trip.Company, c => c.Airport); // Modified based on DB Changes at 25-08-2012
                }
                if (trip.ClientID != null && trip.ClientID > 0) container.LoadProperty(trip, c => c.Client);
                if (trip.CustomerID != null && trip.CustomerID > 0) container.LoadProperty(trip, c => c.Customer);
                if (trip.DepartmentID != null && trip.DepartmentID > 0) container.LoadProperty(trip, c => c.Department);
                if (trip.AuthorizationID != null && trip.AuthorizationID > 0) container.LoadProperty(trip, c => c.DepartmentAuthorization);
                if (trip.PassengerRequestorID != null && trip.PassengerRequestorID > 0) container.LoadProperty(trip, c => c.Passenger);
                if (trip.PostflightLegs != null) container.LoadProperty(trip, c => c.PostflightLegs);
                if (trip.PostflightTripExceptions != null) container.LoadProperty(trip, c => c.PostflightTripExceptions);

                foreach (PostflightLeg Leg in trip.PostflightLegs)
                {
                    if (Leg.AccountID != null && Leg.AccountID > 0) container.LoadProperty(Leg, c => c.Account);
                    if (Leg.DepartICAOID != null && Leg.DepartICAOID > 0) container.LoadProperty(Leg, c => c.Airport1);
                    if (Leg.ArriveICAOID != null && Leg.ArriveICAOID > 0) container.LoadProperty(Leg, c => c.Airport);
                    //if (Leg.Company != null) Container.Detach(Leg.Company);
                    if (Leg.ArrivalFBO != null && Leg.ArrivalFBO > 0) container.LoadProperty(Leg, c => c.FBO);
                    if (Leg.DepatureFBO != null && Leg.DepatureFBO > 0) container.LoadProperty(Leg, c => c.FBO1);
                    if (Leg.ClientID != null && Leg.ClientID > 0) container.LoadProperty(Leg, c => c.Client);
                    if (Leg.CrewID != null && Leg.CrewID > 0) container.LoadProperty(Leg, c => c.Crew);
                    if (Leg.CustomerID != null && Leg.CustomerID > 0) container.LoadProperty(Leg, c => c.Customer);
                    if (Leg.DelayTypeID != null && Leg.DelayTypeID > 0) container.LoadProperty(Leg, c => c.DelayType);
                    if (Leg.DepartmentID != null && Leg.DepartmentID > 0) container.LoadProperty(Leg, c => c.Department);
                    if (Leg.AuthorizationID != null && Leg.AuthorizationID > 0) container.LoadProperty(Leg, c => c.DepartmentAuthorization);
                    if (Leg.FlightCategoryID != null && Leg.FlightCategoryID > 0) container.LoadProperty(Leg, c => c.FlightCatagory);
                    if (Leg.PassengerRequestorID != null && Leg.PassengerRequestorID > 0) container.LoadProperty(Leg, c => c.Passenger);

                    if (Leg.PostflightCrews != null) container.LoadProperty(Leg, c => c.PostflightCrews);
                    if (Leg.PostflightExpenses != null) container.LoadProperty(Leg, c => c.PostflightExpenses);
                    if (Leg.PostflightPassengers != null) container.LoadProperty(Leg, c => c.PostflightPassengers);
                    if (Leg.PostflightSIFLs != null) container.LoadProperty(Leg, c => c.PostflightSIFLs);

                    ////swapping legs as per the data loaded from Entity Framework
                    //Airport temp_airport = new Airport();
                    //temp_airport = Leg.Airport1;
                    //Leg.Airport1 = Leg.Airport;
                    //Leg.Airport = temp_airport;

                    foreach (PostflightCrew Crew in Leg.PostflightCrews)
                    {
                        if (Crew.CustomerID != null && Crew.CustomerID > 0) container.LoadProperty(Crew, c => c.Customer);
                        if (Crew.CrewID != null && Crew.CrewID > 0) container.LoadProperty(Crew, c => c.Crew);
                    }
                    foreach (PostflightExpense Expense in Leg.PostflightExpenses)
                    {
                        if (Expense.CustomerID != null && Expense.CustomerID > 0) container.LoadProperty(Expense, c => c.Customer);
                        if (Expense.CrewID != null && Expense.CrewID > 0) container.LoadProperty(Expense, c => c.Crew);
                        if (Expense.AccountID != null && Expense.AccountID > 0) container.LoadProperty(Expense, c => c.Account);
                        if (Expense.AirportID != null && Expense.AirportID > 0) container.LoadProperty(Expense, c => c.Airport);
                        if (Expense.HomebaseID != null && Expense.HomebaseID > 0) container.LoadProperty(Expense, c => c.Company);
                        if (Expense.Company != null)  // Added this for fixing navigation property issue
                        {
                            if (Expense.Company.HomebaseAirportID != null && Expense.Company.HomebaseAirportID > 0) container.LoadProperty(Expense.Company, c => c.Airport);
                        }
                        if (Expense.FBOID != null && Expense.FBOID > 0) container.LoadProperty(Expense, c => c.FBO);
                        if (Expense.FleetID != null && Expense.FleetID > 0) container.LoadProperty(Expense, c => c.Fleet);
                        if (Expense.FlightCategoryID != null && Expense.FlightCategoryID > 0) container.LoadProperty(Expense, c => c.FlightCatagory);
                        if (Expense.FuelLocatorID != null && Expense.FuelLocatorID > 0) container.LoadProperty(Expense, c => c.FuelLocator);
                        if (Expense.PaymentTypeID != null && Expense.PaymentTypeID > 0) container.LoadProperty(Expense, c => c.PaymentType);
                        if (Expense.PaymentVendorID != null && Expense.PaymentVendorID > 0) container.LoadProperty(Expense, c => c.Vendor);
                        //if (Expense.UserMaster != null) container.LoadProperty(Expense, c => c.UserMaster);

                        if (Expense.PostflightNotes != null) container.LoadProperty(Expense, c => c.PostflightNotes);
                    }
                    foreach (PostflightPassenger Passenger in Leg.PostflightPassengers)
                    {
                        if (Passenger.CustomerID != null && Passenger.CustomerID > 0) container.LoadProperty(Passenger, c => c.Customer);
                        if (Passenger.FlightPurposeID != null && Passenger.FlightPurposeID > 0) container.LoadProperty(Passenger, c => c.FlightPurpose);
                        if (Passenger.PassengerID != null && Passenger.PassengerID > 0) container.LoadProperty(Passenger, c => c.Passenger);
                        //if (Passenger.UserMaster != null) container.LoadProperty(Passenger, c => c.UserMaster);
                    }
                    foreach (PostflightSIFL Sifl in Leg.PostflightSIFLs)
                    {
                        if (Sifl.ArriveICAOID != null && Sifl.ArriveICAOID > 0) container.LoadProperty(Sifl, s => s.Airport);
                        if (Sifl.DepartICAOID != null && Sifl.DepartICAOID > 0) container.LoadProperty(Sifl, s => s.Airport1);
                        if (Sifl.CustomerID != null && Sifl.CustomerID > 0) container.LoadProperty(Sifl, s => s.Customer);
                        if (Sifl.FareLevelID != null && Sifl.FareLevelID > 0) container.LoadProperty(Sifl, s => s.FareLevel);
                        if (Sifl.AssociatedPassengerID != null && Sifl.AssociatedPassengerID > 0) container.LoadProperty(Sifl, s => s.Passenger);
                        if (Sifl.PassengerRequestorID != null && Sifl.PassengerRequestorID > 0) container.LoadProperty(Sifl, s => s.Passenger1);
                    }
                }
            }
        }

        /// <summary>
        /// Method to update Trip information
        /// </summary>
        /// <returns>Trip entity</returns>
        public ReturnValue<Data.Postflight.PostflightMain> UpdateTrip(PostflightMain trip)
        {
            ReturnValue<Data.Postflight.PostflightMain> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(trip))
            {
                ReturnValue = new ReturnValue<PostflightMain>();
                try
                {
                    using (Container = new PostflightDataModelContainer())
                    {
                        bool SaveTrip = true;
                        base.CheckLock = true;
                        ReturnValue.ReturnFlag = base.Validate(trip, ref ReturnValue.ErrorMessage);

                        if (ReturnValue.ReturnFlag)
                        {
                            if (trip.PostflightLegs != null && trip.PostflightLegs.Count > 0)
                            {
                                foreach (PostflightLeg Leg in trip.PostflightLegs.Where(x => x.IsDeleted == false).ToList())
                                {
                                    ReturnValue<PostflightLeg> ReturnLegValue = new ReturnValue<PostflightLeg>();
                                    base.CheckLock = false;
                                    ReturnLegValue.ReturnFlag = base.Validate(Leg, ref ReturnLegValue.ErrorMessage);

                                    if (!ReturnLegValue.ReturnFlag)
                                    {
                                        SaveTrip = false;
                                        ReturnLegValue.ErrorMessage = "Leg " + Leg.LegNUM + ": \\n" + ReturnLegValue.ErrorMessage;
                                        ReturnValue.ErrorMessage += ReturnLegValue.ErrorMessage;
                                        break;
                                    }
                                }
                            }
                        }
                        else
                            SaveTrip = false;


                        if (SaveTrip)
                        {
                            trip.CustomerID = CustomerID;
                            if (UserPrincipal.Identity.Name != null)
                                trip.LastUpdUID = UserPrincipal.Identity.Name.Trim();

                            #region EXCEPTIONS

                            // Fix for #7531
                            if (trip.State != TripEntityState.Added && trip.State != TripEntityState.Deleted)
                            {
                                if (trip != null && trip.PostflightTripExceptions != null)
                                {
                                    foreach (PostflightTripException Exception in trip.PostflightTripExceptions.ToList())
                                    {
                                        trip.PostflightTripExceptions.Remove(Exception);
                                    }
                                }
                            }

                            List<PostflightTripException> ExceptionList = new List<PostflightTripException>();
                            PostflightValidationsManager ValidMgr = new PostflightValidationsManager();
                            var validateResult = ValidMgr.ValidateBusinessRules(trip);
                            if (validateResult != null && validateResult.ReturnFlag == true && validateResult.EntityList.Count > 0)
                            {
                                ExceptionList = validateResult.EntityList;

                                // Bind the Exception List into Trip Object
                                if (ExceptionList.Count > 0)
                                {
                                    trip.IsException = true;

                                    foreach (PostflightTripException Exception in ExceptionList)
                                    {
                                        PostflightTripException ExceptionObj = new PostflightTripException();
                                        ExceptionObj.ExceptionGroup = GetSubmoduleModule(Convert.ToInt64(Exception.ExceptionGroup));
                                        ExceptionObj.ExceptionDescription = Exception.ExceptionDescription;
                                        ExceptionObj.ExceptionTemplateID = Exception.ExceptionTemplateID;
                                        ExceptionObj.DisplayOrder = Exception.DisplayOrder;
                                        trip.PostflightTripExceptions.Add(ExceptionObj);
                                    }
                                }
                                else
                                {
                                    trip.IsException = false;
                                }
                            }
                            else
                            {
                                trip.IsException = false;
                            }

                            #endregion

                            //Update parent-child references    
                            //UpdateParentChildLinks(ref trip);

                            Container.ContextOptions.LazyLoadingEnabled = false;
                            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                            Container.ContextOptions.ProxyCreationEnabled = false;

                            //Update operation goes here
                            UpdateObjectState(ref trip, ref Container);

                            Container.SaveChanges();

                            LockManager<Data.Postflight.PostflightMain> lockManager = new LockManager<PostflightMain>();
                            lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Postflight.PostflightMain, trip.POLogID);

                            //Attach tripBusinessErrors object to Trip object and return the business errors status
                            //Return Trip updtae status   
                            PostflightMain ret = new PostflightMain();
                            ret.POLogID = trip.POLogID;
                            ReturnValue.EntityInfo = ret;
                            ReturnValue.ReturnFlag = true;
                        }
                        else
                        {
                            ReturnValue.ReturnFlag = false;
                            ReturnValue.EntityInfo = trip;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    if (ex.Message.Contains("The changes to the database were committed successfully"))
                    {
                        PostflightMain ret = new PostflightMain();
                        ret.POLogID = trip.POLogID;
                        ReturnValue.EntityInfo = ret;
                        ReturnValue.ReturnFlag = true;
                    }
                    else
                    {
                        if (ex.InnerException != null)
                        {
                            ReturnValue.ErrorMessage = ex.InnerException.Message;
                            Logger.Write(String.Format("Error :Postflight Update Log , will still return:{0}", ex.InnerException.Message), FlightPak.Common.Constants.Policy.DataLayer);
                        }
                        else
                        {
                            Logger.Write(String.Format("Error :Postflight Update Log , will still return:{0}", ex.ToString()), FlightPak.Common.Constants.Policy.DataLayer);
                            ReturnValue.ErrorMessage = ex.Message;
                        }
                        ReturnValue.ReturnFlag = false;
                        //ReturnValue.EntityInfo = null;
                    }
                }

                return ReturnValue;
            }
        }

        /// <summary>
        /// Method to delete a Existing Trip Exceptions, before adding new one.
        /// </summary>
        /// <param name="SourceObject">Trip Object to Delete</param>        
        public ReturnValue<Data.Postflight.PostflightTripException> DeleteExistingTripExceptions(long POLogID, long POTripExceptionID)
        {
            ReturnValue<Data.Postflight.PostflightTripException> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(POLogID, POTripExceptionID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<PostflightTripException>();
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;

                        if (Container.PostflightTripExceptions != null)
                        {
                            PostflightTripException ExistingPOException = Container.PostflightTripExceptions
                               .Where(poException => poException.POTripExceptionID == POTripExceptionID && poException.POLogID == POLogID).SingleOrDefault();

                            if (ExistingPOException != null)
                                Container.PostflightTripExceptions.DeleteObject(ExistingPOException);
                            Container.SaveChanges();
                        }

                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// Method for Deleting records, which has been set as delete flag from UI
        /// </summary>
        /// <param name="trip"></param>
        /// <param name="Container"></param>
        private void UpdateDeletedState(List<Int64> DeletedLegIDs, Dictionary<Int64, Int64> DeletedCrewIDs, Dictionary<Int64, Int64> DeletedPAXIDs, Dictionary<Int64, Int64> DeletedExpenseIDs, Dictionary<Int64, Int64> DeletedSIFLIDs, Dictionary<Int64, Int64> DeletedExceptionIDs, ref PostflightDataModelContainer Container)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(DeletedLegIDs, DeletedCrewIDs, DeletedPAXIDs, DeletedExpenseIDs, DeletedSIFLIDs, DeletedExceptionIDs, Container))
            {
                if (DeletedLegIDs != null)
                {
                    PostflightLeg ExistingPOLegEntity = null;
                    foreach (Int64 DeletedLegID in DeletedLegIDs)
                    {
                        ExistingPOLegEntity = Container.PostflightLegs
                       .Where(poLeg => poLeg.POLegID == DeletedLegID).SingleOrDefault();

                        if (ExistingPOLegEntity != null)
                        {
                            #region "History For Deleted Leg"

                            Airport objDepartAirport = Container.Airports.Where(x => x.AirportID == ExistingPOLegEntity.DepartICAOID).SingleOrDefault();
                            Airport objArriveAirport = Container.Airports.Where(x => x.AirportID == ExistingPOLegEntity.ArriveICAOID).SingleOrDefault();

                            HistoryDescription.AppendLine(string.Format("Deleted Leg {0} Depart: {1} Arrive: {2} Schedule Date/Time: {3}", ExistingPOLegEntity.LegNUM, objDepartAirport.IcaoID, objArriveAirport.IcaoID, ExistingPOLegEntity.ScheduledTM));

                            #endregion

                            Container.DeleteObject(ExistingPOLegEntity);
                        }
                    }
                }
                if (DeletedExpenseIDs != null)
                {
                    bool IsDeleteExpense = false;
                    PostflightExpense ExistingPOexpenseEntity = null;

                    foreach (KeyValuePair<Int64, Int64> DeletedExpense in DeletedExpenseIDs)
                    {
                        if (DeletedLegIDs == null)
                            IsDeleteExpense = true;
                        if (DeletedLegIDs != null && DeletedLegIDs.Contains(DeletedExpense.Value) == false)
                            IsDeleteExpense = true;

                        if (IsDeleteExpense)
                        {
                            ExistingPOexpenseEntity = Container.PostflightExpenses
                                     .Where(poExpense => poExpense.PostflightExpenseID == DeletedExpense.Key).SingleOrDefault();

                            if (ExistingPOexpenseEntity != null)
                            {
                                #region "History For Deleted Expenses"

                                HistoryDescription.AppendLine(string.Format("Slip Deleted On Leg {0}  Slip No: {1}", ExistingPOexpenseEntity.LegNUM, ExistingPOexpenseEntity.SlipNUM));

                                #endregion

                                Container.PostflightExpenses.DeleteObject(ExistingPOexpenseEntity);
                            }
                        }
                    }
                }

                if (DeletedPAXIDs != null)
                {
                    bool IsDeletePax = false;
                    PostflightPassenger ExistingPOPassengerEntity = null;
                    foreach (KeyValuePair<Int64, Int64> DeletedPAX in DeletedPAXIDs)
                    {
                        if (DeletedLegIDs == null)
                            IsDeletePax = true;
                        if (DeletedLegIDs != null && DeletedLegIDs.Contains(DeletedPAX.Value) == false)
                            IsDeletePax = true;

                        if (IsDeletePax)
                        {
                            ExistingPOPassengerEntity = Container.PostflightPassengers
                                .Where(poPassenger => poPassenger.PostflightPassengerListID == DeletedPAX.Key).SingleOrDefault();

                            if (ExistingPOPassengerEntity != null)
                            {
                                #region "History For Deleted Passenger"

                                Passenger objPassenger = Container.Passengers.Where(x => x.PassengerRequestorID == ExistingPOPassengerEntity.PassengerID).SingleOrDefault();
                                if(objPassenger!=null)  // It was null when a BLCOKED# marked passenger was delete from list.
                                    HistoryDescription.AppendLine(string.Format("Passenger Deleted On Leg {0}  {1}", ExistingPOPassengerEntity.PostflightLeg.LegNUM, objPassenger.PassengerName + " " + objPassenger.PassengerRequestorCD));
                                else if(ExistingPOPassengerEntity!=null)
                                    HistoryDescription.AppendLine(string.Format("Passenger Deleted On Leg {0}  {1}", ExistingPOPassengerEntity.PostflightLeg.LegNUM, ExistingPOPassengerEntity.PassengerFirstName));


                                #endregion

                                Container.PostflightPassengers.DeleteObject(ExistingPOPassengerEntity);
                            }
                        }
                    }
                }

                if (DeletedCrewIDs != null)
                {
                    bool IsDeleteCrew = false;
                    PostflightCrew ExistingPOCrewEntity = null;

                    foreach (KeyValuePair<Int64, Int64> DeletedCrew in DeletedCrewIDs)
                    {
                        if (DeletedLegIDs == null)
                            IsDeleteCrew = true;
                        if (DeletedLegIDs != null && DeletedLegIDs.Contains(DeletedCrew.Value) == false)
                            IsDeleteCrew = true;

                        if (IsDeleteCrew)
                        {
                            ExistingPOCrewEntity = Container.PostflightCrews
                            .Where(poCrew => poCrew.PostflightCrewListID == DeletedCrew.Key).SingleOrDefault();

                            if (ExistingPOCrewEntity != null)
                            {
                                #region "History For Deleted Crew"

                                HistoryDescription.AppendLine(string.Format("Crew Deleted On Leg {0} Crew Code: {1}", ExistingPOCrewEntity.PostflightLeg.LegNUM, ExistingPOCrewEntity.CrewCode));

                                #endregion

                                Container.PostflightCrews.DeleteObject(ExistingPOCrewEntity);
                            }
                        }
                    }
                }

                if (DeletedSIFLIDs != null)
                {
                    bool IsDeleteSIFL = false;
                    PostflightSIFL ExistingPOSIFLEntity = null;

                    foreach (KeyValuePair<Int64, Int64> DeletedSIFL in DeletedSIFLIDs)
                    {
                        if (DeletedLegIDs == null)
                            IsDeleteSIFL = true;
                        if (DeletedLegIDs != null && DeletedLegIDs.Contains(DeletedSIFL.Value) == false)
                            IsDeleteSIFL = true;

                        if (IsDeleteSIFL)
                        {
                            ExistingPOSIFLEntity = Container.PostflightSIFLs
                                .Where(poSIFL => poSIFL.POSIFLID == DeletedSIFL.Key).SingleOrDefault();

                            if (ExistingPOSIFLEntity != null)
                                Container.PostflightSIFLs.DeleteObject(ExistingPOSIFLEntity);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to save object state relation
        /// </summary>
        /// <param name="SourceObject">Trip Object to set state</param>        
        private void UpdateParentChildLinks(ref PostflightMain SourceObject)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SourceObject))
            {
                long logID = SourceObject.POLogID;
                long legID = 0;
                long expenseID = 0;

                if (logID > 0)
                {
                    SourceObject.PostflightLegs.ToList().ForEach(obj => obj.POLogID = logID);

                    SourceObject.PostflightLegs.ToList().ForEach(delegate(PostflightLeg Leg)
                    {
                        legID = Leg.POLegID;
                        Leg.PostflightExpenses.ToList().ForEach(obj => obj.POLogID = logID);
                        Leg.PostflightExpenses.ToList().ForEach(obj => obj.POLegID = legID);

                        Leg.PostflightPassengers.ToList().ForEach(obj => obj.POLogID = logID);
                        Leg.PostflightPassengers.ToList().ForEach(obj => obj.POLegID = legID);

                        Leg.PostflightCrews.ToList().ForEach(obj => obj.POLogID = logID);
                        Leg.PostflightCrews.ToList().ForEach(obj => obj.POLegID = legID);

                        // Added for SIFL
                        Leg.PostflightSIFLs.ToList().ForEach(obj => obj.POLogID = logID);
                        Leg.PostflightSIFLs.ToList().ForEach(obj => obj.POLegID = legID);

                    });

                    SourceObject.PostflightLegs.ToList().ForEach(delegate(PostflightLeg Leg)
                    {
                        Leg.PostflightExpenses.ToList().ForEach(delegate(PostflightExpense expense)
                        {
                            if (expense != null)
                            {
                                expenseID = expense.PostflightExpenseID;
                                expense.PostflightNotes.ToList().ForEach(obj => obj.PostflightExpenseID = expenseID);
                            }
                        });
                    });


                    SourceObject.PostflightTripExceptions.ToList().ForEach(obj => obj.POLogID = logID);

                }
            }
        }

        /// <summary>
        /// Method to save object state to either added or modified for each object
        /// </summary>
        /// <param name="SourceObject">Trip Object to set state</param>
        /// <param name="Container">Entity Framework Container object to save the state</param>
        private void UpdateObjectState(ref PostflightMain SourceObject, ref PostflightDataModelContainer Container)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SourceObject, Container))
            {
                #region MAIN
                
                PreflightMain tripMain=new PreflightMain();
                long POTripID = SourceObject.POLogID;
                PostflightMain ExistingPOMainEntity = Container.PostflightMains.Where(poMain => poMain.POLogID == POTripID).SingleOrDefault();
                if (SourceObject.TripID != null)
                {
                    long tripId = (long)SourceObject.TripID;
                    tripMain = (from trips in Container.PreflightMains
                                where (trips.TripID == tripId && trips.IsDeleted == false)
                                select trips).FirstOrDefault();
                }
                
                if (ExistingPOMainEntity != null)
                    Container.LoadProperty(ExistingPOMainEntity, e => e.PostflightTripExceptions);

                if (SourceObject.State == Data.Postflight.TripEntityState.Added)
                {
                    SourceObject.IsDeleted = false;
                    
                    Container.PostflightMains.AddObject(SourceObject);
                    #region "History for New Log"
                    
                    if (SourceObject.IsPostedFromPreflight)
                        HistoryDescription.AppendLine("Logged from Preflight Trip: " + tripMain.TripNUM);
                    else if (SourceObject.IsTripToSaveFromPreflightLogEvent) // This Property we are using to identify if Log created via Postflight > RetreiveTrip
                        HistoryDescription.AppendLine("New Log Retrieved from Trip: " + tripMain.TripNUM); 
                    else
                        HistoryDescription.AppendLine("New Log Added"); //: " + POTripID.ToString());
                    #endregion
                }
                else if (SourceObject.State == Data.Postflight.TripEntityState.Modified)
                {
                    Container.PostflightMains.Attach(ExistingPOMainEntity);
                    Container.PostflightMains.ApplyCurrentValues(SourceObject);

                    #region "History For Modified Log"

                    var originalValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOMainEntity).OriginalValues;
                    var CurrentValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOMainEntity).CurrentValues;
                    PropertyInfo[] properties = typeof(PostflightMain).GetProperties();

                    for (int i = 0; i < originalValues.FieldCount; i++)
                    {
                        var fieldname = originalValues.GetName(i);

                        object value1 = originalValues.GetValue(i);
                        object value2 = CurrentValues.GetValue(i);

                        #region "DispatchNum"
                        if (fieldname.ToLower() == "dispatchnum")
                        {
                            string OldDispatchNum = string.Empty;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldDispatchNum = value1.ToString();

                            string NewDispatchNum = string.Empty;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewDispatchNum = value2.ToString();

                            if (OldDispatchNum == string.Empty && NewDispatchNum != string.Empty)
                                HistoryDescription.AppendLine(string.Format("New Dispatch Number Added: {0} ", NewDispatchNum));
                            else if (OldDispatchNum != NewDispatchNum)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Dispatch Number",
                                    (OldDispatchNum != null ? OldDispatchNum : string.Empty),
                                    (NewDispatchNum != null ? NewDispatchNum : string.Empty))
                                    );
                        }
                        #endregion

                        #region "HomeBase"

                        if (fieldname.ToLower() == "homebaseid")
                        {
                            Int64 OldHomebaseID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldHomebaseID = Convert.ToInt64(value1);

                            Int64 NewHomebaseID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewHomebaseID = Convert.ToInt64(value2);

                            Company OldCompany = Container.Companies.Where(x => x.HomebaseID == OldHomebaseID).SingleOrDefault();
                            Company NewCompany = Container.Companies.Where(x => x.HomebaseID == NewHomebaseID).SingleOrDefault();

                            string OldAirportID = string.Empty;
                            string NewAirportID = string.Empty;
                            if (OldCompany != null)
                            {
                                Airport OldAirport = Container.Airports.Where(x => x.AirportID == OldCompany.HomebaseAirportID).SingleOrDefault();
                                if (OldAirport != null)
                                {
                                    OldAirportID = OldAirport.IcaoID;
                                }
                            }

                            if (NewCompany != null)
                            {
                                Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewCompany.HomebaseAirportID).SingleOrDefault();
                                if (NewAirportID != null)
                                {
                                    NewAirportID = NewAirport.IcaoID;
                                }
                            }

                            if (!string.IsNullOrEmpty(OldAirportID) && !string.IsNullOrEmpty(NewAirportID) && !OldAirportID.Equals(NewAirportID))
                            {
                                HistoryDescription.AppendLine(string.Format("Homebase Changed From {0} To {1}", OldAirportID, NewAirportID));
                            }
                        }


                        #endregion

                        #region "Flight Number"

                        if (fieldname.ToLower() == "flightnum")
                        {
                            string OldFlightNum = string.Empty;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldFlightNum = value1.ToString();

                            string NewFlightNum = string.Empty;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewFlightNum = value2.ToString();

                            if (OldFlightNum == string.Empty && NewFlightNum != string.Empty)
                                HistoryDescription.AppendLine(string.Format("New Flight Number Added: {0} ", NewFlightNum));
                            else if (OldFlightNum != NewFlightNum)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Flight Number",
                                    (OldFlightNum != null ? OldFlightNum : string.Empty),
                                    (NewFlightNum != null ? NewFlightNum : string.Empty))
                                    );
                        }

                        #endregion

                        #region "Client"

                        if (fieldname.ToLower() == "clientid")
                        {
                            Int64 OldClientID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldClientID = Convert.ToInt64(value1);

                            Int64 NewClientID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewClientID = Convert.ToInt64(value2);

                            Client OldClient = Container.Clients.Where(x => x.ClientID == OldClientID).SingleOrDefault();
                            Client NewClient = Container.Clients.Where(x => x.ClientID == NewClientID).SingleOrDefault();

                            if (OldClientID == 0 && NewClientID != 0)
                                HistoryDescription.AppendLine(string.Format("New Client Code Added: {0} ", NewClient.ClientCD));
                            else if (OldClientID != NewClientID)
                                HistoryDescription.AppendLine(string.Format("{0} Changed From {1} To {2}", "Client Code",
                                    (OldClient != null ? OldClient.ClientCD : string.Empty),
                                    (NewClient != null ? NewClient.ClientCD : string.Empty)));
                        }


                        #endregion

                        #region "Tail Number"

                        if (fieldname.ToLower() == "fleetid")
                        {
                            Int64 OldFleetID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldFleetID = Convert.ToInt64(value1);

                            Int64 NewFleetID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewFleetID = Convert.ToInt64(value2);

                            Fleet OldFleet = Container.Fleets.Where(x => x.FleetID == OldFleetID).SingleOrDefault();
                            Fleet NewFleet = Container.Fleets.Where(x => x.FleetID == NewFleetID).SingleOrDefault();

                            if (OldFleetID == 0 && NewFleetID != 0)
                                HistoryDescription.AppendLine(string.Format("New Tail Number Added: {0} ", NewFleet.TailNum));
                            else if (OldFleetID != NewFleetID)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Tail Number",
                                    (OldFleet != null ? OldFleet.TailNum : string.Empty),
                                    (NewFleet != null ? NewFleet.TailNum : string.Empty))
                                    );
                        }

                        #endregion

                        #region "Completed"
                        if (fieldname.ToLower() == "iscompleted")
                        {
                            bool OldIsCompleted = false;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldIsCompleted = (bool)value1;

                            bool NewIsCompleted = false;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewIsCompleted = (bool)value2;

                            if (OldIsCompleted != NewIsCompleted)
                                HistoryDescription.AppendLine(string.Format("{0} marked as {1} To {2}", "Log",
                                    (((bool)OldIsCompleted) ? "Checked" : "Unchecked"),
                                    (((bool)NewIsCompleted) ? "Checked" : "Unchecked")));
                        }
                        #endregion

                        #region "Requestor"

                        if (fieldname.ToLower() == "passengerrequestorid")
                        {
                            Int64 OldPassengerID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldPassengerID = Convert.ToInt64(value1);

                            Int64 NewPassengerID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewPassengerID = Convert.ToInt64(value2);

                            Passenger Oldpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == OldPassengerID).SingleOrDefault();
                            Passenger Newpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == NewPassengerID).SingleOrDefault();

                            if (OldPassengerID == 0 && NewPassengerID != 0)
                                HistoryDescription.AppendLine(string.Format("New Requestor Code Added: {0} ", Newpassenger.PassengerRequestorCD));
                            else if (OldPassengerID != NewPassengerID)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Requestor Code",
                                    (Oldpassenger != null ? Oldpassenger.PassengerRequestorCD : string.Empty),
                                    (Newpassenger != null ? Newpassenger.PassengerRequestorCD : string.Empty)));
                        }

                        #endregion

                        #region "Account"

                        if (fieldname.ToLower() == "accountid")
                        {
                            Int64 OldAccountID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAccountID = Convert.ToInt64(value1);

                            Int64 NewAccountID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAccountID = Convert.ToInt64(value2);

                            Account OldAccount = Container.Accounts.Where(x => x.AccountID == OldAccountID).SingleOrDefault();
                            Account NewAccount = Container.Accounts.Where(x => x.AccountID == NewAccountID).SingleOrDefault();

                            if (OldAccountID == 0 && NewAccountID != 0)
                                HistoryDescription.AppendLine(string.Format("New Account Number Added: {0} ", NewAccount.AccountNum));
                            else if (OldAccountID != NewAccountID)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Account Number",
                                    (OldAccount != null ? OldAccount.AccountNum : string.Empty),
                                    (NewAccount != null ? NewAccount.AccountNum : string.Empty)));
                        }
                        #endregion

                        #region "Department"

                        if (fieldname.ToLower() == "departmentid")
                        {
                            Int64 OldDepartmentID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldDepartmentID = Convert.ToInt64(value1);

                            Int64 NewDepartmentID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewDepartmentID = Convert.ToInt64(value2);

                            Department OldDepartment = Container.Departments.Where(x => x.DepartmentID == OldDepartmentID).SingleOrDefault();
                            Department NewDepartment = Container.Departments.Where(x => x.DepartmentID == NewDepartmentID).SingleOrDefault();

                            if (OldDepartmentID == 0 && NewDepartmentID != 0)
                                HistoryDescription.AppendLine(string.Format("New Department Code Added: {0} ", NewDepartment.DepartmentCD));
                            else if (OldDepartmentID != NewDepartmentID)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Department Code",
                                    (OldDepartment != null ? OldDepartment.DepartmentCD : string.Empty),
                                    (NewDepartment != null ? NewDepartment.DepartmentCD : string.Empty)));

                        }


                        #endregion

                        #region "Authorization"

                        if (fieldname.ToLower() == "authorizationid")
                        {
                            Int64 OldAuthorizationID = 0;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldAuthorizationID = Convert.ToInt64(value1);

                            Int64 NewAuthorizationID = 0;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewAuthorizationID = Convert.ToInt64(value2);

                            DepartmentAuthorization OldAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == OldAuthorizationID).SingleOrDefault();
                            DepartmentAuthorization NewAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == NewAuthorizationID).SingleOrDefault();

                            if (OldAuthorizationID == 0 && NewAuthorizationID != 0)
                                HistoryDescription.AppendLine(string.Format("New Authorization Code Added: {0} ", NewAuthorization.AuthorizationCD));
                            else if (OldAuthorizationID != NewAuthorizationID)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Authorization Code",
                                    (OldAuthorization != null ? OldAuthorization.AuthorizationCD : string.Empty),
                                    (NewAuthorization != null ? NewAuthorization.AuthorizationCD : string.Empty)));
                        }


                        #endregion

                        #region "Description"
                        if (fieldname.ToLower() == "pomaindescription")
                        {
                            string OldDescription = string.Empty;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldDescription = value1.ToString();

                            string NewDescription = string.Empty;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewDescription = value2.ToString();

                            if (OldDescription == string.Empty && NewDescription != string.Empty)
                                HistoryDescription.AppendLine(string.Format("New Description Added: {0} ", NewDescription));
                            else if (OldDescription != NewDescription)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Description",
                                    (OldDescription != null ? OldDescription : string.Empty),
                                    (NewDescription != null ? NewDescription : string.Empty))
                                    );
                        }
                        #endregion

                        #region "Notes"
                        if (fieldname.ToLower() == "notes")
                        {
                            string OldNotes = string.Empty;
                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                OldNotes = value1.ToString();

                            string NewNotes = string.Empty;
                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                NewNotes = value2.ToString();

                            if (OldNotes == string.Empty && NewNotes != string.Empty)
                                HistoryDescription.AppendLine(string.Format("New Notes Added: {0} ", NewNotes));
                            else if (OldNotes != NewNotes)
                                HistoryDescription.AppendLine(string.Format("{0}  Changed From {1} To {2}", "Notes",
                                    (OldNotes != null ? OldNotes : string.Empty),
                                    (NewNotes != null ? NewNotes : string.Empty))
                                    );
                        }
                        #endregion
                    }
                    #endregion
                }
                else
                {
                    Container.PostflightMains.Attach(ExistingPOMainEntity);
                }
                #endregion

                #region LEGS

                if (SourceObject.PostflightLegs != null && SourceObject.PostflightLegs.Count > 0)
                {
                    foreach (PostflightLeg Leg in SourceObject.PostflightLegs.ToList())
                    {
                        long POLegID = (long)Leg.POLegID;
                        Leg.POLogID = SourceObject.POLogID;
                        Leg.CustomerID = CustomerID;
                        Leg.LastUpdUID = SourceObject.LastUpdUID;

                        PostflightLeg ExistingPOLegEntity = null;

                        switch (Leg.State)
                        {
                            case Data.Postflight.TripEntityState.Added:
                                {
                                    if (ExistingPOMainEntity != null)
                                        ExistingPOMainEntity.PostflightLegs.Add(Leg);
                                    Container.ObjectStateManager.ChangeObjectState(Leg, System.Data.EntityState.Added);

                                    #region "History for New Leg"
                                    HistoryDescription.AppendLine("New Leg(s) added to Log with Details :");
                                    Airport DepAiport = new Airport();
                                    Airport ArrAirport = new Airport();
                                    if (Leg.DepartICAOID != null)
                                        DepAiport = Container.Airports.Where(x => x.AirportID == Leg.DepartICAOID).SingleOrDefault();
                                    if (Leg.ArriveICAOID != null)
                                        ArrAirport = Container.Airports.Where(x => x.AirportID == Leg.ArriveICAOID).SingleOrDefault();

                                    if (DepAiport != null && ArrAirport != null && Leg.ScheduledTM != null)
                                        HistoryDescription.AppendLine(string.Format("Leg Number: {0} Departs ICAO: {1} Arrive ICAO: {2} Schedule Date/Time: {3}", Leg.LegNUM, DepAiport.IcaoID, ArrAirport.IcaoID, String.Format(CultureInfo.InvariantCulture, "{0:" + UserPrincipal.Identity.Settings.ApplicationDateFormat + " HH:mm}", (DateTime)Leg.ScheduledTM)));
                                    else if (DepAiport != null && ArrAirport != null && Leg.ScheduledTM == null)
                                        HistoryDescription.AppendLine(string.Format("Leg Number: {0} Departs ICAO: {1} Arrive ICAO: {2}", Leg.LegNUM, DepAiport.IcaoID, ArrAirport.IcaoID));

                                    #endregion

                                }
                                break;
                            case Data.Postflight.TripEntityState.Modified:
                                {
                                    if (POLegID > 0)
                                    {
                                        ExistingPOLegEntity = Container.PostflightLegs
                                        .Where(poLeg => poLeg.POLegID == POLegID).SingleOrDefault();

                                        if (ExistingPOLegEntity != null)
                                            Container.PostflightLegs.Attach(ExistingPOLegEntity);

                                        Container.PostflightLegs.ApplyCurrentValues(Leg);

                                        #region "History For Modified Leg"

                                        PostflightLeg originalLeg = new PostflightLeg();
                                        originalLeg = ExistingPOLegEntity;

                                        var originalLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).OriginalValues;
                                        var CurrentLegValues = Container.ObjectStateManager.GetObjectStateEntry(originalLeg).CurrentValues;

                                        PropertyInfo[] properties = typeof(PostflightLeg).GetProperties();
                                        for (int i = 0; i < originalLegValues.FieldCount; i++)
                                        {
                                            var fieldname = originalLegValues.GetName(i);

                                            object value1 = originalLegValues.GetValue(i);
                                            object value2 = CurrentLegValues.GetValue(i);

                                            if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                            {
                                                #region "Depart ICAOID"

                                                if (fieldname.ToLower() == "departicaoid")
                                                {
                                                    Int64 OldDepartICAOID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDepartICAOID = Convert.ToInt64(value1);

                                                    Int64 NewDepartICAOID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDepartICAOID = Convert.ToInt64(value2);

                                                    Airport OldAiport = Container.Airports.Where(x => x.AirportID == OldDepartICAOID).SingleOrDefault();
                                                    Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewDepartICAOID).SingleOrDefault();

                                                    if (OldDepartICAOID == 0 && NewDepartICAOID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Depart ICAO Added: {0} ", NewAirport.IcaoID));
                                                    else if (OldDepartICAOID != NewDepartICAOID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Depart ICAO",
                                                            (OldAiport != null ? OldAiport.IcaoID : string.Empty),
                                                            (NewAirport != null ? NewAirport.IcaoID : string.Empty)));
                                                }

                                                #endregion

                                                #region "Arrive ICAOID"

                                                if (fieldname.ToLower() == "arriveicaoid")
                                                {
                                                    Int64 OldArriveICAOID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldArriveICAOID = Convert.ToInt64(value1);

                                                    Int64 NewArriveICAOID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewArriveICAOID = Convert.ToInt64(value2);

                                                    Airport OldAiport = Container.Airports.Where(x => x.AirportID == OldArriveICAOID).SingleOrDefault();
                                                    Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewArriveICAOID).SingleOrDefault();

                                                    if (OldArriveICAOID == 0 && NewArriveICAOID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Arrive ICAO Added: {0} ", NewAirport.IcaoID));
                                                    else if (OldArriveICAOID != NewArriveICAOID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Arrive ICAO",
                                                                (OldAiport != null ? OldAiport.IcaoID : string.Empty),
                                                                (NewAirport != null ? NewAirport.IcaoID : string.Empty)));
                                                }

                                                #endregion

                                                #region "Distance"
                                                if (fieldname.ToLower() == "distance")
                                                {
                                                    string OldDistance = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDistance = value1.ToString();

                                                    string NewDistance = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDistance = value2.ToString();

                                                    if (OldDistance == string.Empty && NewDistance != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Distance Added: {0} ", NewDistance));
                                                    else if (OldDistance != NewDistance)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Distance",
                                                            (OldDistance != null ? OldDistance : string.Empty),
                                                            (NewDistance != null ? NewDistance : string.Empty))
                                                            );
                                                }
                                                #endregion

                                                #region "Statute Miles"
                                                if (fieldname.ToLower() == "statutemiles")
                                                {
                                                    string OldStatuteMiles = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldStatuteMiles = value1.ToString();

                                                    string NewStatuteMiles = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewStatuteMiles = value2.ToString();

                                                    if (OldStatuteMiles == string.Empty && NewStatuteMiles != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New State Miles Added: {0} ", NewStatuteMiles));
                                                    else if (OldStatuteMiles != NewStatuteMiles)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "State Miles",
                                                        (OldStatuteMiles != null ? OldStatuteMiles : string.Empty),
                                                        (NewStatuteMiles != null ? NewStatuteMiles : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Delay Type"

                                                if (fieldname.ToLower() == "delaytypeid")
                                                {
                                                    Int64 OldDelayTypeID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDelayTypeID = Convert.ToInt64(value1);

                                                    Int64 NewDelayTypeID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDelayTypeID = Convert.ToInt64(value2);

                                                    DelayType OldDelayType = Container.DelayTypes.Where(x => x.DelayTypeID == OldDelayTypeID).SingleOrDefault();
                                                    DelayType NewDelayType = Container.DelayTypes.Where(x => x.DelayTypeID == NewDelayTypeID).SingleOrDefault();

                                                    if (OldDelayTypeID == 0 && NewDelayTypeID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Delay Type Added: {0} ", NewDelayType.DelayTypeCD));
                                                    else if (OldDelayTypeID != NewDelayTypeID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0} {1} Changed From {2} To {3}", originalLeg.LegNUM, "Delay Type",
                                                            (OldDelayType != null ? OldDelayType.DelayTypeCD : string.Empty),
                                                            (NewDelayType != null ? NewDelayType.DelayTypeCD : string.Empty)));
                                                }

                                                #endregion

                                                #region "Delay Time"
                                                if (fieldname.ToLower() == "delaytm")
                                                {
                                                    string OldDelayTM = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDelayTM = value1.ToString();

                                                    string NewDelayTM = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDelayTM = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldDelayTM == "0" && NewDelayTM != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Delay Time Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewDelayTM), 3).ToString())));
                                                        else if (OldDelayTM != NewDelayTM)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Delay Time",
                                                            (OldDelayTM != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldDelayTM), 3).ToString()) : string.Empty),
                                                            (NewDelayTM != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewDelayTM), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldDelayTM == "0" && NewDelayTM != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Delay Time Added: {0} ", Math.Round(Convert.ToDecimal(NewDelayTM), 1).ToString()));
                                                        else if (OldDelayTM != NewDelayTM)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Delay Time",
                                                            (OldDelayTM != null ? Math.Round(Convert.ToDecimal(OldDelayTM), 1).ToString() : string.Empty),
                                                            (NewDelayTM != null ? Math.Round(Convert.ToDecimal(NewDelayTM), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Schedule DateTime"
                                                if (fieldname.ToLower() == "scheduledtm")
                                                {
                                                    DateTime? OldScheduledTM = null;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldScheduledTM = Convert.ToDateTime(value1);

                                                    DateTime? NewScheduledTM = null;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewScheduledTM = Convert.ToDateTime(value2);

                                                    HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Scheduled Date",
                                                    (OldScheduledTM != null ? OldScheduledTM.Value.ToShortDateString() : null),
                                                    (NewScheduledTM != null ? NewScheduledTM.Value.ToShortDateString() : null))
                                                    );

                                                    HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Scheduled Time",
                                                    (OldScheduledTM != null ? OldScheduledTM.Value.ToShortTimeString() : null),
                                                    (NewScheduledTM != null ? NewScheduledTM.Value.ToShortTimeString() : null))
                                                    );
                                                }
                                                #endregion

                                                #region "Block Out"
                                                if (fieldname.ToLower() == "blockout")
                                                {
                                                    string OldBlockOut = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldBlockOut = value1.ToString();

                                                    string NewBlockOut = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewBlockOut = value2.ToString();

                                                    if (OldBlockOut == string.Empty && NewBlockOut != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Out Time Added: {0} ", NewBlockOut));
                                                    else if (OldBlockOut != NewBlockOut)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Out Time",
                                                        (OldBlockOut != null ? OldBlockOut : string.Empty),
                                                        (NewBlockOut != null ? NewBlockOut : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Time Off"
                                                if (fieldname.ToLower() == "timeoff")
                                                {
                                                    string OldTimeOff = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldTimeOff = value1.ToString();

                                                    string NewTimeOff = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewTimeOff = value2.ToString();

                                                    if (OldTimeOff == string.Empty && NewTimeOff != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Off Time Added: {0} ", NewTimeOff));
                                                    else if (OldTimeOff != NewTimeOff)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Off Time",
                                                        (OldTimeOff != null ? OldTimeOff : string.Empty),
                                                        (NewTimeOff != null ? NewTimeOff : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Time On"
                                                if (fieldname.ToLower() == "timeon")
                                                {
                                                    string OldTimeOn = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldTimeOn = value1.ToString();

                                                    string NewTimeOn = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewTimeOn = value2.ToString();

                                                    if (OldTimeOn == string.Empty && NewTimeOn != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New On Time Added: {0} ", NewTimeOn));
                                                    else if (OldTimeOn != NewTimeOn)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "On Time",
                                                        (OldTimeOn != null ? OldTimeOn : string.Empty),
                                                        (NewTimeOn != null ? NewTimeOn : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Block In"
                                                if (fieldname.ToLower() == "blockin")
                                                {
                                                    string OldBlockIN = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldBlockIN = value1.ToString();

                                                    string NewBlockIN = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewBlockIN = value2.ToString();

                                                    if (OldBlockIN == string.Empty && NewBlockIN != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New In Time Added: {0} ", NewBlockIN));
                                                    else if (OldBlockIN != NewBlockIN)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "In Time",
                                                        (OldBlockIN != null ? OldBlockIN : string.Empty),
                                                        (NewBlockIN != null ? NewBlockIN : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Out Date"
                                                if (fieldname.ToLower() == "outbounddttm")
                                                {
                                                    DateTime? OldOutboundDTTM = null;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldOutboundDTTM = Convert.ToDateTime(value1);

                                                    DateTime? NewOutboundDTTM = null;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewOutboundDTTM = Convert.ToDateTime(value2);

                                                    if (OldOutboundDTTM == null && NewOutboundDTTM != null)
                                                        HistoryDescription.AppendLine(string.Format("New Out Date Added: {0} ", NewOutboundDTTM.Value.ToShortTimeString()));
                                                    else if (OldOutboundDTTM != NewOutboundDTTM)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Out Date",
                                                        (OldOutboundDTTM != null ? OldOutboundDTTM.Value.ToShortDateString() : null),
                                                        (NewOutboundDTTM != null ? NewOutboundDTTM.Value.ToShortDateString() : null))
                                                        );
                                                }
                                                #endregion

                                                #region "In Date"
                                                if (fieldname.ToLower() == "inbounddttm")
                                                {
                                                    DateTime? OldInboundDTTM = null;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldInboundDTTM = Convert.ToDateTime(value1);

                                                    DateTime? NewInboundDTTM = null;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewInboundDTTM = Convert.ToDateTime(value2);

                                                    if (OldInboundDTTM == null && NewInboundDTTM != null)
                                                        HistoryDescription.AppendLine(string.Format("New In Date Added: {0} ", NewInboundDTTM.Value.ToShortTimeString()));
                                                    else if (OldInboundDTTM != NewInboundDTTM)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "In Date",
                                                        (OldInboundDTTM != null ? OldInboundDTTM.Value.ToShortDateString() : null),
                                                        (NewInboundDTTM != null ? NewInboundDTTM.Value.ToShortDateString() : null))
                                                        );
                                                }
                                                #endregion

                                                #region "Block Hours"
                                                if (fieldname.ToLower() == "blockhours")
                                                {
                                                    string OldBlockHours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldBlockHours = value1.ToString();

                                                    string NewBlockHours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewBlockHours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldBlockHours == "0" && NewBlockHours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Block Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewBlockHours), 3).ToString())));
                                                        else if (OldBlockHours != NewBlockHours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Block Hours",
                                                            (OldBlockHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldBlockHours), 3).ToString()) : string.Empty),
                                                            (NewBlockHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewBlockHours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldBlockHours == "0" && NewBlockHours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Block Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewBlockHours), 1).ToString()));
                                                        else if (OldBlockHours != NewBlockHours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Block Hours",
                                                            (OldBlockHours != null ? Math.Round(Convert.ToDecimal(OldBlockHours), 1).ToString() : string.Empty),
                                                            (NewBlockHours != null ? Math.Round(Convert.ToDecimal(NewBlockHours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Flight Hours"
                                                if (fieldname.ToLower() == "flighthours")
                                                {
                                                    string OldFlightHours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFlightHours = value1.ToString();

                                                    string NewFlightHours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFlightHours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldFlightHours == "0" && NewFlightHours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Flight Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewFlightHours), 3).ToString())));
                                                        else if (OldFlightHours != NewFlightHours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Flight Hours",
                                                            (OldFlightHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldFlightHours), 3).ToString()) : string.Empty),
                                                            (NewFlightHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewFlightHours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldFlightHours == "0" && NewFlightHours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Flight Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewFlightHours), 1).ToString()));
                                                        else if (OldFlightHours != NewFlightHours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Flight Hours",
                                                            (OldFlightHours != null ? Math.Round(Convert.ToDecimal(OldFlightHours), 1).ToString() : string.Empty),
                                                            (NewFlightHours != null ? Math.Round(Convert.ToDecimal(NewFlightHours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Duty Hours"
                                                if (fieldname.ToLower() == "dutyhrs")
                                                {
                                                    string OldDutyHrs = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDutyHrs = value1.ToString();

                                                    string NewDutyHrs = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDutyHrs = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldDutyHrs == "0" && NewDutyHrs != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Duty Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewDutyHrs), 3).ToString())));
                                                        else if (OldDutyHrs != NewDutyHrs)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Duty Hours",
                                                            (OldDutyHrs != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldDutyHrs), 3).ToString()) : string.Empty),
                                                            (NewDutyHrs != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewDutyHrs), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldDutyHrs == "0" && NewDutyHrs != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Duty Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewDutyHrs), 1).ToString()));
                                                        else if (OldDutyHrs != NewDutyHrs)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Duty Hours",
                                                            (OldDutyHrs != null ? Math.Round(Convert.ToDecimal(OldDutyHrs), 1).ToString() : string.Empty),
                                                            (NewDutyHrs != null ? Math.Round(Convert.ToDecimal(NewDutyHrs), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Flight Purpose"
                                                if (fieldname.ToLower() == "flightpurpose")
                                                {
                                                    string OldFlightPurpose = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFlightPurpose = value1.ToString();

                                                    string NewFlightPurpose = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFlightPurpose = value2.ToString();

                                                    if (OldFlightPurpose == string.Empty && NewFlightPurpose != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Purpose Added: {0} ", NewFlightPurpose));
                                                    else if (OldFlightPurpose != NewFlightPurpose)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Purpose",
                                                        (OldFlightPurpose != null ? OldFlightPurpose : string.Empty),
                                                        (NewFlightPurpose != null ? NewFlightPurpose : string.Empty))
                                                        );

                                                    //HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Purpose", value1, value2));
                                                }
                                                #endregion

                                                #region "Requestor"

                                                if (fieldname.ToLower() == "passengerrequestorid")
                                                {
                                                    Int64 OldPassengerID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldPassengerID = Convert.ToInt64(value1);

                                                    Int64 NewPassengerID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewPassengerID = Convert.ToInt64(value2);

                                                    Passenger Oldpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == OldPassengerID).SingleOrDefault();
                                                    Passenger Newpassenger = Container.Passengers.Where(x => x.PassengerRequestorID == NewPassengerID).SingleOrDefault();

                                                    if (OldPassengerID == 0 && NewPassengerID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Requestor Code Added: {0} ", Newpassenger.PassengerRequestorCD));
                                                    else if (OldPassengerID != NewPassengerID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Requestor Code",
                                                            (Oldpassenger != null ? Oldpassenger.PassengerRequestorCD : string.Empty),
                                                            (Newpassenger != null ? Newpassenger.PassengerRequestorCD : string.Empty)));
                                                }


                                                #endregion

                                                #region "Department"

                                                if (fieldname.ToLower() == "departmentid")
                                                {
                                                    Int64 OldDepartmentID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDepartmentID = Convert.ToInt64(value1);

                                                    Int64 NewDepartmentID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDepartmentID = Convert.ToInt64(value2);

                                                    Department OldDepartment = Container.Departments.Where(x => x.DepartmentID == OldDepartmentID).SingleOrDefault();
                                                    Department NewDepartment = Container.Departments.Where(x => x.DepartmentID == NewDepartmentID).SingleOrDefault();

                                                    if (OldDepartmentID == 0 && NewDepartmentID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Department Code Added: {0} ", NewDepartment.DepartmentCD));
                                                    else if (OldDepartmentID != NewDepartmentID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Department Code",
                                                            (OldDepartment != null ? OldDepartment.DepartmentCD : string.Empty),
                                                            (NewDepartment != null ? NewDepartment.DepartmentCD : string.Empty)));
                                                }


                                                #endregion

                                                #region "Authorization"

                                                if (fieldname.ToLower() == "authorizationid")
                                                {
                                                    Int64 OldAuthorizationID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldAuthorizationID = Convert.ToInt64(value1);

                                                    Int64 NewAuthorizationID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewAuthorizationID = Convert.ToInt64(value2);

                                                    DepartmentAuthorization OldAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == OldAuthorizationID).SingleOrDefault();
                                                    DepartmentAuthorization NewAuthorization = Container.DepartmentAuthorizations.Where(x => x.AuthorizationID == NewAuthorizationID).SingleOrDefault();

                                                    if (OldAuthorizationID == 0 && NewAuthorizationID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Authorization Code Added: {0} ", NewAuthorization.AuthorizationCD));
                                                    else if (OldAuthorizationID != NewAuthorizationID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Authorization Code",
                                                            (OldAuthorization != null ? OldAuthorization.AuthorizationCD : string.Empty),
                                                            (NewAuthorization != null ? NewAuthorization.AuthorizationCD : string.Empty)));
                                                }


                                                #endregion

                                                #region "Flight Category"

                                                if (fieldname.ToLower() == "flightcategoryid")
                                                {
                                                    Int64 OldFlightCategoryID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFlightCategoryID = Convert.ToInt64(value1);

                                                    Int64 NewFlightCategoryID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFlightCategoryID = Convert.ToInt64(value2);

                                                    FlightCatagory OldFlightCategory = Container.FlightCatagories.Where(x => x.FlightCategoryID == OldFlightCategoryID).SingleOrDefault();
                                                    FlightCatagory NewFlightCategory = Container.FlightCatagories.Where(x => x.FlightCategoryID == NewFlightCategoryID).SingleOrDefault();

                                                    if (OldFlightCategoryID == 0 && NewFlightCategoryID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Category Code Added: {0} ", NewFlightCategory.FlightCatagoryCD));
                                                    else if (OldFlightCategoryID != NewFlightCategoryID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Category Code",
                                                            (OldFlightCategory != null ? OldFlightCategory.FlightCatagoryCD : string.Empty),
                                                            (NewFlightCategory != null ? NewFlightCategory.FlightCatagoryCD : string.Empty)));
                                                }


                                                #endregion

                                                #region "Client"

                                                if (fieldname.ToLower() == "clientid")
                                                {
                                                    Int64 OldClientID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldClientID = Convert.ToInt64(value1);

                                                    Int64 NewClientID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewClientID = Convert.ToInt64(value2);

                                                    Client OldClient = Container.Clients.Where(x => x.ClientID == OldClientID).SingleOrDefault();
                                                    Client NewClient = Container.Clients.Where(x => x.ClientID == NewClientID).SingleOrDefault();

                                                    if (OldClientID == 0 && NewClientID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Client Code Added: {0} ", NewClient.ClientCD));
                                                    else if (OldClientID != NewClientID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Client Code",
                                                            (OldClient != null ? OldClient.ClientCD : string.Empty),
                                                            (NewClient != null ? NewClient.ClientCD : string.Empty)));
                                                }


                                                #endregion

                                                #region "Flight Number"
                                                if (fieldname.ToLower() == "flightnum")
                                                {
                                                    string OldFlightNum = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFlightNum = value1.ToString();

                                                    string NewFlightNum = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFlightNum = value2.ToString();

                                                    if (OldFlightNum == string.Empty && NewFlightNum != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Flight Number Added: {0} ", NewFlightNum));
                                                    else if (OldFlightNum != NewFlightNum)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Flight Number",
                                                        (OldFlightNum != null ? OldFlightNum : string.Empty),
                                                        (NewFlightNum != null ? NewFlightNum : string.Empty))
                                                        );

                                                    //HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Flight Number", value1, value2));
                                                }
                                                #endregion

                                                #region "FAR Number"
                                                if (fieldname.ToLower() == "fedaviationregnum")
                                                {
                                                    string OldFAR = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFAR = value1.ToString();

                                                    string NewFAR = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFAR = value2.ToString();

                                                    if (OldFAR == string.Empty && NewFAR != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New FAR Added: {0} ", NewFAR));
                                                    else if (OldFAR.Trim() != NewFAR.Trim())
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "FAR",
                                                        (OldFAR != null ? OldFAR : string.Empty),
                                                        (NewFAR != null ? NewFAR : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Duty Type"
                                                if (fieldname.ToLower() == "dutytype")
                                                {
                                                    decimal OldDutyTYPE = 1;
                                                    string OldDutyTYPEVal = "Dom.";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldDutyTYPE = (decimal)value1;

                                                    decimal NewDutyTYPE = 1;
                                                    string NewDutyTYPEVal = "Dom.";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewDutyTYPE = (decimal)value2;

                                                    if (OldDutyTYPE == 2)
                                                        OldDutyTYPEVal = "Intl.";
                                                    if (NewDutyTYPE == 2)
                                                        NewDutyTYPEVal = "Intl.";

                                                    HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Dom/Intl", OldDutyTYPEVal, NewDutyTYPEVal));
                                                }
                                                #endregion

                                                #region "End Duty"
                                                if (fieldname.ToLower() == "isdutyend")
                                                {
                                                    HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "End Duty",
                                                        ((!string.IsNullOrEmpty(value1.ToString()) && (bool)value1) ? "Yes" : "No"),
                                                        ((!string.IsNullOrEmpty(value2.ToString()) && (bool)value2) ? "Yes" : "No")));
                                                }
                                                #endregion

                                                #region "Crew Currency"
                                                if (fieldname.ToLower() == "crewcurrency")
                                                {
                                                    string OldCrewCurrency = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldCrewCurrency = value1.ToString();

                                                    string NewCrewCurrency = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewCrewCurrency = value2.ToString();

                                                    if (OldCrewCurrency == string.Empty && NewCrewCurrency != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Crew Rules Code Added: {0} ", NewCrewCurrency));
                                                    else if (!string.IsNullOrEmpty(OldCrewCurrency) && !string.IsNullOrEmpty(NewCrewCurrency) && OldCrewCurrency != NewCrewCurrency)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Crew Rules Code",
                                                        (OldCrewCurrency != null ? OldCrewCurrency : string.Empty),
                                                        (NewCrewCurrency != null ? NewCrewCurrency : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Flight Cost"
                                                if (fieldname.ToLower() == "flightcost")
                                                {
                                                    string OldFlightCost = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFlightCost = value1.ToString();

                                                    string NewFlightCost = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFlightCost = value2.ToString();

                                                    if (OldFlightCost == string.Empty && NewFlightCost != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Flight Cost Added: {0} ", NewFlightCost));
                                                    else if (!string.IsNullOrEmpty(OldFlightCost) && !string.IsNullOrEmpty(NewFlightCost) && OldFlightCost != NewFlightCost)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Flight Cost",
                                                        (OldFlightCost != null ? OldFlightCost : string.Empty),
                                                        (NewFlightCost != null ? NewFlightCost : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Account"

                                                if (fieldname.ToLower() == "accountid")
                                                {
                                                    Int64 OldAccountID = 0;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldAccountID = Convert.ToInt64(value1);

                                                    Int64 NewAccountID = 0;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewAccountID = Convert.ToInt64(value2);

                                                    Account OldAccount = Container.Accounts.Where(x => x.AccountID == OldAccountID).SingleOrDefault();
                                                    Account NewAccount = Container.Accounts.Where(x => x.AccountID == NewAccountID).SingleOrDefault();

                                                    if (OldAccountID == 0 && NewAccountID != 0)
                                                        HistoryDescription.AppendLine(string.Format("New Account Number Added: {0} ", NewAccount.AccountNum));
                                                    else if (OldAccountID != NewAccountID)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Account Number",
                                                            (OldAccount != null ? OldAccount.AccountNum : string.Empty),
                                                            (NewAccount != null ? NewAccount.AccountNum : string.Empty)));
                                                }
                                                #endregion

                                                #region "Fuel Out"
                                                if (fieldname.ToLower() == "fuelout")
                                                {
                                                    string OldFuelOut = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFuelOut = value1.ToString();

                                                    string NewFuelOut = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFuelOut = value2.ToString();

                                                    if (OldFuelOut == string.Empty && NewFuelOut != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Fuel Out Added: {0} ", NewFuelOut));
                                                    else if (!string.IsNullOrEmpty(OldFuelOut) && !string.IsNullOrEmpty(NewFuelOut) && OldFuelOut != NewFuelOut)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Fuel Out",
                                                        (OldFuelOut != null ? OldFuelOut : string.Empty),
                                                        (NewFuelOut != null ? NewFuelOut : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Fuel In"
                                                if (fieldname.ToLower() == "fuelin")
                                                {
                                                    string OldFuelIn = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFuelIn = value1.ToString();

                                                    string NewFuelIn = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFuelIn = value2.ToString();

                                                    if (OldFuelIn == string.Empty && NewFuelIn != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Fuel In Added: {0} ", NewFuelIn));
                                                    else if (!string.IsNullOrEmpty(OldFuelIn) && !string.IsNullOrEmpty(NewFuelIn) && OldFuelIn != NewFuelIn)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Fuel In",
                                                        (OldFuelIn != null ? OldFuelIn : string.Empty),
                                                        (NewFuelIn != null ? NewFuelIn : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Fuel Used"
                                                if (fieldname.ToLower() == "fuelused")
                                                {
                                                    string OldFuelUsed = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFuelUsed = value1.ToString();

                                                    string NewFuelUsed = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFuelUsed = value2.ToString();

                                                    if (OldFuelUsed == string.Empty && NewFuelUsed != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Fuel Used Added: {0} ", NewFuelUsed));
                                                    else if (!string.IsNullOrEmpty(OldFuelUsed) && !string.IsNullOrEmpty(NewFuelUsed) && OldFuelUsed != NewFuelUsed)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Fuel Used",
                                                        (OldFuelUsed != null ? OldFuelUsed : string.Empty),
                                                        (NewFuelUsed != null ? NewFuelUsed : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Fuel Burn"
                                                if (fieldname.ToLower() == "fuelburn")
                                                {
                                                    decimal OldFuelBurn = 1;
                                                    string OldFuelBurnVal = "Pounds";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldFuelBurn = (decimal)value1;

                                                    decimal NewFuelBurn = 1;
                                                    string NewFuelBurnVal = "Pounds";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewFuelBurn = (decimal)value2;

                                                    if (OldFuelBurn == 2)
                                                        OldFuelBurnVal = "Kilos";
                                                    if (NewFuelBurn == 2)
                                                        NewFuelBurnVal = "Kilos";

                                                    HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Fuel Burn Units", OldFuelBurnVal, NewFuelBurnVal));
                                                }
                                                #endregion

                                                #region "Cargo Out"
                                                if (fieldname.ToLower() == "cargoout")
                                                {
                                                    string OldCargoOut = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldCargoOut = value1.ToString();

                                                    string NewCargoOut = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewCargoOut = value2.ToString();

                                                    if (OldCargoOut == string.Empty && NewCargoOut != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Cargo Out Added: {0} ", NewCargoOut));
                                                    else if (!string.IsNullOrEmpty(OldCargoOut) && !string.IsNullOrEmpty(NewCargoOut) && OldCargoOut != NewCargoOut)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Cargo Out",
                                                        (OldCargoOut != null ? OldCargoOut : string.Empty),
                                                        (NewCargoOut != null ? NewCargoOut : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Cargo In"
                                                if (fieldname.ToLower() == "cargoin")
                                                {
                                                    string OldCargoIn = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldCargoIn = value1.ToString();

                                                    string NewCargoIn = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewCargoIn = value2.ToString();

                                                    if (OldCargoIn == string.Empty && NewCargoIn != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Cargo In Added: {0} ", NewCargoIn));
                                                    else if (!string.IsNullOrEmpty(OldCargoIn) && !string.IsNullOrEmpty(NewCargoIn) && OldCargoIn != NewCargoIn)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Cargo In",
                                                        (OldCargoIn != null ? OldCargoIn : string.Empty),
                                                        (NewCargoIn != null ? NewCargoIn : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Cargo Through"
                                                if (fieldname.ToLower() == "cargothru")
                                                {
                                                    string OldCargoThru = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldCargoThru = value1.ToString();

                                                    string NewCargoThru = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewCargoThru = value2.ToString();

                                                    if (OldCargoThru == string.Empty && NewCargoThru != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Cargo Through Added: {0} ", NewCargoThru));
                                                    else if (!string.IsNullOrEmpty(OldCargoThru) && !string.IsNullOrEmpty(NewCargoThru) && OldCargoThru != NewCargoThru)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Cargo Through",
                                                        (OldCargoThru != null ? OldCargoThru : string.Empty),
                                                        (NewCargoThru != null ? NewCargoThru : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "AirFrame Hours"
                                                if (fieldname.ToLower() == "airframehours")
                                                {
                                                    string OldAirFrameHours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldAirFrameHours = value1.ToString();

                                                    string NewAirFrameHours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewAirFrameHours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldAirFrameHours == "0" && NewAirFrameHours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Airframe Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewAirFrameHours), 3).ToString())));
                                                        else if (OldAirFrameHours != NewAirFrameHours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Airframe Hours",
                                                            (OldAirFrameHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldAirFrameHours), 3).ToString()) : string.Empty),
                                                            (NewAirFrameHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewAirFrameHours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldAirFrameHours == "0" && NewAirFrameHours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Airframe Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewAirFrameHours), 1).ToString()));
                                                        else if (OldAirFrameHours != NewAirFrameHours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Airframe Hours",
                                                            (OldAirFrameHours != null ? Math.Round(Convert.ToDecimal(OldAirFrameHours), 1).ToString() : string.Empty),
                                                            (NewAirFrameHours != null ? Math.Round(Convert.ToDecimal(NewAirFrameHours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Engine1 Hours"
                                                if (fieldname.ToLower() == "engine1hours")
                                                {
                                                    string OldEngine1Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine1Hours = value1.ToString();

                                                    string NewEngine1Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine1Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldEngine1Hours == "0" && NewEngine1Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 1 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine1Hours), 3).ToString())));
                                                        else if (OldEngine1Hours != NewEngine1Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 1 Hours",
                                                            (OldEngine1Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldEngine1Hours), 3).ToString()) : string.Empty),
                                                            (NewEngine1Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine1Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldEngine1Hours == "0" && NewEngine1Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 1 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewEngine1Hours), 1).ToString()));
                                                        else if (OldEngine1Hours != NewEngine1Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 1 Hours",
                                                            (OldEngine1Hours != null ? Math.Round(Convert.ToDecimal(OldEngine1Hours), 1).ToString() : string.Empty),
                                                            (NewEngine1Hours != null ? Math.Round(Convert.ToDecimal(NewEngine1Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Engine2 Hours"
                                                if (fieldname.ToLower() == "engine2hours")
                                                {
                                                    string OldEngine2Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine2Hours = value1.ToString();

                                                    string NewEngine2Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine2Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldEngine2Hours == "0" && NewEngine2Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 2 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine2Hours), 3).ToString())));
                                                        else if (OldEngine2Hours != NewEngine2Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 2 Hours",
                                                            (OldEngine2Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldEngine2Hours), 3).ToString()) : string.Empty),
                                                            (NewEngine2Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine2Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldEngine2Hours == "0" && NewEngine2Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 2 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewEngine2Hours), 1).ToString()));
                                                        else if (OldEngine2Hours != NewEngine2Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 2 Hours",
                                                            (OldEngine2Hours != null ? Math.Round(Convert.ToDecimal(OldEngine2Hours), 1).ToString() : string.Empty),
                                                            (NewEngine2Hours != null ? Math.Round(Convert.ToDecimal(NewEngine2Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Engine3 Hours"
                                                if (fieldname.ToLower() == "engine3hours")
                                                {
                                                    string OldEngine3Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine3Hours = value1.ToString();

                                                    string NewEngine3Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine3Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldEngine3Hours == "0" && NewEngine3Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 3 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine3Hours), 3).ToString())));
                                                        else if (OldEngine3Hours != NewEngine3Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 3 Hours",
                                                            (OldEngine3Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldEngine3Hours), 3).ToString()) : string.Empty),
                                                            (NewEngine3Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine3Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldEngine3Hours == "0" && NewEngine3Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 3 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewEngine3Hours), 1).ToString()));
                                                        else if (OldEngine3Hours != NewEngine3Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 3 Hours",
                                                            (OldEngine3Hours != null ? Math.Round(Convert.ToDecimal(OldEngine3Hours), 1).ToString() : string.Empty),
                                                            (NewEngine3Hours != null ? Math.Round(Convert.ToDecimal(NewEngine3Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Engine4 Hours"
                                                if (fieldname.ToLower() == "engine4hours")
                                                {
                                                    string OldEngine4Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine4Hours = value1.ToString();

                                                    string NewEngine4Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine4Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldEngine4Hours == "0" && NewEngine4Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 4 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine4Hours), 3).ToString())));
                                                        else if (OldEngine4Hours != NewEngine4Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 4 Hours",
                                                            (OldEngine4Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldEngine4Hours), 3).ToString()) : string.Empty),
                                                            (NewEngine4Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewEngine4Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldEngine4Hours == "0" && NewEngine4Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Engine 4 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewEngine4Hours), 1).ToString()));
                                                        else if (OldEngine4Hours != NewEngine4Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 4 Hours",
                                                            (OldEngine4Hours != null ? Math.Round(Convert.ToDecimal(OldEngine4Hours), 1).ToString() : string.Empty),
                                                            (NewEngine4Hours != null ? Math.Round(Convert.ToDecimal(NewEngine4Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Reverser1 Hours"
                                                if (fieldname.ToLower() == "re1hours")
                                                {
                                                    string OldRe1Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe1Hours = value1.ToString();

                                                    string NewRe1Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe1Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldRe1Hours == "0" && NewRe1Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 1 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe1Hours), 3).ToString())));
                                                        else if (OldRe1Hours != NewRe1Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 1 Hours",
                                                            (OldRe1Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldRe1Hours), 3).ToString()) : string.Empty),
                                                            (NewRe1Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe1Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldRe1Hours == "0" && NewRe1Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 1 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewRe1Hours), 1).ToString()));
                                                        else if (OldRe1Hours != NewRe1Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 1 Hours",
                                                            (OldRe1Hours != null ? Math.Round(Convert.ToDecimal(OldRe1Hours), 1).ToString() : string.Empty),
                                                            (NewRe1Hours != null ? Math.Round(Convert.ToDecimal(NewRe1Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Reverser2 Hours"
                                                if (fieldname.ToLower() == "re2hours")
                                                {
                                                    string OldRe2Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe2Hours = value1.ToString();

                                                    string NewRe2Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe2Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldRe2Hours == "0" && NewRe2Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 2 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe2Hours), 3).ToString())));
                                                        else if (OldRe2Hours != NewRe2Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 2 Hours",
                                                            (OldRe2Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldRe2Hours), 3).ToString()) : string.Empty),
                                                            (NewRe2Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe2Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldRe2Hours == "0" && NewRe2Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 2 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewRe2Hours), 1).ToString()));
                                                        else if (OldRe2Hours != NewRe2Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 2 Hours",
                                                            (OldRe2Hours != null ? Math.Round(Convert.ToDecimal(OldRe2Hours), 1).ToString() : string.Empty),
                                                            (NewRe2Hours != null ? Math.Round(Convert.ToDecimal(NewRe2Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Reverser3 Hours"
                                                if (fieldname.ToLower() == "re3hours")
                                                {
                                                    string OldRe3Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe3Hours = value1.ToString();

                                                    string NewRe3Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe3Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldRe3Hours == "0" && NewRe3Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 3 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe3Hours), 3).ToString())));
                                                        else if (OldRe3Hours != NewRe3Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 3 Hours",
                                                            (OldRe3Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldRe3Hours), 3).ToString()) : string.Empty),
                                                            (NewRe3Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe3Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldRe3Hours == "0" && NewRe3Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 3 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewRe3Hours), 1).ToString()));
                                                        else if (OldRe3Hours != NewRe3Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 3 Hours",
                                                            (OldRe3Hours != null ? Math.Round(Convert.ToDecimal(OldRe3Hours), 1).ToString() : string.Empty),
                                                            (NewRe3Hours != null ? Math.Round(Convert.ToDecimal(NewRe3Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Reverser4 Hours"
                                                if (fieldname.ToLower() == "re4hours")
                                                {
                                                    string OldRe4Hours = "0";
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe4Hours = value1.ToString();

                                                    string NewRe4Hours = "0";
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe4Hours = value2.ToString();

                                                    if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                    {
                                                        if (OldRe4Hours == "0" && NewRe4Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 4 Hours Added: {0} ", ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe4Hours), 3).ToString())));
                                                        else if (OldRe4Hours != NewRe4Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 4 Hours",
                                                            (OldRe4Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldRe4Hours), 3).ToString()) : string.Empty),
                                                            (NewRe4Hours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewRe4Hours), 3).ToString()) : string.Empty))
                                                            );
                                                    }
                                                    else
                                                    {
                                                        if (OldRe4Hours == "0" && NewRe4Hours != "0")
                                                            HistoryDescription.AppendLine(string.Format("New Reverser 4 Hours Added: {0} ", Math.Round(Convert.ToDecimal(NewRe4Hours), 1).ToString()));
                                                        else if (OldRe4Hours != NewRe4Hours)
                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 4 Hours",
                                                            (OldRe4Hours != null ? Math.Round(Convert.ToDecimal(OldRe4Hours), 1).ToString() : string.Empty),
                                                            (NewRe4Hours != null ? Math.Round(Convert.ToDecimal(NewRe4Hours), 1).ToString() : string.Empty))
                                                            );
                                                    }
                                                }
                                                #endregion

                                                #region "Engine1 Cycles"
                                                if (fieldname.ToLower() == "engine1cycle")
                                                {
                                                    string OldEngine1Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine1Cycle = value1.ToString();

                                                    string NewEngine1Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine1Cycle = value2.ToString();

                                                    if (OldEngine1Cycle == string.Empty && NewEngine1Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Engine 1 Cycles Added: {0} ", NewEngine1Cycle));
                                                    else if (!string.IsNullOrEmpty(OldEngine1Cycle) && !string.IsNullOrEmpty(NewEngine1Cycle) && OldEngine1Cycle != NewEngine1Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 1 Cycles",
                                                        (OldEngine1Cycle != null ? OldEngine1Cycle : string.Empty),
                                                        (NewEngine1Cycle != null ? NewEngine1Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Engine2 Cycles"
                                                if (fieldname.ToLower() == "engine2cycle")
                                                {
                                                    string OldEngine2Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine2Cycle = value1.ToString();

                                                    string NewEngine2Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine2Cycle = value2.ToString();

                                                    if (OldEngine2Cycle == string.Empty && NewEngine2Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Engine 2 Cycles Added: {0} ", NewEngine2Cycle));
                                                    else if (!string.IsNullOrEmpty(OldEngine2Cycle) && !string.IsNullOrEmpty(NewEngine2Cycle) && OldEngine2Cycle != NewEngine2Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 2 Cycles",
                                                        (OldEngine2Cycle != null ? OldEngine2Cycle : string.Empty),
                                                        (NewEngine2Cycle != null ? NewEngine2Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Engine3 Cycles"
                                                if (fieldname.ToLower() == "engine3cycle")
                                                {
                                                    string OldEngine3Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine3Cycle = value1.ToString();

                                                    string NewEngine3Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine3Cycle = value2.ToString();

                                                    if (OldEngine3Cycle == string.Empty && NewEngine3Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Engine 3 Cycles Added: {0} ", NewEngine3Cycle));
                                                    else if (!string.IsNullOrEmpty(OldEngine3Cycle) && !string.IsNullOrEmpty(NewEngine3Cycle) && OldEngine3Cycle != NewEngine3Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 3 Cycles",
                                                        (OldEngine3Cycle != null ? OldEngine3Cycle : string.Empty),
                                                        (NewEngine3Cycle != null ? NewEngine3Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Engine4 Cycles"
                                                if (fieldname.ToLower() == "engine4cycle")
                                                {
                                                    string OldEngine4Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldEngine4Cycle = value1.ToString();

                                                    string NewEngine4Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewEngine4Cycle = value2.ToString();

                                                    if (OldEngine4Cycle == string.Empty && NewEngine4Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Engine 4 Cycles Added: {0} ", NewEngine4Cycle));
                                                    else if (!string.IsNullOrEmpty(OldEngine4Cycle) && !string.IsNullOrEmpty(NewEngine4Cycle) && OldEngine4Cycle != NewEngine4Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Engine 4 Cycles",
                                                        (OldEngine4Cycle != null ? OldEngine4Cycle : string.Empty),
                                                        (NewEngine4Cycle != null ? NewEngine4Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Reverser1 Cycles"
                                                if (fieldname.ToLower() == "re1cycle")
                                                {
                                                    string OldRe1Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe1Cycle = value1.ToString();

                                                    string NewRe1Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe1Cycle = value2.ToString();

                                                    if (OldRe1Cycle == string.Empty && NewRe1Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Reverser 1 Cycles Added: {0} ", NewRe1Cycle));
                                                    else if (!string.IsNullOrEmpty(OldRe1Cycle) && !string.IsNullOrEmpty(NewRe1Cycle) && OldRe1Cycle != NewRe1Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 1 Cycles",
                                                        (OldRe1Cycle != null ? OldRe1Cycle : string.Empty),
                                                        (NewRe1Cycle != null ? NewRe1Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Reverser2 Cycles"
                                                if (fieldname.ToLower() == "re2cycle")
                                                {
                                                    string OldRe2Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe2Cycle = value1.ToString();

                                                    string NewRe2Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe2Cycle = value2.ToString();

                                                    if (OldRe2Cycle == string.Empty && NewRe2Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Reverser 2 Cycles Added: {0} ", NewRe2Cycle));
                                                    else if (!string.IsNullOrEmpty(OldRe2Cycle) && !string.IsNullOrEmpty(NewRe2Cycle) && OldRe2Cycle != NewRe2Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 2 Cycles",
                                                        (OldRe2Cycle != null ? OldRe2Cycle : string.Empty),
                                                        (NewRe2Cycle != null ? NewRe2Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Reverser3 Cycles"
                                                if (fieldname.ToLower() == "re3cycle")
                                                {
                                                    string OldRe3Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe3Cycle = value1.ToString();

                                                    string NewRe3Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe3Cycle = value2.ToString();

                                                    if (OldRe3Cycle == string.Empty && NewRe3Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Reverser 3 Cycles Added: {0} ", NewRe3Cycle));
                                                    else if (!string.IsNullOrEmpty(OldRe3Cycle) && !string.IsNullOrEmpty(NewRe3Cycle) && OldRe3Cycle != NewRe3Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 3 Cycles",
                                                        (OldRe3Cycle != null ? OldRe3Cycle : string.Empty),
                                                        (NewRe3Cycle != null ? NewRe3Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion

                                                #region "Reverser4 Cycles"
                                                if (fieldname.ToLower() == "re4cycle")
                                                {
                                                    string OldRe4Cycle = string.Empty;
                                                    if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                        OldRe4Cycle = value1.ToString();

                                                    string NewRe4Cycle = string.Empty;
                                                    if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                        NewRe4Cycle = value2.ToString();

                                                    if (OldRe4Cycle == string.Empty && NewRe4Cycle != string.Empty)
                                                        HistoryDescription.AppendLine(string.Format("New Reverser 4 Cycles Added: {0} ", NewRe4Cycle));
                                                    else if (!string.IsNullOrEmpty(OldRe4Cycle) && !string.IsNullOrEmpty(NewRe4Cycle) && OldRe4Cycle != NewRe4Cycle)
                                                        HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", originalLeg.LegNUM, "Reverser 4 Cycles",
                                                        (OldRe4Cycle != null ? OldRe4Cycle : string.Empty),
                                                        (NewRe4Cycle != null ? NewRe4Cycle : string.Empty))
                                                        );
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                break;
                            case Data.Postflight.TripEntityState.NoChange:
                                {
                                    if (POLegID > 0)
                                    {
                                        ExistingPOLegEntity = Container.PostflightLegs
                                          .Where(poLeg => poLeg.POLegID == POLegID).SingleOrDefault();

                                        if (ExistingPOLegEntity != null)
                                            Container.PostflightLegs.Attach(ExistingPOLegEntity);

                                        Container.PostflightLegs.ApplyCurrentValues(Leg);
                                    }
                                }
                                break;
                        }

                        #region CREW
                        foreach (PostflightCrew Crew in Leg.PostflightCrews.ToList())
                        {
                            long POCrewID = (long)Crew.PostflightCrewListID;
                            Crew.POLogID = SourceObject.POLogID;
                            Crew.POLegID = Leg.POLegID;
                            Crew.CustomerID = CustomerID;
                            Crew.LastUpdUID = SourceObject.LastUpdUID;
                            PostflightCrew ExistingPOCrewEntity = null;
                            switch (Crew.State)
                            {
                                case Data.Postflight.TripEntityState.Added:
                                    {
                                        if (ExistingPOLegEntity != null)
                                        {
                                            ExistingPOLegEntity.PostflightCrews.Add(Crew);
                                        }
                                        Container.ObjectStateManager.ChangeObjectState(Crew, System.Data.EntityState.Added);

                                        if (Leg.State != TripEntityState.Deleted)
                                        {
                                            #region "History For Added Crew"
                                            HistoryDescription.AppendLine(string.Format("New Crew added to Leg: {0}  Crew Code: {1} Duty Type: {2} ", Leg.LegNUM, !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Crew.DutyTYPE));
                                            #endregion
                                        }
                                    }
                                    break;
                                case Data.Postflight.TripEntityState.Modified:
                                    {
                                        if (POCrewID > 0)
                                        {
                                            ExistingPOCrewEntity = Container.PostflightCrews
                                            .Where(poCrew => poCrew.PostflightCrewListID == POCrewID).SingleOrDefault();

                                            if (ExistingPOCrewEntity != null)
                                                Container.PostflightCrews.Attach(ExistingPOCrewEntity);

                                            Container.PostflightCrews.ApplyCurrentValues(Crew);

                                            if (Leg.State != TripEntityState.Deleted)
                                            {
                                                #region "History For Modified Crew"
                                                var originalCrewValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOCrewEntity).OriginalValues;
                                                var CurrentCrewValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOCrewEntity).CurrentValues;
                                                PropertyInfo[] properties = typeof(PostflightCrew).GetProperties();
                                                for (int i = 0; i < originalCrewValues.FieldCount; i++)
                                                {
                                                    var fieldname = originalCrewValues.GetName(i);

                                                    object value1 = originalCrewValues.GetValue(i);
                                                    object value2 = CurrentCrewValues.GetValue(i);

                                                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                                    {
                                                        #region "Duty TYPE"
                                                        if (fieldname.ToLower() == "dutytype")
                                                        {
                                                            if (!string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                            {
                                                                if (Crew.DutyTYPE != null && !string.IsNullOrEmpty(Crew.DutyTYPE.ToString()))
                                                                    HistoryDescription.AppendLine(string.Format("Dutytype for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM, value1, value2));
                                                            }
                                                        }
                                                        #endregion

                                                        #region "Take Off Day"
                                                        if (fieldname.ToLower() == "takeoffday")
                                                        {
                                                            string OldTakeOffDay = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldTakeOffDay = value1.ToString();

                                                            string NewTakeOffDay = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewTakeOffDay = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Take Off Day for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                (OldTakeOffDay != null ? OldTakeOffDay : string.Empty),
                                                                (NewTakeOffDay != null ? NewTakeOffDay : string.Empty))
                                                                );
                                                        }
                                                        #endregion

                                                        #region "Take Off Night"
                                                        if (fieldname.ToLower() == "takeoffnight")
                                                        {
                                                            string OldTakeOffNight = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldTakeOffNight = value1.ToString();

                                                            string NewTakeOffNight = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewTakeOffNight = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Take Off Night for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : Crew.Crew.CrewCD, Leg.LegNUM,
                                                               (OldTakeOffNight != null ? OldTakeOffNight : string.Empty),
                                                               (NewTakeOffNight != null ? NewTakeOffNight : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Landing Day"
                                                        if (fieldname.ToLower() == "landingday")
                                                        {
                                                            string OldLandingDay = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldLandingDay = value1.ToString();

                                                            string NewLandingDay = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewLandingDay = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Landing Day for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldLandingDay != null ? OldLandingDay : string.Empty),
                                                               (NewLandingDay != null ? NewLandingDay : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Landing Night"
                                                        if (fieldname.ToLower() == "landingnight")
                                                        {
                                                            string OldLandingNight = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldLandingNight = value1.ToString();

                                                            string NewLandingNight = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewLandingNight = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Landing Night for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldLandingNight != null ? OldLandingNight : string.Empty),
                                                               (NewLandingNight != null ? NewLandingNight : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Approach Precision"
                                                        if (fieldname.ToLower() == "approachprecision")
                                                        {
                                                            string OldApproachPrecision = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldApproachPrecision = value1.ToString();

                                                            string NewApproachPrecision = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewApproachPrecision = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Approach Precision for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldApproachPrecision != null ? OldApproachPrecision : string.Empty),
                                                               (NewApproachPrecision != null ? NewApproachPrecision : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Approach Non-Precision"
                                                        if (fieldname.ToLower() == "approachnonprecision")
                                                        {
                                                            string OldApproachNonPrecision = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldApproachNonPrecision = value1.ToString();

                                                            string NewApproachNonPrecision = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewApproachNonPrecision = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Approach Non-Precision for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldApproachNonPrecision != null ? OldApproachNonPrecision : string.Empty),
                                                               (NewApproachNonPrecision != null ? NewApproachNonPrecision : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Instrument"
                                                        if (fieldname.ToLower() == "instrument")
                                                        {
                                                            string OldInstrument = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldInstrument = value1.ToString();

                                                            string NewInstrument = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewInstrument = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Instrument Time for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldInstrument != null ? OldInstrument : string.Empty),
                                                               (NewInstrument != null ? NewInstrument : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Night"
                                                        if (fieldname.ToLower() == "night")
                                                        {
                                                            string OldNight = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldNight = value1.ToString();

                                                            string NewNight = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewNight = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Night Time for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldNight != null ? OldNight : string.Empty),
                                                               (NewNight != null ? NewNight : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "RemainOverNight"
                                                        if (fieldname.ToLower() == "remainovernight")
                                                        {
                                                            string OldRemainOverNight = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldRemainOverNight = value1.ToString();

                                                            string NewRemainOverNight = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewRemainOverNight = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("RON for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldRemainOverNight != null ? OldRemainOverNight : string.Empty),
                                                               (NewRemainOverNight != null ? NewRemainOverNight : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Beginning Duty"
                                                        if (fieldname.ToLower() == "beginningduty")
                                                        {
                                                            string OldBeginningDuty = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldBeginningDuty = value1.ToString();

                                                            string NewBeginningDuty = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewBeginningDuty = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Duty Begin for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldBeginningDuty != null ? OldBeginningDuty : string.Empty),
                                                               (NewBeginningDuty != null ? NewBeginningDuty : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Duty End"
                                                        if (fieldname.ToLower() == "dutyend")
                                                        {
                                                            string OldDutyEnd = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldDutyEnd = value1.ToString();

                                                            string NewDutyEnd = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewDutyEnd = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Duty End for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                               (OldDutyEnd != null ? OldDutyEnd : string.Empty),
                                                               (NewDutyEnd != null ? NewDutyEnd : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Duty Hours"
                                                        if (fieldname.ToLower() == "dutyhours")
                                                        {
                                                            string OldDutyHours = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldDutyHours = value1.ToString();

                                                            string NewDutyHours = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewDutyHours = value2.ToString();
                                                            if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Duty Hours for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                   (OldDutyHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldDutyHours), 3).ToString()) : string.Empty),
                                                                   (NewDutyHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewDutyHours), 3).ToString()) : string.Empty))
                                                                   );
                                                            }
                                                            else
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Duty Hours for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                (OldDutyHours != null ? Math.Round(Convert.ToDecimal(OldDutyHours), 1).ToString() : string.Empty),
                                                                (NewDutyHours != null ? Math.Round(Convert.ToDecimal(NewDutyHours), 1).ToString() : string.Empty))
                                                                );
                                                            }
                                                        }
                                                        #endregion

                                                        #region "Block Hours"
                                                        if (fieldname.ToLower() == "blockhours")
                                                        {
                                                            string OldBlockHours = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldBlockHours = value1.ToString();

                                                            string NewBlockHours = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewBlockHours = value2.ToString();
                                                            if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Block Hours for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                (OldBlockHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldBlockHours), 3).ToString()) : string.Empty),
                                                                (NewBlockHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewBlockHours), 3).ToString()) : string.Empty)));
                                                            }
                                                            else
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Block Hours for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                (OldBlockHours != null ? Math.Round(Convert.ToDecimal(OldBlockHours), 1).ToString() : string.Empty),
                                                                (NewBlockHours != null ? Math.Round(Convert.ToDecimal(NewBlockHours), 1).ToString() : string.Empty)));
                                                            }
                                                        }
                                                        #endregion

                                                        #region "Flight Hours"
                                                        if (fieldname.ToLower() == "flighthours")
                                                        {
                                                            string OldFlightHours = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFlightHours = value1.ToString();

                                                            string NewFlightHours = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFlightHours = value2.ToString();

                                                            if (UserPrincipal.Identity.Settings.TimeDisplayTenMin == 2)
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Flight Hours for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                   (OldFlightHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(OldFlightHours),3).ToString()) : string.Empty),
                                                                   (NewFlightHours != null ? ConvertTenthsToMins(Math.Round(Convert.ToDecimal(NewFlightHours), 3).ToString()) : string.Empty))
                                                                   );
                                                            }
                                                            else
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Flight Hours for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                   (OldFlightHours != null ? Math.Round(Convert.ToDecimal(OldFlightHours), 1).ToString() : string.Empty),
                                                                   (NewFlightHours != null ? Math.Round(Convert.ToDecimal(NewFlightHours), 1).ToString() : string.Empty))
                                                                   );
                                                            }
                                                        }
                                                        #endregion

                                                        #region "Seat"
                                                        if (fieldname.ToLower() == "seat")
                                                        {
                                                            string OldSeat = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldSeat = value1.ToString();

                                                            string NewSeat = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewSeat = value2.ToString();
                                                            if ((!String.IsNullOrWhiteSpace(OldSeat) || !String.IsNullOrWhiteSpace(NewSeat)) && OldSeat != NewSeat)
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Seat Assigned for Crew {0} On Leg {1} Changed From {2} To {3}", !string.IsNullOrWhiteSpace(Crew.CrewCode) ? Crew.CrewCode : "", Leg.LegNUM,
                                                                   ((!String.IsNullOrWhiteSpace(OldSeat) && OldSeat !="0") ? OldSeat : "No Selection"),
                                                                   ((!String.IsNullOrWhiteSpace(NewSeat) && NewSeat != "0") ? NewSeat : "No Selection"))
                                                                   );
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                }

                                                #endregion
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                        #endregion

                        #region EXPENSE
                        //Below code - to update object state and detach referenced objects in Postflight Expenses per each leg
                        foreach (PostflightExpense Expense in Leg.PostflightExpenses.ToList())
                        {
                            Expense.POLogID = SourceObject.POLogID;
                            Expense.POLegID = Leg.POLegID;
                            Expense.CustomerID = CustomerID;
                            Expense.LastUpdUID = SourceObject.LastUpdUID;
                            long POExpenseID = (long)Expense.PostflightExpenseID;
                            PostflightExpense ExistingPOExpenseEntity = null;
                            switch (Expense.State)
                            {
                                case Data.Postflight.TripEntityState.Added:
                                    {
                                        if (ExistingPOLegEntity != null)
                                        {
                                            ExistingPOLegEntity.PostflightExpenses.Add(Expense);
                                        }
                                        Container.ObjectStateManager.ChangeObjectState(Expense, System.Data.EntityState.Added);

                                        if (Leg.State != TripEntityState.Deleted)
                                        {
                                            #region "History For Added Expenses"
                                            HistoryDescription.AppendLine(string.Format("New Expenses added to Leg: {0}", Leg.LegNUM));
                                            #endregion
                                        }
                                    }
                                    break;
                                case Data.Postflight.TripEntityState.Modified:
                                    {
                                        if (POExpenseID > 0)
                                        {
                                            ExistingPOExpenseEntity = Container.PostflightExpenses
                                            .Where(poExpense => poExpense.PostflightExpenseID == POExpenseID).SingleOrDefault();

                                            if (ExistingPOExpenseEntity != null)
                                                Container.PostflightExpenses.Attach(ExistingPOExpenseEntity);

                                            Container.PostflightExpenses.ApplyCurrentValues(Expense);

                                            if (Leg.State != TripEntityState.Deleted)
                                            {
                                                #region "History For Modified Expenses"
                                                var originalPaxValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOExpenseEntity).OriginalValues;
                                                var CurrentPaxValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOExpenseEntity).CurrentValues;
                                                PropertyInfo[] properties = typeof(PostflightExpense).GetProperties();
                                                for (int i = 0; i < originalPaxValues.FieldCount; i++)
                                                {
                                                    var fieldname = originalPaxValues.GetName(i);

                                                    object value1 = originalPaxValues.GetValue(i);
                                                    object value2 = CurrentPaxValues.GetValue(i);

                                                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                                    {
                                                        #region "Account Number"
                                                        if (fieldname.ToLower() == "accountid")
                                                        {
                                                            Int64 OldAccountID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldAccountID = Convert.ToInt64(value1);

                                                            Int64 NewAccountID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewAccountID = Convert.ToInt64(value2);

                                                            Account OldAccount = Container.Accounts.Where(x => x.AccountID == OldAccountID).SingleOrDefault();
                                                            Account NewAccount = Container.Accounts.Where(x => x.AccountID == NewAccountID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Acct No. Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldAccount != null ? OldAccount.AccountNum + ": " + OldAccount.AccountDescription : string.Empty),
                                                                (NewAccount != null ? NewAccount.AccountNum + ": " + NewAccount.AccountDescription : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Account Period"
                                                        if (fieldname.ToLower() == "accountperiod")
                                                        {
                                                            string OldAccountPeriod = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldAccountPeriod = value1.ToString();

                                                            string NewAccountPeriod = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewAccountPeriod = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Acct Period Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldAccountPeriod != null ? OldAccountPeriod : string.Empty),
                                                               (NewAccountPeriod != null ? NewAccountPeriod : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "HomeBase"

                                                        if (fieldname.ToLower() == "homebaseid")
                                                        {
                                                            Int64 OldHomebaseID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldHomebaseID = Convert.ToInt64(value1);

                                                            Int64 NewHomebaseID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewHomebaseID = Convert.ToInt64(value2);

                                                            Company OldCompany = Container.Companies.Where(x => x.HomebaseID == OldHomebaseID).SingleOrDefault();
                                                            Company NewCompany = Container.Companies.Where(x => x.HomebaseID == NewHomebaseID).SingleOrDefault();

                                                            string OldAirportID = string.Empty;
                                                            string NewAirportID = string.Empty;
                                                            if (OldCompany != null)
                                                            {
                                                                Airport OldAirport = Container.Airports.Where(x => x.AirportID == OldCompany.HomebaseAirportID).SingleOrDefault();
                                                                if (OldAirport != null)
                                                                {
                                                                    OldAirportID = OldAirport.IcaoID;
                                                                }
                                                            }

                                                            if (NewCompany != null)
                                                            {
                                                                Airport NewAirport = Container.Airports.Where(x => x.AirportID == NewCompany.HomebaseAirportID).SingleOrDefault();
                                                                if (NewAirportID != null)
                                                                {
                                                                    NewAirportID = NewAirport.IcaoID;
                                                                }
                                                            }

                                                            if (!string.IsNullOrEmpty(OldAirportID) && !string.IsNullOrEmpty(NewAirportID))
                                                            {
                                                                HistoryDescription.AppendLine(string.Format("Leg {0} Homebase Changed From {1} To {2}", Leg.LegNUM, OldAirportID, NewAirportID));
                                                            }
                                                        }


                                                        #endregion

                                                        #region "Purchase Date"
                                                        if (fieldname.ToLower() == "purchasedt")
                                                        {
                                                            DateTime? OldPurchaseDT = null;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldPurchaseDT = Convert.ToDateTime(value1);

                                                            DateTime? NewPurchaseDT = null;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewPurchaseDT = Convert.ToDateTime(value2);

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Purchase Date Changed From {1} To {2}", Leg.LegNUM,
                                                            (OldPurchaseDT != null ? OldPurchaseDT.Value.ToShortDateString() : null),
                                                            (NewPurchaseDT != null ? NewPurchaseDT.Value.ToShortDateString() : null))
                                                            );
                                                        }
                                                        #endregion

                                                        #region "FBO"
                                                        if (fieldname.ToLower() == "fboid")
                                                        {
                                                            long OldFBOID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFBOID = Convert.ToInt64(value1);

                                                            long NewFBOID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFBOID = Convert.ToInt64(value2);

                                                            FBO OldFBO = Container.FBOes.Where(x => x.FBOID == OldFBOID).SingleOrDefault();
                                                            FBO NewFBO = Container.FBOes.Where(x => x.FBOID == NewFBOID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: FBO Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldFBO != null ? OldFBO.FBOCD : string.Empty),
                                                                (NewFBO != null ? NewFBO.FBOCD : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Fuel Locator"
                                                        if (fieldname.ToLower() == "fuellocatorid")
                                                        {
                                                            long OldFuelLocatorID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFuelLocatorID = Convert.ToInt64(value1);

                                                            long NewFuelLocatorID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFuelLocatorID = Convert.ToInt64(value2);
                                                            FuelLocator OldFuelLocator = Container.FuelLocators.Where(x => x.FuelLocatorID == OldFuelLocatorID).SingleOrDefault();
                                                            FuelLocator NewFuelLocator = Container.FuelLocators.Where(x => x.FuelLocatorID == NewFuelLocatorID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Fuel Locator Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldFuelLocator != null ? OldFuelLocator.FuelLocatorCD : string.Empty),
                                                                (NewFuelLocator != null ? NewFuelLocator.FuelLocatorCD : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Payment Type"
                                                        if (fieldname.ToLower() == "paymenttypeid")
                                                        {
                                                            long OldPaymentTypeID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldPaymentTypeID = Convert.ToInt64(value1);

                                                            long NewPaymentTypeID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewPaymentTypeID = Convert.ToInt64(value2);

                                                            PaymentType OldPaymentType = Container.PaymentTypes.Where(x => x.PaymentTypeID == OldPaymentTypeID).SingleOrDefault();
                                                            PaymentType NewPaymentType = Container.PaymentTypes.Where(x => x.PaymentTypeID == NewPaymentTypeID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Pymt Type Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldPaymentType != null ? OldPaymentType.PaymentTypeCD : string.Empty),
                                                                (NewPaymentType != null ? NewPaymentType.PaymentTypeCD : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Payment Vendor"
                                                        if (fieldname.ToLower() == "paymentvendorid")
                                                        {
                                                            long OldPaymentVendorID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldPaymentVendorID = Convert.ToInt64(value1);

                                                            long NewPaymentVendorID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewPaymentVendorID = Convert.ToInt64(value2);

                                                            Vendor OldPaymentVendor = Container.Vendors.Where(x => x.VendorID == OldPaymentVendorID).SingleOrDefault();
                                                            Vendor NewPaymentVendor = Container.Vendors.Where(x => x.VendorID == NewPaymentVendorID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Vendor Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldPaymentVendor != null ? OldPaymentVendor.VendorCD + ":" + OldPaymentVendor.VendorContactName : string.Empty),
                                                                (NewPaymentVendor != null ? NewPaymentVendor.VendorCD + ":" + NewPaymentVendor.VendorContactName : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Crew"
                                                        if (fieldname.ToLower() == "crewid")
                                                        {
                                                            long OldCrewID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldCrewID = Convert.ToInt64(value1);

                                                            long NewCrewID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewCrewID = Convert.ToInt64(value2);

                                                            Crew OldCrew = Container.Crews.Where(x => x.CrewID == OldCrewID).SingleOrDefault();
                                                            Crew NewCrew = Container.Crews.Where(x => x.CrewID == NewCrewID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Crew Code Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldCrew != null ? OldCrew.CrewCD : string.Empty),
                                                                (NewCrew != null ? NewCrew.CrewCD : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Flight Category"
                                                        if (fieldname.ToLower() == "flightcategoryid")
                                                        {
                                                            long OldFlightCategoryID = 0;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFlightCategoryID = Convert.ToInt64(value1);

                                                            long NewFlightCategoryID = 0;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFlightCategoryID = Convert.ToInt64(value2);
                                                            FlightCatagory OldFlightCategory = Container.FlightCatagories.Where(x => x.FlightCategoryID == OldFlightCategoryID).SingleOrDefault();
                                                            FlightCatagory NewFlightCategory = Container.FlightCatagories.Where(x => x.FlightCategoryID == NewFlightCategoryID).SingleOrDefault();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: Flt Category Changed From {1} To {2}", Leg.LegNUM,
                                                                (OldFlightCategory != null ? OldFlightCategory.FlightCatagoryCD : string.Empty),
                                                                (NewFlightCategory != null ? NewFlightCategory.FlightCatagoryCD : string.Empty)));
                                                        }
                                                        #endregion

                                                        #region "Invoice Number"
                                                        if (fieldname.ToLower() == "invoicenum")
                                                        {
                                                            string OldInvoiceNUM = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldInvoiceNUM = value1.ToString();

                                                            string NewInvoiceNUM = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewInvoiceNUM = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Invoice No. Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldInvoiceNUM != null ? OldInvoiceNUM : string.Empty),
                                                               (NewInvoiceNUM != null ? NewInvoiceNUM : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Expense Amount"
                                                        if (fieldname.ToLower() == "expenseamt")
                                                        {
                                                            string OldExpenseAMT = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldExpenseAMT = value1.ToString();

                                                            string NewExpenseAMT = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewExpenseAMT = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Expense Amt Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldExpenseAMT != null ? OldExpenseAMT : string.Empty),
                                                               (NewExpenseAMT != null ? NewExpenseAMT : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Fuel Purchase"
                                                        if (fieldname.ToLower() == "fuelpurchase")
                                                        {
                                                            int OldFuelPurchase = 0;
                                                            string OldFuelPurchaseVal = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFuelPurchase = Convert.ToInt32(value1);

                                                            int NewFuelPurchase = 0;
                                                            string NewFuelPurchaseVal = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFuelPurchase = Convert.ToInt32(value2);

                                                            if (OldFuelPurchase == 1)
                                                                OldFuelPurchaseVal = "U.S. Gallons";
                                                            else if (OldFuelPurchase == 2)
                                                                OldFuelPurchaseVal = "Liters";
                                                            else if (OldFuelPurchase == 3)
                                                                OldFuelPurchaseVal = "Imp. Gallons";
                                                            else if (OldFuelPurchase == 4)
                                                                OldFuelPurchaseVal = "Pounds";
                                                            else if (OldFuelPurchase == 5)
                                                                OldFuelPurchaseVal = "Kilos";

                                                            if (NewFuelPurchase == 1)
                                                                NewFuelPurchaseVal = "U.S. Gallons";
                                                            else if (NewFuelPurchase == 2)
                                                                NewFuelPurchaseVal = "Liters";
                                                            else if (NewFuelPurchase == 3)
                                                                NewFuelPurchaseVal = "Imp. Gallons";
                                                            else if (NewFuelPurchase == 4)
                                                                NewFuelPurchaseVal = "Pounds";
                                                            else if (NewFuelPurchase == 5)
                                                                NewFuelPurchaseVal = "Kilos";

                                                            HistoryDescription.AppendLine(string.Format("Leg {0}: {1} Changed From {2} To {3}", Leg.LegNUM, "Fuel Unit", OldFuelPurchaseVal, NewFuelPurchaseVal));
                                                        }
                                                        #endregion

                                                        #region "Fuel Quantity"
                                                        if (fieldname.ToLower() == "fuelqty")
                                                        {
                                                            string OldFuelQTY = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFuelQTY = value1.ToString();

                                                            string NewFuelQTY = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFuelQTY = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Fuel Quantity Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldFuelQTY != null ? OldFuelQTY : string.Empty),
                                                               (NewFuelQTY != null ? NewFuelQTY : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Unit Price"
                                                        if (fieldname.ToLower() == "unitprice")
                                                        {
                                                            string OldUnitPrice = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldUnitPrice = value1.ToString();

                                                            string NewUnitPrice = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewUnitPrice = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Unit Price Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldUnitPrice != null ? OldUnitPrice : string.Empty),
                                                               (NewUnitPrice != null ? NewUnitPrice : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Post Fuel Price"
                                                        if (fieldname.ToLower() == "postfuelprice")
                                                        {
                                                            string OldPostFuelPrice = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldPostFuelPrice = value1.ToString();

                                                            string NewPostFuelPrice = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewPostFuelPrice = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Posted Price Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldPostFuelPrice != null ? OldPostFuelPrice : string.Empty),
                                                               (NewPostFuelPrice != null ? NewPostFuelPrice : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Federal TAX"
                                                        if (fieldname.ToLower() == "federaltax")
                                                        {
                                                            string OldFederalTAX = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldFederalTAX = value1.ToString();

                                                            string NewFederalTAX = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewFederalTAX = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Federal Tax Amt Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldFederalTAX != null ? OldFederalTAX : string.Empty),
                                                               (NewFederalTAX != null ? NewFederalTAX : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Sate TAX"
                                                        if (fieldname.ToLower() == "satetax")
                                                        {
                                                            string OldSateTAX = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldSateTAX = value1.ToString();

                                                            string NewSateTAX = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewSateTAX = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} State Tax Amt Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldSateTAX != null ? OldSateTAX : string.Empty),
                                                               (NewSateTAX != null ? NewSateTAX : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Sale TAX"
                                                        if (fieldname.ToLower() == "saletax")
                                                        {
                                                            string OldSaleTAX = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldSaleTAX = value1.ToString();

                                                            string NewSaleTAX = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewSaleTAX = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Sale Tax Amt Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldSaleTAX != null ? OldSaleTAX : string.Empty),
                                                               (NewSaleTAX != null ? NewSaleTAX : string.Empty))
                                                               );
                                                        }
                                                        #endregion

                                                        #region "Expense Amount"
                                                        if (fieldname.ToLower() == "expenseamt")
                                                        {
                                                            string OldExpenseAMT = string.Empty;
                                                            if (value1 != null && !string.IsNullOrEmpty(value1.ToString()))
                                                                OldExpenseAMT = value1.ToString();

                                                            string NewExpenseAMT = string.Empty;
                                                            if (value2 != null && !string.IsNullOrEmpty(value2.ToString()))
                                                                NewExpenseAMT = value2.ToString();

                                                            HistoryDescription.AppendLine(string.Format("Leg {0} Total Expenses Amount Changed From {1} To {2}", Leg.LegNUM,
                                                               (OldExpenseAMT != null ? OldExpenseAMT : string.Empty),
                                                               (NewExpenseAMT != null ? NewExpenseAMT : string.Empty))
                                                               );
                                                        }
                                                        #endregion
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    break;
                            }
                            if (Expense.PostflightNotes != null && Expense.PostflightNotes.Count > 0)
                            {
                                PostflightNote PONote = Expense.PostflightNotes.FirstOrDefault();
                                long PONotesID = (long)PONote.PostflightNoteID;
                                PONote.PostflightExpenseID = Expense.PostflightExpenseID;
                                PostflightNote ExistingPOExpenseNoteEntity = null;
                                switch (PONote.State)
                                {
                                    case Data.Postflight.TripEntityState.Added:
                                        {
                                            if (ExistingPOExpenseEntity != null)
                                            {
                                                ExistingPOExpenseEntity.PostflightNotes.Add(PONote);
                                            }
                                            Container.ObjectStateManager.ChangeObjectState(PONote, System.Data.EntityState.Added);
                                        }
                                        break;
                                    case Data.Postflight.TripEntityState.Modified:
                                        {
                                            ExistingPOExpenseNoteEntity = Container.PostflightNotes
                                           .Where(poExpenseNote => poExpenseNote.PostflightNoteID == PONotesID).SingleOrDefault();

                                            if (ExistingPOExpenseNoteEntity != null)
                                                Container.PostflightNotes.Attach(ExistingPOExpenseNoteEntity);

                                            Container.PostflightNotes.ApplyCurrentValues(PONote);
                                        }
                                        break;
                                }
                            }
                        }
                        #endregion

                        #region PAX

                        //Below code - to update object state and detach referenced objects in Postflight Passengers per each leg
                        foreach (PostflightPassenger Passenger in Leg.PostflightPassengers.ToList())
                        {
                            Passenger.POLogID = SourceObject.POLogID;
                            Passenger.CustomerID = CustomerID;
                            Passenger.LastUpdUID = SourceObject.LastUpdUID;
                            long POPassengerID = (long)Passenger.PostflightPassengerListID;

                            PostflightPassenger ExistingPOPassengerEntity = null;

                            switch (Passenger.State)
                            {
                                case Data.Postflight.TripEntityState.Added:
                                    {
                                        if (ExistingPOLegEntity != null)
                                        {
                                            ExistingPOLegEntity.PostflightPassengers.Add(Passenger);
                                        }
                                        Container.ObjectStateManager.ChangeObjectState(Passenger, System.Data.EntityState.Added);

                                        if (Leg.State != TripEntityState.Deleted)
                                        {
                                            #region "History For Added Passenger"
                                            Passenger objPassenger = Container.Passengers.Where(x => x.PassengerRequestorID == Passenger.PassengerID).SingleOrDefault();
                                            FlightPurpose objPurpose = Container.FlightPurposes.Where(x => x.FlightPurposeID == Passenger.FlightPurposeID).SingleOrDefault();

                                            string paxName = Passenger.PassengerLastName + ", " + Passenger.PassengerFirstName + " " + Passenger.PassengerMiddleName;

                                            if (objPassenger != null && objPurpose != null)
                                                HistoryDescription.AppendLine(string.Format("Passenger added to Leg: {0}  Code: {1} Name: {2} Purpose: {3}", Leg.LegNUM, objPassenger.PassengerRequestorCD, paxName, objPurpose.FlightPurposeCD));
                                            else if (objPassenger != null && objPurpose == null)
                                                HistoryDescription.AppendLine(string.Format("Passenger added to Leg: {0}  Code: {1} Name: {2} Purpose: {3}", Leg.LegNUM, objPassenger.PassengerRequestorCD, paxName, string.Empty));

                                            #endregion
                                        }
                                    }
                                    break;
                                case Data.Postflight.TripEntityState.Modified:
                                    {
                                        ExistingPOPassengerEntity = Container.PostflightPassengers
                                          .Where(poPassenger => poPassenger.PostflightPassengerListID == POPassengerID).SingleOrDefault();

                                        if (ExistingPOPassengerEntity != null)
                                            Container.PostflightPassengers.Attach(ExistingPOPassengerEntity);

                                        Container.PostflightPassengers.ApplyCurrentValues(Passenger);

                                        if (Leg.State != TripEntityState.Deleted)
                                        {
                                            #region "History For Modified Pax"
                                            var originalPaxValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOPassengerEntity).OriginalValues;
                                            var CurrentPaxValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOPassengerEntity).CurrentValues;
                                            PropertyInfo[] properties = typeof(PostflightPassenger).GetProperties();
                                            for (int i = 0; i < originalPaxValues.FieldCount; i++)
                                            {
                                                var fieldname = originalPaxValues.GetName(i);

                                                object value1 = originalPaxValues.GetValue(i);
                                                object value2 = CurrentPaxValues.GetValue(i);
                                                string paxName = Passenger.PassengerLastName + ", " + Passenger.PassengerFirstName + " " + Passenger.PassengerMiddleName;

                                                if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                                {
                                                    #region "Passenger Name"
                                                    if (fieldname.ToLower() == "passengerid")
                                                    {
                                                        HistoryDescription.AppendLine(string.Format("Leg {0} Name Changed From {1} To {2}", Leg.LegNUM, value1.ToString(), value2.ToString()));
                                                    }
                                                    #endregion

                                                    #region "Flight Purpose"
                                                    if (fieldname.ToLower() == "flightpurposeid")
                                                    {
                                                       

                                                            if (Passenger.FlightPurposeID != null && !string.IsNullOrEmpty(Passenger.FlightPurposeID.ToString()))
                                                            {
                                                               
                                                                IQueryable<FlightPurpose> FlightPurposes;
                                                                FlightPurpose OldPurpose = new FlightPurpose(), NewPurpose = new FlightPurpose();

                                                                if (!string.IsNullOrEmpty(value1.ToString()))
                                                                {
                                                                    //get the old FlightPurposes
                                                                    FlightPurposes = getFlightPurpose(Container, (Int64)value1, CustomerID);
                                                                    OldPurpose = new FlightPurpose();
                                                                    if (FlightPurposes != null && FlightPurposes.ToList().Count > 0)
                                                                        OldPurpose = FlightPurposes.ToList()[0];
                                                                }

                                                                if (!string.IsNullOrEmpty(value2.ToString()))
                                                                {
                                                                    //get the new FlightPurposes
                                                                    FlightPurposes = getFlightPurpose(Container, (Int64)value2, CustomerID);
                                                                    NewPurpose = new FlightPurpose();
                                                                    if (FlightPurposes != null && FlightPurposes.ToList().Count > 0)
                                                                        NewPurpose = FlightPurposes.ToList()[0];
                                                                }

                                                                //if old value is empty and new value is not empty then it means a new passenger has assigned to the leg
                                                                if (string.IsNullOrEmpty(value1.ToString()) && !string.IsNullOrEmpty(value2.ToString()))
                                                                    HistoryDescription.AppendLine(string.Format("Passenger {0} Added to Leg {1}. Purpose: {2}", paxName, Leg.LegNUM, NewPurpose.FlightPurposeCD));
                                                                //if old value is not empty and new value is empty then it means a passenger is un assigned from the leg
                                                                else if (!string.IsNullOrEmpty(value1.ToString()) && string.IsNullOrEmpty(value2.ToString()))
                                                                    HistoryDescription.AppendLine(string.Format("Passenger {0} Deleted from Leg {1}", paxName, Leg.LegNUM));
                                                                else
                                                                    //if both new and old valus are not empty then the value has been changed
                                                                    HistoryDescription.AppendLine(string.Format("Flight Purpose for {0} on Leg {1} Changed From {2} To {3} ", paxName, Leg.LegNUM, OldPurpose.FlightPurposeCD, NewPurpose.FlightPurposeCD));
                                                            }
                                                            else
                                                                HistoryDescription.AppendLine(string.Format("Passenger {0} Deleted from Leg {1}", paxName, Leg.LegNUM));
                                                        
                                                    }
                                                    #endregion

                                                    #region "Billing"
                                                    if (fieldname.ToLower() == "billing")
                                                    {
                                                        HistoryDescription.AppendLine(string.Format("Billing Code for {0} on Leg {1} Changed From {2} To {3}", paxName, Leg.LegNUM, value1.ToString(), value2.ToString()));
                                                    }
                                                    #endregion
                                                }
                                            }

                                            #endregion
                                        }
                                    }
                                    break;
                            }
                        }

                        #endregion

                        #region SIFL
                        //Below code - to update object state and detach referenced objects in Postfilght SIFL per each leg
                        foreach (PostflightSIFL Sifl in Leg.PostflightSIFLs.ToList())
                        {
                            Sifl.POLogID = SourceObject.POLogID;
                            //Sifl.POLegID = Leg.POLegID;
                            Sifl.CustomerID = CustomerID;
                            Sifl.LastUpdUID = SourceObject.LastUpdUID;
                            long POSiflID = (long)Sifl.POSIFLID;
                            PostflightSIFL ExistingPOSiflEntity = null;

                            switch (Sifl.State)
                            {
                                case Data.Postflight.TripEntityState.Added:
                                    {
                                        if (ExistingPOLegEntity != null)
                                        {
                                            ExistingPOLegEntity.PostflightSIFLs.Add(Sifl);
                                        }
                                        Container.ObjectStateManager.ChangeObjectState(Sifl, System.Data.EntityState.Added);

                                        if (Leg.State != TripEntityState.Deleted)
                                        {
                                            #region "History For Added sifl"
                                            if (Sifl != null)
                                            {
                                                Passenger objsiflPassenger = Container.Passengers.Where(x => x.PassengerRequestorID == Sifl.PassengerRequestorID).SingleOrDefault();
                                                HistoryDescription.AppendLine(string.Format("SIFL is recalculated to {0} for Passenger {1}", Sifl.AmtTotal, objsiflPassenger.PassengerName));

                                                //HistoryDescription.AppendLine(string.Format("SIFL is added for Passenger {0}", objsiflPassenger.PassengerName));
                                            }
                                            #endregion
                                        }
                                    }
                                    break;
                                case Data.Postflight.TripEntityState.Modified:
                                    {
                                        ExistingPOSiflEntity = Container.PostflightSIFLs
                                            .Where(sifl => sifl.POSIFLID == POSiflID).SingleOrDefault();

                                        if (ExistingPOSiflEntity != null)
                                            Container.PostflightSIFLs.Attach(ExistingPOSiflEntity);

                                        Container.PostflightSIFLs.ApplyCurrentValues(Sifl);

                                        if (Leg.State != TripEntityState.Deleted)
                                        {
                                            #region "History For Modified sifl"

                                            Passenger objsiflPassenger1 = Container.Passengers.Where(x => x.PassengerRequestorID == Sifl.PassengerRequestorID).SingleOrDefault();
                                            var originalSiflValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOSiflEntity).OriginalValues;
                                            var CurrentSiflValues = Container.ObjectStateManager.GetObjectStateEntry(ExistingPOSiflEntity).CurrentValues;

                                            PropertyInfo[] properties = typeof(PostflightSIFL).GetProperties();
                                            for (int i = 0; i < originalSiflValues.FieldCount; i++)
                                            {
                                                var fieldname = originalSiflValues.GetName(i);

                                                object value1 = originalSiflValues.GetValue(i);
                                                object value2 = CurrentSiflValues.GetValue(i);

                                                if (fieldname != null && fieldname.ToLower() == "amttotal")
                                                {
                                                    if (value1 != value2 && (value1 == null || !value1.Equals(value2)))
                                                    {
                                                        HistoryDescription.AppendLine(string.Format("SIFL is recalculated  from {0} to {1} for Passenger {2}", value1, value2, objsiflPassenger1.PassengerName));
                                                    }
                                                }
                                            }

                                            #endregion
                                        }
                                    }
                                    break;
                            }
                        }
                        #endregion
                    }
                }
                #endregion

                UpdateDeletedState(SourceObject.DeletedLegIDs, SourceObject.DeletedCrewIDs, SourceObject.DeletedPAXIDs, SourceObject.DeletedExpenseIDs, SourceObject.DeletedSIFLIDs, SourceObject.DeletedExceptionIDs, ref Container);

                #region EXCEPTIONS

                if (SourceObject.State == TripEntityState.Modified || SourceObject.State == TripEntityState.NoChange)
                {
                    if (ExistingPOMainEntity != null)
                    {
                        foreach (PostflightTripException PostException in ExistingPOMainEntity.PostflightTripExceptions.ToList())
                        {
                            Container.ObjectStateManager.ChangeObjectState(PostException, System.Data.EntityState.Deleted);
                        }
                    }
                }

                if (SourceObject.PostflightTripExceptions != null && SourceObject.PostflightTripExceptions.Count > 0)
                {
                    SourceObject.IsException = true;
                }

                foreach (PostflightTripException POException in SourceObject.PostflightTripExceptions.ToList())
                {
                    POException.POLogID = SourceObject.POLogID;
                    POException.LastUpdUID = UserPrincipal.Identity.Name;
                    POException.CustomerID = CustomerID;

                    if (SourceObject.State != TripEntityState.NoChange)
                    {
                        if (SourceObject.State != TripEntityState.Added)
                        {
                            PostflightTripException PostFlightException = new PostflightTripException();
                            PostFlightException.State = TripEntityState.Added;
                            PostFlightException.POLogID = SourceObject.POLogID;
                            PostFlightException.LastUpdUID = UserPrincipal.Identity.Name;
                            PostFlightException.CustomerID = CustomerID;
                            PostFlightException.ExceptionDescription = POException.ExceptionDescription;
                            PostFlightException.ExceptionGroup = POException.ExceptionGroup;
                            PostFlightException.ExceptionTemplateID = POException.ExceptionTemplateID;
                            PostFlightException.DisplayOrder = POException.DisplayOrder;
                            ExistingPOMainEntity.PostflightTripExceptions.Add(PostFlightException);
                        }
                        else
                        {
                            Container.ObjectStateManager.ChangeObjectState(POException, System.Data.EntityState.Added);
                        }
                    }
                    else
                    {
                        PostflightTripException PostFlightException = new PostflightTripException();
                        PostFlightException.State = TripEntityState.Added;
                        PostFlightException.POLogID = SourceObject.POLogID;
                        PostFlightException.LastUpdUID = UserPrincipal.Identity.Name;
                        PostFlightException.CustomerID = CustomerID;
                        PostFlightException.ExceptionDescription = POException.ExceptionDescription;
                        PostFlightException.ExceptionGroup = POException.ExceptionGroup;
                        PostFlightException.ExceptionTemplateID = POException.ExceptionTemplateID;
                        PostFlightException.DisplayOrder = POException.DisplayOrder;
                        ExistingPOMainEntity.PostflightTripExceptions.Add(PostFlightException);

                        Container.ObjectStateManager.ChangeObjectState(PostFlightException, System.Data.EntityState.Added);
                    }
                }

                //if (SourceObject.PostflightTripExceptions != null && SourceObject.PostflightTripExceptions.Count > 0 && UserPrincipal.Identity.Settings.IsLogsheetWarning == true)
                //{
                //    SourceObject.IsException = true;

                //    foreach (PostflightTripException POException in SourceObject.PostflightTripExceptions.ToList())
                //    {
                //        POException.POLogID = SourceObject.POLogID;
                //        POException.LastUpdUID = UserPrincipal.Identity.Name;
                //        POException.CustomerID = SourceObject.CustomerID;

                //        PostflightTripException PostFlightException = new PostflightTripException();
                //        PostFlightException.State = TripEntityState.Added;
                //        PostFlightException.POLogID = SourceObject.POLogID;
                //        PostFlightException.LastUpdUID = UserPrincipal.Identity.Name;
                //        PostFlightException.CustomerID = SourceObject.CustomerID;
                //        PostFlightException.ExceptionDescription = POException.ExceptionDescription;
                //        PostFlightException.ExceptionGroup = POException.ExceptionGroup;
                //        PostFlightException.ExceptionTemplateID = POException.ExceptionTemplateID;
                //        PostFlightException.DisplayOrder = POException.DisplayOrder;

                //        if (SourceObject.State != TripEntityState.Added)
                //        {
                //            if (ExistingPOMainEntity.PostflightTripExceptions != null)
                //                ExistingPOMainEntity.PostflightTripExceptions.Add(PostFlightException);
                //            else
                //                Container.ObjectStateManager.ChangeObjectState(POException, System.Data.EntityState.Added);
                //        }
                //        else
                //            Container.ObjectStateManager.ChangeObjectState(POException, System.Data.EntityState.Added);
                //    }
                //}
                //else
                //{
                //    SourceObject.IsException = false;
                //}

                #endregion

                #region LOG HISTORY

                if (HistoryDescription != null && HistoryDescription.Length > 0)
                {
                    if (SourceObject.State != TripEntityState.NoChange)
                    {
                        PostflightLogHistory TripHistory = new PostflightLogHistory();
                        if (SourceObject.State == TripEntityState.Added)
                        {
                            TripHistory.POLogID = SourceObject.POLogID;
                        }
                        else
                        {
                            TripHistory.POLogID = ExistingPOMainEntity.POLogID;
                        }
                        TripHistory.CustomerID = CustomerID;
                        TripHistory.HistoryDescription = HistoryDescription.ToString();
                        TripHistory.LastUpdUID = UserPrincipal.Identity.Name;
                        TripHistory.LastUpdTS = DateTime.UtcNow;

                        if (SourceObject.State != TripEntityState.Added)
                        {
                            ExistingPOMainEntity.PostflightLogHistories.Add(TripHistory);
                        }
                        else
                        {
                            Container.PostflightLogHistories.AddObject(TripHistory);
                        }
                    }
                }

                #endregion
            }
        }

        
        protected IQueryable<FlightPurpose> getFlightPurpose(FlightPak.Data.Postflight.PostflightDataModelContainer db, Int64 FlightPurposeID, Int64 CustomerID)
        {
            Func<FlightPak.Data.Postflight.PostflightDataModelContainer, Int64, Int64, IQueryable<FlightPurpose>> getFlightPurpose;

            getFlightPurpose = System.Data.Objects.CompiledQuery.Compile<FlightPak.Data.Postflight.PostflightDataModelContainer, Int64, Int64, IQueryable<FlightPurpose>>
            ((FlightPak.Data.Postflight.PostflightDataModelContainer cont, Int64 flightPurposeid, Int64 customerID) => from flightPurpose in cont.FlightPurposes
                                                                                                                where flightPurpose.FlightPurposeID == FlightPurposeID && CustomerID == flightPurpose.CustomerID
                                                                                                                select flightPurpose);

            return getFlightPurpose.Invoke(db, FlightPurposeID, CustomerID);
        }


        protected string GetSubmoduleModule(long SubModuleID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SubModuleID))
            {
                string Retval = string.Empty;
                switch (SubModuleID)
                {
                    case 1: Retval = "0"; break;
                    case 2: Retval = "1"; break;
                    case 3: Retval = "2"; break;
                    case 4: Retval = "3"; break;
                    case 5: Retval = "4"; break;
                }

                return Retval;
            }
        }

        #region Postflight Log History

        /// <summary>
        /// Method to Get Postflight Log History
        /// </summary>
        /// <param name="CustomerId">Pass Customer ID</param>
        /// <param name="POLogId">Pass Postflight Log ID</param>
        /// <returns>Returns Postflight Log History List</returns>
        public ReturnValue<Data.Postflight.PostflightLogHistory> GetHistory(long CustomerId, long POLogId)
        {
            ReturnValue<Data.Postflight.PostflightLogHistory> ret = null;
            PostflightDataModelContainer cs = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CustomerId, POLogId))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ret = new ReturnValue<Data.Postflight.PostflightLogHistory>();
                    using (cs = new PostflightDataModelContainer())
                    {
                        cs.ContextOptions.LazyLoadingEnabled = false;
                        cs.ContextOptions.ProxyCreationEnabled = false;
                        ret.EntityList = cs.GetHistory(CustomerId, POLogId).ToList();
                        ret.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ret;
        }
        #endregion

        /// <summary>
        /// Method to delete Trip information
        /// </summary>
        /// <returns>Delete Trip status</returns>
        public ReturnValue<Data.Postflight.PostflightMain> DeleteTrip(long POLogID)
        {
            ReturnValue<Data.Postflight.PostflightMain> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(POLogID))
            {
                ReturnValue = new ReturnValue<PostflightMain>();

                using (Container = new PostflightDataModelContainer())
                {
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    PostflightMain ExistingPOMainEntity = Container.PostflightMains
                       .Where(poMain => poMain.POLogID == POLogID && poMain.CustomerID == CustomerID).SingleOrDefault();

                    Container.PostflightMains.Attach(ExistingPOMainEntity);
                    ExistingPOMainEntity.LastUpdUID = UserPrincipal.Identity.Name;
                    ExistingPOMainEntity.IsDeleted = true;
                    Container.PostflightMains.ApplyCurrentValues(ExistingPOMainEntity);
                    Container.SaveChanges();

                    ReturnValue.ReturnFlag = true;
                }
            }
            return ReturnValue;
        }

        #endregion

        #region "Trip PAX"

        /// <summary>
        /// Method to Get All Passengers List
        /// </summary>
        /// <param name="customerID">Pass Customer ID</param>
        /// <returns>Passengers List</returns>
        public ReturnValue<Data.Postflight.POGetAllPassenger> GetPassengerList()
        {
            ReturnValue<Data.Postflight.POGetAllPassenger> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                long clientID = 0;
                if (UserPrincipal.Identity.ClientId != null)
                    clientID = Convert.ToInt64(UserPrincipal.Identity.ClientId);

                Results = new ReturnValue<Data.Postflight.POGetAllPassenger>();
                using (Container = new PostflightDataModelContainer())
                {
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Results.EntityList = Container.POGetAllPassenger(CustomerID, clientID).ToList();
                    foreach (var item in Results.EntityList)
                    {
                        if (item != null && !string.IsNullOrEmpty(item.PassportNum))
                            item.PassportNum = Crypting.Decrypt(item.PassportNum);
                    }
                    Results.ReturnFlag = true;
                }
            }
            return Results;
        }

        /// <summary>
        /// Method to Get All Passengers List
        /// </summary>
        /// <param name="PassengerRequestorID">Pass PassengerRequestor ID</param>
        /// <returns>Passengers List</returns>
        public ReturnValue<Data.Postflight.POGetAllPassenger> GetPassengerList(List<long> PassengerRequestorIDs)
        {
            ReturnValue<Data.Postflight.POGetAllPassenger> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerRequestorIDs))
            {
                long clientID = 0;
                List<POGetAllPassenger> Passengers = null;
                if (UserPrincipal.Identity.ClientId != null)
                    clientID = Convert.ToInt64(UserPrincipal.Identity.ClientId);

                Results = new ReturnValue<Data.Postflight.POGetAllPassenger>();
                using (Container = new PostflightDataModelContainer())
                {
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    Passengers = Container.POGetAllPassenger(CustomerID, clientID).ToList();
                    Results.EntityList = Passengers.FindAll(pax => PassengerRequestorIDs.Exists(paxID => paxID == pax.PassengerRequestorID));
                    foreach (var item in Results.EntityList)
                    {
                        if (item != null && !string.IsNullOrEmpty(item.PassportNum))
                            item.PassportNum = Crypting.Decrypt(item.PassportNum);
                    }
                    Results.ReturnFlag = true;
                }
            }
            return Results;
        }
        #endregion

        //#region "Validate Entire Trip"
        ///// <summary>
        ///// Method to Validate Trip information
        ///// </summary>
        ///// <returns>Delete Trip status</returns>
        //private RulesException ValidateTrip(PostflightMain trip)
        //{
        //    //ValidateMain();
        //    //ValidateLegs();
        //    //ValidateCrews();
        //    //ValidatePax();
        //    //ValidateExpenses();
        //    //return TripBusinessErrors;
        //    throw new NotImplementedException();
        //}

        ///// <summary>
        ///// Method to Validate Main information
        ///// </summary>       
        //private void ValidateMain(PostflightMain postflightLeg)
        //{
        //    //Validate Leg
        //    //build any business error as required and add to tripBusinessErrors
        //    //if(tripBusinessErrors!=null)
        //    //    tripBusinessErrors = new TripBusinessErrors();
        //    //tripBusinessErrors.Add(BusinessError)
        //}

        ///// <summary>
        ///// Method to Validate Legs information
        ///// </summary>     
        //private ReturnValue<Data.Postflight.PostflightLeg> ValidateLegs(PostflightLeg postflightLeg)
        //{
        //    //Validate Leg
        //    //build any business error as required and add to tripBusinessErrors
        //    //if(tripBusinessErrors!=null)
        //    //    tripBusinessErrors = new TripBusinessErrors();
        //    //tripBusinessErrors.Add(BusinessError)
        //    throw new NotImplementedException();
        //}

        ///// <summary>
        ///// Method to Validate Crews information
        ///// </summary>
        ///// <returns>Validate Trip Crew</returns>
        //private void ValidateCrews(PostflightCrew postflightCrew)
        //{
        //    //Validate Leg
        //    //build any business error as required and add to tripBusinessErrors
        //    //if(tripBusinessErrors!=null)
        //    //    tripBusinessErrors = new TripBusinessErrors();
        //    //tripBusinessErrors.Add(BusinessError)
        //}

        ///// <summary>
        ///// Method to Validate Pax information
        ///// </summary>
        ///// <returns>Validate Trip Pax</returns>
        //private void ValidatePax(PostflightPassenger postflightPassenger)
        //{
        //    //Validate Leg
        //    //build any business error as required and add to tripBusinessErrors
        //    //if(tripBusinessErrors!=null)
        //    //    tripBusinessErrors = new TripBusinessErrors();
        //    //tripBusinessErrors.Add(BusinessError)
        //}

        ////Add more methods as require
        //#endregion

        #region "Trip Expenses"
        public ReturnValue<Data.Postflight.GetPostflightExpenseList> GetExpList()
        {
            ReturnValue<Data.Postflight.GetPostflightExpenseList> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Postflight.GetPostflightExpenseList>();
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        Results.EntityList = Container.GetPostflightExpenseList(CustomerID).ToList();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return Results;
        }

        public ReturnValue<Data.Postflight.PostflightExpense> GetTripExpense(PostflightExpense expense)
        {
            ReturnValue<Data.Postflight.PostflightExpense> Results = null;
            Data.Postflight.PostflightExpense tripExpense = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(expense))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    expense.CustomerID = CustomerID;
                    Results = new ReturnValue<Data.Postflight.PostflightExpense>();
                    tripExpense = new PostflightExpense();
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;

                        tripExpense = (from TripExpenses in Container.PostflightExpenses
                                       where (TripExpenses.PostflightExpenseID == expense.PostflightExpenseID)
                                       select TripExpenses).FirstOrDefault();

                        SetLazyLoadProps_ForExpense(tripExpense, ref Container);

                        Results.EntityInfo = tripExpense;
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }

            return Results;
        }

        /// <summary>
        /// Method to set load property for referenced objects
        /// </summary>
        /// <param name="trip"></param>
        /// <returns></returns>
        private void SetLazyLoadProps_ForExpense(Data.Postflight.PostflightExpense expense, ref PostflightDataModelContainer container)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(expense, container))
            {
                if (expense != null)
                {
                    if (expense.POLogID != null && expense.POLogID > 0) container.LoadProperty(expense, c => c.PostflightMain);
                    if (expense.POLegID != null && expense.POLegID > 0) container.LoadProperty(expense, c => c.PostflightLeg);
                    if (expense.CustomerID != null && expense.CustomerID > 0) container.LoadProperty(expense, c => c.Customer);
                    if (expense.CrewID != null && expense.CrewID > 0) container.LoadProperty(expense, c => c.Crew);
                    if (expense.AccountID != null && expense.AccountID > 0) container.LoadProperty(expense, c => c.Account);
                    if (expense.AirportID != null && expense.AirportID > 0) container.LoadProperty(expense, c => c.Airport);
                    if (expense.HomebaseID != null && expense.HomebaseID > 0) container.LoadProperty(expense, c => c.Company);
                    //if (expense.Company != null && expense.Company.HomebaseID > 0) container.LoadProperty(expense, c => c.Company.Airport);
                    if (expense.FBOID != null && expense.FBOID > 0) container.LoadProperty(expense, c => c.FBO);
                    if (expense.FleetID != null && expense.FleetID > 0) container.LoadProperty(expense, c => c.Fleet);
                    if (expense.FlightCategoryID != null && expense.FlightCategoryID > 0) container.LoadProperty(expense, c => c.FlightCatagory);
                    if (expense.FuelLocatorID != null && expense.FuelLocatorID > 0) container.LoadProperty(expense, c => c.FuelLocator);
                    if (expense.PaymentTypeID != null && expense.PaymentTypeID > 0) container.LoadProperty(expense, c => c.PaymentType);
                    if (expense.PaymentVendorID != null && expense.PaymentVendorID > 0) container.LoadProperty(expense, c => c.Vendor);
                    if (expense.PostflightNotes != null) container.LoadProperty(expense, c => c.PostflightNotes);
                }
            }
        }

        /// <summary>
        /// Method to update Trip information
        /// </summary>
        /// <returns>Trip entity</returns>
        public ReturnValue<Data.Postflight.PostflightExpense> UpdateExpenseCatalog(PostflightExpense expense)
        {
            ReturnValue<Data.Postflight.PostflightExpense> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(expense))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<PostflightExpense>();
                    try
                    {
                        LockManager<Data.Postflight.PostflightExpense> lockManager = new LockManager<PostflightExpense>();
                        lockManager.Lock(FlightPak.Common.Constants.EntitySet.Postflight.PostFlightExpenses, expense.PostflightExpenseID);

                        using (Container = new PostflightDataModelContainer())
                        {
                            Container.ContextOptions.LazyLoadingEnabled = false;
                            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                            Container.ContextOptions.ProxyCreationEnabled = false;

                            //Update operation goes here
                            UpdateExpenseObjectState(expense, ref Container);

                            Container.SaveChanges();

                            PostflightExpense ret = new PostflightExpense();
                            ret.POLogID = expense.POLogID;
                            ret.PostflightExpenseID = expense.PostflightExpenseID;
                            ReturnValue.EntityInfo = ret;
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception e)
                    {
                        //Manually Handled
                        ReturnValue.ReturnFlag = false;
                        ReturnValue.ErrorMessage = e.Message;
                    }
                    finally
                    {
                        LockManager<Data.Postflight.PostflightExpense> lockManager = new LockManager<PostflightExpense>();
                        lockManager.UnLock(FlightPak.Common.Constants.EntitySet.Postflight.PostFlightExpenses, expense.PostflightExpenseID);
                    }

                }, FlightPak.Common.Constants.Policy.DataLayer);

                return ReturnValue;
            }

        }

        /// <summary>
        /// Method to save object state to either added or modified for each object
        /// </summary>
        /// <param name="SourceObject">Trip Object to set state</param>
        /// <param name="Container">Entity Framework Container object to save the state</param>
        private void UpdateExpenseObjectState(PostflightExpense Expense, ref PostflightDataModelContainer Container)
        {
            PostflightExpense ExistingPOExpenseEntity = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(Expense, Container))
            {
                long POExpenseID = Expense.PostflightExpenseID;
                Expense.CustomerID = CustomerID;
                Expense.LastUpdUID = UserPrincipal.Identity.Name;

                if (Expense.State == Data.Postflight.TripEntityState.Added)
                {
                    Container.PostflightExpenses.AddObject(Expense);
                }
                else if (Expense.State == Data.Postflight.TripEntityState.Modified)
                {
                    if (POExpenseID > 0)
                    {
                        ExistingPOExpenseEntity = Container.PostflightExpenses
                   .Where(poExpense => poExpense.PostflightExpenseID == POExpenseID).SingleOrDefault();

                        if (ExistingPOExpenseEntity != null)
                            Container.PostflightExpenses.Attach(ExistingPOExpenseEntity);

                        Container.PostflightExpenses.ApplyCurrentValues(Expense);
                    }
                }
                if (Expense.PostflightNotes != null && Expense.PostflightNotes.Count > 0)
                {
                    PostflightNote PONote = Expense.PostflightNotes.FirstOrDefault();
                    long PONotesID = (long)PONote.PostflightNoteID;
                    PONote.PostflightExpenseID = Expense.PostflightExpenseID;
                    PostflightNote ExistingPOExpenseNoteEntity = null;
                    switch (PONote.State)
                    {
                        case Data.Postflight.TripEntityState.Added:
                            {
                                if (ExistingPOExpenseEntity != null)
                                {
                                    ExistingPOExpenseEntity.PostflightNotes.Add(PONote);
                                }
                                Container.ObjectStateManager.ChangeObjectState(PONote, System.Data.EntityState.Added);
                            }
                            break;
                        case Data.Postflight.TripEntityState.Modified:
                            {
                                ExistingPOExpenseNoteEntity = Container.PostflightNotes
                               .Where(poExpenseNote => poExpenseNote.PostflightNoteID == PONotesID).SingleOrDefault();

                                if (ExistingPOExpenseNoteEntity != null)
                                    Container.PostflightNotes.Attach(ExistingPOExpenseNoteEntity);

                                Container.PostflightNotes.ApplyCurrentValues(PONote);
                            }
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Method to delete a Postflight Expense
        /// </summary>
        /// <param name="SourceObject">Trip Object to Delete</param>        
        public ReturnValue<Data.Postflight.PostflightExpense> DeleteExpenseCatalog(long PostflightExpenseID)
        {
            ReturnValue<Data.Postflight.PostflightExpense> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PostflightExpenseID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    ReturnValue = new ReturnValue<PostflightExpense>();
                    try
                    {
                        using (Container = new PostflightDataModelContainer())
                        {
                            Container.ContextOptions.LazyLoadingEnabled = false;
                            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                            Container.ContextOptions.ProxyCreationEnabled = false;

                            PostflightExpense ExistingPOExpenseEntity = Container.PostflightExpenses
                               .Where(poExpense => poExpense.PostflightExpenseID == PostflightExpenseID && poExpense.CustomerID == CustomerID).SingleOrDefault();

                            Container.PostflightExpenses.DeleteObject(ExistingPOExpenseEntity);

                            Container.SaveChanges();

                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (Exception e)
                    {
                        //Manually Handled
                        ReturnValue.ReturnFlag = false;
                        ReturnValue.ErrorMessage = e.Message;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        #endregion

        #region OtherCrewDuty
        /// <summary>
        /// 
        /// </summary>
        /// <param name="trip"></param>
        /// <returns></returns>
        public ReturnValue<Data.Postflight.PostflightSimulatorLog> UpdateOtherCrewDutyLog(PostflightSimulatorLog OtherCrewLog)
        {
            ReturnValue<Data.Postflight.PostflightSimulatorLog> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(OtherCrewLog))
            {
                ReturnValue = new ReturnValue<PostflightSimulatorLog>();
                using (Container = new PostflightDataModelContainer())
                {
                    try
                    {
                        bool isSaveOtherCrewDuty = true;
                        ReturnValue.ReturnFlag = base.Validate(OtherCrewLog, ref ReturnValue.ErrorMessage);

                        if (!ReturnValue.ReturnFlag)
                        {
                            isSaveOtherCrewDuty = false;
                            ReturnValue.ReturnFlag = false;
                        }

                        if (isSaveOtherCrewDuty)
                        {
                            Container.ContextOptions.LazyLoadingEnabled = false;
                            Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                            Container.ContextOptions.ProxyCreationEnabled = false;

                            if (UserPrincipal.Identity.Name != null)
                                OtherCrewLog.LastUserID = UserPrincipal.Identity.Name.Trim();

                            OtherCrewLog.CustomerID = CustomerID;
                            UpdateOtherCrewDutyObjectState(ref OtherCrewLog, ref Container);
                            Container.SaveChanges();

                            PostflightSimulatorLog simLogObj = new PostflightSimulatorLog();
                            simLogObj.CustomerID = CustomerID;
                            simLogObj.SimulatorID = OtherCrewLog.SimulatorID;
                            ReturnValue.EntityInfo = simLogObj;
                            ReturnValue.ReturnFlag = true;
                        }
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        //Manually Handled
                        Container.SaveChanges();
                        PostflightSimulatorLog ocret = new PostflightSimulatorLog();
                        ocret.CustomerID = CustomerID;
                        ReturnValue.EntityInfo = ocret;
                        ReturnValue.ReturnFlag = true;
                    }
                    catch (Exception ex)
                    {
                        //Manually Handled
                        ReturnValue.ErrorMessage = ex.Message;
                        ReturnValue.ReturnFlag = false;
                    }
                }
                return ReturnValue;
            }
        }

        /// <summary>
        /// Method to save object state to either added or modified for each object
        /// </summary>
        /// <param name="SourceObject">Trip Object to set state</param>
        /// <param name="Container">Entity Framework Container object to save the state</param>
        private void UpdateOtherCrewDutyObjectState(ref PostflightSimulatorLog SourceObject, ref PostflightDataModelContainer Container)
        {
            PostflightSimulatorLog ExistingPOOtherCrewDutyEntity = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SourceObject, Container))
            {
                long POSimulatorID = SourceObject.SimulatorID;

                if (SourceObject.State == Data.Postflight.TripEntityState.Added)
                {
                    if (POSimulatorID > 0)
                    {
                        ExistingPOOtherCrewDutyEntity = Container.PostflightSimulatorLogs
                   .Where(poOtherCrew => poOtherCrew.SimulatorID == POSimulatorID).SingleOrDefault();

                        Container.PostflightSimulatorLogs.Attach(ExistingPOOtherCrewDutyEntity);
                        Container.PostflightSimulatorLogs.ApplyCurrentValues(SourceObject);
                    }
                    else
                    {
                        Container.PostflightSimulatorLogs.AddObject(SourceObject);
                    }
                }
                else if (SourceObject.State == Data.Postflight.TripEntityState.Modified)
                {
                    if (POSimulatorID > 0)
                    {
                        ExistingPOOtherCrewDutyEntity = Container.PostflightSimulatorLogs
                   .Where(poOtherCrew => poOtherCrew.SimulatorID == POSimulatorID).SingleOrDefault();

                        Container.PostflightSimulatorLogs.Attach(ExistingPOOtherCrewDutyEntity);
                        Container.PostflightSimulatorLogs.ApplyCurrentValues(SourceObject);
                    }
                }
            }
        }

        public ReturnValue<Data.Postflight.PostflightSimulatorLog> DeleteOtherCrewDutyLog(long OtherCrewSimulatorID)
        {
            ReturnValue<Data.Postflight.PostflightSimulatorLog> ReturnValue = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(OtherCrewSimulatorID))
            {

                ReturnValue = new ReturnValue<PostflightSimulatorLog>();
                try
                {
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.UseLegacyPreserveChangesBehavior = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;

                        PostflightSimulatorLog ExistingPOOtherCrewDutyEntity = Container.PostflightSimulatorLogs
                             .Where(poOtherCrew => poOtherCrew.SimulatorID == OtherCrewSimulatorID).SingleOrDefault();

                        Container.PostflightSimulatorLogs.DeleteObject(ExistingPOOtherCrewDutyEntity);

                        Container.SaveChanges();

                        ReturnValue.ReturnFlag = true;
                    }
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    ReturnValue.ReturnFlag = false;
                    ReturnValue.ErrorMessage = ex.Message;
                }

            }
            return ReturnValue;
        }

        public ReturnValue<Data.Postflight.GetOtherCrewDutyLog> GetOtherCrewDutyLog()
        {
            ReturnValue<Data.Postflight.GetOtherCrewDutyLog> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Postflight.GetOtherCrewDutyLog>();
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        Results.EntityList = Container.GetOtherCrewDutyLog(CustomerID).ToList();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }

        #endregion

        #region "Passenger passport"

        public ReturnValue<Data.Postflight.GetPassportByPassengerID> GetPassportByPassengerID(Int64 PassengerID, string PassportNum)
        {
            ReturnValue<Data.Postflight.GetPassportByPassengerID> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(PassengerID, PassportNum))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Postflight.GetPassportByPassengerID>();
                    using (Container = new PostflightDataModelContainer())
                    {
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        Results.EntityList = Container.GetPassportByPassengerID(CustomerID, PassengerID, PassportNum).ToList();
                        foreach (var item in Results.EntityList)
                        {
                            if (item != null && !string.IsNullOrEmpty(item.PassportNum))
                                item.PassportNum = Crypting.Decrypt(item.PassportNum);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }

        #endregion

        /// <summary>
        /// Method to Get Minutes from Decimal Value
        /// </summary>
        /// <param name="time">Pass Time Value</param>
        /// <returns>Retruns Converted Value into Minutes</returns>
        public string ConvertTenthsToMins(String time)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(time))
            {
                string result = "00:00";
                decimal tm_tenth = Math.Round(Convert.ToDecimal(time), 3);
                Int32 tm_mins = Convert.ToInt32(Math.Round(tm_tenth * 60, 3));
                TimeSpan ts = new TimeSpan(0, Convert.ToInt32(tm_mins), 0);
                string hrs = ((ts.Days * 24) + ts.Hours).ToString();
                string mins = ts.Minutes.ToString();
                if (hrs.Length == 1)
                    hrs = "0" + hrs;
                if (mins.Length == 1)
                    mins = "0" + mins;
                result = hrs + ":" + mins;
                return result;
            }
        }

        /// <summary>
        /// Method to return Trip informations based on Parameters
        /// </summary>
        /// <returns>Postflight Main Trip List For Report</returns>
        public ReturnValue<Data.Postflight.GetPOReportSearch> GetPoReportList(Int64 HomeBaseID, Int64 ClientID, Int64 FleetID, bool IsPersonal, bool IsCompleted, DateTime StartDate, DateTime EndDate, Int64 FromLogNum, Int64 ToLogNum)
        {
            ReturnValue<Data.Postflight.GetPOReportSearch> Results = null;
            Data.Postflight.PostflightDataModelContainer Container = null;

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(HomeBaseID, ClientID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromLogNum, ToLogNum))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Results = new ReturnValue<Data.Postflight.GetPOReportSearch>();
                    using (Container = new PostflightDataModelContainer())
                    {
                        if (ClientID == 0)
                            ClientID = Convert.ToInt64(UserPrincipal.Identity.ClientId);

                        Container.ContextOptions.ProxyCreationEnabled = false;
                        Results.EntityList = Container.GetPOReportSearch(CustomerID, ClientID, HomeBaseID, FleetID, IsPersonal, IsCompleted, StartDate, EndDate, FromLogNum, ToLogNum).ToList();
                        Results.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return Results;
        }
    }
}