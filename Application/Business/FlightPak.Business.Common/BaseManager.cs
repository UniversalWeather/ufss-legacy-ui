﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data.Objects.DataClasses;

namespace FlightPak.Business.Common
{
    public class BaseManager
    {
        private ExceptionManager exManager;

        protected Int64 CustomerID
        {
            get { return UserPrincipal.Identity.CustomerID; }
        }
        protected Int64? ClientId
        {
            get { return UserPrincipal.Identity.ClientId; }
        }

        protected Int64? HomeBaseID
        {
            get { return UserPrincipal.Identity.HomeBaseId; }
        }

        protected string UserName
        {
            get { return UserPrincipal.Identity.Name; }
        }
        protected FlightPak.Framework.Security.FPPrincipal UserPrincipal
        {
            get
            {
                // If the current principal is null, this will throw an Invalid cast exception. To handle that one 
                using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
                {
                    //Handle methods throguh exception manager with return flag
                    exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                    return exManager.Process<FlightPak.Framework.Security.FPPrincipal>(() =>
                    {
                        return (FlightPak.Framework.Security.FPPrincipal)System.Threading.Thread.CurrentPrincipal;
                    }, FlightPak.Common.Constants.Policy.DataLayer);
                }
            }
        }
        public BaseManager()
        {
        }
        protected bool Validate(object validationObject, ref string errormessage)
        {
            var context = new ValidationContext(validationObject, serviceProvider: null, items: null);
            var results = new List<ValidationResult>();
            var entityObject = (EntityObject)validationObject;
            Int64 entityKey = (entityObject == null ? 0 :
              entityObject.EntityKey == null ? 0 :
                entityObject.EntityKey.EntityKeyValues.Length == 0 ? 0 :
                  Convert.ToInt64(entityObject.EntityKey.EntityKeyValues[0].Value.ToString()));
            //If entity key > 0 assume we are in edit mode
            if (CheckLock && entityKey > 0)
            {
                var obj = new LockManager<EntityObject>().CheckLock(entityObject.EntityKey.EntitySetName, entityKey);
                if (!obj)
                {
                    errormessage = "Record already locked";
                    return false;
                }
            }
            var isValid = Validator.TryValidateObject(validationObject, context, results, true);
            foreach (var items in results)
            {
                errormessage = errormessage + items.ErrorMessage + "\\n";
            }

            return isValid;
        }

        protected bool CheckLock
        {
            get;
            set;
        }
    }
}
