﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FlightPak.Business.Common
{
    [DataContract(Name = "ReturnValueOf{0}")]
    public class ReturnValue<T>
    {
        [DataMember(Name = "ReturnFlag")]
        public bool ReturnFlag = false;
        [DataMember(Name = "ErrorMessage")]
        public string ErrorMessage = string.Empty;
        [DataMember(Name = "EntityList")]
        public List<T> EntityList;
        [DataMember(Name = "LockMessage")]
        public string LockMessage = string.Empty;
        [DataMember(Name = "RulesExceptionFlag")]
        public bool RulesExceptionFlag = false;
        [DataMember(Name = "ExceptionList")]
        public List<RulesException> ExceptionList;
        /// Method to return an entity instead of a list, required for Preflight and Postflight
        [DataMember(Name = "EntityInfo")]
        public T EntityInfo;
    }
}
