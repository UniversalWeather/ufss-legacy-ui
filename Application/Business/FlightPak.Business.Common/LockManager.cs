﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Common
{
    public class LockManager<TEntity> : BaseManager where TEntity : class
    {
        private ExceptionManager exManager;
        public ReturnValue<TEntity> Lock(string entitySetName, Int64 primaryKey)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(entitySetName, primaryKey))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<ReturnValue<TEntity>>(() =>
                {
                    using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
                    {
                        ReturnValue<TEntity> returnValue = new ReturnValue<TEntity>();
                        ObjectParameter param = new ObjectParameter(LockingConstants.ReturnValue, typeof(int));
                        ObjectParameter param1 = new ObjectParameter(LockingConstants.ReturnMessage, typeof(string));
                        context.Lock(UserPrincipal.Identity.CustomerID, entitySetName, UserPrincipal.Identity.SessionID, UserPrincipal.Identity.Name, primaryKey, LockingConstants.Lock, param, param1);
                        switch ((int)param.Value)
                        {
                            case -101:
                                returnValue.ReturnFlag = true;
                                returnValue.LockMessage = LockingConstants.Status101;
                                break;
                            case -102:
                                returnValue.ReturnFlag = false;
                                returnValue.LockMessage = LockingConstants.Status102 + (string)param1.Value;
                                break;
                            default:
                                returnValue.ReturnFlag = false;
                                returnValue.LockMessage = LockingConstants.StatusUnknown;
                                break;
                        }
                        return returnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

            //return new ReturnValue<TEntity>();
        }

        public ReturnValue<TEntity> UnLock(string entitySetName, Int64 primaryKey)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(entitySetName, primaryKey))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<ReturnValue<TEntity>>(() =>
                {
                    using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
                    {
                        ReturnValue<TEntity> returnValue = new ReturnValue<TEntity>();
                        ObjectParameter param = new ObjectParameter(LockingConstants.ReturnValue, typeof(int));
                        ObjectParameter param1 = new ObjectParameter(LockingConstants.ReturnMessage, typeof(string));
                        context.Lock(UserPrincipal.Identity.CustomerID, entitySetName, UserPrincipal.Identity.SessionID, UserPrincipal.Identity.Name, primaryKey, LockingConstants.UnLock, param, param1);
                        switch ((int)param.Value)
                        {
                            case -103:
                                returnValue.ReturnFlag = false;
                                returnValue.LockMessage = LockingConstants.Status103;
                                break;
                            case -104:
                                returnValue.ReturnFlag = true;
                                returnValue.LockMessage = LockingConstants.Status104;
                                break;
                            default:
                                returnValue.ReturnFlag = false;
                                returnValue.LockMessage = LockingConstants.StatusUnknown;
                                break;
                        }
                        return returnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        public bool CheckLock(string entitySetName, Int64 primaryKey)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(entitySetName, primaryKey))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
                    {
                        return context.CheckLock(entitySetName, base.UserPrincipal.Identity.SessionID, primaryKey).FirstOrDefault() > 0;
                        
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }
        }
    }

    internal class LockingConstants
    {
        public const string Status101 = "Locked successfully";
        public const string Status102 = "Record already locked by user: ";
        public const string Status103 = "Unlock failiure";
        public const string Status104 = "Unlocked successfully";
        public const string StatusUnknown = "Unkown error happened while locking";
        public const string Lock = "LOCK";
        public const string UnLock = "UNLOCK";
        public const string ReturnValue = "ReturnValue";
        public const string ReturnMessage = "ReturnMessage";
    }
}
