﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Common
{
    public class LockDetails<TEntity> : BaseManager where TEntity : class
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Common.GetAllLocks> GetAllLocks()
        {


            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<ReturnValue<Data.Common.GetAllLocks>>(() =>
                {
                    using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
                    {
                        ReturnValue<Data.Common.GetAllLocks> returnValue = new ReturnValue<Data.Common.GetAllLocks>();
                        //context.GetAllLocks(UserPrincipal.Identity.CustomerID);
                        returnValue.ReturnFlag = true;
                        returnValue.EntityList = context.GetAllLocks(CustomerID).ToList();
                        return returnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }


        public ReturnValue<Data.Common.GetAllLocks> DeleteLock(Int64 FPLockID)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(FPLockID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<ReturnValue<Data.Common.GetAllLocks>>(() =>
                {
                    using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
                    {
                        ReturnValue<Data.Common.GetAllLocks> returnValue = new ReturnValue<Data.Common.GetAllLocks>();
                        context.DeleteLock(FPLockID);
                        returnValue.ReturnFlag = true;
                        return returnValue;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }


    }
}