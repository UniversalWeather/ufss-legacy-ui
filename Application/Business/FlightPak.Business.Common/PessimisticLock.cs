﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Objects.DataClasses;

namespace FlightPak.Business.Common
{
    public class PessimisticLock<TEntity> where TEntity : class
    {
        public ReturnValue<TEntity> Lock(string entitySetName, long primaryKey, string currentUser, Nullable<long> customerId = null, Nullable<Guid> sessionId = null)
        {
            using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
            {
                ReturnValue<TEntity> returnValue = new ReturnValue<TEntity>();
                ObjectParameter param = new ObjectParameter(LockingConstants.ReturnValue, typeof(int));
                //context.Lock(customerId, primaryKey, sessionId, currentUser, (long?)entitySetName.EntityKey.EntityKeyValues[0].Value, LockingConstants.Lock, param);
                context.Lock(customerId, entitySetName, sessionId, currentUser, primaryKey, LockingConstants.Lock, param);
                switch ((int)param.Value)
                {
                    case -101:
                        returnValue.ReturnFlag = true;
                        returnValue.LockMessage = LockingConstants.Status101;
                        break;
                    case -102:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.Status102;
                        break;
                    default:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.StatusUnknown;
                        break;
                }
                return returnValue;
            }
            //return new ReturnValue<TEntity>();
        }

        public ReturnValue<TEntity> UnLock(string entitySetName, long primaryKey, string currentUser, Nullable<long> customerId = null, Nullable<Guid> sessionId = null)
        {
            using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
            {
                ReturnValue<TEntity> returnValue = new ReturnValue<TEntity>();
                ObjectParameter param = new ObjectParameter(LockingConstants.ReturnValue, typeof(int));
                //context.Lock(customerId, entity.EntityKey.EntitySetName, sessionId, currentUser, (long?)entity.EntityKey.EntityKeyValues[0].Value, LockingConstants.UnLock, param);
                context.Lock(customerId, entitySetName, sessionId, currentUser, primaryKey, LockingConstants.UnLock, param);
                switch ((int)param.Value)
                {
                    case -103:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.Status103;
                        break;
                    case -104:
                        returnValue.ReturnFlag = true;
                        returnValue.LockMessage = LockingConstants.Status104;
                        break;
                    default:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.StatusUnknown;
                        break;
                }
                return returnValue;
            }
        }
    }

    internal class LockingConstants
    {
        public const string Status101 = "Locked successfully";
        public const string Status102 = "Record already locked";
        public const string Status103 = "Unlock failiure";
        public const string Status104 = "Unlocked successfully";
        public const string StatusUnknown = "Unkown error happened while locking";
        public const string Lock = "LOCK";
        public const string UnLock = "UNLOCK";
        public const string ReturnValue = "ReturnValue";
    }
}
