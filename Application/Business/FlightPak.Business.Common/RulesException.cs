﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.Common
{
    public enum Severity { Information = 0, Warning = 1, Critical = 2 }

    public enum Group { Main = 0, Legs = 1, Crew = 2, Passenger = 3, FBO = 4, Catering = 5, Expenses = 6, CorporateRequest = 7 }
    
    public class RulesException
    {
        public string ExceptionMessage = string.Empty;
        public Severity ExceptionSeverity;
        public Group ExceptionGroup;
    }
}
