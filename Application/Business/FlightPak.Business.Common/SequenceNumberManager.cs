﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Common
{
    public class SequenceNumberManager : BaseManager
    {
        private ExceptionManager exManager;
        public Nullable<Int64> GetSequenceNumber(string ModuleName)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(ModuleName))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<Nullable<Int64>>(() =>
                {
                    using (Data.Common.CommonModelContainer context = new Data.Common.CommonModelContainer())
                    {
                        ObjectParameter param = new ObjectParameter("LV_PID", typeof(Int64));
                        context.GetSequenceNumber(base.CustomerID, ModuleName, param);
                        return (Int64)param.Value == 0 ? (Int64?)null : (Int64)param.Value;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }
    }
}
