﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data;
using System.ComponentModel;

namespace FlightPak.Business.Common
{
    public static class EMCommon
    {
        public static void SetAllModified<T>(this T entity, ObjectContext context) where T : IEntityWithKey
        {

            var stateEntry = context.ObjectStateManager.GetObjectStateEntry(entity.EntityKey);

            var propertyNameList = stateEntry.CurrentValues.DataRecordInfo.FieldMetadata.Select(pn => pn.FieldType.Name);

            foreach (var propName in propertyNameList)
            {

                stateEntry.SetModifiedProperty(propName);

            }

        }
        public static DataTable ToDataTable<T>(this List<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name != "Customer" && prop.Name != "CustomerReference" && prop.Name != "EntityKey" &&
                    prop.Name != "EntityState" && prop.Name != "UMPermission" && prop.Name != "UMPermissionReference" && prop.Name != "UserGroup" && prop.Name != "UserGroupReference")
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.Name != "Customer" && prop.Name != "CustomerReference" && prop.Name != "EntityKey" &&
                    prop.Name != "EntityState" && prop.Name != "UMPermission" && prop.Name != "UMPermissionReference" && prop.Name != "UserGroup" && prop.Name != "UserGroupReference")
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

    }
}
