﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Collections.ObjectModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using FlightPak.Framework.Caching;

namespace FlightPak.Business.Common
{
    public class SystemPreferenceManager : BaseManager
    {
        private ExceptionManager exManager;
        public string GetUserDefaultCalendarView()
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<string>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;

                        var userPreference = container.FPSystemPrefs.Where(x => x.ReqUID == UserName && x.CustomerID == CustomerID).FirstOrDefault();
                        return userPreference != null ? userPreference.CalanderPage : null;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        public bool UpdateCalendarDefaultView(string viewMode)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(viewMode))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;

                        container.UpdateDefaultCalendarView(CustomerID, UserName, viewMode);
                        return true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }


        #region Utilities - User Preferences

        /// <summary>
        /// Method to Get List of User Preferences for Widgets
        /// </summary>
        /// <param name="userGroupID">Pass User Group ID</param>
        /// <returns>Returns UserPreferenceUtilities List</returns>
        public ReturnValue<Data.Common.GetUserPreferenceUtilitiesList> GetUserPreferenceList(Int64 userGroupID)
        {
            ReturnValue<Data.Common.GetUserPreferenceUtilitiesList> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetUserPreferenceUtilitiesList>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetUserPreferenceUtilitiesList(UserName, CustomerID, userGroupID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// Method to Add New User Preference
        /// </summary>
        /// <param name="categoryName">Pass Category Name (optional)</param>
        /// <param name="subCategoryName">Pass Sub-Category Name (optional)</param>
        /// <param name="keyName">Pass Key Name</param>
        /// <param name="keyValue">Pass Key Value</param>
        /// <returns>Returns Boolean Value</returns>
        public bool AddUserPreference(string categoryName, string subCategoryName, string keyName, string keyValue)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(categoryName, subCategoryName, keyName, keyValue))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        var result = container.AddUserPreference(UserName, CustomerID, categoryName, subCategoryName, keyName, keyValue);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                        // Once the Preference is updated, we should clear all the objects in cache.
                        var cacheManager = new CacheManager();
                        cacheManager.RemoveAll();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        /// Method to Delete User Preference
        /// </summary>
        public void DeleteUserPreference()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        container.DeleteUserPreference(UserName, CustomerID);
                        // Once the Preference is updated, we should clear all the objects in cache.
                        var cacheManager = new CacheManager();
                        cacheManager.RemoveAll();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        #endregion

        #region "User Preference - Bookmarks"
        public ReturnValue<Data.Common.GetBookmark> GetUserPreference(Data.Common.UserPreference oUserPreference)
        {
            ReturnValue<Data.Common.GetBookmark> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oUserPreference))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer())
                    {
                        // Fix for recursive infinite loop
                        ReturnValue = new ReturnValue<Data.Common.GetBookmark>();
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        ReturnValue.EntityList = Container.GetBookmark(CustomerID, oUserPreference.UserName, oUserPreference.CategoryName).ToList();
                        ReturnValue.ReturnFlag = true;
                    }                    
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        public ReturnValue<Data.Common.UserPreference> AddBookmark(Data.Common.UserPreference oUserPreference)
        {
            ReturnValue<Data.Common.UserPreference> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oUserPreference))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer())
                    {
                        // Fix for recursive infinite loop
                        ReturnValue = new ReturnValue<Data.Common.UserPreference>();
                        Container.ContextOptions.LazyLoadingEnabled = false;
                        Container.ContextOptions.ProxyCreationEnabled = false;
                        ReturnValue.EntityList = Container.AddBookmark(oUserPreference.UserPreferenceID,
                                                                        oUserPreference.CustomerID,
                                                                        oUserPreference.UserName,
                                                                        oUserPreference.CategoryName,
                                                                        oUserPreference.SubCategoryName,
                                                                        oUserPreference.KeyName,
                                                                        oUserPreference.KeyValue,
                                                                        oUserPreference.LastUpdTS).ToList();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        #endregion "User Preference - Bookmarks"
    }
}
