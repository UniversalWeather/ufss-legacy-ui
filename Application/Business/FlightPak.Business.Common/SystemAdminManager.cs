﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Common
{
    public class SystemAdminManager : BaseManager
    {
        private ExceptionManager exManager;

        /// <summary>
        /// Method to Change Crew from Old to New Record
        /// </summary>
        /// <param name="oldCrewID">Pass Old Crew ID</param>
        /// <param name="newCrewID">Pass New Crew ID</param>
        /// <param name="crewCD">Pass New Crew Code</param>
        /// <returns>Returns Boolean Value</returns>
        public bool ChangeCrew(long oldCrewID, long newCrewID, string crewCD)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldCrewID, newCrewID, crewCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        var result = container.ChangeCrew(oldCrewID, newCrewID, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name, crewCD);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        /// Method to Change Airport from Old to New Record
        /// </summary>
        /// <param name="oldIcaoId">Pass Old ICAO ID</param>
        /// <param name="newIcaoId">Pass New ICAO ID</param>
        /// <returns>Returns Boolean Value</returns>
        public bool ChangeAirport(string oldIcaoId, string newIcaoId)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldIcaoId, newIcaoId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        var result = container.ChangeAirport(oldIcaoId, newIcaoId, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        ///  Method to Change Fleet from Old to New Record
        /// </summary>
        /// <param name="oldFleetId">Pass Old Fleet ID</param>
        ///  /// <param name="newTailNum">New Tail Num</param>
        /// <param name="newFleetId">Pass New Fleet ID</param>
        /// <param name="effectiveDTTM">Pass Effective Date Time (Optional)</param>
        /// <returns>Returns Boolean Value</returns>
        public bool ChangeFleet(long oldFleetId, string newTailNum, DateTime? effectiveDTTM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldFleetId, newTailNum, effectiveDTTM))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        DateTime? effectivedate = effectiveDTTM;

                        var result = container.ChangeFleet(oldFleetId, newTailNum, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name, effectivedate);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        /// Method to Change Passenger from Old to New Record
        /// </summary>
        /// <param name="oldPassengerID">Pass Old Passenger ID</param>
        /// <param name="newPassengerID">Pass New Passenger ID</param>
        /// <param name="paxCD">Pass New Passenger Code</param>
        /// <returns>Returns Boolean Value</returns>
        public bool ChangePassenger(long oldPassengerID, long newPassengerID, string paxCD)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldPassengerID, newPassengerID, paxCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        var result = container.ChangePassenger(oldPassengerID, newPassengerID, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name, paxCD);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        /// Method to Retrieve Preflight Trip
        /// </summary>
        /// <param name="tripId">Pass Trip ID</param>
        /// <returns>Returns Boolean Value</returns>
        public void RetrievePreflightTrip(long tripId)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(tripId))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        container.RetrievePreflightTrip(tripId, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name);
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        /// Method to Change Account Number from Old to New Record
        /// </summary>
        /// <param name="oldAccountID">Pass Old Account ID</param>
        /// <param name="newAccountID">Pass New Account ID</param>
        /// <param name="oldAccountNum">Pass Old Account Number</param>
        /// <param name="newAccountNum">Pass New Account Number</param>
        /// <returns>Returns Boolean Value</returns>
        public bool ChangeAccountNumber(long oldAccountID, long newAccountID, string oldAccountNum, string newAccountNum)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldAccountID, newAccountID, oldAccountNum, newAccountNum))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        var result = container.ChangeAccountNumber(oldAccountID, newAccountID, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name, oldAccountNum, newAccountNum);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        /// <summary>
        /// To get the values of CrewPAXCleanUpList
        /// </summary>
        /// <param name="orderBy"></param>
        /// <param name="showInactive"></param>
        /// <param name="reportTime"></param>
        /// <returns></returns>
        public ReturnValue<Data.Common.GetCrewPAXCleanUpList> GetCrewPAXCleanUpList(string orderBy, bool showInactive, DateTime reportTime)
        {
            ReturnValue<Data.Common.GetCrewPAXCleanUpList> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetCrewPAXCleanUpList>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetCrewPAXCleanUpList(UserName, orderBy, showInactive, reportTime).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        /// InactivateCrewPAXForCleanUp
        /// </summary>
        /// <param name="orderBy"></param>
        /// <param name="showInactive"></param>
        /// <param name="reportTime"></param>
        /// <returns></returns>
        public ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp> GetInactivateCrewPAXForCleanUp(string orderBy, bool showInactive, DateTime reportTime)
        {
            ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetInactivateCrewPAXForCleanUp>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetInactivateCrewPAXForCleanUp(CustomerID, orderBy, showInactive, reportTime, UserName).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        /// <summary>
        ///  Method to Change CQCUstomerCD from Old to New Record
        /// </summary>
        /// <param name="oldCQCUstomerID">Pass Old CQCUstomer ID</param>
        ///  /// <param name="newCQCUstomerCD">New CQCUstomer Code</param>
        /// <param name="newCQCUstomerID">Pass New CQCUstomer ID</param>
        /// <param name="effectiveDTTM">Pass Effective Date Time (Optional)</param>
        /// <returns>Returns Boolean Value</returns>
        public bool ChangeCQCustomerCD(long oldCQCUstomerID, string newCQCUstomerCD, DateTime? effectiveDTTM)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oldCQCUstomerID, newCQCUstomerCD, effectiveDTTM))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        container.ContextOptions.LazyLoadingEnabled = false;
                        container.ContextOptions.ProxyCreationEnabled = false;
                        DateTime? effectivedate = effectiveDTTM;

                        var result = container.ChangeCQCustomerCD(oldCQCUstomerID, newCQCUstomerCD, UserPrincipal.Identity.CustomerID, UserPrincipal.Identity.Name, effectivedate);
                        if (result != null && result.ToList()[0].Value)
                            return true;
                        else
                            return false;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }
    }
}
