﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Business.Common
{
    public class RecentActivityManager : BaseManager
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Common.GetRecentActivityPreflight> GetRecentActivityPreflight(int MaxRecordCount)
        {
            ReturnValue<Data.Common.GetRecentActivityPreflight> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetRecentActivityPreflight>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetRecentActivityPreflight(base.UserName, MaxRecordCount).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Common.GetRecentActivityPostflight> GetRecentActivityPostflight(int MaxRecordCount)
        {
            ReturnValue<Data.Common.GetRecentActivityPostflight> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetRecentActivityPostflight>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetRecentActivityPostflight(base.UserName, MaxRecordCount).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Common.GetRecentActivityCorporateRequest> GetRecentActivityCorporateRequest(int MaxRecordCount)
        {
            ReturnValue<Data.Common.GetRecentActivityCorporateRequest> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetRecentActivityCorporateRequest>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetRecentActivityCorporateRequest(base.UserName, MaxRecordCount).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Common.GetRecentActivityCharterQuote> GetRecentActivityCharterQuote(int MaxRecordCount)
        {
            ReturnValue<Data.Common.GetRecentActivityCharterQuote> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Common.CommonModelContainer Container = new Data.Common.CommonModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Common.GetRecentActivityCharterQuote>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetRecentActivityCharterQuote(base.UserName, MaxRecordCount).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

       
    }
}
