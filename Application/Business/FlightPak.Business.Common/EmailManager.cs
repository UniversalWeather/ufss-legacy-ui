﻿using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Collections.ObjectModel;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using System.IO;


namespace FlightPak.Business.Common
{
    public class EmailManager
    {
        private ExceptionManager exManager;
        /// <summary>
        /// This method is used to send email
        /// </summary>
        /// <param name="entity.CustomerID">Customer Id</param>
        /// <param name="entity.EmailPrio">Need to define</param>
        /// <param name="entity.EmailReqTime">Current Time stamp</param>
        /// <param name="entity.EmailReqUID">Requestor</param>
        /// <param name="entity.EmailSentTo">semi colon delimated recipients</param>
        /// <param name="entity.EmailStatus">Need to define (Since there is no column to set type of email, temprorily we are using this one to set type of email)</param>
        /// <param name="entity.EmailTmplID">Template Id</param>
        /// <param name="entity.EmailTranID">Primary Key Id (No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.IsDeleted">Is Deleted(No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.LastUpdTS">Last Updated Time Stamp (No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.LastUPDUID">Last updated userd Id</param>
        public void SendEmail(Data.Common.FPEmailTranLog entity, System.Collections.Generic.Dictionary<string, string> replaceParameters)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(entity))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        bool EnableEmailBatchMode = ConfigurationManager.AppSettings["EnableEmailBatchMode"] != null && ConfigurationManager.AppSettings["EnableEmailBatchMode"] == "true";
                        #region Replace all the parameters with actual value
                        var emailTemplate = container.GetEMailTemplateById(entity.EmailTmplID).ToList().FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            //Assign subject and body to current email
                            entity.EmailSubject = emailTemplate.EmailSubject;
                            entity.EmailBody = emailTemplate.EmailBody;

                            //If the config value has been set to true, Use inline mail sending and update the prior flags. Else the same mail will be handled by batch process
                            if (!EnableEmailBatchMode)
                            {
                                entity.EmailStatus = "D";
                                entity.IsDeleted = true;
                                entity.LastTryTimeStamp = DateTime.UtcNow;
                                entity.RetryCount = 1;
                            }
                            foreach (var item in replaceParameters)
                            {
                                entity.EmailBody = entity.EmailBody.Replace(item.Key, item.Value);
                            }
                        }
                        #endregion

                        container.AddToFPEmailTranLogs(entity);
                        container.SaveChanges();
                        if (!EnableEmailBatchMode)
                        {
                            SendMail(entity.EmailSentTo, entity.EmailSentTo, entity.EmailBody, entity.EmailSubject, entity.IsBodyHTML.GetValueOrDefault());
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        /// <summary>
        /// This method is used to send email
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="entity.CustomerID">Customer Id</param>
        /// <param name="entity.EmailPrio">Need to define</param>
        /// <param name="entity.EmailReqTime">Current Time stamp</param>
        /// <param name="entity.EmailReqUID">Requestor</param>
        /// <param name="entity.EmailSentTo">semi colon delimated recipients</param>
        /// <param name="entity.EmailStatus">Need to define (Since there is no column to set type of email, temprorily we are using this one to set type of email)</param>
        /// <param name="entity.EmailTmplID">Template Id</param>
        /// <param name="entity.EmailTranID">Primary Key Id (No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.IsDeleted">Is Deleted(No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.LastUpdTS">Last Updated Time Stamp (No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.LastUPDUID">Last updated userd Id</param>
        public void SendEmail(Data.Common.FPEmailTranLog entity, System.Collections.Generic.Dictionary<string, string> replaceParameters, string FromEmail, List<String> attachments)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(entity))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        bool EnableEmailBatchMode = ConfigurationManager.AppSettings["EnableEmailBatchMode"] != null && ConfigurationManager.AppSettings["EnableEmailBatchMode"] == "true";
                        #region Replace all the parameters with actual value
                        var emailTemplate = container.GetEMailTemplateById(entity.EmailTmplID).ToList().FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            //Assign subject and body to current email
                            entity.EmailSubject = emailTemplate.EmailSubject;
                            entity.EmailBody = emailTemplate.EmailBody;

                            //If the config value has been set to true, Use inline mail sending and update the prior flags. Else the same mail will be handled by batch process
                            if (!EnableEmailBatchMode)
                            {
                                entity.EmailStatus = "D";
                                entity.IsDeleted = true;
                                entity.LastTryTimeStamp = DateTime.UtcNow;
                                entity.RetryCount = 1;
                            }
                            foreach (var item in replaceParameters)
                            {
                                entity.EmailBody = entity.EmailBody.Replace(item.Key, item.Value);
                            }
                        }
                        #endregion

                        container.AddToFPEmailTranLogs(entity);
                        container.SaveChanges();
                        if (!EnableEmailBatchMode)
                        {
                            SendMail(entity.EmailSentTo, entity.EmailSentTo, entity.EmailBody, entity.EmailSubject, FromEmail, attachments, true);
                        }
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        /// <summary>
        /// This method is used to send email
        /// </summary>
        /// <param name="entity.CustomerID">Customer Id</param>
        /// <param name="entity.EmailPrio">Need to define</param>
        /// <param name="entity.EmailReqTime">Current Time stamp</param>
        /// <param name="entity.EmailReqUID">Requestor</param>
        /// <param name="entity.EmailSentTo">semi colon delimated recipients</param>
        /// <param name="entity.EmailStatus">Need to define (Since there is no column to set type of email, temprorily we are using this one to set type of email)</param>
        /// <param name="entity.EmailTmplID">Template Id</param>
        /// <param name="entity.EmailTranID">Primary Key Id (No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.IsDeleted">Is Deleted(No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.LastUpdTS">Last Updated Time Stamp (No need to set this one in entity. If this is attached, it will be ommited)</param>
        /// <param name="entity.LastUPDUID">Last updated userd Id</param>
        public void SendCalanderInvites(Data.Common.FPEmailTranLog entity)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(entity))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    using (Data.Common.CommonModelContainer container = new Data.Common.CommonModelContainer())
                    {
                        entity.EmailStatus = FlightPak.Common.Constants.Email.CalendarInvites;
                        container.AddToFPEmailTranLogs(entity);
                        container.SaveChanges();
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);

            }

        }

        /// <summary>
        /// Send mail to single recipient
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string body, string subject, bool isBodyHTML = false)
        {
            if (toAddress != null)
            {
                MailMessage message = new MailMessage();

                message.To.Add(new MailAddress(toAddress, toDisplayName));

                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = isBodyHTML;

                SmtpClient client = new SmtpClient();
                client.Send(message);
            }
        }

        /// <summary>
        /// Send mail to single recipient
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string body, string subject, string FromEmail, bool isBodyHTML = false)
        {
            if (toAddress != null)
            {
                MailMessage message = new MailMessage();

                message.To.Add(new MailAddress(toAddress, toDisplayName));

                if (!string.IsNullOrEmpty(FromEmail))
                {
                    message.From = new MailAddress(FromEmail);
                }

                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = isBodyHTML;

                SmtpClient client = new SmtpClient();
                client.Send(message);
            }
        }

        /// <summary>
        /// Send mail to single recipient
        /// </summary>
        /// <Issue>
        /// 2806 E-mail Button - Crew Report data missing / PAX Incorrect Report (VS)
        /// </Issue>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string body, string subject, string FromEmail, List<String> attachments, bool isBodyHTML = false)
        {
            if (!string.IsNullOrEmpty(toAddress))
            {
                MailMessage message = new MailMessage();

                if (!string.IsNullOrEmpty(FromEmail))
                {
                    message = new MailMessage(FromEmail, toAddress);
                    //message.From = new MailAddress(FromEmail, FromEmail);
                }
                
                //message.To.Add(new MailAddress(toAddress, toDisplayName));

                message.Subject = subject;
                message.Body = body;

                List<Attachment> Attachments = CreateFileFromBase64String(attachments);

                foreach (Attachment attachment in Attachments)
                    message.Attachments.Add(attachment);

                message.IsBodyHtml = isBodyHTML;

                SmtpClient client = new SmtpClient();
                client.SendAsync(message,null);
            }
        }

        public static List<Attachment> CreateFileFromBase64String(List<String> attachments)
        {
            List<Attachment> retAttachments = null;
            try
            {
                if (attachments.Count != 0 && attachments != null)
                {
                    retAttachments = new List<Attachment>();

                    foreach (String attachment in attachments)
                    {
                        string[] sAttachment = attachment.Split(new string[] { "|||" }, StringSplitOptions.None);
                        byte[] rptStream = null;

                        int vCount = 0;
                        string attachFileName = string.Empty;
                        foreach (string strVal in sAttachment)
                        {
                            if (vCount==0 )
                                rptStream = System.Convert.FromBase64String(strVal);

                            if (vCount != 0)
                                attachFileName = strVal;
                            
                            vCount += 1;
                        }

                        MemoryStream memoryStream = new MemoryStream(rptStream);
                        memoryStream.Write(rptStream, 0, rptStream.Length);

                        // Set the position to the beginning of the stream.
                        memoryStream.Seek(0, SeekOrigin.Begin);
                        Guid guidFileName;
                        guidFileName = Guid.NewGuid();

                        // Create attachment
                        System.Net.Mime.ContentType contentType = new System.Net.Mime.ContentType();
                        contentType.MediaType = System.Net.Mime.MediaTypeNames.Application.Pdf;
                        contentType.Name = (attachFileName != string.Empty ? attachFileName : guidFileName.ToString()) + ".pdf";
                        retAttachments.Add(new System.Net.Mail.Attachment(memoryStream, contentType));

                    }
                }
                return retAttachments;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Send mail to single recipient with CC
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="ccAddress"></param>
        /// <param name="ccDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string ccAddress, string ccDisplayName, string body, string subject)
        {
            MailMessage message = new MailMessage();

            message.To.Add(new MailAddress(toAddress, toDisplayName));

            message.CC.Add(new MailAddress(ccAddress, ccDisplayName));

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to single recipient with CC & BCC
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="ccAddress"></param>
        /// <param name="ccDisplayName"></param>
        /// <param name="bccAddress"></param>
        /// <param name="bccDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string ccAddress, string ccDisplayName, string bccAddress, string bccDisplayName, string body, string subject)
        {
            MailMessage message = new MailMessage();

            message.To.Add(new MailAddress(toAddress, toDisplayName));

            message.CC.Add(new MailAddress(ccAddress, ccDisplayName));

            message.Bcc.Add(new MailAddress(bccAddress, bccDisplayName));

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to list of recipients
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(MailAddressCollection toAddress, string body, string subject)
        {
            MailMessage message = new MailMessage();

            foreach (var item in toAddress)
                message.To.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to list of recipients with CC
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(MailAddressCollection toAddress, MailAddressCollection ccAddress, string body, string subject)
        {
            MailMessage message = new MailMessage();

            foreach (var item in toAddress)
                message.To.Add(item);
            foreach (var item in ccAddress)
                message.CC.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to list of recipients with CC & BCC
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="bccAddress"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(MailAddressCollection toAddress, MailAddressCollection ccAddress, MailAddressCollection bccAddress, string body, string subject)
        {
            MailMessage message = new MailMessage();

            foreach (var item in toAddress)
                message.To.Add(item);
            foreach (var item in ccAddress)
                message.CC.Add(item);
            foreach (var item in bccAddress)
                message.Bcc.Add(item);
            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }

        /// <summary>
        /// Send mail to single recipient with attachment
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string body, string subject, Collection<Attachment> attachment)
        {
            MailMessage message = new MailMessage();

            message.To.Add(new MailAddress(toAddress, toDisplayName));

            foreach (var item in attachment)
                message.Attachments.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to single recipient with CC & attachment
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="ccAddress"></param>
        /// <param name="ccDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string ccAddress, string ccDisplayName, string body, string subject, Collection<Attachment> attachment)
        {
            MailMessage message = new MailMessage();

            message.To.Add(new MailAddress(toAddress, toDisplayName));

            message.CC.Add(new MailAddress(ccAddress, ccDisplayName));

            foreach (var item in attachment)
                message.Attachments.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to single recipient with CC, BCC, attachment
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="toDisplayName"></param>
        /// <param name="ccAddress"></param>
        /// <param name="ccDisplayName"></param>
        /// <param name="bccAddress"></param>
        /// <param name="bccDisplayName"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(string toAddress, string toDisplayName, string ccAddress, string ccDisplayName, string bccAddress, string bccDisplayName, string body, string subject, Collection<Attachment> attachment)
        {
            MailMessage message = new MailMessage();

            message.To.Add(new MailAddress(toAddress, toDisplayName));

            message.CC.Add(new MailAddress(ccAddress, ccDisplayName));

            message.Bcc.Add(new MailAddress(bccAddress, bccDisplayName));

            foreach (var item in attachment)
                message.Attachments.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to list of recipients with attachments
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(MailAddressCollection toAddress, string body, string subject, Collection<Attachment> attachment)
        {
            MailMessage message = new MailMessage();

            foreach (var item in toAddress)
                message.To.Add(item);

            foreach (var item in attachment)
                message.Attachments.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to list of recipients with CC & attachments
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(MailAddressCollection toAddress, MailAddressCollection ccAddress, string body, string subject, Collection<Attachment> attachment)
        {
            MailMessage message = new MailMessage();

            foreach (var item in toAddress)
                message.To.Add(item);
            foreach (var item in ccAddress)
                message.CC.Add(item);
            foreach (var item in attachment)
                message.Attachments.Add(item);

            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }
        /// <summary>
        /// Send mail to list of recipients with CC, BCC, attachments
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="ccAddress"></param>
        /// <param name="bccAddress"></param>
        /// <param name="body"></param>
        /// <param name="subject"></param>
        public static void SendMail(MailAddressCollection toAddress, MailAddressCollection ccAddress, MailAddressCollection bccAddress, string body, string subject, Collection<Attachment> attachment)
        {
            MailMessage message = new MailMessage();

            foreach (var item in toAddress)
                message.To.Add(item);
            foreach (var item in ccAddress)
                message.CC.Add(item);
            foreach (var item in bccAddress)
                message.Bcc.Add(item);
            foreach (var item in attachment)
                message.Attachments.Add(item);
            message.Subject = subject;
            message.Body = body;

            SmtpClient client = new SmtpClient();
            client.Send(message);
        }


        /// <summary>
        /// This method is used to send invite to single attendee
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="subject"></param>
        /// <param name="summary"></param>
        /// <param name="location"></param>
        /// <param name="organizerName"></param>
        /// <param name="organizerEmail"></param>
        /// <param name="attendeeName"></param>
        /// <param name="attendeeEmail"></param>
        /// <returns></returns>
        public static void SendMeetingRequest(DateTime start, DateTime end, string subject, string summary,
           string location, string organizerName, string organizerEmail, string attendeeName, string attendeeEmail)
        {
            MailAddressCollection col = new MailAddressCollection();
            col.Add(new MailAddress(attendeeEmail, attendeeName));
            SendMeetingRequest(start, end, subject, summary, location, organizerName, organizerEmail, col);
        }

        /// <summary>
        /// This method is used to send invite to list of attendee
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="subject"></param>
        /// <param name="summary"></param>
        /// <param name="location"></param>
        /// <param name="organizerName"></param>
        /// <param name="organizerEmail"></param>
        /// <param name="attendeeList"></param>
        /// <returns></returns>
        public static void SendMeetingRequest(DateTime start, DateTime end, string subject, string summary,
            string location, string organizerName, string organizerEmail, MailAddressCollection attendeeList, bool isBodyHtml = false)
        {
            MailMessage msg = new MailMessage();

            //  Set up the different mime types contained in the message
            System.Net.Mime.ContentType textType = new System.Net.Mime.ContentType("text/plain");
            System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
            System.Net.Mime.ContentType calendarType = new System.Net.Mime.ContentType("text/calendar");

            //  Add parameters to the calendar header
            calendarType.Parameters.Add("method", "REQUEST");
            calendarType.Parameters.Add("name", "meeting.ics");

            //  Create message body parts
            //  create the Body in text format
            string bodyText = "Type:Single Meeting\r\nOrganizer: {0}\r\nStart Time:{1}\r\nEnd Time:{2}\r\nTime Zone:{3}\r\nLocation: {4}\r\n\r\n*~*~*~*~*~*~*~*~*~*\r\n\r\n{5}";
            bodyText = string.Format(bodyText,
                organizerName,
                start.ToLongDateString() + " " + start.ToLongTimeString(),
                end.ToLongDateString() + " " + end.ToLongTimeString(),
                System.TimeZone.CurrentTimeZone.StandardName,
                location,
                summary);

            AlternateView textView = AlternateView.CreateAlternateViewFromString(bodyText, textType);
            msg.AlternateViews.Add(textView);

            //create the Body in HTML format
            string bodyHTML = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\r\n<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">\r\n<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 6.5.7652.24\">\r\n<TITLE>{0}</TITLE>\r\n</HEAD>\r\n<BODY>\r\n<!-- Converted from text/plain format -->\r\n<P><FONT SIZE=2>Type:Single Meeting<BR>\r\nOrganizer:{1}<BR>\r\nStart Time:{2}<BR>\r\nEnd Time:{3}<BR>\r\nTime Zone:{4}<BR>\r\nLocation:{5}<BR>\r\n<BR>\r\n*~*~*~*~*~*~*~*~*~*<BR>\r\n<BR>\r\n{6}<BR>\r\n</FONT>\r\n</P>\r\n\r\n</BODY>\r\n</HTML>";
            bodyHTML = string.Format(bodyHTML,
                summary,
                organizerName,
                start.ToLongDateString() + " " + start.ToLongTimeString(),
                end.ToLongDateString() + " " + end.ToLongTimeString(),
                System.TimeZone.CurrentTimeZone.StandardName,
                location,
                summary);

            AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(bodyHTML, HTMLType);
            msg.AlternateViews.Add(HTMLView);

            //create the Body in VCALENDAR format
            string calDateFormat = "yyyyMMddTHHmmssZ";
            string bodyCalendar = "BEGIN:VCALENDAR\r\nMETHOD:REQUEST\r\nPRODID:Microsoft CDO for Microsoft Exchange\r\nVERSION:2.0\r\nBEGIN:VTIMEZONE\r\nTZID:(GMT-06.00) Central Time (US & Canada)\r\nX-MICROSOFT-CDO-TZID:11\r\nBEGIN:STANDARD\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0500\r\nTZOFFSETTO:-0600\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=11;BYDAY=1SU\r\nEND:STANDARD\r\nBEGIN:DAYLIGHT\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0600\r\nTZOFFSETTO:-0500\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=3;BYDAY=2SU\r\nEND:DAYLIGHT\r\nEND:VTIMEZONE\r\nBEGIN:VEVENT\r\nDTSTAMP:{8}\r\nDTSTART:{0}\r\nSUMMARY:{7}\r\nUID:{5}\r\nATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=\"{9}\":MAILTO:{9}\r\nACTION;RSVP=TRUE;CN=\"{4}\":MAILTO:{4}\r\nORGANIZER;CN=\"{3}\":mailto:{4}\r\nLOCATION:{2}\r\nDTEND:{1}\r\nDESCRIPTION:{7}\\N\r\nSEQUENCE:1\r\nPRIORITY:5\r\nCLASS:\r\nCREATED:{8}\r\nLAST-MODIFIED:{8}\r\nSTATUS:CONFIRMED\r\nTRANSP:OPAQUE\r\nX-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\nX-MICROSOFT-CDO-INSTTYPE:0\r\nX-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\nX-MICROSOFT-CDO-ALLDAYEVENT:FALSE\r\nX-MICROSOFT-CDO-IMPORTANCE:1\r\nX-MICROSOFT-CDO-OWNERAPPTID:-1\r\nX-MICROSOFT-CDO-ATTENDEE-CRITICAL-CHANGE:{8}\r\nX-MICROSOFT-CDO-OWNER-CRITICAL-CHANGE:{8}\r\nBEGIN:VALARM\r\nACTION:DISPLAY\r\nDESCRIPTION:REMINDER\r\nTRIGGER;RELATED=START:-PT00H15M00S\r\nEND:VALARM\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n";
            bodyCalendar = string.Format(bodyCalendar,
                start.ToUniversalTime().ToString(calDateFormat),
                end.ToUniversalTime().ToString(calDateFormat),
                location,
                organizerName,
                organizerEmail,
                Guid.NewGuid().ToString("B"),
                summary,
                subject,
                DateTime.Now.ToUniversalTime().ToString(calDateFormat),
                attendeeList.ToString());

            AlternateView calendarView = AlternateView.CreateAlternateViewFromString(bodyCalendar, calendarType);
            calendarView.TransferEncoding = TransferEncoding.SevenBit;
            msg.AlternateViews.Add(calendarView);

            //  Adress the message
            msg.From = new MailAddress(organizerEmail);
            foreach (MailAddress attendee in attendeeList)
            {
                msg.To.Add(attendee);
            }
            msg.Subject = subject;
            msg.IsBodyHtml = isBodyHtml;

            SmtpClient client = new SmtpClient();
            client.Send(msg);
        }
    }
}
