﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Business.Common
{
    [Serializable] 
    public class FuelFileData
    {
        public const String CSVFileFolder = "FuelUploadedCSVFiles";
        public long VendorID;
        public bool IsCustomMapped;
        public int NumberOfColumns;        
        public string FilePath;
        public string FileName;
        public string BaseFileName;
        public string FullPhysicalFilePath;
        public string UploadFileData;
        public bool isAllFieldSet;
        public int FBO;
        public int ICAO;
        public int IATA;
        public int Vendor;
        public int Text;
        public int High;
        public int Low;
        public int Price;
        public int EffectiveDate;
        public string csvXMLParser;
        public List<Dictionary<string, string>> parsedCSV;
        public bool IsMaintainHistoricalFuelData;
        public bool IsErrorLogFile;
        public FuelFileData()
        {
            this.FBO = -1;
            this.ICAO = -1;
            this.IATA = -1;
            this.Vendor = -1;
            this.Text = -1;
            this.High = -1;
            this.Low = -1;
            this.Price = -1;
            this.EffectiveDate = -1;
            this.IsCustomMapped = false;
        }

        public bool isAnyFieldHaveSameValue(int value)
        {
            if (value == FBO)
                return true;
            else if (value == ICAO)
                return true;
            else if (value == IATA)
                return true;
            else if (value == Vendor)
                return true;
            else if (value == Text)
                return true;
            else if (value == High)
                return true;
            else if (value == Low)
                return true;
            else if (value == Price)
                return true;
            else if (value == EffectiveDate)
                return true;
            else
                return false;
        }

        public bool haveRequiredColumnsFound()
        {
            if ((FBO > -1 || Vendor > -1) && (ICAO > -1 || IATA > -1) && Price > -1 && EffectiveDate > -1 && High > -1 && Low > -1)
            {
                return true;
            }
            return false;
        }

    }
}
