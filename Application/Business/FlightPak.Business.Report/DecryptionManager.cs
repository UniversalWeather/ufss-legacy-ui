﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using FlightPak.Framework.Encryption;
using System.IO;
using System.Collections;
using System.Xml;
using System.Security;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using FlightPak.Common.Constants;

namespace FlightPak.Business.Report
{
    public class DecryptionManager
    {
        private ExceptionManager exManager;
        public ReportResponse GetDecryptedData(ReportRequest request)
        {
            DataTable _dataTable = null;
            ReportResponse _response = new ReportResponse();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(request))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {

                    #region Retrive Parameter Info for SP
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportDataContainer"].ConnectionString);

                    ProcedureConstants IListProcs = new ProcedureConstants();
                    // Veracode fix: Delink the proc name from request
                    string procName = IListProcs.IProcedureNames.Where(x => x == request.procName).FirstOrDefault();
                    SqlCommand command = new SqlCommand(procName, conn);

                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlCommandBuilder.DeriveParameters(command);
                    conn.Close();
                    #endregion
                    #region Assign Parameter for SP

                    foreach (var param in request.paramInfo.Split('$'))
                    {
                        string[] paramArray = param.Split(';');
                        if (paramArray.Count() >= 1 && !string.IsNullOrEmpty(paramArray[0]))
                        {
                            //paramPair.Add(paramArray[0], paramArray[1]);
                            if (command.Parameters[paramArray[0]].DbType == DbType.DateTime && string.IsNullOrEmpty(paramArray[1]))
                            {
                                command.Parameters[paramArray[0]].Value = DBNull.Value;
                            }
                            else
                            {
                                command.Parameters[paramArray[0]].Value = paramArray[1];
                            }
                        }

                    }
                    #endregion
                    #region Execute SP
                    _dataTable = new DataTable();
                    _dataTable.Load(command.ExecuteReader());
                    conn.Close();
                    foreach (System.Data.DataColumn col in _dataTable.Columns) col.ReadOnly = false;
                    //SqlDataAdapter adapter = new SqlDataAdapter(command);
                    //adapter.Fill(_dataTable);
                    #endregion
                    #region Decrypt Data
                    string[] encryptedColumn = request.encryptedColumnNames.Split(';');
                    Crypto crypto = new Crypto();
                    if (encryptedColumn.Count() > 0)
                    {
                        foreach (DataRow row in _dataTable.Rows)
                        {
                            foreach (string item in encryptedColumn)
                            {
                                if (!string.IsNullOrEmpty(item))
                                    row[item] = crypto.Decrypt(row[item].ToString());
                            }
                        }
                    }

                    _dataTable.TableName = "Table1";
                    #endregion
                    #region Convert datatable to XElement
                    _response.datasource = WriteXml(_dataTable);
                    #endregion

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return _response;
            }

        }

        public ReportResponse GetPartialDecryptedData(ReportPartialRequest request)
        {
            DataTable _dataTable = null;
            ReportResponse _response = new ReportResponse();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(request))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    #region Retrive Parameter Info for SP
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ReportDataContainer"].ConnectionString);

                    ProcedureConstants IListProcs = new ProcedureConstants();
                    // Veracode fix: Delink the proc name from request
                    string procName = IListProcs.IProcedureNames.Where(x => x == request.procName).FirstOrDefault();
                    
                    SqlCommand command = new SqlCommand(procName, conn);

                    command.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlCommandBuilder.DeriveParameters(command);

                    #endregion
                    #region Assign Parameter for SP

                    foreach (var param in request.paramInfo.Split('$'))
                    {
                        string[] paramArray = param.Split(';');
                        if (paramArray.Count() >= 1 && !string.IsNullOrEmpty(paramArray[0]))
                        {
                            //paramPair.Add(paramArray[0], paramArray[1]);
                            if (command.Parameters[paramArray[0]].DbType == DbType.DateTime && string.IsNullOrEmpty(paramArray[1]))
                            {
                                command.Parameters[paramArray[0]].Value = DBNull.Value;
                            }
                            else
                            {
                                command.Parameters[paramArray[0]].Value = paramArray[1];
                            }
                        }

                    }
                    #endregion
                    #region Execute SP
                    _dataTable = new DataTable();
                    _dataTable.Load(command.ExecuteReader());
                    conn.Close();
                    foreach (System.Data.DataColumn col in _dataTable.Columns) col.ReadOnly = false;
                    //SqlDataAdapter adapter = new SqlDataAdapter(command);
                    //adapter.Fill(_dataTable);
                    #endregion
                    #region Decrypt Data
                    string[] encryptedColumn = request.encryptedColumnNames.Split(';');
                    Crypto crypto = new Crypto();
                    if (encryptedColumn.Count() > 0)
                    {
                        foreach (DataRow row in _dataTable.Rows)
                        {
                            foreach (string item in encryptedColumn)
                            {
                                if (!string.IsNullOrEmpty(item))
                                    row[item] = crypto.Decrypt(row[item].ToString());
                            }
                        }
                    }

                    #region Decrypt Delimeted Values
                    string[] encryptedDelimetedColumn = request.partialEncryptedColumnNames.Split(';');
                    if (encryptedDelimetedColumn.Count() > 0)
                    {
                        foreach (DataRow row in _dataTable.Rows)
                        {
                            foreach (string item in encryptedDelimetedColumn)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    while (row[item].ToString().IndexOf("@#$") != -1)
                                    {
                                        int firstIndex = row[item].ToString().IndexOf("@#$");
                                        int lastIndex = row[item].ToString().IndexOf("=@#$");
                                        string modifiedstring = row[item].ToString().Substring(firstIndex + 3, lastIndex - firstIndex - 2);
                                        try
                                        {
                                            row[item] = row[item].ToString().Replace("@#$" + modifiedstring + "@#$", crypto.Decrypt(modifiedstring));
                                        }
                                        catch (Exception)
                                        {

                                            throw;
                                        }

                                    }
                                }
                            }
                        }
                    }

                    #endregion

                    _dataTable.TableName = "Table1";
                    #endregion
                    #region Convert datatable to XElement
                    _response.datasource = WriteXml(_dataTable);
                    #endregion
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return _response;
            }


        }

        private XmlElement WriteXml(DataTable _dataTable)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(_dataTable))
            {
                //ds.WriteXml(sw, XmlWriteMode.IgnoreSchema);
                //DataTable dt = ds.Tables[0];
                MemoryStream _memoryStream = new MemoryStream();

                StringBuilder _stringBuilder = new StringBuilder();

                _stringBuilder.Append(@"<NewDataSet>");
                if (_dataTable.Rows.Count == 0)
                {
                    _stringBuilder.Append(@"<Table>");
                    foreach (DataColumn col in _dataTable.Columns)
                    {
                        _stringBuilder.Append(@"<" + XmlConvert.EncodeName(col.ColumnName) + @">");
                        _stringBuilder.Append("");
                        _stringBuilder.Append(@"</" + XmlConvert.EncodeName(col.ColumnName) + @">");
                    }
                    _stringBuilder.Append(@"<" + XmlConvert.EncodeName("DummyColumn") + @">");
                    _stringBuilder.Append("Fail");
                    _stringBuilder.Append(@"</" + XmlConvert.EncodeName("DummyColumn") + @">");
                    _stringBuilder.AppendLine(@"</Table>");
                }
                foreach (DataRow row in _dataTable.Rows)
                {
                    _stringBuilder.Append(@"<Table>");
                    foreach (DataColumn col in _dataTable.Columns)
                    {
                        _stringBuilder.Append(@"<" + XmlConvert.EncodeName(col.ColumnName) + @">");
                        if (col.DataType == typeof(string))
                            row[col] = SecurityElement.Escape(row[col].ToString());
                        _stringBuilder.Append(row[col].ToString());
                        _stringBuilder.Append(@"</" + XmlConvert.EncodeName(col.ColumnName) + @">");
                    }
                    _stringBuilder.Append(@"<" + XmlConvert.EncodeName("DummyColumn") + @">");
                    _stringBuilder.Append("Success");
                    _stringBuilder.Append(@"</" + XmlConvert.EncodeName("DummyColumn") + @">");
                    _stringBuilder.AppendLine(@"</Table>");
                }
                _stringBuilder.Append(@"</NewDataSet>");
                System.Text.ASCIIEncoding encoding = new ASCIIEncoding();
                _memoryStream.Write(encoding.GetBytes(_stringBuilder.ToString()), 0, _stringBuilder.Length);
                _memoryStream.Position = 0;
                
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.DtdProcessing = DtdProcessing.Prohibit;
                XmlReader reader = XmlReader.Create(_memoryStream, settings);
                XmlDocument _document = new XmlDocument();
                _document.XmlResolver = null;
                _document.Load(reader);
                
                return _document.DocumentElement;
            }
        }
    }
    #region Request & Response

    [MessageContract(IsWrapped = true)]
    public class ReportResponse
    {
        [MessageBodyMember]
        public XmlElement datasource;
    }
    [MessageContract(IsWrapped = true)]
    public class ReportRequest
    {
        [MessageBodyMember]
        public String procName;
        [MessageBodyMember]
        public String paramInfo;
        [MessageBodyMember]
        public String encryptedColumnNames;
    }
    [MessageContract(IsWrapped = true)]
    public class ReportPartialRequest
    {
        [MessageBodyMember]
        public String procName;
        [MessageBodyMember]
        public String paramInfo;
        [MessageBodyMember]
        public String encryptedColumnNames;
        [MessageBodyMember]
        public String partialEncryptedColumnNames;
    }
    #endregion

}
