﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Business.Common;
using FlightPak.Data.Calculations;

namespace FlightPak.Business.Calculations
{
    public  class PreflightCalcManager: BaseManager
    {
        public ReturnValue<Data.Calculations.PreflightHomeBaseSettings> GetHomeBaseSetting(Int64 HomeBaseID)
        {
            ReturnValue<Data.Calculations.PreflightHomeBaseSettings> Results = new ReturnValue<Data.Calculations.PreflightHomeBaseSettings>();
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();

            Container.ContextOptions.ProxyCreationEnabled = false;
            Results.EntityList = Container.GetPreflightHomeBaseSettings(CustomerID, HomeBaseID).ToList();
            if (Results.EntityList != null && Results.EntityList.Count > 0)
                Results.ReturnFlag = true;
            else
                Results.ReturnFlag = false;
            return Results;
        }
    }
}
