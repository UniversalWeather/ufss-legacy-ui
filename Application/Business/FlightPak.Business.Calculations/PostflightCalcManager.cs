﻿using System;
using System.Linq;
using FlightPak.Business.Common;
using System.Collections.Generic;

namespace FlightPak.Business.Calculations
{
    public class PostflightCalcManager : BaseManager
    {
        /// <summary>
        /// Method to Get Fleet Charge Rate Informations
        /// </summary>
        /// <param name="FleetID">Pass Fleet ID</param>
        /// <returns>Returns Fleet Charge Rate List</returns>
        public ReturnValue<Data.Calculations.GetPOFleetChargeRate> GetFleetChargeRateList(Int64 FleetID, DateTime CurrentDate)
        {
            ReturnValue<Data.Calculations.GetPOFleetChargeRate> Results = new ReturnValue<Data.Calculations.GetPOFleetChargeRate>();
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();
            Container.ContextOptions.ProxyCreationEnabled = false;
            Results.EntityList = Container.GetPOFleetChargeRate(CustomerID, FleetID, CurrentDate).ToList();
            Results.ReturnFlag = true;
            return Results; 
        }

        /// <summary>
        /// Method to Get Tenth Conversion Informations
        /// </summary>
        /// <param name="HomeBaseID">Pass Homebase ID</param>
        /// <returns>Returns Tenth Conversion List</returns>
        private List<Data.Calculations.GetPOTenthsConversion> GetPOTenthsConversionList(Int64 HomeBaseID)
        {
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();
            //Container.ContextOptions.LazyLoadingEnabled = false;
            Container.ContextOptions.ProxyCreationEnabled = false;
            //Results.EntityList = Container.GetPostflightMain().ToList();
            return Container.GetPOTenthsConversion(CustomerID, HomeBaseID).ToList();
        }

        /// <summary>
        /// Method to Get Homebase Informations
        /// </summary>
        /// <param name="HomeBaseID">Pass Homebase ID</param>
        /// <returns>Returns Homebase Settings List</returns>
        public ReturnValue<Data.Calculations.GetPOHomeBaseSetting> GetHomeBaseSetting(Int64 HomeBaseID)
        {
            ReturnValue<Data.Calculations.GetPOHomeBaseSetting> Results = new ReturnValue<Data.Calculations.GetPOHomeBaseSetting>();
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();

            Container.ContextOptions.ProxyCreationEnabled = false;
            Results.EntityList = Container.GetPOHomeBaseSetting(CustomerID, HomeBaseID).ToList();
            if (Results.EntityList != null && Results.EntityList.Count > 0)
            {
                Results.EntityList[0].TenthConversions = new System.Collections.Generic.List<Data.Calculations.GetPOTenthsConversion>();
                Results.EntityList[0].TenthConversions = GetPOTenthsConversionList(HomeBaseID);

                Results.EntityList[0].DutyConversions = new System.Collections.Generic.List<Data.Calculations.GetDutySettings>();
                Results.EntityList[0].DutyConversions = GetDutySettings(HomeBaseID);

                Results.ReturnFlag = true;
            }
            else
            {
                Results.ReturnFlag = false;
            }
            return Results;
        }

        /// <summary>
        /// Method to Get Crew Duty Informations
        /// </summary>
        /// <param name="HomeBaseID">Pass Homebase ID</param>
        /// <returns>Returns Crew Duty Settings List</returns>
        private List<Data.Calculations.GetDutySettings> GetDutySettings(Int64 HomeBaseID)
        {
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();
            Container.ContextOptions.ProxyCreationEnabled = false;
            return Container.GetDutySettings(CustomerID, HomeBaseID).ToList();
        }

        /// <summary>
        /// Method to Get Fleet Profile list
        /// </summary>
        /// <param name="FleetID">Pass Fleet ID</param>
        /// <returns>Returns Fleet Profile List</returns>
        public ReturnValue<Data.Calculations.GetPOFleetProfile> GetFleetProfileList(Int64 FleetID)
        {
            ReturnValue<Data.Calculations.GetPOFleetProfile> Results = new ReturnValue<Data.Calculations.GetPOFleetProfile>();
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();
            Container.ContextOptions.ProxyCreationEnabled = false;
            Results.EntityList = Container.GetPOFleetProfile(CustomerID, FleetID).ToList();
            Results.ReturnFlag = true;
            return Results;
        }

        /// <summary>
        /// Method to Get SIFL Rate Informations
        /// </summary>
        /// <param name="currentDate">Pass Current Date</param>
        /// <returns>Returns SIFL Rate List</returns>
        public ReturnValue<Data.Calculations.GetPOSIFLRate> GetSIFLRateList(DateTime CurrentDate)
        {
            ReturnValue<Data.Calculations.GetPOSIFLRate> Results = new ReturnValue<Data.Calculations.GetPOSIFLRate>();
            Data.Calculations.CalculationsDataModelContainer Container = new Data.Calculations.CalculationsDataModelContainer();
            Container.ContextOptions.ProxyCreationEnabled = false;
            Results.EntityList = Container.GetPOSIFLRate(CustomerID, CurrentDate).ToList();
            Results.ReturnFlag = true;
            return Results;
        }

    }
}
