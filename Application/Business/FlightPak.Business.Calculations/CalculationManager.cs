﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Business.MasterCatalog;
using FlightPak.Data.MasterCatalog;
using System.Runtime.InteropServices;


namespace FlightPak.Business.Calculations
{
    public class CalculationManager
    {
        #region Miles Calculation
        /// <summary>
        /// Get the input values from UI For DepartureICAO & Arrival ICAO & calculate Miles
        /// </summary>
        /// <param name="DepartAirportID"></param>
        /// <param name="ArriveAirportID"></param>
        /// <returns></returns>
        public static double GetDistance(Int64 DepartAirportID, Int64 ArriveAirportID)
        {
            double lnMiles = 0.0;

            if (DepartAirportID != null && ArriveAirportID != null)
            {


                AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = new List<GetAllAirport>();
                List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = new List<GetAllAirport>();
                var objRetVal = AirportCatalog.GetAirportByAirportID(DepartAirportID);
                if (objRetVal.ReturnFlag)
                {
                    DepAirport = objRetVal.EntityList;
                }
                objRetVal = null;
                objRetVal = AirportCatalog.GetAirportByAirportID(ArriveAirportID);
                if (objRetVal.ReturnFlag)
                {
                    ArrAirport = objRetVal.EntityList;
                }


                //List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = (from Arpt in objRetVal.EntityList
                //                                                               where Arpt.AirportID == DepartAirportID
                //                                                               select Arpt).ToList();
                //List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = (from Arpt in objRetVal.EntityList
                //                                                               where Arpt.AirportID == ArriveAirportID
                //                                                               select Arpt).ToList();
                double deplatdeg = 0;
                double deplatmin = 0;
                string lcdeplatdir = "N";

                double deplngdeg = 0;
                double deplngmin = 0;
                string lcdeplngdir = "E";



                double arrlatdeg = 0;
                double arrlatmin = 0;
                string lcArrLatDir = "N";

                double arrlngdeg = 0;
                double arrlngmin = 0;
                string arrlngdir = "E";


                if (DepAirport != null && DepAirport.Count > 0)
                {
                    if (DepAirport[0].LatitudeDegree != null)
                        deplatdeg = (double)DepAirport[0].LatitudeDegree;
                    if (DepAirport[0].LatitudeMinutes != null)
                        deplatmin = (double)DepAirport[0].LatitudeMinutes;
                    if (DepAirport[0].LatitudeNorthSouth != null)
                        lcdeplatdir = DepAirport[0].LatitudeNorthSouth;

                    if (DepAirport[0].LongitudeDegrees != null)
                        deplngdeg = (double)DepAirport[0].LongitudeDegrees;
                    if (DepAirport[0].LongitudeMinutes != null)
                        deplngmin = (double)DepAirport[0].LongitudeMinutes;
                    if (DepAirport[0].LongitudeEastWest != null)
                        lcdeplngdir = DepAirport[0].LongitudeEastWest;
                }

                if (ArrAirport != null && ArrAirport.Count > 0)
                {
                    if (ArrAirport[0].LatitudeDegree != null)
                        arrlatdeg = (double)ArrAirport[0].LatitudeDegree;
                    if (ArrAirport[0].LatitudeMinutes != null)
                        arrlatmin = (double)ArrAirport[0].LatitudeMinutes;
                    if (ArrAirport[0].LatitudeNorthSouth != null)
                        lcArrLatDir = ArrAirport[0].LatitudeNorthSouth;
                    if (ArrAirport[0].LongitudeDegrees != null)
                        arrlngdeg = (double)ArrAirport[0].LongitudeDegrees;
                    if (ArrAirport[0].LongitudeMinutes != null)
                        arrlngmin = (double)ArrAirport[0].LongitudeMinutes;
                    if (ArrAirport[0].LongitudeEastWest != null)
                        arrlngdir = ArrAirport[0].LongitudeEastWest;
                }


                double lnDepLat = ((deplatdeg + (deplatmin / 60)) * ((lcdeplatdir.ToUpper() == "N") ? 1 : -1));
                double lnDepLng = ((deplngdeg + (deplngmin / 60)) * ((lcdeplngdir.ToUpper() == "W") ? 1 : -1));
                double lnArrLat = ((arrlatdeg + (arrlatmin / 60)) * ((lcArrLatDir.ToUpper() == "N") ? 1 : -1));
                double lnArrLng = ((arrlngdeg + (arrlngmin / 60)) * ((arrlngdir.ToUpper() == "W") ? 1 : -1));

                double dLat1InRad = lnDepLat * (Math.PI / 180.0);
                double dLong1InRad = lnDepLng * (Math.PI / 180.0);
                double dLat2InRad = lnArrLat * (Math.PI / 180.0);
                double dLong2InRad = lnArrLng * (Math.PI / 180.0);

                double dLongitude = dLong2InRad - dLong1InRad;
                double dLatitude = dLat2InRad - dLat1InRad;

                double lnLongDiff = lnArrLng - lnDepLng;

                double RadlnDepLat = (Math.PI / 180.0) * lnDepLat;
                double RadlnArrLat = (Math.PI / 180.0) * lnArrLat;
                double RadlnlnLongDiff = (Math.PI / 180.0) * lnLongDiff;


                lnMiles = 60 * ((180.0 / Math.PI) * (
                                               Math.Acos(
                                                               Math.Sin(
                                                               RadlnDepLat
                                                               )
                                                               *
                                                               Math.Sin(
                                                               RadlnArrLat
                                                               )
                                                               +
                                                               Math.Cos(
                                                               RadlnDepLat
                                                               )
                                                               *
                                                               Math.Cos(
                                                               RadlnArrLat
                                                               )
                                                               *
                                                               Math.Cos(
                                                               RadlnlnLongDiff
                                                               )
                                                         )
                                                    )
                            );
            }

            if (double.IsNaN(lnMiles))
                lnMiles = 0;
            // int intlnMiles =  lnMiles.ToString().IndexOf('.');
            //lnMiles = Convert.ToDouble(lnMiles.ToString().Substring(0,intlnMiles));
            return Math.Floor(lnMiles);
        }

        #endregion

        #region Bias Calculation
        /// <summary>
        /// Get the input values for DepartureICAO, ArrivalICAO,AircarftID & PowerSetting & calculate BIAS
        /// </summary>
        /// <param name="DepartAirportID"></param>
        /// <param name="ArriveAirportID"></param>
        /// <param name="AircraftID"></param>
        /// <param name="PowerSetting"></param>
        /// <returns></returns>
        public static List<double> CalcBiasTas(Int64 DepartAirportID, Int64 ArriveAirportID, Int64 AircraftID, string PowerSetting)
        {

            AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();


            List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = new List<GetAllAirport>();
            List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = new List<GetAllAirport>();
            var objRetVal = AirportCatalog.GetAirportByAirportID(DepartAirportID);
            if (objRetVal.ReturnFlag)
            {
                DepAirport = objRetVal.EntityList;
            }
            objRetVal = null;
            objRetVal = AirportCatalog.GetAirportByAirportID(ArriveAirportID);
            if (objRetVal.ReturnFlag)
            {
                ArrAirport = objRetVal.EntityList;
            }




            AircraftManager AircraftCatalog = new FlightPak.Business.MasterCatalog.AircraftManager();
            var objRetValue = AircraftCatalog.GetAircraftByAircraftID(AircraftID);
            List<FlightPak.Data.MasterCatalog.Aircraft> aircraft = new List<Aircraft>();

            if (objRetValue.ReturnFlag)
            {
                aircraft = objRetValue.EntityList;
            }


            double lnToBias = 0.0f;
            double lnLndBias = 0.0f;
            double lnTas = 0.0f;


            //List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = (from Arpt in objRetVal.EntityList
            //                                                               where Arpt.AirportID == DepartAirportID
            //                                                               select Arpt).ToList();
            //List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = (from Arpt in objRetVal.EntityList
            //                                                               where Arpt.AirportID == ArriveAirportID
            //                                                               select Arpt).ToList();


            //List<FlightPak.Data.MasterCatalog.Aircraft> aircraft = (from airCrft in objRetValue.EntityList
            //                                                        where airCrft.AircraftID == AircraftID
            //                                                        select airCrft).ToList();




            if (aircraft != null && aircraft.Count > 0)
            {

                switch (PowerSetting)
                {
                    case "1":
                        {
                            if (aircraft[0].PowerSettings1TakeOffBias != null)
                                lnToBias = (double)aircraft[0].PowerSettings1TakeOffBias;
                            if (aircraft[0].PowerSettings1LandingBias != null)
                                lnLndBias = (double)aircraft[0].PowerSettings1LandingBias;
                            if (aircraft[0].PowerSettings1TrueAirSpeed != null)
                                lnTas = (double)aircraft[0].PowerSettings1TrueAirSpeed;
                            break;
                        }


                    case "2":
                        {
                            if (aircraft[0].PowerSettings2TakeOffBias != null)
                                lnToBias = (double)aircraft[0].PowerSettings2TakeOffBias;
                            if (aircraft[0].PowerSettings2LandingBias != null)
                                lnLndBias = (double)aircraft[0].PowerSettings2LandingBias;
                            if (aircraft[0].PowerSettings2TrueAirSpeed != null)
                                lnTas = (double)aircraft[0].PowerSettings2TrueAirSpeed;
                            break;
                        }

                    case "3":
                        {
                            if (aircraft[0].PowerSettings3TakeOffBias != null)
                                lnToBias = (double)aircraft[0].PowerSettings3TakeOffBias;
                            if (aircraft[0].PowerSettings3LandingBias != null)
                                lnLndBias = (double)aircraft[0].PowerSettings3LandingBias;
                            if (aircraft[0].PowerSettings3TrueAirSpeed != null)
                                lnTas = (double)aircraft[0].PowerSettings3TrueAirSpeed;
                            break;
                        }
                    default:
                        {
                            lnToBias = 0;
                            lnLndBias = 0;
                            lnTas = 0;
                            break;
                        }



                }
                if (DepAirport != null && DepAirport.Count > 0)
                    if (DepAirport[0].TakeoffBIAS != null)
                        lnToBias = lnToBias + (double)DepAirport[0].TakeoffBIAS;
                if (ArrAirport != null && ArrAirport.Count > 0)
                    if (ArrAirport[0].LandingBIAS != null)
                        lnLndBias = lnLndBias + (double)ArrAirport[0].LandingBIAS;

            }

            return new List<double>() { lnToBias, lnLndBias, lnTas };
        }

        #endregion

        #region private Functions
        /// <summary>
        /// Gets the dateTime as the input & returns the Quarter period
        /// </summary>
        /// <param name="Dateval"></param>
        /// <returns></returns>
        private static int GetQtr(DateTime Dateval)
        {
            int lnqtr = 0;

            if (Dateval.Month == 12 || Dateval.Month == 1 || Dateval.Month == 2) //Must retrieve from TripLeg table in DB
            {
                lnqtr = 1;
            }
            else if (Dateval.Month == 3 || Dateval.Month == 4 || Dateval.Month == 5)
            {
                lnqtr = 2;
            }
            else if (Dateval.Month == 6 || Dateval.Month == 7 || Dateval.Month == 8)
            {
                lnqtr = 3;
            }
            else if (Dateval.Month == 9 || Dateval.Month == 10 || Dateval.Month == 11)
            {
                lnqtr = 4;
            }
            else
                lnqtr = 0;

            return lnqtr;
        }

        #endregion

        #region Wind

        public static double GetWind(Int64 DepartAirportID, Int64 ArriveAirportID, int _windReliability, Int64 AircraftID, string Quarter)
        {
            double lnWinds = 0;

            AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
            AircraftManager AircraftCatalog = new FlightPak.Business.MasterCatalog.AircraftManager();
            WorldWindManager WorldWindCatalog = new FlightPak.Business.MasterCatalog.WorldWindManager();
            WindStatManager WindCatalog = new FlightPak.Business.MasterCatalog.WindStatManager();

            List<FlightPak.Data.MasterCatalog.Aircraft> aircraft = new List<Aircraft>();
            var objRetValue = AircraftCatalog.GetAircraftByAircraftID(AircraftID);
            if (objRetValue.ReturnFlag)
            {
                aircraft = objRetValue.EntityList;
            }




            WindStat windStat = new WindStat();

            List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = new List<GetAllAirport>();
            List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = new List<GetAllAirport>();
            var objRetVal = AirportCatalog.GetAirportByAirportID(DepartAirportID);
            if (objRetVal.ReturnFlag)
            {
                DepAirport = objRetVal.EntityList;
            }
            objRetVal = null;
            objRetVal = AirportCatalog.GetAirportByAirportID(ArriveAirportID);
            if (objRetValue.ReturnFlag)
            {
                ArrAirport = objRetVal.EntityList;
            }


            //List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = (from Arpt in objRetVal.EntityList
            //                                                               where Arpt.AirportID == DepartAirportID
            //                                                               select Arpt).ToList();
            //List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = (from Arpt in objRetVal.EntityList
            //                                                               where Arpt.AirportID == ArriveAirportID
            //                                                               select Arpt).ToList();

            //List<FlightPak.Data.MasterCatalog.Aircraft> aircraft = (from aircrat in objRetValue.EntityList
            //                                                        where aircrat.AircraftID == AircraftID
            //                                                        select aircrat).ToList();


            double _windAltitude = 0;

            if (aircraft != null && aircraft.Count > 0)
            {
                if (aircraft[0].WindAltitude != null)
                    _windAltitude = Convert.ToDouble(aircraft[0].WindAltitude);
            }

            double _lnAltd = 0;
            string lcAltd = string.Empty;

            string lcDepLatDir = string.Empty;
            string lcDepLngDir = string.Empty;
            double lnDepLat = 0.0f;
            double lnDepLng = 0.0f;
            string lcDepZone = string.Empty;
            string lcArrZone = string.Empty;
            string lcArrLatDir = string.Empty;
            string lcArrLngDir = string.Empty;
            double lnArrLat = 0.0f;
            double lnArrLng = 0.0f;
            double lnHemi = 0;
            double lnDeltaLng = 0;
            double lnDeltaLat = 0;
            double lnSlope = 0;
            double lnCompass = 0;
            int lnDirect = 0;
            double lnQuad = 0;
            int lnQTR = Convert.ToInt16(Quarter);
            int lnStd = 0;


            double lnRel_Factor = 0.0f;
            long lnDepartZone = 0;
            long lnArrivZone = 0;
            string lcDepartZone = string.Empty;
            string lcArrivZone = string.Empty;
            string lcQuatr = string.Empty;
            string lcAltitude = string.Empty;

            if (_windAltitude != 0)
            {
                _lnAltd = _windAltitude;
            }
            else
            {
                _lnAltd = 0;
            }
            if (_lnAltd < 5000)
            {
                lnWinds = 0;
            }
            else
            {
                if (_lnAltd >= 35000)
                {
                    lcAltd = "4";
                }
                else if (_lnAltd >= 25000)
                {
                    lcAltd = "3";
                }
                else
                {
                    lcAltd = "2";
                }

                if (_windReliability == 1)
                {
                    lnRel_Factor = 0;
                }
                else if (_windReliability == 2)
                {
                    lnRel_Factor = 0.67;
                }
                else if (_windReliability == 3)
                {
                    lnRel_Factor = 1.04;
                }
                else
                {
                    lnRel_Factor = 0;
                }


                if (DepAirport != null && DepAirport.Count > 0)
                {
                    if (DepAirport[0].LatitudeNorthSouth != null)
                        lcDepLatDir = DepAirport[0].LatitudeNorthSouth;
                    if (DepAirport[0].LongitudeEastWest != null)
                        lcDepLngDir = DepAirport[0].LongitudeEastWest;
                    if (DepAirport[0].LatitudeDegree != null && DepAirport[0].LatitudeMinutes != null && DepAirport[0].LatitudeNorthSouth != null)
                        lnDepLat = (((double)DepAirport[0].LatitudeDegree) + ((double)(DepAirport[0].LatitudeMinutes) / 60)) * ((DepAirport[0].LatitudeNorthSouth.ToUpper() == "N") ? 1 : -1);
                    if (DepAirport[0].LongitudeDegrees != null && DepAirport[0].LongitudeMinutes != null && DepAirport[0].LongitudeEastWest != null)
                        lnDepLng = (((double)DepAirport[0].LongitudeDegrees) + (((double)DepAirport[0].LongitudeMinutes) / 60)) * ((DepAirport[0].LongitudeEastWest.ToUpper() == "W") ? 1 : -1);
                    if (DepAirport[0].WindZone != null)
                        lcDepZone = Convert.ToString(DepAirport[0].WindZone);
                }

                if (ArrAirport != null && ArrAirport.Count > 0)
                {
                    if (ArrAirport[0].LatitudeNorthSouth != null)
                        lcArrLatDir = ArrAirport[0].LatitudeNorthSouth;
                    if (ArrAirport[0].LongitudeEastWest != null)
                        lcArrLngDir = ArrAirport[0].LongitudeEastWest;
                    if (ArrAirport[0].LatitudeDegree != null && ArrAirport[0].LatitudeMinutes != null && ArrAirport[0].LatitudeNorthSouth != null)
                        lnArrLat = ((double)ArrAirport[0].LatitudeDegree + ((double)ArrAirport[0].LatitudeMinutes / 60)) * ((ArrAirport[0].LatitudeNorthSouth.ToUpper() == "N") ? 1 : -1);
                    if (ArrAirport[0].LongitudeDegrees != null && ArrAirport[0].LongitudeMinutes != null && ArrAirport[0].LongitudeEastWest != null)
                        lnArrLng = ((double)ArrAirport[0].LongitudeDegrees + ((double)ArrAirport[0].LongitudeMinutes / 60)) * ((ArrAirport[0].LongitudeEastWest.ToUpper() == "W") ? 1 : -1);
                    if (ArrAirport[0].WindZone != null)
                    {
                        lcArrZone = Convert.ToString(ArrAirport[0].WindZone);
                        lnArrivZone = (long)ArrAirport[0].WindZone;
                    }
                    if (DepAirport[0].WindZone != null)
                        lnDepartZone = (long)DepAirport[0].WindZone;
                }

                var objRetWorldWindInfo = WorldWindCatalog.GetWorldWindInfo(lnDepartZone, lnArrivZone, Quarter, lcAltd);
                FlightPak.Data.MasterCatalog.WorldWind worldWind = new Data.MasterCatalog.WorldWind();
                bool WorldwindFound = false;
                if (objRetWorldWindInfo.ReturnFlag)
                {
                    if (objRetWorldWindInfo.EntityList.Count > 0)
                    {
                        worldWind = objRetWorldWindInfo.EntityList[0];
                        if (worldWind != null)
                        {
                            WorldwindFound = true;
                            if (worldWind.Direction != null && worldWind.WorldWindStandard != null)
                                lnWinds = (int)worldWind.Direction - ((int)worldWind.WorldWindStandard * lnRel_Factor);
                        }
                    }
                }
                if (!WorldwindFound)
                {
                    objRetWorldWindInfo = WorldWindCatalog.GetWorldWindInfo(lnArrivZone, lnDepartZone, Quarter, lcAltd);
                    if (objRetWorldWindInfo.ReturnFlag)
                    {
                        if (objRetWorldWindInfo.EntityList.Count > 0)
                        {
                            worldWind = objRetWorldWindInfo.EntityList[0];
                            if (worldWind != null)
                            {
                                WorldwindFound = true;
                                if (worldWind.ReturnDirection != null && worldWind.WorldWindStandard != null)
                                    lnWinds = (int)worldWind.ReturnDirection - ((int)worldWind.WorldWindStandard * lnRel_Factor);
                            }
                        }
                    }
                }

                if (!WorldwindFound)
                {
                    if (lcDepLatDir.ToUpper() == "N" && lcArrLatDir.ToUpper() == "N")
                    {
                        lnHemi = 0;
                    }
                    else if (lcDepLatDir.ToUpper() == "S" && lcArrLatDir.ToUpper() == "S")
                    {
                        lnHemi = 1;
                    }
                    else
                    {
                        lnHemi = 2;
                    }
                    //}


                    //Get Quad and Direction of Travel

                    if ((lnDepLng < 0))
                    {
                        lnDepLng = 360 + lnDepLng;
                    }
                    if (lnArrLng < 0)
                    {

                        lnArrLng = 360 + lnArrLng;
                    }
                    lnDepLat = (90 + lnDepLat);
                    lnArrLat = (90 + lnArrLat);

                    lnDeltaLng = (lnDepLng - lnArrLng) * -1;
                    lnDeltaLat = (lnDepLat - lnArrLat) * -1;
                    if (lnDeltaLng > 180)
                    {
                        lnDeltaLng = (360 - lnDeltaLng) * -1;
                    }

                    else if (lnDeltaLng < -180)
                    {
                        lnDeltaLng = lnDeltaLng + 360;
                    }



                    if (lnDeltaLng < 0.1 && lnDeltaLng > -0.1)
                    {
                        lnSlope = 0;
                    }
                    else
                    {
                        lnSlope = lnDeltaLat / lnDeltaLng;
                    }
                    #region Wind Calculation -Get Quad and Direct Values
                    //Get Quad and Direct Values

                    lnCompass = GetHeading(lnSlope, lnDeltaLat, lnDeltaLng);
                    lnDirect = 1;
                    lnQuad = 1;
                    if (lnCompass >= 337.5 && lnCompass <= 360)
                    {
                        lnDirect = -1;
                        lnQuad = 2;
                    }
                    if (lnCompass >= 22.5 && lnCompass <= 67.4999999)
                    {

                        lnDirect = -1;
                        lnQuad = 3;
                    }

                    if (lnCompass >= 67.5 && lnCompass <= 112.4999999)
                    {

                        lnQuad = 1;

                    }

                    if (lnCompass >= 112.5 && lnCompass <= 202.4999999)
                    {

                        lnQuad = 4;
                    }

                    if (lnCompass >= 202.5 && lnCompass <= 247.4999999)
                    {

                        lnQuad = 3;
                    }

                    if (lnCompass >= 247.5 && lnCompass <= 292.4999999)
                    {

                        lnDirect = -1;
                        lnQuad = 1;

                    }

                    if (lnCompass >= 292.5 && lnCompass <= 337.4999999)
                    {
                        lnDirect = -1;
                        lnQuad = 4;
                    }
                    //Get Quad and Direct Values

                    #endregion
                    #region Final calculation of Winds & Standard Deviation
                    //Get Winds and Standard Deviation
                    string lcHemi = Convert.ToString(lnHemi);
                    string lcQuad = Convert.ToString(lnQuad);
                    string lcQTR = Convert.ToString(lnQTR);


                    var objRetWindStatInfo = WindCatalog.GetWindStatInfo(lcHemi, lcQuad, lcAltd, lcQTR);

                    if (objRetWindStatInfo.ReturnFlag)
                    {
                        if (objRetWindStatInfo.EntityList.Count > 0)
                        {
                            if (objRetWindStatInfo.EntityList[0].Wind != null)
                                lnWinds = (int)objRetWindStatInfo.EntityList[0].Wind;
                            if (objRetWindStatInfo.EntityList[0].WindStatStandard != null)
                                lnStd = (int)objRetWindStatInfo.EntityList[0].WindStatStandard;
                        }
                        else
                        {
                            lnWinds = 0;
                            lnStd = 0;
                        }
                    }
                    else
                    {
                        lnWinds = 0;
                        lnStd = 0;
                    }

                    lnWinds = (lnWinds * lnDirect) - (lnStd * lnRel_Factor);

                    #endregion
                }
            }

            return Math.Truncate(lnWinds);
        }

        public static double GetWindBasedOnParameter(double aircraftWindAltitude, int _windReliability, string Quarter,
            double deplatdeg, double deplatmin, string lcDepLatDir,
            double deplngdeg, double deplngmin, string lcDepLngDir, long lnDepartZone,
            double arrlatdeg, double arrlatmin, string lcArrLatDir,
            double arrlngdeg, double arrlngmin, string lcArrLngDir, long lnArrivZone
            )
        {
            double lnWinds = 0;


            WorldWindManager WorldWindCatalog = new FlightPak.Business.MasterCatalog.WorldWindManager();
            WindStatManager WindCatalog = new FlightPak.Business.MasterCatalog.WindStatManager();

            //List<FlightPak.Data.MasterCatalog.Aircraft> aircraft = new List<Aircraft>();
            WindStat windStat = new WindStat();

            //List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirport = new List<GetAllAirport>();
            //List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirport = new List<GetAllAirport>();


            double _windAltitude = 0;
            _windAltitude = aircraftWindAltitude;

            double _lnAltd = 0;
            string lcAltd = string.Empty;

            //string lcDepLatDir = string.Empty;
            //string lcDepLngDir = string.Empty;
            double lnDepLat = 0.0f;
            double lnDepLng = 0.0f;
            string lcDepZone = string.Empty;
            string lcArrZone = string.Empty;
            //string lcArrLatDir = string.Empty;
            //string lcArrLngDir = string.Empty;
            double lnArrLat = 0.0f;
            double lnArrLng = 0.0f;
            double lnHemi = 0;
            double lnDeltaLng = 0;
            double lnDeltaLat = 0;
            double lnSlope = 0;
            double lnCompass = 0;
            int lnDirect = 0;
            double lnQuad = 0;
            int lnQTR = Convert.ToInt16(Quarter);
            int lnStd = 0;


            double lnRel_Factor = 0.0f;
            //long lnDepartZone = 0;
            //long lnArrivZone = 0;
            string lcDepartZone = string.Empty;
            string lcArrivZone = string.Empty;
            string lcQuatr = string.Empty;
            string lcAltitude = string.Empty;

            if (_windAltitude != 0)
            {
                _lnAltd = _windAltitude;
            }
            else
            {
                _lnAltd = 0;
            }
            if (_lnAltd < 5000)
            {
                lnWinds = 0;
            }
            else
            {
                if (_lnAltd >= 35000)
                {
                    lcAltd = "4";
                }
                else if (_lnAltd >= 25000)
                {
                    lcAltd = "3";
                }
                else
                {
                    lcAltd = "2";
                }

                if (_windReliability == 1)
                {
                    lnRel_Factor = 0;
                }
                else if (_windReliability == 2)
                {
                    lnRel_Factor = 0.67;
                }
                else if (_windReliability == 3)
                {
                    lnRel_Factor = 1.04;
                }
                else
                {
                    lnRel_Factor = 0;
                }


                //if (DepAirport != null && DepAirport.Count > 0)
                //{
                //if (DepAirport[0].LatitudeNorthSouth != null)
                //    lcDepLatDir = DepAirport[0].LatitudeNorthSouth;
                //if (DepAirport[0].LongitudeEastWest != null)
                //    lcDepLngDir = DepAirport[0].LongitudeEastWest;
                //if (DepAirport[0].LatitudeDegree != null && DepAirport[0].LatitudeMinutes != null && DepAirport[0].LatitudeNorthSouth != null)
                lnDepLat = (deplatdeg + (deplatmin / 60)) * ((lcDepLatDir.ToUpper() == "N") ? 1 : -1);

                //if (DepAirport[0].LongitudeDegrees != null && DepAirport[0].LongitudeMinutes != null && DepAirport[0].LongitudeEastWest != null)
                lnDepLng = (deplngdeg + (deplngmin / 60)) * ((lcDepLngDir.ToUpper() == "W") ? 1 : -1);
                //if (DepAirport[0].WindZone != null)
                lcDepZone = Convert.ToString(lnDepartZone);
                //}

                //if (ArrAirport != null && ArrAirport.Count > 0)
                //{
                //if (ArrAirport[0].LatitudeNorthSouth != null)
                //    lcArrLatDir = ArrAirport[0].LatitudeNorthSouth;
                //if (ArrAirport[0].LongitudeEastWest != null)
                //    lcArrLngDir = ArrAirport[0].LongitudeEastWest;
                //if (ArrAirport[0].LatitudeDegree != null && ArrAirport[0].LatitudeMinutes != null && ArrAirport[0].LatitudeNorthSouth != null)
                lnArrLat = (arrlatdeg + (arrlatmin / 60)) * ((lcArrLatDir.ToUpper() == "N") ? 1 : -1);
                //if (ArrAirport[0].LongitudeDegrees != null && ArrAirport[0].LongitudeMinutes != null && ArrAirport[0].LongitudeEastWest != null)
                    lnArrLng = (arrlngdeg + (arrlngmin / 60)) * ((lcArrLngDir.ToUpper() == "W") ? 1 : -1);
                //if (ArrAirport[0].WindZone != null)
                //{
                //    lcArrZone = Convert.ToString(lnArrivZone);
                //    lnArrivZone = (long)lnArrivZone;
                //}
                //if (DepAirport[0].WindZone != null)
                //    lnDepartZone = (long)DepAirport[0].WindZone;
                //}

                var objRetWorldWindInfo = WorldWindCatalog.GetWorldWindInfo(lnDepartZone, lnArrivZone, Quarter, lcAltd);
                FlightPak.Data.MasterCatalog.WorldWind worldWind = new Data.MasterCatalog.WorldWind();
                bool WorldwindFound = false;
                if (objRetWorldWindInfo.ReturnFlag)
                {
                    if (objRetWorldWindInfo.EntityList.Count > 0)
                    {
                        worldWind = objRetWorldWindInfo.EntityList[0];
                        if (worldWind != null)
                        {
                            WorldwindFound = true;
                            if (worldWind.Direction != null && worldWind.WorldWindStandard != null)
                                lnWinds = (int)worldWind.Direction - ((int)worldWind.WorldWindStandard * lnRel_Factor);
                        }
                    }
                }
                if (!WorldwindFound)
                {
                    objRetWorldWindInfo = WorldWindCatalog.GetWorldWindInfo(lnArrivZone, lnDepartZone, Quarter, lcAltd);
                    if (objRetWorldWindInfo.ReturnFlag)
                    {
                        if (objRetWorldWindInfo.EntityList.Count > 0)
                        {
                            worldWind = objRetWorldWindInfo.EntityList[0];
                            if (worldWind != null)
                            {
                                WorldwindFound = true;
                                if (worldWind.ReturnDirection != null && worldWind.WorldWindStandard != null)
                                    lnWinds = (int)worldWind.ReturnDirection - ((int)worldWind.WorldWindStandard * lnRel_Factor);
                            }
                        }
                    }
                }

                if (!WorldwindFound)
                {
                    if (lcDepLatDir.ToUpper() == "N" && lcArrLatDir.ToUpper() == "N")
                    {
                        lnHemi = 0;
                    }
                    else if (lcDepLatDir.ToUpper() == "S" && lcArrLatDir.ToUpper() == "S")
                    {
                        lnHemi = 1;
                    }
                    else
                    {
                        lnHemi = 2;
                    }
                    //}


                    //Get Quad and Direction of Travel

                    if ((lnDepLng < 0))
                    {
                        lnDepLng = 360 + lnDepLng;
                    }
                    if (lnArrLng < 0)
                    {

                        lnArrLng = 360 + lnArrLng;
                    }
                    lnDepLat = (90 + lnDepLat);
                    lnArrLat = (90 + lnArrLat);

                    lnDeltaLng = (lnDepLng - lnArrLng) * -1;
                    lnDeltaLat = (lnDepLat - lnArrLat) * -1;
                    if (lnDeltaLng > 180)
                    {
                        lnDeltaLng = (360 - lnDeltaLng) * -1;
                    }

                    else if (lnDeltaLng < -180)
                    {
                        lnDeltaLng = lnDeltaLng + 360;
                    }



                    if (lnDeltaLng < 0.1 && lnDeltaLng > -0.1)
                    {
                        lnSlope = 0;
                    }
                    else
                    {
                        lnSlope = lnDeltaLat / lnDeltaLng;
                    }
                    #region Wind Calculation -Get Quad and Direct Values
                    //Get Quad and Direct Values

                    lnCompass = GetHeading(lnSlope, lnDeltaLat, lnDeltaLng);
                    lnDirect = 1;
                    lnQuad = 1;
                    if (lnCompass >= 337.5 && lnCompass <= 360)
                    {
                        lnDirect = -1;
                        lnQuad = 2;
                    }
                    if (lnCompass >= 22.5 && lnCompass <= 67.4999999)
                    {

                        lnDirect = -1;
                        lnQuad = 3;
                    }

                    if (lnCompass >= 67.5 && lnCompass <= 112.4999999)
                    {

                        lnQuad = 1;

                    }

                    if (lnCompass >= 112.5 && lnCompass <= 202.4999999)
                    {

                        lnQuad = 4;
                    }

                    if (lnCompass >= 202.5 && lnCompass <= 247.4999999)
                    {

                        lnQuad = 3;
                    }

                    if (lnCompass >= 247.5 && lnCompass <= 292.4999999)
                    {

                        lnDirect = -1;
                        lnQuad = 1;

                    }

                    if (lnCompass >= 292.5 && lnCompass <= 337.4999999)
                    {
                        lnDirect = -1;
                        lnQuad = 4;
                    }
                    //Get Quad and Direct Values

                    #endregion
                    #region Final calculation of Winds & Standard Deviation
                    //Get Winds and Standard Deviation
                    string lcHemi = Convert.ToString(lnHemi);
                    string lcQuad = Convert.ToString(lnQuad);
                    string lcQTR = Convert.ToString(lnQTR);


                    var objRetWindStatInfo = WindCatalog.GetWindStatInfo(lcHemi, lcQuad, lcAltd, lcQTR);

                    if (objRetWindStatInfo.ReturnFlag)
                    {
                        if (objRetWindStatInfo.EntityList.Count > 0)
                        {
                            if (objRetWindStatInfo.EntityList[0].Wind != null)
                                lnWinds = (int)objRetWindStatInfo.EntityList[0].Wind;
                            if (objRetWindStatInfo.EntityList[0].WindStatStandard != null)
                                lnStd = (int)objRetWindStatInfo.EntityList[0].WindStatStandard;
                        }
                        else
                        {
                            lnWinds = 0;
                            lnStd = 0;
                        }
                    }
                    else
                    {
                        lnWinds = 0;
                        lnStd = 0;
                    }

                    lnWinds = (lnWinds * lnDirect) - (lnStd * lnRel_Factor);

                    #endregion
                }
            }

            return Math.Truncate(lnWinds);
        }


        #endregion

        #region  – Get Heading- SubFunction in Winds
        /// <summary>
        ///  Function will return the compass heading based on the slope and delta latitude and longitude.
        /// </summary>
        /// <param name="Slope"></param>
        /// <param name="DeltaLat"></param>
        /// <param name="DeltaLng"></param>
        /// <returns></returns>
        public static double GetHeading(double Slope, double DeltaLat, double DeltaLng)
        {
            double lnval = 0;

            if (Slope < .0001 && Slope > -0.0001)
            {

                lnval = 1;
            }

            if (Slope < 1)
            {
                if (DeltaLat >= 0 && DeltaLng < 0)
                {

                    lnval = 90 + (Slope * 60);
                }

                if (DeltaLat < 0 && DeltaLng < 0)
                {

                    lnval = 90 + (Slope * 60);
                }

                if (DeltaLat < 0 && DeltaLng >= 0)
                {

                    lnval = 270 + (Slope * 60);
                }

                if (DeltaLat >= 0 && DeltaLng >= 0)
                {
                    lnval = 270 + (Slope * 60);
                }
            }
            else if (Slope > 1)
            {
                if (DeltaLat >= 0 && DeltaLng < 0)
                {
                    lnval = -60 / Slope;
                }

                if (DeltaLat < 0 && DeltaLng < 0)
                {
                    lnval = 180 - (60 / Slope);
                }

                if (DeltaLat < 0 && DeltaLng >= 0)
                {
                    lnval = 180 - (60 / Slope);
                }
                if (DeltaLat >= 0 && DeltaLng >= 0)
                {
                    lnval = 360 - (60 / Slope);
                }
            }
            else if (Slope == 1)
            {
                if (DeltaLat >= 0 && DeltaLng < 0)
                {
                    lnval = 45;
                }

                if (DeltaLat < 0 && DeltaLng < 0)
                {
                    lnval = 135;
                }

                if (DeltaLat < 0 && DeltaLng >= 0)
                {
                    lnval = 225;
                }

                if (DeltaLat >= 0 && DeltaLng >= 0)
                {
                    lnval = 315;
                }

            }

            return lnval;
        }
        #endregion

        #region ETE Calculation
        /// <summary>
        /// Get the input values from UI screens for Wind, LandingBias,TrueAirSpeed,TakeOffBias,Miles
        /// </summary>
        /// <param name="lnwind"></param>
        /// <param name="lnLndBias"></param>
        /// <param name="lntas"></param>
        /// <param name="lnToBias"></param>
        /// <param name="lnmiles"></param>
        /// <param name="DateVal"></param>
        /// <returns></returns>
        public static double GetIcaoEte(double lnwind, double lnLndBias, double lntas, double lnToBias, double lnmiles, DateTime DateVal)
        {
            double lnEte = 0;


            string lcLndBias = string.Empty;

            var r = GetQtr(DateVal);
            if ((lnwind + lntas) > 0)
            {
                double lnFHr = 0;
                double lnFMin = 0;
                lnEte = Math.Round(lnmiles / (lntas + lnwind), 3);
                string lcEte = Convert.ToString(lnEte);
                int lnPos = lcEte.IndexOf('.');
                double lnHr = 0, lnMin = 0;
                if (lnPos > 0)
                {
                    lnHr = Convert.ToDouble(lcEte.Substring(0, lnPos));
                    lnMin = Convert.ToDouble(lcEte.Substring(lnPos)) * 60;
                }
                else
                {
                    lnHr = Convert.ToDouble(lcEte.ToString());
                }
                lnFHr = lnHr;
                lnFMin = lnMin;
                lnHr = 0;
                lnMin = 0;
                string lcToBias = string.Empty;
                lcToBias = Convert.ToString(Math.Round(lnToBias, 3));
                lcToBias.Trim();
                lnPos = lcToBias.IndexOf('.');
                if (lnPos > 0)
                {
                    lnHr = Convert.ToDouble(lcToBias.Substring(0, lnPos));
                    lnMin = Convert.ToDouble(lcToBias.Substring(lnPos)) * 60;//ROUND(VAL(SUBSTR(lcToBias,lnPos+1))/1000 * 60,0)
                }
                else
                {
                    lnHr = Convert.ToDouble(lcToBias.ToString());
                }
                lnFHr = lnFHr + lnHr;
                lnFMin = lnFMin + lnMin;
                lcLndBias = Convert.ToString(Math.Round(lnLndBias, 3));

                lnHr = 0;
                lnMin = 0;

                lnPos = lcLndBias.IndexOf('.');//AT(".",lcLndBias)
                if (lnPos > 0)
                {
                    lnHr = Convert.ToDouble(lcLndBias.Substring(0, lnPos));//VAL(LEFT(lcLndBias,lnPos-1))
                    lnMin = Convert.ToDouble(lcLndBias.Substring(lnPos)) * 60; //ROUND(VAL(SUBSTR(lcLndBias,lnPos+1))/1000 * 60,0)
                }
                else
                {
                    lnHr = Convert.ToDouble(lcLndBias.ToString());//VAL(LEFT(lcLndBias,lnPos-1))
                }
                lnFHr = lnFHr + lnHr;
                lnFMin = lnFMin + lnMin;
                lnEte = lnFHr + (double)Math.Round((lnFMin / 60), 1);

                lcEte = lnEte.ToString();
                lnPos = lcEte.IndexOf('.');
                string lcHr, lcMin;
                lcHr = lcMin = string.Empty;
                if (lnPos > 0)
                {
                    lcHr = lcEte.Substring(0, lnPos);
                    lcMin = lcEte.Substring(lnPos + 1, 1);
                    lcEte = lcHr + "." + lcMin;
                    lnEte = Convert.ToDouble(lcEte);
                }
            }

            return lnEte;

        }
        #endregion

        #region GMT Calculation
        /// <summary>
        /// GMT Calculation
        /// </summary>
        /// <param name="ldDtTm"></param>
        /// <param name="AP_OffsetToGMTEnd">DepartureAirport Offset to GMT</param>
        /// <param name="airport_dst_yn">AirPort Destination -Yes/No</param>
        /// <param name="AP_DayLightSDT">Departure AirPort DayLightSavingStartDate </param>
        /// <param name="AP_DaylLightSTM">Departure DaylLightSavingStartTime</param>
        /// /// <param name="AP_DayLightSDT">Departure AirPort DayLightSavingEndDate </param>
        /// <param name="AP_DaylLightSTM">Departure DaylLightSavingEndTime</param>
        /// <param name="AP_OffsetToGMTStart">ArrivalAirportOffsetToGMT</param>
        /// <param name="AP_OffsetToGMT">AirPortOffsetToGMT</param>
        /// <returns></returns>
        public static DateTime GetGMT(Int64 AirportID, DateTime ldDtTm, bool IIFindGMT, [Optional, DefaultParameterValue(false)]bool IIGmt)
        {
            DateTime ldNewDtTm = DateTime.MinValue;

            DateTime ldDstBegtm = DateTime.MinValue;
            DateTime ldDstEndtm = DateTime.MinValue;
            double lnDst = 0;

            try
            {

                if (ldDtTm == null || ldDtTm == DateTime.MinValue)
                {
                    return ldDtTm;
                }
                ldNewDtTm = ldDtTm;
                if (IIGmt == true)
                {
                    return ldDtTm;
                }

                AirportManager AirportCatalog = new FlightPak.Business.MasterCatalog.AirportManager();
                var objRetVal = AirportCatalog.GetAirportByAirportID(AirportID);

                List<FlightPak.Data.MasterCatalog.GetAllAirport> airport = new List<GetAllAirport>();
                if (objRetVal.ReturnFlag)
                {
                    airport = objRetVal.EntityList;
                }


                //List<FlightPak.Data.MasterCatalog.GetAllAirport> airport = (from Arpt in objRetVal.EntityList
                //                                                            where Arpt.AirportID == AirportID
                //                                                            select Arpt).ToList();

                //Find Local and Home Time Based on GMT
                //DST Begin date and time stored in ldDstBeg1
                DateTime ldDstBeg1 = DateTime.MinValue;
                DateTime ldDstEnd1 = DateTime.MinValue;
                bool lbIsDayLightSaving = false;
                decimal lnOffsetToGMT = 0;

                if (airport != null && airport.Count > 0)
                {
                    if (airport[0].IsDayLightSaving != null)
                    {
                        if (airport[0].DayLightSavingStartDT != null && airport[0].DayLightSavingEndDT != null)
                            lbIsDayLightSaving = (bool)airport[0].IsDayLightSaving;
                    }

                    if (airport[0].DayLightSavingStartDT != null)
                    {
                        ldDstBeg1 = (DateTime)airport[0].DayLightSavingStartDT;
                        ldDstBeg1 = new DateTime(ldDtTm.Year, ldDstBeg1.Month, ldDstBeg1.Day);
                        if (airport[0].DaylLightSavingStartTM != null)
                            ldDstBegtm = Convert.ToDateTime(airport[0].DaylLightSavingStartTM);
                        else
                            lbIsDayLightSaving = false;

                        ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                        ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                    }
                    if (airport[0].DayLightSavingEndDT != null)
                    {
                        ldDstEnd1 = (DateTime)airport[0].DayLightSavingEndDT;
                        ldDstEnd1 = new DateTime(ldDtTm.Year, ldDstEnd1.Month, ldDstEnd1.Day);
                        if (airport[0].DayLightSavingEndTM != null)
                            ldDstEndtm = Convert.ToDateTime(airport[0].DayLightSavingEndTM);
                        else
                            lbIsDayLightSaving = false;
                        ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                        ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                        ldDstEnd1 = ldDstEnd1.AddSeconds(-60);
                    }

                    if (airport[0].OffsetToGMT != null)
                        lnOffsetToGMT = (decimal)airport[0].OffsetToGMT;



                    if (!IIFindGMT)
                    {
                        if (lbIsDayLightSaving)
                        {
                            //bool summertimeflag = true;
                            if (ldDstEnd1.Month < ldDstBeg1.Month)
                            {
                                if (ldDtTm.Month >= ldDstBeg1.Month && ldDtTm.Month <= 12)
                                {
                                    ldDstEnd1 = new DateTime(ldDstEnd1.Year + 1, ldDstEnd1.Month, ldDstEnd1.Day);
                                    ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                                    ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                                }

                                else if (ldDtTm.Month >= 1 && ldDtTm.Month <= ldDstEnd1.Month)
                                {
                                    ldDstBeg1 = new DateTime(ldDstBeg1.Year - 1, ldDstBeg1.Month, ldDstBeg1.Day);
                                    ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                                    ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                                }
                            }


                            if (ldDtTm.Equals(ldDstBeg1))
                            {
                                double ldBegOffSet = (double)(lnOffsetToGMT * 3600) + 3600;
                                //TimeSpan timeSpanBeg = new TimeSpan((long)(ldBegOffSet));
                                ldNewDtTm = ldDtTm.AddSeconds(ldBegOffSet);
                            }
                            else if (ldDtTm.Equals(ldDstEnd1))
                            {
                                double ldEndOffSet = (((double)lnOffsetToGMT * 3600) + (1 * 3600));
                                //TimeSpan timeSpanEnd = new TimeSpan((long)(ldEndOffSet));
                                ldNewDtTm = ldDtTm.AddSeconds(ldEndOffSet);
                            }

                            else if ((ldDstBeg1 < ldDstEnd1) && (ldDtTm >= ldDstBeg1 && ldDtTm <= ldDstEnd1))
                            {
                                double airPortOffSet = (((double)lnOffsetToGMT * 3600) + 3600);
                                //TimeSpan timeSpan = new TimeSpan((long)(airPortOffSet));
                                ldNewDtTm = ldDtTm.AddSeconds(airPortOffSet);
                            }
                            else
                            {
                                double gmtOffSet = ((double)lnOffsetToGMT * 3600);
                                //TimeSpan timeSpan4 = new TimeSpan((long)(gmtOffSet));
                                ldNewDtTm = ldDtTm.AddSeconds(gmtOffSet);
                            }
                        }
                        else
                        {
                            double gmtOffSet = ((double)lnOffsetToGMT * 3600) + lnDst;
                            //TimeSpan timeSpan4 = new TimeSpan((long)(gmtOffSet));
                            ldNewDtTm = ldDtTm.AddSeconds(gmtOffSet);
                        }
                    }
                    else
                    {
                        if (lbIsDayLightSaving)
                        {
                            if ((ldDstEnd1.Month) < (ldDstBeg1.Month))
                            {
                                if (ldDtTm.Month >= ldDstBeg1.Month && ldDtTm.Month <= 12)
                                {
                                    ldDstEnd1 = new DateTime(ldDstEnd1.Year + 1, ldDstEnd1.Month, ldDstEnd1.Day);
                                    ldDstEnd1 = ldDstEnd1.AddHours(ldDstEndtm.Hour);
                                    ldDstEnd1 = ldDstEnd1.AddMinutes(ldDstEndtm.Minute);
                                }
                                else if ((ldDtTm.Month >= 1) && (ldDtTm.Month <= ldDstEnd1.Month))
                                {
                                    ldDstBeg1 = new DateTime(ldDstBeg1.Year - 1, ldDstBeg1.Month, ldDstBeg1.Day);
                                    ldDstBeg1 = ldDstBeg1.AddHours(ldDstBegtm.Hour);
                                    ldDstBeg1 = ldDstBeg1.AddMinutes(ldDstBegtm.Minute);
                                }
                            }
                            double x5 = (double)lnOffsetToGMT * 3600;
                            //TimeSpan ts5 = new TimeSpan((long)(x5));
                            DateTime ltLocDstChgBeg = ldDstBeg1.AddSeconds(x5);
                            //DateTime ltLocDstChgBeg = ldDstBeg1.Add(ts5);

                            double x6 = ((double)airport[0].OffsetToGMT * 3600) + (((double)lnOffsetToGMT != 0) ? (1 * 3600) : 0);
                            //TimeSpan ts6 = new TimeSpan((long)(x6));
                            DateTime ltLocDstChgEnd = ldDstEnd1.AddSeconds(x6);
                            //DateTime ltLocDstChgEnd = ldDstEnd1.Add(ts6);



                            double x7 = (double)lnOffsetToGMT * 3600;
                            //TimeSpan ts7 = new TimeSpan((long)(x7));

                            double x8 = (((double)lnOffsetToGMT * 3600) + 3540);
                            //TimeSpan ts8 = new TimeSpan((long)(x8));

                            if ((ldDtTm >= ltLocDstChgBeg) && (ldDtTm <= ltLocDstChgEnd))
                            {

                                lnDst = 1 * 3600;
                            }
                            if ((ldDtTm >= ldDstBeg1.AddSeconds(x7)) && (ldDtTm <= ldDstBeg1.AddSeconds(x8)))
                            {
                                lnDst = 0;
                            }
                        }

                        double x9 = (((double)lnOffsetToGMT * 3600) + lnDst);
                        //TimeSpan ts9 = new TimeSpan((long)(x9));
                        ldNewDtTm = ldDtTm.AddSeconds(-(x9));

                    }
                }
            }
            catch (Exception e)
            {
                //manually handled
                ldNewDtTm = ldDtTm;
            }
            return ldNewDtTm;
        }
        #endregion

        #region Domestic/International

        //Domestic/International

        public static string CalcDomIntl(Int64 DepartAirportID, Int64 ArriveAirportID, Int64 HomebaseAirportID)
        {
            AirportManager AirportCatalog = new AirportManager();




            List<FlightPak.Data.MasterCatalog.GetAllAirport> DepAirPort = new List<GetAllAirport>();
            List<FlightPak.Data.MasterCatalog.GetAllAirport> ArrAirPort = new List<GetAllAirport>();
            List<FlightPak.Data.MasterCatalog.GetAllAirport> HomebaseAirport = new List<GetAllAirport>();
            var objRetVal = AirportCatalog.GetAirportByAirportID(DepartAirportID);
            if (objRetVal.ReturnFlag)
            {
                DepAirPort = objRetVal.EntityList;
            }
            objRetVal = null;
            objRetVal = AirportCatalog.GetAirportByAirportID(ArriveAirportID);
            if (objRetVal.ReturnFlag)
            {
                ArrAirPort = objRetVal.EntityList;
            }
            objRetVal = null;
            objRetVal = AirportCatalog.GetAirportByAirportID(HomebaseAirportID);
            if (objRetVal.ReturnFlag)
            {
                HomebaseAirport = objRetVal.EntityList;
            }


            //List<GetAllAirport> DepAirPort = (from airprt in objRetVal.EntityList
            //                                  where airprt.IcaoID == DepartIcaoID
            //                                  select airprt).ToList();
            //List<GetAllAirport> ArrAirPort = (from airprt in objRetVal.EntityList
            //                                  where airprt.IcaoID == ArriveIcaoID
            //                                  select airprt).ToList();
            //List<GetAllAirport> HomebaseAirport = (from airprt in objRetVal.EntityList
            //                                       where airprt.IcaoID == HomebaseICAOID
            //                                       select airprt).ToList();


            string nDutyType;

            string tripLeg_dutytype = string.Empty;
            //Initialized local variables for Depart, Arrival and MainBase
            string lcDepCtry = string.Empty;
            string lcArrCtry = string.Empty;
            string lcMainBase = string.Empty;
            string LcArrCtry = string.Empty;


            if (DepAirPort != null && DepAirPort.Count > 0)
                if (DepAirPort[0].CountryName != null)
                    lcDepCtry = DepAirPort[0].CountryName;
            if (ArrAirPort != null && ArrAirPort.Count > 0)
                if (ArrAirPort[0].CountryName != null)
                    LcArrCtry = ArrAirPort[0].CountryName;
            if (HomebaseAirport != null && HomebaseAirport.Count > 0)
                if (HomebaseAirport[0].CountryName != null)
                    lcMainBase = HomebaseAirport[0].CountryName;

            if (lcDepCtry != null && lcDepCtry == lcMainBase || lcArrCtry != null && lcArrCtry == lcMainBase)
            {
                nDutyType = "2";
            }

            else
            {
                nDutyType = "1";
            }

            tripLeg_dutytype = nDutyType;  // Here Duty Type is saved in Tripleg table and displayed on user interface && Need to take the values from 'tripLeg' table ( bp.tripLeg_dutytype)

            return tripLeg_dutytype;

        }
        //Domestic/International 
        #endregion
    }
}
