﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlightPak.Data.Common;
using FlightPak.Data.CorporateRequest;
using FlightPak.Business.Common;

namespace FlightPak.Business.ICorporateRequest
{
    public interface ICorporateRequestManager
    {
        #region "Corporate Request Main"
        ReturnValue<CRMain> GetRequest(Int64 RequestNumber);
        ReturnValue<CRMain> UpdateRequest(CRMain request);
        ReturnValue<CRMain> DeleteRequest(Int64 RequestNumber);
        #endregion


        #region "Corporate Request Leg"
        ReturnValue<CRMain> AddCorporateRequestLeg(CRLeg corporaterequestLeg);
        ReturnValue<CRMain> UpdateCorporateRequestLeg(CRLeg corporaterequestLeg);
        ReturnValue<CRMain> DeleteCorporateRequestLeg(string TripNumber, string LegID, string TripLegID);
        ReturnValue<CRLeg> ChangePrivate(CRLeg oCRLeg);
        #endregion


        #region "Corporate Request Passenger"

        ReturnValue<CRMain> DeleteCorporateRequestCrew(string TripNumber, string LegID, string TripLegID);
        ReturnValue<CRMain> UpdateCorporateRequestCrew(CRPassenger corporaterequestPassenger);
        ReturnValue<CRMain> AddCorporateRequestCrew(CRPassenger corporaterequestPassenger);
        #endregion


        #region "Corporate Change Queue"
        ReturnValue<GetAllCRMain> GetCRChangeQueue();
        ReturnValue<CRMain> QueueTransfer(CRMain oCRMain);
        ReturnValue<CRMain> DenyRequest(CRMain oCRMain);
        ReturnValue<CRMain> AcceptRequest(CRMain oCRMain);
        ReturnValue<CRMain> ApproveRequest(CRMain oCRMain);
        #endregion

        #region "Submit Request"
        ReturnValue<CRMain> UpdateCRMainCorpStatus(CRMain oCRMain);
        #endregion

        #region "Cancel Request"
        ReturnValue<CRMain> CancelRequest(CRMain oCRMain);
        #endregion

    }
}
