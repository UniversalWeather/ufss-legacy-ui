﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.Utilities
{
    public class AirportPairManager : BaseManager
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Utilities.AirportPair> Add(Data.Utilities.AirportPair oAirportPairs)
        {
            ReturnValue<Data.Utilities.AirportPair> ReturnValue = new ReturnValue<Data.Utilities.AirportPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairs))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.AirportPairs.AddObject(oAirportPairs);
                        foreach (Data.Utilities.AirportPairDetail oAirportPairsDetail in oAirportPairs.AirportPairDetails)
                        {
                            oAirportPairsDetail.LastUpdUID = oAirportPairs.LastUpdUID;
                            oAirportPairsDetail.CustomerID = oAirportPairs.CustomerID;
                            if (oAirportPairsDetail.AirportPairDetailID < 0)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(oAirportPairsDetail, System.Data.EntityState.Added);
                                objContainer.AirportPairDetails.AddObject(oAirportPairsDetail);
                            }
                            else
                            {
                                Common.EMCommon.SetAllModified<Data.Utilities.AirportPairDetail>(oAirportPairsDetail, objContainer);
                            }
                        }
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oAirportPairs;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.AirportPair> Update(Data.Utilities.AirportPair oAirportPairs)
        {
            ReturnValue<Data.Utilities.AirportPair> ReturnValue = new ReturnValue<Data.Utilities.AirportPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairs))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.AirportPairs.Attach(oAirportPairs);
                        objContainer.ObjectStateManager.ChangeObjectState(oAirportPairs, System.Data.EntityState.Modified);
                        foreach (Data.Utilities.AirportPairDetail oAirportPairsDetail in oAirportPairs.AirportPairDetails)
                        {
                            oAirportPairsDetail.LastUpdUID = oAirportPairs.LastUpdUID;
                            oAirportPairsDetail.CustomerID = oAirportPairs.CustomerID;
                            if (oAirportPairsDetail.AirportPairDetailID < 0)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(oAirportPairsDetail, System.Data.EntityState.Added);
                                objContainer.AirportPairDetails.AddObject(oAirportPairsDetail);
                            }
                            else
                            {
                                Common.EMCommon.SetAllModified<Data.Utilities.AirportPairDetail>(oAirportPairsDetail, objContainer);
                            }
                        }
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.AirportPair> Delete(Data.Utilities.AirportPair oAirportPairs)
        {
            ReturnValue<Data.Utilities.AirportPair> ReturnValue = new ReturnValue<Data.Utilities.AirportPair>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairs))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.AirportPairs.Attach(oAirportPairs);
                        objContainer.AirportPairs.DeleteObject(oAirportPairs);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.AirportPair> GetList()
        {
            ReturnValue<Data.Utilities.AirportPair> ReturnValue = new ReturnValue<Data.Utilities.AirportPair>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetAirportPair> GetAirportPairs(Data.Utilities.AirportPair oAirportPairs)
        {
            ReturnValue<Data.Utilities.GetAirportPair> ReturnValue = new ReturnValue<Data.Utilities.GetAirportPair>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetAirportPair(oAirportPairs.AirportPairID, oAirportPairs.CustomerID, oAirportPairs.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
