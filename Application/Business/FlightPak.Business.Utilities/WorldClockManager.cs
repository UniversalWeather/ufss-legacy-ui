﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.Utilities
{
    public class WorldClockManager : BaseManager
    {
        private ExceptionManager exManager;

        public Boolean AddWorldClock(long AirportID, bool IsWorldClock)
        {
            Boolean ReturnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID, IsWorldClock))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.AddWorldClock(AirportID, CustomerID, IsWorldClock);
                    ReturnValue = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public Boolean DeleteWorldClock(long AirportID)
        {
            Boolean ReturnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.DeleteWorldClock(AirportID);
                    ReturnValue = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public Boolean UpdateWorldClock(string AirportID, bool IsWorldClock)
        {
            Boolean ReturnValue = false;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(AirportID, IsWorldClock))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    Data.MasterCatalog.MasterDataContainer objContainer = new Data.MasterCatalog.MasterDataContainer();
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.UpdateWorldClock(AirportID, CustomerID, IsWorldClock);
                    ReturnValue = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }


        public ReturnValue<Data.Utilities.GetAllAirportForWorldClock> GetAllAirportForWorldClock()
        {
            ReturnValue<Data.Utilities.GetAllAirportForWorldClock> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer Container = new Data.Utilities.UtilitiesDataModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Utilities.GetAllAirportForWorldClock>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetAllAirportForWorldClock(CustomerID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        
    }
}
