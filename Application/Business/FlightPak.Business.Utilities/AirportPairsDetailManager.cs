﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.Utilities
{
    public class AirportPairsDetailManager : BaseManager
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Utilities.AirportPairDetail> Add(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            ReturnValue<Data.Utilities.AirportPairDetail> ReturnValue = new ReturnValue<Data.Utilities.AirportPairDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairsDetail))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.AirportPairDetails.AddObject(oAirportPairsDetail);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oAirportPairsDetail;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.AirportPairDetail> Update(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            ReturnValue<Data.Utilities.AirportPairDetail> ReturnValue = new ReturnValue<Data.Utilities.AirportPairDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairsDetail))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.AirportPairDetails.Attach(oAirportPairsDetail);
                        objContainer.ObjectStateManager.ChangeObjectState(oAirportPairsDetail, System.Data.EntityState.Modified);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.AirportPairDetail> Delete(Data.Utilities.AirportPairDetail oAirportPairsDetail)
        {
            ReturnValue<Data.Utilities.AirportPairDetail> ReturnValue = new ReturnValue<Data.Utilities.AirportPairDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oAirportPairsDetail))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.AirportPairDetails.Attach(oAirportPairsDetail);
                        objContainer.AirportPairDetails.DeleteObject(oAirportPairsDetail);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.AirportPairDetail> GetList()
        {
            ReturnValue<Data.Utilities.AirportPairDetail> ReturnValue = new ReturnValue<Data.Utilities.AirportPairDetail>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetAirportPairDetail> GetAirportPairsDetail(Data.Utilities.AirportPairDetail oGetAirportPairDetail)
        {
            ReturnValue<Data.Utilities.GetAirportPairDetail> ReturnValue = new ReturnValue<Data.Utilities.GetAirportPairDetail>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetAirportPairDetail(oGetAirportPairDetail.AirportPairDetailID, oGetAirportPairDetail.CustomerID, oGetAirportPairDetail.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber> GetAirportPairDetailByPairNumber(Data.Utilities.AirportPairDetail oGetAirportPairDetail)
        {
            ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber> ReturnValue = new ReturnValue<Data.Utilities.GetAirportPairDetailByPairNumber>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetAirportPairDetailByPairNumber(oGetAirportPairDetail.AirportPairID, oGetAirportPairDetail.CustomerID, oGetAirportPairDetail.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
