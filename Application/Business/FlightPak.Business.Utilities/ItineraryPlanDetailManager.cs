﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.Utilities
{
    public class ItineraryPlanDetailManager : BaseManager
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Utilities.ItineraryPlanDetail> Add(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            ReturnValue<Data.Utilities.ItineraryPlanDetail> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlanDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlanDetail))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.ItineraryPlanDetails.AddObject(oItineraryPlanDetail);
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oItineraryPlanDetail;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.ItineraryPlanDetail> Update(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            ReturnValue<Data.Utilities.ItineraryPlanDetail> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlanDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlanDetail))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.ItineraryPlanDetails.Attach(oItineraryPlanDetail);
                        objContainer.ObjectStateManager.ChangeObjectState(oItineraryPlanDetail, System.Data.EntityState.Modified);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.ItineraryPlanDetail> Delete(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            ReturnValue<Data.Utilities.ItineraryPlanDetail> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlanDetail>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlanDetail))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.ItineraryPlanDetails.Attach(oItineraryPlanDetail);
                        objContainer.ItineraryPlanDetails.DeleteObject(oItineraryPlanDetail);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.ItineraryPlanDetail> GetList()
        {
            ReturnValue<Data.Utilities.ItineraryPlanDetail> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlanDetail>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetItineraryPlanDetail> GetItineraryPlanDetail(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            ReturnValue<Data.Utilities.GetItineraryPlanDetail> ReturnValue = new ReturnValue<Data.Utilities.GetItineraryPlanDetail>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetItineraryPlanDetail(oItineraryPlanDetail.ItineraryPlanDetailID, oItineraryPlanDetail.CustomerID, oItineraryPlanDetail.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID> GetItineraryPlanDetailByItineraryPlanID(Data.Utilities.ItineraryPlanDetail oItineraryPlanDetail)
        {
            ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID> ReturnValue = new ReturnValue<Data.Utilities.GetItineraryPlanDetailByItineraryPlanID>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetItineraryPlanDetailByItineraryPlanID(oItineraryPlanDetail.ItineraryPlanID, oItineraryPlanDetail.CustomerID, oItineraryPlanDetail.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
