﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Configuration;

namespace FlightPak.Business.Utilities
{

    public class LocatorManager : BaseManager
    {
        private ExceptionManager exManager;
        public ReturnValue<Data.Utilities.GetLocatorAirport> GetAllLocatorAirport(Int64 airportID, string icaoID, string iata, string countryCD, Int64 countryID, decimal longestRunway, string cityName, Int64 metroID, string metroCD, int milesFrom, string stateName, string airportName, bool isHeliport, bool isEntryPort, bool isInActive)
        {

            ReturnValue<Data.Utilities.GetLocatorAirport> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(airportID, icaoID, iata, countryCD, countryID, longestRunway, cityName, metroID, metroCD, milesFrom, stateName, airportName, isHeliport, isEntryPort, isInActive))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer Container = new Data.Utilities.UtilitiesDataModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Utilities.GetLocatorAirport>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;

                    //This should be retrived from Services --> web.config
                    int MaxCommandTimeOut = -1;
                    if (ConfigurationManager.AppSettings["MaxCommandTimeOut"] != null)
                    {
                        MaxCommandTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["MaxCommandTimeOut"]);
                    }
                    Container.CommandTimeout = MaxCommandTimeOut;
                    if (!(airportID == 0 && icaoID == string.Empty && iata == string.Empty && countryCD == string.Empty && countryID == 0 && longestRunway == 0 && cityName == string.Empty && metroID == 0 && metroCD == string.Empty && milesFrom == 0 && stateName == string.Empty && airportName == string.Empty && Convert.ToBoolean(isHeliport) == false && (Convert.ToBoolean(isEntryPort) == false || Convert.ToBoolean(isEntryPort) == true) && (Convert.ToBoolean(isInActive) == false || Convert.ToBoolean(isInActive) == true)))
                    {
                        ReturnValue.EntityList = Container.GetLocatorAirport(CustomerID, airportID, icaoID, iata, countryCD, countryID, longestRunway, cityName, metroID, metroCD, milesFrom, stateName, airportName, Convert.ToBoolean(isHeliport), Convert.ToBoolean(isEntryPort), Convert.ToBoolean(isInActive)).ToList();
                    }
                    else
                    {
                        ReturnValue.EntityList = null;
                    }
                    ReturnValue.ReturnFlag = true;
                    Container.CommandTimeout = null;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetLocatorHotel> GetAllLocatorHotel(Int64 airportID, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string hotelName, int milesFrom)
        {
            ReturnValue<Data.Utilities.GetLocatorHotel> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, hotelName, milesFrom))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer Container = new Data.Utilities.UtilitiesDataModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Utilities.GetLocatorHotel>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetLocatorHotel(CustomerID, airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, hotelName, milesFrom).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }


        public ReturnValue<Data.Utilities.GetLocatorVendor> GetAllLocatorVendor(Int64 airportid, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string name, int milesFrom, Int64 aircraftID, string aircraftCD)
        {
            ReturnValue<Data.Utilities.GetLocatorVendor> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(airportid, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom, aircraftID, aircraftCD))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer Container = new Data.Utilities.UtilitiesDataModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Utilities.GetLocatorVendor>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetLocatorVendor(CustomerID, airportid, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom, aircraftID, aircraftCD).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }


        public ReturnValue<Data.Utilities.GetLocatorCustom> GetAllLocatorCustom(Int64 airportID, string icaoID, string iata, string stateName, string cityName, string countryCD, Int64 countryID, Int64 metroID, string metroCD, string name, int milesFrom)
        {
            ReturnValue<Data.Utilities.GetLocatorCustom> ReturnValue = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    Data.Utilities.UtilitiesDataModelContainer Container = new Data.Utilities.UtilitiesDataModelContainer();
                    // Fix for recursive infinite loop
                    ReturnValue = new ReturnValue<Data.Utilities.GetLocatorCustom>();
                    Container.ContextOptions.LazyLoadingEnabled = false;
                    Container.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = Container.GetLocatorCustom(CustomerID, airportID, icaoID, iata, stateName, cityName, countryCD, countryID, metroID, metroCD, name, milesFrom).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
