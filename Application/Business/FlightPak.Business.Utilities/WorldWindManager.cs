﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.Utilities
{
    public class WorldWindManager : BaseManager
    {
        private ExceptionManager exManager;
        
        public ReturnValue<Data.Utilities.GetWorldWindAirRoutes> GetWorldWindAirRoutes(Int64 DepartureAirportID, Int64 ArriavalAirportID)
        {
            ReturnValue<Data.Utilities.GetWorldWindAirRoutes> ReturnValue = new ReturnValue<Data.Utilities.GetWorldWindAirRoutes>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetWorldWindAirRoutes(CustomerID, DepartureAirportID, ArriavalAirportID).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        
    }
}
