﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data.Objects.DataClasses;
using FlightPak.Business.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
namespace FlightPak.Business.Utilities
{
    public class ItineraryPlanManager : BaseManager
    {
        private ExceptionManager exManager;

        public ReturnValue<Data.Utilities.ItineraryPlan> Add(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            ReturnValue<Data.Utilities.ItineraryPlan> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlan>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlan))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.ItineraryPlans.AddObject(oItineraryPlan);
                        foreach (Data.Utilities.ItineraryPlanDetail oItineraryPlansDetail in oItineraryPlan.ItineraryPlanDetails)
                        {
                            oItineraryPlansDetail.LastUpdUID = oItineraryPlan.LastUpdUID;
                            oItineraryPlansDetail.CustomerID = oItineraryPlan.CustomerID;
                            if (oItineraryPlansDetail.ItineraryPlanDetailID < 0)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(oItineraryPlansDetail, System.Data.EntityState.Added);
                                objContainer.ItineraryPlanDetails.AddObject(oItineraryPlansDetail);
                            }
                            else
                            {
                                Common.EMCommon.SetAllModified<Data.Utilities.ItineraryPlanDetail>(oItineraryPlansDetail, objContainer);
                            }
                        }
                        objContainer.ContextOptions.LazyLoadingEnabled = false;
                        objContainer.ContextOptions.ProxyCreationEnabled = false;
                        objContainer.SaveChanges();
                    }
                    ReturnValue.EntityInfo = oItineraryPlan;
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.ItineraryPlan> Update(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            ReturnValue<Data.Utilities.ItineraryPlan> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlan>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlan))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.ItineraryPlans.Attach(oItineraryPlan);
                        objContainer.ObjectStateManager.ChangeObjectState(oItineraryPlan, System.Data.EntityState.Modified);
                        foreach (Data.Utilities.ItineraryPlanDetail oItineraryPlansDetail in oItineraryPlan.ItineraryPlanDetails)
                        {
                            oItineraryPlansDetail.LastUpdUID = oItineraryPlan.LastUpdUID;
                            oItineraryPlansDetail.CustomerID = oItineraryPlan.CustomerID;
                            if (oItineraryPlansDetail.ItineraryPlanDetailID < 0)
                            {
                                objContainer.ObjectStateManager.ChangeObjectState(oItineraryPlansDetail, System.Data.EntityState.Added);
                                objContainer.ItineraryPlanDetails.AddObject(oItineraryPlansDetail);
                            }
                            else
                            {
                                Common.EMCommon.SetAllModified<Data.Utilities.ItineraryPlanDetail>(oItineraryPlansDetail, objContainer);
                            }
                        }
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.ItineraryPlan> Delete(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            ReturnValue<Data.Utilities.ItineraryPlan> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlan>();
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(oItineraryPlan))
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    using (Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer())
                    {
                        objContainer.ItineraryPlans.Attach(oItineraryPlan);
                        objContainer.ItineraryPlans.DeleteObject(oItineraryPlan);
                        objContainer.SaveChanges();
                        ReturnValue.ReturnFlag = true;
                    }
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.ItineraryPlan> GetList()
        {
            ReturnValue<Data.Utilities.ItineraryPlan> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlan>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    throw new NotImplementedException();

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }

        public ReturnValue<Data.Utilities.GetItineraryPlan> GetItineraryPlans(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            ReturnValue<Data.Utilities.GetItineraryPlan> ReturnValue = new ReturnValue<Data.Utilities.GetItineraryPlan>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    ReturnValue.EntityList = objContainer.GetItineraryPlan(oItineraryPlan.ItineraryPlanID, oItineraryPlan.CustomerID, oItineraryPlan.IsDeleted).ToList();
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
        
        public ReturnValue<Data.Utilities.ItineraryPlan> UpdateItineraryPlanTransfer(Data.Utilities.ItineraryPlan oItineraryPlan)
        {
            ReturnValue<Data.Utilities.ItineraryPlan> ReturnValue = new ReturnValue<Data.Utilities.ItineraryPlan>();
            Data.Utilities.UtilitiesDataModelContainer objContainer = new Data.Utilities.UtilitiesDataModelContainer();
            //This tracing will let you know about the execution time of an method, and the passing parameters value
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                //Handle methods throguh exception manager
                exManager.Process(() =>
                {
                    // Fix for recursive infinite loop
                    objContainer.ContextOptions.LazyLoadingEnabled = false;
                    objContainer.ContextOptions.ProxyCreationEnabled = false;
                    objContainer.UpdateItineraryPlanTransfer(oItineraryPlan.ItineraryPlanID, CustomerID, oItineraryPlan.LastUpdUID);
                    ReturnValue.ReturnFlag = true;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
            return ReturnValue;
        }
    }
}
