﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;

using FlightPak.Framework.Caching;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Framework.Security
{
    [Serializable]
    public class FPPrincipal : IPrincipal
    {
        private readonly FPIdentity _identity;

        // In this constructor we can't construct tracing & Exception handling.
        // Reason: this._identity is a read only field and can be assigned in constructor alone. when we implement exception handling block, compiler taking it as a normal method block.
        public FPPrincipal(Guid SessionId, bool GetFromCache = true)
        {

            var cacheManager = new CacheManager();
            var fpPrincipal = cacheManager.Get(SessionId.ToString());

            if (fpPrincipal != null && GetFromCache)
            {
                this._identity = ((FPPrincipal)fpPrincipal).Identity;
            }
            else
            {
                this._identity = new FPIdentity(SessionId);
                // If there is any call to wcf before authentication, Identity was filled with default values and added to cache.
                // Even after successful login the cache was not replaced, so it gave unexpected result set (always empty beacuse the default value for customer id = 0)
                // So did a extra check (this.Identity.CustomerID > 0) to overcome this scenario.
                if (GetFromCache && this.Identity.CustomerID > 0)
                    cacheManager.Add(this, SessionId.ToString());
            }

        }
        /// <summary>
        /// Check the user is in role with the BinarySearch
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            if ((role != null) && (this.Identity.Roles != null))
            {
                return this.Identity.Roles.BinarySearch(role.ToLower()) >= 0;
            }
            return false;
        }

        public bool IsInModule(string moduleId)
        {
            if ((moduleId != null) && (this.Identity.ModuleId != null))
            {
                return this.Identity.ModuleId.BinarySearch(moduleId.ToLower()) >= 0;
            }
            return false;
        }

        public bool IsInClient(Int64 clientId, ref string clientCode)
        {
            string id = "$" + Convert.ToString(clientId) + "$";
            bool returnValue = this.Identity.ClientInfo.BinarySearch(id) >= 0;
            if (returnValue)
                clientCode = this.Identity.ClientInfo.Where(x => x.Contains(id)).FirstOrDefault().Replace(id, "");
            return returnValue;
        }

        IIdentity IPrincipal.Identity
        {
            get { return _identity; }
        }

        public FPIdentity Identity
        {
            get { return _identity; }
        }
        public DateTime GetDate(DateTime dt)
        {
            return dt;
        }
    }
}
