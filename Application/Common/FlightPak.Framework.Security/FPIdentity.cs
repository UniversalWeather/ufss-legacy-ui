﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Principal;
using FlightPak.Framework.Data;

namespace FlightPak.Framework.Security
{
    [Serializable]
    public class FPIdentity : IIdentity
    {

        private readonly string _name;
        private readonly string _email;
        private readonly Int64 _customerID;
        private readonly Guid _sessionID;
        private readonly List<string> _roles;
        private readonly bool _isAuthenticated;
        private readonly string _homeBase;
        private readonly bool _isSysAdmin;
        private readonly bool _isDispatcher;
        private readonly bool _homePage;
        private readonly Int64? _homeBaseId;
        private readonly string _firstName;
        private readonly string _middleName;
        private readonly string _lastName;
        private readonly Int64? _clientId;
        private readonly FPSettings _fpSettings;
        private readonly List<GetUserPreferenceUtilitiesList> _fpUserPreferences;
        private readonly List<string> _clientInfo;
        private readonly string _airportName;
        private readonly Int64 _airportId;
        private readonly string _airportICAOCd;
        private readonly string _clientCd;
        private readonly bool _isTripPrivacy;
        private readonly bool _isCorpRequestApproval;
        private readonly string _landingPage;
        private readonly Int64? _travelCoordID;
        private readonly string _travelCoordCD;
        private readonly List<string> _moduleId;

        #region Company Info
        private readonly bool _isAll;
        private readonly bool _isAPIS;
        private readonly bool _isATS;
        private readonly bool _isMapping;
        private readonly bool _isUVFuel;
        private readonly bool _isUVTripLink;
        #endregion

        // In this constructor we can't construct tracing & Exception handling.
        // Reason: this._identity is a read only field and can be assigned in constructor alone. when we implement exception handling block, compiler taking it as a normal method block.
        public FPIdentity(Guid SessionId)
        {
            this._fpSettings = new FPSettings(SessionId);
            using (Framework.Data.FrameworkModelContainer container = new Data.FrameworkModelContainer())
            {
                var returnValue = container.CheckLogin(SessionId).FirstOrDefault();
                _isAuthenticated = returnValue.HasValue ? returnValue.Value > 0 : false;

                if (_isAuthenticated)
                {
                    var resultSet = container.GetIdentity(SessionId).FirstOrDefault();
                    if (resultSet != null)
                    {
                        this._name = resultSet.UserName;
                        this._email = resultSet.EmailID;
                        //Functionally there is no way to have a user with out customer id.
                        this._customerID = resultSet.CustomerID.GetValueOrDefault();
                        if (resultSet.Roles != null)
                        {
                            // As per review document, roles should be 
                            // 1. Cached  - Done
                            // 2. Sorted  - Done in code
                            // 3. Set to lower case - Done in SP
                            // 4. Remove trailing spaces - Done in SP
                            this._roles = resultSet.Roles.Split(',').ToList<string>();
                            this._roles.Sort();
                        }
                        if (resultSet.Modules != null)
                        {
                            
                            this._moduleId = resultSet.Modules.Split(',').ToList<string>();
                            this._moduleId.Sort();
                        }
                        this._homeBase = resultSet.HomeBase;
                        //By default it will
                        this._isSysAdmin = resultSet.IsSystemAdmin.GetValueOrDefault();
                        this._isDispatcher = resultSet.IsDispatcher.GetValueOrDefault();
                        this._homeBaseId = resultSet.HomebaseID;
                        this._firstName = resultSet.FirstName;
                        this._middleName = resultSet.MiddleName;
                        this._lastName = resultSet.LastName;
                        if (resultSet.Info != null)
                            this._clientInfo = resultSet.Info.Split(',').ToList<string>();
                        this._airportName = resultSet.AirportName;
                        this._isCorpRequestApproval = resultSet.IsCorpRequestApproval.GetValueOrDefault();
                        this._airportId = resultSet.AirportID;
                        this._airportICAOCd = resultSet.IcaoID;
                        this._isTripPrivacy = resultSet.IsTripPrivacy.GetValueOrDefault();
                        this._clientId = this._clientInfo != null && this.ClientInfo.Count > 0 ? Convert.ToInt64(this.ClientInfo[0].Replace("$", "").Split(':').ToList<string>()[0]) : (Int64?)null;
                        this._clientCd = this._clientInfo != null && this.ClientInfo.Count > 0 ? this.ClientInfo[0].Replace("$", "").Split(':').ToList<string>()[1] : (string)null;
                        this._travelCoordID = resultSet.TravelCoordID;
                        this._travelCoordCD = resultSet.TravelCoordCD;
                        // Customer specific infos
                        this._isAll = resultSet.IsAll.GetValueOrDefault();
                        this._isAPIS = resultSet.IsAPIS.GetValueOrDefault();
                        this._isATS = resultSet.IsATS.GetValueOrDefault();
                        this._isMapping = resultSet.IsMapping.GetValueOrDefault();
                        this._isUVFuel = resultSet.IsUVFuel.GetValueOrDefault();
                        this._isUVTripLink = resultSet.IsUVTripLink.GetValueOrDefault();
                        this._sessionID = SessionId;
                        this._landingPage = resultSet.LandingPage;
                        this._fpUserPreferences = container.GetUserPreferenceUtilitiesList(SessionId, 100036).ToList();
                    }
                }
            }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Email
        {
            get { return _email; }
        }

        public List<string> Roles
        {
            get { return _roles; }
        }
        public Int64 CustomerID
        {
            get { return _customerID; }
        }

        public string AuthenticationType
        {
            get { return "FlightPakIdentity"; }
        }

        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
        }

        public string HomeBase
        {
            get { return _homeBase; }
        }

        public bool IsSysAdmin
        {
            get { return _isSysAdmin; }
        }

        public bool IsDispatcher
        {
            get { return _isDispatcher; }
        }

        public FPSettings Settings
        {
            get { return _fpSettings; }
        }
        public List<GetUserPreferenceUtilitiesList> UserPreferences
        {
            get { return _fpUserPreferences; }
        }
        public string HomePage
        {
            get { return _homeBase; }
        }

        public Guid SessionID
        {
            get { return this._sessionID; }
        }

        public Int64? HomeBaseId { get { return _homeBaseId; } }

        public string FirstName { get { return _firstName; } }

        public string MiddleName { get { return _middleName; } }

        public string LastName { get { return _lastName; } }

        public Int64? ClientId { get { return _clientId; } }

        public List<string> ClientInfo { get { return _clientInfo; } }

        public string AirportName { get { return _airportName; } }

        public Int64 AirportId { get { return _airportId; } }

        public string AirportICAOCd { get { return _airportICAOCd; } }

        public string ClientCd { get { return _clientCd; } }

        public bool IsTripPrivacy { get { return _isTripPrivacy; } }

        public bool IsCorpRequestApproval { get { return _isCorpRequestApproval; } }

        public string LandingPage { get { return _landingPage;} }

        public Int64? TravelCoordID { get { return _travelCoordID; } }

        public string TravelCoordCD { get { return _travelCoordCD; } }

        public List<string> ModuleId { get { return _moduleId; } }

        #region Company info
        public bool IsAll { get { return _isAll; } }
        public bool IsAPIS { get { return _isAPIS; } }
        public bool IsATS { get { return _isATS; } }
        public bool IsMapping { get { return _isMapping; } }
        public bool IsUVFuel { get { return _isUVFuel; } }
        public bool IsUVTripLink { get { return _isUVTripLink; } }
        #endregion
    }
}
