﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Framework.Security
{
    [Serializable]
    public class FPSettings
    {
        // In this constructor we can't construct tracing & Exception handling.
        // Reason: this._identity is a read only field and can be assigned in constructor alone. when we implement exception handling block, compiler taking it as a normal method block.
        #region Variables
        private readonly Guid _sessionID;
        private readonly bool _isAuthenticated;
        private readonly Int64 _HomebaseID;
        private readonly Int64? _CustomerID;
        private readonly Int64? _ClientID;
        private readonly Int64? _CrewDutyID;
        private readonly Int64? _DefaultFlightCatID;
        private readonly Int64? _DefaultCheckListGroupID;
        private readonly Int64? _DepartmentID;
        private readonly Int64? _ScheduleServiceWaitListID;
        private readonly bool _IsDisplayNoteFlag;
        private readonly bool _IsReportCrewDuty;
        private readonly bool _IsZeroSuppressActivityAircftRpt;
        private readonly bool _IsZeroSuppressActivityCrewRpt;
        private readonly bool _IsZeroSuppressActivityDeptRpt;
        private readonly bool _IsZeroSuppressActivityPassengerRpt;
        private readonly bool _IsAutomaticDispatchNum;
        private readonly bool _IsAutomaticCommentTrips;
        private readonly bool _IsConflictCheck;
        private readonly bool _IsTimeStampRpt;
        private readonly bool _IsTripsheetView;
        private readonly bool _IsTextAutoTab;
        private readonly bool _IsChecklistWarning;
        private readonly bool _IsLogsheetWarning;
        private readonly bool _IsDeactivateHomeBaseFilter;
        private readonly bool _IsInvoiceDTsystemDT;
        private readonly bool _IsQuoteDetailPrint;
        private readonly bool _isQuotePrintFees;
        private readonly bool _IsQuotePrintSum;
        private readonly bool _IsQuotePrintSubtotal;
        private readonly bool _IsCityDescription;
        private readonly bool _IsAirportDescription;
        private readonly bool _IsQuoteICAODescription;
        private readonly bool _IsQuotePrepaidMinimUsageFeeAmt;
        private readonly bool _IsQuoteFlightChargePrepaid;
        private readonly bool _IsQuotePrepaidSegementFee;
        private readonly bool _IsQuoteAdditionalCrewPrepaid;
        private readonly bool _IsQuoteStdCrewRonPrepaid;
        private readonly bool _IsQuoteAdditonalCrewRON;
        private readonly bool _IsQuoteFlightLandingFeePrint;
        private readonly bool _IsQuoteWaitingChargePrepaid;
        private readonly bool _IsQuotePrintAdditionalFee;
        private readonly bool _IsQuoteDispcountAmtPrint;
        private readonly bool _IsQuoteSubtotalPrint;
        private readonly bool _IsQuoteTaxesPrint;
        private readonly bool _IsQuoteInvoicePrint;
        private readonly bool _IsInvoiceRptLegNum;
        private readonly bool _IsInvoiceRptDetailDT;
        private readonly bool _IsInvoiceRptFromDescription;
        private readonly bool _IsQuoteDetailRptToDescription;
        private readonly bool _IsQuoteDetailInvoiceAmt;
        private readonly bool _IsQuoteDetailFlightHours;
        private readonly bool _IsQuoteDetailMiles;
        private readonly bool _IsQuoteDetailPassengerCnt;
        private readonly bool _IsQuoteFeelColumnDescription;
        private readonly bool _IsQuoteFeelColumnInvoiceAmt;
        private readonly bool _IsQuoteDispatch;
        private readonly bool _IsQuoteInformation;
        private readonly bool _IsCompanyName;
        private readonly bool _IsChangeQueueDetail;
        private readonly bool _IsQuoteChangeQueueFees;
        private readonly bool _IsQuoteChangeQueueSum;
        private readonly bool _IsQuoteChangeQueueSubtotal;
        private readonly bool _IsChangeQueueCity;
        private readonly bool _IsChangeQueueAirport;
        private readonly bool _IsQuoteChangeQueueICAO;
        private readonly bool _IsQuotePrepaidMinimumUsage2FeeAdj;
        private readonly bool _IsQuotePrepaidMin2UsageFeeAdj;
        private readonly bool _IsQuoteFlight2ChargePrepaid;
        private readonly bool _IsQuotePrepaidSegement2Fee;
        private readonly bool _IsQuoteAdditional2CrewPrepaid;
        private readonly bool _IsQuoteUnpaidAdditional2Crew;
        private readonly bool _IsQuoteStdCrew2RONPrepaid;
        private readonly bool _IsQuoteUnpaidStd2CrewRON;
        private readonly bool _IsQuoteAdditional2CrewRON;
        private readonly bool _IsQuoteUpaidAdditionl2CrewRON;
        private readonly bool _IsQuoteFlight2LandingFeePrint;
        private readonly bool _IsQuoteWaiting2ChargePrepaid;
        private readonly bool _IsQuoteUpaid2WaitingChg;
        private readonly bool _IsQuoteAdditional2FeePrint;
        private readonly bool _IsQuoteDiscount2AmountPrepaid;
        private readonly bool _IsQuoteUnpaid2DiscountAmt;
        private readonly bool _IsQuoteSubtotal2Print;
        private readonly bool _IsQuoteTaxes2Print;
        private readonly bool _IsQuoteInvoice2Print;
        private readonly bool _IsRptQuoteDetailLegNum;
        private readonly bool _IsRptQuoteDetailDepartureDT;
        private readonly bool _IsRptQuoteDetailFromDescription;
        private readonly bool _IsRptQuoteDetailToDescription;
        private readonly bool _IsRptQuoteDetailQuotedAmt;
        private readonly bool _IsRptQuoteDetailFlightHours;
        private readonly bool _IsRptQuoteDetailMiles;
        private readonly bool _IsRptQuoteDetailPassengerCnt;
        private readonly bool _IsRptQuoteFeeDescription;
        private readonly bool _IsRptQuoteFeeQuoteAmt;
        private readonly bool _IsQuoteInformation2;
        private readonly bool _IsCompanyName2;
        private readonly bool _QuoteDescription;
        private readonly bool _IsQuoteSales;
        private readonly bool _IsQuoteTailNum;
        private readonly bool _IsQuoteTypeCD;
        private readonly bool _IsCorpReqActivateAlert;
        private readonly bool _IsQuoteUnpaidFlight2Chg;
        private readonly bool _IsQuoteUnpaidFlightChg;
        private readonly bool _IsQuote2PrepayPrint;
        private readonly bool _IsQuotePrepayPrint;
        private readonly bool _IsQuoteRemaining2AmtPrint;
        private readonly bool _IsQuoteRemainingAmtPrint;
        private readonly bool _IsQuoteDeactivateAutoUpdateRates;
        private readonly bool _CorpReqExcChgWithinWeekend;
        private readonly bool _IsAutomaticCalcLegTimes;
        private readonly bool _IsCorpReqAllowLogUpdCateringInfo;
        private readonly bool _IsCorpReqAllowLogUpdCrewInfo;
        private readonly bool _IsCorpReqAllowLogUpdFBOInfo;
        private readonly bool _IsCorpReqAllowLogUpdPaxInfo;
        private readonly bool _IsCorpReqActivateApproval;
        private readonly bool _IsCorpReqOverrideSubmittoChgQueue;
        private readonly bool _IsQuoteTaxDiscount;
        private readonly bool _IsTransWaitListPassengerfrmPreflightPostflight;
        private readonly bool _IsQuoteEdit;
        private readonly bool _IsAutoRON;
        private readonly bool _IsAutoRollover;
        private readonly bool _IsQuoteWarningMessage;
        private readonly bool _IsInActive;
        private readonly bool _IsChecklistAssign;
        private readonly bool _IsLogoUpload;
        private readonly bool _IsSLeg;
        private readonly bool _IsVLeg;
        private readonly bool _IsSConflict;
        private readonly bool _IsVConflict;
        private readonly bool _IsSChecklist;
        private readonly bool _IsVChecklist;
        private readonly bool _IsSCrewCurrency;
        private readonly bool _IsVCrewCurrency;
        private readonly bool _IsSPassenger;
        private readonly bool _IsVPassenger;
        private readonly bool _IsBlockSeats;
        private readonly bool _IsDepartAuthDuringReqSelection;
        private readonly bool _IsNonLogTripSheetIncludeCrewHIST;
        private readonly bool _IsAutoValidateTripManager;
        private readonly bool _IsQuoteFirstPage;
        private readonly bool _IFirstPG;
        private readonly bool _IsAutoCreateCrewAvailInfo;
        private readonly bool _IsAutoPassenger;
        private readonly bool _IsAutoCrew;
        private readonly bool _IsReqOnly;
        private readonly bool _IsCanPass;
        private readonly bool _IsAutoDispat;
        private readonly bool _IsAutoRevisionNum;
        private readonly bool _IsAutoPopulated;
        private readonly bool _IsOutlook;
        private readonly bool _IsCharterQuoteFee3;
        private readonly bool _IsCharterQuoteFee4;
        private readonly bool _IsCharterQuote5;
        private readonly bool _IsCOLFee3;
        private readonly bool _IsCOLFee4;
        private readonly bool _IsCOLFee5;
        private readonly bool _IsANotes;
        private readonly bool _IsCNotes;
        private readonly bool _IsPNotes;
        private readonly bool _IsOpenHistory;
        private readonly bool _IsEndDutyClient;
        private readonly bool _IsBlackberrySupport;
        private readonly bool _IsSSL;
        private readonly bool _IsAllBase;
        private readonly bool _IsSIFLCalMessage;
        private readonly bool _IsCrewOlap;
        private readonly bool _IsMaxCrew;
        private readonly bool _IsTailInsuranceDue;
        private readonly bool _IsCrewQaulifiedFAR;
        private readonly bool _IsTailBase;
        private readonly bool _IsEnableTSA;
        private readonly bool _IsScheduleDTTM;
        private readonly bool _IsAPISSupport;
        private readonly bool _IsPassengerOlap;
        private readonly bool _IsAutoFillAF;
        private readonly bool _IsAPISAlert;
        private readonly bool _IsSTMPBlock;
        private readonly bool _IsEnableTSAPX;
        private readonly string _TripMGRDefaultStatus;
        private readonly string _SpecificationShort3;
        private readonly string _SpecificationShort4;
        private readonly string _FlightPurpose;
        private readonly string _HomebasePhoneNUM;
        private readonly string _FedAviationRegNum;
        private readonly string _CurrencySymbol;
        private readonly string _LastUpdUID;
        private readonly string _UserID;
        private readonly string _FederalACCT;
        private readonly string _ChtQouteDOMSegCHGACCT;
        private readonly string _HomebaseCD;
        private readonly string _FlightCatagoryPassenger;
        private readonly string _FlightCatagoryPassengerNum;
        private readonly string _MainFee;
        private readonly string _CQCrewDuty;
        private readonly string _CompanyName;
        private readonly string _CorpReqStartTM;
        private readonly string _TaxiTime;
        private readonly string _AutoClimbTime;
        private readonly string _QuoteSubtotal;
        private readonly string _QuoteTaxesDefault;
        private readonly string _DefaultInvoice;
        private readonly string _QouteStdCrew2RONDeposit;
        private readonly string _QuoteAddition2CrewRON;
        private readonly string _uote2LandingFeeDefault;
        private readonly string _QuoteWaiting2Charge;
        private readonly string _Additional2FeeDefault;
        private readonly string _Discount2AmountDeposit;
        private readonly string _Quote2Subtotal;
        private readonly string _Quote2TaxesDefault;
        private readonly string _QuoteInvoice2Default;
        private readonly string _QuotePrepayDefault;
        private readonly string _QuoteRemaining2AmountDefault;
        private readonly string _QuoteRemainingAmountDefault;
        private readonly string _TripsheetRptWriteGroup;
        private readonly DateTime? _ActiveDT;
        private readonly DateTime? _BackupDT;
        private readonly DateTime? _LastUpdTS;
        private readonly DateTime? _AcceptDTTM;
        private readonly int? _LogFixed;
        private readonly int? _LogRotary;
        private readonly int? _WindReliability;
        private readonly int? _Specdec3;
        private readonly int? _Specdec4;
        private readonly int? _QuoteLogoPosition;
        private readonly int? _IlogoPOS;
        private readonly int? _TripsheetSearchBack;
        private readonly int? _TripS;
        private readonly int? _LayoverHrsSIFL;
        private readonly int? _PassengerOFullName;
        private readonly int? _PassengerOMiddle;
        private readonly int? _PassengerOLast;
        private readonly int? _PassengerOLastFirst;
        private readonly int? _PassengerOLastMiddle;
        private readonly int? _PassengerOLastLast;
        private readonly int? _CrewOFullName;
        private readonly int? _CrewOMiddle;
        private readonly int? _CrewOLast;
        private readonly int? _CrewOLastFirst;
        private readonly int? _CrewOLastMiddle;
        private readonly int? _CrewOLastLast;
        private readonly int? _ReportWriteLogoPosition;
        private readonly int? _Searchack;
        private readonly int? _SearchBack;
        private readonly decimal? _DutyBasis;
        private readonly decimal? _AircraftBasis;
        private readonly decimal? _FuelPurchase;
        private readonly decimal? _FuelBurnKiloLBSGallon;
        private readonly decimal? _IsAppliedTax;
        private readonly decimal? _RptSize;
        private readonly decimal? _CorpReqTMEntryFormat;
        private readonly decimal? _ScheduleServiceTimeFormat;
        private readonly decimal? _SSHRFormat;
        private readonly decimal? _DayMonthFormat;
        private readonly decimal? _ScheduledServicesSecurity;
        private readonly decimal? _WebSecurityType;
        private readonly decimal? _QuoteIDTYPE;
        private readonly decimal? _TimeDisplayTenMin;
        private readonly decimal? _HoursMinutesCONV;
        private readonly decimal? _ElapseTMRounding;
        private readonly decimal? _ServerRPF;
        private readonly decimal? _ImageCnt;
        private readonly decimal? _ImagePosition;
        private readonly decimal? _AircraftBlockFlight;
        private readonly decimal? _ServerReport;
        private readonly decimal? _ChtQouteDOMSegCHG;
        private readonly decimal? _ChtQouteIntlSegCHG;
        private readonly decimal? _SegmentFeeAlaska;
        private readonly decimal? _SegementFeeHawaii;
        private readonly decimal? _QuotePageOffsettingInvoice;
        private readonly decimal? _QuotePageOff;
        private readonly decimal? _MinuteHoursRON;
        private readonly decimal? _RestDaysMinimum;
        private readonly decimal? _RestCalendarQTRMinimum;
        private readonly decimal? _FiscalYRStart;
        private readonly decimal? _FiscalYREnd;
        private readonly decimal? _PasswordValidDays;
        private readonly decimal? _PasswordHistoryNum;
        private readonly decimal? _TripsheetDTWarning;
        private readonly decimal? _ScheduleServiceListCTDetail;
        private readonly decimal? _RowsOnPageCnt;
        private readonly decimal? _DayLanding;
        private readonly decimal? _DayLandingMinimum;
        private readonly decimal? _NightLanding;
        private readonly decimal? _NightllandingMinimum;
        private readonly decimal? _Approach;
        private readonly decimal? _ApproachMinimum;
        private readonly decimal? _Instrument;
        private readonly decimal? _InstrumentMinimum;
        private readonly decimal? _TakeoffDay;
        private readonly decimal? _TakeoffDayMinimum;
        private readonly decimal? _TakeoffNight;
        private readonly decimal? _TakeofNightMinimum;
        private readonly decimal? _Day7;
        private readonly decimal? _Day30;
        private readonly decimal? _Day90;
        private readonly decimal? _Month6;
        private readonly decimal? _Month12;
        private readonly decimal? _Day365;
        private readonly decimal? _RestDays;
        private readonly decimal? _CQMessageCD;
        private readonly decimal? _IVMessageCD;
        private readonly decimal? _RefreshTM;
        private readonly decimal? _SearchGridTM;
        private readonly decimal? _CorpReqMinimumRunwayLength;
        private readonly decimal? _CorpReqChgQueueTimer;
        private readonly decimal? _AugmentCrewPercentage;
        private readonly decimal? _GroundTM;
        private readonly decimal? _CQFederalTax;
        private readonly decimal? _RuralTax;
        private readonly decimal? _GroundTMIntl;
        private readonly decimal? _CorpReqWeeklyCalTimer;
        private readonly decimal? _FleetCalendar5DayTimer;
        private readonly decimal? _Crew5DayTimer;
        private readonly decimal? _CorpReqExcChgWithinHours;
        private readonly decimal? _DayMaximum7;
        private readonly decimal? _DayMaximum30;
        private readonly decimal? _CalendarMonthMaximum;
        private readonly decimal? _DayMaximum90;
        private readonly decimal? _CalendarQTRMaximum;
        private readonly decimal? _MonthMaximum6;
        private readonly decimal? _Previous2QTRMaximum;
        private readonly decimal? _MonthMaximum12;
        private readonly decimal? _CalendarYearMaximum;
        private readonly decimal? _DayMaximum365;
        private readonly string _UserNameCBP;
        private readonly string _UserPasswordCBP;
        private readonly string _ApplicationMessage;
        private readonly string _ChtQouteHeaderDetailRpt;
        private readonly string _ChtQouteFooterDetailRpt;
        private readonly string _ChtQuoteCompany;
        private readonly string _ChtQuoteCompanyNameINV;
        private readonly string _ChtQuoteHeaderINV;
        private readonly string _ChtQuoteFooterINV;
        private readonly string _HTMLHeader;
        private readonly string _HTMLFooter;
        private readonly string _HTMLBackgroun;
        private readonly string _WebPageBackgroundColor;
        private readonly string _HeardTextRpt;
        private readonly string _FooterTextRpt;
        private readonly string _MenuBackground;
        private readonly string _MenuHeader;
        private readonly string _MenuFooter;
        private readonly string _SSBACKGRND;
        private readonly string _SSHEADER;
        private readonly string _SSFOOTER;
        private readonly string _WebBackground;
        private readonly string _WebHeader;
        private readonly string _WebFooter;
        private readonly string _QuoteLogo;
        private readonly string _Ilogo;
        private readonly string _TripsheetTabColor;
        private readonly string _TripSheetLogoPosition;
        private readonly string _PrivateTrip;
        private readonly string _ReportWriteLogo;
        private readonly string _RptTabOutbdInstLab1;
        private readonly string _RptTabOutbdInstLab2;
        private readonly string _RptTabOutbdInstLab3;
        private readonly string _RptTabOutbdInstLab4;
        private readonly string _RptTabOutbdInstLab5;
        private readonly string _RptTabOutbdInstLab6;
        private readonly string _RptTabOutbdInstLab7;
        private readonly string _CrewLogCustomLBLShort1;
        private readonly string _CrewLogCustomLBLShort2;
        private readonly string _UserPassword;
        private readonly string _ServerName;
        private readonly string _UserEmail;
        private readonly string _XMLPath;
        private readonly string _StdCrewRON;
        private readonly string _AdditionalCrewCD;
        private readonly string _AdditionalCrewRON;
        private readonly string _WaitTM;
        private readonly string _LandingFee;
        private readonly string _ApplicationDateFormat;
        private readonly string _CrewLogCustomLBLLong1;
        private readonly string _CrewLogCustomLBLLong2;
        private readonly string _SpecificationLong3;
        private readonly string _SpecificationLong4;
        private readonly string _GeneralAviationDesk;
        private readonly string _BaseFax;
        private readonly string _CanPassNum;
        private readonly string _Tsense;
        private readonly string _APISUrl;
        private readonly string _LoginUrl;
        private readonly string _BaseDescription;
        private readonly string _AccountFormat;
        private readonly string _FederalTax;
        private readonly string _StateTax;
        private readonly string _SaleTax;
        private readonly string _RuralAccountNum;
        private readonly string _ChtQouteIntlSegCHGACCT;
        private readonly string _SeqmentAircraftAlaska;
        private readonly string _SegementAircraftHawaii;
        private readonly string _Domain;
        private readonly string _QuoteMinimumUseFeeDepositAmt;
        private readonly string _DepostFlightCharge;
        private readonly string _QuoteSegmentFeeDeposit;
        private readonly string _QuoteAdditonCrewDeposit;
        private readonly string _QuoteStdCrewRONDeposit;
        private readonly string _QuoteAdditionCrewRON;
        private readonly string _QuoteLandingFeeDefault;
        private readonly string _QuoteWaitingCharge;
        private readonly string _QuoteAdditionalFeeDefault;
        private readonly string _DiscountAmountDefault;
        private readonly string _QuoteMinimumUse2FeeDepositAdj;
        private readonly string _Deposit2FlightCharge;
        private readonly string _QuoteSegment2FeeDeposit;
        private readonly string _QuoteAddition2CrewDeposit;
        private readonly string _QuoteTitle;
        private readonly string _QuotePrepay2Default;
        private readonly string _CorpAccountFormat;
        private readonly string _CustomRptMessage;
        private readonly bool _IsMiles;
        private readonly bool _IsKilometer;
        private readonly decimal _CrewChecklistAlertDays;
        private readonly decimal _BusinessWeekStart;
        private readonly int? _CRSearchBack;
        private readonly bool _IsPODepartPercentage;
        private readonly bool _IsDefaultTripDepartureDate;
        #endregion

        #region Properties
        public Int64 HomebaseID { get { return _HomebaseID; } }
        public Int64? CustomerID { get { return _CustomerID; } }
        public Int64? ClientID { get { return _ClientID; } }
        public Int64? CrewDutyID { get { return _CrewDutyID; } }
        public Int64? DefaultFlightCatID { get { return _DefaultFlightCatID; } }
        public Int64? DefaultCheckListGroupID { get { return _DefaultCheckListGroupID; } }
        public Int64? DepartmentID { get { return _DepartmentID; } }
        public Int64? ScheduleServiceWaitListID { get { return _ScheduleServiceWaitListID; } }
        public bool IsDisplayNoteFlag { get { return _IsDisplayNoteFlag; } }
        public bool IsReportCrewDuty { get { return _IsReportCrewDuty; } }
        public bool IsZeroSuppressActivityAircftRpt { get { return _IsZeroSuppressActivityAircftRpt; } }
        public bool IsZeroSuppressActivityCrewRpt { get { return _IsZeroSuppressActivityCrewRpt; } }
        public bool IsZeroSuppressActivityDeptRpt { get { return _IsZeroSuppressActivityDeptRpt; } }
        public bool IsZeroSuppressActivityPassengerRpt { get { return _IsZeroSuppressActivityPassengerRpt; } }
        public bool IsAutomaticDispatchNum { get { return _IsAutomaticDispatchNum; } }
        public bool IsAutomaticCommentTrips { get { return _IsAutomaticCommentTrips; } }
        public bool IsConflictCheck { get { return _IsConflictCheck; } }
        public bool IsTimeStampRpt { get { return _IsTimeStampRpt; } }
        public bool IsTripsheetView { get { return _IsTripsheetView; } }
        public bool IsTextAutoTab { get { return _IsTextAutoTab; } }
        public bool IsChecklistWarning { get { return _IsChecklistWarning; } }
        public bool IsLogsheetWarning { get { return _IsLogsheetWarning; } }
        public bool IsDeactivateHomeBaseFilter { get { return _IsDeactivateHomeBaseFilter; } }
        public bool IsInvoiceDTsystemDT { get { return _IsInvoiceDTsystemDT; } }
        public bool IsQuoteDetailPrint { get { return _IsQuoteDetailPrint; } }
        public bool isQuotePrintFees { get { return _isQuotePrintFees; } }
        public bool IsQuotePrintSum { get { return _IsQuotePrintSum; } }
        public bool IsQuotePrintSubtotal { get { return _IsQuotePrintSubtotal; } }
        public bool IsCityDescription { get { return _IsCityDescription; } }
        public bool IsAirportDescription { get { return _IsAirportDescription; } }
        public bool IsQuoteICAODescription { get { return _IsQuoteICAODescription; } }
        public bool IsQuotePrepaidMinimUsageFeeAmt { get { return _IsQuotePrepaidMinimUsageFeeAmt; } }
        public bool IsQuoteFlightChargePrepaid { get { return _IsQuoteFlightChargePrepaid; } }
        public bool IsQuotePrepaidSegementFee { get { return _IsQuotePrepaidSegementFee; } }
        public bool IsQuoteAdditionalCrewPrepaid { get { return _IsQuoteAdditionalCrewPrepaid; } }
        public bool IsQuoteStdCrewRonPrepaid { get { return _IsQuoteStdCrewRonPrepaid; } }
        public bool IsQuoteAdditonalCrewRON { get { return _IsQuoteAdditonalCrewRON; } }
        public bool IsQuoteFlightLandingFeePrint { get { return _IsQuoteFlightLandingFeePrint; } }
        public bool IsQuoteWaitingChargePrepaid { get { return _IsQuoteWaitingChargePrepaid; } }
        public bool IsQuotePrintAdditionalFee { get { return _IsQuotePrintAdditionalFee; } }
        public bool IsQuoteDispcountAmtPrint { get { return _IsQuoteDispcountAmtPrint; } }
        public bool IsQuoteSubtotalPrint { get { return _IsQuoteSubtotalPrint; } }
        public bool IsQuoteTaxesPrint { get { return _IsQuoteTaxesPrint; } }
        public bool IsQuoteInvoicePrint { get { return _IsQuoteInvoicePrint; } }
        public bool IsInvoiceRptLegNum { get { return _IsInvoiceRptLegNum; } }
        public bool IsInvoiceRptDetailDT { get { return _IsInvoiceRptDetailDT; } }
        public bool IsInvoiceRptFromDescription { get { return _IsInvoiceRptFromDescription; } }
        public bool IsQuoteDetailRptToDescription { get { return _IsQuoteDetailRptToDescription; } }
        public bool IsQuoteDetailInvoiceAmt { get { return _IsQuoteDetailInvoiceAmt; } }
        public bool IsQuoteDetailFlightHours { get { return _IsQuoteDetailFlightHours; } }
        public bool IsQuoteDetailMiles { get { return _IsQuoteDetailMiles; } }
        public bool IsQuoteDetailPassengerCnt { get { return _IsQuoteDetailPassengerCnt; } }
        public bool IsQuoteFeelColumnDescription { get { return _IsQuoteFeelColumnDescription; } }
        public bool IsQuoteFeelColumnInvoiceAmt { get { return _IsQuoteFeelColumnInvoiceAmt; } }
        public bool IsQuoteDispatch { get { return _IsQuoteDispatch; } }
        public bool IsQuoteInformation { get { return _IsQuoteInformation; } }
        public bool IsCompanyName { get { return _IsCompanyName; } }
        public bool IsChangeQueueDetail { get { return _IsChangeQueueDetail; } }
        public bool IsQuoteChangeQueueFees { get { return _IsQuoteChangeQueueFees; } }
        public bool IsQuoteChangeQueueSum { get { return _IsQuoteChangeQueueSum; } }
        public bool IsQuoteChangeQueueSubtotal { get { return _IsQuoteChangeQueueSubtotal; } }
        public bool IsChangeQueueCity { get { return _IsChangeQueueCity; } }
        public bool IsChangeQueueAirport { get { return _IsChangeQueueAirport; } }
        public bool IsQuoteChangeQueueICAO { get { return _IsQuoteChangeQueueICAO; } }
        public bool IsQuotePrepaidMinimumUsage2FeeAdj { get { return _IsQuotePrepaidMinimumUsage2FeeAdj; } }
        public bool IsQuotePrepaidMin2UsageFeeAdj { get { return _IsQuotePrepaidMin2UsageFeeAdj; } }
        public bool IsQuoteFlight2ChargePrepaid { get { return _IsQuoteFlight2ChargePrepaid; } }
        public bool IsQuotePrepaidSegement2Fee { get { return _IsQuotePrepaidSegement2Fee; } }
        public bool IsQuoteAdditional2CrewPrepaid { get { return _IsQuoteAdditional2CrewPrepaid; } }
        public bool IsQuoteUnpaidAdditional2Crew { get { return _IsQuoteUnpaidAdditional2Crew; } }
        public bool IsQuoteStdCrew2RONPrepaid { get { return _IsQuoteStdCrew2RONPrepaid; } }
        public bool IsQuoteUnpaidStd2CrewRON { get { return _IsQuoteUnpaidStd2CrewRON; } }
        public bool IsQuoteAdditional2CrewRON { get { return _IsQuoteAdditional2CrewRON; } }
        public bool IsQuoteUpaidAdditionl2CrewRON { get { return _IsQuoteUpaidAdditionl2CrewRON; } }
        public bool IsQuoteFlight2LandingFeePrint { get { return _IsQuoteFlight2LandingFeePrint; } }
        public bool IsQuoteWaiting2ChargePrepaid { get { return _IsQuoteWaiting2ChargePrepaid; } }
        public bool IsQuoteUpaid2WaitingChg { get { return _IsQuoteUpaid2WaitingChg; } }
        public bool IsQuoteAdditional2FeePrint { get { return _IsQuoteAdditional2FeePrint; } }
        public bool IsQuoteDiscount2AmountPrepaid { get { return _IsQuoteDiscount2AmountPrepaid; } }
        public bool IsQuoteUnpaid2DiscountAmt { get { return _IsQuoteUnpaid2DiscountAmt; } }
        public bool IsQuoteSubtotal2Print { get { return _IsQuoteSubtotal2Print; } }
        public bool IsQuoteTaxes2Print { get { return _IsQuoteTaxes2Print; } }
        public bool IsQuoteInvoice2Print { get { return _IsQuoteInvoice2Print; } }
        public bool IsRptQuoteDetailLegNum { get { return _IsRptQuoteDetailLegNum; } }
        public bool IsRptQuoteDetailDepartureDT { get { return _IsRptQuoteDetailDepartureDT; } }
        public bool IsRptQuoteDetailFromDescription { get { return _IsRptQuoteDetailFromDescription; } }
        public bool IsRptQuoteDetailToDescription { get { return _IsRptQuoteDetailToDescription; } }
        public bool IsRptQuoteDetailQuotedAmt { get { return _IsRptQuoteDetailQuotedAmt; } }
        public bool IsRptQuoteDetailFlightHours { get { return _IsRptQuoteDetailFlightHours; } }
        public bool IsRptQuoteDetailMiles { get { return _IsRptQuoteDetailMiles; } }
        public bool IsRptQuoteDetailPassengerCnt { get { return _IsRptQuoteDetailPassengerCnt; } }
        public bool IsRptQuoteFeeDescription { get { return _IsRptQuoteFeeDescription; } }
        public bool IsRptQuoteFeeQuoteAmt { get { return _IsRptQuoteFeeQuoteAmt; } }
        public bool IsQuoteInformation2 { get { return _IsQuoteInformation2; } }
        public bool IsCompanyName2 { get { return _IsCompanyName2; } }
        public bool QuoteDescription { get { return _QuoteDescription; } }
        public bool IsQuoteSales { get { return _IsQuoteSales; } }
        public bool IsQuoteTailNum { get { return _IsQuoteTailNum; } }
        public bool IsQuoteTypeCD { get { return _IsQuoteTypeCD; } }
        public bool IsCorpReqActivateAlert { get { return _IsCorpReqActivateAlert; } }
        public bool IsQuoteUnpaidFlight2Chg { get { return _IsQuoteUnpaidFlight2Chg; } }
        public bool IsQuoteUnpaidFlightChg { get { return _IsQuoteUnpaidFlightChg; } }
        public bool IsQuote2PrepayPrint { get { return _IsQuote2PrepayPrint; } }
        public bool IsQuotePrepayPrint { get { return _IsQuotePrepayPrint; } }
        public bool IsQuoteRemaining2AmtPrint { get { return _IsQuoteRemaining2AmtPrint; } }
        public bool IsQuoteRemainingAmtPrint { get { return _IsQuoteRemainingAmtPrint; } }
        public bool IsQuoteDeactivateAutoUpdateRates { get { return _IsQuoteDeactivateAutoUpdateRates; } }
        public bool CorpReqExcChgWithinWeekend { get { return _CorpReqExcChgWithinWeekend; } }
        public bool IsAutomaticCalcLegTimes { get { return _IsAutomaticCalcLegTimes; } }
        public bool IsCorpReqAllowLogUpdCateringInfo { get { return _IsCorpReqAllowLogUpdCateringInfo; } }
        public bool IsCorpReqAllowLogUpdCrewInfo { get { return _IsCorpReqAllowLogUpdCrewInfo; } }
        public bool IsCorpReqAllowLogUpdFBOInfo { get { return _IsCorpReqAllowLogUpdFBOInfo; } }
        public bool IsCorpReqAllowLogUpdPaxInfo { get { return _IsCorpReqAllowLogUpdPaxInfo; } }
        public bool IsCorpReqActivateApproval { get { return _IsCorpReqActivateApproval; } }
        public bool IsCorpReqOverrideSubmittoChgQueue { get { return _IsCorpReqOverrideSubmittoChgQueue; } }
        public bool IsQuoteTaxDiscount { get { return _IsQuoteTaxDiscount; } }
        public bool IsTransWaitListPassengerfrmPreflightPostflight { get { return _IsTransWaitListPassengerfrmPreflightPostflight; } }
        public bool IsQuoteEdit { get { return _IsQuoteEdit; } }
        public bool IsAutoRON { get { return _IsAutoRON; } }
        public bool IsAutoRollover { get { return _IsAutoRollover; } }
        public bool IsQuoteWarningMessage { get { return _IsQuoteWarningMessage; } }
        public bool IsInActive { get { return _IsInActive; } }
        public bool IsChecklistAssign { get { return _IsChecklistAssign; } }
        public bool IsLogoUpload { get { return _IsLogoUpload; } }
        public bool IsSLeg { get { return _IsSLeg; } }
        public bool IsVLeg { get { return _IsVLeg; } }
        public bool IsSConflict { get { return _IsSConflict; } }
        public bool IsVConflict { get { return _IsVConflict; } }
        public bool IsSChecklist { get { return _IsSChecklist; } }
        public bool IsVChecklist { get { return _IsVChecklist; } }
        public bool IsSCrewCurrency { get { return _IsSCrewCurrency; } }
        public bool IsVCrewCurrency { get { return _IsVCrewCurrency; } }
        public bool IsSPassenger { get { return _IsSPassenger; } }
        public bool IsVPassenger { get { return _IsVPassenger; } }
        public bool IsBlockSeats { get { return _IsBlockSeats; } }
        public bool IsDepartAuthDuringReqSelection { get { return _IsDepartAuthDuringReqSelection; } }
        public bool IsNonLogTripSheetIncludeCrewHIST { get { return _IsNonLogTripSheetIncludeCrewHIST; } }
        public bool IsAutoValidateTripManager { get { return _IsAutoValidateTripManager; } }
        public bool IsQuoteFirstPage { get { return _IsQuoteFirstPage; } }
        public bool IFirstPG { get { return _IFirstPG; } }
        public bool IsAutoCreateCrewAvailInfo { get { return _IsAutoCreateCrewAvailInfo; } }
        public bool IsAutoPassenger { get { return _IsAutoPassenger; } }
        public bool IsAutoCrew { get { return _IsAutoCrew; } }
        public bool IsReqOnly { get { return _IsReqOnly; } }
        public bool IsCanPass { get { return _IsCanPass; } }
        public bool IsAutoDispat { get { return _IsAutoDispat; } }
        public bool IsAutoRevisionNum { get { return _IsAutoRevisionNum; } }
        public bool IsAutoPopulated { get { return _IsAutoPopulated; } }
        public bool IsOutlook { get { return _IsOutlook; } }
        public bool IsCharterQuoteFee3 { get { return _IsCharterQuoteFee3; } }
        public bool IsCharterQuoteFee4 { get { return _IsCharterQuoteFee4; } }
        public bool IsCharterQuote5 { get { return _IsCharterQuote5; } }
        public bool IsCOLFee3 { get { return _IsCOLFee3; } }
        public bool IsCOLFee4 { get { return _IsCOLFee4; } }
        public bool IsCOLFee5 { get { return _IsCOLFee5; } }
        public bool IsANotes { get { return _IsANotes; } }
        public bool IsCNotes { get { return _IsCNotes; } }
        public bool IsPNotes { get { return _IsPNotes; } }
        public bool IsOpenHistory { get { return _IsOpenHistory; } }
        public bool IsEndDutyClient { get { return _IsEndDutyClient; } }
        public bool IsBlackberrySupport { get { return _IsBlackberrySupport; } }
        public bool IsSSL { get { return _IsSSL; } }
        public bool IsAllBase { get { return _IsAllBase; } }
        public bool IsSIFLCalMessage { get { return _IsSIFLCalMessage; } }
        public bool IsCrewOlap { get { return _IsCrewOlap; } }
        public bool IsMaxCrew { get { return _IsMaxCrew; } }
        public bool IsTailInsuranceDue { get { return _IsTailInsuranceDue; } }
        public bool IsCrewQaulifiedFAR { get { return _IsCrewQaulifiedFAR; } }
        public bool IsTailBase { get { return _IsTailBase; } }
        public bool IsEnableTSA { get { return _IsEnableTSA; } }
        public bool IsScheduleDTTM { get { return _IsScheduleDTTM; } }
        public bool IsAPISSupport { get { return _IsAPISSupport; } }
        public bool IsPassengerOlap { get { return _IsPassengerOlap; } }
        public bool IsAutoFillAF { get { return _IsAutoFillAF; } }
        public bool IsAPISAlert { get { return _IsAPISAlert; } }
        public bool IsSTMPBlock { get { return _IsSTMPBlock; } }
        public bool IsEnableTSAPX { get { return _IsEnableTSAPX; } }
        public string TripMGRDefaultStatus { get { return _TripMGRDefaultStatus; } }
        public string SpecificationShort3 { get { return _SpecificationShort3; } }
        public string SpecificationShort4 { get { return _SpecificationShort4; } }
        public string FlightPurpose { get { return _FlightPurpose; } }
        public string HomebasePhoneNUM { get { return _HomebasePhoneNUM; } }
        public string FedAviationRegNum { get { return _FedAviationRegNum; } }
        public string CurrencySymbol { get { return _CurrencySymbol; } }
        public string LastUpdUID { get { return _LastUpdUID; } }
        public string UserID { get { return _UserID; } }
        public string FederalACCT { get { return _FederalACCT; } }
        public string ChtQouteDOMSegCHGACCT { get { return _ChtQouteDOMSegCHGACCT; } }
        public string HomebaseCD { get { return _HomebaseCD; } }
        public string FlightCatagoryPassenger { get { return _FlightCatagoryPassenger; } }
        public string FlightCatagoryPassengerNum { get { return _FlightCatagoryPassengerNum; } }
        public string MainFee { get { return _MainFee; } }
        public string CQCrewDuty { get { return _CQCrewDuty; } }
        public string CompanyName { get { return _CompanyName; } }
        public string CorpReqStartTM { get { return _CorpReqStartTM; } }
        public string TaxiTime { get { return _TaxiTime; } }
        public string AutoClimbTime { get { return _AutoClimbTime; } }
        public string QuoteSubtotal { get { return _QuoteSubtotal; } }
        public string QuoteTaxesDefault { get { return _QuoteTaxesDefault; } }
        public string DefaultInvoice { get { return _DefaultInvoice; } }
        public string QouteStdCrew2RONDeposit { get { return _QouteStdCrew2RONDeposit; } }
        public string QuoteAddition2CrewRON { get { return _QuoteAddition2CrewRON; } }
        public string uote2LandingFeeDefault { get { return _uote2LandingFeeDefault; } }
        public string QuoteWaiting2Charge { get { return _QuoteWaiting2Charge; } }
        public string Additional2FeeDefault { get { return _Additional2FeeDefault; } }
        public string Discount2AmountDeposit { get { return _Discount2AmountDeposit; } }
        public string Quote2Subtotal { get { return _Quote2Subtotal; } }
        public string Quote2TaxesDefault { get { return _Quote2TaxesDefault; } }
        public string QuoteInvoice2Default { get { return _QuoteInvoice2Default; } }
        public string QuotePrepayDefault { get { return _QuotePrepayDefault; } }
        public string QuoteRemaining2AmountDefault { get { return _QuoteRemaining2AmountDefault; } }
        public string QuoteRemainingAmountDefault { get { return _QuoteRemainingAmountDefault; } }
        public string TripsheetRptWriteGroup { get { return _TripsheetRptWriteGroup; } }
        public DateTime? ActiveDT { get { return _ActiveDT; } }
        public DateTime? BackupDT { get { return _BackupDT; } }
        public DateTime? LastUpdTS { get { return _LastUpdTS; } }
        public DateTime? AcceptDTTM { get { return _AcceptDTTM; } }
        public int? LogFixed { get { return _LogFixed; } }
        public int? LogRotary { get { return _LogRotary; } }
        public int? WindReliability { get { return _WindReliability; } }
        public int? Specdec3 { get { return _Specdec3; } }
        public int? Specdec4 { get { return _Specdec4; } }
        public int? QuoteLogoPosition { get { return _QuoteLogoPosition; } }
        public int? IlogoPOS { get { return _IlogoPOS; } }
        public int? TripsheetSearchBack { get { return _TripsheetSearchBack; } }
        public int? TripS { get { return _TripS; } }
        public int? LayoverHrsSIFL { get { return _LayoverHrsSIFL; } }
        public int? PassengerOFullName { get { return _PassengerOFullName; } }
        public int? PassengerOMiddle { get { return _PassengerOMiddle; } }
        public int? PassengerOLast { get { return _PassengerOLast; } }
        public int? PassengerOLastFirst { get { return _PassengerOLastFirst; } }
        public int? PassengerOLastMiddle { get { return _PassengerOLastMiddle; } }
        public int? PassengerOLastLast { get { return _PassengerOLastLast; } }
        public int? CrewOFullName { get { return _CrewOFullName; } }
        public int? CrewOMiddle { get { return _CrewOMiddle; } }
        public int? CrewOLast { get { return _CrewOLast; } }
        public int? CrewOLastFirst { get { return _CrewOLastFirst; } }
        public int? CrewOLastMiddle { get { return _CrewOLastMiddle; } }
        public int? CrewOLastLast { get { return _CrewOLastLast; } }
        public int? ReportWriteLogoPosition { get { return _ReportWriteLogoPosition; } }
        public int? Searchack { get { return _Searchack; } }
        public int? SearchBack { get { return _SearchBack; } }
        public decimal? DutyBasis { get { return _DutyBasis; } }
        public decimal? AircraftBasis { get { return _AircraftBasis; } }
        public decimal? FuelPurchase { get { return _FuelPurchase; } }
        public decimal? FuelBurnKiloLBSGallon { get { return _FuelBurnKiloLBSGallon; } }
        public decimal? IsAppliedTax { get { return _IsAppliedTax; } }
        public decimal? RptSize { get { return _RptSize; } }
        public decimal? CorpReqTMEntryFormat { get { return _CorpReqTMEntryFormat; } }
        public decimal? ScheduleServiceTimeFormat { get { return _ScheduleServiceTimeFormat; } }
        public decimal? SSHRFormat { get { return _SSHRFormat; } }
        public decimal? DayMonthFormat { get { return _DayMonthFormat; } }
        public decimal? ScheduledServicesSecurity { get { return _ScheduledServicesSecurity; } }
        public decimal? WebSecurityType { get { return _WebSecurityType; } }
        public decimal? QuoteIDTYPE { get { return _QuoteIDTYPE; } }
        public decimal? TimeDisplayTenMin { get { return _TimeDisplayTenMin; } }
        public decimal? HoursMinutesCONV { get { return _HoursMinutesCONV; } }
        public decimal? ElapseTMRounding { get { return _ElapseTMRounding; } }
        public decimal? ServerRPF { get { return _ServerRPF; } }
        public decimal? ImageCnt { get { return _ImageCnt; } }
        public decimal? ImagePosition { get { return _ImagePosition; } }
        public decimal? AircraftBlockFlight { get { return _AircraftBlockFlight; } }
        public decimal? ServerReport { get { return _ServerReport; } }
        public decimal? ChtQouteDOMSegCHG { get { return _ChtQouteDOMSegCHG; } }
        public decimal? ChtQouteIntlSegCHG { get { return _ChtQouteIntlSegCHG; } }
        public decimal? SegmentFeeAlaska { get { return _SegmentFeeAlaska; } }
        public decimal? SegementFeeHawaii { get { return _SegementFeeHawaii; } }
        public decimal? QuotePageOffsettingInvoice { get { return _QuotePageOffsettingInvoice; } }
        public decimal? QuotePageOff { get { return _QuotePageOff; } }
        public decimal? MinuteHoursRON { get { return _MinuteHoursRON; } }
        public decimal? RestDaysMinimum { get { return _RestDaysMinimum; } }
        public decimal? RestCalendarQTRMinimum { get { return _RestCalendarQTRMinimum; } }
        public decimal? FiscalYRStart { get { return _FiscalYRStart; } }
        public decimal? FiscalYREnd { get { return _FiscalYREnd; } }
        public decimal? PasswordValidDays { get { return _PasswordValidDays; } }
        public decimal? PasswordHistoryNum { get { return _PasswordHistoryNum; } }
        public decimal? TripsheetDTWarning { get { return _TripsheetDTWarning; } }
        public decimal? ScheduleServiceListCTDetail { get { return _ScheduleServiceListCTDetail; } }
        public decimal? RowsOnPageCnt { get { return _RowsOnPageCnt; } }
        public decimal? DayLanding { get { return _DayLanding; } }
        public decimal? DayLandingMinimum { get { return _DayLandingMinimum; } }
        public decimal? NightLanding { get { return _NightLanding; } }
        public decimal? NightllandingMinimum { get { return _NightllandingMinimum; } }
        public decimal? Approach { get { return _Approach; } }
        public decimal? ApproachMinimum { get { return _ApproachMinimum; } }
        public decimal? Instrument { get { return _Instrument; } }
        public decimal? InstrumentMinimum { get { return _InstrumentMinimum; } }
        public decimal? TakeoffDay { get { return _TakeoffDay; } }
        public decimal? TakeoffDayMinimum { get { return _TakeoffDayMinimum; } }
        public decimal? TakeoffNight { get { return _TakeoffNight; } }
        public decimal? TakeofNightMinimum { get { return _TakeofNightMinimum; } }
        public decimal? Day7 { get { return _Day7; } }
        public decimal? Day30 { get { return _Day30; } }
        public decimal? Day90 { get { return _Day90; } }
        public decimal? Month6 { get { return _Month6; } }
        public decimal? Month12 { get { return _Month12; } }
        public decimal? Day365 { get { return _Day365; } }
        public decimal? RestDays { get { return _RestDays; } }
        public decimal? CQMessageCD { get { return _CQMessageCD; } }
        public decimal? IVMessageCD { get { return _IVMessageCD; } }
        public decimal? RefreshTM { get { return _RefreshTM; } }
        public decimal? SearchGridTM { get { return _SearchGridTM; } }
        public decimal? CorpReqMinimumRunwayLength { get { return _CorpReqMinimumRunwayLength; } }
        public decimal? CorpReqChgQueueTimer { get { return _CorpReqChgQueueTimer; } }
        public decimal? AugmentCrewPercentage { get { return _AugmentCrewPercentage; } }
        public decimal? GroundTM { get { return _GroundTM; } }
        public decimal? CQFederalTax { get { return _CQFederalTax; } }
        public decimal? RuralTax { get { return _RuralTax; } }
        public decimal? GroundTMIntl { get { return _GroundTMIntl; } }
        public decimal? CorpReqWeeklyCalTimer { get { return _CorpReqWeeklyCalTimer; } }
        public decimal? FleetCalendar5DayTimer { get { return _FleetCalendar5DayTimer; } }
        public decimal? Crew5DayTimer { get { return _Crew5DayTimer; } }
        public decimal? CorpReqExcChgWithinHours { get { return _CorpReqExcChgWithinHours; } }
        public decimal? DayMaximum7 { get { return _DayMaximum7; } }
        public decimal? DayMaximum30 { get { return _DayMaximum30; } }
        public decimal? CalendarMonthMaximum { get { return _CalendarMonthMaximum; } }
        public decimal? DayMaximum90 { get { return _DayMaximum90; } }
        public decimal? CalendarQTRMaximum { get { return _CalendarQTRMaximum; } }
        public decimal? MonthMaximum6 { get { return _MonthMaximum6; } }
        public decimal? Previous2QTRMaximum { get { return _Previous2QTRMaximum; } }
        public decimal? MonthMaximum12 { get { return _MonthMaximum12; } }
        public decimal? CalendarYearMaximum { get { return _CalendarYearMaximum; } }
        public decimal? DayMaximum365 { get { return _DayMaximum365; } }
        public string UserNameCBP { get { return _UserNameCBP; } }
        public string UserPasswordCBP { get { return _UserPasswordCBP; } }
        public string ApplicationMessage { get { return _ApplicationMessage; } }
        public string ChtQouteHeaderDetailRpt { get { return _ChtQouteHeaderDetailRpt; } }
        public string ChtQouteFooterDetailRpt { get { return _ChtQouteFooterDetailRpt; } }
        public string ChtQuoteCompany { get { return _ChtQuoteCompany; } }
        public string ChtQuoteCompanyNameINV { get { return _ChtQuoteCompanyNameINV; } }
        public string ChtQuoteHeaderINV { get { return _ChtQuoteHeaderINV; } }
        public string ChtQuoteFooterINV { get { return _ChtQuoteFooterINV; } }
        public string HTMLHeader { get { return _HTMLHeader; } }
        public string HTMLFooter { get { return _HTMLFooter; } }
        public string HTMLBackgroun { get { return _HTMLBackgroun; } }
        public string WebPageBackgroundColor { get { return _WebPageBackgroundColor; } }
        public string HeardTextRpt { get { return _HeardTextRpt; } }
        public string FooterTextRpt { get { return _FooterTextRpt; } }
        public string MenuBackground { get { return _MenuBackground; } }
        public string MenuHeader { get { return _MenuHeader; } }
        public string MenuFooter { get { return _MenuFooter; } }
        public string SSBACKGRND { get { return _SSBACKGRND; } }
        public string SSHEADER { get { return _SSHEADER; } }
        public string SSFOOTER { get { return _SSFOOTER; } }
        public string WebBackground { get { return _WebBackground; } }
        public string WebHeader { get { return _WebHeader; } }
        public string WebFooter { get { return _WebFooter; } }
        public string QuoteLogo { get { return _QuoteLogo; } }
        public string Ilogo { get { return _Ilogo; } }
        public string TripsheetTabColor { get { return _TripsheetTabColor; } }
        public string TripSheetLogoPosition { get { return _TripSheetLogoPosition; } }
        public string PrivateTrip { get { return _PrivateTrip; } }
        public string ReportWriteLogo { get { return _ReportWriteLogo; } }
        public string RptTabOutbdInstLab1 { get { return _RptTabOutbdInstLab1; } }
        public string RptTabOutbdInstLab2 { get { return _RptTabOutbdInstLab2; } }
        public string RptTabOutbdInstLab3 { get { return _RptTabOutbdInstLab3; } }
        public string RptTabOutbdInstLab4 { get { return _RptTabOutbdInstLab4; } }
        public string RptTabOutbdInstLab5 { get { return _RptTabOutbdInstLab5; } }
        public string RptTabOutbdInstLab6 { get { return _RptTabOutbdInstLab6; } }
        public string RptTabOutbdInstLab7 { get { return _RptTabOutbdInstLab7; } }
        public string CrewLogCustomLBLShort1 { get { return _CrewLogCustomLBLShort1; } }
        public string CrewLogCustomLBLShort2 { get { return _CrewLogCustomLBLShort2; } }
        public string UserPassword { get { return _UserPassword; } }
        public string ServerName { get { return _ServerName; } }
        public string UserEmail { get { return _UserEmail; } }
        public string XMLPath { get { return _XMLPath; } }
        public string StdCrewRON { get { return _StdCrewRON; } }
        public string AdditionalCrewCD { get { return _AdditionalCrewCD; } }
        public string AdditionalCrewRON { get { return _AdditionalCrewRON; } }
        public string WaitTM { get { return _WaitTM; } }
        public string LandingFee { get { return _LandingFee; } }
        public string ApplicationDateFormat { get { return String.IsNullOrEmpty(_ApplicationDateFormat) ? "MM/dd/yyyy" : _ApplicationDateFormat; } }
        public string CrewLogCustomLBLLong1 { get { return _CrewLogCustomLBLLong1; } }
        public string CrewLogCustomLBLLong2 { get { return _CrewLogCustomLBLLong2; } }
        public string SpecificationLong3 { get { return _SpecificationLong3; } }
        public string SpecificationLong4 { get { return _SpecificationLong4; } }
        public string GeneralAviationDesk { get { return _GeneralAviationDesk; } }
        public string BaseFax { get { return _BaseFax; } }
        public string CanPassNum { get { return _CanPassNum; } }
        public string Tsense { get { return _Tsense; } }
        public string APISUrl { get { return _APISUrl; } }
        public string LoginUrl { get { return _LoginUrl; } }
        public string BaseDescription { get { return _BaseDescription; } }
        public string AccountFormat { get { return _AccountFormat; } }
        public string FederalTax { get { return _FederalTax; } }
        public string StateTax { get { return _StateTax; } }
        public string SaleTax { get { return _SaleTax; } }
        public string RuralAccountNum { get { return _RuralAccountNum; } }
        public string ChtQouteIntlSegCHGACCT { get { return _ChtQouteIntlSegCHGACCT; } }
        public string SeqmentAircraftAlaska { get { return _SeqmentAircraftAlaska; } }
        public string SegementAircraftHawaii { get { return _SegementAircraftHawaii; } }
        public string Domain { get { return _Domain; } }
        public string QuoteMinimumUseFeeDepositAmt { get { return _QuoteMinimumUseFeeDepositAmt; } }
        public string DepostFlightCharge { get { return _DepostFlightCharge; } }
        public string QuoteSegmentFeeDeposit { get { return _QuoteSegmentFeeDeposit; } }
        public string QuoteAdditonCrewDeposit { get { return _QuoteAdditonCrewDeposit; } }
        public string QuoteStdCrewRONDeposit { get { return _QuoteStdCrewRONDeposit; } }
        public string QuoteAdditionCrewRON { get { return _QuoteAdditionCrewRON; } }
        public string QuoteLandingFeeDefault { get { return _QuoteLandingFeeDefault; } }
        public string QuoteWaitingCharge { get { return _QuoteWaitingCharge; } }
        public string QuoteAdditionalFeeDefault { get { return _QuoteAdditionalFeeDefault; } }
        public string DiscountAmountDefault { get { return _DiscountAmountDefault; } }
        public string QuoteMinimumUse2FeeDepositAdj { get { return _QuoteMinimumUse2FeeDepositAdj; } }
        public string Deposit2FlightCharge { get { return _Deposit2FlightCharge; } }
        public string QuoteSegment2FeeDeposit { get { return _QuoteSegment2FeeDeposit; } }
        public string QuoteAddition2CrewDeposit { get { return _QuoteAddition2CrewDeposit; } }
        public string QuoteTitle { get { return _QuoteTitle; } }
        public string QuotePrepay2Default { get { return _QuotePrepay2Default; } }
        public string CorpAccountFormat { get { return _CorpAccountFormat; } }
        public string CustomRptMessage { get { return _CustomRptMessage; } }
        public bool IsMiles { get { return _IsMiles; } }
        public bool IsKilometer { get { return _IsKilometer; } }
        public decimal CrewChecklistAlertDays { get { return _CrewChecklistAlertDays; } }
        public decimal BusinessWeekStart { get { return _BusinessWeekStart; } }
        public int? CRSearchBack { get { return _CRSearchBack; } }
        public bool IsPODepartPercentage { get { return _IsPODepartPercentage; } }
        public bool IsDefaultTripDepartureDate { get { return _IsDefaultTripDepartureDate; } }

        #endregion

        #region Constructor
        public FPSettings(Guid SessionId)
        {

            using (Framework.Data.FrameworkModelContainer container = new Data.FrameworkModelContainer())
            {
                var returnValue = container.CheckLogin(SessionId).FirstOrDefault();
                _isAuthenticated = returnValue.HasValue ? returnValue.Value > 0 : false;
                if (_isAuthenticated)
                {
                    var resultSet = container.GetUserSettings(SessionId).FirstOrDefault();
                    if (resultSet != null)
                    {
                        #region Assign result set values to properties
                        this._sessionID = SessionId;
                        this._HomebaseID = resultSet.HomebaseID;
                        this._CustomerID = resultSet.CustomerID;
                        this._ClientID = resultSet.ClientID;
                        this._CrewDutyID = resultSet.CrewDutyID;
                        this._DefaultFlightCatID = resultSet.DefaultFlightCatID;
                        this._DefaultCheckListGroupID = resultSet.DefaultCheckListGroupID;
                        this._DepartmentID = resultSet.DepartmentID;
                        this._ScheduleServiceWaitListID = resultSet.ScheduleServiceWaitListID;
                        this._IsDisplayNoteFlag = resultSet.IsDisplayNoteFlag.GetValueOrDefault();
                        this._IsReportCrewDuty = resultSet.IsReportCrewDuty.GetValueOrDefault();
                        this._IsZeroSuppressActivityAircftRpt = resultSet.IsZeroSuppressActivityAircftRpt.GetValueOrDefault();
                        this._IsZeroSuppressActivityCrewRpt = resultSet.IsZeroSuppressActivityCrewRpt.GetValueOrDefault();
                        this._IsZeroSuppressActivityDeptRpt = resultSet.IsZeroSuppressActivityDeptRpt.GetValueOrDefault();
                        this._IsZeroSuppressActivityPassengerRpt = resultSet.IsZeroSuppressActivityPassengerRpt.GetValueOrDefault();
                        this._IsAutomaticDispatchNum = resultSet.IsAutomaticDispatchNum.GetValueOrDefault();
                        this._IsAutomaticCommentTrips = resultSet.IsAutomaticCommentTrips.GetValueOrDefault();
                        this._IsConflictCheck = resultSet.IsConflictCheck.GetValueOrDefault();
                        this._IsTimeStampRpt = resultSet.IsTimeStampRpt.GetValueOrDefault();
                        this._IsTripsheetView = resultSet.IsTripsheetView.GetValueOrDefault();
                        this._IsTextAutoTab = resultSet.IsTextAutoTab.GetValueOrDefault();
                        this._IsChecklistWarning = resultSet.IsChecklistWarning.GetValueOrDefault();
                        this._IsLogsheetWarning = resultSet.IsLogsheetWarning.GetValueOrDefault();
                        this._IsDeactivateHomeBaseFilter = resultSet.IsDeactivateHomeBaseFilter.GetValueOrDefault();
                        this._IsInvoiceDTsystemDT = resultSet.IsInvoiceDTsystemDT.GetValueOrDefault();
                        this._IsQuoteDetailPrint = resultSet.IsQuoteDetailPrint.GetValueOrDefault();
                        this._isQuotePrintFees = resultSet.isQuotePrintFees.GetValueOrDefault();
                        this._IsQuotePrintSum = resultSet.IsQuotePrintSum.GetValueOrDefault();
                        this._IsQuotePrintSubtotal = resultSet.IsQuotePrintSubtotal.GetValueOrDefault();
                        this._IsCityDescription = resultSet.IsCityDescription.GetValueOrDefault();
                        this._IsAirportDescription = resultSet.IsAirportDescription.GetValueOrDefault();
                        this._IsQuoteICAODescription = resultSet.IsQuoteICAODescription.GetValueOrDefault();
                        this._IsQuotePrepaidMinimUsageFeeAmt = resultSet.IsQuotePrepaidMinimUsageFeeAmt.GetValueOrDefault();
                        this._IsQuoteFlightChargePrepaid = resultSet.IsQuoteFlightChargePrepaid.GetValueOrDefault();
                        this._IsQuotePrepaidSegementFee = resultSet.IsQuotePrepaidSegementFee.GetValueOrDefault();
                        this._IsQuoteAdditionalCrewPrepaid = resultSet.IsQuoteAdditionalCrewPrepaid.GetValueOrDefault();
                        this._IsQuoteStdCrewRonPrepaid = resultSet.IsQuoteStdCrewRonPrepaid.GetValueOrDefault();
                        this._IsQuoteAdditonalCrewRON = resultSet.IsQuoteAdditonalCrewRON.GetValueOrDefault();
                        this._IsQuoteFlightLandingFeePrint = resultSet.IsQuoteFlightLandingFeePrint.GetValueOrDefault();
                        this._IsQuoteWaitingChargePrepaid = resultSet.IsQuoteWaitingChargePrepaid.GetValueOrDefault();
                        this._IsQuotePrintAdditionalFee = resultSet.IsQuotePrintAdditionalFee.GetValueOrDefault();
                        this._IsQuoteDispcountAmtPrint = resultSet.IsQuoteDispcountAmtPrint.GetValueOrDefault();
                        this._IsQuoteSubtotalPrint = resultSet.IsQuoteSubtotalPrint.GetValueOrDefault();
                        this._IsQuoteTaxesPrint = resultSet.IsQuoteTaxesPrint.GetValueOrDefault();
                        this._IsQuoteInvoicePrint = resultSet.IsQuoteInvoicePrint.GetValueOrDefault();
                        this._IsInvoiceRptLegNum = resultSet.IsInvoiceRptLegNum.GetValueOrDefault();
                        this._IsInvoiceRptDetailDT = resultSet.IsInvoiceRptDetailDT.GetValueOrDefault();
                        this._IsInvoiceRptFromDescription = resultSet.IsInvoiceRptFromDescription.GetValueOrDefault();
                        this._IsQuoteDetailRptToDescription = resultSet.IsQuoteDetailRptToDescription.GetValueOrDefault();
                        this._IsQuoteDetailInvoiceAmt = resultSet.IsQuoteDetailInvoiceAmt.GetValueOrDefault();
                        this._IsQuoteDetailFlightHours = resultSet.IsQuoteDetailFlightHours.GetValueOrDefault();
                        this._IsQuoteDetailMiles = resultSet.IsQuoteDetailMiles.GetValueOrDefault();
                        this._IsQuoteDetailPassengerCnt = resultSet.IsQuoteDetailPassengerCnt.GetValueOrDefault();
                        this._IsQuoteFeelColumnDescription = resultSet.IsQuoteFeelColumnDescription.GetValueOrDefault();
                        this._IsQuoteFeelColumnInvoiceAmt = resultSet.IsQuoteFeelColumnInvoiceAmt.GetValueOrDefault();
                        this._IsQuoteDispatch = resultSet.IsQuoteDispatch.GetValueOrDefault();
                        this._IsQuoteInformation = resultSet.IsQuoteInformation.GetValueOrDefault();
                        this._IsCompanyName = resultSet.IsCompanyName.GetValueOrDefault();
                        this._IsChangeQueueDetail = resultSet.IsChangeQueueDetail.GetValueOrDefault();
                        this._IsQuoteChangeQueueFees = resultSet.IsQuoteChangeQueueFees.GetValueOrDefault();
                        this._IsQuoteChangeQueueSum = resultSet.IsQuoteChangeQueueSum.GetValueOrDefault();
                        this._IsQuoteChangeQueueSubtotal = resultSet.IsQuoteChangeQueueSubtotal.GetValueOrDefault();
                        this._IsChangeQueueCity = resultSet.IsChangeQueueCity.GetValueOrDefault();
                        this._IsChangeQueueAirport = resultSet.IsChangeQueueAirport.GetValueOrDefault();
                        this._IsQuoteChangeQueueICAO = resultSet.IsQuoteChangeQueueICAO.GetValueOrDefault();
                        this._IsQuotePrepaidMinimumUsage2FeeAdj = resultSet.IsQuotePrepaidMinimumUsage2FeeAdj.GetValueOrDefault();
                        this._IsQuotePrepaidMin2UsageFeeAdj = resultSet.IsQuotePrepaidMin2UsageFeeAdj.GetValueOrDefault();
                        this._IsQuoteFlight2ChargePrepaid = resultSet.IsQuoteFlight2ChargePrepaid.GetValueOrDefault();
                        this._IsQuotePrepaidSegement2Fee = resultSet.IsQuotePrepaidSegement2Fee.GetValueOrDefault();
                        this._IsQuoteAdditional2CrewPrepaid = resultSet.IsQuoteAdditional2CrewPrepaid.GetValueOrDefault();
                        this._IsQuoteUnpaidAdditional2Crew = resultSet.IsQuoteUnpaidAdditional2Crew.GetValueOrDefault();
                        this._IsQuoteStdCrew2RONPrepaid = resultSet.IsQuoteStdCrew2RONPrepaid.GetValueOrDefault();
                        this._IsQuoteUnpaidStd2CrewRON = resultSet.IsQuoteUnpaidStd2CrewRON.GetValueOrDefault();
                        this._IsQuoteAdditional2CrewRON = resultSet.IsQuoteAdditional2CrewRON.GetValueOrDefault();
                        this._IsQuoteUpaidAdditionl2CrewRON = resultSet.IsQuoteUpaidAdditionl2CrewRON.GetValueOrDefault();
                        this._IsQuoteFlight2LandingFeePrint = resultSet.IsQuoteFlight2LandingFeePrint.GetValueOrDefault();
                        this._IsQuoteWaiting2ChargePrepaid = resultSet.IsQuoteWaiting2ChargePrepaid.GetValueOrDefault();
                        this._IsQuoteUpaid2WaitingChg = resultSet.IsQuoteUpaid2WaitingChg.GetValueOrDefault();
                        this._IsQuoteAdditional2FeePrint = resultSet.IsQuoteAdditional2FeePrint.GetValueOrDefault();
                        this._IsQuoteDiscount2AmountPrepaid = resultSet.IsQuoteDiscount2AmountPrepaid.GetValueOrDefault();
                        this._IsQuoteUnpaid2DiscountAmt = resultSet.IsQuoteUnpaid2DiscountAmt.GetValueOrDefault();
                        this._IsQuoteSubtotal2Print = resultSet.IsQuoteSubtotal2Print.GetValueOrDefault();
                        this._IsQuoteTaxes2Print = resultSet.IsQuoteTaxes2Print.GetValueOrDefault();
                        this._IsQuoteInvoice2Print = resultSet.IsQuoteInvoice2Print.GetValueOrDefault();
                        this._IsRptQuoteDetailLegNum = resultSet.IsRptQuoteDetailLegNum.GetValueOrDefault();
                        this._IsRptQuoteDetailDepartureDT = resultSet.IsRptQuoteDetailDepartureDT.GetValueOrDefault();
                        this._IsRptQuoteDetailFromDescription = resultSet.IsRptQuoteDetailFromDescription.GetValueOrDefault();
                        this._IsRptQuoteDetailToDescription = resultSet.IsRptQuoteDetailToDescription.GetValueOrDefault();
                        this._IsRptQuoteDetailQuotedAmt = resultSet.IsRptQuoteDetailQuotedAmt.GetValueOrDefault();
                        this._IsRptQuoteDetailFlightHours = resultSet.IsRptQuoteDetailFlightHours.GetValueOrDefault();
                        this._IsRptQuoteDetailMiles = resultSet.IsRptQuoteDetailMiles.GetValueOrDefault();
                        this._IsRptQuoteDetailPassengerCnt = resultSet.IsRptQuoteDetailPassengerCnt.GetValueOrDefault();
                        this._IsRptQuoteFeeDescription = resultSet.IsRptQuoteFeeDescription.GetValueOrDefault();
                        this._IsRptQuoteFeeQuoteAmt = resultSet.IsRptQuoteFeeQuoteAmt.GetValueOrDefault();
                        this._IsQuoteInformation2 = resultSet.IsQuoteInformation2.GetValueOrDefault();
                        this._IsCompanyName2 = resultSet.IsCompanyName2.GetValueOrDefault();
                        this._QuoteDescription = resultSet.QuoteDescription.GetValueOrDefault();
                        this._IsQuoteSales = resultSet.IsQuoteSales.GetValueOrDefault();
                        this._IsQuoteTailNum = resultSet.IsQuoteTailNum.GetValueOrDefault();
                        this._IsQuoteTypeCD = resultSet.IsQuoteTypeCD.GetValueOrDefault();
                        this._IsCorpReqActivateAlert = resultSet.IsCorpReqActivateAlert.GetValueOrDefault();
                        this._IsQuoteUnpaidFlight2Chg = resultSet.IsQuoteUnpaidFlight2Chg.GetValueOrDefault();
                        this._IsQuoteUnpaidFlightChg = resultSet.IsQuoteUnpaidFlightChg.GetValueOrDefault();
                        this._IsQuote2PrepayPrint = resultSet.IsQuote2PrepayPrint.GetValueOrDefault();
                        this._IsQuotePrepayPrint = resultSet.IsQuotePrepayPrint.GetValueOrDefault();
                        this._IsQuoteRemaining2AmtPrint = resultSet.IsQuoteRemaining2AmtPrint.GetValueOrDefault();
                        this._IsQuoteRemainingAmtPrint = resultSet.IsQuoteRemainingAmtPrint.GetValueOrDefault();
                        this._IsQuoteDeactivateAutoUpdateRates = resultSet.IsQuoteDeactivateAutoUpdateRates.GetValueOrDefault();
                        this._CorpReqExcChgWithinWeekend = resultSet.CorpReqExcChgWithinWeekend.GetValueOrDefault();
                        this._IsAutomaticCalcLegTimes = resultSet.IsAutomaticCalcLegTimes.GetValueOrDefault();
                        this._IsCorpReqAllowLogUpdCateringInfo = resultSet.IsCorpReqAllowLogUpdCateringInfo.GetValueOrDefault();
                        this._IsCorpReqAllowLogUpdCrewInfo = resultSet.IsCorpReqAllowLogUpdCrewInfo.GetValueOrDefault();
                        this._IsCorpReqAllowLogUpdFBOInfo = resultSet.IsCorpReqAllowLogUpdFBOInfo.GetValueOrDefault();
                        this._IsCorpReqAllowLogUpdPaxInfo = resultSet.IsCorpReqAllowLogUpdPaxInfo.GetValueOrDefault();
                        this._IsCorpReqActivateApproval = resultSet.IsCorpReqActivateApproval.GetValueOrDefault();
                        this._IsCorpReqOverrideSubmittoChgQueue = resultSet.IsCorpReqOverrideSubmittoChgQueue.GetValueOrDefault();
                        this._IsQuoteTaxDiscount = resultSet.IsQuoteTaxDiscount.GetValueOrDefault();
                        this._IsTransWaitListPassengerfrmPreflightPostflight = resultSet.IsTransWaitListPassengerfrmPreflightPostflight.GetValueOrDefault();
                        this._IsQuoteEdit = resultSet.IsQuoteEdit.GetValueOrDefault();
                        this._IsAutoRON = resultSet.IsAutoRON.GetValueOrDefault();
                        this._IsAutoRollover = resultSet.IsAutoRollover.GetValueOrDefault();
                        this._IsQuoteWarningMessage = resultSet.IsQuoteWarningMessage.GetValueOrDefault();
                        this._IsInActive = resultSet.IsInActive.GetValueOrDefault();
                        this._IsChecklistAssign = resultSet.IsChecklistAssign.GetValueOrDefault();
                        this._IsLogoUpload = resultSet.IsLogoUpload.GetValueOrDefault();
                        this._IsSLeg = resultSet.IsSLeg.GetValueOrDefault();
                        this._IsVLeg = resultSet.IsVLeg.GetValueOrDefault();
                        this._IsSConflict = resultSet.IsSConflict.GetValueOrDefault();
                        this._IsVConflict = resultSet.IsVConflict.GetValueOrDefault();
                        this._IsSChecklist = resultSet.IsSChecklist.GetValueOrDefault();
                        this._IsVChecklist = resultSet.IsVChecklist.GetValueOrDefault();
                        this._IsSCrewCurrency = resultSet.IsSCrewCurrency.GetValueOrDefault();
                        this._IsVCrewCurrency = resultSet.IsVCrewCurrency.GetValueOrDefault();
                        this._IsSPassenger = resultSet.IsSPassenger.GetValueOrDefault();
                        this._IsVPassenger = resultSet.IsVPassenger.GetValueOrDefault();
                        this._IsBlockSeats = resultSet.IsBlockSeats.GetValueOrDefault();
                        this._IsDepartAuthDuringReqSelection = resultSet.IsDepartAuthDuringReqSelection.GetValueOrDefault();
                        this._IsNonLogTripSheetIncludeCrewHIST = resultSet.IsNonLogTripSheetIncludeCrewHIST.GetValueOrDefault();
                        this._IsAutoValidateTripManager = resultSet.IsAutoValidateTripManager.GetValueOrDefault();
                        this._IsQuoteFirstPage = resultSet.IsQuoteFirstPage.GetValueOrDefault();
                        this._IFirstPG = resultSet.IFirstPG.GetValueOrDefault();
                        this._IsAutoCreateCrewAvailInfo = resultSet.IsAutoCreateCrewAvailInfo.GetValueOrDefault();
                        this._IsAutoPassenger = resultSet.IsAutoPassenger.GetValueOrDefault();
                        this._IsAutoCrew = resultSet.IsAutoCrew.GetValueOrDefault();
                        this._IsReqOnly = resultSet.IsReqOnly.GetValueOrDefault();
                        this._IsCanPass = resultSet.IsCanPass.GetValueOrDefault();
                        this._IsAutoDispat = resultSet.IsAutoDispat.GetValueOrDefault();
                        this._IsAutoRevisionNum = resultSet.IsAutoRevisionNum.GetValueOrDefault();
                        this._IsAutoPopulated = resultSet.IsAutoPopulated.GetValueOrDefault();
                        this._IsOutlook = resultSet.IsOutlook.GetValueOrDefault();
                        this._IsCharterQuoteFee3 = resultSet.IsCharterQuoteFee3.GetValueOrDefault();
                        this._IsCharterQuoteFee4 = resultSet.IsCharterQuoteFee4.GetValueOrDefault();
                        this._IsCharterQuote5 = resultSet.IsCharterQuote5.GetValueOrDefault();
                        this._IsCOLFee3 = resultSet.IsCOLFee3.GetValueOrDefault();
                        this._IsCOLFee4 = resultSet.IsCOLFee4.GetValueOrDefault();
                        this._IsCOLFee5 = resultSet.IsCOLFee5.GetValueOrDefault();
                        this._IsANotes = resultSet.IsANotes.GetValueOrDefault();
                        this._IsCNotes = resultSet.IsCNotes.GetValueOrDefault();
                        this._IsPNotes = resultSet.IsPNotes.GetValueOrDefault();
                        this._IsOpenHistory = resultSet.IsOpenHistory.GetValueOrDefault();
                        this._IsEndDutyClient = resultSet.IsEndDutyClient.GetValueOrDefault();
                        this._IsBlackberrySupport = resultSet.IsBlackberrySupport.GetValueOrDefault();
                        this._IsSSL = resultSet.IsSSL.GetValueOrDefault();
                        this._IsAllBase = resultSet.IsAllBase.GetValueOrDefault();
                        this._IsSIFLCalMessage = resultSet.IsSIFLCalMessage.GetValueOrDefault();
                        this._IsCrewOlap = resultSet.IsCrewOlap.GetValueOrDefault();
                        this._IsMaxCrew = resultSet.IsMaxCrew.GetValueOrDefault();
                        this._IsTailInsuranceDue = resultSet.IsTailInsuranceDue.GetValueOrDefault();
                        this._IsCrewQaulifiedFAR = resultSet.IsCrewQaulifiedFAR.GetValueOrDefault();
                        this._IsTailBase = resultSet.IsTailBase.GetValueOrDefault();
                        this._IsEnableTSA = resultSet.IsEnableTSA.GetValueOrDefault();
                        this._IsScheduleDTTM = resultSet.IsScheduleDTTM.GetValueOrDefault();
                        this._IsAPISSupport = resultSet.IsAPISSupport.GetValueOrDefault();
                        this._IsPassengerOlap = resultSet.IsPassengerOlap.GetValueOrDefault();
                        this._IsAutoFillAF = resultSet.IsAutoFillAF.GetValueOrDefault();
                        this._IsAPISAlert = resultSet.IsAPISAlert.GetValueOrDefault();
                        this._IsSTMPBlock = resultSet.IsSTMPBlock.GetValueOrDefault();
                        this._IsEnableTSAPX = resultSet.IsEnableTSAPX.GetValueOrDefault();
                        this._TripMGRDefaultStatus = resultSet.TripMGRDefaultStatus;
                        this._SpecificationShort3 = resultSet.SpecificationShort3;
                        this._SpecificationShort4 = resultSet.SpecificationShort4;
                        this._FlightPurpose = resultSet.FlightPurpose;
                        this._HomebasePhoneNUM = resultSet.HomebasePhoneNUM;
                        this._FedAviationRegNum = resultSet.FedAviationRegNum;
                        this._CurrencySymbol = resultSet.CurrencySymbol;
                        this._LastUpdUID = resultSet.LastUpdUID;
                        this._UserID = resultSet.UserID;
                        this._FederalACCT = resultSet.FederalACCT;
                        this._ChtQouteDOMSegCHGACCT = resultSet.ChtQouteDOMSegCHGACCT;

                        this._FlightCatagoryPassenger = resultSet.FlightCatagoryPassenger;
                        this._FlightCatagoryPassengerNum = resultSet.FlightCatagoryPassengerNum;
                        this._MainFee = resultSet.MainFee;
                        this._CQCrewDuty = resultSet.CQCrewDuty;
                        this._CompanyName = resultSet.CompanyName;
                        this._CorpReqStartTM = resultSet.CorpReqStartTM;
                        this._TaxiTime = resultSet.TaxiTime;
                        this._AutoClimbTime = resultSet.AutoClimbTime;
                        this._QuoteSubtotal = resultSet.QuoteSubtotal;
                        this._QuoteTaxesDefault = resultSet.QuoteTaxesDefault;
                        this._DefaultInvoice = resultSet.DefaultInvoice;
                        this._QouteStdCrew2RONDeposit = resultSet.QouteStdCrew2RONDeposit;
                        this._QuoteAddition2CrewRON = resultSet.QuoteAddition2CrewRON;
                        this._uote2LandingFeeDefault = resultSet.uote2LandingFeeDefault;
                        this._QuoteWaiting2Charge = resultSet.QuoteWaiting2Charge;
                        this._Additional2FeeDefault = resultSet.Additional2FeeDefault;
                        this._Discount2AmountDeposit = resultSet.Discount2AmountDeposit;
                        this._Quote2Subtotal = resultSet.Quote2Subtotal;
                        this._Quote2TaxesDefault = resultSet.Quote2TaxesDefault;
                        this._QuoteInvoice2Default = resultSet.QuoteInvoice2Default;
                        this._QuotePrepayDefault = resultSet.QuotePrepayDefault;
                        this._QuoteRemaining2AmountDefault = resultSet.QuoteRemaining2AmountDefault;
                        this._QuoteRemainingAmountDefault = resultSet.QuoteRemainingAmountDefault;
                        this._TripsheetRptWriteGroup = resultSet.TripsheetRptWriteGroup;
                        this._ActiveDT = resultSet.ActiveDT;
                        this._BackupDT = resultSet.BackupDT;
                        this._LastUpdTS = resultSet.LastUpdTS;
                        this._AcceptDTTM = resultSet.AcceptDTTM;
                        this._LogFixed = resultSet.LogFixed;
                        this._LogRotary = resultSet.LogRotary;
                        this._WindReliability = resultSet.WindReliability;
                        this._Specdec3 = resultSet.Specdec3;
                        this._Specdec4 = resultSet.Specdec4;
                        this._QuoteLogoPosition = resultSet.QuoteLogoPosition;
                        this._IlogoPOS = resultSet.IlogoPOS;
                        this._TripsheetSearchBack = resultSet.TripsheetSearchBack;
                        this._TripS = resultSet.TripS;
                        this._LayoverHrsSIFL = resultSet.LayoverHrsSIFL;
                        this._PassengerOFullName = resultSet.PassengerOFullName;
                        this._PassengerOMiddle = resultSet.PassengerOMiddle;
                        this._PassengerOLast = resultSet.PassengerOLast;
                        this._PassengerOLastFirst = resultSet.PassengerOLastFirst;
                        this._PassengerOLastMiddle = resultSet.PassengerOLastMiddle;
                        this._PassengerOLastLast = resultSet.PassengerOLastLast;
                        this._CrewOFullName = resultSet.CrewOFullName;
                        this._CrewOMiddle = resultSet.CrewOMiddle;
                        this._CrewOLast = resultSet.CrewOLast;
                        this._CrewOLastFirst = resultSet.CrewOLastFirst;
                        this._CrewOLastMiddle = resultSet.CrewOLastMiddle;
                        this._CrewOLastLast = resultSet.CrewOLastLast;
                        this._ReportWriteLogoPosition = resultSet.ReportWriteLogoPosition;
                        this._Searchack = resultSet.Searchack;
                        this._SearchBack = resultSet.SearchBack;
                        this._DutyBasis = resultSet.DutyBasis;
                        this._AircraftBasis = resultSet.AircraftBasis;
                        this._FuelPurchase = resultSet.FuelPurchase;
                        this._FuelBurnKiloLBSGallon = resultSet.FuelBurnKiloLBSGallon;
                        this._IsAppliedTax = resultSet.IsAppliedTax;
                        this._RptSize = resultSet.RptSize;
                        this._CorpReqTMEntryFormat = resultSet.CorpReqTMEntryFormat;
                        this._ScheduleServiceTimeFormat = resultSet.ScheduleServiceTimeFormat;
                        this._SSHRFormat = resultSet.SSHRFormat;
                        this._DayMonthFormat = resultSet.DayMonthFormat;
                        this._ScheduledServicesSecurity = resultSet.ScheduledServicesSecurity;
                        this._WebSecurityType = resultSet.WebSecurityType;
                        this._QuoteIDTYPE = resultSet.QuoteIDTYPE;
                        this._TimeDisplayTenMin = resultSet.TimeDisplayTenMin;
                        this._HoursMinutesCONV = resultSet.HoursMinutesCONV;
                        this._ElapseTMRounding = resultSet.ElapseTMRounding;
                        this._ServerRPF = resultSet.ServerRPF;
                        this._ImageCnt = resultSet.ImageCnt;
                        this._ImagePosition = resultSet.ImagePosition;
                        this._AircraftBlockFlight = resultSet.AircraftBlockFlight;
                        this._ServerReport = resultSet.ServerReport;
                        this._ChtQouteDOMSegCHG = resultSet.ChtQouteDOMSegCHG;
                        this._ChtQouteIntlSegCHG = resultSet.ChtQouteIntlSegCHG;
                        this._SegmentFeeAlaska = resultSet.SegmentFeeAlaska;
                        this._SegementFeeHawaii = resultSet.SegementFeeHawaii;
                        this._QuotePageOffsettingInvoice = resultSet.QuotePageOffsettingInvoice;
                        this._QuotePageOff = resultSet.QuotePageOff;
                        this._MinuteHoursRON = resultSet.MinuteHoursRON;
                        this._RestDaysMinimum = resultSet.RestDaysMinimum;
                        this._RestCalendarQTRMinimum = resultSet.RestCalendarQTRMinimum;
                        this._FiscalYRStart = resultSet.FiscalYRStart;
                        this._FiscalYREnd = resultSet.FiscalYREnd;
                        this._PasswordValidDays = resultSet.PasswordValidDays;
                        this._PasswordHistoryNum = resultSet.PasswordHistoryNum;
                        this._TripsheetDTWarning = resultSet.TripsheetDTWarning;
                        this._ScheduleServiceListCTDetail = resultSet.ScheduleServiceListCTDetail;
                        this._RowsOnPageCnt = resultSet.RowsOnPageCnt;
                        this._DayLanding = resultSet.DayLanding;
                        this._DayLandingMinimum = resultSet.DayLandingMinimum;
                        this._NightLanding = resultSet.NightLanding;
                        this._NightllandingMinimum = resultSet.NightllandingMinimum;
                        this._Approach = resultSet.Approach;
                        this._ApproachMinimum = resultSet.ApproachMinimum;
                        this._Instrument = resultSet.Instrument;
                        this._InstrumentMinimum = resultSet.InstrumentMinimum;
                        this._TakeoffDay = resultSet.TakeoffDay;
                        this._TakeoffDayMinimum = resultSet.TakeoffDayMinimum;
                        this._TakeoffNight = resultSet.TakeoffNight;
                        this._TakeofNightMinimum = resultSet.TakeofNightMinimum;
                        this._Day7 = resultSet.Day7;
                        this._Day30 = resultSet.Day30;
                        this._Day90 = resultSet.Day90;
                        this._Month6 = resultSet.Month6;
                        this._Month12 = resultSet.Month12;
                        this._Day365 = resultSet.Day365;
                        this._RestDays = resultSet.RestDays;
                        this._CQMessageCD = resultSet.CQMessageCD;
                        this._IVMessageCD = resultSet.IVMessageCD;
                        this._RefreshTM = resultSet.RefreshTM;
                        this._SearchGridTM = resultSet.SearchGridTM;
                        this._CorpReqMinimumRunwayLength = resultSet.CorpReqMinimumRunwayLength;
                        this._CorpReqChgQueueTimer = resultSet.CorpReqChgQueueTimer;
                        this._AugmentCrewPercentage = resultSet.AugmentCrewPercentage;
                        this._GroundTM = resultSet.GroundTM;
                        this._CQFederalTax = resultSet.CQFederalTax;
                        this._RuralTax = resultSet.RuralTax;
                        this._GroundTMIntl = resultSet.GroundTMIntl;
                        this._CorpReqWeeklyCalTimer = resultSet.CorpReqWeeklyCalTimer;
                        this._FleetCalendar5DayTimer = resultSet.FleetCalendar5DayTimer;
                        this._Crew5DayTimer = resultSet.Crew5DayTimer;
                        this._CorpReqExcChgWithinHours = resultSet.CorpReqExcChgWithinHours;
                        this._DayMaximum7 = resultSet.DayMaximum7;
                        this._DayMaximum30 = resultSet.DayMaximum30;
                        this._CalendarMonthMaximum = resultSet.CalendarMonthMaximum;
                        this._DayMaximum90 = resultSet.DayMaximum90;
                        this._CalendarQTRMaximum = resultSet.CalendarQTRMaximum;
                        this._MonthMaximum6 = resultSet.MonthMaximum6;
                        this._Previous2QTRMaximum = resultSet.Previous2QTRMaximum;
                        this._MonthMaximum12 = resultSet.MonthMaximum12;
                        this._CalendarYearMaximum = resultSet.CalendarYearMaximum;
                        this._DayMaximum365 = resultSet.DayMaximum365;
                        this._UserNameCBP = resultSet.UserNameCBP;
                        this._UserPasswordCBP = resultSet.UserPasswordCBP;
                        this._ApplicationMessage = resultSet.ApplicationMessage;
                        this._ChtQouteHeaderDetailRpt = resultSet.ChtQouteHeaderDetailRpt;
                        this._ChtQouteFooterDetailRpt = resultSet.ChtQouteFooterDetailRpt;
                        this._ChtQuoteCompany = resultSet.ChtQuoteCompany;
                        this._ChtQuoteCompanyNameINV = resultSet.ChtQuoteCompanyNameINV;
                        this._ChtQuoteHeaderINV = resultSet.ChtQuoteHeaderINV;
                        this._ChtQuoteFooterINV = resultSet.ChtQuoteFooterINV;
                        this._HTMLHeader = resultSet.HTMLHeader;
                        this._HTMLFooter = resultSet.HTMLFooter;
                        this._HTMLBackgroun = resultSet.HTMLBackgroun;
                        this._WebPageBackgroundColor = resultSet.WebPageBackgroundColor;
                        this._HeardTextRpt = resultSet.HeardTextRpt;
                        this._FooterTextRpt = resultSet.FooterTextRpt;
                        this._MenuBackground = resultSet.MenuBackground;
                        this._MenuHeader = resultSet.MenuHeader;
                        this._MenuFooter = resultSet.MenuFooter;
                        this._SSBACKGRND = resultSet.SSBACKGRND;
                        this._SSHEADER = resultSet.SSHEADER;
                        this._SSFOOTER = resultSet.SSFOOTER;
                        this._WebBackground = resultSet.WebBackground;
                        this._WebHeader = resultSet.WebHeader;
                        this._WebFooter = resultSet.WebFooter;
                        this._QuoteLogo = resultSet.QuoteLogo;
                        this._Ilogo = resultSet.Ilogo;
                        this._TripsheetTabColor = resultSet.TripsheetTabColor;
                        this._TripSheetLogoPosition = resultSet.TripSheetLogoPosition;
                        this._PrivateTrip = resultSet.PrivateTrip;
                        this._ReportWriteLogo = resultSet.ReportWriteLogo;
                        this._RptTabOutbdInstLab1 = resultSet.RptTabOutbdInstLab1;
                        this._RptTabOutbdInstLab2 = resultSet.RptTabOutbdInstLab2;
                        this._RptTabOutbdInstLab3 = resultSet.RptTabOutbdInstLab3;
                        this._RptTabOutbdInstLab4 = resultSet.RptTabOutbdInstLab4;
                        this._RptTabOutbdInstLab5 = resultSet.RptTabOutbdInstLab5;
                        this._RptTabOutbdInstLab6 = resultSet.RptTabOutbdInstLab6;
                        this._RptTabOutbdInstLab7 = resultSet.RptTabOutbdInstLab7;
                        this._CrewLogCustomLBLShort1 = resultSet.CrewLogCustomLBLShort1;
                        this._CrewLogCustomLBLShort2 = resultSet.CrewLogCustomLBLShort2;
                        this._UserPassword = resultSet.UserPassword;
                        this._ServerName = resultSet.ServerName;
                        this._UserEmail = resultSet.UserEmail;
                        this._XMLPath = resultSet.XMLPath;
                        this._StdCrewRON = resultSet.StdCrewRON;
                        this._AdditionalCrewCD = resultSet.AdditionalCrewCD;
                        this._AdditionalCrewRON = resultSet.AdditionalCrewRON;
                        this._WaitTM = resultSet.WaitTM;
                        this._LandingFee = resultSet.LandingFee;
                        this._ApplicationDateFormat = resultSet.ApplicationDateFormat;
                        this._CrewLogCustomLBLLong1 = resultSet.CrewLogCustomLBLLong1;
                        this._CrewLogCustomLBLLong2 = resultSet.CrewLogCustomLBLLong2;
                        this._SpecificationLong3 = resultSet.SpecificationLong3;
                        this._SpecificationLong4 = resultSet.SpecificationLong4;
                        this._GeneralAviationDesk = resultSet.GeneralAviationDesk;
                        this._BaseFax = resultSet.BaseFax;
                        this._CanPassNum = resultSet.CanPassNum;
                        this._Tsense = resultSet.Tsense;
                        this._APISUrl = resultSet.APISUrl;
                        this._LoginUrl = resultSet.LoginUrl;
                        this._BaseDescription = resultSet.BaseDescription;
                        this._AccountFormat = resultSet.AccountFormat;
                        this._FederalTax = resultSet.FederalTax;
                        this._StateTax = resultSet.StateTax;
                        this._SaleTax = resultSet.SaleTax;
                        this._RuralAccountNum = resultSet.RuralAccountNum;
                        this._ChtQouteIntlSegCHGACCT = resultSet.ChtQouteIntlSegCHGACCT;
                        this._SeqmentAircraftAlaska = resultSet.SeqmentAircraftAlaska;
                        this._SegementAircraftHawaii = resultSet.SegementAircraftHawaii;
                        this._Domain = resultSet.Domain;
                        this._QuoteMinimumUseFeeDepositAmt = resultSet.QuoteMinimumUseFeeDepositAmt;
                        this._DepostFlightCharge = resultSet.DepostFlightCharge;
                        this._QuoteSegmentFeeDeposit = resultSet.QuoteSegmentFeeDeposit;
                        this._QuoteAdditonCrewDeposit = resultSet.QuoteAdditonCrewDeposit;
                        this._QuoteStdCrewRONDeposit = resultSet.QuoteStdCrewRONDeposit;
                        this._QuoteAdditionCrewRON = resultSet.QuoteAdditionCrewRON;
                        this._QuoteLandingFeeDefault = resultSet.QuoteLandingFeeDefault;
                        this._QuoteWaitingCharge = resultSet.QuoteWaitingCharge;
                        this._QuoteAdditionalFeeDefault = resultSet.QuoteAdditionalFeeDefault;
                        this._DiscountAmountDefault = resultSet.DiscountAmountDefault;
                        this._QuoteMinimumUse2FeeDepositAdj = resultSet.QuoteMinimumUse2FeeDepositAdj;
                        this._Deposit2FlightCharge = resultSet.Deposit2FlightCharge;
                        this._QuoteSegment2FeeDeposit = resultSet.QuoteSegment2FeeDeposit;
                        this._QuoteAddition2CrewDeposit = resultSet.QuoteAddition2CrewDeposit;
                        this._QuoteTitle = resultSet.QuoteTitle;
                        this._QuotePrepay2Default = resultSet.QuotePrepay2Default;
                        this._CorpAccountFormat = resultSet.CorpAccountFormat;
                        this._CustomRptMessage = resultSet.CustomRptMessage;
                        this._IsMiles = resultSet.IsMiles.GetValueOrDefault();
                        this._IsKilometer = resultSet.IsKilometer.GetValueOrDefault();
                        this._CrewChecklistAlertDays = resultSet.CrewChecklistAlertDays.GetValueOrDefault();
                        this._BusinessWeekStart = resultSet.BusinessWeekStart.GetValueOrDefault();
                        this._CRSearchBack = resultSet.CRSearchBack;
                        this._IsPODepartPercentage = resultSet.IsPODepartPercentage.GetValueOrDefault();
                        this._IsDefaultTripDepartureDate = resultSet.IsDefaultTripDepartureDate;

                        #endregion
                    }
                }
            }
        }
        #endregion
    }
}
