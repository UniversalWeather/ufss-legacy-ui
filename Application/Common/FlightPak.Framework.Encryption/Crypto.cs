﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Security.Principal;
using System.Configuration;

namespace FlightPak.Framework.Encryption
{
    /// <summary>
    /// Summary description for Crypto
    /// </summary>
    public class Crypto
    {
        public Crypto()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string GenerateHash(string Password, string SaltString)
        {
            try
            {
                byte[] inputstring = Encoding.UTF8.GetBytes(Password + SaltString);
                byte[] bytesalt = Encoding.UTF8.GetBytes(SaltString);

                HashAlgorithm algorithm = new SHA256Managed();

                byte[] plainTextWithSaltBytes = new byte[inputstring.Length + bytesalt.Length];

                for (int i = 0; i < inputstring.Length; i++)
                {
                    plainTextWithSaltBytes[i] = inputstring[i];
                }
                for (int i = 0; i < bytesalt.Length; i++)
                {
                    plainTextWithSaltBytes[inputstring.Length + i] = bytesalt[i];
                }

                return Convert.ToBase64String(algorithm.ComputeHash(plainTextWithSaltBytes)) + SaltString;
            }
            catch (Exception ex)
            {
                //Manually Handled
                return String.Empty;
            }
        }

        public string GenerateKey()
        {
            RijndaelManaged aesEncryption = new RijndaelManaged();
            try
            {
                aesEncryption.KeySize = 256;
                aesEncryption.BlockSize = 128;
                aesEncryption.Mode = CipherMode.CBC;
                aesEncryption.Padding = PaddingMode.PKCS7;
                aesEncryption.GenerateIV();
                string ivStr = Convert.ToBase64String(aesEncryption.IV);
                aesEncryption.GenerateKey();
                string keyStr = Convert.ToBase64String(aesEncryption.Key);
                string completeKey = ivStr + "," + keyStr;

                aesEncryption.Dispose();
                return Convert.ToBase64String(ASCIIEncoding.UTF8.GetBytes(completeKey));
            }
            catch (Exception ex)
            {
                aesEncryption.Dispose();
                //Manually Handled
                return string.Empty;
            }
        }

        public string GenerateSaltValue()
        {
            UnicodeEncoding utf16 = new UnicodeEncoding();

            if (utf16 != null)
            {
                // Create a random number object seeded from the value
                // of the last random seed value. This is done
                // interlocked because it is a static value and we want
                // it to roll forward safely.

                Random random = new Random(unchecked((int)DateTime.Now.Ticks));

                if (random != null)
                {
                    // Create an array of random values.

                    byte[] saltValue = new byte[5];

                    random.NextBytes(saltValue);

                    // Convert the salt value to a string. Note that the resulting string
                    // will still be an array of binary values and not a printable string. 
                    // Also it does not convert each byte to a double byte.

                    string saltValueString = utf16.GetString(saltValue);

                    // Return the salt value as a string.

                    return saltValueString;
                }
            }

            return null;
        }
        private string GetKey()
        {
            //System.Threading.Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity("tets"), new string[] { });

            /*
             * Build definition for developement : DEBUG
             * Build definition for deployment : RELEASE
             * Since Visual studio requires Generic Principal (not our custom one) to open Settings.settings file, We are having two different configurations.
             * 
             */

#if DEBUG
            return ConfigurationManager.AppSettings["EncryptionKey"];
#else

            return Properties.Settings.Default.EncryptionKey;
#endif
        }

        public string Encrypt(string plainStr)
        {
            try
            {
                byte[] cipherText;
                string completeEncodedKey = GetKey();
                using (RijndaelManaged aesEncryption = new RijndaelManaged())
                {

                    aesEncryption.KeySize = 256;
                    aesEncryption.BlockSize = 128;
                    aesEncryption.Mode = CipherMode.CBC;
                    aesEncryption.Padding = PaddingMode.PKCS7;
                    aesEncryption.IV = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(completeEncodedKey)).Split(',')[0]);
                    aesEncryption.Key = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(completeEncodedKey)).Split(',')[1]);
                    byte[] plainText = ASCIIEncoding.UTF8.GetBytes(plainStr);
                    ICryptoTransform crypto = aesEncryption.CreateEncryptor();
                    // The result of the encryption and decryption            
                    cipherText = crypto.TransformFinalBlock(plainText, 0, plainText.Length);
                }
                return Convert.ToBase64String(cipherText);
            }
            catch (Exception ex)
            {
                //Manually Handled
                return string.Empty;
            }
        }

        public string Decrypt(string encryptedText)
        {
            try
            {
                string decryptedText = string.Empty;
                string completeEncodedKey = GetKey();
                using (RijndaelManaged aesEncryption = new RijndaelManaged())
                {
                    aesEncryption.KeySize = 256;
                    aesEncryption.BlockSize = 128;
                    aesEncryption.Mode = CipherMode.CBC;
                    aesEncryption.Padding = PaddingMode.PKCS7;
                    aesEncryption.IV = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(completeEncodedKey)).Split(',')[0]);
                    aesEncryption.Key = Convert.FromBase64String(ASCIIEncoding.UTF8.GetString(Convert.FromBase64String(completeEncodedKey)).Split(',')[1]);
                    ICryptoTransform decrypto = aesEncryption.CreateDecryptor();
                    byte[] encryptedBytes = Convert.FromBase64CharArray(encryptedText.ToCharArray(), 0, encryptedText.Length);
                    decryptedText=ASCIIEncoding.UTF8.GetString(decrypto.TransformFinalBlock(encryptedBytes, 0, encryptedBytes.Length));
                }
                return decryptedText;
            }
            catch (Exception ex)
            {
                //Manually Handled
                return string.Empty;
            }
        }
    }
}
