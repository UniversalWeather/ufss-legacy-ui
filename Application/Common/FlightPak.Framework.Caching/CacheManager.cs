﻿using System;
using System.Runtime.Caching;
//For Tracing and Exception Handling
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Collections;
using System.Web;

namespace FlightPak.Framework.Caching
{
    public class CacheManager
    {
        ObjectCache cache = MemoryCache.Default;
        private ExceptionManager exManager;

        #region "Security Related Methods"
        public Object Get(string SessionID)
        {
            Object cacheObject = null;
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SessionID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    cacheObject = cache.Get(SessionID);

                }, FlightPak.Common.Constants.Policy.DataLayer);
                return cacheObject;
            }

        }


        public bool Add(Object CacheObject, string SessionID)
        {

            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(CacheObject, SessionID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                exManager.Process(() =>
                {
                    CacheItemPolicy CachePolicy = new CacheItemPolicy();
                    CachePolicy.SlidingExpiration = new TimeSpan(4, 0, 0);
                    cache.Set(SessionID, CacheObject, CachePolicy);
                }, FlightPak.Common.Constants.Policy.DataLayer);
                return true;
            }

        }

        public bool Remove(string SessionID)
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs(SessionID))
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    // MSDN Comment for cache.Remove return type
                    // An object that represents the value of the removed cache entry that was specified by the key, or null if the specified entry was not found.
                    return cache.Remove(SessionID) != null;

                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }

        public bool RemoveAll()
        {
            using (FlightPak.Common.Tracing.Tracer.NewTraceInputs())
            {
                //Handle methods throguh exception manager with return flag
                exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
                return exManager.Process<bool>(() =>
                {
                    // MSDN Comment for cache.Remove return type
                    // An object that represents the value of the removed cache entry that was specified by the key, or null if the specified entry was not found.

                    IDictionaryEnumerator enumerator = ((IEnumerable)cache).GetEnumerator() as IDictionaryEnumerator;
                    bool flag = true;
                    while (enumerator.MoveNext())
                    {
                        //flag is used to make sure all the objects in cache is removed.
                        //If anything failed, it should return false
                        flag &= cache.Remove(Convert.ToString(enumerator.Key)) != null;
                    }
                    return flag;
                }, FlightPak.Common.Constants.Policy.DataLayer);
            }
        }
        #endregion

    }
}
