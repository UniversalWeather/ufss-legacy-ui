﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common
{
    public class PreFlight 
    {
        #region Enumerators
        public enum TenthMinuteFormat
        {
            Tenth = 1,
            Minute = 2,
            Other = 0
        }
        #endregion

        public string Preflight_ConvertTenthsToMins(string time, decimal? TimeDisplayTenMin)
        {
            string result = "00:00";            
            if (!string.IsNullOrEmpty(time))
            {
                if (TimeDisplayTenMin != null && TenthMinuteFormat.Minute == (TenthMinuteFormat)TimeDisplayTenMin)
                {
                    TimeSpan tempObj = TimeSpan.FromHours(double.Parse(time));
                    return tempObj.ToString().Substring(0, 5);
                }
                else
                {
                    if (time.IndexOf(".") < 0)
                        time = time + ".0";
                    result = Math.Round(Convert.ToDecimal(time), 1).ToString();
                }
            }
            return result;
        }
        public double RoundElpTime(double lnElp_Time, decimal? vTimeDisplayTenMin, decimal? ElapseTMRounding)
        {
            if (vTimeDisplayTenMin != null && vTimeDisplayTenMin == 2)
            {
                double lnElp_Min = 0.0;
                double lnEteRound = 0.0;

                if (ElapseTMRounding == 1)
                    lnEteRound = 0;
                else if (ElapseTMRounding == 2)
                    lnEteRound = 5;
                else if (ElapseTMRounding == 3)
                    lnEteRound = 10;
                else if (ElapseTMRounding == 4)
                    lnEteRound = 15;
                else if (ElapseTMRounding == 5)
                    lnEteRound = 30;
                else if (ElapseTMRounding == 6)
                    lnEteRound = 60;

                if (lnEteRound > 0)
                {

                    lnElp_Min = (lnElp_Time - Math.Floor(lnElp_Time)) * 60;
                    //lnElp_Min = lnElp_Min % lnEteRound;
                    if (((lnElp_Min % lnEteRound) / lnEteRound) <= 0.5)
                        lnElp_Min = lnElp_Min - (lnElp_Min % lnEteRound);
                    else
                        lnElp_Min = lnElp_Min + (lnEteRound - (lnElp_Min % lnEteRound));

                    if (lnElp_Min > 0 && lnElp_Min < 60)
                        lnElp_Time = Math.Floor(lnElp_Time) + lnElp_Min / 60;

                    if (lnElp_Min == 0)
                        lnElp_Time = Math.Floor(lnElp_Time);


                    if (lnElp_Min == 60)
                        lnElp_Time = Math.Ceiling(lnElp_Time);

                }
            }
            return lnElp_Time;
        }
        public decimal ConvertToKilomenterBasedOnCompanyProfile(decimal Miles, bool IsKilometer)
        {
            if (IsKilometer == null ? false : IsKilometer)
                return Math.Floor(Miles * 1.852M);
            else
                return Miles;
        }
    }
}
