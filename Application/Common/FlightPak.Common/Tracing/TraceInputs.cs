﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Threading;

namespace FlightPak.Common.Tracing
{
    internal class TraceInputs : IDisposable
    {
        //We can add any number of class file and add the class name in ExecutingMethod. So we won't trace a tracing.
        const string currentNameSpace = "FlightPak.Common.Tracing";
        const string helperClassFile1 = "TraceInputs";
        const string helperClassFile2 = "Tracer";
        const string AssemblyFilter = "Assembly";
        const string ExecutionTimeFilter = "ExecutionTime";

        private readonly MethodBase methodBase;
        private readonly LogEntry logEntry;
        private readonly Stopwatch _stopwatch;
        internal IDisposable Timer;
        private bool _tracerDisposed;

        /// <summary>
        /// When constructor initialized, execution method base will be retrived and passed to the another constructor
        /// </summary>
        /// <param name="args"></param>
        public TraceInputs(params object[] args)
            : this(ExecutingMethod, args)
        {
        }
        public TraceInputs(MethodBase method, params object[] args)
        {


            //Make sure tracing is enabled
            if (!Tracer.TracingEnabled) return;
            methodBase = method;
            logEntry = new LogEntry
            {
                Categories = new[] { "FlightPak.Trace" },
                Priority = 0,
                Severity = TraceEventType.Verbose
            };

            //logEntry.ExtendedProperties[AssemblyFilter] = AssemblyNameOf(methodBase);
            //_le.ExtendedProperties[UserFilter] = UserCode;
            //logEntry.Message = string.Format("{0} --> ({1})", AssemblyClassMethodNameOf(methodBase), ArgsToString(methodBase, args));
            //logEntry.Title = "0";
            _stopwatch = Stopwatch.StartNew();

            Stopwatch.GetTimestamp();
            #region

            #endregion
            //writer.Write(logEntry);
            //Logger.Write(logEntry);


        }
        private static MethodBase ExecutingMethod
        {
            get
            {
                MethodBase result = null;
                var trace = new StackTrace(false);
                for (int index = 0; index < trace.FrameCount; ++index)
                {
                    StackFrame frame = trace.GetFrame(index);
                    MethodBase method = frame.GetMethod();

                    Type t = method.DeclaringType;
                    //Leave the current context
                    if (!(t.Namespace == currentNameSpace && (t.Name == helperClassFile1 || t.Name == helperClassFile2)))
                    {
                        result = method;
                        break;
                    }
                }
                return result;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_tracerDisposed)
            {
                _tracerDisposed = true;

                if (Tracer.TracingEnabled)
                {
                    decimal secondsElapsed = GetSecondsElapsed(_stopwatch.ElapsedMilliseconds);
                    logEntry.Title = secondsElapsed.ToString();
                    logEntry.Message = string.Format("{0}", AssemblyClassMethodNameOf(methodBase));
                    logEntry.ProcessName = Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null ? Thread.CurrentPrincipal.Identity.Name : string.Empty;
                    Logger.Write(logEntry);
                }

                if (Timer != null)
                {
                    Timer.Dispose();
                }
            }
        }
        public static string AssemblyNameOf(MemberInfo mi)
        {
            return mi == null ? "" : mi.DeclaringType.Assembly.GetName().Name;
        }
        private static string AssemblyClassMethodNameOf(MemberInfo mi)
        {
            return string.Format("{0}.{1}", mi == null ? string.Empty : mi.DeclaringType.FullName, MethodNameOf(mi));
        }
        private static string MethodNameOf(MemberInfo mi)
        {
            return mi == null ? string.Empty : mi.Name;
        }
        private static StringBuilder ArgsToString(MethodBase mb, params object[] args)
        {
            var argv = new StringBuilder();
            ParameterInfo[] pis = mb.GetParameters();
            int i = 0;
            foreach (var pi in pis)
            {
                argv.AppendFormat("{0}=\"{1}\"{2}", pi.Name, i < args.Length ? ArgToString(args[i]) : "?", i < pis.Length - 1 ? "& " : "");
                i++;
            }
            return argv;
        }
        private static string ArgToString(object arg)
        {
            string ret;
            var a = arg as Array;
            if (arg == null)
            {
                ret = "<null>";
            }
            else if (a != null)
            {
                const int MAX = 10;
                int i = 0;
                int maxArgs = a.Length > MAX ? MAX : a.Length;
                var sb = new StringBuilder();
                foreach (var o in a)
                {
                    sb.AppendFormat("{0}{1}", o, i++ < maxArgs - 1 ? "," : "");
                    if (i == MAX)
                    {
                        break;
                    }
                }
                if (a.Length > MAX)
                {
                    sb.Append(",...");
                }
                ret = "[" + sb + "]";
            }
            else
            {
                ret = Convert.ToString(arg);
            }
            return ret;
        }
        private static decimal GetSecondsElapsed(long milliseconds)
        {
            decimal result = Convert.ToDecimal(milliseconds) / 1000m;
            return Math.Round(result, 6);
        }
    }
}
