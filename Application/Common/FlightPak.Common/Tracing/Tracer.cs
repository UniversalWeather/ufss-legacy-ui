﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace FlightPak.Common.Tracing
{
    public static class Tracer
    {
        /// <summary>
        /// Tracing setting dictated by the in-memory flag
        /// </summary>
        public static bool TracingEnabled
        {
            get { return TraceFlag.Value; }
            set { TraceFlag.Value = value; }
        }

        #region TraceFlag class

        /// <summary>
        /// class provides concurrent access to the trace flag within same appdomain
        /// using an instance of itself as lockable object for C# lock statement
        /// </summary>
        private static class TraceFlag
        {
            static TraceFlag()
            {
                try
                {
                    Value = Logger.Writer.IsTracingEnabled();
                    //Value = true;
                }
                catch (Exception ex)
                {
                    //Manually Handled
                    Value = false;
                }
            }

            public static bool Value { get; set; }
        }

        #endregion



        /// <summary>
        /// Conditional factory version.<br/>
        /// Instead of doing "using (new TraceInputs(foo, bar)",
        /// you do "using (Tracing.NewTraceInputs(foo, bar))"
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IDisposable NewTraceInputs(params object[] args)
        {
            if (TracingEnabled)
            {
                // only instantiate the traceinputs class if tracing is enabled
                return new TraceInputs(args);
            }

            return null;
        }
    }
}
