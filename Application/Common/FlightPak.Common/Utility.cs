﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Configuration;
using System.Text;
using System.Linq;


namespace FlightPak.Common
{
    using System.Collections;
    using System.Net;

    public static class Utility
    {
       
        //To support globalization rules. 
        public static IFormatProvider GetFormatProvider
        {
            get
            {
                const IFormatProvider iFormatProvider = null;
                return iFormatProvider;
            }
        }

        /// <summary>
        ///  Method to find whether the input string contains an integer value
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        public static bool IsInteger(string theValue)
        {
            var _isNumber = new Regex(@"^\d+$");
            Match m = _isNumber.Match(theValue);
            return m.Success;
        }

       

        /// <summary>
        /// 
        /// </summary>
        public static CultureInfo GetCultureInfo
        {
            get
            {
                CultureInfo cultureInfo = CultureInfo.CurrentCulture;
                return cultureInfo;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static StringComparison GetStringComparisonCultureInfo
        {
            get { return StringComparison.CurrentCulture; }
        }

        /// <summary>
        /// Extension method for support add range in collection
        /// </summary>
        /// <typeparam name="T">The type on which the extension is applied</typeparam>
        /// <param name="collection"></param>
        /// <param name="values"></param>
        public static void AddRange<T>(this Collection<T> collection, IEnumerable<T> values)
        {
            foreach (T item in values)
            {
                collection.Add(item);
            }
        }

  
        /// <summary>
        /// Returns the value of the specified key from the configuration app settings.
        /// </summary>
        /// <param name="key">Appsetting key</param>
        /// <returns>Configuration connection string value for the specified key.</returns>
        public static string GetValueFromConfig(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }


        /// <summary>
        /// Returns the value of the specified key from the configuration connection string settings.
        /// </summary>
        /// <param name="key">Connection string key</param>
        /// <returns>Configuration connection string value for the specified key.</returns>
        public static string GetConnectionStringFromConfig(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }
        /// <summary>
        /// Returns the value of WildCard string
        /// </summary>
        /// <param name="input">name of input string</param>
        /// <returns>Wild card Sql Value</returns>

        public static string GetSqlWildcard(string input)
        {
            return input.Replace('*', '%');
        }

        /// <summary>
        /// This method is used to convert given string value to boolean value.
        /// </summary>
        /// <param name="value">Given string value</param>
        /// <returns>true or false</returns>
        public static bool ToBoolean(string value)
        {
            string strValue = value.ToLower(GetCultureInfo);

            if (strValue.Equals("1") || strValue.Equals("true") || strValue.Equals("t") || strValue.Equals("y") || strValue.Equals("on") || strValue.Equals("yes") || strValue.Equals("ok"))
            {
                return true;
            }
            return false;
        }

      

        /// <summary>
        /// A common method  added to convert string to Int32 wherever required.
        /// </summary>
        /// <param name="value">String to be converted to int value</param>
        /// <returns>Appropriate int value is input is in proper format</returns>
        public static int ConvertToInt32(string value)
        {
            int result=0;
            try
            {
                if (!String.IsNullOrEmpty(value))
                {
                    result = Int32.Parse(value);
                }
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;
        }

        /// <summary>
        /// A common method  added to convert string to Int16 wherever required.
        /// </summary>
        /// <param name="value">String to be converted to short value</param>
        /// <returns>Appropriate short value is input is in proper format</returns>
        public static short ConvertToInt16(string value)
        {
            short result;
            try
            {
                result = Int16.Parse(value);
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;
        }

        /// <summary>
        /// A common method  added to convert string to Int64 wherever required.
        /// </summary>
        /// <param name="value">String to be converted to long value</param>
        /// <returns>Appropriate long value is input is in proper format</returns>
        public static long ConvertToInt64(string value)
        {
            long result;
            try
            {
                result = Int64.Parse(value);
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;
        }

        /// <summary>
        /// A common method  added to convert string to double wherever required.
        /// </summary>
        /// <param name="value">String to be converted to double value</param>
        /// <returns>Appropriate double value is input is in proper format</returns>
        public static double ConvertToDouble(string value)
        {
            double result;
            try
            {
                result = Double.Parse(value);
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;

        }

        /// <summary>
        /// A common method  added to convert string to decimal wherever required.
        /// </summary>
        /// <param name="value">String to be converted to double value</param>
        /// <returns>Appropriate double value is input is in proper format</returns>
        public static decimal ConvertToDecimal(string value)
        {
            decimal result;
            try
            {
                result = Decimal.Parse(value);
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;

        }

        /// <summary>
        /// A common method  added to convert string to DateTime type wherever required.
        /// </summary>
        /// <param name="value">string to be converted to bool</param>
        /// <returns>true or false if string is a proper source, else exception will be thrown.</returns>
        public static bool ConvertToBoolean(string value)
        {
            bool result;
            try
            {
                result = Boolean.Parse(value);
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;
        }

        /// <summary>
        /// A common method  added to convert string to datetime wherever required.
        /// </summary>
        /// <param name="value">String to be converted to datetime value</param>
        /// <returns>Appropriate datetime value is input is in proper format</returns>
        public static DateTime ConvertToDateTime(string value)
        {
            DateTime result;
            try
            {
                result = DateTime.Parse(value);
            }
            catch (FormatException exception)
            {
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
            return result;
        }

        public static DateTime ConvertFromAmericanDateFormatToDateTime(string value)
        {

            try
            {

                var dateInfo = new DateTimeFormatInfo { ShortDatePattern = "MM/dd/yyyy" };
                DateTime validDate = Convert.ToDateTime(value, dateInfo);
                return validDate;
            }
            catch (FormatException exception)
            {
                
                throw new FormatException(exception.Message + "Unable to format string :" + value);
            }
        }

        /// <summary>
        /// A common method  added to convert string to Guid wherever required.
        /// </summary>
        /// <param name="value">string value to be converted to Guid</param>
        /// <returns>new guid if the string is in proper format.</returns>
        public static Guid ConvertToGuid(string value)
        {
            var id=new Guid();
            try
            {
                if (!String.IsNullOrEmpty(value) && value != Guid.Empty.ToString())
                {
                    id = new Guid(value);
                }
            }
            catch (Exception exception)
            {
               // Logger.LogErrorMessage(exception.Message + "Unable to format string :" + value);
                throw;
            }
            return id;
        }

        /// <summary>
        /// A common method  get the file format based on bytes
        /// </summary>
        /// <param name="value">bytes value to be converted to bytes</param>
        /// <returns>new guid if the string is in proper format.</returns>
        /// 
        public static string GetImageFormatExtension(byte[] bytes)
        {
            var bmp = Encoding.ASCII.GetBytes("BM");     // BMP
            var gif = Encoding.ASCII.GetBytes("GIF");    // GIF
            var png = new byte[] { 137, 80, 78, 71 };    // PNG
            var tiff = new byte[] { 73, 73, 42 };         // TIFF
            var tiff2 = new byte[] { 77, 77, 42 };         // TIFF
            var jpeg = new byte[] { 255, 216, 255, 224 }; // jpeg
            var jpeg2 = new byte[] { 255, 216, 255, 225 }; // jpeg canon
            if (bmp.SequenceEqual(bytes.Take(bmp.Length)))
                return ".bmp";
            if (gif.SequenceEqual(bytes.Take(gif.Length)))
                return ".gif";
            if (png.SequenceEqual(bytes.Take(png.Length)))
                return ".png";
            if (tiff.SequenceEqual(bytes.Take(tiff.Length)))
                return ".tiff";
            if (tiff2.SequenceEqual(bytes.Take(tiff2.Length)))
                return ".tiff";
            if (jpeg.SequenceEqual(bytes.Take(jpeg.Length)))
                return ".jpeg";
            if (jpeg2.SequenceEqual(bytes.Take(jpeg2.Length)))
                return ".jpeg";
            return string.Empty;
        }

        /// <summary>
        /// Check weather the given ETE value is 0. If the ETE value is 0 then set it default to 1 as per new rule
        /// </summary>
        /// <param name="eteStr">decimal value as string</param>
        /// <returns>ETE</returns>
        public static string DefaultEteResult(string eteStr)
        {
            if (!string.IsNullOrEmpty(eteStr))
            {
                if (eteStr == "00:00" || eteStr == "0:0" || eteStr == "0:00" || eteStr == "00:0")
                    eteStr = "00:05"; // Set default to 00.05 if the ETE is calculated 0 as per new rule
                else if (eteStr == "00.00" || eteStr == "0.0" || eteStr == "0.00" || eteStr == "00.0" || eteStr == "0")
                    eteStr = "0.1"; // Set default to 1 if the ETE is calculated 0 as per new rule    
            }
            
            return eteStr;
        }

        /// ------------------------------------------------------------------------
        /// <summary>
        /// Deep Comparison two objects if they are alike. The objects are consider alike if 
        /// they are:
        /// <list type="ordered">
        ///    <item>of the same <see cref="System.Type"/>,</item>
        ///    <item>have the same number of methods, properties and fields</item>
        ///    <item>the public and private properties and fields values reflect each other's. </item>
        /// </list>
        /// </summary>
        /// <param name="original"></param>
        /// <param name="comparedToObject"></param>
        /// <param name="ignoreListProp"></param>
        /// <param name="containListProp"></param>
        /// <returns></returns>
        /// ------------------------------------------------------------------------
        public static bool CompareObjects(object original, object comparedToObject, string[] ignoreListProp, string[] containListProp, bool repeat = false)
        {

            if (original.GetType() != comparedToObject.GetType())
                return false;

            // ...............................................
            // Compare Number of Private and public Methods
            // ...............................................
            MethodInfo[] originalMethods = original
              .GetType()
              .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            MethodInfo[] comparedMethods = comparedToObject
              .GetType()
              .GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            if (comparedMethods.Length != originalMethods.Length)
                return false;

            // ...............................................
            // Compare Number of Private and public Properties
            // ................................................
            PropertyInfo[] originalProperties = original
              .GetType()
              .GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            PropertyInfo[] comparedProperties = comparedToObject
              .GetType()
              .GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            if (comparedProperties.Length != originalProperties.Length)
                return false;

            // ...........................................
            // Compare number of public and private fields
            // ...........................................
            FieldInfo[] originalFields = original
              .GetType()
              .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

            FieldInfo[] comparedToFields = comparedToObject
              .GetType()
              .GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);


            if (comparedToFields.Length != originalFields.Length)
                return false;

            // ........................................
            // compare field values
            // ........................................
            foreach (FieldInfo fi in originalFields)
            {

                // check to see if the object to contains the field          
                FieldInfo fiComparedObj = comparedToObject.GetType().GetField(fi.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

                if (fiComparedObj == null)
                    return false;

                // Get the value of the field from the original object        
                Object srcValue = original.GetType().InvokeMember(fi.Name,
                  BindingFlags.GetField | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static,
                  null,
                  original,
                  null);

                // Get the value of the field
                object comparedObjFieldValue = comparedToObject
                  .GetType()
                  .InvokeMember(fiComparedObj.Name,
                  BindingFlags.GetField | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static,
                  null,
                  comparedToObject,
                  null);

                // -------------------------------
                // now compare the field values
                // -------------------------------
                if (!ignoreListProp.Contains(fiComparedObj.Name.ToUpper()))
                {
                    if (srcValue == null)
                    {
                        if (comparedObjFieldValue != null)
                            return false;
                        //else
                        //    return true;
                    }
                    if (srcValue != null && comparedObjFieldValue != null)
                    {
                        if (srcValue.GetType() != comparedObjFieldValue.GetType()) return false;
                        if (!srcValue.ToString().Equals(comparedObjFieldValue.ToString())) return false;
                    }
                }
            }

            int propCount = 0;
            // ........................................
            // compare each Property values
            // ........................................
            foreach (PropertyInfo pi in originalProperties)
            {
                propCount += 1;
                // check to see if the object to contains the field          
                PropertyInfo piComparedObj = comparedToObject
                  .GetType()
                  .GetProperty(pi.Name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);

                if (piComparedObj == null)
                    return false;

                // Get the value of the property from the original object        
                Object srcValue = original
                  .GetType()
                  .InvokeMember(pi.Name,
                  BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static,
                  null,
                  original,
                  null);

                // Get the value of the property
                object comparedObjValue = comparedToObject
                  .GetType()
                  .InvokeMember(piComparedObj.Name,
                  BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static ,
                  null,
                  comparedToObject,
                  null);

                // -------------------------------
                // now compare the property values
                // -------------------------------
                if (!ignoreListProp.Contains(piComparedObj.Name.ToUpper()))
                {
                    if (srcValue == null)
                    {
                        if (comparedObjValue != null) return false;
                        //else
                        //    return true;
                    }
                    if (srcValue != null && comparedObjValue != null)
                    {
                        if (srcValue.GetType() != comparedObjValue.GetType()) return false;
                        if (!srcValue.ToString().Equals(comparedObjValue.ToString())) return false;
                    }


                    if (containListProp.Contains(pi.Name.ToUpper()))
                    {
                        if (pi.PropertyType.Name.ToUpper() == "ENTITYCOLLECTION`1")
                        {
                            IEnumerable srcEnumerable = (IEnumerable)srcValue;
                            IEnumerable comEnumerable = (IEnumerable)comparedObjValue;

                            if (comEnumerable != null && (srcEnumerable.Cast<object>().Count() != comEnumerable.Cast<object>().Count())) return false;

                            int itemCount = 0;
                            if (srcEnumerable != null && comEnumerable != null)
                            {
                                foreach (var srcItem in srcEnumerable)
                                {
                                    var comItem = comEnumerable.Cast<object>().ElementAt(itemCount);
                                    itemCount += 1;
                                    // Get the value of the property from the original object        

                                    if (srcItem == null)
                                    {
                                        if (comItem != null) return false;
                                    }

                                    if (srcItem != null && comItem != null)
                                    {
                                        if ((srcItem != null && comItem == null) || (srcItem == null && comItem != null))
                                            return false;

                                        if (srcItem != null && comItem != null)
                                        {
                                            bool resSub = CompareObjects(srcItem, comItem, ignoreListProp, containListProp, true);
                                            if (resSub == false) return false;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((srcValue != null && comparedObjValue == null) || (srcValue == null && comparedObjValue != null))
                                return false;

                            if (repeat == false)
                            {
                                if (srcValue != null && comparedObjValue != null)
                                {
                                    if (srcValue != "" && comparedObjValue != "")
                                    {
                                        bool resSub = CompareObjects(srcValue, comparedObjValue, ignoreListProp, containListProp, true);
                                        if (resSub == false) return false;
                                    }
                                }
                            }

                        }
                    }
                }

            }

            return true;
        }
        /// <summary>
        /// Get a substring of the first N characters.
        /// </summary>
        public static string Truncate(this string inputstring, int length)
        {
            if (inputstring != null && inputstring.Length > length)
            {
                inputstring = inputstring.Substring(0, length);
            }
            return inputstring;
        }
       
        /// <summary>
        ///  Read CSV file data and return string list
        /// </summary>
        /// <param name="csvFileData">Pass CSV file data as string</param>
        /// <param name="readNoOfRecords">Number of records read from File or -1 for read all the record from csvFileData</param>
        /// <returns></returns>
        public static List<List<string>> FetchPreviewRecordsInSession(string csvFileData, int readNoOfRecords)
        {
            string[] columnSplitter = { "\",\"" };
            string line;
            string[] columns = null;
            int titleColumnsCount = 0;
            int cnt = 0;
            var readFileRow = new List<string>();
            var readTotalFile = new List<List<string>>();
            var blankColumns = new List<int>();
            
            // Use regx use for split when CSV file column value contain comma character
            var regx = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

            using (var sr = new StringReader(csvFileData))
            {
                while ((line = sr.ReadLine()) != null && cnt != readNoOfRecords)
                {

                    columns = line.Contains(columnSplitter[0]) ? line.Split(columnSplitter, StringSplitOptions.None).Select(c => c.Trim()).ToArray() : regx.Split(line).Select(c => c.Trim()).ToArray();

                    //for (int index = 0; index < columns.Count(); index++)
                    //{
                    //    if (columns[index].Contains("\""))
                    //    {
                    //        columns[index] = columns[index].Replace("\"", "");
                    //    }
                    //}
                    if (cnt == 0)
                    {
                        titleColumnsCount = columns.Length;
                    }
                    if (cnt == 0)
                    {
                        for (int i = 0; i < columns.Length; i++)
                        {
                            if (columns[i].Trim().Length > 0)
                            {
                                readFileRow.Add(columns[i].Trim());
                            }
                            else
                            {
                                blankColumns.Add(i);
                            }
                        }
                        //titleColumnsCount = readFileRow.Count;
                    }
                    else if (titleColumnsCount == columns.Length)
                    {
                        readFileRow.AddRange(columns.Where((t, i) => !blankColumns.Contains(i)));
                    }
                    if (readFileRow.Count > 0)
                    {
                        readTotalFile.Add(readFileRow);
                        readFileRow = new List<string>();
                        cnt++;
                    }

                }
            }
            return readTotalFile;
        }

        /// <summary>
        /// Validate the CSV file content weather the content is valid string or not, maximum 20% of total string lengh is allowed for other unknown character
        /// </summary>
        /// <param name="content">csv file data</param>
        /// <param name="validMargin">Percentage of data to consider invalid per row</param>
        /// <returns>CsvValidationResult</returns>
        public static CsvValidationResult ValidateCsvContent(string content, int validMargin = 20)
        {
            CsvValidationResult objCsvValidationResult = new CsvValidationResult();
            objCsvValidationResult.IsValid = true;
            objCsvValidationResult.InvalidPercentage = 0;
            objCsvValidationResult.InvalidContent = "";

            string RegExp = "[a-zA-Z0-9,-.!@#$%^&*()_+|\\,./:?\\{\\}\\[\\];\"<>=' ]";
            string[] lines = content.Split(new string[] {"\r", "\n"}, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length > 0)
            {
                List<string> lstStringList = new List<string>();

                if (lines.Length <= 30)
                {
                    lstStringList = lines.ToList();
                }
                else
                {
                    lstStringList = lines.Take(10).ToList();
                    lstStringList.AddRange(lines.Skip(lines.Length/2 - 5).Take(10));
                    lstStringList.AddRange(lines.Skip(lines.Length - 10).Take(10));
                }

                foreach (var line in lstStringList)
                {
                    if (string.IsNullOrWhiteSpace(line))
                        continue;

                    Regex rgx = new Regex(RegExp);
                    int marginResult = (rgx.Replace(line, "").Length*100/line.Length);
                    if (marginResult >= validMargin)
                    {
                        objCsvValidationResult.IsValid = false;
                        objCsvValidationResult.InvalidPercentage = marginResult;
                        objCsvValidationResult.InvalidContent = line;
                        break;
                    }
                }
            }

            return objCsvValidationResult;
        }
    }

    /// <summary>
    /// Used to return CSV file validation result
    /// </summary>
    public class CsvValidationResult
    {
        public bool IsValid { get; set; }
        public int InvalidPercentage { get; set; }
        public string InvalidContent { get; set; }
    }
}
