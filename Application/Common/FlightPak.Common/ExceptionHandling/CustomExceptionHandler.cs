﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using System.Collections.Specialized;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace FlightPak.Common.ExceptionHandling
{
    [ConfigurationElementType(typeof(CustomHandlerData))]
    public class CustomExceptionHandler : IExceptionHandler
    {
        public CustomExceptionHandler(NameValueCollection ignore)
        {
        }

        public Exception HandleException(Exception exception, Guid handlingInstanceId)
        {
            if (exception.GetType() == typeof(System.Data.UpdateException))
            {
                if (exception.InnerException.Message.ToUpper().Contains("-500011"))
                {
                    FlightPak.Framework.Encryption.Crypto crypting = new Framework.Encryption.Crypto();
                    string VisaNumber = string.Empty;
                    string[] SplitException = new string[2];
                    SplitException = exception.InnerException.Message.Split('~');
                    if (!string.IsNullOrEmpty(SplitException[1]))
                    {
                        VisaNumber = crypting.Decrypt(SplitException[1]);
                    }
                    return new BaseException("Visa Number <b>" + VisaNumber + "</b> is not Unique.");
                }
                else if (exception.InnerException.Message.ToUpper().Contains("-500012"))
                {
                    FlightPak.Framework.Encryption.Crypto crypting = new Framework.Encryption.Crypto();
                    string PassportNumber = string.Empty;
                    string[] SplitException = new string[2];
                    SplitException = exception.InnerException.Message.Split('~');
                    if (!string.IsNullOrEmpty(SplitException[1]))
                    {
                        PassportNumber = crypting.Decrypt(SplitException[1]);
                    }
                    return new BaseException("Passport Number <b>" + PassportNumber + "</b> is not Unique.");
                }               
                if (exception.InnerException.Message.ToUpper().Contains("CANNOT INSERT DUPLICATE KEY ROW IN OBJECT"))
                {
                    return new BaseException("Unique Code is Required.");
                }
                else if (exception.InnerException.Message.ToUpper().Contains("-500010"))
                {
                    return new BaseException("This Record Exists In A Related Table. Deletion is not allowed. Please Inactivate the Record.");
                }
                else if (exception.InnerException.Message.ToUpper().Contains("-500013"))
                {
                    return new BaseException("Could not update all company related tables, Please try later.");
                }
            }
            else if (exception.GetType() == typeof(System.InvalidCastException))
            {
                if (exception.Message.ToUpper().Contains("UNABLE TO CAST OBJECT OF TYPE 'SYSTEM.SECURITY.PRINCIPAL.GENERICPRINCIPAL' TO TYPE 'FLIGHTPAK.FRAMEWORK.SECURITY.FPPRINCIPAL'"))
                {
                    return new BaseException("Login session has expired or logged-in using other browser or other device.");
                }
            }
            return exception;
            // Perform processing here. The exception you return will be 
            // passed to the next exception handler in the chain.
        }
    }
}
