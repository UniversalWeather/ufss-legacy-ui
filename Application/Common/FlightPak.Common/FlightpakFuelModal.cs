﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common
{
    public class FlightpakFuelModal
    {
        public string FBO { get; set; }
        public string ICAO { get; set; }
        public string IATA { get; set; }
        public string Vendor { get; set; }
        public int? GallonFrom { get; set; }
        public int? GallonTo { get; set; }
        public decimal? Price { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string NoteText { get; set; }
    }
}
