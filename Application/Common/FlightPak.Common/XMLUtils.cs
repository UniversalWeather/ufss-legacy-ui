﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.Text;
using System.IO;

namespace FlightPak.Common
{
    public class XMLUtils
    {
        public static XmlReader GetXMLReaderObjectByFileName(string fileName)
        {
            XmlReader reader = null;
            try
            {
                string ext = fileName.Substring(fileName.IndexOf("."), fileName.Length - fileName.IndexOf(".")).ToLower();
                if (ext == ".xml" && GetXmlFileWhiteList().Any(p => p.ToLower() == fileName.ToLower()))
                {
                    string xmlFileName = GetXmlFileWhiteList().FirstOrDefault(p => p.ToLower() == fileName.ToLower());
                    XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
                    xmlReaderSettings.DtdProcessing = DtdProcessing.Prohibit;
                    reader = XmlReader.Create(HttpContext.Current.Server.MapPath("Config\\" + xmlFileName), xmlReaderSettings);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return reader;
        }

        public static List<string> GetXmlFileWhiteList()
        {
            List<string> objXmlFileNameList = new List<string>();
            objXmlFileNameList.Add("CalenderMainTab.xml");
            objXmlFileNameList.Add("CharterMainTab.xml");
            objXmlFileNameList.Add("CorporateFlightMainTab.xml");
            objXmlFileNameList.Add("CQAircraftProfile.xml");
            objXmlFileNameList.Add("CQInvoice.xml");
            objXmlFileNameList.Add("CQItinerary.xml");
            objXmlFileNameList.Add("CQNotesAlerts.xml");
            objXmlFileNameList.Add("CQPaxProfile.xml");
            objXmlFileNameList.Add("CQQuoteLog.xml");
            objXmlFileNameList.Add("CQQuoteReport.xml");
            objXmlFileNameList.Add("CQTripStatus.xml");
            objXmlFileNameList.Add("CRAircraftApprovalSummary.xml");
            objXmlFileNameList.Add("CRApprovalAuditSummary.xml");
            objXmlFileNameList.Add("CRCorporateAircraftApprovalForm.xml");
            objXmlFileNameList.Add("CRPassengerItinerary.xml");
            objXmlFileNameList.Add("CRTripItinerary.xml");
            objXmlFileNameList.Add("DatabaseMainTab.xml");
            objXmlFileNameList.Add("DBAccounts.xml");
            objXmlFileNameList.Add("DBAircraftDutyTypes.xml");
            objXmlFileNameList.Add("DBAircraftType.xml");
            objXmlFileNameList.Add("DBAirport.xml");
            objXmlFileNameList.Add("DBBudgets.xml");
            objXmlFileNameList.Add("DBCharterQuoteCustomer.xml");
            objXmlFileNameList.Add("DBCrewAdditionalInformation.xml");
            objXmlFileNameList.Add("DBCrewChecklist.xml");
            objXmlFileNameList.Add("DBCrewDutyTypes.xml");
            objXmlFileNameList.Add("DBCrewRoster.xml");
            objXmlFileNameList.Add("DBCustomUVAirports.xml");
            objXmlFileNameList.Add("DBDelayTypes.xml");
            objXmlFileNameList.Add("DBDepartmentAuthorization.xml");
            objXmlFileNameList.Add("DBFleetProfile.xml");
            objXmlFileNameList.Add("DBFlightCategories.xml");
            objXmlFileNameList.Add("DBFlightPurposes.xml");
            objXmlFileNameList.Add("DBFuelLocator.xml");
            objXmlFileNameList.Add("DBPassengerAdditionalInformation.xml");
            objXmlFileNameList.Add("DBPassengerRequestorIIExport.xml");
            objXmlFileNameList.Add("DBPassengersRequestors.xml");
            objXmlFileNameList.Add("DBPassengersRequestorsII.xml");
            objXmlFileNameList.Add("DBPayableVendors.xml");
            objXmlFileNameList.Add("DBPaymentTypes.xml");
            objXmlFileNameList.Add("DBSalespersons.xml");
            objXmlFileNameList.Add("DBSIFLRates.xml");
            objXmlFileNameList.Add("DBTripsheetChecklist.xml");
            objXmlFileNameList.Add("DBTripsheetCheckListGroup.xml");
            objXmlFileNameList.Add("DBUserGroupAcess.xml");
            objXmlFileNameList.Add("DBVendorListing.xml");
            objXmlFileNameList.Add("FleetManager.xml");
            objXmlFileNameList.Add("LocatorAirport.xml");
            objXmlFileNameList.Add("LocatorCustom.xml");
            objXmlFileNameList.Add("LocatorHotel.xml");
            objXmlFileNameList.Add("LocatorVendor.xml");
            objXmlFileNameList.Add("NonPilotLog.xml");
            objXmlFileNameList.Add("POSTAdhocExport.xml");
            objXmlFileNameList.Add("PostAircraftArrivals.xml");
            objXmlFileNameList.Add("PostAircraftFlightHistory.xml");
            objXmlFileNameList.Add("PostAircraftPerformanceAnalysis.xml");
            objXmlFileNameList.Add("PostAircraftTypeUtilization.xml");
            objXmlFileNameList.Add("PostAnnualCrewStatistics.xml");
            objXmlFileNameList.Add("PostAnnualFleetStatistics.xml");
            objXmlFileNameList.Add("POSTBillingByCodeTrip.xml");
            objXmlFileNameList.Add("POSTBillingByPassengerTrip.xml");
            objXmlFileNameList.Add("POSTBudgetActualVarianceReport.xml");
            objXmlFileNameList.Add("PostCostTrackingBudgetSummary.xml");
            objXmlFileNameList.Add("POSTCostTrackingCostSummary.xml");
            objXmlFileNameList.Add("POSTCQCustomerBillingSummary.xml");
            objXmlFileNameList.Add("POSTCQCustomerFlightHistory.xml");
            objXmlFileNameList.Add("POSTCQLeadSourceHistory.xml");
            objXmlFileNameList.Add("POSTCQSalesPersonSummary.xml");
            objXmlFileNameList.Add("PostCrewAlerts.xml");
            objXmlFileNameList.Add("PostCrewCurrency.xml");
            objXmlFileNameList.Add("PostCrewCurrencyII.xml");
            objXmlFileNameList.Add("PostCrewExpense.xml");
            objXmlFileNameList.Add("PostCrewWorkloadIndex.xml");
            objXmlFileNameList.Add("PostDelayDetail.xml");
            objXmlFileNameList.Add("PostDelaySummary.xml");
            objXmlFileNameList.Add("PostDepartmentAuthorizationDetail.xml");
            objXmlFileNameList.Add("PostDepartmentAuthorizationSummary.xml");
            objXmlFileNameList.Add("POSTDepartmentChargeBackDetailByAircraft.xml");
            objXmlFileNameList.Add("POSTDepartmentChargeBackDetailByRequestor.xml");
            objXmlFileNameList.Add("POSTDepartmentChargeBackSummary.xml");
            objXmlFileNameList.Add("PostDepartmentFlightActivityByRequestor.xml");
            objXmlFileNameList.Add("PostDepartmentUsage.xml");
            objXmlFileNameList.Add("PostDeptAuthDetailStatistics.xml");
            objXmlFileNameList.Add("PostDeptAuthSummaryStatistics.xml");
            objXmlFileNameList.Add("POSTDetailBillingByDeptAuth.xml");
            objXmlFileNameList.Add("POSTDetailBillingByTrip.xml");
            objXmlFileNameList.Add("PostDomesticInternationalSummary.xml");
            objXmlFileNameList.Add("PostEUEmission.xml");
            objXmlFileNameList.Add("POSTExpense.xml");
            objXmlFileNameList.Add("POSTExpenseCatalog.xml");
            objXmlFileNameList.Add("PostFleetFuelHistory.xml");
            objXmlFileNameList.Add("PostFleetFuelPurchaseSummary.xml");
            objXmlFileNameList.Add("PostFleetOperationsHistory.xml");
            objXmlFileNameList.Add("PostFleetUtilization.xml");
            objXmlFileNameList.Add("POSTFlightActitivity.xml");
            objXmlFileNameList.Add("POSTFlightActivity.xml");
            objXmlFileNameList.Add("PostFlightCategoryAnalysis.xml");
            objXmlFileNameList.Add("PostFlightLog.xml");
            objXmlFileNameList.Add("PostFlightLogExceptionReport.xml");
            objXmlFileNameList.Add("PostFlightMainTab.xml");
            objXmlFileNameList.Add("PostFlightOperationsSummary.xml");
            objXmlFileNameList.Add("PostFuelPurchaseSummary.xml");
            objXmlFileNameList.Add("PostFuelSlipTransactions.xml");
            objXmlFileNameList.Add("PostFuelSummaryFuelLocator.xml");
            objXmlFileNameList.Add("PostFuelSummaryTailNumber.xml");
            objXmlFileNameList.Add("PostLegAnalysisDistance.xml");
            objXmlFileNameList.Add("PostLegAnalysisHours.xml");
            objXmlFileNameList.Add("PostNonFlightCurrency.xml");
            objXmlFileNameList.Add("POSTNonPilotFlightAnalysis.xml");
            objXmlFileNameList.Add("PostNonPilotLog.xml");
            objXmlFileNameList.Add("POSTOperationalCostSummary.xml");
            objXmlFileNameList.Add("POSTOperationalCostSummaryCombined.xml");
            objXmlFileNameList.Add("PostPassengerFlightHistory.xml");
            objXmlFileNameList.Add("PostPassengerFlightPurposeSummary.xml");
            objXmlFileNameList.Add("PostPassengerFlightSummary.xml");
            objXmlFileNameList.Add("POSTPassengerSIFLHistory.xml");
            objXmlFileNameList.Add("POSTPaxBillingDetail.xml");
            objXmlFileNameList.Add("POSTPaxBillingSummary.xml");
            objXmlFileNameList.Add("POSTPaymentTypeAnalysis.xml");
            objXmlFileNameList.Add("PostPilotFlightAnalysis-Testing.xml");
            objXmlFileNameList.Add("PostPilotFlightAnalysis.xml");
            objXmlFileNameList.Add("PostPilotLog.xml");
            objXmlFileNameList.Add("PostPilotLogII.xml");
            objXmlFileNameList.Add("PostPilotTypeAnalysis.xml");
            objXmlFileNameList.Add("POSTRequestorChargeBackDetailByDept.xml");
            objXmlFileNameList.Add("PostRouteFrequencyReport.xml");
            objXmlFileNameList.Add("PostStateMileage.xml");
            objXmlFileNameList.Add("POSTSummaryBillingByDeptAuth.xml");
            objXmlFileNameList.Add("POSTSummaryBillingByTrip.xml");
            objXmlFileNameList.Add("PostTimeInTypeReport.xml");
            objXmlFileNameList.Add("POSTVendorPaymentAnalysis.xml");
            objXmlFileNameList.Add("POSTWorkloadIndex.xml");
            objXmlFileNameList.Add("PREAdhocExport.xml");
            objXmlFileNameList.Add("PREAircraftCalendarI.xml");
            objXmlFileNameList.Add("PREAircraftCalendarII.xml");
            objXmlFileNameList.Add("PREAircraftCalendarIII.xml");
            objXmlFileNameList.Add("PREAircraftCalendarIV.xml");
            objXmlFileNameList.Add("PREAircraftStatus.xml");
            objXmlFileNameList.Add("PREComponentStatus.xml");
            objXmlFileNameList.Add("PRECorporateCalendar.xml");
            objXmlFileNameList.Add("PRECorporateItinerary.xml");
            objXmlFileNameList.Add("PRECQCustomerProfile.xml");
            objXmlFileNameList.Add("PRECrewCalendarCrew.xml");
            objXmlFileNameList.Add("PRECrewCalendarDate.xml");
            objXmlFileNameList.Add("PREDailyBaseActivityI.xml");
            objXmlFileNameList.Add("PREDailyBaseActivityII.xml");
            objXmlFileNameList.Add("PREDestination.xml");
            objXmlFileNameList.Add("PREFleetCalendarAircraft.xml");
            objXmlFileNameList.Add("PREFleetCalendarDate.xml");
            objXmlFileNameList.Add("PREFleetScheduleAircraft.xml");
            objXmlFileNameList.Add("PREFleetScheduleAndPaxInfoDate.xml");
            objXmlFileNameList.Add("PREFleetScheduleDate.xml");
            objXmlFileNameList.Add("PREFlightFollowingLog.xml");
            objXmlFileNameList.Add("PreFlightMainTab.xml");
            objXmlFileNameList.Add("PREGADesk.xml");
            objXmlFileNameList.Add("PREGeneralDeclarationAPDX.xml");
            objXmlFileNameList.Add("PREGeneralDeclarationAPDXpmSub.xml");
            objXmlFileNameList.Add("PREGeneralDeclarationSub.xml");
            objXmlFileNameList.Add("PREHoursLandingsProjection.xml");
            objXmlFileNameList.Add("PREMaintenanceAlerts.xml");
            objXmlFileNameList.Add("PREMonthlyCrewCalendar.xml");
            objXmlFileNameList.Add("PREMonthlyFleetCalendar.xml");
            objXmlFileNameList.Add("PREMonthlyScheduleCalendar.xml");
            objXmlFileNameList.Add("PREPassengerItinerary.xml");
            objXmlFileNameList.Add("PREPlannerScheduleCalendar.xml");
            objXmlFileNameList.Add("PREPRETSSummary.xml");
            objXmlFileNameList.Add("PREScheduleCalendarBusinessWeek.xml");
            objXmlFileNameList.Add("PREScheduleCalendarByWeek.xml");
            objXmlFileNameList.Add("PREScheduleDayCalendar.xml");
            objXmlFileNameList.Add("PREStandardServiceForm.xml");
            objXmlFileNameList.Add("PRETripCostQuote.xml");
            objXmlFileNameList.Add("PRETripSheetAircraftAdditionalSub.xml");
            objXmlFileNameList.Add("PRETripSheetAlerts.xml");
            objXmlFileNameList.Add("PRETripSheetAlertsSub.xml");
            objXmlFileNameList.Add("PRETripSheetAudit.xml");
            objXmlFileNameList.Add("PRETripSheetBestFuelVendorSub.xml");
            objXmlFileNameList.Add("PRETripsheetCancellation.xml");
            objXmlFileNameList.Add("PRETripSheetGeneralDeclarationAPDXpmSub.xml");
            objXmlFileNameList.Add("PRETripSheetGeneralDeclarationAPDXSub.xml");
            objXmlFileNameList.Add("PRETripSheetGeneralDeclarationSub.xml");
            objXmlFileNameList.Add("PRETripSheetMainAlertsSub.xml");
            objXmlFileNameList.Add("PRETripSheetMainSub.xml");
            objXmlFileNameList.Add("PRETripSheetPassengerNotesSub.xml");
            objXmlFileNameList.Add("PRETripSheetPAXAlertSub.xml");
            objXmlFileNameList.Add("PRETripExceptions.xml");
            objXmlFileNameList.Add("PRETSCanPassForm.xml");
            objXmlFileNameList.Add("PRETSCrewDutyOverview.xml");
            objXmlFileNameList.Add("PRETSFlightLogInformation.xml");
            objXmlFileNameList.Add("PRETSGeneralDeclaration.xml");
            objXmlFileNameList.Add("PRETSInternationalData.xml");
            objXmlFileNameList.Add("PRETSOverview.xml");
            objXmlFileNameList.Add("PRETSPairForm.xml");
            objXmlFileNameList.Add("PRETSPaxProfile.xml");
            objXmlFileNameList.Add("PRETSPaxTravelAuthorization.xml");
            objXmlFileNameList.Add("PRETSTripSheetAlert.xml");
            objXmlFileNameList.Add("PRETSTripSheetChecklist.xml");
            objXmlFileNameList.Add("PRETSTripSheetItinerary.xml");
            objXmlFileNameList.Add("PRETSTripSheetMain.xml");
            objXmlFileNameList.Add("PREUVTripForm.xml");
            objXmlFileNameList.Add("PREWeeklyCalendar.xml");
            objXmlFileNameList.Add("PREWeeklyCalendarII.xml");
            objXmlFileNameList.Add("PREWeeklyCrewScheduleCalendar.xml");
            objXmlFileNameList.Add("PREWeeklyDetailScheduleCalendar.xml");
            objXmlFileNameList.Add("PREWeeklyFleetScheduleCalendar.xml");
            objXmlFileNameList.Add("UTAirportPairs.xml");
            objXmlFileNameList.Add("UTItineraryPlanning.xml");
            objXmlFileNameList.Add("UTWindsOnWorldAirRoutes.xml");

            return objXmlFileNameList;
        }
    }
}
