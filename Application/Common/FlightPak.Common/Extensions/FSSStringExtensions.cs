﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Extensions
{
        public static class FSSStringExtensions
        {
            public static string GetStringOrNull(this Object value)
            {
                if (value != null)
                {
                    return value.ToString();
                }
                return "<span style='font-weight:bold'>NULL</span>";
            }
        }
}
