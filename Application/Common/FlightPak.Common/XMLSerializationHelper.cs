﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Xml;
using System.Text;
using System.IO;

namespace FlightPak.Common
{
    /// <summary>
    /// XMl serialization helper class, which can be used to serialize any type into xml string and deseriali back.
    /// </summary>
    public class XMLSerializationHelper
    {
        /// <summary>
        /// Serializes the given object in to xml string.
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="value">Given object</param>
        /// <returns>xml string</returns>
        public static string Serialize<T>(T value)
        {
            if (value == null)
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
            settings.Indent = false;
            settings.OmitXmlDeclaration = false;

            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Deserializes the xml string into the given type
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="xml">xml string</param>
        /// <returns>given entity</returns>
        public static T Deserialize<T>(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return default(T);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlReaderSettings settings = new XmlReaderSettings();

            using (StringReader textReader = new StringReader(xml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }
    }
}