﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class Email
    {
        public const string EmailConstant = "E";
        public const string CalendarInvites = "CI";
        public class Status
        {
            public const string NotDone = "N";
            public const string Done = "D";
        }
    }

    public class EmailTemplates
    {
        public const long NewUserCreationTemplate = 2;
        public const long ChangePasswordTemplate = 3;
        public const long ResetPasswordTemplate = 4;
        public const long LockedUserTemplate = 5;
    }

    public class EmailSubject
    {
        public const string UnAssignedTrip = "Unassigned from";
        public const string AssignedTrip = "AssignedTrip";
        public const string UpdateTrip = "Update";
        public const string SubjectUnAssignTrip = "Some unassignments have been made in the trip, Kindly look at the following attachment";
        public const string SubjectUpdateTrip = "Some updations have been made in the trip, Kindly look at the following attachment";
    }
}
