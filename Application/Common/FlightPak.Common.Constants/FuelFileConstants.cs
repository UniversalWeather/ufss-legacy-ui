﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;

namespace FlightPak.Common.Constants
{
    public class FuelFileConstants
    {
        public const string FuelFileData = "FuelFileData1";
        public const string CsvFileHeaders = "CsvFileHeaders";
        public const string ErrorMessageNoFileSelected = "No file uploaded.";
        public const string FuelVendor = "FuelVendor";
        public const string ErrorMessageWrongFileTypeUpload = "Upload Fail. Please upload CSV format file.";
        public const string XsltParserID = "XsltParserID";
        public const string InvalidFuelRecords = "InvalidFuelRecords";
        public const string XSLTFileNameForErrorLog = "ErrorLogUpload";
        public const string FirstXSLTPart = " <xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\""
                                             + "     xmlns:msxml=\"urn:schemas-microsoft-com:xslt\""
                                             + "     xmlns:exslt=\"http://exslt.org/common\""
                                             + "     exclude-result-prefixes=\"exslt msxml\""
                                             + "     extension-element-prefixes=\"exslt msxml\">"
                                             + " <xsl:variable name='newline'><xsl:text>&#10;</xsl:text></xsl:variable>"
                                             +"  <xsl:variable name=\"delimiter\" select=\"','\" />"
                                             +"  <xsl:variable name='pattern' select=\"'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\\+\\$ '\"></xsl:variable>"
                                             + " <xsl:output method=\"html\" />";
        public const string LastXSLTPart = "</xsl:stylesheet>";
        public const string FlightpakFuelID = "FlightpakFuelID";
    }
}