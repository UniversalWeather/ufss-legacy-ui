﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class SequenceNumber
    {

        public const string MasterModuleCurrentNo = "MasterModuleCurrentNo";
        public const string PreflightCurrentNo = "PreflightCurrentNo";
        public const string PostflightCurrentNo = "PostflightCurrentNo";
        public const string PostflightSlipCurrentNo = "PostflightSlipCurrentNo";
        public const string UtilityCurrentNo = "UtilityCurrentNo";
        public const string CharterQuoteCurrentNo = "CharterQuoteCurrentNo";
        public const string CorpReqCurrentNo = "CorpReqCurrentNo";
        public const string FuelVendorCurrentNo = "FuelVendorCurrentNo";
        public const string NonFunctionalCurrentNo = "NonFunctionalCurrentNo";
        public const string TripCurrentNo = "TripCurrentNo";
        public const string TripLogCurrentNo = "TripLogCurrentNo";

    }
}
