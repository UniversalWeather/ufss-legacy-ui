﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class ProcedureConstants
    {
        public readonly IList<string> IProcedureNames = new List<string> {
            // Add your proc names here with out any spaces.
            "spGetReportCrewDefinitionInformation",
            "spGetReportCrewInformation",
            "spGetReportCrewPassportInformation",
            "spGetReportCrewRosterVisaInformation",
            "spGetReportFleetProfileInformationSub2",
            "spGetReportFleetProfileMhtmlInformation",
            "spGetReportPassengerAdditionalInformation",
            "spGetReportPassengerPassportInformation",
            "spGetReportPassengerRequestor1ExportInformation",
            "spGetReportPassengerRequestor1Information",
            "spGetReportPassengerRequestor1MhtmlInformation",
            "spGetReportPassengerRequestor2Information",
            "spGetReportPassengerRequestor2MhtmlInformation",
            "spGetReportPassengerVISAInformation",
            "spGetReportPRETSCrewAdditionalInformation",
            "spGetReportPRETSCrewInformation",
            "spGetReportPRETSExport_Main",
            "spGetReportPRETSGeneralDeclarationAPDX1Sub",
            "spGetReportPRETSGeneralDeclarationAPDXExport",
            "spGetReportPRETSGeneralDeclarationAPDXpm1Sub",
            "spGetReportPRETSGeneralDeclarationAPDXpmExport",
            "spGetReportPRETSGeneralDeclarationExport",
            "spGetReportPRETSGeneralDeclarationSub2",
            "spGetReportPRETSGeneralDeclarationSub3",
            "spGetReportPRETSGeneralDeclarationSub4",
            "spGetReportPRETSInternationalDataCrewInformation",
            "spGetReportPRETSInternationalDataExportInformation",
            "spGetReportPRETSInternationalDataPaxInformation",
            "spGetReportPRETSPAXAdditionalInformation",
            "spGetReportPRETSPAXNoFlyInformation",
            "spGetReportPRETSPAXNoFlyInformation1",
            "spGetReportPRETSPaxProfileExport",
            "spGetReportPRETSPaxProfileSub",
            "spGetReportPRETSTripSheetExportInformation",
            "spGetReportPRETSVISAInfo",
            "spGetReportCQPaxProfileInformation",
            "spGetReportCrewExportInformation",
	    "spGetReportDBCharterQuoteCustomer"
        }.AsReadOnly();
    }
}
