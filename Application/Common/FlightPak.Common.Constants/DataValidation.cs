﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    /// <summary>
    /// Prefix and suffix space should be added here.
    /// </summary>
    public class DataValidation
    {
        public const string MaximumLength = " maximum length exceeds";

        public const string IsRequired = " is Required";

        public const string YearValidation = " should be 1900 to 2100";

        public const string InvalidEmailFormat = " - Invalid Format";

        public const string LatitudeDegree = " 0-90";

        public const string LatitudeMinutes = " 0-60";

        public const string LongitudeDegree = " 0-180";

        public const string LongitudeMinutes = " 0-60";

        public const string InvalidFormat = " Invalid Format";

        public const string TripMGRDefaultStatus = " Should Be 'T''W''U''C''H'";
        
        public const string Specdec = " Range should be between 0 and 3";

        public const string ConstantMach = " for Constant Mach";

        public const string LongRangeCruise = " for Long Range Cruise";

        public const string HighSpeedCruise = " for High Speed Cruise";

        public const string ReportLogoMessage = "For best results, upload a 3.98\" x 1.25\" vector-based image with a minimum 150 DPI resolution.";

        public const string DeletionNotAllowed = "This record is already in use. Deletion is not allowed.";
    }
}
