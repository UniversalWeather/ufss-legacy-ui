﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common
{
    public class ModuleNameConstants
    {
        #region Database
        public class Database
        {
            //This is used to display module name in error display of caught exceptions 
            public const string Account = "Account";
            public const string Aircraft = "Aircraft";
            public const string AircraftDuty = "Aircraft Duty";
            public const string Airport = "Airport";
            public const string Authorization = "Authorization";
            public const string Budget = "Budget";
            public const string Catering = "Catering";
            public const string Client = "Client";
            public const string Company = "Company";
            public const string ConversionTable = "Conversion Table";
            public const string Country = "Country";
            public const string CQCustomer = "CQCustomer";
            public const string CQCustomerContact = "CQCustomerContact";
            public const string Crew = "Crew";
            public const string CrewAircraftAssigned = "Crew Aircraft Assigned";
            public const string CrewCheckList = "Crew CheckList";
            public const string CrewCheckListDetail = "Crew CheckList Detail";
            public const string CrewCurrency = "Crew Currency";
            public const string CrewDefinition = "Crew Definition";
            public const string CrewDutyRules = "Crew Duty Rules";
            public const string CrewDutyType = "Crew Duty Type";
            public const string CrewGroup = "Crew Group";
            public const string CrewGroupOrder = "Crew Group Order";
            public const string CrewInformation = "Crew Information";
            public const string CrewPassengerPassport = "Crew Passenger Passport";
            public const string CrewPassengerVisa = "Crew Passenger Visa";
            public const string CrewRating = "Crew Rating";
            public const string CrewRosterAdditionalInfo = "Crew Roster Additional Information";
            public const string CrewRosterAircraftType = "Crew Roster Aircraft Type";
            public const string CustomAddress = "Custom Address";
            public const string Customer = "Customer";
            public const string CustomerLicense = "Customer License";
            public const string CustomerModuleAccess = "Customer Module Access";
            public const string CustomFlightLog = "Custom Flight Log";
            public const string CustomPilotLog = "Custom Pilot Log";
            public const string DBGeneralInfo = "DB General Info";
            public const string DelayType = "Delay Type";
            public const string Department = "Department";
            public const string DepartmentAuthorization = "Department Authorization";
            public const string DepartmentGroup = "Department Group";
            public const string DepartmentGroupOrder = "Department Group Order";
            public const string DSTRegion = "DST Region";
            public const string EmergencyContact = "Emergency Contact";
            public const string ExchangeRate = "Exchange Rate";
            public const string FareLevel = "Fare Level";
            public const string FBO = "FBO";
            public const string FeeSchedule = "Fee Schedule";
            public const string FeeGroup = "Fee Group";
            public const string FileWarehouse = "File Warehouse";
            public const string Fleet = "Fleet";
            public const string FleetChargeRate = "Fleet Charge Rate";
            public const string FleetChargeHistory = "FleetChargeHistory";
            public const string FleetCharterRate = "Fleet Charter Rate";
            public const string FleetComponent = "Fleet Component";
            public const string FleetForeCast = "Fleet ForeCast";
            public const string FleetGroup = "Fleet Group";
            public const string FlightCategory = "Flight Category";
            public const string FlightPurpose = "Flight Purpose";
            public const string FleetGroupOrder = "Fleet Group Order";
            public const string FleetInformation = "Fleet Information";
            public const string FleetNewCharterRate = "Fleet New Charter Rate";
            public const string FleetPair = "Fleet Pair";
            public const string FleetProfileDefinition = "Fleet Profile Definition";
            public const string FleetProfile = "Fleet Profile";
            public const string FleetProfileInformation = "Fleet Profile Information";
            public const string FPBookmark = "FP Bookmark";
            public const string FuelLocator = "Fuel Locator";
            public const string FuelVendor = "Fuel Vendor";
            public const string Hotel = "Hotel";
            public const string LeadSource = "Lead Source Catalog";
            public const string LostBusiness = "Lost Business";
            public const string Metro = "Metro";
            public const string Passenger = "Passenger";
            public const string PassengerAdditionalInfo = "Passenger Additional Information";
            public const string PassengerGroup = "Passenger Group";
            public const string PassengerGroupOrder = "Passenger Group Order";
            public const string PassengerInformation = "Passenger Information";
            public const string PassengerRequestor = "Passenger/Requestor";
            public const string PayableVendor = "Payable Vendor";
            public const string PaymentType = "Payment Type";
            public const string RoomType = "Room Type";
            public const string Runway = "Runway";
            public const string SalesPerson = "Sales Person";
            public const string TailHistory = "Tail History";
            public const string TenthMinConversion = "Tenth Min Conversion";
            public const string Transport = "Transport";
            public const string TravelCoordinator = "Travel Coordinator";
            public const string TravelCoordinatorFleet = "Travel Coordinator Fleet";
            public const string TravelCoordinatorRequestor = "Travel Coordinator Requestor";
            public const string TripManagerCheckList = "Preflight CheckList";
            public const string TripManagerCheckListGroup = "Preflight CheckList Group";
            public const string TripPrivacySettings = "Trip Privacy Settings";
            public const string TSAClear = "TSA Clear";
            public const string TSANoFly = "TSA No Fly";
            public const string TSASelect = "TSA Select";
            public const string TSDefinitionLog = "TS Definition Log";
            public const string UMPermission = "UM Permission";
            public const string UserGroup = "User Group";
            public const string UserGroupMapping = "User Group Mapping";
            public const string UserGroupPermission = "User Group Permission";
            public const string UserMaster = "User Master";
            public const string UserPassword = "User Password";
            public const string Vendor = "Vendor";
            public const string VendorContact = "Vendor Contact";
            public const string WindStat = "Wind Stat";
            public const string WorldWind = "World Wind";
            public const string Global = "Global";
            public const string FuleFileXSLTGenerator = "FuleFileXSLTGenerator";
            public const string HistoricalFuelPrice = "HistoricalFuelPrice";
        }
        #endregion

        #region Database Reports

        public class DatabaseReport
        {
            public const string PassengerRequestor = "Passengers/Requestors";
         
            public const string Account = "Accounts";
            public const string AircraftDuty = "Aircraft Duty Types";
            public const string Aircraft = "Aircraft Type";
            public const string Airport = "Airport";
            public const string CrewCheckList = "Crew Checklist";
            public const string CrewDutyType = "Crew Duty Types";
            public const string DelayType = "Delay Types";
            public const string DepartmentAuthorization = "Department/Authorization";
            public const string FlightCategory = "Flight Categories";
            public const string FlightPurpose = "Flight Purposes";
            public const string PassengerInformation = "Passenger Additional Information";
            public const string CrewReports = "Crew Roster";
            public const string TripManagerChecklist = "Tripsheet Check List";
            public const string FleetProfile = "Fleet Profile";
            public const string LeadSource = "Lead Source Catalog";

        }

        #endregion

        #region Preflight
        public class Preflight
        {
            #region Preflight
            public const string TripManager = "Preflight";
            public const string PreflightMain = "Preflight";
            public const string PreflightLeg = "Preflight - Leg";
            public const string PreflightCrew = "Preflight - Crew";
            public const string PreflightPAX = "Preflight - PAX";
            public const string PreflightLogistics = "Preflight - Logistics";
            public const string PreflightUWAServices = "Preflight - UWA Services";
            public const string PreflightSIFL = "Preflight - SIFL";
            public const string PreflightAPIS = "Preflight - APIS";
            public const string PreflightCorporateRequest = "Preflight - Corporate Request";

            #region ATS RoomType Mapping

            public const string PreflightATSRoomTypeSingle = "SINGLE";
            public const string PreflightATSRoomTypeSingleRoom = "SINGLE ROOM";
            public const string PreflightATSRoomTypeDB = "DB";
            public const string PreflightATSRoomTypeDOUBLE = "DOUBLE";
            public const string PreflightATSRoomTypeDOUBLEFORTWO = "DOUBLE ROOM FOR TWO PEOPLE";
            public const string PreflightATSRoomTypeAPARTMENT = "APARTMENT";
            public const string PreflightATSRoomTypeBUNGALOW = "BUNGALOW";
            public const string PreflightATSRoomTypeCABIN = "CABIN";
            public const string PreflightATSRoomTypeSUITE = "SUITE";
            public const string PreflightATSRoomTypeOTHER = "OTHER";

            #endregion
            #region ATS RoomType Description

            public const string PreflightATSRoomTypeDescSTANDARD = "STANDARD";
            public const string PreflightATSRoomTypeDescAMBASSADOR = "AMBASSADOR";
            public const string PreflightATSRoomTypeDescDELUXE = "DELUXE";
            public const string PreflightATSRoomTypeDescEX = "EX";
            public const string PreflightATSRoomTypeDescEXECUTIVE = "EXECUTIVE";
            public const string PreflightATSRoomTypeDescFOURPOSTER = "FOUR-POSTER";
            public const string PreflightATSRoomTypeDescONEBEDROOM = "ONE-BEDROOM";
            public const string PreflightATSRoomTypeDescOT = "OT";
            public const string PreflightATSRoomTypeDescOTHER = "OTHER";

            #endregion

            /// <summary>
            /// Equals to string "C"
            /// </summary>
            public const string Employee_Type_Controlled = "C";

            /// <summary>
            /// Equals to string "N"
            /// </summary>
            public const string Employee_Type_NonControlled = "N";

            /// <summary>
            /// Equals to string "G"
            /// </summary>
            public const string Employee_Type_Guest = "G";

            #endregion

            #region Schedule Calendar
            /// <summary>
            /// Equals to string "ScheduleCalendarBase"
            /// </summary>
            public const string ScheduleCalendarBase = "ScheduleCalendarBase";
            /// <summary>
            ///  Equals to string "ScheduleCalendar"
            /// </summary>
            public const string ScheduleCalendar = "ScheduleCalendar";

            /// <summary>
            ///  Equals to string "View_WeeklyFleet"
            /// </summary>
            public const string WeeklyFleet = "View_WeeklyFleet";

            /// <summary>
            ///  Equals to string "View_WeeklyCrew"
            /// </summary>
            public const string WeeklyCrew = "View_WeeklyCrew";

            /// <summary>
            ///  Equals to string "View_WeeklyDetail"
            /// </summary>
            public const string WeeklyDetail = "View_WeeklyDetail";

            /// <summary>
            /// Equals to string "View_Weekly"
            /// </summary>
            public const string WeeklyMain = "View_Weekly";

            /// <summary>
            /// Equals to string "View_BusinessWeek"
            /// </summary>
            public const string BusinessWeek = "View_BusinessWeek";

            /// <summary>
            /// Equals to string "View_Monthly"
            /// </summary>
            public const string Month = "View_Monthly";

            /// <summary>
            ///  Equals to string "View_Corporate"
            /// </summary>
            public const string Corporate = "View_Corporate";

            /// <summary>
            /// Equals to string "View_Day"
            /// </summary>
            public const string Day = "View_Day";
            /// <summary>
            /// Equals to string "View_Planner"
            /// </summary>
            public const string Planner = "View_Planner";
            /// <summary>
            /// Equals to string "Fleet Calendar Entry"
            /// </summary>
            public const string FleetCalendarEntry = "Fleet Calendar Entry";
            /// <summary>
            /// Equals to string "Crew Calendar Entry"
            /// </summary>
            public const string CrewCalendarEntry = "Crew Calendar Entry";
            /// <summary>
            /// Equals to string "Advanced Filter Settings"
            /// </summary>
            public const string AdvancedFilterSettings = "Advanced Filter Settings";
            /// <summary>
            /// Equals to string "Advanced Display Options"
            /// </summary>
            public const string AdvancedDisplayOptions = "Advanced Display Options";
            /// <summary>
            /// Equals to string "Quick Fleet Entry"
            /// </summary>
            public const string QuickFleetEntry = "Quick Fleet Entry";
            /// <summary>
            /// Equals to string "Quick Crew Entry"
            /// </summary>
            public const string QuickCrewEntry = "Quick Crew Entry";
            /// <summary>
            /// Equals to string "Legend"
            /// </summary>
            public const string Legend = "Legend";
            /// <summary>
            /// Equals to string "TailNum"
            /// </summary>
            public const string TailNum = "TailNum";
            /// <summary>
            /// Equals to string "CrewCD"
            /// </summary>
            public const string CewCode = "CrewCD";
            /// <summary>
            /// Equals to string "Description"
            /// </summary>
            public const string Resource_Description = "Description";

            /// <summary>
            /// Equals to string "Fleet"
            /// </summary>
            public const string FleetMode = "Fleet";
            #endregion

        }
        #endregion

        #region CorporateRequest
        public class CorporateRequest
        {
            #region CorporateRequest
            public const string CorporateRequestTripManager = "CorporateRequestTrip Manager";
            public const string CorporateRequestMain = "CorporateRequestTrip Manager - Request";
            public const string CorporateRequestLeg = "CorporateRequestTrip Manager - Leg";
            public const string CorporateRequestPAX = "CorporateRequestTrip Manager - PAX";
            public const string CorporateRequestLogistics = "CorporateRequestTrip Manager - Logistics";
            public const string ChangeQueue = "Change Queue";
            public const string CorporateRequestTransfer = "Corporate Request - Transfer";
            public const string CorporateRequestDeny = "Corporate Request - Deny";
            public const string CorporateRequestMarkAccepted = "Corporate Request - MarkAccepted";

          
            /// <summary>
            /// Equals to string "C"
            /// </summary>
            public const string Employee_Type_Controlled = "C";

            /// <summary>
            /// Equals to string "N"
            /// </summary>
            public const string Employee_Type_NonControlled = "N";

            /// <summary>
            /// Equals to string "G"
            /// </summary>
            public const string Employee_Type_Guest = "G";

            #endregion
        }
        #endregion

        #region Postflight
        public class Postflight
        {
            public const string POMain = "Flight Log Manager - Postflight";
            public const string POLeg = "Flight Log Manager - Legs";
            public const string POCrew = "Flight Log Manager - Crew";
            public const string POPassenger = "Flight Log Manager - Passenger";
            public const string POExpense = "Flight Log Manager - Expenses";
            public const string POOtherCrewDuty = "Flight Log Manager - Other Crew Duty";
            public const string POExpenseCatalog = "Flight Log Manager - Expense Catalog";
        }
        #endregion

        #region Reports
        public class Reports
        {
            public const string ReportsConstantName = "Reports";
        }
        #endregion

        #region System Administration
        public class SysAdmin
        {
            public const string SystemAdmin = "System Administration";
        }
        #endregion

        #region Charter Quote
        public class CharterQuote
        {
            public const string CQFile = "Charter Quote - File";
            public const string CQOnFile = "Charter Quote - Quote on File";
            public const string CQLeg = "Charter Quote - Quote & Leg Details";
            public const string CQPax = "Charter Quote - Passenger";
            public const string CQLogistics = "Charter Quote - Logistics";
            public const string CQReports = "Charter Quote - Reports";
            public const string CQExpQuote = "Express Quote";
        }
        #endregion

        #region Utilities
        public class Utilities
        {
            //This is used to display module name in error display of caught exceptions 
            public const string AirportPairs = "Airport Pairs";
            public const string AirportPairLegs = "Airport Pairs Legs";
            public const string ItineraryPlan = "Itinerary Plan";
            public const string ItineraryPlanLegs = "Itinerary Plan Legs";
            public const string Locator = "Locator";
            public const string WorldWinds = "World Winds";
            public const string WorldClock = "World Clock";
            public const string RecentActivities = "Recent Activities";
            public const string Bookmark = "Bookmark";
        }
        #endregion

        #region Search
        public class Search
        {
            public const string BasicSearch = "Global Search";
        }
        #endregion
    }
}
