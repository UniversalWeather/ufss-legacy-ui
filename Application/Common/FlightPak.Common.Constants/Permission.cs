﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class Permission
    {

        public class Database
        {
            #region Database
            //This is used for checking access permissions for logged in user.
            #region Metro
            public const string ViewMetro = "ViewMetro";
            public const string AddMetro = "AddMetro";
            public const string EditMetro = "EditMetro";
            public const string DeleteMetro = "DeleteMetro";
            #endregion

            #region Accounts
            public const string ViewAccounts = "ViewAccounts";
            public const string AddAccounts = "AddAccounts";
            public const string EditAccounts = "EditAccounts";
            public const string DeleteAccounts = "DeleteAccounts";
            #endregion

            #region AccountsPopup
            public const string ViewAccountsPopup = "ViewAccountsPopup";
            public const string AddAccountsPopup = "AddAccountsPopup";
            public const string EditAccountsPopup = "EditAccountsPopup";
            public const string DeleteAccountsPopup = "DeleteAccountsPopup";
            #endregion

            #region BudgetCatalog
            public const string ViewBudgetCatalog = "ViewBudget";
            public const string AddBudgetCatalog = "AddBudget";
            public const string EditBudgetCatalog = "EditBudget";
            public const string DeleteBudgetCatalog = "DeleteBudget";
            #endregion

            #region ClientCode
            public const string ViewClientCode = "ViewClientCode";
            public const string AddClientCode = "AddClientCode";
            public const string EditClientCode = "EditClientCode";
            public const string DeleteClientCode = "DeleteClientCode";
            #endregion

            #region ClientCodePopup
            public const string ViewClientCodePopup = "ViewClientCodePopup";
            public const string AddClientCodePopup = "AddClientCodePopup";
            public const string EditClientCodePopup = "EditClientCodePopup";
            public const string DeleteClientCodePopup = "DeleteClientCodePopup";
            #endregion

            #region CompanyMasterPopup
            public const string ViewCompanyMasterPopup = "ViewCompanyMasterPopup";
            public const string AddCompanyMasterPopup = "AddCompanyMasterPopup";
            public const string EditCompanyMasterPopup = "EditCompanyMasterPopup";
            public const string DeleteCompanyMasterPopup = "DeleteCompanyMasterPopup";
            #endregion

            #region CompanyMasterPopupMultipleSelection
            public const string ViewCompanyMasterPopupMultipleSelection = "ViewCompanyMasterPopupMultipleSelection";
            public const string AddCompanyMasterPopupMultipleSelection = "AddCompanyMasterPopupMultipleSelection";
            public const string EditCompanyMasterPopupMultipleSelection = "EditCompanyMasterPopupMultipleSelection";
            public const string DeleteCompanyMasterPopupMultipleSelection = "DeleteCompanyMasterPopupMultipleSelection";
            #endregion

            #region CompanyProfileCatalog
            public const string ViewCompanyProfileCatalog = "ViewCompanyProfile";
            public const string AddCompanyProfileCatalog = "AddCompanyProfile";
            public const string EditCompanyProfileCatalog = "EditCompanyProfile";
            public const string DeleteCompanyProfileCatalog = "DeleteCompanyProfile";
            #endregion

            #region CountryCatalog
            public const string ViewCountryCatalog = "ViewCountry";
            public const string AddCountryCatalog = "AddCountry";
            public const string EditCountryCatalog = "EditCountry";
            public const string DeleteCountryCatalog = "DeleteCountry";
            #endregion

            #region CustomFlightLog
            public const string ViewCustomFlightLog = "ViewCustomFlightLog";
            public const string AddCustomFlightLog = "AddCustomFlightLog";
            public const string EditCustomFlightLog = "EditCustomFlightLog";
            public const string DeleteCustomFlightLog = "DeleteCustomFlightLog";
            #endregion

            #region CustomPilotLog
            public const string ViewCustomPilotLog = "ViewCustomPilotLog";
            public const string AddCustomPilotLog = "AddCustomPilotLog";
            public const string EditCustomPilotLog = "EditCustomPilotLog";
            public const string DeleteCustomPilotLog = "DeleteCustomPilotLog";
            #endregion

            #region DepartmentAuthorizationCatalog
            public const string ViewDepartmentAuthorizationCatalog = "ViewDepartmentAuthorization";
            public const string AddDepartmentAuthorizationCatalog = "AddDepartmentAuthorization";
            public const string EditDepartmentAuthorizationCatalog = "EditDepartmentAuthorization";
            public const string DeleteDepartmentAuthorizationCatalog = "DeleteDepartmentAuthorization";
            #endregion

            #region DepartmentGroup
            public const string ViewDepartmentGroup = "ViewDepartmentGroup";
            public const string AddDepartmentGroup = "AddDepartmentGroup";
            public const string EditDepartmentGroup = "EditDepartmentGroup";
            public const string DeleteDepartmentGroup = "DeleteDepartmentGroup";
            #endregion

            #region ExchangeRateMasterpopup
            public const string ViewExchangeRateMasterpopup = "ViewExchangeRateMasterpopup";
            public const string AddExchangeRateMasterpopup = "AddExchangeRateMasterpopup";
            public const string EditExchangeRateMasterpopup = "EditExchangeRateMasterpopup";
            public const string DeleteExchangeRateMasterpopup = "DeleteExchangeRateMasterpopup";
            #endregion

            #region ExchangeRatesCatalog
            public const string ViewExchangeRatesCatalog = "ViewExchangeRates";
            public const string AddExchangeRatesCatalog = "AddExchangeRates";
            public const string EditExchangeRatesCatalog = "EditExchangeRates";
            public const string DeleteExchangeRatesCatalog = "DeleteExchangeRates";
            #endregion

            #region MetroMasterPopup
            public const string ViewMetroMasterPopup = "ViewMetroMasterPopup";
            public const string AddMetroMasterPopup = "AddMetroMasterPopup";
            public const string EditMetroMasterPopup = "EditMetroMasterPopup";
            public const string DeleteMetroMasterPopup = "DeleteMetroMasterPopup";
            #endregion

            #region PaymentType
            public const string ViewPaymentType = "ViewPaymentType";
            public const string AddPaymentType = "AddPaymentType";
            public const string EditPaymentType = "EditPaymentType";
            public const string DeletePaymentType = "DeletePaymentType";
            #endregion

            #region PaymentTypePopup
            public const string ViewPaymentTypePopup = "ViewPaymentTypePopup";
            public const string AddPaymentTypePopup = "AddPaymentTypePopup";
            public const string EditPaymentTypePopup = "EditPaymentTypePopup";
            public const string DeletePaymentTypePopup = "DeletePaymentTypePopup";
            #endregion

            #region SiflRateCatalog
            public const string ViewSiflRateCatalog = "ViewSiflRate";
            public const string AddSiflRateCatalog = "AddSiflRate";
            public const string EditSiflRateCatalog = "EditSiflRate";
            public const string DeleteSiflRateCatalog = "DeleteSiflRate";
            #endregion

            #region TenthMinConversion
            public const string ViewTenthMinConversion = "ViewTenthMinConversion";
            public const string AddTenthMinConversion = "AddTenthMinConversion";
            public const string EditTenthMinConversion = "EditTenthMinConversion";
            public const string DeleteTenthMinConversion = "DeleteTenthMinConversion";
            #endregion

            #region TripManagerCheckListCatalog
            public const string ViewTripManagerCheckListCatalog = "ViewTripManagerCheckList";
            public const string AddTripManagerCheckListCatalog = "AddTripManagerCheckList";
            public const string EditTripManagerCheckListCatalog = "EditTripManagerCheckList";
            public const string DeleteTripManagerCheckListCatalog = "DeleteTripManagerCheckList";
            #endregion

            #region TripManagerCheckListGroupCatalog
            public const string ViewTripManagerCheckListGroupCatalog = "ViewTripManagerCheckListGroup";
            public const string AddTripManagerCheckListGroupCatalog = "AddTripManagerCheckListGroup";
            public const string EditTripManagerCheckListGroupCatalog = "EditTripManagerCheckListGroup";
            public const string DeleteTripManagerCheckListGroupCatalog = "DeleteTripManagerCheckListGroup";
            #endregion

            #region TripPrivacySettings
            public const string ViewTripPrivacySettings = "ViewTripPrivacySettings";
            public const string AddTripPrivacySettings = "AddTripPrivacySettings";
            public const string EditTripPrivacySettings = "EditTripPrivacySettings";
            public const string DeleteTripPrivacySettings = "DeleteTripPrivacySettings";
            #endregion

            #region AirportCatalog
            public const string ViewAirport = "ViewAirport";
            public const string AddAirport = "AddAirport";
            public const string EditAirport = "EditAirport";
            public const string DeleteAirport = "DeleteAirport";
            #endregion

            #region DaylightSavingTimeRegion
            public const string ViewDaylightSavingTimeRegion = "ViewDaylightSavingTimeRegion";
            public const string AddDaylightSavingTimeRegion = "AddDaylightSavingTimeRegion";
            public const string EditDaylightSavingTimeRegion = "EditDaylightSavingTimeRegion";
            public const string DeleteDaylightSavingTimeRegion = "DeleteDaylightSavingTimeRegion";
            #endregion

            #region AircraftDutyType
            public const string ViewAircraftDutyType = "ViewAircraftDutyType";
            public const string AddAircraftDutyType = "AddAircraftDutyType";
            public const string EditAircraftDutyType = "EditAircraftDutyType";
            public const string DeleteAircraftDutyType = "DeleteAircraftDutyType";
            #endregion

            #region AircraftType
            public const string ViewAircraftType = "ViewAircraftType";
            public const string AddAircraftType = "AddAircraftType";
            public const string EditAircraftType = "EditAircraftType";
            public const string DeleteAircraftType = "DeleteAircraftType";
            #endregion

            #region DelayTypeCatalog
            public const string ViewDelayTypeCatalog = "ViewDelayType";
            public const string AddDelayTypeCatalog = "AddDelayType";
            public const string EditDelayTypeCatalog = "EditDelayType";
            public const string DeleteDelayTypeCatalog = "DeleteDelayType";
            #endregion

            #region FleetChargeHistory
            public const string ViewFleetChargeHistory = "ViewFleetChargeHistory";
            public const string AddFleetChargeHistory = "AddFleetChargeHistory";
            public const string EditFleetChargeHistory = "EditFleetChargeHistory";
            public const string DeleteFleetChargeHistory = "DeleteFleetChargeHistory";
            #endregion

            #region FleetCharterRates
            public const string ViewFleetCharterRates = "ViewFleetCharterRates";
            public const string AddFleetCharterRates = "AddFleetCharterRates";
            public const string EditFleetCharterRates = "EditFleetCharterRates";
            public const string DeleteFleetCharterRates = "DeleteFleetCharterRates";
            #endregion

            #region FleetComponentHistory
            public const string ViewFleetComponentHistory = "ViewFleetComponentHistory";
            public const string AddFleetComponentHistory = "AddFleetComponentHistory";
            public const string EditFleetComponentHistory = "EditFleetComponentHistory";
            public const string DeleteFleetComponentHistory = "DeleteFleetComponentHistory";
            #endregion

            #region FleetForecast
            public const string ViewFleetForecast = "ViewFleetForecast";
            public const string AddFleetForecast = "AddFleetForecast";
            public const string EditFleetForecast = "EditFleetForecast";
            public const string DeleteFleetForecast = "DeleteFleetForecast";
            #endregion

            #region FleetGroupCatalog
            public const string ViewFleetGroupCatalog = "ViewFleetGroup";
            public const string AddFleetGroupCatalog = "AddFleetGroup";
            public const string EditFleetGroupCatalog = "EditFleetGroup";
            public const string DeleteFleetGroupCatalog = "DeleteFleetGroup";
            #endregion

            #region FlightCategory
            public const string ViewFlightCategory = "ViewFlightCategory";
            public const string AddFlightCategory = "AddFlightCategory";
            public const string EditFlightCategory = "EditFlightCategory";
            public const string DeleteFlightCategory = "DeleteFlightCategory";
            #endregion

            #region FlightPurpose
            public const string ViewFlightPurpose = "ViewFlightPurpose";
            public const string AddFlightPurpose = "AddFlightPurpose";
            public const string EditFlightPurpose = "EditFlightPurpose";
            public const string DeleteFlightPurpose = "DeleteFlightPurpose";
            #endregion

            #region FleetProfileAdditionalInfoCatalog
            public const string ViewFleetProfileAdditionalInfoCatalog = "ViewFleetProfileAdditionalInfo";
            public const string AddFleetProfileAdditionalInfoCatalog = "AddFleetProfileAdditionalInfo";
            public const string EditFleetProfileAdditionalInfoCatalog = "EditFleetProfileAdditionalInfo";
            public const string DeleteFleetProfileAdditionalInfoCatalog = "DeleteFleetProfileAdditionalInfo";
            #endregion

            #region FleetProfile
            public const string ViewFleetProfile = "ViewFleetProfile";
            public const string AddFleetProfile = "AddFleetProfile";
            public const string EditFleetProfile = "EditFleetProfile";
            public const string DeleteFleetProfile = "DeleteFleetProfile";
            #endregion

            #region FleetProfileEngineAirframeInfo
            public const string ViewFleetProfileEngineAirframeInfo = "ViewFleetProfileEngineAirframeInfo";
            public const string AddFleetProfileEngineAirframeInfo = "AddFleetProfileEngineAirframeInfo";
            public const string EditFleetProfileEngineAirframeInfo = "EditFleetProfileEngineAirframeInfo";
            public const string DeleteFleetProfileEngineAirframeInfo = "DeleteFleetProfileEngineAirframeInfo";
            #endregion

            #region FleetProfileInternationalData
            public const string ViewFleetProfileInternationalData = "ViewFleetProfileInternationalData";
            public const string AddFleetProfileInternationalData = "AddFleetProfileInternationalData";
            public const string EditFleetProfileInternationalData = "EditFleetProfileInternationalData";
            public const string DeleteFleetProfileInternationalData = "DeleteFleetProfileInternationalData";
            #endregion

            #region Catering
            public const string ViewCatering = "ViewCatering";
            public const string AddCatering = "AddCatering";
            public const string EditCatering = "EditCatering";
            public const string DeleteCatering = "DeleteCatering";
            #endregion

            #region FBOCatalog
            public const string ViewFBO = "ViewFBO";
            public const string AddFBO = "AddFBO";
            public const string EditFBO = "EditFBO";
            public const string DeleteFBO = "DeleteFBO";
            #endregion

            #region FuelLocator
            public const string ViewFuelLocator = "ViewFuelLocator";
            public const string AddFuelLocator = "AddFuelLocator";
            public const string EditFuelLocator = "EditFuelLocator";
            public const string DeleteFuelLocator = "DeleteFuelLocator";
            #endregion

            #region FuelVendor
            public const string ViewFuelVendor = "ViewFuelVendor";
            public const string AddFuelVendor = "AddFuelVendor";
            public const string EditFuelVendor = "EditFuelVendor";
            public const string DeleteFuelVendor = "DeleteFuelVendor";
            #endregion

            #region HotelCatalog
            public const string ViewHotel = "ViewHotel";
            public const string AddHotel = "AddHotel";
            public const string EditHotel = "EditHotel";
            public const string DeleteHotel = "DeleteHotel";
            #endregion

            #region PayableVendor
            public const string ViewPayableVendor = "ViewPayableVendor";
            public const string AddPayableVendor = "AddPayableVendor";
            public const string EditPayableVendor = "EditPayableVendor";
            public const string DeletePayableVendor = "DeletePayableVendor";
            #endregion

            #region TransportCatalog
            public const string ViewTransport = "ViewTransport";
            public const string AddTransport = "AddTransport";
            public const string EditTransport = "EditTransport";
            public const string DeleteTransport = "DeleteTransport";
            #endregion

            #region VendorCatalog
            public const string ViewVendorCatalog = "ViewVendor";
            public const string AddVendorCatalog = "AddVendor";
            public const string EditVendorCatalog = "EditVendor";
            public const string DeleteVendorCatalog = "DeleteVendor";
            #endregion

            #region VendorContacts
            public const string ViewVendorContacts = "ViewVendorContacts";
            public const string AddVendorContacts = "AddVendorContacts";
            public const string EditVendorContacts = "EditVendorContacts";
            public const string DeleteVendorContacts = "DeleteVendorContacts";
            #endregion

            #region CrewCheckListCatalog
            public const string ViewCrewCheckListCatalog = "ViewCrewCheckList";
            public const string AddCrewCheckListCatalog = "AddCrewCheckList";
            public const string EditCrewCheckListCatalog = "EditCrewCheckList";
            public const string DeleteCrewCheckListCatalog = "DeleteCrewCheckList";
            #endregion

            #region CrewDutyRulesCatalog
            public const string ViewCrewDutyRulesCatalog = "ViewCrewDutyRules";
            public const string AddCrewDutyRulesCatalog = "AddCrewDutyRules";
            public const string EditCrewDutyRulesCatalog = "EditCrewDutyRules";
            public const string DeleteCrewDutyRulesCatalog = "DeleteCrewDutyRules";
            #endregion

            #region CrewDutyTypes
            public const string ViewCrewDutyTypes = "ViewCrewDutyTypes";
            public const string AddCrewDutyTypes = "AddCrewDutyTypes";
            public const string EditCrewDutyTypes = "EditCrewDutyTypes";
            public const string DeleteCrewDutyTypes = "DeleteCrewDutyTypes";
            #endregion

            #region CrewGroupCatalog
            public const string ViewCrewGroupCatalog = "ViewCrewGroup";
            public const string AddCrewGroupCatalog = "AddCrewGroup";
            public const string EditCrewGroupCatalog = "EditCrewGroup";
            public const string DeleteCrewGroupCatalog = "DeleteCrewGroup";
            #endregion

            #region CrewRosterAddInfoCatalog
            public const string ViewCrewRosterAddInfoCatalog = "ViewCrewRosterAddInfo";
            public const string AddCrewRosterAddInfoCatalog = "AddCrewRosterAddInfo";
            public const string EditCrewRosterAddInfoCatalog = "EditCrewRosterAddInfo";
            public const string DeleteCrewRosterAddInfoCatalog = "DeleteCrewRosterAddInfo";
            #endregion

            #region CrewRosterChecklistSettings
            public const string ViewCrewRosterChecklistSettings = "ViewCrewRosterChecklistSettings";
            public const string AddCrewRosterChecklistSettings = "AddCrewRosterChecklistSettings";
            public const string EditCrewRosterChecklistSettings = "EditCrewRosterChecklistSettings";
            public const string DeleteCrewRosterChecklistSettings = "DeleteCrewRosterChecklistSettings";
            #endregion

            #region CrewRoster
            public const string ViewCrewRoster = "ViewCrewRoster";
            public const string AddCrewRoster = "AddCrewRoster";
            public const string EditCrewRoster = "EditCrewRoster";
            public const string DeleteCrewRoster = "DeleteCrewRoster";
            #endregion

            #region CustomerAddressCatalog
            public const string ViewCustomerAddressCatalog = "ViewCustomerAddress";
            public const string AddCustomerAddressCatalog = "AddCustomerAddress";
            public const string EditCustomerAddressCatalog = "EditCustomerAddress";
            public const string DeleteCustomerAddressCatalog = "DeleteCustomerAddress";
            #endregion

            #region EmergencyContactCatalog
            public const string ViewEmergencyContactCatalog = "ViewEmergencyContact";
            public const string AddEmergencyContactCatalog = "AddEmergencyContact";
            public const string EditEmergencyContactCatalog = "EditEmergencyContact";
            public const string DeleteEmergencyContactCatalog = "DeleteEmergencyContact";
            #endregion

            #region PassengerAdditionalInfo
            public const string ViewPassengerAdditionalInfo = "ViewPassengerAdditionalInfo";
            public const string AddPassengerAdditionalInfo = "AddPassengerAdditionalInfo";
            public const string EditPassengerAdditionalInfo = "EditPassengerAdditionalInfo";
            public const string DeletePassengerAdditionalInfo = "DeletePassengerAdditionalInfo";
            #endregion

            #region PassengerCatalog
            public const string ViewPassengerCatalog = "ViewPassenger";
            public const string AddPassengerCatalog = "AddPassenger";
            public const string EditPassengerCatalog = "EditPassenger";
            public const string DeletePassengerCatalog = "DeletePassenger";
            #endregion

            #region PassengerRequestor
            public const string ViewPassengerRequestor = "ViewPassengerRequestor";
            public const string AddPassengerRequestor = "AddPassengerRequestor";
            public const string EditPassengerRequestor = "EditPassengerRequestor";
            public const string DeletePassengerRequestor = "DeletePassengerRequestor";
            #endregion

            #region PassengerGroupCatalog
            public const string ViewPassengerGroupCatalog = "ViewPassengerGroup";
            public const string AddPassengerGroupCatalog = "AddPassengerGroup";
            public const string EditPassengerGroupCatalog = "EditPassengerGroup";
            public const string DeletePassengerGroupCatalog = "DeletePassengerGroup";
            #endregion

            #region TravelCoordinator
            public const string ViewTravelCoordinator = "ViewTravelCoordinator";
            public const string AddTravelCoordinator = "AddTravelCoordinator";
            public const string EditTravelCoordinator = "EditTravelCoordinator";
            public const string DeleteTravelCoordinator = "DeleteTravelCoordinator";
            #endregion

            #region TravelCoordinatorAssignedAircraft
            public const string ViewTravelCoordinatorAssignedAircraft = "ViewTravelCoordinatorAssignedAircraft";
            public const string AddTravelCoordinatorAssignedAircraft = "AddTravelCoordinatorAssignedAircraft";
            public const string EditTravelCoordinatorAssignedAircraft = "EditTravelCoordinatorAssignedAircraft";
            public const string DeleteTravelCoordinatorAssignedAircraft = "DeleteTravelCoordinatorAssignedAircraft";
            #endregion

            #region CBPLoginInformation
            public const string ViewCBPLoginInformation = "ViewCBPLoginInformation";
            public const string AddCBPLoginInformation = "AddCBPLoginInformation";
            public const string EditCBPLoginInformation = "EditCBPLoginInformation";
            public const string DeleteCBPLoginInformation = "DeleteCBPLoginInformation";
            #endregion

            #region CrewAdditionalInformation
            public const string ViewCrewAdditionalInformation = "ViewCrewAddlInfoCodes";
            public const string AddCrewAdditionalInformation = "AddCrewAddlInfoCodes";
            public const string EditCrewAdditionalInformation = "EditCrewAddlInfoCodes";
            public const string DeleteCrewAdditionalInformation = "DeleteCrewAddlInfoCodes";
            #endregion

            #region CrewAircraftAssigned
            public const string ViewCrewAircraftAssigned = "ViewCrewAircraftAssigned";
            public const string AddCrewAircraftAssigned = "AddCrewAircraftAssigned";
            public const string EditCrewAircraftAssigned = "EditCrewAircraftAssigned";
            public const string DeleteCrewAircraftAssigned = "DeleteCrewAircraftAssigned";
            #endregion

            #region CrewCurrency
            public const string ViewCrewCurrency = "ViewCrewCurrency";
            public const string AddCrewCurrency = "AddCrewCurrency";
            public const string EditCrewCurrency = "EditCrewCurrency";
            public const string DeleteCrewCurrency = "DeleteCrewCurrency";
            #endregion

            #region CrewPassportCatalog
            public const string ViewCrewPassportCatalog = "ViewCrewPassport";
            public const string AddCrewPassportCatalog = "AddCrewPassport";
            public const string EditCrewPassportCatalog = "EditCrewPassport";
            public const string DeleteCrewPassportCatalog = "DeleteCrewPassport";
            #endregion

            #region CrewRoster-Email Prefernce
            public const string ViewCrewRosterEmailPrefernce = "ViewCrewRoster-Email Prefernce";
            public const string AddCrewRosterEmailPrefernce = "AddCrewRoster-Email Prefernce";
            public const string EditCrewRosterEmailPrefernce = "EditCrewRoster-Email Prefernce";
            public const string DeleteCrewRosterEmailPrefernce = "DeleteCrewRoster-Email Prefernce";
            #endregion

            #region CrewRosterReports
            public const string ViewCrewRosterReports = "ViewCrewRosterReports";
            public const string AddCrewRosterReports = "AddCrewRosterReports";
            public const string EditCrewRosterReports = "EditCrewRosterReports";
            public const string DeleteCrewRosterReports = "DeleteCrewRosterReports";
            #endregion

            #region CrewTypeRating
            public const string ViewCrewTypeRating = "ViewCrewTypeRating";
            public const string AddCrewTypeRating = "AddCrewTypeRating";
            public const string EditCrewTypeRating = "EditCrewTypeRating";
            public const string DeleteCrewTypeRating = "DeleteCrewTypeRating";
            #endregion

            #region FBOChoiceSelection
            public const string ViewFBOChoiceSelection = "ViewFBOChoiceSelection";
            public const string AddFBOChoiceSelection = "AddFBOChoiceSelection";
            public const string EditFBOChoiceSelection = "EditFBOChoiceSelection";
            public const string DeleteFBOChoiceSelection = "DeleteFBOChoiceSelection";
            #endregion

            #region GeneralNotes
            public const string ViewGeneralNotes = "ViewGeneralNotes";
            public const string AddGeneralNotes = "AddGeneralNotes";
            public const string EditGeneralNotes = "EditGeneralNotes";
            public const string DeleteGeneralNotes = "DeleteGeneralNotes";
            #endregion

            #region TravelCoordinatorAssignedRequestors
            public const string ViewTravelCoordinatorAssignedRequestors = "ViewTravelCoordinatorAssignedRequestors";
            public const string AddTravelCoordinatorAssignedRequestors = "AddTravelCoordinatorAssignedRequestors";
            public const string EditTravelCoordinatorAssignedRequestors = "EditTravelCoordinatorAssignedRequestors";
            public const string DeleteTravelCoordinatorAssignedRequestors = "DeleteTravelCoordinatorAssignedRequestors";
            #endregion

            #region Authorization
            public const string ViewAuthorization = "ViewAuthorization";
            public const string AddAuthorization = "AddAuthorization";
            public const string EditAuthorization = "EditAuthorization";
            public const string DeleteAuthorization = "DeleteAuthorization";
            #endregion

            #region "CQCustomer"
            public const string ViewCQCustomerCatalog = "ViewCQCustomer";
            public const string AddCQCustomerCatalog = "AddCQCustomer";
            public const string EditCQCustomerCatalog = "EditCQCustomer";
            public const string DeleteCQCustomerCatalog = "DeleteCQCustomer";
            #endregion

            #region "CQCustomerContact"
            public const string ViewCQCustomerContactCatalog = "ViewCQCustomerContact";
            public const string AddCQCustomerContactCatalog = "AddCQCustomerContact";
            public const string EditCQCustomerContactCatalog = "EditCQCustomerContact";
            public const string DeleteCQCustomerContactCatalog = "DeleteCQCustomerContact";
            #endregion

            #region LeadSourceCatalog
            public const string ViewLeadSourceCatalog = "ViewLeadSource";
            public const string AddLeadSourceCatalog = "AddLeadSource";
            public const string EditLeadSourceCatalog = "EditLeadSource";
            public const string DeleteLeadSourceCatalog = "DeleteLeadSource";
            #endregion

            #region FeeGroupCatalog
            public const string ViewFeeGroup = "ViewFeeGroup";
            public const string AddFeeGroup = "AddFeeGroup";
            public const string EditFeeGroup = "EditFeeGroup";
            public const string DeleteFeeGroup = "DeleteFeeGroup";
            #endregion

            #region LostBusinessCatalog
            public const string ViewLostBusiness = "ViewLostBusiness";
            public const string AddLostBusiness = "AddLostBusiness";
            public const string EditLostBusiness = "EditLostBusiness";
            public const string DeleteLostBusiness = "DeleteLostBusiness";
            #endregion

            #region SalesPersonCatalog
            public const string ViewSalesPersonCatalog = "ViewSalesPerson";
            public const string AddSalesPersonCatalog = "AddSalesPerson";
            public const string EditSalesPersonCatalog = "EditSalesPerson";
            public const string DeleteSalesPersonCatalog = "DeleteSalesPerson";
            #endregion

            #region FeeScheduleCatalog
            public const string ViewFeeSchedule = "ViewFeeSchedule";
            public const string AddFeeSchedule = "AddFeeSchedule";
            public const string EditFeeSchedule = "EditFeeSchedule";
            public const string DeleteFeeSchedule = "DeleteFeeSchedule";
            #endregion

            #region FuelFileXsltGenerator
            public const string ViewFuelFileXsltGenerator = "ViewFuelFileXsltGenerator";
            public const string AddFuelFileXsltGenerator = "AddFuelFileXsltGenerator";
            public const string EditFuelFileXsltGenerator = "EditFuelFileXsltGenerator";
            public const string DeleteFuelFileXsltGenerator = "DeleteFuelFileXsltGenerator";
            #endregion

            #region HistoricalFuelFileData
            public const string ViewHistoricalFuelFileData = "ViewHistoricalFuelData";
            public const string AddHistoricalFuelFileData = "AddHistoricalFuelData";
            public const string EditHistoricalFuelFileData = "EditHistoricalFuelData";
            public const string DeleteHistoricalFuelFileData = "DeleteHistoricalFuelData";
            #endregion

            #endregion
        }


        public class DatabaseReports
        {
            public const string ViewPassengerReport = "ViewPassengerRequestorReports";
            public const string ViewPassengerInfoReport = "ViewPassengerInfoReport";
            public const string ViewAccountReport = "ViewAccountsReports";
            public const string ViewAircraftDutyTypeReport = "ViewAircraftDutyTypeReports";
            public const string ViewAircraftTypeReport = "ViewAircraftTypeReports";
            public const string ViewAirportReport = "ViewAirportReports";
            public const string ViewCrewCheckListCatalogReport = "ViewCrewCheckListReports";
            public const string ViewCrewDutyTypesReport = "ViewCrewDutyTypesReports";
            public const string ViewDelayTypeCatalogReport = "ViewDelayTypeReports";
            public const string ViewDepartmentAuthorizationReport = "ViewDepartmentAuthorizationReports";
            public const string ViewFlightCategoryReport = "ViewFlightCategoryReports";
            public const string ViewFlightPurposeReport = "ViewFlightPurposeReports";
            public const string ViewCrewRosterReports = "ViewCrewRosterReport";
            public const string ViewTripManagerChecklistReports = "ViewTripManagerCheckListReports";
            public const string ViewFleetProfileReports = "ViewFleetProfileReports";
            public const string ViewLeadSourceCatalogReport = "ViewLeadSourceReports";
            public const string ViewFeeGroupReport = "ViewFeeGroupReports";
            public const string ViewLostBusinessReport = "ViewLostBusinessReports";
        }

        #region Preflight
        public class Preflight
        {
            public const string ViewTripManager = "ViewTripManager";
            public const string AddTripManager = "AddTripManager";
            public const string EditTripManager = "EditTripManager";
            public const string DeleteTripManager = "DeleteTripManager";
            
            public const string ViewPreFlightMain = "ViewPreFlightMain";
            public const string AddPreFlightMain = "AddPreFlightMain";
            public const string EditPreFlightMain = "EditPreFlightMain";
            public const string DeletePreFlightMain = "DeletePreFlightMain";
            
            public const string ViewPreflightLeg = "ViewPreflightLeg";
            public const string AddPreflightLeg = "AddPreflightLeg";
            public const string EditPreflightLeg = "EditPreflightLeg";
            public const string DeletePreflightLeg = "DeletePreflightLeg";

            public const string ViewPreflightCrew = "ViewPreflightCrew";
            public const string AddPreflightCrew = "AddPreflightCrew";
            public const string EditPreflightCrew = "EditPreflightCrew";
            public const string DeletePreflightCrew = "DeletePreflightCrew";

            public const string ViewPreflightCrewAvailable = "ViewPreflightCrewAvailable";
            public const string AddPreflightCrewAvailable = "AddPreflightCrewAvailable";

            public const string ViewPreflightCrewHotel = "ViewPreflightCrewHotel";
            public const string AddPreflightCrewHotel = "AddPreflightCrewHotel";
            public const string EditPreflightCrewHotel = "EditPreflightCrewHotel";
            public const string DeletePreflightCrewHotel = "DeletePreflightCrewHotel";

            public const string ViewPreflightCrewTransport = "ViewPreflightCrewTransport";
            public const string AddPreflightCrewTransport = "AddPreflightCrewTransport";
            public const string EditPreflightCrewTransport = "EditPreflightCrewTransport";
            public const string DeletePreflightCrewTransport = "DeletePreflightCrewTransport";

            public const string ViewPreflightPassenger = "ViewPreflightPassenger";
            public const string AddPreflightPassenger = "AddPreflightPassenger";
            public const string EditPreflightPassenger = "EditPreflightPassenger";
            public const string DeletePreflightPassenger = "DeletePreflightPassenger";

            public const string ViewPreflightPassengerHotel = "ViewPreflightPassengerHotel";
            public const string AddPreflightPassengerHotel = "AddPreflightPassengerHotel";
            public const string EditPreflightPassengerHotel = "EditPreflightPassengerHotel";
            public const string DeletePreflightPassengerHotel = "DeletePreflightPassengerHotel";

            public const string ViewPreflightPassengerTransport = "ViewPreflightPassengerTransport";
            public const string AddPreflightPassengerTransport = "AddPreflightPassengerTransport";
            public const string EditPreflightPassengerTransport = "EditPreflightPassengerTransport";
            public const string DeletePreflightPassengerTransport = "DeletePreflightPassengerTransport";
            
            public const string ViewPreflightLegNotes = "ViewPreflightLegNotes";
            public const string AddPreflightLegNotes = "AddPreflightLegNotes";
            public const string EditPreflightLegNotes = "EditPreflightLegNotes";
            public const string DeletePreflightLegNotes = "DeletePreflightLegNotes";
            
            public const string ViewPreflightFuel = "ViewPreflightFuel";
            public const string AddPreflightFuel = "AddPreflightFuel";
            public const string EditPreflightFuel = "EditPreflightFuel";
            public const string DeletePreflightFuel = "DeletePreflightFuel";

            public const string ViewPreflightCatering = "ViewPreflightCatering";
            public const string AddPreflightCatering = "AddPreflightCatering";
            public const string EditPreflightCatering = "EditPreflightCatering";
            public const string DeletePreflightCatering = "DeletePreflightCatering";
            
            public const string ViewPreflightFBO = "ViewPreflightFBO";
            public const string AddPreflightFBO = "AddPreflightFBO";
            public const string EditPreflightFBO = "EditPreflightFBO";
            public const string DeletePreflightFBO = "DeletePreflightFBO";

            public const string ViewPreflightUWA = "ViewPreflightUWA";
            public const string AddPreflightUWA = "AddPreflightUWA";
            public const string EditPreflightUWA = "EditPreflightUWA";
            public const string DeletePreflightUWA = "DeletePreflightUWA";
            
            public const string ViewTripLog = "ViewTripLog";
            public const string AddTripLog = "AddTripLog";
            public const string EditTripLog = "EditTripLog";
            public const string DeleteTripLog = "DeleteTripLog";

            public const string ViewTripSIFL = "ViewTripSIFL";
            public const string AddTripSIFL = "AddTripSIFL";
            public const string EditTripSIFL = "EditTripSIFL";
            public const string DeleteTripSIFL = "DeleteTripSIFL";

            public const string ViewPreflightOutboundInstruction = "ViewPreflightOutboundInstruction";
            public const string AddPreflightOutboundInstruction = "AddPreflightOutboundInstruction";
            public const string EditPreflightOutboundInstruction = "EditPreflightOutboundInstruction";
            public const string DeletePreflightOutboundInstruction = "DeletePreflightOutboundInstruction";

            public const string ViewPreflightChecklist = "ViewPreflightChecklist";
            public const string AddPreflightChecklist = "AddPreflightChecklist";
            public const string EditPreflightChecklist = "EditPreflightChecklist";
            public const string DeletePreflightChecklist = "DeletePreflightChecklist";

            public const string ViewPreflightAPIS = "ViewPreflightAPIS";
            public const string AddPreflightAPIS = "AddPreflightAPIS";
            public const string EditPreflightAPIS = "EditPreflightAPIS";
            public const string DeletePreflightAPIS = "DeletePreflightAPIS";

            public const string ViewPreflightReports = "ViewPreflightReports";
            public const string AddPreflightReports = "AddPreflightReports";
            public const string EditPreflightReports = "EditPreflightReports";
            public const string DeletePreflightReports = "DeletePreflightReports";

            public const string ViewPreflightTripsheetReportWriter = "ViewPreflightTripsheetReportWriter";
            public const string AddPreflightTripsheetReportWriter = "AddPreflightTripsheetReportWriter";
            public const string EditPreflightTripsheetReportWriter = "EditPreflightTripsheetReportWriter";
            public const string DeletePreflightTripsheetReportWriter = "DeletePreflightTripsheetReportWriter";

            public const string ViewPreflightCorporateRequest = "ViewPreflightCorporateRequest";
            public const string AddPreflightCorporateRequest = "AddPreflightCorporateRequest";
            public const string EditPreflightCorporateRequest = "EditPreflightCorporateRequest";
            public const string DeletePreflightCorporateRequest = "DeletePreflightCorporateRequest";

            #region Schedule Calendar

            public const string ViewCalendar = "ViewCalendar";
            public const string AddCalendar = "AddCalendar";
            public const string EditCalendar = "EditCalendar";
            public const string DeleteCalendar = "DeleteCalendar";

            #region "Fleet Calendar Entry"

            public const string ViewFleetCalendarEntry = "ViewFleetCalendarEntry";
            public const string AddFleetCalendarEntry = "AddFleetCalendarEntry";
            public const string EditFleetCalendarEntry = "EditFleetCalendarEntry";
            public const string DeleteFleetCalendarEntry = "DeleteFleetCalendarEntry";

            #endregion

            #region "Crew Calendar Entry"

            public const string ViewCrewCalendarEntry = "ViewCrewCalendarEntry";
            public const string AddCrewCalendarEntry = "AddCrewCalendarEntry";
            public const string EditCrewCalendarEntry = "EditCrewCalendarEntry";
            public const string DeleteCrewCalendarEntry = "DeleteCrewCalendarEntry";

            #endregion

            #endregion
            
        }
        #endregion
        
        #region Postflight
        public class Postflight
        {
            public const string ViewPOLogManager = "ViewPOLogManager";
            public const string AddPOLogManager = "AddPOLogManager";
            public const string EditPOLogManager = "EditPOLogManager";
            public const string DeletePOLogManager = "DeletePOLogManager";

            public const string ViewPOAugCrewAdjust = "ViewPOAugCrewAdjust";
            public const string AddPOAugCrewAdjust = "AddPOAugCrewAdjust";
            public const string EditPOAugCrewAdjust = "EditPOAugCrewAdjust";
            public const string DeletePOAugCrewAdjust = "DeletePOAugCrewAdjust";

            public const string ViewPOCompleteLog = "ViewPOCompleteLog";
            public const string AddPOCompleteLog = "AddPOCompleteLog";
            public const string EditPOCompleteLog = "EditPOCompleteLog";
            public const string DeletePOCompleteLog = "DeletePOCompleteLog";

            public const string ViewPOCrewDutyAdjust = "ViewPOCrewDutyAdjust";
            public const string AddPOCrewDutyAdjust = "AddPOCrewDutyAdjust";
            public const string EditPOCrewDutyAdjust = "EditPOCrewDutyAdjust";
            public const string DeletePOCrewDutyAdjust = "DeletePOCrewDutyAdjust";

            public const string ViewPOExpenseCatalog = "ViewPOExpenseCatalog";
            public const string AddPOExpenseCatalog = "AddPOExpenseCatalog";
            public const string EditPOExpenseCatalog = "EditPOExpenseCatalog";
            public const string DeletePOExpenseCatalog = "DeletePOExpenseCatalog";

            public const string ViewPOLogCrew = "ViewPOLogCrew";
            public const string AddPOLogCrew = "AddPOLogCrew";
            public const string EditPOLogCrew = "EditPOLogCrew";
            public const string DeletePOLogCrew = "DeletePOLogCrew";

            public const string ViewPOLogLegs = "ViewPOLogLegs";
            public const string AddPOLogLegs = "AddPOLogLegs";
            public const string EditPOLogLegs = "EditPOLogLegs";
            public const string DeletePOLogLegs = "DeletePOLogLegs";

            public const string ViewPOLogManagerAdjustTab = "ViewPOLogManagerAdjustTab";
            public const string AddPOLogManagerAdjustTab = "AddPOLogManagerAdjustTab";
            public const string EditPOLogManagerAdjustTab = "EditPOLogManagerAdjustTab";
            public const string DeletePOLogManagerAdjustTab = "DeletePOLogManagerAdjustTab";

            public const string ViewPOLogPax = "ViewPOLogPax";
            public const string AddPOLogPax = "AddPOLogPax";
            public const string EditPOLogPax = "EditPOLogPax";
            public const string DeletePOLogPax = "DeletePOLogPax";

            public const string ViewPORetrieveLog = "ViewPORetrieveLog";
            public const string AddPORetrieveLog = "AddPORetrieveLog";
            public const string EditPORetrieveLog = "EditPORetrieveLog";
            public const string DeletePORetrieveLog = "DeletePORetrieveLog";

            public const string ViewPOOtherCrewDutyLog = "ViewPOOtherCrewDutyLog";
            public const string AddPOOtherCrewDutyLog = "AddPOOtherCrewDutyLog";
            public const string EditPOOtherCrewDutyLog = "EditPOOtherCrewDutyLog";
            public const string DeletePOOtherCrewDutyLog = "DeletePOOtherCrewDutyLog";

            public const string ViewPOSIFL = "ViewPOSIFL";
            public const string AddPOSIFL = "AddPOSIFL";
            public const string EditPOSIFL = "EditPOSIFL";
            public const string DeletePOSIFL = "DeletePOSIFL";

        }
        #endregion

        #region CharterQuote
        public class CharterQuote
        {
            public const string ViewCQFile = "ViewCQFile";
            public const string AddCQFile = "AddCQFile";
            public const string EditCQFile = "EditCQFile";
            public const string DeleteCQFile = "DeleteCQFile";

            public const string ViewCQQuotesOnFile = "ViewCQQuotesOnFile";
            public const string AddCQQuotesOnFile = "AddCQQuotesOnFile";
            public const string EditCQQuotesOnFile = "EditCQQuotesOnFile";
            public const string DeleteCQQuotesOnFile = "DeleteCQQuotesOnFile";
            
            public const string ViewCQQuoteDetail = "ViewCQQuoteDetail";
            public const string AddCQQuoteDetail = "AddCQQuoteDetail";
            public const string EditCQQuoteDetail = "EditCQQuoteDetail";
            public const string DeleteCQQuoteDetail = "DeleteCQQuoteDetail";
            
            public const string ViewCQLeg = "ViewCQLeg";
            public const string AddCQLeg = "AddCQLeg";
            public const string EditCQLeg = "EditCQLeg";
            public const string DeleteCQLeg = "DeleteCQLeg";

            public const string ViewCQLiveTrip = "ViewCQLiveTrip";
            public const string AddCQLiveTrip = "AddCQLiveTrip";
            public const string EditCQLiveTrip = "EditCQLiveTrip";
            public const string DeleteCQLiveTrip = "DeleteCQLiveTrip";

            public const string ViewCQLogistics = "ViewCQLogistics";
            public const string AddCQLogistics = "AddCQLogistics";
            public const string EditCQLogistics = "EditCQLogistics";
            public const string DeleteCQLogistics = "DeleteCQLogistics";

            public const string ViewCQPAXManifest = "ViewCQPAXManifest";
            public const string AddCQPAXManifest = "AddCQPAXManifest";
            public const string EditCQPAXManifest = "EditCQPAXManifest";
            public const string DeleteCQPAXManifest = "DeleteCQPAXManifest";

            public const string ViewCQPAXManifestOther = "ViewCQPAXManifestOther";
            public const string AddCQPAXManifestOther = "AddCQPAXManifestOther";
            public const string EditCQPAXManifestOther = "EditCQPAXManifestOther";
            public const string DeleteCQPAXManifestOther = "DeleteCQPAXManifestOther";

            public const string ViewCQInvoiceReport = "ViewCQInvoiceReport";
            public const string AddCQInvoiceReport = "AddCQInvoiceReport";
            public const string EditCQInvoiceReport = "EditCQInvoiceReport";
            public const string DeleteCQInvoiceReport = "DeleteCQInvoiceReport";

            public const string ViewCQQuoteReport = "ViewCQQuoteReport";
            public const string AddCQQuoteReport = "AddCQQuoteReport";
            public const string EditCQQuoteReport = "EditCQQuoteReport";
            public const string DeleteCQQuoteReport = "DeleteCQQuoteReport";

            public const string ViewCQReportWriter = "ViewCQReportWriter";
            public const string AddCQReportWriter = "AddCQReportWriter";
            public const string EditCQReportWriter = "EditCQReportWriter";
            public const string DeleteCQReportWriter = "DeleteCQReportWriter";

            // For Express Quote
            public const string ViewCQExpressQuote = "ViewCQExpressQuote";
            public const string AddCQExpressQuote = "AddCQExpressQuote";
            public const string EditCQExpressQuote = "EditCQExpressQuote";
            public const string DeleteCQExpressQuote = "DeleteCQExpressQuote";

        }
        #endregion

        #region CorporateRequest
        public class CorporateRequest
        {
            public const string ViewCRManager = "ViewCRManager";
            public const string AddCRManager = "AddCRManager";
            public const string EditCRManager = "EditCRManager";
            public const string DeleteCRManager = "DeleteCRManager";
            
            public const string ViewCRLeg = "ViewCRLeg";
            public const string AddCRLeg = "AddCRLeg";
            public const string EditCRLeg = "EditCRLeg";
            public const string DeleteCRLeg = "DeleteCRLeg";
            
            public const string ViewCRLogistics = "ViewCRLogistics";
            public const string AddCRLogistics = "AddCRLogistics";
            public const string EditCRLogistics = "EditCRLogistics";
            public const string DeleteCRLogistics = "DeleteCRLogistics";
            
            public const string ViewCRPAXManifest = "ViewCRPAXManifest";
            public const string AddCRPAXManifest = "AddCRPAXManifest";
            public const string EditCRPAXManifest = "EditCRPAXManifest";
            public const string DeleteCRPAXManifest = "DeleteCRPAXManifest";

            public const string ViewCRPAXManifestOther = "ViewCRPAXManifestOther";
            public const string AddCRPAXManifestOther = "AddCRPAXManifestOther";
            public const string EditCRPAXManifestOther = "EditCRPAXManifestOther";
            public const string DeleteCRPAXManifestOther = "DeleteCRPAXManifestOther";

            public const string ViewCRChangeQueue = "ViewCRChangeQueue";
            public const string AddCRChangeQueue = "AddCRChangeQueue";
            public const string EditCRChangeQueue = "EditCRChangeQueue";
            public const string DeleteCRChangeQueue = "DeleteCRChangeQueue";

            public const string ViewCRTransfer = "ViewCRTransfer";
            public const string ViewCRDeny = "ViewCRDeny";
            public const string ViewCRMarkAccepted = "ViewCRMarkAccepted";
        }
        #endregion

        public class Utilities
        {

            #region Airport
            public const string ViewAirport = "ViewUtilAirport";
            public const string AddAirport = "AddUtilAirport";
            public const string EditAirport = "EditUtilAirport";
            public const string DeleteAirports = "DeleteUtilAirport";
            #endregion

            #region AirportPairs
            public const string ViewAirportPairs = "ViewAirportPairs";
            public const string AddAirportPairs = "AddAirportPairs";
            public const string EditAirportPairs = "EditAirportPairs";
            public const string DeleteAirportPairs = "DeleteAirportPairs";
            #endregion

            #region AirportPairLegs
            public const string ViewAirportPairLegs = "ViewAirportPairLegs";
            public const string AddAirportPairLegs = "AddAirportPairLegs";
            public const string EditAirportPairLegs = "EditAirportPairLegs";
            public const string DeleteAirportPairLegs = "DeleteAirportPairLegs";
            #endregion

            #region ItineraryPlan
            public const string ViewItineraryPlan = "ViewItineraryPlan";
            public const string AddItineraryPlan = "AddItineraryPlan";
            public const string EditItineraryPlan = "EditItineraryPlan";
            public const string DeleteItineraryPlan = "DeleteItineraryPlan";
            #endregion

            #region ItineraryPlanLegs
            public const string ViewItineraryPlanLegs = "ViewItineraryPlanLegs";
            public const string AddItineraryPlanLegs = "AddItineraryPlanLegs";
            public const string EditItineraryPlanLegs = "EditItineraryPlanLegs";
            public const string DeleteItineraryPlanLegs = "DeleteItineraryPlanLegs";
            #endregion

            #region WolrdClock
            public const string ViewWolrdClock = "ViewWolrdClock";
            public const string AddWolrdClock = "AddWolrdClock";
            public const string EditWolrdClock = "EditWolrdClock";
            public const string DeleteWolrdClock = "DeleteWolrdClock";
            #endregion

            #region WolrdWinds
            public const string ViewWolrdWinds = "ViewWolrdWinds";
            public const string AddWolrdWinds = "AddWolrdWinds";
            public const string EditWolrdWinds = "EditWolrdWinds";
            public const string DeleteWolrdWinds = "DeleteWolrdWinds";
            #endregion

            #region UVTripPlanner
            public const string ViewUVTripPlanner = "ViewUVTripPlanner";
            public const string AddUVTripPlanner = "AddUVTripPlanner";
            public const string EditUVTripPlanner = "EditUVTripPlanner";
            public const string DeleteUVTripPlanner = "DeleteUVTripPlanner";
            #endregion

            #region Locator
            public const string ViewLocator = "ViewLocator";
            #endregion

            #region "Bookmark"
            public const string ViewBookmark = "ViewBookmark";
            public const string AddBookmark = "AddBookmark";
            public const string EditBookmark = "EditBookmark";
            public const string DeleteBookmark = "DeleteBookmark";
            #endregion
        }
        
    }

}
