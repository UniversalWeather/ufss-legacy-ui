﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class ModuleId
    {
        // Module names has been mapped against module id's for better maintainability
        public const string CharterQuote = "10001";
        public const string CharterQuoteReports = "10002";
        public const string CorporateRequest = "10003";
        public const string CorporateRequestReports = "10004";
        public const string Database = "10005";
        public const string DatabaseReports = "10006";
        public const string Favorites = "10007";
        public const string Menu = "10008";
        public const string Postflight = "10009";
        public const string PostflightReports = "10010";
        public const string Preflight = "10011";
        public const string PreflightReports = "10012";
        public const string ReportWriter = "10013";
        public const string ScheduledService = "10014";
        public const string SecurityAdministration = "10015";
        public const string SystemAdministration = "10016";
        public const string TripsheetReports = "10017";
        public const string TripsheetRWReports = "10018";
        public const string Utility = "10019";
        public const string UtilityReports = "10020";
        public const string Universal = "10021";

    }
}
