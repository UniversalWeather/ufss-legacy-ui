﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common
{
    public class ModuleNameConstants
    {
        #region Database
        public class Database
        {
            //This is used to display module name in error display of caught exceptions 
            public const string Metro = "Metro";
            public const string Account = "Account";
            public const string DST = "Day Light Saving";

        }
        #endregion
    }
}
