﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class EntitySet
    {
        #region Database
        public class Database
        {
            //This will be the exact underlying EntitySet name.            
            public const string Account = "Account";
            public const string Aircraft = "Aircraft";
            public const string AircraftDuty = "AircraftDuty";
            public const string Airport = "Airport";
            public const string Budget = "Budget";
            public const string Catering = "Catering";
            public const string Client = "Client";
            public const string Company = "Company";
            public const string ConversionTable = "ConversionTable";
            public const string Country = "Country";
            public const string CQCustomer = "CQCustomer";
            public const string CQCustomerContact = "CQCustomerContact";
            public const string Crew = "Crew";
            public const string CrewAircraftAssigned = "CrewAircraftAssigned";
            public const string CrewCheckList = "CrewCheckList";
            public const string CrewCheckListDetail = "CrewCheckListDetail";
            public const string CrewDefinition = "CrewDefinition";
            public const string CrewDutyRules = "CrewDutyRules";
            public const string CrewDutyType = "CrewDutyType";
            public const string CrewGroup = "CrewGroup";
            public const string CrewGroupOrder = "CrewGroupOrder";
            public const string CrewInformation = "CrewInformation";
            public const string CrewPassengerPassport = "CrewPassengerPassport";
            public const string CrewPassengerVisa = "CrewPassengerVisa";
            public const string CrewRating = "CrewRating";
            public const string CustomAddress = "CustomAddress";
            public const string Customer = "Customer";
            public const string CustomerLicense = "CustomerLicense";
            public const string CustomerModuleAccess = "CustomerModuleAccess";
            public const string DBGeneralInfo = "DBGeneralInfo";
            public const string DelayType = "DelayType";
            public const string Department = "Department";
            public const string DepartmentAuthorization = "DepartmentAuthorization";
            public const string DepartmentGroup = "DepartmentGroup";
            public const string DepartmentGroupOrder = "DepartmentGroupOrder";
            public const string DSTRegion = "DSTRegion";
            public const string EmergencyContact = "EmergencyContact";
            public const string ExchangeRate = "ExchangeRate";
            public const string FareLevel = "FareLevel";
            public const string FBO = "FBO";
            public const string FeeGroup = "FeeGroup";
            public const string FeeSchedule = "FeeSchedule";
            public const string FileWarehouse = "FileWarehouse";
            public const string Fleet = "Fleet";
            public const string FleetChargeRate = "FleetChargeRate";
            public const string FleetChargeHistory = "FleetChargeHistory";
            public const string FleetCharterRate = "FleetCharterRate";
            public const string FleetComponent = "FleetComponent";
            public const string FleetForeCast = "FleetForeCast";
            public const string FleetGroup = "FleetGroup";
            public const string FlightCategory = "FlightCategory";
            public const string FlightPurpose = "FlightPurpose";
            public const string FleetGroupOrder = "FleetGroupOrder";
            public const string FleetInformation = "FleetInformation";
            public const string FleetNewCharterRate = "FleetNewCharterRate";
            public const string FleetPair = "FleetPair";
            public const string FleetProfileDefinition = "FleetProfileDefinition";
            public const string FleetProfile = "FleetProfile";
            public const string FleetProfileInformation = "FleetProfileInformation";
            public const string FPBookmark = "FPBookmark";
            public const string FuelLocator = "FuelLocator";
            public const string FuelVendor = "FuelVendor";
            public const string FlightpakFuel = "FlightpakFuel";
            public const string Hotel = "Hotel";
            public const string LeadSource = "LeadSource";
            public const string LostBusiness = "Lost Business";
            public const string Metro = "Metro";
            public const string Passenger = "Passenger";
            public const string PassengerRequestor = "PassengerRequestor";
            public const string PassengerAdditionalInfo = "PassengerAdditionalInfo";
            public const string PassengerGroup = "PassengerGroup";
            public const string PassengerGroupOrder = "PassengerGroupOrder";
            public const string PassengerInformation = "PassengerInformation";
            public const string PaymentType = "PaymentType";
            public const string RoomType = "RoomType";
            public const string Runway = "Runway";
            public const string SalesPerson = "SalesPerson";
            public const string TailHistory = "TailHistory";
            public const string Transport = "Transport";
            public const string TravelCoordinator = "TravelCoordinator";
            public const string TravelCoordinatorFleet = "TravelCoordinatorFleet";
            public const string TravelCoordinatorRequestor = "TravelCoordinatorRequestor";
            public const string TripManagerCheckList = "TripManagerCheckList";
            public const string TripManagerCheckListGroup = "TripManagerCheckListGroup";
            public const string TSAClear = "TSAClear";
            public const string TSANoFly = "TSANoFly";
            public const string TSASelect = "TSASelect";
            public const string TSDefinitionLog = "TSDefinitionLog";
            public const string UMPermission = "UMPermission";
            public const string UserGroup = "UserGroup";
            public const string UserGroupMapping = "UserGroupMapping";
            public const string UserGroupPermission = "UserGroupPermission";
            public const string UserMaster = "UserMaster";
            public const string UserPassword = "UserPassword";
            public const string Vendor = "Vendor";
            public const string VendorContact = "VendorContact";
            public const string WindStat = "WindStat";
            public const string WorldWind = "WorldWind";
        }
        #endregion

        #region Preflight

        public class Preflight
        {
            public const string PreflightMain = "PreflightMains";
            public const string PreflightLeg = "PreflightLegs";
        }
        #endregion

        #region CorporateRequest

        public class CorporateRequest
        {
            public const string CRMain = "CRMains";
            public const string CRLeg = "CRLegs";
        }
        #endregion

        #region CharterQuote

        public class CharterQuote
        {
            public const string CQFile = "CQFiles";
            public const string CQMain = "CQMains";
            public const string CQLeg = "CQLegs";
            public const string CQQuoteMain = "CQQuoteMains";
            public const string CQInvoiceMain = "CQInvoiceMains";
        }
        #endregion

        #region Postflight
        /// <summary>
        /// Class for maintaining Contstants for Locking / Unlocking scenarios
        /// </summary>
        public class Postflight
        {
            public const string PostflightMain = "PostflightMains";
            public const string PostFlightLegs = "PostFlightLegs";
            public const string PostFlightCrew = "PostFlightCrews";
            public const string PostFlightPassenger = "PostFlightPassengers";
            public const string PostFlightExpenses = "PostFlightExpenses";
        }
        #endregion

        #region Utilities
        public class Utilities
        {
            //This will be the exact underlying EntitySet name.            
            public const string AirportPair = "AirportPair";
            public const string AirportPairLeg = "AirportPairLeg";
            public const string ItineraryPlan = "ItineraryPlan";
            public const string ItineraryPlanLeg = "ItineraryPlanLeg";
            public const string Locator = "Locator";
            public const string WorldWind = "WorldWind";
            public const string WorldClock = "WorldClock";
            public const string Bookmark = "Bookmark";
        }
        #endregion
    }
}
