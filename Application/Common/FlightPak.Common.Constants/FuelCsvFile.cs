﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public static class FuelCsvFile
    {
        public const string text_IATA = "IATA";
        public const string text_ICAO = "ICAO";
        public const string text_SUPPLIER = "SUPPLIER";
        public const string text_FBONAME = "FBO";
        public const string text_DATE = "DATE";
        public const string text_LOW = "LOW,GAL FROM";
        public const string text_HIGH = "HIGH,GAL TO";
        public const string text_PRICE = "PRICE";
        public const string text_TEXT = "TEXT,NOTE";
    }
}
