﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Common.Constants
{
    public class Policy
    {
        public const string ExceptionPolicy = "AllException";
        public const string DataLayer = "DataLayer";
        public const string BusinessLayer = "BusinessLayer";
        public const string ServiceLayer = "ServiceLayer";
        public const string UILayer = "UILayer";
        public const string UWAServiceLayer = "UWAServiceLayer";
    }
}
