@echo off

set Auth_WebService_dir=FlightPak.AuthenticationServices
set fp_admin_webapp_dir=FlightPak.Web.Admin
set fp_webapp_dir=FlightPak.Web
set Master_WebService_dir=FlightPak.Services
set Report_Webservice_dir=FlightPak.ReportServices

::DEV SERVER
set DevWeb=172.31.254.43
set DevApp=hrdfpk2appw01.universalweather.rdn

::TEST SERVER
set TestWeb=hrtzfpk2iisw01.universalweather.rdn
set TestApp=hrtfpk2appw01.universalweather.rdn

::R1 FOLDER
set R1AppShare=c$\IIS.Rel1
set R1WebShare=c$\IIS.Rel1

::R21 FOLDER
set R21AppShare=c$\IIS.Rel2.1
set R21WebShare=c$\IIS.Rel2.1

::DEV2 FOLDER
set D2AppShare=c$\IIS.Dev2
set D2WebShare=c$\IIS.Dev2

:FILEPATH
::source file path

set Build_SOurce=.
set Auth_WebService=%Build_SOurce%\build\release\%Auth_WebService_dir%
set fp_admin_webapp=%Build_SOurce%\build\release\%fp_admin_webapp_dir%
set fp_webapp=%Build_SOurce%\build\release\%fp_webapp_dir%
set Master_WebService=%Build_SOurce%\build\release\%Master_WebService_dir%
set Report_WebService=%Build_SOurce%\build\release\%Report_WebService_dir%


::UWA Adapter configuration file with path

::For Dev
set Dev_UWAAdapterFile=%Build_SOurce%\Configuration\Development\FlightPak.Services\UWA.ServiceAdapters.dll.config

::For Test
set Test_UWAAdapterFile=%Build_SOurce%\Configuration\Testing\FlightPak.Services\UWA.ServiceAdapters.dll.config


if "%1%"=="" GOTO ERROR
if "%1%"=="DEV" GOTO DEV
if "%1%"=="TEST" GOTO TEST
if "%1%"=="TRN" GOTO TRN
echo Invalid environment parameter
goto END

:DEV
set WebMachine=%DevWeb%
set AppMachine=%DevApp%
goto SETFOLDER

:TEST
set WebMachine=%TestWeb%
set AppMachine=%TestApp%
goto SETFOLDER

:TRN
echo IT Team please update the sharename, machine names the batchfile :)
set WebMachine=
set AppMachine=
goto END


:SETFOLDER

if "%2%"=="" GOTO ERROR
if "%2%"=="R1" GOTO R1
if "%2%"=="R21" GOTO R21
if "%2%"=="D2" GOTO D2
echo Invalid environment parameter
goto END

:R1
set AppShare=%R1AppShare%
set WebShare=%R1WebShare%
goto START

:R21
set AppShare=%R21AppShare%
set WebShare=%R21WebShare%
goto START

:D2
set AppShare=%D2AppShare%
set WebShare=%D2WebShare%
goto START

:START
echo Please Enter the username/password for %1% environment App Server
set Username =""
set Password = ""

:: Get the user credentials for App Server share
set /p Username=Type Username: %=%
set /p Password=Type Password: %=%


::App
net use t: \\%AppMachine%\%AppShare% /user:%Username% %Password%

if %1%==TEST goto TESTMACHINE

::Web Server for DEV environment
net use s: \\%WebMachine%\%WebShare% /user:fpkadmin KIj1gY$e5SM

goto COPY


:TESTMACHINE 
::Web Server for Test environment
net use s: \\%WebMachine%\%WebShare% /user:fpkadmin KIj1gY$e5SM


:Copy
:: Assuming the current location as the build location

::backup before copy
set today=%date:/=%
set today=%today:~4%
set now=%time: =%
set now=%now::=%
set now=%now:.=%
set backupdirname=%today%-%now%
::set backupdirname=%date:~4,2%-%date:~7,2%-%date:~10,2%-%time:~0,2%-%time:~3,2%
@echo.
@echo.
@echo Backup existing deployment....
@echo.
@echo Backup folder name %backupdirname%
@echo.
@echo.
::copy app server files
xcopy t:\%Auth_WebService_dir% t:\backup\%backupdirname%\%Auth_WebService_dir% /v /D /E /Y /R /I 
xcopy t:\%Master_WebService_dir% t:\backup\%backupdirname%\%Master_WebService_dir% /v /D /E /Y /R /I 
xcopy t:\%Report_WebService_dir% t:\backup\%backupdirname%\%Report_WebService_dir% /v /D /E /Y /R /I 
xcopy t:\%fp_admin_webapp_dir% t:\backup\%backupdirname%\%fp_admin_webapp_dir% /v /D /E /Y /R /I 
::pause
::copy web server files
xcopy s:\%fp_webapp_dir% s:\backup\%backupdirname%\%fp_webapp_dir% /v /D /E /Y /R /I 
::pause
::delete existing deployment
@echo.
@echo Deleting existing deployment
@echo.

::stop IIS before delete the old deployed version
iisreset /stop

rmdir t:\%Auth_WebService_dir% /s /q
rmdir t:\%Master_WebService_dir% /s /q
rmdir t:\%Report_WebService_dir% /s /q
rmdir t:\%fp_admin_webapp_dir% /s /q
::delete web server files
rmdir s:\%fp_webapp_dir% /s /q
::pause 


::App machine copy
@echo.
@echo Copy to App Server
xcopy %Auth_WebService% t:\%Auth_WebService_dir%\ /v /D /E /Y /R /I
xcopy %Master_WebService% t:\%Master_WebService_dir%\ /v /D /E /Y /R /I
xcopy %Report_WebService% t:\%Report_WebService_dir%\ /v /D /E /Y /R /I
xcopy %fp_admin_webapp% t:\%fp_admin_webapp_dir%\ /v /D /E /Y /R /I

if %1%==TEST goto COPYFORTEST
:COPYFORDEV
xcopy %Dev_UWAAdapterFile% t:\%Master_WebService_dir%\bin\ /v /D /Y /R /I
goto COPYWEB

:COPYFORTEST
xcopy %Test_UWAAdapterFile% t:\%Master_WebService_dir%\bin\ /v /D /Y /R /I


:COPYWEB

::pause
::Web machine copy
@echo Copy to Web Server
xcopy %fp_webapp% s:\%fp_webapp_dir%\ /v /D /E /Y /R /I
::pause

::start IIS with the new version installed
iisreset /start


net use s: /delete 
net use t: /delete

@echo.
@echo.
@echo If you receive any error, please make sure you have correct source path 
@echo.
@echo.
@echo Copy complete :)
@echo.
@echo.


goto end

:ERROR
echo Please pass the environment DEV or TEST or TRN


:end