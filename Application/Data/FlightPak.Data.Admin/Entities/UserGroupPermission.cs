using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IUserGroupPermission))]
	public partial class UserGroupPermission : IUserGroupPermission
	{
		static UserGroupPermission()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UserGroupPermission), typeof(IUserGroupPermission)), typeof(UserGroupPermission));
		}
	}

	public interface IUserGroupPermission
	{
	}
}
