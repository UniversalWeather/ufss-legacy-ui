using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IFlightpakSequence))]
	public partial class FlightpakSequence : IFlightpakSequence
	{
		static FlightpakSequence()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FlightpakSequence), typeof(IFlightpakSequence)), typeof(FlightpakSequence));
		}
	}

	public interface IFlightpakSequence
	{
	}
}
