using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IUserMaster))]
	public partial class UserMaster : IUserMaster
	{
		static UserMaster()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UserMaster), typeof(IUserMaster)), typeof(UserMaster));
		}
	}

	public interface IUserMaster
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name" + DataValidation.IsRequired)]
        string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name" + DataValidation.IsRequired)]
        string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name" + DataValidation.IsRequired)]
        string LastName { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Home Base" + DataValidation.IsRequired)]
        //long? HomeBaseID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "E-mail" + DataValidation.IsRequired)]
        string EmailID { get; set; }

    }
}
