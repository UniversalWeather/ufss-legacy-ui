using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IDSTRegion))]
	public partial class DSTRegion : IDSTRegion
	{
		static DSTRegion()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DSTRegion), typeof(IDSTRegion)), typeof(DSTRegion));
		}
	}

	public interface IDSTRegion
	{
	}
}
