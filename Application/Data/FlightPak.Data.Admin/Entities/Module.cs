using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IModule))]
	public partial class Module : IModule
	{
		static Module()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Module), typeof(IModule)), typeof(Module));
		}
	}

	public interface IModule
	{
	}
}
