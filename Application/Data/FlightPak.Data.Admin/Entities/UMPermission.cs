using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IUMPermission))]
	public partial class UMPermission : IUMPermission
	{
		static UMPermission()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UMPermission), typeof(IUMPermission)), typeof(UMPermission));
		}
	}

	public interface IUMPermission
	{
	}
}
