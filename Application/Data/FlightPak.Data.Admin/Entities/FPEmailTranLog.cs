using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IFPEmailTranLog))]
	public partial class FPEmailTranLog : IFPEmailTranLog
	{
		static FPEmailTranLog()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FPEmailTranLog), typeof(IFPEmailTranLog)), typeof(FPEmailTranLog));
		}
	}

	public interface IFPEmailTranLog
	{
	}
}
