using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(ICustomAddress))]
	public partial class CustomAddress : ICustomAddress
	{
		static CustomAddress()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CustomAddress), typeof(ICustomAddress)), typeof(CustomAddress));
		}
	}

	public interface ICustomAddress
	{
	}
}
