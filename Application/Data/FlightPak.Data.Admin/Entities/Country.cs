using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(ICountry))]
	public partial class Country : ICountry
	{
		static Country()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Country), typeof(ICountry)), typeof(Country));
		}
	}

	public interface ICountry
	{
	}
}
