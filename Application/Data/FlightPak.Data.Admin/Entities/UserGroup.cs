using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IUserGroup))]
	public partial class UserGroup : IUserGroup
	{
		static UserGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UserGroup), typeof(IUserGroup)), typeof(UserGroup));
		}
	}

	public interface IUserGroup
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "User Group" + DataValidation.IsRequired)]
        string UserGroupCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string UserGroupDescription { get; set; }
	}
}
