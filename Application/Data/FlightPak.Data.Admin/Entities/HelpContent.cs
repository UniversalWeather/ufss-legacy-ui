using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IHelpContent))]
	public partial class HelpContent : IHelpContent
	{
		static HelpContent()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(HelpContent), typeof(IHelpContent)), typeof(HelpContent));
		}
	}

	public interface IHelpContent
	{
	}
}
