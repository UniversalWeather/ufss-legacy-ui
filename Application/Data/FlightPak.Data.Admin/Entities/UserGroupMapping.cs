using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Admin
{
	[MetadataType(typeof(IUserGroupMapping))]
	public partial class UserGroupMapping : IUserGroupMapping
	{
		static UserGroupMapping()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UserGroupMapping), typeof(IUserGroupMapping)), typeof(UserGroupMapping));
		}
	}

	public interface IUserGroupMapping
	{
	}
}
