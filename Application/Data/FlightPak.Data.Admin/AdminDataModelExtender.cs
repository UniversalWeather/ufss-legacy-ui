﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FlightPak.Data.Admin
{
    public partial class Customer
    {
        [DataMemberAttribute()]
        public IEnumerable<MasterCatalog.CustomerFuelVendorAccess> CustomerFuelVendorAccess { get; set; }
    }
    
    [DataContract(Name = "CustomerFuelVendorAccess")]
    public partial class CustomerFuelVendorAccess
    {
        [DataMemberAttribute()]
        public Int64 CustomerFuelVendorAccessID { get; set; }
        [DataMemberAttribute()]
        public Int64 CustomerID { get; set; }
        [DataMemberAttribute()]
        public Int64 FuelVendorID { get; set; }
        [DataMemberAttribute()]
        public bool CanAccess { get; set; }
        [DataMemberAttribute()]
        public string LastUpdUID { get; set; }
        [DataMemberAttribute()]
        public DateTime LastUpdTS { get; set; }
        [DataMemberAttribute()]
        public bool IsDeleted { get; set; }
        [DataMemberAttribute()]
        public bool IsInActive { get; set; }
    }
}
