﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FlightPak.Data.Calculations
{
    /// <summary>
    /// Class to hold Postflight Home Base setting 
    /// </summary>
    public partial class GetPOHomeBaseSetting
    {
        [DataMemberAttribute()]
        public List<GetPOFleetChargeRate> FleetChargeRates { get; set; }

        [DataMemberAttribute()]
        public List<GetPOTenthsConversion> TenthConversions { get; set; }

        [DataMemberAttribute()]
        public List<GetDutySettings> DutyConversions { get; set; }
    }
}
