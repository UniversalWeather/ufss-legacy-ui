using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Utilities
{
	[MetadataType(typeof(IItineraryPlanDetail))]
	public partial class ItineraryPlanDetail : IItineraryPlanDetail
	{
		static ItineraryPlanDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ItineraryPlanDetail), typeof(IItineraryPlanDetail)), typeof(ItineraryPlanDetail));
		}
	}

	public interface IItineraryPlanDetail
	{
	}
}
