using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Utilities
{
	[MetadataType(typeof(ICustomer))]
	public partial class Customer : ICustomer
	{
		static Customer()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Customer), typeof(ICustomer)), typeof(Customer));
		}
	}

	public interface ICustomer
	{
	}
}
