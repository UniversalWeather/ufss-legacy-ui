using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Utilities
{
	[MetadataType(typeof(IItineraryPlan))]
	public partial class ItineraryPlan : IItineraryPlan
	{
		static ItineraryPlan()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ItineraryPlan), typeof(IItineraryPlan)), typeof(ItineraryPlan));
		}
	}

	public interface IItineraryPlan
	{
	}
}
