using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Utilities
{
	[MetadataType(typeof(IAirportPair))]
	public partial class AirportPair : IAirportPair
	{
		static AirportPair()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(AirportPair), typeof(IAirportPair)), typeof(AirportPair));
		}
	}

	public interface IAirportPair
	{
	}
}
