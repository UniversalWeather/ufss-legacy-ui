using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Utilities
{
	[MetadataType(typeof(IAirportPairDetail))]
	public partial class AirportPairDetail : IAirportPairDetail
	{
		static AirportPairDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(AirportPairDetail), typeof(IAirportPairDetail)), typeof(AirportPairDetail));
		}
	}

	public interface IAirportPairDetail
	{
	}
}
