﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace FlightPak.Data.CharterQuote
{
    public partial class CQFile
    {
        [DataMember]
        public string HomeBaseAirportICAOID { get; set; }
        [DataMember]
        public Int64 HomeBaseAirportID { get; set; }

        [DataMember]
        public Int64 QuoteNumInProgress { get; set; }

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

        [DataMemberAttribute()]
        public bool AllowFileToSave { get; set; }

    }


    public partial class CQMain
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

        [DataMemberAttribute()]
        public List<FileWarehouse> ImageList { get; set; }
    }

    public partial class CQFleetChargeDetail
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQLeg
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

        [DataMemberAttribute()]
        public bool DepartAirportChanged { get; set; }

        [DataMemberAttribute()]
        public bool ArrivalAirportChanged { get; set; }

        [DataMemberAttribute()]
        public bool ShowWarningMessage { get; set; }
    }

    public partial class CQPassenger
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQFBOList
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQCateringList
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQHotelList
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQTransportList
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQQuoteMain
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQQuoteDetail
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQQuoteAdditionalFee
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }


    public partial class CQInvoiceMain
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }


    public partial class CQInvoiceQuoteDetail
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQInvoiceAdditionalFee
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class CQInvoiceSummary
    {

        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }

    }

    public partial class FileWarehouse
    {
        [DataMemberAttribute()]
        public CQRequestEntityState State { get; set; }

        [DataMemberAttribute()]
        public CQRequestActionMode Mode { get; set; }
    }

    /// <summary>
    /// Enum to identify if the CharterQuote entity 
    /// </summary>
    [DataContract(Name = "CQRequestEntityState")]
    public enum CQRequestEntityState
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Added = 1,
        [EnumMember]
        Modified = 2,
        [EnumMember]
        Deleted = 3
    }

    /// <summary>
    /// Enum to identify if the CharterQuote is in Edit mode for record locking
    /// </summary>
    [DataContract(Name = "CQRequestActionMode")]
    public enum CQRequestActionMode
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Edit = 1,
        [EnumMember]
        Saving = 2,
        [EnumMember]
        Cancelled = 3
    }


    #region ExceptionTemplate for CharterQuote Module


    public partial class ExceptionTemplate
    {
        [DataMemberAttribute()]
        public CQBusinessErrorReference CQErrorReference { get; set; }

        [DataMemberAttribute()]
        public CQBusinessErrorModule CQErrorReferenceModule { get; set; }

        [DataMemberAttribute()]
        public CQBusinessErrorSubModule CQErrorReferenceSubModule { get; set; }

        [DataMemberAttribute()]
        public string DisplayOrder { get; set; }

    }

    [DataContract(Name = "CQBusinessErrorReference")]
    public enum CQBusinessErrorReference
    {
        [EnumMember]
        OverCustomerCreditLimit = 3000,
        [EnumMember]
        DepartureAirportInActive = 3001,
        [EnumMember]
        ArrivalAirportInActive = 3002,
        [EnumMember]
        CurrentLegArrivalDateTimeOverlapsNextLegDepartureDateTime = 3003,
        [EnumMember]
        CurrentLegLocalDepartDateTimeOverlapsPrevLegLocalArrivalDateTime = 3004,
        [EnumMember]
        LegDistbeyondAircraftCapability = 3005,
        [EnumMember]
        ArrivalAirportMaxRunwayBelowAircraftRequirements = 3006,
        [EnumMember]
        LegTimeAloftAircraftCapability = 3007,
        [EnumMember]
        HomebaseReq = 3008,
        [EnumMember]
        VendorCodeReq = 3009,
        [EnumMember]
        TailNumberReq = 3010,
        [EnumMember]
        TypeCodeReq = 3011,
        [EnumMember]
        DepartIcaoReq = 3012,
        [EnumMember]
        ArrivalIcaoReq = 3013,
        [EnumMember]
        EstDepartureDate = 3014,
        [EnumMember]
        QuoteDate = 3015,
        [EnumMember]
        DepartureDateReq = 3016,
        [EnumMember]
        ArrivalDateReq = 3017,
        [EnumMember]
        PassengerExceedsAircraftCapacity = 3018,
        [EnumMember]
        QuoteHasNoLeg = 3019,
    }

    [DataContract(Name = "CQBusinessErrorModule")]
    public enum CQBusinessErrorModule
    {
        [EnumMember]
        CharterQuote = 10001,
    }

    [DataContract(Name = "CQBusinessErrorSubModule")]
    public enum CQBusinessErrorSubModule
    {
        [EnumMember]
        CQFile = 0,
        [EnumMember]
        CQMain = 1,
        [EnumMember]
        CQLeg = 2,
        [EnumMember]
        CQPax = 3,
        [EnumMember]
        CQLogistics = 4,
        [EnumMember]
        CQReports = 5,
    }
    #endregion
}
