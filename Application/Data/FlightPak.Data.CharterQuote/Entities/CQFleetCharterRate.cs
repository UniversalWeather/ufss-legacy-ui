using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQFleetCharterRate))]
	public partial class CQFleetCharterRate : ICQFleetCharterRate
	{
		static CQFleetCharterRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQFleetCharterRate), typeof(ICQFleetCharterRate)), typeof(CQFleetCharterRate));
		}
	}

	public interface ICQFleetCharterRate
	{
	}
}
