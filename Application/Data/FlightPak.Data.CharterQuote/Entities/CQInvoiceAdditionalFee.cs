using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQInvoiceAdditionalFee))]
	public partial class CQInvoiceAdditionalFee : ICQInvoiceAdditionalFee
	{
		static CQInvoiceAdditionalFee()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQInvoiceAdditionalFee), typeof(ICQInvoiceAdditionalFee)), typeof(CQInvoiceAdditionalFee));
		}
	}

	public interface ICQInvoiceAdditionalFee
	{
	}
}
