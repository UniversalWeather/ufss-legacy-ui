using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQHotelList))]
	public partial class CQHotelList : ICQHotelList
	{
		static CQHotelList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQHotelList), typeof(ICQHotelList)), typeof(CQHotelList));
		}
	}

	public interface ICQHotelList
	{
	}
}
