using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQInvoiceMain))]
	public partial class CQInvoiceMain : ICQInvoiceMain
	{
		static CQInvoiceMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQInvoiceMain), typeof(ICQInvoiceMain)), typeof(CQInvoiceMain));
		}
	}

	public interface ICQInvoiceMain
	{
	}
}
