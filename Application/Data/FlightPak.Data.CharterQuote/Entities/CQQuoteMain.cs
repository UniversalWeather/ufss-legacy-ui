using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQQuoteMain))]
	public partial class CQQuoteMain : ICQQuoteMain
	{
		static CQQuoteMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQQuoteMain), typeof(ICQQuoteMain)), typeof(CQQuoteMain));
		}
	}

	public interface ICQQuoteMain
	{
	}
}
