using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IAirport))]
	public partial class Airport : IAirport
	{
		static Airport()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Airport), typeof(IAirport)), typeof(Airport));
		}
	}

	public interface IAirport
	{
	}
}
