using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQReportDetail))]
	public partial class CQReportDetail : ICQReportDetail
	{
		static CQReportDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQReportDetail), typeof(ICQReportDetail)), typeof(CQReportDetail));
		}
	}

	public interface ICQReportDetail
	{
	}
}
