using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQQuoteSummary))]
	public partial class CQQuoteSummary : ICQQuoteSummary
	{
		static CQQuoteSummary()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQQuoteSummary), typeof(ICQQuoteSummary)), typeof(CQQuoteSummary));
		}
	}

	public interface ICQQuoteSummary
	{
	}
}
