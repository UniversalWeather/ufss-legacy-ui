using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQQuoteAdditionalFee))]
	public partial class CQQuoteAdditionalFee : ICQQuoteAdditionalFee
	{
		static CQQuoteAdditionalFee()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQQuoteAdditionalFee), typeof(ICQQuoteAdditionalFee)), typeof(CQQuoteAdditionalFee));
		}
	}

	public interface ICQQuoteAdditionalFee
	{
	}
}
