using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQMessage))]
	public partial class CQMessage : ICQMessage
	{
		static CQMessage()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQMessage), typeof(ICQMessage)), typeof(CQMessage));
		}
	}

	public interface ICQMessage
	{
	}
}
