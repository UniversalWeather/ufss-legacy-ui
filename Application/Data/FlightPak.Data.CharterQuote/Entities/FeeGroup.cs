using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IFeeGroup))]
	public partial class FeeGroup : IFeeGroup
	{
		static FeeGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FeeGroup), typeof(IFeeGroup)), typeof(FeeGroup));
		}
	}

	public interface IFeeGroup
	{
	}
}
