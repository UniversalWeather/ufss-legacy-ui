using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IView_CharterQuoteMainList))]
	public partial class View_CharterQuoteMainList : IView_CharterQuoteMainList
	{
		static View_CharterQuoteMainList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(View_CharterQuoteMainList), typeof(IView_CharterQuoteMainList)), typeof(View_CharterQuoteMainList));
		}
	}

	public interface IView_CharterQuoteMainList
	{
	}
}
