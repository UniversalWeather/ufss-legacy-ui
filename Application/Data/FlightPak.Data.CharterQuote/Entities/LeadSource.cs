using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ILeadSource))]
	public partial class LeadSource : ILeadSource
	{
		static LeadSource()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(LeadSource), typeof(ILeadSource)), typeof(LeadSource));
		}
	}

	public interface ILeadSource
	{
	}
}
