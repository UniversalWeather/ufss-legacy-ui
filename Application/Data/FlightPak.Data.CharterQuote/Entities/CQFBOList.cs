using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQFBOList))]
	public partial class CQFBOList : ICQFBOList
	{
		static CQFBOList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQFBOList), typeof(ICQFBOList)), typeof(CQFBOList));
		}
	}

	public interface ICQFBOList
	{
	}
}
