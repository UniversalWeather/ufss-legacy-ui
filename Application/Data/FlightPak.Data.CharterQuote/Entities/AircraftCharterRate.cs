using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IAircraftCharterRate))]
	public partial class AircraftCharterRate : IAircraftCharterRate
	{
		static AircraftCharterRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(AircraftCharterRate), typeof(IAircraftCharterRate)), typeof(AircraftCharterRate));
		}
	}

	public interface IAircraftCharterRate
	{
	}
}
