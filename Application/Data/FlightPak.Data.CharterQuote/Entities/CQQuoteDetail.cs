using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQQuoteDetail))]
	public partial class CQQuoteDetail : ICQQuoteDetail
	{
		static CQQuoteDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQQuoteDetail), typeof(ICQQuoteDetail)), typeof(CQQuoteDetail));
		}
	}

	public interface ICQQuoteDetail
	{
	}
}
