using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQInvoiceSummary))]
	public partial class CQInvoiceSummary : ICQInvoiceSummary
	{
		static CQInvoiceSummary()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQInvoiceSummary), typeof(ICQInvoiceSummary)), typeof(CQInvoiceSummary));
		}
	}

	public interface ICQInvoiceSummary
	{
	}
}
