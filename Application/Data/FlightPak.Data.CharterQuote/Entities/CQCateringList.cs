using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQCateringList))]
	public partial class CQCateringList : ICQCateringList
	{
		static CQCateringList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCateringList), typeof(ICQCateringList)), typeof(CQCateringList));
		}
	}

	public interface ICQCateringList
	{
	}
}
