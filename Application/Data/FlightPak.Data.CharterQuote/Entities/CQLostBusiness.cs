using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQLostBusiness))]
	public partial class CQLostBusiness : ICQLostBusiness
	{
		static CQLostBusiness()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQLostBusiness), typeof(ICQLostBusiness)), typeof(CQLostBusiness));
		}
	}

	public interface ICQLostBusiness
	{
	}
}
