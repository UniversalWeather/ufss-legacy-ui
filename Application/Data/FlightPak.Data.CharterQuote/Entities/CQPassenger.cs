using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQPassenger))]
	public partial class CQPassenger : ICQPassenger
	{
		static CQPassenger()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQPassenger), typeof(ICQPassenger)), typeof(CQPassenger));
		}
	}

	public interface ICQPassenger
	{
	}
}
