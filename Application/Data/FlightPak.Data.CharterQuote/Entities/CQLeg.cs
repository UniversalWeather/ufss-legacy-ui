using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.CharterQuote
{
    [MetadataType(typeof(ICQLeg))]
    public partial class CQLeg : ICQLeg
    {
        static CQLeg()
        {
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQLeg), typeof(ICQLeg)), typeof(CQLeg));
        }
    }

    public interface ICQLeg
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Departs ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> DAirportID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrives ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> AAirportID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Departure Date" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> DepartureDTTMLocal { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrival Date" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> ArrivalDTTMLocal { get; set; }
    }
}
