using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQReportHeader))]
	public partial class CQReportHeader : ICQReportHeader
	{
		static CQReportHeader()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQReportHeader), typeof(ICQReportHeader)), typeof(CQReportHeader));
		}
	}

	public interface ICQReportHeader
	{
	}
}
