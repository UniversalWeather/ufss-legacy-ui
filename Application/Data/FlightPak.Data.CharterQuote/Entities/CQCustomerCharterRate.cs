using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQCustomerCharterRate))]
	public partial class CQCustomerCharterRate : ICQCustomerCharterRate
	{
		static CQCustomerCharterRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCustomerCharterRate), typeof(ICQCustomerCharterRate)), typeof(CQCustomerCharterRate));
		}
	}

	public interface ICQCustomerCharterRate
	{
	}
}
