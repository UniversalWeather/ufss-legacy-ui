using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IExchangeRate))]
	public partial class ExchangeRate : IExchangeRate
	{
		static ExchangeRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ExchangeRate), typeof(IExchangeRate)), typeof(ExchangeRate));
		}
	}

	public interface IExchangeRate
	{
	}
}
