using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IFileWarehouse))]
	public partial class FileWarehouse : IFileWarehouse
	{
		static FileWarehouse()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FileWarehouse), typeof(IFileWarehouse)), typeof(FileWarehouse));
		}
	}

	public interface IFileWarehouse
	{
	}
}
