using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQInvoiceQuoteDetail))]
	public partial class CQInvoiceQuoteDetail : ICQInvoiceQuoteDetail
	{
		static CQInvoiceQuoteDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQInvoiceQuoteDetail), typeof(ICQInvoiceQuoteDetail)), typeof(CQInvoiceQuoteDetail));
		}
	}

	public interface ICQInvoiceQuoteDetail
	{
	}
}
