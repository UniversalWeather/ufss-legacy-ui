using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQCustomerContact))]
	public partial class CQCustomerContact : ICQCustomerContact
	{
		static CQCustomerContact()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCustomerContact), typeof(ICQCustomerContact)), typeof(CQCustomerContact));
		}
	}

	public interface ICQCustomerContact
	{
	}
}
