using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQFile))]
	public partial class CQFile : ICQFile
	{
		static CQFile()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQFile), typeof(ICQFile)), typeof(CQFile));
		}
	}

	public interface ICQFile
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Trip Date is Required" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> EstDepartureDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Quote Date is Required" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> QuoteDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Homebase is Required" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> HomebaseID { get; set; }
	}
}
