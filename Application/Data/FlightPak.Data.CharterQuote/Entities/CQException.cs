using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQException))]
	public partial class CQException : ICQException
	{
		static CQException()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQException), typeof(ICQException)), typeof(CQException));
		}
	}

	public interface ICQException
	{
	}
}
