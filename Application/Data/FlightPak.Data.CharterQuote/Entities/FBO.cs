using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IFBO))]
	public partial class FBO : IFBO
	{
		static FBO()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FBO), typeof(IFBO)), typeof(FBO));
		}
	}

	public interface IFBO
	{
	}
}
