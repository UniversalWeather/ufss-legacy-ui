using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQTransportList))]
	public partial class CQTransportList : ICQTransportList
	{
		static CQTransportList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQTransportList), typeof(ICQTransportList)), typeof(CQTransportList));
		}
	}

	public interface ICQTransportList
	{
	}
}
