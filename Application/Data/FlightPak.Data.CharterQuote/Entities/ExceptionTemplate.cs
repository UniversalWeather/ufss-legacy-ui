using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IExceptionTemplate))]
	public partial class ExceptionTemplate : IExceptionTemplate
	{
		static ExceptionTemplate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ExceptionTemplate), typeof(IExceptionTemplate)), typeof(ExceptionTemplate));
		}
	}

	public interface IExceptionTemplate
	{
	}
}
