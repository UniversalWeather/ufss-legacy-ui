using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQCustomer))]
	public partial class CQCustomer : ICQCustomer
	{
		static CQCustomer()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCustomer), typeof(ICQCustomer)), typeof(CQCustomer));
		}
	}

	public interface ICQCustomer
	{
	}
}
