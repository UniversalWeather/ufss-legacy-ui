using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQFleetChargeDetail))]
	public partial class CQFleetChargeDetail : ICQFleetChargeDetail
	{
		static CQFleetChargeDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQFleetChargeDetail), typeof(ICQFleetChargeDetail)), typeof(CQFleetChargeDetail));
		}
	}

	public interface ICQFleetChargeDetail
	{
	}
}
