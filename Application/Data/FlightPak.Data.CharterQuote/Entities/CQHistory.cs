using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQHistory))]
	public partial class CQHistory : ICQHistory
	{
		static CQHistory()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQHistory), typeof(ICQHistory)), typeof(CQHistory));
		}
	}

	public interface ICQHistory
	{
	}
}
