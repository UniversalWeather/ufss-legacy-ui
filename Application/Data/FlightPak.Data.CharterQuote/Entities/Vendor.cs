using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IVendor))]
	public partial class Vendor : IVendor
	{
		static Vendor()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Vendor), typeof(IVendor)), typeof(Vendor));
		}
	}

	public interface IVendor
	{
	}
}
