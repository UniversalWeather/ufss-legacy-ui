using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ISalesPerson))]
	public partial class SalesPerson : ISalesPerson
	{
		static SalesPerson()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(SalesPerson), typeof(ISalesPerson)), typeof(SalesPerson));
		}
	}

	public interface ISalesPerson
	{
	}
}
