using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICrewDutyRule))]
	public partial class CrewDutyRule : ICrewDutyRule
	{
		static CrewDutyRule()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewDutyRule), typeof(ICrewDutyRule)), typeof(CrewDutyRule));
		}
	}

	public interface ICrewDutyRule
	{
	}
}
