using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(IFleet))]
	public partial class Fleet : IFleet
	{
		static Fleet()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Fleet), typeof(IFleet)), typeof(Fleet));
		}
	}

	public interface IFleet
	{
	}
}
