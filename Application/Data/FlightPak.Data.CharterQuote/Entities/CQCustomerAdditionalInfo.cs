using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CharterQuote
{
	[MetadataType(typeof(ICQCustomerAdditionalInfo))]
	public partial class CQCustomerAdditionalInfo : ICQCustomerAdditionalInfo
	{
		static CQCustomerAdditionalInfo()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCustomerAdditionalInfo), typeof(ICQCustomerAdditionalInfo)), typeof(CQCustomerAdditionalInfo));
		}
	}

	public interface ICQCustomerAdditionalInfo
	{
	}
}
