﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace FlightPak.Data.Postflight
{
    /// <summary>
    /// PostflightMain class to hold Trip properties
    /// </summary>
    public partial class PostflightMain
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        [DataMemberAttribute()]
        public TripAction TripActionStatus { get; set; }

        /// <summary>
        /// Property to identify if the Postflight record came from Preflight or an orphan Postflight entry
        /// </summary>
        [DataMemberAttribute()]
        public bool IsPostedFromPreflight { get; set; }

        /// <summary>
        /// This Property we are using to identify if Log created via Postflight > RetreiveTrip
        /// </summary>
        [DataMemberAttribute()]
        public bool IsTripToSaveFromPreflightLogEvent { get; set; }

        /// <summary>
        /// Property to identify if the Postflight record is locked
        /// </summary>
        [DataMemberAttribute()]
        public object RecordLockInfo { get; set; }

        /// <summary>
        /// Property to store list of Copy functionalities from POMain to legs. (ID, Desc) will be copied to all legs.
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<POCopyFunctionalty, POCopyInfo> CopyInfoToAllLegs { get; set; }

        /// <summary>
        /// Property to store deleted leg id's
        /// </summary>
        [DataMemberAttribute()]
        public List<Int64> DeletedLegIDs { get; set; }

        /// <summary>
        /// Property to store deleted Expense id's
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<Int64, Int64> DeletedExpenseIDs { get; set; }

        /// <summary>
        /// Property to store deleted Crew id's
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<Int64, Int64> DeletedCrewIDs { get; set; }

        /// <summary>
        /// Property to store deleted PAX id's
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<Int64, Int64> DeletedPAXIDs { get; set; }

        /// <summary>
        /// Property to store deleted SIFL id's
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<Int64, Int64> DeletedSIFLIDs { get; set; }

        /// <summary>
        /// Property to store deleted Exception id's
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<Int64, Int64> DeletedExceptionIDs { get; set; }

        /// <summary>
        /// Property to store last updated trip level datetime
        /// </summary>
        [DataMemberAttribute()]
        public DateTime TripLastUpdatedOn { get; set; }
        /// <summary>
        /// Property to store last updated trip level User First Name
        /// </summary>
        [DataMemberAttribute()]
        public string TripLastUpdatedUser { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Trip Leg properties
    /// </summary>
    public partial class PostflightSimulatorLog
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Trip Leg properties
    /// </summary>
    public partial class PostflightLeg
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Leg Crew properties
    /// </summary>
    public partial class PostflightCrew
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        [DataMemberAttribute()]
        public string CrewCode { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Trip and Leg Passenger properties
    /// </summary>
    public partial class PostflightPassenger
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Trip Expenses related properties
    /// </summary>
    public partial class PostflightExpense
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Trip SIFL related properties
    /// </summary>
    public partial class PostflightSIFL
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        [DataMemberAttribute()]
        public int SiflIndex { get; set; } // Added for deleting particular index in UI
    }

    /// <summary>
    /// PostflightMain class to hold Trip Notes and its properties
    /// </summary>
    public partial class PostflightNote
    {
        [DataMemberAttribute()]
        public string OriginalNotes { get; set; }

        [DataMemberAttribute()]
        public TripEntityState State { get; set; }
    }

    /// <summary>
    /// PostflightMain class to hold Trip Business errors
    /// </summary>
    public partial class PostflightTripException
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }
    }

    /// <summary>
    /// Enum to identify if the Postflight entity (Leg, Passenger, Leg Expense, Crew has been added, modified or deleted)
    /// </summary>
    [DataContract(Name = "TripEntityState")]
    public enum TripEntityState
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Added = 1,
        [EnumMember]
        Modified = 2,
        [EnumMember]
        Deleted = 3
    }

    /// <summary>
    /// Enum to identify if the Postflight Trip is in Edit mode for record locking
    /// </summary>
    [DataContract(Name = "TripAction")]
    public enum TripAction
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        NewRecord = 1,
        [EnumMember]
        RecordNowLockedForEdit = 2,
        [EnumMember]
        RecordLocked = 3,
        [EnumMember]
        RecordUnLocked = 4,
        [EnumMember]
        Cancelled = 5,
    }

    /// <summary>
    /// Enum to identify if the Postflight Copy Info functionality
    /// </summary>
    [DataContract(Name = "POCopyFunctionalty")]
    public enum POCopyFunctionalty
    {
        [EnumMember]
        Requestor = 0,
        [EnumMember]
        Department = 1,
        [EnumMember]
        Authorization = 2,
        [EnumMember]
        Description = 3,
        [EnumMember]
        Account = 4,
        [EnumMember]
        FlightNumber = 5
    }

    /// <summary>
    /// Class to store Postflight Copy information to all legs
    /// </summary>
    [DataContract(Name = "POCopyInfo")]
    public class POCopyInfo
    {
        [DataMemberAttribute()]
        public bool IsCopied { get; set; }

        [DataMemberAttribute()]
        public Int64 IDToCopy { get; set; }

        [DataMemberAttribute()]
        public string DescToCopy { get; set; }

        [DataMemberAttribute()]
        public Dictionary<string, string> OtherCopyDetails { get; set; }
    }

    public partial class ExceptionTemplate
    {
        [DataMemberAttribute()]
        public POBusinessErrorReference POErrorReference { get; set; }

        [DataMemberAttribute()]
        public POBusinessErrorModule POErrorReferenceModule { get; set; }

        [DataMemberAttribute()]
        public POBusinessErrorSubModule POErrorReferenceSubModule { get; set; }

        [DataMemberAttribute()]
        public string DisplayOrder { get; set; }

    }

    [DataContract(Name = "POBusinessErrorReference")]
    public enum POBusinessErrorReference
    {
        [EnumMember]
        AircraftTailNoReq = 2000,
        [EnumMember]
        DupLogNoExist = 2001,
        [EnumMember]
        DepartureICAOIDReq = 2002,
        [EnumMember]
        ArrivalICAOIDReq = 2003,
        [EnumMember]
        BlockTimeEarlierThanSchTime = 2004,
        [EnumMember]
        SchAndBlockTimeDiffExceedsLimit = 2005,
        [EnumMember]
        LegDistBeyondAircraftCapability = 2006,
        [EnumMember]
        LegTimeAloftBeyondAircraftCapability = 2007,
        [EnumMember]
        DepartureAirportInActive = 2008,
        [EnumMember]
        ArrivalAirportInActive = 2009,
        [EnumMember]
        DepICAODiffFromLastArrivalICAO = 2010,
        [EnumMember]
        FuelOutGreaterThanAircraftMaxLimit = 2011,
        [EnumMember]
        FuelInLessThanAircraftMinLimit = 2012,
        [EnumMember]
        InvalidOutOffTimes = 2013,
        [EnumMember]
        DiffBetOutOffTimesGreaterThan30Mins = 2014,
        [EnumMember]
        InvalidOnInTimes = 2015,
        [EnumMember]
        DiffBetOnInTimesGreaterThan30Mins = 2016,
        [EnumMember]
        RequestorMissing = 2017,
        [EnumMember]
        DepartmentMissing = 2018,
        [EnumMember]
        AuthorizationMissing = 2019,
        [EnumMember]
        FlightCatagoryMissing = 2020,
        [EnumMember]
        FuelOutMissing = 2021,
        [EnumMember]
        FuelInMissing = 2022,
        [EnumMember]
        FuelUsedMissing = 2023,
        [EnumMember]
        DutyDepTmConflictWithPrevLegArrTm = 2024,
        [EnumMember]
        DutyArrTmConflictWithNextLegDepTm = 2025,
        [EnumMember]
        InConsistantOutTimeWithSchTime = 2026,
        [EnumMember]
        SchDtTmConflictWithPrevLegArrTm = 2027,
        [EnumMember]
        OutDtTmConflictWithPrevLegArrTm = 2028,
        [EnumMember]
        OffDtTmConflictWithPrevLegArrTm = 2029,
        [EnumMember]
        OnDtTmConflictWithPrevLegArrTm = 2030,
        [EnumMember]
        InDtTmConflictWithPrevLegArrTm = 2031,
        [EnumMember]
        SchDtTmConflictWithNextLegDepTm = 2032,
        [EnumMember]
        OutDtTmConflictWithNextLegDepTm = 2033,
        [EnumMember]
        OnDtTmConflictWithNextLegDepTm = 2034,
        [EnumMember]
        OffDtTmConflictWithNextLegDepTm = 2035,
        [EnumMember]
        InDtTmConflictWithNextLegDepTm = 2036,
        [EnumMember]
        TakeOffLandingMismatch = 2037,
        [EnumMember]
        NightTakeOffsNoNightTimeHours = 2038,
        [EnumMember]
        NightLandingsNoNightTimeHours = 2039,
        [EnumMember]
        NightTimeHoursGreaterThanFlightHours = 2040,
        [EnumMember]
        InstrumentTmHrsGreaterThanFlightHrs = 2041,
        [EnumMember]
        AccountNumberRequired = 2042,
        [EnumMember]
        YearNumberIsRequired = 2043,
        [EnumMember]
        TripRequiresAtleastoneLeg = 2044,
        [EnumMember]
        InvalidScheduledDate = 2045,
        [EnumMember]
        LogDepartmentInactive = 2046,
        [EnumMember]
        LogAuthorizationInactive = 2047,
        [EnumMember]
        LegDepartmentInactive = 2048,
        [EnumMember]
        LegAuthorizationInactive = 2049,
        [EnumMember]
        CrewTravellingInternationalLegDoesNotHavePassport = 2050,
        [EnumMember]
        PassengerTravellingInternationalLegDoesNotHavePassport = 2051,
        [EnumMember]        
        FuelUsedNotNegativeValue = 2052
    }

    [DataContract(Name = "POBusinessErrorModule")]
    public enum POBusinessErrorModule
    {
        [EnumMember]
        Postflight = 10009,
    }

    [DataContract(Name = "POBusinessErrorSubModule")]
    public enum POBusinessErrorSubModule
    {
        [EnumMember]
        PostflightMain = 1,
        [EnumMember]
        Legs = 2,
        [EnumMember]
        Crew = 3,
        [EnumMember]
        Passenger = 4,
        [EnumMember]
        Expense = 5,
        [EnumMember]
        OtherCrewDutyLog = 6,
        [EnumMember]
        ExpenseCatalog = 7
    }
}
