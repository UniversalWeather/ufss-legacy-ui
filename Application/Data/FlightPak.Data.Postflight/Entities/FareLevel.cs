using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IFareLevel))]
	public partial class FareLevel : IFareLevel
	{
		static FareLevel()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FareLevel), typeof(IFareLevel)), typeof(FareLevel));
		}
	}

	public interface IFareLevel
	{
	}
}
