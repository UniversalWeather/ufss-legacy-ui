using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.Postflight
{
    [MetadataType(typeof(IPostflightSimulatorLog))]
    public partial class PostflightSimulatorLog : IPostflightSimulatorLog
    {
        static PostflightSimulatorLog()
        {
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightSimulatorLog), typeof(IPostflightSimulatorLog)), typeof(PostflightSimulatorLog));
        }
    }

    public interface IPostflightSimulatorLog
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Crew Code" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> CrewID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Crew Duty Type Code" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> DutyTypeID { get; set; }
    }
}
