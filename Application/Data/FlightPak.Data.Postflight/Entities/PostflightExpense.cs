using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightExpense))]
	public partial class PostflightExpense : IPostflightExpense
	{
		static PostflightExpense()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightExpense), typeof(IPostflightExpense)), typeof(PostflightExpense));
		}
	}

	public interface IPostflightExpense
	{
	}
}
