using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IFlightCatagory))]
	public partial class FlightCatagory : IFlightCatagory
	{
		static FlightCatagory()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FlightCatagory), typeof(IFlightCatagory)), typeof(FlightCatagory));
		}
	}

	public interface IFlightCatagory
	{
	}
}
