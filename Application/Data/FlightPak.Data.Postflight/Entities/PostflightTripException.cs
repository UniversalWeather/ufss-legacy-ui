using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightTripException))]
	public partial class PostflightTripException : IPostflightTripException
	{
		static PostflightTripException()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightTripException), typeof(IPostflightTripException)), typeof(PostflightTripException));
		}
	}

	public interface IPostflightTripException
	{
	}
}
