using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPaymentType))]
	public partial class PaymentType : IPaymentType
	{
		static PaymentType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PaymentType), typeof(IPaymentType)), typeof(PaymentType));
		}
	}

	public interface IPaymentType
	{
	}
}
