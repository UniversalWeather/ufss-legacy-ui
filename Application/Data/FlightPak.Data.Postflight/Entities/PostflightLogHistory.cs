using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightLogHistory))]
	public partial class PostflightLogHistory : IPostflightLogHistory
	{
		static PostflightLogHistory()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightLogHistory), typeof(IPostflightLogHistory)), typeof(PostflightLogHistory));
		}
	}

	public interface IPostflightLogHistory
	{
	}
}
