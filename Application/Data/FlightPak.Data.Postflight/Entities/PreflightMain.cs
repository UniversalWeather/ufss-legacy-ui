using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPreflightMain))]
	public partial class PreflightMain : IPreflightMain
	{
		static PreflightMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightMain), typeof(IPreflightMain)), typeof(PreflightMain));
		}
	}

	public interface IPreflightMain
	{
	}
}
