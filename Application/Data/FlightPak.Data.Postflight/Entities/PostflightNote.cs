using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightNote))]
	public partial class PostflightNote : IPostflightNote
	{
		static PostflightNote()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightNote), typeof(IPostflightNote)), typeof(PostflightNote));
		}
	}

	public interface IPostflightNote
	{
	}
}
