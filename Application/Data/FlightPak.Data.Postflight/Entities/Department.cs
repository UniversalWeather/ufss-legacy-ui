using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IDepartment))]
	public partial class Department : IDepartment
	{
		static Department()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Department), typeof(IDepartment)), typeof(Department));
		}
	}

	public interface IDepartment
	{
	}
}
