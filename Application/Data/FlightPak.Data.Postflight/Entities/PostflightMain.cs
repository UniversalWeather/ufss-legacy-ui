using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightMain))]
	public partial class PostflightMain : IPostflightMain
	{
		static PostflightMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightMain), typeof(IPostflightMain)), typeof(PostflightMain));
		}
	}

	public interface IPostflightMain
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tail No." + DataValidation.IsRequired)]
        Nullable<global::System.Int64> FleetID { get; set; }
	}
}
