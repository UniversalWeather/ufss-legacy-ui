using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(ICrewDutyType))]
	public partial class CrewDutyType : ICrewDutyType
	{
		static CrewDutyType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewDutyType), typeof(ICrewDutyType)), typeof(CrewDutyType));
		}
	}

	public interface ICrewDutyType
	{
	}
}
