using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightLeg))]
	public partial class PostflightLeg : IPostflightLeg
	{
		static PostflightLeg()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightLeg), typeof(IPostflightLeg)), typeof(PostflightLeg));
		}
	}

	public interface IPostflightLeg
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Departs ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> DepartICAOID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrives ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> ArriveICAOID { get; set; }
	}
}
