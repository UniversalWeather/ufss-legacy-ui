using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightSIFL))]
	public partial class PostflightSIFL : IPostflightSIFL
	{
		static PostflightSIFL()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightSIFL), typeof(IPostflightSIFL)), typeof(PostflightSIFL));
		}
	}

	public interface IPostflightSIFL
	{
	}
}
