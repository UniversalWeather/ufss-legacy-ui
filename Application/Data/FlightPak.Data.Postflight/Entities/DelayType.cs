using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IDelayType))]
	public partial class DelayType : IDelayType
	{
		static DelayType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DelayType), typeof(IDelayType)), typeof(DelayType));
		}
	}

	public interface IDelayType
	{
	}
}
