using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightCrew))]
	public partial class PostflightCrew : IPostflightCrew
	{
		static PostflightCrew()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightCrew), typeof(IPostflightCrew)), typeof(PostflightCrew));
		}
	}

	public interface IPostflightCrew
	{
	}
}
