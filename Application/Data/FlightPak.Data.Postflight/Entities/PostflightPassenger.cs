using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IPostflightPassenger))]
	public partial class PostflightPassenger : IPostflightPassenger
	{
		static PostflightPassenger()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightPassenger), typeof(IPostflightPassenger)), typeof(PostflightPassenger));
		}
	}

	public interface IPostflightPassenger
	{
	}
}
