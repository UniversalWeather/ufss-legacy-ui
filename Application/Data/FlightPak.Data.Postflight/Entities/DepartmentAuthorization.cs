using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Postflight
{
	[MetadataType(typeof(IDepartmentAuthorization))]
	public partial class DepartmentAuthorization : IDepartmentAuthorization
	{
		static DepartmentAuthorization()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DepartmentAuthorization), typeof(IDepartmentAuthorization)), typeof(DepartmentAuthorization));
		}
	}

	public interface IDepartmentAuthorization
	{
	}
}
