﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Data.Common
{
    public static class PessimisticLock
    {
        public static ReturnValue<TEntity> Lock<TEntity>(this TEntity entity, string currentUser, Nullable<long> customerId = null, Nullable<Guid> sessionId = null) where TEntity : IEntityWithKey
        {
            using (Model1Container context = new Model1Container())
            {
                ReturnValue<TEntity> returnValue = new ReturnValue<TEntity>();
                ObjectParameter param = new ObjectParameter(LockingConstants.ReturnValue, typeof(int));
                context.Lock(customerId, entity.EntityKey.EntitySetName, sessionId, currentUser, (long?)entity.EntityKey.EntityKeyValues[0].Value, LockingConstants.Lock, param);
                switch ((int)param.Value)
                {
                    case -101:
                        returnValue.ReturnFlag = true;
                        returnValue.LockMessage = LockingConstants.Status101;
                        returnValue.EntityInfo = entity;
                        break;
                    case -102:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.Status102;
                        returnValue.EntityInfo = entity;
                        break;
                    default:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.StatusUnknown;
                        returnValue.EntityInfo = entity;
                        break;
                }
                return returnValue;
            }
            //return new ReturnValue<TEntity>();
        }

        public static ReturnValue<TEntity> UnLock<TEntity>(this TEntity entity, string currentUser, Nullable<long> customerId = null, Nullable<Guid> sessionId = null) where TEntity : IEntityWithKey
        {
            using (Model1Container context = new Model1Container())
            {
                ReturnValue<TEntity> returnValue = new ReturnValue<TEntity>();
                ObjectParameter param = new ObjectParameter(LockingConstants.ReturnValue, typeof(int));
                context.Lock(customerId, entity.EntityKey.EntitySetName, sessionId, currentUser, (long?)entity.EntityKey.EntityKeyValues[0].Value, LockingConstants.UnLock, param);
                switch ((int)param.Value)
                {
                    case -103:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.Status103;
                        returnValue.EntityInfo = entity;
                        break;
                    case -104:
                        returnValue.ReturnFlag = true;
                        returnValue.LockMessage = LockingConstants.Status104;
                        returnValue.EntityInfo = entity;
                        break;
                    default:
                        returnValue.ReturnFlag = false;
                        returnValue.LockMessage = LockingConstants.StatusUnknown;
                        returnValue.EntityInfo = entity;
                        break;
                }
                return returnValue;
            }
        }
    }

    internal class LockingConstants
    {
        public const string Status101 = "Locked successfully";
        public const string Status102 = "Record already locked";
        public const string Status103 = "Unlock failiure";
        public const string Status104 = "Unlocked successfully";
        public const string StatusUnknown = "Unkown error happened while locking";
        public const string Lock = "LOCK";
        public const string UnLock = "UNLOCK";
        public const string ReturnValue = "ReturnValue";
    }
}
