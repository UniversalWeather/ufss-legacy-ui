﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;

namespace FlightPak.Data.Common
{
    public interface IMasterCatalogQuery<T>
    {
        bool Add(T t);
        bool Update(T t);
        bool Delete(T t);
        List<T> GetList();
    }
}
