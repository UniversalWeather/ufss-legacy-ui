﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlightPak.Data.Common
{
    public interface ICacheable
    {
        void RefreshCache();
    }
}
