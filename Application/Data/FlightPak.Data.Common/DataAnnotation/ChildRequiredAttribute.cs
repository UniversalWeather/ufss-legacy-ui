﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using System.Collections;

namespace FlightPak.Data.Common.DataAnnotation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.Property, AllowMultiple = false)]
    public class ChildRequiredAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as IList;
            if (list != null && list.Count > 0)
            {
                return true;
            }
            return false;

        }
    }
}
