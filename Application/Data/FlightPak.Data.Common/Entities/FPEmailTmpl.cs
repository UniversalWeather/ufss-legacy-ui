using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Common
{
	[MetadataType(typeof(IFPEmailTmpl))]
	public partial class FPEmailTmpl : IFPEmailTmpl
	{
		static FPEmailTmpl()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FPEmailTmpl), typeof(IFPEmailTmpl)), typeof(FPEmailTmpl));
		}
	}

	public interface IFPEmailTmpl
	{
	}
}
