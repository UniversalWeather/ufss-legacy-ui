using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Common
{
	[MetadataType(typeof(IFPBookmark))]
	public partial class FPBookmark : IFPBookmark
	{
		static FPBookmark()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FPBookmark), typeof(IFPBookmark)), typeof(FPBookmark));
		}
	}

	public interface IFPBookmark
	{
	}
}
