using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Common
{
	[MetadataType(typeof(IFPSystemPref))]
	public partial class FPSystemPref : IFPSystemPref
	{
		static FPSystemPref()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FPSystemPref), typeof(IFPSystemPref)), typeof(FPSystemPref));
		}
	}

	public interface IFPSystemPref
	{
	}
}
