using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Common
{
	[MetadataType(typeof(IFPLoginSession))]
	public partial class FPLoginSession : IFPLoginSession
	{
		static FPLoginSession()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FPLoginSession), typeof(IFPLoginSession)), typeof(FPLoginSession));
		}
	}

	public interface IFPLoginSession
	{
	}
}
