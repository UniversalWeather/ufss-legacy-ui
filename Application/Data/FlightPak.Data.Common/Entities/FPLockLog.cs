using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Common
{
	[MetadataType(typeof(IFPLockLog))]
	public partial class FPLockLog : IFPLockLog
	{
		static FPLockLog()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FPLockLog), typeof(IFPLockLog)), typeof(FPLockLog));
		}
	}

	public interface IFPLockLog
	{
	}
}
