using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Common
{
	[MetadataType(typeof(IUserPreference))]
	public partial class UserPreference : IUserPreference
	{
		static UserPreference()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UserPreference), typeof(IUserPreference)), typeof(UserPreference));
		}
	}

	public interface IUserPreference
	{
	}
}
