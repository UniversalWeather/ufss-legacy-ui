using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITripManagerCheckListGroup))]
	public partial class TripManagerCheckListGroup : ITripManagerCheckListGroup
	{
		static TripManagerCheckListGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TripManagerCheckListGroup), typeof(ITripManagerCheckListGroup)), typeof(TripManagerCheckListGroup));
		}
	}

	public interface ITripManagerCheckListGroup
	{
         [Required(AllowEmptyStrings = false, ErrorMessage = "Checklist Group Code" + DataValidation.IsRequired)]
        string CheckGroupCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
         string CheckGroupDescription { get; set; } 
	}
}
