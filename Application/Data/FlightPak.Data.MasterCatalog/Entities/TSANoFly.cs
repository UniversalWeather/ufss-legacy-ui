using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITSANoFly))]
	public partial class TSANoFly : ITSANoFly
	{
		static TSANoFly()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TSANoFly), typeof(ITSANoFly)), typeof(TSANoFly));
		}
	}

	public interface ITSANoFly
	{
	}
}
