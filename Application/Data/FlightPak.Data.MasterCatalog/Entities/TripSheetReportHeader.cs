using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITripSheetReportHeader))]
	public partial class TripSheetReportHeader : ITripSheetReportHeader
	{
		static TripSheetReportHeader()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TripSheetReportHeader), typeof(ITripSheetReportHeader)), typeof(TripSheetReportHeader));
		}
	}

	public interface ITripSheetReportHeader
	{
	}
}
