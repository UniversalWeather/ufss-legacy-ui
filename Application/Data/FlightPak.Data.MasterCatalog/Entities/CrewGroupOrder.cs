using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewGroupOrder))]
	public partial class CrewGroupOrder : ICrewGroupOrder
	{
		static CrewGroupOrder()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewGroupOrder), typeof(ICrewGroupOrder)), typeof(CrewGroupOrder));
		}
	}

	public interface ICrewGroupOrder
	{
	}
}
