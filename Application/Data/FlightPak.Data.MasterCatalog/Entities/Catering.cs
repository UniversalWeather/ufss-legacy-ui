using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICatering))]
	public partial class Catering : ICatering
	{
		static Catering()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Catering), typeof(ICatering)), typeof(Catering));
		}
	}

	public interface ICatering
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Company Name" + DataValidation.IsRequired)]
        string CateringVendor { get; set; }
	}
}
