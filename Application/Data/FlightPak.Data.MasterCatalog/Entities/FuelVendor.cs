using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelVendor))]
	public partial class FuelVendor : IFuelVendor
	{
		static FuelVendor()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelVendor), typeof(IFuelVendor)), typeof(FuelVendor));
		}
	}

	public interface IFuelVendor
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name" + DataValidation.IsRequired)]
        string VendorName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string VendorDescription { get; set; }
	}
}
