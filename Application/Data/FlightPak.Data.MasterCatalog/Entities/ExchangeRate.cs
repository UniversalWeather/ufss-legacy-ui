using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IExchangeRate))]
	public partial class ExchangeRate : IExchangeRate
	{
		static ExchangeRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ExchangeRate), typeof(IExchangeRate)), typeof(ExchangeRate));
		}
	}

	public interface IExchangeRate
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Rate Code" + DataValidation.IsRequired)]
        string ExchRateCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string ExchRateDescription { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "From Date" + DataValidation.IsRequired)]
        DateTime? FromDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "To Date" + DataValidation.IsRequired)]
        DateTime? ToDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Exchange rate" + DataValidation.IsRequired)]
        [RegularExpression("^[0-9]{0,4}(\\.[0-9]{1,5})?$", ErrorMessage = "Exchange rate" + DataValidation.InvalidFormat)]
        Decimal? ExchRate { get; set; }
        
	}
}
