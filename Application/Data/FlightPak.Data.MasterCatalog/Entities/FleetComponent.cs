using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetComponent))]
	public partial class FleetComponent : IFleetComponent
	{
		static FleetComponent()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetComponent), typeof(IFleetComponent)), typeof(FleetComponent));
		}
	}

	public interface IFleetComponent
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Serial Number" + DataValidation.IsRequired)]
        string SericalNum { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Account Description" + DataValidation.IsRequired)]
        string FleetComponentDescription { get; set; }
	}
}
