using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IHotel))]
	public partial class Hotel : IHotel
	{
		static Hotel()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Hotel), typeof(IHotel)), typeof(Hotel));
		}
	}

	public interface IHotel
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Hotel Name" + DataValidation.IsRequired)]
        string Name { get; set; }
	}
}
