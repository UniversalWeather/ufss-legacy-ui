using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewGroup))]
	public partial class CrewGroup : ICrewGroup
	{
		static CrewGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewGroup), typeof(ICrewGroup)), typeof(CrewGroup));
		}
	}

	public interface ICrewGroup
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Crew Group Code" + DataValidation.IsRequired)]
        string CrewGroupCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string CrewGroupDescription { get; set; }
	}
}
