using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICompany))]
	public partial class Company : ICompany
	{
		static Company()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Company), typeof(ICompany)), typeof(Company));
		}
	}

	public interface ICompany
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "MainBase " + DataValidation.IsRequired)]
        Int64? HomebaseAirportID { get; set; }

        [RegularExpression("^[0-9]{0,2}(\\.[0-9]{1,2})?$", ErrorMessage = "Ground Time Domestic" + DataValidation.InvalidFormat)]
        decimal? GroundTM { get; set; }

        [RegularExpression("^[0-9]{0,2}(\\.[0-9]{1,2})?$", ErrorMessage = "Ground Time International" + DataValidation.InvalidFormat)]
        decimal? GroundTMIntl { get; set; }

        [RegularExpression(".*[T]|[W]|[U]|[C]|[H].*", ErrorMessage = "Trip Manager Default Status" + DataValidation.TripMGRDefaultStatus)]
        string TripMGRDefaultStatus { get; set; }

        [Range(0, 3, ErrorMessage = "Crew Log Custom Labels Long 3 Dec" + DataValidation.Specdec)]
        Int32? Specdec3 { get; set; }
        
        [Range(0,3, ErrorMessage = "Crew Log Custom Labels Long 4 Dec" + DataValidation.Specdec)]
        Int32? Specdec4 { get; set; }
        
	}
}
