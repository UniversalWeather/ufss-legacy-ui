using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetChargeRate))]
	public partial class FleetChargeRate : IFleetChargeRate
	{
		static FleetChargeRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetChargeRate), typeof(IFleetChargeRate)), typeof(FleetChargeRate));
		}
	}

	public interface IFleetChargeRate
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Begin Date" + DataValidation.IsRequired)]
        DateTime? BeginRateDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "End Date" + DataValidation.IsRequired)]
        DateTime? EndRateDT { get; set; }
	}
}
