using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewDutyType))]
	public partial class CrewDutyType : ICrewDutyType
	{
		static CrewDutyType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewDutyType), typeof(ICrewDutyType)), typeof(CrewDutyType));
		}
	}

	public interface ICrewDutyType
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string DutyTypeCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string DutyTypesDescription { get; set; }
	}
}
