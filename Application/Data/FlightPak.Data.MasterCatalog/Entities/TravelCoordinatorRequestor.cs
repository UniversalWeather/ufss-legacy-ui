using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITravelCoordinatorRequestor))]
	public partial class TravelCoordinatorRequestor : ITravelCoordinatorRequestor
	{
		static TravelCoordinatorRequestor()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TravelCoordinatorRequestor), typeof(ITravelCoordinatorRequestor)), typeof(TravelCoordinatorRequestor));
		}
	}

	public interface ITravelCoordinatorRequestor
	{
	}
}
