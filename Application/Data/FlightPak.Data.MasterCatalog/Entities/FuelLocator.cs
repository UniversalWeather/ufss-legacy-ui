using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelLocator))]
	public partial class FuelLocator : IFuelLocator
	{
		static FuelLocator()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelLocator), typeof(IFuelLocator)), typeof(FuelLocator));
		}
	}

    public interface IFuelLocator
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Locator Code" + DataValidation.IsRequired)]
        string FuelLocatorCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string FuelLocatorDescription { get; set; }

    }
}
