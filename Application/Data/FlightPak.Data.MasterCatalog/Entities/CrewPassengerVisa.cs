using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewPassengerVisa))]
	public partial class CrewPassengerVisa : ICrewPassengerVisa
	{
		static CrewPassengerVisa()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewPassengerVisa), typeof(ICrewPassengerVisa)), typeof(CrewPassengerVisa));
		}
	}

	public interface ICrewPassengerVisa
	{
	}
}
