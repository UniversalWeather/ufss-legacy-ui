using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFeeGroup))]
	public partial class FeeGroup : IFeeGroup
	{
		static FeeGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FeeGroup), typeof(IFeeGroup)), typeof(FeeGroup));
		}
	}

	public interface IFeeGroup
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string FeeGroupCD { get; set; }
	}
}
