using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFareLevel))]
	public partial class FareLevel : IFareLevel
	{
		static FareLevel()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FareLevel), typeof(IFareLevel)), typeof(FareLevel));
		}
	}

	public interface IFareLevel
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Start Date" + DataValidation.IsRequired)]
        DateTime? StartDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "End Date" + DataValidation.IsRequired)]
        DateTime? EndDT { get; set; }

        [RegularExpression("^[0-9]{0,3}(\\.[0-9]{1,4})?$", ErrorMessage = "Fare Level 1" + DataValidation.InvalidFormat)]
        decimal? Rate1 { get; set; }

        [RegularExpression("^[0-9]{0,3}(\\.[0-9]{1,4})?$", ErrorMessage = "Fare Level 2" + DataValidation.InvalidFormat)]
        decimal? Rate2 { get; set; }

        [RegularExpression("^[0-9]{0,3}(\\.[0-9]{1,4})?$", ErrorMessage = "Fare Level 3" + DataValidation.InvalidFormat)]
        decimal? Rate3 { get; set; }

        [RegularExpression("^[0-9]{0,3}(\\.[0-9]{1,4})?$", ErrorMessage = "Terminal Charge" + DataValidation.InvalidFormat)]
        decimal? TerminalCHG { get; set; }
	}
}
