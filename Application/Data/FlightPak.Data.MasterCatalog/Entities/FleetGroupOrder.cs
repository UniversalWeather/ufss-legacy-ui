using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetGroupOrder))]
	public partial class FleetGroupOrder : IFleetGroupOrder
	{
		static FleetGroupOrder()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetGroupOrder), typeof(IFleetGroupOrder)), typeof(FleetGroupOrder));
		}
	}

	public interface IFleetGroupOrder
	{
	}
}
