using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewRating))]
	public partial class CrewRating : ICrewRating
	{
		static CrewRating()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewRating), typeof(ICrewRating)), typeof(CrewRating));
		}
	} 

	public interface ICrewRating
	{
	}
}
