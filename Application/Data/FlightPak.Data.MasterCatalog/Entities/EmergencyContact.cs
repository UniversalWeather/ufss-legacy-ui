using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IEmergencyContact))]
	public partial class EmergencyContact : IEmergencyContact
	{
		static EmergencyContact()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(EmergencyContact), typeof(IEmergencyContact)), typeof(EmergencyContact));
		}
	}

	public interface IEmergencyContact
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Emergency Contact Code" + DataValidation.IsRequired)]
        string EmergencyContactCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name" + DataValidation.IsRequired)]
        string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name" + DataValidation.IsRequired)]
        string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Business Phone" + DataValidation.IsRequired)]
        string BusinessPhone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Business E-mail" + DataValidation.IsRequired)]
        string EmailAddress { get; set; }
	}
}
