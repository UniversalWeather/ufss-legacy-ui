using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IAircraft))]
	public partial class Aircraft : IAircraft
	{
		static Aircraft()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Aircraft), typeof(IAircraft)), typeof(Aircraft));
		}
	}

	public interface IAircraft
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Aircraft Type Code" + DataValidation.IsRequired)]
        string AircraftCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Aircraft Description" + DataValidation.IsRequired)]
        string AircraftDescription { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Power Setting" + DataValidation.IsRequired)]
        string PowerSetting { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Max Wind Altitude" + DataValidation.IsRequired)]
        decimal? WindAltitude { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "True Air Speed" + DataValidation.ConstantMach + DataValidation.IsRequired)]
        decimal? PowerSettings1TrueAirSpeed { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "True Air Speed" + DataValidation.LongRangeCruise + DataValidation.IsRequired)]
        decimal? PowerSettings2TrueAirSpeed { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "True Air Speed" + DataValidation.HighSpeedCruise + DataValidation.IsRequired)]
        decimal? PowerSettings3TrueAirSpeed { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Hour Range" + DataValidation.ConstantMach + DataValidation.IsRequired)]
        decimal? PowerSettings1HourRange { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Hour Range" + DataValidation.LongRangeCruise + DataValidation.IsRequired)]
        decimal? PowerSettings2HourRange { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Hour Range" + DataValidation.HighSpeedCruise + DataValidation.IsRequired)]
        decimal? PowerSettings3HourRange { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "T/O Bias" + DataValidation.ConstantMach + DataValidation.IsRequired)]
        decimal? PowerSettings1TakeOffBias { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "T/O Bias" + DataValidation.LongRangeCruise + DataValidation.IsRequired)]
        decimal? PowerSettings2TakeOffBias { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "T/O Bias" + DataValidation.HighSpeedCruise + DataValidation.IsRequired)]
        decimal? PowerSettings3TakeOffBias { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Lnd Bias" + DataValidation.ConstantMach + DataValidation.IsRequired)]
        decimal? PowerSettings1LandingBias { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Lnd Bias" + DataValidation.LongRangeCruise + DataValidation.IsRequired)]
        decimal? PowerSettings2LandingBias { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Lnd Bias" + DataValidation.HighSpeedCruise + DataValidation.IsRequired)]
        decimal? PowerSettings3LandingBias { get; set; }

	}
}
