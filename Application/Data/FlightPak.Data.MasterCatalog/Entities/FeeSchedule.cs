using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFeeSchedule))]
	public partial class FeeSchedule : IFeeSchedule
	{
		static FeeSchedule()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FeeSchedule), typeof(IFeeSchedule)), typeof(FeeSchedule));
		}
	}

	public interface IFeeSchedule
	{
         [Required(AllowEmptyStrings = false, ErrorMessage = "Fee Schedule Group Code" + DataValidation.IsRequired)]
         Int64? FeeGroupID { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "Account Number" + DataValidation.IsRequired)]
         Int64? AccountID { get; set; }
	}
}
