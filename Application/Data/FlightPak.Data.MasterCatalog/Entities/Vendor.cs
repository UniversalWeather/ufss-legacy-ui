using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IVendor))]
	public partial class Vendor : IVendor
	{
		static Vendor()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Vendor), typeof(IVendor)), typeof(Vendor));
		}
	}

	public interface IVendor
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string VendorCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Vendor Name" + DataValidation.IsRequired)]
        string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Name" + DataValidation.IsRequired)]
        string BillingName { get; set; }

        //[RegularExpression("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*",ErrorMessage="Other Email" + DataValidation.InvalidEmailFormat)]
        //string EmailID { get; set; }

        //[RegularExpression("[nN]|[sS]",ErrorMessage="")]
        //string LatitudeNorthSouth { get; set; }

        [Range(0, 60, ErrorMessage = "Latitude Minutes" + DataValidation.LatitudeMinutes)]
        decimal? LatitudeMinutes { get; set; }

        [Range(0, 90, ErrorMessage = "Latitude Degree" + DataValidation.LatitudeDegree)]
        decimal? LatitudeDegree { get; set; }

        ////[RegularExpression("[eE]|[wW]",ErrorMessage="")]
        ////string LongitudeEastWest { get; set; }
                
        [Range(0, 60, ErrorMessage = "Longitude Minutes" + DataValidation.LongitudeMinutes)]
        decimal? LongitudeMinutes { get; set; }


        [Range(0, 180, ErrorMessage = "Longitude Degree" + DataValidation.LongitudeDegree)]
        decimal? LongitudeDegree { get; set; }
	}
}
