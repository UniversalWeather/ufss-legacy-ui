using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPassengerCheckListDetail))]
	public partial class PassengerCheckListDetail : IPassengerCheckListDetail
	{
		static PassengerCheckListDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PassengerCheckListDetail), typeof(IPassengerCheckListDetail)), typeof(PassengerCheckListDetail));
		}
	}

	public interface IPassengerCheckListDetail
	{
	}
}
