using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IMetro))]
	public partial class Metro : IMetro
	{
		static Metro()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Metro), typeof(IMetro)), typeof(Metro));
		}
	}

	public interface IMetro
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Metro Code" + DataValidation.IsRequired)]
        string MetroCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string MetroName { get; set; }
	}
}
