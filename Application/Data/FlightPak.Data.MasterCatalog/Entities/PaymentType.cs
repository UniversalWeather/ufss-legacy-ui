using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPaymentType))]
	public partial class PaymentType : IPaymentType
	{
		static PaymentType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PaymentType), typeof(IPaymentType)), typeof(PaymentType));
		}
	}

	public interface IPaymentType
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Payment Code" + DataValidation.IsRequired)]
        string PaymentTypeCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string PaymentTypeDescription { get; set; }
	}
}
