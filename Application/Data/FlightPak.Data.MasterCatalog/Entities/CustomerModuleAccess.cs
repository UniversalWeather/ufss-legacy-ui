using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICustomerModuleAccess))]
	public partial class CustomerModuleAccess : ICustomerModuleAccess
	{
		static CustomerModuleAccess()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CustomerModuleAccess), typeof(ICustomerModuleAccess)), typeof(CustomerModuleAccess));
		}
	}

	public interface ICustomerModuleAccess
	{
	}
}
