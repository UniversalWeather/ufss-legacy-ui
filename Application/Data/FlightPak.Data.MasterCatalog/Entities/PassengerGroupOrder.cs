using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPassengerGroupOrder))]
	public partial class PassengerGroupOrder : IPassengerGroupOrder
	{
		static PassengerGroupOrder()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PassengerGroupOrder), typeof(IPassengerGroupOrder)), typeof(PassengerGroupOrder));
		}
	}

	public interface IPassengerGroupOrder
	{
	}
}
