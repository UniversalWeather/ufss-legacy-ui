using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelFileXsltParser))]
	public partial class FuelFileXsltParser : IFuelFileXsltParser
	{
		static FuelFileXsltParser()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelFileXsltParser), typeof(IFuelFileXsltParser)), typeof(FuelFileXsltParser));
		}
	}

	public interface IFuelFileXsltParser
	{
	}
}
