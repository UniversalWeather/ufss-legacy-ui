using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IAirport))]
	public partial class Airport : IAirport
	{
		static Airport()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Airport), typeof(IAirport)), typeof(Airport));
		}
	}

	public interface IAirport
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "ICAO" + DataValidation.IsRequired)]
        string IcaoID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Latitude NS" + DataValidation.IsRequired)]
        string LatitudeNorthSouth { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Longitude EW" + DataValidation.IsRequired)]
        string LongitudeEastWest { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Region" + DataValidation.IsRequired)]
        Int64? DSTRegionID { get; set; }
	}
}
