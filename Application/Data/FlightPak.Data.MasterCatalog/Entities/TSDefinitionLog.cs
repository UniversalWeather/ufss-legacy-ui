using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITSDefinitionLog))]
	public partial class TSDefinitionLog : ITSDefinitionLog
	{
		static TSDefinitionLog()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TSDefinitionLog), typeof(ITSDefinitionLog)), typeof(TSDefinitionLog));
		}
	}

	public interface ITSDefinitionLog
	{
	}
}
