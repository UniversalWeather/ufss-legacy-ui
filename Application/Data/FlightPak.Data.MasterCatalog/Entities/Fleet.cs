using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleet))]
	public partial class Fleet : IFleet
	{
		static Fleet()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Fleet), typeof(IFleet)), typeof(Fleet));
		}
	}

	public interface IFleet
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Aircraft Code" + DataValidation.IsRequired)]
        string AircraftCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tail No." + DataValidation.IsRequired)]
        string TailNum { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Serial No." + DataValidation.IsRequired)]
        string SerialNum { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Type Code" + DataValidation.IsRequired)]
        string TypeDescription { get; set; }        
	}
}
