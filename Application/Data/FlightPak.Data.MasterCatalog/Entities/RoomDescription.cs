using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IRoomDescription))]
	public partial class RoomDescription : IRoomDescription
	{
		static RoomDescription()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(RoomDescription), typeof(IRoomDescription)), typeof(RoomDescription));
		}
	}

	public interface IRoomDescription
	{
	}
}
