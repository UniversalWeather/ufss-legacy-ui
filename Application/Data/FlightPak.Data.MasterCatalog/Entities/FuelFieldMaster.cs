using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelFieldMaster))]
	public partial class FuelFieldMaster : IFuelFieldMaster
	{
		static FuelFieldMaster()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelFieldMaster), typeof(IFuelFieldMaster)), typeof(FuelFieldMaster));
		}
	}

	public interface IFuelFieldMaster
	{
	}
}
