using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelFieldSynonym))]
	public partial class FuelFieldSynonym : IFuelFieldSynonym
	{
		static FuelFieldSynonym()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelFieldSynonym), typeof(IFuelFieldSynonym)), typeof(FuelFieldSynonym));
		}
	}

	public interface IFuelFieldSynonym
	{
	}
}
