using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IRunway))]
	public partial class Runway : IRunway
	{
		static Runway()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Runway), typeof(IRunway)), typeof(Runway));
		}
	}

	public interface IRunway
	{
	}
}
