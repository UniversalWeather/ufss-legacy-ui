using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITripSheetReportDetail))]
	public partial class TripSheetReportDetail : ITripSheetReportDetail
	{
		static TripSheetReportDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TripSheetReportDetail), typeof(ITripSheetReportDetail)), typeof(TripSheetReportDetail));
		}
	}

	public interface ITripSheetReportDetail
	{
	}
}
