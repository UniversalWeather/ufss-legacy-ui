using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IDepartmentGroupOrder))]
	public partial class DepartmentGroupOrder : IDepartmentGroupOrder
	{
		static DepartmentGroupOrder()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DepartmentGroupOrder), typeof(IDepartmentGroupOrder)), typeof(DepartmentGroupOrder));
		}
	}

	public interface IDepartmentGroupOrder
	{
	}
}
