using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IAircraftDuty))]
	public partial class AircraftDuty : IAircraftDuty
	{
		static AircraftDuty()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(AircraftDuty), typeof(IAircraftDuty)), typeof(AircraftDuty));
		}
	}

	public interface IAircraftDuty
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Aircraft Duty Code" + DataValidation.IsRequired)]
        string AircraftDutyCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string AircraftDutyDescription { get; set; }
	}
}
