using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewCheckList))]
	public partial class CrewCheckList : ICrewCheckList
	{
		static CrewCheckList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewCheckList), typeof(ICrewCheckList)), typeof(CrewCheckList));
		}
	}

	public interface ICrewCheckList
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Checklist Code" + DataValidation.IsRequired)]
        string CrewCheckCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string CrewChecklistDescription { get; set; }
	}
}
