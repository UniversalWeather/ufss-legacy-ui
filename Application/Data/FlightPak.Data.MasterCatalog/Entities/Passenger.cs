using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPassenger))]
	public partial class Passenger : IPassenger
	{
		static Passenger()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Passenger), typeof(IPassenger)), typeof(Passenger));
		}
	}

	public interface IPassenger
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "PAX First Name " + DataValidation.IsRequired)]
        string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "PAX Last Name " + DataValidation.IsRequired)]
        string LastName { get; set; }
	}
}
