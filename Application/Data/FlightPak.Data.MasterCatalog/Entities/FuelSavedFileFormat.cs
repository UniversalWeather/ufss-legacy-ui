using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelSavedFileFormat))]
	public partial class FuelSavedFileFormat : IFuelSavedFileFormat
	{
		static FuelSavedFileFormat()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelSavedFileFormat), typeof(IFuelSavedFileFormat)), typeof(FuelSavedFileFormat));
		}
	}

	public interface IFuelSavedFileFormat
	{
	}
}
