using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IRoomType))]
	public partial class RoomType : IRoomType
	{
		static RoomType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(RoomType), typeof(IRoomType)), typeof(RoomType));
		}
	}

	public interface IRoomType
	{
	}
}
