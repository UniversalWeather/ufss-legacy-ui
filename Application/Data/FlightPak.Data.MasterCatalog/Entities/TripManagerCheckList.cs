using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITripManagerCheckList))]
	public partial class TripManagerCheckList : ITripManagerCheckList
	{
		static TripManagerCheckList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TripManagerCheckList), typeof(ITripManagerCheckList)), typeof(TripManagerCheckList));
		}
	}

	public interface ITripManagerCheckList
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Checklist Code" + DataValidation.IsRequired)]
        string CheckListCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Group Code" + DataValidation.IsRequired)]
        Int64? CheckGroupID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string CheckListDescription { get; set; }
	}
}
