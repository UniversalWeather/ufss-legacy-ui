using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IWorldWind))]
	public partial class WorldWind : IWorldWind
	{
		static WorldWind()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(WorldWind), typeof(IWorldWind)), typeof(WorldWind));
		}
	}

	public interface IWorldWind
	{
	}
}
