using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICustomerFuelVendorAccess))]
	public partial class CustomerFuelVendorAccess : ICustomerFuelVendorAccess
	{
		static CustomerFuelVendorAccess()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CustomerFuelVendorAccess), typeof(ICustomerFuelVendorAccess)), typeof(CustomerFuelVendorAccess));
		}
	}

	public interface ICustomerFuelVendorAccess
	{
	}
}
