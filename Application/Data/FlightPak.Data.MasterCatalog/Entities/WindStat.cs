using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IWindStat))]
	public partial class WindStat : IWindStat
	{
		static WindStat()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(WindStat), typeof(IWindStat)), typeof(WindStat));
		}
	}

	public interface IWindStat
	{
	}
}
