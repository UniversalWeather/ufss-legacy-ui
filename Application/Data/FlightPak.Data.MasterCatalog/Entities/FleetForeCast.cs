using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetForeCast))]
	public partial class FleetForeCast : IFleetForeCast
	{
		static FleetForeCast()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetForeCast), typeof(IFleetForeCast)), typeof(FleetForeCast));
		}
	}

	public interface IFleetForeCast
	{
	}
}
