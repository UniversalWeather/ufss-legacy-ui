using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ISalesPerson))]
	public partial class SalesPerson : ISalesPerson
	{
		static SalesPerson()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(SalesPerson), typeof(ISalesPerson)), typeof(SalesPerson));
		}
	}

	public interface ISalesPerson
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Sales Person Code" + DataValidation.IsRequired)]
        string SalesPersonCD { get; set; }
	}
}
