using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFlightCatagory))]
	public partial class FlightCatagory : IFlightCatagory
	{
		static FlightCatagory()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FlightCatagory), typeof(IFlightCatagory)), typeof(FlightCatagory));
		}
	}

	public interface IFlightCatagory
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Flight Category Code" + DataValidation.IsRequired)]
        string FlightCatagoryCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string FlightCatagoryDescription { get; set; }
	}
}
