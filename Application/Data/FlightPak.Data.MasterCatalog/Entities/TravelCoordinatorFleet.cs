using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITravelCoordinatorFleet))]
	public partial class TravelCoordinatorFleet : ITravelCoordinatorFleet
	{
		static TravelCoordinatorFleet()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TravelCoordinatorFleet), typeof(ITravelCoordinatorFleet)), typeof(TravelCoordinatorFleet));
		}
	}

	public interface ITravelCoordinatorFleet
	{
	}
}
