using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
    [MetadataType(typeof(ICQCustomer))]
    public partial class CQCustomer : ICQCustomer
    {
        static CQCustomer()
        {
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCustomer), typeof(ICQCustomer)), typeof(CQCustomer));
        }
    }

    public interface ICQCustomer
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Customer Code" + DataValidation.IsRequired)]
        string CQCustomerCD { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Customer Name" + DataValidation.IsRequired)]
        string CQCustomerName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name" + DataValidation.IsRequired)]
        string BillingName { get; set; }
    }


}
