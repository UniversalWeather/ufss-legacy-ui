using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITSFlightLog))]
	public partial class TSFlightLog : ITSFlightLog
	{
		static TSFlightLog()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TSFlightLog), typeof(ITSFlightLog)), typeof(TSFlightLog));
		}
	}

	public interface ITSFlightLog
	{
	}
}
