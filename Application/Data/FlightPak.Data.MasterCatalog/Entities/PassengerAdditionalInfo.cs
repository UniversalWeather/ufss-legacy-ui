using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPassengerAdditionalInfo))]
	public partial class PassengerAdditionalInfo : IPassengerAdditionalInfo
	{
		static PassengerAdditionalInfo()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PassengerAdditionalInfo), typeof(IPassengerAdditionalInfo)), typeof(PassengerAdditionalInfo));
		}
	}

	public interface IPassengerAdditionalInfo
	{
	}
}
