using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IClientCodeMapping))]
	public partial class ClientCodeMapping : IClientCodeMapping
	{
		static ClientCodeMapping()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ClientCodeMapping), typeof(IClientCodeMapping)), typeof(ClientCodeMapping));
		}
	}

	public interface IClientCodeMapping
	{
	}
}
