using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewDutyRules))]
	public partial class CrewDutyRules : ICrewDutyRules
	{
		static CrewDutyRules()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewDutyRules), typeof(ICrewDutyRules)), typeof(CrewDutyRules));
		}
	}

	public interface ICrewDutyRules
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Duty Rule Code" + DataValidation.IsRequired)]
        string CrewDutyRuleCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string CrewDutyRulesDescription { get; set; }
	}
}
