using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetGroup))]
	public partial class FleetGroup : IFleetGroup
	{
		static FleetGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetGroup), typeof(IFleetGroup)), typeof(FleetGroup));
		}
	}

	public interface IFleetGroup
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Fleet Group Code" + DataValidation.IsRequired)]
        string FleetGroupCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string FleetGroupDescription { get; set; }
	}
}
