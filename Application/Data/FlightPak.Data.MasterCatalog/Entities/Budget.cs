using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IBudget))]
	public partial class Budget : IBudget
	{
		static Budget()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Budget), typeof(IBudget)), typeof(Budget));
		}
	}
    //Code for Data Anotation
	public interface IBudget
	{
        //Place the code below for Data Anotations

        //[Range(Minvalue, MaxValue, ErrorMessage = "")]
        //[RegularExpression("RegexPattern",ErrorMessage="")]
        //[StringLength(MaxCharacters,ErrorMessage ="")];
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Account No." + DataValidation.IsRequired)]
        string AccountNum { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Year" + DataValidation.IsRequired)]
        [Range(1900, 2100, ErrorMessage = "Year" + DataValidation.YearValidation)]
        string FiscalYear { get; set; }                
	}
}
