using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IConversionTable))]
	public partial class ConversionTable : IConversionTable
	{
		static ConversionTable()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(ConversionTable), typeof(IConversionTable)), typeof(ConversionTable));
		}
	}

	public interface IConversionTable
	{
	}
}
