using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICQLostBusiness))]
	public partial class CQLostBusiness : ICQLostBusiness
	{
		static CQLostBusiness()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQLostBusiness), typeof(ICQLostBusiness)), typeof(CQLostBusiness));
		}
	}

	public interface ICQLostBusiness
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string CQLostBusinessCD { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string CQLostBusinessDescription { get; set; }
	}
}
