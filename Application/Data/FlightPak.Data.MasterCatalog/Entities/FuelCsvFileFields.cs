using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFuelCsvFileFields))]
	public partial class FuelCsvFileFields : IFuelCsvFileFields
	{
		static FuelCsvFileFields()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelCsvFileFields), typeof(IFuelCsvFileFields)), typeof(FuelCsvFileFields));
		}
	}

	public interface IFuelCsvFileFields
	{
	}
}
