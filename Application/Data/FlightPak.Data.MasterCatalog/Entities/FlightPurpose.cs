using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFlightPurpose))]
	public partial class FlightPurpose : IFlightPurpose
	{
		static FlightPurpose()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FlightPurpose), typeof(IFlightPurpose)), typeof(FlightPurpose));
		}
	}

	public interface IFlightPurpose
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Flight Purpose Code" + DataValidation.IsRequired)]
        string FlightPurposeCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string FlightPurposeDescription { get; set; }
	}
}
