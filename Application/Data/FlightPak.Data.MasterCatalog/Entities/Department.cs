using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IDepartment))]
	public partial class Department : IDepartment
	{
		static Department()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Department), typeof(IDepartment)), typeof(Department));
		}
	}

	public interface IDepartment
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Department Code" + DataValidation.IsRequired)]
        string DepartmentCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string DepartmentName { get; set; }

        //CustomValidator
        //Invalid Client Code
        //Unique Department Code is Required.
	}
}
