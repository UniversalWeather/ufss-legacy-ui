using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewAircraftAssigned))]
	public partial class CrewAircraftAssigned : ICrewAircraftAssigned
	{
		static CrewAircraftAssigned()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewAircraftAssigned), typeof(ICrewAircraftAssigned)), typeof(CrewAircraftAssigned));
		}
	}

	public interface ICrewAircraftAssigned
	{
	}
}
