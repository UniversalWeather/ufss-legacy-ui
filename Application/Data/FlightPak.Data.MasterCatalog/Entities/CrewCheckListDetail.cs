using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewCheckListDetail))]
	public partial class CrewCheckListDetail : ICrewCheckListDetail
	{
		static CrewCheckListDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewCheckListDetail), typeof(ICrewCheckListDetail)), typeof(CrewCheckListDetail));
		}
	}

	public interface ICrewCheckListDetail
	{
	}
}
