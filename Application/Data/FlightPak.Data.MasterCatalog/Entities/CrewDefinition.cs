using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewDefinition))]
	public partial class CrewDefinition : ICrewDefinition
	{
		static CrewDefinition()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewDefinition), typeof(ICrewDefinition)), typeof(CrewDefinition));
		}
	}

	public interface ICrewDefinition
	{
	}
}
