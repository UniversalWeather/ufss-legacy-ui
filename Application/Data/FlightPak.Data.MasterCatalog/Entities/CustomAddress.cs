using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICustomAddress))]
	public partial class CustomAddress : ICustomAddress
	{
		static CustomAddress()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CustomAddress), typeof(ICustomAddress)), typeof(CustomAddress));
		}
	}

	public interface ICustomAddress
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Custom Address Code" + DataValidation.IsRequired)]
        string CustomAddressCD { get; set; }

        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*",ErrorMessage="Other E-mail" + DataValidation.InvalidEmailFormat)]
        string OtherEmail { get; set; }

        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*",ErrorMessage="Personal E-mail" + DataValidation.InvalidEmailFormat)]
        string PersonalEmail { get; set; }

        [RegularExpression("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*",ErrorMessage="Business E-mail" + DataValidation.InvalidEmailFormat)]
        string BusinessEmail { get; set; }    
	}
}
