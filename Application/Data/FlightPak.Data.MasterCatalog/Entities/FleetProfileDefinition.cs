using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetProfileDefinition))]
	public partial class FleetProfileDefinition : IFleetProfileDefinition
	{
		static FleetProfileDefinition()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetProfileDefinition), typeof(IFleetProfileDefinition)), typeof(FleetProfileDefinition));
		}
	}

	public interface IFleetProfileDefinition
	{
	}
}
