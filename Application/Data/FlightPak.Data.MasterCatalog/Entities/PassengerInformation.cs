using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPassengerInformation))]
	public partial class PassengerInformation : IPassengerInformation
	{
		static PassengerInformation()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PassengerInformation), typeof(IPassengerInformation)), typeof(PassengerInformation));
		}
	}

	public interface IPassengerInformation
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Additional Info Code" + DataValidation.IsRequired)]
        string PassengerInfoCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string PassengerDescription { get; set; }
	}
}
