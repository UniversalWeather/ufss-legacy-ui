using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewInformation))]
	public partial class CrewInformation : ICrewInformation
	{
		static CrewInformation()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewInformation), typeof(ICrewInformation)), typeof(CrewInformation));
		}
	}

	public interface ICrewInformation
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Additional Info Code" + DataValidation.IsRequired)]
        string CrewInfoCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string CrewInformationDescription { get; set; }
	}
}
