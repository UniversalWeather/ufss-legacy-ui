using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITransport))]
	public partial class Transport : ITransport
	{
		static Transport()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Transport), typeof(ITransport)), typeof(Transport));
		}
	}

	public interface ITransport
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Company Name" + DataValidation.IsRequired)]
        string TransportationVendor { get; set; }
	}
}
