using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IDepartmentGroup))]
	public partial class DepartmentGroup : IDepartmentGroup
	{
		static DepartmentGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DepartmentGroup), typeof(IDepartmentGroup)), typeof(DepartmentGroup));
		}
	}

	public interface IDepartmentGroup
	{
         [Required(AllowEmptyStrings = false, ErrorMessage = "Department Group Code" + DataValidation.IsRequired)]
         string DepartmentGroupCD { get; set; }

         [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
         string DepartmentGroupDescription { get; set; }
	}
}
