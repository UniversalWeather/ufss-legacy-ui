using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICustomerLicense))]
	public partial class CustomerLicense : ICustomerLicense
	{
		static CustomerLicense()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CustomerLicense), typeof(ICustomerLicense)), typeof(CustomerLicense));
		}
	}

	public interface ICustomerLicense
	{
	}
}
