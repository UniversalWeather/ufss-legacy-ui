using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetNewCharterRate))]
	public partial class FleetNewCharterRate : IFleetNewCharterRate
	{
		static FleetNewCharterRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetNewCharterRate), typeof(IFleetNewCharterRate)), typeof(FleetNewCharterRate));
		}
	}

	public interface IFleetNewCharterRate
	{
	}
}
