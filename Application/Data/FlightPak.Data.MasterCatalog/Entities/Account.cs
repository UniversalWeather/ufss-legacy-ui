using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IAccount))]
	public partial class Account : IAccount
	{
		static Account()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Account), typeof(IAccount)), typeof(Account));
		}
	}
    //For Data Anotation
	public interface IAccount
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Account No." + DataValidation.IsRequired)]
        string AccountNum { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Account Description" + DataValidation.IsRequired)]
        string AccountDescription { get; set; }
	}
}
