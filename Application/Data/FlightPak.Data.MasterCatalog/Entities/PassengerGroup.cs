using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IPassengerGroup))]
	public partial class PassengerGroup : IPassengerGroup
	{
		static PassengerGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PassengerGroup), typeof(IPassengerGroup)), typeof(PassengerGroup));
		}
	}

	public interface IPassengerGroup
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string PassengerGroupCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string PassengerGroupName { get; set; }
	}
}
