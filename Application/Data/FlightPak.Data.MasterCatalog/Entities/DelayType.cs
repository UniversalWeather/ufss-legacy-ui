using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IDelayType))]
	public partial class DelayType : IDelayType
	{
		static DelayType()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DelayType), typeof(IDelayType)), typeof(DelayType));
		}
	}

	public interface IDelayType
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Delay Type Code" + DataValidation.IsRequired)]
        string DelayTypeCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string DelayTypeDescription { get; set; }
	}
}
