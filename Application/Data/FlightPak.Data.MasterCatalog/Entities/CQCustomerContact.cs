using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICQCustomerContact))]
	public partial class CQCustomerContact : ICQCustomerContact
	{
		static CQCustomerContact()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQCustomerContact), typeof(ICQCustomerContact)), typeof(CQCustomerContact));
		}
	}

	public interface ICQCustomerContact
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name" + DataValidation.IsRequired)]
        string FirstName { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name" + DataValidation.IsRequired)]
        string LastName { get; set; }
	}
}
