using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITravelCoordinator))]
	public partial class TravelCoordinator : ITravelCoordinator
	{
		static TravelCoordinator()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TravelCoordinator), typeof(ITravelCoordinator)), typeof(TravelCoordinator));
		}
	}

	public interface ITravelCoordinator
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string TravelCoordCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name" + DataValidation.IsRequired)]
        string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name" + DataValidation.IsRequired)]
        string LastName { get; set; }
	}
}
