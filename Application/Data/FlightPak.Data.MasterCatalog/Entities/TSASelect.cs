using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITSASelect))]
	public partial class TSASelect : ITSASelect
	{
		static TSASelect()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TSASelect), typeof(ITSASelect)), typeof(TSASelect));
		}
	}

	public interface ITSASelect
	{
	}
}
