using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetInformation))]
	public partial class FleetInformation : IFleetInformation
	{
		static FleetInformation()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetInformation), typeof(IFleetInformation)), typeof(FleetInformation));
		}
	}

	public interface IFleetInformation
	{
	}
}
