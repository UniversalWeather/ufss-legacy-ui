using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ITSAClear))]
	public partial class TSAClear : ITSAClear
	{
		static TSAClear()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TSAClear), typeof(ITSAClear)), typeof(TSAClear));
		}
	}

	public interface ITSAClear
	{
	}
}
