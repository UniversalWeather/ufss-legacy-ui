using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IHistoricalFuelPrice))]
	public partial class HistoricalFuelPrice : IHistoricalFuelPrice
	{
		static HistoricalFuelPrice()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(HistoricalFuelPrice), typeof(IHistoricalFuelPrice)), typeof(HistoricalFuelPrice));
		}
	}

	public interface IHistoricalFuelPrice
	{
	}
}
