using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFBO))]
	public partial class FBO : IFBO
	{
		static FBO()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FBO), typeof(IFBO)), typeof(FBO));
		}
	}

    public interface IFBO
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "FBO Name" + DataValidation.IsRequired)]
        string FBOVendor { get; set; }
    }
}
