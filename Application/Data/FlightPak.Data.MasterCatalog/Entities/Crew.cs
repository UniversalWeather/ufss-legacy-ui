using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrew))]
	public partial class Crew : ICrew
	{
		static Crew()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Crew), typeof(ICrew)), typeof(Crew));
		}
	}

	public interface ICrew
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name" + DataValidation.IsRequired)]
        string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name" + DataValidation.IsRequired)]
        string LastName { get; set; }
	}
}
