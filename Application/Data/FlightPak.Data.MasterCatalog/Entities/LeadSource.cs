using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ILeadSource))]
	public partial class LeadSource : ILeadSource
	{
		static LeadSource()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(LeadSource), typeof(ILeadSource)), typeof(LeadSource));
		}
	}

	public interface ILeadSource
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Lead Source Code" + DataValidation.IsRequired)]
        string LeadSourceCD { get; set; }
	}
}
