using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IFleetProfileInformation))]
	public partial class FleetProfileInformation : IFleetProfileInformation
	{
		static FleetProfileInformation()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetProfileInformation), typeof(IFleetProfileInformation)), typeof(FleetProfileInformation));
		}
	}

	public interface IFleetProfileInformation
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code" + DataValidation.IsRequired)]
        string FleetInfoCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string FleetProfileAddInfDescription { get; set; }  
	}
}
