using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(ICrewDutyTypeXRef))]
	public partial class CrewDutyTypeXRef : ICrewDutyTypeXRef
	{
		static CrewDutyTypeXRef()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewDutyTypeXRef), typeof(ICrewDutyTypeXRef)), typeof(CrewDutyTypeXRef));
		}
	}

	public interface ICrewDutyTypeXRef
	{
	}
}
