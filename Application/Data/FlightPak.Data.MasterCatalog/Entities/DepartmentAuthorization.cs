using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;
namespace FlightPak.Data.MasterCatalog
{
	[MetadataType(typeof(IDepartmentAuthorization))]
	public partial class DepartmentAuthorization : IDepartmentAuthorization
	{
		static DepartmentAuthorization()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(DepartmentAuthorization), typeof(IDepartmentAuthorization)), typeof(DepartmentAuthorization));
		}
	}

	public interface IDepartmentAuthorization
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Authorization Code" + DataValidation.IsRequired)]
        string AuthorizationCD { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Description" + DataValidation.IsRequired)]
        string DeptAuthDescription { get; set; }
	}
}
