﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace FlightPak.Data.MasterCatalog
{
    public partial class FlightpakFuel
    {
        [SkipProperty]
        [DataMemberAttribute()]
        public string InvalidRecordErrorMessage { get; set; }
        [SkipProperty]
        [DataMemberAttribute()]
        public string AirportIdentifier { get; set; }
    }

    public class SkipPropertyAttribute : Attribute { /* Skip Property Attribute class */ }
}
