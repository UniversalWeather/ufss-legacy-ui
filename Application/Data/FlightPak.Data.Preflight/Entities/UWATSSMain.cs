using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IUWATSSMain))]
	public partial class UWATSSMain : IUWATSSMain
	{
		static UWATSSMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UWATSSMain), typeof(IUWATSSMain)), typeof(UWATSSMain));
		}
	}

	public interface IUWATSSMain
	{
	}
}
