using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ITripManagerCheckListGroup))]
	public partial class TripManagerCheckListGroup : ITripManagerCheckListGroup
	{
		static TripManagerCheckListGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TripManagerCheckListGroup), typeof(ITripManagerCheckListGroup)), typeof(TripManagerCheckListGroup));
		}
	}

	public interface ITripManagerCheckListGroup
	{
	}
}
