using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightCrewList))]
	public partial class PreflightCrewList : IPreflightCrewList
	{
		static PreflightCrewList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightCrewList), typeof(IPreflightCrewList)), typeof(PreflightCrewList));
		}
	}

	public interface IPreflightCrewList
	{
	}
}
