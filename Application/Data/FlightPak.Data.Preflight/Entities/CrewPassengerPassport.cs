using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ICrewPassengerPassport))]
	public partial class CrewPassengerPassport : ICrewPassengerPassport
	{
		static CrewPassengerPassport()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewPassengerPassport), typeof(ICrewPassengerPassport)), typeof(CrewPassengerPassport));
		}
	}

	public interface ICrewPassengerPassport
	{
	}
}
