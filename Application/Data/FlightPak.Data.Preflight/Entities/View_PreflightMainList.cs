using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IView_PreflightMainList))]
	public partial class View_PreflightMainList : IView_PreflightMainList
	{
		static View_PreflightMainList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(View_PreflightMainList), typeof(IView_PreflightMainList)), typeof(View_PreflightMainList));
		}
	}

	public interface IView_PreflightMainList
	{
	}
}
