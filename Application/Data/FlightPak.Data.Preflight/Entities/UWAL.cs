using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IUWAL))]
	public partial class UWAL : IUWAL
	{
		static UWAL()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UWAL), typeof(IUWAL)), typeof(UWAL));
		}
	}

	public interface IUWAL
	{
	}
}
