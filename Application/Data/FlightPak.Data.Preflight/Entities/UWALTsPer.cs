using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IUWALTsPer))]
	public partial class UWALTsPer : IUWALTsPer
	{
		static UWALTsPer()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UWALTsPer), typeof(IUWALTsPer)), typeof(UWALTsPer));
		}
	}

	public interface IUWALTsPer
	{
	}
}
