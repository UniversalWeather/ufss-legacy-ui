using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.Preflight
{
    [MetadataType(typeof(IPreflightLeg))]
    public partial class PreflightLeg : IPreflightLeg
    {
        static PreflightLeg()
        {
            TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightLeg), typeof(IPreflightLeg)), typeof(PreflightLeg));
        }
    }

    public interface IPreflightLeg
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Departs ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> DepartICAOID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrives ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> ArriveICAOID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Departure" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> DepartureDTTMLocal { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrival" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> ArrivalDTTMLocal { get; set; }
    }
}
