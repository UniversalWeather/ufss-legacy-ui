using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightCateringList))]
	public partial class PreflightCateringList : IPreflightCateringList
	{
		static PreflightCateringList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightCateringList), typeof(IPreflightCateringList)), typeof(PreflightCateringList));
		}
	}

	public interface IPreflightCateringList
	{
	}
}
