using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ITripManagerCheckList))]
	public partial class TripManagerCheckList : ITripManagerCheckList
	{
		static TripManagerCheckList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TripManagerCheckList), typeof(ITripManagerCheckList)), typeof(TripManagerCheckList));
		}
	}

	public interface ITripManagerCheckList
	{
	}
}
