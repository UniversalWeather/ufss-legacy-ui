using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightTransportList))]
	public partial class PreflightTransportList : IPreflightTransportList
	{
		static PreflightTransportList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightTransportList), typeof(IPreflightTransportList)), typeof(PreflightTransportList));
		}
	}

	public interface IPreflightTransportList
	{
	}
}
