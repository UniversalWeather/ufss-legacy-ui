using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightTripException))]
	public partial class PreflightTripException : IPreflightTripException
	{
		static PreflightTripException()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightTripException), typeof(IPreflightTripException)), typeof(PreflightTripException));
		}
	}

	public interface IPreflightTripException
	{
	}
}
