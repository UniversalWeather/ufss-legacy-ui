using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPostflightMain))]
	public partial class PostflightMain : IPostflightMain
	{
		static PostflightMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PostflightMain), typeof(IPostflightMain)), typeof(PostflightMain));
		}
	}

	public interface IPostflightMain
	{
	}
}
