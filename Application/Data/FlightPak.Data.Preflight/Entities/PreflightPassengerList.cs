using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightPassengerList))]
	public partial class PreflightPassengerList : IPreflightPassengerList
	{
		static PreflightPassengerList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightPassengerList), typeof(IPreflightPassengerList)), typeof(PreflightPassengerList));
		}
	}

	public interface IPreflightPassengerList
	{
	}
}
