using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IHotel))]
	public partial class Hotel : IHotel
	{
		static Hotel()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Hotel), typeof(IHotel)), typeof(Hotel));
		}
	}

	public interface IHotel
	{
	}
}
