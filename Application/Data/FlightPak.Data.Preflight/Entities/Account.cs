using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IAccount))]
	public partial class Account : IAccount
	{
		static Account()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Account), typeof(IAccount)), typeof(Account));
		}
	}

	public interface IAccount
	{
	}
}
