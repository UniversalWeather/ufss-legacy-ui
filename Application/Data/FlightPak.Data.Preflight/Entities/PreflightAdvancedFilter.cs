using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightAdvancedFilter))]
	public partial class PreflightAdvancedFilter : IPreflightAdvancedFilter
	{
		static PreflightAdvancedFilter()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightAdvancedFilter), typeof(IPreflightAdvancedFilter)), typeof(PreflightAdvancedFilter));
		}
	}

	public interface IPreflightAdvancedFilter
	{
	}
}
