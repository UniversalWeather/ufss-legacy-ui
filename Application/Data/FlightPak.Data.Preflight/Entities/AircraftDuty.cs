using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IAircraftDuty))]
	public partial class AircraftDuty : IAircraftDuty
	{
		static AircraftDuty()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(AircraftDuty), typeof(IAircraftDuty)), typeof(AircraftDuty));
		}
	}

	public interface IAircraftDuty
	{
	}
}
