using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightCheckList))]
	public partial class PreflightCheckList : IPreflightCheckList
	{
		static PreflightCheckList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightCheckList), typeof(IPreflightCheckList)), typeof(PreflightCheckList));
		}
	}

	public interface IPreflightCheckList
	{
	}
}
