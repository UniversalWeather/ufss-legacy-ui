using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightScheduleCalenderPreference))]
	public partial class PreflightScheduleCalenderPreference : IPreflightScheduleCalenderPreference
	{
		static PreflightScheduleCalenderPreference()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightScheduleCalenderPreference), typeof(IPreflightScheduleCalenderPreference)), typeof(PreflightScheduleCalenderPreference));
		}
	}

	public interface IPreflightScheduleCalenderPreference
	{
	}
}
