using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightHotelList))]
	public partial class PreflightHotelList : IPreflightHotelList
	{
		static PreflightHotelList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightHotelList), typeof(IPreflightHotelList)), typeof(PreflightHotelList));
		}
	}

	public interface IPreflightHotelList
	{
	}
}
