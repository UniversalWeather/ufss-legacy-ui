using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightTripHistory))]
	public partial class PreflightTripHistory : IPreflightTripHistory
	{
		static PreflightTripHistory()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightTripHistory), typeof(IPreflightTripHistory)), typeof(PreflightTripHistory));
		}
	}

	public interface IPreflightTripHistory
	{
	}
}
