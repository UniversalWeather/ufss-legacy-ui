using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFleetChargeRate))]
	public partial class FleetChargeRate : IFleetChargeRate
	{
		static FleetChargeRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetChargeRate), typeof(IFleetChargeRate)), typeof(FleetChargeRate));
		}
	}

	public interface IFleetChargeRate
	{
	}
}
