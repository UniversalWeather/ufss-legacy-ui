using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ICQFile))]
	public partial class CQFile : ICQFile
	{
		static CQFile()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQFile), typeof(ICQFile)), typeof(CQFile));
		}
	}

	public interface ICQFile
	{
	}
}
