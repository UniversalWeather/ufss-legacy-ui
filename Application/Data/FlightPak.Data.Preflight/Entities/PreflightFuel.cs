using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightFuel))]
	public partial class PreflightFuel : IPreflightFuel
	{
		static PreflightFuel()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightFuel), typeof(IPreflightFuel)), typeof(PreflightFuel));
		}
	}

	public interface IPreflightFuel
	{
	}
}
