using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ICrewCheckList))]
	public partial class CrewCheckList : ICrewCheckList
	{
		static CrewCheckList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewCheckList), typeof(ICrewCheckList)), typeof(CrewCheckList));
		}
	}

	public interface ICrewCheckList
	{
	}
}
