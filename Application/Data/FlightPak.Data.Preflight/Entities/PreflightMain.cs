using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightMain))]
	public partial class PreflightMain : IPreflightMain
	{
		static PreflightMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightMain), typeof(IPreflightMain)), typeof(PreflightMain));
		}
	}

	public interface IPreflightMain
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Depart Date" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> EstDepartureDT { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Type Code" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> AircraftID { get; set; }
	}
}
