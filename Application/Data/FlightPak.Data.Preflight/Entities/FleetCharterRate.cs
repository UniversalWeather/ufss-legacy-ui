using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFleetCharterRate))]
	public partial class FleetCharterRate : IFleetCharterRate
	{
		static FleetCharterRate()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetCharterRate), typeof(IFleetCharterRate)), typeof(FleetCharterRate));
		}
	}

	public interface IFleetCharterRate
	{
	}
}
