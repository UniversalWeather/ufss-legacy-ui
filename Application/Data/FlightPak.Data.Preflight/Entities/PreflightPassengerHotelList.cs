using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightPassengerHotelList))]
	public partial class PreflightPassengerHotelList : IPreflightPassengerHotelList
	{
		static PreflightPassengerHotelList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightPassengerHotelList), typeof(IPreflightPassengerHotelList)), typeof(PreflightPassengerHotelList));
		}
	}

	public interface IPreflightPassengerHotelList
	{
	}
}
