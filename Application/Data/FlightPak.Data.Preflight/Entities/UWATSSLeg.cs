using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IUWATSSLeg))]
	public partial class UWATSSLeg : IUWATSSLeg
	{
		static UWATSSLeg()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UWATSSLeg), typeof(IUWATSSLeg)), typeof(UWATSSLeg));
		}
	}

	public interface IUWATSSLeg
	{
	}
}
