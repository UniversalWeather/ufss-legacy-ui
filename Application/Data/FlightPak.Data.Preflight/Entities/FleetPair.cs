using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFleetPair))]
	public partial class FleetPair : IFleetPair
	{
		static FleetPair()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetPair), typeof(IFleetPair)), typeof(FleetPair));
		}
	}

	public interface IFleetPair
	{
	}
}
