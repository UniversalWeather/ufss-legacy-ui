using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IUWASpServ))]
	public partial class UWASpServ : IUWASpServ
	{
		static UWASpServ()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UWASpServ), typeof(IUWASpServ)), typeof(UWASpServ));
		}
	}

	public interface IUWASpServ
	{
	}
}
