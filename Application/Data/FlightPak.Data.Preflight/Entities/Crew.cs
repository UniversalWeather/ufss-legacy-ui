using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ICrew))]
	public partial class Crew : ICrew
	{
		static Crew()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Crew), typeof(ICrew)), typeof(Crew));
		}
	}

	public interface ICrew
	{
	}
}
