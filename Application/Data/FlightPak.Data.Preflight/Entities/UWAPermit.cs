using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IUWAPermit))]
	public partial class UWAPermit : IUWAPermit
	{
		static UWAPermit()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UWAPermit), typeof(IUWAPermit)), typeof(UWAPermit));
		}
	}

	public interface IUWAPermit
	{
	}
}
