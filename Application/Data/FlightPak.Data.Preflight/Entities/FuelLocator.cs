using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFuelLocator))]
	public partial class FuelLocator : IFuelLocator
	{
		static FuelLocator()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelLocator), typeof(IFuelLocator)), typeof(FuelLocator));
		}
	}

	public interface IFuelLocator
	{
	}
}
