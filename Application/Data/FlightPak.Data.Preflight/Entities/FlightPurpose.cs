using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFlightPurpose))]
	public partial class FlightPurpose : IFlightPurpose
	{
		static FlightPurpose()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FlightPurpose), typeof(IFlightPurpose)), typeof(FlightPurpose));
		}
	}

	public interface IFlightPurpose
	{
	}
}
