using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightHotelCrewPassengerList))]
	public partial class PreflightHotelCrewPassengerList : IPreflightHotelCrewPassengerList
	{
		static PreflightHotelCrewPassengerList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightHotelCrewPassengerList), typeof(IPreflightHotelCrewPassengerList)), typeof(PreflightHotelCrewPassengerList));
		}
	}

	public interface IPreflightHotelCrewPassengerList
	{
	}
}
