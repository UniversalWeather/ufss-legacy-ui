using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IAircraft))]
	public partial class Aircraft : IAircraft
	{
		static Aircraft()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Aircraft), typeof(IAircraft)), typeof(Aircraft));
		}
	}

	public interface IAircraft
	{
	}
}
