using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFlightpakFuel))]
	public partial class FlightpakFuel : IFlightpakFuel
	{
		static FlightpakFuel()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FlightpakFuel), typeof(IFlightpakFuel)), typeof(FlightpakFuel));
		}
	}

	public interface IFlightpakFuel
	{
	}
}
