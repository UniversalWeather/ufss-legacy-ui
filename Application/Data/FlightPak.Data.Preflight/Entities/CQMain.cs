using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ICQMain))]
	public partial class CQMain : ICQMain
	{
		static CQMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CQMain), typeof(ICQMain)), typeof(CQMain));
		}
	}

	public interface ICQMain
	{
	}
}
