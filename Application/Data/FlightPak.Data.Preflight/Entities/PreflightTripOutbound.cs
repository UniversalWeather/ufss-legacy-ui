using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightTripOutbound))]
	public partial class PreflightTripOutbound : IPreflightTripOutbound
	{
		static PreflightTripOutbound()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightTripOutbound), typeof(IPreflightTripOutbound)), typeof(PreflightTripOutbound));
		}
	}

	public interface IPreflightTripOutbound
	{
	}
}
