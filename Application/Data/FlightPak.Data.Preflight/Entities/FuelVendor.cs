using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFuelVendor))]
	public partial class FuelVendor : IFuelVendor
	{
		static FuelVendor()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FuelVendor), typeof(IFuelVendor)), typeof(FuelVendor));
		}
	}

	public interface IFuelVendor
	{
	}
}
