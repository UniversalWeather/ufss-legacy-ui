using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightTripSIFL))]
	public partial class PreflightTripSIFL : IPreflightTripSIFL
	{
		static PreflightTripSIFL()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightTripSIFL), typeof(IPreflightTripSIFL)), typeof(PreflightTripSIFL));
		}
	}

	public interface IPreflightTripSIFL
	{
	}
}
