using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IEmergencyContact))]
	public partial class EmergencyContact : IEmergencyContact
	{
		static EmergencyContact()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(EmergencyContact), typeof(IEmergencyContact)), typeof(EmergencyContact));
		}
	}

	public interface IEmergencyContact
	{
	}
}
