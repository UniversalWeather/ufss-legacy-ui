using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightFBOList))]
	public partial class PreflightFBOList : IPreflightFBOList
	{
		static PreflightFBOList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightFBOList), typeof(IPreflightFBOList)), typeof(PreflightFBOList));
		}
	}

	public interface IPreflightFBOList
	{
	}
}
