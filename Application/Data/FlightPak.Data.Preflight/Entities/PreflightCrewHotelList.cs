using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightCrewHotelList))]
	public partial class PreflightCrewHotelList : IPreflightCrewHotelList
	{
		static PreflightCrewHotelList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightCrewHotelList), typeof(IPreflightCrewHotelList)), typeof(PreflightCrewHotelList));
		}
	}

	public interface IPreflightCrewHotelList
	{
	}
}
