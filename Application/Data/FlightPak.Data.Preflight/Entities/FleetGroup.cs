using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IFleetGroup))]
	public partial class FleetGroup : IFleetGroup
	{
		static FleetGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(FleetGroup), typeof(IFleetGroup)), typeof(FleetGroup));
		}
	}

	public interface IFleetGroup
	{
	}
}
