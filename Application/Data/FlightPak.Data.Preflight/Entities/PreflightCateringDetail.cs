using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(IPreflightCateringDetail))]
	public partial class PreflightCateringDetail : IPreflightCateringDetail
	{
		static PreflightCateringDetail()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(PreflightCateringDetail), typeof(IPreflightCateringDetail)), typeof(PreflightCateringDetail));
		}
	}

	public interface IPreflightCateringDetail
	{
	}
}
