using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.Preflight
{
	[MetadataType(typeof(ICrewGroup))]
	public partial class CrewGroup : ICrewGroup
	{
		static CrewGroup()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CrewGroup), typeof(ICrewGroup)), typeof(CrewGroup));
		}
	}

	public interface ICrewGroup
	{
	}
}
