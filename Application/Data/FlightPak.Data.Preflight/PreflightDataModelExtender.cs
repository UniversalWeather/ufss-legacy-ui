﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace FlightPak.Data.Preflight
{
    public partial class PreflightMain
    {
        //[DataMember]
        //public string DepartICAOID { get; set; }
        //[DataMember]
        //public Int64? DepartAirportID { get; set; }
        //[DataMember]
        //public string DepartAirportName { get; set; }

        [DataMember]
        public string DescLabel { get; set; }

        [DataMember]
        public string HomeBaseAirportICAOID { get; set; }
        [DataMember]
        public Int64 HomeBaseAirportID { get; set; }

        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        [DataMemberAttribute()]
        public TripActionMode Mode { get; set; }

        [DataMemberAttribute()]
        public bool AllowTripToSave { get; set; }

        [DataMemberAttribute()]
        public bool AllowTripToNavigate { get; set; }


        [DataMemberAttribute()]
        public bool isUpdateFuel { get; set; }

        [DataMemberAttribute()]
        public bool isTripSaved { get; set; }

        [DataMemberAttribute()]
        public bool resetLogisticsChecklist { get; set; }


        [DataMemberAttribute()]
        public bool IsViewModelEdited { get; set; }


        /// <summary>
        /// Property to store list of Copy functionalities from POMain to legs. (ID, Desc) will be copied to all legs.
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<PreflightCopyFunctionalty, PreflightCopyInfo> CopyInfoToAllLegs { get; set; }

        //#region Trip Related Linked Attributes
        //[DataMemberAttribute()]
        //public string HombaseCD { get; set; }
        //[DataMemberAttribute()]
        //public string HombaseDescription { get; set; }

        //[DataMemberAttribute()]
        //public string ClientCD { get; set; }
        //[DataMemberAttribute()]
        //public string ClientDescription { get; set; }

        //[DataMemberAttribute()]
        //public string TailNum { get; set; }
        ////public string ClientDescription { get; set; }

        //[DataMemberAttribute()]
        //public string AircraftCD { get; set; }
        //[DataMemberAttribute()]
        //public string AircraftDescription { get; set; }

        //[DataMemberAttribute()]
        //public string CrewCD { get; set; }
        //[DataMemberAttribute()]
        //public string CrewName { get; set; }
        ////[DataMemberAttribue()]
        ////public string CrewMiddleInitial { get; set; }
        ////[DataMemberAttribute()]
        ////public string LastName { get; set; }

        //[DataMemberAttribute()]
        //public string EmergencyContactCD { get; set; }
        //[DataMemberAttribute()]
        //public string EmergencyContactName { get; set; }

        //[DataMemberAttribute()]
        //public string PassengerRequestorCD { get; set; }
        //[DataMemberAttribute()]
        //public string PassengerRequestorName { get; set; }
        //[DataMemberAttribute()]
        //public string PassengerRequestorPhoneNum { get; set; }

        //[DataMemberAttribute()]
        //public string AccountNum { get; set; }
        //[DataMemberAttribute()]
        //public string AccountDescription { get; set; }

        //[DataMemberAttribute()]
        //public string DepartmentCD { get; set; }
        //[DataMemberAttribute()]
        //public string DepartmentName { get; set; }

        //[DataMemberAttribute()]
        //public string AuthorizationCD { get; set; }
        //[DataMemberAttribute()]
        //public string DeptAuthDescription { get; set; }

        //[DataMemberAttribute()]
        //public string DispatcherName { get; set; }


        //#endregion

    }

    /// <summary>
    /// PreflightMain class to hold Trip Leg properties
    /// </summary>
    public partial class PreflightLeg
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        [DataMember]
        public string DescLabelLeg { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }

        [DataMemberAttribute()]
        public bool DepartAirportChanged { get; set; }

        [DataMemberAttribute()]
        public bool ArrivalAirportChanged { get; set; }



        [DataMemberAttribute()]
        public bool DepartAirportChangedUpdateCrew { get; set; }

        [DataMemberAttribute()]
        public bool ArrivalAirportChangedUpdateCrew { get; set; }


        [DataMemberAttribute()]
        public bool DepartAirportChangedUpdatePAX { get; set; }

        [DataMemberAttribute()]
        public bool ArrivalAirportChangedUpdatePAX { get; set; }

        [DataMemberAttribute()]
        public bool IsLegEdited { get; set; }
        //#region Leg Related attributes


        //[DataMemberAttribute()]
        //public string DeptIcaoID { get; set; }
        //[DataMemberAttribute()]
        //public string DeptAirportName { get; set; }


        //[DataMemberAttribute()]
        //public string ArrivalIcaoID { get; set; }
        //[DataMemberAttribute()]
        //public string ArrivalAirportName { get; set; }


        //[DataMemberAttribute()]
        //public string CrewDutyRuleCD { get; set; }
        ////[DataMemberAttribute()]
        ////public string CrewDutyRulesDescription { get; set; }


        //[DataMemberAttribute()]
        //public string PassengerRequestorCD { get; set; }
        //[DataMemberAttribute()]
        //public string PassengerRequestorName { get; set; }
        ////[DataMemberAttribute()]
        ////public string PassengerRequestorPhoneNum { get; set; }

        //[DataMemberAttribute()]
        //public string AccountNum { get; set; }
        //[DataMemberAttribute()]
        //public string AccountDescription { get; set; }

        //[DataMemberAttribute()]
        //public string DepartmentCD { get; set; }
        //[DataMemberAttribute()]
        //public string DeptName { get; set; }

        //[DataMemberAttribute()]
        //public string AuthorizationCD { get; set; }
        //[DataMemberAttribute()]
        //public string DeptAuthDescription { get; set; }

        //[DataMemberAttribute()]
        //public string ClientCD { get; set; }
        //[DataMemberAttribute()]
        //public string ClientDescription { get; set; }


        //[DataMemberAttribute()]
        //public string FlightCatagoryCD { get; set; }
        ////[DataMemberAttribute()]
        ////public string ClientDescription { get; set; }

        //#endregion


    }

    /// <summary>
    /// PreflightLeg class to hold Trip Crew properties
    /// </summary>
    public partial class PreflightCrewList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightCrew class to hold Crew Hotel properties
    /// </summary>
    public partial class PreflightCrewHotelList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightLeg class to hold Trip transport properties
    /// </summary>
    public partial class PreflightTransportList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }


    
    /// <summary>
    /// PreflightHotelList class to hold Trip Hotel properties
    /// </summary>
    public partial class PreflightHotelList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Hotel info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
        [DataMemberAttribute()]
        public List<Int64?> CrewIDList;
        [DataMemberAttribute()]
        public List<Int64?> PAXIDList;

        [DataMemberAttribute()]
        public Int64 HotelIdentifier;
        [DataMemberAttribute()]
        public string HotelCode { get; set; }
        [DataMemberAttribute()]
        public string CountryCode { get; set; }
    }

    




    /// <summary>
    /// PreflightLeg class to hold Trip FBO properties
    /// </summary>
    public partial class PreflightFBOList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightLeg class to hold Trip Catering properties
    /// </summary>
    public partial class PreflightCateringDetail
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightLeg class to hold Trip OutBound properties
    /// </summary>
    public partial class PreflightTripOutbound
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightLeg class to hold Trip PreflightPassengerList properties
    /// </summary>
    public partial class PreflightPassengerList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightLeg class to hold Trip PreflightPassengerList properties
    /// </summary>
    public partial class PreflightTripSIFL
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// PreflightLeg class to hold Trip PreflightPassengerHotelList properties
    /// </summary>
    public partial class PreflightPassengerHotelList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }



    /// <summary>
    /// PreflightLeg class to hold Trip CheckList properties
    /// </summary>
    public partial class PreflightCheckList
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }

    /// <summary>
    /// UWAPermit class to hold its state properties
    /// </summary>
    public partial class UWAPermit
    {
        [DataMemberAttribute()]
        public TripEntityState State { get; set; }

        /// <summary>
        /// Property to be used in UI to display Leg info as description
        /// </summary>
        [DataMemberAttribute()]
        public string Description { get; set; }
    }


    public partial class ExceptionTemplate
    {

        //[DataMemberAttribute()]
        //public PreflightExceptionCheckList ExceptionChecklist { get; set; }

        //[DataMemberAttribute()]
        //public PreflightExceptionModule ExceptionModule { get; set; }

        //[DataMemberAttribute()]
        //public PreflightExceptionSubModule ExceptionSubModule { get; set; }

    }


    /// <summary>
    /// Enum to identify if the Postflight entity (Leg, Passenger, Leg Expense, Crew has been added, modified or deleted)
    /// </summary>
    [DataContract(Name = "TripEntityState")]
    public enum TripEntityState
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Added = 1,
        [EnumMember]
        Modified = 2,
        [EnumMember]
        Deleted = 3
    }

    /// <summary>
    /// Enum to identify if the Postflight Trip is in Edit mode for record locking
    /// </summary>
    [DataContract(Name = "TripActionMode")]
    public enum TripActionMode
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Edit = 1,
        [EnumMember]
        Saving = 2,
        [EnumMember]
        Cancelled = 3
    }



    /// <summary>
    /// Enum to identify if the Preflight Copy Info functionality
    /// </summary>
    [DataContract(Name = "PreflightCopyFunctionalty")]
    public enum PreflightCopyFunctionalty
    {
        [EnumMember]
        Requestor = 0,
        [EnumMember]
        Department = 1,
        [EnumMember]
        Authorization = 2,
        [EnumMember]
        Description = 3,
        [EnumMember]
        Account = 4,
        [EnumMember]
        FlightCategory = 5,
        [EnumMember]
        CrewDutyRule = 6,
        [EnumMember]
        CQCustomer = 7,
    }

    /// <summary>
    /// Class to store Preflight Copy information to all legs
    /// </summary>
    [DataContract(Name = "PreflightCopyInfo")]
    public class PreflightCopyInfo
    {
        [DataMemberAttribute()]
        public bool IsCopied { get; set; }

        [DataMemberAttribute()]
        public Int64 IDToCopy { get; set; }

        [DataMemberAttribute()]
        public string CDToCopy { get; set; }

        [DataMemberAttribute()]
        public string DescToCopy { get; set; }

        [DataMemberAttribute()]
        public Dictionary<string, string> OtherCopyDetails { get; set; }
    }


    [DataContract(Name = "PreflightExceptionCheckList")]
    public enum PreflightExceptionCheckList
    {
        [EnumMember]
        AircraftTypeReq = 1000,
        [EnumMember]
        ArrivalICAOIDReq = 1001,
        [EnumMember]
        DepartureICAOIDReq = 1002,
        [EnumMember]
        LegDistbeyondAircraftCapability = 1003,
        [EnumMember]
        CurrentandPreviousLegExceedsDefaultsSystemRange = 1004,
        [EnumMember]
        CurrentLegArrivalDateTimeOverlapsNextLegDepartureDateTime = 1005,
        [EnumMember]
        TripRequiresAtleastoneLeg = 1006,
        [EnumMember]
        ArrivalAirportMaxRunwayBelowAircraftRequirements = 1007,
        [EnumMember]
        ArrivalAirportInActive = 1008,
        [EnumMember]
        DepartureAirportInActive = 1009,
        [EnumMember]
        HomebaseRequired = 1010,
        [EnumMember]
        TailNoRequired = 1011,
        [EnumMember]
        DepartDateRequired = 1012,
        [EnumMember]
        ClientCodeRequired = 1013,
        [EnumMember]
        LocalDepartDateRequired = 1014,
        [EnumMember]
        LocalArrivalDateRequired = 1015,
        [EnumMember]
        LegClientCodeRequired = 1016,
        [EnumMember]
        CurrentLegLocalDepartDateTimeOverlapsPrevLegLocalArrivalDateTime = 1017,
        [EnumMember]
        DutyTypeNotAvailableForCrew = 1018,
        [EnumMember]
        LegTimeAloftAircraftCapability = 1019,
        [EnumMember]
        AircraftFARRuleviolationexistsAircraftnotassignedtoFAR = 1020,
        [EnumMember]
        AircraftConflictExistsForLeg = 1021,
        [EnumMember]
        NumberofPassengersExceedsAircraftSeatCapacity = 1022,
        [EnumMember]
        PassengerPassportExpire = 1023,
        [EnumMember]
        CrewPassportExpire = 1024,
        [EnumMember]
        CrewisNotActiveAndIsAssignedonLeg = 1025,
        [EnumMember]
        CrewChecklistConflicts = 1026,
        [EnumMember]
        CrewTypeRating = 1027,
        [EnumMember]
        CrewConflicts = 1028,
        [EnumMember]
        PaxConflicts = 1029,
        [EnumMember]
        LegInspection = 1030,
        [EnumMember]
        CrewCodeflyinglegnumnotqualifiedCrewDutyRule = 1031,
        [EnumMember]
        CrewNotAssignedToLeg = 1032,
        [EnumMember]
        CrewisAlsoFlyingOnTripFallsWithin24HourPeriod = 1033,
        [EnumMember]
        TripDepartmentInactive = 1034,
        [EnumMember]
        TripAuthorizationInactive = 1035,
        [EnumMember]
        LegDepartmentInactive = 1036,
        [EnumMember]
        LegAuthorizationInactive = 1037,
        [EnumMember]
        CrewNoPassportIntLeg = 1038,
        [EnumMember]
        PAXNoPassportIntLeg = 1039,
        [EnumMember]
        PAXNoChoicePassportIntLeg = 1040,
        [EnumMember]
        CrewNoChoicePassportIntLeg = 1041,
        [EnumMember]
        DepatureICAOShouldBeSameAsPreviousLegArrICAO = 1042,
        [EnumMember]
        LogisticArrivalFBOICAOId = 1043,
        [EnumMember]
        LogisticDepatureFBOICAOId = 1044,
        [EnumMember]
        NumberOfPassengerExceedAirCraftCapacity = 1045,
        [EnumMember]
        PICNotAssignedToCrewInLeg = 1046,
        [EnumMember]
        FlightCategoryInactive = 1047

    }

    [DataContract(Name = "PreflightExceptionModule")]
    public enum PreflightExceptionModule
    {
        [EnumMember]
        Preflight = 10011,
    }

    [DataContract(Name = "PreflightExceptionSubModule")]
    public enum PreflightExceptionSubModule
    {
        [EnumMember]
        PreflightMain = 1,
        [EnumMember]
        Legs = 2,
        [EnumMember]
        Crew = 3,
        [EnumMember]
        Passenger = 4,
        [EnumMember]
        FBO = 5,
        [EnumMember]
        Catering = 6,
        [EnumMember]
        Fuel = 7,
        [EnumMember]
        UWA_Services = 8,
        [EnumMember]
        Corporate_Request = 9
    }





}
