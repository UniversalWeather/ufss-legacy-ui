using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ITransport))]
	public partial class Transport : ITransport
	{
		static Transport()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Transport), typeof(ITransport)), typeof(Transport));
		}
	}

	public interface ITransport
	{
	}
}
