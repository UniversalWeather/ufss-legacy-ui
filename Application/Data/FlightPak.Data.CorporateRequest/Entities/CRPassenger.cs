using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRPassenger))]
	public partial class CRPassenger : ICRPassenger
	{
		static CRPassenger()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRPassenger), typeof(ICRPassenger)), typeof(CRPassenger));
		}
	}

	public interface ICRPassenger
	{
	}
}
