using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRTransportList))]
	public partial class CRTransportList : ICRTransportList
	{
		static CRTransportList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRTransportList), typeof(ICRTransportList)), typeof(CRTransportList));
		}
	}

	public interface ICRTransportList
	{
	}
}
