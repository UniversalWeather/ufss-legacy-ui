using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRCateringList))]
	public partial class CRCateringList : ICRCateringList
	{
		static CRCateringList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRCateringList), typeof(ICRCateringList)), typeof(CRCateringList));
		}
	}

	public interface ICRCateringList
	{
	}
}
