using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRDispatchNote))]
	public partial class CRDispatchNote : ICRDispatchNote
	{
		static CRDispatchNote()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRDispatchNote), typeof(ICRDispatchNote)), typeof(CRDispatchNote));
		}
	}

	public interface ICRDispatchNote
	{
	}
}
