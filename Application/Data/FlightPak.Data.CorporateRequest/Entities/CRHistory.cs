using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRHistory))]
	public partial class CRHistory : ICRHistory
	{
		static CRHistory()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRHistory), typeof(ICRHistory)), typeof(CRHistory));
		}
	}

	public interface ICRHistory
	{
	}
}
