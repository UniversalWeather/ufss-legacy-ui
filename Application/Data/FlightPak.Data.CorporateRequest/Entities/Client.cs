using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(IClient))]
	public partial class Client : IClient
	{
		static Client()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Client), typeof(IClient)), typeof(Client));
		}
	}

	public interface IClient
	{
	}
}
