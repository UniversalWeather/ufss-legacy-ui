using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRHotelList))]
	public partial class CRHotelList : ICRHotelList
	{
		static CRHotelList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRHotelList), typeof(ICRHotelList)), typeof(CRHotelList));
		}
	}

	public interface ICRHotelList
	{
	}
}
