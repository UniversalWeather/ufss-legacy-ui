using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(IPassenger))]
	public partial class Passenger : IPassenger
	{
		static Passenger()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Passenger), typeof(IPassenger)), typeof(Passenger));
		}
	}

	public interface IPassenger
	{
	}
}
