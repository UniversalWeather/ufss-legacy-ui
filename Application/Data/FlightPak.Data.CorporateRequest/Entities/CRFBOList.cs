using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRFBOList))]
	public partial class CRFBOList : ICRFBOList
	{
		static CRFBOList()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRFBOList), typeof(ICRFBOList)), typeof(CRFBOList));
		}
	}

	public interface ICRFBOList
	{
	}
}
