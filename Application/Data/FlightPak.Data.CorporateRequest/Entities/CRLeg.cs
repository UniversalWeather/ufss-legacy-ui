using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRLeg))]
	public partial class CRLeg : ICRLeg
	{
		static CRLeg()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRLeg), typeof(ICRLeg)), typeof(CRLeg));
		}
	}

	public interface ICRLeg
	{

        [Required(AllowEmptyStrings = false, ErrorMessage = "Departs ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> DAirportID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrives ICAO" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> AAirportID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Departure" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> DepartureDTTMLocal { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Arrival" + DataValidation.IsRequired)]
        Nullable<global::System.DateTime> ArrivalDTTMLocal { get; set; }
	}
}
