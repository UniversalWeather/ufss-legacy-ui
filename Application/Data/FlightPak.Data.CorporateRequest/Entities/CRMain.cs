using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using FlightPak.Common.Constants;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICRMain))]
	public partial class CRMain : ICRMain
	{
		static CRMain()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(CRMain), typeof(ICRMain)), typeof(CRMain));
		}
	}

	public interface ICRMain
	{
        [Required(AllowEmptyStrings = false, ErrorMessage = "Type Code" + DataValidation.IsRequired)]
        Nullable<global::System.Int64> AircraftID { get; set; }
	}
}
