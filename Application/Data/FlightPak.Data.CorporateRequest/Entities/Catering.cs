using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICatering))]
	public partial class Catering : ICatering
	{
		static Catering()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Catering), typeof(ICatering)), typeof(Catering));
		}
	}

	public interface ICatering
	{
	}
}
