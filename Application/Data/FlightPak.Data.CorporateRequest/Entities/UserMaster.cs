using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(IUserMaster))]
	public partial class UserMaster : IUserMaster
	{
		static UserMaster()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(UserMaster), typeof(IUserMaster)), typeof(UserMaster));
		}
	}

	public interface IUserMaster
	{
	}
}
