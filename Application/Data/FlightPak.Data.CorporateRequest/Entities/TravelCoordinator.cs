using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ITravelCoordinator))]
	public partial class TravelCoordinator : ITravelCoordinator
	{
		static TravelCoordinator()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TravelCoordinator), typeof(ITravelCoordinator)), typeof(TravelCoordinator));
		}
	}

	public interface ITravelCoordinator
	{
	}
}
