using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace FlightPak.Data.CorporateRequest
{
	[MetadataType(typeof(ICompany))]
	public partial class Company : ICompany
	{
		static Company()
		{
			TypeDescriptor.AddProviderTransparent(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Company), typeof(ICompany)), typeof(Company));
		}
	}

	public interface ICompany
	{
	}
}
