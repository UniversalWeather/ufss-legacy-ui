﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;


namespace FlightPak.Data.CorporateRequest
{
    //public partial class CorporateRequest
    //{
        
    //}

    public partial class CRMain
    {
        [DataMember]
        public string HomeBaseAirportICAOID { get; set; }
        [DataMember]
        public Int64 HomeBaseAirportID { get; set; }

        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }

        [DataMemberAttribute()]
        public CorporateRequestTripActionMode Mode { get; set; }

        [DataMemberAttribute()]
        public bool AllowTripToSave { get; set; }

        [DataMemberAttribute()]
        public bool AllowTripToNavigate { get; set; }

        [DataMemberAttribute()]
        public bool isTripSaved { get; set; }
        /// <summary>
        /// Property to store list of Copy functionalities from POMain to legs. (ID, Desc) will be copied to all legs.
        /// </summary>
        [DataMemberAttribute()]
        public Dictionary<CorporateRequestCopyFunctionalty, CorporateRequestCopyInfo> CopyInfoToAllLegs { get; set; }
    }


    /// <summary>
    /// Enum to identify if the Preflight Copy Info functionality
    /// </summary>
    [DataContract(Name = "CorporateRequestCopyFunctionalty")]
    public enum CorporateRequestCopyFunctionalty
    {
        [EnumMember]
        Requestor = 0,
        [EnumMember]
        Department = 1,
        [EnumMember]
        Authorization = 2,
        [EnumMember]
        Description = 3,
        [EnumMember]
        Account = 4,
        [EnumMember]
        FlightCategory = 5      
    }

    /// <summary>
    /// Class to store Preflight Copy information to all legs
    /// </summary>
    [DataContract(Name = "CorporateRequestCopyInfo")]
    public class CorporateRequestCopyInfo
    {
        [DataMemberAttribute()]
        public bool IsCopied { get; set; }

        [DataMemberAttribute()]
        public Int64 IDToCopy { get; set; }

        [DataMemberAttribute()]
        public string CDToCopy { get; set; }

        [DataMemberAttribute()]
        public string DescToCopy { get; set; }

        [DataMemberAttribute()]
        public Dictionary<string, string> OtherCopyDetails { get; set; }
    }


    /// <summary>
    /// PreflightMain class to hold Trip Leg properties
    /// </summary>
    public partial class CRLeg
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }

        [DataMemberAttribute()]
        public bool DepartAirportChanged { get; set; }

        [DataMemberAttribute()]
        public bool ArrivalAirportChanged { get; set; }
    }

     /// <summary>
    /// Enum to identify if the Corporate Request entity (Leg, Passenger has been added, modified or deleted)
    /// </summary>
    [DataContract(Name = "CorporateRequestTripEntityState")]
    public enum CorporateRequestTripEntityState
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Added = 1,
        [EnumMember]
        Modified = 2,
        [EnumMember]
        Deleted = 3
    }

    /// <summary>
    /// Enum to identify if the  Corporate Request  Trip is in Edit mode for record locking
    /// </summary>
    [DataContract(Name = "CorporateRequestTripActionMode")]
    public enum CorporateRequestTripActionMode
    {
        [EnumMember]
        NoChange = 0,
        [EnumMember]
        Edit = 1,
        [EnumMember]
        Saving = 2,
        [EnumMember]
        Cancelled = 3
    }


      /// <summary>
    /// CorporateRequest class to hold Trip transport properties
    /// </summary>
    public partial class CRHotelList
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }
    }

    public partial class CRDispatchNote
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }
    }


    public partial class CRTransportList
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }
    }

    /// <summary>
    /// CorporateRequest class to hold Trip CorporateRequestPassengerList properties
    /// </summary>
    public partial class CRPassenger
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }
    }

    /// <summary>
    /// CorporateRequest class to hold Trip CorporateRequestFBO List properties
    /// </summary>
    public partial class CRFBOList
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }
    }

    /// <summary>
    /// CorporateRequest class to hold Trip CorporateRequestCatering List properties
    /// </summary>
    public partial class CRCateringList
    {
        [DataMemberAttribute()]
        public CorporateRequestTripEntityState State { get; set; }
    }

    [DataContract(Name = "CorporateRequestExceptionModule")]
    public enum CorporateRequestExceptionModule
    {
        [EnumMember]
        CRMain = 10002,
    }

    [DataContract(Name = "CorporateRequestExceptionSubModule")]
    public enum CorporateRequestExceptionSubModule
    {
        [EnumMember]
        CRmain = 1,
        [EnumMember]
        Legs = 2,       
        [EnumMember]
        Passenger = 3,
        [EnumMember]
        FBO = 5,
        [EnumMember]
        Catering = 6
    }

    [DataContract(Name = "CorporateRequestExceptionCheckList")]
    public enum CorporateRequestExceptionCheckList
    {
        [EnumMember]
        AircraftTypeReq = 4000,
        [EnumMember]
        ArrivalICAOIDReq = 4001,
        [EnumMember]
        DepartureICAOIDReq = 4002,
        [EnumMember]
        LegDistbeyondAircraftCapability = 4003,
        [EnumMember]
        CurrentandPreviousLegExceedsDefaultsSystemRange = 4004,
        [EnumMember]
        CurrentLegArrivalDateTimeOverlapsNextLegDepartureDateTime = 4005,
        [EnumMember]
        RequestRequiresAtleastoneLeg = 4006,
        [EnumMember]
        ArrivalAirportMaxRunwayBelowAircraftRequirements = 4007,
        [EnumMember]
        ArrivalAirportInActive = 4008,
        [EnumMember]
        DepartureAirportInActive = 4009,
        [EnumMember]
        HomebaseRequired = 1010,
        [EnumMember]
        TailNoRequired = 1011,
        [EnumMember]
        DepartDateRequired = 4010,
        [EnumMember]
        ClientCodeRequired = 1013,
        [EnumMember]
        LocalDepartDateRequired = 4011,
        [EnumMember]
        LocalArrivalDateRequired = 4012,
        [EnumMember]
        LegClientCodeRequired = 1016,
        [EnumMember]
        CurrentLegLocalDepartDateTimeOverlapsPrevLegLocalArrivalDateTime = 4013,       
        [EnumMember]
        LegTimeAloftAircraftCapability = 4014,
        //[EnumMember]
        //AircraftFARRuleviolationexistsAircraftnotassignedtoFAR = 4015,
        [EnumMember]
        AircraftConflictExistsForLeg = 4017,
        [EnumMember]
        NumberofPassengersExceedsAircraftSeatCapacity = 4022, 
        [EnumMember]
        RequestDepartmentInactive = 4018,
        [EnumMember]
        TripAuthorizationInactive = 4019,
        [EnumMember]
        LegDepartmentInactive = 4020,
        [EnumMember]
        LegAuthorizationInactive = 4021,
    }


}
